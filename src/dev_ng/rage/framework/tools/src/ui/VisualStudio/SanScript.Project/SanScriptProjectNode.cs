﻿namespace SanScript.Project
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.IO;
    using System.Reflection;
    using System.Runtime.InteropServices;
    using System.Windows.Forms;
    using Microsoft.Build.Execution;
    using Microsoft.VisualStudio;
    using Microsoft.VisualStudio.Project;
    using Microsoft.VisualStudio.Shell.Interop;
    using SanScript.Project.Library;
    using OleConstants = Microsoft.VisualStudio.OLE.Interop.Constants;
    using VsCommands = Microsoft.VisualStudio.VSConstants.VSStd97CmdID;
    using VsCommands2K = Microsoft.VisualStudio.VSConstants.VSStd2KCmdID;

    /// <summary>
    /// Represents the SanScript project node inside the solution explorer.
    /// </summary>
    public class SanScriptProjectNode : ProjectNode
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="ImageIndex"/> property.
        /// </summary>
        private static int _imageOffset;

        /// <summary>
        /// The private image list for this hierarchy node.
        /// </summary>
        private static ImageList _sanScriptImageList;

        /// <summary>
        /// A private reference to the package that owns the factory that created this node.
        /// </summary>
        private SanScriptProjectPackage _package;

        /// <summary>
        /// A private value indicating whether the 
        /// </summary>
        private bool _buildingSelected;
        #endregion Fields

        #region Constuctors
        /// <summary>
        /// Initialises static members of the <see cref="SanScriptProjectNode"/> class.
        /// </summary>
        static SanScriptProjectNode()
        {
            string path = "SanScript.Project.Resources.SanScriptImageList.bmp";
            Assembly assembly = typeof(SanScriptProjectNode).Assembly;

            _sanScriptImageList =
                Utilities.GetImageList(assembly.GetManifestResourceStream(path));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="SanScriptProjectNode"/> class.
        /// </summary>
        /// <param name="package">
        /// The package who owns the factory that created this project.
        /// </param>
        public SanScriptProjectNode(SanScriptProjectPackage package)
        {
            this._package = package;
            _imageOffset = this.ImageHandler.ImageList.Images.Count;
            this.CanProjectDeleteItems = true;

            foreach (Image img in _sanScriptImageList.Images)
            {
                this.ImageHandler.AddImage(img);
            }
        }
        #endregion Constuctors

        #region Properties
        /// <summary>
        /// Gets the offset of the project node bitmap in the image list.
        /// </summary>
        public override int ImageIndex
        {
            get { return _imageOffset; }
        }

        /// <summary>
        /// Gets the unique identifier used to register the project factory under
        /// HKLM\Software\Microsoft\VisualStudio\%version%\Projects.
        /// </summary>
        public override System.Guid ProjectGuid
        {
            get { return typeof(SanScriptProjectFactory).GUID; }
        }

        /// <summary>
        /// Gets a caption for VSHPROPID_TypeName.
        /// </summary>
        public override string ProjectType
        {
            get { return "SanScriptProject"; }
        }

        /// <summary>
        /// Gets the offset of the project node bitmap in the image list.
        /// </summary>
        internal static int ImageOffset
        {
            get { return _imageOffset + (int)SanScriptImageName.Project; }
        }

        /// <summary>
        /// Gets the marshalled hierarchy from this object.
        /// </summary>
        private IVsHierarchy InteropSafeHierarchy
        {
            get
            {
                IntPtr unknown = Utilities.QueryInterfaceIUnknown(this);
                if (unknown == IntPtr.Zero)
                {
                    return null;
                }

                IVsHierarchy hierarchy = Marshal.GetObjectForIUnknown(unknown) as IVsHierarchy;
                return hierarchy;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Overriding to provide customisation of files on add files. This will replace tokens
        /// in the file with actual values.
        /// </summary>
        /// <param name="source">
        /// The fill path to the template file.
        /// </param>
        /// <param name="target">
        /// The fill path to the destination file.
        /// </param>
        public override void AddFileFromTemplate(string source, string target)
        {
            if (!File.Exists(source))
            {
                string msg = String.Format("Template file not found: {0}", source);
                throw new FileNotFoundException(msg);
            }

            string targetDirectory = Path.GetDirectoryName(target);
            if (!Directory.Exists(targetDirectory))
            {
                Directory.CreateDirectory(targetDirectory);
            }

            File.Copy(source, target);
        }

        /// <summary>
        /// Closes the project node.
        /// </summary>
        /// <returns>
        /// A success or failure value.
        /// </returns>
        public override int Close()
        {
            if (this.Site != null)
            {
                Type type = typeof(ISanScriptLibraryManager);
                object service = this.Site.GetService(type);
                ISanScriptLibraryManager libraryManager = service as ISanScriptLibraryManager;
                if (libraryManager != null)
                {
                    libraryManager.UnregisterHierarchy(this.InteropSafeHierarchy);
                }
            }

            return base.Close();
        }

        /// <summary>
        /// Creates a file node based on the specified project element.
        /// </summary>
        /// <param name="item">
        /// The msbuild item to be analyzed
        /// </param>
        /// <returns>
        /// A new <see cref="SanScriptFileNode"/> or FileNode object.
        /// </returns>
        public override FileNode CreateFileNode(ProjectElement item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            SanScriptFileNode newNode = new SanScriptFileNode(this, item);
            return newNode;
        }

        /// <summary>
        /// Gets the GUID value of the node.
        /// </summary>
        /// <param name="propid">
        /// A __VSHPROPID or __VSHPROPID2 value of the guid property.
        /// </param>
        /// <param name="guid">
        /// The guid to return for the property.
        /// </param>
        /// <returns>
        /// A success or failure value.
        /// </returns>
        public override int GetGuidProperty(int propid, out Guid guid)
        {
            if ((__VSHPROPID)propid == __VSHPROPID.VSHPROPID_PreferredLanguageSID)
            {
                guid = new Guid(GuidList.guidSanScriptLanguageString);
            }
            else
            {
                return base.GetGuidProperty(propid, out guid);
            }

            return VSConstants.S_OK;
        }

        /// <summary>
        /// Factory method for reference container node
        /// </summary>
        /// <returns>ReferenceContainerNode created</returns>
        protected override ReferenceContainerNode CreateReferenceContainerNode()
        {
            return null;
        }

        /// <summary>
        /// Loads a project file. Called from the factory CreateProject to load the project.
        /// </summary>
        /// <param name="fileName">
        /// File name of the project that will be created.
        /// </param>
        /// <param name="location">
        /// Location where the project will be created.
        /// </param>
        /// <param name="name">
        /// If applicable, the name of the template to use when cloning a new project.
        /// </param>
        /// <param name="flags">
        /// Set of flag values taken from the VSCREATEPROJFLAGS enumeration.
        /// </param>
        /// <param name="project">
        /// Identifier of the interface that the caller wants returned.
        /// </param>
        /// <param name="canceled">
        /// An out parameter specifying if the project creation was canceled
        /// </param>
        public override void Load(
            string filename,
            string location,
            string name,
            uint flags,
            ref Guid project,
            out int canceled)
        {
            base.Load(filename, location, name, flags, ref project, out canceled);

            Type type = typeof(ISanScriptLibraryManager);
            object service = this.Site.GetService(type);
            ISanScriptLibraryManager libraryManager = service as ISanScriptLibraryManager;
            if (libraryManager != null)
            {
                libraryManager.RegisterHierarchy(this.InteropSafeHierarchy);
            }
        }

        /// <summary>
        /// Handles menus originating from IOleCommandTarget.
        /// </summary>
        /// <param name="cmdGroup">
        /// Unique identifier of the command group
        /// </param>
        /// <param name="cmd">
        /// The command to be executed.
        /// </param>
        /// <param name="handled">
        /// Specifies whether the menu was handled.
        /// </param>
        /// <returns>
        /// A QueryStatusResult describing the status of the menu.
        /// </returns>
        protected override QueryStatusResult QueryStatusCommandFromOleCommandTarget(Guid cmdGroup, uint cmd, out bool handled)
        {
            handled = false;
            if (cmdGroup == VsMenus.guidStandardCommandSet2K)
            {
                switch ((VsCommands2K)cmd)
                {
                    case VsCommands2K.ADDREFERENCE:
                        return QueryStatusResult.NOTSUPPORTED;
                    case VsCommands2K.COMPILE:
                        var selectedItems = this.GetSelectedNodes();
                        QueryStatusResult result = QueryStatusResult.SUPPORTED | QueryStatusResult.ENABLED;
                        foreach (var selectedItem in selectedItems)
                        {
                            SanScriptFileNode node = selectedItem as SanScriptFileNode;
                            if (node == null)
                            {
                                result = QueryStatusResult.NOTSUPPORTED | QueryStatusResult.INVISIBLE;
                                break;
                            }

                            Uri uri = new Uri(node.Url);
                            if (!uri.IsFile)
                            {
                                result = QueryStatusResult.NOTSUPPORTED | QueryStatusResult.INVISIBLE;
                                break;
                            }

                            bool hasExtension = Path.HasExtension(uri.AbsolutePath);
                            if (!hasExtension)
                            {
                                result = QueryStatusResult.NOTSUPPORTED | QueryStatusResult.INVISIBLE;
                                break;
                            }

                            if (!String.Equals(Path.GetExtension(uri.AbsolutePath), ".sc"))
                            {
                                result = QueryStatusResult.NOTSUPPORTED | QueryStatusResult.INVISIBLE;
                                break;
                            }
                        }

                        handled = true;
                        return result;
                }
            }

            QueryStatusResult baseResult = base.QueryStatusCommandFromOleCommandTarget(cmdGroup, cmd, out handled);
            return baseResult;
        }

        /// <summary>
        /// Executes a command that can only be executed once the whole selection is known.
        /// </summary>
        /// <param name="cmdGroup">Unique identifier of the command group</param>
        /// <param name="cmdId">The command to be executed.</param>
        /// <param name="cmdExecOpt">Values describe how the object should execute the command.</param>
        /// <param name="vaIn">Pointer to a VARIANTARG structure containing input arguments. Can be NULL</param>
        /// <param name="vaOut">VARIANTARG structure to receive command output. Can be NULL.</param>
        /// <param name="commandOrigin">The origin of the command. From IOleCommandTarget or hierarchy.</param>
        /// <param name="selectedNodes">The list of the selected nodes.</param>
        /// <param name="handled">An out parameter specifying that the command was handled.</param>
        /// <returns>If the method succeeds, it returns S_OK. If it fails, it returns an error code.</returns>
        protected override int ExecCommandThatDependsOnSelectedNodes(Guid cmdGroup, uint cmdId, uint cmdExecOpt, IntPtr vaIn, IntPtr vaOut, CommandOrigin commandOrigin, System.Collections.Generic.IList<HierarchyNode> selectedNodes, out bool handled)
        {
            if (cmdGroup == VsMenus.guidStandardCommandSet2K)
            {
                switch ((VsCommands2K)cmdId)
                {
                    case VsCommands2K.COMPILE:
                        if (_buildingSelected == false)
                        {
                            try
                            {
                                _buildingSelected = true;

                                IVsOutputWindow output = (IVsOutputWindow)this.ProjectMgr.Site.GetService(typeof(SVsOutputWindow));
                                IVsOutputWindowPane pane;
                                Guid paneGuid = VSConstants.OutputWindowPaneGuid.BuildOutputPane_guid;
                                output.GetPane(ref paneGuid, out pane);

                                BackgroundWorker worker = new BackgroundWorker();
                                worker.DoWork += new DoWorkEventHandler(BuildSelected);
                                worker.RunWorkerAsync(pane);
                            }
                            finally
                            {
                                _buildingSelected = false;
                            }
                        }

                        handled = true;
                        return VSConstants.S_OK;
                }
            }

            handled = false;
            return base.ExecCommandThatDependsOnSelectedNodes(cmdGroup, cmdId, cmdExecOpt, vaIn, vaOut, commandOrigin, selectedNodes, out handled);
        }

        private void BuildSelected(object sender, DoWorkEventArgs e)
        {
            IVsOutputWindowPane pane = (IVsOutputWindowPane)e.Argument;
            if (pane == null)
            {
                this.Build("CompileFiles");
            }
            else
            {
                pane.Clear();
                pane.Activate();
                //pane.OutputString(String.Format("------ Build started: File count: {0} ------"));
                this.Build("CompileFiles", pane);
            }
        }

        protected override ProjectInstance HandleBuildConfig(ProjectInstance config)
        {
            //if (!_buildingSelected)
            //{
            //    return base.HandleBuildConfig(config);
            //}
            //else
            //{
                var selectedItems = this.GetSelectedNodes();
                var selectedPaths = new System.Collections.Generic.List<string>();
                Uri directory = new Uri(this.BuildProject.FullPath);
                foreach (var selectedItem in selectedItems)
                {
                    SanScriptFileNode node = selectedItem as SanScriptFileNode;
                    if (node == null)
                    {
                        continue;
                    }

                    Uri uri = new Uri(node.Url);
                    if (!uri.IsFile)
                    {
                        continue;
                    }

                    bool hasExtension = Path.HasExtension(uri.AbsolutePath);
                    if (!hasExtension)
                    {
                        continue;
                    }

                    if (!String.Equals(Path.GetExtension(uri.AbsolutePath), ".sc"))
                    {
                        continue;
                    }

                    selectedPaths.Add(uri.OriginalString.Replace("/", "\\"));
                }

                string value = string.Join(";", selectedPaths);
                config.SetProperty("Selected", value);

                return config;
            //}
        }

        /// <summary>
        /// Returns the configuration dependent property pages.
        /// </summary>
        /// <returns>
        /// The guids of the property pages for this project node.
        /// </returns>
        protected override Guid[] GetConfigurationDependentPropertyPages()
        {
            Guid[] result = new Guid[2];
            result[0] = typeof(SanScriptBuildPropertyPage).GUID;
            result[1] = typeof(BuildEventPropertyPage).GUID;
            return result;
        }

        /// <summary>
        /// Returns the configuration independent property pages.
        /// </summary>
        /// <returns>
        /// The guids of the property pages for this project node.
        /// </returns>
        protected override Guid[] GetConfigurationIndependentPropertyPages()
        {
            Guid[] result = new Guid[1];
            result[0] = typeof(SanScriptGeneralPropertyPage).GUID;
            return result;
        }

        /// <summary>
        /// Returns the priority project designer pages.
        /// </summary>
        /// <returns>
        /// The guids of the property pages for this project node.
        /// </returns>
        protected override Guid[] GetPriorityProjectDesignerPages()
        {
            return new Guid[1];
        }
        #endregion Methods
    } // SanScript.Project.SanScriptProjectNode {Class}
} // SanScript.Project {Namespace}
