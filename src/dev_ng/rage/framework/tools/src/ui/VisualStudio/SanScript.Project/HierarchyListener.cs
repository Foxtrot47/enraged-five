﻿namespace SanScript.Project
{
    using System;
    using System.Diagnostics;
    using Microsoft.VisualStudio;
    using Microsoft.VisualStudio.Shell.Interop;

    /// <summary>
    /// 
    /// </summary>
    internal class HierarchyListener : IVsHierarchyEvents, IDisposable
    {
        private uint cookie;
        private IVsHierarchy hierarchy;
        public HierarchyListener(IVsHierarchy hierarchy)
        {
            if (null == hierarchy)
            {
                throw new ArgumentNullException("hierarchy");
            }
            this.hierarchy = hierarchy;
        }

        public bool IsListening
        {
            get { return (0 != cookie); }
        }

        #region Events
        /// <summary>
        /// 
        /// </summary>
        private EventHandler<HierarchyEventArgs> onItemAdded;

        /// <summary>
        /// 
        /// </summary>
        private EventHandler<HierarchyEventArgs> onItemDeleted;

        /// <summary>
        /// 
        /// </summary>
        public event EventHandler<HierarchyEventArgs> OnAddItem
        {
            add { onItemAdded += value; }
            remove { onItemAdded -= value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public event EventHandler<HierarchyEventArgs> OnDeleteItem
        {
            add { onItemDeleted += value; }
            remove { onItemDeleted -= value; }
        }

        #endregion Events

        #region Methods
        public void StartListening(bool doInitialScan)
        {
            if (0 != cookie)
            {
                return;
            }
            Microsoft.VisualStudio.ErrorHandler.ThrowOnFailure(
                hierarchy.AdviseHierarchyEvents(this, out cookie));
            if (doInitialScan)
            {
                InternalScanHierarchy(VSConstants.VSITEMID_ROOT);
            }
        }

        public void StopListening()
        {
            InternalStopListening(true);
        }

        public void Dispose()
        {
            InternalStopListening(false);
            cookie = 0;
            hierarchy = null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hicon"></param>
        /// <returns></returns>
        public int OnInvalidateIcon(IntPtr hicon)
        {
            // Do Nothing.
            return VSConstants.S_OK;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="itemidParent"></param>
        /// <returns></returns>
        public int OnInvalidateItems(uint itemidParent)
        {
            // TODO: Find out if this event is needed.
            return VSConstants.S_OK;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="itemidParent"></param>
        /// <param name="itemidSiblingPrev"></param>
        /// <param name="itemidAdded"></param>
        /// <returns></returns>
        public int OnItemAdded(uint itemidParent, uint itemidSiblingPrev, uint itemidAdded)
        {
            // Check if the item is a python file.
            string name;
            if (!IsSanScriptFile(itemidAdded, out name))
            {
                return VSConstants.S_OK;
            }

            // This item is a python file, so we can notify that it is added to the hierarchy.
            if (null != onItemAdded)
            {
                HierarchyEventArgs args = new HierarchyEventArgs(itemidAdded, name);
                onItemAdded(hierarchy, args);
            }
            return VSConstants.S_OK;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="itemid"></param>
        /// <returns></returns>
        public int OnItemDeleted(uint itemid)
        {
            // Notify that the item is deleted only if it is a python file.
            string name;
            if (!IsSanScriptFile(itemid, out name))
            {
                return VSConstants.S_OK;
            }
            if (null != onItemDeleted)
            {
                HierarchyEventArgs args = new HierarchyEventArgs(itemid, name);
                onItemDeleted(hierarchy, args);
            }
            return VSConstants.S_OK;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="itemidParent"></param>
        /// <returns></returns>
        public int OnItemsAppended(uint itemidParent)
        {
            // TODO: Find out what this event is about.
            return VSConstants.S_OK;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="itemid"></param>
        /// <param name="propid"></param>
        /// <param name="flags"></param>
        /// <returns></returns>
        public int OnPropertyChanged(uint itemid, int propid, uint flags)
        {
            return VSConstants.S_OK;
        }

        /// <summary>
        /// Gets the item id.
        /// </summary>
        /// <param name="variantValue">VARIANT holding an itemid.</param>
        /// <returns>Item Id of the concerned node</returns>
        private static uint GetItemId(object variantValue)
        {
            if (variantValue == null)
            {
                return VSConstants.VSITEMID_NIL;
            }

            if (variantValue is int)
            {
                return (uint)(int)variantValue;
            }

            if (variantValue is uint)
            {
                return (uint)variantValue;
            }

            if (variantValue is short)
            {
                return (uint)(short)variantValue;
            }

            if (variantValue is ushort)
            {
                return (uint)(ushort)variantValue;
            }

            if (variantValue is long)
            {
                return (uint)(long)variantValue;
            }

            return VSConstants.VSITEMID_NIL;
        }

        /// <summary>
        /// Do a recursive walk on the hierarchy to find all the python files in it.
        /// It will generate an event for every file found.
        /// </summary>
        private void InternalScanHierarchy(uint itemId)
        {
            uint currentItem = itemId;
            while (currentItem != VSConstants.VSITEMID_NIL)
            {
                string itemName;
                if ((null != onItemAdded) && IsSanScriptFile(currentItem, out itemName))
                {
                    HierarchyEventArgs args = new HierarchyEventArgs(currentItem, itemName);
                    onItemAdded(hierarchy, args);
                }

                object propertyValue;
                bool canScanSubitems = true;
                int property = (int)__VSHPROPID.VSHPROPID_HasEnumerationSideEffects; 
                int hr = hierarchy.GetProperty(currentItem, property, out propertyValue);
                if ((hr == VSConstants.S_OK) && (propertyValue is bool))
                {
                    canScanSubitems = !(bool)propertyValue;
                }

                if (canScanSubitems)
                {
                    object child;
                    property = (int)__VSHPROPID.VSHPROPID_FirstChild;
                    hr = hierarchy.GetProperty(currentItem, property, out child);
                    if (VSConstants.S_OK == hr)
                    {
                        this.InternalScanHierarchy(GetItemId(child));
                    }
                }

                object sibling;
                property = (int)__VSHPROPID.VSHPROPID_NextSibling;
                hr = hierarchy.GetProperty(currentItem, property, out sibling);
                if (hr != VSConstants.S_OK)
                {
                    currentItem = VSConstants.VSITEMID_NIL;
                }
                else
                {
                    currentItem = GetItemId(sibling);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="throwOnError"></param>
        /// <returns></returns>
        private bool InternalStopListening(bool throwOnError)
        {
            if ((null != hierarchy) || (0 == cookie))
            {
                return false;
            }
            int hr = hierarchy.UnadviseHierarchyEvents(cookie);
            if (throwOnError)
            {
                Microsoft.VisualStudio.ErrorHandler.ThrowOnFailure(hr);
            }
            cookie = 0;
            return Microsoft.VisualStudio.ErrorHandler.Succeeded(hr);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="itemId">
        /// 
        /// </param>
        /// <param name="canonicalName">
        /// 
        /// </param>
        /// <returns>
        /// 
        /// </returns>
        private bool IsSanScriptFile(uint itemId, out string canonicalName)
        {
            Guid typeGuid = Guid.Empty;
            canonicalName = null;
            int hr = VSConstants.S_OK;
            try
            {
                int propertyId = (int)__VSHPROPID.VSHPROPID_TypeGuid;
                hr = hierarchy.GetGuidProperty(itemId, propertyId, out typeGuid);
            }
            catch (System.Runtime.InteropServices.COMException)
            {
            }

            Guid physicalFileGuid = VSConstants.GUID_ItemType_PhysicalFile;
            if (ErrorHandler.Failed(hr) || physicalFileGuid != typeGuid)
            {
                return false;
            }

            // This item is a file; find if it is a pyhon file.
            hr = hierarchy.GetCanonicalName(itemId, out canonicalName);
            if (Microsoft.VisualStudio.ErrorHandler.Failed(hr))
            {
                return false;
            }

            string extension = System.IO.Path.GetExtension(canonicalName);
            if (String.Compare(extension, ".sc", StringComparison.OrdinalIgnoreCase) == 0)
            {
                return true;
            }

            if (string.Compare(extension, ".sch", StringComparison.OrdinalIgnoreCase) == 0)
            {
                return true;
            }

            return false;
        }
        #endregion Methods
    } // SanScript.Project.HierarchyListener {Class}
} // SanScript.Project {Namespace}
