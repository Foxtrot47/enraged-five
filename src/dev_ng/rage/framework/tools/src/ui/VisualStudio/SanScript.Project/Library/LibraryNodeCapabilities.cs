﻿namespace SanScript.Project.Library
{
    using System;
    using Microsoft.VisualStudio.Shell.Interop;

    /// <summary>
    /// Enumeration of the capabilities of a node. It is possible to combine different values
    /// to support more capabilities.
    /// </summary>
    [Flags]
    public enum LibraryNodeCapabilities
    {
        /// <summary>
        /// Specifies that the library node has no capabilities.
        /// </summary>
        None = _LIB_LISTCAPABILITIES.LLC_NONE,

        /// <summary>
        /// Specifies that the library node has a object contained within it that can be
        /// browsed.
        /// </summary>
        HasBrowseObject = _LIB_LISTCAPABILITIES.LLC_HASBROWSEOBJ,

        /// <summary>
        /// Specifies that the library node supports a description panel.
        /// </summary>
        HasDescriptionPane = _LIB_LISTCAPABILITIES.LLC_HASDESCPANE,

        /// <summary>
        /// Specifies that the library node has a source context.
        /// </summary>
        HasSourceContext = _LIB_LISTCAPABILITIES.LLC_HASSOURCECONTEXT,

        /// <summary>
        /// Specifies that the library node contains commands for it.
        /// </summary>
        HasCommands = _LIB_LISTCAPABILITIES.LLC_HASCOMMANDS,

        /// <summary>
        /// Specifies that the library node allows modifications through drag and drop.
        /// </summary>
        AllowDragDrop = _LIB_LISTCAPABILITIES.LLC_ALLOWDRAGDROP,

        /// <summary>
        /// Specifies that the library node can be renamed.
        /// </summary>
        AllowRename = _LIB_LISTCAPABILITIES.LLC_ALLOWRENAME,

        /// <summary>
        /// Specifies that the library node can be deleted.
        /// </summary>
        AllowDelete = _LIB_LISTCAPABILITIES.LLC_ALLOWDELETE,

        /// <summary>
        /// Specifies that the library node allows source control operations to be performed
        /// on it.
        /// </summary>
        AllowSourceControl = _LIB_LISTCAPABILITIES.LLC_ALLOWSCCOPS,
    } // SanScript.Project.Library.LibraryNodeCapabilities {Enum}
} // SanScript.Project.Library {Namespace}
