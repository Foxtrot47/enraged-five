﻿namespace SanScript.Project
{
    using System;
    using System.Runtime.InteropServices;
    using Microsoft.VisualStudio.Project;
    using IOleServiceProvider = Microsoft.VisualStudio.OLE.Interop.IServiceProvider;

    /// <summary>
    /// Creates the SanScript project type.
    /// </summary>
    [Guid(GuidList.guidSanScriptProjectFactoryString)]
    public class SanScriptProjectFactory : ProjectFactory
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SanScriptProjectFactory"/> class.
        /// </summary>
        /// <param name="package">
        /// The package who created this factory.
        /// </param>
        public SanScriptProjectFactory(SanScriptProjectPackage package)
            : base(package)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Creates a new project node object that represents the SanScript project.
        /// </summary>
        /// <returns>
        /// A new project node object that represents the SanScript project.
        /// </returns>
        protected override ProjectNode CreateProject()
        {
            SanScriptProjectPackage package = this.Package as SanScriptProjectPackage;
            if (package == null)
            {
                string msg = "Project factory was created with a null package";
                throw new NotSupportedException(msg);
            }

            SanScriptProjectNode project = new SanScriptProjectNode(package);
            IServiceProvider provider = package as IServiceProvider;
            if (provider == null)
            {
                string msg = "Unable to cast the registered package to a service provider";
                throw new InvalidCastException(msg);
            }

            Type type = typeof(IOleServiceProvider);
            IOleServiceProvider oleProvider = provider.GetService(type) as IOleServiceProvider;
            if (oleProvider == null)
            {
                string msg = "Unable to cast the registered package to a service provider";
                throw new InvalidCastException(msg);
            }

            project.SetSite(oleProvider);
            return project;
        }
        #endregion Methods
    } // SanScript.Project.SanScriptProjectFactory {Class}
} // SanScript.Project {Namespace}
