﻿namespace SanScript.EditorExtensions.Classification
{
    using System.ComponentModel.Composition;
    using Microsoft.VisualStudio.Text;
    using Microsoft.VisualStudio.Text.Classification;
    using Microsoft.VisualStudio.Utilities;

    /// <summary>
    /// Creates a classifier for a given Microsoft.VisualStudio.Text.ITextBuffer in the given
    /// environment.
    /// </summary>
    [Export(typeof(IClassifierProvider))]
    [ContentType("SanScript")]
    internal class ClassifierProvider : IClassifierProvider
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="ClassificationRegistryService"/>
        /// property.
        /// </summary>
        private IClassificationTypeRegistryService _classificationRegistryService;
        #endregion Fields

        #region Properties
        /// <summary>
        /// Gets or sets the service that maintains the collection of all known classification
        /// types.
        /// </summary>
        [Import]
        private IClassificationTypeRegistryService ClassificationRegistryService
        {
            get { return this._classificationRegistryService; }
            set { this._classificationRegistryService = value; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Gets a classifier for the given text buffer.
        /// </summary>
        /// <param name="textBuffer">
        /// The Microsoft.VisualStudio.Text.ITextBuffer to classify.
        /// </param>
        /// <returns>
        /// A classifier for the text buffer, or null if the provider cannot provide one in its
        /// current state.
        /// </returns>
        IClassifier IClassifierProvider.GetClassifier(ITextBuffer textBuffer)
        {
            return new Classifier(textBuffer, this._classificationRegistryService);
        }
        #endregion Methods
    } // SanScript.EditorExtensions.Classification.ClassifierProvider {Class}
} // SanScript.EditorExtensions.Classification {Namespace}