﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using DLCTextBuilder.ViewModel;
using RSG.Base.Editor.Command;
using DLCTextBuilder.Helpers;
using System.Diagnostics;
using System.IO;
using System.Collections.Specialized;
using System.Threading;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading.Tasks;
using RSG.Base.Logging;


namespace DLCTextBuilder
{
    public class EnumBooleanConverter : IValueConverter
    {
        #region IValueConverter Members
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string parameterString = parameter as string;
            if (parameterString == null)
                return DependencyProperty.UnsetValue;

            if (Enum.IsDefined(value.GetType(), value) == false)
                return DependencyProperty.UnsetValue;

            object parameterValue = Enum.Parse(value.GetType(), parameterString);

            return parameterValue.Equals(value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string parameterString = parameter as string;
            if (parameterString == null)
                return DependencyProperty.UnsetValue;

            return Enum.Parse(targetType, parameterString);
        }
        #endregion
    }
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : INotifyPropertyChanged
	{
		private string m_newTextFile;
		Process m_logProcess = null;
		Process m_gxtLog = null;
		Thread m_executingTask = null;
		enum BuildState {IDLE,RUNNING,FINISHED};
		volatile BuildState m_buildInProgress = BuildState.IDLE;
		public string NewTextFile
		{
			get { return m_newTextFile; }
			set { m_newTextFile = value; }
		}


        private DLCManagerViewModel managerVm { get;set; }

        public MainWindow()
		{
			RSG.Base.Logging.LogFactory.Initialize();
			InitializeComponent();
		}
		private void showUniversalLog()
		{
			m_logProcess = RSG.Base.Logging.LogFactory.ShowUniversalLogViewer(RSG.Base.Logging.LogFactory.ApplicationLogFilename);
			if(!DLCGlobals.BUILD_SUCCESS)
				m_gxtLog = RSG.Base.Logging.LogFactory.ShowUniversalLogViewer(Directory.GetCurrentDirectory()+DLCGlobals.GXT_LOGFILE);
			DLCGlobals.BUILD_SUCCESS = true;
		}

		private void closeUniversalLog()
		{
			if (m_logProcess != null && !m_logProcess.HasExited)
			{
				m_logProcess.CloseMainWindow();

				if (!m_logProcess.WaitForExit(100))
					m_logProcess.Kill();
			}
		}
		private void ui_dlcTextList_openFolder(object sender, MouseButtonEventArgs e)
		{
			//TextFile currentDlc = ui_dlcTextList.SelectedItem as TextFile;
			//if (currentDlc != null)
			//{
			//	DLCGlobals.Instance.CheckoutFile(currentDlc.Name);
			//	Process.Start(currentDlc.Name);
			//}
		}

		private void SetSelectedComboItem()
		{
			XmlSerializer serializer = new XmlSerializer(typeof(int));
			try
			{
				//using (TextReader reader = new StreamReader("savedata.xml"))
				//{
				//	var list = (int)serializer.Deserialize(reader);
				//	ui_dlcCombo.SelectedIndex = list;
				//	reader.Close();
				//}
			}
			catch { }
		}
		private void SaveSelectedComboItem()
		{
			XmlSerializer serializer = new XmlSerializer(typeof(int));
			using (TextWriter writer = new StreamWriter("savedata.xml"))
			{
				//serializer.Serialize(writer,ui_dlcCombo.SelectedIndex);
				//writer.Close();
			}
		}
		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			DLCGlobals.Instance.LogEntries = (CLogEntries)this.Resources["logEntries"];
			SetSelectedComboItem();
			this.managerVm = new ViewModel.DLCManagerViewModel();
			this.DataContext = managerVm;

		}


		private void ui_logList_Loaded(object sender, RoutedEventArgs e)
		{
			//ui_logList.ItemsSource=DLCGlobals.Instance.LogEntries;
		}

		private void ui_resetLog_Click(object sender, RoutedEventArgs e)
		{
			DLCGlobals.Instance.LogEntries.Clear();
		}


		private void Window_Closed(object sender, System.ComponentModel.CancelEventArgs e)
		{
			closeUniversalLog();
			//Application.Current.Shutdown();
			Environment.Exit(0);
		}
		protected override void OnClosed(EventArgs e)
		{
			base.OnClosed(e);

			Application.Current.Shutdown();
		}

		private void ui_sourceRadioDev_Checked(object sender, RoutedEventArgs e)
        {
            var currentProject = managerVm.SelectedProject;
            var currentBranch = managerVm.SelectedBranch;
            currentProject?.CheckTextDir(currentBranch);			
		}

		private void ui_buildAll_Click(object sender, RoutedEventArgs e)
		{
			if (m_buildInProgress == BuildState.IDLE)
            {
                m_buildInProgress = BuildState.RUNNING;
                managerVm.StatusText = "Building";
                var currentProject = managerVm.SelectedProject;
                var currentBranch = managerVm.SelectedBranch;
                currentProject?.CheckTextDir(currentBranch);
                BuildTask task = new BuildTask(currentProject, currentBranch, managerVm.AllLanguages ?
                        managerVm.Languages.ToList() : new List<string>() { managerVm.SelectedLanguage }, managerVm.ExcludeDebug, managerVm.BuildAsPatch, managerVm.OpenExplorer);
                m_buildInProgress = BuildState.FINISHED;
                var bw = new BackgroundWorker();
                bw.DoWork += (o, args) =>
                {
                    Logs = task.Start();
                    managerVm.HasErrors = Logs.Count > 0;
                    m_buildInProgress = BuildState.IDLE;
                    managerVm.StatusText = "Finished with " + (Logs.Count > 0 ? "" : "no ") + "errors";
                };
				bw.RunWorkerAsync();
            }
		}
		public List<string> Logs { get; set; }
		public delegate void TestTask(List<string> input);


		private void ui_showLogs_Click(object sender, RoutedEventArgs e)
		{
			foreach (var item in Logs)
			{
				RSG.Base.Logging.LogFactory.ShowUniversalLogViewer(item);
			}
		}

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}