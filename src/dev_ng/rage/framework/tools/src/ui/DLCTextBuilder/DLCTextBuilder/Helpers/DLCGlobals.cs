﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Configuration;
using RSG.Metadata.Model;
using RSG.Platform;
using RSG.SourceControl.Perforce;
using P4API;
using System.IO;

using RSGLog = RSG.Base.Logging;
using System.Drawing;
using System.Windows.Media;
using System.ComponentModel;
using System.Collections.ObjectModel;


namespace DLCTextBuilder.Helpers
{
		public enum LogLevel
		{
			Warning,
			Error,
			Success,
			Info,
			P4Log,
			GxtLog,
			Exception,
		}
		public class LogEntry 
		{
			public LogEntry()
			{

			}
			public LogEntry(string str, LogLevel severity)
			{
				m_LogLine=str;
				m_severity = severity;
				m_time = DateTime.Now;
				switch(severity)
				{
					case LogLevel.Warning:
						m_color = System.Windows.Media.Brushes.Yellow;
					break;
					case LogLevel.Error:
						m_color = System.Windows.Media.Brushes.Red;
						break;
					case LogLevel.Success:
						m_color = System.Windows.Media.Brushes.GreenYellow;
						break;
					case LogLevel.Info:
						m_color = System.Windows.Media.Brushes.DimGray;
						break;
					case LogLevel.P4Log:
						m_color = System.Windows.Media.Brushes.Orange;
						break;
					case LogLevel.GxtLog:
						m_color = System.Windows.Media.Brushes.CornflowerBlue;
						break;
					default:
						m_color = System.Windows.Media.Brushes.White;
					break;
				}
			}
			private string m_LogLine;

			public string LogLine
			{
				get { return m_LogLine; }
				set { m_LogLine = value; }
			}

			private LogLevel m_severity;

			public LogLevel Severity
			{
				get { return m_severity; }
				set { m_severity = value; }
			}
			private SolidColorBrush m_color;

			public SolidColorBrush Colour
			{
				get { return m_color; }
				set { m_color = value; }
			}
			private DateTime m_time;

			public string LogTime
			{
				get { return m_time.ToLongTimeString(); }
			}

			public DateTime LogTimeFull
			{
				set { m_time = value; }
			}
			
			
			
			
		}
	public class CLogEntries : ObservableCollection<LogEntry>
	{

	}
    public enum Branches
    {
        dev,
        dev_temp,
        dev_ng,
        dev_ng_live,
        dev_gen9,
        dev_gen9_disc,
		japanese_ng
    }
    public class DLCGlobals
    {
        public static Dictionary<string, string> BranchToTextFolderMapping = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase)
        {
            {"dev_gen9","dev_gen9"},
            {"dev_gen9_sga","dev_gen9_sga"},
            {"dev_ng","dev_ng"},
            {"dev_ng_live","dev_ng_live\\patch"},
            {"japanese_ng","japanese_ng"},
            {"japanese_ng_TU","japanese_ng\\patch"},
            {"titleupdate_gen9","dev_gen9\\patch"},
            {"titleupdate_gen9_sga","dev_gen9_sga\\patch"},
            {"titleupdate_ng","dev_ng\\patch"},
            {"titleupdate_ng_live","dev_ng_live\\patch"},
        };
		public static Dictionary<Platform, string> PlatformEnumToBuildPlatform = new Dictionary<Platform, string>()
		{
			{Platform.XboxOne, "xboxone"},
			{Platform.XBSX, "xbsx"},
			{Platform.XboxOneGDK, "xboxone"},
			{Platform.PS3, "ps3"},
			{Platform.PS4, "ps4"},
			{Platform.PS5, "ps5"},
			{Platform.Win64, "x64"},
		};
		public static Dictionary<Platform, string> PlatformEnumToPlatformKey = new Dictionary<Platform, string>()
		{
			{Platform.XboxOne, "xb1"},
			{Platform.XBSX, "xbsx"},
			{Platform.XboxOneGDK, "xb1"},
			{Platform.PS3, "ps3"},
			{Platform.PS4, "ps4"},
			{Platform.PS5, "ps5"},
			{Platform.Win64, "pc"},
		};

		public static string GetGxtMaker( string branch)
		{
			return new FileInfo(System.Configuration.ConfigurationManager.AppSettings[$"{branch}_gxtpath"] ?? GXT_FALLBACK).FullName;
		}
		public static string GetPlatformString(Platform platform)
		{
			return System.Configuration.ConfigurationManager.AppSettings[$"{platform}_platform"] ?? PlatformEnumToBuildPlatform[platform];
		}

		public string OVERLAYINFO_FILE = "overlayinfo.xml";
		public string TEXT_METAFILE = "dlctext.xml";
		public IConfig TOOLS_CONFIG = null;
		public int ACTIVE_CL = 0;
		private const string GXT_MAKER = @"x:\gta5\assets_ng\GameText\scripts\gxtmaker2.exe";
		private const string GXT_FALLBACK = @"x:\gta5\tools_ng\bin\DLCTextBuilder\gxtmaker2.exe";
		public static int getStringHashCode(string hashSource)
		{
			return hashSource.ToLower().GetHashCode();
		}
		public static Font BOLD_FONT = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
		public static Font REGULAR_FONT = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular);
		public static Font OUTPUT_BOLD_FONT = new Font("Courier New", 11.25f, FontStyle.Bold);
		public static Font OUTPUT_REGULAR_FONT = new Font("Courier New", 11.25f, FontStyle.Regular);
		public static string GXT_LOGFILE = @"\gxtlog_";
		public static string GXT_LOG_DIR = @"x:\gta5\tools_ng\bin\DLCTextBuilder\logs\";
		public static string TITLEUPDATE_BASE_PATH = @"x:\gta5\titleupdate\";
        public static string[] TITLEUPDATE_TEXT_SOURCE =
        {
            @"x:\gta5\assets\GameText\titleupdate\patch\",
            @"x:\gta5\assets_ng\GameText\dev_ng\patch\",
            @"x:\gta5\assets_ng\GameText\dev_ng_live\patch\",
            @"x:\gta5\assets_ng\GameText\dev_temp\patch\",
            @"x:\gta5\assets_ng\GameText\japanese_ng\patch\",
            @"x:\gta5\assets_gen9\GameText\dev_gen9\patch\",
            @"x:\gta5\assets_gen9_disc\GameText\dev_gen9_disc\patch\"
        };
        public static string[] TITLEUPDATE_PATHS = 
		{ 
			"dev", 
			"dev_ng", 
			"dev_ng_live", 
			"dev_gen9", 
			"dev_gen9_sga",
			"dev_temp", 
			"japanese_ng" 
		};
		public static bool BUILD_SUCCESS = true;
		public string GXT_LOG = @"TextBuilder_output.txt";
		public string APP_LOG = @"TextBuilder_status.txt";
		public List<string> PLATFORMS = null;
		public Dictionary<string,string> PLATFORMKEY = null;
		private static DLCGlobals instance;
        public ObservableCollection<string> m_languages = new ObservableCollection<string>
        {
            "american",
            "chinese",
            "chinesesimp",
            "french",
            "german",
            "italian",
            "japanese",
            "korean",
            "mexican",
            "polish",
            "portuguese",
            "russian",
            "spanish"
        };
        public ObservableCollection<string> Languages
        {
            get { return m_languages; }
        }
        private CLogEntries m_log = new CLogEntries();
		public CLogEntries LogEntries
		{
			get { return m_log; }
			set { m_log = value; }
		}

		public void Log_d_Error(string lines)
		{
			LoggerInternal("[ERROR]: " + lines, RSGLog.Log.Log__Debug);
		}
		public void Log_d_Info(string lines)
		{
			LoggerInternal("[INFO]: " + lines, RSGLog.Log.Log__Debug);
		}
		public void Log_d_Warning(string lines)
		{
			LoggerInternal("[WARNING]: " + lines, RSGLog.Log.Log__Debug);
		}
		public void Log_d_Perforce(string lines)
		{
			LoggerInternal("[Perforce]: " + lines, RSGLog.Log.Log__Debug);
		}
		public void Log_Warning(string lines)
		{
			LoggerInternal(lines, RSGLog.Log.Log__Warning);
		}
		public void Log_Error(string lines)
		{
			LoggerInternal(lines, RSGLog.Log.Log__Error);
		}
		public void Log_Info(string lines)
		{
			LoggerInternal(lines, RSGLog.Log.Log__Message);
		}
		public void Log_Debug(string lines)
		{
			LoggerInternal(lines, RSGLog.Log.Log__Debug);
		}
		public void Log_Exception(string lines,Exception exc)
		{
			try
			{
				RSGLog.Log.Log__Exception(exc,lines);
			}
			catch (System.Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
		}
		delegate void LogFoo(string log, params object[] args);
		private void LoggerInternal(string line,LogFoo foo)
		{
			try
			{
				foo(line,RSGLog.LogMessageEventArgs.Empty);
				RSGLog.LogFactory.FlushApplicationLog();
			}
			catch (System.Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
			System.IO.StreamWriter file = null;
			file = new System.IO.StreamWriter(APP_LOG, true);
			file.WriteLine(line);
			file.Close();
		}
		private void LoggerDep(String lines, LogLevel logLevel)
		{
			// Write the string to a file.append mode is enabled so that the log
			// lines get appended to  test.txt than wiping content and writing the log
			#region 
			//System.IO.StreamWriter file = null;
			//if(logLevel==0)
			//    file = new System.IO.StreamWriter(APP_LOG, true);
			//else
			//    file = new System.IO.StreamWriter(GXT_LOG, true);
			//file.WriteLine(lines);
			//m_log.Add(new LogEntry(lines,logLevel));
			//file.Close();
			#endregion
		}
		private void DeleteLog(string logFile)
		{
			if (File.Exists(logFile))
				File.Delete(logFile);
		}
		private void initialise()
		{
			if (!Directory.Exists(GXT_LOG_DIR))
			{
				Directory.CreateDirectory(GXT_LOG_DIR);
			}
			DeleteLog(APP_LOG);
			DeleteLog(GXT_LOG);
			Log_d_Info("Init Globals");
#if ENABLE_GXTCHECK
			{
				if (!File.Exists(GXT_MAKER))
				{
					Log_d_Warning("assets_ng builder not found, using fallback executable");
					if (!File.Exists(GXT_FALLBACK))
					{
						Log_d_Error("Fallback not found, contact email");
						System.Windows.MessageBox.Show("GXTMaker not found, contact orkun.bektas@rockstarlondon.com");
					}
				}
			}
#endif
			TOOLS_CONFIG = ConfigFactory.CreateConfig();
			PLATFORMS = new List<string>();
			PLATFORMKEY = new Dictionary<string,string>();

			Log_d_Info("Enabled Platforms:");
			PLATFORMS.Add("x64");
			PLATFORMS.Add("xbox360");
			PLATFORMS.Add("ps3");
			PLATFORMS.Add("ps4");
			PLATFORMS.Add("xboxone");

			PLATFORMKEY.Add("x64","pc");
			PLATFORMKEY.Add("xboxone", "xb1");
			PLATFORMKEY.Add("xbox360", "xbox360");
			PLATFORMKEY.Add("ps3", "ps3");
			PLATFORMKEY.Add("ps4", "ps4");

// 			foreach (var item in TOOLS_CONFIG.Project.DefaultBranch.Targets)
// 			{
// 				if (item.Value.Enabled)
// 				{
// 					Platform platform = item.Value.Platform;
// 					string plat = platform.ToString().ToLower();
// 					PLATFORMS.Add(plat);
// 					Log_d_Info(plat);
// 				}
// 			}
			Log_d_Info("End Init Globals");
		}
		public void CheckoutFile(string fullPath)
		{
			Log_d_Perforce("[Perforce]: CheckoutFile: " + fullPath);
			if (ACTIVE_CL == 0)
				CreateChangeList();

			try
			{
				using (P4 p4 = TOOLS_CONFIG.Project.SCMConnect())
				{
					List<string> arguments = new List<string>();

					if (ACTIVE_CL != 0)
					{
						arguments.Add("-c");
						arguments.Add(ACTIVE_CL.ToString());
						arguments.Add(fullPath);
					}
					p4.Run("add", false, arguments.ToArray());
					p4.Run("edit", false, arguments.ToArray());
					if (!File.Exists(fullPath))
					{
						File.Open(fullPath, FileMode.CreateNew).Close();
					}
					Log_d_Perforce("[CheckoutFile]: Success: " + fullPath);
				}
			}
			catch (System.Exception ex)
			{
				Log_d_Perforce("[CheckoutFile]: FAIL " + ex.Message);
			}
		}
		public void CreateChangeList()
		{
			Log_d_Perforce("[CreateChangeList]: ");
			try
			{
				bool clExists = false;
				using (P4 p4 = TOOLS_CONFIG.Project.SCMConnect())
				{
					List<string> args = new List<string>();
					string clDescPrefix = "DLC Text Builder Changelist";
					args.Add("-u");
					args.Add(p4.User);
					args.Add("-s");
					args.Add("pending");
					P4RecordSet retSet = p4.Run("changes", false, args.ToArray());
					foreach (P4API.P4Record record in retSet)
					{
						if (record["desc"].StartsWith(clDescPrefix))
						{
							clExists = true;
							if (!int.TryParse(record["change"], out ACTIVE_CL))
							{
								clExists = false;
								ACTIVE_CL = 0;
							}
							break;
						}
					}
					if (!clExists)
					{
						P4PendingChangelist newPendCL = p4.CreatePendingChangelist(clDescPrefix + " [" + DateTime.Now.ToShortDateString() + "]");
						if (newPendCL != null)
							ACTIVE_CL = newPendCL.Number;
					}
					Log_d_Perforce("[CreateChangeList]: Success! CL#" + ACTIVE_CL);
				}
			}
			catch (System.Exception ex)
			{
				Log_d_Perforce("[Perforce]: [CreateChangeList]: FAIL " + ex.Message);
			}
		}


#region Singleton

		private DLCGlobals()
		{
			initialise();
		}
		public static DLCGlobals Instance
		{
			get
			{
				if (instance == null)
				{
					instance=new DLCGlobals();
				}
				return instance;
			}
		}
#endregion
	}
}
