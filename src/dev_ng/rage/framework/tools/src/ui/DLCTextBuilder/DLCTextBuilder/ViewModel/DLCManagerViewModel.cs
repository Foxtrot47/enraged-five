﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using DLCTextBuilder.Helpers;
using System.IO;
using System.Collections.ObjectModel;
using System.Windows.Forms.VisualStyles;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Platform;

namespace DLCTextBuilder.ViewModel
{
	public class DLCManagerViewModel : ViewModelBase
    {
        public static DLCManagerViewModel Instance = null;
        public static ObservableCollection<string> m_languages = new ObservableCollection<string>
        {
            "american",
            "chinese",
            "chinesesimp",
            "french",
            "german",
            "italian",
            "japanese",
            "korean",
            "mexican",
            "polish",
            "portuguese",
            "russian",
            "spanish"
        };

        public string SelectedLanguage
        {
            get => m_selectedLanguage;
            set => SetPropertyValue(ref m_selectedLanguage, value);
        }

        private string m_selectedLanguage;
        public ObservableCollection<string> Languages
        {
            get => m_languages;
            set => SetPropertyValue(ref m_languages, value);
        }
        #region DLC Entries
        private volatile ObservableCollection<DLCViewModel> m_dlcEntries = new ObservableCollection<DLCViewModel>();
        private IBranch m_selectedBranch;
        private DLCViewModel m_selectedProject;
        private ObservableCollection<IBranch> m_branches;
        private bool m_allLanguages;
        private bool m_excludeDebug;
        private bool m_hasErrors;
        private bool m_openExplorer;
        private bool m_buildAsPatch;
        private string m_statusText =  IdleText;

        public ObservableCollection<DLCViewModel> DLCEntries
		{
			get => m_dlcEntries;
            set => SetPropertyValue(ref m_dlcEntries,value);
        }
		#endregion
		public DLCManagerViewModel()
        {
            Instance = this;
            m_branches = new ObservableCollection<IBranch>();
			m_dlcEntries = new ObservableCollection<DLCViewModel>();
            SelectedLanguage = Languages.First();
			PopulateDlcInfo();	
		}

        public IBranch SelectedBranch
        {
            get => m_selectedBranch;
            set
            {
                if (!SetPropertyValue(ref m_selectedBranch, value)) return;
                if(SelectedBranch!=null)
                    SelectedProject.CheckTextDir(SelectedBranch);
            }
        }

        public ObservableCollection<TextFile> TextEntries => SelectedProject.TextEntries;

        public DLCViewModel SelectedProject
        {
            get => m_selectedProject;
            set
            {
                if (!SetPropertyValue(ref m_selectedProject, value)) return;
                Branches.Clear();
                Branches.AddRange(
                    SelectedProject.Project.Branches.Values
                        .Where(a=> DLCGlobals.BranchToTextFolderMapping.Keys.Any(x=>String.Equals(x, a.Name, StringComparison.CurrentCultureIgnoreCase)))
                        .Where(a=>a.Project.IsDLC || DLCGlobals.BranchToTextFolderMapping[a.Name].Contains("patch")));
                NotifyPropertyChanged(nameof(TextEntries));
                SelectedBranch = m_branches.FirstOrDefault();
            }
        }
        public ObservableCollection<IBranch> Branches
        {
            get => m_branches;
            set {
                if (SetPropertyValue(ref m_branches, value))
                {
                } }
        }

        private static string IdleText = "Press \"Build Text\" to build the text RPF's";

        public string StatusText
        {
            get => m_statusText;
            set
            {
                SetPropertyValue(ref m_statusText, value);
                NotifyPropertyChanged(nameof(StatusText));
            }
        }

        public bool HasErrors
        {
            get => m_hasErrors;
            set => SetPropertyValue(ref m_hasErrors, value);
        }
        public bool AllLanguages    
        {
            get => m_allLanguages;
            set => SetPropertyValue(ref m_allLanguages, value);
        }

        public bool BuildAsPatch
        {
            get => m_buildAsPatch;
            set => SetPropertyValue(ref m_buildAsPatch, value);
        }

        public bool OpenExplorer
        {
            get => m_openExplorer;
            set => SetPropertyValue(ref m_openExplorer, value);
        }
        public bool ExcludeDebug
        {
            get => m_excludeDebug;
            set => SetPropertyValue(ref m_excludeDebug, value);
        }

        private void PopulateDlcInfo()
		{
            DLCEntries.Add(new DLCViewModel( DLCGlobals.Instance.TOOLS_CONFIG.Project));
            DLCEntries.AddRange(
                DLCGlobals.Instance.TOOLS_CONFIG.Project.Config.DLCProjects.Select(a => new DLCViewModel(a.Value)));
            SelectedProject = DLCEntries.FirstOrDefault();
            SelectedBranch = m_branches.FirstOrDefault();
        }
	}
}
