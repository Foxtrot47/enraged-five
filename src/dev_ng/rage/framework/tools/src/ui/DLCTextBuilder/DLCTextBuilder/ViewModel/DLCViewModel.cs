﻿//#define TEST_MODE
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using System.IO;
using DLCTextBuilder.Helpers;
using System.Diagnostics;
using System.Windows;
using System.Collections.ObjectModel;
using System.Windows.Documents;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Platform;

namespace DLCTextBuilder.ViewModel
{
	public class TextFile
	{
		private string m_name;
        public FileInfo m_file;
        public string Name => m_file.Name;
        public string FullName => m_file.FullName;


        public TextFile(string name)
		{
			m_file = new FileInfo(name);
			this.m_name = name;
		}

    }

    public class DLCViewModel : ViewModelBase, ICloneable
    {

        public DLCViewModel(IProject project)
        {
            m_project = project;
        }
        private IProject m_project;
        private ObservableCollection<IBranch> m_branches;

        public IProject Project
        {
            get => m_project;
            set => m_project = value;
        }

        public ObservableCollection<IBranch> Branches
        {
            get => m_branches;
            set => SetPropertyValue(ref m_branches,value);
        }



        public string TextRoot(IBranch branch) => branch.Text;
        public string FriendlyName => Project.FriendlyName;

        public object Clone()
        {
            throw new NotImplementedException();
        }

        public void AddTextFile(string newTextFile, IBranch currentBranch)
        {
        }

        public bool HasLogs => Logs.Count > 0;
        public ObservableCollection<string> Logs
        {
            get => m_resultLogs;
            set
            {
                SetPropertyValue(ref m_resultLogs, value);
                OnPropertyChanged(nameof(HasLogs));
            }
        }

        public ObservableCollection<TextFile> TextEntries
        {
            get => m_textEntries;
            set => SetPropertyValue(ref m_textEntries, value);
        }

        private Dictionary<string, ObservableCollection<string>> m_languageFilesToBuild = new Dictionary<string, ObservableCollection<string>>(StringComparer.CurrentCultureIgnoreCase);
        private ObservableCollection<TextFile> m_textEntries = new ObservableCollection<TextFile>();
        private ObservableCollection<string> m_resultLogs = new ObservableCollection<string>();

        public Dictionary<string, ObservableCollection<string>> LanguageFilesToBuild
        {
            get => m_languageFilesToBuild;
            set => SetPropertyValue(ref m_languageFilesToBuild,value);
        }
        public bool HasTextFiles { get; set; } = false;
        public void CheckTextDir(IBranch currentBranch)
        {
            TextEntries.Clear();
            var textPath = Path.Combine(currentBranch.Text,DLCGlobals.BranchToTextFolderMapping[currentBranch.Name]);
            var textDirectory = new DirectoryInfo(textPath);
            var languages = DLCManagerViewModel.Instance.Languages
                .Select(a => new KeyValuePair<string, string>(a,
					//VERY DIRTY HACK
                    Path.Combine(currentBranch.Text, currentBranch.Text.Contains("gta5_dlc") ? "" : (DLCGlobals.BranchToTextFolderMapping[currentBranch.Name]))))
                .ToDictionary(a => a.Key, a => a.Value);
            var addedFiles = new HashSet<string>();
			languages.Keys.OrderByDescending(a=>a.Length).ForEach(l =>
            {
				List<string> files = new List<string>();

                void addFiles(string path)
                {
                    try
                    {
                        var subf = Directory.EnumerateFiles(path, $"{l}*.txt").Where(a=>!addedFiles.Contains(a)).ToList();
                        addedFiles.AddRange(subf);
                        //subf = subf.Select(f => new FileInfo(f))
                        //    .Where(f => languages.Where(k => k.Key != l.Key && k.Key.Length > l.Key.Length)
                        //        .Any(x => !f.Name.Contains(x.Key))).Select(a => a.FullName).ToList();
                        files.AddRange(subf);
                    }
                    catch (Exception e)
                    {

                    }

                }
				addFiles($"{languages[l]}");
				addFiles(Path.Combine(languages[l], l));
                m_languageFilesToBuild[l] = new ObservableCollection<string>(files);
            });
            m_languageFilesToBuild.Values.SelectMany(a => a.ToList()).OrderBy(a=>a)
                .Select(a => new TextFile(a)).ForEach(a=>TextEntries.Add(a));
        }
        
        public List<string> Build(IBranch selectedBranch, List<string> selectedLanguages, bool releaseBuild, bool patchBuild, bool openExplorer)
        {
            var ret = new List<string>();
            var destinationPaths = new HashSet<string>();
            foreach (var language in selectedLanguages)
            {
                if (LanguageFilesToBuild.TryGetValue(language,out var filesToBuild))
                {
                    var textFileList = string.Join(" ", filesToBuild.Reverse().Where(a => !releaseBuild || !a.ToLower().Contains("debug")));
                    if (!string.IsNullOrEmpty(textFileList))
                    {
                        DLCGlobals.Instance.Log_Info(
                            @"[GXTMAKER] [BuildTextFiles: Building [" + language.ToUpper() + "] text for " + Project.FriendlyName + $"] {selectedBranch.Name}");
                        foreach (var platformTarget in selectedBranch.Targets.Keys)
                        {
                            var platform = DLCGlobals.GetPlatformString(platformTarget);
                            string platformKey = DLCGlobals.GetPlatformKeyString(platformTarget);
                            string buildPlatform = platform;
                            string outputPath = Path.Combine(selectedBranch.Build, platform);
#if !TEST_MODE
                            if (!Directory.Exists(outputPath))
                            {
                                Directory.CreateDirectory(outputPath);
                            }

#endif
                            var rpfName = Path.Combine(selectedBranch.Build, platform);
                            if (!Project.IsDLC)
                            {
                                rpfName = Path.Combine(rpfName, "patch");
                            }
                            rpfName = Path.Combine(rpfName, "data","lang");
                            var suffix = releaseBuild && !Project.IsDLC ? "_rel" : "";
                            var extension = Project.IsDLC ? "DLC.rpf" : ".rpf";
                            rpfName = Path.Combine(rpfName, $"{language}{suffix}{extension}");
                            string targetDirectory = new FileInfo(rpfName).Directory.FullName;
                            string tempName = FriendlyName.Replace(' ', '_').Replace(':', '_');
                            string logFile = DLCGlobals.GXT_LOG_DIR + tempName + DLCGlobals.GXT_LOGFILE + language + "_" + platform + ".ulog";
                            string logDir = Path.GetDirectoryName(logFile);
                            bool success = false;
                            if (!Directory.Exists(logDir))
                            {
                                Directory.CreateDirectory(logDir);
                            }
                            if (!Directory.Exists(targetDirectory))
                            {
                                Directory.CreateDirectory(targetDirectory);
                            }
                            string gxtString = "-nologwindow -log " + logFile + " -platform " + platform + " -platformkey " + platformKey + " " + rpfName + " " + textFileList;
                            gxtString.Replace(@"\\", @"\");
#if TEST_MODE
					Console.WriteLine("GXT DEBUG START");
					string[] words = gxtString.Split(' ');
					foreach (var item in words)
					{
						Console.WriteLine("  "+item);
					}
					Console.WriteLine("GXT DEBUG END");
#else
                            DLCGlobals.Instance.CheckoutFile(rpfName);
                            try
                            {
                                var proc = new Process
                                {
                                    StartInfo = new ProcessStartInfo
                                    {
                                        FileName = DLCGlobals.GetGxtMaker(selectedBranch.Name),
                                        Arguments = gxtString,
                                        UseShellExecute = false,
                                        RedirectStandardOutput = true,
                                        CreateNoWindow = true
                                    }
                                };
                                proc.Start();
                                DLCGlobals.Instance.Log_Info(@"[GXTMAKER] [BuildTextFiles: " + gxtString + "]");
                                while (!proc.StandardOutput.EndOfStream)
                                {
                                    string line = proc.StandardOutput.ReadLine();
                                    //DLCGlobals.Instance.Log_Info("[GXTMAKER] " + line);
                                }
                                if (proc.ExitCode == 0)
                                {
                                    DLCGlobals.Instance.Log_Info("[GXTMAKER] " + "Build finished successfully");
                                    outputPath = logFile;
                                    destinationPaths.Add(targetDirectory);
                                }
                                else
                                {
                                    DLCGlobals.Instance.Log_Error("[GXTMAKER] " + "Build errors, check log");
                                    DLCGlobals.BUILD_SUCCESS = false;
                                    ret.Add(logFile);
                                }

                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("Make sure that you have //depot/gta5/assets_ng/GameText/scripts/gxtmaker2.exe");
                            }
#endif
                        }

                    }
                }
            }
            if(openExplorer)
                foreach (var process in destinationPaths.Select(p => new Process
                {
                    StartInfo = new ProcessStartInfo( p) {UseShellExecute = true, Verb = "open"}
                }))
                {
                    process.Start();
                }
            return ret;
        }
    }
}
