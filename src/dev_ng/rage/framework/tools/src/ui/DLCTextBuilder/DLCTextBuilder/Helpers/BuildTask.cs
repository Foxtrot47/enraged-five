﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DLCTextBuilder.ViewModel;
using System.Collections.ObjectModel;
using System.Threading;
using RSG.Base.Configuration;

namespace DLCTextBuilder.Helpers
{
	public class BuildTask
	{
        private readonly bool m_openExplorer;
        public bool ReleaseBuild { get; set; }
        public bool PatchBuild { get; set; }
        public IBranch SelectedBranch { get;set; }
		public DLCViewModel Model { get; set; }
		public List<string> SelectedLanguages { get;set; }
		public BuildTask(DLCViewModel modelToBuild, IBranch selectedBranch, List<string> selectedLanguages, bool releaseBuild, bool patchBuild, bool openExplorer)
        {
            m_openExplorer = openExplorer;
            Model = modelToBuild;
            PatchBuild = patchBuild;
            ReleaseBuild = releaseBuild;
            SelectedLanguages = selectedLanguages;
            SelectedBranch = selectedBranch;
        }

		public List<string> Start()
		{
            return Model.Build(SelectedBranch, SelectedLanguages, ReleaseBuild, PatchBuild,m_openExplorer);
		}
	}
}
