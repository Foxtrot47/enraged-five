﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Configuration;
using RSG.Editor;
using RSG.ShortcutMenu.Tasks;

namespace ShortcutMenu.ViewModels
{
    /// <summary>
    /// View model for the shortcut menu task bar icon.
    /// </summary>
    public class TaskbarIconViewModel : NotifyPropertyChangedBase
    {
        #region Fields
        /// <summary>
        /// The shortcut menu model.
        /// </summary>
        private ShortcutMenuRoot _shortcutMenu;

        /// <summary>
        /// Branch that we are running the tray icon from.
        /// </summary>
        private readonly IBranch _branch;

        /// <summary>
        /// Private field for the <see cref="TrayIconTooltip"/> property.
        /// </summary>
        private String _trayIconTooltip;
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="TaskbarIconViewModel"/> class.
        /// </summary>
        /// <param name="branch"></param>
        public TaskbarIconViewModel(IBranch branch)
        {
            _branch = branch;
            _trayIconTooltip = String.Format("R* Shortcuts - {0}", _branch.Project.Name);
        }
        #endregion

        #region Properties
        /// <summary>
        /// Tooltip to display in the tray icon.
        /// </summary>
        public String TrayIconTooltip
        {
            get { return _trayIconTooltip; }
            set { SetProperty(ref _trayIconTooltip, value); }
        }
        #endregion
    }
}
