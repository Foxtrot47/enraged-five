﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using Microsoft.Win32;
using RSG.Base.Configuration;
using RSG.Base.Configuration.Services;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Base.OS;
using RSG.Editor;
using RSG.Editor.Controls;
using RSG.Editor.SharedCommands;
using RSG.ShortcutMenu.Common;
using RSG.ShortcutMenu.Services;
using RSG.ShortcutMenu.Tasks;
using ShortcutMenu.Commands;
using ShortcutMenu.ViewModels;

namespace ShortcutMenu
{
    /// <summary>
    /// Application that runs the shortcut menu.
    /// </summary>
    public class ShortcutMenuApp : RsApplication, IShortcutItemInvoker
    {
        #region Program Entry Point
        /// <summary>
        /// Main entry point into the application. The first thing to get called.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            ShortcutMenuApp app = new ShortcutMenuApp();
            app.ShutdownMode = ShutdownMode.OnExplicitShutdown;
            RsApplication.OnEntry(app, ApplicationMode.Single);
        }
        #endregion

        #region Constants
        /// <summary>
        /// Guid for the taskbar menu.
        /// </summary>
        private static readonly Guid _trayMenuId = new Guid("28DB64D9-5471-4EDD-A160-B671CE5C0EE8");

        /// <summary>
        /// Folder that the shortcut menu executable lives in.
        /// </summary>
        private const String _shortcutMenuBinDirectory = @"$(toolsbin)\ShortcutMenu";

        /// <summary>
        /// Location of the shortcut menu configuration file.
        /// </summary>
        private const String _shortcutMenuConfigFilename = @"$(toolsconfig)\shortcutMenu\shortcuts.xml";

        /// <summary>
        /// Build version to display if we aren't able to establish a connection to the deployment service.
        /// </summary>
        private const String _unknownBuildVersionName = "Unknown";

        /// <summary>
        /// Registry key that contains the path the the bugstar executable.
        /// </summary>
        private const String _bugstarIconRegistryKey = "HKEY_LOCAL_MACHINE\\SOFTWARE\\Classes\\Bugstar\\DefaultIcon";

        /// <summary>
        /// How long to show the taskbar balloon for (10 seconds).
        /// </summary>
        private const int _defaultBalloonDisplayTimeout = 10000;
        #endregion // Constants

        #region Fields
        /// <summary>
        /// Service host for the shortcut menu service.
        /// </summary>
        private ServiceHost _shortcutMenuServiceHost;

        /// <summary>
        /// The task bar icon that this application is hosted in.
        /// </summary>
        private RsTaskbarIcon _taskbarIcon;

        /// <summary>
        /// Reference to the task bars context menu.
        /// </summary>
        private ContextMenu _contextMenu;

        /// <summary>
        /// The routed command for restarting the application.
        /// </summary>
        private TaskRoutedCommand _restartRoutedCommand;

        /// <summary>
        /// Command line options.
        /// </summary>
        private readonly CommandOptions _options;

        /// <summary>
        /// The shortcut menu model.
        /// </summary>
        private ShortcutMenuRoot _shortcutMenuRoot;

        /// <summary>
        /// Shortcut item factory.
        /// </summary>
        private readonly ShortcutItemFactory _factory;

        /// <summary>
        /// Lookup for shortcut items based on their guids.
        /// Note only items that have a guid set will be present in this dictionary.
        /// </summary>
        private readonly IDictionary<Guid, IShortcutItem> _itemLookup = new Dictionary<Guid, IShortcutItem>();

        /// <summary>
        /// Lookup mapping the dynamically created command bar items back to their
        /// corresponding shortcut configuration items.
        /// </summary>
        private readonly IDictionary<CommandBarItem, IShortcutItem> _menuItems = new Dictionary<CommandBarItem, IShortcutItem>();

        /// <summary>
        /// Cancellation token source for shutting down the rag interface.
        /// </summary>
        private readonly CancellationTokenSource _shutdownTokenSource;

        /// <summary>
        /// File system watcher to monitor for changes to the non-bootstrapped shortcut menu assembly directory.
        /// </summary>
        private readonly FileSystemWatcher _appDirWatcher;

        /// <summary>
        /// File system watcher to monitor for changes to the shortcut menu xml file.
        /// </summary>
        private readonly FileSystemWatcher _configDirWatcher;

        /// <summary>
        /// Location of the bugstar executable.
        /// </summary>
        private readonly string _bugstarExe;

        /// <summary>
        /// Whether we should be showing the balloon popup indicating that bugstar isn't
        /// running on the users machine.
        /// </summary>
        private bool _shownBugstarNotRunningWarning = false;

        /// <summary>
        /// Flag indicating that the "restart required" balloon notification has already been
        /// displayed to the user.
        /// </summary>
        private bool _shownRestartRequiredNotification = false;

        /// <summary>
        /// Dispatcher timer for checking whether bugstar is running.
        /// </summary>
        private readonly DispatcherTimer _bugstarCheckTimer;
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="ShortcutMenuApp"/> class.
        /// </summary>
        public ShortcutMenuApp()
            : base()
        {
            // Register any commandline options the app supports.
            RegisterCommandlineArg("nobootstrap", LongOption.ArgType.None, "Allows the shortcut menu application to start in a non-bootstrapped environment.");

            // Command line options.
            _options = new CommandOptions();
            _factory = new ShortcutItemFactory(Log);
            _shutdownTokenSource = new CancellationTokenSource();

            // Merge in the resource dictionary that contains our taskbar icon.
            MergeTaskbarIconResources();

            // Make sure the exit command is always present.
            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Exit,
                ushort.MaxValue,
                true,
                "Exit",
                new Guid("508E8C4F-ADDD-46A6-A430-8F84174D22F0"),
                _trayMenuId);

            // Get the bugstar exe path from the registry.
            _bugstarExe = GetBugstarExecutablePath();

            // Set up the timers for checking bugstar/updating the build version.
            _bugstarCheckTimer = new DispatcherTimer(TimeSpan.FromMinutes(5), DispatcherPriority.Normal, BugstarCheckTimer_Tick, Dispatcher);
            _bugstarCheckTimer.Start();

            // Perforce works by sync to a temp file prior to renaming it to the correct filename, so we
            // need to hook up a Changed event handler for local user edits as well as a Renamed handler
            // for when files are syncing from p4.

            // Set up a file watcher which monitors for changes to the shortcut menu bin folder.
            string resolvedShortcutMenuFolder = _options.Branch.Environment.Subst(_shortcutMenuBinDirectory);
            _appDirWatcher = new FileSystemWatcher(resolvedShortcutMenuFolder);
            _appDirWatcher.EnableRaisingEvents = true;
            _appDirWatcher.Changed += OnShortcutsBinFolderChanged;
            _appDirWatcher.Renamed += OnShortcutsBinFolderRenamed;

            // Set up the file watcher for changes to the shortcut menu config files.
            string resolvedShortcutMenuConfigFile = _options.Branch.Environment.Subst(_shortcutMenuConfigFilename);
            _configDirWatcher = new FileSystemWatcher(Path.GetDirectoryName(resolvedShortcutMenuConfigFile));
            _configDirWatcher.Filter = "*.xml";
            _configDirWatcher.EnableRaisingEvents = true;
            _configDirWatcher.Changed += OnShortcutsFileChanged;
            _configDirWatcher.Renamed += OnShortcutsFileChanged;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the unique name for this application that is used for mutex creation, and
        /// folder organisation inside the local app data folder and the registry.
        /// </summary>
        public override string UniqueName
        {
            get { return "ShortcutMenu"; }
        }

        /// <summary>
        /// Gets the collection containing all of the commands that should be shown on the tool
        /// bar tray.
        /// </summary>
        [Bindable(true)]
        public CommandBarItemViewModelCollection TrayMenuCommands
        {
            get { return CommandBarItemViewModel.Create(_trayMenuId); }
        }

        /// <summary>
        /// Gets or sets the value indicating whether the bugstar checker is currently enabled.
        /// </summary>
        public bool BugstarCheckerEnabled
        {
            get { return _bugstarCheckTimer.IsEnabled; }
            set { _bugstarCheckTimer.IsEnabled = value; }
        }

        /// <summary>
        /// Don't track application session stats for the shortcut menu yet.
        /// Need to fix the startup location that is tracked due to it starting from a
        /// bootstrapped location.
        /// </summary>
        public override bool TrackSessionStatistics
        {
            get { return false; }
        }
        #endregion

        #region RsApplication Overrides
        /// <summary>
        /// Override to create the main window for the application.
        /// </summary>
        /// <returns>
        /// The System.Windows.Window that will be the main window for this application.
        /// </returns>
        protected override Window CreateMainWindow()
        {
            // App doesn't have a main window.
            return null;
        }

        /// <summary>
        /// Starts the shortcut menu service.
        /// </summary>
        /// <param name="e">
        /// The event data.
        /// </param>
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            // Make sure we're running through the bootstrapper (or have the --nobootstrap commandline arg).
            bool checkForBootstrapper = !CommandLineOptions.HasOption("nobootstrap");
            if (checkForBootstrapper)
            {
                Process thisProcess = Process.GetCurrentProcess();
                if (!thisProcess.IsBootstrapped())
                {
                    RsMessageBox.Show("The Shortcut Menu must be run via the bootstrapper.\n\n" +
                        "Please ensure you are launching the application via the ShortcutMenu.bat " +
                        " batch file (" + Path.Combine(_options.Branch.Environment.Subst(_shortcutMenuBinDirectory), "ShortcutMenu.bat") +
                        ") or are providing it with the --nobootstrap commandline argument.");
                    ForceExit();
                    return;
                }
            }

            // Create the taskbar icon by finding the resource.
            Log.Message("Initialising the taskbar icon.");
            _taskbarIcon = (RsTaskbarIcon)FindResource("TaskbarIcon");
            _taskbarIcon.DataContext = new TaskbarIconViewModel(_options.Branch);

            _contextMenu = _taskbarIcon.ContextMenu;
            Debug.Assert(_contextMenu != null, "Context menu is null");
            _contextMenu.ItemsSource = TrayMenuCommands;

            // Create a new restart task and routed command
            RestartTask restartTask = new RestartTask();

            _restartRoutedCommand = new TaskRoutedCommand(
                restartTask.Name,
                GetType(),
                new InputGestureCollection(),
                String.Empty,
                String.Empty,
                CommonIcons.Restart,
                restartTask);

            // Add a command definition for the command and add a binding.
            RockstarCommandManager.AddCommandDefinition(new InvokeMenuItemDefinition(_restartRoutedCommand, restartTask));
            RockstarCommandManager.AddBinding<IShortcutItemTask>(
                _taskbarIcon.ContextMenu,
                _restartRoutedCommand,
                this.OnInvokeTask,
                this.CanInvokeTask);

            // Set up the other bindings.
            RockstarCommandManager.AddBinding(
                _contextMenu, RockstarCommands.Exit, this.ExitInvoked);

            new ToggleBugstarCheckerAction(this.ApplicationResolver)
                .AddBinding(ShortcutMenuCommands.ToggleBugstarChecker, _contextMenu);

            // Remove the key binding associated with the exit command
            // (it won't work anyways and makes the shortcut menu wider than it needs to be).
            RockstarCommands.Exit.InputGestures.Clear();

            // Parse the config file.
            _shortcutMenuRoot = ParseShortcutMenuConfigFile();

            // Create the lookup for use by the invoke service.
            CreateItemLookup();

            // Recreate the list of command instances.
            UpdateCommandInstances();

            // Start up the WCF service.
            Log.Message("Starting shortcut menu service host.");
            try
            {
                ShortcutMenuService shortcutMenuService = new ShortcutMenuService((IShortcutItemInvoker)this);
                _shortcutMenuServiceHost = new ServiceHost(shortcutMenuService);
                _shortcutMenuServiceHost.Open();
            }
            catch (Exception ex)
            {
                Log.ToolException(ex, "Exception occurred while starting the shortcut menu service.");
                RsMessageBox.Show("Unable to start the shortcut menu service.\n\nThis may be because the port it requires is being used by another application or there is another instance of the shortcut menu running which hasn't closed down properly.");
                Shutdown(1);
            }
        }

        /// <summary>
        /// Refreshes the menu item's text based on the environment.
        /// </summary>
        private void RefreshMenuItemText()
        {
            foreach (KeyValuePair<CommandBarItem, IShortcutItem> pair in _menuItems)
            {
                if (pair.Value != null)
                {
                    pair.Key.Text = EscapeAndSubstItemName(pair.Value.Name, _options.Branch.Environment);
                }
            }
        }

        /// <summary>
        /// Make sure we notify the long running tasks that they should shutdown.
        /// </summary>
        /// <param name="persistentDirectory"></param>
        protected override void HandleOnExiting(string persistentDirectory)
        {
            _bugstarCheckTimer.Stop();

            if (_shortcutMenuServiceHost != null)
            {
                Log.Message("Closing shortcut menu service host.");
                if (_shortcutMenuServiceHost.State != CommunicationState.Faulted)
                {
                    _shortcutMenuServiceHost.Close();
                }
                else
                {
                    _shortcutMenuServiceHost.Abort();
                }
            }
            base.HandleOnExiting(persistentDirectory);
        }
        #endregion

        #region Initialisation
        /// <summary>
        /// Makes sure that the task bar icon resource dictionary is merged in with the
        /// applications resources.
        /// </summary>
        private void MergeTaskbarIconResources()
        {
            Collection<ResourceDictionary> resources = this.Resources.MergedDictionaries;
            if (resources == null)
            {
                return;
            }

            string dictionaryPath = "pack://application:,,,/ShortcutMenu;component/TaskbarIconResources.xaml";
            Uri source = new Uri(dictionaryPath, System.UriKind.RelativeOrAbsolute);

            ResourceDictionary newDictionary = new ResourceDictionary();
            try
            {
                newDictionary.Source = source;
            }
            catch (Exception)
            {
                Debug.Assert(newDictionary != null, "Unable to create the standard themes.");
                return;
            }

            resources.Add(newDictionary);
        }

        /// <summary>
        /// Acquires the Bugstar installation path.
        /// </summary>
        /// <returns></returns>
        private String GetBugstarExecutablePath()
        {
            String value = (String)Registry.GetValue(_bugstarIconRegistryKey, "", String.Empty);
            //Value comes in as exactly (include "): "C:\...\*.exe,1"
            if (String.IsNullOrEmpty(value))
            {
                //Bugstar has never been run, so missing registry
                return null;
            }
            char[] tokens = new char[1] { ',' };
            String[] splitValues = value.Split(tokens);
            return splitValues[0].Replace("\"", "");
        }
        #endregion

        #region Event Handlers
        /// <summary>
        /// Tick event handler for checking whether bugstar is currently running.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BugstarCheckTimer_Tick(object sender, EventArgs e)
        {
            Process[] pname = Process.GetProcessesByName("Bugstar");
            if (pname.Length == 0)
            {
                // Pop Notification only if we haven't pushed one yet.
                if (_shownBugstarNotRunningWarning == false)
                {
                    MessageBalloon balloon = new MessageBalloon(
                        "Bugstar Not Running",
                        "Bugstar is not currently running. Click on this popup to launch Bugstar.",
                        ShortcutMenuIcons.Information,
                        _defaultBalloonDisplayTimeout);

                    _taskbarIcon.ShowCustomBalloon(balloon, PopupAnimation.Fade, _defaultBalloonDisplayTimeout);
                    _taskbarIcon.CustomBalloon.Child.MouseDown += TaskbarIcon_TrayBalloonTipClicked;
                    _taskbarIcon.CustomBalloon.Closed += TaskbarIcon_TrayBalloonTipClosed;
                    _shownBugstarNotRunningWarning = true;
                }
            }
            else
            {
                _shownBugstarNotRunningWarning = false;
            }
        }

        /// <summary>
        /// Event handler that gets called when the task bars balloon tooltip closes.
        /// This is only used when a bugstar is not running balloon is shown.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TaskbarIcon_TrayBalloonTipClosed(object sender, EventArgs e)
        {
            Popup popup = sender as Popup;
            if (popup != null)
            {
                popup.Child.MouseDown -= TaskbarIcon_TrayBalloonTipClicked;
                popup.Closed -= TaskbarIcon_TrayBalloonTipClosed;
            }
        }

        /// <summary>
        /// Event handler that gets called when the "bugstar not running" balloon tooltip
        /// message is clicked on.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TaskbarIcon_TrayBalloonTipClicked(object sender, MouseButtonEventArgs e)
        {
            // Start bugstar if it exists.
            if (File.Exists(_bugstarExe))
            {
                Process.Start(_bugstarExe);
            }
            _shownBugstarNotRunningWarning = false;

            _taskbarIcon.CustomBalloon.Child.MouseDown -= TaskbarIcon_TrayBalloonTipClicked;
            _taskbarIcon.CustomBalloon.Closed -= TaskbarIcon_TrayBalloonTipClosed;
            _taskbarIcon.CloseBalloon();
        }

        /// <summary>
        /// Event handler when the FileSystemWatcher detects that the shortcut menu
        /// binaries/assemblies have been changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnShortcutsBinFolderChanged(object sender, FileSystemEventArgs e)
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => OnShortcutAssembliesChanged());
            }
            else
            {
                OnShortcutAssembliesChanged();
            }
        }

        /// <summary>
        /// Event handler when the FileSystemWatcher detects that the shortcut menu
        /// binaries/assemblies have been renamed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnShortcutsBinFolderRenamed(object sender, RenamedEventArgs e)
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => OnShortcutAssembliesChanged());
            }
            else
            {
                OnShortcutAssembliesChanged();
            }
        }

        /// <summary>
        /// Displays a balloon message the user informing them that the shortcut menu assemblies
        /// have changed and that a restart is required.
        /// </summary>
        private void OnShortcutAssembliesChanged()
        {
            if (_shownRestartRequiredNotification)
            {
                return;
            }
            _shownRestartRequiredNotification = true;

            MessageBalloon balloon = new MessageBalloon(
                "Shortcut Menu Updated",
                "The shortcut menu has been updated.  Please restart it at your earliest convenience.",
                ShortcutMenuIcons.Warning,
                null,
                true);

            //show balloon and close it after 4 seconds
            _taskbarIcon.ShowCustomBalloon(balloon, PopupAnimation.Slide, null);

            // Add a restart menu item as well in case the user closes the balloon.
            CommandBarItem menuItem =
                RockstarCommandManager.AddCommandInstance(
                    _restartRoutedCommand,
                    UInt16.MaxValue - 1,
                    true,
                    "Restart (Required for Shortcut Menu update)",
                    Guid.NewGuid(),
                    _trayMenuId);
        }

        /// <summary>
        /// Event handler when the FileSystemWatcher detects that the shortcut menu
        /// configuration files having been changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnShortcutsFileChanged(object sender, FileSystemEventArgs e)
        {
            if (!String.Equals(Path.GetExtension(e.FullPath), ".xml", StringComparison.InvariantCultureIgnoreCase))
            {
                return;
            }

            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => OnShortcutsFileChanged());
            }
            else
            {
                OnShortcutsFileChanged();
            }
        }

        /// <summary>
        /// Updates the shortcut menu items when an xml file in the shortcuts menu config directory changes.
        /// </summary>
        private void OnShortcutsFileChanged()
        {
            if (!_contextMenu.IsOpen)
            {
                // Parse the config file.
                _shortcutMenuRoot = ParseShortcutMenuConfigFile();

                // Create lookup mapping guid's to items.
                CreateItemLookup();

                // Populate the popup menu.
                UpdateCommandInstances();
            }
            else
            {
                // Defer the update to when the context menu closes.
                _contextMenu.Closed += ContextMenu_Closed;
            }
        }

        /// <summary>
        /// Event handler for when the context menu closes.  This is only used if the
        /// context menu was open when we detected a change to the shortcut menus
        /// configuration files.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ContextMenu_Closed(object sender, RoutedEventArgs e)
        {
            // Parse the config file.
            _shortcutMenuRoot = ParseShortcutMenuConfigFile();

            // Create lookup mapping guid's to items.
            CreateItemLookup();

            // Populate the popup menu.
            UpdateCommandInstances();

            // Remove the event handler.
            _contextMenu.Closed -= ContextMenu_Closed;
        }
        #endregion

        #region Shortcut Menu Utils
        /// <summary>
        /// Loads the shortcut menu configuration file.
        /// </summary>
        /// <returns></returns>
        private ShortcutMenuRoot ParseShortcutMenuConfigFile()
        {
            const int maxRetries = 10;
            const int retryWaitMs = 100;

            ShortcutMenuRoot menu = null;

            for (int attempt = 0; attempt < maxRetries; attempt++)
            {
                try
                {
                    String file = _options.Branch.Environment.Subst(_shortcutMenuConfigFilename);
                    menu = ShortcutMenuRoot.Load(file, _factory, Log);
                }
                catch (IOException ex)
                {
                    if (attempt < maxRetries - 1)
                    {
                        Log.Warning(
                            "Failed to load shortcut menu config. IOException, so will retry in {0}ms. ({1})",
                            retryWaitMs,
                            ex.Message);
                        Thread.Sleep(retryWaitMs);
                    }
                    else
                    {
                        Log.ToolException(ex, "Failed to parse the shortcut menu config file.");
                        break;
                    }
                }
                catch (System.Exception ex)
                {
                    Log.ToolException(ex, "Failed to parse the shortcut menu config file.");
                    break;
                }
            }

            return menu;
        }

        /// <summary>
        /// Creates the item lookup table based on the loaded shortcut items.
        /// </summary>
        private void CreateItemLookup()
        {
            _itemLookup.Clear();

            if (_shortcutMenuRoot != null)
            {
                PopulateItemLookup(_shortcutMenuRoot);
            }
        }

        /// <summary>
        /// Recursive method for populating the item lookup table.
        /// </summary>
        /// <param name="itemList"></param>
        /// <returns></returns>
        private void PopulateItemLookup(IShortcutItemGroup group)
        {
            foreach (IShortcutItem item in group.Items)
            {
                if (item.Guid != Guid.Empty)
                {
                    _itemLookup.Add(item.Guid, item);
                }

                if (item is IShortcutItemGroup)
                {
                    PopulateItemLookup((IShortcutItemGroup)item);
                }
            }
        }

        /// <summary>
        /// Updates the command instances that are registered with the command manager
        /// based on the shortcut menu configuration.
        /// </summary>
        private void UpdateCommandInstances()
        {
            // Remove all items first.
            foreach (CommandBarItem item in _menuItems.Keys)
            {
                RockstarCommandManager.RemoveCommandBarItem(item);
            }
            _menuItems.Clear();

            if (_shortcutMenuRoot != null)
            {
                CreateInstancesForGroup(_shortcutMenuRoot, _trayMenuId);
            }
        }

        /// <summary>
        /// Creates all the command instances for a shortcut item group.
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        private void CreateInstancesForGroup(IShortcutItemGroup group, Guid parentId)
        {
            bool nextItemStartsGroup = false;

            foreach (IShortcutItem item in group.Items)
            {
                if (item is IShortcutItemGroup)
                {
                    Guid groupGuid = Guid.NewGuid();
                    CommandBarItem menuItem =
                        RockstarCommandManager.AddMenu(
                            0,
                            nextItemStartsGroup,
                            EscapeAndSubstItemName(item.Name, _options.Branch.Environment),
                            groupGuid,
                            parentId);
                    _menuItems[menuItem] = item;

                    CreateInstancesForGroup((IShortcutItemGroup)item, groupGuid);

                    if (item.Name == "Utilities")
                    {
                        AddToggleCommandInstances(groupGuid);
                    }

                    nextItemStartsGroup = false;
                }
                else if (item is IShortcutItemTask)
                {
                    IShortcutItemTask task = (IShortcutItemTask)item;

                    TaskRoutedCommand routedCommand = new TaskRoutedCommand(
                        EscapeAndSubstItemName(item.Name, _options.Branch.Environment) + DateTime.UtcNow.Ticks.ToString(),
                        GetType(),
                        new InputGestureCollection(),
                        String.Empty,
                        String.Empty,
                        task.GetImage(_options.Branch.Environment),
                        task);

                    RockstarCommandManager.AddCommandDefinition(new InvokeMenuItemDefinition(routedCommand, task));
                    RockstarCommandManager.AddBinding<IShortcutItemTask>(
                        _taskbarIcon.ContextMenu,
                        routedCommand,
                        this.OnInvokeTask,
                        this.CanInvokeTask);

                    CommandBarItem menuItem =
                        RockstarCommandManager.AddCommandInstance(
                            routedCommand,
                            0,
                            nextItemStartsGroup,
                            EscapeAndSubstItemName(item.Name, _options.Branch.Environment),
                            Guid.NewGuid(),
                            parentId);
                    _menuItems[menuItem] = item;

                    nextItemStartsGroup = false;
                }
                else if (item is SeparatorItem)
                {
                    nextItemStartsGroup = true;
                }
                else if (item is IShortcutItem)
                {
                    CommandBarItem menuItem =
                        RockstarCommandManager.AddCommandInstance(
                            ShortcutMenuCommands.Null,
                            0,
                            nextItemStartsGroup,
                            EscapeAndSubstItemName(item.Name, _options.Branch.Environment),
                            Guid.NewGuid(),
                            parentId);
                    _menuItems[menuItem] = item;
                    nextItemStartsGroup = false;
                }
            }
        }

        /// <summary>
        /// Escapes underscores and substitutes variables using the provided environment.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="env"></param>
        /// <returns></returns>
        private String EscapeAndSubstItemName(String name, IEnvironment env)
        {
            String substedName = env.Subst(name);
            return substedName.Replace("_", "__");
        }

        /// <summary>
        /// Adds the bugstar/build version checker toggle options to the command manager.
        /// </summary>
        /// <param name="parentId"></param>
        private void AddToggleCommandInstances(Guid parentId)
        {
            CommandBarItem menuItem =
                RockstarCommandManager.AddCommandInstance(
                    ShortcutMenuCommands.ToggleBugstarChecker,
                    UInt16.MaxValue - 1,
                    true,
                    "Bugstar Checker",
                    Guid.NewGuid(),
                    parentId);
            _menuItems[menuItem] = null;

            menuItem =
                RockstarCommandManager.AddCommandInstance(
                    ShortcutMenuCommands.ToggleBuildVersionChecker,
                    UInt16.MaxValue,
                    false,
                    "Build Version Checker",
                    Guid.NewGuid(),
                    parentId);
            _menuItems[menuItem] = null;
        }

        /// <summary>
        /// Retrieves shortcut items of a specific type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="menu"></param>
        /// <returns></returns>
        private IEnumerable<T> GetShortcutItemsOfType<T>(ShortcutMenuRoot menu) where T : IShortcutItem
        {
            foreach (T returnItem in GetShortcutItemsOfType<T>(menu))
            {
                yield return returnItem;
            }
        }

        /// <summary>
        /// Retrieves shortcut items of a specific type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="menu"></param>
        /// <returns></returns>
        private IEnumerable<T> GetShortcutItemsOfType<T>(IShortcutItemGroup group) where T : IShortcutItem
        {
            foreach (IShortcutItem item in group.Items)
            {
                if (item is T)
                {
                    yield return (T)item;
                }

                if (item is IShortcutItemGroup)
                {
                    foreach (T returnItem in GetShortcutItemsOfType<T>((IShortcutItemGroup)item))
                    {
                        yield return returnItem;
                    }
                }
            }
        }
        #endregion

        #region Command Handlers
        /// <summary>
        /// Called whenever the <see cref="RockstarCommands.Exit"/> command is fired.
        /// </summary>
        /// <param name="data">
        /// The execute data that has been sent with the command.
        /// </param>
        private void ExitInvoked(ExecuteCommandData data)
        {
            OnExit(data);
        }

        /// <summary>
        /// Resolver for getting at the shortcut menu application.
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public ShortcutMenuApp ApplicationResolver(CommandData data)
        {
            return this;
        }

        /// <summary>
        /// CanExecute handler for whenever we need to determine if a
        /// <see cref="TaskRoutedCommand"/> command can be executed.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private bool CanInvokeTask(CanExecuteCommandData<IShortcutItemTask> data)
        {
            return true;
        }

        /// <summary>
        /// Execute handler for whenever a <see cref="TaskRoutedCommand"/> command is fired.
        /// </summary>
        /// <param name="data">
        /// The execute data that has been sent with the command.
        /// </param>
        private void OnInvokeTask(ExecuteCommandData<IShortcutItemTask> data)
        {
            try
            {
                data.Parameter.Invoke(new TaskInvokeArgs(this, _options.Branch, Log));
            }
            catch (System.Exception ex)
            {
                String taskName = _options.Branch.Environment.Subst(data.Parameter.Name);
                Log.ToolException(ex, "Exception occurred while running the {0} task", taskName);

                IMessageBoxService msgService = this.GetService<IMessageBoxService>();
                if (msgService != null)
                {
                    String message = String.Format("An unhandled exception occurred while attempting to run the '{0}' task.\n\n" +
                        "Please contact the tools team attaching the shorcutmenu log file.", taskName);
                    msgService.Show(message, null, MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
        #endregion

        #region IShortcutItemInvoker Implementation
        /// <summary>
        /// Invokes the particular shortcut item based on it's guid.
        /// </summary>
        /// <param name="guid"></param>
        /// <returns>Whether an item with the specified guid exists.</returns>
        public bool InvokeShortcutItem(Guid guid)
        {
            bool invoked = false;

            IShortcutItem item;
            if (_itemLookup.TryGetValue(guid, out item))
            {
                IShortcutItemTask taskItem = item as IShortcutItemTask;
                if (taskItem != null)
                {
                    try
                    {
                        taskItem.Invoke(new TaskInvokeArgs(this, _options.Branch, Log));
                        invoked = true;
                    }
                    catch (System.Exception ex)
                    {
                        String taskName = _options.Branch.Environment.Subst(taskItem.Name);
                        Log.ToolException(ex, "Exception occurred while running the {0} task from the service.", taskName);
                        throw;
                    }
                }
            }

            return invoked;
        }
        #endregion
    }
}
