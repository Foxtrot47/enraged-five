﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using RSG.Base.Extensions;

namespace ShortcutMenu
{
    /// <summary>
    /// Provides static properties that give access to common icons that can be used by the
    /// shortcut menu application.
    /// </summary>
    public static class ShortcutMenuIcons
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Information"/> property.
        /// </summary>
        private static BitmapSource _information;

        /// <summary>
        /// The private field used for the <see cref="Warning"/> property.
        /// </summary>
        private static BitmapSource _warning;
        /// <summary>
        /// A generic object that provides thread safety access to the image sources.
        /// </summary>
        private static object _syncRoot = new object();
        #endregion

        #region Properties
        /// <summary>
        /// Gets the icon that shows a add action icon.
        /// </summary>
        public static BitmapSource Information
        {
            get
            {
                if (_information == null)
                {
                    lock (_syncRoot)
                    {
                        if (_information == null)
                        {
                            EnsureLoaded(ref _information, "information32.png");
                        }
                    }
                }

                return _information;
            }
        }

        /// <summary>
        /// Gets the icon that shows a blank file icon.
        /// </summary>
        public static BitmapSource Warning
        {
            get
            {
                if (_warning == null)
                {
                    lock (_syncRoot)
                    {
                        if (_warning == null)
                        {
                            EnsureLoaded(ref _warning, "warning32.png");
                        }
                    }
                }

                return _warning;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Ensures that the specified image source is loaded with the specified resource name.
        /// </summary>
        /// <param name="source">
        /// The image source that should be loaded.
        /// </param>
        /// <param name="resourceName">
        /// The name of the resource the loaded image source should set as its source.
        /// </param>
        private static void EnsureLoaded(ref BitmapSource source, string resourceName)
        {
            if (source != null)
            {
                return;
            }

            string assemblyName = typeof(ShortcutMenuIcons).Assembly.GetName().Name;
            string path = "Resources/" + resourceName;
            string bitmapPath = "pack://application:,,,/{0};component/{1}";
            bitmapPath = bitmapPath.FormatInvariant(assemblyName, path);
            Uri uri = new Uri(bitmapPath, UriKind.RelativeOrAbsolute);

            source = new BitmapImage(uri);
            source.Freeze();
        }
        #endregion
    }
}
