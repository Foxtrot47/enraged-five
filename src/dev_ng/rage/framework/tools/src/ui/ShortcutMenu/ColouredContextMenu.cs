﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using RSG.Editor;
using RSG.Editor.Controls.Commands;

namespace ShortcutMenu
{
    /// <summary>
    /// A customisation of the <see cref="RSG.Editor.Controls.Commands.RsContextMenu"/> class
    /// which adds support for coloured menu items.
    /// </summary>
    public class ColouredContextMenu : RsContextMenu
    {
        #region Methods
        /// <summary>
        /// Creates or identifies the element used to display the child items.
        /// </summary>
        /// <returns>
        /// A new <see cref="ColouredMenuItem"/>.
        /// </returns>
        protected override DependencyObject GetContainerForItemOverride()
        {
            ColouredMenuItem newItem = new ColouredMenuItem();
            newItem.IsPlacedInContextMenu = true;
            return newItem;
        }
        #endregion
    }
}
