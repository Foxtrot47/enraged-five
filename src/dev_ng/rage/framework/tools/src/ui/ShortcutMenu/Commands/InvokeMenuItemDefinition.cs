﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using RSG.Editor;
using RSG.ShortcutMenu.Tasks;

namespace ShortcutMenu.Commands
{
    /// <summary>
    /// Command definition for invoking a menu item.
    /// </summary>
    public class InvokeMenuItemDefinition : ButtonCommand
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Task"/> property.
        /// </summary>
        private readonly IShortcutItemTask _task;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="InvokeMenuItemDefinition"/> class.
        /// </summary>
        /// <param name="command">Task to invoke.</param>
        public InvokeMenuItemDefinition(TaskRoutedCommand command, IShortcutItemTask task)
            : base(command)
        {
            _task = task;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the parameter that is routed through the element tree with the command.
        /// </summary>
        public override object CommandParameter
        {
            get { return ((TaskRoutedCommand)Command).Task; }
        }

        /// <summary>
        /// Shortcut menu item this definition is for.
        /// </summary>
        public IShortcutItemTask Task
        {
            get { return _task; }
        }
        #endregion
    }
}
