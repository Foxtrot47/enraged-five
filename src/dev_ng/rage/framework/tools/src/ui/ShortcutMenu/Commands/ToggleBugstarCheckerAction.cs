﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Editor;

namespace ShortcutMenu.Commands
{
    /// <summary>
    /// Toggle action for modifying the state of the automatic bugstar checker.
    /// </summary>
    public class ToggleBugstarCheckerAction : ToggleButtonAction<ShortcutMenuApp>
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="ToggleBugstarCheckerAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public ToggleBugstarCheckerAction(ParameterResolverDelegate<ShortcutMenuApp> resolver)
            : base(resolver)
        {
        }
        #endregion

        #region Methods
        /// <summary>
        /// Determines what the initial toggle state for the definition to the specified
        /// command is.
        /// </summary>
        /// <param name="command">
        /// The command whose definitions toggle state is being set.
        /// </param>
        /// <returns>
        /// The initial toggle state for the definition to the specified command.
        /// </returns>
        protected override bool GetInitialToggleState(RockstarRoutedCommand command)
        {
            return true;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="app">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// A task that represents the work done by the command handler.
        /// </returns>
        public override void Execute(ShortcutMenuApp app)
        {
            app.BugstarCheckerEnabled = IsToggled;
        }
        #endregion
    }
}
