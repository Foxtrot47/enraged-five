﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using RSG.Editor;
using RSG.Editor.Controls.Commands;
using RSG.Editor.Controls.Helpers;
using ShortcutMenu.Commands;
using System.Windows.Documents;

namespace ShortcutMenu
{
    /// <summary>
    /// A customisation of the <see cref="RSG.Editor.Controls.Commands.RsMenuItem"/> class
    /// which adds support for coloured menu items.
    /// </summary>
    public class ColouredMenuItem : RsMenuItem
    {
        #region Fields
        /// <summary>
        /// Reference to the grid that has been defined inside the control template.
        /// </summary>
        private Grid _enclosingGrid;
        #endregion

        #region Methods
        /// <summary>
        /// Called whenever the control template for this instance is applied.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            this._enclosingGrid = this.GetTemplateChild("PART_EnclosingGrid") as Grid;
            if (this._enclosingGrid != null)
            {
                // Set the colourings....
                CommandBarItemViewModel viewmodel = this.DataContext as CommandBarItemViewModel;
                if (viewmodel != null)
                {
                    InvokeMenuItemDefinition definition = viewmodel.CommandDefinition as InvokeMenuItemDefinition;
                    if (definition != null && definition.Task != null)
                    {
                        if (definition.Task.BackgroundColor != Colors.Transparent)
                        {
                            _enclosingGrid.Background = new SolidColorBrush(definition.Task.BackgroundColor);
                        }
                        if (definition.Task.ForegroundColor != Colors.Transparent)
                        {
                            TextElement.SetForeground(_enclosingGrid, new SolidColorBrush(definition.Task.ForegroundColor));
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Creates or identifies the element used to display the child items.
        /// </summary>
        /// <returns>
        /// A new <see cref="ColouredMenuItem"/>.
        /// </returns>
        protected override DependencyObject GetContainerForItemOverride()
        {
            ColouredMenuItem newItem = new ColouredMenuItem();
            newItem.IsPlacedInContextMenu = this.IsPlacedInContextMenu;
            return newItem;
        }
        #endregion
    }
}
