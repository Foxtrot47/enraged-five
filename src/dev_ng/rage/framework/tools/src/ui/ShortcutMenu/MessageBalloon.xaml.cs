﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RSG.Base.OS;
using RSG.Editor.Controls;

namespace ShortcutMenu
{
    /// <summary>
    /// Interaction logic for NotificationBalloon.xaml
    /// </summary>
    public partial class MessageBalloon : RsUserControl
    {
        #region Fields
        /// <summary>
        /// The timeout to set when the user hovers over the balloon.
        /// </summary>
        private readonly int? _closeTimeout;

        /// <summary>
        /// Flag indicating whether the balloon is closing.
        /// </summary>
        private bool _isClosing = false;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MessageBalloon"/> class using
        /// the provided caption, message and icon.
        /// </summary>
        /// <param name="caption"></param>
        /// <param name="message"></param>
        /// <param name="icon"></param>
        /// <param name="showRestartButton"></param>
        public MessageBalloon(string caption, string message, BitmapSource icon, int? closeTimeout, bool showRestartButton = false)
        {
            _closeTimeout = closeTimeout;

            InitializeComponent();

            CaptionTextBlock.Text = caption;
            MessageTextBlock.Text = message;
            MainImage.IconBitmap = icon;

            if (showRestartButton)
            {
                RestartButton.Visibility = Visibility.Visible;
            }

            RsTaskbarIcon.AddBalloonClosingHandler(this, OnBalloonClosing);
        }
        #endregion

        #region Event Handlers
        /// <summary>
        /// By subscribing to the <see cref="RsTaskbarIcon.BalloonClosingEvent"/>
        /// and setting the "Handled" property to true, we suppress the popup
        /// from being closed in order to display the custom fade-out animation.
        /// </summary>
        private void OnBalloonClosing(object sender, RoutedEventArgs e)
        {
            // Suppresses the popup from being closed immediately
            e.Handled = true;
            _isClosing = true;
        }

        /// <summary>
        /// Resolves the <see cref="RsTaskbarIcon"/> that displayed
        /// the balloon and requests a close action.
        /// </summary>
        private void CloseImage_MouseDown(object sender, MouseButtonEventArgs e)
        {
            //the tray icon assigned this attached property to simplify access
            RsTaskbarIcon taskbarIcon = RsTaskbarIcon.GetParentTaskbarIcon(this);
            taskbarIcon.CloseBalloon();
            e.Handled = true;
        }

        /// <summary>
        /// If the users hovers over the balloon, we don't close it.
        /// </summary>
        private void MainGrid_MouseEnter(object sender, MouseEventArgs e)
        {
            //if we're already running the fade-out animation, do not interrupt anymore
            //(makes things too complicated for the sample)
            if (_isClosing)
            {
                return;
            }

            //the tray icon assigned this attached property to simplify access
            RsTaskbarIcon taskbarIcon = RsTaskbarIcon.GetParentTaskbarIcon(this);
            taskbarIcon.ResetBalloonCloseTimer();
        }

        /// <summary>
        /// If the users leaves the balloon reset the close timeout.
        /// </summary>
        private void MainGrid_MouseLeave(object sender, MouseEventArgs e)
        {
            //if we're already running the fade-out animation, do not interrupt anymore
            //(makes things too complicated for the sample)
            if (_isClosing)
            {
                return;
            }

            //the tray icon assigned this attached property to simplify access
            RsTaskbarIcon taskbarIcon = RsTaskbarIcon.GetParentTaskbarIcon(this);
            taskbarIcon.ResetBalloonCloseTimer(_closeTimeout);
        }

        /// <summary>
        /// Closes the popup once the fade-out animation completed.
        /// The animation was triggered in XAML through the attached
        /// BalloonClosing event.
        /// </summary>
        private void OnFadeOutCompleted(object sender, EventArgs e)
        {
            Popup pp = (Popup)Parent;
            pp.IsOpen = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RestartButton_Click(object sender, RoutedEventArgs e)
        {
            RsApplication app = Application.Current as RsApplication;
            if (app != null)
            {
                // Close the popup and restart the application.
                RsTaskbarIcon taskbarIcon = RsTaskbarIcon.GetParentTaskbarIcon(this);
                taskbarIcon.CloseBalloon();

                app.Shutdown(ExitCode.Restart);
            }
        }
        #endregion
    }
}
