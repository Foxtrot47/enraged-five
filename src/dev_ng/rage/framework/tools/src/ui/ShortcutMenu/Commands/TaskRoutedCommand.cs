﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using RSG.Editor;
using RSG.ShortcutMenu.Tasks;

namespace ShortcutMenu.Commands
{
    /// <summary>
    /// Custom routed command for a shortcut menu item task.  This is required so that the
    /// <see cref="InvokeMenuItemDefinition"/> class can get access to the shortcut menu
    /// item task for use as the command parameter.
    /// </summary>
    public class TaskRoutedCommand : RockstarRoutedCommand
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Task"/> property.
        /// </summary>
        private readonly IShortcutItemTask _task;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="TaskRoutedCommand" /> class.
        /// </summary>
        /// <param name="name">
        /// Declared name for serialization.
        /// </param>
        /// <param name="ownerType">
        /// The type that is registering the command.
        /// </param>
        /// <param name="inputGestures">
        /// Default input gestures associated with this command.
        /// </param>
        /// <param name="category">
        /// The name of the category this command belongs to.
        /// </param>
        /// <param name="description">
        /// The description for this command.
        /// </param>
        /// <param name="icon">
        /// The icon this command should use.
        /// </param>
        public TaskRoutedCommand(
            string name,
            Type ownerType,
            InputGestureCollection inputGestures,
            string category,
            string description,
            BitmapSource icon,
            IShortcutItemTask task)
            : base(name, ownerType, inputGestures, category, description, icon)
        {
            _task = task;
        }
        #endregion

        #region Properties
        /// <summary>
        /// The task this routed command is for.
        /// </summary>
        public IShortcutItemTask Task
        {
            get { return _task; }
        }
        #endregion
    }
}
