﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace CutFileViewer
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            string strFile = String.Empty;

            if (args.Length > 0)
            {
                strFile = args[0];
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1(strFile));
        }
    }
}
