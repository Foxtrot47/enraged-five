﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Collections;
using System.IO;
using System.Text.RegularExpressions;

namespace CutFileViewer
{
    public partial class Form1 : Form
    {
        public class CutObject
        {
            public string strName = String.Empty;
            public string strType = String.Empty;
            public int iObjectId;
            public long iStreamingHash;
            public string strStreamingName = String.Empty;
        }

        public class CutEventArg
        {
            public string strName = String.Empty;
            public string strType = String.Empty;
            public float fTotalDuration = -1;
            public int iAttachedObject = -1;
            public int iAttachedBoneHash = -1;
            public ArrayList alObjectId = new ArrayList();

            // camera cut
            public float fFarDrawDistance = 0;
            public float fNearDrawDistance = 0;
        }

        public class CutEvent
        {
            public string fTime;
            public string strType = String.Empty;
            public int iEventArgRef;
            public int iEventID;
            public int iObjectID;
        }

        public Form1(string strFile)
        {
            InitializeComponent();

            filterList = eventList;

            if (strFile != String.Empty)
                Open(strFile);
        }

        uint uiFlags = 0;
        float fTotalDuration = 0;
        int iRangeStart = 0;
        int iRangeEnd = 0;
        bool bListArgs = false;
        private ArrayList alObjects = new ArrayList();
        private ArrayList alEventArgs = new ArrayList();
        private ArrayList alLoadEvents = new ArrayList();
        private ArrayList alEvents = new ArrayList();
        private string[] flagList = new string[] { 
                    "CUTSCENE_FADE_IN_GAME_FLAG",
        "CUTSCENE_FADE_OUT_GAME_FLAG",       
        "CUTSCENE_FADE_IN_FLAG",                 
        "CUTSCENE_FADE_OUT_FLAG",                
        "CUTSCENE_SHORT_FADE_OUT_FLAG",           
        "CUTSCENE_LONG_FADE_OUT_FLAG",           
        "CUTSCENE_FADE_BETWEEN_SECTIONS_FLAG",   

        // Lights
        "CUTSCENE_NO_AMBIENT_LIGHTS_FLAG",        // Set when the ambient lights should be turned off for the duration of the cut scene
        "CUTSCENE_NO_VEHICLE_LIGHTS_FLAG",        // Set when the vehicle lights should be turned off for the duration of the cut scene

		// Audio
        "CUTSCENE_USE_ONE_AUDIO_FLAG",            // Set when planning to concatenate cutscenes and there will be only 1 audio track for the entire scene
        "CUTSCENE_MUTE_MUSIC_PLAYER_FLAG",        // Set when game music should be muted for the entire cut scene
        "CUTSCENE_LEAK_RADIO_FLAG",               // Set when it is ok for the game music (from the radio) can play through the cut scene

        // Misc editor flags
        "CUTSCENE_TRANSLATE_BONE_IDS_FLAG",       // Set when the bone ids need to be translated using the spec file during export  
        "CUTSCENE_INTERP_CAMERA_FLAG",            // Set when we should interpolate between camera cuts during export

        // Sectioning flags
        "CUTSCENE_IS_SECTIONED_FLAG",             // Indicates when the data contained in the file (and the animations) have been sectioned
        "CUTSCENE_SECTION_BY_CAMERA_CUTS_FLAG",   // Set when we should segment based on the camera cuts
        "CUTSCENE_SECTION_BY_DURATION_FLAG",      // Set when we should segment based on a specified duration for each section
        "CUTSCENE_SECTION_BY_SPLIT_FLAG",         // Set when we should segment on the camera's SectionSplit attribute keys

        "CUTSCENE_USE_PARENT_SCALE_FLAG",         // Set when exported models should factor in their parent node's scale

        "CUTSCENE_USE_ONE_SCENE_ORIENTATION_FLAG",// Set when planning to concatenate cutscenes and we should use only the first scene's orientation

        "CUTSCENE_ENABLE_DEPTH_OF_FIELD_FLAG",    // Set when the depth of field camera settings are ready for prime time

		"CUTSCENE_STREAM_PROCESSED",

		"CUTSCENE_USE_STORY_MODE_FLAG",

		"CUTSCENE_USE_IN_GAME_DOF_START_FLAG",
		"CUTSCENE_USE_IN_GAME_DOF_END_FLAG",
        "CUTSCENE_USE_CATCHUP_CAMERA_FLAG",
        "CUTSCENE_USE_BLENDOUT_CAMERA_FLAG",
		"CUTSCENE_PART_FLAG",
		"CUTSCENE_INTERNAL_CONCAT_FLAG",
		"CUTSCENE_EXTERNAL_CONCAT_FLAG",
        "CUTSCENE_USE_AUDIO_EVENTS_CONCAT_FLAG",

        };
        private string[] eventList = new string[] { 

        "CUTSCENE_LOAD_SCENE_EVENT",              // Tell the game to get ready for the cut scene using the offset and extra room info.  cutfLoadSceneEventArgs
    "CUTSCENE_UNLOAD_SCENE_EVENT",            // Unload the scene
    "CUTSCENE_LOAD_ANIM_DICT_EVENT",          // Load an animation dictionary.  cutfNameEventArgs
    "CUTSCENE_UNLOAD_ANIM_DICT_EVENT",        // Unload the animation dictionary.
    "CUTSCENE_LOAD_AUDIO_EVENT",              // Load/preseek the audio.  cutfNameEventArgs
    "CUTSCENE_UNLOAD_AUDIO_EVENT",            // Unload the audio.  cutfNameEventArgs
    "CUTSCENE_LOAD_MODELS_EVENT",             // Load the models needed for the scene.  cutfObjectIdListEventArgs
    "CUTSCENE_UNLOAD_MODELS_EVENT",           // Unload the models that are no longer needed for the scene.  cutfObjectIdListEventArgs
    "CUTSCENE_LOAD_PARTICLE_EFFECTS_EVENT",   // Load the particle effects.  cutfObjectIdListEventArgs
    "CUTSCENE_UNLOAD_PARTICLE_EFFECTS_EVENT", // Unload the particle effects.  cutfObjectIdListEventArgs
    "CUTSCENE_LOAD_OVERLAYS_EVENT",           // Load the screen overlays.  cutfObjectIdListEventArgs
    "CUTSCENE_UNLOAD_OVERLAYS_EVENT",         // Unload the overlays.  cutfObjectIdListEventArgs
    "CUTSCENE_LOAD_SUBTITLES_EVENT",          // Load the subtitles.  cutfNameEventArgs
    "CUTSCENE_UNLOAD_SUBTITLES_EVENT",        // Unload the subtitles.  cutfNameEventArgs
    "CUTSCENE_HIDE_OBJECTS_EVENT",            // Hide the specified objects.  cutfObjectIdListEventArgs
    "CUTSCENE_SHOW_OBJECTS_EVENT",            // Unhide the specified objects.  cutfObjectIdListEventArgs
    "CUTSCENE_FIXUP_OBJECTS_EVENT",           // Fixup the specified physics objects.  cutfObjectIdListEventArgs
    "CUTSCENE_REVERT_FIXUP_OBJECTS_EVENT",    // Undo the fixup to the specified physics objects.  cutfObjectIdListEventArgs
    "CUTSCENE_ADD_BLOCKING_BOUNDS_EVENT",     // Add the blocking bounds.  cutfObjectIdListEventArgs
    "CUTSCENE_REMOVE_BLOCKING_BOUNDS_EVENT",  // Remove the blocking bounds.    cutfObjectIdListEventArgs
        
    // Playback events
    "CUTSCENE_FADE_OUT_EVENT",                // Fade out to black.  cutfScreenFadeEventArgs
    "CUTSCENE_FADE_IN_EVENT",                 // Fade in from black.  cutfScreenFadeEventArgs
    "CUTSCENE_SET_ANIM_EVENT",                // Set the model to play the given animation.  cutfObjectIdNameEventArgs
    "CUTSCENE_CLEAR_ANIM_EVENT",              // Clear the animation from the model.  cutfObjectIdNameEventArgs
    "CUTSCENE_PLAY_PARTICLE_EFFECT_EVENT",    // Trigger the particle effect.  cutfPlayParticleEffectEventArgs
    "CUTSCENE_STOP_PARTICLE_EFFECT_EVENT",    // Stop the particle effect.  cutfPlayParticleEffectEventArgs
    "CUTSCENE_SHOW_OVERLAY_EVENT",            // Show the overlay.  cutfEventArgs
    "CUTSCENE_HIDE_OVERLAY_EVENT",            // Hide the overlay.  cutfEventArgs
    "CUTSCENE_PLAY_AUDIO_EVENT",              // Play audio.  cutfNameEventArgs
    "CUTSCENE_STOP_AUDIO_EVENT",              // Stop audio.  cutfNameEventArgs
    "CUTSCENE_SHOW_SUBTITLE_EVENT",           // Show the subtitle.  cutfSubtitleEventArgs
    "CUTSCENE_HIDE_SUBTITLE_EVENT",           // Hide the subtitle.  cutfSubtitleEventArgs
    "CUTSCENE_SET_DRAW_DISTANCE_EVENT",       // Set the draw distance.  cutfTwoFloatValuesEventArgs.  Data to CUTSCENE_CAMERA_CUT_EVENT.  In practice, this event will only be fired when editing the Draw Distances with the Rag Widgets.
    "CUTSCENE_SET_ATTACHMENT_EVENT",          // Sets up a parent-child relationship between two model objects, connected at a specified bone.  cutfAttachmentEventArgs
    "CUTSCENE_SET_VARIATION_EVENT",           // Set a ped variation.  Technically, this is considered a custom GTA event, but is included for backwards compatibility.  ???
    "CUTSCENE_ACTIVATE_BLOCKING_BOUNDS_EVENT",    // Activate the blocking bounds.  cutfEventArgs
    "CUTSCENE_DEACTIVATE_BLOCKING_BOUNDS_EVENT",  // Deactivate the blocking bounds.  cutfEventArgs
    "CUTSCENE_HIDE_HIDDEN_OBJECT_EVENT",      // Hide the hidden object.  cutfEventArgs
    "CUTSCENE_SHOW_HIDDEN_OBJECT_EVENT",      // Show the hidden object.  cutfEventArgs
    "CUTSCENE_FIX_FIXUP_OBJECT_EVENT",        // Fix the fixup object.  cutfEventArgs
    "CUTSCENE_REVERT_FIXUP_OBJECT_EVENT",     // Revert the fixup object.  cutfEventArgs

    // New Load events
    "CUTSCENE_ADD_REMOVAL_BOUNDS_EVENT",      // Add the removal bounds.  cutfObjectIdListEventArgs
    "CUTSCENE_REMOVE_REMOVAL_BOUNDS_EVENT",   // Remove the removal bounds.    cutfObjectIdListEventArgs

    // New Playback events
    "CUTSCENE_CAMERA_CUT_EVENT",              // A camera cut.  cutfCameraCutEventArgs
    "CUTSCENE_ACTIVATE_REMOVAL_BOUNDS_EVENT",     // Activate the removal bounds.  cutfEventArgs
    "CUTSCENE_DEACTIVATE_REMOVAL_BOUNDS_EVENT",   // Deactivate the removal bounds.  cutfEventArgs

	"CUTSCENE_LOAD_RAYFIRE_EVENT",			// Load the rayfire's needed for the scene.  
	"CUTSCENE_UNLOAD_RAYFIRE_EVENT",
    "CUTSCENE_ENABLE_DOF_EVENT",
	"CUTSCENE_DISABLE_DOF_EVENT",
    "CUTSCENE_CATCHUP_CAMERA_EVENT",
    "CUTSCENE_BLENDOUT_CAMERA_EVENT",
    "CUTSCENE_TRIGGER_DECAL_EVENT",
    "CUTSCENE_REMOVE_DECAL_EVENT",

    "CUTSCENE_ENABLE_CASCADE_SHADOW_BOUNDS_EVENT",

	"CUTSCENE_CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER", 
	"CUTSCENE_CASCADE_SHADOWS_SET_WORLD_HEIGHT_UPDATE", 
	"CUTSCENE_CASCADE_SHADOWS_SET_RECEIVER_HEIGHT_UPDATE", 
	"CUTSCENE_CASCADE_SHADOWS_SET_AIRCRAFT_MODE", 
	"CUTSCENE_CASCADE_SHADOWS_SET_DYNAMIC_DEPTH_MODE", 
	"CUTSCENE_CASCADE_SHADOWS_SET_FLY_CAMERA_MODE", 

	"CUTSCENE_CASCADE_SHADOWS_SET_CASCADE_BOUNDS_HFOV", 
	"CUTSCENE_CASCADE_SHADOWS_SET_CASCADE_BOUNDS_VFOV",
	"CUTSCENE_CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SCALE", 
	"CUTSCENE_CASCADE_SHADOWS_SET_ENTITY_TRACKER_SCALE", 
	"CUTSCENE_CASCADE_SHADOWS_SET_SPLIT_Z_EXP_WEIGHT",
	"CUTSCENE_CASCADE_SHADOWS_SET_DITHER_RADIUS_SCALE", 
	
	"CUTSCENE_CASCADE_SHADOWS_SET_WORLD_HEIGHT_MINMAX",
	"CUTSCENE_CASCADE_SHADOWS_SET_RECEIVER_HEIGHT_MINMAX", 
	"CUTSCENE_CASCADE_SHADOWS_SET_DEPTH_BIAS", 
	"CUTSCENE_CASCADE_SHADOWS_SET_SLOPE_BIAS", 

	"CUTSCENE_CASCADE_SHADOWS_SET_SHADOW_SAMPLE_TYPE", 

    "CUTSCENE_RESET_ADAPTION_EVENT", 
	"CUTSCENE_CASCADE_SHADOWS_SET_DYNAMIC_DEPTH_VALUE",
	
	"CUTSCENE_SET_LIGHT_EVENT",
	"CUTSCENE_CLEAR_LIGHT_EVENT", 

    "CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE"
        
        };
        private string[] filterList;

        private void ReadInfo(string strCutXMLFile)
        {
            using (XmlTextReader textReader = new XmlTextReader(strCutXMLFile))
            {
                if (textReader.ReadToFollowing("fTotalDuration"))
                {
                    if (textReader.HasAttributes)
                    {
                        fTotalDuration = (float)Convert.ToDouble(textReader.GetAttribute(0));
                    }
                }

                textReader.Close();
            }

            using (XmlTextReader textReader = new XmlTextReader(strCutXMLFile))
            {
                if (textReader.ReadToFollowing("iRangeStart"))
                {
                    if (textReader.HasAttributes)
                    {
                        iRangeStart = (int)Convert.ToInt32(textReader.GetAttribute(0));
                    }
                }

                textReader.Close();
            }

            using (XmlTextReader textReader = new XmlTextReader(strCutXMLFile))
            {
                if (textReader.ReadToFollowing("iRangeEnd"))
                {
                    if (textReader.HasAttributes)
                    {
                        iRangeEnd = (int)Convert.ToInt32(textReader.GetAttribute(0));
                    }
                }

                textReader.Close();
            }
        }

        private void ReadFlags(string strCutXMLFile)
        {
            using (XmlTextReader textReader = new XmlTextReader(strCutXMLFile))
            {
                if (textReader.ReadToFollowing("iCutsceneFlags"))
                {
                    string strInner = textReader.ReadInnerXml();
                    strInner = strInner.Replace(" ", "");
                    string[] aFlags = Regex.Split(strInner, "\r\n");

                    uiFlags = UInt32.Parse(aFlags[1]);
                }

                textReader.Close();
            }
        }

        private void ReadObjects(string strCutXMLFile)
        {
            var xmlDoc = new XmlDocument();
            xmlDoc.Load(strCutXMLFile);

            XmlNodeList items = xmlDoc.SelectNodes("/rage__cutfCutsceneFile2/pCutsceneObjects/Item");

            foreach (XmlNode item in items)
            {
                CutObject cutsceneObject = new CutObject();

                if (item.Attributes.Count > 0)
                {
                    cutsceneObject.strType = item.Attributes[0].Value;

                    foreach (XmlNode child in item.ChildNodes)
                    {
                        if (child.Name == "cName")
                        {
                            cutsceneObject.strName = child.InnerText;
                        }

                        if (child.Name == "iObjectId")
                        {
                            if (child.Attributes.Count > 0)
                            {
                                cutsceneObject.iObjectId = Convert.ToInt32(child.Attributes[0].Value);
                            }
                        }

                        if (child.Name == "StreamingName")
                        {
                            cutsceneObject.strStreamingName = child.InnerText;
                        }

                        if (child.Name == "AnimStreamingBase")
                        {
                            if (child.Attributes.Count > 0)
                            {
                                cutsceneObject.iStreamingHash = Convert.ToInt64(child.Attributes[0].Value);
                            }
                        }
                    }

                    if (cutsceneObject.strName == String.Empty)
                    {
                        if (cutsceneObject.strType == "rage__cutfAssetManagerObject")
                        {
                            cutsceneObject.strName = "AssetManager";
                        }

                        if (cutsceneObject.strType == "rage__cutfAnimationManagerObject")
                        {
                            cutsceneObject.strName = "AnimationManager";
                        }
                    }

                    alObjects.Add(cutsceneObject);
                }
            }
        }

        private void ReadEventArgs(string strCutXMLFile)
        {
            var xmlDoc = new XmlDocument();
            xmlDoc.Load(strCutXMLFile);

            XmlNodeList items = xmlDoc.SelectNodes("/rage__cutfCutsceneFile2/pCutsceneEventArgsList/Item");

            foreach (XmlNode item in items)
            {
                CutEventArg cutsceneArg = new CutEventArg();

                if (item.Attributes.Count > 0)
                {
                    cutsceneArg.strType = item.Attributes[0].Value;

                    if (cutsceneArg.strType == "rage__cutfObjectIdListEventArgs")
                    {
                        foreach (XmlNode child in item)
                        {
                            if (child.Name == "iObjectIdList")
                            {
                                string strContents = child.InnerText;
                                strContents = strContents.Replace(System.Environment.NewLine, " ");
                                strContents = strContents.Replace("\t", "");
                                string[] saSplit = strContents.Split(new Char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                for (int i = 0; i < saSplit.Length; ++i)
                                {
                                    cutsceneArg.alObjectId.Add(Convert.ToInt32(saSplit[i]));
                                }
                            }
                        }
                    }

                    if (cutsceneArg.strType == "rage__cutfObjectIdNameEventArgs" ||
                        cutsceneArg.strType == "rage__cutfObjectIdEventArgs" ||
                        cutsceneArg.strType == "rage__cutfObjectVariationEventArgs")
                    {
                        foreach (XmlNode child in item)
                        {
                            if (child.Name == "iObjectId")
                            {
                                if (child.Attributes.Count > 0)
                                {
                                    cutsceneArg.alObjectId.Add(Convert.ToInt32(child.Attributes[0].Value));
                                }
                            }
                        }
                    }

                    if (cutsceneArg.strType == "rage__cutfNameEventArgs")
                    {
                        foreach (XmlNode child in item)
                        {
                            if (child.Name == "cName")
                            {
                                cutsceneArg.strName = child.InnerText;
                            }
                        }
                    }

                    if (cutsceneArg.strType == "rage__cutfCameraCutEventArgs")
                    {
                        foreach (XmlNode child in item)
                        {
                            if (child.Name == "cName")
                            {
                                cutsceneArg.strName = child.InnerText;
                            }

                            if (child.Name == "fNearDrawDistance")
                            {
                                if (child.Attributes.Count > 0)
                                {
                                    cutsceneArg.fNearDrawDistance = (float)(Convert.ToDouble(child.Attributes[0].Value));
                                }
                            }

                            if (child.Name == "fFarDrawDistance")
                            {
                                if (child.Attributes.Count > 0)
                                {
                                    cutsceneArg.fFarDrawDistance = (float)(Convert.ToDouble(child.Attributes[0].Value));
                                }
                            }
                        }
                    }

                    if (cutsceneArg.strType == "rage__cutfSubtitleEventArgs")
                    {
                        foreach (XmlNode child in item)
                        {
                            if (child.Name == "fSubtitleDuration")
                            {
                                if (child.Attributes.Count > 0)
                                {
                                    cutsceneArg.fTotalDuration = (float)Convert.ToDouble(child.Attributes[0].Value);
                                }
                            }
                        }
                    }

                    if (cutsceneArg.strType == "rage__cutfPlayParticleEffectEventArgs" ||
                        cutsceneArg.strType == "rage__cutfTriggerLightEffectEventArgs")
                    {
                        foreach (XmlNode child in item)
                        {
                            if (child.Name == "iAttachParentId")
                            {
                                if (child.Attributes.Count > 0)
                                {
                                    cutsceneArg.iAttachedObject = (int)Convert.ToInt32(child.Attributes[0].Value);
                                }
                            }

                            if (child.Name == "iAttachBoneHash")
                            {
                                if (child.Attributes.Count > 0)
                                {
                                    cutsceneArg.iAttachedBoneHash = (int)Convert.ToInt32(child.Attributes[0].Value);
                                }
                            }
                        }
                    }

                    alEventArgs.Add(cutsceneArg);
                }
            }
        }

        private void ReadEvents(string strCutXMLFile, string strList, ArrayList alContent)
        {
            var xmlDoc = new XmlDocument();
            xmlDoc.Load(strCutXMLFile);

            XmlNodeList items = xmlDoc.SelectNodes("/rage__cutfCutsceneFile2/" + strList + "/Item");

            foreach (XmlNode item in items)
            {
                CutEvent cutsceneLoadEvent = new CutEvent();

                if (item.Attributes.Count > 0)
                {
                    cutsceneLoadEvent.strType = item.Attributes[0].Value;

                    foreach (XmlNode child in item)
                    {
                        if (child.Name == "fTime")
                        {
                            if (child.Attributes.Count > 0)
                            {
                                cutsceneLoadEvent.fTime = child.Attributes[0].Value;
                            }
                        }

                        if (child.Name == "iEventId")
                        {
                            if (child.Attributes.Count > 0)
                            {
                                cutsceneLoadEvent.iEventID = Convert.ToInt32(child.Attributes[0].Value);
                            }
                        }

                        if (child.Name == "iEventArgsIndex")
                        {
                            if (child.Attributes.Count > 0)
                            {
                                cutsceneLoadEvent.iEventArgRef = Convert.ToInt32(child.Attributes[0].Value);
                            }
                        }

                        if (child.Name == "iObjectId")
                        {
                            if (child.Attributes.Count > 0)
                            {
                                cutsceneLoadEvent.iObjectID = Convert.ToInt32(child.Attributes[0].Value);
                            }
                        }
                    }

                    alContent.Add(cutsceneLoadEvent);
                }
            }
        }

        //private void ReadEvents(string strCutXMLFile)
        //{
        //    using (XmlTextReader textReader = new XmlTextReader(strCutXMLFile))
        //    {
        //        textReader.ReadToFollowing("pCutsceneEventList");

        //        XmlReader objectReader = textReader.ReadSubtree();

        //        while (objectReader.ReadToFollowing("Item"))
        //        {
        //            CutEvent cutsceneLoadEvent = new CutEvent();

        //            if (objectReader.HasAttributes)
        //            {
        //                cutsceneLoadEvent.strType = objectReader.GetAttribute(0);

        //                XmlReader timeReader = objectReader.ReadSubtree();
        //                XmlReader eventID = objectReader.ReadSubtree();
        //                XmlReader eventArgRef = objectReader.ReadSubtree();
        //                XmlReader objectID = objectReader.ReadSubtree();

        //                if (timeReader.ReadToFollowing("fTime"))
        //                {
        //                    if (timeReader.HasAttributes)
        //                    {
        //                        cutsceneLoadEvent.fTime = timeReader.GetAttribute(0);
        //                    }
        //                }

        //                if (eventID.ReadToFollowing("iEventId"))
        //                {
        //                    if (eventID.HasAttributes)
        //                    {
        //                        cutsceneLoadEvent.iEventID = Convert.ToInt32(eventID.GetAttribute(0));
        //                    }
        //                }

        //                if (eventArgRef.ReadToFollowing("iEventArgsIndex"))
        //                {
        //                    if (eventArgRef.HasAttributes)
        //                    {
        //                        cutsceneLoadEvent.iEventArgRef = Convert.ToInt32(eventArgRef.GetAttribute(0));
        //                    }
        //                }

        //                if (objectID.ReadToFollowing("iObjectId"))
        //                {
        //                    if (objectID.HasAttributes)
        //                    {
        //                        cutsceneLoadEvent.iObjectID = Convert.ToInt32(objectID.GetAttribute(0));
        //                    }
        //                }

        //                alEvents.Add(cutsceneLoadEvent);
        //            }
        //        }

        //        textReader.Close();
        //    }
        //}

        private string GetEventName(int iEventID)
        {
            if (iEventID > eventList.Length)
            {
                return "CUTSCENE_EVENT_FIRST_RESERVED_FOR_PROJECT_USE";
            }

            return eventList[iEventID];
        }

        private bool IsFlagSet(int flag)
        {
            uint uiFlagCopy = uiFlags;

            unsafe
            {
                uint* p = &uiFlagCopy;
                return (p[flag >> 5] & (1 << (flag & 31))) != 0;
            }
        }

        private void PrintObjectView()
        {
            treeView1.Nodes.Clear();

            for (int i = 0; i < alObjects.Count; ++i)
            {
                CutObject cutObject = (CutObject)alObjects[i];

                TreeNode n = new TreeNode();
                n.Name = cutObject.strName;
                n.Text += cutObject.strType + "/" + ((cutObject.strName.Equals("")) ? "UNDEFINED" : cutObject.strName) + "/" + cutObject.iObjectId.ToString();
                if (cutObject.strStreamingName != String.Empty) n.Text += "/" + cutObject.strStreamingName;
                if (cutObject.iStreamingHash != 0) n.Text += "/" + cutObject.iStreamingHash.ToString();
                treeView1.Nodes.Add(n);

                for (int j = 0; j < alEvents.Count; ++j)
                {
                    CutEvent cutEvent = (CutEvent)alEvents[j];

                    if (cutEvent.iEventArgRef == -1)
                    {
                        if (cutEvent.iObjectID == cutObject.iObjectId)
                        {
                            if (!ShowEvent(GetEventName(cutEvent.iEventID))) continue;

                            double frames = Convert.ToDouble(cutEvent.fTime) * 30.0;
                            int frames_int = (int)Math.Ceiling(frames);
                            string s = "[EVENT] " + cutEvent.strType + " @ " + cutEvent.fTime + "s/" + frames_int + " - " + GetEventName(cutEvent.iEventID) + "/" + cutEvent.iEventID;

                            TreeNode n2 = new TreeNode();
                            n2.Text = s;
                            n.Nodes.Add(n2);
                        }

                        continue;
                    }

                    CutEventArg cutEventArg = (CutEventArg)alEventArgs[cutEvent.iEventArgRef];

                    if (cutEventArg.strType == "rage__cutfCameraCutEventArgs" || cutEventArg.strType == "rage__cutfPlayParticleEffectEventArgs" || cutObject.strType == "rage__cutfAudioObject" || cutObject.strType == "rage__cutfCameraObject" || cutEventArg.strType == "rage__cutfTriggerLightEffectEventArgs" || cutEventArg.strType == "rage__cutfSubtitleEventArgs")
                    {
                        if (cutEvent.iObjectID == cutObject.iObjectId)
                        {
                            if (!ShowEvent(GetEventName(cutEvent.iEventID))) continue;

                            double frames = Convert.ToDouble(cutEvent.fTime) * 30.0;
                            int frames_int = (int)Math.Ceiling(frames);
                            string s = "[EVENT] " + cutEvent.strType + " @ " + cutEvent.fTime + "s/" + frames_int + " - " + GetEventName(cutEvent.iEventID) + "/" + cutEvent.iEventID;

                            if (bListArgs)
                            {
                                s += " - " + cutEventArg.strType;
                            }

                            TreeNode n2 = new TreeNode();
                            n2.Text = s;
                            n.Nodes.Add(n2);
                        }
                    }

                    for (int k = 0; k < cutEventArg.alObjectId.Count; ++k)
                    {
                        int iObjectId = (int)cutEventArg.alObjectId[k];

                        if (iObjectId == cutObject.iObjectId)
                        {
                            if (!ShowEvent(GetEventName(cutEvent.iEventID))) continue;

                            double frames = Convert.ToDouble(cutEvent.fTime) * 30.0;
                            int frames_int = (int)Math.Ceiling(frames);
                            string s = "[EVENT] " + cutEvent.strType + " @ " + cutEvent.fTime + "s/" + frames_int + " - " + GetEventName(cutEvent.iEventID) + "/" + cutEvent.iEventID;

                            if (bListArgs)
                            {
                                s += " - " + cutEventArg.strType;
                            }

                            TreeNode n2 = new TreeNode();
                            n2.Text = s;
                            n.Nodes.Add(n2);
                        }
                    }
                }

                for (int j = 0; j < alLoadEvents.Count; ++j)
                {
                    CutEvent cutEvent = (CutEvent)alLoadEvents[j];

                    if (cutObject.strType == "rage__cutfAudioObject")
                    {
                        if (cutEvent.iObjectID == cutObject.iObjectId)
                        {
                            if (!ShowEvent(GetEventName(cutEvent.iEventID))) continue;

                            double frames = Convert.ToDouble(cutEvent.fTime) * 30.0;
                            int frames_int = (int)Math.Ceiling(frames);
                            string s = "[LOAD EVENT] " + cutEvent.strType + " @ " + cutEvent.fTime + "s/" + frames_int + " - " + GetEventName(cutEvent.iEventID) + "/" + cutEvent.iEventID;

                            TreeNode n2 = new TreeNode();
                            n2.Text = s;
                            n.Nodes.Add(n2);
                        }
                    }

                    CutEventArg cutEventArg = (CutEventArg)alEventArgs[cutEvent.iEventArgRef];

                    for (int k = 0; k < cutEventArg.alObjectId.Count; ++k)
                    {
                        int iObjectId = (int)cutEventArg.alObjectId[k];

                        if (iObjectId == cutObject.iObjectId)
                        {
                            if (!ShowEvent(GetEventName(cutEvent.iEventID))) continue;

                            double frames = Convert.ToDouble(cutEvent.fTime) * 30.0;
                            int frames_int = (int)Math.Ceiling(frames);
                            string s = "[LOAD EVENT] " + cutEvent.strType + " @ " + cutEvent.fTime + "s/" + frames_int + " - " + GetEventName(cutEvent.iEventID) + "/" + cutEvent.iEventID;

                            if (bListArgs)
                            {
                                s += " - " + cutEventArg.strType;
                            }

                            TreeNode n2 = new TreeNode();
                            n2.Text = s;
                            n.Nodes.Add(n2);
                        }
                    }
                }
            }
        }

        private void Print()
        {
            string strFullString = String.Empty;
            double frames = fTotalDuration * 30;
            int frames_int = (int)Math.Ceiling(frames);
            strFullString += "Total Duration = " + fTotalDuration + "/" + frames_int + "\r\n";
            strFullString += "Range = " + iRangeStart + "-" + iRangeEnd + "\r\n\r\n";

            strFullString += "**FLAGS**\r\n\r\n";
            for (int i = 0; i < flagList.Length; ++i)
            {
                if (IsFlagSet(i))
                {
                    strFullString += flagList[i] + "\r\n";
                }
            }

            strFullString += "\r\n";

            strFullString += "**OBJECTS**\r\n\r\n";
            for (int i = 0; i < alObjects.Count; ++i)
            {
                CutObject cutObject = (CutObject)alObjects[i];

                strFullString += cutObject.strType + "/" + ((cutObject.strName.Equals("")) ? "UNDEFINED" : cutObject.strName) + "/" + cutObject.iObjectId.ToString();
                if (cutObject.strStreamingName != String.Empty) strFullString += "/" + cutObject.strStreamingName;
                if (cutObject.iStreamingHash != 0) strFullString += "/" + cutObject.iStreamingHash.ToString();

                strFullString += "\r\n";
            }

            strFullString += "\r\n**LOAD EVENTS**\r\n\r\n";
           
            for (int i = 0; i < alLoadEvents.Count; ++i)
            {
                CutEvent cutLoadEvent = (CutEvent)alLoadEvents[i];

                if (!ShowEvent(GetEventName(cutLoadEvent.iEventID))) continue;

                frames = Convert.ToDouble(cutLoadEvent.fTime) * 30.0;
                frames_int = (int)Math.Ceiling(frames);
                strFullString += cutLoadEvent.strType + " @ " + cutLoadEvent.fTime + "s/" + frames_int + " - " + GetEventName(cutLoadEvent.iEventID) + "/" + cutLoadEvent.iEventID;

                for (int k = 0; k < alObjects.Count; ++k)
                {
                    CutObject cutObject = (CutObject)alObjects[k];

                    if (cutObject.iObjectId == cutLoadEvent.iObjectID)
                    {
                        strFullString += " - " + cutObject.strName;
                        break;
                    }
                }

                CutEventArg cutEventArg = (CutEventArg)alEventArgs[cutLoadEvent.iEventArgRef];

                if (bListArgs)
                {
                    strFullString += " - " + cutEventArg.strType;
                }

                if (cutEventArg.alObjectId.Count == 0)
                {
                    if (cutEventArg.strName != String.Empty)
                    {
                        strFullString += "\r\n" + cutEventArg.strName;
                    }
                }
                else
                {
                    for (int j = 0; j < cutEventArg.alObjectId.Count; ++j)
                    {
                        int iObjectId = (int)cutEventArg.alObjectId[j];

                        for (int k = 0; k < alObjects.Count; ++k)
                        {
                            CutObject cutObject = (CutObject)alObjects[k];

                            if (cutObject.iObjectId == iObjectId)
                            {
                                strFullString += "\r\n" + cutObject.strName;
                                break;
                            }
                        }
                    }
                }

                strFullString += "\r\n" + "\r\n";
            }

            strFullString += "**EVENTS**\r\n\r\n";

            for (int i = 0; i < alEvents.Count; ++i)
            {
                CutEvent cutEvent = (CutEvent)alEvents[i];

                if(!ShowEvent(GetEventName(cutEvent.iEventID))) continue;

                frames = Convert.ToDouble(cutEvent.fTime) * 30.0;
                frames_int = (int)Math.Ceiling(frames);
                strFullString += cutEvent.strType + " @ " + cutEvent.fTime + "s/" + frames_int + " - " + GetEventName(cutEvent.iEventID) + "/" + cutEvent.iEventID;

                for (int k = 0; k < alObjects.Count; ++k)
                {
                    CutObject cutObject = (CutObject)alObjects[k];

                    if (cutObject.iObjectId == cutEvent.iObjectID)
                    {
                        strFullString += " - " + cutObject.strName;
                        break;
                    }
                }

                if (cutEvent.iEventArgRef == -1)
                {
                    strFullString += "\r\n\r\n";
                    continue;
                }

                CutEventArg cutEventArg = (CutEventArg)alEventArgs[cutEvent.iEventArgRef];

                if (bListArgs)
                {
                    strFullString += " - " + cutEventArg.strType;
                }

                if (cutEventArg.alObjectId.Count == 0)
                {
                    if (cutEventArg.strName != String.Empty)
                    {
                        strFullString += "\r\n" + cutEventArg.strName;
                    }

                    if (cutEventArg.strType == "rage__cutfSubtitleEventArgs")
                    {
                        strFullString += "\r\n" + cutEventArg.fTotalDuration.ToString();
                    }

                    if (cutEventArg.strType == "rage__cutfPlayParticleEffectEventArgs" ||
                        cutEventArg.strType == "rage__cutfTriggerLightEffectEventArgs")
                    {
                        strFullString += "\r\n";

                        for (int k = 0; k < alObjects.Count; ++k)
                        {
                            CutObject cutObject = (CutObject)alObjects[k];

                            if (cutObject.iObjectId == cutEventArg.iAttachedObject)
                            {
                                strFullString += cutObject.strName + "/";
                            }
                        }

                        strFullString += cutEventArg.iAttachedObject.ToString();
                        strFullString += "\r\n" + cutEventArg.iAttachedBoneHash.ToString();
                    }

                    if (cutEventArg.strType == "rage__cutfCameraCutEventArgs")
                    {
                        strFullString += "\r\n" + cutEventArg.fNearDrawDistance.ToString();
                        strFullString += "\r\n" + cutEventArg.fFarDrawDistance.ToString();
                    }
                }
                else
                {

                    for (int j = 0; j < cutEventArg.alObjectId.Count; ++j)
                    {
                        int iObjectId = (int)cutEventArg.alObjectId[j];

                        for (int k = 0; k < alObjects.Count; ++k)
                        {
                            CutObject cutObject = (CutObject)alObjects[k];

                            if (cutObject.iObjectId == iObjectId)
                            {
                                strFullString += "\r\n" + cutObject.strName;
                                break;
                            }
                        }
                    }
                }

                strFullString += "\r\n" + "\r\n";
            }

            textBox1.Text = strFullString;
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog1.Title = "Open Cut File";

            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Open(openFileDialog1.FileName);
            }
        }

        private void Reset()
        {
            this.Text = "cutfileviewer";
            textBox1.Text = String.Empty;
            fTotalDuration = 0;
            alObjects.Clear();
            alEventArgs.Clear();
            alLoadEvents.Clear();
            alEvents.Clear();
            toolStripStatusLabel1.Text = String.Empty;
        }

        private void Open(string strFilename)
        {
            Reset();

            try
            {
                ReadFlags(strFilename);
                ReadInfo(strFilename);
                ReadObjects(strFilename);
                ReadEventArgs(strFilename);
                ReadEvents(strFilename, "pCutsceneLoadEventList", alLoadEvents);
                ReadEvents(strFilename, "pCutsceneEventList", alEvents);
                Print();
                PrintObjectView();
            }
            catch (Exception e)
            {
                MessageBox.Show(this, e.Message, "Exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Reset();
                return;
            }

            FileInfo fiFile = new FileInfo(strFilename);

            this.Text = fiFile.Name + " - cutfileviewer";
            toolStripStatusLabel1.Text = strFilename;
        }

        private void aboutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Form2 f = new Form2();
            f.ShowDialog();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "") return;

            Clipboard.SetText(textBox1.Text);
        }

        private void textBox1_DragDrop(object sender, DragEventArgs e)
        {
            string[] FileList = (string[])e.Data.GetData(DataFormats.FileDrop, false);

            FileInfo f = new FileInfo(FileList[0]);
            if (f.Extension == ".cutxml" || f.Extension == ".lightxml" || f.Extension == ".cutsub")
            {
                Open(FileList[0]);
            }
        }

        private void textBox1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.Copy;
            else
                e.Effect = DragDropEffects.None; 
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Title = "Save";

            if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                TextWriter tw = new StreamWriter(saveFileDialog1.FileName);
                tw.Write(textBox1.Text.Replace("\n", "\r\n"));
                tw.Close();
            }
        }

        private bool ShowEvent(string strEventID)
        {
            if (filterList != null)
            {
                for (int i = 0; i < filterList.Length; ++i)
                {
                    if (filterList[i] == strEventID)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private void filterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (toolStripStatusLabel1.Text == "") return;

            Form3 frmFilter = new Form3(eventList, filterList);
            if (frmFilter.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                filterList = frmFilter.filterList;
                textBox1.Text = "";
                Print();
                PrintObjectView();
            }
        }

        private void refreshToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (toolStripStatusLabel1.Text != "")
            {
                Open(toolStripStatusLabel1.Text);
            }
        }

        private void treeView1_DragDrop(object sender, DragEventArgs e)
        {
            string[] FileList = (string[])e.Data.GetData(DataFormats.FileDrop, false);

            FileInfo f = new FileInfo(FileList[0]);
            if (f.Extension == ".cutxml" || f.Extension == ".lightxml" || f.Extension == ".cutsub")
            {
                Open(FileList[0]);
            }
        }

        private void treeView1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.Copy;
            else
                e.Effect = DragDropEffects.None;
        }

        private void listEventArgsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bListArgs = !bListArgs;
            refreshToolStripMenuItem_Click(null, null);
        }
    }
}
