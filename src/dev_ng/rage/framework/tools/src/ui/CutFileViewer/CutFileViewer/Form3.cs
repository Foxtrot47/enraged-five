﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CutFileViewer
{
    public partial class Form3 : Form
    {
        private string[] eventList;
        public string[] filterList;

        public Form3(string[] mainEventList, string[] existingFilterList)
        {
            InitializeComponent();

            eventList = mainEventList;

            for (int i = 0; i < eventList.Length; ++i)
            {
                checkedListBox1.Items.Add(eventList[i]);
            }

            for (int i = 0; i < checkedListBox1.Items.Count; ++i)
            {
                checkedListBox1.SetItemChecked(i, true);
            }

            for (int i = 0; i < checkedListBox1.Items.Count; ++i)
            {
                checkedListBox1.SetItemChecked(i, false);

                for(int j=0; j < existingFilterList.Length; ++j)
                {
                    if (checkedListBox1.Items[i].ToString() == existingFilterList[j])
                    {
                        checkedListBox1.SetItemChecked(i, true);
                    }
                }
            }
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            //for (int i = 0; i < eventList.Length; ++i)
            //{
            //    checkedListBox1.Items.Add(eventList[i]);
            //}

            //for (int i = 0; i < checkedListBox1.Items.Count; ++i)
            //{
            //    checkedListBox1.SetItemChecked(i, true);
            //}
        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            filterList = new string[checkedListBox1.CheckedItems.Count];

            for (int i = 0; i < checkedListBox1.CheckedItems.Count; ++i)
            {
                filterList[i] = checkedListBox1.CheckedItems[i].ToString();
            }

            DialogResult = DialogResult.OK;
            Close();
        }

        private void btnSelectAll_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < checkedListBox1.Items.Count; ++i)
            {
                checkedListBox1.SetItemChecked(i, true);
            }
        }

        private void btnDeSelectAll_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < checkedListBox1.Items.Count; ++i)
            {
                checkedListBox1.SetItemChecked(i, false);
            }
        }
    }
}
