﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using RSG.Editor;

namespace HashComputer
{
    /// <summary>
    /// View model which can be used as the data context for the <see cref="MainWindow"/>.
    /// </summary>
    public class MainWindowViewModel : NotifyPropertyChangedBase
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Input"/> property.
        /// </summary>
        private String _input = String.Empty;

        /// <summary>
        /// Private field for the <see cref="FinaliseHash"/> property.
        /// </summary>
        private bool _finaliseHash = true;

        /// <summary>
        /// Private field for the <see cref="HashWidth"/> property.
        /// </summary>
        private int _hashWidth = 32;

        /// <summary>
        /// Private field for the <see cref="InitValue"/> property.
        /// </summary>
        private uint _initValue = 0;

        /// <summary>
        /// Private field for the <see cref="UnsignedHash"/> property.
        /// </summary>
        private uint _unsignedHash;

        /// <summary>
        /// Private field for the <see cref="SignedHash"/> property.
        /// </summary>
        private int _signedHash;

        /// <summary>
        /// Private field for the <see cref="PasteCommand"/> property.
        /// </summary>
        private readonly ICommand _pasteCommand;

        /// <summary>
        /// Private field for the <see cref="CopyCommand"/> property.
        /// </summary>
        private readonly ICommand _copyCommand;
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="MainWindowViewModel"/> class.
        /// </summary>
        public MainWindowViewModel()
        {
            _pasteCommand = new RelayCommand<TextBox>(param => OnPaste(param));
            _copyCommand = new RelayCommand<String>(param => OnCopy(param));
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the users input value.
        /// </summary>
        public String Input
        {
            get { return _input; }
            set
            {
                if (SetProperty(ref _input, value))
                {
                    UpdateHashValues();
                }
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether we want the hash to be finalised.
        /// </summary>
        public bool FinaliseHash
        {
            get { return _finaliseHash; }
            set
            {
                if (SetProperty(ref _finaliseHash, value))
                {
                    UpdateHashValues();
                }
            }
        }

        /// <summary>
        /// Gets or sets the initialisation value for the hash algorithm.
        /// </summary>
        public uint InitValue
        {
            get { return _initValue; }
            set
            {
                if (SetProperty(ref _initValue, value))
                {
                    UpdateHashValues();
                }
            }
        }

        /// <summary>
        /// Gets or sets the number of bits to use in the hash value.
        /// </summary>
        public int HashWidth
        {
            get { return _hashWidth; }
            set
            {
                if (SetProperty(ref _hashWidth, value))
                {
                    UpdateHashValues();
                }
            }
        }

        /// <summary>
        /// Gets the unsigned hash value of the input string.
        /// </summary>
        public uint UnsignedHash
        {
            get { return _unsignedHash; }
            private set { SetProperty(ref _unsignedHash, value); }
        }

        /// <summary>
        /// Gets the signed hash value of the input string.
        /// </summary>
        public int SignedHash
        {
            get { return _signedHash; }
            private set { SetProperty(ref _signedHash, value); }
        }
        #endregion

        #region Commands
        /// <summary>
        /// Command for pasting text from the clipboard into a textbox.
        /// </summary>
        public ICommand PasteCommand
        {
            get { return _pasteCommand; }
        }

        /// <summary>
        /// Command for copying text to the clipboard.
        /// </summary>
        public ICommand CopyCommand
        {
            get { return _copyCommand; }
        }

        /// <summary>
        /// Execute handler for the <see cref="PasteCommand"/>.
        /// </summary>
        /// <param name="textBox"></param>
        private void OnPaste(TextBox textBox)
        {
            if (textBox != null && Clipboard.ContainsText())
            {
                textBox.Text = Clipboard.GetText();
            }
        }

        /// <summary>
        /// Execute handler for the <see cref="CopyCommand"/>.
        /// </summary>
        /// <param name="text"></param>
        private void OnCopy(String text)
        {
            Clipboard.SetText(text);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Updates the hash values based on the input string, the has width and 
        /// whether we wish to finalise the hash or not.
        /// </summary>
        private void UpdateHashValues()
        {
            uint unsignedHash;

            // Use the appropriate hashing method.
            if (_finaliseHash)
            {
                unsignedHash = RSG.ManagedRage.StringHashUtil.atStringHash(_input, _initValue);
            }
            else
            {
                unsignedHash = RSG.ManagedRage.StringHashUtil.atPartialStringHash(_input, _initValue);
            }

            // Check if we need to apply a mask to the resulting value.
            if (_hashWidth < 32)
            {
                long mask = (1 << _hashWidth) - 1;
                unsignedHash &= (uint)mask;
            }

            // Set the updated hash values.
            UnsignedHash = unsignedHash;
            SignedHash = (int)unsignedHash;
        }
        #endregion
    }
}
