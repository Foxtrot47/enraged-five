﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Editor;

namespace HashComputer
{
    /// <summary>
    /// View model for <see cref="ProcessBatchWindow"/>.
    /// </summary>
    public class ProcessFileViewModel : NotifyPropertyChangedBase
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="InputFile"/> property.
        /// </summary>
        private String _inputFile;

        /// <summary>
        /// Private field for the <see cref="OutputFile"/> property.
        /// </summary>
        private String _outputFile;

        /// <summary>
        /// Private field for the <see cref="FinaliseHash"/> property.
        /// </summary>
        private bool _finaliseHash;

        /// <summary>
        /// Private field for the <see cref="HashWidth"/> property.
        /// </summary>
        private int _hashWidth;

        /// <summary>
        /// Private field for the <see cref="InitValue"/> property.
        /// </summary>
        private uint _initValue;
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="ProcessBatchViewModel"/> class
        /// using the specified default hash options.
        /// </summary>
        /// <param name="finaliseHash"></paramparam>
        /// <param name="hashWidth"></param>
        /// <param name="initValue"></param>
        public ProcessFileViewModel(bool finaliseHash, int hashWidth, uint initValue)
        {
            _finaliseHash = finaliseHash;
            _hashWidth = hashWidth;
            _initValue = initValue;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the input file containing the list of string values to hash.
        /// </summary>
        public String InputFile
        {
            get { return _inputFile; }
            set { SetProperty(ref _inputFile, value); }
        }

        /// <summary>
        /// Gets or setst the output file to which the resulting hash values will be output.
        /// </summary>
        public String OutputFile
        {
            get { return _outputFile; }
            set { SetProperty(ref _outputFile, value); }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether we want the hash to be finalised.
        /// </summary>
        public bool FinaliseHash
        {
            get { return _finaliseHash; }
            set { SetProperty(ref _finaliseHash, value); }
        }

        /// <summary>
        /// Gets or sets the initialisation value for the hash algorithm.
        /// </summary>
        public uint InitValue
        {
            get { return _initValue; }
            set { SetProperty(ref _initValue, value); }
        }

        /// <summary>
        /// Gets or sets the number of bits to use in the hash value.
        /// </summary>
        public int HashWidth
        {
            get { return _hashWidth; }
            set { SetProperty(ref _hashWidth, value); }
        }
        #endregion
    }
}
