﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Editor;

namespace HashComputer.Commands
{
    /// <summary>
    /// Action for browsing for the input file for the batch hash computer.
    /// </summary>
    public class BrowseForInputFileAction : ButtonAction<ProcessFileViewModel>
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="BrowseForInputFileAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public BrowseForInputFileAction(ParameterResolverDelegate<ProcessFileViewModel> resolver)
            : base(resolver)
        {
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="vm">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(ProcessFileViewModel vm)
        {
            ICommonDialogService dlgService = this.GetService<ICommonDialogService>();
            if (dlgService == null)
            {
                Debug.Fail("Unable to open file as service is missing.");
                return;
            }

            // Ask the user where he wishes to save the file.
            String filter = "Text File (*.txt)|*.txt|All Files (*.*)|*.*";
            String location;
            if (dlgService.ShowOpenFile(null, null, filter, 0, out location))
            {
                vm.InputFile = location;
            }
        }
        #endregion
    }
}
