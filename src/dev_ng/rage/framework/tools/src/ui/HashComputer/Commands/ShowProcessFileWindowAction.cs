﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Editor;

namespace HashComputer.Commands
{
    /// <summary>
    /// Action for showing the process batch window.
    /// </summary>
    public class ShowProcessFileWindowAction : ButtonAction<MainWindow>
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="ShowProcessFileWindowAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public ShowProcessFileWindowAction(ParameterResolverDelegate<MainWindow> resolver)
            : base(resolver)
        {
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="mainWindow">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(MainWindow mainWindow)
        {
            MainWindowViewModel vm = mainWindow.DataContext as MainWindowViewModel;
            if (vm == null)
            {
                Debug.Fail("Data context isn't valid.");
                throw new ArgumentException("Data context isn't valid.");
            }

            ProcessFile window = new ProcessFile();
            window.DataContext = new ProcessFileViewModel(vm.FinaliseHash, vm.HashWidth, vm.InitValue);
            window.Owner = mainWindow;
            window.ShowDialog();
        }
        #endregion
    }
}
