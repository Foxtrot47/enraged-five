﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using RSG.Editor;

namespace HashComputer.Commands
{
    /// <summary>
    /// Action for computing hashes based on an input file.
    /// </summary>
    public class ProcessFileAction : ButtonAction<ProcessFileViewModel>
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="ProcessFileAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public ProcessFileAction(ParameterResolverDelegate<ProcessFileViewModel> resolver)
            : base(resolver)
        {
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="vm">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(ProcessFileViewModel vm)
        {
            return !String.IsNullOrWhiteSpace(vm.InputFile) &&
                !String.IsNullOrWhiteSpace(vm.OutputFile) &&
                File.Exists(vm.InputFile);
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="vm">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(ProcessFileViewModel vm)
        {
            IMessageBoxService msgService = this.GetService<IMessageBoxService>();
            if (msgService == null)
            {
                Debug.Assert(false, "Unable to show message box as service is missing.");
                return;
            }

            // Extract the data from the input file.
            IList<String> inputs = new List<String>();
            try
            {
                using (StreamReader reader = new StreamReader(vm.InputFile))
                {
                    String line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        inputs.Add(line);
                    }
                }
            }
            catch (System.Exception ex)
            {
                msgService.Show("There was a problem reading the input file.\n\nNo output file will be generated.", "Unable to process input file.",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return;
            }

            try
            {
                using (TextWriter writer = new StreamWriter(vm.OutputFile))
                {
                    foreach (String input in inputs)
                    {
                        uint hash = ComputeUnsignedHash(vm, input);
                        writer.WriteLine(String.Format("\"{0}\",{1},{2},0x{2:X}", input, hash, (int)hash));
                    }
                }
            }
            catch (System.Exception ex)
            {
                msgService.Show("There was a problem writing to the output file.  Is it currently open in another application?\n\nNo output file has been generated.", "Unable to write output file.",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return;
            }

            msgService.Show("The output file was successfully created.");
        }

        /// <summary>
        /// Computes the unsigned hash value of the given string.
        /// </summary>
        /// <param name="vm"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        private uint ComputeUnsignedHash(ProcessFileViewModel vm, String input)
        {
            // Use the appropriate hashing method.
            uint unsignedHash;
            if (vm.FinaliseHash)
            {
                unsignedHash = RSG.ManagedRage.StringHashUtil.atStringHash(input, vm.InitValue);
            }
            else
            {
                unsignedHash = RSG.ManagedRage.StringHashUtil.atPartialStringHash(input, vm.InitValue);
            }

            // Check if we need to apply a mask to the resulting value.
            if (vm.HashWidth < 32)
            {
                long mask = (1 << vm.HashWidth) - 1;
                unsignedHash &= (uint)mask;
            }

            return unsignedHash;
        }
        #endregion
    }
}
