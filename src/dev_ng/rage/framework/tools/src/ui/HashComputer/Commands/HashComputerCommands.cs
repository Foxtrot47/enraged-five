﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using RSG.Editor;
using RSG.Editor.SharedCommands;

namespace HashComputer.Commands
{
    /// <summary>
    /// Defines core commands for the hash computer application.
    /// </summary>
    public static class HashComputerCommands
    {
        #region Fields
        /// <summary>
        /// String table resource manager for getting at the command related strings.
        /// </summary>
        private static readonly CommandResourceManager _commandResourceManager =
            new CommandResourceManager(
                "HashComputer.Resources.CommandStringTable",
                typeof(HashComputerCommands).Assembly);

        /// <summary>
        /// The private cache of System.Windows.Input.RoutedCommand objects.
        /// </summary>
        private static readonly RockstarRoutedCommand[] _internalCommands =
            new RockstarRoutedCommand[Enum.GetValues(typeof(CommandId)).Length];
        #endregion

        #region Enumerations
        /// <summary>
        /// Defines all of the command identifiers for the different hash computer commands.
        /// </summary>
        private enum CommandId
        {
            /// <summary>
            /// Used to identifier the <see cref="HashComputerCommands.ShowProcessFileWindow"/>
            /// routed command.
            /// </summary>
            ShowProcessFileWindow,

            /// <summary>
            /// Used to identifier the <see cref="HashComputerCommands.BrowseForInputFile"/>
            /// routed command.
            /// </summary>
            BrowseForInputFile,

            /// <summary>
            /// Used to identifier the <see cref="HashComputerCommands.BrowseForOutputFile"/>
            /// routed command.
            /// </summary>
            BrowseForOutputFile,

            /// <summary>
            /// Used to identifier the <see cref="HashComputerCommands.ProcessFile"/>
            /// routed command.
            /// </summary>
            ProcessFile
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the value that represents the show process file window command.
        /// </summary>
        public static RockstarRoutedCommand ShowProcessFileWindow
        {
            get { return EnsureCommandExists(CommandId.ShowProcessFileWindow); }
        }

        /// <summary>
        /// Gets the value that represents the browse input file command.
        /// </summary>
        public static RockstarRoutedCommand BrowseForInputFile
        {
            get { return EnsureCommandExists(CommandId.BrowseForInputFile); }
        }

        /// <summary>
        /// Gets the value that represents the browse output file command.
        /// </summary>
        public static RockstarRoutedCommand BrowseForOutputFile
        {
            get { return EnsureCommandExists(CommandId.BrowseForOutputFile); }
        }

        /// <summary>
        /// Gets the value that represents the process file command.
        /// </summary>
        public static RockstarRoutedCommand ProcessFile
        {
            get { return EnsureCommandExists(CommandId.ProcessFile); }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Creates a new System.Windows.Input.RoutedCommand object that is associated with the
        /// specified identifier.
        /// </summary>
        /// <param name="id">
        /// The identifier for the command to create.
        /// </param>
        /// <param name="provider">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        /// <returns>
        /// The newly create System.Windows.Input.RoutedCommand object.
        /// </returns>
        private static RockstarRoutedCommand CreateCommand(String id, BitmapSource icon)
        {
            String name = _commandResourceManager.GetString(id, CommandStringCategory.Name);
            String category = _commandResourceManager.GetString(id, CommandStringCategory.Category);
            String description = _commandResourceManager.GetString(id, CommandStringCategory.Description);
            InputGestureCollection gestures = _commandResourceManager.GetGestures(id);

            return new RockstarRoutedCommand(
                name, typeof(HashComputerCommands), gestures, category, description, icon);
        }

        /// <summary>
        /// Makes sure the internal value for the command associated with the specified
        /// identifier exists and returns it.
        /// </summary>
        /// <param name="commandId">
        /// The identifier for the command that should be created and returned.
        /// </param>
        /// <param name="icon">
        /// The icon that the command should be using.
        /// </param>
        /// <returns>
        /// The <see cref="RSG.Editor.RockstarRoutedCommand"/> object associated with the
        /// specified identifier.
        /// </returns>
        private static RockstarRoutedCommand EnsureCommandExists(CommandId commandId, BitmapSource icon = null)
        {
            int commandIndex = (int)commandId;
            if (commandIndex < -1 || commandIndex >= _internalCommands.Length)
            {
                return null;
            }

            RockstarRoutedCommand command = _internalCommands[commandIndex];
            if (command == null)
            {
                lock (_internalCommands)
                {
                    if (command == null)
                    {
                        command = CreateCommand(commandId.ToString(), icon);
                        _internalCommands[commandIndex] = command;
                    }
                }
            }

            return command;
        }
        #endregion
    }
}
