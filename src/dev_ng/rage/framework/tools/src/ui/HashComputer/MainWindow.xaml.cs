﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using HashComputer.Commands;
using RSG.Editor;
using RSG.Editor.Controls;

namespace HashComputer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : RsMainWindow
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            DataContext = new MainWindowViewModel();
            AddCommandInstances();
            AttachCommandBindings();
        }
        #endregion // Constructor(s)

        #region Methods
        /// <summary>
        /// Adds the commands/menus to the command manager.
        /// </summary>
        internal void AddCommandInstances()
        {
            // File menu.
            RockstarCommandManager.AddCommandInstance(
                HashComputerCommands.ShowProcessFileWindow,
                0,
                false,
                "Process File",
                new Guid("8EECD8CD-8028-4C1D-BFB1-07C4F04AA300"),
                RockstarCommandManager.FileMenuId);
        }

        /// <summary>
        /// Attaches all of the necessary command bindings to this instance.
        /// </summary>
        private void AttachCommandBindings()
        {
            // File management commands.
            new ShowProcessFileWindowAction(this.MainWindowResolver)
                .AddBinding(HashComputerCommands.ShowProcessFileWindow, this);
        }

        /// <summary>
        /// Resolves the main window view model from the datacontext.
        /// </summary>
        /// <param name="commandData"></param>
        /// <returns></returns>
        private MainWindowViewModel MainWindowViewModelResolver(CommandData commandData)
        {
            MainWindowViewModel vm = null;
            if (!this.Dispatcher.CheckAccess())
            {
                vm = this.Dispatcher.Invoke<MainWindowViewModel>(() => this.DataContext as MainWindowViewModel);
            }
            else
            {
                vm = this.DataContext as MainWindowViewModel;
            }

            if (vm == null)
            {
                Debug.Fail("Data context isn't valid.");
                throw new ArgumentException("Data context isn't valid.");
            }
            return vm;
        }

        /// <summary>
        /// Resolves the main window.
        /// </summary>
        /// <param name="commandData"></param>
        /// <returns></returns>
        private MainWindow MainWindowResolver(CommandData commandData)
        {
            return this;
        }
        #endregion
    }
}
