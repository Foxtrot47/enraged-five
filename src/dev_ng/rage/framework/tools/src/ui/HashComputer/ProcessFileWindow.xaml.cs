﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using HashComputer.Commands;
using RSG.Editor;
using RSG.Editor.Controls;

namespace HashComputer
{
    /// <summary>
    /// Interaction logic for ProcessFile.xaml
    /// </summary>
    public partial class ProcessFile : RsWindow
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="ProcessFile"/> class.
        /// </summary>
        public ProcessFile()
        {
            InitializeComponent();
            AttachCommandBindings();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Attaches all of the necessary command bindings to this instance.
        /// </summary>
        private void AttachCommandBindings()
        {
            new BrowseForInputFileAction(this.DataContextResolver)
                .AddBinding(HashComputerCommands.BrowseForInputFile, this);
            new BrowseForOutputFileAction(this.DataContextResolver)
                .AddBinding(HashComputerCommands.BrowseForOutputFile, this);
            new ProcessFileAction(this.DataContextResolver)
                .AddBinding(HashComputerCommands.ProcessFile, this);
        }

        /// <summary>
        /// Resolves the process batch view model from the datacontext.
        /// </summary>
        /// <param name="commandData"></param>
        /// <returns></returns>
        private ProcessFileViewModel DataContextResolver(CommandData commandData)
        {
            ProcessFileViewModel vm = null;
            if (!this.Dispatcher.CheckAccess())
            {
                vm = this.Dispatcher.Invoke<ProcessFileViewModel>(() => this.DataContext as ProcessFileViewModel);
            }
            else
            {
                vm = this.DataContext as ProcessFileViewModel;
            }

            if (vm == null)
            {
                Debug.Fail("Data context isn't valid.");
                throw new ArgumentException("Data context isn't valid.");
            }
            return vm;
        }
        #endregion
    }
}
