﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("UnityTool")]
[assembly: AssemblyDescription("A tool that makes a vcproj file a unity of cpp files. Written by Derek Ward")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Rockstar North")]
[assembly: AssemblyProduct("UnityTool")]
[assembly: AssemblyCopyright("Copyright © Rockstar North 2008")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("1b2dfe6d-1a08-445d-8b0f-8ab2f4961223")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
