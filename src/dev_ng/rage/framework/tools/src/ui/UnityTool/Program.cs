//--------------------------------------------------------------------------------------------------------------------
// UnityTool - Derek Ward - (c) Rockstar North  - 2008, Updated Jan 2011.
//
// - Converts a vcproj file to build it's cpp files as less units of compilation by including cpp files within new new cpp files.
// - Less units of compilation requires less header file pre-processing and ultimately makes the total cost of compilation jobs less expensive.
// - Works on a hierarchical basis. ie. Master, Major, Folder ( which is optional - you can tune the no. of units of compilation )
//
// Various issues to be aware of :-
//      - Ressurected tool from a long time ago, still works but a rewrite probably is required given that 
//      - Requirements have changed. It's likely this will form the basis of a tool in the project builder and may not be used thereafter.
//      - Upon doing this you have to deal with collisions of #defines, class definations static global variables etc.
//      - Works to a folder resolution, some unity compilation units may be too big OR too small. ( next version should handle this )
//      - The app can be run as CLI or GUI, the GUI is incomplete but was initially conceived to allow user preferences over files to be considered as part of the unity build.
//      - Individual files at a root level may or may not be handled by the unity build ( it's written to folder resolution for convenience )
//      - This is expected to be used to prove the viability of a unity build.
//      - Contatins structural assumptions about vcproj format, no schema is used.
//--------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Windows.Forms;

using System.Resources;
using System.Reflection;
using System.Threading;
using System.Globalization;

using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Xsl;
using System.Xml.XPath;
using System.Drawing;

namespace UnityTool
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static int Main(string[] args)
        {
            ProcessArgs(args);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Form1 form = new Form1();

            if (ms_bGui)
            {
                LoadImages(form);
            }
            
            form.textBox1.AppendText(ms_InputFileName);

            ms_unityTool = new UnityTool(form.treeView1, form);
            ms_unityTool.Msg("{0} v{1}",ms_appName, ms_version);

            if (ms_majorFiles > 0)
            {
                ms_unityTool.SetNumMajorFiles(ms_majorFiles);
                ms_unityTool.Msg("{0} major files will be used", ms_majorFiles);
            }

            ms_unityTool.SetConfiguration(ms_buildConfig + "|" + ms_buildPlatform);

            form.toolStripButton1.Click += new EventHandler(toolStripButton1_ButtonClick);
            form.toolStripButton2.Click += new EventHandler(toolStripButton2_ButtonClick);

            if (!ms_bGui)
            {
                ms_unityTool.Process(ms_InputFileName, ms_OutputFileName, ms_bPreununity, ms_bUnunity, ms_bUnity, ms_bBackup);
                return 0;
            }

            Application.Run(form);
            return 0;
        }

        //--------------------------------------------------------------------------------------------------------------------
        private static void LoadImages(Form1 form)
        {
            form.treeView1.CheckBoxes = true;
            form.treeView1.ImageList = new ImageList();
            form.treeView1.ImageList.Images.Add(Image.FromFile(ms_imageFolderClosed));
            form.treeView1.ImageList.Images.Add(Image.FromFile(ms_imageFolderOpen));
            form.treeView1.ImageList.Images.Add(Image.FromFile(ms_imageCppFile));
            form.treeView1.ImageList.Images.Add(Image.FromFile(ms_imageVcproj));
        }

        //-------------------------------------------------------------------
        static public void ProcessArgs(String[] args)
        {
            bool bFoundFile = false;
            bool bFoundOutputFile = false;

            for (int i = 0; i < args.Length; i++)
            {
                if (args[i] == ms_helpString1 || args[i] == ms_helpString2|| args[i] == ms_helpString3)
                {
                    DisplayHelp();
                    Environment.Exit(0);
                }
                else if (args[i] == ms_gui)
                {
                    ms_bGui = true;
                }
                else if (args[i].Contains(ms_numMajorFiles))
                { // i should do this better really. tsk tsk.
                    i++;
                    ms_majorFiles = int.Parse(args[i]);
                }
                else if (args[i].Contains(ms_config))
                { // i should do this better really. tsk tsk.
                    i++;
                    ms_buildConfig = args[i];                    
                }
                else if (args[i].Contains(ms_platform))
                { // i should do this better really. tsk tsk.
                    i++;
                    ms_buildPlatform = args[i];                    
                }
                else if (args[i].Contains(ms_exclude))
                { // i should do this better really. tsk tsk.
                    i++;
                    UnityTool.ms_exclusions.Add(args[i]);
                }                    
                else if (args[i] == ms_backup)
                {
                    ms_bBackup = true;
                }       
                else if (args[i] == ms_unityConvert)
                {
                    ms_bUnity = true;
                }
                else if (args[i] == ms_unityRevert)
                {
                    ms_bUnunity = true;
                }
                else if (args[i] == ms_unityTest)
                {
                    ms_bPreununity = true;
                }
                else if (args[i] == ms_unityBuildMaster)
                {
                    UnityTool.ms_bBuildFromMaster = true;
                }
                else if (args[i] == ms_unityBuildMinor)
                { 
                    UnityTool.ms_bBuildFromMinor = true;
                }
                else
                {
                    if (bFoundFile && !bFoundOutputFile)
                    {
                        bFoundOutputFile = true;
                        ms_OutputFileName = args[i];
                    }
                    else if (!bFoundFile)
                    {
                        bFoundFile = true;
                        ms_InputFileName = args[i];
                    }
                    else
                    {
                        // ignoring this 
                    }
                }
            }
            if (!bFoundOutputFile)
                ms_OutputFileName = ms_InputFileName;
        }

        //--------------------------------------------------------------------------------------------------------------------
        static private void DisplayHelp()
        {
            Console.WriteLine("Usage");
            Console.WriteLine(String.Format("     {0} inputfilename [outputfilename] -option", ms_appName));
            Console.WriteLine(String.Format("     {0} : To convert to a unity project ", ms_unityConvert));
            Console.WriteLine(String.Format("     {0} : To enable gui ", ms_gui));            
            Console.WriteLine(String.Format("     {0} : To unconvert from a unity project ", ms_unityRevert));
            Console.WriteLine(String.Format("     {0} : To start from a backup of the project prefixed {1} ", ms_unityTest, UnityTool.ms_backupSuffix));
            Console.WriteLine(String.Format("     {0} : To unity build from the single master file", ms_unityBuildMaster));
            Console.WriteLine(String.Format("     {0} : To unity build from minor unity files", ms_unityBuildMinor));
            Console.WriteLine(String.Format("     {0} : To specify the number of major files to use.", ms_numMajorFiles));
            Console.WriteLine(String.Format("     {0} : To backup the vcproj before conversion.", ms_backup));
            Console.WriteLine(String.Format("     {0} : To specify the config.", ms_config));
            Console.WriteLine(String.Format("     {0} : To specify the platform.", ms_platform));
            Console.WriteLine(String.Format("     {0} : To exclude a filename.", ms_exclude));                          
            Console.WriteLine(String.Format("     {0} {1} {2} : To Display this help ", ms_helpString1, ms_helpString2, ms_helpString3));            
        }

        static private void toolStripButton1_ButtonClick(Object sender, EventArgs args)
        {
            ms_unityTool.Process(ms_InputFileName, ms_OutputFileName, true, false, true, true);
        }

        static private void toolStripButton2_ButtonClick(Object sender, EventArgs args)
        {
            ms_unityTool.Process(ms_InputFileName, ms_OutputFileName, true, true, false, false);
        }

        //--------------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------------
        static public string ms_appName             = @"UnityTool";
        static public string ms_version             = @"2.0";
        static public string ms_InputFileName       = @"";
        static public string ms_OutputFileName      = @"";
        static public bool ms_bPreununity           = false;
        static public bool ms_bUnunity              = false;
        static public bool ms_bUnity                = false;
        static public string ms_unityConvert        = "-unity";
        static public string ms_unityRevert         = "-ununity";
        static public string ms_unityTest           = "-preununity";
        static public string ms_backup              = "-backup";
        static public string ms_unityBuildMinor     = "-minor";
        static public string ms_unityBuildMaster    = "-master";
        static public string ms_config              = "-buildconfig";
        static public string ms_platform            = "-platform";
        static public string ms_gui                 = "-gui";
        static public string ms_exclude             = "-exclude";
        static public string ms_helpString1         = "-help";        
        static public string ms_helpString2         = "-?";
        static public string ms_helpString3         = "-h";
        static public string ms_numMajorFiles       = "-nummajor";
        static public string ms_imageFolderClosed   = @".\resources\folderClosed.gif"; // ack should really be compiled resources ideally!
        static public string ms_imageFolderOpen     = @".\resources\folderOpen.gif";
        static public string ms_imageCppFile        = @".\resources\cppFile.gif";
        static public string ms_imageVcproj         = @".\resources\vcproj.gif";
        static public bool ms_bGui                  = false;
        static public bool ms_bBackup               = false;
        static public string ms_buildConfig         = "Release";
        static public string ms_buildPlatform       = "Xbox 360";
        static public int ms_majorFiles             = -1;
        static public UnityTool ms_unityTool;
    }

    //--------------------------------------------------------------------------------------------------------------------
    //--- UNITY TOOL CLASS                                                                                             ---
    //--------------------------------------------------------------------------------------------------------------------

    class UnityTool
    {
        public UnityTool() { }
        public UnityTool(TreeView treeView, Form1 form) { m_treeView = treeView; m_form = form; }
        ~UnityTool() { }

        //-------------------------------------------------------------------
        public void Msg(string format, params object[] arg)
        {
            string str = String.Format(format, arg);
            m_form.listBox1.Items.Add(str);
            Console.WriteLine(str);
        }

        //-------------------------------------------------------------------
        private void WriteStandardErrorMsg(String file, int lineNum, int linePos, String message)
        {
            string str = String.Format("{0}({1},{2}): error: {3}", file, lineNum, linePos, message);
            m_form.listBox1.Items.Add(str);
            Console.WriteLine(str);
        }
        //-------------------------------------------------------------------
        private void WriteStandardErrorMsg(String file, XmlException e)
        {
            string detail = e.InnerException != null ? " " + e.InnerException.Message : "";
            WriteStandardErrorMsg(file, e.LineNumber, e.LinePosition, e.Message + detail);
        }
        //-------------------------------------------------------------------
        private void WriteStandardErrorMsg(String file, XmlSchemaException e)
        {
            string detail = e.InnerException != null ? " " + e.InnerException.Message : "";
            WriteStandardErrorMsg(file, e.LineNumber, e.LinePosition, e.Message + detail);
        }
        //-------------------------------------------------------------------
        private void WriteStandardErrorMsg(String file, XsltException e)
        {
            string detail = e.InnerException != null ? " " + e.InnerException.Message : "";
            WriteStandardErrorMsg(file, e.LineNumber, e.LinePosition, e.Message + detail);
        }

        //-----------------------------------------------------------------------------------------------------------
        public void Process(string inputFileName, string outputFileName, bool bPreununity, bool bUnunity, bool bUnity, bool bBackup)
        {
            XmlDocument doc = XmlLoad(inputFileName);
            
            if (bPreununity)
                UnUnityise(doc, inputFileName);

            if (bUnity)
                Unityise(doc, inputFileName, bBackup);

            if (bUnunity)
                UnUnityise(doc, inputFileName);
        }

        //-------------------------------------------------------------------
        private XmlDocument XmlLoad(string inputFileName)
        {
            Msg("XmlLoad {0}", inputFileName);
            XmlDocument doc = new XmlDocument();

            try
            {
                XmlReaderSettings readerSettings = new XmlReaderSettings();
                readerSettings.IgnoreProcessingInstructions = true;
                XmlReader reader = XmlReader.Create(inputFileName, readerSettings);
                doc.Load(reader);
                reader.Close();
            }
            catch (XmlException e)
            {
                WriteStandardErrorMsg(inputFileName, e);
                Environment.Exit(-1);
            }
            catch (Exception e)
            {
                WriteStandardErrorMsg(inputFileName, 0, 0, e.Message);
                Environment.Exit(-1);
            }
            return doc;
        }

        //-------------------------------------------------------------------
        public bool FilePassesFilter(string filename)
        {
            bool passes = false;

            if (filename.Contains(ms_fileFilter))
                passes = true;

            foreach (string s in ms_exclusions)
            {
              if (filename.Contains(s))
              {
                  passes = false;
              }
            }

            return passes;
        }

        //-------------------------------------------------------------------
        public void ProcessFilter(XPathNavigator nav, string filterName, FileStream fs, StreamWriter sw, bool bRecurse, TreeNode node)
        {           
            if (nav.MoveToFirstChild())
            {
                node = node.Nodes.Add(filterName);
                node.Checked = true;
                // files and filters now expected... put all into a unity cpp file
                do
                {
                    if (nav.HasAttributes)
                    {
                        bool bGotFilename = nav.Name == "File" && nav.MoveToFirstAttribute() && nav.Name == "RelativePath";

                        if (!bGotFilename && nav.Name == "Filter" && bRecurse)
                        {
                            nav.MoveToFirstAttribute();
                            filterName = nav.Value;
                            nav.MoveToParent();
                            ProcessFilter(nav, filterName, fs, sw, bRecurse, node);
                        }
                        else if (bGotFilename)
                        {
                            string filename = nav.Value;
                            nav.MoveToParent();

                            if (FilePassesFilter(filename))
                            {
                                TreeNode leafNode = node.Nodes.Add(GetFileOfFile(filename));
                                leafNode.ImageIndex = 2;
                                // Write the .cpp filename to the .cpp file for #including
                                string str = String.Format("#include \"..\\{0}\"", filename);
                                sw.WriteLine(str);
                                sw.Flush();

                                Msg("     Unityising... {0}", filename);

                                if (!HasFileConfig(nav))
                                {
                                    Msg(" No file Config... excluding from build {0} ", filename);
                                    ExcludeFileFromBuild(nav);
                                    leafNode.Checked = true;
                                }
                                else
                                {
                                    Msg(" Has file Config... checking file settings {0}", filename);
                                    nav.MoveToFirstChild();
                                    nav.MoveToFirstAttribute();

                                    do
                                    {
                                        bool bGotoParent = true;
                                        if (nav.Value == ms_configuration)
                                        {
                                            bool bFoundExclusion = false;                                            
                                            while (nav.MoveToNextAttribute())
                                            {
                                                if (nav.Name == "ExcludedFromBuild")
                                                {                                                   
                                                   bFoundExclusion = true;
                                                   Msg("{0} is already excluded from build", filename);
                                                }
                                            }

                                            XPathNavigator nav2 = nav.Clone();
                                            nav2.MoveToParent();

                                            // check if it has a custom build step
                                            bool customBuildStep = false;
                                            if (nav2.HasChildren)
                                            {
                                                if (nav2.MoveToFirstChild())
                                                {
                                                    if (nav2.Name == "Tool")
                                                    {
                                                        customBuildStep = true;
                                                        Msg("{0} has a custom build step.", filename);
                                                        nav.MoveToFirstChild();
                                                        nav.DeleteSelf();
                                                        nav.MoveToParent();
                                                    }
                                                }
                                            }                                            

                                            if (!bFoundExclusion && !customBuildStep)
                                            {
                                                Msg("{0} is now excluded from build", filename);
                                                nav.MoveToParent();
                                                nav.CreateAttribute(null, "ExcludedFromBuild", nav.NamespaceURI, "true");
                                                leafNode.Checked = true;
                                                bGotoParent = false;
                                            }
                                        }
                                        if(bGotoParent)
                                            nav.MoveToParent();
                                    } while (nav.MoveToNext() && nav.MoveToFirstAttribute());                                    
                                    nav.MoveToParent();
                                }
                            }
                        }
                    }
                } while (nav.MoveToNext()); // next file
                nav.MoveToParent();       // back to the parent                
            }
        }

       //-------------------------------------------------------------------
        private bool HasFileConfig(XPathNavigator nav)
        {
            bool bHasFileConfig = false;
            if (nav.MoveToFirstChild())
            {
                do
                {
                    Msg(nav.Name);
                    if (nav.Name == "FileConfiguration")
                    {
                        bHasFileConfig = true;
                    }
                }
                while (nav.MoveToNext() && !bHasFileConfig);
                nav.MoveToParent();
            }
            return bHasFileConfig;
        }

        //-------------------------------------------------------------------
        private void ExcludeFileFromBuild(XPathNavigator nav)
        {
            // ADD a file configuration for this file
            string str = String.Format("{0}{1}{2}", ms_excludeStringPrefix, ms_configuration, ms_excludeStringSuffix);
            Msg("Exclude file from build : {0}",str);
            nav.AppendChild(str);
        }

        //-------------------------------------------------------------------
        public void ProcessRootUnityFilter(XPathNavigator nav, XPathNavigator navUnity, string filterName, bool bRecurse, string filename, TreeNode node)
        {
            Msg("ProcessRootUnityFilter {0} {1}",filterName, filename);
            string folder = String.Format(".\\{0}\\",ms_unityFolder);
            string bareFileName = filterName + ms_unityMinorFileSuffix;
            string unityCppFile = folder + bareFileName;

            // Open the file for #INCLUDES into a .cpp file.
            DirectoryInfo di = Directory.CreateDirectory(folder);
            FileStream fs = new FileStream(unityCppFile, FileMode.Create);
            StreamWriter sw = new StreamWriter(fs);

            sw.WriteLine(ms_minorFileHeader);
            sw.Flush();

            ProcessFilter(nav, filterName, fs, sw, bRecurse, node); // recursive function

            sw.WriteLine(ms_minorFileFooter);
            sw.Flush();

            // Add a new file into the DOM
            {
                string relPathToFile = String.Format("<File RelativePath=\"{0}\">", unityCppFile);

                Msg("...WRITING {0}", relPathToFile);

                if (!ms_bBuildFromMinor)
                    relPathToFile += ms_excludeStringPrefix + ms_configuration + ms_excludeStringSuffix;
                relPathToFile += @"</File>";
                try
                {
                    navUnity.InsertBefore(relPathToFile);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            // distribute unity load by scattering sequentially through unity minor files creates..
            string dir              = GetDirectoryOfFile(ref filename);
            Directory.SetCurrentDirectory(dir);
            int fileId              = (m_filterCount % m_numUnityMajorFiles) + 1;

            string file = Path.GetFileName(filename).Replace(".vcproj", "");          
            string majorFile        = String.Format("{0}\\{1}{2}.cpp", ms_unityFolder,ms_majorFilename, String.Format("_{0}_{1}", file, fileId ));

            FileStream fsMajor      = new FileStream(majorFile, FileMode.Append);
            StreamWriter swMajor    = new StreamWriter(fsMajor);

            swMajor.WriteLine(String.Format("#include \".\\{0}\"",bareFileName));

            swMajor.Flush();
            swMajor.Close();
            fsMajor.Close();

            // Close unity file for #includes
            sw.Close();
            fs.Close();
        }

        //-------------------------------------------------------------------
        public string GetFilterName(XPathNavigator nav)
        {
            // Get the filter name
            if (nav.HasAttributes && nav.MoveToFirstAttribute())
            {
                string filterName = nav.Value;
                nav.MoveToParent();
                return filterName;
            }
            return "";
        }

        //-------------------------------------------------------------------
        public int UnityProcess(XPathNavigator nav, XPathNavigator navUnity, string filename, TreeNode node)
        {
            Msg("UnityProcess {0}",filename);           

            if (nav.LocalName == "Filter")
            {
                string filterName = GetFilterName(nav);

                // we have a filter that has children now process it as a root unity folder
                if (nav.HasChildren)
                {
                    if (filterName != ms_unityFolder)
                    {
                        Msg("Processing Filter {0}", filterName);
                        ProcessRootUnityFilter(nav, navUnity, filterName, true, filename, node);
                    }
                    else
                    {
                       // nav.DeleteSelf(); // moves to parent
                       // nav.MoveToFirstChild(); // ack relies on __unity being the first child - improve later
                    }
                }
            }
            else
            {
                // process a file!!!
                 Msg("Processing File?");
            }
            return 1;
        }

        //-------------------------------------------------------------------
        // Note to self : use System.IO.Path!
        private string GetDirectoryOfFile(ref string filename)
        {
            int slashIndex = filename.LastIndexOfAny("\\/".ToCharArray());
            string dirName = Environment.CurrentDirectory;
            string FN = filename;
            if (slashIndex >= 0)
            {
                dirName = filename.Substring(0, slashIndex);
                FN = filename.Substring(slashIndex + 1);
            }
            return dirName;
        }

        //-------------------------------------------------------------------
        // Note to self : use System.IO.Path!
        private string GetFileOfFile(string filename)
        {
            int slashIndex = filename.LastIndexOfAny("\\/".ToCharArray());
            string dirName = Environment.CurrentDirectory;
            string FN = filename;
            if (slashIndex >= 0)
            {
                dirName = filename.Substring(0, slashIndex);
                FN = filename.Substring(slashIndex + 1);
            }
            return FN;
        }

        //-------------------------------------------------------------------
        public void Unityise(XmlDocument doc, string filename, bool backup)
        {
            Msg("UnityTool Unityising...");
            {
                string dir = GotoProjectDirectory(ref filename);

                if (backup)
                {
                    Backup(doc, filename);
                }

                XPathNodeIterator iterator;
                XPathNavigator navUnity;
                NavigateToFirstChild(doc, out iterator, out navUnity);
               
                CreateUnityFolder(navUnity);
                
                CreateMasterFile(filename, dir, navUnity, false);               
                CreateMajorFiles(filename, dir, navUnity, false);
               
                Process(filename, iterator, navUnity);
                WriteFooters(filename, dir, navUnity);

                AddComment(doc);

                Save(doc, filename);
            }
            Msg("UnityTool Unityising Complete");
        }

        private void AddComment(XmlDocument doc)
        {
            XmlComment newComment;
            newComment = doc.CreateComment(ms_UnityiseComment + ms_configuration);
            
            //Add the new node to the document.
            XmlElement root = doc.DocumentElement;
            doc.InsertBefore(newComment, root);
        }

        //--------------------------------------------------------------------------------------------------------------------
        private void WriteFooters(string filename, string dir, XPathNavigator navUnity)
        {
            Msg("WriteFooters");
            CreateMajorFiles(filename, dir, navUnity, true); // only writes footer
            CreateMasterFile(filename, dir, navUnity, true); // only writes footer
        }

        //--------------------------------------------------------------------------------------------------------------------
        private void Process(string filename, XPathNodeIterator iterator, XPathNavigator navUnity)
        {
            Msg("Process {0}", filename);
            XPathNavigator nav2 = iterator.Current.Clone();

            TreeNode node = m_treeView.Nodes.Add(filename);
            node.Checked = true;
            node.ImageIndex = 0;
            node.Expand();

            if (nav2.HasChildren)
            {
                nav2.MoveToFirstChild();
                nav2.MoveToNext(); // got to skip over the unity filter I added
                do
                {
                    m_filterCount += UnityProcess(nav2, navUnity, filename, node);
                } while (nav2.MoveToNext());
                nav2.MoveToParent();

                // not working yet.
                //    ProcessRootUnityFilter(nav2, "base", false); // process files at base level
            }
        }

        //--------------------------------------------------------------------------------------------------------------------
        private string GotoProjectDirectory(ref string filename)
        {
            string dir = GetDirectoryOfFile(ref filename);
            Directory.SetCurrentDirectory(dir);
            return dir;
        }

        //--------------------------------------------------------------------------------------------------------------------
        private static void Backup(XmlDocument doc, string filename)
        {
            doc.Save(filename + ms_backupSuffix);
        }

        //--------------------------------------------------------------------------------------------------------------------
        private static void NavigateToFirstChild(XmlDocument doc, out XPathNodeIterator iterator, out XPathNavigator navUnity)
        {
            XPathNavigator nav = doc.CreateNavigator();
            nav.MoveToRoot();

            // Compile a standard XPath expression
            XPathExpression expr;
            expr = nav.Compile(ms_filesExpression);
            iterator = nav.Select(expr);
            iterator.MoveNext();

            navUnity = iterator.Current.Clone();

            navUnity.MoveToFirstChild();
        }

        //--------------------------------------------------------------------------------------------------------------------
        private void Save(XmlDocument doc, string filename)
        {
            Msg("Save");
            Msg("  Unity folders Processed {0}", m_filterCount);
            doc.Save(filename);
            doc.Save(ms_developmentXMLOutput);
        }

        //--------------------------------------------------------------------------------------------------------------------
        private void CreateMajorFiles(string filename, string dir, XPathNavigator navUnity, bool bFooter)
        {
            Msg("CreateMajorFiles");
            for (int i = 1; i <= m_numUnityMajorFiles; i++)
            {
                string majorFile = String.Format("\\{0}\\{1}_{2}",ms_unityFolder,ms_majorFilename,Path.GetFileName(filename).Replace(".vcproj","") + "_" + i + @".cpp");

                FileStream fs;
                StreamWriter sw;
                if (bFooter)
                {
                    fs = new FileStream(dir + majorFile, FileMode.Append);
                    sw = new StreamWriter(fs);
                    sw.WriteLine(ms_majorFileFooter);
                }
                else
                {
                    fs = new FileStream(dir + majorFile, FileMode.Create);
                    sw = new StreamWriter(fs);
                    sw.WriteLine(ms_majorFileHeader);

                    string relPathToFile = ms_fileRelPathPrefix + majorFile + ms_fileRelPathSuffix;

                    navUnity.InsertAfter(relPathToFile);
                    navUnity.MoveToParent();
                    navUnity.MoveToFirstChild();
                    while (navUnity.MoveToNext()) ;
                    if (ms_bBuildFromMaster || ms_bBuildFromMinor)
                        navUnity.AppendChild(ms_excludeStringPrefix + ms_configuration + ms_excludeStringSuffix);
                    else if (ms_bWritePreCompiledHeader)
                        navUnity.PrependChild(ms_precompiledHeaderHeader + ms_configuration + ms_precompiledHeaderFooter);
                }
                sw.Close();
                fs.Close();
            }
        }

        //--------------------------------------------------------------------------------------------------------------------
        private void CreateMasterFile(string filename, string dir, XPathNavigator navUnity, bool bFooter)
        {
            Msg("CreateMasterFile");
            string masterFile = ms_masterFilename;

            string file = Path.GetFileName(filename).Replace(".vcproj", "");
            masterFile = masterFile.Replace(".cpp", String.Format("_{0}.cpp",file) );

            FileStream fs;
            StreamWriter sw;
            if (bFooter)
            {
                fs = new FileStream(dir + masterFile, FileMode.Append);
                sw = new StreamWriter(fs);
                sw.WriteLine(ms_masterFileFooter);
            }
            else
            {
                fs = new FileStream(dir + masterFile, FileMode.Create);
                sw = new StreamWriter(fs);
                sw.WriteLine(ms_masterFileHeader);

                for (int i = 1; i <= m_numUnityMajorFiles; i++)
                {
                    sw.WriteLine(String.Format("#include \".\\{0}_{1}_{2}.cpp\"", ms_majorFilename,file,i));
                }
            }

            sw.Close();
            fs.Close();

            if (!bFooter)
            {
                navUnity.PrependChild(ms_fileRelPathPrefix + masterFile + ms_fileRelPathSuffix);
                navUnity.MoveToFirstChild(); // inside the unity folder.
                
                if (ms_bBuildFromMaster && ms_bWritePreCompiledHeader)
                    navUnity.PrependChild(ms_precompiledHeaderHeader + ms_configuration + ms_precompiledHeaderFooter);
                else if (!ms_bBuildFromMaster || ms_bBuildFromMinor)
                    navUnity.PrependChild(ms_excludeStringPrefix + ms_configuration + ms_excludeStringSuffix);
            }
        }

        //--------------------------------------------------------------------------------------------------------------------
        private static void CreateUnityFolder(XPathNavigator navUnity)
        {
            // physically on disk create the folder
            Directory.CreateDirectory(ms_unityFolder);
            // create it in the xml file

            navUnity.InsertBefore(ms_filterPrefix + ms_unityFolder + ms_filterSuffix);
            navUnity.MoveToParent(); // inside the unity folder.
            navUnity.MoveToFirstChild(); // at the unity folder.
        }
        //-------------------------------------------------------------------
        public void UnUnityise(XmlDocument doc, string filename)
        {
            Msg("Ununityising");
            if (File.Exists(filename + ms_backupSuffix))
            {
                Msg("Reverting to backup {0}", filename + ms_backupSuffix); 
                doc.Load(filename + ms_backupSuffix);
                doc.Save(filename);
            }
        }

        public void SetNumMajorFiles(int set) { m_numUnityMajorFiles = set; }
        public void SetConfiguration(string set) { ms_configuration = set; }
        

        //--------------------------------------------------------------------------------------------------------------------
        private int m_numUnityMajorFiles                = 8;
        public int m_filterCount                        = 0;
        public TreeView m_treeView                      = null;
        public Form1 m_form                             = null;

        static public string ms_backupSuffix            = @"_" + Program.ms_appName + "_backup.xml";
        static public string ms_masterFilename          = @"\_Unity\__UnityMaster.cpp";
        static public string ms_majorFilename           = @"_UnityMajor";
        static public string ms_filesExpression         = @"/VisualStudioProject/Files";
        static public string ms_unityFolder             = @"_Unity";
        static public string ms_unityMinorFileSuffix    = @"UnityMinor.cpp";
        static public string ms_masterFileHeader        = @"";//@"#include ""..\stdafx.h""";
        static public string ms_masterFileFooter        = @"// This file was created automatically by " + Program.ms_appName + ".";
        static public string ms_majorFileHeader         = @"";//@"#include ""..\stdafx.h""";
        static public string ms_majorFileFooter         = @"// This file was created automatically by " + Program.ms_appName + ".";
        static public string ms_minorFileHeader         = @"";//@"#include ""..\stdafx.h""";
        static public string ms_minorFileFooter         = @"// This file was created automatically by " + Program.ms_appName + ".";
        static public string ms_fileFilter              = @".cpp";
        static public string ms_fileRelPathPrefix       = @"<File RelativePath="".";
        static public string ms_fileRelPathSuffix       = @"""> </File>";
        static public string ms_filterPrefix            = @"<Filter Name=""";
        static public string ms_filterSuffix            = @"""></Filter>";
        static public string ms_developmentXMLOutput    = @"test.xml";
        static public string ms_excludeStringPrefix     = @"<FileConfiguration Name=""";
        static public string ms_excludeStringSuffix     = @""" ExcludedFromBuild=""true""></FileConfiguration>";
        static public string ms_configuration           = Program.ms_buildConfig + "|" + Program.ms_buildPlatform; //@"Release|Xbox 360";////@"Debug|Win32";
        static public string ms_precompiledHeaderHeader = @"<FileConfiguration Name=""";
        static public string ms_precompiledHeaderFooter = @"""><Tool Name=""VCCLCompilerTool"" PrecompiledHeaderThrough=""..\StdAfx.h"" /> </FileConfiguration>";
        static public string ms_UnityiseComment         = @"Unitiyised with " + Program.ms_appName + " ";
        static public List<string> ms_exclusions        = new List<string>();

        static public bool ms_bWritePreCompiledHeader   = false;
        static public bool ms_bBuildFromMaster          = false;
        static public bool ms_bBuildFromMinor           = false;
        static public bool ms_bIsUnityised              = false;
    } // class program
} // namespace UnityTool
