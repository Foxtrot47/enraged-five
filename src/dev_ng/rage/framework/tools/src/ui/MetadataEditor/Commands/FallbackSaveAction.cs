﻿//---------------------------------------------------------------------------------------------
// <copyright file="FallbackSaveAction.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace MetadataEditor.Commands
{
    using RSG.Editor;
    using RSG.Editor.SharedCommands;
    using RSG.Project.Commands;

    /// <summary>
    /// Provides a fall back action for the save commands that resets the text on all of the
    /// save command instances.
    /// </summary>
    public class FallbackSaveAction : ButtonAction<ProjectCommandArgs>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="FallbackSaveAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public FallbackSaveAction(ProjectCommandResolver resolver)
            : base(new ParameterResolverDelegate<ProjectCommandArgs>(resolver))
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the command can be executed based on the specified command
        /// parameter.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(ProjectCommandArgs args)
        {
            string fallbackText = "Save Selected Items";
            RockstarCommandManager.UpdateItemText(RockstarCommands.Save, fallbackText);

            fallbackText = "Save Selected Items As...";
            RockstarCommandManager.UpdateItemText(RockstarCommands.SaveAs, fallbackText);
            return false;
        }

        /// <summary>
        /// The logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(ProjectCommandArgs args)
        {
        }
        #endregion Methods
    } // MetadataEditor.Commands.FallbackSaveAction {Class}
} // MetadataEditor.Commands {Namespace}
