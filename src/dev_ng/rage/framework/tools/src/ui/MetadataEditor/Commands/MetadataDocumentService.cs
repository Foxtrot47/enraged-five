﻿//---------------------------------------------------------------------------------------------
// <copyright file="DocumentManagerService.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace MetadataEditor.Commands
{
    using RSG.Editor;
    using RSG.Metadata.View;
    using RSG.Metadata.ViewModel.Tunables;
    using RSG.Project.Commands;

    /// <summary>
    /// Implements the document manager service for Dialogue Star documents.
    /// </summary>
    public class MetadataDocumentService : DocumentManagerService
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MetadataDocumentService"/> class.
        /// </summary>
        /// <param name="serviceProvider">
        /// A command service provider that can be used to get the command dialog service and
        /// the message service.
        /// </param>
        public MetadataDocumentService(ICommandServiceProvider serviceProvider)
            : base(serviceProvider)
        {
        }
        #endregion Constructors

        /// <summary>
        /// 
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        public override System.Windows.FrameworkElement GetControlForDocument(object content)
        {
            return new TunableStructureControl(content as TunableStructureViewModel);
        }
    } // MetadataEditor.Commands.DialogueStarDocumentService {Class}
} // MetadataEditor.Commands {Namespace}
