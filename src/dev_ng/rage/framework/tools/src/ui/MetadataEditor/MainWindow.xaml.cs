﻿//---------------------------------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace MetadataEditor
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.IO;
    using System.Linq;
    using System.Windows;
    using System.Windows.Interop;
    using MetadataEditor.Commands;
    using MetadataEditor.Resources;
    using RSG.Base.Extensions;
    using RSG.Base.Logging;
    using RSG.Base.Logging.Universal;
    using RSG.Editor;
    using RSG.Editor.Controls;
    using RSG.Editor.Controls.Dock;
    using RSG.Editor.Controls.Dock.Document;
    using RSG.Editor.Controls.Dock.ViewModel;
    using RSG.Editor.Controls.Perforce;
    using RSG.Editor.Controls.Perforce.Commands;
    using RSG.Editor.SharedCommands;
    using RSG.Interop.Microsoft.Windows;
    using RSG.Metadata.Commands;
    using RSG.Metadata.View;
    using RSG.Metadata.ViewModel.Tunables;
    using RSG.Project.Commands;
    using RSG.Project.View;
    using RSG.Project.ViewModel;

    /// <summary>
    /// Contains the code behind the application main window control.
    /// </summary>
    public partial class MainWindow : RsMainWindow
    {
        #region Fields
        /// <summary>
        /// The global identifier used for the add menu inside the collection node context
        /// menu.
        /// </summary>
        private static Guid _addCollectionContextMenu;

        /// <summary>
        /// The global identifier used for the add menu inside the collection folder node
        /// context menu.
        /// </summary>
        private static Guid _addCollectionFolderContextMenu;

        /// <summary>
        /// The global identifier used for the add menu inside the top level file menu.
        /// </summary>
        private static Guid _addFileMenu;

        /// <summary>
        /// The global identifier used for the add menu inside the project node context menu.
        /// </summary>
        private static Guid _addProjectContextMenu;

        /// <summary>
        /// The global identifier used for the add menu inside the project folder node context
        /// menu.
        /// </summary>
        private static Guid _addProjectFolderContextMenu;

        /// <summary>
        /// The global identifier used for the edit menu inside the main menu.
        /// </summary>
        private static Guid _editMenu;

        /// <summary>
        /// The global identifier used for the edit toolbar inside the toolbar tray.
        /// </summary>
        private static Guid _editToolbar;

        /// <summary>
        /// The global identifier used for the new menu inside the top level file menu.
        /// </summary>
        private static Guid _newFileMenu;

        /// <summary>
        /// The global identifier used for the open menu inside the top level file menu.
        /// </summary>
        private static Guid _openFileMenu;

        /// <summary>
        /// The global identifier used for the perforce menu inside the main menu.
        /// </summary>
        private static Guid _perforceMenu;

        /// <summary>
        /// The global identifier used for the project menu inside the main menu.
        /// </summary>
        private static Guid _projectMenu;

        /// <summary>
        /// The global identifier used for the project toolbar inside the toolbar tray.
        /// </summary>
        private static Guid _projectToolbar;

        /// <summary>
        /// The global identifier used for the standard toolbar inside the toolbar tray.
        /// </summary>
        private static Guid _standardToolbar;

        /// <summary>
        /// The private reference to the project explorer control in this windows resources.
        /// </summary>
        private ProjectCollectionExplorer _explorer;

        /// <summary>
        /// The main file backup manager for the application that backups all modified opened
        /// documents.
        /// </summary>
        private FileBackupManager _fileBackup;

        /// <summary>
        /// The main file watcher for the application that looks after all loaded projects and
        /// documents.
        /// </summary>
        private FileWatcherManager _fileWatcher;

        /// <summary>
        /// A private collection of full paths to files that have been modified since the
        /// application was last active.
        /// </summary>
        private HashSet<string> _modifiedFiles;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="MainWindow" /> class.
        /// </summary>
        static MainWindow()
        {
            _addFileMenu = new Guid("284F8658-43DB-425A-80B1-4949345BFE84");
            _editMenu = new Guid("84C0201A-1618-47F0-85AF-B202F32541CA");
            _editToolbar = new Guid("083B6FD6-7DE5-4570-A927-807A9136CF7F");
            _newFileMenu = new Guid("4D6E79CE-DD41-497F-8A19-44B6E8948902");
            _openFileMenu = new Guid("8DA7EDD5-3A72-4B31-80DD-B67A8C10D71A");
            _projectMenu = new Guid("42108DAC-C87C-4526-83E1-4A07389943C4");
            _standardToolbar = new Guid("10DFEBA2-FDA5-43C2-B498-243A7A92DF08");
            _projectToolbar = new Guid("26985A82-2D71-4186-920F-C871353E25BA");
            _addCollectionContextMenu = new Guid("CB6ABE75-3757-4071-B0F3-2672688964A6");
            _addCollectionFolderContextMenu = new Guid("CCC5A909-4387-49D6-9466-219C37896D2F");
            _addProjectContextMenu = new Guid("9AB403C7-DAA8-4DFA-8CA7-A9C1D0A957F9");
            _addProjectFolderContextMenu = new Guid("15916F49-FA2E-4031-A24E-067D8709DF08");
            _perforceMenu = new Guid("3E81E832-3CBC-49CE-937A-839E6ED92C93");
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        /// <param name="application">
        /// The instance to the application this is going to be the main window for.
        /// </param>
        public MainWindow(RsApplication application)
        {
            this._modifiedFiles = new HashSet<string>();
            this.InitializeComponent();
            object explorerResource = this.TryFindResource("Explorer");
            this._explorer = explorerResource as ProjectCollectionExplorer;
            if (this._explorer == null)
            {
                throw new NotSupportedException();
            }

            MainWindowDataContext dataContext = new MainWindowDataContext(this);
            this.DataContext = dataContext;

            this.AddCommandInstances(application);
            this.AttachCommandBindings();
            this.AttachCommandBindingsToDocumentPane();
            this.AttachCommandBindingsToExplorer();

            this._fileWatcher = new FileWatcherManager();
            this._fileWatcher.ModifiedTimeChanged += this.OnModifiedTimeChanged;
            this._fileWatcher.ReadOnlyFlagChanged += this.OnReadOnlyFlagChanged;

            string temp = Path.GetTempPath();
            string backupPath = Path.Combine(temp, "Rockstar Games", application.UniqueName);
            this._fileBackup = new FileBackupManager(TimeSpan.FromMinutes(7), backupPath);

            dataContext.CollectionNode.ProjectLoaded += this.OnProjectLoaded;
            dataContext.CollectionNode.ProjectUnloaded += this.OnProjectUnloaded;
            dataContext.CollectionNode.ProjectFailed += this.OnProjectFailed;
            dataContext.CollectionNode.LoadedStateChanged += this.CollectionLoadedStateChanged;

            var docService = application.GetService<IDocumentManagerService>();
            if (docService != null)
            {
                docService.CollectionClosing += this.OnCollectionClosing;
            }

            App app = application as App;
            app.SetBackupManager(this._fileBackup);
            app.SetFileWatcher(this._fileWatcher);
            app.SetViewSite(dataContext.ViewSite);

            this.AllowDrop = true;
            this.Drop += MainWindow_Drop;
            this.DragOver += MainWindow_DragOver;
        }

        void MainWindow_DragOver(object sender, DragEventArgs e)
        {
            DataObject data = e.Data as DataObject;
            if (data == null)
            {
                e.Effects = DragDropEffects.None;
                return;
            }

            StringCollection fileDropList = data.GetFileDropList();
            if (fileDropList.Count == 1)
            {
                if (String.Equals(Path.GetExtension(fileDropList[0]), ".metaprojs"))
                {
                    e.Effects = DragDropEffects.Copy;
                    return;
                }
            }

            e.Effects = DragDropEffects.None;
        }

        void MainWindow_Drop(object sender, DragEventArgs e)
        {
            DataObject data = e.Data as DataObject;
            if (data == null)
            {
                e.Effects = DragDropEffects.None;
                return;
            }

            StringCollection fileDropList = data.GetFileDropList();
            RsApplication app = Application.Current as RsApplication;
            if (fileDropList.Count == 1)
            {
                if (String.Equals(Path.GetExtension(fileDropList[0]), ".metaprojs"))
                {
                    string fullPath = fileDropList[0];
                    CloseCollectionAction close = new CloseCollectionAction(app);
                    ProjectCommandArgs args = this.CommandResolver(null);
                    try
                    {
                        if (close.CanExecute(args))
                        {
                            close.Execute(args);
                        }
                    }
                    catch (OperationCanceledException)
                    {
                        return;
                    }

                    MainWindowDataContext dc = this.DataContext as MainWindowDataContext;
                    dc.CollectionNode.Load(fullPath);
                    app.AddToMruList("Projects and Collections", fullPath, new string[0]);
                }
                else
                {
                    IMessageBoxService msgService = app.GetService<IMessageBoxService>();
                    msgService.ShowStandardErrorBox(
                        "Only dragging in .metaprojs files is supported. Other files must be opened via the File menu or added to an existing project.", null);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender">
        /// 
        /// </param>
        /// <param name="e">
        /// 
        /// </param>
        private void CollectionLoadedStateChanged(object sender, ValueChangedEventArgs<bool> e)
        {
            ProjectCollectionNode collection = sender as ProjectCollectionNode;
            if (collection == null)
            {
                return;
            }

            if (e.NewValue)
            {
                this.Title = collection.Model.FullPath ?? String.Empty;
            }
            else
            {
                this.Title = String.Empty;
            }
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// The action resolver that returns a instance of the project command args class that
        /// project commands can use to execute their logic.
        /// </summary>
        /// <param name="commandData">
        /// The command data sent with the executing command.
        /// </param>
        /// <returns>
        /// The currently active document item inside the view site.
        /// </returns>
        internal ProjectCommandArgs CommandResolver(CommandData commandData)
        {
            MainWindowDataContext dc = this.DataContext as MainWindowDataContext;
            if (dc == null)
            {
                return null;
            }

            ProjectCommandArgs args = new ProjectCommandArgs();
            args.CollectionNode = dc.CollectionNode;
            args.NodeSelectionDelegate = this.SelectNode;
            args.NodeRenameDelegate = this.RenameNode;
            args.OpenedDocuments = dc.ViewSite.AllDocuments;
            args.ViewSite = dc.ViewSite;
            if (this._explorer != null)
            {
                args.SelectedNodes = this._explorer.SelectedItems;
            }

            return args;
        }

        /// <summary>
        /// Adds command instances to the command manager for commands that are located inside
        /// the Add menu off of the collection node context menu.
        /// </summary>
        private void AddCollectionContextMenuAddCommands()
        {
            ushort index = 0;
            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.AddNewProject,
                index++,
                false,
                "New Project...",
                new Guid("9BBEDB12-4343-40C1-BDC3-BF30624A80C4"),
                _addCollectionContextMenu);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.AddExistingProject,
                index++,
                false,
                "Existing Project...",
                new Guid("FC3458B6-B3BC-4B86-BAA1-D000DDC9C7A6"),
                _addCollectionContextMenu);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.AddCollectionFolder,
                index++,
                true,
                "New Collection Folder...",
                new Guid("C2095534-F369-4B68-AF6F-C23C9B9993D7"),
                _addCollectionContextMenu);
        }

        /// <summary>
        /// Adds command instances to the command manager for commands that are located inside
        /// the Add menu off of the collection folder node context menu.
        /// </summary>
        private void AddCollectionFolderContextMenuAddCommands()
        {
            ushort index = 0;
            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.AddNewProject,
                index++,
                false,
                "New Project...",
                new Guid("3464A989-4DC0-4234-92FA-136CCF0E6AB9"),
                _addCollectionFolderContextMenu);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.AddExistingProject,
                index++,
                false,
                "Existing Project...",
                new Guid("0B955BA8-688C-44F8-B723-82764C76DF00"),
                _addCollectionFolderContextMenu);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.AddCollectionFolder,
                index++,
                true,
                "New Collection Folder...",
                new Guid("DA3878AB-A1AE-4A0F-9A14-9CA90B168EB2"),
                _addCollectionFolderContextMenu);
        }

        /// <summary>
        /// Adds all of the commands instances for the metadata application to the command
        /// manager.
        /// </summary>
        /// <param name="application">
        /// The current application instance.
        /// </param>
        private void AddCommandInstances(RsApplication application)
        {
            application.RegisterMruList(
                "Projects and Collections",
                true,
                RockstarCommands.OpenProjectFromMru,
                new Guid("B50A0170-21C9-4894-823C-0DEA301B7452"),
                new Guid("DA0A4944-014F-4B9D-AE7C-95E748E8AAC8"),
                new Guid("1403DCF6-BC7F-440B-B5D5-82C869AE1523"));

            this.AddCommandInstancesToMainMenu();
            this.AddCommandInstancesToFileMenu();
            this.AddCommandInstancesToProjectDynamicFolderContextMenu();
            this.AddCommandInstancesToEditMenu();
            this.AddCommandInstancesToPerforceMenu();
            this.AddCommandInstancesToProjectMenu();
            this.AddCommandInstancesToCollectionContextMenu();
            this.AddCommandInstancesToCollectionFolderContextMenu();
            this.AddCommandInstancesToProjectContextMenu();
            this.AddCommandInstancesToProjectFolderContextMenu();
            this.AddCommandInstancesToProjectItemContextMenu();
            this.AddCommandInstancesToDocumentContextMenu();
            this.AddToolbarInstancesAndCommands();

            this.AddCommandInstancesToMetadataTreeView();
        }

        /// <summary>
        /// Adds command instances to the command manager for commands that are located inside
        /// the context menu for a collection node inside the project explorer.
        /// </summary>
        private void AddCommandInstancesToCollectionContextMenu()
        {
            ushort index = 0;
            RockstarCommandManager.AddMenu(
                index++,
                "Add",
                _addCollectionContextMenu,
                ProjectCommandIds.CollectionContextMenu);

            this.AddCollectionContextMenuAddCommands();

            Guid perforceMenu = new Guid("6A5116BB-2AF6-4A79-91BF-D01C026957DA");
            RockstarCommandManager.AddMenu(
                index++,
                "Perforce",
                perforceMenu,
                ProjectCommandIds.CollectionContextMenu);

            PerforceCommands.CreateStandardMenu(
                perforceMenu,
                new Guid("AC86E6DA-8921-43ED-A48C-6EF1FE948A8A"),
                new Guid("FE8F6D47-F826-4E73-9C39-8111CADDDBE4"),
                new Guid("87E07721-6B6D-4AFB-B88B-C8A0620A7E20"),
                new Guid("7D87922F-E5D5-4A72-8C4A-C345B7F7D03B"),
                new Guid("EE23EA60-D9FA-4A05-9C8E-A9152A5E0226"),
                new Guid("607462F9-2AA0-426A-B0A6-8B64F5616854"));
        }

        /// <summary>
        /// Adds command instances to the command manager for commands that are located inside
        /// the context menu for a collection folder node inside the project explorer.
        /// </summary>
        private void AddCommandInstancesToCollectionFolderContextMenu()
        {
            ushort index = 0;
            RockstarCommandManager.AddMenu(
                index++,
                "Add",
                _addCollectionFolderContextMenu,
                ProjectCommandIds.CollectionFolderContextMenu);

            this.AddCollectionFolderContextMenuAddCommands();

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Delete,
                index++,
                true,
                "Delete",
                new Guid("8DE8032A-F88E-45DB-9085-CA754B4D1271"),
                ProjectCommandIds.CollectionFolderContextMenu);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Rename,
                index++,
                false,
                "Rename",
                new Guid("E92FF961-9B3F-4EAA-9DF7-EA0C13E0AF33"),
                ProjectCommandIds.CollectionFolderContextMenu);
        }

        /// <summary>
        /// Adds command instances to the command manager for commands that are located inside
        /// the context menu for open documents tab.
        /// </summary>
        private void AddCommandInstancesToDocumentContextMenu()
        {
            ushort index = 0;
            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.LocateInExplorer,
                index++,
                false,
                "Locate In Explorer",
                new Guid("75221C6E-C82D-42FE-933E-FD892122BF4A"),
                DockingCommandIds.DocumentContextMenu);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Save,
                index++,
                true,
                "Save",
                new Guid("1FDC1D74-974C-4063-8924-781CE8B07C39"),
                DockingCommandIds.DocumentContextMenu);

            RockstarCommandManager.AddCommandInstance(
                DockingCommands.CloseItem,
                index++,
                false,
                "Close",
                new Guid("E2BB3FF3-959A-408A-B3E9-A0308964600F"),
                DockingCommandIds.DocumentContextMenu);

            RockstarCommandManager.AddCommandInstance(
                DockingCommands.CloseAll,
                index++,
                false,
                "Close All Documents",
                new Guid("430409B4-75AC-4D1B-8915-2910C0AECB09"),
                DockingCommandIds.DocumentContextMenu);

            RockstarCommandManager.AddCommandInstance(
                DockingCommands.CloseAllButItem,
                index++,
                false,
                "Close All But This",
                new Guid("6F098087-4BFD-4D66-A98B-E0C8A0FBE163"),
                DockingCommandIds.DocumentContextMenu);

            RockstarCommandManager.AddCommandInstance(
                DockingCommands.CopyFullPath,
                index++,
                true,
                "Copy Full Path",
                new Guid("3D1A222E-CB65-40BA-9500-BB8E96F0A516"),
                DockingCommandIds.DocumentContextMenu);

            RockstarCommandManager.AddCommandInstance(
                DockingCommands.OpenContainingFolder,
                index++,
                false,
                "Open Containing Folder",
                new Guid("67A6B59B-DD55-42A8-92EB-4232333408E4"),
                DockingCommandIds.DocumentContextMenu);
        }

        /// <summary>
        /// Adds command instances to the command manager for commands that are located inside
        /// the main menus Edit menu.
        /// </summary>
        private void AddCommandInstancesToEditMenu()
        {
            ushort index = 0;
            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Undo,
                index++,
                true,
                "Undo",
                new Guid("01F4FB10-18DB-44AD-8B8D-1574E44A5E65"),
                _editMenu);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Redo,
                index++,
                false,
                "Redo",
                new Guid("7AB3BEB4-43B2-48C3-9A3C-494429DE2A31"),
                _editMenu);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Cut,
                index++,
                true,
                "Cut",
                new Guid("6CC2C020-14FB-4FA4-A856-82ECE479ADFA"),
                _editMenu);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Copy,
                index++,
                false,
                "Copy",
                new Guid("E57464A6-DA15-48B3-AB72-558B738D64AC"),
                _editMenu);

            RockstarCommandManager.AddCommandInstance(
                MetadataCommands.CopyTunableName,
                index++,
                false,
                "Copy Tunable Name",
                new Guid("BE37F16A-C49E-4F9B-A69B-08190F34498A"),
                _editMenu);

            RockstarCommandManager.AddCommandInstance(
                MetadataCommands.CopyTunableType,
                index++,
                false,
                "Copy Tunable Type",
                new Guid("475D2103-2392-4BFE-ADBD-EF1F6291AE8A"),
                _editMenu);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Paste,
                index++,
                false,
                "Paste",
                new Guid("F5A37662-C721-4019-8D6B-84C46B31E3BC"),
                _editMenu);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Delete,
                index++,
                false,
                "Delete",
                new Guid("5C093D82-DC81-49E6-9AF1-D94F19501488"),
                _editMenu);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.SelectAll,
                index++,
                true,
                "Select All",
                new Guid("FB5A174F-204C-4E32-9B1A-38A9F413DF2E"),
                _editMenu);
        }

        /// <summary>
        /// Adds command instances to the command manager for commands that are located inside
        /// the Edit toolbar.
        /// </summary>
        private void AddCommandInstancesToEditToolbar()
        {
            ushort index = 0;
            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Cut,
                index++,
                false,
                "Cut",
                new Guid("E3A6E307-890D-4E63-94A7-3D69C603DCD6"),
                _editToolbar);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Copy,
                index++,
                false,
                "Copy",
                new Guid("B66EDA4F-74D7-4DEE-9CBF-F2341B253A88"),
                _editToolbar);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Paste,
                index++,
                false,
                "Paste",
                new Guid("57707365-377A-493A-AA24-E8AF22365CFB"),
                _editToolbar);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Delete,
                index++,
                false,
                "Delete",
                new Guid("EC6B328D-38EA-410D-AD40-A674D94FC393"),
                _editToolbar);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.SelectAll,
                index++,
                true,
                "Select All",
                new Guid("3A195C37-78F0-4DDF-B31E-6C87FD2B7713"),
                _editToolbar);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Expand,
                index++,
                true,
                "Expand",
                new Guid("3A7823D6-C074-40CB-A656-1AA986C9D9DD"),
                _editToolbar);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.ExpandAll,
                index++,
                false,
                "Expand All",
                new Guid("471AA3AB-7A33-4DB8-909B-AEC8486C5ECF"),
                _editToolbar);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Collapse,
                index++,
                false,
                "Collapse",
                new Guid("DA36ED4B-D1E0-45AC-B297-CEB45AB655D9"),
                _editToolbar);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.CollapseAll,
                index++,
                false,
                "Collapse All",
                new Guid("176CA388-5609-45E6-9179-EAC3E1709D12"),
                _editToolbar);

            RockstarCommandManager.AddCommandInstance(
                MetadataCommands.MoveMetadataItemUp,
                index++,
                true,
                "Move Metadata Item Up",
                new Guid("181CBC58-7808-45B5-8BD1-6BBA09CEA758"),
                _editToolbar);

            RockstarCommandManager.AddCommandInstance(
                MetadataCommands.MoveMetadataItemDown,
                index++,
                false,
                "Move Metadata Item Down",
                new Guid("D950067B-79B2-4C60-BE7A-C67EC6C31A29"),
                _editToolbar);
        }

        /// <summary>
        /// Adds command instances to the command manager for commands that are located inside
        /// the main menus File menu.
        /// </summary>
        private void AddCommandInstancesToFileMenu()
        {
            ushort index = 0;
            RockstarCommandManager.AddMenu(
                index++, "New", _newFileMenu, RockstarCommandManager.FileMenuId);

            RockstarCommandManager.AddMenu(
                index++, "Open", _openFileMenu, RockstarCommandManager.FileMenuId);

            RockstarCommandManager.AddMenu(
                index++, true, "Add", _addFileMenu, RockstarCommandManager.FileMenuId);

            this.AddFileMenuNewCommands();
            this.AddFileMenuOpenCommands();
            this.AddFileMenuAddCommands();

            RockstarCommandManager.AddCommandInstance(
                DockingCommands.CloseItem,
                index++,
                true,
                "Close",
                new Guid("3C920F77-E443-45CA-A030-FF7E19FBE65A"),
                RockstarCommandManager.FileMenuId);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.CloseProjectCollection,
                index++,
                false,
                "Close Project Collection",
                new Guid("324C3328-2283-4217-99E0-306F34F2DA02"),
                RockstarCommandManager.FileMenuId);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Save,
                index++,
                true,
                "Save Selected Items",
                new Guid("3BAA7D00-DAF6-410C-A109-3DAB40A783E1"),
                RockstarCommandManager.FileMenuId);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.SaveAs,
                index++,
                false,
                "Save Selected Items As...",
                new Guid("123F7582-27AD-40CC-ABDE-D8D9D546FC29"),
                RockstarCommandManager.FileMenuId);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.SaveAll,
                index++,
                false,
                "Save All",
                new Guid("1C1F7391-2F12-4A09-8D5E-BDFC118A7AD1"),
                RockstarCommandManager.FileMenuId);

            RockstarCommandManager.AddCommandInstance(
                MetadataCommands.SaveAndResourceFile,
                index++,
                true,
                "Save and Resource",
                new Guid("8405C0BA-48CB-467B-9879-C981DBBF8097"),
                RockstarCommandManager.FileMenuId);

            RockstarCommandManager.AddCommandInstance(
                MetadataCommands.ResourceFile,
                index++,
                false,
                "Resource",
                new Guid("EB7C08A2-B4EC-4D75-B94A-8822C8F74A82"),
                RockstarCommandManager.FileMenuId);
        }

        /// <summary>
        /// Adds command instances to the command manager for commands that are located inside
        /// the main menu command bar.
        /// </summary>
        private void AddCommandInstancesToMainMenu()
        {
            RockstarCommandManager.AddEditMenu(_editMenu);
            RockstarCommandManager.AddProjectMenu(_projectMenu);
            RockstarCommandManager.AddTopLevelMenu(5, "Perforce", _perforceMenu);
        }

        /// <summary>
        /// Adds command instances to the command manager for commands that are located inside
        /// the metadata document tree view editor.
        /// </summary>
        private void AddCommandInstancesToMetadataTreeView()
        {
            ushort index = 0;
            RockstarCommandManager.AddCommandInstance(
                MetadataCommands.AddNewMetadataItem,
                index++,
                false,
                "Add New Item",
                new Guid("BF16423A-81B4-4F39-8941-56D1306FB03D"),
                MetadataCommandIds.TreeListViewItemContextMenu);

            RockstarCommandManager.AddCommandInstance(
                MetadataCommands.InsertNewMetadataItemAbove,
                index++,
                false,
                "Insert New Item Above",
                new Guid("88733D58-EDEC-43C2-846E-41CAB79F3D62"),
                MetadataCommandIds.TreeListViewItemContextMenu);

            RockstarCommandManager.AddCommandInstance(
                MetadataCommands.InsertNewMetadataItemBelow,
                index++,
                false,
                "Insert New Item Below",
                new Guid("EE4B9858-80C3-4FF2-A0E3-4A825B24A62E"),
                MetadataCommandIds.TreeListViewItemContextMenu);

            Guid id = new Guid("87F7B1CE-98F5-427B-9F65-47D4B23083FF");
            RockstarCommandManager.AddMenu(
                index++,
                true,
                "Change Pointer Type",
                id,
                MetadataCommandIds.TreeListViewItemContextMenu);

            RockstarCommandManager.AddCommandInstance(
                MetadataCommands.ChangePointerType,
                0,
                false,
                "Change Pointer Type",
                new Guid("2D738E34-84EC-4689-93A8-A24F2601F6C7"),
                id,
                new CommandInstanceCreationArgs()
                {
                    AreMultiItemsShown = true
                });

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Expand,
                index++,
                true,
                "Expand",
                new Guid("A2A273BD-AD04-4E96-A452-FBEE3FA301DC"),
                MetadataCommandIds.TreeListViewItemContextMenu);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.ExpandAll,
                index++,
                false,
                "Expand All",
                new Guid("CA0D318A-BCED-4E0D-9D74-CCEE2D0B0B94"),
                MetadataCommandIds.TreeListViewItemContextMenu);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Collapse,
                index++,
                false,
                "Collapse",
                new Guid("79DBD1B2-2E88-41CA-AD7E-CEFF3F90BAC8"),
                MetadataCommandIds.TreeListViewItemContextMenu);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.CollapseAll,
                index++,
                false,
                "Collapse All",
                new Guid("485F01E6-A058-42A5-B13F-243D4137F0BE"),
                MetadataCommandIds.TreeListViewItemContextMenu);

            ////RockstarCommandManager.AddCommandInstance(
            ////    MetadataCommands.ChangePointerType,
            ////    index++,
            ////    true,
            ////    "Change Pointer Type",
            ////    new Guid("6D2A24E0-3745-4CCA-A621-041A065409A4"),
            ////    MetadataCommandIds.TreeListViewItemContextMenu);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Cut,
                index++,
                true,
                "Cut",
                new Guid("FBB566EB-DCEC-473D-8C45-131E2C79CF99"),
                MetadataCommandIds.TreeListViewItemContextMenu);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Copy,
                index++,
                false,
                "Copy",
                new Guid("59598560-4F44-4F7B-9FD5-BA6C4829D128"),
                MetadataCommandIds.TreeListViewItemContextMenu);

            RockstarCommandManager.AddCommandInstance(
                MetadataCommands.CopyTunableName,
                index++,
                false,
                "Copy Tunable Name",
                new Guid("466AA969-35AD-4C02-95F7-1B48BFAEBB3E"),
                MetadataCommandIds.TreeListViewItemContextMenu);

            RockstarCommandManager.AddCommandInstance(
                MetadataCommands.CopyTunableType,
                index++,
                false,
                "Copy Tunable Type",
                new Guid("4DD51F40-A431-410D-8E7B-41B6A64FD4CD"),
                MetadataCommandIds.TreeListViewItemContextMenu);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Paste,
                index++,
                false,
                "Paste",
                new Guid("316D3CAD-79A4-4C35-BB1E-E6AF23E4E145"),
                MetadataCommandIds.TreeListViewItemContextMenu);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Delete,
                index++,
                false,
                "Delete",
                new Guid("3A580486-7ADA-4F9B-90F4-E120DF69AC55"),
                MetadataCommandIds.TreeListViewItemContextMenu);

            RockstarCommandManager.AddCommandInstance(
                MetadataCommands.MoveMetadataItemUp,
                index++,
                true,
                "Move Up",
                new Guid("6FA051EC-0B77-4933-A831-4EB6EC58AE53"),
                MetadataCommandIds.TreeListViewItemContextMenu);

            RockstarCommandManager.AddCommandInstance(
                MetadataCommands.MoveMetadataItemDown,
                index++,
                false,
                "Move Down",
                new Guid("EDA5AD14-F50D-4AFB-B38F-032D05284BCC"),
                MetadataCommandIds.TreeListViewItemContextMenu);

            RockstarCommandManager.AddCommandInstance(
                MetadataCommands.ResetToDefault,
                index++,
                false,
                "Reset to Default",
                new Guid("238C9FD6-6AD0-464D-9C53-6262E764979A"),
                MetadataCommandIds.TreeListViewItemContextMenu);
        }

        /// <summary>
        /// Adds command instances to the command manager for commands that are located inside
        /// the context menu for a project node inside the project explorer.
        /// </summary>
        private void AddCommandInstancesToProjectContextMenu()
        {
            ushort index = 0;
            RockstarCommandManager.AddMenu(
                index++,
                "Add",
                _addProjectContextMenu,
                ProjectCommandIds.ProjectContextMenu);

            this.AddProjectContextMenuAddCommands();

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.ReloadProject,
                index++,
                true,
                "Reload",
                new Guid("CEB7A765-3D64-49B2-8BEB-122ACB7AB99E"),
                ProjectCommandIds.ProjectContextMenu);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.UnloadProject,
                index++,
                false,
                "Unload",
                new Guid("63C3DD3A-286D-4AF4-933B-B5D1D3240528"),
                ProjectCommandIds.ProjectContextMenu);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Delete,
                index++,
                true,
                "Delete",
                new Guid("D0D320F1-9BF7-4B3F-9E08-A7604F3CAF0C"),
                ProjectCommandIds.ProjectContextMenu);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.Regenerate,
                index++,
                false,
                "Update Dynamic Folders",
                new Guid("6DEED6A1-D40A-4575-BDE2-AAE4B7F20F05"),
                ProjectCommandIds.ProjectContextMenu);

            RockstarCommandManager.AddCommandInstance(
                MetadataCommands.ValidateProjectDefinitions,
                index++,
                true,
                "Validate Metadata Definitions",
                new Guid("FF442D94-BD77-41EC-8C36-0E81E6A74F18"),
                ProjectCommandIds.ProjectContextMenu);

            Guid perforceMenu = new Guid("87C0372E-403D-4D67-9F9D-B9A41901B707");
            RockstarCommandManager.AddMenu(
                index++,
                "Perforce",
                perforceMenu,
                ProjectCommandIds.ProjectContextMenu);

            PerforceCommands.CreateStandardMenu(
                perforceMenu,
                new Guid("8DA0D781-908A-404E-A5C0-7E9CBB4E0BBE"),
                new Guid("BD450321-AA7E-4BC5-A770-78D5F6E4B59A"),
                new Guid("301595ED-F2B1-44BD-8744-3A3DF7556E7B"),
                new Guid("33C8F10A-4523-4479-8919-49960C3B3422"),
                new Guid("C94A1007-210F-4B97-B841-D23380E65DA0"),
                new Guid("822CBBCF-F667-4AB7-8B17-5BDDD9FA25E2"));
        }

        /// <summary>
        /// Adds command instances to the command manager for commands that are located inside
        /// the context menu for a project dynamic folder node inside the project explorer.
        /// </summary>
        private void AddCommandInstancesToProjectDynamicFolderContextMenu()
        {
            ushort index = 0;
            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Delete,
                index++,
                false,
                "Delete",
                new Guid("54FF49D2-B91A-474F-AF11-CFCF20345E1C"),
                ProjectCommandIds.ProjectDynamicFolderContextMenu);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.Regenerate,
                index++,
                false,
                "Update Dynamic Folder",
                new Guid("21667CB0-703F-4F37-84CE-7FBA2CB1C2D1"),
                ProjectCommandIds.ProjectDynamicFolderContextMenu);
        }

        /// <summary>
        /// Adds command instances to the command manager for commands that are located inside
        /// the context menu for a project folder node inside the project explorer.
        /// </summary>
        private void AddCommandInstancesToProjectFolderContextMenu()
        {
            ushort index = 0;
            RockstarCommandManager.AddMenu(
                index++,
                "Add",
                _addProjectFolderContextMenu,
                ProjectCommandIds.ProjectFolderContextMenu);

            this.AddProjectFolderContextMenuAddCommands();

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Delete,
                index++,
                true,
                "Delete",
                new Guid("7B19B452-BAA6-4E83-A4F8-172AF2FCEAF3"),
                ProjectCommandIds.ProjectFolderContextMenu);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Rename,
                index++,
                false,
                "Rename",
                new Guid("FBF8C124-3C04-4A67-8F6E-799D0077C2E8"),
                ProjectCommandIds.ProjectFolderContextMenu);
        }

        /// <summary>
        /// Adds command instances to the command manager for commands that are located inside
        /// the context menu for a file node inside the project explorer.
        /// </summary>
        private void AddCommandInstancesToProjectItemContextMenu()
        {
            ushort index = 0;
            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.OpenFileFromProject,
                index++,
                false,
                "Open",
                new Guid("F705ED13-F745-4FAF-832E-761531E138C8"),
                ProjectCommandIds.ProjectItemContextMenu);

            Guid perforceMenu = new Guid("18B02A2B-A6C6-496A-8C1F-5A09CC556A57");
            RockstarCommandManager.AddMenu(
                index++,
                "Perforce",
                perforceMenu,
                ProjectCommandIds.ProjectItemContextMenu);

            PerforceCommands.CreateStandardMenu(
                perforceMenu,
                new Guid("AD25390A-479B-47AB-9669-DA688138CD00"),
                new Guid("2BEEE766-CDC7-45B7-B394-561BB6ABDE14"),
                new Guid("F9EAA22D-1496-4FFA-9045-E1C43678F88C"),
                new Guid("161C8E02-BFC9-44E3-BC51-CEC917AA5A93"),
                new Guid("ABC6C71D-A818-4B4A-A4F8-690C22FABF12"),
                new Guid("BF5991A7-8B3F-4B48-98CD-2585652F18BF"));
        }

        /// <summary>
        /// Adds command instances to the command manager for commands that are located inside
        /// the main menus Perforce menu.
        /// </summary>
        private void AddCommandInstancesToPerforceMenu()
        {
            PerforceCommands.CreateStandardMenu(
                _perforceMenu,
                new Guid("9AB2080A-16F3-4CEC-BE06-004318A31427"),
                new Guid("10DF2D36-A8A0-4696-A863-297D2DD8E27A"),
                new Guid("7EC35DB5-3B7B-411F-8CD6-9EAA6AF32CDF"),
                new Guid("40FEB9F7-573F-4060-AED1-1A6598128124"),
                new Guid("79ACC3C8-CCD5-4CA4-9048-C9F025FB927D"),
                new Guid("810F8DC5-6497-46F5-B184-E46428C2D8CF"));
        }

        /// <summary>
        /// Adds command instances to the command manager for commands that are located inside
        /// the main menus Project menu.
        /// </summary>
        private void AddCommandInstancesToProjectMenu()
        {
            ushort index = 0;
            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.AddNewItem,
                index++,
                false,
                "Add New Item...",
                new Guid("51A09DA8-5B8B-4FF0-BD72-6977E9D699F9"),
                _projectMenu);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.AddExistingItem,
                index++,
                false,
                "Add Existing Item...",
                new Guid("D712510C-725A-453F-8163-8CDC348EADAE"),
                _projectMenu);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.ReloadProject,
                index++,
                true,
                "Reload",
                new Guid("4C3D5733-C9DB-43BC-8835-B29C1A29CFF3"),
                _projectMenu);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.UnloadProject,
                index++,
                false,
                "Unload",
                new Guid("99A05656-E576-4645-9A90-ED38DEF4D6A7"),
                _projectMenu);


            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.Regenerate,
                index++,
                true,
                "Update Dynamic Folders",
                new Guid("6B05387E-0ABF-44FD-BFDC-FB293BA3EC8C"),
                _projectMenu);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.AddCollectionFolder,
                index++,
                true,
                "Add New Solution Folder",
                new Guid("A31CA04B-0171-4B70-A861-169FFA795408"),
                _projectMenu);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.AddProjectFolder,
                index++,
                false,
                "New Folder",
                new Guid("86562F18-A404-4FD3-A5F6-3FF4707FEA26"),
                _projectMenu);

            RockstarCommandManager.AddCommandInstance(
                MetadataCommands.ValidateProjectDefinitions,
                index++,
                true,
                "Validate Metadata Definitions",
                new Guid("3AA250AB-FC4C-4886-B937-A60DB92CD56F"),
                _projectMenu);
        }
        
        /// <summary>
        /// Adds command instances to the command manager for commands that are located inside
        /// the project toolbar.
        /// </summary>
        private void AddCommandInstancesToProjectToolbar()
        {
            ushort index = 0;
            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.AddNewItem,
                index++,
                false,
                "New Item...",
                new Guid("EB6F8B5B-7C49-4D43-AC6D-DA9B8023CD2F"),
                _projectToolbar);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.AddExistingItem,
                index++,
                false,
                "Existing Item...",
                new Guid("3322B4F7-FF8F-4A8B-8691-2C33045FB5B0"),
                _projectToolbar);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.AddProjectFolder,
                index++,
                false,
                "New Folder...",
                new Guid("6EF6BD9E-F72A-4EE5-B1D9-B6B1221FB0B9"),
                _projectToolbar);
        }

        /// <summary>
        /// Adds command instances to the command manager for commands that are located inside
        /// the standard toolbar.
        /// </summary>
        private void AddCommandInstancesToStandardToolbar()
        {
            ushort index = 0;
            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Save,
                index++,
                true,
                "Save Selected Items",
                new Guid("A7784EBE-20D4-4DE9-8122-0AC9E87BD804"),
                _standardToolbar);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.SaveAll,
                index++,
                false,
                "Save All",
                new Guid("C28FFC03-32CE-4B08-B583-433FF2C4AC7C"),
                _standardToolbar);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Undo,
                index++,
                true,
                "Undo",
                new Guid("E33327F1-99A4-4E8A-B17D-71020CD3CB4D"),
                _standardToolbar);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Redo,
                index++,
                false,
                "Redo",
                new Guid("2299E370-8993-4C18-A372-2AAC90E4B770"),
                _standardToolbar);
        }

        /// <summary>
        /// Adds command instances to the command manager for commands that are located inside
        /// the Add menu off of the File menu.
        /// </summary>
        private void AddFileMenuAddCommands()
        {
            ushort index = 0;
            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.AddNewProject,
                index++,
                false,
                "New Project...",
                new Guid("516EC49C-E583-4711-8673-D94D48E67C2A"),
                _addFileMenu);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.AddExistingProject,
                index++,
                false,
                "Existing Project...",
                new Guid("E22283EC-E737-4EA8-9CB1-AFC236C2C7F3"),
                _addFileMenu);
        }

        /// <summary>
        /// Adds command instances to the command manager for commands that are located inside
        /// the New menu off of the File menu.
        /// </summary>
        private void AddFileMenuNewCommands()
        {
            ushort index = 0;
            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.CreateNewProject,
                index++,
                false,
                "Project...",
                new Guid("A78C4AB0-7F8B-4930-AC65-CDBF62272900"),
                _newFileMenu);
        }

        /// <summary>
        /// Adds command instances to the command manager for commands that are located inside
        /// the Open menu off of the File menu.
        /// </summary>
        private void AddFileMenuOpenCommands()
        {
            ushort index = 0;
            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.OpenProjectCollectionFile,
                index++,
                false,
                "Project Collection...",
                new Guid("E4FF77F9-20FE-41C7-81DC-22837A1EEB22"),
                _openFileMenu);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.OpenProjectFile,
                index++,
                false,
                "Project Inside New Collection...",
                new Guid("81AA2E50-9D88-46E9-BDD9-35EA2DEBE2DC"),
                _openFileMenu);
        }

        /// <summary>
        /// Adds command instances to the command manager for commands that are located inside
        /// the Add menu off of the project node context menu.
        /// </summary>
        private void AddProjectContextMenuAddCommands()
        {
            ushort index = 0;
            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.AddNewItem,
                index++,
                false,
                "New Item...",
                new Guid("38409B4C-8A2D-47F0-A44D-9A39EE1854FA"),
                _addProjectContextMenu);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.AddExistingItem,
                index++,
                false,
                "Existing Item...",
                new Guid("6D3937BE-D667-441A-8F12-6B86EFE80183"),
                _addProjectContextMenu);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.AddProjectFolder,
                index++,
                false,
                "New Folder...",
                new Guid("3C4E221C-EEB2-439C-B6BE-7E285ED634C4"),
                _addProjectContextMenu);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.AddProjectDynamicFolder,
                index++,
                false,
                "New Dynamic Folder...",
                new Guid("959C6B4F-AD6A-4E5F-A69B-B7D27D58CEF9"),
                _addProjectContextMenu);

        }

        /// <summary>
        /// Adds command instances to the command manager for commands that are located inside
        /// the Add menu off of the project folder node context menu.
        /// </summary>
        private void AddProjectFolderContextMenuAddCommands()
        {
            ushort index = 0;
            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.AddNewItem,
                index++,
                false,
                "New Item...",
                new Guid("BA000EFB-AF14-4BAE-8788-E6043CD6B5F2"),
                _addProjectFolderContextMenu);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.AddExistingItem,
                index++,
                false,
                "Existing Item...",
                new Guid("8DFBFCEB-7D99-480B-9E15-D82977B0F5C6"),
                _addProjectFolderContextMenu);

            RockstarCommandManager.AddCommandInstance(
                ProjectCommands.AddProjectFolder,
                index++,
                false,
                "New Folder...",
                new Guid("51F76347-EFD3-4F36-B44D-1A3A92EAEBAA"),
                _addProjectFolderContextMenu);
        }

        /// <summary>
        /// Adds all of the toolbars to the command manager and the commands that are located
        /// inside them.
        /// </summary>
        private void AddToolbarInstancesAndCommands()
        {
            RockstarCommandManager.AddToolbar(0, 0, "Standard", _standardToolbar);
            RockstarCommandManager.AddToolbar(0, 1, "Project", _projectToolbar);
            RockstarCommandManager.AddToolbar(0, 2, "Edit", _editToolbar);

            this.AddCommandInstancesToStandardToolbar();
            this.AddCommandInstancesToProjectToolbar();
            this.AddCommandInstancesToEditToolbar();
        }

        /// <summary>
        /// Attaches all of the necessary command bindings for this application.
        /// </summary>
        private void AttachCommandBindings()
        {
            ICommandAction action = new OpenFileFromProjectAction(this.CommandResolver);
            action.AddBinding(ProjectCommands.OpenFileFromProject, this);

            action = new CloseAllButActiveDocumentAction(this.CommandResolver);
            action.AddBinding(DockingCommands.CloseAllButItem, this);

            action = new CloseAllDocumentsAction(this.CommandResolver);
            action.AddBinding(DockingCommands.CloseAll, this);

            action = new CloseDocumentAction(this.CommandResolver);
            action.AddBinding(DockingCommands.CloseItem, this);

            action = new SaveAllAction(this.CommandResolver);
            action.AddBinding(RockstarCommands.SaveAll, this);

            OpenCollectionAction openCollectionAction = new OpenCollectionAction(this.CommandResolver);
            openCollectionAction.AddBinding(ProjectCommands.OpenProjectCollectionFile, this);
            openCollectionAction.CollectionOpened += OnCollectionOpened;

            action = new OpenProjectAction(this.CommandResolver);
            action.AddBinding(ProjectCommands.OpenProjectFile, this);
            ((OpenProjectAction)action).ProjectOpened += OnProjectOpened;

            action = new AddNewItemAction(this.CommandResolver);
            action.AddBinding(ProjectCommands.AddNewItem, this);

            action = new RegenerateAction(this.CommandResolver);
            action.AddBinding(ProjectCommands.Regenerate, this);

            action = new AddProjectDynamicFolderAction(this.CommandResolver);
            action.AddBinding(ProjectCommands.AddProjectDynamicFolder, this);

            action = new AddExistingItemAction(this.CommandResolver);
            action.AddBinding(ProjectCommands.AddExistingItem, this);

            action = new AddProjectFolderAction(this.CommandResolver);
            action.AddBinding(ProjectCommands.AddProjectFolder, this);

            action = new CloseCollectionAction(this.CommandResolver);
            action.AddBinding(ProjectCommands.CloseProjectCollection, this);

            action = new AddCollectionFolderAction(this.CommandResolver);
            action.AddBinding(ProjectCommands.AddCollectionFolder, this);

            action = new AddNewProjectAction(this.CommandResolver);
            action.AddBinding(ProjectCommands.AddNewProject, this);

            action = new AddExistingProjectAction(this.CommandResolver);
            action.AddBinding(ProjectCommands.AddExistingProject, this);

            action = new CreateNewProjectAction(this.CommandResolver);
            action.AddBinding(ProjectCommands.CreateNewProject, this);

            action = new ReloadProjectAction(this.CommandResolver);
            action.AddBinding(ProjectCommands.ReloadProject, this);

            action = new UnloadProjectAction(this.CommandResolver);
            action.AddBinding(ProjectCommands.UnloadProject, this);

            action = new ValidateProjectDefinitionsAction(this.CommandResolver);
            action.AddBinding(MetadataCommands.ValidateProjectDefinitions, this);

            action = new FallbackSaveAction(this.CommandResolver);
            action.AddBinding(RockstarCommands.Save, this);
            action.AddBinding(RockstarCommands.SaveAs, this);

            action = new EditAction(this.PerforceCommandResolver);
            action.AddBinding(PerforceCommands.Edit, typeof(RsDocumentItem));

            action = new AddAction(this.PerforceCommandResolver);
            action.AddBinding(PerforceCommands.Add, typeof(RsDocumentItem));

            action = new OpenRecentProjectAction(this.CommandResolver, openCollectionAction);
            action.AddBinding(RockstarCommands.OpenProjectFromMru, this);

            action = new UnloadProjectAction(this.CommandResolver);
            action.AddBinding(ProjectCommands.UnloadProject, this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnCollectionClosing(object sender, CollectionClosedEventArgs e)
        {
            if (String.IsNullOrWhiteSpace(e.ProjectCollection.FullPath))
            {
                return;
            }

            IDocumentManagerService service = sender as IDocumentManagerService;
            Dictionary<string, int> openedDocuments = new Dictionary<string, int>();
            int index = 0;
            foreach (var openedDocument in service.OpenedDocuments)
            {
                openedDocuments[openedDocument.FullPath] = index++;
            }

            e.ProjectCollection.SetOpenedDocuments(openedDocuments);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnProjectOpened(object sender, ProjectOpenedEventArgs e)
        {
            RsApplication app = Application.Current as RsApplication;
            if (app != null)
            {
                app.AddToMruList(
                    "Projects and Collections", e.FullPath, new string[] { "Project" });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnCollectionOpened(object sender, CollectionOpenedEventArgs e)
        {
            RsApplication app = Application.Current as RsApplication;
            MainWindowDataContext dc = this.DataContext as MainWindowDataContext;
            if (app != null && dc != null)
            {
                app.AddToMruList("Projects and Collections", e.FullPath, new string[0]);

                string temp = Path.GetTempPath();
                string path = Path.Combine(temp, "Rockstar Games", app.UniqueName);
                path = Path.Combine(path, "Backup Files", e.ProjectCollection.Name);
                this._fileBackup.BackupDirectory = path;

                if (!e.AlreadyOpened)
                {
                    // Need to open the previously opened documents.
                    foreach (string filename in e.ProjectCollection.PreviouslyOpenedDocuments)
                    {
                        FileNode node = e.ProjectCollection.FindNodeWithFullPath(filename) as FileNode;
                        if (node == null)
                        {
                            continue;
                        }

                        OpenFileFromProjectAction action = new OpenFileFromProjectAction(app);
                        BuildProjectCommandArgs args = new BuildProjectCommandArgs();
                        args.CollectionNode = dc.CollectionNode;
                        args.NodeSelectionDelegate = this.SelectNode;
                        args.NodeRenameDelegate = this.RenameNode;
                        args.OpenedDocuments = dc.ViewSite.AllDocuments;
                        args.ViewSite = dc.ViewSite;
                        if (this._explorer != null)
                        {
                            args.SelectedNodes = this._explorer.SelectedItems;
                        }

                        action.Execute(args, node);
                    }
                }
            }
        }

        /// <summary>
        /// Attaches all of the necessary command bindings to the document pane type.
        /// </summary>
        private void AttachCommandBindingsToDocumentPane()
        {
            ICommandAction action = action = new SaveDocumentAction(this.CommandResolver);
            action.AddBinding(RockstarCommands.Save, typeof(RsDocumentPane));

            action = new SaveDocumentAsAction(this.CommandResolver);
            action.AddBinding(RockstarCommands.SaveAs, typeof(RsDocumentPane));

            action = new SaveAndResourceFileAction(this.CommandResolver);
            action.AddBinding(MetadataCommands.SaveAndResourceFile, typeof(RsDocumentPane));

            action = new ResourceFileAction(this.CommandResolver);
            action.AddBinding(MetadataCommands.ResourceFile, typeof(RsDocumentPane));

            action = new LocateInExplorerAction(this.CommandResolver);
            action.AddBinding(ProjectCommands.LocateInExplorer, typeof(RsDocumentPane));
        }

        /// <summary>
        /// Attaches all of the necessary command bindings to the explorer control instance.
        /// </summary>
        private void AttachCommandBindingsToExplorer()
        {
            if (this._explorer == null)
            {
                throw new InvalidOperationException();
            }

            ICommandAction action = new SaveItemAction(this.CommandResolver);
            action.AddBinding(RockstarCommands.Save, this._explorer);

            action = new SaveItemAsAction(this.CommandResolver);
            action.AddBinding(RockstarCommands.SaveAs, this._explorer);

            action = new DeleteItemAction(this.CommandResolver);
            action.AddBinding(RockstarCommands.Delete, this._explorer);

            action = new SaveAndResourceFileAction(this.CommandResolver);
            action.AddBinding(MetadataCommands.SaveAndResourceFile, this._explorer);

            action = new ResourceFileAction(this.CommandResolver);
            action.AddBinding(MetadataCommands.ResourceFile, this._explorer);
        }

        /// <summary>
        /// Retrieves the correct list of strings representing full paths to perform perforce
        /// operations on depending on the command source.
        /// </summary>
        /// <param name="commandData">
        /// The command data that has called this resolver.
        /// </param>
        /// <returns>
        /// The correct list of strings representing full paths to perform perforce operations
        /// on depending on the command source.
        /// </returns>
        private IList<string> PerforceCommandResolver(CommandData commandData)
        {
            MainWindowDataContext dc = this.DataContext as MainWindowDataContext;
            if (dc == null || dc.ViewSite == null)
            {
                return new List<string>();
            }

            DocumentItem activeDocument = dc.ViewSite.ActiveDocument;
            if (activeDocument == null)
            {
                return new List<string>();
            }

            return new List<string>() { activeDocument.FullPath };
        }

        /// <summary>
        /// Handles the event of registered files being modified outside of the editor
        /// environment.
        /// </summary>
        private void HandleModification()
        {
            MainWindowDataContext dc = this.DataContext as MainWindowDataContext;
            if (dc == null)
            {
                return;
            }

            List<ISaveableDocument> items = new List<ISaveableDocument>();
            items.AddRange(dc.CollectionNode.GetProjects());
            items.AddRange(dc.ViewSite.AllDocuments);

            List<DocumentItem> modifiedDocuments = new List<DocumentItem>();
            List<ProjectNode> modifiedProjects = new List<ProjectNode>();
            while (this._modifiedFiles.Count > 0)
            {
                string fullPath = this._modifiedFiles.First();
                this._modifiedFiles.Remove(fullPath);
                foreach (DocumentItem item in dc.ViewSite.AllDocuments)
                {
                    if (!String.Equals(fullPath, item.FullPath))
                    {
                        continue;
                    }

                    modifiedDocuments.Add(item);
                }

                foreach (ProjectNode item in dc.CollectionNode.GetProjects())
                {
                    if (!String.Equals(fullPath, item.FullPath))
                    {
                        continue;
                    }

                    modifiedProjects.Add(item);
                }
            }

            bool yesToAll = false;
            bool noToAll = false;
            foreach (DocumentItem document in modifiedDocuments)
            {
                if (noToAll)
                {
                    return;
                }

                bool performReload = true;
                if (!yesToAll)
                {
                    string msg = StringTable.FileExternallyModifiedMsg;
                    if (document.IsModified)
                    {
                        msg = StringTable.ModifiedFileExternallyModifiedMsg;
                    }

                    IMessageBox msgBox = new RsMessageBox();
                    msgBox.Text = msg.FormatInvariant(document.FullPath);
                    msgBox.SetIconToExclamation();
                    long yesValueResult = 1;
                    long yesToAllValueResult = 2;
                    long noValueResult = 3;
                    long noToAllValueResult = 4;
                    msgBox.AddButton("Yes", yesValueResult, true, false);
                    msgBox.AddButton("Yes to All", yesToAllValueResult, false, false);
                    msgBox.AddButton("No", noValueResult, false, true);
                    msgBox.AddButton("No to All", noToAllValueResult, false, false);
                    long result = msgBox.ShowMessageBox(this);
                    if (result == yesToAllValueResult)
                    {
                        yesToAll = true;
                    }
                    else if (result == noValueResult)
                    {
                        performReload = false;
                    }
                    else if (result == noToAllValueResult)
                    {
                        performReload = false;
                        noToAll = true;
                    }
                }

                if (!performReload)
                {
                    continue;
                }

                ProjectDocumentItem projectDocument = document as ProjectDocumentItem;
                if (projectDocument != null)
                {
                    FileNode node = projectDocument.FileNode;
                    ICreatesDocumentContent creator = node as ICreatesDocumentContent;
                    if (creator != null)
                    {
                        object content = null;
                        try
                        {
                            IUniversalLog log =
                                LogFactory.CreateUniversalLog("Content Creation");
                            string logFlename = Path.Combine(
                                Path.GetTempPath(), "content_creation");

                            logFlename = Path.ChangeExtension(
                                logFlename, UniversalLogFile.EXTENSION);
                            UniversalLogFile target = new UniversalLogFile(
                                logFlename, FileMode.Create, FlushMode.Never, log);

                            ICreateDocumentContentController controller =
                                creator.CreateDocumentContentController;
                            if (controller != null)
                            {
                                CreateContentArgs e =
                                    new CreateContentArgs(
                                        log, dc.ViewSite, document.UndoEngine);
                                content = controller.CreateContent(node as HierarchyNode, e);
                            }
                        }
                        catch (Exception ex)
                        {
                            RsMessageBox.Show(
                                StringTable.FailedFileParsing.FormatInvariant(ex.Message),
                                null,
                                MessageBoxButton.OK,
                                MessageBoxImage.Error);
                        }

                        document.UndoEngine.ResetAllStacks();
                        document.Content =
                            new TunableStructureControl(content as TunableStructureViewModel);
                    }
                }
            }

            foreach (ProjectNode project in modifiedProjects)
            {
                if (noToAll)
                {
                    return;
                }

                bool performReload = true;
                if (!yesToAll)
                {
                    string msg = StringTable.FileExternallyModifiedMsg;
                    if (project.IsModified)
                    {
                        msg = StringTable.ModifiedFileExternallyModifiedMsg;
                    }

                    IMessageBox msgBox = new RsMessageBox();
                    msgBox.Text = msg.FormatInvariant(project.FullPath);
                    msgBox.SetIconToExclamation();
                    long yesValueResult = 1;
                    long yesToAllValueResult = 2;
                    long noValueResult = 3;
                    long noToAllValueResult = 4;
                    msgBox.AddButton("Yes", yesValueResult, true, false);
                    msgBox.AddButton("Yes to All", yesToAllValueResult, false, false);
                    msgBox.AddButton("No", noValueResult, false, true);
                    msgBox.AddButton("No to All", noToAllValueResult, false, false);
                    long result = msgBox.ShowMessageBox(this);
                    if (result == yesToAllValueResult)
                    {
                        yesToAll = true;
                    }
                    else if (result == noValueResult)
                    {
                        performReload = false;
                    }
                    else if (result == noToAllValueResult)
                    {
                        performReload = false;
                        noToAll = true;
                    }
                }

                if (!performReload)
                {
                    continue;
                }

                Application app = Application.Current;
                ICommandServiceProvider provider = app as ICommandServiceProvider;
                ProjectCommandArgs args = new ProjectCommandArgs();
                args.SelectedNodes = Enumerable.Repeat(project, 1);
                args.OpenedDocuments = dc.ViewSite.AllDocuments;
                args.ViewSite = dc.ViewSite;

                List<string> openedFiles = new List<string>();
                foreach (DocumentItem document in dc.ViewSite.AllDocuments)
                {
                    ProjectDocumentItem projectDocument = document as ProjectDocumentItem;
                    if (projectDocument == null)
                    {
                        continue;
                    }

                    FileNode node = projectDocument.FileNode;
                    if (node == null)
                    {
                        continue;
                    }

                    if (!Object.ReferenceEquals(project, node.ParentProject))
                    {
                        continue;
                    }

                    openedFiles.Add(projectDocument.FullPath);
                }

                try
                {
                    UnloadProjectAction unloadAction = new UnloadProjectAction(provider);
                    unloadAction.Execute(args);

                    ReloadProjectAction reloadAction = new ReloadProjectAction(provider);
                    reloadAction.Execute(args);
                }
                catch (OperationCanceledException)
                {
                    return;
                }

                List<IHierarchyNode> openedNodes = new List<IHierarchyNode>();
                foreach (string openedFile in openedFiles)
                {
                    IHierarchyNode node = project.FindNodeWithFullPath(openedFile);
                    if (node == null)
                    {
                        continue;
                    }

                    openedNodes.Add(node);
                }

                args.SelectedNodes = openedNodes;
                args.OpenedDocuments = dc.ViewSite.AllDocuments;
                args.ViewSite = dc.ViewSite;
                OpenFileFromProjectAction openAction = new OpenFileFromProjectAction(provider);
                openAction.Execute(args, null);
            }
        }

        /// <summary>
        /// Called whenever the last write time for a registered file gets updated.
        /// </summary>
        /// <param name="fullPath">
        /// The full path of the file that was modified.
        /// </param>
        private void OnModifiedTimeChanged(string fullPath)
        {
            this._modifiedFiles.Add(fullPath);
            IntPtr foreground = User32.GetForegroundWindow();
            foreach (Window window in Application.Current.Windows)
            {
                WindowInteropHelper helper = new WindowInteropHelper(window);
                if (helper == null)
                {
                    continue;
                }

                IntPtr handle = helper.Handle;
                if (foreground.ToInt64() == handle.ToInt64())
                {
                    this.HandleModification();
                    return;
                }
            }

            Application.Current.Activated += this.HandleModificationOnceActivated;
        }

        /// <summary>
        /// Called once the application becomes active so that previous modifications to
        /// registered files can be handled.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.EventArgs containing the event data. Always empty.
        /// </param>
        private void HandleModificationOnceActivated(object sender, EventArgs e)
        {
            Application.Current.Activated -= this.HandleModificationOnceActivated;
            this.HandleModification();
        }

        /// <summary>
        /// Called whenever the last write time for a registered file gets updated.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The RSG.Editor.ModifiedTimeChangedEventArgs containing the event data including the
        /// full path to the file that has been modified.
        /// </param>
        private void OnModifiedTimeChanged(object sender, ModifiedTimeChangedEventArgs e)
        {
            if (this.Dispatcher.CheckAccess())
            {
                this.OnModifiedTimeChanged(e.FullPath);
            }
            else
            {
                this.Dispatcher.BeginInvoke(
                    new Action(
                        delegate
                        {
                            this.OnModifiedTimeChanged(e.FullPath);
                        }));
            }
        }

        /// <summary>
        /// Called whenever a project failed to load either the first time or every time the
        /// user tries to reload it.
        /// </summary>
        /// <param name="sender">
        /// The collection this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The RSG.Project.ViewModel.ProjectLoadFailedEventArgs containing the event data.
        /// </param>
        private void OnProjectFailed(object sender, ProjectLoadFailedEventArgs e)
        {
            if (this.Dispatcher.CheckAccess())
            {
                ProjectNode node = e.Project;

                string name = Path.GetFileNameWithoutExtension(node.Model.Include);
                e.Project.Text = StringTable.ProjectLoadFailedTextFormat.FormatCurrent(name);
                e.Project.NotifyPropertyChanged("Icon");
                e.Project.ClearChildren();
                HierarchyNode failedNode = new HierarchyNode();
                failedNode.Text = StringTable.ProjectLoadFailedNodeText;
                node.AddChild(failedNode);
                this.SelectNode(e.Project);

                if (e.Reloaded == true)
                {
                    RsMessageBox.Show(
                        e.Exception.Message,
                        null,
                        MessageBoxButton.OK,
                        MessageBoxImage.Error);
                }
            }
            else
            {
                this.Dispatcher.BeginInvoke(
                    new Action(
                        delegate
                        {
                            this.OnProjectFailed(sender, e);
                        }));
            }
        }

        /// <summary>
        /// Called whenever a project is loaded in the attached collection.
        /// </summary>
        /// <param name="sender">
        /// The collection this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The RSG.Project.ViewModel.ProjectLoadedStateChangedEventArgs containing the event
        /// data.
        /// </param>
        private void OnProjectLoaded(object sender, ProjectLoadedStateChangedEventArgs e)
        {
            lock (this._fileWatcher)
            {
                this._fileWatcher.RegisterFile(e.Project.FullPath);
            }
        }

        /// <summary>
        /// Called whenever a project is unloaded or removed from the attached collection.
        /// </summary>
        /// <param name="sender">
        /// The collection this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The RSG.Project.ViewModel.ProjectLoadedStateChangedEventArgs containing the event
        /// data.
        /// </param>
        private void OnProjectUnloaded(object sender, ProjectLoadedStateChangedEventArgs e)
        {
            lock (this._fileWatcher)
            {
                this._fileWatcher.UnregisterFile(e.Project.FullPath);
            }
        }

        /// <summary>
        /// Called whenever the read-only flag for a registered document changes.
        /// </summary>
        /// <param name="fullPath">
        /// The full path of the file whose read-only flag was changed.
        /// </param>
        /// <param name="newValue">
        /// The new value of the read-only flag.
        /// </param>
        private void OnReadOnlyFlagChanged(string fullPath, bool newValue)
        {
            MainWindowDataContext dc = this.DataContext as MainWindowDataContext;
            if (dc == null || dc.ViewSite == null)
            {
                return;
            }

            IEnumerable<DocumentItem> documents = dc.ViewSite.AllDocuments;
            if (documents == null)
            {
                return;
            }

            foreach (ProjectDocumentItem item in documents)
            {
                if (!String.Equals(item.FullPath, fullPath))
                {
                    continue;
                }

                item.IsReadOnly = newValue;
            }
        }

        /// <summary>
        /// Called whenever the read-only flag for a registered document changes.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The RSG.Editor.ReadOnlyChangedEventArgs containing the event data including the
        /// full path to the file that has been modified.
        /// </param>
        private void OnReadOnlyFlagChanged(object sender, ReadOnlyChangedEventArgs e)
        {
            DispatcherHelper.CheckBeginInvokeOnUI(
                new Action(
                        delegate
                        {
                            this.OnReadOnlyFlagChanged(e.FullPath, e.NewValue);
                        }));
        }

        /// <summary>
        /// Shows the rename edit control for the specified hierarchy node inside the project
        /// explorer.
        /// </summary>
        /// <param name="node">
        /// The node that should be renamed.
        /// </param>
        private void RenameNode(IHierarchyNode node)
        {
            if (node == null)
            {
                return;
            }

            DispatcherHelper.CheckBeginInvokeOnUI(
                new Action(
                delegate
                {
                    if (this._explorer != null)
                    {
                        this._explorer.RenameItem(node);
                    }
                }));
        }

        /// <summary>
        /// Selects the specified hierarchy node inside the project explorer.
        /// </summary>
        /// <param name="node">
        /// The node that should be selected.
        /// </param>
        private void SelectNode(IHierarchyNode node)
        {
            if (node == null)
            {
                return;
            }

            if (this.Dispatcher.CheckAccess())
            {
                if (this._explorer != null)
                {
                    this._explorer.SelectItem(node);
                }
            }
            else
            {
                this.Dispatcher.BeginInvoke(
                    new Action(
                        delegate
                        {
                            this.SelectNode(node);
                        }));
            }
        }
        #endregion Methods
    } // MetadataEditor.MainWindow {Class}
} // MetadataEditor {Namespace}
