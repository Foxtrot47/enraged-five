﻿//---------------------------------------------------------------------------------------------
// <copyright file="MetadataAddItemWindow.xaml.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace MetadataEditor
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.IO;
    using System.Text.RegularExpressions;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using System.Windows.Data;
    using RSG.Base.Extensions;
    using RSG.Editor;
    using RSG.Editor.Controls;
    using RSG.Metadata.Model.Definitions;
    using RSG.Metadata.ViewModel.Project;
    using RSG.Project.Model;
    using RSG.Project.View.Resources;
    using RSG.Project.ViewModel.Definitions;

    /// <summary>
    /// Interaction logic for the MetadataAddItemWindow.
    /// </summary>
    public partial class MetadataAddItemWindow : RsWindow
    {
        #region Fields
        /// <summary>
        /// 
        /// </summary>
        private ObservableCollection<ProjectItemDefinition> _activeDefinitions;

        /// <summary>
        /// The private reference to the new items scope so that a unique name can be
        /// determined.
        /// </summary>
        private IProjectItemScope _itemScope;

        /// <summary>
        /// 
        /// </summary>
        private ObservableCollection<StructureProviders> _providers;

        /// <summary>
        /// The regular expression that has been set up from the search control.
        /// </summary>
        private Regex _regex;

        /// <summary>
        /// A private value indicating whether the default name is still being displayed for
        /// the selected definition.
        /// </summary>
        private bool _usingDefaultName;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MetadataAddItemWindow"/> class.
        /// </summary>
        public MetadataAddItemWindow()
        {
            this._usingDefaultName = true;
            this._providers = new ObservableCollection<StructureProviders>();
            this._activeDefinitions = new ObservableCollection<ProjectItemDefinition>();
            this.InitializeComponent();

            Binding binding = new Binding();
            binding.Source = this._providers;
            binding.BindsDirectlyToSource = true;

            this.Providers.SetBinding(ItemsControl.ItemsSourceProperty, binding);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<ProjectItemDefinition> ActiveDefinitions
        {
            get { return this._activeDefinitions; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Sets the definition for this window and gives access to the definitions that will
        /// be available for selection.
        /// </summary>
        /// <param name="project">
        /// The project whose definition contains the available child definitions for
        /// selection.
        /// </param>
        public void SetDefinition(StandardProjectNode project)
        {
            this.Title = SelectorWindowResources.AddItemTitle;
            if (this.TemplateListBox == null || project == null || project.Definition == null)
            {
                return;
            }

            project.EnsureDefinitionsAreLoaded();
            this._itemScope = project.Model as IProjectItemScope;

            StructureProviders roots = new StructureProviders("Root Structures", this);
            StructureProviders children = new StructureProviders("Child Structures", this);
            foreach (IStructure structure in project.MetadataDefinitions.Structures)
            {
                StructureProvider parent = null;
                HashSet<string> extensions = new HashSet<string>();
                if (!structure.IsRootStructure)
                {
                    parent = children.GetOrCreateProvider("All", null);
                }
                else
                {
                    string[] parts = structure.Category.SafeSplit('.', true);
                    if (parts.Length == 0)
                    {
                        parts = new string[] { "Miscellaneous" };
                    }

                    foreach (string part in parts)
                    {
                        parent = roots.GetOrCreateProvider(part, parent);
                    }
                }

                string filter = structure.Filter;
                try
                {
                    if (!String.IsNullOrWhiteSpace(filter))
                    {
                        string[] parts = filter.Split(new char[] { '|' });
                        for (int i = 1; i < parts.Length; i += 2)
                        {
                            string[] patterns = parts[i].Split(new char[] { ';' });
                            foreach (string pattern in patterns)
                            {
                                int startIndex = pattern.IndexOf(".");
                                if (startIndex == -1)
                                {
                                    startIndex = 0;
                                }

                                extensions.Add(pattern.Substring(startIndex));
                            }
                        }
                    }
                }
                catch
                {
                    Debug.Fail("Unable to parse the filter string '{0}'.", filter);
                }

                if (extensions.Count == 0)
                {
                    extensions.Add(".meta");
                }

                if (parent != null)
                {
                    foreach (string extension in extensions)
                    {
                        parent.Definitions.Add(
                            new StructureProjectItemDefinition(structure, extension));
                    }
                }
            }

            this._providers.Add(roots);
            this._providers.Add(children);

            if (roots.Count > 0)
            {
                roots.IsExpanded = true;
            }
            else
            {
                children.IsExpanded = true;
            }

            Binding binding = new Binding("ActiveDefinitions");
            binding.Source = this;
            binding.UpdateSourceTrigger = UpdateSourceTrigger.Explicit;
            this.TemplateListBox.SetBinding(ListBox.ItemsSourceProperty, binding);
            this.TemplateListBox.ItemContainerGenerator.StatusChanged += this.OnStatusChanged;

            IEnumerable source = this.TemplateListBox.Items;
            if (source == null)
            {
                return;
            }

            ICollectionView view = CollectionViewSource.GetDefaultView(source);
            if (view == null)
            {
                return;
            }

            view.Filter = this.FilterTemplateList;
            view.SortDescriptions.Clear();
            foreach (SortDescription sortDescription in this.GetCurrentSortDescriptions())
            {
                view.SortDescriptions.Add(sortDescription);
            }
        }

        /// <summary>
        /// Determines whether the
        /// <see cref="RSG.Editor.SharedCommands.RockstarViewCommands.ConfirmDialog"/> command
        /// can be executed at this level.
        /// </summary>
        /// <param name="e">
        /// The command data that is past from the command system.
        /// </param>
        /// <returns>
        /// True if the command can be executed here; otherwise, false.
        /// </returns>
        protected override bool CanConfirmDialog(CanExecuteCommandData e)
        {
            if (String.IsNullOrWhiteSpace(this.NameValueTextBox.Text))
            {
                return false;
            }

            if (String.IsNullOrWhiteSpace(this.LocationValueTextBox.Text))
            {
                return false;
            }

            string pattern = "\\A[^{0}]*\\z";
            pattern = String.Format(pattern, new String(Path.GetInvalidFileNameChars()));
            var regex = new System.Text.RegularExpressions.Regex(pattern);
            if (!regex.IsMatch(this.NameValueTextBox.Text))
            {
                return false;
            }

            pattern = "\\A[a-zA-Z]:\\\\[^{0}]*\\z";
            pattern = String.Format(pattern, new String(Path.GetInvalidPathChars()));
            regex = new System.Text.RegularExpressions.Regex(pattern);
            if (!regex.IsMatch(this.LocationValueTextBox.Text))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Handles the
        /// <see cref="RSG.Editor.SharedCommands.RockstarViewCommands.ConfirmDialog"/> command
        /// after it gets caught here.
        /// </summary>
        /// <param name="e">
        /// The command data that is past from the command system.
        /// </param>
        protected override void ConfirmDialog(ExecuteCommandData e)
        {
            this.DialogResult = true;
        }

        /// <summary>
        /// The method the collection view for the list box uses to determine whether a item is
        /// to be shown or not.
        /// </summary>
        /// <param name="item">
        /// The item that is tested for visibility.
        /// </param>
        /// <returns>
        /// True if the specified item should be displayed; otherwise, false.
        /// </returns>
        private bool FilterTemplateList(object item)
        {
            if (this.TemplateSearchControl == null)
            {
                return true;
            }

            if (String.IsNullOrEmpty(this.TemplateSearchControl.SearchText))
            {
                return true;
            }

            ProjectItemDefinition definition = item as ProjectItemDefinition;
            if (definition != null && this._regex != null)
            {
                return this._regex.IsMatch(definition.Name);
            }

            return true;
        }

        /// <summary>
        /// Gets the sort descriptions that the list box should be using based on the currently
        /// selected sort algorithm.
        /// </summary>
        /// <returns>
        /// The sort descriptions that the list box should be using.
        /// </returns>
        private List<SortDescription> GetCurrentSortDescriptions()
        {
            List<SortDescription> sorts = new List<SortDescription>();
            ListSortDirection typeDirection = ListSortDirection.Descending;
            if (this.SortSelector != null)
            {
                switch (this.SortSelector.SelectedIndex)
                {
                    case 0:
                        sorts.Add(new SortDescription("Name", ListSortDirection.Ascending));
                        typeDirection = ListSortDirection.Ascending;
                        break;
                    case 1:
                        sorts.Add(new SortDescription("Name", ListSortDirection.Descending));
                        typeDirection = ListSortDirection.Descending;
                        break;
                }
            }

            sorts.Add(new SortDescription("Type", typeDirection));
            return sorts;
        }

        /// <summary>
        /// Called when the browse button is pressed. Shows the select folder windows dialog
        /// for the user to select.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs containing the event data.
        /// </param>
        private void OnBrowseButtonClicked(object sender, RoutedEventArgs e)
        {
            string directory = null;
            Guid id = new Guid("834A6F2A-71BA-4C87-B99D-0761BC38363D");
            RsSelectFolderDialog dlg = new RsSelectFolderDialog(id);
            dlg.Title = SelectorWindowResources.BrowseItemLocationTitle;

            bool? result = dlg.ShowDialog();
            if (result == true)
            {
                directory = dlg.FileName;
            }
            else
            {
                return;
            }

            this.LocationValueTextBox.Text = Path.GetFullPath(directory);
        }

        /// <summary>
        /// Called whenever the selected definition changes so that the name can be updated to
        /// match the new selection.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Controls.SelectionChangedEventArgs containing the event data.
        /// </param>
        private void OnDefinitionSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!this._usingDefaultName || this._itemScope == null)
            {
                return;
            }

            RSG.Project.ViewModel.Definitions.IDefinition definition = this.TemplateListBox.SelectedItem as RSG.Project.ViewModel.Definitions.IDefinition;
            if (definition == null)
            {
                if (this.TemplateListBox.SelectedIndex != -1)
                {
                    return;
                }

                ItemContainerGenerator generator = this.TemplateListBox.ItemContainerGenerator;
                if (generator == null)
                {
                    return;
                }

                Control container = generator.ContainerFromIndex(0) as Control;
                if (container == null)
                {
                    generator.StatusChanged += this.OnStatusChanged;
                    return;
                }

                this.TemplateListBox.SelectedItem = container.DataContext;
                return;
            }

            string baseFilename = definition.DefaultName + definition.Extension;
            string name = this._itemScope.GetUniqueFilename(baseFilename);
            this.NameValueTextBox.TextChanged -= this.OnNameChanged;
            this.NameValueTextBox.Text = name;
            this.NameValueTextBox.TextChanged += this.OnNameChanged;
        }

        /// <summary>
        /// Called whenever the text inside the name text box changes outside of the text
        /// changing due to default name updating.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Controls.TextChangedEventArgs containing the event data.
        /// </param>
        private void OnNameChanged(object sender, TextChangedEventArgs e)
        {
            this._usingDefaultName = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnProviderCollapsed(object sender, RoutedEventArgs e)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnProviderExpanded(object sender, RoutedEventArgs e)
        {
            StructureProviders dc = ((Expander)sender).DataContext as StructureProviders;

            foreach (StructureProviders providers in this._providers)
            {
                if (!Object.ReferenceEquals(dc, providers))
                {
                    providers.IsExpanded = false;
                }
            }
        }

        /// <summary>
        /// Called whenever the sort option changes so that the sort description can be update
        /// on the collection view.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Controls.SelectionChangedEventArgs containing the event data.
        /// </param>
        private void OnSortSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.TemplateListBox == null)
            {
                return;
            }

            IEnumerable source = this.TemplateListBox.Items;
            if (source == null)
            {
                return;
            }

            ICollectionView view = CollectionViewSource.GetDefaultView(source);
            if (view == null)
            {
                return;
            }

            view.SortDescriptions.Clear();
            foreach (SortDescription sortDescription in this.GetCurrentSortDescriptions())
            {
                view.SortDescriptions.Add(sortDescription);
            }

            view.Refresh();
        }

        /// <summary>
        /// Called whenever the container generator for the list box updates its status. This
        /// is used to make sure a item is selected whenever the item source changes.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.EventArgs containing the event data.
        /// </param>
        private void OnStatusChanged(object sender, EventArgs e)
        {
            ItemContainerGenerator generator = sender as ItemContainerGenerator;
            if (generator == null)
            {
                return;
            }

            if (generator.Status == GeneratorStatus.ContainersGenerated)
            {
                Control container = generator.ContainerFromIndex(0) as Control;
                if (container == null)
                {
                    return;
                }

                this.TemplateListBox.SelectedItem = container.DataContext;
                generator.StatusChanged -= this.OnStatusChanged;
            }
        }

        /// <summary>
        /// Refreshes the filtering of the current definition list based on a routed event
        /// being fired. This handler to attached to the search events among other ones.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Controls.SelectionChangedEventArgs containing the event data.
        /// </param>
        private void RefreshSearch(object sender, RoutedEventArgs e)
        {
            this.TemplateSearchControl.HasError = false;
            IEnumerable source = this.TemplateListBox.Items;
            if (source == null)
            {
                return;
            }

            ICollectionView view = CollectionViewSource.GetDefaultView(source);
            if (view == null)
            {
                return;
            }

            string searchText = this.TemplateSearchControl.SearchText;
            if (String.IsNullOrEmpty(this.TemplateSearchControl.SearchText))
            {
                this._regex = null;
            }
            else
            {
                string pattern = searchText;
                RegexOptions options = RegexOptions.None;
                if (!this.TemplateSearchControl.RegularExpressionsChecked)
                {
                    pattern = Regex.Escape(pattern);
                }

                if (!this.TemplateSearchControl.MatchCaseChecked)
                {
                    options |= RegexOptions.IgnoreCase;
                }

                if (this.TemplateSearchControl.MatchWordChecked)
                {
                    pattern = String.Format(@"^{0}$", pattern);
                }

                try
                {
                    this._regex = new Regex(pattern, options);
                }
                catch (ArgumentException)
                {
                    this.TemplateSearchControl.HasError = true;
                }
            }

            RsHighlightTextBlock.SetHighlightExpression(this, this._regex);
            view.Filter = this.FilterTemplateList;
            this.TemplateListBox.ScrollIntoView(this.TemplateListBox.SelectedItem);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="provider"></param>
        private void SetDefinitions(StructureProvider provider)
        {
            if (provider == null)
            {
                this._activeDefinitions =
                    new ObservableCollection<ProjectItemDefinition>();
            }
            else
            {
                this._activeDefinitions =
                    new ObservableCollection<ProjectItemDefinition>(provider.Definitions);
            }

            var exp = this.TemplateListBox.GetBindingExpression(ListBox.ItemsSourceProperty);
            if (exp != null)
            {
                exp.UpdateTarget();
            }
        }
        #endregion Methods

        #region Classes
        /// <summary>
        /// 
        /// </summary>
        [DebuggerDisplay("{_name}")]
        private class StructureProvider : NotifyPropertyChangedBase
        {
            #region Fields
            /// <summary>
            /// 
            /// </summary>
            private ObservableCollection<StructureProvider> _children;

            /// <summary>
            /// 
            /// </summary>
            private ObservableCollection<ProjectItemDefinition> _definitions;

            /// <summary>
            /// 
            /// </summary>
            private bool _isExpanded;

            /// <summary>
            /// 
            /// </summary>
            private bool _isSelected;

            /// <summary>
            /// 
            /// </summary>
            private string _name;

            /// <summary>
            /// 
            /// </summary>
            private MetadataAddItemWindow _window;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// 
            /// </summary>
            /// <param name="name"></param>
            /// <param name="window"></param>
            public StructureProvider(string name, MetadataAddItemWindow window)
            {
                this._name = name;
                this._window = window;
                this._children = new ObservableCollection<StructureProvider>();
                this._definitions = new ObservableCollection<ProjectItemDefinition>();
                ICollectionView view = CollectionViewSource.GetDefaultView(this._children);
                if (view == null)
                {
                    return;
                }

                view.SortDescriptions.Clear();
                view.SortDescriptions.Add(new SortDescription("Name", ListSortDirection.Ascending));
            }
            #endregion Constructors

            #region Properties
            /// <summary>
            /// 
            /// </summary>
            public ObservableCollection<StructureProvider> Children
            {
                get { return this._children; }
            }

            /// <summary>
            /// 
            /// </summary>
            public ObservableCollection<ProjectItemDefinition> Definitions
            {
                get { return this._definitions; }
            }

            /// <summary>
            /// 
            /// </summary>
            public bool IsExpanded
            {
                get { return this._isExpanded; }
                set { this.SetProperty(ref this._isExpanded, value); }
            }

            /// <summary>
            /// 
            /// </summary>
            public bool IsSelected
            {
                get { return this._isSelected; }
                set
                {
                    if (this.SetProperty(ref this._isSelected, value))
                    {
                        if (value)
                        {
                            this._window.SetDefinitions(this);
                        }
                    }
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public string Name
            {
                get { return this._name; }
            }
            #endregion Properties

            #region Methods
            /// <summary>
            /// 
            /// </summary>
            public void Reset()
            {
                this.IsSelected = false;
                foreach (StructureProvider child in this.Children)
                {
                    child.Reset();
                }
            }
            #endregion Methods
        } // MetadataAddItemWindow.StructureProvider {Class}

        /// <summary>
        /// 
        /// </summary>
        private class StructureProviders : ObservableCollection<StructureProvider>
        {
            #region Fields
            /// <summary>
            /// 
            /// </summary>
            private bool _isExpanded;

            /// <summary>
            /// 
            /// </summary>
            private string _name;

            /// <summary>
            /// 
            /// </summary>
            private MetadataAddItemWindow _window;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// 
            /// </summary>
            /// <param name="name"></param>
            /// <param name="window"></param>
            public StructureProviders(string name, MetadataAddItemWindow window)
            {
                this._name = name;
                this._window = window;
                ICollectionView view = CollectionViewSource.GetDefaultView(this);
                if (view == null)
                {
                    return;
                }

                view.SortDescriptions.Clear();
                view.SortDescriptions.Add(new SortDescription("Name", ListSortDirection.Ascending));
            }
            #endregion Constructors

            #region Properties
            /// <summary>
            /// 
            /// </summary>
            public bool IsExpanded
            {
                get
                {
                    return this._isExpanded;
                }

                set
                {
                    this._isExpanded = value;
                    this.OnPropertyChanged(new PropertyChangedEventArgs("IsExpanded"));

                    if (value)
                    {
                        StructureProvider selected = this.GetSelectedProvider(null);
                        if (this.Count > 0 && selected == null)
                        {
                            this[0].IsSelected = true;
                            selected = this[0];
                        }

                        this._window.SetDefinitions(selected);
                    }
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public string Name
            {
                get { return this._name; }
            }
            #endregion Properties

            #region Methods
            /// <summary>
            /// 
            /// </summary>
            /// <param name="name"></param>
            /// <param name="parent"></param>
            /// <returns></returns>
            public StructureProvider GetOrCreateProvider(string name, StructureProvider parent)
            {
                ObservableCollection<StructureProvider> source = this;
                if (parent != null)
                {
                    source = parent.Children;
                }

                foreach (StructureProvider provider in source)
                {
                    if (String.Equals(provider.Name, name))
                    {
                        return provider;
                    }
                }

                {
                    StructureProvider provider = new StructureProvider(name, this._window);
                    source.Add(provider);
                    return provider;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="parent"></param>
            /// <returns></returns>
            private StructureProvider GetSelectedProvider(StructureProvider parent)
            {
                ObservableCollection<StructureProvider> source = this;
                if (parent != null)
                {
                    source = parent.Children;
                }

                foreach (StructureProvider provider in source)
                {
                    if (provider.IsSelected)
                    {
                        return provider;
                    }

                    StructureProvider child = this.GetSelectedProvider(provider);
                    if (child != null)
                    {
                        return child;
                    }
                }

                return null;
            }
            #endregion Methods
        } // MetadataAddItemWindow.StructureProviders {Class}
        #endregion Classes
    } // MetadataEditor.MetadataAddItemWindow {Class}
} // MetadataEditor {Namespace}
