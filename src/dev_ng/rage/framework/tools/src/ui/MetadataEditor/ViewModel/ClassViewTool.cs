﻿//---------------------------------------------------------------------------------------------
// <copyright file="ClassViewTool.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace MetadataEditor.ViewModel
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows;
    using System.Windows.Data;
    using MetadataEditor.Resources;
    using RSG.Editor;
    using RSG.Editor.Controls.Dock.ViewModel;
    using RSG.Metadata.ViewModel.ClassView;
    using RSG.Metadata.ViewModel.Project;
    using RSG.Project.ViewModel;

    /// <summary>
    /// Represents the class view tool window. This class cannot be inherited.
    /// </summary>
    internal sealed class ClassViewTool : ToolWindowItem
    {
        #region Fields
        /// <summary>
        /// The generic object that provides a synchronisation lock for this collection.
        /// </summary>
        private object _lock = new object();

        /// <summary>
        /// A collection of currently loaded projects.
        /// </summary>
        private ObservableCollection<ClassViewProjectNode> _projects;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ClassViewTool"/> class.
        /// </summary>
        /// <param name="collectionNode">
        /// The object that contains the loaded project collection.
        /// </param>
        /// <param name="parentPane">
        /// The parent docking pane.
        /// </param>
        public ClassViewTool(ProjectCollectionNode collectionNode, DockingPane parentPane)
            : base(parentPane, StringTable.ClassViewTitle)
        {
            this._projects = new ObservableCollection<ClassViewProjectNode>();
            collectionNode.ProjectLoaded += this.OnProjectLoaded;
            collectionNode.ProjectUnloaded += this.OnProjectUnloaded;
            collectionNode.LoadedStateChanged += this.OnCollectionLoadedChanged;

            MainWindow mainWindow = Application.Current.MainWindow as MainWindow;
            this.Content = mainWindow.TryFindResource("ClassView");
            mainWindow.Dispatcher.BeginInvoke(
            (Action)(() =>
            {
                BindingOperations.EnableCollectionSynchronization(this._projects, this._lock);
            }));
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the observable collection of projects that are currently loaded in the
        /// application.
        /// </summary>
        public ObservableCollection<ClassViewProjectNode> Projects
        {
            get { return this._projects; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Called whenever the attached collection is unloaded or loaded.
        /// </summary>
        /// <param name="sender">
        /// The collection this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The RSG.Editor.ValueChangedEventArgs{T} containing the event data.
        /// </param>
        private void OnCollectionLoadedChanged(object sender, ValueChangedEventArgs<bool> e)
        {
            if (!e.NewValue)
            {
                lock (this._projects)
                {
                    this._projects.Clear();
                }
            }
        }

        /// <summary>
        /// Called whenever a project is loaded in the attached collection.
        /// </summary>
        /// <param name="sender">
        /// The collection this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The RSG.Project.ViewModel.ProjectLoadedStateChangedEventArgs containing the event
        /// data.
        /// </param>
        private void OnProjectLoaded(object sender, ProjectLoadedStateChangedEventArgs e)
        {
            lock (this._projects)
            {
                this._projects.Add(new ClassViewProjectNode(e.Project as StandardProjectNode));
            }
        }

        /// <summary>
        /// Called whenever a project is unloaded or removed from the attached collection.
        /// </summary>
        /// <param name="sender">
        /// The collection this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The RSG.Project.ViewModel.ProjectLoadedStateChangedEventArgs containing the event
        /// data.
        /// </param>
        private void OnProjectUnloaded(object sender, ProjectLoadedStateChangedEventArgs e)
        {
            lock (this._projects)
            {
                foreach (ClassViewProjectNode project in this._projects)
                {
                    if (Object.ReferenceEquals(project.StandardProject, e.Project))
                    {
                        this._projects.Remove(project);
                        break;
                    }
                }
            }
        }
        #endregion Methods
    } // MetadataEditor.ViewModel.ClassViewTool {Class}
} // MetadataEditor.ViewModel {Namespace}
