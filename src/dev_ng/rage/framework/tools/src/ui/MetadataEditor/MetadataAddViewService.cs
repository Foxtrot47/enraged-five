﻿//---------------------------------------------------------------------------------------------
// <copyright file="MetadataAddViewService.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace MetadataEditor
{
    using System.IO;
    using System.Windows;
    using RSG.Metadata.ViewModel.Project;
    using RSG.Project.View;
    using RSG.Project.ViewModel;
    using RSG.Project.ViewModel.Definitions;

    /// <summary>
    /// Provides functionality to show the views specific to the project add commands.
    /// </summary>
    public class MetadataAddViewService : ProjectAddViewService
    {
        #region Methods
        /// <summary>
        /// Shows the new item selector dialog window.
        /// </summary>
        /// <param name="project">
        /// The project whose item definitions should be available to select.
        /// </param>
        /// <param name="initialLocation">
        /// The initial location for the new item.
        /// </param>
        /// <param name="initialName">
        /// The initial name for the new item.
        /// </param>
        /// <param name="selected">
        /// The item that the user selected, or null if the user has chosen to cancel.
        /// </param>
        /// <param name="fullPath">
        /// The full path where the user has chosen the new item to be located.
        /// </param>
        public override void ShowNewItemSelector(
            ProjectNode project,
            string initialLocation,
            string initialName,
            out ProjectItemDefinition selected,
            out string fullPath)
        {
            MetadataAddItemWindow window = new MetadataAddItemWindow();
            window.Owner = Application.Current.MainWindow;
            window.SetDefinition(project as StandardProjectNode);
            window.LocationValueTextBox.Text = initialLocation;
            if (initialName != null)
            {
                window.NameValueTextBox.Text = initialName;
            }

            if (window.ShowDialog() != true)
            {
                selected = null;
                fullPath = null;
                return;
            }

            selected = window.TemplateListBox.SelectedItem as ProjectItemDefinition;
            if (selected == null)
            {
                fullPath = null;
                return;
            }

            string name = window.NameValueTextBox.Text;
            if (!name.EndsWith(selected.Extension))
            {
                name += selected.Extension;
            }

            fullPath = Path.Combine(window.LocationValueTextBox.Text, name);
        }
        #endregion Methods
    } // MetadataEditor.MetadataAddViewService {Class}
} // MetadataEditor {Namespace}
