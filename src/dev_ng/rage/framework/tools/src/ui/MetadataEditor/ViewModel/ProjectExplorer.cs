﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProjectExplorer.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace MetadataEditor.ViewModel
{
    using System.Windows;
    using MetadataEditor.Resources;
    using RSG.Editor.Controls.Dock.ViewModel;
    using RSG.Project.ViewModel;

    /// <summary>
    /// Represents the project explorer tool window. This class cannot be inherited.
    /// </summary>
    internal sealed class ProjectExplorer : ToolWindowItem
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="CollectionNode"/> property.
        /// </summary>
        private ProjectCollectionNode _collectionNode;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ProjectExplorer"/> class.
        /// </summary>
        /// <param name="collectionNode">
        /// The object that contains the loaded project collection.
        /// </param>
        /// <param name="parentPane">
        /// The parent docking pane.
        /// </param>
        public ProjectExplorer(ProjectCollectionNode collectionNode, DockingPane parentPane)
            : base(parentPane, StringTable.ProjectExplorerTitle)
        {
            this._collectionNode = collectionNode;
            MainWindow mainWindow = Application.Current.MainWindow as MainWindow;
            this.Content = mainWindow.TryFindResource("Explorer");
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the collection node that represents the root of the project system.
        /// </summary>
        public ProjectCollectionNode CollectionNode
        {
            get { return this._collectionNode; }
        }
        #endregion Properties

        #region Methods
        #endregion Methods
    } // MetadataEditor.ViewModel.ProjectExplorer {Class}
} // MetadataEditor.ViewModel {Namespace}
