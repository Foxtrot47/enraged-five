﻿//---------------------------------------------------------------------------------------------
// <copyright file="App.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace MetadataEditor
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows;
    using MetadataEditor.Commands;
    using RSG.Editor;
    using RSG.Editor.Controls;
    using RSG.Editor.Controls.Dock.ViewModel;
    using RSG.Editor.Controls.Perforce;
    using RSG.Project.Commands;
    using RSG.Project.Model;
    using RSG.Project.View;
    using RSG.Project.ViewModel;
    using RSG.Project.ViewModel.Definitions;

    /// <summary>
    /// Defines the main entry point to the application and is responsible for creating and
    /// running the application.
    /// </summary>
    public class App : RsApplication
    {
        #region Fields
        /// <summary>
        /// The private reference to the perforce service that this application exposes to the
        /// command actions.
        /// </summary>
        private IPerforceService _perforceService;

        /// <summary>
        /// The private reference to the document manager service that this application exposes
        /// to the command actions.
        /// </summary>
        private IDocumentManagerService _documentManagerService;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="App"/> class.
        /// </summary>
        public App()
        {
            this._perforceService = new PerforceService();
            this._documentManagerService = new MetadataDocumentService(this);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a value indicating whether the layout for the main window get serialised on
        /// exit and deserialised during loading.
        /// </summary>
        protected override bool MakeLayoutPersistent
        {
            get { return true; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Main entry point into the application. The first thing to get called.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            App app = new App();
            app.ShutdownMode = ShutdownMode.OnMainWindowClose;
            RsApplication.OnEntry(app, ApplicationMode.Multiple);
        }

        /// <summary>
        /// Determines whether the specified windows can close without the need for user
        /// interaction. For example unsaved data.
        /// </summary>
        /// <param name="windows">
        /// The windows that are to be tested.
        /// </param>
        /// <returns>
        /// True if the specified windows can be closed without user interaction; otherwise,
        /// false.
        /// </returns>
        public override bool CanCloseWithoutInteraction(ISet<Window> windows)
        {
            return false;
        }

        /// <summary>
        /// Gets the service object of the specified type making sure that before the base is
        /// called the <see cref="RSG.Project.ViewModel.IProjectAddViewService"/> interface is
        /// handled.
        /// </summary>
        /// <param name="serviceType">
        /// An object that specifies the type of service object to get.
        /// </param>
        /// <returns>
        /// The service of the specified type if found; otherwise, null.
        /// </returns>
        public override object GetService(Type serviceType)
        {
            if (serviceType == typeof(ICommonDialogService))
            {
                return new MetadataCommonDialogService();
            }
            else if (serviceType == typeof(IProjectAddViewService))
            {
                return new MetadataAddViewService();
            }
            else if (serviceType == typeof(IPerforceService))
            {
                return this._perforceService;
            }
            else if (serviceType == typeof(IDocumentManagerService))
            {
                return this._documentManagerService;
            }

            return base.GetService(serviceType);
        }

        /// <summary>
        /// Handles the specified windows being closed. This is called after the method
        /// <see cref="CanCloseWithoutInteraction"/> returns false.
        /// </summary>
        /// <param name="windows">
        /// The windows to handle.
        /// </param>
        /// <param name="e">
        /// A instance of CancelEventArgs that can be used to cancel the closing of the
        /// specified windows.
        /// </param>
        public override void HandleWindowsClosing(ISet<Window> windows, CancelEventArgs e)
        {
            MainWindow window = this.MainWindow as MainWindow;
            if (window == null)
            {
                return;
            }

            if (!windows.Contains(window))
            {
                return;
            }

            try
            {
                CloseCollectionAction action = new CloseCollectionAction(this);
                action.Execute(window.CommandResolver(null));
            }
            catch (OperationCanceledException)
            {
                e.Cancel = true;
                return;
            }
        }

        /// <summary>
        /// Sets the backup manager for the document manager service.
        /// </summary>
        /// <param name="backupManager">
        /// The instance of the backup manager to use.
        /// </param>
        internal void SetBackupManager(FileBackupManager backupManager)
        {
            this._documentManagerService.SetBackupManager(backupManager);
        }

        /// <summary>
        /// Sets the file watcher for the document manager service.
        /// </summary>
        /// <param name="fileWatcher">
        /// The instance of the file watcher to use.
        /// </param>
        internal void SetFileWatcher(FileWatcherManager fileWatcher)
        {
            this._documentManagerService.SetFileWatcher(fileWatcher);
        }

        /// <summary>
        /// Sets the view site for the document manager service.
        /// </summary>
        /// <param name="viewSite">
        /// The instance of the viewSite site to use.
        /// </param>
        internal void SetViewSite(ViewSite viewSite)
        {
            this._documentManagerService.SetViewSite(viewSite);
        }

        /// <summary>
        /// Override to create the main window for the application.
        /// </summary>
        /// <returns>
        /// The System.Windows.Window that will be the main window for this application.
        /// </returns>
        protected override Window CreateMainWindow()
        {
            MainWindow mainWindow = new MainWindow(this);
            return mainWindow;
        }

        /// <summary>
        /// Called immediately after the main windows Show method has been called.
        /// </summary>
        /// <param name="mainWindow">
        /// A reference to the main window that was shown.
        /// </param>
        /// <returns>
        /// A task representing the work done by this method.
        /// </returns>
        protected async override Task OnMainWindowShown(Window mainWindow)
        {
            MainWindowDataContext dc = this.MainWindow.DataContext as MainWindowDataContext;
            if (dc == null)
            {
                Debug.Assert(
                    dc != null,
                    "Unable to complete startup operations as the data context is missing");
                return;
            }

            string commandline = null;
            await Task.Factory.StartNew(
                new Action(delegate
                    {
                        commandline = this.TrailingArguments.FirstOrDefault();
                        if (!File.Exists(commandline))
                        {
                            return;
                        }
                    }));

            string extension = Path.GetExtension(commandline);
            if (String.Equals(extension, ".metaprojs"))
            {
                dc.CollectionNode.Load(commandline);
                this.AddToMruList("Projects and Collections", commandline, new string[0]);
            }
            else if (String.Equals(extension, ".metaproj"))
            {
                ProjectDefinition selectedDefinition = null;
                StringComparison comparisonType = StringComparison.OrdinalIgnoreCase;
                ProjectCollectionNode collection = dc.CollectionNode;
                foreach (ProjectDefinition def in collection.Definition.ProjectDefinitions)
                {
                    if (String.Equals(def.Extension, extension, comparisonType))
                    {
                        selectedDefinition = def;
                        break;
                    }
                }

                ProjectItem newItem = collection.Model.AddNewProjectItem("Project", commandline);
                ProjectNode projectNode = selectedDefinition.CreateProjectNode();
                projectNode.SetParentAndModel(collection, newItem);
                collection.AddChild(projectNode);
                projectNode.Load();

                collection.IsModified = true;
                collection.Loaded = true;
                collection.IsTemporaryFile = true;
                collection.Name = Path.GetFileNameWithoutExtension(commandline);
                this.AddToMruList(
                    "Projects and Collections", commandline, new string[] { "Project" });
            }
        }
        #endregion Methods
    } // MetadataEditor.App {Class}
} // MetadataEditor {Namespace}
