﻿//---------------------------------------------------------------------------------------------
// <copyright file="StandardProjectWizard.xaml.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace MetadataEditor
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using RSG.Editor;
    using RSG.Editor.Controls;
    using RSG.Metadata.Model.Definitions;

    /// <summary>
    /// Contains the code behind the application main window control.
    /// </summary>
    public partial class StandardProjectWizard : RsWindow
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="StandardProjectWizard"/> class.
        /// </summary>
        public StandardProjectWizard()
        {
            this.InitializeComponent();
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Determines whether the
        /// <see cref="RSG.Editor.SharedCommands.RockstarViewCommands.ConfirmDialog"/> command
        /// can be executed at this level.
        /// </summary>
        /// <param name="e">
        /// The command data that is past from the command system.
        /// </param>
        /// <returns>
        /// True if the command can be executed here; otherwise, false.
        /// </returns>
        protected override bool CanConfirmDialog(CanExecuteCommandData e)
        {
            if (this.DirectoryTextBox == null)
            {
                return false;
            }

            if (String.IsNullOrWhiteSpace(this.DirectoryTextBox.Text))
            {
                return false;
            }

            string pattern = "\\A[a-zA-Z]:\\\\[^{0}]*\\z";
            pattern = String.Format(pattern, new String(Path.GetInvalidPathChars()));
            Regex regex = new Regex(pattern);
            if (!regex.IsMatch(this.DirectoryTextBox.Text))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Called when the browse button is pressed. Shows the select folder windows dialog
        /// for the user to select.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs containing the event data.
        /// </param>
        private void OnBrowseButtonClicked(object sender, RoutedEventArgs e)
        {
            string directory = null;
            Guid id = new Guid("4ECC5E6E-4A88-4521-B57A-7D58B8F152DA");

            RsSelectFolderDialog dlg = new RsSelectFolderDialog(id);
            dlg.Title = "Definitions Directory";

            bool? result = dlg.ShowDialog();
            if (result == true)
            {
                directory = dlg.FileName;
            }
            else
            {
                return;
            }

            this.DirectoryTextBox.Text = Path.GetFullPath(directory);
        }

        /// <summary>
        /// Called whenever the root definitions directory value changes.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs containing the event data.
        /// </param>
        private async void OnDirectoryChanged(object sender, TextChangedEventArgs e)
        {
            if (this.CountValues == null || this.LoadingIcon == null)
            {
                return;
            }

            this.CountValues.Visibility = Visibility.Collapsed;
            this.LoadingIcon.Visibility = Visibility.Visible;
            this.LoadingIcon.IsSpinning = true;
            await this.UpdateStatistics();
            this.LoadingIcon.Visibility = Visibility.Collapsed;
            this.CountValues.Visibility = Visibility.Visible;
            this.LoadingIcon.IsSpinning = false;
        }

        /// <summary>
        /// Called whenever the option to inlcude sub directories changes.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs containing the event data.
        /// </param>
        private async void OnIncludeSubDirectoriesChanged(object sender, RoutedEventArgs e)
        {
            if (this.CountValues == null || this.LoadingIcon == null)
            {
                return;
            }

            this.CountValues.Visibility = Visibility.Collapsed;
            this.LoadingIcon.Visibility = Visibility.Visible;
            this.LoadingIcon.IsSpinning = true;
            await this.UpdateStatistics();
            this.LoadingIcon.Visibility = Visibility.Collapsed;
            this.CountValues.Visibility = Visibility.Visible;
            this.LoadingIcon.IsSpinning = false;
        }

        /// <summary>
        /// Called whenever the refresh button is clicked. This updates the statistics from the
        /// current options.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs containing the event data.
        /// </param>
        private async void OnRefreshClicked(object sender, RoutedEventArgs e)
        {
            if (this.CountValues == null || this.LoadingIcon == null)
            {
                return;
            }

            this.CountValues.Visibility = Visibility.Collapsed;
            this.LoadingIcon.Visibility = Visibility.Visible;
            this.LoadingIcon.IsSpinning = true;
            await this.UpdateStatistics();
            this.LoadingIcon.Visibility = Visibility.Collapsed;
            this.CountValues.Visibility = Visibility.Visible;
            this.LoadingIcon.IsSpinning = false;
        }

        /// <summary>
        /// Updates the psc statistics using the current options.
        /// </summary>
        private async Task UpdateStatistics()
        {
            if (this.DirectoryTextBox == null)
            {
                return;
            }

            if (String.IsNullOrWhiteSpace(this.DirectoryTextBox.Text))
            {
                return;
            }

            string pattern = "\\A[a-zA-Z]:\\\\[^{0}]*\\z";
            pattern = String.Format(pattern, new String(Path.GetInvalidPathChars()));
            Regex regex = new Regex(pattern);
            if (!regex.IsMatch(this.DirectoryTextBox.Text))
            {
                return;
            }

            SearchOption option = SearchOption.AllDirectories;
            if (this.IncludeSubDirectories.IsChecked != true)
            {
                option = SearchOption.TopDirectoryOnly;
            }

            string path = this.DirectoryTextBox.Text;
            string[] files = Directory.GetFiles(path, "*.psc", option);

            System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
            DefinitionDictionary dictionary = new DefinitionDictionary();

            await Task.WhenAll(from f in files select dictionary.LoadFileAsync(f));

            sw.Stop();

            this.StructureCount.Text = dictionary.Structures.Count.ToString();
            this.EnumerationCount.Text = dictionary.Enumerations.Count.ToString();
            this.ConstantCount.Text = dictionary.Constants.Count.ToString();
            this.Time.Text = sw.ElapsedMilliseconds.ToString();
        }
        #endregion Methods
    } // MetadataEditor.StandardProjectWizard {Class}
} // MetadataEditor {Namespace}
