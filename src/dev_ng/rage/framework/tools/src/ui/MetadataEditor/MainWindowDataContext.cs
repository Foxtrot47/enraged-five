﻿//---------------------------------------------------------------------------------------------
// <copyright file="MainWindowDataContext.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace MetadataEditor
{
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Windows;
    using MetadataEditor.ItemDefinitions;
    using MetadataEditor.Resources;
    using MetadataEditor.ViewModel;
    using RSG.Editor;
    using RSG.Editor.Controls.Dock;
    using RSG.Editor.Controls.Dock.ViewModel;
    using RSG.Editor.Controls.Perforce;
    using RSG.Project.ViewModel;
    using RSG.Project.ViewModel.Definitions;

    /// <summary>
    /// Represents the data context that is attached to the main window for this application.
    /// This class cannot be inherited.
    /// </summary>
    internal sealed class MainWindowDataContext : NotifyPropertyChangedBase
    {
        #region Fields
        /// <summary>
        /// The private reference to the view model inside the class view tool window.
        /// </summary>
        private ClassViewTool _classView;

        /// <summary>
        /// The private field used for the <see cref="CollectionNode"/> property.
        /// </summary>
        private ProjectCollectionNode _collectionNode;

        /// <summary>
        /// The private reference to the tool window item that is representing the project
        /// explorer.
        /// </summary>
        private ProjectExplorer _projectExplorer;

        /// <summary>
        /// The private field used for the <see cref="ViewSite"/> property.
        /// </summary>
        private ViewSite _viewSite;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MainWindowDataContext"/> class.
        /// </summary>
        /// <param name="window">
        /// A reference to the main window that this class is being attached to.
        /// </param>
        public MainWindowDataContext(MainWindow window)
        {
            this.InitialiseProjectCollection();
            this.InitialiseViewSite();
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the collection node that represents the root of the project system.
        /// </summary>
        public ProjectCollectionNode CollectionNode
        {
            get { return this._collectionNode; }
        }

        /// <summary>
        /// Gets the view site that represents the root group of the docking manager in the
        /// main window.
        /// </summary>
        public ViewSite ViewSite
        {
            get { return this._viewSite; }
            private set { this.SetProperty(ref this._viewSite, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates the definitions that are used by the project manager.
        /// </summary>
        /// <returns>
        /// The project collection definition that this application uses.
        /// </returns>
        private ProjectCollectionDefinition InitialiseProjectDefinitions()
        {
            ProjectCollectionDefinition collection = new CollectionDefinition();
            return collection;
        }

        /// <summary>
        /// Creates the project collection using the metadata editor project item definitions.
        /// </summary>
        private void InitialiseProjectCollection()
        {
            ProjectCollectionDefinition definition = this.InitialiseProjectDefinitions();
            this._collectionNode = new ProjectCollectionNode(definition);
            this._collectionNode.PerforceService = new PerforceService();
        }

        /// <summary>
        /// Creates the main view site for the root group and adds the project explorer and the
        /// document pane to it.
        /// </summary>
        private void InitialiseViewSite()
        {
            this._viewSite = ViewSite.CreateMainViewSite();
            ToolWindowPane pane = new ToolWindowPane(this._viewSite);
            this._classView = new ClassViewTool(this._collectionNode, pane);
            this._projectExplorer = new ProjectExplorer(this._collectionNode, pane);

            pane.SplitterLength = new SplitterLength(2, SplitterUnitType.Stretch);
            pane.InsertChild(0, this._projectExplorer);
            pane.InsertChild(1, this._classView);

            DocumentPane documentPane = new DocumentPane(this._viewSite);
            documentPane.SplitterLength = new SplitterLength(7, SplitterUnitType.Stretch);

            this._viewSite.RootGroup.InsertChild(0, pane);
            this._viewSite.RootGroup.InsertChild(1, documentPane);
        }
        #endregion Methods
    } // MetadataEditor.MainWindowDataContext {Class}
} // MetadataEditor {Namespace}
