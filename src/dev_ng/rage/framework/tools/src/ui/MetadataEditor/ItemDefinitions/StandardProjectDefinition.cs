﻿//---------------------------------------------------------------------------------------------
// <copyright file="StandardProjectDefinition.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace MetadataEditor.ItemDefinitions
{
    using System;
    using System.IO;
    using System.Reflection;
    using System.Text;
    using System.Windows;
    using System.Windows.Media.Imaging;
    using MetadataEditor.Resources;
    using RSG.Interop.Microsoft.Windows;
    using RSG.Metadata.ViewModel.Project;
    using RSG.Project.ViewModel;
    using RSG.Project.ViewModel.Definitions;

    /// <summary>
    /// Represents the definition for a standard project in the metadata editor. This project
    /// allows the user to add and remove files as they see fit and saves all items using a
    /// relative path from the project file. This class cannot be inherited.
    /// </summary>
    internal sealed class StandardProjectDefinition : ProjectDefinition
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="StandardProjectDefinition"/> class.
        /// </summary>
        public StandardProjectDefinition()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the description for this defined project.
        /// </summary>
        public override string Description
        {
            get { return StringTable.StandardProjectDescription; }
        }

        /// <summary>
        /// Gets the name that is used as the default value for a new instance of this
        /// definition.
        /// </summary>
        public override string DefaultName
        {
            get { return StringTable.StandardProjectDefaultName; }
        }

        /// <summary>
        /// Gets the extension used for this defined project.
        /// </summary>
        public override string Extension
        {
            get { return ".metaproj"; }
        }

        /// <summary>
        /// Gets the string used as the filter in a open or save dialog for this defined
        /// project.
        /// </summary>
        public override string Filter
        {
            get { return StringTable.StandardProjectFilter; }
        }

        /// <summary>
        /// Gets the icon that is used to display this project inside the add new project
        /// dialog window.
        /// </summary>
        public override BitmapSource Icon
        {
            get { return Icons.StandardProjectIcon; }
        }

        /// <summary>
        /// Gets the name for this defined project.
        /// </summary>
        public override string Name
        {
            get { return StringTable.StandardProjectName; }
        }

        /// <summary>
        /// Gets the filter to use in the Add Existing Item dialog window for this project.
        /// </summary>
        public override string NewItemFilter
        {
            get { return StringTable.NewItemFilterFilter; }
        }

        /// <summary>
        /// Gets a byte array containing the data that is used to create a new instance of the
        /// defined project.
        /// </summary>
        public override byte[] Template
        {
            get
            {
                byte[] buffer = new byte[0];
                Assembly assembly = Assembly.GetExecutingAssembly();
                string name = "MetadataEditor.ItemDefinitions.StandardProjectTemplate.xml";
                using (Stream stream = assembly.GetManifestResourceStream(name))
                {
                    buffer = new byte[stream.Length];
                    stream.Read(buffer, 0, buffer.Length);
                }

                return buffer;
            }
        }

        /// <summary>
        /// Gets the type of this item to be displayed in the add dialog.
        /// </summary>
        public override string Type
        {
            get { return StringTable.StandardProjectType; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates the project node that represents a standard metadata project inside the
        /// project explorer.
        /// </summary>
        /// <returns>
        /// A new <see cref="ProjectNode"/> object that represents a standard metadata project
        /// inside the project explorer.
        /// </returns>
        public override ProjectNode CreateProjectNode()
        {
            return new StandardProjectNode(this);
        }

        /// <summary>
        /// Gives the definition a opportunity to run a wizard before the project is created so
        /// that the user can set certain properties into the template before the file gets
        /// saved and replace all other variables.
        /// </summary>
        /// <param name="template">
        /// The template that is being used to create the project.
        /// </param>
        /// <param name="fullPath">
        /// The fullPath the new project is being saved to. So that relative paths can be
        /// determined.
        /// </param>
        /// <returns>
        /// A value indicating whether the wizard finished successfully. If false is returned
        /// the creation process is cancelled.
        /// </returns>
        /// <remarks>
        /// This method should modify the specified template with any additional properties
        /// from the wizard and replaced variables.
        /// </remarks>
        public override bool ReplaceVariables(ref byte[] template, string fullPath)
        {
            string text = Encoding.UTF8.GetString(template);

            text = text.Replace("$(*NewGuid)", Guid.NewGuid().ToString("D"));
            text = text.Replace("$(Name)", Path.GetFileNameWithoutExtension(fullPath));

            StandardProjectWizard wizard = new StandardProjectWizard();
            wizard.Owner = Application.Current.MainWindow;
            if (wizard.ShowDialog() != true)
            {
                return false;
            }

            string directory = wizard.DirectoryTextBox.Text;
            string root = Shlwapi.MakeRelativeFromFileToDirectory(fullPath, directory);
            text = text.Replace("$(DefinitionRoot)", root);

            string includeSubdirectories = "True";
            if (wizard.IncludeSubDirectories.IsChecked != true)
            {
                includeSubdirectories = "False";
            }

            text = text.Replace("$(IncludeSubDirectories)", includeSubdirectories);
            template = Encoding.UTF8.GetBytes(text);
            return true;
        }
        #endregion Methods
    } // MetadataEditor.ItemDefinitions.StandardProjectDefinition {Class}
} // MetadataEditor.ItemDefinitions {Namespace}
