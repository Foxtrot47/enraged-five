﻿//---------------------------------------------------------------------------------------------
// <copyright file="CollectionDefinition.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace MetadataEditor.ItemDefinitions
{
    using RSG.Project.ViewModel.Definitions;
    using MetadataEditor.Resources;

    /// <summary>
    /// Represents the definition for the metadata editor project collection. This class cannot
    /// be inherited.
    /// </summary>
    internal sealed class CollectionDefinition : ProjectCollectionDefinition
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CollectionDefinition"/> class.
        /// </summary>
        public CollectionDefinition()
        {
            this.AddProjectDefinition(new StandardProjectDefinition());
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the extension used for this defined collection.
        /// </summary>
        public override string Extension
        {
            get { return StringTable.CollectionExtension; }
        }

        /// <summary>
        /// Gets the string used as the filter in a open or save dialog for this defined
        /// collection.
        /// </summary>
        public override string Filter
        {
            get { return StringTable.CollectionFilter; }
        }
        #endregion Properties
    } // MetadataEditor.ItemDefinitions.MainWindowDataContext {Class}
} // MetadataEditor.ItemDefinitions {Namespace}
