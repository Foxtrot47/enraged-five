//
// File: iToolSettings.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of iToolSettings.cs class
//

using System;
using System.Collections.Generic;
using System.Text;

namespace ToolInterface
{

    /// <summary>
    /// Tool Settings (abstract base class)
    /// </summary>
    public abstract class iToolSettings : global::System.Configuration.ApplicationSettingsBase
    {
        #region Properties
        public abstract String Name { get; }
        #endregion // Properties
    }

} // End of ToolInterface namespace

// End of file
