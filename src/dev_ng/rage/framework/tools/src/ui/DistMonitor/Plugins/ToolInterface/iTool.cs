//
// File: iTool.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of iTool.cs class
//

using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace ToolInterface
{

    #region Enumerations
    /// <summary>
    /// Global Tool Status
    /// </summary>
    public enum etToolStatus
    {
        Idle,
        Running,
        Finished
    }
    #endregion // Enumerations

    /// <summary>
    /// Abstract Tool Interface
    /// </summary>
    public interface iTool
    {
        #region Events
        /// <summary>
        /// Event raised when tool want to display an alert message
        /// </summary>
        event EventHandler<cToolMessageEventArgs> ToolAlertMessageEvent;

        /// <summary>
        /// Event raised when tool is finished
        /// </summary>
        event EventHandler<cToolEventArgs> ToolFinishedEvent;
        #endregion // Events

        #region Properties
        /// <summary>
        /// Tool Name (used for UI display)
        /// </summary>
        String Name { get; }

        /// <summary>
        /// Label name to sync to.
        /// </summary>
        String SyncLabel { get; set; }

        /// <summary>
        /// Do we sync to p4?
        /// </summary>
        bool Sync { get; set; }

        /// <summary>
        /// The tools ouptut folder
        /// </summary>
        String OutputFolderName { get; set; }

        /// <summary>
        /// Update the webserver?
        /// </summary>
        bool UpdateWebserver { get; set; }
      
        /// <summary>
        /// Tool status (used for monitoring tool)
        /// </summary>
        etToolStatus Status { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Initialise User-Interface components
        /// </summary>
        /// The Control returned should be suitable for display on a 
        /// tabbed control.  This will limit the user-visible area and
        /// particularly suits ListView-like controls.
        /// <returns>Control (placed on tab)</returns>
        /// 
        Control InitialiseUI(bool defaults);
        
        /// <summary>
        /// Initialise Tool for Machine
        /// </summary>
        /// <param name="machine"></param>
        void Initialise(MachineInterface.iMachine machine);

        /// <summary>
        /// Start Tool for Machine
        /// </summary>
        /// <param name="machine"></param>
        void Start(GameInterface.iGame game, GameInterface.cConfiguration config, MachineInterface.iMachine machine);

        /// <summary>
        /// Global Monitor Tool method
        /// </summary>
        /// This is run in a separate tool monitor thread from the machine 
        /// monitor threads.  This was initially added to make tool-wide
        /// decisions such as completion status but it could be used as
        /// a more generic tool-wide feature.
        void Monitor();

        /// <summary>
        /// Monitor Tool for Machine
        /// </summary>
        /// <param name="machine"></param>
        void Monitor(MachineInterface.iMachine machine);
        #endregion // Methods

        #region Event Handlers
        /// <summary>
        /// Pre-tool Execution Event Handler
        /// </summary>
        void PreEvent();

        /// <summary>
        /// Post-tool Execution Event Handler
        /// </summary>
        void PostEvent();
        #endregion // Event Handlers
    }

    #region Event Argument Classes
    /// <summary>
    /// Tool Event Argument Class
    /// </summary>
    public sealed class cToolEventArgs : EventArgs
    {
        #region Properties and Associated Member Data
        public iTool Tool
        {
            get { return m_Tool; }
        }
        private iTool m_Tool;
        #endregion // Properties and Associated Member Data

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="tool"></param>
        public cToolEventArgs(iTool tool)
        {
            m_Tool = tool;
        }
        #endregion // Constructor
    }

    /// <summary>
    /// Tool Message Event Argument Class
    /// </summary>
    public sealed class cToolMessageEventArgs : EventArgs
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Message string
        /// </summary>
        public String Message
        {
            get { return m_sMessage; }
            private set { m_sMessage = value; }
        }
        private String m_sMessage;
        #endregion // Properties and Associated Member Data

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="message">Message string</param>
        public cToolMessageEventArgs(String message)
        {
            this.Message = message;
        }
        #endregion // Constructor
    };
    #endregion // Event Argument Classes

} // End of ToolInterface namespace

// End of file
