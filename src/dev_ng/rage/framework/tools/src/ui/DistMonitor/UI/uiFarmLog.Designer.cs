namespace DistMonitor.UI
{
    partial class uiFarmLog
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabCtrlLogs = new System.Windows.Forms.TabControl();
            this.tabAppLog = new System.Windows.Forms.TabPage();
            this.tabThreads = new System.Windows.Forms.TabPage();
            this.uiThreadPoolView = new RSG.Base.Threading.uiThreadPoolView();
            this.tabScheduler = new System.Windows.Forms.TabPage();
            this.Log = new RSG.Base.Logging.uiApplicationLogger();
            this.SchedulerLog = new RSG.Base.Logging.uiGenericLogger();
            this.tabCtrlLogs.SuspendLayout();
            this.tabAppLog.SuspendLayout();
            this.tabThreads.SuspendLayout();
            this.tabScheduler.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabCtrlLogs
            // 
            this.tabCtrlLogs.Controls.Add(this.tabAppLog);
            this.tabCtrlLogs.Controls.Add(this.tabThreads);
            this.tabCtrlLogs.Controls.Add(this.tabScheduler);
            this.tabCtrlLogs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabCtrlLogs.Location = new System.Drawing.Point(0, 0);
            this.tabCtrlLogs.Name = "tabCtrlLogs";
            this.tabCtrlLogs.SelectedIndex = 0;
            this.tabCtrlLogs.Size = new System.Drawing.Size(523, 150);
            this.tabCtrlLogs.TabIndex = 1;
            // 
            // tabAppLog
            // 
            this.tabAppLog.Controls.Add(this.Log);
            this.tabAppLog.Location = new System.Drawing.Point(4, 22);
            this.tabAppLog.Name = "tabAppLog";
            this.tabAppLog.Padding = new System.Windows.Forms.Padding(3);
            this.tabAppLog.Size = new System.Drawing.Size(515, 124);
            this.tabAppLog.TabIndex = 0;
            this.tabAppLog.Text = "Application Log";
            this.tabAppLog.UseVisualStyleBackColor = true;
            // 
            // tabThreads
            // 
            this.tabThreads.Controls.Add(this.uiThreadPoolView);
            this.tabThreads.Location = new System.Drawing.Point(4, 22);
            this.tabThreads.Name = "tabThreads";
            this.tabThreads.Padding = new System.Windows.Forms.Padding(3);
            this.tabThreads.Size = new System.Drawing.Size(515, 124);
            this.tabThreads.TabIndex = 1;
            this.tabThreads.Text = "Threads";
            this.tabThreads.UseVisualStyleBackColor = true;
            // 
            // uiThreadPoolView
            // 
            this.uiThreadPoolView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiThreadPoolView.Interval = false;
            this.uiThreadPoolView.Location = new System.Drawing.Point(3, 3);
            this.uiThreadPoolView.Monitoring = false;
            this.uiThreadPoolView.Name = "uiThreadPoolView";
            this.uiThreadPoolView.Size = new System.Drawing.Size(509, 118);
            this.uiThreadPoolView.TabIndex = 0;
            // 
            // tabScheduler
            // 
            this.tabScheduler.Controls.Add(this.SchedulerLog);
            this.tabScheduler.Location = new System.Drawing.Point(4, 22);
            this.tabScheduler.Name = "tabScheduler";
            this.tabScheduler.Padding = new System.Windows.Forms.Padding(3);
            this.tabScheduler.Size = new System.Drawing.Size(515, 124);
            this.tabScheduler.TabIndex = 2;
            this.tabScheduler.Text = "Scheduler Log";
            this.tabScheduler.UseVisualStyleBackColor = true;
            // 
            // Log
            // 
            this.Log.DebugColour = System.Drawing.Color.Empty;
            this.Log.DebugPrefix = "Debug: ";
            this.Log.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Log.ErrorColour = System.Drawing.Color.Empty;
            this.Log.ErrorPrefix = "Error: ";
            this.Log.Location = new System.Drawing.Point(3, 3);
            this.Log.Name = "Log";
            this.Log.Size = new System.Drawing.Size(509, 118);
            this.Log.TabIndex = 1;
            this.Log.Text = "";
            // 
            // SchedulerLog
            // 
            this.SchedulerLog.DebugColour = System.Drawing.Color.Empty;
            this.SchedulerLog.DebugPrefix = "Debug: ";
            this.SchedulerLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SchedulerLog.ErrorColour = System.Drawing.Color.Empty;
            this.SchedulerLog.ErrorPrefix = "Error: ";
            this.SchedulerLog.Location = new System.Drawing.Point(3, 3);
            this.SchedulerLog.Name = "SchedulerLog";
            this.SchedulerLog.Size = new System.Drawing.Size(509, 118);
            this.SchedulerLog.TabIndex = 0;
            this.SchedulerLog.Text = "";
            // 
            // uiFarmLog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabCtrlLogs);
            this.Name = "uiFarmLog";
            this.Size = new System.Drawing.Size(523, 150);
            this.tabCtrlLogs.ResumeLayout(false);
            this.tabAppLog.ResumeLayout(false);
            this.tabThreads.ResumeLayout(false);
            this.tabScheduler.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabCtrlLogs;
        private System.Windows.Forms.TabPage tabAppLog;
        private System.Windows.Forms.TabPage tabThreads;
        private RSG.Base.Threading.uiThreadPoolView uiThreadPoolView;
        private System.Windows.Forms.TabPage tabScheduler;
        private RSG.Base.Logging.uiApplicationLogger Log;
        public RSG.Base.Logging.uiGenericLogger SchedulerLog;
    }
}
