//
// File: uiGameConfigControl.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of uiGameConfigControl.cs class
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace DistMonitor.UI
{

    #region Game Config Control
    internal partial class uiGameConfigControl : ToolStrip
    {
        #region Events
        /// <summary>
        /// Event raised when a new Game, Configuration or Area Tool is selected.
        /// </summary>
        public event EventHandler<uiGameConfigSelectionEventArgs> OnGameConfigSelectionEvent;
        #endregion

        #region Properties and Associated Member Data
        /// <summary>
        /// Game List we are viewing
        /// </summary>
        public cGameList GameList
        {
            get { return m_GameList; }
            set { m_GameList = value; Repopulate(); }
        }
        private cGameList m_GameList;
        #endregion

        #region Private Member Data
        ToolStripComboBox m_cbGame;
        ToolStripComboBox m_cbConfig;
        ToolStripComboBox m_cbTool;
        #endregion

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public uiGameConfigControl()
        {
            InitializeComponent();
            m_cbGame = new ToolStripComboBox();
            m_cbConfig = new ToolStripComboBox();
            m_cbTool = new ToolStripComboBox();
            
            m_cbGame.DropDownStyle = ComboBoxStyle.DropDownList;
            m_cbGame.SelectedIndexChanged += ComboBoxChanged;

            m_cbConfig.DropDownStyle = ComboBoxStyle.DropDownList;
            m_cbConfig.AutoSize = false;
            m_cbConfig.SelectedIndexChanged += ConfigComboChanged;
            
            m_cbTool.DropDownStyle = ComboBoxStyle.DropDownList;
            m_cbTool.AutoSize = false;
            m_cbTool.SelectedIndexChanged += ToolComboChanged;

            Items.Add(new ToolStripLabel("Game:"));
            Items.Add(m_cbGame);
            Items.Add(new ToolStripLabel("Config:"));
            Items.Add(m_cbConfig);
            Items.Add(new ToolStripLabel("Tool:"));
            Items.Add(m_cbTool);
        }
        #endregion // Constructor

        #region Controller Methods
        /// <summary>
        /// Retrieve the current selected game
        /// </summary>
        /// <returns></returns>
        public GameInterface.iGame GetSelectedGame()
        {
            return (GameList.Games[m_cbGame.SelectedIndex]);
        }

        /// <summary>
        /// Retrieve the current selected game configuration
        /// </summary>
        /// <returns></returns>
        public GameInterface.cConfiguration GetSelectedConfiguration()
        {
            String conf = m_cbConfig.SelectedItem.ToString();
            foreach (GameInterface.cConfiguration config in GetSelectedGame().Configurations)
            {
                if (config.ToString() == conf)
                    return (config);
            }

            throw new NullReferenceException("No valid configuration found.");
        }

        /// <summary>
        /// Retrieve the current selected area tool
        /// </summary>
        /// <returns></returns>
        public ToolInterface.iTool GetSelectedAreaTool()
        {
            if (null != m_cbTool.SelectedItem)
            {
                String tool = m_cbTool.SelectedItem.ToString();
                foreach (Type toolType in Program.Controller.ToolPlugins.Plugins)
                {
                    ToolInterface.iTool tl = Program.Controller.ToolPlugins.CreatePluginInstance(toolType);
                    if (tl.Name == tool)
                        return tl;
                }
            }                
            return (null);
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Repopulate combo boxes content based on current game list
        /// </summary>
        private void Repopulate()
        {
            if (null == GameList)
            {
                m_cbGame.Items.Clear();
                m_cbConfig.Items.Clear();                
            }
            else
            {
                foreach (GameInterface.iGame game in GameList.Games)
                    m_cbGame.Items.Add(game.Name);
                m_cbGame.SelectedIndex = 0;
                Propagate();
                m_cbConfig.SelectedIndex = 0;
                m_cbTool.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// Propagate Game Combo Changes
        /// </summary>
        private void Propagate()
        {
            GameInterface.iGame game = GetSelectedGame();
            m_cbConfig.Items.Clear();
            m_cbTool.Items.Clear();

            int nLargest = 0;
            
            foreach (GameInterface.cConfiguration config in game.Configurations)
            {
                m_cbConfig.Items.Add(config.ToString());
                Size sz = TextRenderer.MeasureText(config.ToString(), m_cbConfig.Font);
                if (sz.Width > nLargest)
                    nLargest = sz.Width;
            }
            m_cbConfig.SelectedIndex = 0;
            m_cbConfig.Width = nLargest + 20;

            nLargest = 0;
            m_cbTool.Items.Add("None");
            foreach (Type toolType in Program.Controller.ToolPlugins.Plugins)
            {
                ToolInterface.iTool tool = Program.Controller.ToolPlugins.CreatePluginInstance(toolType);
                m_cbTool.Items.Add(tool.Name);
                Size sz = TextRenderer.MeasureText(tool.Name, m_cbTool.Font);
                if (sz.Width > nLargest)
                    nLargest = sz.Width;
            }
            m_cbTool.SelectedIndex = 0;

            m_cbTool.Width = nLargest + 20;

            RaiseGameConfigSelectionEvent();
        }

        /// <summary>
        /// Raise Game Config Selection Event (if there are any listeners)
        /// </summary>
        private void RaiseGameConfigSelectionEvent()
        {
            if (null != OnGameConfigSelectionEvent)
                OnGameConfigSelectionEvent(this,
                    new uiGameConfigSelectionEventArgs(GetSelectedGame(),
                                           GetSelectedConfiguration(),
                                           GetSelectedAreaTool()));
        }
        #endregion

        #region Event Handlers
        /// <summary>
        /// Event handler when game list changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void GameListChangeEvent(object sender, EventArgs e)
        {
            Repopulate();
            GetSelectedGame().ActiveConfiguration = GetSelectedConfiguration();
            
            RaiseGameConfigSelectionEvent();
        }

        /// <summary>
        /// Event handler for user changing a combobox selection
        /// </summary>
        /// <param name="sender">Event source</param>
        /// <param name="e">Arguments</param>
        public void ComboBoxChanged(object sender, EventArgs e)
        {
            Propagate();

            RaiseGameConfigSelectionEvent();
        }

        /// <summary>
        /// Event handler for Configuration Combobox changes
        /// </summary>
        /// <param name="sender">Event source</param>
        /// <param name="e">Arguments</param>
        public void ConfigComboChanged(object sender, EventArgs e)
        {
            GetSelectedGame().ActiveConfiguration = GetSelectedConfiguration();

            RaiseGameConfigSelectionEvent();
        }

        /// <summary>
        /// Event handler for Area Tool Combobox changes
        /// </summary>
        /// <param name="sender">Event source</param>
        /// <param name="e">Arguments</param>
        public void ToolComboChanged(object sender, EventArgs e)
        {
            RaiseGameConfigSelectionEvent();
        }
        #endregion // Event Handlers
    }
    #endregion // Game Config Control

    #region Event Argument Classes
    /// <summary>
    /// Game, Configuration, Area Tool Selection Event Arguments
    /// </summary>
    public class uiGameConfigSelectionEventArgs : EventArgs
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Selected Game
        /// </summary>
        public GameInterface.iGame Game
        {
            get { return m_Game; }
            private set { m_Game = value; }
        }
        private GameInterface.iGame m_Game;

        /// <summary>
        /// Selected Configuration
        /// </summary>
        public GameInterface.cConfiguration Configuration
        {
            get { return m_Configuration; }
            private set { m_Configuration = value; }
        }
        private GameInterface.cConfiguration m_Configuration;

        /// <summary>
        /// Selected Tool (can be null)
        /// </summary>
        public ToolInterface.iTool Tool
        {
            get { return m_Tool; }
            private set { m_Tool = value; }
        }
        private ToolInterface.iTool m_Tool;
        #endregion // Properties and Associated Member Data

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="game"></param>
        /// <param name="config"></param>
        /// <param name="tool"></param>
        public uiGameConfigSelectionEventArgs(GameInterface.iGame game, 
                                              GameInterface.cConfiguration config, 
                                              ToolInterface.iTool tool)
        {
            Game = game;
            Configuration = config;
            Tool = tool;
        }
        #endregion // Constructor
    };
    #endregion

} // End of DistMonitor.UI namespace

// End of file
