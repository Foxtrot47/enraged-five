namespace DistMonitor.Scheduler.UI
{
    partial class uiScheduledTaskProperties
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uiScheduledTaskProperties));
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.tblMainLayout = new System.Windows.Forms.TableLayoutPanel();
            this.dtStartDatePicker = new System.Windows.Forms.DateTimePicker();
            this.lblText1 = new System.Windows.Forms.Label();
            this.lblText2 = new System.Windows.Forms.Label();
            this.lblText3 = new System.Windows.Forms.Label();
            this.lblText4 = new System.Windows.Forms.Label();
            this.lblText6 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.cbGame = new System.Windows.Forms.ComboBox();
            this.cbConfiguration = new System.Windows.Forms.ComboBox();
            this.cbTool = new System.Windows.Forms.ComboBox();
            this.lblText7 = new System.Windows.Forms.Label();
            this.lblText8 = new System.Windows.Forms.Label();
            this.cbSchedule = new System.Windows.Forms.ComboBox();
            this.dtEndDatePicker = new System.Windows.Forms.DateTimePicker();
            this.tblLayoutButtons = new System.Windows.Forms.TableLayoutPanel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.tblMainLayout.SuspendLayout();
            this.tblLayoutButtons.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.Location = new System.Drawing.Point(0, 0);
            this.splitContainer.Name = "splitContainer";
            this.splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.tblMainLayout);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.tblLayoutButtons);
            this.splitContainer.Size = new System.Drawing.Size(391, 262);
            this.splitContainer.SplitterDistance = 231;
            this.splitContainer.TabIndex = 0;
            // 
            // tblMainLayout
            // 
            this.tblMainLayout.ColumnCount = 2;
            this.tblMainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.97087F));
            this.tblMainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 74.02913F));
            this.tblMainLayout.Controls.Add(this.dtStartDatePicker, 1, 4);
            this.tblMainLayout.Controls.Add(this.lblText1, 0, 0);
            this.tblMainLayout.Controls.Add(this.lblText2, 0, 1);
            this.tblMainLayout.Controls.Add(this.lblText3, 0, 2);
            this.tblMainLayout.Controls.Add(this.lblText4, 0, 3);
            this.tblMainLayout.Controls.Add(this.lblText6, 0, 4);
            this.tblMainLayout.Controls.Add(this.txtName, 1, 0);
            this.tblMainLayout.Controls.Add(this.cbGame, 1, 1);
            this.tblMainLayout.Controls.Add(this.cbConfiguration, 1, 2);
            this.tblMainLayout.Controls.Add(this.cbTool, 1, 3);
            this.tblMainLayout.Controls.Add(this.lblText7, 0, 6);
            this.tblMainLayout.Controls.Add(this.lblText8, 0, 5);
            this.tblMainLayout.Controls.Add(this.cbSchedule, 1, 6);
            this.tblMainLayout.Controls.Add(this.dtEndDatePicker, 1, 5);
            this.tblMainLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblMainLayout.Location = new System.Drawing.Point(0, 0);
            this.tblMainLayout.Name = "tblMainLayout";
            this.tblMainLayout.RowCount = 7;
            this.tblMainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tblMainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tblMainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tblMainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tblMainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tblMainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tblMainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tblMainLayout.Size = new System.Drawing.Size(391, 231);
            this.tblMainLayout.TabIndex = 0;
            // 
            // dtStartDatePicker
            // 
            this.dtStartDatePicker.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.dtStartDatePicker.CustomFormat = "dd MMMM yyyy \'at\' HH:mm";
            this.dtStartDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtStartDatePicker.Location = new System.Drawing.Point(104, 138);
            this.dtStartDatePicker.Name = "dtStartDatePicker";
            this.dtStartDatePicker.Size = new System.Drawing.Size(278, 20);
            this.dtStartDatePicker.TabIndex = 13;
            // 
            // lblText1
            // 
            this.lblText1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblText1.AutoSize = true;
            this.lblText1.Location = new System.Drawing.Point(33, 10);
            this.lblText1.Name = "lblText1";
            this.lblText1.Size = new System.Drawing.Size(65, 13);
            this.lblText1.TabIndex = 0;
            this.lblText1.Text = "Task Name:";
            // 
            // lblText2
            // 
            this.lblText2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblText2.AutoSize = true;
            this.lblText2.Location = new System.Drawing.Point(60, 43);
            this.lblText2.Name = "lblText2";
            this.lblText2.Size = new System.Drawing.Size(38, 13);
            this.lblText2.TabIndex = 1;
            this.lblText2.Text = "Game:";
            // 
            // lblText3
            // 
            this.lblText3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblText3.AutoSize = true;
            this.lblText3.Location = new System.Drawing.Point(26, 76);
            this.lblText3.Name = "lblText3";
            this.lblText3.Size = new System.Drawing.Size(72, 13);
            this.lblText3.TabIndex = 2;
            this.lblText3.Text = "Configuration:";
            // 
            // lblText4
            // 
            this.lblText4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblText4.AutoSize = true;
            this.lblText4.Location = new System.Drawing.Point(67, 109);
            this.lblText4.Name = "lblText4";
            this.lblText4.Size = new System.Drawing.Size(31, 13);
            this.lblText4.TabIndex = 3;
            this.lblText4.Text = "Tool:";
            // 
            // lblText6
            // 
            this.lblText6.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblText6.AutoSize = true;
            this.lblText6.Location = new System.Drawing.Point(6, 142);
            this.lblText6.Name = "lblText6";
            this.lblText6.Size = new System.Drawing.Size(92, 13);
            this.lblText6.TabIndex = 5;
            this.lblText6.Text = "Start Date / Time:";
            // 
            // txtName
            // 
            this.txtName.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtName.Location = new System.Drawing.Point(104, 6);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(278, 20);
            this.txtName.TabIndex = 7;
            // 
            // cbGame
            // 
            this.cbGame.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cbGame.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbGame.FormattingEnabled = true;
            this.cbGame.Location = new System.Drawing.Point(104, 39);
            this.cbGame.Name = "cbGame";
            this.cbGame.Size = new System.Drawing.Size(278, 21);
            this.cbGame.TabIndex = 8;
            // 
            // cbConfiguration
            // 
            this.cbConfiguration.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cbConfiguration.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbConfiguration.FormattingEnabled = true;
            this.cbConfiguration.Location = new System.Drawing.Point(104, 72);
            this.cbConfiguration.Name = "cbConfiguration";
            this.cbConfiguration.Size = new System.Drawing.Size(278, 21);
            this.cbConfiguration.TabIndex = 9;
            // 
            // cbTool
            // 
            this.cbTool.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cbTool.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTool.FormattingEnabled = true;
            this.cbTool.Location = new System.Drawing.Point(104, 105);
            this.cbTool.Name = "cbTool";
            this.cbTool.Size = new System.Drawing.Size(278, 21);
            this.cbTool.TabIndex = 10;
            // 
            // lblText7
            // 
            this.lblText7.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblText7.AutoSize = true;
            this.lblText7.Location = new System.Drawing.Point(43, 208);
            this.lblText7.Name = "lblText7";
            this.lblText7.Size = new System.Drawing.Size(55, 13);
            this.lblText7.TabIndex = 6;
            this.lblText7.Text = "Schedule:";
            // 
            // label1
            // 
            this.lblText8.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblText8.AutoSize = true;
            this.lblText8.Location = new System.Drawing.Point(9, 175);
            this.lblText8.Name = "lblText8";
            this.lblText8.Size = new System.Drawing.Size(89, 13);
            this.lblText8.TabIndex = 14;
            this.lblText8.Text = "End Date / Time:";
            // 
            // cbSchedule
            // 
            this.cbSchedule.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cbSchedule.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSchedule.FormattingEnabled = true;
            this.cbSchedule.Items.AddRange(new object[] {
            "Weekly",
            "Daily",
            "BiDaily",          
            "Hourly",
            "Minutes5",
            "Once"});
            this.cbSchedule.Location = new System.Drawing.Point(104, 204);
            this.cbSchedule.Name = "cbSchedule";
            this.cbSchedule.Size = new System.Drawing.Size(278, 21);
            this.cbSchedule.TabIndex = 12;
            // 
            // dtEndDatePicker
            // 
            this.dtEndDatePicker.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.dtEndDatePicker.CustomFormat = "dd MMMM yyyy \'at\' HH:mm";
            this.dtEndDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtEndDatePicker.Location = new System.Drawing.Point(104, 171);
            this.dtEndDatePicker.Name = "dtEndDatePicker";
            this.dtEndDatePicker.Size = new System.Drawing.Size(278, 20);
            this.dtEndDatePicker.TabIndex = 15;
            // 
            // tblLayoutButtons
            // 
            this.tblLayoutButtons.ColumnCount = 3;
            this.tblLayoutButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblLayoutButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tblLayoutButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tblLayoutButtons.Controls.Add(this.btnCancel, 2, 0);
            this.tblLayoutButtons.Controls.Add(this.btnOK, 1, 0);
            this.tblLayoutButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblLayoutButtons.Location = new System.Drawing.Point(0, 0);
            this.tblLayoutButtons.Name = "tblLayoutButtons";
            this.tblLayoutButtons.RowCount = 1;
            this.tblLayoutButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblLayoutButtons.Size = new System.Drawing.Size(391, 27);
            this.tblLayoutButtons.TabIndex = 0;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(313, 3);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 20);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(232, 3);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 20);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // uiScheduledTaskProperties
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(391, 262);
            this.Controls.Add(this.splitContainer);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "uiScheduledTaskProperties";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Scheduled Task Properties";
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            this.splitContainer.ResumeLayout(false);
            this.tblMainLayout.ResumeLayout(false);
            this.tblMainLayout.PerformLayout();
            this.tblLayoutButtons.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.TableLayoutPanel tblLayoutButtons;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.TableLayoutPanel tblMainLayout;
        private System.Windows.Forms.Label lblText1;
        private System.Windows.Forms.Label lblText2;
        private System.Windows.Forms.Label lblText3;
        private System.Windows.Forms.Label lblText4;
        private System.Windows.Forms.Label lblText6;
        private System.Windows.Forms.Label lblText7;
        private System.Windows.Forms.Label lblText8;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.ComboBox cbGame;
        private System.Windows.Forms.ComboBox cbConfiguration;
        private System.Windows.Forms.ComboBox cbTool;
        private System.Windows.Forms.ComboBox cbSchedule;
        private System.Windows.Forms.DateTimePicker dtStartDatePicker;
        private System.Windows.Forms.DateTimePicker dtEndDatePicker;
    }
}