//
// File: uiPluginListDlg.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of uiPluginListDlg class
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DistMonitor.UI
{

    /// <summary>
    /// DistMonitor Plugin List Dialog
    /// </summary>
    public partial class uiPluginListDlg : Form
    {
        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public uiPluginListDlg()
        {
            InitializeComponent();

            if (!this.DesignMode)
                Initialise();
        }
        #endregion // Constructor

        #region Private Methods
        /// <summary>
        /// Initialisation Method (invoked in constructor when not in design mode)
        /// </summary>
        private void Initialise()
        {
            this.lvMachinePlugins.Plugins = Program.Controller.MachinePlugins.GetPluginList();
            this.lvToolPlugins.Plugins = Program.Controller.ToolPlugins.GetPluginList();
            this.lvToolSettingPlugins.Plugins = Program.Controller.ToolSettings.GetPluginList();
        }
        #endregion // Private Methods

        #region UI Event Handlers
        /// <summary>
        /// Close Button Click Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion // UI Event Handlers
    }

} // End of DistMonitor.UI namespace

// End of file
