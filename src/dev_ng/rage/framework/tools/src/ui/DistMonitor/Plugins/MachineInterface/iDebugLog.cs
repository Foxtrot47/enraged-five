//
// File: iDebugLog.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of iDebugLog.cs class
//

using System;
using System.Collections.Generic;
using System.Text;

namespace MachineInterface
{
    
    /// <summary>
    /// Abstract Debug Log Interface
    /// </summary>
    public interface iDebugLog
    {
        #region Interface Events
        /// <summary>
        /// Event raised when debug log echoes a debug message
        /// </summary>
        event EventHandler<cDebugLogTextEventArgs> DebugTextEvent;
        
        /// <summary>
        /// Event raised when debug log determines a new machine state
        /// </summary>
        event EventHandler<cMachineStatusEventArgs> MachineStatusChangeEvent;
        #endregion

        #region Interface Properties
        /// <summary>
        /// Connected flag
        /// </summary>
        bool Connected { get; }
        bool Enabled { get; set; }
        #endregion // Interface Properties

        #region Interface Methods
        /// <summary>
        /// Connect debug log to a specified machine
        /// </summary>
        /// <param name="machine"></param>
        void ConnectTo(iMachine machine);

        /// <summary>
        /// Disconnect from machine
        /// </summary>
        void Disconnect();
        #endregion // Interface Methods
    }

    /// <summary>
    /// Debug Log Text Event Arguments Class
    /// </summary>
    public class cDebugLogTextEventArgs : EventArgs
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Machine, source of the debug message
        /// </summary>
        public iMachine Machine
        {
            get { return m_Machine; }
            private set { m_Machine = value; }
        }
        private iMachine m_Machine;

        /// <summary>
        /// Debug text associated with event
        /// </summary>
        public String Text
        {
            get { return m_sText; }
            private set { m_sText = value; }
        }
        private String m_sText;
        #endregion // Properties and Associated Member Data

        #region Constructor
        /// <summary>
        /// Constructor, specifying debug text to log
        /// </summary>
        /// <param name="text"></param>
        public cDebugLogTextEventArgs(iMachine machine, String text)
        {
            Machine = machine;
            Text = text;
        }
        #endregion // Constructor
    };

} // End of MachineInterface namespace

// End of file
