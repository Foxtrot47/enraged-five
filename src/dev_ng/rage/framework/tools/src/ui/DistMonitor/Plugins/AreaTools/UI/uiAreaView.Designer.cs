namespace AreaTools.UI
{
    partial class uiAreaView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ListViewGroup listViewGroup1 = new System.Windows.Forms.ListViewGroup("Queued", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup2 = new System.Windows.Forms.ListViewGroup("In Progress", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup3 = new System.Windows.Forms.ListViewGroup("Completed", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup4 = new System.Windows.Forms.ListViewGroup("Shafted", System.Windows.Forms.HorizontalAlignment.Left);
            this.menuCtxStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miArea = new System.Windows.Forms.ToolStripMenuItem();
            this.miAreaMarkAsQueued = new System.Windows.Forms.ToolStripMenuItem();
            this.miAreaMarkAsFinished = new System.Windows.Forms.ToolStripMenuItem();
            this.miAreaSep1 = new System.Windows.Forms.ToolStripSeparator();
            this.miAreaMarkAsShafted = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.miShowGroups = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.miForceRefresh = new System.Windows.Forms.ToolStripMenuItem();
            this.tblMainLayout = new System.Windows.Forms.TableLayoutPanel();
            this.lvAreaWorkUnits = new System.Windows.Forms.ListView();
            this.hdrID = new System.Windows.Forms.ColumnHeader();
            this.hdrStart = new System.Windows.Forms.ColumnHeader();
            this.hdrEnd = new System.Windows.Forms.ColumnHeader();
            this.hdrState = new System.Windows.Forms.ColumnHeader();
            this.hdrAssignedMachine = new System.Windows.Forms.ColumnHeader();
            this.hdrStartTime = new System.Windows.Forms.ColumnHeader();
            this.hdrEndTime = new System.Windows.Forms.ColumnHeader();
            this.hdrTimeoutIn = new System.Windows.Forms.ColumnHeader();
            this.hdrTimeout = new System.Windows.Forms.ColumnHeader();
            this.tblProgressBar = new System.Windows.Forms.TableLayoutPanel();
            this.lblText1 = new System.Windows.Forms.Label();
            this.pbProgress = new System.Windows.Forms.ProgressBar();
            this.hdrExceptions = new System.Windows.Forms.ColumnHeader();
            this.menuCtxStrip.SuspendLayout();
            this.tblMainLayout.SuspendLayout();
            this.tblProgressBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuCtxStrip
            // 
            this.menuCtxStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miArea,
            this.toolStripSeparator1,
            this.miShowGroups,
            this.toolStripMenuItem1,
            this.miForceRefresh});
            this.menuCtxStrip.Name = "menuCtxStrip";
            this.menuCtxStrip.Size = new System.Drawing.Size(188, 82);
            this.menuCtxStrip.Opening += new System.ComponentModel.CancelEventHandler(this.menuCtxStrip_Opening);
            // 
            // miArea
            // 
            this.miArea.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miAreaMarkAsQueued,
            this.miAreaMarkAsFinished,
            this.miAreaSep1,
            this.miAreaMarkAsShafted});
            this.miArea.Name = "miArea";
            this.miArea.Size = new System.Drawing.Size(187, 22);
            this.miArea.Text = "&Area";
            // 
            // miAreaMarkAsQueued
            // 
            this.miAreaMarkAsQueued.Name = "miAreaMarkAsQueued";
            this.miAreaMarkAsQueued.Size = new System.Drawing.Size(164, 22);
            this.miAreaMarkAsQueued.Text = "Mark as &Queued";
            this.miAreaMarkAsQueued.Click += new System.EventHandler(this.miAreaMarkAsQueued_Click);
            // 
            // miAreaMarkAsFinished
            // 
            this.miAreaMarkAsFinished.Name = "miAreaMarkAsFinished";
            this.miAreaMarkAsFinished.Size = new System.Drawing.Size(164, 22);
            this.miAreaMarkAsFinished.Text = "Mark as &Finished";
            this.miAreaMarkAsFinished.Click += new System.EventHandler(this.miAreaMarkAsFinished_Click);
            // 
            // miAreaSep1
            // 
            this.miAreaSep1.Name = "miAreaSep1";
            this.miAreaSep1.Size = new System.Drawing.Size(161, 6);
            // 
            // miAreaMarkAsShafted
            // 
            this.miAreaMarkAsShafted.Name = "miAreaMarkAsShafted";
            this.miAreaMarkAsShafted.Size = new System.Drawing.Size(164, 22);
            this.miAreaMarkAsShafted.Text = "Mark as &Shafted";
            this.miAreaMarkAsShafted.Click += new System.EventHandler(this.miAreaMarkAsShafted_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(184, 6);
            // 
            // miShowGroups
            // 
            this.miShowGroups.Name = "miShowGroups";
            this.miShowGroups.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.G)));
            this.miShowGroups.Size = new System.Drawing.Size(187, 22);
            this.miShowGroups.Text = "Show &Groups";
            this.miShowGroups.Click += new System.EventHandler(this.miShowGroups_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(184, 6);
            // 
            // miForceRefresh
            // 
            this.miForceRefresh.Name = "miForceRefresh";
            this.miForceRefresh.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.miForceRefresh.Size = new System.Drawing.Size(187, 22);
            this.miForceRefresh.Text = "&Refresh";
            this.miForceRefresh.Click += new System.EventHandler(this.miRefresh_Click);
            // 
            // tblMainLayout
            // 
            this.tblMainLayout.ColumnCount = 1;
            this.tblMainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblMainLayout.Controls.Add(this.lvAreaWorkUnits, 0, 1);
            this.tblMainLayout.Controls.Add(this.tblProgressBar, 0, 0);
            this.tblMainLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblMainLayout.Location = new System.Drawing.Point(0, 0);
            this.tblMainLayout.Name = "tblMainLayout";
            this.tblMainLayout.RowCount = 2;
            this.tblMainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tblMainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblMainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tblMainLayout.Size = new System.Drawing.Size(982, 228);
            this.tblMainLayout.TabIndex = 1;
            // 
            // lvAreaWorkUnits
            // 
            this.lvAreaWorkUnits.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.hdrID,
            this.hdrStart,
            this.hdrEnd,
            this.hdrState,
            this.hdrAssignedMachine,
            this.hdrStartTime,
            this.hdrEndTime,
            this.hdrTimeoutIn,
            this.hdrTimeout,
            this.hdrExceptions});
            this.lvAreaWorkUnits.ContextMenuStrip = this.menuCtxStrip;
            this.lvAreaWorkUnits.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvAreaWorkUnits.FullRowSelect = true;
            listViewGroup1.Header = "Queued";
            listViewGroup1.Name = "grpQueued";
            listViewGroup2.Header = "In Progress";
            listViewGroup2.Name = "grpInProgress";
            listViewGroup3.Header = "Completed";
            listViewGroup3.Name = "grpCompleted";
            listViewGroup4.Header = "Shafted";
            listViewGroup4.Name = "grpShafted";
            this.lvAreaWorkUnits.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup1,
            listViewGroup2,
            listViewGroup3,
            listViewGroup4});
            this.lvAreaWorkUnits.Location = new System.Drawing.Point(3, 30);
            this.lvAreaWorkUnits.Name = "lvAreaWorkUnits";
            this.lvAreaWorkUnits.Size = new System.Drawing.Size(976, 195);
            this.lvAreaWorkUnits.TabIndex = 5;
            this.lvAreaWorkUnits.UseCompatibleStateImageBehavior = false;
            this.lvAreaWorkUnits.View = System.Windows.Forms.View.Details;
            // 
            // hdrID
            // 
            this.hdrID.Text = "ID";
            this.hdrID.Width = 40;
            // 
            // hdrStart
            // 
            this.hdrStart.Text = "Start";
            this.hdrStart.Width = 100;
            // 
            // hdrEnd
            // 
            this.hdrEnd.Text = "End";
            this.hdrEnd.Width = 100;
            // 
            // hdrState
            // 
            this.hdrState.Text = "State";
            this.hdrState.Width = 100;
            // 
            // hdrAssignedMachine
            // 
            this.hdrAssignedMachine.Text = "Assigned Machine";
            this.hdrAssignedMachine.Width = 130;
            // 
            // hdrStartTime
            // 
            this.hdrStartTime.Text = "Start Time";
            this.hdrStartTime.Width = 123;
            // 
            // hdrEndTime
            // 
            this.hdrEndTime.Text = "End Time";
            this.hdrEndTime.Width = 130;
            // 
            // hdrTimeoutIn
            // 
            this.hdrTimeoutIn.Text = "Timeout In";
            this.hdrTimeoutIn.Width = 89;
            // 
            // hdrTimeout
            // 
            this.hdrTimeout.Text = "Timeout Count";
            this.hdrTimeout.Width = 90;
            // 
            // tblProgressBar
            // 
            this.tblProgressBar.ColumnCount = 2;
            this.tblProgressBar.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tblProgressBar.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblProgressBar.Controls.Add(this.lblText1, 0, 0);
            this.tblProgressBar.Controls.Add(this.pbProgress, 1, 0);
            this.tblProgressBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblProgressBar.Location = new System.Drawing.Point(3, 3);
            this.tblProgressBar.Name = "tblProgressBar";
            this.tblProgressBar.RowCount = 1;
            this.tblProgressBar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblProgressBar.Size = new System.Drawing.Size(976, 21);
            this.tblProgressBar.TabIndex = 6;
            // 
            // lblText1
            // 
            this.lblText1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblText1.AutoSize = true;
            this.lblText1.Location = new System.Drawing.Point(19, 4);
            this.lblText1.Name = "lblText1";
            this.lblText1.Size = new System.Drawing.Size(78, 13);
            this.lblText1.TabIndex = 0;
            this.lblText1.Text = "Total Progress:";
            // 
            // pbProgress
            // 
            this.pbProgress.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbProgress.Location = new System.Drawing.Point(103, 3);
            this.pbProgress.Name = "pbProgress";
            this.pbProgress.Size = new System.Drawing.Size(870, 15);
            this.pbProgress.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.pbProgress.TabIndex = 1;
            // 
            // hdrExceptions
            // 
            this.hdrExceptions.Text = "Exceptions Count";
            this.hdrExceptions.Width = 100;
            // 
            // uiAreaView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tblMainLayout);
            this.Name = "uiAreaView";
            this.Size = new System.Drawing.Size(982, 228);
            this.menuCtxStrip.ResumeLayout(false);
            this.tblMainLayout.ResumeLayout(false);
            this.tblProgressBar.ResumeLayout(false);
            this.tblProgressBar.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip menuCtxStrip;
        private System.Windows.Forms.ToolStripMenuItem miShowGroups;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem miForceRefresh;
        private System.Windows.Forms.TableLayoutPanel tblMainLayout;
        private System.Windows.Forms.ToolStripMenuItem miArea;
        private System.Windows.Forms.ToolStripMenuItem miAreaMarkAsQueued;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem miAreaMarkAsFinished;
        private System.Windows.Forms.ListView lvAreaWorkUnits;
        private System.Windows.Forms.ColumnHeader hdrID;
        private System.Windows.Forms.ColumnHeader hdrStart;
        private System.Windows.Forms.ColumnHeader hdrEnd;
        private System.Windows.Forms.ColumnHeader hdrState;
        private System.Windows.Forms.ColumnHeader hdrAssignedMachine;
        private System.Windows.Forms.ColumnHeader hdrStartTime;
        private System.Windows.Forms.ColumnHeader hdrEndTime;
        private System.Windows.Forms.TableLayoutPanel tblProgressBar;
        private System.Windows.Forms.Label lblText1;
        private System.Windows.Forms.ProgressBar pbProgress;
        private System.Windows.Forms.ColumnHeader hdrTimeout;
        private System.Windows.Forms.ColumnHeader hdrTimeoutIn;
        private System.Windows.Forms.ToolStripSeparator miAreaSep1;
        private System.Windows.Forms.ToolStripMenuItem miAreaMarkAsShafted;
        private System.Windows.Forms.ColumnHeader hdrExceptions;
    }
}
