//
// File: iMachine.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of iMachine.cs class
//

using System;
using System.Drawing;
using System.Net;

namespace MachineInterface
{

    #region Enumerations
    /// <summary>
    /// Machine reboot type
    /// </summary>
    public enum etRebootType
    {
        Warm,       // Warm Reboot
        Cold        // Cold Reboot (typically takes much longer)
    }

    /// <summary>
    /// Machine State
    /// </summary>
    public enum etMachineStatus
    {
        Unknown,    // Machine status is unknown (default)
        Off,        // Machine is off (i.e. unreachable)
        Rebooting,  // Machine is rebooting
        On,         // Machine is powered on but not running a game title
        Running,    // Macchie is running a game title
        Exception   // Machine game title has caused an exception
    }
    #endregion // Enumerations

    #region Machine Interface
    /// <summary>
    /// Abstract Machine Interface
    /// </summary>
    /// For a machine to be used in a machine farm it must meet this interface.
    public interface iMachine
    {
        #region Events
        /// <summary>
        /// Event raised when machine's status changes
        /// </summary>
        event EventHandler<cMachineStatusEventArgs> MachineStatusChangeEvent;

        /// <summary>
        /// Event raised when machine's enabled status changes
        /// </summary>
        event EventHandler<cMachineEnabledEventArgs> MachineEnabledChangeEvent;
        #endregion // Events

        #region Properties
        /// <summary>
        /// Machine friendly name (for matching game builds)
        /// </summary>
        String Name { get; }

        /// <summary>
        /// Machine Architecture name (for matching stats output directories, no spaces!)
        /// </summary>
        String ArchName { get; }

        /// <summary>
        /// IP Address of Machine
        /// </summary>
        IPAddress IP { get; }

        /// <summary>
        /// User-friendly hostname of Machine
        /// </summary>
        String HostName { get; }

        /// <summary>
        /// Enabled status of Machine (ignored by farm if disabled)
        /// </summary>
        bool Enabled { get; set; }

        /// <summary>
        /// Available status of Machine (not assigned work if not unavailable)
        /// Machines become unavailable if they fail a lot
        /// </summary>
        bool Available { get; set; }

        /// <summary>
        /// Number of times the machine has failed in some way.
        /// </summary>
        Int32 Failures { get; set; }

        /// <summary>
        /// Has the machine has failed in some way.
        /// </summary>
        bool Failed { get; set; }

        /// <summary>
        /// Shunt other users off machine?.
        /// </summary>
        bool ForceDisconnectOnConnect { get; set; }

        /// <summary>
        /// Image bitmap to use when displaying machine
        /// </summary>
        Bitmap Bitmap { get; }

        /// <summary>
        /// Debug log associated with machine
        /// </summary>
        iDebugLog DebugLog { get; }

        /// <summary>
        /// Machine status
        /// </summary>
        etMachineStatus Status { get; }
        #endregion

        #region Controller Methods
        /// <summary>
        /// Single Machine Initialisation (enabled by default)
        /// </summary>
        /// <param name="ip">IPAddress of machine</param>
        void Initialise(IPAddress ip);

        /// <summary>
        /// Single Machine Initialisation (user-defined enabled status)
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="enabled"></param>
        void Initialise(IPAddress ip, bool enabled);

        /// <summary>
        /// Start game configuration on machine
        /// </summary>
        /// <param name="game"></param>
        /// <param name="config"></param>
        void Start(GameInterface.iGame game, 
                   GameInterface.cConfiguration config);
        
        /// <summary>
        /// Start game configuration on machine with additional user-specified arguments
        /// </summary>
        /// <param name="game"></param>
        /// <param name="config"></param>
        /// <param name="sArgs"></param>
        void Start(GameInterface.iGame game, 
                   GameInterface.cConfiguration config,
                   String sArgs);

        /// <summary>
        /// Connect to machine.
        /// </summary>
        void Connect();

        /// <summary>
        /// Disconnect from machine.
        /// </summary>
        void Disconnect();

        /// <summary>
        /// Warm Reboot machine.
        /// </summary>
        void Reboot();

        /// <summary>
        /// Reboot machine
        /// </summary>
        /// <param name="type">Reboot type (see etRebootType)</param>
        void Reboot(etRebootType type);

        /// <summary>
        /// Ping machine to determine if machine is available
        /// </summary>
        /// <returns>Latency in milliseconds (>=1000 timeout)</returns>
        long Ping();

        /// <summary>
        /// Add runtime parameters to game parameter string
        /// </summary>
        /// <param name="ip">Game param string</param>
        void AddRunTimeParams( ref String sArgs);
        #endregion

        #region Event Handlers
        /// <summary>
        /// Machine Monitor connects to this to update the Machine's Status state
        /// </summary>
        /// <param name="sender">Source of event (cMachineMonitor)</param>
        /// <param name="e">Event argument object</param>
        void OnMonitorMachineStateChange(Object sender, cMachineStatusEventArgs e);

        /// <summary>
        /// Machine Farm connects to this to update the Machine's Enabled state
        /// </summary>
        /// <param name="sender">Source of event (cMachineFarm)</param>
        /// <param name="e">Event argument object</param>
        void OnMachineEnabledChange(Object sender, cMachineEnabledEventArgs e);
        #endregion // Event Handlers
    }
    #endregion // Machine Interface

    #region Event Argument Classes
    /// <summary>
    /// Machine Status Event Arguments Class
    /// </summary>
    /// This is used to pass data between a secondary machine class and a 
    /// iMachine implementation class to update it's status information.
    /// E.g. used by cXbDebugLog's OnMachineStatusEvent.
    public class cMachineStatusEventArgs : EventArgs
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Machine associated with the event
        /// </summary>
        public iMachine Machine
        {
            get { return m_Machine; }
        }
        private iMachine m_Machine;

        /// <summary>
        /// New Machine Status
        /// </summary>
        public etMachineStatus Status
        {
            get { return m_Status; }
        }
        private etMachineStatus m_Status;
        #endregion // Properties and Associated Member Data

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public cMachineStatusEventArgs(iMachine machine, etMachineStatus status)
        {
            m_Machine = machine;
            m_Status = status;
        }
        #endregion // Constructor
    }

    /// <summary>
    /// Machine Enabled Event Arguments Class
    /// </summary>
    /// This is used to pass data between a secondary machine class and a 
    /// iMachine implementation class to update it's enabled information.
    public class cMachineEnabledEventArgs : EventArgs
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Machine associated with the event
        /// </summary>
        public iMachine Machine
        {
            get { return m_Machine; }
        }
        private iMachine m_Machine;

        /// <summary>
        /// New Machine Enabled
        /// </summary>
        public bool Enabled
        {
            get { return m_bEnabled; }
        }
        private bool m_bEnabled;
        #endregion // Properties and Associated Member Data

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public cMachineEnabledEventArgs(iMachine machine, bool enabled)
        {
            m_Machine = machine;
            m_bEnabled = enabled;
        }
        #endregion // Constructor
    }
    #endregion // Event Argument Classes

} // End of MachineInterface namespace

// End of file
