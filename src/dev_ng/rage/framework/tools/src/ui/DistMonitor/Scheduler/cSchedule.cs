//
// File: iSchedule.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of iSchedule.cs class
//

using System;
using System.Collections.Generic;
using System.Text;

namespace DistMonitor.Scheduler
{

    /// <summary>
    /// Schedule Class
    /// </summary>
    internal class cSchedule : IComparable
    {
        #region Enumerations
        /// <summary>
        /// Schedule Type
        /// </summary>
        public enum etType
        {
            Weekly,
            Daily,
            BiDaily,
            Hourly,
            Minutes5,
            Once,
        }
        #endregion // Enumerations

        #region Properties and Associated Member Data
        /// <summary>
        /// Last Schedule Run Time
        /// </summary>
        public DateTime LastTime 
        {
            get { return m_dtLastTime; } 
        }
        private DateTime m_dtLastTime;

        /// <summary>
        /// Next Schedule Run Time
        /// </summary>
        public DateTime NextTime
        {
            get { return m_dtNextTime; }
        }
        private DateTime m_dtNextTime;

        /// <summary>
        /// Schedule End Time
        /// </summary>
        public DateTime EndTime
        {
            get { return m_dtEndTime; }
        }
        private DateTime m_dtEndTime;

        /// <summary>
        /// Type
        /// </summary>
        public etType Type
        {
            get { return m_eType; }
        }
        private etType m_eType;
        #endregion // Properties and Associated Member Data

        #region Constructor
        /// <summary>
        /// Schedule Constructor
        /// </summary>
        /// <param name="name"></param>
        /// <param name="start"></param>
        /// <param name="type"></param>
        public cSchedule(etType type, DateTime start, DateTime last, DateTime end)
        {
            m_eType = type;
            m_dtNextTime = start;            
            m_dtEndTime = end;

            if (default(DateTime) != m_dtLastTime)
                this.CalculateNextInvokeTime();

            m_dtLastTime = last; // DW - I moved this - this is causing screwed up schedules not to run I think.
        }
        #endregion // Constructor

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        public void CalculateNextInvokeTime()
        {            
            m_dtLastTime = m_dtNextTime;
            
            // Calculate next invoke time in the future!  This ensure our
            // task is not attempted multiple times for no reason.
            switch (m_eType)
            {
                case etType.Weekly:
                    while (m_dtNextTime < DateTime.Now)
                    {
                        m_dtNextTime = m_dtNextTime.AddDays(7);                        
                    }

                    while (m_dtEndTime < DateTime.Now)
                    {
                        m_dtEndTime = m_dtEndTime.AddDays(7);
                    }
                    break;
                case etType.Daily:
                    while (m_dtNextTime < DateTime.Now)
                    {
                        m_dtNextTime = m_dtNextTime.AddDays(1);
                    }

                    while (m_dtEndTime < DateTime.Now)
                    {
                        m_dtEndTime = m_dtEndTime.AddDays(1);
                    }
                    break;
                case etType.Hourly:
                    while (m_dtNextTime < DateTime.Now)
                    {
                        m_dtNextTime = m_dtNextTime.AddHours(1);
                    }

                    while (m_dtEndTime < DateTime.Now)
                    {
                        m_dtEndTime = m_dtEndTime.AddHours(1);
                    }
                    break;
                case etType.BiDaily:
                    while (m_dtNextTime < DateTime.Now)
                    {
                        m_dtNextTime = m_dtNextTime.AddDays(2);
                    }

                    while (m_dtEndTime < DateTime.Now)
                    {
                        m_dtEndTime = m_dtEndTime.AddDays(2);
                    }
                    break;
                case etType.Minutes5:
                    while (m_dtNextTime < DateTime.Now)
                    {
                        m_dtNextTime = m_dtNextTime.AddMinutes(5);
                    }

                    while (m_dtEndTime < DateTime.Now)
                    {
                        m_dtEndTime = m_dtEndTime.AddMinutes(5);
                    }
                    break;

                case etType.Once:
                    // Ignore
                    break;
            }

        }
        #endregion // Controller Methods

        #region Object Overridden Methods
        /// <summary>
        /// Convert to string representation
        /// </summary>
        /// <returns>String representation of cSchedule object</returns>
        public override String ToString()
        {
            return (m_eType.ToString());
        }
        #endregion // Object Overridden Methods

        #region IComparable Interface Methods
        /// <summary>
        /// Compare two cSchedule objects
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(Object other)
        {
            if (other is cSchedule)
            {
                cSchedule schedule = (other as cSchedule);
                return (m_dtNextTime.CompareTo(schedule.NextTime));
            }

            throw new ArgumentException("Compared object must be cSchedule.");
        }
        #endregion // IComparable Interface Methods
    }

} // End of DistMonitor.Scheduler namespace

// End of file
