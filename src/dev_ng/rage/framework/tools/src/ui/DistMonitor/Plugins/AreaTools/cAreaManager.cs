//
// File: cAreaMonitor.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of cAreaMonitor.cs class
//

using System;
using System.Collections.Generic;
using System.Net;

using RSG.Base.Math;

namespace AreaTools
{

    /// <summary>
    /// Area Manager
    /// </summary>
    public class cAreaManager
    {
        #region Events
        /// <summary>
        /// Event raised by AreaManager when a WorkUnit's Status changes
        /// </summary>
        public event EventHandler<cAreaManagerWorkUnitEventArgs> WorkUnitStatusChangeEvent;

        /// <summary>
        /// Event raised by AreaManager when the initial Work Units have been created
        /// </summary>
        public event EventHandler<EventArgs> WorkUnitsPopulatedEvent;
        #endregion // Events

        #region Properties and Associated Member Data
        /// <summary>
        /// Game Area we are managing
        /// </summary>
        public cArea Area
        {
            get { return m_Area; }
            private set { m_Area = value; }
        }
        private cArea m_Area;

        /// <summary>
        /// start work unit we wish to begin with
        /// </summary>
        public Int32 StartWorkUnit
        {
            get { return m_StartWorkUnit; }
            set { m_StartWorkUnit = value; }
        }
        private Int32 m_StartWorkUnit;

        /// <summary>
        /// end work unit we wish to begin with
        /// </summary>
        public Int32 EndWorkUnit
        {
            get { return m_EndWorkUnit; }
            set { m_EndWorkUnit = value; }
        }
        private Int32 m_EndWorkUnit;

        /// <summary>
        /// List of workunits covering Area
        /// </summary>
        public List<AreaTools.cWorkUnit> WorkUnits
        {
            get { return m_WorkUnits; }
            private set { m_WorkUnits = value; }
        }
        private List<AreaTools.cWorkUnit> m_WorkUnits;

        /// <summary>
        /// File Monitors
        /// </summary>
        public Dictionary<IPAddress, cAreaProgressFileMonitor> FileMonitors
        {
            get { return m_FileMonitors; }
            private set { m_FileMonitors = value; }
        }
        private Dictionary<IPAddress, cAreaProgressFileMonitor> m_FileMonitors;
        #endregion // Properties and Associated Member Data

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public cAreaManager()
        {
            Area = null;
            WorkUnits = new List<cWorkUnit>();
        }
        #endregion // Constructor

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="area"></param>
        public void Initialise(cArea area)
        {
            // Clear existing work units
            foreach (cWorkUnit unit in WorkUnits)
            {
                unit.WorkUnitStatusChangedEvent -= this.OnWorkUnitStatusChanged;
            }

            // Setup new area
            Area = area;
            WorkUnits = new List<cWorkUnit>();
            FileMonitors = new Dictionary<IPAddress, cAreaProgressFileMonitor>();

            // Loop through Area constructing suitably sized work-units
            cWorkUnit.Reset();
            int idx = 0;
            for (float fX = Area.Start.X; 
                 fX < Area.End.X; 
                 fX += (Area.WorkUnit.X*Area.SectorSize))
            {
                for (float fY = Area.Start.Y; 
                     fY < Area.End.Y; 
                     fY += (Area.WorkUnit.Y*Area.SectorSize))
                {
                    float fSize = ((Area.WorkUnit.Y * Area.SectorSize) - 1.0f);
                    Vector2f start = new Vector2f(fX, fY);
                    Vector2f end = new Vector2f(fX + fSize, fY + fSize);

                    cWorkUnit unit = new cWorkUnit(start, end);
                    unit.WorkUnitStatusChangedEvent += this.OnWorkUnitStatusChanged;
                    WorkUnits.Add(unit);

                    if (idx < StartWorkUnit || idx > EndWorkUnit)
                    {
                        unit.State = cWorkUnit.etState.Completed;
                    }
                    idx++;
                }
            }

            if (null != WorkUnitsPopulatedEvent)
                WorkUnitsPopulatedEvent(this, new EventArgs());
        }

        /// <summary>
        /// Retrieve current work unit the machine is processing
        /// </summary>
        /// <param name="machine"></param>
        /// <returns></returns>
        public cWorkUnit CurrentWorkUnit(MachineInterface.iMachine machine)
        {
            cWorkUnit wu = null;
            foreach (cWorkUnit w in WorkUnits)
            {
                if ((w.AssignedMachine == machine) && (cWorkUnit.etState.Completed != w.State && cWorkUnit.etState.Shafted != w.State))
                {
                    wu = w;
                    break;
                }
            }

            return (wu);
        }

        /// <summary>
        /// Retrieve next available work unit for a machine to process
        /// </summary>
        /// <returns>Next available work unit</returns>
        public cWorkUnit NextWorkUnit(MachineInterface.iMachine machine)
        {
            cWorkUnit wu = null;
            {
                Int32 idx = 0;

                // DW - Machine that have a shaftcount should *preferably* not be assigned workunits that have been previously shafted.
                foreach (cWorkUnit w in WorkUnits)
                {
                    if (cWorkUnit.etState.Queued == w.State && idx >= StartWorkUnit && idx <= EndWorkUnit)
                    {
                        if (machine.Enabled && machine.Available && machine.Failures == 0 && w.ShaftCount == 0)
                        {
                            wu = w;
                            wu.Start(machine);
                            break;
                        }
                    }

                    idx++;
                }
            }

            if (wu == null)
            {
                Int32 idx = 0;
                foreach (cWorkUnit w in WorkUnits)
                {
                    if (cWorkUnit.etState.Queued == w.State && idx >= StartWorkUnit && idx <= EndWorkUnit)
                    {
                        wu = w;
                        wu.Start(machine);
                        break;
                    }

                    idx++;
                }
            }

            return (wu);
        }

        /// <summary>
        /// Determine if all work units have been finished
        /// </summary>
        /// <returns>true iff all work units have been finished, false otherwise</returns>
        public bool HasCompleted()
        {
            bool result = true;
            foreach (cWorkUnit w in this.WorkUnits)
            {
                if (cWorkUnit.etState.Completed != w.State && cWorkUnit.etState.Shafted != w.State)
                {
                    result = false;
                    break;
                }
            }

            return (result);
        }
        #endregion // Controller Methods

        #region Work Unit Event Handlers
        /// <summary>
        /// Work Unit Status Changed Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnWorkUnitStatusChanged(Object sender, cAreaManagerWorkUnitEventArgs e)
        {
            if (null != this.WorkUnitStatusChangeEvent)
                WorkUnitStatusChangeEvent(this, e);
        }
        #endregion // Work Unit Event Handlers
    }

    #region Event Argument Classes
    /// <summary>
    /// 
    /// </summary>
    public class cAreaManagerWorkUnitEventArgs : EventArgs
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// 
        /// </summary>
        public AreaTools.cWorkUnit WorkUnit
        {
            get { return m_WorkUnit; }
            private set { m_WorkUnit = value; }
        }
        private AreaTools.cWorkUnit m_WorkUnit;
        #endregion // Properties and Associated Member Data

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="workunit"></param>
        public cAreaManagerWorkUnitEventArgs(AreaTools.cWorkUnit workunit)
        {
            WorkUnit = workunit;
        }
        #endregion // Constructor
    };
    #endregion // Event Argument Classes

} // End of DistMonitor namespace

// End of file
