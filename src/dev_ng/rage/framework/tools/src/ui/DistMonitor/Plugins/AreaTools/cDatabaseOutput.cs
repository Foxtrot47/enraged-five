//
// File: cDatabaseOutput.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of cDatabaseOutput.cs class
//

using System;
using System.Diagnostics;

using RSG.Base.Logging;

namespace AreaTools
{

    /// <summary>
    /// Quick and dirty class to check output and insert in database if 
    /// everything looks ok.  Already have a PHP script that does the data
    /// insertion so we just invoke it.
    /// </summary>
    internal class cDatabaseOutput
    {
        private static readonly String ImportDBScript = "X:\\gta5\\tools\\script\\util\\stats\\importDB.bat";
        /// <summary>
        /// Insert Performance Stats Data into DB
        /// </summary>
        public static void PerformanceDataInsert(string outputFolderName)
        {
            try
            {
                String s = String.Format("{0} perfstats", outputFolderName);
                Process proc = Process.Start(ImportDBScript, s);
                proc.WaitForExit(1000*60*180); // at most wait for 1.5 hours.
            }
            catch (Exception ex)
            {
                Log.Log__Error(String.Format("Error inserting Performance Data into Database: {0}.", ex.Message));
            }
        }

        /// <summary>
        /// Insert Memory Stats Data into DB
        /// </summary>
        public static void MemoryDataInsert(string outputFolderName)
        {
            try
            {
                String s = String.Format("{0} memstats", outputFolderName);
                Process proc = Process.Start(ImportDBScript, s);
                proc.WaitForExit(1000 * 60 * 180); // at most wait for 1.5 hours.
            }
            catch (Exception ex)
            {
                Log.Log__Error(String.Format("Error inserting Memory Data into Database: {0}.", ex.Message));
            }

        }
    }

} // End of AreaTools namespace

// End of file
