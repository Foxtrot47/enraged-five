//
// File: uiMachineView.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of uiMachineView.cs class
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace DistMonitor.UI
{

    #region Machine View
    /// <summary>
    /// Generic Machine View
    /// </summary>
    internal partial class uiMachineView : UserControl
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// 
        /// </summary>
        public cMachineCollection Machines
        {
            get { return m_Machines; }
            set { m_Machines = value; UpdateView(); }
        }
        private cMachineCollection m_Machines;
        
        /// <summary>
        /// 
        /// </summary>
        public List<cMachine> SelectedMachines
        {
            get 
            {
                List<cMachine> selection = new List<cMachine>();
                foreach (ListViewItem item in this.lvMachines.SelectedItems)
                {
                    uiMachineListViewItem machItem = 
                        (item as uiMachineListViewItem);
                    selection.Add(machItem.Machine);
                }
                return (selection);
            }
        }
        #endregion // Properties

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public uiMachineView()
        {
            InitializeComponent();
            m_Machines = new cMachineCollection();
        }
        #endregion // Constructor

        #region Controller Methods
        /// <summary>
        /// Update machine view
        /// </summary>
        public void UpdateView()
        {
            lvMachines.BeginUpdate();
            lvMachines.Items.Clear();
            if (null != m_Machines)
            {
                foreach (cMachine machine in m_Machines)
                {
                    lvMachines.Items.Add(new uiMachineListViewItem(machine));
                }   
            }
            lvMachines.EndUpdate();
        }
        #endregion // Controller Methods
        
        #region Private Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="machine"></param>
        public void OnMachineAdd(Object sender, cMachineEventArgs e)
        {
            lvMachines.Items.Add(new uiMachineListViewItem(e.Machine));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="machine"></param>
        public void OnMachineRemove(Object sender, cMachineEventArgs e)
        {
            uiMachineListViewItem machItem = null;

            foreach (ListViewItem item in lvMachines.Items)
            {
                uiMachineListViewItem machIt = (item as uiMachineListViewItem);
                if (machIt.Machine == e.Machine)
                {
                    machItem = machIt;
                    break;
                }
            }

            if (null != machItem)
                lvMachines.Items.Remove(machItem);
        }
        #endregion // Private Event Handlers
    }
    #endregion Machine View

    #region Machine List View Item
    /// <summary>
    /// ListViewItem representing a cMachine
    /// </summary>
    internal class uiMachineListViewItem : ListViewItem
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Machine this ListViewItem is representing
        /// </summary>
        public cMachine Machine
        {
            get { return m_Machine; }
            private set { m_Machine = value; }
        }
        private cMachine m_Machine;
        #endregion // Properties and Associated Member Data

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="machine"></param>
        public uiMachineListViewItem(cMachine machine)
            : base("")
        {
            Machine = machine;
            Initialise();
        }
        #endregion // Constructor

        #region Controller Methods
        /// <summary>
        /// Refresh item
        /// </summary>
        public void UpdateView()
        {
            // Calculate latency
            long ms_Latency = MachineInterface.cMachineUtils.PingIP(Machine.IP);
            String sLatency = String.Empty;
            if (0 == ms_Latency)
                sLatency = String.Format("< 1");
            else if (long.MaxValue == ms_Latency)
                sLatency = "Timeout";
            else
                sLatency = String.Format("{0}", ms_Latency);

            this.SubItems[0].Text = Machine.IP.ToString();
            this.SubItems[1].Text = sLatency;
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Initialisation
        /// </summary>
        private void Initialise()
        {
            this.SubItems.Add(new ListViewItem.ListViewSubItem(this, ""));
            this.SubItems.Add(new ListViewItem.ListViewSubItem(this, ""));

            UpdateView();
        }
        #endregion // Private Methods
    }
    #endregion // Machine List View Item

    #region Event Argument Classes
    /// <summary>
    /// Machine Event Argument Class
    /// </summary>
    internal class cMachineEventArgs : EventArgs
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Machine corresponding to event
        /// </summary>
        public cMachine Machine
        {
            get { return m_Machine; }
            private set { m_Machine = value; }
        }
        private cMachine m_Machine;
        #endregion // Properties and Associated Member Data

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public cMachineEventArgs(cMachine machine)
        {
            Machine = machine;
        }
        #endregion // Constructor
    }
    #endregion // Event Argument Classes

} // End of DistMonitor.UI namespace

// End of file
