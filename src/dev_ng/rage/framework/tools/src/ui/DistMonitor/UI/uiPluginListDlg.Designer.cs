namespace DistMonitor.UI
{
    partial class uiPluginListDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitMain = new System.Windows.Forms.SplitContainer();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabMachinePlugins = new System.Windows.Forms.TabPage();
            this.lvMachinePlugins = new RSG.Base.Controls.uiPluginListView();
            this.tabToolPlugins = new System.Windows.Forms.TabPage();
            this.lvToolPlugins = new RSG.Base.Controls.uiPluginListView();
            this.tabToolSettingPlugins = new System.Windows.Forms.TabPage();
            this.lvToolSettingPlugins = new RSG.Base.Controls.uiPluginListView();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnClose = new System.Windows.Forms.Button();
            this.splitMain.Panel1.SuspendLayout();
            this.splitMain.Panel2.SuspendLayout();
            this.splitMain.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabMachinePlugins.SuspendLayout();
            this.tabToolPlugins.SuspendLayout();
            this.tabToolSettingPlugins.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitMain
            // 
            this.splitMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitMain.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitMain.IsSplitterFixed = true;
            this.splitMain.Location = new System.Drawing.Point(0, 0);
            this.splitMain.Name = "splitMain";
            this.splitMain.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitMain.Panel1
            // 
            this.splitMain.Panel1.Controls.Add(this.tabControl);
            // 
            // splitMain.Panel2
            // 
            this.splitMain.Panel2.Controls.Add(this.tableLayoutPanel1);
            this.splitMain.Size = new System.Drawing.Size(518, 348);
            this.splitMain.SplitterDistance = 314;
            this.splitMain.TabIndex = 1;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabMachinePlugins);
            this.tabControl.Controls.Add(this.tabToolPlugins);
            this.tabControl.Controls.Add(this.tabToolSettingPlugins);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(518, 314);
            this.tabControl.TabIndex = 0;
            // 
            // tabMachinePlugins
            // 
            this.tabMachinePlugins.Controls.Add(this.lvMachinePlugins);
            this.tabMachinePlugins.Location = new System.Drawing.Point(4, 22);
            this.tabMachinePlugins.Name = "tabMachinePlugins";
            this.tabMachinePlugins.Padding = new System.Windows.Forms.Padding(3);
            this.tabMachinePlugins.Size = new System.Drawing.Size(428, 247);
            this.tabMachinePlugins.TabIndex = 0;
            this.tabMachinePlugins.Text = "Machine Plugins";
            this.tabMachinePlugins.UseVisualStyleBackColor = true;
            // 
            // lvMachinePlugins
            // 
            this.lvMachinePlugins.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvMachinePlugins.FullRowSelect = true;
            this.lvMachinePlugins.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvMachinePlugins.Location = new System.Drawing.Point(3, 3);
            this.lvMachinePlugins.Name = "lvMachinePlugins";
            this.lvMachinePlugins.Plugins = null;
            this.lvMachinePlugins.Size = new System.Drawing.Size(422, 241);
            this.lvMachinePlugins.TabIndex = 0;
            this.lvMachinePlugins.UseCompatibleStateImageBehavior = false;
            this.lvMachinePlugins.View = System.Windows.Forms.View.Details;
            // 
            // tabToolPlugins
            // 
            this.tabToolPlugins.Controls.Add(this.lvToolPlugins);
            this.tabToolPlugins.Location = new System.Drawing.Point(4, 22);
            this.tabToolPlugins.Name = "tabToolPlugins";
            this.tabToolPlugins.Padding = new System.Windows.Forms.Padding(3);
            this.tabToolPlugins.Size = new System.Drawing.Size(428, 247);
            this.tabToolPlugins.TabIndex = 1;
            this.tabToolPlugins.Text = "Tool Plugins";
            this.tabToolPlugins.UseVisualStyleBackColor = true;
            // 
            // lvToolPlugins
            // 
            this.lvToolPlugins.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvToolPlugins.FullRowSelect = true;
            this.lvToolPlugins.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvToolPlugins.Location = new System.Drawing.Point(3, 3);
            this.lvToolPlugins.Name = "lvToolPlugins";
            this.lvToolPlugins.Plugins = null;
            this.lvToolPlugins.Size = new System.Drawing.Size(422, 241);
            this.lvToolPlugins.TabIndex = 1;
            this.lvToolPlugins.UseCompatibleStateImageBehavior = false;
            this.lvToolPlugins.View = System.Windows.Forms.View.Details;
            // 
            // tabToolSettingPlugins
            // 
            this.tabToolSettingPlugins.Controls.Add(this.lvToolSettingPlugins);
            this.tabToolSettingPlugins.Location = new System.Drawing.Point(4, 22);
            this.tabToolSettingPlugins.Name = "tabToolSettingPlugins";
            this.tabToolSettingPlugins.Padding = new System.Windows.Forms.Padding(3);
            this.tabToolSettingPlugins.Size = new System.Drawing.Size(510, 288);
            this.tabToolSettingPlugins.TabIndex = 2;
            this.tabToolSettingPlugins.Text = "Tool Setting Plugins";
            this.tabToolSettingPlugins.UseVisualStyleBackColor = true;
            // 
            // lvToolSettingPlugins
            // 
            this.lvToolSettingPlugins.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvToolSettingPlugins.FullRowSelect = true;
            this.lvToolSettingPlugins.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvToolSettingPlugins.Location = new System.Drawing.Point(3, 3);
            this.lvToolSettingPlugins.Name = "lvToolSettingPlugins";
            this.lvToolSettingPlugins.Plugins = null;
            this.lvToolSettingPlugins.Size = new System.Drawing.Size(504, 282);
            this.lvToolSettingPlugins.TabIndex = 1;
            this.lvToolSettingPlugins.UseCompatibleStateImageBehavior = false;
            this.lvToolSettingPlugins.View = System.Windows.Forms.View.Details;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.btnClose, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(518, 30);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(440, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // uiPluginListDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(518, 348);
            this.Controls.Add(this.splitMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "uiPluginListDlg";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Plugins";
            this.splitMain.Panel1.ResumeLayout(false);
            this.splitMain.Panel2.ResumeLayout(false);
            this.splitMain.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.tabMachinePlugins.ResumeLayout(false);
            this.tabToolPlugins.ResumeLayout(false);
            this.tabToolSettingPlugins.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitMain;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabMachinePlugins;
        private System.Windows.Forms.TabPage tabToolPlugins;
        private System.Windows.Forms.TabPage tabToolSettingPlugins;
        private RSG.Base.Controls.uiPluginListView lvMachinePlugins;
        private RSG.Base.Controls.uiPluginListView lvToolPlugins;
        private RSG.Base.Controls.uiPluginListView lvToolSettingPlugins;
    }
}