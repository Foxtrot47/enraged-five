namespace DistMonitor.UI
{
    partial class uiMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uiMainForm));
            this.toolStrip = new System.Windows.Forms.ToolStripContainer();
            this.statusBar = new System.Windows.Forms.StatusStrip();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.uiFarmView1 = new DistMonitor.UI.uiFarmView();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.uiFarmLog1 = new DistMonitor.UI.uiFarmLog();
            this.menuMain = new System.Windows.Forms.MenuStrip();
            this.miFile = new System.Windows.Forms.ToolStripMenuItem();
            this.miFileOptions = new System.Windows.Forms.ToolStripMenuItem();
            this.miFilePlugins = new System.Windows.Forms.ToolStripMenuItem();
            this.miFileSep1 = new System.Windows.Forms.ToolStripSeparator();
            this.miFileExit = new System.Windows.Forms.ToolStripMenuItem();
            this.miScheduler = new System.Windows.Forms.ToolStripMenuItem();
            this.miSchedulerEnabled = new System.Windows.Forms.ToolStripMenuItem();
            this.miSchedulerSep1 = new System.Windows.Forms.ToolStripSeparator();
            this.miSchedulerConfigure = new System.Windows.Forms.ToolStripMenuItem();
            this.miView = new System.Windows.Forms.ToolStripMenuItem();
            this.miViewLogPane = new System.Windows.Forms.ToolStripMenuItem();
            this.miViewLogs = new System.Windows.Forms.ToolStripMenuItem();
            this.miViewLogsClearAppLog = new System.Windows.Forms.ToolStripMenuItem();
            this.miClearSchedulerLog = new System.Windows.Forms.ToolStripMenuItem();
            this.miViewLogsClearMachineLogs = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miHelpAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.uiGameConfigBar = new DistMonitor.UI.uiGameConfigControl();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnConfigureFarm = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnStart = new System.Windows.Forms.ToolStripButton();
            this.btnStop = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnReboot = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.btnPing = new System.Windows.Forms.ToolStripButton();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.menuCtxNotify = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miCtxRestore = new System.Windows.Forms.ToolStripMenuItem();
            this.miCtxSep1 = new System.Windows.Forms.ToolStripSeparator();
            this.miCtxOptions = new System.Windows.Forms.ToolStripMenuItem();
            this.miCtxSep2 = new System.Windows.Forms.ToolStripSeparator();
            this.miCtxExit = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip.BottomToolStripPanel.SuspendLayout();
            this.toolStrip.ContentPanel.SuspendLayout();
            this.toolStrip.TopToolStripPanel.SuspendLayout();
            this.toolStrip.SuspendLayout();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.menuMain.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.menuCtxNotify.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip
            // 
            // 
            // toolStrip.BottomToolStripPanel
            // 
            this.toolStrip.BottomToolStripPanel.Controls.Add(this.statusBar);
            // 
            // toolStrip.ContentPanel
            // 
            this.toolStrip.ContentPanel.Controls.Add(this.splitContainer);
            this.toolStrip.ContentPanel.Size = new System.Drawing.Size(698, 506);
            this.toolStrip.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStrip.Location = new System.Drawing.Point(0, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(698, 602);
            this.toolStrip.TabIndex = 0;
            this.toolStrip.Text = "toolStripContainer1";
            // 
            // toolStrip.TopToolStripPanel
            // 
            this.toolStrip.TopToolStripPanel.Controls.Add(this.menuMain);
            this.toolStrip.TopToolStripPanel.Controls.Add(this.uiGameConfigBar);
            this.toolStrip.TopToolStripPanel.Controls.Add(this.toolStrip1);
            // 
            // statusBar
            // 
            this.statusBar.Dock = System.Windows.Forms.DockStyle.None;
            this.statusBar.Location = new System.Drawing.Point(0, 0);
            this.statusBar.Name = "statusBar";
            this.statusBar.Size = new System.Drawing.Size(698, 22);
            this.statusBar.TabIndex = 0;
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.Location = new System.Drawing.Point(0, 0);
            this.splitContainer.Name = "splitContainer";
            this.splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.splitContainer1);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.uiFarmLog1);
            this.splitContainer.Size = new System.Drawing.Size(698, 506);
            this.splitContainer.SplitterDistance = 350;
            this.splitContainer.TabIndex = 0;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.uiFarmView1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tabControl);
            this.splitContainer1.Size = new System.Drawing.Size(698, 350);
            this.splitContainer1.SplitterDistance = 231;
            this.splitContainer1.TabIndex = 0;
            // 
            // uiFarmView1
            // 
            this.uiFarmView1.AutoSize = true;
            this.uiFarmView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiFarmView1.Farm = null;
            this.uiFarmView1.Location = new System.Drawing.Point(0, 0);
            this.uiFarmView1.Name = "uiFarmView1";
            this.uiFarmView1.Size = new System.Drawing.Size(231, 350);
            this.uiFarmView1.TabIndex = 2;
            // 
            // tabControl
            // 
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(463, 350);
            this.tabControl.TabIndex = 0;
            // 
            // uiFarmLog1
            // 
            this.uiFarmLog1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiFarmLog1.Farm = null;
            this.uiFarmLog1.Location = new System.Drawing.Point(0, 0);
            this.uiFarmLog1.Name = "uiFarmLog1";
            this.uiFarmLog1.Size = new System.Drawing.Size(698, 152);
            this.uiFarmLog1.TabIndex = 0;
            // 
            // menuMain
            // 
            this.menuMain.Dock = System.Windows.Forms.DockStyle.None;
            this.menuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miFile,
            this.miScheduler,
            this.miView,
            this.helpToolStripMenuItem});
            this.menuMain.Location = new System.Drawing.Point(0, 0);
            this.menuMain.Name = "menuMain";
            this.menuMain.Size = new System.Drawing.Size(698, 24);
            this.menuMain.TabIndex = 0;
            this.menuMain.Text = "menuStrip1";
            // 
            // miFile
            // 
            this.miFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miFileOptions,
            this.miFilePlugins,
            this.miFileSep1,
            this.miFileExit});
            this.miFile.Name = "miFile";
            this.miFile.Size = new System.Drawing.Size(35, 20);
            this.miFile.Text = "&File";
            // 
            // miFileOptions
            // 
            this.miFileOptions.Image = ((System.Drawing.Image)(resources.GetObject("miFileOptions.Image")));
            this.miFileOptions.ImageTransparentColor = System.Drawing.Color.Green;
            this.miFileOptions.Name = "miFileOptions";
            this.miFileOptions.Size = new System.Drawing.Size(134, 22);
            this.miFileOptions.Text = "&Options...";
            this.miFileOptions.Click += new System.EventHandler(this.miFileOptions_Click);
            // 
            // miFilePlugins
            // 
            this.miFilePlugins.Image = ((System.Drawing.Image)(resources.GetObject("miFilePlugins.Image")));
            this.miFilePlugins.ImageTransparentColor = System.Drawing.Color.Fuchsia;
            this.miFilePlugins.Name = "miFilePlugins";
            this.miFilePlugins.Size = new System.Drawing.Size(134, 22);
            this.miFilePlugins.Text = "&Plugins...";
            this.miFilePlugins.Click += new System.EventHandler(this.miFilePlugins_Click);
            // 
            // miFileSep1
            // 
            this.miFileSep1.Name = "miFileSep1";
            this.miFileSep1.Size = new System.Drawing.Size(131, 6);
            // 
            // miFileExit
            // 
            this.miFileExit.Name = "miFileExit";
            this.miFileExit.Size = new System.Drawing.Size(134, 22);
            this.miFileExit.Text = "E&xit";
            this.miFileExit.Click += new System.EventHandler(this.miFileExit_Click);
            // 
            // miScheduler
            // 
            this.miScheduler.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miSchedulerEnabled,
            this.miSchedulerSep1,
            this.miSchedulerConfigure});
            this.miScheduler.Name = "miScheduler";
            this.miScheduler.Size = new System.Drawing.Size(66, 20);
            this.miScheduler.Text = "&Scheduler";
            this.miScheduler.DropDownOpening += new System.EventHandler(this.miScheduler_DropDownOpening);
            // 
            // miSchedulerEnabled
            // 
            this.miSchedulerEnabled.Name = "miSchedulerEnabled";
            this.miSchedulerEnabled.Size = new System.Drawing.Size(144, 22);
            this.miSchedulerEnabled.Text = "&Enabled";
            this.miSchedulerEnabled.Click += new System.EventHandler(this.miSchedulerEnabled_Click);
            // 
            // miSchedulerSep1
            // 
            this.miSchedulerSep1.Name = "miSchedulerSep1";
            this.miSchedulerSep1.Size = new System.Drawing.Size(141, 6);
            // 
            // miSchedulerConfigure
            // 
            this.miSchedulerConfigure.Name = "miSchedulerConfigure";
            this.miSchedulerConfigure.Size = new System.Drawing.Size(144, 22);
            this.miSchedulerConfigure.Text = "&Configure...";
            this.miSchedulerConfigure.Click += new System.EventHandler(this.miSchedulerConfigure_Click);
            // 
            // miView
            // 
            this.miView.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miViewLogPane,
            this.miViewLogs});
            this.miView.Name = "miView";
            this.miView.Size = new System.Drawing.Size(41, 20);
            this.miView.Text = "&View";
            this.miView.DropDownOpening += new System.EventHandler(this.miView_DropDownOpening);
            // 
            // miViewLogPane
            // 
            this.miViewLogPane.Name = "miViewLogPane";
            this.miViewLogPane.Size = new System.Drawing.Size(129, 22);
            this.miViewLogPane.Text = "&Log Pane";
            this.miViewLogPane.Click += new System.EventHandler(this.miViewLogPane_Click);
            // 
            // miViewLogs
            // 
            this.miViewLogs.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miViewLogsClearAppLog,
            this.miClearSchedulerLog,
            this.miViewLogsClearMachineLogs});
            this.miViewLogs.Name = "miViewLogs";
            this.miViewLogs.Size = new System.Drawing.Size(129, 22);
            this.miViewLogs.Text = "Logs";
            // 
            // miViewLogsClearAppLog
            // 
            this.miViewLogsClearAppLog.Name = "miViewLogsClearAppLog";
            this.miViewLogsClearAppLog.Size = new System.Drawing.Size(185, 22);
            this.miViewLogsClearAppLog.Text = "&Clear Application Log";
            this.miViewLogsClearAppLog.Click += new System.EventHandler(this.miViewLogsClearAppLog_Click);
            // 
            // miClearSchedulerLog
            // 
            this.miClearSchedulerLog.Name = "miClearSchedulerLog";
            this.miClearSchedulerLog.Size = new System.Drawing.Size(185, 22);
            this.miClearSchedulerLog.Text = "Clear &Scheduler Log";
            this.miClearSchedulerLog.Click += new System.EventHandler(this.miClearSchedulerLog_Click);
            // 
            // miViewLogsClearMachineLogs
            // 
            this.miViewLogsClearMachineLogs.Name = "miViewLogsClearMachineLogs";
            this.miViewLogsClearMachineLogs.Size = new System.Drawing.Size(185, 22);
            this.miViewLogsClearMachineLogs.Text = "C&lear Machine Logs";
            this.miViewLogsClearMachineLogs.Click += new System.EventHandler(this.miViewLogsClearMachineLogs_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miHelpAbout});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // miHelpAbout
            // 
            this.miHelpAbout.Image = ((System.Drawing.Image)(resources.GetObject("miHelpAbout.Image")));
            this.miHelpAbout.ImageTransparentColor = System.Drawing.Color.Silver;
            this.miHelpAbout.Name = "miHelpAbout";
            this.miHelpAbout.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.miHelpAbout.Size = new System.Drawing.Size(145, 22);
            this.miHelpAbout.Text = "&About...";
            this.miHelpAbout.Click += new System.EventHandler(this.miHelpAbout_Click);
            // 
            // uiGameConfigBar
            // 
            this.uiGameConfigBar.Dock = System.Windows.Forms.DockStyle.None;
            this.uiGameConfigBar.GameList = null;
            this.uiGameConfigBar.Location = new System.Drawing.Point(3, 24);
            this.uiGameConfigBar.Name = "uiGameConfigBar";
            this.uiGameConfigBar.Size = new System.Drawing.Size(492, 25);
            this.uiGameConfigBar.TabIndex = 1;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnConfigureFarm,
            this.toolStripSeparator1,
            this.btnStart,
            this.btnStop,
            this.toolStripSeparator2,
            this.btnReboot,
            this.toolStripSeparator3,
            this.btnPing});
            this.toolStrip1.Location = new System.Drawing.Point(3, 49);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(352, 25);
            this.toolStrip1.TabIndex = 2;
            // 
            // btnConfigureFarm
            // 
            this.btnConfigureFarm.Image = ((System.Drawing.Image)(resources.GetObject("btnConfigureFarm.Image")));
            this.btnConfigureFarm.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnConfigureFarm.Name = "btnConfigureFarm";
            this.btnConfigureFarm.Size = new System.Drawing.Size(113, 22);
            this.btnConfigureFarm.Text = "Configure Farm...";
            this.btnConfigureFarm.Click += new System.EventHandler(this.btnConfigureFarm_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // btnStart
            // 
            this.btnStart.Image = ((System.Drawing.Image)(resources.GetObject("btnStart.Image")));
            this.btnStart.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(51, 22);
            this.btnStart.Text = "Start";
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnStop
            // 
            this.btnStop.Image = ((System.Drawing.Image)(resources.GetObject("btnStop.Image")));
            this.btnStop.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(49, 22);
            this.btnStop.Text = "Stop";
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnReboot
            // 
            this.btnReboot.Image = ((System.Drawing.Image)(resources.GetObject("btnReboot.Image")));
            this.btnReboot.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnReboot.Name = "btnReboot";
            this.btnReboot.Size = new System.Drawing.Size(62, 22);
            this.btnReboot.Text = "Reboot";
            this.btnReboot.Click += new System.EventHandler(this.btnReboot_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // btnPing
            // 
            this.btnPing.Image = ((System.Drawing.Image)(resources.GetObject("btnPing.Image")));
            this.btnPing.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPing.Name = "btnPing";
            this.btnPing.Size = new System.Drawing.Size(47, 22);
            this.btnPing.Text = "Ping";
            this.btnPing.Click += new System.EventHandler(this.btnPing_Click);
            // 
            // notifyIcon
            // 
            this.notifyIcon.ContextMenuStrip = this.menuCtxNotify;
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "notifyIcon1";
            this.notifyIcon.Visible = true;
            this.notifyIcon.DoubleClick += new System.EventHandler(this.notifyIcon_DoubleClick);
            // 
            // menuCtxNotify
            // 
            this.menuCtxNotify.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miCtxRestore,
            this.miCtxSep1,
            this.miCtxOptions,
            this.miCtxSep2,
            this.miCtxExit});
            this.menuCtxNotify.Name = "menuCtxNotify";
            this.menuCtxNotify.Size = new System.Drawing.Size(135, 82);
            this.menuCtxNotify.Opening += new System.ComponentModel.CancelEventHandler(this.menuCtxNotify_Opening);
            // 
            // miCtxRestore
            // 
            this.miCtxRestore.Name = "miCtxRestore";
            this.miCtxRestore.Size = new System.Drawing.Size(134, 22);
            this.miCtxRestore.Text = "&Restore";
            this.miCtxRestore.Click += new System.EventHandler(this.miCtxRestore_Click);
            // 
            // miCtxSep1
            // 
            this.miCtxSep1.Name = "miCtxSep1";
            this.miCtxSep1.Size = new System.Drawing.Size(131, 6);
            // 
            // miCtxOptions
            // 
            this.miCtxOptions.Image = ((System.Drawing.Image)(resources.GetObject("miCtxOptions.Image")));
            this.miCtxOptions.ImageTransparentColor = System.Drawing.Color.Green;
            this.miCtxOptions.Name = "miCtxOptions";
            this.miCtxOptions.Size = new System.Drawing.Size(134, 22);
            this.miCtxOptions.Text = "&Options...";
            this.miCtxOptions.Click += new System.EventHandler(this.miCtxOptions_Click);
            // 
            // miCtxSep2
            // 
            this.miCtxSep2.Name = "miCtxSep2";
            this.miCtxSep2.Size = new System.Drawing.Size(131, 6);
            // 
            // miCtxExit
            // 
            this.miCtxExit.Name = "miCtxExit";
            this.miCtxExit.Size = new System.Drawing.Size(134, 22);
            this.miCtxExit.Text = "E&xit";
            this.miCtxExit.Click += new System.EventHandler(this.miCtxExit_Click);
            // 
            // uiMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(698, 602);
            this.Controls.Add(this.toolStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuMain;
            this.Name = "uiMainForm";
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.uiMainForm_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.uiMainForm_FormClosing);
            this.Resize += new System.EventHandler(this.uiMainForm_Resize);
            this.toolStrip.BottomToolStripPanel.ResumeLayout(false);
            this.toolStrip.BottomToolStripPanel.PerformLayout();
            this.toolStrip.ContentPanel.ResumeLayout(false);
            this.toolStrip.TopToolStripPanel.ResumeLayout(false);
            this.toolStrip.TopToolStripPanel.PerformLayout();
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            this.splitContainer.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.menuMain.ResumeLayout(false);
            this.menuMain.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.menuCtxNotify.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer toolStrip;
        private System.Windows.Forms.StatusStrip statusBar;
        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.MenuStrip menuMain;
        private System.Windows.Forms.ToolStripMenuItem miFile;
        private System.Windows.Forms.ToolStripMenuItem miFileExit;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnConfigureFarm;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btnStart;
        private System.Windows.Forms.ToolStripButton btnStop;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton btnReboot;
        private System.Windows.Forms.ToolStripMenuItem miHelpAbout;
        private System.Windows.Forms.ToolStripMenuItem miFileOptions;
        private System.Windows.Forms.ToolStripSeparator miFileSep1;
        private System.Windows.Forms.ToolStripMenuItem miView;
        private System.Windows.Forms.ToolStripMenuItem miViewLogPane;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton btnPing;
        private DistMonitor.UI.uiFarmLog uiFarmLog1;
        internal DistMonitor.UI.uiGameConfigControl uiGameConfigBar;
        private System.Windows.Forms.ToolStripMenuItem miViewLogs;
        private System.Windows.Forms.ToolStripMenuItem miViewLogsClearAppLog;
        private System.Windows.Forms.ToolStripMenuItem miViewLogsClearMachineLogs;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private DistMonitor.UI.uiFarmView uiFarmView1;
        public System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.ContextMenuStrip menuCtxNotify;
        private System.Windows.Forms.ToolStripMenuItem miCtxOptions;
        private System.Windows.Forms.ToolStripMenuItem miCtxRestore;
        private System.Windows.Forms.ToolStripSeparator miCtxSep1;
        private System.Windows.Forms.ToolStripSeparator miCtxSep2;
        private System.Windows.Forms.ToolStripMenuItem miCtxExit;
        private System.Windows.Forms.ToolStripMenuItem miScheduler;
        private System.Windows.Forms.ToolStripMenuItem miSchedulerEnabled;
        private System.Windows.Forms.ToolStripSeparator miSchedulerSep1;
        private System.Windows.Forms.ToolStripMenuItem miSchedulerConfigure;
        private System.Windows.Forms.ToolStripMenuItem miClearSchedulerLog;
        private System.Windows.Forms.ToolStripMenuItem miFilePlugins;
    }
}

