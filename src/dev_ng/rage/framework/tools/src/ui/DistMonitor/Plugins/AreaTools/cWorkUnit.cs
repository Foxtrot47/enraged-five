//
// File: cWorkUnit.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of cWorkUnit.cs class
//

using System;
using System.Collections.Generic;
using System.Text;

using RSG.Base.Math;

namespace AreaTools
{

    /// <summary>
    /// 
    /// </summary>
    public class cWorkUnit
    {
        #region Enumerations
        /// <summary>
        /// Work unit state enumeration
        /// </summary>
        public enum etState
        {
            Queued,
            Started,
            Completed,
            Shafted           
        }
        #endregion // Enumerations

        #region Events
        /// <summary>
        /// Event raised by work unit when its status changes
        /// </summary>
        public event EventHandler<cAreaManagerWorkUnitEventArgs> WorkUnitStatusChangedEvent;
        #endregion

        #region Properties and Associated Member Data
        /// <summary>
        /// Workunit unique identifier
        /// </summary>
        public UInt32 ID
        {
            get { return m_nID; }
            private set { m_nID = value; }
        }
        private UInt32 m_nID;
        
        /// <summary>
        /// Workunit state
        /// </summary>
        public etState State
        {
            get { return m_eState; }
            set { m_eState = value; }
        }
        private etState m_eState;

        /// <summary>
        /// Start World Coordinates of WorkUnit
        /// </summary>
        public Vector2f StartWorld
        {
            get { return m_vfStartWorld; }
            private set { m_vfStartWorld = value; }
        }
        private Vector2f m_vfStartWorld;

        /// <summary>
        /// End World Coordinates of WorkUnit
        /// </summary>
        public Vector2f EndWorld
        {
            get { return m_vfEndWorld; }
            private set { m_vfEndWorld = value; }
        }
        private Vector2f m_vfEndWorld;

        /// <summary>
        /// Machine this WorkUnit is currently assigned to
        /// </summary>
        public MachineInterface.iMachine AssignedMachine
        {
            get { return m_AssignedMachine; }
            private set { m_AssignedMachine = value; }
        }
        private MachineInterface.iMachine m_AssignedMachine;

        /// <summary>
        /// Start Time of this Work Unit
        /// </summary>
        public DateTime StartTime
        {
            get { return m_dtStartTime; }
            private set { m_dtStartTime = value; }
        }
        private DateTime m_dtStartTime;

        /// <summary>
        /// End Time of this Work Unit
        /// </summary>
        public DateTime EndTime
        {
            get { return m_dtEndTime; }
            private set { m_dtEndTime = value; }
        }
        private DateTime m_dtEndTime;

        /// <summary>
        /// Timeout at propert
        /// </summary>
        public TimeSpan TimeoutAt
        {
            get { return m_tsTimeout; }
            set { m_tsTimeout = value; RaiseWorkUnitStatusChangedEvent(); }
        }
        private TimeSpan m_tsTimeout;

        /// <summary>
        /// Timeouts count for this area
        /// </summary>
        public long TimeoutCount
        {
            get { return m_cntTimeouts; }
            private set { m_cntTimeouts = value; }
        }
        private long m_cntTimeouts;

        /// <summary>
        /// Exceptions count for this area
        /// </summary>
        public long ExceptionCount
        {
            get { return m_cntExceptions; }
            private set { m_cntExceptions = value; }
        }
        private long m_cntExceptions;

        /// <summary>
        /// Shaft count - the number of times it's been marked as shafted
        /// </summary>
        public long ShaftCount
        {
            get { return m_cntShafts; }
            private set { m_cntShafts = value; }
        }
        private long m_cntShafts;

        #endregion // Properties and Associated Member Data

        #region Static Member Data
        private static Object ms_IDLock = new Object();
        private static UInt32 ms_nCurrentID = 0;
        #endregion // Static Member Data

        #region Constructor
        /// <summary>
        /// cWorkUnit Constructor
        /// </summary>
        /// <param name="start">Start of work unit (in game-world coordinates)</param>
        /// <param name="end">End of work unit (in game-world coordinates)</param>
        public cWorkUnit(Vector2f start, Vector2f end)
        {
            lock (ms_IDLock)
            {
                this.ID = ms_nCurrentID;
                ++ms_nCurrentID;
            }

            AssignedMachine = null;
            StartWorld = start;
            EndWorld = end;
            State = etState.Queued;
            ResetTimeoutCount();
            ShaftCount = 0;
        }
        #endregion

        #region Controller Methods
        /// <summary>
        /// Reset Current ID
        /// </summary>
        public static void Reset()
        {
            lock (ms_IDLock)
            {
                ms_nCurrentID = 0;
            }
        }

        /// <summary>
        /// Mark workunit as started, and assign to machine
        /// </summary>
        public void Start(MachineInterface.iMachine machine)
        {
            if (etState.Queued != State)
                throw new exWorkUnitException(this, "Workunit already started.");

            State = etState.Started;
            AssignedMachine = machine;
            StartTime = DateTime.Now;

            RaiseWorkUnitStatusChangedEvent();
        }

        /// <summary>
        /// Mark workunit as queued
        /// </summary>
        public void Restart()
        {
            State = etState.Queued;
            AssignedMachine = null;
            StartTime = DateTime.Now;
            TimeoutCount = 0;
            ExceptionCount = 0;

            RaiseWorkUnitStatusChangedEvent();
        }

        /// <summary>
        /// Mark workunit as completed.
        /// </summary>
        public void Finish()
        {
            if (etState.Started != State)
                throw new exWorkUnitException(this, "Workunit not started.");

            State = etState.Completed;
            EndTime = DateTime.Now;

            RaiseWorkUnitStatusChangedEvent();
        }

        /// <summary>
        /// Mark workunit as completed.
        /// </summary>
        public void Shaft()
        {
            if (etState.Started != State)
                throw new exWorkUnitException(this, "Workunit not started.");

            ShaftCount++;
            State = etState.Shafted;
            EndTime = DateTime.Now;

            RaiseWorkUnitStatusChangedEvent();

            // DW - 18/02/2010 Restart- since it could be a problem with the machine
            // not the work unit, so we basically give it 2 lives.
            // FYI A machine that continually shafts workunits will be disabled.
            // thus adding this to the end of the queue means that that machine is 
            // likely flagged as rogue before this workunit will restart.
            // thats the theory anyway.
            if (ShaftCount <= 2)
            {
                Restart();
            }
        }

        /// <summary>
        /// Override State (for manual override through UI)
        /// </summary>
        /// <param name="state"></param>
        public void OverrideState(etState state)
        {
            switch (state)
            {
                case etState.Queued:
                    this.State = etState.Queued;
                    this.AssignedMachine = null;
                    RaiseWorkUnitStatusChangedEvent();
                    break;
                case etState.Completed:
                    this.State = etState.Completed;
                    this.EndTime = DateTime.Now;
                    RaiseWorkUnitStatusChangedEvent();
                    break;
                case etState.Shafted:
                    this.State = etState.Shafted;
                    this.EndTime = DateTime.Now;
                    RaiseWorkUnitStatusChangedEvent();
                    break;
            }
        }

        /// <summary>
        /// Increment timeout counter
        /// </summary>
        public void IncTimeoutCount()
        {
            ++m_cntTimeouts;
            RaiseWorkUnitStatusChangedEvent();
        }
        
        /// <summary>
        /// Increment exceptions counter
        /// </summary>
        public void IncExceptionCount()
        {
            ++m_cntExceptions;
            RaiseWorkUnitStatusChangedEvent();
        }

        /// <summary>
        /// Reset timeout counter
        /// </summary>
        public void ResetTimeoutCount()
        {
            m_cntTimeouts = 0;
            RaiseWorkUnitStatusChangedEvent();
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Raise Work Unit Status Changed Event Helper Method
        /// </summary>
        private void RaiseWorkUnitStatusChangedEvent()
        {
            if (null != WorkUnitStatusChangedEvent)
                WorkUnitStatusChangedEvent(this, new cAreaManagerWorkUnitEventArgs(this));
        }
        #endregion // Private Methods
    }

    #region Exception Class
    /// <summary>
    /// Exception raised with any Work Unit problems
    /// </summary>
    public class exWorkUnitException : Exception
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Associated workunit.
        /// </summary>
        public cWorkUnit WorkUnit
        {
            get { return m_WorkUnit; }
        }
        private cWorkUnit m_WorkUnit;
        #endregion // Properties and Associated Member Data

        #region Constructors
        public exWorkUnitException(cWorkUnit unit)
             : base( )
        {
            m_WorkUnit = unit;
        }

        public exWorkUnitException(cWorkUnit unit, String sMessage)
             : base(sMessage)
        {
            m_WorkUnit = unit;
        }

        public exWorkUnitException(cWorkUnit unit, String sMessage, Exception InnerException)
             : base(sMessage, InnerException)
        {
            m_WorkUnit = unit;
        }

        public exWorkUnitException(cWorkUnit unit, System.Runtime.Serialization.SerializationInfo info, 
                                   System.Runtime.Serialization.StreamingContext context)
             : base(info, context)
        {
            m_WorkUnit = unit;
        }
        #endregion // Constructors
    }
    #endregion

} // End of DistMonitor.Game namespace


// End of file
