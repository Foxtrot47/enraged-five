//
// File: uiMachineForm.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of uiMachineForm.cs class
//

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Windows.Forms;

using RSG.Base.Logging;

namespace DistMonitor.UI
{

    /// <summary>
    /// Machine Configuration Form
    /// </summary>
    internal partial class uiMachineForm : Form
    {
        #region Properties
        /// <summary>
        /// Machine Collection (read access)
        /// </summary>
        public cMachineCollection Machines
        {
            get { return m_Machines; }
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Mapping from Types to Indexes
        /// </summary>
        private List<Type> m_MachineTypeMapping;

        /// <summary>
        /// Machine Collection
        /// </summary>
        private cMachineCollection m_Machines;
        #endregion

        #region Contructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public uiMachineForm()
        {
            InitializeComponent();
            m_MachineTypeMapping = new List<Type>();
            m_Machines = new cMachineCollection();
            lvMachineView.Machines = m_Machines;

            m_Machines.MachineAddedEvent += lvMachineView.OnMachineAdd;
            m_Machines.MachineRemovedEvent += lvMachineView.OnMachineRemove;

            UpdateMachinesTypes();

            // Populate machine collection
            foreach (MachineInterface.iMachine machine in 
                            Program.Controller.MachineFarm.Machines)
            {
                m_Machines.Add(new cMachine(machine.IP, machine.ArchName));
            }
        }
        #endregion

        #region UI Event Handlers
        /// <summary>
        /// Add a Machine IP
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                IPAddress ip = IPAddress.Parse(txtMachineIP.Text);
                cMachine machine = new cMachine(ip, cbMachine.SelectedItem.ToString());

                m_Machines.Add(machine);
                lvMachineView.UpdateView();
            }
            catch (FormatException ex)
            {
                Log.Log__Error(String.Format("Machine {0} not added: {1}.",
                                   txtMachineIP.Text, ex.Message));
            }
        }

        /// <summary>
        /// Remove selected Machine IP
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (0 == lvMachineView.SelectedMachines.Count)
                return;

            try
            {
                List<cMachine> machines = lvMachineView.SelectedMachines;
                foreach (cMachine machine in machines)
                {
                    this.m_Machines.Remove(machine);
                }
            }
            catch (Exception ex)
            {
                Log.Log__Error(String.Format("Machine list inconsistent: {1}.",
                                   ex.Message));
            }
        }

        /// <summary>
        /// Selection Change Event Handler
        /// </summary>
        /// Update the enabled state of the Remove button.
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lvFarm_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            btnRemove.Enabled = (0 != lvMachineView.SelectedMachines.Count);
        }

        /// <summary>
        /// IP Address Text Changed Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMachineIP_TextChanged(object sender, EventArgs e)
        {
            btnAdd.Enabled = (0 != txtMachineIP.Text.Length);
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Update Machine Types Combobox (from Controller loaded plugins)
        /// </summary>
        private void UpdateMachinesTypes()
        {
            cbMachine.Items.Clear();
            Debug.Assert(Program.Controller.MachinePlugins.Plugins.Count > 0);

            foreach (Type machineType in Program.Controller.MachinePlugins.Plugins)
            {
                MachineInterface.iMachine machine =
                    Program.Controller.MachinePlugins.CreatePluginInstance(machineType);
                cbMachine.Items.Add(machine.Name);
                m_MachineTypeMapping.Add(machineType);
            }
            cbMachine.SelectedIndex = 0;
        }
        #endregion // Private Methods
    }

    #region Machine Collection Class
    /// <summary>
    /// Collection of Machines (private helper class)
    /// </summary>
    /// If you are outside of the uiMachineForm class you probably want to use
    /// the MachineInterface.iMachine interface instead.
    internal sealed class cMachineCollection : System.Collections.IEnumerable
    {
        #region Events
        /// <summary>
        /// Event raised when machine is added to our collection
        /// </summary>
        public event EventHandler<cMachineEventArgs> MachineAddedEvent;

        /// <summary>
        /// Event raised when machine is removed from our collection
        /// </summary>
        public event EventHandler<cMachineEventArgs> MachineRemovedEvent;
        #endregion // Events

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public int Count
        {
            get { return Machines.Count; }
        }

        #region [] Property Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public cMachine this[int index]
        {
            get { return Machines[index]; }
        }
        #endregion // Operator Overridden Methods
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Machine List (underlying data structure)
        /// </summary>
        private List<cMachine> Machines;
        #endregion // Member Data

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public cMachineCollection()
        {
            Machines = new List<cMachine>();
        }
        #endregion // Constructor

        #region Controller Methods
        /// <summary>
        /// Add a machine to the collection
        /// </summary>
        /// <param name="machine">Machine to add</param>
        public void Add(cMachine machine)
        {
            Machines.Add(machine);

            if (null != MachineAddedEvent)
                MachineAddedEvent(this, new cMachineEventArgs(machine));
        }

        /// <summary>
        /// Remove machine from collection
        /// </summary>
        /// <param name="machine">Machine to remove</param>
        public void Remove(cMachine machine)
        {
            Machines.Remove(machine);

            if (null != MachineRemovedEvent)
                MachineRemovedEvent(this, new cMachineEventArgs(machine));
        }
        #endregion // Controller Methods

        #region IEnumerable Overridden Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public System.Collections.IEnumerator GetEnumerator()
        {
            return (new cMachineCollectionIterator(this));
        }
        #endregion // IEnumerable Overridden Methods

        #region public methods
        public void Deduplicate()
        {
            // DW - in .Net3.5 consider a HashSet - a bit easier unless we need a set order?
            Machines.Sort();
            Int32 index = 0;
            while (index < Machines.Count - 1)
            {
                if (Machines[index].CompareTo(Machines[index + 1]) == 0)
                    Machines.RemoveAt(index);
                else
                    index++;
            }
        }
        #endregion // public methods
    };

    /// <summary>
    /// Machine Collection Iterator Class
    /// </summary>
    internal sealed class cMachineCollectionIterator : System.Collections.IEnumerator
    {
        #region Properties
        /// <summary>
        /// Current object
        /// </summary>
        public Object Current
        {
            get 
            {
                if (m_nIndex <= -1)
                    throw new InvalidOperationException("Index out-of-range.");
                return (m_MachineCollection[m_nIndex]);
            }
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Associated machine collection
        /// </summary>
        private cMachineCollection m_MachineCollection;

        /// <summary>
        /// Current index
        /// </summary>
        private int m_nIndex;
        #endregion // Member Data

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="collection"></param>
        public cMachineCollectionIterator(cMachineCollection collection)
        {
            m_MachineCollection = collection;
            m_nIndex = -1;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool MoveNext()
        {
            ++m_nIndex;
            if (m_nIndex < m_MachineCollection.Count)
                return true;
            else
            {
                m_nIndex = -1;
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Reset()
        {
            m_nIndex = -1;
        }
    };
    #endregion // Machine Collection Class

    /// <summary>
    /// Quick and dirty Machine abstraction (private helper class)
    /// </summary>
    /// This saves creating instance of the machine plugin implementations of
    /// MachineInterface.iMachine just to configure our farm.
    /// 
    /// If you are outside of the uiMachineForm class you probably want to use
    /// the MachineInterface.iMachine interface instead.
    public sealed class cMachine : IComparable
    {
        #region Properties
        /// <summary>
        /// Machine IP Address
        /// </summary>
        public IPAddress IP
        {
            get { return m_IP; }
        }
        private IPAddress m_IP;

        /// <summary>
        /// Machine Architecture String
        /// </summary>
        public String Arch
        {
            get { return m_sArch; }
        }
        private String m_sArch;
        #endregion // Properties

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="ip">IP Address</param>
        /// <param name="arch">Architecture String</param>
        public cMachine(IPAddress ip, String arch)
        {
            m_IP = ip;
            m_sArch = arch;
        }
        #endregion // Constructor

        #region Interface obligations
        public int CompareTo(object obj)
        {
            cMachine otherMachine = obj as cMachine;
            if (otherMachine != null)
                return this.IP.ToString().CompareTo(otherMachine.IP.ToString());
            else
                throw new ArgumentException("Object is not a cMachine");
        }
        #endregion // Interface obligations

    };

} // End of DistMonitor.UI namespace

// End of file
