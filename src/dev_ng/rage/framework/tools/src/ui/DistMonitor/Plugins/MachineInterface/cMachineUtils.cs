//
// File: cMachineUtils.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of cMachineUtils.cs class
//
// References: http://www.c-sharpcorner.com/UploadFile/saurabhnandu/PingUtilityinCS12032005042616AM/PingUtilityinCS.aspx
//
//

using System;
using System.Diagnostics;
using System.Net;
using System.Net.NetworkInformation;

using RSG.Base.Logging;

namespace MachineInterface
{
    
    /// <summary>
    /// Common Machine Utilities Class
    /// </summary>
    /// Set of static methods that should be shareable/useful between all of
    /// the Machine Plugin implementations.
    /// 
    public class cMachineUtils
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="args"></param>
        /// <param name="waitSeconds"></param>
        /// <returns>true iff process exits ok, false otherwise</returns>
        public static bool ExecuteWaitAndLogResult(String cmd, String args, int waitSeconds)
        {
            Log.Log__Warning(String.Format("*** ExecuteWaitAndLogResult: {0} {1}", cmd, args));

            Process proc = new Process();
            proc.StartInfo.CreateNoWindow = true;
            proc.StartInfo.UseShellExecute = false;
            proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            proc.StartInfo.FileName = cmd;
            proc.StartInfo.Arguments = args;
            proc.Start();

            proc.WaitForExit(waitSeconds * 1000);

            if (proc.HasExited)
            {
                if (0 != proc.ExitCode)
                {
                    Log.Log__Debug(String.Format("Execution of {0} ({1}) failed with exit code: {2}.",
                                       cmd, args, proc.ExitCode));
                }
                else
                {
                    Log.Log__Debug(String.Format("Execution of {0} ({1}) succeeded with exit code: {2}.",
                                       cmd, args, proc.ExitCode));
                }
                return (0 == proc.ExitCode);
            }

            // Assume process will exit ok
            return (true);
        }

        public static long PingIP(IPAddress ip)
        {
            Ping pinger = new Ping();
            
            PingReply reply = pinger.Send(ip, 1000);
            if (reply.Status == IPStatus.Success)
                return (reply.RoundtripTime);
            else
                return (long.MaxValue);
        }

        public static long PingHost(String host)
        {
            try
            {
                IPHostEntry ipHost = Dns.GetHostEntry(host);
                return (PingIP(ipHost.AddressList[0]));
            }
            catch (Exception)
            {
                throw new exPingException(String.Format("DNS could not resolve host: {0}.", host));
            }
        }
    }
    
    /// <summary>
    /// Ping Exception Class
    /// </summary>
    /// An instance of this exception class is thrown when there is an error
    /// in the ping utility methods above.
    public class exPingException : Exception
    {
        #region Constructors
        public exPingException()
            : base( )
        {
        }

        public exPingException(String sMessage)
             : base(sMessage)
        {
        }

        public exPingException(String sMessage, Exception InnerException)
             : base(sMessage, InnerException)
        {
        }

        public exPingException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
             : base(info, context)
        {
        }
        #endregion
    }


} // End of MachineInterface namespace

// End of file
