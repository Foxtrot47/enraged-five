//
// File: cMachineMonitor.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of cMachineMonitor.cs class
//

using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

using RSG.Base.Logging;

namespace DistMonitor
{

    /// <summary>
    /// Monitors and controls a machine
    /// </summary>
    /// This is where hopefully all the multi-threading stuff can go.
    internal sealed class cMachineMonitor : IDisposable
    {
        #region Events
        /// <summary>
        /// Event raised when any status member is updated
        /// </summary>
        /// 
        public event EventHandler<EventArgs> MonitorStatusChangeEvent;

        /// <summary>
        /// Event raised when monitor detects a change of machine status
        /// </summary>
        /// E.g. if the ping utility times-out then we set the status of the
        /// machine to Off.  As its obviously become unreachable.
        /// 
        public event EventHandler<MachineInterface.cMachineStatusEventArgs> MachineStatusChangeEvent;
        #endregion

        #region Properties and Associated Member Data
        /// <summary>
        /// Machine this is monitoring
        /// </summary>
        public MachineInterface.iMachine Machine 
        {
            get { return m_Machine; }
            private set { m_Machine = value; }
        }
        private MachineInterface.iMachine m_Machine;

        /// <summary>
        /// Monitoring active?
        /// </summary>
        public bool Monitoring
        {
            get { return m_bMonitoring; }
            set { m_bMonitoring = value; }
        }
        private bool m_bMonitoring;

        /// <summary>
        /// Machine Latency (milliseconds)
        /// </summary>
        public long Latency
        {
            get { return m_msLatency; }
            private set { m_msLatency = value; }
        }
        private long m_msLatency;

        /// <summary>
        /// Machine Failures 
        /// </summary>
        public Int32 Failures
        {
            get { return m_msFailures; }
            private set { m_msFailures = value; }
        }
        private Int32 m_msFailures;

        /// <summary>
        /// Machine Failed 
        /// </summary>
        public bool Failed
        {
            get { return m_msFailed; }
            private set { m_msFailed = value; }
        }
        private bool m_msFailed;


        /// <summary>
        /// Machine Log File
        /// </summary>
        public GenericLogFile LogFile
        {
            get { return m_LogFile; }
            private set { m_LogFile = value; }
        }
        private GenericLogFile m_LogFile;
        #endregion // Properties and Associated Member Data

        #region Member Data
        /// <summary>
        /// Previous Machine's status
        /// </summary>
        private MachineInterface.etMachineStatus m_LastStatus;
        #endregion // Member Data

        #region Constructor / Destructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="machine"></param>
        /// <param name="controller"></param>
        /// <param name="startMonitorThread"></param>
        public cMachineMonitor(MachineInterface.iMachine machine)
        {
            Initialise(machine, true);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="machine"></param>
        /// <param name="controller"></param>
        /// <param name="startMonitorThread"></param>
        public cMachineMonitor(MachineInterface.iMachine machine,
                               bool startMonitorThread)
        {
            Initialise(machine, startMonitorThread);
        }

        /// <summary>
        /// Destructor
        /// </summary>
        ~cMachineMonitor()
        {
            Dispose();
        }

        public void Dispose()
        {
            Monitoring = false;
        }
        #endregion // Constructor / Destructor

        #region Controller Methods
        #region Starting
        /// <summary>
        /// Monitor Machine
        /// </summary>
        public void Start( )
        {
            // Initialise tool (if selected)
            if (null != Program.Controller.Tool)
            {
                Program.Controller.Tool.Initialise(Machine);
            }

            Program.Controller.ThreadPool.QueueWorkItem(
                new RSG.Base.Threading.WorkItemCallback(this.StartThreadFunc),
                String.Format("Start_{0}", Machine.IP));
        }
        #endregion // Starting

        #region Stopping
        /// <summary>
        /// Stop Machine and Reboot
        /// </summary>
        public void StopAndReboot()
        {
            Program.Controller.ThreadPool.QueueWorkItem(
                new RSG.Base.Threading.WorkItemCallback(this.RebootThreadFunc),
                String.Format("StopAndReboot_{0}", Machine.IP));
        }
        #endregion // Stopping

        #region Rebooting
        public void Reboot(MachineInterface.etRebootType type)
        {
            switch (type)
            {
                case MachineInterface.etRebootType.Cold:
                    Program.Controller.ThreadPool.QueueWorkItem(
                        new RSG.Base.Threading.WorkItemCallback(this.RebootColdThreadFunc),
                        String.Format("RebootCold_{0}", Machine.IP));
                    break;
                case MachineInterface.etRebootType.Warm:
                    Program.Controller.ThreadPool.QueueWorkItem(
                        new RSG.Base.Threading.WorkItemCallback(this.RebootThreadFunc),
                        String.Format("RebootWarm_{0}", Machine.IP));
                    break;
            }
        }
        #endregion // Rebooting

        #region Status
        /// <summary>
        /// Monitor Machine, reporting status changes
        /// </summary>
        private void Monitor()
        {
            Log.Log__Message(String.Format("Monitoring thread running for {0}.",
                                 Machine.IP.ToString()));
            Monitoring = true;
            Program.Controller.ThreadPool.QueueWorkItem(
                new RSG.Base.Threading.WorkItemCallback(this.MonitorThreadFunc),
                String.Format("Monitor_{0}", Machine.IP));
        }

        /// <summary>
        /// Ping Machine and Monitor Result
        /// </summary>
        public void Ping()
        {
            Program.Controller.ThreadPool.QueueWorkItem(
                new RSG.Base.Threading.WorkItemCallback(this.PingThreadFunc),
                String.Format("Ping_{0}", Machine.IP));
        }
        #endregion // Status
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Common initialisation method
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="machine"></param>
        /// <param name="startMonitorThread"></param>
        private void Initialise(MachineInterface.iMachine machine,
                                bool startMonitorThread)
        {
            Machine = machine;
            Latency = 0;
            Failures = 0;
            Failed = false;
            LogFile = new GenericLogFile(String.Format("{0}.log", machine.IP));
            Machine.DebugLog.DebugTextEvent += OnDebugLogTextEvent;
            m_LastStatus = MachineInterface.etMachineStatus.Unknown;

            // Start monitoring the machine
            if (startMonitorThread)
                this.Monitor();
        }

        /// <summary>
        /// Machine Debug Log Text Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnDebugLogTextEvent(Object sender, MachineInterface.cDebugLogTextEventArgs e)
        {
            LogFile.LogMessage(sender, new LogMessageEventArgs(e.Text));
        }
        #endregion

        #region ThreadPool Invocation Methods
        #region Starting
        private Object StartThreadFunc(Object stateInfo)
        {
            try
            {
                if (null == Thread.CurrentThread.Name)
                    Thread.CurrentThread.Name = (String)(stateInfo);

                // Invoke Tool Start method (if applicable, otherwise
                // invoke our Machine.Start method to run game.
                if (Machine.Enabled && Machine.Available)
                {
                    if (null != Program.Controller.Tool)
                        Program.Controller.Tool.Start(Program.Controller.Game, Program.Controller.Configuration, Machine);
                    else
                        Machine.Start(Program.Controller.Game, Program.Controller.Configuration);
                }
            }
            catch (Exception ex)
            {
                Log.Log__Error(String.Format("Exception [{0}]: during machine monitoring task. {1}.",
                                   Thread.CurrentThread.Name, ex.Message));
            }
            return (true);
        }
        #endregion // Starting

        #region Rebooting
        private Object RebootThreadFunc(Object stateInfo)
        {
            try
            {
                if (null == Thread.CurrentThread.Name)
                    Thread.CurrentThread.Name = (String)(stateInfo);
                Machine.Reboot();
            }
            catch (Exception ex)
            {
                Log.Log__Error(String.Format("Exception [{0}]: during machine reboot task. {1}.",
                                   Thread.CurrentThread.Name, ex.Message));
            }
            return (true);
        }

        private Object RebootColdThreadFunc(Object stateInfo)
        {
            try
            {
                if (null == Thread.CurrentThread.Name)
                    Thread.CurrentThread.Name = (String)(stateInfo); 
                Machine.Reboot(MachineInterface.etRebootType.Cold);
            }
            catch (Exception ex)
            {
                Log.Log__Error(String.Format("Exception [{0}]: during machine cold reboot task. {1}.",
                                   Thread.CurrentThread.Name, ex.Message));
            }
            return (true);
        }
        #endregion

        #region Status
        private Object MonitorThreadFunc(Object stateInfo)
        {
            try
            {
                // Initialisation
                if (null == Thread.CurrentThread.Name)
                    Thread.CurrentThread.Name = (String)(stateInfo);

                // Monitoring Loop
                while (m_bMonitoring)
                {
                    try
                    {
                        MonitorMachineStatus();

                        // Invoke Monitor method for active tool (through ToolInterface)
                        if ((Machine.Enabled && Machine.Available) &&
                            (cController.etState.Running == Program.Controller.State) &&
                            (null != Program.Controller.Tool))
                            Program.Controller.Tool.Monitor(Machine);

                        Thread.Sleep(Properties.Settings.Default.MonitorInterval * 1000);
                        //cLogger.Log__Debug(String.Format("Monitor for Machine {0} alive.", Machine.IP));
                    }
                    catch (Exception ex)
                    {
                        Log.Log__Warning(String.Format("Monitor for Machine {0} exception: {1}.",
                                             Machine.IP, ex.Message));
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Log__Error(String.Format("Exception [{0}]: during machine monitoring task. {1}.",
                                   Thread.CurrentThread.Name, ex.Message));
            }
            return (true);
        }

        private Object PingThreadFunc(Object stateInfo)
        {
            try
            {
                if (null == Thread.CurrentThread.Name)
                    Thread.CurrentThread.Name = (String)(stateInfo);
                Latency = Machine.Ping();

                RaiseMonitorStatusChangeEvent();
                if (Latency == long.MaxValue)
                    RaiseMachineStatusChangeEvent(MachineInterface.etMachineStatus.Off);
            }
            catch (Exception ex)
            {
                Log.Log__Error(String.Format("Exception [{0}]: during machine ping task. {1}.",
                                   Thread.CurrentThread.Name, ex.Message));
            }
            return (true);
        }
        #endregion // Status

        /// <summary>
        /// Raise Status Change Event (if we have listeners)
        /// </summary>
        private void RaiseMonitorStatusChangeEvent()
        {
            if (null != MonitorStatusChangeEvent)
                MonitorStatusChangeEvent(this, new EventArgs());
        }

        private void RaiseMachineStatusChangeEvent(MachineInterface.etMachineStatus status)
        {
            if (null != MachineStatusChangeEvent)
                MachineStatusChangeEvent(this, new MachineInterface.cMachineStatusEventArgs(this.Machine, status));
        }
        #endregion

        #region Common Monitoring Functions
        /// <summary>
        /// Monitor Machine Status
        /// </summary>
        private void MonitorMachineStatus()
        {
            if (m_LastStatus != Machine.Status)
            {
                m_LastStatus = Machine.Status;
            }
            /* DW/DM - this is suspected to the indirect cause of the model and view having a differnt idea about whether a machine is on or not.
             * so the fix is to not attempt to connect to off machines because it sets the machine to be on
                        else if ((Machine.Status == MachineInterface.etMachineStatus.Off) ||
                                 (!Machine.DebugLog.Connected))

             */
            else if (!Machine.DebugLog.Connected)
            {
                // Machine is off or at least not connected to our debug log
                // so we should try to reconnect, in order to update its status.
                Machine.DebugLog.ConnectTo(Machine);
            }

            // Update latency (irrespective of machine status)
            Latency = MachineInterface.cMachineUtils.PingIP(Machine.IP);
            if (Latency == long.MaxValue)
                RaiseMachineStatusChangeEvent(MachineInterface.etMachineStatus.Off);

            Failures = Machine.Failures;
            Failed = Machine.Failed;

            RaiseMonitorStatusChangeEvent();
        }
        #endregion // Common Monitoring Functions
    }

} // End of DistMonitor namespace

// End of file
