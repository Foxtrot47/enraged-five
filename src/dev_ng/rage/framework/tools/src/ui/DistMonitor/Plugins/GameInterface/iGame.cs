//
// File: iGame.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of iGame.cs class
//

using System;
using System.Collections.Generic;
using System.Text;

namespace GameInterface
{

    /// <summary>
    /// Game Data Interface
    /// </summary>
    /// Links Game Data and Area Tools available for that game together.
    /// This is implemented as an interface so we can support additional
    /// ways of reading such game data.
    /// 
    /// At the moment this comes from an XML data file (see cGameXML) but
    /// in future could be read from a SQL Database.
    public interface iGame
    {
        #region Interface Properties
        /// <summary>
        /// Game name
        /// </summary>
        String Name { get; }
        
        /// <summary>
        /// Game root directory (local)
        /// </summary>
        String RootDirectory { get; }

        float SectorSize { get; }

        /// <summary>
        /// List of available game configurations
        /// </summary>
        List<cConfiguration> Configurations { get; }

        /// <summary>
        /// Active Configuration (selected on UI)
        /// </summary>
        cConfiguration ActiveConfiguration { get; set; }
        #endregion // Interface Properties

        #region Interface Methods
        #endregion
    }

} // End of GameInterface namespace

// End of file
