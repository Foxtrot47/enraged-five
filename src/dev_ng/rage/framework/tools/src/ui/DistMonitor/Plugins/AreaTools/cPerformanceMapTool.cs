//
// File: cPerformanceMapTool.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of cPerformanceMapTool.cs class
//

using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

using RSN.Base.Logging;

namespace AreaTools
{

    /// <summary>
    /// 
    /// </summary>
    public sealed class cPerformanceMapTool : cPerformanceTool, RSN.Base.Plugins.iPlugin
    {        
        #region Constants
        private readonly String NAME = "Performance Statistics Capture (Map Only)";
        private readonly String DESC = "Rockstar Game Performance Statistics Capture Plugin (Map Only)";
        #endregion // Constants

        #region Properties and Associated Member Data
        public override String Name
        {
            get { return NAME; }
        }

        public override Version Version
        {
            get
            {
                Assembly assembly = Assembly.GetExecutingAssembly();
                return (assembly.GetName().Version);
            }
        }

        public override String Description
        {
            get { return DESC; }
        }
        #endregion // Properties and Asscociated Member Data

        #region Controller Methods
        public override void Start(GameInterface.iGame game, 
                                   GameInterface.cConfiguration config,
                                   MachineInterface.iMachine machine)
        {
            lock (ms_WorkUnitLock)
            {
                // So we recreate our FileMonitor to point to the current date's file, if
                // we are running overnight and pass a date boundary.
                this.Initialise(machine);

                m_Game = game;
                m_Configuration = config;

                // Fetch current or next work unit for machine.
                cWorkUnit wu = this.AreaManager.CurrentWorkUnit(machine);
                bool resume = (null != wu) ? true : false;

                if (null == wu)
                    wu = this.AreaManager.NextWorkUnit(machine);

                // Terminate as no outstanding workunits and machine is idle.
                if (null == wu)
                {
                    cLogger.Log__Message(String.Format("Machine {0} has no remaining work units, queue empty.", machine.IP));
                    return;
                }

                // Otherwise have work to do so lets start it...
                String sArguments;
                // If we are resuming and have a last sector specified...
                if (resume && (null != this.AreaManager.FileMonitors[machine.IP].LastSector))
                    sArguments = String.Format("-fuckoff -nocars -nopeds -noambient -sectortools -runperf -sx {0} -sy {1} -ex {2} -ey {3} -rx {4} -ry {5}",
                                               wu.StartWorld.X, wu.StartWorld.Y, wu.EndWorld.X, wu.EndWorld.Y,
                                               this.AreaManager.FileMonitors[machine.IP].LastSector.X,
                                               this.AreaManager.FileMonitors[machine.IP].LastSector.Y);
                // Otherwise we just fire up with our sub-area coordinates
                else
                    sArguments = String.Format("-fuckoff -nocars -nopeds -noambient -sectortools -runperf -sx {0} -sy {1} -ex {2} -ey {3}",
                                               wu.StartWorld.X, wu.StartWorld.Y, wu.EndWorld.X, wu.EndWorld.Y);

                if (this.GameTime == "midnight")
                    sArguments += " -midnight ";

                machine.Start(m_Game, m_Configuration, sArguments);
            }
        }
        #endregion // Controller Methods
    }

} // End of AreaTools namespace

// End of file
