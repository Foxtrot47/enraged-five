//
// File: cScheduledTaskManager.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of cScheduledTaskManager.cs class
//

using System;
using System.Collections.Generic;
using System.Threading;
using System.IO;
using System.Text;
using System.Xml;

using RSG.Base.Logging;

namespace DistMonitor.Scheduler
{

    /// <summary>
    /// Scheduled Task Manager Class
    /// </summary>
    internal class cScheduledTaskManager
    {
        #region Constants
        private const int INTERVAL_POLL = 120000;
        #endregion // Constants

        #region Enumerations
        /// <summary>
        /// 
        /// </summary>
        private enum etState
        {
            Idle,
            TaskRunning
        }
        #endregion // Enumerations

        #region Events
        /// <summary>
        /// Event raised when a Scheduled Task is started
        /// </summary>
        public event EventHandler<cScheduledTaskEventArgs> TaskStartedEvent;

        /// <summary>
        /// Event raised when a Scheduled Task is finished
        /// </summary>
        public event EventHandler<cScheduledTaskEventArgs> TaskFinishedEvent;

        /// <summary>
        /// Event raised when a Scheduled Task is added
        /// </summary>
        public event EventHandler<cScheduledTaskEventArgs> TaskAddedEvent;

        /// <summary>
        /// Event raised when a Scheduled Task is removed
        /// </summary>
        public event EventHandler<cScheduledTaskEventArgs> TaskRemovedEvent;

        /// <summary>
        /// Event raised when Scheduled Task Manager is enabled or disabled
        /// </summary>
        public event EventHandler<EventArgs> SchedulerStateChangeEvent;

        /// <summary>
        /// Event raised when a managed Scheduled Task is changed
        /// </summary>
        public event EventHandler<cScheduledTaskEventArgs> TaskChangedEvent;
        #endregion // Events

        #region Properties and Associated Member Data
        /// <summary>
        /// Enabled flag
        /// </summary>
        public bool Enabled
        {
            get { return m_bEnabled; }
            set 
            {
                m_bEnabled = value;

                if (null != Logger)
                    Logger.Log__Warning(this.Enabled ? "Scheduler Enabled." : "Scheduler Disabled.");

                if (null != SchedulerStateChangeEvent)
                    SchedulerStateChangeEvent(this, new EventArgs());
            }
        }
        private bool m_bEnabled;

        /// <summary>
        /// List of Scheduled Tasks
        /// </summary>
        public List<cScheduledTask> ScheduledTasks
        {
            get { return m_ScheduledTasks; }
            private set { m_ScheduledTasks = value; }
        }
        private List<cScheduledTask> m_ScheduledTasks;

        /// <summary>
        /// Application Thread Pool
        /// </summary>
        public RSG.Base.Threading.cThreadPool ThreadPool
        {
            get { return m_ThreadPool; }
            private set { m_ThreadPool = value; }
        }
        private RSG.Base.Threading.cThreadPool m_ThreadPool;

        /// <summary>
        /// Scheduler Logger Object
        /// </summary>
        public RSG.Base.Logging.GenericLogger Logger
        {
            get { return m_Logger; }
            private set { m_Logger = value; }
        }
        private RSG.Base.Logging.GenericLogger m_Logger;
        #endregion // Properties and Associated Member Data

        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private etState m_eState;
        #endregion // Member Data

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public cScheduledTaskManager(RSG.Base.Threading.cThreadPool pool, 
                                     DistMonitor.UI.uiFarmLog farmlog)
        {
            this.m_eState = etState.Idle;
            this.m_ThreadPool = pool;
            this.Enabled = false;
            this.Logger = new RSG.Base.Logging.GenericLogger();
            this.ScheduledTasks = new List<cScheduledTask>();

            this.Logger.LogError += farmlog.SchedulerLog.LogError;
            this.Logger.LogWarning += farmlog.SchedulerLog.LogWarning;
            this.Logger.LogMessage += farmlog.SchedulerLog.LogMessage;
#if DEBUG
            this.Logger.LogDebug += farmlog.SchedulerLog.LogDebug;
#endif // DEBUG

            Logger.Log__Message("DistMonitor Scheduler Initialised.");
            Logger.Log__Message("Starting scheduler thread.");

            this.m_ThreadPool.QueueWorkItem(
                new RSG.Base.Threading.WorkItemCallback(this.SchedulerThreadFunc), null);
        }
        #endregion // Constructor

        #region Controller Methods
        /// <summary>
        /// Add task
        /// </summary>
        /// <param name="task"></param>
        public void AddTask(cScheduledTask task)
        {
            if (null == task)
                throw new ArgumentNullException("Task argument is null");

            task.TaskChangedEvent += this.OnScheduledTaskChange;
            this.ScheduledTasks.Add(task);

            if (null != this.TaskAddedEvent)
                TaskAddedEvent(this, new cScheduledTaskEventArgs(task));
        }

        /// <summary>
        /// Remove task
        /// </summary>
        /// <param name="task"></param>
        public void RemoveTask(cScheduledTask task)
        {
            if (null == task)
                throw new ArgumentNullException("Task argument is null");

            this.ScheduledTasks.Remove(task);
            task.TaskChangedEvent -= this.OnScheduledTaskChange;

            if (null != this.TaskRemovedEvent)
                TaskRemovedEvent(this, new cScheduledTaskEventArgs(task));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="task"></param>
        public void StartTask(cScheduledTask task)
        {
            if (etState.TaskRunning == m_eState)
            {
                Logger.Log__Error(String.Format("Cannot start task {0} as task already running.",
                                  task.Name));
                return;
            }

            m_eState = etState.TaskRunning;
            Logger.Log__Message(String.Format("Starting task {0}.", task.Name));

            // Immediately calculate next invoke time (so we don't invoke task again)
            Logger.Log__Message(String.Format("Pre CalculateNextInvokeTime Next {0} End {1}", task.Schedule.NextTime.ToString(), task.Schedule.EndTime.ToString()));
            task.Schedule.CalculateNextInvokeTime();
            Logger.Log__Message(String.Format("Post CalculateNextInvokeTime Next {0} End {1}", task.Schedule.NextTime.ToString(), task.Schedule.EndTime.ToString()));

            // Resolve task configuration to our plugins classes
            GameInterface.iGame game = null;
            GameInterface.cConfiguration config = null;
            ToolInterface.iTool tool = null;

            // Find game and configuration
            foreach (GameInterface.iGame g in Program.Controller.GameList.Games)
            {
                if (g.Name == task.Game)
                {
                    game = g;
                    foreach (GameInterface.cConfiguration c in g.Configurations)
                    {
                        if (c.ToString() == task.Configuration)
                        {
                            config = c;
                            break;
                        }
                    }
                    break;
                }
            }
            // Find tool
            foreach (Type t in Program.Controller.ToolPlugins.Plugins)
            {
                ToolInterface.iTool toolInst = 
                    Program.Controller.ToolPlugins.CreatePluginInstance(t);
                if (toolInst.Name == task.Tool)
                {
                    tool = toolInst;
                    break;
                }
            }

            tool.ToolFinishedEvent += OnToolFinished;
            Program.Controller.Start(game, config, tool);

            if (null != TaskStartedEvent)
                TaskStartedEvent(this, new cScheduledTaskEventArgs(task));
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="task"></param>
        public void StopTask(cScheduledTask task)
        {
            // Immediately calculate next invoke time (so we don't invoke task again)
            Logger.Log__Message(String.Format("StopTask Pre CalculateNextInvokeTime Next {0} End {1}", task.Schedule.NextTime.ToString(), task.Schedule.EndTime.ToString()));
            task.Schedule.CalculateNextInvokeTime();
            Logger.Log__Message(String.Format("StopTask Post CalculateNextInvokeTime Next {0} End {1}", task.Schedule.NextTime.ToString(), task.Schedule.EndTime.ToString()));

            if (etState.TaskRunning != m_eState)
            {
                Logger.Log__Error(String.Format("Cannot stop task {0} as task not running.",
                                  task.Name));
                return;
            }

            m_eState = etState.Idle;
            Logger.Log__Message(String.Format("Stoping task {0}.", task.Name));

            Program.Controller.Stop();
        }

        /// <summary>
        /// Reset internal state (manual stopping tasks)
        /// </summary>
        public void ResetState()
        {
            this.m_eState = etState.Idle;
        }

        /// <summary>
        /// Load Scheduled Tasks from XML File
        /// </summary>
        /// <param name="filename">XML Filename</param>
        public void LoadXml(String filename)
        {
            Logger.Log__Message(String.Format("Loading Scheduled Tasks from XML: {0}.", 
                System.IO.Path.GetFullPath(filename)));
            
            if (!File.Exists(filename))
            {
                Logger.Log__Message(String.Format("\tScheduled Tasks XML file does not exist."));
                return;
            }

            try
            {
                using (XmlReader xmlReader = new XmlTextReader(filename))
                {
                    int nCount = 0;
                    System.Diagnostics.Debug.Assert(xmlReader.IsStartElement("ScheduledTaskManager"));
                    
                    // Read top-level attributes
                    for (int nAttr = 0; nAttr < xmlReader.AttributeCount; ++nAttr)
                    {
                        xmlReader.MoveToAttribute(nAttr);
                        if ("Enabled" == xmlReader.Name)
                            this.Enabled = bool.Parse(xmlReader.Value);
                        if ("Count" == xmlReader.Name)
                            nCount = int.Parse(xmlReader.Value);
                    }
                    for (int nTask = 0; nTask < nCount; ++nTask)
                    {
                        cScheduledTask task = new cScheduledTask();
                        task.DeserializeXml(xmlReader);
                        this.AddTask(task);

                        Logger.Log__Message("\tLoaded " + task.Name);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Log__Error(String.Format("Error loading Scheduled Tasks from {0}: {1}.",
                                   filename, ex.Message));
            }
        }

        /// <summary>
        /// Save Scheduled Tasks to XML File
        /// </summary>
        /// <param name="filename">XML Filename</param>
        public void SaveXml(String filename)
        {
            Logger.Log__Message(String.Format("Saving Scheduled Tasks to XML: {0}.", filename));

            try
            {
                using (XmlWriter xmlWriter = new XmlTextWriter(filename, Encoding.UTF8))
                {
                    xmlWriter.WriteStartDocument();
                    xmlWriter.WriteStartElement("ScheduledTaskManager");
                    xmlWriter.WriteAttributeString("Enabled", this.Enabled.ToString());
                    xmlWriter.WriteAttributeString("Count", this.ScheduledTasks.Count.ToString());
                    foreach (cScheduledTask task in this.ScheduledTasks)
                    {
                        task.SerializeXml(xmlWriter);
                    }
                    xmlWriter.WriteEndElement();
                    xmlWriter.WriteEndDocument();
                }
            }
            catch (Exception ex)
            {
                Log.Log__Error(String.Format("Error saving Scheduled Tasks to {0}: {1}.",
                                   filename, ex.Message));
            }
        }
        #endregion // Controller Methods

        #region Private Event Handlers
        /// <summary>
        /// Managed Scheduled Task Change Event Handler
        /// </summary>
        /// <param name="sender">Source of event</param>
        /// <param name="e">Event arguments object</param>
        private void OnScheduledTaskChange(Object sender, cScheduledTaskEventArgs e)
        {
            if ((null != sender) && (null != TaskChangedEvent))
                TaskChangedEvent(this, e);
        }

        /// <summary>
        /// Tool Finished Event Handler
        /// </summary>
        /// <param name="sender">Source of event</param>
        /// <param name="e">Event arguments object</param>
        private void OnToolFinished(Object sender, ToolInterface.cToolEventArgs e)
        {
            Log.Log__Message(String.Format("Tool {0} finished.", e.Tool.Name));
            m_eState = etState.Idle;

            // Disconnect
            e.Tool.ToolFinishedEvent -= OnToolFinished;
        }
        #endregion // Private Event Handlers

        #region Private Scheduler Thread Methods
        /// <summary>
        /// Scheduler Thread Function Loop
        /// </summary>
        private Object SchedulerThreadFunc(Object state)
        {
            // Initialisation
            if (null == Thread.CurrentThread.Name)
                Thread.CurrentThread.Name = "DistMonitor_SchedulerThread";

            while (true)
            {
                Logger.Log__Debug("Scheduler Monitor Thread Alive.");

                try
                {
                    if (this.Enabled)
                    {
                        // Loop through tasks checking time
                        //TODO add schedule time overlap detection... prevent this at configuration stage!
                        foreach (cScheduledTask task in ScheduledTasks)
                        {
                            if ((task.Schedule.LastTime != task.Schedule.NextTime) &&
                                (task.Schedule.NextTime <= DateTime.Now))
                            {
                                Log.Log__Message(String.Format("Schedule started at {0}", DateTime.Now.ToString()));
                                StartTask(task);

                                // DW - Save Scheduled Task Settings NOW - since if the run crashes for any reasonit will simply rerun the schedule as soon as you restart the app.
                                SaveXml(Properties.Settings.Default.TaskFile);
                            }
                            else if (task.Schedule.EndTime <= DateTime.Now)
                            {
                                // DW - stop this now, it has expired prematurely!
                                Log.Log__Message(String.Format("Scheduled task hit it's end time."));
                                StopTask(task);
                                Log.Log__Message(String.Format("The task should now be moved on."));

                                // DW - Save Scheduled Task Settings NOW - since if the run crashes for any reasonit will simply rerun the schedule as soon as you restart the app.
                                SaveXml(Properties.Settings.Default.TaskFile);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.Log__Error(String.Format("Scheduled Task Exception {1}.", ex.Message));
                }

                Thread.Sleep(Properties.Settings.Default.SchedulerInterval * 1000);
            }
        }
        #endregion // Private Scheduler Thread Methods
    }

} // End of DistMonitor.Scheduler namespace

// End of file
