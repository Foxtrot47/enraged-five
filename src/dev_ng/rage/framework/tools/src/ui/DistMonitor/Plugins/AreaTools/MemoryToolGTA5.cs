//
// File: cMemoryToolGTA5.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of cMemoryTool.cs class
//

using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

using RSG.Base.Logging;

namespace AreaTools
{

    /// <summary>
    /// 
    /// </summary>
    public sealed class MemoryToolGTA5 : MemoryToolBase, RSG.Base.Plugins.iPlugin
    {
        #region Constants
        private readonly String NAME = "Memory Statistics Capture: GTA5";
        private readonly String DESC = "Rockstar Game Memory Statistics Capture Plugin";
        private readonly String DIRECTORY = "K:\\streamGTA5\\stats\\memstats";
        #endregion // Constants

        #region Properties and Associated Member Data
        public override String Name
        {
            get { return NAME; }
        }

        public override Version Version
        {
            get
            {
                Assembly assembly = Assembly.GetExecutingAssembly();
                return (assembly.GetName().Version);
            }
        }

        public override String Description
        {
            get { return DESC; }
        }
        #endregion // Properties and Asscociated Member Data

        #region Static Member Data
        /// <summary>
        /// WorkUnit Request Code lock
        /// </summary>
        private static Object ms_WorkUnitLock = new Object();
        #endregion // Static Member Data

        #region Constructor(s)
        /// <summary>
        /// Machine Initialisation
        /// </summary>
        public override void Initialise(MachineInterface.iMachine machine)
        {
            Debug.Assert(GameTime.Length > 0, "Game Time not set.");

            String sDate = String.Format("{0:yyyy}{0:MM}{0:dd}", DateTime.Now);

            // DW - hack for date - clean up later 
            if (UseDate.Length > 0)
            {
                sDate = UseDate;
            }

            String sFilename = Path.Combine(this.OutputDirectory, this.GameLevel);
            sFilename = Path.Combine(sFilename, machine.ArchName);
            sFilename = Path.Combine(sFilename, GameTime);
            sFilename = Path.Combine(sFilename, sDate);
            String sProgressFilename = Path.Combine(sFilename, machine.IP.ToString() + ".txt");

            if (this.AreaManager.FileMonitors.ContainsKey(machine.IP))
                this.AreaManager.FileMonitors.Remove(machine.IP);
            this.AreaManager.FileMonitors.Add(machine.IP, new AreaTools.cAreaProgressFileMonitor(sProgressFilename));
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Return tool output directory.
        /// </summary>
        /// <returns></returns>
        public override String GetOutputDirectory()
        {
            return (DIRECTORY);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="game"></param>
        /// <param name="config"></param>
        /// <param name="machine"></param>
        public override void Start(GameInterface.iGame game,
                                   GameInterface.cConfiguration config,
                                   MachineInterface.iMachine machine)
        {
            lock (ms_WorkUnitLock)
            {
                // So we recreate our FileMonitor to point to the current date's file, if
                // we are running overnight and pass a date boundary.
                this.Initialise(machine);

                m_Game = game;
                m_Configuration = config;

                // Fetch current or next work unit for machine.
                cWorkUnit wu = this.AreaManager.CurrentWorkUnit(machine);
                bool resume = (null != wu) ? true : false;

                if (null == wu)
                    wu = this.AreaManager.NextWorkUnit(machine);

                // Terminate as no outstanding workunits and machine is idle.
                if (null == wu)
                {
                    Log.Log__Message(String.Format("Machine {0} has no remaining work units, queue empty.", machine.IP));
                    return;
                }

                // Otherwise have work to do so lets start it...
                String sArguments;
                if (resume && (null != this.AreaManager.FileMonitors[machine.IP].LastSector))
                    sArguments = String.Format("-nocars -nopeds -noambient -fuckoff -sectortools -runmem -sx {0} -sy {1} -ex {2} -ey {3} -rx {4} -ry {5}",
                                               wu.StartWorld.X, wu.StartWorld.Y, wu.EndWorld.X, wu.EndWorld.Y,
                                               this.AreaManager.FileMonitors[machine.IP].LastSector.X,
                                               this.AreaManager.FileMonitors[machine.IP].LastSector.Y);
                else
                    sArguments = String.Format("-nocars -nopeds -noambient -fuckoff -sectortools -runmem -sx {0} -sy {1} -ex {2} -ey {3}",
                                               wu.StartWorld.X, wu.StartWorld.Y, wu.EndWorld.X, wu.EndWorld.Y);

                sArguments += String.Format(" -sectordimx {0} -sectordimy {1} -buildlabel {2} -builddate {3} -buildlaunchtime {4}", 
                                            m_Game.SectorSize, 
                                            m_Game.SectorSize,
                                            SyncLabel,
                                            "Fuck nose",
                                            DateTime.Now.ToString());

                if (this.GameTime == "midnight")
                    sArguments += " -midnight ";
                if ("" == this.GameLevel || "none" == this.GameLevel)
                    sArguments += " -level=gta5 "; // DW GTA5 HACK
                else
                    sArguments += " -level=" + this.GameLevel + " "; // DW GTA5 HACK

                if ("" != this.UseDate)
                {
                    sArguments += " -usedate=" + this.UseDate + " ";
                }

                machine.Start(m_Game, m_Configuration, sArguments);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override void Finish()
        {
            if (UpdateWebserver)
            {
                Log.Log__Message("Inserting Memory Data into Database...");
                cDatabaseOutput.MemoryDataInsert(OutputFolderName);
            }
        }
        #endregion // Controller Methods
    }

} // End of AreaTools namespace

// End of file
