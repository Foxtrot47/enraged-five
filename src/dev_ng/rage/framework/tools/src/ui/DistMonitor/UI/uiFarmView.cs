//
// File: uiFarmView.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of uiFarmView.cs class
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace DistMonitor.UI
{

    /// <summary>
    /// Machine Farm View Component
    /// </summary>
    internal partial class uiFarmView : UserControl
    {
        #region Delegates
        /// <summary>
        /// Delegate for thread-safe Repopulating the ListView
        /// </summary>
        private delegate void RepopulateDelegate();

        /// <summary>
        /// Delegate for thread-safe Refreshing single Machine's ListViewItem
        /// </summary>
        private delegate void MachineViewUpdateDelegate(Object sender, MachineInterface.iMachine machine);

        /// <summary>
        /// Delegate for thread-safe Refreshing the ListView
        /// </summary>
        private delegate void MonitorUpdateDelegate(object sender, cFarmMonitorEventArgs e);
        
        /// <summary>
        /// Delegate for thread-safe Refreshing single Machine's ListViewItem
        /// </summary>
        private delegate void MachineStatusChangeDelegate(Object sender, MachineInterface.cMachineStatusEventArgs e);

        /// <summary>
        /// Delegate for thread-safe handling Machine Enabled Change event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private delegate void MachineEnabledChangeDelegate(Object sender, MachineInterface.cMachineEnabledEventArgs e);
        #endregion

        #region Events
        /// <summary>
        /// Event raised when a machine is selected
        /// </summary>
        public event EventHandler<cFarmViewMachineEventArgs> MachineSelectionChangedEvent;

        /// <summary>
        /// Event raised when checked status of machine's ListViewItem is changed
        /// </summary>
        public event EventHandler<cFarmViewMachineEnabledEventArgs> MachineCheckedChangedEvent;
        #endregion

        #region Properties and Associated Member Data
        /// <summary>
        /// Farm this view is viewing
        /// </summary>
        public cMachineFarm Farm
        {
            get { return m_Farm; }
            set { m_Farm = value; Repopulate(); }
        }
        private cMachineFarm m_Farm;

        /// <summary>
        /// Selected Machines
        /// </summary>
        public ListView.SelectedListViewItemCollection SelectedMachines
        {
            get { return (lvStatus.SelectedItems); }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public uiFarmView()
        {
            InitializeComponent();
            lvStatus.SelectedIndexChanged += OnSelectedIndexChanged;
            lvStatus.LargeImageList = imageList;
            lvStatus.SmallImageList = imageList;
        }
        #endregion // Constructor

        #region Private Methods
        /// <summary>
        /// Repopulate farm list
        /// </summary>
        private void Repopulate()
        {
            if (lvStatus.InvokeRequired)
            {
                Invoke(new RepopulateDelegate(Repopulate));
            }
            else
            {
                lvStatus.BeginUpdate();
                lvStatus.Items.Clear();
                imageList.Images.Clear();
                if (null != Farm)
                {
                    foreach (cMachineMonitor monitor in Farm.Monitors)
                    {
                        uiMonitorListViewItem item = new uiMonitorListViewItem(monitor, monitor.Machine.IP.ToString());
                        
                        // Check we have a group defined for the architecture
                        ListViewGroup archGroup = null;
                        foreach (ListViewGroup group in lvStatus.Groups)
                        {
                            if (group.Header == monitor.Machine.Name)
                            {
                                archGroup = group;
                                break;
                            }
                        }
                        if (null == archGroup)
                        {
                            archGroup = new ListViewGroup(monitor.Machine.Name);
                            lvStatus.Groups.Add(archGroup);
                        }
                        item.Group = archGroup;

                        imageList.Images.Add(monitor.Machine.IP.ToString(), new Bitmap(monitor.Machine.Bitmap));
                        lvStatus.Items.Add(item);
                    }
                }
                lvStatus.EndUpdate();
            }
        }
        #endregion

        #region Machine Farm Event Handlers
        /// <summary>
        /// Event handler for when machine is added to Machine Farm
        /// </summary>
        /// <param name="sender">Source of event (cMachineFarm)</param>
        /// <param name="e">Object containing machine concerned</param>
        public void OnMachineAdd(Object sender, cFarmMachineEventArgs e)
        {
            // DHM TODO!!!!
            Repopulate();
        }

        /// <summary>
        /// Event handler for when machine is removed from Machine Farm
        /// </summary>
        /// <param name="sender">Source of event (cMachineFarm)</param>
        /// <param name="e">Object containing machine concerned</param>
        public void OnMachineRemove(Object sender, cFarmMachineEventArgs e)
        {
            // DHM TODO!!!!
            Repopulate();
        }

        /// <summary>
        /// Event handler for when machine monitors are updated
        /// </summary>
        /// <param name="sender">Source of event (cMachineFarm)</param>
        /// <param name="e"></param>
        public void OnMonitorUpdate(Object sender, cFarmMonitorEventArgs e)
        {
            if (lvStatus.InvokeRequired)
            {
                Invoke(new MonitorUpdateDelegate(OnMonitorUpdate), new Object[] { sender, e });
            }
            else
            {
                foreach (ListViewItem item in this.lvStatus.Items)
                {
                    uiMonitorListViewItem monitorItem = (item as uiMonitorListViewItem);
                    if (monitorItem.Monitor == e.Monitor)
                    {
                        monitorItem.UpdateView();
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Event handler for when machine's status is changed
        /// </summary>
        /// <param name="sender">Source of event (cMachineFarm)</param>
        /// <param name="e"></param>
        public void OnMachineStatusChange(Object sender, MachineInterface.cMachineStatusEventArgs e)
        {
            if (lvStatus.InvokeRequired )
            {
                Invoke(new MachineStatusChangeDelegate(OnMachineStatusChange), new Object[] { sender, e });
            }
            else
            {
                foreach (ListViewItem item in this.lvStatus.Items)
                {
                    uiMonitorListViewItem monitorItem = (item as uiMonitorListViewItem);
                    if (monitorItem.Monitor.Machine == e.Machine)
                    {
                        monitorItem.UpdateView();
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender">Source of event (cMachineFarm)</param>
        /// <param name="e"></param>
        public void OnMachineEnabledChange(Object sender, MachineInterface.cMachineEnabledEventArgs e)
        {
            if (lvStatus.InvokeRequired)
            {
                Invoke(new MachineEnabledChangeDelegate(OnMachineEnabledChange), new Object[] { sender, e });
            }
            else
            {
                foreach (ListViewItem item in this.lvStatus.Items)
                {
                    uiMonitorListViewItem monitorItem = (item as uiMonitorListViewItem);
                    if (monitorItem.Monitor.Machine == e.Machine)
                    {
                        monitorItem.UpdateView();
                        break;
                    }
                }
            }
        }
        #endregion

        #region UI Event Handlers
        /// <summary>
        /// Event handler for ListView Selected Index Change, raises our published event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnSelectedIndexChanged(Object sender, EventArgs e)
        {
            if (null != MachineSelectionChangedEvent)
            {
                // For each selected machine...
                foreach (uiMonitorListViewItem item in lvStatus.SelectedItems)
                    MachineSelectionChangedEvent(this, new cFarmViewMachineEventArgs(item.Monitor.Machine));
            }
        }

        /// <summary>
        /// Machine Checked Status Changed, raises our published event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lvStatus_ItemChecked(Object sender, ItemCheckedEventArgs e)
        {
            if (null != MachineCheckedChangedEvent)
            {
                // For each selected machine...
                uiMonitorListViewItem item = (e.Item as uiMonitorListViewItem);
                MachineCheckedChangedEvent(this, new cFarmViewMachineEnabledEventArgs(item.Monitor.Machine, item.Checked));
            }
        }

        /// <summary>
        /// Context Menu Opening Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ctxMenuView_Opening(Object sender, CancelEventArgs e)
        {
            miShowGroups.Checked = lvStatus.ShowGroups;
            miMachine.Enabled = (lvStatus.SelectedItems.Count > 0);
            miCtxConfigure.Enabled = (cController.etState.Running != Program.Controller.State);
        }

        /// <summary>
        /// Context Menu Show Groups Item Event Handler
        /// </summary>
        /// <param name="sender">Show of event (ToolStripMenuItem)</param>
        /// <param name="e"></param>
        private void miShowGroups_Click(Object sender, EventArgs e)
        {
            lvStatus.ShowGroups = !lvStatus.ShowGroups;
        }

        /// <summary>
        /// Context Menu Refresh Event Handler
        /// </summary>
        /// <param name="sender">Source of event (ToolStripMenuItem)</param>
        /// <param name="e">Event arguments</param>
        private void miRefresh_Click(Object sender, EventArgs e)
        {
            foreach (ListViewItem item in lvStatus.Items)
            {
                uiMonitorListViewItem monitorItem = (item as uiMonitorListViewItem);
                monitorItem.UpdateView();
            }
        }

        /// <summary>
        /// Context Menu Machine Reboot Warm Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void miMachineReboot_Click(Object sender, EventArgs e)
        {
            foreach (ListViewItem item in lvStatus.SelectedItems)
            {
                uiMonitorListViewItem monitorItem = (item as uiMonitorListViewItem);
                monitorItem.Monitor.Machine.Reboot();
            }
        }

        /// <summary>
        /// Context Menu Machine Reboot Cold Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void miMachineRebootCold_Click(Object sender, EventArgs e)
        {
            foreach (ListViewItem item in lvStatus.SelectedItems)
            {
                uiMonitorListViewItem monitorItem = (item as uiMonitorListViewItem);
                monitorItem.Monitor.Machine.Reboot(MachineInterface.etRebootType.Cold);
            }
        }

        /// <summary>
        /// Context Menu Configure Farm Menu Item Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void miCtxConfigure_Click(object sender, EventArgs e)
        {
            Program.Controller.ConfigureFarm();
        }
        #endregion // UI Event Handlers

        private void lvStatus_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }

    /// <summary>
    /// ListViewItem representing a cMachineMonitor
    /// </summary>
    internal class uiMonitorListViewItem : ListViewItem
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// MachineMonitor this ListViewItem is representing
        /// </summary>
        public cMachineMonitor Monitor
        {
            get { return m_Monitor; }
            private set { m_Monitor = value; }
        }
        private cMachineMonitor m_Monitor;
        #endregion // Properties and Associated Member Data

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="machine"></param>
        public uiMonitorListViewItem(cMachineMonitor monitor, int imageIndex)
            : base("", imageIndex)
        {
            Monitor = monitor;
            Initialise();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="machine"></param>
        /// <param name="imageKey"></param>
        public uiMonitorListViewItem(cMachineMonitor monitor, String imageKey)
            : base("", imageKey)
        {
            Monitor = monitor;
            Initialise();
        }
        #endregion // Constructor

        #region Controller Methods
        /// <summary>
        /// Refresh item
        /// </summary>
        public void UpdateView()
        {
            this.Checked = Monitor.Machine.Enabled;
            this.SubItems[1].Text = Monitor.Machine.IP.ToString();            
            this.SubItems[2].Text = Monitor.Machine.Status.ToString();

            String sLatency = String.Empty;
            if (0 == Monitor.Latency)
                sLatency = String.Format("< 1");
            else if (long.MaxValue == Monitor.Latency)
                sLatency = "Timeout";
            else
                sLatency = String.Format("{0}", Monitor.Latency);

            this.SubItems[3].Text = sLatency;

            this.SubItems[4].Text = Monitor.Machine.Failures.ToString();
            this.SubItems[5].Text = Monitor.Machine.Failed.ToString();
            this.SubItems[6].Text = Monitor.Machine.Available.ToString();


            if (!Monitor.Machine.Enabled || Monitor.Machine.Status == MachineInterface.etMachineStatus.Off)
            {
                this.BackColor = System.Drawing.Color.LightGray;
            }
            else if (!Monitor.Machine.Available ||
                      Monitor.Machine.Failed ||
                    !(Monitor.Machine.Status == MachineInterface.etMachineStatus.On ||
                      Monitor.Machine.Status == MachineInterface.etMachineStatus.Running ||
                      Monitor.Machine.Status == MachineInterface.etMachineStatus.Rebooting) )
            {
                this.BackColor = System.Drawing.Color.Red;
            }
            else if (Monitor.Machine.Failures > 0)
            {
                this.BackColor = System.Drawing.Color.Yellow;
            }
            else 
            {
                this.BackColor = System.Drawing.Color.LightGreen;
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Initialisation
        /// </summary>
        private void Initialise()
        {
            this.Checked = Monitor.Machine.Enabled;
            this.SubItems.Add(new ListViewItem.ListViewSubItem(this, ""));
            this.SubItems.Add(new ListViewItem.ListViewSubItem(this, ""));
            this.SubItems.Add(new ListViewItem.ListViewSubItem(this, ""));
            this.SubItems.Add(new ListViewItem.ListViewSubItem(this, ""));
            this.SubItems.Add(new ListViewItem.ListViewSubItem(this, ""));
            this.SubItems.Add(new ListViewItem.ListViewSubItem(this, ""));

            UpdateView();
        }
        #endregion // Private Methods
    }

    #region Event Argument Classes
    /// <summary>
    /// 
    /// </summary>
    public class cFarmViewMachineEventArgs : EventArgs
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Machine corresponding to event
        /// </summary>
        public MachineInterface.iMachine Machine
        {
            get { return m_Machine; }
            private set { m_Machine = value; }
        }
        private MachineInterface.iMachine m_Machine;
        #endregion // Properties and Associated Member Data

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public cFarmViewMachineEventArgs(MachineInterface.iMachine machine)
        {
            Machine = machine;
        }
        #endregion // Constructor
    }

    /// <summary>
    /// 
    /// </summary>
    public class cFarmViewMachineEnabledEventArgs : cFarmViewMachineEventArgs
    {        
        #region Properties and Associated Member Data
        /// <summary>
        /// Enabled State
        /// </summary>
        public bool Checked
        {
            get { return m_bChecked; }
            private set { m_bChecked = value; }
        }
        private bool m_bChecked;
        #endregion // Properties and Associated Member Data

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public cFarmViewMachineEnabledEventArgs(MachineInterface.iMachine machine, bool check)
            : base(machine)
        {
            Checked = check;
        }
        #endregion // Constructor
    }
    #endregion // Event Argument Classes

} // End of DistMonitor.UI namespace

// End of file
