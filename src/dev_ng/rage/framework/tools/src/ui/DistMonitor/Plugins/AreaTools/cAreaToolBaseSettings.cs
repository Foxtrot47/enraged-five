//
// File: cAreaToolBaseSettings.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of cAreaToolBaseSettings.cs class
//

using System;
using System.Reflection;

namespace AreaTools
{

    /// <summary>
    /// Common Area Tools Settings Class
    /// </summary>
    [global::System.Configuration.SettingsGroupName("General")]
    public sealed class cAreaToolBaseSettings : ToolInterface.iToolSettings, RSG.Base.Plugins.iPlugin
    {
        #region Constants
        private String NAME = "Area Tools";
        private String DESC = "Common Area Tools Settings Plugin";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Name
        /// </summary>
        public override String Name
        {
            get { return NAME; }
        }

        public Version Version
        {
            get
            {
                Assembly assembly = Assembly.GetExecutingAssembly();
                return (assembly.GetName().Version);
            }
        }

        public String Description
        {
            get { return DESC; }
        }

        /// <summary>
        /// Database Performance Data insertion script
        /// </summary>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Configuration.SettingsDescriptionAttribute("Performance Database Script.")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("C:\\wamp\\www\\toolstats\\stats\\tools\\UpdatePerformanceStatsDB.php")]
        public String PerformanceDBScript
        {
            get { return ((String)(this["PerformanceDBScript"])); }
            set { this["PerformanceDBScript"] = value; }
        }

        /// <summary>
        /// Database Memory Data insertion script
        /// </summary>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Configuration.SettingsDescriptionAttribute("Memory Database Script.")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("C:\\wamp\\www\\toolstats\\stats\\tools\\UpdateMemoryUsageStatsDB.php")]
        public String MemoryDBScript
        {
            get { return ((String)(this["MemoryDBScript"])); }
            set { this["MemoryDBScript"] = value; }
        }

        /// <summary>
        /// Email From Address
        /// </summary>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Configuration.SettingsDescriptionAttribute("Email From Address.")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("distmonitor_areatools@rockstarnorth.com")]
        public String EmailFromAddress
        {
            get { return ((String)(this["EmailFromAddress"])); }
            set { this["EmailFromAddress"] = value; }
        }

        /// <summary>
        /// Mailing List XML File for plugin
        /// </summary>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Configuration.SettingsDescriptionAttribute("Mailing List XML File for plugin.")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Data\\AreaTools_MailingList.xml")]
        public String MailingList
        {
            get { return ((String)(this["MailingList"])); }
            set { this["MailingList"] = value; }
        }

        /// <summary>
        /// Mailing List XML File for plugin
        /// </summary>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Configuration.SettingsDescriptionAttribute("Public mailing List XML File for plugin.")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Data\\AreaTools_MailingListPublic.xml")]
        public String MailingListPublic
        {
            get { return ((String)(this["MailingListPublic"])); }
            set { this["MailingListPublic"] = value; }
        }

        /// <summary>
        /// Maximum timeout count before machine is disabled
        /// </summary>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Configuration.SettingsDescriptionAttribute("Maximum timeout count before machine is disabled.")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("3")]
        public int MaxTimeoutCount
        {
            get { return ((int)(this["MaxTimeoutCount"])); }
            set { this["MaxTimeoutCount"] = value; }
        }

        /// <summary>
        /// The threshold of the number of machine failures ( misc issues with a machine - be it machine related or not ) 
        /// ... that are unnacceptable.
        /// </summary>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Configuration.SettingsDescriptionAttribute("The threshold of the number of machine failures ")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("5")]
        public int UnacceptableFailures
        {
            get { return ((int)(this["UnacceptableFailures"])); }
            set { this["UnacceptableFailures"] = value; }
        }

        /// <summary>
        /// Maximum exceptions count before area is marked shafted
        /// </summary>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Configuration.SettingsDescriptionAttribute("Maximum exceptions count before area is marked shafted.")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("6")]
        public int MaxExceptionCount
        {
            get { return ((int)(this["MaxExceptionCount"])); }
            set { this["MaxExceptionCount"] = value; }
        }

        /// <summary>
        /// News entries directory
        /// </summary>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Configuration.SettingsDescriptionAttribute("News entry directory.")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("N:\\streamGTA\\stats\\news")]
        public String NewsDirectory
        {
            get { return ((String)(this["NewsDirectory"])); }
            set { this["NewsDirectory"] = value; }
        }

        /// <summary>
        /// News logs directory
        /// </summary>
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Configuration.SettingsDescriptionAttribute("News logs directory.")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("N:\\streamGTA\\stats\\news\\logs")]
        public String NewsLogsDirectory
        {
            get { return ((String)(this["NewsLogsDirectory"])); }
            set { this["NewsLogsDirectory"] = value; }
        }

        #endregion // Properties

        #region Static Properties and Member Data
        /// <summary>
        /// Static default object
        /// </summary>
        public static cAreaToolBaseSettings Default
        {
            get { return defaultInstance; }
        }
        private static cAreaToolBaseSettings defaultInstance =
            ((cAreaToolBaseSettings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new cAreaToolBaseSettings())));
        #endregion // Static Member Data
    }

} // End of AreaTools namespace

// End of file
