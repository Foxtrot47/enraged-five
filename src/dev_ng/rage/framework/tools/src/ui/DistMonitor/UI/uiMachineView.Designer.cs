namespace DistMonitor.UI
{
    partial class uiMachineView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvMachines = new System.Windows.Forms.ListView();
            this.hdrMachineIP = new System.Windows.Forms.ColumnHeader();
            this.hdrLatency = new System.Windows.Forms.ColumnHeader();
            this.SuspendLayout();
            // 
            // lvMachines
            // 
            this.lvMachines.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.hdrMachineIP,
            this.hdrLatency});
            this.lvMachines.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvMachines.FullRowSelect = true;
            this.lvMachines.Location = new System.Drawing.Point(0, 0);
            this.lvMachines.Name = "lvMachines";
            this.lvMachines.Size = new System.Drawing.Size(526, 150);
            this.lvMachines.TabIndex = 0;
            this.lvMachines.UseCompatibleStateImageBehavior = false;
            this.lvMachines.View = System.Windows.Forms.View.Details;
            // 
            // hdrMachineIP
            // 
            this.hdrMachineIP.Text = "IP Address";
            this.hdrMachineIP.Width = 152;
            // 
            // hdrLatency
            // 
            this.hdrLatency.Text = "Latency (ms)";
            this.hdrLatency.Width = 84;
            // 
            // uiMachineView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lvMachines);
            this.Name = "uiMachineView";
            this.Size = new System.Drawing.Size(526, 150);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lvMachines;
        private System.Windows.Forms.ColumnHeader hdrMachineIP;
        private System.Windows.Forms.ColumnHeader hdrLatency;
    }
}
