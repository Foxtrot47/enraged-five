//
// File: cGameXML.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of cGameXML.cs class
//

using System;
using System.Collections.Generic;
using System.Text;

namespace GameInterface
{

    /// <summary>
    /// Game definition loader from XML file
    /// </summary>
    public class cGameXML : iGame
    {
        #region Properties and Associated Member Data
        public String Name 
        {
            get { return m_sName; }
        }
        private String m_sName;

        public String RootDirectory 
        {
            get { return m_sRoot; } 
        }
        private String m_sRoot;

        public float SectorSize
        {
            get { return m_fSectorSize; }
        }
        private float m_fSectorSize;

        public List<cConfiguration> Configurations 
        {
            get { return m_Configurations; } 
        }
        private List<cConfiguration> m_Configurations;

        public cConfiguration ActiveConfiguration
        {
            get { return m_ActiveConfiguration; }
            set { m_ActiveConfiguration = value; }
        }
        private cConfiguration m_ActiveConfiguration;
        #endregion

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="root"></param>
        /// <param name="configs"></param>
        public cGameXML(String name, String root, float fSectorSize, List<cConfiguration> configs)
        {
            m_sName = name;
            m_sRoot = root;
            m_fSectorSize = fSectorSize;
            m_Configurations = configs;
        }
        #endregion
    }

} // End of GameInterface namespace

// End of file
