//
// File: uiAreaMapControl.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of uiAreaMapControl.cs class
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using RSG.Base.Math;

namespace AreaTools.UI
{

    /// <summary>
    /// 
    /// </summary>
    public partial class uiAreaMapControl : UserControl
    {
        #region Constants
        const float WORLD_WIDTH = 6000.0f;
        const float WORLD_HEIGHT = 6000.0f;
        const float IMAGE_WIDTH = 512.0f;
        const float IMAGE_HEIGHT = 512.0f;
        #endregion

        #region Delegates
        /// <summary>
        /// Work Units Change Event Delegate (thread-safe UI access)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private delegate void OnWorkUnitChangeDelegate(Object sender, AreaTools.cAreaManagerWorkUnitEventArgs e);

        /// <summary>
        /// Work Units Populated Event Delegate (thread-safe UI access)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private delegate void OnWorkUnitsPopulatedDelegate(Object sender, EventArgs e);
        #endregion // Delegates

        #region Properties and Associated Member Data
        public cAreaManager AreaManager
        {
            get { return m_AreaManager; }
            set { m_AreaManager = value; }
        }
        private cAreaManager m_AreaManager;
        #endregion // Properties and Associated Member Data

        #region Member Data
        private Image mapImage = null;
        #endregion // Member Data

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public uiAreaMapControl()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            this.mapImage = new Bitmap(Properties.Resources.gta5_map_gta5);
        }
        #endregion // Constructor

        /// <summary>
        /// Overridden OnPaint Event Handler
        /// </summary>
        protected override void OnPaint(PaintEventArgs e)
        {
            Graphics graphics = e.Graphics;
            Pen pen = new Pen(Color.Black, 1.0f);
            Pen axisPen = new Pen(Color.Black, 2.0f);
            Brush brush = new SolidBrush(Color.White);
            Brush brushQueued = new SolidBrush(Color.Red);
            Brush brushProgress = new SolidBrush(Color.Blue);
            Brush brushFinished = new SolidBrush(Color.Green);

            // Draw Background Image
            graphics.DrawImageUnscaled(this.mapImage, new Point(0, 0));

            // Draw 0,0 Axes
            Vector2i xaxiss = WorldToImage(new Vector2f(-WORLD_WIDTH / 2.0f, 0.0f));
            Vector2i xaxisf = WorldToImage(new Vector2f(WORLD_WIDTH / 2.0f, 0.0f));
            Vector2i yaxiss = WorldToImage(new Vector2f(0.0f, -WORLD_HEIGHT / 2.0f));
            Vector2i yaxisf = WorldToImage(new Vector2f(0.0f, WORLD_HEIGHT / 2.0f));
            graphics.DrawLine(axisPen, xaxiss.X, xaxiss.Y, xaxisf.X, xaxisf.Y);
            graphics.DrawLine(axisPen, yaxiss.X, yaxiss.Y, yaxisf.X, yaxisf.Y);

            // Loop through AreaManager Areas....
            foreach (cWorkUnit wu in this.AreaManager.WorkUnits)
            {
                Vector2f size = new Vector2f(wu.EndWorld.X - wu.StartWorld.X, wu.EndWorld.Y - wu.StartWorld.Y);
                Vector2i vStart = WorldToImage(wu.StartWorld);
                Vector2i vSize = WorldToImage(size);

                switch (wu.State)
                {
                    case cWorkUnit.etState.Queued:
                        graphics.DrawString(wu.ID.ToString(), this.Font, brushQueued,
                                            vStart.X,
                                            vStart.Y);
                        //graphics.DrawRectangle(penQueued, new Rectangle(vStart.X, vStart.Y, vSize.X, vSize.Y));
                        break;
                    case cWorkUnit.etState.Started:
                        graphics.DrawString(wu.ID.ToString(), this.Font, brushProgress,
                                            vStart.X,
                                            vStart.Y);
                        //graphics.DrawRectangle(penProgress, new Rectangle(vStart.X, vStart.Y, vSize.X, vSize.Y));
                        break;
                    case cWorkUnit.etState.Completed:
                        graphics.DrawString(wu.ID.ToString(), this.Font, brushFinished,
                                            vStart.X,
                                            vStart.Y);
                        break;
                    case cWorkUnit.etState.Shafted:
                        graphics.DrawString(wu.ID.ToString(), this.Font, brushFinished,
                                            vStart.X,
                                            vStart.Y);
                        //graphics.DrawRectangle(penFinished, new Rectangle(vStart.X, vStart.Y, vSize.X, vSize.Y));
                        break;                    
                }
            }

            // Draw key
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vWorld"></param>
        /// <returns></returns>
        private Vector2i WorldToImage(Vector2f vWorld)
        {
            float pxWidth = IMAGE_WIDTH / WORLD_WIDTH;
            float pxHeight = IMAGE_HEIGHT / WORLD_HEIGHT;

            Vector2i vImage = new Vector2i();
            vImage.X = (int)Math.Round(((vWorld.X * pxWidth) + (IMAGE_WIDTH / 2.0f)));
            vImage.Y = (int)Math.Round(IMAGE_HEIGHT - ((vWorld.Y * pxHeight) + (IMAGE_HEIGHT / 2.0f)));

            return (vImage);
        }

        #region Area Manager Event Handlers
        /// <summary>
        /// Event triggered (by cAreaManager) when a Work Unit is updated
        /// </summary>
        /// <param name="sender">Event source object</param>
        /// <param name="e">Event arguments object (specifying changed work unit)</param>
        public void OnWorkUnitChange(Object sender, AreaTools.cAreaManagerWorkUnitEventArgs e)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new OnWorkUnitChangeDelegate(OnWorkUnitChange), new Object[] { sender, e });
            }
            else
            {
                this.Refresh();
            }
        }

        /// <summary>
        /// Event triggered (by cAreaManager) when Work Units are initially populated
        /// </summary>
        /// <param name="sender">Event source object</param>
        /// <param name="e">Event arguments object</param>
        public void OnWorkUnitsPopulated(Object sender, EventArgs e)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new OnWorkUnitsPopulatedDelegate(OnWorkUnitsPopulated), new Object[] { sender, e });
            }
            else
            {
                this.Refresh();
            }
        }
        #endregion // Area Manager Event Handlers
    }

} // End of AreaTools.UI namespace

// End of file
