namespace DistMonitor.Scheduler.UI
{
    partial class uiSchedulerConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uiSchedulerConfig));
            this.splitMain = new System.Windows.Forms.SplitContainer();
            this.tblLayoutMain = new System.Windows.Forms.TableLayoutPanel();
            this.ctxMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miCtxProperties = new System.Windows.Forms.ToolStripMenuItem();
            this.miCtxSep1 = new System.Windows.Forms.ToolStripSeparator();
            this.miCtxRefresh = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnAddTask = new System.Windows.Forms.ToolStripButton();
            this.btnRemoveTask = new System.Windows.Forms.ToolStripButton();
            this.sep1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnProperties = new System.Windows.Forms.ToolStripButton();
            this.tblLayoutButtons = new System.Windows.Forms.TableLayoutPanel();
            this.btnOK = new System.Windows.Forms.Button();
            this.TaskListView = new DistMonitor.Scheduler.UI.uiScheduledTaskListView();
            this.hdrName = new System.Windows.Forms.ColumnHeader();
            this.hdrNextRunTime = new System.Windows.Forms.ColumnHeader();
            this.hdrEndTime = new System.Windows.Forms.ColumnHeader();
            this.hdrLastRunTime = new System.Windows.Forms.ColumnHeader();
            this.hdrSchedule = new System.Windows.Forms.ColumnHeader();
            this.splitMain.Panel1.SuspendLayout();
            this.splitMain.Panel2.SuspendLayout();
            this.splitMain.SuspendLayout();
            this.tblLayoutMain.SuspendLayout();
            this.ctxMenu.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.tblLayoutButtons.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitMain
            // 
            this.splitMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitMain.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitMain.IsSplitterFixed = true;
            this.splitMain.Location = new System.Drawing.Point(0, 0);
            this.splitMain.Name = "splitMain";
            this.splitMain.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitMain.Panel1
            // 
            this.splitMain.Panel1.Controls.Add(this.tblLayoutMain);
            // 
            // splitMain.Panel2
            // 
            this.splitMain.Panel2.Controls.Add(this.tblLayoutButtons);
            this.splitMain.Size = new System.Drawing.Size(660, 414);
            this.splitMain.SplitterDistance = 379;
            this.splitMain.TabIndex = 0;
            // 
            // tblLayoutMain
            // 
            this.tblLayoutMain.ColumnCount = 1;
            this.tblLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblLayoutMain.Controls.Add(this.TaskListView, 0, 1);
            this.tblLayoutMain.Controls.Add(this.toolStrip1, 0, 0);
            this.tblLayoutMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblLayoutMain.Location = new System.Drawing.Point(0, 0);
            this.tblLayoutMain.Name = "tblLayoutMain";
            this.tblLayoutMain.RowCount = 2;
            this.tblLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tblLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblLayoutMain.Size = new System.Drawing.Size(660, 379);
            this.tblLayoutMain.TabIndex = 0;
            // 
            // ctxMenu
            // 
            this.ctxMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miCtxProperties,
            this.miCtxSep1,
            this.miCtxRefresh});
            this.ctxMenu.Name = "contextMenuStrip1";
            this.ctxMenu.Size = new System.Drawing.Size(147, 54);
            this.ctxMenu.Opening += new System.ComponentModel.CancelEventHandler(this.ctxMenu_Opening);
            // 
            // miCtxProperties
            // 
            this.miCtxProperties.Image = ((System.Drawing.Image)(resources.GetObject("miCtxProperties.Image")));
            this.miCtxProperties.ImageTransparentColor = System.Drawing.Color.Green;
            this.miCtxProperties.Name = "miCtxProperties";
            this.miCtxProperties.Size = new System.Drawing.Size(146, 22);
            this.miCtxProperties.Text = "&Properties...";
            this.miCtxProperties.Click += new System.EventHandler(this.miCtxProperties_Click);
            // 
            // miCtxSep1
            // 
            this.miCtxSep1.Name = "miCtxSep1";
            this.miCtxSep1.Size = new System.Drawing.Size(143, 6);
            // 
            // miCtxRefresh
            // 
            this.miCtxRefresh.Name = "miCtxRefresh";
            this.miCtxRefresh.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.miCtxRefresh.Size = new System.Drawing.Size(146, 22);
            this.miCtxRefresh.Text = "&Refresh";
            this.miCtxRefresh.Click += new System.EventHandler(this.miCtxRefresh_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAddTask,
            this.btnRemoveTask,
            this.sep1,
            this.btnProperties});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(660, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnAddTask
            // 
            this.btnAddTask.Image = ((System.Drawing.Image)(resources.GetObject("btnAddTask.Image")));
            this.btnAddTask.ImageTransparentColor = System.Drawing.Color.Green;
            this.btnAddTask.Name = "btnAddTask";
            this.btnAddTask.Size = new System.Drawing.Size(71, 22);
            this.btnAddTask.Text = "Add Task";
            this.btnAddTask.Click += new System.EventHandler(this.btnAddTask_Click);
            // 
            // btnRemoveTask
            // 
            this.btnRemoveTask.Image = ((System.Drawing.Image)(resources.GetObject("btnRemoveTask.Image")));
            this.btnRemoveTask.ImageTransparentColor = System.Drawing.Color.Green;
            this.btnRemoveTask.Name = "btnRemoveTask";
            this.btnRemoveTask.Size = new System.Drawing.Size(91, 22);
            this.btnRemoveTask.Text = "Remove Task";
            this.btnRemoveTask.Click += new System.EventHandler(this.btnRemoveTask_Click);
            // 
            // sep1
            // 
            this.sep1.Name = "sep1";
            this.sep1.Size = new System.Drawing.Size(6, 25);
            // 
            // btnProperties
            // 
            this.btnProperties.Image = ((System.Drawing.Image)(resources.GetObject("btnProperties.Image")));
            this.btnProperties.ImageTransparentColor = System.Drawing.Color.Green;
            this.btnProperties.Name = "btnProperties";
            this.btnProperties.Size = new System.Drawing.Size(88, 22);
            this.btnProperties.Text = "Properties...";
            this.btnProperties.Click += new System.EventHandler(this.btnProperties_Click);
            // 
            // tblLayoutButtons
            // 
            this.tblLayoutButtons.ColumnCount = 2;
            this.tblLayoutButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblLayoutButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tblLayoutButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tblLayoutButtons.Controls.Add(this.btnOK, 1, 0);
            this.tblLayoutButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblLayoutButtons.Location = new System.Drawing.Point(0, 0);
            this.tblLayoutButtons.Name = "tblLayoutButtons";
            this.tblLayoutButtons.RowCount = 1;
            this.tblLayoutButtons.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tblLayoutButtons.Size = new System.Drawing.Size(660, 31);
            this.tblLayoutButtons.TabIndex = 0;
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(582, 3);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "&OK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // TaskListView
            // 
            this.TaskListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.hdrName,
            this.hdrNextRunTime,
            this.hdrEndTime,
            this.hdrLastRunTime,
            this.hdrSchedule});
            this.TaskListView.ContextMenuStrip = this.ctxMenu;
            this.TaskListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TaskListView.FullRowSelect = true;
            this.TaskListView.Location = new System.Drawing.Point(3, 28);
            this.TaskListView.MultiSelect = false;
            this.TaskListView.Name = "TaskListView";
            this.TaskListView.ScheduledTaskManager = null;
            this.TaskListView.Size = new System.Drawing.Size(654, 348);
            this.TaskListView.TabIndex = 2;
            this.TaskListView.UseCompatibleStateImageBehavior = false;
            this.TaskListView.View = System.Windows.Forms.View.Details;
            // 
            // hdrName
            // 
            this.hdrName.Text = "Name";
            this.hdrName.Width = 267;
            // 
            // hdrNextRunTime
            // 
            this.hdrNextRunTime.Text = "Next Run Time";
            this.hdrNextRunTime.Width = 117;
            // 
            // hdrEndTime
            // 
            this.hdrEndTime.Text = "End Time";
            this.hdrEndTime.Width = 117;
            // 
            // hdrLastRunTime
            // 
            this.hdrLastRunTime.Text = "Last Run Time";
            this.hdrLastRunTime.Width = 120;
            // 
            // hdrSchedule
            // 
            this.hdrSchedule.Text = "Schedule";
            this.hdrSchedule.Width = 115;
            // 
            // uiSchedulerConfig
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(660, 414);
            this.Controls.Add(this.splitMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "uiSchedulerConfig";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Scheduler Configuration:";
            this.splitMain.Panel1.ResumeLayout(false);
            this.splitMain.Panel2.ResumeLayout(false);
            this.splitMain.ResumeLayout(false);
            this.tblLayoutMain.ResumeLayout(false);
            this.tblLayoutMain.PerformLayout();
            this.ctxMenu.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tblLayoutButtons.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitMain;
        private System.Windows.Forms.TableLayoutPanel tblLayoutButtons;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.TableLayoutPanel tblLayoutMain;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnAddTask;
        private System.Windows.Forms.ToolStripButton btnRemoveTask;
        private System.Windows.Forms.ToolStripSeparator sep1;
        private System.Windows.Forms.ToolStripButton btnProperties;
        private uiScheduledTaskListView TaskListView;
        private System.Windows.Forms.ColumnHeader hdrName;
        private System.Windows.Forms.ColumnHeader hdrLastRunTime;
        private System.Windows.Forms.ColumnHeader hdrNextRunTime;
        private System.Windows.Forms.ColumnHeader hdrEndTime;        
        private System.Windows.Forms.ColumnHeader hdrSchedule;
        private System.Windows.Forms.ContextMenuStrip ctxMenu;
        private System.Windows.Forms.ToolStripMenuItem miCtxRefresh;
        private System.Windows.Forms.ToolStripMenuItem miCtxProperties;
        private System.Windows.Forms.ToolStripSeparator miCtxSep1;
    }
}