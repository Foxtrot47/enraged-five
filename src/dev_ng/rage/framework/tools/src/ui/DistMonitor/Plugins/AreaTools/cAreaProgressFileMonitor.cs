//
// File: cAreaProgressFileMonitor.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of cAreaProgressFileMonitor.cs class
//

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace AreaTools
{
    
    /// <summary>
    /// AreTool Progress File Monitor Class
    /// </summary>
    public class cAreaProgressFileMonitor
    {
        #region Enumerations
        /// <summary>
        /// State Enumeration
        /// </summary>
        public enum etState
        {
            Queued,
            Started,
            Finished
        }
        #endregion // Enumerations

        #region Properties and Associated Member Data
        /// <summary>
        /// Progress state
        /// </summary>
        public etState State
        {
            get { return m_eState; }
            private set { m_eState = value; }
        }
        private etState m_eState;

        /// <summary>
        /// Filename
        /// </summary>
        public String Filename
        {
            get { return m_sFilename; }
            private set { m_sFilename = value; }
        }
        private String m_sFilename;

        /// <summary>
        /// File Modified Date/Time
        /// </summary>
        /// Calculated to avoid Window's File Modified Time caching as file
        /// is not closed between writes from the Game.
        public DateTime ModifiedTime
        {
            get { return m_dtModifiedTime; }
            private set { m_dtModifiedTime = value; }
        }
        private DateTime m_dtModifiedTime;

        /// <summary>
        /// Last sector processed
        /// </summary>
        public RSG.Base.Math.Vector2i LastSector
        {
            get { return m_vLastSector; }
            private set { m_vLastSector = value; }
        }
        private RSG.Base.Math.Vector2i m_vLastSector;

        /// <summary>
        /// Previous sector processed
        /// </summary>
        public RSG.Base.Math.Vector2i PreviousSector
        {
            get { return m_vPreviousSector; }
            private set { m_vPreviousSector = value; }
        }
        private RSG.Base.Math.Vector2i m_vPreviousSector;
        #endregion // Properties and Associated Member Data

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public cAreaProgressFileMonitor(String filename)
        {
            State = etState.Queued;
            Filename = filename;
            ModifiedTime = DateTime.Now;

            this.Parse();
        }
        #endregion // Constructor

        #region Controller Methods
        /// <summary>
        /// Parse File
        /// </summary>
        public void Parse()
        {
            if (!File.Exists(Filename))
            {
                RSG.Base.Logging.Log.Log__Error(String.Format("Progress file {0} does not exist.  Progress monitor fail.", this.Filename));
                return;
            }

            FileStream fs = File.Open(Filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            StreamReader sr = new StreamReader(fs);
            List<String> contents = new List<String>();
            while (!sr.EndOfStream)
                contents.Add(sr.ReadLine());

            if (contents.Count > 0)
            {
                // Find start and end tokens
                for (int nLine = 0; nLine < contents.Count; ++nLine )
                {
                    String sTrimLine = contents[nLine].Trim();
                    if (0 == sTrimLine.Length)
                        continue;

                    if (sTrimLine.StartsWith("START") && (State != etState.Started))
                    {
                        State = etState.Started;
                        continue;
                    }
                    else if (sTrimLine.StartsWith("END") && (State != etState.Finished))
                    {
                        State = etState.Finished;
                        continue;
                    }

                    if (nLine == (contents.Count - 2))
                    {
                        PreviousSector = ParseCoords(sTrimLine);
                    }
                    else if (nLine == (contents.Count - 1))
                    {
                        RSG.Base.Math.Vector2i tempLastSector = ParseCoords(sTrimLine);
                        if ((null != tempLastSector) &&
                            ((null == LastSector) || 
                             (tempLastSector.X != LastSector.X) || (tempLastSector.Y != LastSector.Y)))
                        {
                            LastSector = tempLastSector;
                            ModifiedTime = DateTime.Now;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Flush( )
        {
            this.m_eState = etState.Queued;
            this.m_vLastSector = null;
            this.m_vPreviousSector = null;

            StreamWriter fs = File.CreateText(Filename);
            fs.Close();
        }
        #endregion  // Controller Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sCoords"></param>
        /// <returns></returns>
        private RSG.Base.Math.Vector2i ParseCoords(String line)
        {
            // Attempt to parse coords
            String[] sCoords = line.Split(new char[] { ' ' },
                                    StringSplitOptions.RemoveEmptyEntries);
            if (2 != sCoords.Length)
                return (null);

            RSG.Base.Math.Vector2i vVec =
                new RSG.Base.Math.Vector2i(int.Parse(sCoords[0]), int.Parse(sCoords[1]));
            return vVec;
        }
        #endregion // Controller Methods
    }

} // End of DistMonitor.AreaTools namespace

// End of file
