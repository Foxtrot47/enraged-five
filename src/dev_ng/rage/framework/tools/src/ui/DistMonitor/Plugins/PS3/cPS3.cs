//
// File: Class1.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of Class1.cs class
//

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net;
using System.Reflection;
using System.Resources;
using System.Threading;
using System.Diagnostics;

using RSG.Base.Logging;

namespace PS3
{
    
    /// <summary>
    /// 
    /// </summary>
    public class cPS3 : MachineInterface.iMachine, RSG.Base.Plugins.IPlugin
    {
        #region Constants
        public static readonly String NAME = "PS3";
        public static readonly String DESC = "Sony PS3 Machine Plugin";
        public static readonly String ARCH_NAME = "PS3";
        public static readonly String SDK_PATH = "X:\\usr";
        public static readonly String SDK_SN_SYSTEMS = "%SN_PS3_PATH%";
        public static readonly String SDK_SN_SYSTEMS_PS3_BIN = Path.Combine("%SN_PS3_PATH%", "bin");
        public static readonly String SDK_PS3_RUN_EXE = Path.Combine(SDK_SN_SYSTEMS_PS3_BIN, "ps3run.exe");
        public static readonly String SDK_PS3_TM_EXE = Path.Combine(SDK_SN_SYSTEMS_PS3_BIN, "ps3tm.exe");
       
        public static readonly int DELAY_REBOOT = 20;
        public static readonly int DELAY_START = 10;
        public static readonly int INTERVAL_START = 100;
        public static readonly int DELAY_COPY = 30;
        public static readonly int PS3_EXE_TIMEOUT = 100;
        #endregion // Constants

        #region Events
        public event EventHandler<MachineInterface.cMachineStatusEventArgs> MachineStatusChangeEvent;

        public event EventHandler<MachineInterface.cMachineEnabledEventArgs> MachineEnabledChangeEvent;
        #endregion // Events

        #region Private Locking Data
        /// <summary>
        /// </summary>
        private static object StartupLock = new Object();
        #endregion

        #region Properties and Associated Member Data
        public String Name
        {
            get { return NAME; }
        }

        public Version Version
        {
            get
            {
                Assembly assembly = Assembly.GetExecutingAssembly();
                return (assembly.GetName().Version);
            }
        }

        public String Description
        {
            get { return DESC; }
        }

        public String ArchName
        {
            get { return ARCH_NAME; }
        }

        public IPAddress IP
        {
            get { return m_IP; }
            set { m_IP = value; }
        }
        private IPAddress m_IP;

        public String HostName
        {
            get 
            {
                if (null != IP)
                {
                    IPHostEntry host = System.Net.Dns.GetHostEntry(IP);
                    return (host.HostName);
                }
                return (String.Empty);
            }
        }

        public bool Enabled
        {
            get { return m_bEnabled; }
            set { m_bEnabled = value; }
        }
        private bool m_bEnabled;

        public bool Available
        {
            get { return m_bAvailable; }
            set { m_bAvailable = value; }
        }
        private bool m_bAvailable;


        public Int32 Failures
        {
            get { return m_Failures; }
            set { m_Failures = value; }
        }
        private Int32 m_Failures = 0;

        public bool Failed
        {
            get { return m_Failed; }
            set { m_Failed = value; }
        }
        private bool m_Failed = false;

        public bool ForceDisconnectOnConnect
        {
            get { return m_bForceDisconnectOnConnect; }
            set { m_bForceDisconnectOnConnect = value; }
        }
        private bool m_bForceDisconnectOnConnect = false;

        public Bitmap Bitmap
        {
            get
            {
                Assembly a = Assembly.GetExecutingAssembly();
                Stream imgStream = a.GetManifestResourceStream("PS3.Data.PS3.bmp");
                return (Bitmap.FromStream(imgStream) as Bitmap);
            }
        }

        public MachineInterface.iDebugLog DebugLog
        {
            get { return m_DebugLog; }
            private set { m_DebugLog = value; }
        }
        private MachineInterface.iDebugLog m_DebugLog;

        public MachineInterface.etMachineStatus Status
        {
            get { return m_eStatus; }
            private set { m_eStatus = value; }
        }
        private MachineInterface.etMachineStatus m_eStatus;
        #endregion

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public cPS3()
        {
            IP = null;
            Enabled = true;
            Available = true;
        }

        /// <summary>
        /// Constructor, specifying an IP Address
        /// </summary>
        /// <param name="ip"></param>
        public cPS3(IPAddress ip)
        {
            Initialise(ip);
        }
        
        /// <summary>
        /// Constructor, specifying an IP Address and initial enabled state
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="enabled"></param>
        public cPS3(IPAddress ip, bool enabled)
        {
            Initialise(ip, enabled);
        }

        #endregion

        #region Controller Methods
        #region Initialisation
        public void Initialise(IPAddress ip)
        {
            m_IP = ip;
            Enabled = true;
            Available = true;
            if (m_IP != null)
            {
                DebugLog = new cPS3DebugLog(this);
                DebugLog.MachineStatusChangeEvent += MachineStatusEvent;
                DebugLog.ConnectTo(this);
            }
        }

        public void Initialise(IPAddress ip, bool enabled)
        {
            Initialise(ip);
            Enabled = enabled;
        }
        #endregion // Initialisation

        #region Starting


        // sorry no default parameters in c# hence these overloads. ( might be a better way of doing this though? )
        public void Start(GameInterface.iGame game,
                          GameInterface.cConfiguration config,
                          String sArgs)
        {
            Start(game, config, sArgs, INTERVAL_START);
        }

        public void Start(GameInterface.iGame game,
                          GameInterface.cConfiguration config)
        {
            Start(game, config, "", INTERVAL_START);
        }

        public void Start(GameInterface.iGame game, 
                          GameInterface.cConfiguration config,
                          int delay)
        {
            Start(game, config, "", delay);
        }

        public void Start(GameInterface.iGame game, 
                          GameInterface.cConfiguration config,
                          String sArgs,
                          int delay)
        {
            lock (StartupLock)
            {
                Log.Log__Warning(String.Format("{0} starting game {1}.", this.IP.ToString(), game.Name));
                //sArgs = sArgs + " -statsip " + this.IP; // Hack in the IP.  If we need more params like this then maybe add an AddRuntimeParams function
                AddRunTimeParams( ref sArgs);
                StartGame(game, config, sArgs);
                Thread.Sleep(delay);
            }
        }
        #endregion
       
        #region Reboot/connect/disconnect
        public void Connect()
        {
            // force a disconnect first...            
            {
                Log.Log__Warning(String.Format("{0} Soft Disconnecting on TM. : Status:{1} Enabled:{2} Failed:{3} Available:{4}",
                                        this.IP.ToString(), 
                                        this.Status.ToString(), 
                                        this.Enabled.ToString(), 
                                        this.Failed.ToString(), 
                                        this.Available.ToString()));

                String sCmdDisconnect = Environment.ExpandEnvironmentVariables(SDK_PS3_RUN_EXE);
                String sArgumentsDisconnect = String.Format("-t {0} -d ", IP.ToString());

                // If exe terminates withing DELAY_REBOOT * 1000ms then reboot has been successful and status change machine
                if (true == MachineInterface.cMachineUtils.ExecuteWaitAndLogResult(sCmdDisconnect, sArgumentsDisconnect, DELAY_REBOOT))
                {
                    Log.Log__Debug(String.Format("{0} Disconnected on TM.", this.IP.ToString()));
                }
                else 
                {
                    Log.Log__Warning(String.Format("{0} Disconnected on TM failed.", this.IP.ToString()));

                    if (ForceDisconnectOnConnect)
                    {
                       Log.Log__Warning(String.Format("{0} Force Disconnecting on TM. : Status:{1} Enabled:{2} Failed:{3} Available:{4}",
                                            this.IP.ToString(), 
                                            this.Status.ToString(), 
                                            this.Enabled.ToString(), 
                                            this.Failed.ToString(), 
                                            this.Available.ToString()));

                        String sCmd = Environment.ExpandEnvironmentVariables(SDK_PS3_RUN_EXE);
                        String sArguments = String.Format("-t {0} -k ", IP.ToString());

                        // If exe terminates withing DELAY_REBOOT * 1000ms then reboot has been successful and status change machine
                        if (true == MachineInterface.cMachineUtils.ExecuteWaitAndLogResult(sCmd, sArguments, DELAY_REBOOT))
                        {
                            Log.Log__Debug(String.Format("{0} Force Disconnecting on TM.", this.IP.ToString()));
                        }
                        else
                        {
                            this.Available = false; // DW - disable if you can't force disconnect
                            Log.Log__Warning(String.Format("{0} Force Disconnecting on TM failed. - The machine has been disabled.", this.IP.ToString()));
                        }
                    }
                    else
                    {
                        this.Available = false;
                    }
                }
            }
/*
            {
                Log.Log__Warning(String.Format("{0} Connecting to TM.", this.IP.ToString()));
                String sCmd = Environment.ExpandEnvironmentVariables(SDK_PS3_TM_EXE);
                String sArguments = String.Format("-t {0} -c ", IP.ToString());

                // If exe terminates withing DELAY_REBOOT * 1000ms then reboot has been successful and status change machine
                if (true == MachineInterface.cMachineUtils.ExecuteWaitAndLogResult(sCmd, sArguments, DELAY_REBOOT))
                {
                    Log.Log__Debug(String.Format("{0} Connected to TM.", this.IP.ToString()));
                }
                else
                {
                    Log.Log__Warning(String.Format("{0} Connect to TM failed.", this.IP.ToString()));
                }
            }
 */
        }

        public void Disconnect()
        {
            m_DebugLog.Disconnect();
            Thread.Sleep(2000); // since the thred loop for the debuglog has it's own sleep.

            Log.Log__Debug(String.Format("{0} Disconnecting from TM. Status:{1} Enabled:{2} Failed:{3} Available:{4}", 
                                this.IP.ToString(), 
                                this.Status.ToString(), 
                                this.Enabled.ToString(), 
                                this.Failed.ToString(), 
                                this.Available.ToString()));

            String sCmd = Environment.ExpandEnvironmentVariables(SDK_PS3_TM_EXE);
            String sArguments = String.Format("-t {0} -d ", IP.ToString());

            // If exe terminates withing DELAY_REBOOT * 1000ms then reboot has been successful and status change machine
            if (true == MachineInterface.cMachineUtils.ExecuteWaitAndLogResult(sCmd, sArguments, DELAY_REBOOT))
            {
                Log.Log__Debug(String.Format("{0} Disconnected from TM.", this.IP.ToString()));
            }
            else
            {
                Log.Log__Warning(String.Format("{0} Disconnect to TM failed.", this.IP.ToString()));
            }

            //m_DebugLog = null; // force garbage collection, cos the PS3 is garbage
            //GC.Collect();
        }

        public void Reboot()
        {
            Reboot(MachineInterface.etRebootType.Warm);
        }

        public void RebootCold()
        {
            Reboot(MachineInterface.etRebootType.Cold);
        }

        public void Reboot(MachineInterface.etRebootType type)
        {
 
            Log.Log__Warning(String.Format("{0} {1} rebooting.", this.IP.ToString(), type.ToString()));

            String sCmd = Environment.ExpandEnvironmentVariables(SDK_PS3_RUN_EXE);
            String sArguments = String.Format("-t {0} -r ", IP.ToString());

            // If exe terminates withing DELAY_REBOOT * 1000ms then reboot has been successful and status change machine
            if (true == MachineInterface.cMachineUtils.ExecuteWaitAndLogResult(sCmd, sArguments, DELAY_REBOOT))
            {
                this.Status = MachineInterface.etMachineStatus.On;
                if (null != MachineStatusChangeEvent)
                    MachineStatusEvent(this, new MachineInterface.cMachineStatusEventArgs(this, this.Status));
            }
            
        }
        #endregion

        #region Status
        public long Ping()
        {
            return (MachineInterface.cMachineUtils.PingIP(this.IP));
        }

        public void AddRunTimeParams(ref String sArgs)
        {
            sArgs = sArgs + " -statsip " + this.IP;
        }
        #endregion // Status
        #endregion // Controller Methods

        #region Object Overridden Methods
        /// <summary>
        /// Convert object to String representation
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return (String.Format("{0}:{1}", IP, NAME));
        }
        #endregion

        #region Event Handlers
        /// <summary>
        /// Event handler for when cMachineMonitor needs to change Machine's Status state
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnMonitorMachineStateChange(Object sender, MachineInterface.cMachineStatusEventArgs e)
        {
            if (this.Status != e.Status)
            {
                this.Status = e.Status;

                if (null != MachineStatusChangeEvent)
                    MachineStatusChangeEvent(this, e);
            }
        }

        /// <summary>
        /// Event handler for when cMachineFarm needs to change Machine's Enabled state
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnMachineEnabledChange(Object sender, MachineInterface.cMachineEnabledEventArgs e)
        {
            if (this.Enabled != e.Enabled)
            {
                this.Enabled = e.Enabled;

                if (null != MachineEnabledChangeEvent)
                    MachineEnabledChangeEvent(this, e);
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Start game on destination machine
        /// </summary>
        /// <param name="game">Game to start</param>
        /// <param name="config">Configuration to start</param>
        private void StartGame(GameInterface.iGame game, GameInterface.cConfiguration config, String sArgs)
        {
            String sCmd = Environment.ExpandEnvironmentVariables(SDK_PS3_RUN_EXE);
//            String sPs3RunArguments = String.Format("-f {0} -z {6} -t {1} -h {2} -p -r {0}\\{3} -common={2}\\common -platform={2}\\ps3 {4} {5}", 
//                game.RootDirectory, IP.ToString(), game.RootDirectory, config.Executable, sArgs, config.Arguments, PS3_EXE_TIMEOUT);

            String sPs3RunArguments = String.Format("-f {0} -z {6} -t {1} -h {2} -p -r {0}\\{3} {4} {5}",
                game.RootDirectory, IP.ToString(), game.RootDirectory, config.Executable, sArgs, config.Arguments, PS3_EXE_TIMEOUT);



            if (true == MachineInterface.cMachineUtils.ExecuteWaitAndLogResult(sCmd, sPs3RunArguments, DELAY_START))
            {
                this.Status = MachineInterface.etMachineStatus.Running;
                if (null != MachineStatusChangeEvent)
                    MachineStatusEvent(this, new MachineInterface.cMachineStatusEventArgs(this, this.Status));
            }
#if false
            else // DW - connection issues.... test
            {
                this.Initialise(this.IP);
                if (true == MachineInterface.cMachineUtils.ExecuteWaitAndLogResult(sCmd, sPs3RunArguments, DELAY_START))
                {
                    this.Status = MachineInterface.etMachineStatus.Running;
                    if (null != MachineStatusChangeEvent)
                        MachineStatusEvent(this, new MachineInterface.cMachineStatusEventArgs(this, this.Status));
                }

            }
#endif // false
        }
        #endregion

        #region Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MachineStatusEvent(object sender, MachineInterface.cMachineStatusEventArgs e)
        {
            Status = e.Status;
        }
        #endregion
    }

} // End of PS3 namespace

// End of file
