//
// File: uiOptionsForm.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of uiOptionsForm.cs class
//

using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace DistMonitor.UI
{

    /// <summary>
    /// 
    /// </summary>
    public partial class uiOptionsForm : Form
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// List of settings
        /// </summary>
        public List<System.Configuration.ApplicationSettingsBase> Settings
        {
            get { return m_Settings; }
            private set { m_Settings = value; }
        }
        private List<System.Configuration.ApplicationSettingsBase> m_Settings;
        #endregion // Properties and Associated Member Data

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public uiOptionsForm()
        {
            InitializeComponent();
            this.Settings = new List<System.Configuration.ApplicationSettingsBase>();
        }
        #endregion

        #region UI Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uiOptionsForm_Load(object sender, EventArgs e)
        {
            pgSettings.SelectedObject = Properties.Settings.Default;
            
            // Tool Settings
            foreach (Type settingType in Program.Controller.ToolSettings.Plugins)
            {
                ToolInterface.iToolSettings settings = 
                    Program.Controller.ToolSettings.CreatePluginInstance(settingType);
                this.Settings.Add(settings);

                // Add UI
                uiToolSettingsTabPage tab = new uiToolSettingsTabPage(settings, settings.Name);
                tabControl.TabPages.Add(tab);
            }
        }
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    internal class uiToolSettingsTabPage : TabPage
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Settings object
        /// </summary>
        public ToolInterface.iToolSettings Settings
        {
            get { return m_Settings; }
            private set { m_Settings = value; }
        }
        private ToolInterface.iToolSettings m_Settings;
        #endregion // Properties and Associated Member Data

        #region Member Data
        /// <summary>
        /// Property Grid settings display
        /// </summary>
        private PropertyGrid m_PropertyGrid;
        #endregion // Member Data

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="settings"></param>
        public uiToolSettingsTabPage(ToolInterface.iToolSettings settings)
            : base()
        {
            Settings = settings;
            Initialise();
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="settings"></param>
        /// <param name="text"></param>
        public uiToolSettingsTabPage(ToolInterface.iToolSettings settings, String text)
            : base(text)
        {
            Settings = settings;
            Initialise();
        }
        #endregion // Constructor

        #region Private Methods
        /// <summary>
        /// Common initialisation
        /// </summary>
        private void Initialise()
        {
            m_PropertyGrid = new PropertyGrid();
            m_PropertyGrid.SelectedObject = this.Settings;
            m_PropertyGrid.Dock = DockStyle.Fill;

            this.Controls.Add(m_PropertyGrid);
        }
        #endregion // Private Methods
    };

} // End of DistMonitor.UI namespace

// End of file
