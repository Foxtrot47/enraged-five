//
// File: uiMainForm.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of uiMainForm.cs class
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using RSG.Base.Logging;

namespace DistMonitor.UI
{

    /// <summary>
    /// Main Application Form Class
    /// </summary>
    internal partial class uiMainForm : Form
    {
        #region Delegates
        /// <summary>
        /// Generic event handler delegate for thread-safe UI updates
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private delegate void GenericEventDelegate(Object sender, EventArgs e);
        #endregion // Delegates

        #region Properties and Associated Member Data
        /// <summary>
        /// Application Log File
        /// </summary>
        private RSG.Base.Logging.LogFile m_LogFile;
        #endregion

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public uiMainForm()
        {
            // UI Initialisation
            InitializeComponent();
        }
        #endregion // Constructor

        #region UI Event Handlers
        #region File Menu Event Handlers
        /// <summary>
        /// File Options Menu Item Click Handler
        /// </summary>
        private void miFileOptions_Click(object sender, EventArgs e)
        {
            UI.uiOptionsForm uiForm = new UI.uiOptionsForm();
            if (DialogResult.OK == uiForm.ShowDialog())
            {
                Properties.Settings.Default.Save();
                foreach (System.Configuration.ApplicationSettingsBase settings in uiForm.Settings)
                    settings.Save();
            }
            else
            {
                Properties.Settings.Default.Reload();
                foreach (System.Configuration.ApplicationSettingsBase settings in uiForm.Settings)
                    settings.Reload();
            }
        }

        /// <summary>
        /// File Plugins Menu Item Click Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void miFilePlugins_Click(Object sender, EventArgs e)
        {
            using (uiPluginListDlg pluginDlg = new uiPluginListDlg())
            {
                pluginDlg.ShowDialog();
            }
        }

        /// <summary>
        /// File Exit Menu Item Click Handler
        /// </summary>
        private void miFileExit_Click(object sender, EventArgs e)
        {
            Close();
        }
        #endregion // File Menu Event Handlers

        #region Scheduler Menu Event Handlers
        /// <summary>
        /// Scheduler Menu Item Opening Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void miScheduler_DropDownOpening(Object sender, EventArgs e)
        {
            miSchedulerEnabled.Checked = (Program.Controller.ScheduleManager.Enabled);
            miSchedulerEnabled.Enabled = (cController.etState.Running != Program.Controller.State);
            miSchedulerConfigure.Enabled = (cController.etState.Running != Program.Controller.State);
        }

        /// <summary>
        /// Scheduler Enabled Menu Item Click Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void miSchedulerEnabled_Click(Object sender, EventArgs e)
        {
            Program.Controller.ScheduleManager.Enabled =
                !Program.Controller.ScheduleManager.Enabled;
        }

        /// <summary>
        /// Scheduler Configure Menu Item Click Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void miSchedulerConfigure_Click(Object sender, EventArgs e)
        {
            using (Scheduler.UI.uiSchedulerConfig configForm = new Scheduler.UI.uiSchedulerConfig())
            {
                configForm.SetScheduledTaskManager(Program.Controller.ScheduleManager);
                configForm.ShowDialog();
            }
        }
        #endregion // Scheduler Menu Event Handlers

        #region View Menu Event Handlers
        /// <summary>
        /// View Menu Item Opening Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void miView_DropDownOpening(object sender, EventArgs e)
        {
            miViewLogPane.Checked = !splitContainer.IsSplitterFixed;
        }

        /// <summary>
        /// View Log Pane Menu Item Click Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void miViewLogPane_Click(object sender, EventArgs e)
        {
            if (splitContainer.IsSplitterFixed)
            {
                // Show Log Pane
                splitContainer.IsSplitterFixed = false;
                splitContainer.SplitterDistance = (int)(2.0 * splitContainer.ClientSize.Height / 3.0);
            }
            else
            {
                // Hide Log Pane
                splitContainer.IsSplitterFixed = true;
                splitContainer.SplitterDistance = splitContainer.ClientSize.Height;
            }
        }

        /// <summary>
        /// View Logs Clear Application Log Menu Item Click Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void miViewLogsClearAppLog_Click(object sender, EventArgs e)
        {
            uiFarmLog1.ClearAppLog();
        }

        /// <summary>
        /// View Logs Clear Scheduler Log Menu Item Click Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void miClearSchedulerLog_Click(object sender, EventArgs e)
        {
            uiFarmLog1.ClearSchedulerLog();
        }

        /// <summary>
        /// View Logs Clear Machine Logs Menu Item Click Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void miViewLogsClearMachineLogs_Click(object sender, EventArgs e)
        {
            uiFarmLog1.ClearMachineLogs();
        }
        #endregion // View Menu Event Handlers

        #region Help Menu Event Handlers
        /// <summary>
        /// Help Menu Item Click Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void miHelpAbout_Click(object sender, EventArgs e)
        {
            UI.uiAboutBox aboutBox = new UI.uiAboutBox();
            aboutBox.ShowDialog();
        }
        #endregion // Help Menu Event Handlers

        #region ToolBar Button Event Handlers
        /// <summary>
        /// Configure Farm Button Click Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnConfigureFarm_Click(object sender, EventArgs e)
        {
            Program.Controller.ConfigureFarm();
        }

        /// <summary>
        /// Start Button Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnStart_Click(object sender, EventArgs e)
        {
            Program.Controller.Start();
        }

        /// <summary>
        /// Stop Button Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnStop_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to stop the current session?", 
                                "Confirmation", MessageBoxButtons.YesNo) == DialogResult.No)
                return;

            Program.Controller.Stop();
        }

        /// <summary>
        /// Reboot Button Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnReboot_Click(object sender, EventArgs e)
        {
            Program.Controller.Reboot(MachineInterface.etRebootType.Warm);
        }

        /// <summary>
        /// Ping Button Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPing_Click(object sender, EventArgs e)
        {
            Program.Controller.Ping();
        }
        #endregion // ToolBar Button Event Handlers

        #region Form Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uiMainForm_Load(object sender, EventArgs e)
        {
            // Setup Log File (ensure we get all log messages, rather than
            // create it in the controller)
            String sDate = String.Format("{1}_{0:yyyy}{0:MM}{0:dd}.log", DateTime.Now, Properties.Settings.Default.LogFile);
            m_LogFile = new LogFile(sDate);

#if DEBUG
            String sText = String.Format("{0} - {1} DEBUG", Application.ProductName, Application.ProductVersion);
#else
            String sText = String.Format("{0} - {1}", Application.ProductName, Application.ProductVersion);
#endif
            Text = sText;
            notifyIcon.Text = sText;
            Log.Log__Message(sText);

            // Initialise Application Controller
            Program.Controller.Initialise(uiFarmView1, uiFarmLog1);
            Program.Controller.MachineFarmStartEvent += this.OnMachineFarmStart;
            Program.Controller.MachineFarmStopEvent += this.OnMachineFarmStop;
            Program.Controller.MachineFarmRebootEvent += this.OnMachineFarmReboot;
            Program.Controller.ScheduleManager.SchedulerStateChangeEvent += this.OnScheduledTaskManagerEnabledChange;

            // UI/View Hooks
            uiGameConfigBar.GameList = Program.Controller.GameList;

            btnStart.Enabled = true;
            btnStop.Enabled = false;

            uiGameConfigBar.OnGameConfigSelectionEvent += this.OnGameConfigSelectionChange;
            Program.Controller.SetCurrentConfig(uiGameConfigBar.GetSelectedGame(),
                                                uiGameConfigBar.GetSelectedConfiguration(),
                                                uiGameConfigBar.GetSelectedAreaTool());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uiMainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Program.Controller.Shutdown();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uiMainForm_Resize(object sender, EventArgs e)
        {
            if ((FormWindowState.Maximized == this.WindowState) ||
                (FormWindowState.Normal == this.WindowState))
            {
                ShowInTaskbar = true;
            }
            else
            {
                ShowInTaskbar = false;
            }
        }

        private void notifyIcon_DoubleClick(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Maximized;
        }
        #endregion // Form Event Handlers

        #region Notify Icon Context Menu Event Handlers
        /// <summary>
        /// Notify Icon Context Menu Opening Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuCtxNotify_Opening(object sender, CancelEventArgs e)
        {
            miCtxRestore.Enabled = (this.WindowState == FormWindowState.Minimized);
            miCtxOptions.Enabled = (Program.Controller.State == cController.etState.Stopped);
            miCtxExit.Enabled = (Program.Controller.State == cController.etState.Stopped);
        }

        /// <summary>
        /// Notify Icon Context Menu Restore Menu Item Click Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void miCtxRestore_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }

        /// <summary>
        /// Notify Icon Context Menu Options Menu Item Click Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void miCtxOptions_Click(object sender, EventArgs e)
        {
            miFileOptions_Click(sender, e);
        }

        /// <summary>
        /// Notify Icon Context Menu Exit Menu Item Click Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void miCtxExit_Click(object sender, EventArgs e)
        {
            Close();
        }
        #endregion // Notify Icon Context Menu Event Handlers
        #endregion // UI Event Handlers

        #region Controller Methods
        /// <summary>
        /// Display alert message
        /// </summary>
        /// <param name="message"></param>
        public void DisplayAlertMessage(String message)
        {            
#if DEBUG
            String caption = String.Format("{0} - {1} DEBUG", Application.ProductName, 
                                           Application.ProductVersion);
#else
            String caption = String.Format("{0} - {1}", Application.ProductName, 
                                           Application.ProductVersion);
#endif // DEBUG

            if (notifyIcon.Visible)
            {
                notifyIcon.BalloonTipTitle = caption;
                notifyIcon.BalloonTipText = String.Format("Alert: {0}", message);
                notifyIcon.ShowBalloonTip(0);
            }
            else
            {
                MessageBox.Show(String.Format("Alert: {0}", message), caption);
            }
        }
        #endregion // Controller Methods

        #region Controller Event Handlers
        /// <summary>
        /// Handler for Controller Machine Farm Start Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnMachineFarmStart(Object sender, EventArgs e)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new GenericEventDelegate(OnMachineFarmStart), new Object[] { sender, e });
            }
            else
            {
                btnStop.Enabled = true;
                btnStart.Enabled = false;
                btnReboot.Enabled = false;
                uiGameConfigBar.Enabled = false;
            }
        }

        /// <summary>
        /// Handler for Controller Machine Farm Stop Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnMachineFarmStop(Object sender, EventArgs e)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new GenericEventDelegate(OnMachineFarmStop), new Object[] { sender, e });
            }
            else
            {
                btnStart.Enabled = true;
                btnReboot.Enabled = true;
                btnStop.Enabled = false;
                uiGameConfigBar.Enabled = true;
            }
        }

        /// <summary>
        /// Handler for Controller Machine Farm Reboot Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnMachineFarmReboot(Object sender, EventArgs e)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new GenericEventDelegate(OnMachineFarmReboot), new Object[] { sender, e });
            }
            else
            {
                // Nothing yet...
            }
        }

        /// <summary>
        /// Handler for Game Configuration UI changes
        /// </summary>
        /// <param name="sender">Source of event (cScheduledTaskManager)</param>
        /// <param name="e">Event argument object</param>
        private void OnGameConfigSelectionChange(Object sender, UI.uiGameConfigSelectionEventArgs e)
        {
            Program.Controller.SetCurrentConfig(e.Game, e.Configuration, e.Tool);
        }

        /// <summary>
        /// Handler for Scheduled Task Manager Enabled Change
        /// </summary>
        /// <param name="sender">Source of event (cScheduledTaskManager)</param>
        /// <param name="e">Event argument object</param>
        private void OnScheduledTaskManagerEnabledChange(Object sender, EventArgs e)
        {
            Scheduler.cScheduledTaskManager manager = 
                (sender as Scheduler.cScheduledTaskManager);
            btnStart.Enabled = !manager.Enabled;
            btnStop.Enabled = false;
        }
        #endregion // Controller Event Handlers
    }

} // End of DistMonitor

// End of file
