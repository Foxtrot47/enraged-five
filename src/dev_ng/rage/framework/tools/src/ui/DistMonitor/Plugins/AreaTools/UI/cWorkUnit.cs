//
// File: cWorkUnit.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of cWorkUnit.cs class
//

using System;
using System.Collections.Generic;
using System.Text;

using RsBase.Math;

namespace DistMonitor.AreaTools
{

    /// <summary>
    /// 
    /// </summary>
    public class cWorkUnit
    {
        #region Enumerations
        /// <summary>
        /// Work unit state enumeration
        /// </summary>
        public enum etState
        {
            Queued,
            Started,
            Completed,
            Error
        }
        #endregion // Enumerations

        #region Properties and Associated Member Data
        /// <summary>
        /// Workunit unique identifier
        /// </summary>
        public UInt32 ID
        {
            get { return m_nID; }
            private set { m_nID = value; }
        }
        private UInt32 m_nID;
        
        /// <summary>
        /// Workunit state
        /// </summary>
        public etState State
        {
            get { return m_eState; }
            private set { m_eState = value; }
        }
        private etState m_eState;

        /// <summary>
        /// Start World Coordinates of WorkUnit
        /// </summary>
        public cVector2<float> StartWorld
        {
            get { return m_vfStartWorld; }
            private set { m_vfStartWorld = value; }
        }
        private cVector2<float> m_vfStartWorld;

        /// <summary>
        /// End World Coordinates of WorkUnit
        /// </summary>
        public cVector2<float> EndWorld
        {
            get { return m_vfEndWorld; }
            private set { m_vfEndWorld = value; }
        }
        private cVector2<float> m_vfEndWorld;

        /// <summary>
        /// Machine this WorkUnit is currently assigned to
        /// </summary>
        public MachineInterface.iMachine AssignedMachine
        {
            get { return m_AssignedMachine; }
            private set { m_AssignedMachine = value; }
        }
        private MachineInterface.iMachine m_AssignedMachine;
        #endregion // Properties and Associated Member Data

        #region Static Member Data
        private static Object ms_IDLock = new Object();
        private static UInt32 ms_nCurrentID = 0;
        #endregion // Static Member Data

        #region Constructor
        /// <summary>
        /// cWorkUnit Constructor
        /// </summary>
        /// <param name="start">Start of work unit (in game-world coordinates)</param>
        /// <param name="end">End of work unit (in game-world coordinates)</param>
        public cWorkUnit(cVector2<float> start, cVector2<float> end)
        {
            lock (ms_IDLock)
            {
                this.ID = ms_nCurrentID;
                ++ms_nCurrentID;
            }

            AssignedMachine = null;
            StartWorld = start;
            EndWorld = end;
            State = etState.Queued;
        }
        #endregion

        #region Controller Methods
        /// <summary>
        /// Mark workunit as started, and assign to machine
        /// </summary>
        public void Start(MachineInterface.iMachine machine)
        {
            if (etState.Queued != State)
                throw new exWorkUnitException(this, "Workunit already started.");

            State = etState.Started;
            AssignedMachine = machine;
        }
        #endregion
    }

    #region Exception Class
    /// <summary>
    /// Exception raised with any Work Unit problems
    /// </summary>
    public class exWorkUnitException : Exception
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Associated workunit.
        /// </summary>
        public cWorkUnit WorkUnit
        {
            get { return m_WorkUnit; }
        }
        private cWorkUnit m_WorkUnit;
        #endregion // Properties and Associated Member Data

        #region Constructors
        public exWorkUnitException(cWorkUnit unit)
             : base( )
        {
            m_WorkUnit = unit;
        }

        public exWorkUnitException(cWorkUnit unit, String sMessage)
             : base(sMessage)
        {
            m_WorkUnit = unit;
        }

        public exWorkUnitException(cWorkUnit unit, String sMessage, Exception InnerException)
             : base(sMessage, InnerException)
        {
            m_WorkUnit = unit;
        }

        public exWorkUnitException(cWorkUnit unit, System.Runtime.Serialization.SerializationInfo info, 
                                   System.Runtime.Serialization.StreamingContext context)
             : base(info, context)
        {
            m_WorkUnit = unit;
        }
        #endregion // Constructors
    }
    #endregion

} // End of DistMonitor.Game namespace


// End of file
