//
// File: cMachineFarm.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of cMachineFarm.cs class
//

using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

using RSG.Base.Logging;

namespace DistMonitor
{

    /// <summary>
    /// Class representing a farm of machines
    /// </summary>
    internal sealed class cMachineFarm
    {
        #region Events
        /// <summary>
        /// Event raised whenever a machine is added to the farm.
        /// </summary>
        public event EventHandler<cFarmMachineEventArgs> MachineAddEvent;
        
        /// <summary>
        /// Event raised whenever a machine is removed from the farm.
        /// </summary>
        public event EventHandler<cFarmMachineEventArgs> MachineRemoveEvent;

        /// <summary>
        /// Event raised whenever a machine monitor is updated (periodic event raised in monitor threads)
        /// </summary>
        public event EventHandler<cFarmMonitorEventArgs> MonitorUpdateEvent;

        /// <summary>
        /// Event raised whenever a machine's status is changed
        /// </summary>
        public event EventHandler<MachineInterface.cMachineStatusEventArgs> MachineStatusChangeEvent;

        /// <summary>
        /// Event raised whenever a machine's enabled flag is changed
        /// </summary>
        public event EventHandler<MachineInterface.cMachineEnabledEventArgs> MachineEnabledChangeEvent;
        #endregion // Events

        #region Properties and Associated Member Data
        /// <summary>
        /// List of machines making up the farm
        /// </summary>
        public List<MachineInterface.iMachine> Machines
        {
            get { return m_Machines; }
            private set { m_Machines = value; }
        }
        private List<MachineInterface.iMachine> m_Machines;

        /// <summary>
        /// List of machine monitors
        /// </summary>
        public List<cMachineMonitor> Monitors
        {
            get { return m_Monitors; }
            private set { m_Monitors = value; }
        }
        private List<cMachineMonitor> m_Monitors;
        #endregion

        #region Constructors / Destructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public cMachineFarm()
        {
            Machines = new List<MachineInterface.iMachine>();
            Monitors = new List<cMachineMonitor>();
        }

        /// <summary>
        /// Constructor, specifying machines to initialize
        /// </summary>
        /// <param name="machines">List of machines to initialise Farm with</param>
        public cMachineFarm(List<MachineInterface.iMachine> machines)
        {
            Initialise(machines, true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="machines"></param>
        /// <param name="startMonitorThreads"></param>
        public cMachineFarm(List<MachineInterface.iMachine> machines,
                            bool startMonitorThreads)
        {
            Initialise(machines, startMonitorThreads);
        }
        #endregion // Constructors / Destructor

        #region Controller Methods
        /// <summary>
        /// Add a machine to the farm (automatically start monitoring machine)
        /// </summary>
        /// <param name="machine">Machine to add</param>
        public void Add(MachineInterface.iMachine machine)
        {
            this.Add(machine, true);
        }

        /// <summary>
        /// Add a machine to the farm
        /// </summary>
        /// <param name="machine">Machine to add</param>
        /// <param name="startMonitorThread">Start monitoring machine flag</param>
        public void Add(MachineInterface.iMachine machine, bool startMonitorThread)
        {
            cMachineMonitor monitor = 
                new cMachineMonitor(machine, startMonitorThread);
            monitor.MonitorStatusChangeEvent += OnMonitorUpdate;
            monitor.MachineStatusChangeEvent += OnMachineStatusChange;
            monitor.MachineStatusChangeEvent += machine.OnMonitorMachineStateChange;
            machine.MachineStatusChangeEvent += OnMachineStatusChange;
            machine.MachineEnabledChangeEvent += OnMachineEnabledChange;
            Machines.Add(machine);
            Monitors.Add(monitor);

            if (null != MachineAddEvent)
                MachineAddEvent(this, new cFarmMachineEventArgs(machine));

            Log.Log__Message(String.Format("Machine {0} added to farm.", 
                                 machine.IP.ToString()));
        }

        /// <summary>
        /// Remove a machine from the farm
        /// </summary>
        /// <param name="machine"></param>
        public void Remove(MachineInterface.iMachine machine)
        {
            cMachineMonitor monitor = null;
            foreach (cMachineMonitor mon in Monitors)
            {
                if (mon.Machine == machine)
                {
                    monitor = mon;
                    break;
                }
            }

            monitor.Monitoring = false;
            monitor.MonitorStatusChangeEvent -= OnMonitorUpdate;
            monitor.MachineStatusChangeEvent -= OnMachineStatusChange;
            monitor.MachineStatusChangeEvent -= machine.OnMonitorMachineStateChange;
            machine.MachineStatusChangeEvent -= OnMachineStatusChange;
            machine.MachineEnabledChangeEvent -= OnMachineEnabledChange;

            monitor.LogFile.Close();

            Monitors.Remove(monitor);
            Machines.Remove(machine);
         
            if (null != MachineRemoveEvent)
                MachineRemoveEvent(this, new cFarmMachineEventArgs(machine));

            Log.Log__Message(String.Format("Machine {0} remove from farm.", 
                                 machine.IP.ToString()));
        }

        /// <summary>
        /// Remove all machines from the farm
        /// </summary>
        public void RemoveAll()
        {
            while (Machines.Count > 0)
            {
                this.Remove(Machines[0]);
            }
        }
        #endregion // Controller Methods

        #region Public Event Handlers
        /// <summary>
        /// Handler for Machine Enabled Changes in Farm View
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnMachineEnabledChange(Object sender, UI.cFarmViewMachineEnabledEventArgs e)
        {
            e.Machine.Enabled = e.Checked;
        }

        #endregion // Public Event Handlers

        #region Public Methods

        public void MakeAllAvailable()
        {
            foreach (MachineInterface.iMachine machine in Machines)
            {
                machine.Available = true;
            }
        }

        public void Connect()
        {
            foreach (MachineInterface.iMachine machine in Machines)
            {
                if (!machine.Enabled || !machine.Available)
                {
                    machine.ForceDisconnectOnConnect = false;
                }
                else
                {
                    machine.ForceDisconnectOnConnect = Properties.Settings.Default.ForceDisconnectOnConnect;
                    if (machine.Enabled && machine.Available && machine.Status != MachineInterface.etMachineStatus.Off)
                    {
                        machine.Connect();
                    }                    
                }                
            }
        }

        public void Disconnect()
        {
            foreach (MachineInterface.iMachine machine in Machines)
            {
                // DW - SHOULD & WILL NOT FORCE DISCONNECT!
                // DW - Even if the machine is not enabled you need to try to disconnect.                
                if (machine.Status != MachineInterface.etMachineStatus.Off)
                {
                    if ((machine.Enabled) || ((!machine.Available) && machine.Failed) || machine.Failures > 0 || ((!machine.Enabled) && machine.Failed))
                    {
                        machine.Disconnect();
                    }
                }                  
            }
        }
        #endregion // Public Methods

        #region Private Methods
        /// <summary>
        /// Common initialisation method
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="machines"></param>
        /// <param name="startMonitorThreads"></param>
        private void Initialise(List<MachineInterface.iMachine> machines,
                                bool startMonitorThreads)
        {
            Machines = new List<MachineInterface.iMachine>();
            Monitors = new List<cMachineMonitor>();
            foreach (MachineInterface.iMachine machine in machines)
            {
                this.Add(machine, startMonitorThreads);
            }
        }
        #endregion // Private Methods

        #region Private Event Handlers
        /// <summary>
        /// Handler invoked for period monitor update events
        /// </summary>
        /// <param name="sender">Event source object (cMachineMonitor)</param>
        /// <param name="e"></param>
        /// This handler simply forwards the event on to any listeners, which
        /// will generally include the UI.uiFarmView component.
        /// 
        private void OnMonitorUpdate(Object sender, EventArgs e)
        {
            if (null != MonitorUpdateEvent)
                MonitorUpdateEvent(this, new cFarmMonitorEventArgs(sender as cMachineMonitor));
        }

        /// <summary>
        /// Handler invoked for machine status change events
        /// </summary>
        /// <param name="sender">Event source object (cMachineMonitor)</param>
        /// <param name="e"></param>
        /// This handler simply forwards the event on to any listeners, which
        /// will generally include the UI.uiFarmView component.
        /// 
        private void OnMachineStatusChange(Object sender, 
                                           MachineInterface.cMachineStatusEventArgs e)
        {
            if (null != MachineStatusChangeEvent)
                MachineStatusChangeEvent(this, e);
        }

        /// <summary>
        /// Handler invoked for machine enabled change events
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnMachineEnabledChange(Object sender,
                                            MachineInterface.cMachineEnabledEventArgs e)
        {
            if (null != MachineEnabledChangeEvent)
                MachineEnabledChangeEvent(this, e);
        }
        #endregion // Event Handlers
    }

    #region Event Argument Classes
    /// <summary>
    /// Single Machine Farm Event Arguments
    /// </summary>
    internal class cFarmMachineEventArgs : EventArgs
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Machine associated with the Farm Machine Event
        /// </summary>
        public MachineInterface.iMachine Machine
        {
            get { return m_Machine; }
            private set { m_Machine = value; }
        }
        private MachineInterface.iMachine m_Machine;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public cFarmMachineEventArgs(MachineInterface.iMachine machine)
        {
            Machine = machine;
        }
        #endregion
    };

    /// <summary>
    /// Single Monitor Farm Event Arguments
    /// </summary>
    internal class cFarmMonitorEventArgs : EventArgs
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Machine associated with the Farm Machine Event
        /// </summary>
        public cMachineMonitor Monitor
        {
            get { return m_Monitor; }
            private set { m_Monitor = value; }
        }
        private cMachineMonitor m_Monitor;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public cFarmMonitorEventArgs(cMachineMonitor monitor)
        {
            Monitor = monitor;
        }
        #endregion
    };
    #endregion // Event Argument Classes

} // End of DistMonitor.Machine namespace

// End of file
