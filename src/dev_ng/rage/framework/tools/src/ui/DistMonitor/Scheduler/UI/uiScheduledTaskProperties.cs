//
// File: uiScheduledTaskProperties.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of uiScheduledTaskProperties.cs class
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using GameInterface;
using ToolInterface;

namespace DistMonitor.Scheduler.UI
{
    
    /// <summary>
    /// Scheduled Task Properties Form
    /// </summary>
    internal partial class uiScheduledTaskProperties : Form
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Name string
        /// </summary>
        public String TaskName
        {
            get { return this.txtName.Text; }
        }

        /// <summary>
        /// Game string
        /// </summary>
        public String Game
        {
            get { return this.cbGame.Text; }
        }

        /// <summary>
        /// Configuration string
        /// </summary>
        public String Configuration
        {
            get { return this.cbConfiguration.Text; }
        }

        /// <summary>
        /// Tool string
        /// </summary>
        public String Tool
        {
            get { return this.cbTool.Text; }
        }

        /// <summary>
        /// Start DateTime
        /// </summary>
        public DateTime StartDateTime
        {
            get { return this.dtStartDatePicker.Value; }
        }

        /// <summary>
        /// End DateTime
        /// </summary>
        public DateTime EndDateTime
        {
            get { return this.dtEndDatePicker.Value; }
        }

        /// <summary>
        /// Schedule string
        /// </summary>
        public String Schedule
        {
            get { return this.cbSchedule.Text; }
        }
        #endregion // Properties and Associated Member Data

        #region Member Data
        /// <summary>
        /// Task being configured
        /// </summary>
        private cScheduledTask m_Task;
        #endregion // Member Data

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public uiScheduledTaskProperties()
        {
            InitializeComponent();
            PopulateComponents();
        }
        #endregion // Constructor

        #region Controller Methods
        /// <summary>
        /// Set reference to Scheduled Task
        /// </summary>
        /// <param name="task"></param>
        public void SetTask(cScheduledTask task)
        {
            m_Task = task;
            RefreshComponents();
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Populate form components (e.g. comboboxes with current configuration data)
        /// </summary>
        private void PopulateComponents()
        {
            // Clear Components
            cbGame.Items.Clear();
            cbConfiguration.Items.Clear();
            cbTool.Items.Clear();

            // Populate Game / Configuration Lists
            foreach (iGame game in Program.Controller.GameList.Games)
            {
                cbGame.Items.Add(game.Name);
                foreach (cConfiguration config in game.Configurations)
                    cbConfiguration.Items.Add(config.ToString());
            }
            // Populate Tool List
            foreach (Type toolType in Program.Controller.ToolPlugins.Plugins)
            {
                iTool tool = Program.Controller.ToolPlugins.CreatePluginInstance(toolType);
                cbTool.Items.Add(tool.Name);
            }

            // Reset selection
            if (cbGame.Items.Count > 0)
                cbGame.SelectedIndex = 0;
            if (cbConfiguration.Items.Count > 0)
                cbConfiguration.SelectedIndex = 0;
            if (cbTool.Items.Count > 0)
                cbTool.SelectedIndex = 0;
            if (cbSchedule.Items.Count > 0)
                cbSchedule.SelectedIndex = 0;
        }

        /// <summary>
        /// Refresh form components to reflect current Task
        /// </summary>
        private void RefreshComponents()
        {
            if (null == m_Task)
                return;

            // Set UI Widgets to match Task Properties
            txtName.Text = m_Task.Name;
            for (int nGame = 0; nGame < cbGame.Items.Count; ++nGame)
            {
                if (cbGame.Items[nGame].ToString() == m_Task.Game)
                {
                    cbGame.SelectedIndex = nGame;
                    break;
                }
            }
            for (int nConfig = 0; nConfig < cbConfiguration.Items.Count; ++nConfig)
            {
                if (cbConfiguration.Items[nConfig].ToString() == m_Task.Configuration)
                {
                    cbConfiguration.SelectedIndex = nConfig;
                    break;
                }
            }
            for (int nTool = 0; nTool < cbTool.Items.Count; ++nTool)
            {
                if (cbTool.Items[nTool].ToString() == m_Task.Tool)
                {
                    cbTool.SelectedIndex = nTool;
                    break;
                }
            }
            dtStartDatePicker.Value = m_Task.Schedule.NextTime;
            dtEndDatePicker.Value = m_Task.Schedule.EndTime;
            cbSchedule.SelectedIndex = (int)m_Task.Schedule.Type;
        }
        #endregion // Private Methods
    }

} // End of DistMonitor.Scheduler.UI namespace

// End of file
