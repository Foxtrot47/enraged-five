//
// File: cAreaProgressFileMonitor.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of cAreaProgressFileMonitor.cs class
//

using System;
using System.Diagnostics;
using System.IO;

namespace DistMonitor.AreaTools
{
    
    /// <summary>
    /// 
    /// </summary>
    public class cAreaProgressFileMonitor
    {
        #region Enumerations
        /// <summary>
        /// State Enumeration
        /// </summary>
        public enum etState
        {
            Queued,
            Started,
            Finished
        }
        #endregion // Enumerations

        #region Properties and Associated Member Data
        /// <summary>
        /// Progress state
        /// </summary>
        public etState State
        {
            get { return m_eState; }
            private set { m_eState = value; }
        }
        private etState m_eState;

        /// <summary>
        /// Filename
        /// </summary>
        public String Filename
        {
            get { return m_sFilename; }
            private set { m_sFilename = value; }
        }
        private String m_sFilename;

        /// <summary>
        /// File Modified Date/Time
        /// </summary>
        /// Calculated to avoid Window's File Modified Time caching as file
        /// is not closed between writes from the Game.
        public DateTime ModifiedTime
        {
            get { return m_dtModifiedTime; }
            private set { m_dtModifiedTime = value; }
        }
        private DateTime m_dtModifiedTime;

        /// <summary>
        /// Last sector processed
        /// </summary>
        public RsBase.Math.cVector2<int> LastSector
        {
            get { return m_vLastSector; }
            private set { m_vLastSector = value; }
        }
        private RsBase.Math.cVector2<int> m_vLastSector;

        /// <summary>
        /// Previous sector processed
        /// </summary>
        public RsBase.Math.cVector2<int> PreviousSector
        {
            get { return m_vPreviousSector; }
            private set { m_vPreviousSector = value; }
        }
        private RsBase.Math.cVector2<int> m_vPreviousSector;
        #endregion // Properties and Associated Member Data

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public cAreaProgressFileMonitor(String filename)
        {
            State = etState.Queued;
            Filename = filename;
            ModifiedTime = DateTime.Now;

            this.Parse();
        }
        #endregion // Constructor

        #region Controller Methods
        /// <summary>
        /// Parse File
        /// </summary>
        void Parse()
        {
            if (!File.Exists(Filename))
                return;

            String[] contents = File.ReadAllLines(Filename);
            if (contents.Length > 0)
            {
                // Find start and end tokens
                for (int nLine = 0; nLine < contents.Length; ++nLine )
                {
                    String sTrimLine = contents[nLine].Trim();
                    if (0 == sTrimLine.Length)
                        continue;

                    if (sTrimLine.StartsWith("START"))
                    {
                        State = etState.Started;
                        continue;
                    }
                    else if (sTrimLine.StartsWith("END"))
                    {
                        State = etState.Finished;
                        continue;
                    }

                    if (nLine == (contents.Length - 3))
                        PreviousSector = ParseCoords(sTrimLine);
                    else if (nLine >= (contents.Length - 2))
                        LastSector = ParseCoords(sTrimLine);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sCoords"></param>
        /// <returns></returns>
        private RsBase.Math.cVector2<int> ParseCoords(String line)
        {
            // Attempt to parse coords
            String[] sCoords = line.Split(new char[] { ' ' },
                                    StringSplitOptions.RemoveEmptyEntries);
            Debug.Assert(2 == sCoords.Length);
            if (2 != sCoords.Length)
                return (null);

            RsBase.Math.cVector2<int> vVec = 
                new RsBase.Math.cVector2<int>(int.Parse(sCoords[0]), int.Parse(sCoords[1]));
            return vVec;
        }
        #endregion // Controller Methods
    }

} // End of DistMonitor.AreaTools namespace

// End of file
