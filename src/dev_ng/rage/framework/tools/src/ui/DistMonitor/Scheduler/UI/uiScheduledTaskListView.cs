using System;
//using System.Collections.Generic;
using System.Windows.Forms;

namespace DistMonitor.Scheduler.UI
{

    /// <summary>
    /// 
    /// </summary>
    internal class uiScheduledTaskListView : ListView
    {
        #region Events
        /// <summary>
        /// Event raised when a task is selected
        /// </summary>
        public event EventHandler<cScheduledTaskEventArgs> TaskSelectionEvent;
        #endregion // Events

        #region Properties and Associated Member Data
        /// <summary>
        /// 
        /// </summary>
        public cScheduledTaskManager ScheduledTaskManager
        {
            get { return m_ScheduledTaskManager; }
            set { m_ScheduledTaskManager = value; if (null != m_ScheduledTaskManager) Repopulate(); }
        }
        private cScheduledTaskManager m_ScheduledTaskManager;
        #endregion // Properties and Associated Member Data

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public uiScheduledTaskListView()
            : base()
        {
            this.SelectedIndexChanged += OnSelectedChanged;
        }
        #endregion // Constructor

        #region Controller Methods
        /// <summary>
        /// Refresh View
        /// </summary>
        public void UpdateView()
        {
            foreach (ListViewItem item in this.Items)
            {
                uiScheduledTaskListViewItem taskItem = (item as uiScheduledTaskListViewItem);
                taskItem.UpdateView();
            }
        }
        #endregion // Controller Methods

        #region Public Event Handlers
        /// <summary>
        /// Scheduled Task Manager Task Added Event Handler
        /// </summary>
        /// <param name="sender">Source of event</param>
        /// <param name="e">Event arguments object</param>
        public void OnTaskAdded(Object sender, cScheduledTaskEventArgs e)
        {
            uiScheduledTaskListViewItem taskItem = new uiScheduledTaskListViewItem(e.Task);
            this.Items.Add(taskItem);
        }

        /// <summary>
        /// Scheduled Task Manager Task Removed Event Handler
        /// </summary>
        /// <param name="sender">Source of event</param>
        /// <param name="e">Event arguments object</param>
        public void OnTaskRemoved(Object sender, cScheduledTaskEventArgs e)
        {
            uiScheduledTaskListViewItem taskItem = null;
            foreach (ListViewItem item in this.Items)
            {
                taskItem = (item as uiScheduledTaskListViewItem);
                if (taskItem.Task == e.Task)
                    break;
            }

            if (null != taskItem)
                this.Items.Remove(taskItem);
        }

        /// <summary>
        /// Scheduled Task Manager Task Changed Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnTaskChanged(Object sender, cScheduledTaskEventArgs e)
        {
            uiScheduledTaskListViewItem taskItem = null;
            foreach (ListViewItem item in this.Items)
            {
                taskItem = (item as uiScheduledTaskListViewItem);
                if (taskItem.Task == e.Task)
                    break;
            }

            if (null != taskItem)
                taskItem.UpdateView();
        }
        #endregion // Public Event Handlers

        #region Private Event Handlers
        /// <summary>
        /// Selected Index Change Event Handler
        /// </summary>
        /// <param name="sender">Source of event</param>
        /// <param name="e">Event arguments object</param>
        private void OnSelectedChanged(Object sender, EventArgs e)
        {            
            if (null != TaskSelectionEvent)
            {
                cScheduledTask task = null;

                if (0 != this.SelectedItems.Count)
                    task = (this.SelectedItems[0] as uiScheduledTaskListViewItem).Task;

                TaskSelectionEvent(this, new cScheduledTaskEventArgs(task));
            }
        }
        #endregion // Private Event Handlers

        #region Private Methods
        /// <summary>
        /// Repopulate the ListView
        /// </summary>
        private void Repopulate()
        {
            this.BeginUpdate();
            this.Items.Clear();
            foreach (cScheduledTask task in this.ScheduledTaskManager.ScheduledTasks)
            {
                this.Items.Add(new uiScheduledTaskListViewItem(task));
            }
            this.EndUpdate();

            OnSelectedChanged(this, new EventArgs());
        }
        #endregion // Private Methods
    }

    #region Scheduled Task List View Item
    /// <summary>
    /// 
    /// </summary>
    internal sealed class uiScheduledTaskListViewItem : ListViewItem
    {
        #region Properties and Associated Member Data
        public cScheduledTask Task
        {
            get { return m_Task; }
            private set { m_Task = value; }
        }
        private cScheduledTask m_Task;
        #endregion // Properties and Associated Member Data

        #region Constructors
        /// <summary>
        /// Basic constructor
        /// </summary>
        /// <param name="task"></param>
        public uiScheduledTaskListViewItem(cScheduledTask task)
            : base()
        {
            m_Task = task;
            Init();
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="task"></param>
        public uiScheduledTaskListViewItem(ListViewGroup group, cScheduledTask task)
            : base(group)
        {
            m_Task = task;
            Init();
        }        
        
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="task"></param>
        public uiScheduledTaskListViewItem(String text, cScheduledTask task)
            : base(text)
        {
            m_Task = task;
            Init();
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="task"></param>
        public uiScheduledTaskListViewItem(String text, ListViewGroup group, cScheduledTask task)
            : base(text, group)
        {
            m_Task = task;
            Init();
        }
        #endregion // Constructors

        #region Controller Methods
        /// <summary>
        /// Refresh representation of cScheduleTask object
        /// </summary>
        public void UpdateView()
        {
            this.SubItems[0].Text = Task.Name;
            this.SubItems[1].Text = Task.Schedule.NextTime.ToString("yyyy-MM-dd HH:mm");
            this.SubItems[2].Text = Task.Schedule.EndTime.ToString("yyyy-MM-dd HH:mm");
            this.SubItems[3].Text = Task.Schedule.LastTime.ToString("yyyy-MM-dd HH:mm");
            this.SubItems[4].Text = Task.Schedule.Type.ToString();
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Initialise ListViewItem
        /// </summary>
        private void Init()
        {
            this.SubItems.Add(new ListViewSubItem());
            this.SubItems.Add(new ListViewSubItem());
            this.SubItems.Add(new ListViewSubItem());
            this.SubItems.Add(new ListViewSubItem());
            this.SubItems.Add(new ListViewSubItem());
            this.UpdateView();
        }
        #endregion // Private Methods
    };
    #endregion // Scheduled Task List View Item
}
