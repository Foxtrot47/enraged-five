//
// File: cGameList.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of cGameList.cs class
//

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

using GameInterface;

using RSG.Base.Logging;
using RSG.Base.Math;

namespace DistMonitor
{
    
    /// <summary>
    /// 
    /// </summary>
    public class cGameList
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// List of available games
        /// </summary>
        public List<iGame> Games
        {
            get { return m_Games; }
            private set { m_Games = value; }
        }
        private List<iGame> m_Games;
        #endregion

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public cGameList()
        {
            Games = new List<iGame>();
        }
        
        /// <summary>
        /// Constructor from prepopulated list of Games
        /// </summary>
        /// <param name="games"></param>
        public cGameList(List<iGame> games)
        {
            Games = games;
        }

        /// <summary>
        /// Constructor from XML file or plugin directory
        /// </summary>
        /// <param name="sFileOrPath"></param>
        public cGameList(String sFileOrPath)
        {
            Games = new List<iGame>();

            if (System.IO.File.Exists(sFileOrPath))
            {
                ParseXML(sFileOrPath);
            }
            else if (System.IO.Directory.Exists(sFileOrPath))
            {
                /// TODO Load All GameInterface.iGame Plugins
            }
            else
            {
                Log.Log__Error(
                    String.Format("Cannot populate game list from file or directory that does not exist ({0}).",
                                  sFileOrPath));
            }
        }
        #endregion

        #region Controller Methods
        /// <summary>
        /// Add a game to the list
        /// </summary>
        /// <param name="game"></param>
        public void Add(iGame game)
        {
            Games.Add(game);
        }

        /// <summary>
        /// Clear all games from the list
        /// </summary>
        public void Clear()
        {
            Games.Clear();
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Parse XML Game Configuration File
        /// </summary>
        /// <param name="sFilename"></param>
        private void ParseXML(String sFilename)
        {
            try
            {
                XmlTextReader xmlReader = new XmlTextReader(sFilename);
                List<GameInterface.cConfiguration> configs = null;
                String sGameName = String.Empty;
                String sGameRoot = String.Empty;
                float fSectorSize = 0.0f;

                while (xmlReader.Read())
                {
                    switch (xmlReader.NodeType)
                    {
                        case XmlNodeType.Element:
                            if ("game" == xmlReader.Name)
                            {
                                configs = new List<GameInterface.cConfiguration>();
                                for (int nAttr = 0; nAttr < xmlReader.AttributeCount; ++nAttr)
                                {
                                    xmlReader.MoveToAttribute(nAttr);
                                    if ("name" == xmlReader.Name)
                                        sGameName = xmlReader.Value;
                                    else if ("root" == xmlReader.Name)
                                        sGameRoot = xmlReader.Value;
                                    else if ("sectorsize" == xmlReader.Name)
                                        float.TryParse(xmlReader.Value, out fSectorSize);
                                }
                            }
                            else if ("configuration" == xmlReader.Name)
                            {
                                String sMachine = String.Empty;
                                String sName = String.Empty;
                                String sRoot = String.Empty;
                                String sExe = String.Empty;
                                String sArguments = String.Empty;
                                for (int nAttr = 0; nAttr < xmlReader.AttributeCount; ++nAttr)
                                {
                                    xmlReader.MoveToAttribute(nAttr);
                                    if ("machine" == xmlReader.Name)
                                        sMachine = xmlReader.Value;
                                    else if ("name" == xmlReader.Name)
                                        sName = xmlReader.Value;
                                    else if ("root" == xmlReader.Name)
                                        sRoot = xmlReader.Value;
                                    else if ("executable" == xmlReader.Name)
                                        sExe = xmlReader.Value;
                                    else if ("args" == xmlReader.Name)
                                        sArguments = xmlReader.Value;
                                }
                                configs.Add(new GameInterface.cConfiguration(sMachine, sName, sRoot, sExe, sArguments));
                            }
                            break;
                        case XmlNodeType.EndElement:
                            if ("game" == xmlReader.Name)
                            {
                                GameInterface.iGame game = 
                                    new GameInterface.cGameXML(sGameName, sGameRoot, fSectorSize, configs);
                                Games.Add(game);
                            }
                            break;
                    }
                }
            }
            catch (XmlException e)
            {
                Log.Log__Error(String.Format("Exception during Game List XML Parsing of {0} at [{1}, {2}]: {1}.",
                                          sFilename, e.LineNumber, e.LinePosition, e.Message));
            }
        }
        #endregion
    }

} // End of DistMonitor.Game namespace


// End of file
