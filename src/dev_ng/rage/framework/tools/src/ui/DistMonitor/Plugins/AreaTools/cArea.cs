//
// File: cArea.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of cArea.cs class
//

using System;
using System.Xml;

using RSG.Base.Logging;
using RSG.Base.Math;

namespace AreaTools
{
    
    /// <summary>
    /// Area class
    /// </summary>
    /// Represents an area within a game world.
    public class cArea
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Area start position in game-world coordinates
        /// </summary>
        public Vector2f Start
        {
            get { return m_vfStart; }
        }
        private Vector2f m_vfStart;

        /// <summary>
        /// Area end position in game-world coordinates
        /// </summary>
        public Vector2f End
        {
            get { return m_vfEnd; }
        }
        private Vector2f m_vfEnd;

        /// <summary>
        /// Workunit size (per machine) in sectors
        /// </summary>
        public Vector2i WorkUnit
        {
            get { return m_vnWorkUnit; }
        }
        private Vector2i m_vnWorkUnit;

        /// <summary>
        /// Sector size in game-world units
        /// </summary>
        public float SectorSize
        {
            get { return m_fSectorSize; }
        }
        private float m_fSectorSize;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="wu"></param>
        public cArea(Vector2f start, Vector2f end, Vector2i wu, float fSectorSize)
        {
            m_vfStart = start;
            m_vfEnd = end;
            m_vnWorkUnit = wu;
            m_fSectorSize = fSectorSize;
        }

        /// <summary>
        /// Constructor (from XML File)
        /// </summary>
        /// <param name="sFilename"></param>
        public cArea(String sFilename)
        {
            ParseXML(sFilename);
        }
        #endregion

        #region Private Methods
                /// <summary>
        /// Parse XML Game Configuration File
        /// </summary>
        /// <param name="sFilename"></param>
        private void ParseXML(String sFilename)
        {
            try
            {
                XmlTextReader xmlReader = new XmlTextReader(sFilename);

                while (xmlReader.Read())
                {
                    switch (xmlReader.NodeType)
                    {
                        case XmlNodeType.Element:
                            if ("area" == xmlReader.Name)
                            {
                                Vector2f start = new Vector2f();
                                Vector2f end = new Vector2f();
                                Vector2i workunit = new Vector2i();
                                float sectorsize = 50.0f;
                                for (int nAttr = 0; nAttr < xmlReader.AttributeCount; ++nAttr)
                                {
                                    xmlReader.MoveToAttribute(nAttr);
                                    if ("start" == xmlReader.Name)
                                        start = Vector2f.Parse(xmlReader.Value);
                                    else if ("end" == xmlReader.Name)
                                        end = Vector2f.Parse(xmlReader.Value);
                                    else if ("workunit" == xmlReader.Name)
                                        workunit = Vector2i.Parse(xmlReader.Value);
                                    else if ("sectorsize" == xmlReader.Name)
                                        sectorsize = float.Parse(xmlReader.Value);
                                }
                                this.m_vfStart = start;
                                this.m_vfEnd = end;
                                this.m_fSectorSize = sectorsize;
                                this.m_vnWorkUnit = workunit;
                            }
                            break;
                        case XmlNodeType.EndElement:
                            break;
                    }
                }
            }
            catch (XmlException e)
            {
                Log.Log__Error(String.Format("Exception during Area XML Parsing of {0} at [{1}, {2}]: {1}.",
                                   sFilename, e.LineNumber, e.LinePosition, e.Message));
            }
        }
        #endregion // Private Methods
    }

} // End of GameInterface namespace

// End of file
