//
// File: uiFarmLog.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of uiFarmLog.cs class
//

using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;

namespace DistMonitor.UI
{

    #region Main uiFarmLog Class
    /// <summary>
    /// Machine Farm Log Control
    /// </summary>
    internal partial class uiFarmLog : UserControl
    {
        #region Delegates
        /// <summary>
        /// Delegate for thread-safe Repopulating the ListView
        /// </summary>
        private delegate void RepopulateDelegate();
        #endregion // Delegates

        #region Properties and Associated Member Data
        /// <summary>
        /// Farm this view is viewing
        /// </summary>
        public cMachineFarm Farm
        {
            get { return m_Farm; }
            set { m_Farm = value; Repopulate(); }
        }
        private cMachineFarm m_Farm;

        /// <summary>
        /// Thread Monitoring active flag
        /// </summary>
        public bool Monitoring
        {
            get { return m_bMonitoring; }
            private set { m_bMonitoring = value; }
        }
        private bool m_bMonitoring;
        #endregion // Properties and Associated Member Data

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public uiFarmLog()
        {
            InitializeComponent();
        }
        #endregion // Constructor

        #region Controller Methods
        /// <summary>
        /// Set ThreadPoolView's ThreadPool
        /// </summary>
        /// <param name="pool"></param>
        public void SetThreadPool(RSG.Base.Threading.cThreadPool pool)
        {
            this.uiThreadPoolView.SetThreadPool(pool);
        }

        /// <summary>
        /// Clear text from main Application Log
        /// </summary>
        public void ClearAppLog()
        {
            Log.Clear();
        }

        /// <summary>
        /// Clear text from Scheduler Log
        /// </summary>
        public void ClearSchedulerLog()
        {
            SchedulerLog.Clear();
        }

        /// <summary>
        /// Clear text from all Machine Logs
        /// </summary>
        public void ClearMachineLogs()
        {
            foreach (TabPage tab in tabCtrlLogs.TabPages)
            {
                if (!(tab is uiFarmLogDebugTabPage))
                    continue;

                uiFarmLogDebugTabPage ftab = (tab as uiFarmLogDebugTabPage);
                ftab.ClearLog();
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Repopulate farm list
        /// </summary>
        private void Repopulate()
        {
            if (tabCtrlLogs.InvokeRequired)
            {
                Invoke(new RepopulateDelegate(Repopulate));
            }
            else
            {
                RemoveMachineLogs();
                if (null != Farm)
                {
                    foreach (cMachineMonitor monitor in Farm.Monitors)
                    {
                        if (null == monitor.Machine.DebugLog)
                            continue;

                        uiFarmLogDebugTabPage pgLog = 
                            new uiFarmLogDebugTabPage(monitor.Machine.IP.ToString(), monitor.Machine);
                        tabCtrlLogs.TabPages.Add(pgLog);
                    }
                }                
            }
        }

        /// <summary>
        /// Remove Machine Logs from View
        /// </summary>
        private void RemoveMachineLogs()
        {
            List<TabPage> pages = new List<TabPage>();
            foreach (TabPage page in tabCtrlLogs.TabPages)
            {
                if (page is uiFarmLogDebugTabPage)
                    pages.Add(page);
            }
            foreach (TabPage page in pages)
            {
                tabCtrlLogs.TabPages.Remove(page);
            }
        }
        #endregion

        #region Machine Farm Event Handlers
        /// <summary>
        /// Event handler for when machine is added to Machine Farm
        /// </summary>
        /// <param name="sender">Source of event (cMachineFarm)</param>
        /// <param name="e">Object containing machine concerned</param>
        public void OnMachineAdd(object sender, cFarmMachineEventArgs e)
        {
            foreach (TabPage tab in tabCtrlLogs.TabPages)
            {
                if ((tab is uiFarmLogDebugTabPage) && (tab.Text == e.Machine.IP.ToString()))
                    throw new Exception(String.Format("Machine {0} already has a log tabpage.  Internal error.",
                                        e.Machine.IP));
            }

            uiFarmLogDebugTabPage pgLog =
                new uiFarmLogDebugTabPage(e.Machine.IP.ToString(), e.Machine);
            tabCtrlLogs.TabPages.Add(pgLog);
        }

        /// <summary>
        /// Event handler for when machine is removed from Machine Farm
        /// </summary>
        /// <param name="sender">Source of event (cMachineFarm)</param>
        /// <param name="e">Object containing machine concerned</param>
        public void OnMachineRemove(object sender, cFarmMachineEventArgs e)
        {
            TabPage machineTab = null;
            foreach (TabPage tab in tabCtrlLogs.TabPages)
            {
                if (tab.Text == e.Machine.IP.ToString())
                    machineTab = tab;
            }
            if (null == machineTab)
                throw new Exception(String.Format("Machine {0} does not have a log tabpage.  Internal error.",
                                    e.Machine.IP));

            tabCtrlLogs.TabPages.Remove(machineTab);
        }

        /// <summary>
        /// Event handler for when machine monitors are updated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnMonitorUpdate(object sender, cFarmMonitorEventArgs e)
        {

        }
        #endregion // Machine Farm Event Handlers
    }
    #endregion // Main uiFarmLog Class

    #region Farm Machine Debug Log Tab
    /// <summary>
    /// Debug Log Tab Page
    /// </summary>
    class uiFarmLogDebugTabPage : TabPage
    {
        #region Delegates
        private delegate void OnDebugTextDelegate(object sender, MachineInterface.cDebugLogTextEventArgs e);
        #endregion

        #region Properties and Associated Member Data
        /// <summary>
        /// Machine
        /// </summary>
        public MachineInterface.iMachine Machine
        {
            get { return m_Machine; }
            private set { m_Machine = value; }
        }
        private MachineInterface.iMachine m_Machine;
        #endregion // Properties and Associated Member Data

        #region Member Data
        /// <summary>
        /// Textbox to display debug messages
        /// </summary>
        private RichTextBox m_TextBox;
        #endregion // Member Data

        #region Constructors / Destructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public uiFarmLogDebugTabPage(MachineInterface.iMachine machine) 
            : base()
        {
            Initialise(machine);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="text"></param>
        public uiFarmLogDebugTabPage(String text, MachineInterface.iMachine machine)
            : base(text)
        {
            Initialise(machine);
        }
        #endregion // Constructors / Destructor

        #region Controller Methods
        /// <summary>
        /// Clear text from log RichTextBox
        /// </summary>
        public void ClearLog()
        {
            m_TextBox.Clear();
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Initialisation
        /// </summary>
        private void Initialise(MachineInterface.iMachine machine)
        {
            Machine = machine;
            m_TextBox = new RichTextBox();
            Controls.Add(m_TextBox);
            m_TextBox.Dock = DockStyle.Fill;

            machine.DebugLog.DebugTextEvent += OnDebugText;

            OnDebugText(this, 
                new MachineInterface.cDebugLogTextEventArgs(Machine, 
                String.Format("DEBUG LOG for {0}.\n\n", Machine.IP)));
        }

        /// <summary>
        /// Event Handler for receiving XBox 360 Debug Text
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnDebugText(object sender, MachineInterface.cDebugLogTextEventArgs e)
        {
            if (this.IsDisposed || m_TextBox.IsDisposed)
                return;

            if ((null == e.Text) || (String.Empty == e.Text))
                return;

            if (m_TextBox.InvokeRequired)
            {
                // Invoke delegate in a thread-safe way
                Invoke(new OnDebugTextDelegate(OnDebugText), new object[] { this, e });
            }
            else
            {
                // Otherwise log the text
                m_TextBox.AppendText(e.Text);
            }
        }
        #endregion
    }
    #endregion // Farm Machine Debug Log Tab

} // End of DistMonitor.UI namespace

// End of file
