//
// File: uiAreaToolConfig.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of uiAreaToolConfig.cs class
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AreaTools.UI
{

    /// <summary>
    /// 
    /// </summary>
    public partial class uiAreaToolConfig : Form
    {
        #region Properties and Associated Member Data

        /// start work unit string
        /// </summary>
        public Int32 StartWorkUnit
        {
            get { return m_StartWorkUnit; }
            private set { m_StartWorkUnit = value; }
        }
        private Int32 m_StartWorkUnit;

        /// end work unit string
        /// </summary>
        public Int32 EndWorkUnit
        {
            get { return m_EndWorkUnit; }
            private set { m_EndWorkUnit = value; }
        }
        private Int32 m_EndWorkUnit;

        /// Use Date string
        /// </summary>
        public String UseDate
        {
            get { return m_sUseDate; }
            private set { m_sUseDate = value; }
        }
        private String m_sUseDate;

        /// <summary>
        /// Game Time string
        /// </summary>
        public String GameTime 
        {
            get { return m_sGameTime; }
            private set { m_sGameTime = value; }
        }
        private String m_sGameTime;
        
        /// <summary>
        /// Game level string.
        /// </summary>
        public String GameLevel
        {
            get { return m_sGameLevel; }
            private set { m_sGameLevel = value; }
        }
        private String m_sGameLevel;
        #endregion // Properties and Associated Member Data

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public uiAreaToolConfig()
        {
            InitializeComponent();
            cbGameTime.SelectedIndex = 0;
            GameTime = cbGameTime.Items[0].ToString();
            GameLevel = cbGameLevel.Items[0].ToString();
            StartWorkUnit = 0;
            EndWorkUnit = 99999;
            
            String sDate = String.Format("{0:yyyy}{0:MM}{0:dd}", DateTime.Now);
            tbUseDate.Text = sDate;
        }
        #endregion // Constructor

        #region UI Event Handlers
        /// <summary>
        /// StartWorkUnit numeric up down Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void nudStartWorkUnit_NumChanged(Object sender, EventArgs e)
        {
            this.StartWorkUnit = (Int32)nudStartWorkUnit.Value;
        }

        /// <summary>
        /// EndWorkUnit numeric up down Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void nudEndWorkUnit_NumChanged(Object sender, EventArgs e)
        {
            this.EndWorkUnit = (Int32)nudEndWorkUnit.Value;
        }

        /// <summary>
        /// UseDate TextBox Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbUseDate_TextChanged(Object sender, EventArgs e)
        {
            this.UseDate = tbUseDate.Text;
        }

        /// <summary>
        /// GameTime Combobox Selection Change Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbGameTime_SelectedIndexChanged(Object sender, EventArgs e)
        {
            this.GameTime = cbGameTime.SelectedItem.ToString().ToLower();
        }

        /// <summary>
        /// GameLevel Combobox Selection Change Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbGameLevel_SelectedIndexChanged(object sender, EventArgs e)
        {
            String level = cbGameLevel.SelectedItem.ToString().ToLower();
            level = level.Replace("gta5 - ", "");
            this.GameLevel = level;
        }
        #endregion // UI Event Handlers

        private void tblConfigLayout_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void uiAreaToolConfig_Load(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }

} // End of AreaTools.UI namespace

// End of file
