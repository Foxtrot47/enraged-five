//
// File: cMailingListXml.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of cMailingListXml.cs class
//

using System;
using System.Net.Mail;
using System.Xml;

using RSG.Base.Logging;

namespace ToolInterface
{

    /// <summary>
    /// Mailing List XML File Parser
    /// </summary>
    public sealed class cMailingListXml
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Mailing List Recipients
        /// </summary>
        public MailAddressCollection Recipients
        {
            get { return m_Recipients; }
            private set { m_Recipients = value; }
        }
        private MailAddressCollection m_Recipients;

        /// <summary>
        /// XML File Filename parsed
        /// </summary>
        public String Filename
        {
            get { return m_sFilename; }
            private set { m_sFilename = value; }
        }
        private String m_sFilename;
        #endregion // Properties and Associated Member Data

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="filename"></param>
        public cMailingListXml(String filename)
        {
            this.Filename = filename;
            this.Recipients = new MailAddressCollection();

            Parse(this.Filename);
        }
        #endregion // Constructor
        
        #region Private Methods
        /// <summary>
        /// Parse XML File for recipients
        /// </summary>
        /// <param name="filename"></param>
        private void Parse(String filename)
        {
            try
            {
                XmlTextReader xmlParser = new XmlTextReader(filename);
                while (xmlParser.Read())
                {
                    if ("recipient" == xmlParser.Name)
                    {
                        String sName = String.Empty;
                        String sEmail = String.Empty;

                        for (int nAttr = 0; nAttr < xmlParser.AttributeCount; ++nAttr)
                        {
                            xmlParser.MoveToAttribute(nAttr);
                            if ("name" == xmlParser.Name)
                                sName = xmlParser.Value;
                            else if ("email" == xmlParser.Name)
                                sEmail = xmlParser.Value;
                        }

                        this.Recipients.Add(new MailAddress(sEmail, sName));
                    }
                }
                xmlParser.Close();
            }
            catch (XmlException ex)
            {
                Log.Log__Error(String.Format("Mailing List XML Parse Error [{0}, {1}]: {2}", 
                                          ex.LineNumber, ex.LinePosition, ex.Message));
            }
        }
        #endregion // Private Methods
    }

} // End of ToolInterface

// End of file
