//
// File: cAreaToolBase.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of cAreaToolBase.cs class
//

using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

using RSG.Base.Logging;

namespace AreaTools
{

    /// <summary>
    /// Abstract base class for all AreaTool Tools
    /// </summary>
    /// This base class is used to implement common functionality between all 
    /// area tools as they are generally very similar in operation.
    /// 
    public abstract class cAreaToolBase : ToolInterface.iTool
    {
        #region Constants
        /// <summary>
        /// Timeout
        /// </summary>
        protected const int TIMEOUT_MINS = 5;
        #endregion // Constants

        #region Events
        /// <summary>
        /// Event raised when alert message are needing to be displayed
        /// </summary>
        public event EventHandler<ToolInterface.cToolMessageEventArgs> ToolAlertMessageEvent;

        /// <summary>
        /// Event raised when tool is finished
        /// </summary>
        public event EventHandler<ToolInterface.cToolEventArgs> ToolFinishedEvent;
        #endregion // Events

        #region Properties and Associated Member Data
        public abstract String Name { get; }

        public ToolInterface.etToolStatus Status
        {
            get { return m_eStatus; }
        }
        ToolInterface.etToolStatus m_eStatus;

        /// <summary>
        /// Area Monitor
        /// </summary>
        public cAreaManager AreaManager
        {
            get { return m_AreaManager; }
            private set { m_AreaManager = value; }
        }
        private cAreaManager m_AreaManager;

        public UI.uiAreaView AreaView
        {
            get { return m_AreaView; }
            private set { m_AreaView = value; }
        }
        private UI.uiAreaView m_AreaView;

        public UI.uiAreaMapControl AreaMap
        {
            get { return m_AreaMap; }
            private set { m_AreaMap = value; }
        }
        private UI.uiAreaMapControl m_AreaMap;

        /// <summary>
        /// Output Directory
        /// </summary>
        public String OutputDirectory
        {
            get { return m_sOutputDirectory; }
            protected set { m_sOutputDirectory = value; }
        }
        private String m_sOutputDirectory;

        /// <summary>
        /// Game Time configuration
        /// </summary>
        public String GameTime
        {
            get { return m_sGameTime; }
            protected set { m_sGameTime = value; }
        }
        private String m_sGameTime;

        /// <summary>
        /// StartWorkUnit configuration
        /// </summary>
        public Int32 StartWorkUnit
        {
            get { return m_sStartWorkUnit; }
            protected set { m_sStartWorkUnit = value; }
        }
        private Int32 m_sStartWorkUnit;

        /// <summary>
        /// EndWorkUnit configuration
        /// </summary>
        public Int32 EndWorkUnit
        {
            get { return m_sEndWorkUnit; }
            protected set { m_sEndWorkUnit = value; }
        }
        private Int32 m_sEndWorkUnit;    

        /// <summary>
        /// UseDate configuration
        /// </summary>
        public String UseDate
        {
            get { return m_sUseDate; }
            protected set { m_sUseDate = value; }
        }
        private String m_sUseDate;

        /// <summary>
        /// UseDate configuration
        /// </summary>
        public String OutputFolderName
        {
            get { return m_sOutputFolderName; }
            set { m_sOutputFolderName = value; }
        }
        private String m_sOutputFolderName;

        /// <summary>
        /// UpdateWebserver 
        /// </summary>
        public bool UpdateWebserver
        {
            get { return m_bUpdateWebserver; }
            set { m_bUpdateWebserver = value; }
        }
        private bool m_bUpdateWebserver;

        /// <summary>
        /// Sync 
        /// </summary>
        public bool Sync
        {
            get { return m_bSync; }
            set { m_bSync = value; }
        }
        private bool m_bSync;

        /// <summary>
        /// Sync Label
        /// </summary>
        public String SyncLabel
        {
            get { return m_sSyncLabel; }
            set { m_sSyncLabel = value; }
        }
        private String m_sSyncLabel;

        /// <summary>
        /// Game Level configuration
        /// </summary>
        public String GameLevel
        {
            get { return m_sGameLevel; }
            protected set { m_sGameLevel = value; }
        }
        private String m_sGameLevel;
        #endregion // Properties and Asscociated Member Data

        #region Member Data
        protected GameInterface.iGame m_Game = null;
        protected GameInterface.cConfiguration m_Configuration = null;
        #endregion // Member Data

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public cAreaToolBase()
        {
            this.m_eStatus = ToolInterface.etToolStatus.Idle;
        }
        #endregion // Constructor

        #region Controller Methods
        /// <summary>
        /// Initialise User Interface Components
        /// </summary>
        public virtual Control InitialiseUI(bool defaults)
        {
            // Display configuration dialog to user
            if (!defaults)
            {
                UI.uiAreaToolConfig areaCfgDlg = new UI.uiAreaToolConfig();
                areaCfgDlg.ShowDialog();
                this.GameTime = areaCfgDlg.GameTime.ToLower();                
                this.GameLevel = areaCfgDlg.GameLevel.ToLower();
                this.UseDate = areaCfgDlg.UseDate;
                this.OutputFolderName = areaCfgDlg.UseDate;
                this.StartWorkUnit = areaCfgDlg.StartWorkUnit;
                this.EndWorkUnit = areaCfgDlg.EndWorkUnit; 
            }
            else
            {
                // Otherwise initialise default configuration
                this.GameTime = "noon";
                this.GameLevel = "gta5"; // DW HACK for GTA5

                String sDate = String.Format("{0:yyyy}{0:MM}{0:dd}", DateTime.Now);
                this.UseDate = sDate;
                this.OutputFolderName = sDate;
                this.StartWorkUnit = 0;
                this.EndWorkUnit = 99999;
            }

            // Initialise Area View
            this.AreaView = new UI.uiAreaView();
            this.AreaMap = new UI.uiAreaMapControl();

            // Initialise Area Manager
            this.AreaManager = new cAreaManager();
            // This line is duplicated - surely a mistake? - DW
            this.AreaMap.AreaManager = this.AreaManager;
            this.AreaMap.AreaManager = this.AreaManager;

            this.AreaMap.AreaManager.StartWorkUnit = this.StartWorkUnit;
            this.AreaMap.AreaManager.EndWorkUnit = this.EndWorkUnit;
            
            this.AreaManager.WorkUnitsPopulatedEvent += this.AreaView.OnWorkUnitsPopulated;
            this.AreaManager.WorkUnitStatusChangeEvent += this.AreaView.OnWorkUnitChange;
            this.AreaManager.WorkUnitsPopulatedEvent += this.AreaMap.OnWorkUnitsPopulated;
            this.AreaManager.WorkUnitStatusChangeEvent += this.AreaMap.OnWorkUnitChange;
            this.AreaManager.Initialise(new cArea("Data\\AreaTools.xml"));

            // Setup our TabControl Container
            TabControl container = new TabControl();
            TabPage pageMain = new TabPage("Area List");
            TabPage pageMap = new TabPage("Area Map");
            pageMain.Controls.Add(this.AreaView);
            pageMap.Controls.Add(this.AreaMap);
            container.TabPages.Add(pageMain);
            container.TabPages.Add(pageMap);
            this.AreaView.Dock = DockStyle.Fill;
            this.AreaMap.Dock = DockStyle.Fill;

            return (container);
        }

        /// <summary>
        /// Machine Initialisation
        /// </summary>
        public virtual void Initialise(MachineInterface.iMachine machine)
        {
            Debug.Assert(GameTime.Length > 0, "Game Time not set.");

            String sDate = String.Format("{0:yyyy}{0:MM}{0:dd}", DateTime.Now);
            
            String sFilename = Path.Combine(this.OutputDirectory, machine.ArchName);
            sFilename = Path.Combine(sFilename, GameTime);
            sFilename = Path.Combine(sFilename, sDate);
            String sProgressFilename = Path.Combine(sFilename, machine.IP.ToString() + ".txt");

            if (this.AreaManager.FileMonitors.ContainsKey(machine.IP))
                this.AreaManager.FileMonitors.Remove(machine.IP);
            this.AreaManager.FileMonitors.Add(machine.IP, new AreaTools.cAreaProgressFileMonitor(sProgressFilename));
        }

        /// <summary>
        /// Area Tool Start Method (abstract)
        /// </summary>
        /// <param name="machine"></param>
        public abstract void Start(GameInterface.iGame game,
                                   GameInterface.cConfiguration config,
                                   MachineInterface.iMachine machine);

        /// <summary>
        /// Area Tool Finish Method (abstract)
        /// </summary>
        /// This is called by the base class' Monitor method.
        public abstract void Finish();

        /// <summary>
        /// Global tool monitor method
        /// </summary>
        public virtual void Monitor()
        {
            if (null == this.AreaManager)
                return;

            // Log warning if we are already finished.
            if (ToolInterface.etToolStatus.Finished == m_eStatus)
                Log.Log__Warning(String.Format("Tool {0} Monitor alive after tool has finished.", this.Name));

            if (this.AreaManager.HasCompleted())
            {
                Log.Log__Debug(String.Format("**** Area Manager Finshed ****"));
                this.m_eStatus = ToolInterface.etToolStatus.Finished;
                Finish();
                RaiseToolFinishedEvent();
            }
        }

        /// <summary>
        /// Machine Monitor Method
        /// </summary>
        public virtual void Monitor(MachineInterface.iMachine machine)
        {
            if ((null == this.AreaManager) || (!this.AreaManager.FileMonitors.ContainsKey(machine.IP)))
                return;

            cAreaProgressFileMonitor areaMonitor = this.AreaManager.FileMonitors[machine.IP];
            TimeSpan tsTimeout = new TimeSpan(0, TIMEOUT_MINS, 0);
            DateTime dtNow = DateTime.Now;
            DateTime dtTimeout = dtNow - tsTimeout;
            cWorkUnit wu = this.AreaManager.CurrentWorkUnit(machine);

            areaMonitor.Parse();

            // Set workunit timeout
            if (null != wu)
            {
                wu.TimeoutAt = (areaMonitor.ModifiedTime + tsTimeout) - dtNow;
            }

            // If the area has finished, simply start a new one.
            if (cAreaProgressFileMonitor.etState.Finished == areaMonitor.State)
            {
                // This small block should be moved into AreaManager
                if (null != wu)
                {
                    if (wu != null)
                        Log.Log__Debug(String.Format("**** Finished **** Machine {0} Workunit ID {1}", machine.IP, wu.ID));
                    wu.Finish();

                    // Ensures that if the game fails to start we don't assume that
                    // the area has been finished during the next tick.
                    areaMonitor.Flush( );
                }

                Log.Log__Debug(String.Format("**** Starting New WorkUnit **** Machine {0}", machine.IP));
                machine.Reboot();
                this.Start(m_Game, m_Configuration, machine);
            }
            // If the machine has timed out, resume from last sector
            else if (areaMonitor.ModifiedTime < dtTimeout)
            {
                // Log timeout and check for dead machines...
                if (null != wu)
                {
                    wu.IncTimeoutCount();
                    if (wu.TimeoutCount >= cAreaToolBaseSettings.Default.MaxTimeoutCount)
                    {
                        machine.Failures++;
                        if (machine.ArchName == "PS3")
                        {
                            String message = String.Format("Machine {0} has timed out {1} times).  The current section will be skipped as it may be crashing every time. This machine has had {2} Failures",
                                                          machine.IP, wu.TimeoutCount,machine.Failures);
                            RaiseToolAlertMessageEvent(message, null);
                            if (wu != null)
                                Log.Log__Debug(String.Format("**** Shafted **** Machine {0} Workunit ID {1}", machine.IP, wu.ID));
                            wu.Shaft();

                            if (machine.Failures >= cAreaToolBaseSettings.Default.UnacceptableFailures)
                            {
                                machine.Failed = true;
                                machine.Available = false;
                            }
                        }
                        else
                        {
                            String message = String.Format("Machine {0} appears to be unresponsive (timed out {1} times).  Power cycle and re-enabled manually.",
                                                           machine.IP, wu.TimeoutCount);
                            RaiseToolAlertMessageEvent(message, null);
                            machine.Available = false;
                            if (wu != null)
                                Log.Log__Debug(String.Format("**** Restarting **** Machine {0} Workunit ID {1}", machine.IP, wu.ID));
                            wu.Restart();
                        }
                    }
                }

                if (machine.Enabled && machine.Available && machine.Status != MachineInterface.etMachineStatus.Off)
                {
                    if (wu != null)
                        Log.Log__Debug(String.Format("**** Starting **** Machine {0} Workunit ID {1}", machine.IP, wu.ID));
                    machine.Reboot();
                    this.Start(m_Game, m_Configuration, machine);
                }
            }
            // Otherwise check machine state, reboot and resume if error
            else if (MachineInterface.etMachineStatus.Exception == machine.Status)
            {
                if (null != wu)
                {
                    wu.IncExceptionCount();

                    if (wu.ExceptionCount >= cAreaToolBaseSettings.Default.MaxExceptionCount)
                    {
                        if (wu != null)
                            Log.Log__Debug(String.Format("**** Shafted (exception) **** Machine {0} Workunit ID {1}", machine.IP, wu.ID));
                        wu.Shaft();
                    }
                    else
                    {
                        if (wu != null)
                            Log.Log__Debug(String.Format("**** Starting ( after exception ) **** Machine {0} Workunit ID {1}", machine.IP, wu.ID));
                        machine.Reboot();
                        this.Start(m_Game, m_Configuration, machine);
                    }
                }
            }
        }

        /// <summary>
        /// Sync to a P4 label.
        /// </summary>
        void SyncToLabel(String syncLabel)
        {
            // should probably specify the port etc. here also - DW
            ProcessStartInfo startInfo = new ProcessStartInfo();            
            startInfo.FileName = "p4.exe";
            String filespec = "//depot/...";
            startInfo.Arguments = String.Format(" sync {0}@{1},@{1}",filespec, syncLabel);
            startInfo.UseShellExecute = false;
            Process proc = Process.Start(startInfo);
            Log.Log__Debug(String.Format("**** Syncing to label ****"));
            proc.WaitForExit(1000 * 60 * 180); // at most wait for 1.5 hours.
            Log.Log__Debug(String.Format("**** Syncing to label completed ****"));
        }

        #endregion // Controller Methods

        #region Event Handlers
        public void PreEvent()
        {
            String sSubject = String.Format("Automated Stats Started: {0}", this.Name);
            String sBody = String.Format("{0} {1} statistics capture has started.",
                                         DateTime.Now, this.Name);

            if (Sync)
            {
                sBody += "\nSynced to Label " + SyncLabel + " (currently disabled)";
                SyncToLabel(SyncLabel);
            }

            SendEmailPublic(sSubject, sBody);
            SendEmail(sSubject, sBody);
        }

        public void PostEvent()
        {
            String sSubject = String.Format("Automated Stats Finished: {0}", this.Name);
            String sBody = String.Format("{0}\n\nNew statistics are available for {1}, they can be viewed at http://rsgedilin1:8080/GameMapStatsWeb-0.0.1-SNAPSHOT/GameMapStatsWeb.html.",
                                         DateTime.Now, this.Name);

            SendEmailPublic(sSubject, sBody);
            SendEmail(sSubject, sBody);
        }
        #endregion // Event Handlers

        #region Private Methods
        /// <summary>
        /// Send Email to Area Tools Mailing List
        /// </summary>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        void SendEmail(String subject, String body)
        {
            ToolInterface.cMailingListXml mailingList =
                new ToolInterface.cMailingListXml(cAreaToolBaseSettings.Default.MailingList);

            ToolInterface.cToolUtils.SendEmail(subject, body,
                cAreaToolBaseSettings.Default.EmailFromAddress, mailingList);
        }

        /// <summary>
        /// Send Email to Area Tools Mailing List
        /// </summary>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        void SendEmailPublic(String subject, String body)
        {
            ToolInterface.cMailingListXml mailingList =
                new ToolInterface.cMailingListXml(cAreaToolBaseSettings.Default.MailingListPublic);

            ToolInterface.cToolUtils.SendEmail(subject, body,
                cAreaToolBaseSettings.Default.EmailFromAddress, mailingList);
        }

        /// <summary>
        /// Raise ToolFinishedEvent to any event listeners
        /// </summary>
        private void RaiseToolFinishedEvent()
        {
            if (null != ToolFinishedEvent)
                ToolFinishedEvent(this, new ToolInterface.cToolEventArgs(this));
        }

        /// <summary>
        /// Raise ToolAlertMessageEvent to any event listeners
        /// </summary>
        /// <param name="message"></param>
        private void RaiseToolAlertMessageEvent(String message, String[] attachments)
        {
            // Send event email
            ToolInterface.cMailingListXml mailingList =
                new ToolInterface.cMailingListXml(cAreaToolBaseSettings.Default.MailingList);
            String sBody = String.Format("AreaTools Alert\n\n{0}.", message);
            ToolInterface.cToolUtils.SendEmail("AreaTools Alert", sBody,
                cAreaToolBaseSettings.Default.EmailFromAddress, mailingList,
                attachments);

            // Raise event
            if (null != ToolAlertMessageEvent)
                ToolAlertMessageEvent(this, new ToolInterface.cToolMessageEventArgs(message));
        }
        #endregion
    }

} // End of AreaTools namespace

// End of file
