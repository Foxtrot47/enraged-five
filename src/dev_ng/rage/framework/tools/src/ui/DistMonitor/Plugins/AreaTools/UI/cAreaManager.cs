//
// File: cAreaMonitor.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of cAreaMonitor.cs class
//

using System;
using System.Collections.Generic;
using System.Text;

using RsBase.Math;

namespace DistMonitor.AreaTools
{

    /// <summary>
    /// Area Manager
    /// </summary>
    public class cAreaManager
    {
        #region Events
        /// <summary>
        /// Event raised by AreaManager when a WorkUnit's Status changes
        /// </summary>
        public event EventHandler<cAreaManagerWorkUnitEventArgs> WorkUnitStatusChangeEvent;

        /// <summary>
        /// Event raised by AreaManager when the initial Work Units have been created
        /// </summary>
        public event EventHandler<EventArgs> WorkUnitsPopulatedEvent;
        #endregion // Events

        #region Properties and Associated Member Data
        /// <summary>
        /// Game Area we are managing
        /// </summary>
        public GameInterface.cArea Area
        {
            get { return m_Area; }
            private set { m_Area = value; }
        }
        private GameInterface.cArea m_Area;

        /// <summary>
        /// List of workunits covering Area
        /// </summary>
        public List<AreaTools.cWorkUnit> WorkUnits
        {
            get { return m_WorkUnits; }
            private set { m_WorkUnits = value; }
        }
        private List<AreaTools.cWorkUnit> m_WorkUnits;
        #endregion // Properties and Associated Member Data

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public cAreaManager()
        {
            Area = null;
            WorkUnits = new List<cWorkUnit>();
        }
        #endregion // Constructor

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="area"></param>
        public void Initialise(GameInterface.cArea area)
        {
            Area = area;
            WorkUnits = new List<cWorkUnit>();

            // Loop through Area constructing suitably sized work-units
            for (float fX = Area.Start.X; 
                 fX < Area.End.X; 
                 fX += (Area.WorkUnit.X*Area.SectorSize))
            {
                for (float fY = Area.Start.Y; 
                     fY < Area.End.Y; 
                     fY += (Area.WorkUnit.Y*Area.SectorSize))
                {
                    float fSize = ((Area.WorkUnit.Y * Area.SectorSize) - 1.0f);
                    cVector2<float> start = new cVector2<float>(fX, fY);
                    cVector2<float> end = new cVector2<float>(fX + fSize, fY + fSize);

                    cWorkUnit unit = new cWorkUnit(start, end);
                    WorkUnits.Add(unit);
                }
            }

            if (null != WorkUnitsPopulatedEvent)
                WorkUnitsPopulatedEvent(this, new EventArgs());
        }

        /// <summary>
        /// Retrieve next available work unit for a machine to process
        /// </summary>
        /// <returns>Next available work unit</returns>
        public cWorkUnit NextWorkUnit(MachineInterface.iMachine machine)
        {
            cWorkUnit wu = null;
            foreach (cWorkUnit w in WorkUnits)
            {
                if (cWorkUnit.etState.Queued == w.State)
                {
                    wu = w;
                    break;
                }
            }

            return (wu);
        }
        #endregion // Controller Methods
    }

    #region Event Argument Classes
    /// <summary>
    /// 
    /// </summary>
    public class cAreaManagerWorkUnitEventArgs : EventArgs
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// 
        /// </summary>
        public AreaTools.cWorkUnit WorkUnit
        {
            get { return m_WorkUnit; }
            private set { m_WorkUnit = value; }
        }
        private AreaTools.cWorkUnit m_WorkUnit;
        #endregion // Properties and Associated Member Data

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="workunit"></param>
        public cAreaManagerWorkUnitEventArgs(AreaTools.cWorkUnit workunit)
        {
            WorkUnit = workunit;
        }
        #endregion // Constructor
    };
    #endregion // Event Argument Classes

} // End of DistMonitor namespace

// End of file
