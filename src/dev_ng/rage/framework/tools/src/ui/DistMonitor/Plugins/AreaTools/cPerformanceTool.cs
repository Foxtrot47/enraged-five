//
// File: cPerformanceTool.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of cPerformanceTool.cs class
//

using System;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

using RSN.Base.Logging;

namespace AreaTools
{

    /// <summary>
    /// Performance Statistics Tool Implementation
    /// </summary>
    public class cPerformanceTool : cAreaToolBase, RSN.Base.Plugins.iPlugin
    {
        #region Constants
        private readonly String NAME = "Performance Statistics Capture";
        private readonly String DESC = "Rockstar Game Performance Statistics Capture Plugin";
        private readonly String DIRECTORY = "N:\\streamGTA\\stats\\perfstats";
        #endregion // Constants

        #region Static Member Data
        /// <summary>
        /// WorkUnit Request Code lock
        /// </summary>
        protected static Object ms_WorkUnitLock = new Object();
        #endregion // Static Member Data

        #region Properties and Associated Member Data
        public override String Name
        {
            get { return NAME; }
        }

        public virtual Version Version
        {
            get
            {
                Assembly assembly = Assembly.GetExecutingAssembly();
                return (assembly.GetName().Version);
            }
        }

        public virtual String Description
        {
            get { return DESC; }
        }
        #endregion // Properties and Asscociated Member Data

        #region Controller Methods
        /// <summary>
        /// Initialisation (once-off)
        /// </summary>
        /// <returns></returns>
        public override Control InitialiseUI(bool defaults)
        {
            OutputDirectory = DIRECTORY;

            return (base.InitialiseUI(defaults));
        }

        /// <summary>
        /// Performance Area Tool Start
        /// </summary>
        /// <param name="game">Game</param>
        /// <param name="config">Game configuration</param>
        /// <param name="machine">Machine</param>
        public override void Start(GameInterface.iGame game,
                                   GameInterface.cConfiguration config,
                                   MachineInterface.iMachine machine)
        {
            lock (ms_WorkUnitLock)
            {
                // So we recreate our FileMonitor to point to the current date's file, if
                // we are running overnight and pass a date boundary.
                this.Initialise(machine);

                m_Game = game;
                m_Configuration = config;

                // Fetch current or next work unit for machine.
                cWorkUnit wu = this.AreaManager.CurrentWorkUnit(machine);
                bool resume = (null != wu) ? true : false;

                if (null == wu)
                    wu = this.AreaManager.NextWorkUnit(machine);

                // Terminate as no outstanding workunits and machine is idle.
                if (null == wu)
                {
                    cLogger.Log__Message(String.Format("Machine {0} has no remaining work units, queue empty.", machine.IP));
                    return;
                }

                // Otherwise have work to do so lets start it...
                cAreaProgressFileMonitor areaMonitor = this.AreaManager.FileMonitors[machine.IP];
                String sArguments;
                if (resume && (null != areaMonitor.LastSector))
                    sArguments = String.Format("-fuckoff -sectortools -runperf -sx {0} -sy {1} -ex {2} -ey {3} -rx {4} -ry {5}",
                                               wu.StartWorld.X, wu.StartWorld.Y, wu.EndWorld.X, wu.EndWorld.Y,
                                               this.AreaManager.FileMonitors[machine.IP].LastSector.X,
                                               this.AreaManager.FileMonitors[machine.IP].LastSector.Y);
                else
                    sArguments = String.Format("-fuckoff -sectortools -runperf -sx {0} -sy {1} -ex {2} -ey {3}",
                                               wu.StartWorld.X, wu.StartWorld.Y, wu.EndWorld.X, wu.EndWorld.Y);

                if (this.GameTime == "midnight")
                    sArguments += " -midnight ";

                machine.Start(m_Game, m_Configuration, sArguments);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override void Finish()
        {
            cLogger.Log__Message("Inserting Performance Data into Database...");
            cDatabaseOutput.PerformanceDataInsert();
        }
        #endregion // Controller Methods
    }

} // End of AreaTools namespace

// End of file
