//
// File: Program.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of Program.cs class
//

using System;
using System.Threading;
using System.Windows.Forms;

namespace DistMonitor
{

    /// <summary>
    /// Static Program Class
    /// </summary>
    static class Program
    {
        #region Constants
        private static readonly String AUTHOR = "David Muir <david.muir@rockstarnorth.com>";
        private static readonly String EMAIL = "derek.ward@rockstarnorth.com";
        #endregion // Constants

        #region Static Member Data
        public static UI.uiMainForm MainForm;
        public static cController Controller;
        #endregion // Static Member Data

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// Entry-point additions include an exception try..catch block to 
        /// ensure any rogue exceptions are caught and initialising the
        /// main thread's name.
        [STAThread]
        static void Main()
        {
            try
            {
                Application.ThreadException += HandleError;
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                Controller = new cController();
                MainForm = new UI.uiMainForm();

                Application.Run(MainForm);
            }
            catch (Exception e)
            {
                RSG.Base.Forms.ExceptionStackTraceDlg stackDlg = new
                    RSG.Base.Forms.ExceptionStackTraceDlg(e, AUTHOR, EMAIL);
                stackDlg.ShowDialog();
            }
        }

        /// <summary>
        /// Thread Exception Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static void HandleError(Object sender, ThreadExceptionEventArgs ex)
        {
            RSG.Base.Logging.Log.Log__Error(String.Format("Unhandled exception: {0}.", 
                                              ex.Exception.Message));
        }
    }

} // End of DistMonitor namespace

// End of file
