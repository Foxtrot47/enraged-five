//
// File: cToolUtils.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of cToolUtils.cs class
//

using System;
using System.Net.Mail;

namespace ToolInterface
{

    /// <summary>
    /// Common Tool Utilities Class
    /// </summary>
    public class cToolUtils
    {
        #region Controller Methods
        /// <summary>
        /// Send Email to specified mailing list
        /// </summary>
        /// <param name="subject">Email subject string</param>
        /// <param name="body">Email body string</param>
        /// <param name="from">Email from address</param>
        /// <param name="mailingList">Mailing list recipients</param>
        public static void SendEmail(String subject, String body, String from, cMailingListXml mailingList)
        {
            cToolUtils.SendEmail(subject, body, from, mailingList, null);
        }

        /// <summary>
        /// Send Email to specified mailing list with attachment(s)
        /// </summary>
        /// <param name="subject">Email subject string</param>
        /// <param name="body">Email body string</param>
        /// <param name="from">Email from address</param>
        /// <param name="mailingList">Mailing list recipients</param>
        /// <param name="attachments">Filename attachments</param>
        public static void SendEmail(String subject, String body, String from, 
                                     cMailingListXml mailingList, String[] attachments)
        {
            MailMessage mail = new MailMessage();

            // Add To recipients from mailing list XML
            foreach (MailAddress address in mailingList.Recipients)
                mail.To.Add(address);

            mail.From = new MailAddress(from);
            mail.Subject = subject;
            mail.Body = body;
            if (null != attachments)
            {
                foreach (String attachment in attachments)
                    mail.Attachments.Add(new Attachment(attachment));
            }

            SmtpClient smtpClient = new SmtpClient("RSGEDIEXG1");
            smtpClient.Send(mail);
        }
        #endregion // Controller Methods
    }

} // End of ToolInterface namespace

// End of file
