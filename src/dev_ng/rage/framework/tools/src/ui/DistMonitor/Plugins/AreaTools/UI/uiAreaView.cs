//
// File: uiAreaView.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of uiAreaView.cs class
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace AreaTools.UI
{

    /// <summary>
    /// 
    /// </summary>
    public partial class uiAreaView : UserControl
    {
        #region Delegates
        /// <summary>
        /// Work Units Change Event Delegate (thread-safe UI access)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private delegate void OnWorkUnitChangeDelegate(Object sender, AreaTools.cAreaManagerWorkUnitEventArgs e);
        
        /// <summary>
        /// Work Units Populated Event Delegate (thread-safe UI access)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private delegate void OnWorkUnitsPopulatedDelegate(Object sender, EventArgs e);
        #endregion // Delegates

        #region Properties and Associated Member Data
        /// <summary>
        /// Associated Area Manager
        /// </summary>
        public AreaTools.cAreaManager AreaManager
        {
            get { return m_AreaManager; }
            set { m_AreaManager = value; }
        }
        private AreaTools.cAreaManager m_AreaManager;
        #endregion // Properties and Associated Member Data

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public uiAreaView()
        {
            InitializeComponent();
        }
        #endregion // Constructor

        #region Area Manager Event Handlers
        /// <summary>
        /// Event triggered (by cAreaManager) when a Work Unit is updated
        /// </summary>
        /// <param name="sender">Event source object</param>
        /// <param name="e">Event arguments object (specifying changed work unit)</param>
        public void OnWorkUnitChange(Object sender, AreaTools.cAreaManagerWorkUnitEventArgs e)
        {
            if (lvAreaWorkUnits.InvokeRequired)
            {
                lvAreaWorkUnits.Invoke(new OnWorkUnitChangeDelegate(OnWorkUnitChange), new Object[] { sender, e });
            }
            else
            {
                lvAreaWorkUnits.BeginUpdate();
                foreach (ListViewItem item in lvAreaWorkUnits.Items)
                {
                    uiWorkUnitListViewItem wuItem = (item as uiWorkUnitListViewItem);
                    if (wuItem.WorkUnit == e.WorkUnit)
                        wuItem.UpdateView(lvAreaWorkUnits.Groups);
                }
                lvAreaWorkUnits.EndUpdate();

                // Update progress bar
                int val = 0;
                foreach (ListViewItem item in lvAreaWorkUnits.Items)
                {
                    uiWorkUnitListViewItem wuItem = (item as uiWorkUnitListViewItem);
                    if (wuItem.WorkUnit.State == cWorkUnit.etState.Completed)
                        ++val;
                }
                pbProgress.Value = val;
            }
        }

        /// <summary>
        /// Event triggered (by cAreaManager) when Work Units are initially populated
        /// </summary>
        /// <param name="sender">Event source object</param>
        /// <param name="e">Event arguments object</param>
        public void OnWorkUnitsPopulated(Object sender, EventArgs e)
        {
            if (lvAreaWorkUnits.InvokeRequired)
            {
                lvAreaWorkUnits.Invoke(new OnWorkUnitsPopulatedDelegate(OnWorkUnitsPopulated), new Object[] { sender, e });
            }
            else
            {
                AreaTools.cAreaManager manager = (sender as AreaTools.cAreaManager);
                lvAreaWorkUnits.Items.Clear();
                lvAreaWorkUnits.BeginUpdate();
                foreach (AreaTools.cWorkUnit wu in manager.WorkUnits)
                {
                    uiWorkUnitListViewItem item = new uiWorkUnitListViewItem(wu);
                    lvAreaWorkUnits.Items.Add(item);
                    item.UpdateView(lvAreaWorkUnits.Groups);
                }
                lvAreaWorkUnits.EndUpdate();

                pbProgress.Maximum = manager.WorkUnits.Count;
                pbProgress.Step = 1;
            }
        }
        #endregion

        #region UI Event Handlers
        /// <summary>
        /// Context Menu Opening Event
        /// </summary>
        /// <param name="sender">Event source object</param>
        /// <param name="e">Event arguments</param>
        private void menuCtxStrip_Opening(object sender, CancelEventArgs e)
        {
            bool bAllStartedOrFinished = true;
            bool bAllQueuedOrStarted = true;
            foreach (ListViewItem item in lvAreaWorkUnits.SelectedItems)
            {
                uiWorkUnitListViewItem workItem = (item as uiWorkUnitListViewItem);
                if (cWorkUnit.etState.Completed == workItem.WorkUnit.State || cWorkUnit.etState.Shafted == workItem.WorkUnit.State)
                    bAllQueuedOrStarted = false;
                else if (cWorkUnit.etState.Queued == workItem.WorkUnit.State)
                    bAllStartedOrFinished = false;
            }

            miShowGroups.Checked = lvAreaWorkUnits.ShowGroups;
            miArea.Enabled = (lvAreaWorkUnits.SelectedItems.Count > 0);
            miAreaMarkAsQueued.Enabled = bAllStartedOrFinished && !bAllQueuedOrStarted;
            miAreaMarkAsFinished.Enabled = bAllQueuedOrStarted && !bAllStartedOrFinished;
        }

        /// <summary>
        /// Context Menu Show Groups Toggle Menu Item Click
        /// </summary>
        /// <param name="sender">Event source object</param>
        /// <param name="e">Event arguments</param>
        private void miShowGroups_Click(object sender, EventArgs e)
        {
            lvAreaWorkUnits.ShowGroups = !lvAreaWorkUnits.ShowGroups;
        }

        /// <summary>
        /// Context Menu Refresh Menu Item Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void miRefresh_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lvAreaWorkUnits.Items)
            {
                uiWorkUnitListViewItem wuItem = (item as uiWorkUnitListViewItem);
                wuItem.UpdateView(lvAreaWorkUnits.Groups);
            }
        }

        /// <summary>
        /// Context Menu Area Mark As Queued Menu Item Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void miAreaMarkAsQueued_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lvAreaWorkUnits.SelectedItems)
            {
                uiWorkUnitListViewItem wuItem = (item as uiWorkUnitListViewItem);
                wuItem.WorkUnit.OverrideState(cWorkUnit.etState.Queued);
            }
        }

        /// <summary>
        /// Context Menu Area Mark As Finished Menu Item Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void miAreaMarkAsFinished_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lvAreaWorkUnits.SelectedItems)
            {
                uiWorkUnitListViewItem wuItem = (item as uiWorkUnitListViewItem);
                wuItem.WorkUnit.OverrideState(cWorkUnit.etState.Completed);
            }
        }

        /// <summary>
        /// Context Menu Area Mark As Shafted Menu Item Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void miAreaMarkAsShafted_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lvAreaWorkUnits.SelectedItems)
            {
                uiWorkUnitListViewItem wuItem = (item as uiWorkUnitListViewItem);
                wuItem.WorkUnit.OverrideState(cWorkUnit.etState.Shafted);
            }
        }
        #endregion // UI Event Handlers

    }

    #region Work Unit ListViewItem Class
    /// <summary>
    /// 
    /// </summary>
    class uiWorkUnitListViewItem : ListViewItem
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Associated Work Unit
        /// </summary>
        public AreaTools.cWorkUnit WorkUnit
        {
            get { return m_WorkUnit; }
            private set { m_WorkUnit = value; }
        }
        private AreaTools.cWorkUnit m_WorkUnit;
        #endregion // Properties and Associated Member Data

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="wu">Associated Work Unit</param>
        public uiWorkUnitListViewItem(AreaTools.cWorkUnit wu)
        {
            WorkUnit = wu;
            this.SubItems.Add(new ListViewSubItem());
            this.SubItems.Add(new ListViewSubItem());
            this.SubItems.Add(new ListViewSubItem());
            this.SubItems.Add(new ListViewSubItem());
            this.SubItems.Add(new ListViewSubItem());
            this.SubItems.Add(new ListViewSubItem());
            this.SubItems.Add(new ListViewSubItem());
            this.SubItems.Add(new ListViewSubItem());
            this.SubItems.Add(new ListViewSubItem());
            this.SubItems.Add(new ListViewSubItem());
        }
        #endregion // Constructor

        #region Controller Methods
        /// <summary>
        /// Refresh view
        /// </summary>
        public void UpdateView(ListViewGroupCollection groups)
        {
            this.Group = groups[(int)WorkUnit.State];
            this.SubItems[0].Text = WorkUnit.ID.ToString();
            this.SubItems[1].Text = WorkUnit.StartWorld.ToString();
            this.SubItems[2].Text = WorkUnit.EndWorld.ToString();
            this.SubItems[3].Text = WorkUnit.State.ToString();
            this.SubItems[4].Text = (null != WorkUnit.AssignedMachine) ? WorkUnit.AssignedMachine.IP.ToString() : "N/A";
            this.SubItems[5].Text = (cWorkUnit.etState.Queued != WorkUnit.State) ? WorkUnit.StartTime.ToString() : "";
            this.SubItems[6].Text = (cWorkUnit.etState.Completed == WorkUnit.State) ? WorkUnit.EndTime.ToString() : "";
            this.SubItems[7].Text = WorkUnit.TimeoutAt.ToString();
            this.SubItems[8].Text = WorkUnit.TimeoutCount.ToString();
            this.SubItems[9].Text = WorkUnit.ExceptionCount.ToString();
        }
        #endregion // Controller Methods
    };
    #endregion Work Unit ListViewItem Class

} // End of DistMonitor.UI namespace

// End of file
