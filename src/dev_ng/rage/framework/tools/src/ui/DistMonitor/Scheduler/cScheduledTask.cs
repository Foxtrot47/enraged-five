//
// File: cScheduledTask.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of cScheduledTask.cs class
//

using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

using GameInterface;
using ToolInterface;

using RSG.Base.Interfaces;

namespace DistMonitor.Scheduler
{

    /// <summary>
    /// Scheduled Task Class
    /// </summary>
    /// A scheduled task object binds a Game, Configuration and Tool object
    /// together with a schedule which describes when the task should be 
    /// executed.
    internal class cScheduledTask : iXmlSerialize, iXmlDeserialize
    {
        #region Events
        /// <summary>
        /// Event raised whenever any property of the task changes
        /// </summary>
        public event EventHandler<cScheduledTaskEventArgs> TaskChangedEvent;
        #endregion // Events

        #region Properties and Associated Member Data
        /// <summary>
        /// 
        /// </summary>
        public String Name
        {
            get { return m_sName; }
        }
        private String m_sName;

        /// <summary>
        /// Game to run for scheduled task
        /// </summary>
        public String Game
        {
            get { return m_sGame; }
        }
        private String m_sGame;

        /// <summary>
        /// Configuration of game to use for scheduled task
        /// </summary>
        public String Configuration
        {
            get { return m_sConfiguration; }
        }
        private String m_sConfiguration;

        /// <summary>
        /// Tool to run for scheduled task
        /// </summary>
        public String Tool
        {
            get { return m_sTool; }
        }
        private String m_sTool;

        /// <summary>
        /// Schedule describing when task should run
        /// </summary>
        public cSchedule Schedule
        {
            get { return m_Schedule; }
        }
        private cSchedule m_Schedule;
        #endregion // Properties and Associated Member Data

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public cScheduledTask()
        {
            DateTime end = DateTime.Now.AddYears(100); // DW - not max since the schedule can move on days 
            m_Schedule = new cSchedule(cSchedule.etType.Daily, DateTime.Now,
                default(DateTime), end);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name"></param>
        /// <param name="game"></param>
        /// <param name="config"></param>
        /// <param name="tool"></param>
        /// <param name="schedule"></param>
        public cScheduledTask(String name, String game, String config, String tool, cSchedule schedule)
        {
            m_sName = name;
            m_sGame = game;
            m_sConfiguration = config;
            m_sTool = tool;
            m_Schedule = schedule;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name"></param>
        /// <param name="game"></param>
        /// <param name="config"></param>
        /// <param name="tool"></param>
        /// <param name="schedule"></param>
        public cScheduledTask(String name, iGame game, cConfiguration config, iTool tool, cSchedule schedule)
        {
            m_sName = name;
            m_sGame = game.Name;
            m_sConfiguration = config.Name;
            m_sTool = tool.Name;
            m_Schedule = schedule;
        }
        #endregion // Constructor

        #region Controller Methods
        /// <summary>
        /// Initialise
        /// </summary>
        /// <param name="name"></param>
        /// <param name="game"></param>
        /// <param name="config"></param>
        /// <param name="tool"></param>
        /// <param name="schedule"></param>
        public void Initialise(String name, String game, String config, String tool, cSchedule schedule)
        {
            m_sName = name;
            m_sGame = game;
            m_sConfiguration = config;
            m_sTool = tool;
            m_Schedule = schedule;

            RaiseTaskChangedEvent();
        }

        /// <summary>
        /// Serialize Object to XML
        /// </summary>
        /// <param name="xmlWriter"></param>
        public void SerializeXml(XmlWriter xmlWriter)
        {
            if (null == xmlWriter)
                throw new ArgumentNullException("Passed XmlWriter is null.");

            xmlWriter.WriteStartElement("ScheduledTask");
            xmlWriter.WriteAttributeString("Name", this.Name);
            xmlWriter.WriteAttributeString("Game", this.Game);
            xmlWriter.WriteAttributeString("Configuration", this.Configuration);
            xmlWriter.WriteAttributeString("Tool", this.Tool);
            xmlWriter.WriteAttributeString("Schedule", this.Schedule.ToString());
            xmlWriter.WriteAttributeString("LastTime", this.Schedule.LastTime.Ticks.ToString());
            xmlWriter.WriteAttributeString("NextTime", this.Schedule.NextTime.Ticks.ToString());
            xmlWriter.WriteAttributeString("EndTime", this.Schedule.EndTime.Ticks.ToString());
            xmlWriter.WriteEndElement();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlReader"></param>
        public void DeserializeXml(XmlReader xmlReader)
        {
            if (null == xmlReader)
                throw new ArgumentNullException("Passed XmlReader is null.");

            xmlReader.Read();
            // Parse new Scheduled Task
            if ("ScheduledTask" == xmlReader.Name)
            {
                String name = String.Empty;
                String game = String.Empty;
                String config = String.Empty;
                String tool = String.Empty;
                String sched = String.Empty;
                cSchedule schedule;
                DateTime start = default(DateTime);
                DateTime last = default(DateTime);
                DateTime end = DateTime.Now.AddYears(100); // DW - not max since the schedule can move on days 

                for (int nAttr = 0; nAttr < xmlReader.AttributeCount; ++nAttr)
                {
                    xmlReader.MoveToAttribute(nAttr);
                    if ("Name" == xmlReader.Name) name = xmlReader.Value;
                    if ("Game" == xmlReader.Name) game = xmlReader.Value;
                    if ("Configuration" == xmlReader.Name) config = xmlReader.Value;
                    if ("Tool" == xmlReader.Name) tool = xmlReader.Value;
                    if ("Schedule" == xmlReader.Name) sched = xmlReader.Value;
                    if ("NextTime" == xmlReader.Name) start = new DateTime(long.Parse(xmlReader.Value));
                    if ("LastTime" == xmlReader.Name) last = new DateTime(long.Parse(xmlReader.Value));
                    if ("EndTime" == xmlReader.Name) end = new DateTime(long.Parse(xmlReader.Value));
                }

                if ("Daily" == sched)
                    schedule = new cSchedule(cSchedule.etType.Daily, start, last, end);
                else if ("BiDaily" == sched)
                    schedule = new cSchedule(cSchedule.etType.BiDaily, start, last, end);
                else if ("Hourly" == sched)
                    schedule = new cSchedule(cSchedule.etType.Hourly, start, last, end);
                else if ("Minutes5" == sched)
                    schedule = new cSchedule(cSchedule.etType.Minutes5, start, last, end);
                else if ("Once" == sched)
                    schedule = new cSchedule(cSchedule.etType.Once, start, last, end);
                else
                    schedule = new cSchedule(cSchedule.etType.Weekly, start, last, end);

                this.Initialise(name, game, config, tool, schedule);
            }
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Raise Task Changed Event
        /// </summary>
        private void RaiseTaskChangedEvent()
        {
            if (null != TaskChangedEvent)
                TaskChangedEvent(this, new cScheduledTaskEventArgs(this));
        }
        #endregion // Private Methods
    }

    #region Event Argument Classes
    /// <summary>
    /// Scheduled Task Event Arguments Class
    /// </summary>
    internal sealed class cScheduledTaskEventArgs : EventArgs
    {
        #region Properties and Associated Member Data
        public cScheduledTask Task
        {
            get { return m_Task; }
            private set { m_Task = value; }
        }
        private cScheduledTask m_Task;
        #endregion // Properties and Associated Member Data

        #region Constructor
        public cScheduledTaskEventArgs(cScheduledTask task)
        {
            Task = task;
        }
        #endregion // Constructor
    }
    #endregion // Event Argument Classes

} // End of DistMonitor.Scheduler namespace

// End of file
