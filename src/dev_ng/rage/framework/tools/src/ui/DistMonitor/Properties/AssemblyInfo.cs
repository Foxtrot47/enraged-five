﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Distribution Monitor")]
[assembly: AssemblyDescription("Distributes tasks between a configurable set of consoles (XBox 360 and PS3).\r\n\r\nDavid Muir <david.muir@rockstarnorth.com>\r\nLuke Openshaw <luke.openshaw@rockstarnorth.com>")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Rockstar North")]
[assembly: AssemblyProduct("Distribution Monitor")]
[assembly: AssemblyCopyright("Copyright © Rockstar North 2007-2009")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("002db17d-4e8e-460c-9e00-5362c3a01f72")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("1.4.0.0")]
[assembly: AssemblyFileVersion("1.4.0.0")]
