//
// File: cXbWatson.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of cXbWatson.cs class
//
// References: Microsoft XDK Sample xbWatson2005
//

using System;
using XDevkit;

using RSG.Base.Logging;

namespace XBox360
{
    
    /// <summary>
    /// XBox 360 Debugging Log (see XDK xbWatson sample)
    /// </summary>
    public class cXbDebugLog : MachineInterface.iDebugLog
    {
        #region Events
        /// <summary>
        /// Event raised when the console echoes a debug message
        /// </summary>
        public event EventHandler<MachineInterface.cDebugLogTextEventArgs> DebugTextEvent;

        /// <summary>
        /// Event raised when the console's status changes
        /// </summary>
        public event EventHandler<MachineInterface.cMachineStatusEventArgs> MachineStatusChangeEvent;
        #endregion // Events

        #region Properties and Associated Member Data
        /// <summary>
        /// Machine we are a debug log of.
        /// </summary>
        public MachineInterface.iMachine Machine
        {
            get { return m_Machine; }
        }
        private MachineInterface.iMachine m_Machine;
        
        /// <summary>
        /// Connection status
        /// </summary>
        public bool Connected
        {
            get { return m_bConnected; }
        }
        private bool m_bConnected;

        public bool Enabled
        {
            get { return m_bEnabled; }
            set { m_bEnabled = value; }
        }
        private bool m_bEnabled;

        #endregion // Properties and Associated Member Data

        #region Member Data
        private IXboxManager m_xbManager;
        private XboxConsole m_xbConsole;
        private IXboxDebugTarget m_xbDebugTarget;
        #endregion

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public cXbDebugLog()
        {
            m_Machine = null;
            m_bConnected = false;
            m_bEnabled = true;
           // m_xbManager = new XboxManagerClass();
        }

        /// <summary>
        /// 
        /// </summary>
        public cXbDebugLog(cXbox360 machine)
        {
            m_Machine = machine;
            m_bConnected = false;
          //  m_xbManager = new XboxManagerClass();
            ConnectTo(machine);
        }
        #endregion // Constructors

        #region Controller Methods
        /// <summary>
        /// Connect debug log to a machine
        /// </summary>
        /// <param name="machine"></param>
        public void ConnectTo(MachineInterface.iMachine machine)
        {
            m_Machine = machine;
            m_xbConsole = m_xbManager.OpenConsole(machine.HostName);
            InitDM();
        }

        /// <summary>
        /// Disconnect debug log
        /// </summary>
        public void Disconnect()
        {
            if (Connected)
            {
                m_xbDebugTarget.DisconnectAsDebugger();
                m_bConnected = false;
                RaiseMachineStatusEvent(MachineInterface.etMachineStatus.Unknown);
            }
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Request notifications from the Xbox devkit
        /// </summary>
        private void InitDM()
        {
            // Get the Xbox we are talking to
            try
            {
                // Hook up Notification Channel - tell the Xbox devkit to send
                // notifications to us. The set of notifications is documented under
                // DmNotify in the help file.
                m_xbConsole.OnStdNotify += new XboxEvents_OnStdNotifyEventHandler(xboxConsole_OnStdNotify);
                m_xbDebugTarget = m_xbConsole.DebugTarget;
                m_xbDebugTarget.ConnectAsDebugger(null, XboxDebugConnectFlags.Force);

                RaiseMachineStatusEvent(MachineInterface.etMachineStatus.On);
            }
            catch (Exception ex)
            {
                // Display an error message if connecting fails. Translating
                // error codes to text is not currently supported.
                m_xbConsole.OnStdNotify -= new XboxEvents_OnStdNotifyEventHandler(xboxConsole_OnStdNotify);
                Log.Log__Error(String.Format("Connection to XBox {0} failed: {1}.", 
                                   Machine.IP, ex.Message));

                RaiseMachineStatusEvent(MachineInterface.etMachineStatus.Off);
            }
        }

        /// <summary>
        /// Callback for DmNotify style notifications
        /// </summary>
        /// <param name="eventCode"></param>
        /// <param name="eventInformation"></param>
        private void xboxConsole_OnStdNotify(XboxDebugEventType eventCode, IXboxEventInfo eventInformation)
        {
            bool bStopped = eventInformation.Info.IsThreadStopped == 0 ? false : true;
            bool NotStopped, Exception = true;
            switch (eventCode)
            {
                // Handler for DM_EXEC changes, mainly for detecting reboots
                case XDevkit.XboxDebugEventType.ExecStateChange:

                    if ((eventInformation.Info.ExecState != XDevkit.XboxExecutionState.Rebooting) &&
                        (null != m_xbDebugTarget))
                        m_xbDebugTarget.StopOn(XboxStopOnFlags.OnStackTrace, true);
                    
                    if (!Connected)
                    {
                        m_bConnected = true;
                        Log.Log__Message(String.Format("Connection to {0} {1} success: logging debug messages.",
                                             Machine.Name, Machine.IP));
                        
                        RaiseMachineStatusEvent(MachineInterface.etMachineStatus.On);
                    }
                    else if (eventInformation.Info.ExecState == XDevkit.XboxExecutionState.Rebooting)
                    {
                        Log.Log__Message(String.Format("{0} {1} rebooting.",
                                             Machine.Name, Machine.IP));

                        RaiseMachineStatusEvent(MachineInterface.etMachineStatus.Rebooting);
                    }
                    else if (eventInformation.Info.ExecState == XDevkit.XboxExecutionState.RebootingTitle)
                    {
                        Log.Log__Message(String.Format("{0} {1} rebooting title.",
                                             Machine.Name, Machine.IP));

                        RaiseMachineStatusEvent(MachineInterface.etMachineStatus.On);
                    }
                    else if (eventInformation.Info.ExecState == XDevkit.XboxExecutionState.Running)
                    {
                        Log.Log__Message(String.Format("{0} {1} running.",
                                             Machine.Name, Machine.IP));

                        RaiseMachineStatusEvent(MachineInterface.etMachineStatus.On);
                    }
                    break;

                // Handler for DM_DEBUGSTR notifications, to display debug
                // print text.
                case XDevkit.XboxDebugEventType.DebugString:

                    RaiseDebugTextEvent(eventInformation.Info.Message);

                    if (bStopped)
                    {
                        eventInformation.Info.Thread.Continue(Exception);
                        m_xbDebugTarget.Go(out NotStopped);
                    }
                    break;

                // Handler for asserts triggered on the Xbox
                case XDevkit.XboxDebugEventType.AssertionFailed:
                    {
                        String sMessage = String.Format("Assertion failed: {0}.", eventInformation.Info.Message);
                        RaiseDebugTextEvent(sMessage);
                    }

                    if (bStopped)
                    {
                        RaiseMachineStatusEvent(MachineInterface.etMachineStatus.Exception);
                    }

                    break;

                // Handler for fatal errors - RIPs in the Xbox OS
                case XDevkit.XboxDebugEventType.RIP:
                    {
                        String sMessage = String.Format("{0} {1} RIP: {2}.", Machine.Name,
                                                        Machine.IP, eventInformation.Info.Message);
                        RaiseDebugTextEvent(sMessage);
                    }

                    if (bStopped)
                    {
                        RaiseMachineStatusEvent(MachineInterface.etMachineStatus.Exception);
                    }
                    break;

                // Handler for a breakpoint - DM_BREAK
                case XDevkit.XboxDebugEventType.ExecutionBreak:
                    {
                        String sMessage = String.Format("{0} {1} Break: {2}.", Machine.Name,
                                                        Machine.IP, eventInformation.Info.Message);
                        RaiseDebugTextEvent(sMessage);
                    }

                    RaiseMachineStatusEvent(MachineInterface.etMachineStatus.Exception);
                    break;

                // Handler for a data breakpoint - DM_DATABREAK
                case XDevkit.XboxDebugEventType.DataBreak:
                    {
                        String sMessage = String.Format("{0} {1} Data Break: {2} {3} {4} {5} {6}.",
                                                    Machine.Name, Machine.IP,
                                                    eventInformation.Info.Address,
                                                    eventInformation.Info.Thread.ThreadId,
                                                    eventInformation.Info.GetType(),
                                                    eventInformation.Info.Address,
                                                    eventInformation.Info.Message);
                        RaiseDebugTextEvent(sMessage);
                    }

                    RaiseMachineStatusEvent(MachineInterface.etMachineStatus.Exception);
                    break;

                // Handler for an exception (access violation, etc.) - DM_EXCEPTION
                case XDevkit.XboxDebugEventType.Exception:

                    if (eventInformation.Info.Flags == XDevkit.XboxExceptionFlags.FirstChance)
                    {
                        if (eventInformation.Info.Code != 0x406D1388) // Ignore the setthreadname exception
                        {
                            String sMessage = String.Format("{0} {1} First Chance Exception: {2} {3} {4} {5}.",
                                                            Machine.Name, Machine.IP,
                                                            eventInformation.Info.Thread.ThreadId,
                                                            eventInformation.Info.Code,
                                                            eventInformation.Info.Address,
                                                            eventInformation.Info.Flags);
                            RaiseDebugTextEvent(sMessage);
                        }
                    }
                    else
                    {
                        String sMessage = String.Format("{0} {1} Exception: {2} {3} {4} {5}.",
                                                        Machine.Name, Machine.IP,
                                                        eventInformation.Info.Thread.ThreadId,
                                                        eventInformation.Info.Code,
                                                        eventInformation.Info.Address,
                                                        eventInformation.Info.Flags);
                        RaiseDebugTextEvent(sMessage);
                    }
 
                    if (bStopped)
                    {
                        RaiseMachineStatusEvent(MachineInterface.etMachineStatus.Exception);
                    }
                    break;

                // Module (Game Title) Loaded
                case XDevkit.XboxDebugEventType.ModuleLoad:
                    {
                        String sExecutable = eventInformation.Info.Module.Executable.GetPEModuleName();
                        if ( 0 != String.Compare("XMsgr.dll", sExecutable, true) &&
                             0 != String.Compare("XApi.dll", sExecutable, true) &&
                             0 != String.Compare("XShell.exe", sExecutable, true))
                        {
                            RaiseMachineStatusEvent(MachineInterface.etMachineStatus.Running);
                        }
                    }
                    break;

                // Module (Game Title) Unloaded
                case XDevkit.XboxDebugEventType.ModuleUnload:

                    RaiseMachineStatusEvent(MachineInterface.etMachineStatus.On);
                    break;

                // Handle all those notification types that don't take special handling.
                default:
                    if ((null != eventInformation.Info.Message) && (eventInformation.Info.Message.Length > 0))
                        RaiseDebugTextEvent(eventInformation.Info.Message);
                    break;
            }
        }

        /// <summary>
        /// Raise Debug Text Event (if we have listeners)
        /// </summary>
        /// <param name="sMessage"></param>
        private void RaiseDebugTextEvent(String message)
        {
            if (null != DebugTextEvent)
                DebugTextEvent(this, new MachineInterface.cDebugLogTextEventArgs(Machine, message));
        }

        /// <summary>
        /// Raise Machine Status Event (if we have listeners)
        /// </summary>
        /// <param name="status">New Machine status enumeration</param>
        private void RaiseMachineStatusEvent(MachineInterface.etMachineStatus status)
        {
            if (null != MachineStatusChangeEvent)
                MachineStatusChangeEvent(this, new MachineInterface.cMachineStatusEventArgs(Machine, status));
        }
        #endregion // Private Methods
    }

} // End of XBox360 namespace

// End of file
