//
// File: cPS3DebugLog.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of cPS3DebugLog.cs class
//

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

//using ps3tmapiInterface;


namespace PS3
{

    /// <summary>
    /// PS3 Debug Log
    /// </summary>
    public class cPS3DebugLog : MachineInterface.iDebugLog
    {
        #region Events
        public event EventHandler<MachineInterface.cDebugLogTextEventArgs> DebugTextEvent;
        public event EventHandler<MachineInterface.cMachineStatusEventArgs> MachineStatusChangeEvent;
        #endregion // Events

        #region Properties and Associated Member Data
        public MachineInterface.iMachine Machine
        {
            get { return m_Machine; }
        }
        private MachineInterface.iMachine m_Machine;

        /// <summary>
        /// 
        /// </summary>
        public bool Connected
        {
            get { return m_bConnected; }
        }
        private bool m_bConnected;

        /// <summary>
        /// 
        /// </summary>
        public bool Enabled
        {
            get { return m_bEnabled; }
            set { m_bEnabled = value; }
        }
        private bool m_bEnabled;
        #endregion // Properties and Associated Member Data


        #region Private Locking Data
        /// <summary>
        /// Hopefully this sorts the problem of trying to connect to multiple PS3's
        /// </summary>
        private static object DebugStartupLock = new Object();
        #endregion


        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public cPS3DebugLog()
        {
            m_Machine = null;
            m_bConnected = false;
            m_bEnabled = false;
        }

        #region Member Data
        private Thread m_readThread;
        //private PS3Target m_target;
        private bool m_bForceDisconnect;
        #endregion

        /// <summary>
        /// 
        /// </summary>
        public cPS3DebugLog(cPS3 machine)
        {
            m_Machine = machine;
            m_bConnected = false;
            m_bForceDisconnect = false;
        }
        #endregion // Constructors

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="machine"></param>
        public void ConnectTo(MachineInterface.iMachine machine)
        {
           lock (DebugStartupLock)
           {
               if (m_bEnabled)
               {
/*                   m_target = new PS3Target();

                   //DebugTextEvent += new DebugEventEventHandler(target_DebugEvent);

                   m_target.ConnectError += new MessageEventHandler(target_ConnectError);
                   m_target.ConnectInfo += new MessageEventHandler(target_ConnectInfo);
                   m_target.ConnectStatus += new ConnectStatusEventHandler(target_ConnectStatus);
                   m_target.DebugEvent += new DebugEventEventHandler(target_DebugEvent);
                   m_target.OutputTTY = true;
                   m_target.TTY += new MessageEventHandler(target_TTYMessage);
                   m_target.TargetEvent += new TargetEventEventHandler(target_TargetEvent);
                   m_target.UnhandledCrash += new MessageEventHandler(target_UnhandledCrash);
                   m_target.UnitStatus += new UnitStatusEventHandler(target_UnitStatus);

                   //m_target.Connect();
                   //m_target.Reset();
                   if (m_target.IsConnected)
                       m_target.Disconnect();
                   m_target.Connect((m_Machine.IP.ToString() == null) ? string.Empty : m_Machine.IP.ToString());*/
               }
               RaiseMachineStatusEvent(MachineInterface.etMachineStatus.On);
               m_bConnected = true;
            
//               Console.WriteLine("testing RaiseDebugTextEvent");
//               RaiseDebugTextEvent("Hello Mum!"); // DW - just testing the event handler
//               Console.WriteLine("should have seen something in the console by now?");
               m_readThread = new Thread(new ThreadStart(DebugThreadProc));
               m_readThread.Priority = ThreadPriority.Lowest;
               m_readThread.Start();
               m_readThread.Name = "PS3 Debug Log Thread";
                  
              
           }
        }

        public void Disconnect()
        {
            Console.WriteLine("aborting debuglog");
            //m_target.Disconnect();
            //m_target.Reset();
            m_bForceDisconnect = true;
            //RaiseMachineStatusEvent(MachineInterface.etMachineStatus.Unknown);
            m_bConnected = false;
            //m_target = null; // force garbage collection, cos the PS3 is garbage
            GC.Collect();

        }

        public void ThreadKillAndWait()
        {
            Console.WriteLine("m_readThread.Abort()");
           // m_readThread.Abort();
            Console.WriteLine("m_readThread.Join()");
          //  m_readThread.Join();
        }

        #endregion

      

        #region Private Functions
        private void DebugThreadProc()
        {
            if (!m_bEnabled)
                return;
            /*
            try
            {
                while (m_target.IsConnected && !m_bForceDisconnect)
                {
                    m_target.Update();
                    Thread.Sleep(1000);
                }
                RaiseMachineStatusEvent(MachineInterface.etMachineStatus.Off);
                m_bConnected = false;
            }
            catch
            {
                RaiseMachineStatusEvent(MachineInterface.etMachineStatus.Exception);
            }
             * */
        }
        #endregion //Private functions


        #region Event Handlers
/*        private void target_ConnectError(object sender, MessageEventArgs e)
        {
            //Console.WriteLine("target_ConnectError event being handled");
            RaiseDebugTextEvent(e.Message);
        }

        private void target_ConnectInfo(object sender, MessageEventArgs e)
        {
            //Console.WriteLine("target_ConnectInfo event being handled");
            RaiseDebugTextEvent(e.Message);
        }

        private void target_ConnectStatus(object sender, ConnectStatusEventArgs e)
        {
            //Console.WriteLine("target_ConnectStatus event being handled");
            string sConnectStatus = e.Status.ToString() + Environment.NewLine;
            RaiseDebugTextEvent(sConnectStatus);
        }

        private void target_DebugEvent(object sender, DebugEventEventArgs e)
        {
            //Console.WriteLine("target_DebugEvent event being handled");
            string sDebugEvent = e.Event.ToString() + Environment.NewLine;
            RaiseDebugTextEvent(sDebugEvent);
        }

        private void target_TTYMessage(object sender, MessageEventArgs e)
        {
            //Console.WriteLine("target_TTYMessage event being handled");
            RaiseDebugTextEvent(e.Message);
        }

        private void target_TargetEvent(object sender, TargetEventEventArgs e)
        {
            //Console.WriteLine("target_TargetEvent event being handled");
            string sTargetEvent = e.Event.ToString() + Environment.NewLine;
            RaiseDebugTextEvent(sTargetEvent);
        }

        private void target_UnhandledCrash(object sender, MessageEventArgs e)
        {
            //Console.WriteLine("target_UnhandledCrash event being handled");
            RaiseDebugTextEvent(e.Message);
        }

        private void target_UnitStatus(object sender, UnitStatusEventArgs e)
        {
            //Console.WriteLine("target_UnhandledCrash event being handled");
            //string sUnitStatus = e.Status.ToString() + Environment.NewLine;
            //RaiseDebugTextEvent(sUnitStatus);
        }
        
        /// <summary>
        /// Raise Debug Text Event (if we have listeners)
        /// </summary>
        /// <param name="sMessage"></param>
        private void RaiseDebugTextEvent(String message)
        {
            //Console.WriteLine("RaiseDebugTextEvent");

            if (null == DebugTextEvent)
                Console.WriteLine("DebugTextEvent is null");

            if (null != DebugTextEvent)
            {                
                DebugTextEvent(this, new MachineInterface.cDebugLogTextEventArgs(Machine, message));
            }
        }
        */
        /// <summary>
        /// Raise Machine Status Event (if we have listeners)
        /// </summary>
        /// <param name="status">New Machine status enumeration</param>
        private void RaiseMachineStatusEvent(MachineInterface.etMachineStatus status)
        {
            //Console.WriteLine("RaiseMachineStatusEvent");
            if (null != MachineStatusChangeEvent)
                MachineStatusChangeEvent(this, new MachineInterface.cMachineStatusEventArgs(Machine, status));
        }
        #endregion
    }

} // End of PS3 namespace

// End of file
