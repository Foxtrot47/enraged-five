namespace AreaTools.UI
{
    partial class uiAreaToolConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitMain = new System.Windows.Forms.SplitContainer();
            this.tblConfigLayout = new System.Windows.Forms.TableLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblText1 = new System.Windows.Forms.Label();
            this.cbGameTime = new System.Windows.Forms.ComboBox();
            this.cbGameLevel = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbUseDate = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.nudStartWorkUnit = new System.Windows.Forms.NumericUpDown();
            this.nudEndWorkUnit = new System.Windows.Forms.NumericUpDown();
            this.tblButtonLayout = new System.Windows.Forms.TableLayoutPanel();
            this.btnOK = new System.Windows.Forms.Button();
            this.splitMain.Panel1.SuspendLayout();
            this.splitMain.Panel2.SuspendLayout();
            this.splitMain.SuspendLayout();
            this.tblConfigLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudStartWorkUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEndWorkUnit)).BeginInit();
            this.tblButtonLayout.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitMain
            // 
            this.splitMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitMain.Location = new System.Drawing.Point(0, 0);
            this.splitMain.Name = "splitMain";
            this.splitMain.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitMain.Panel1
            // 
            this.splitMain.Panel1.Controls.Add(this.tblConfigLayout);
            // 
            // splitMain.Panel2
            // 
            this.splitMain.Panel2.Controls.Add(this.tblButtonLayout);
            this.splitMain.Size = new System.Drawing.Size(382, 225);
            this.splitMain.SplitterDistance = 132;
            this.splitMain.TabIndex = 0;
            // 
            // tblConfigLayout
            // 
            this.tblConfigLayout.ColumnCount = 2;
            this.tblConfigLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tblConfigLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tblConfigLayout.Controls.Add(this.label4, 0, 4);
            this.tblConfigLayout.Controls.Add(this.label2, 0, 2);
            this.tblConfigLayout.Controls.Add(this.lblText1, 0, 0);
            this.tblConfigLayout.Controls.Add(this.cbGameTime, 1, 0);
            this.tblConfigLayout.Controls.Add(this.cbGameLevel, 1, 1);
            this.tblConfigLayout.Controls.Add(this.label1, 0, 1);
            this.tblConfigLayout.Controls.Add(this.tbUseDate, 1, 2);
            this.tblConfigLayout.Controls.Add(this.label3, 0, 3);
            this.tblConfigLayout.Controls.Add(this.nudStartWorkUnit, 1, 3);
            this.tblConfigLayout.Controls.Add(this.nudEndWorkUnit, 1, 4);
            this.tblConfigLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblConfigLayout.Location = new System.Drawing.Point(0, 0);
            this.tblConfigLayout.Name = "tblConfigLayout";
            this.tblConfigLayout.RowCount = 5;
            this.tblConfigLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tblConfigLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tblConfigLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tblConfigLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tblConfigLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tblConfigLayout.Size = new System.Drawing.Size(382, 132);
            this.tblConfigLayout.TabIndex = 0;
            this.tblConfigLayout.Paint += new System.Windows.Forms.PaintEventHandler(this.tblConfigLayout_Paint);
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 112);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "End Work Unit:";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Use Date:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // lblText1
            // 
            this.lblText1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblText1.AutoSize = true;
            this.lblText1.Location = new System.Drawing.Point(22, 7);
            this.lblText1.Name = "lblText1";
            this.lblText1.Size = new System.Drawing.Size(64, 13);
            this.lblText1.TabIndex = 0;
            this.lblText1.Text = "Game Time:";
            // 
            // cbGameTime
            // 
            this.cbGameTime.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cbGameTime.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbGameTime.FormattingEnabled = true;
            this.cbGameTime.Items.AddRange(new object[] {
            "Noon",
            "Midnight"});
            this.cbGameTime.Location = new System.Drawing.Point(92, 3);
            this.cbGameTime.Name = "cbGameTime";
            this.cbGameTime.Size = new System.Drawing.Size(188, 21);
            this.cbGameTime.TabIndex = 1;
            this.cbGameTime.SelectedIndexChanged += new System.EventHandler(this.cbGameTime_SelectedIndexChanged);
            // 
            // cbGameLevel
            // 
            this.cbGameLevel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cbGameLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbGameLevel.FormattingEnabled = true;
            this.cbGameLevel.Items.AddRange(new object[] {
            "None",
            "gta5 - gta5",
            "gta5 - gta4"});
            this.cbGameLevel.Location = new System.Drawing.Point(92, 30);
            this.cbGameLevel.Name = "cbGameLevel";
            this.cbGameLevel.Size = new System.Drawing.Size(188, 21);
            this.cbGameLevel.TabIndex = 3;
            this.cbGameLevel.SelectedIndexChanged += new System.EventHandler(this.cbGameLevel_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Game Level:";
            // 
            // tbUseDate
            // 
            this.tbUseDate.Location = new System.Drawing.Point(92, 57);
            this.tbUseDate.Name = "tbUseDate";
            this.tbUseDate.Size = new System.Drawing.Size(188, 20);
            this.tbUseDate.TabIndex = 5;
            this.tbUseDate.TextChanged += new System.EventHandler(this.tbUseDate_TextChanged);
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Start Work Unit:";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // nudStartWorkUnit
            // 
            this.nudStartWorkUnit.Location = new System.Drawing.Point(92, 83);
            this.nudStartWorkUnit.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.nudStartWorkUnit.Name = "nudStartWorkUnit";
            this.nudStartWorkUnit.Size = new System.Drawing.Size(120, 20);
            this.nudStartWorkUnit.TabIndex = 6;
            this.nudStartWorkUnit.ValueChanged += new System.EventHandler(this.nudStartWorkUnit_NumChanged);
            // 
            // nudEndWorkUnit
            // 
            this.nudEndWorkUnit.Location = new System.Drawing.Point(92, 109);
            this.nudEndWorkUnit.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.nudEndWorkUnit.Name = "nudEndWorkUnit";
            this.nudEndWorkUnit.Size = new System.Drawing.Size(120, 20);
            this.nudEndWorkUnit.TabIndex = 9;
            this.nudEndWorkUnit.ValueChanged += new System.EventHandler(this.nudEndWorkUnit_NumChanged);
            // 
            // tblButtonLayout
            // 
            this.tblButtonLayout.ColumnCount = 2;
            this.tblButtonLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblButtonLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tblButtonLayout.Controls.Add(this.btnOK, 0, 0);
            this.tblButtonLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblButtonLayout.Location = new System.Drawing.Point(0, 0);
            this.tblButtonLayout.Name = "tblButtonLayout";
            this.tblButtonLayout.RowCount = 1;
            this.tblButtonLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tblButtonLayout.Size = new System.Drawing.Size(382, 89);
            this.tblButtonLayout.TabIndex = 0;
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(3, 3);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = "&OK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // uiAreaToolConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(382, 225);
            this.ControlBox = false;
            this.Controls.Add(this.splitMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "uiAreaToolConfig";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Area Tool Configuration";
            this.Load += new System.EventHandler(this.uiAreaToolConfig_Load);
            this.splitMain.Panel1.ResumeLayout(false);
            this.splitMain.Panel2.ResumeLayout(false);
            this.splitMain.ResumeLayout(false);
            this.tblConfigLayout.ResumeLayout(false);
            this.tblConfigLayout.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudStartWorkUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEndWorkUnit)).EndInit();
            this.tblButtonLayout.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitMain;
        private System.Windows.Forms.TableLayoutPanel tblButtonLayout;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.TableLayoutPanel tblConfigLayout;
        private System.Windows.Forms.Label lblText1;
        private System.Windows.Forms.ComboBox cbGameTime;
        private System.Windows.Forms.ComboBox cbGameLevel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbUseDate;
        private System.Windows.Forms.NumericUpDown nudStartWorkUnit;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown nudEndWorkUnit;
    }
}