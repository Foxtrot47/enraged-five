//
// File: uiSchedulerConfig.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of uiSchedulerConfig.cs class
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DistMonitor.Scheduler.UI
{

    /// <summary>
    /// Scheduler Configuration Dialog
    /// </summary>
    internal partial class uiSchedulerConfig : Form
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// 
        /// </summary>
        public cScheduledTaskManager ScheduledTaskManager
        {
            get { return m_ScheduledTaskManager; }
        }
        private cScheduledTaskManager m_ScheduledTaskManager;
        #endregion // Properties and Associated Member Data

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public uiSchedulerConfig()
        {
            InitializeComponent();
            this.TaskListView.TaskSelectionEvent += OnTaskSelection;
        }
        #endregion // Constructor

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="manager"></param>
        public void SetScheduledTaskManager(cScheduledTaskManager manager)
        {
                if (null != this.TaskListView.ScheduledTaskManager)
                {
                    this.TaskListView.ScheduledTaskManager.TaskAddedEvent -= TaskListView.OnTaskAdded;
                    this.TaskListView.ScheduledTaskManager.TaskRemovedEvent -= TaskListView.OnTaskRemoved;
                    this.TaskListView.ScheduledTaskManager.TaskChangedEvent -= TaskListView.OnTaskChanged;
                }

                m_ScheduledTaskManager = manager;
                this.TaskListView.ScheduledTaskManager = manager;

                if (null != this.TaskListView.ScheduledTaskManager)
                {
                    this.TaskListView.ScheduledTaskManager.TaskAddedEvent += TaskListView.OnTaskAdded;
                    this.TaskListView.ScheduledTaskManager.TaskRemovedEvent += TaskListView.OnTaskRemoved;
                    this.TaskListView.ScheduledTaskManager.TaskChangedEvent += TaskListView.OnTaskChanged;
                }
        }
        #endregion // Controller Methods

        #region Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnTaskSelection(Object sender, cScheduledTaskEventArgs e)
        {
            btnRemoveTask.Enabled = (null != e.Task);
            btnProperties.Enabled = (null != e.Task);
        }
        #endregion // Event Handlers

        #region UI Event Handlers
        /// <summary>
        /// Add Task Button Click Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddTask_Click(Object sender, EventArgs e)
        {
            using (uiScheduledTaskProperties uiPropForm = new uiScheduledTaskProperties())
            {
                cScheduledTask task = new cScheduledTask();
                uiPropForm.SetTask(task);

                if (DialogResult.OK != uiPropForm.ShowDialog())
                    return;

                // Otherwise create a new task using the UI configuration
                cSchedule schedule = 
                    new cSchedule((cSchedule.etType)Enum.Parse(typeof(cSchedule.etType), uiPropForm.Schedule, true),
                                  uiPropForm.StartDateTime, default(DateTime), uiPropForm.EndDateTime);

                task.Initialise(uiPropForm.TaskName, uiPropForm.Game, 
                                uiPropForm.Configuration, uiPropForm.Tool,
                                schedule);
                
                ScheduledTaskManager.AddTask(task);
            }
        }

        /// <summary>
        /// Remove Task Button Click Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRemoveTask_Click(Object sender, EventArgs e)
        {
            if (0 == TaskListView.SelectedItems.Count)
                return;

            uiScheduledTaskListViewItem taskItem =
                    (TaskListView.SelectedItems[0] as uiScheduledTaskListViewItem);

            ScheduledTaskManager.RemoveTask(taskItem.Task);
        }

        /// <summary>
        /// Task Properties Button Click Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnProperties_Click(Object sender, EventArgs e)
        {
            if (0 == TaskListView.SelectedItems.Count)
                return;

            using (uiScheduledTaskProperties uiPropForm = new uiScheduledTaskProperties())
            {
                uiScheduledTaskListViewItem taskItem = 
                    (TaskListView.SelectedItems[0] as uiScheduledTaskListViewItem);
                uiPropForm.SetTask(taskItem.Task);

                if (DialogResult.Cancel == uiPropForm.ShowDialog())
                    return;

                cSchedule schedule = 
                    new cSchedule((cSchedule.etType)Enum.Parse(typeof(cSchedule.etType), uiPropForm.Schedule, true),
                                  uiPropForm.StartDateTime, default(DateTime), uiPropForm.EndDateTime);

                taskItem.Task.Initialise(uiPropForm.TaskName, uiPropForm.Game,
                                        uiPropForm.Configuration, uiPropForm.Tool,
                                        schedule);
            }
        }

        /// <summary>
        /// Context Menu Opening Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ctxMenu_Opening(object sender, CancelEventArgs e)
        {
            miCtxProperties.Enabled = (TaskListView.SelectedItems.Count > 0);
        }

        /// <summary>
        /// Context Menu Properties Menu Item Click Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void miCtxProperties_Click(object sender, EventArgs e)
        {
            btnProperties_Click(sender, e);
        }

        /// <summary>
        /// Context Menu Refresh Menu Item Click Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void miCtxRefresh_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in TaskListView.Items)
            {
                uiScheduledTaskListViewItem taskItem =
                    (item as uiScheduledTaskListViewItem);
                taskItem.UpdateView();
            }
        }
        #endregion // UI Event Handlers
    }

} // End of DistMonitor.Scheduler.UI namespace

// End of file
