//
// File: cController.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of cController.cs class
//

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Threading;
using System.Text;
using System.Windows.Forms;

using RSG.Base.Logging;

namespace DistMonitor
{

    /// <summary>
    /// Application Controller Class (owner of all data)
    /// </summary>
    internal class cController
    {
        #region Delegates
        /// <summary>
        /// Delegate for thread-safe MainForm Tool UI initialisation
        /// </summary>
        private delegate void ToolInitialisationDelegate(ToolInterface.iTool tool, bool defaults);
        #endregion // Delegates

        #region Enumerations
        /// <summary>
        /// 
        /// </summary>
        public enum etState
        {
            Stopped,
            Running
        }
        #endregion // Enumerations

        #region Events
        /// <summary>
        /// Event raised when Machine Farm is started
        /// </summary>
        public event EventHandler<EventArgs> MachineFarmStartEvent;

        /// <summary>
        /// Event raised when Machine Farm is stopped
        /// </summary>
        public event EventHandler<EventArgs> MachineFarmStopEvent;

        /// <summary>
        /// Event raised when Machine Farm is rebooted
        /// </summary>
        public event EventHandler<EventArgs> MachineFarmRebootEvent;
        #endregion // Events

        #region Properties and Associated Member Data
        #region Model Data
        /// <summary>
        /// Controller state
        /// </summary>
        public etState State
        {
            get { return m_eState; }
            private set { m_eState = value; }
        }
        private etState m_eState;

        /// <summary>
        /// Thread Pool
        /// </summary>
        public RSG.Base.Threading.cThreadPool ThreadPool
        {
            get { return m_ThreadPool; }
            private set { m_ThreadPool = value; }
        }
        private RSG.Base.Threading.cThreadPool m_ThreadPool;

        /// <summary>
        /// Machine Farm
        /// </summary>
        public cMachineFarm MachineFarm
        {
            get { return m_MachineFarm; }
            private set { m_MachineFarm = value; }
        }
        private cMachineFarm m_MachineFarm;

        /// <summary>
        /// Scheduled Task Manager component
        /// </summary>
        public Scheduler.cScheduledTaskManager ScheduleManager
        {
            get { return m_ScheduleManager; }
            private set { m_ScheduleManager = value; }
        }
        private Scheduler.cScheduledTaskManager m_ScheduleManager;

        /// <summary>
        /// Game List
        /// </summary>
        public cGameList GameList
        {
            get { return m_GameList; }
            private set { m_GameList = value; }
        }
        private cGameList m_GameList;

        /// <summary>
        /// Current Game
        /// </summary>
        public GameInterface.iGame Game
        {
            get { return m_Game; }
            private set { m_Game = value; }
        }
        private GameInterface.iGame m_Game;

        /// <summary>
        /// Current Configuration
        /// </summary>
        public GameInterface.cConfiguration Configuration
        {
            get { return m_Configuration; }
            private set { m_Configuration = value; }
        }
        private GameInterface.cConfiguration m_Configuration;

        /// <summary>
        /// Current Area Tool
        /// </summary>
        public ToolInterface.iTool Tool
        {
            get { return m_Tool; }
            private set { m_Tool = value; }
        }
        private ToolInterface.iTool m_Tool;

        /// <summary>
        /// Machine Plugins
        /// </summary>
        public RSG.Base.Plugins.cPluginLoader<MachineInterface.iMachine> MachinePlugins
        {
            get { return m_MachinePlugins; }
            private set { m_MachinePlugins = value; }
        }
        private RSG.Base.Plugins.cPluginLoader<MachineInterface.iMachine> m_MachinePlugins;

        /// <summary>
        /// Debug Log Plugins
        /// </summary>
        public RSG.Base.Plugins.cPluginLoader<MachineInterface.iDebugLog> DebugLogPlugins
        {
            get { return m_DebugLogPlugins; }
            private set { m_DebugLogPlugins = value; }
        }
        private RSG.Base.Plugins.cPluginLoader<MachineInterface.iDebugLog> m_DebugLogPlugins;

        /// <summary>
        /// Tool Plugins
        /// </summary>
        public RSG.Base.Plugins.cPluginLoader<ToolInterface.iTool> ToolPlugins
        {
            get { return m_ToolPlugins; }
            private set { m_ToolPlugins = value; }
        }
        private RSG.Base.Plugins.cPluginLoader<ToolInterface.iTool> m_ToolPlugins;

        /// <summary>
        /// Tool Plugin Setting
        /// </summary>
        public RSG.Base.Plugins.cPluginLoader<ToolInterface.iToolSettings> ToolSettings
        {
            get { return m_ToolSettings; }
            private set { m_ToolSettings = value; }
        }
        private RSG.Base.Plugins.cPluginLoader<ToolInterface.iToolSettings> m_ToolSettings;
        #endregion // Model Data
        
        #region View Data
        /// <summary>
        /// Farm View
        /// </summary>
        public UI.uiFarmView FarmView
        {
            get { return m_uiFarmView; }
            private set { m_uiFarmView = value; }
        }
        private UI.uiFarmView m_uiFarmView;

        /// <summary>
        /// Farm Log View
        /// </summary>
        public UI.uiFarmLog FarmLog
        {
            get { return m_uiFarmLog; }
            private set { m_uiFarmLog = value; }
        }
        private UI.uiFarmLog m_uiFarmLog;
        #endregion // View Data
        #endregion // Properties and Associated Member Data

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        /// The cController is the central management class in the DistMonitor.
        /// Here we construct the core Model components (MachineFarm, GameList
        /// and ThreadPool) and connect up the Model and View components.
        /// 
        public cController()
        {
        }
        #endregion

        #region Controller Methods
        /// <summary>
        /// Configure Machine Farm
        /// </summary>
        public void ConfigureFarm()
        {
            Log.Log__Message("*** CONFIGURING MACHINE FARM ***");

            UI.uiMachineForm uiForm = new UI.uiMachineForm();

            // Check dialog result, updating machines if appropriate
            if (DialogResult.OK != uiForm.ShowDialog())
                return;

            Program.Controller.MachineFarm.RemoveAll();

            uiForm.Machines.Deduplicate();

            foreach (UI.cMachine machine in uiForm.Machines)
            {
                // Loop through plugins to find plugin suitable for this machine arch.
                for (int nIndex = 0; nIndex < Program.Controller.MachinePlugins.Plugins.Count; ++nIndex)
                {
                    MachineInterface.iMachine mach =
                        (MachineInterface.iMachine)Program.Controller.MachinePlugins.CreatePluginInstance(nIndex);

                    // Determine if we have matched architecture string
                    if (machine.Arch == mach.Name)
                    {
                        mach.Initialise(machine.IP);
                        Program.Controller.MachineFarm.Add(mach);
                    }
                }
            }

            // DW - save the farm now else we can lose the settings if the app crashes...
            SaveFarm();
        }

        /// <summary>
        /// Initialise Configured Farm
        /// </summary>
        public void Initialise(UI.uiFarmView farmview, UI.uiFarmLog farmlog)
        {
            Log.Log__Message("*** INIT MACHINE FARM ***");

            Log.Log__Message(String.Format("{0} Controller initialising...",
                                 System.Windows.Forms.Application.ProductName));
            
            // Model Data
            this.ThreadPool = new RSG.Base.Threading.cThreadPool();
            this.MachineFarm = new cMachineFarm();
            this.GameList = new cGameList("Data\\RSN Games.xml");
            this.ScheduleManager = new Scheduler.cScheduledTaskManager(this.ThreadPool, farmlog);

            // View Data
            this.FarmView = farmview;
            this.FarmLog = farmlog;

            this.FarmLog.SetThreadPool(this.ThreadPool);

            // View -> Model Connections
            this.FarmView.Farm = this.MachineFarm;
            this.FarmLog.Farm = this.MachineFarm;

            // Model -> View Event Connections
            this.MachineFarm.MachineAddEvent += this.FarmView.OnMachineAdd;
            this.MachineFarm.MachineRemoveEvent += this.FarmView.OnMachineRemove;
            this.MachineFarm.MonitorUpdateEvent += this.FarmView.OnMonitorUpdate;
            this.MachineFarm.MachineStatusChangeEvent += this.FarmView.OnMachineStatusChange;
            this.MachineFarm.MachineEnabledChangeEvent += this.FarmView.OnMachineEnabledChange;
            this.MachineFarm.MachineAddEvent += this.FarmLog.OnMachineAdd;
            this.MachineFarm.MachineRemoveEvent += this.FarmLog.OnMachineRemove;
            this.MachineFarm.MonitorUpdateEvent += this.FarmLog.OnMonitorUpdate;

            // View -> Model Event Connections
            this.FarmView.MachineCheckedChangedEvent += this.MachineFarm.OnMachineEnabledChange;

            State = etState.Stopped;
            Properties.Settings.Default.Reload();
            LoadMachinePlugins(Properties.Settings.Default.MachinePlugins);
            LoadDebugLogPlugins(Properties.Settings.Default.MachinePlugins);
            LoadToolPlugins(Properties.Settings.Default.ToolPlugins);
            Debug.Assert(MachinePlugins.Plugins.Count == DebugLogPlugins.Plugins.Count,
                         "Internal Error: not all machine plugins have a debug log plugin defined.");

            // Load Scheduled Task Settings
            this.ScheduleManager.LoadXml(Properties.Settings.Default.TaskFile);

            // Load Farm Machine Settings
            String sFarmMachines = Properties.Settings.Default.FarmMachines;
            String[] machines = sFarmMachines.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (String machine in machines)
            {
                String[] ipArch = machine.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                if (2 != ipArch.Length)
                    continue;

                // Loop through plugins to find plugin suitable for this machine arch.
                for (int nIndex = 0; nIndex < MachinePlugins.Plugins.Count; ++nIndex)
                {
                    MachineInterface.iMachine mach = 
                        (MachineInterface.iMachine)MachinePlugins.CreatePluginInstance(nIndex);
                    
                    // Determine if we have matched architecture string
                    if (ipArch[1] == mach.Name)
                    {
                        mach.Initialise(System.Net.IPAddress.Parse(ipArch[0]));
                        MachineFarm.Add(mach);
                    }
                }
            }

            Game = null;
            Configuration = null;
            Tool = null;
        }

        /// <summary>
        /// Shutdown Configured Farm
        /// </summary>
        public void Shutdown()
        {
            Log.Log__Message("*** SHUTTING DOWN MACHINE FARM ***");

            State = etState.Stopped;

            foreach (cMachineMonitor monitor in MachineFarm.Monitors)
            {
                if (monitor.Machine.Enabled && monitor.Machine.Available && 
                    monitor.Machine.Status != MachineInterface.etMachineStatus.Off)
                   monitor.Reboot(MachineInterface.etRebootType.Warm);
                monitor.Monitoring = false;
            }

            // Save Scheduled Task Settings
            ScheduleManager.SaveXml(Properties.Settings.Default.TaskFile);

            // Save farm machine settings
            SaveFarm();

            // Shutdown ThreadPool
            ThreadPool.Shutdown();
        }

        /// <summary>
        /// Saves the machines added to the farm
        /// </summary>
        public void SaveFarm()
        {
            Log.Log__Message("*** SAVING MACHINE FARM ***");

            String sFarmMachines = String.Empty;
            foreach (MachineInterface.iMachine machine in MachineFarm.Machines)
                sFarmMachines += String.Format("{0};", machine.ToString());
            Properties.Settings.Default.FarmMachines = sFarmMachines;
            Properties.Settings.Default.Save();
        }

        /// <summary>
        /// Set current Game, Config and Area Tools (from UI)
        /// </summary>
        /// <param name="game"></param>
        /// <param name="config"></param>
        /// <param name="tool"></param>
        public void SetCurrentConfig(GameInterface.iGame game, 
                                     GameInterface.cConfiguration config, 
                                     ToolInterface.iTool tool)
        {
            Game = game;
            Configuration = config;
            Tool = tool;

        }

        /// <summary>
        /// Set machines list (e.g. from farm configuration dialog)
        /// </summary>
        /// <param name="machines"></param>
        public void SetMachines(List<MachineInterface.iMachine> machines)
        {
            MachineFarm.RemoveAll();
            foreach (MachineInterface.iMachine machine in machines)
                MachineFarm.Add(machine);
        }

        /// <summary>
        /// Start Machine Farm
        /// </summary>
        public void Start()
        {
            Log.Log__Message("*** STOPPING MACHINE FARM ***");

            State = etState.Running;
            InitialiseTool(Tool, false);

            // DW - I had to move this to before the rest of the stuff here otherwise the machine doesn;t connect to find out it can;t connect before assigning work units.
            MachineFarm.MakeAllAvailable();
            MachineFarm.Connect();

            // Start machines
            foreach (cMachineMonitor monitor in MachineFarm.Monitors)
            {
                monitor.Machine.Failures = 0;

                if (monitor.Machine.Failed)
                {
                    monitor.Machine.Failed = false;
                    monitor.Machine.Available = true;
                }

                if (monitor.Machine.Enabled && monitor.Machine.Available &&
                    monitor.Machine.Status != MachineInterface.etMachineStatus.Off)
                    monitor.Start();
            }                       

            if (null != MachineFarmStartEvent)
                MachineFarmStartEvent(this, new EventArgs());
        }

        /// <summary>
        /// Start machine farm (using configuration, for scheduler)
        /// </summary>
        /// <param name="game"></param>
        /// <param name="config"></param>
        /// <param name="tool"></param>
        public void Start(GameInterface.iGame game, 
                          GameInterface.cConfiguration config, 
                          ToolInterface.iTool tool)
        {
            Log.Log__Message("*** STARTING MACHINE FARM UNDER SCHEDULER ***");

            SetCurrentConfig(game, config, tool);

            State = etState.Running;
            InitialiseTool(Tool, true);

            // Connect to target manager ( in case of ps3s )
            MachineFarm.MakeAllAvailable();
            MachineFarm.Connect();

            // Start machines
            foreach (cMachineMonitor monitor in MachineFarm.Monitors)
            {
                monitor.Machine.Failures = 0;

                if (monitor.Machine.Failed)
                {
                    monitor.Machine.Failed = false;
                    monitor.Machine.Available = true;                   
                }

                if (monitor.Machine.Enabled && monitor.Machine.Available &&
                    monitor.Machine.Status != MachineInterface.etMachineStatus.Off)
                    monitor.Start();
            }

            if (null != MachineFarmStartEvent)
                MachineFarmStartEvent(this, new EventArgs());
        }

        /// <summary>
        /// Stop Machine Farm
        /// </summary>
        public void Stop()
        {
            Log.Log__Message("*** STOPPING MACHINE FARM ***");

            State = etState.Stopped;

            foreach (cMachineMonitor monitor in MachineFarm.Monitors)
            {
                if (monitor.Machine.Enabled && monitor.Machine.Available &&
                    monitor.Machine.Status != MachineInterface.etMachineStatus.Off)
                    monitor.StopAndReboot();
            }

            this.ScheduleManager.ResetState();

            // Disconnect from target manager ( in case of ps3s )
            MachineFarm.Disconnect();

            if (null != MachineFarmStopEvent)
                MachineFarmStopEvent(this, new EventArgs());
        }

        /// <summary>
        /// Reboot Machine Farm
        /// </summary>
        /// <param name="type"></param>
        public void Reboot(MachineInterface.etRebootType type)
        {
            Log.Log__Message("*** REBOOT MACHINE FARM ***");

            foreach (cMachineMonitor monitor in MachineFarm.Monitors)
            {
                if (monitor.Machine.Enabled && monitor.Machine.Available &&
                    monitor.Machine.Status != MachineInterface.etMachineStatus.Off)
                    monitor.Reboot(MachineInterface.etRebootType.Warm);
            }

            if (null != MachineFarmRebootEvent)
                MachineFarmRebootEvent(this, new EventArgs());
        }

        /// <summary>
        /// Ping Machine Farm
        /// </summary>
        public void Ping()
        {
            foreach (cMachineMonitor monitor in MachineFarm.Monitors)
            {
                if (monitor.Machine.Enabled)
                    monitor.Ping();
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Load available machine plugins
        /// </summary>
        /// <param name="sDirectory">Plugin source directory</param>
        private void LoadMachinePlugins(String sDirectory)
        {
            Log.Log__Message("Loading Machine Plugins...");
            MachinePlugins = new RSG.Base.Plugins.cPluginLoader<MachineInterface.iMachine>(sDirectory);
            foreach (Type machineType in MachinePlugins.Plugins)
            {
                MachineInterface.iMachine machine = MachinePlugins.CreatePluginInstance(machineType);
                Log.Log__Message(String.Format("\t{0} loaded.", machine.Name));
            }
        }

        /// <summary>
        /// Load available debug log plugins
        /// </summary>
        /// <param name="sDirectory">Plugin source directory</param>
        private void LoadDebugLogPlugins(String sDirectory)
        {
            Log.Log__Message("Loading Machine Plugins...");
            DebugLogPlugins = new RSG.Base.Plugins.cPluginLoader<MachineInterface.iDebugLog>(sDirectory);
        }

        /// <summary>
        /// Load available tool plugins
        /// </summary>
        /// <param name="sDirectory"></param>
        private void LoadToolPlugins(String sDirectory)
        {
            Log.Log__Message("Loading Tool Plugins...");
            ToolPlugins = new RSG.Base.Plugins.cPluginLoader<ToolInterface.iTool>(sDirectory);
            ToolSettings = new RSG.Base.Plugins.cPluginLoader<ToolInterface.iToolSettings>(sDirectory);
            foreach (Type toolType in ToolPlugins.Plugins)
            {
                ToolInterface.iTool tool = ToolPlugins.CreatePluginInstance(toolType);
                Log.Log__Message(String.Format("\t{0} loaded.", tool.Name));
            }
        }

        /// <summary>
        /// Initialise Tool
        /// </summary>
        private void InitialiseTool(ToolInterface.iTool tool, bool defaults)
        {
            if (null == tool)
                return;

            if (Program.MainForm.InvokeRequired)
            {
                Program.MainForm.Invoke(new ToolInitialisationDelegate(InitialiseTool), new Object[] { tool, defaults });
            }
            else
            {
                TabPage page = new TabPage(Tool.Name);
                SplitContainer split = new SplitContainer();
                page.Controls.Add(split);
                Program.MainForm.tabControl.TabPages.Add(page);
                Control ctrl = Tool.InitialiseUI(defaults);

                // Initialise SplitContainer
                split.Orientation = Orientation.Horizontal;
                split.Dock = DockStyle.Fill;
                split.IsSplitterFixed = true;
                split.SplitterDistance = split.Height;

                // Initialise Tool UI
                ctrl.Dock = DockStyle.Fill;
                split.Panel1.Controls.Add(ctrl);

                // Initialise Additional UI Elements
                Button btnClose = new Button();
                btnClose.Anchor = AnchorStyles.Right;
                btnClose.Text = "Close";
                btnClose.Click += OnCloseButtonClick;
                split.Panel2.Controls.Add(btnClose);

                this.Tool.ToolFinishedEvent += OnToolFinishedEvent;
                this.Tool.ToolAlertMessageEvent += OnToolAlertMessageEvent;

                // Initialise Global Tool Monitor Thread
                this.ThreadPool.QueueWorkItem(new RSG.Base.Threading.WorkItemCallback(this.MonitorTool));

                // Update the webserver update flag at the start.
                this.Tool.UpdateWebserver = Properties.Settings.Default.UpdateWebserver;

                // Update the webserver update flag at the start.
                this.Tool.Sync = Properties.Settings.Default.Sync;
                this.Tool.SyncLabel = Properties.Settings.Default.SyncLabel;

                try
                {
                    Tool.PreEvent();
                }
                catch (Exception ex)
                {
                    Log.Log__Error(String.Format("Exception invoking tool's PreEvent(): {0}.", ex.Message));
                }
            }
        }

        /// <summary>
        /// Global Tool Monitor Thread Function
        /// </summary>
        private Object MonitorTool(Object stateInfo)
        {
            if (null == Thread.CurrentThread.Name)
                Thread.CurrentThread.Name = "DistMonitor_ToolMonitor";

            while ((etState.Running == this.State) &&
                   (ToolInterface.etToolStatus.Finished != this.Tool.Status))
            {
                try
                {
                    Log.Log__Debug(String.Format("Tool Monitor for Tool {0} alive.", this.Tool.Name));

                    this.Tool.Monitor();
                    Thread.Sleep(Properties.Settings.Default.GlobalMonitorInterval * 1000);
                }
                catch (Exception ex)
                {
                    Log.Log__Error(String.Format("Tool Monitor Exception ({0}): {1}", this.Tool.Name,
                                       ex.Message));
                }
            }

            Log.Log__Message("Tool Monitor shutting down.");

            return (true);
        }
        #endregion // Private Methods

        #region Private Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnMachineCheckedChanged(Object sender, MachineInterface.cMachineEnabledEventArgs e)
        {
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnCloseButtonClick(Object sender, EventArgs e)
        {
            if (this.State != etState.Stopped)
                return;

            if ((sender as Button).Parent.Parent.Parent is TabPage)
            {
                TabPage page = ((sender as Button).Parent.Parent.Parent as TabPage);
                Program.MainForm.tabControl.TabPages.Remove(page);
            }            
        }

        /// <summary>
        /// Tool Finished Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnToolFinishedEvent(Object sender, ToolInterface.cToolEventArgs e)
        {
            try
            {
                this.Tool.PostEvent();
            }
            catch (Exception ex)
            {
                Log.Log__Error(String.Format("Exception invoking tool's PostEvent(): {0}.", ex.Message));
            }
            this.Stop();

            // Disconnect
            this.Tool.ToolFinishedEvent -= OnToolFinishedEvent;
            this.Tool.ToolAlertMessageEvent -= OnToolAlertMessageEvent;
        }

        /// <summary>
        /// Tool Alert Message Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnToolAlertMessageEvent(Object sender, ToolInterface.cToolMessageEventArgs e)
        {
            Program.MainForm.DisplayAlertMessage(e.Message);
            Log.Log__Debug(e.Message);
        }
        #endregion // Private Event Handlers
    }
    
} // End of DistMonitor namespace

// End of file
