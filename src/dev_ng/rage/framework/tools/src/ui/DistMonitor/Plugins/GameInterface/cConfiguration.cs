//
// File: cConfiguration.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of cConfiguration.cs class
//

using System;
using System.Collections.Generic;
using System.Text;

namespace GameInterface
{
    
    public class cConfiguration
    {
        #region Properties and Associated Member Data
        public String Machine
        {
            get { return m_sMachine; }
            private set { m_sMachine = value; }
        }
        private String m_sMachine;

        public String Name
        {
            get { return m_sName; }
            private set { m_sName = value; }
        }
        private String m_sName;

        public String MachineRootDirectory
        {
            get { return m_sMachineRoot; }
            private set { m_sMachineRoot = value; }
        }
        private String m_sMachineRoot;

        public String Executable
        {
            get { return m_sExecutable; }
            private set { m_sExecutable = value; }
        }
        private String m_sExecutable;

        public String Arguments
        {
            get { return m_sArguments; }
            private set { m_sArguments = value; }
        }
        private String m_sArguments;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor specifying configuration name and executable name
        /// </summary>
        /// <param name="name"></param>
        /// <param name="executable"></param>
        public cConfiguration(String machine, String name, String root, String executable, String args)
        {
            Machine = machine;
            Name = name;
            MachineRootDirectory = root;
            Executable = executable;
            Arguments = args;
        }
        #endregion

        #region Controller Methods
        /// <summary>
        /// Return configuration as a string
        /// </summary>
        /// <returns>Configuration as user-friendly String</returns>
        public override String ToString()
        {
            return (String.Format("{0} - {1}", Machine, Name));
        }
        #endregion
    }

} // End of GameInterface namespace

// End of file
