namespace DistMonitor.UI
{
    partial class uiFarmView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uiFarmView));
            this.lvStatus = new System.Windows.Forms.ListView();
            this.hdrImage = new System.Windows.Forms.ColumnHeader();
            this.hdrMachine = new System.Windows.Forms.ColumnHeader();
            this.hdrStatus = new System.Windows.Forms.ColumnHeader();
            this.hdrLatency = new System.Windows.Forms.ColumnHeader();
            this.hdrFailures = new System.Windows.Forms.ColumnHeader();
            this.hdrFailed = new System.Windows.Forms.ColumnHeader();
            this.hdrAvailable = new System.Windows.Forms.ColumnHeader();
            this.ctxMenuView = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miMachine = new System.Windows.Forms.ToolStripMenuItem();
            this.miMachineReboot = new System.Windows.Forms.ToolStripMenuItem();
            this.miMachineRebootCold = new System.Windows.Forms.ToolStripMenuItem();
            this.miCtxConfigure = new System.Windows.Forms.ToolStripMenuItem();
            this.miSep2 = new System.Windows.Forms.ToolStripSeparator();
            this.miShowGroups = new System.Windows.Forms.ToolStripMenuItem();
            this.miSep1 = new System.Windows.Forms.ToolStripSeparator();
            this.miRefresh = new System.Windows.Forms.ToolStripMenuItem();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.ctxMenuView.SuspendLayout();
            this.SuspendLayout();
            // 
            // lvStatus
            // 
            this.lvStatus.CheckBoxes = true;
            this.lvStatus.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.hdrImage,
            this.hdrMachine,
            this.hdrStatus,
            this.hdrLatency,
            this.hdrFailures,
            this.hdrFailed,
            this.hdrAvailable});
            this.lvStatus.ContextMenuStrip = this.ctxMenuView;
            this.lvStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvStatus.FullRowSelect = true;
            this.lvStatus.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvStatus.LargeImageList = this.imageList;
            this.lvStatus.Location = new System.Drawing.Point(0, 0);
            this.lvStatus.Name = "lvStatus";
            this.lvStatus.Size = new System.Drawing.Size(549, 303);
            this.lvStatus.SmallImageList = this.imageList;
            this.lvStatus.TabIndex = 2;
            this.lvStatus.UseCompatibleStateImageBehavior = false;
            this.lvStatus.View = System.Windows.Forms.View.Details;
            this.lvStatus.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.lvStatus_ItemChecked);
            this.lvStatus.SelectedIndexChanged += new System.EventHandler(this.lvStatus_SelectedIndexChanged);
            // 
            // hdrImage
            // 
            this.hdrImage.Text = "";
            this.hdrImage.Width = 40;
            // 
            // hdrMachine
            // 
            this.hdrMachine.Text = "Machine";
            this.hdrMachine.Width = 135;
            // 
            // hdrStatus
            // 
            this.hdrStatus.DisplayIndex = 3;
            this.hdrStatus.Text = "Status";
            this.hdrStatus.Width = 118;
            // 
            // hdrLatency
            // 
            this.hdrLatency.DisplayIndex = 4;
            this.hdrLatency.Text = "Latency (ms)";
            this.hdrLatency.Width = 80;
            // 
            // hdrFailures
            // 
            this.hdrFailures.DisplayIndex = 5;
            this.hdrFailures.Text = "Failures";
            // 
            // hdrFailed
            // 
            this.hdrFailed.DisplayIndex = 6;
            this.hdrFailed.Text = "Failed";
            // 
            // hdrAvailable
            // 
            this.hdrAvailable.DisplayIndex = 2;
            this.hdrAvailable.Text = "Available";
            // 
            // ctxMenuView
            // 
            this.ctxMenuView.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miMachine,
            this.miCtxConfigure,
            this.miSep2,
            this.miShowGroups,
            this.miSep1,
            this.miRefresh});
            this.ctxMenuView.Name = "ctxMenuView";
            this.ctxMenuView.Size = new System.Drawing.Size(188, 104);
            this.ctxMenuView.Opening += new System.ComponentModel.CancelEventHandler(this.ctxMenuView_Opening);
            // 
            // miMachine
            // 
            this.miMachine.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miMachineReboot,
            this.miMachineRebootCold});
            this.miMachine.Name = "miMachine";
            this.miMachine.Size = new System.Drawing.Size(187, 22);
            this.miMachine.Text = "Machine";
            // 
            // miMachineReboot
            // 
            this.miMachineReboot.Name = "miMachineReboot";
            this.miMachineReboot.Size = new System.Drawing.Size(144, 22);
            this.miMachineReboot.Text = "&Reboot";
            this.miMachineReboot.Click += new System.EventHandler(this.miMachineReboot_Click);
            // 
            // miMachineRebootCold
            // 
            this.miMachineRebootCold.Name = "miMachineRebootCold";
            this.miMachineRebootCold.Size = new System.Drawing.Size(144, 22);
            this.miMachineRebootCold.Text = "Reboot &Cold";
            this.miMachineRebootCold.Click += new System.EventHandler(this.miMachineRebootCold_Click);
            // 
            // miCtxConfigure
            // 
            this.miCtxConfigure.Image = ((System.Drawing.Image)(resources.GetObject("miCtxConfigure.Image")));
            this.miCtxConfigure.ImageTransparentColor = System.Drawing.Color.Fuchsia;
            this.miCtxConfigure.Name = "miCtxConfigure";
            this.miCtxConfigure.Size = new System.Drawing.Size(187, 22);
            this.miCtxConfigure.Text = "Configure Farm...";
            this.miCtxConfigure.Click += new System.EventHandler(this.miCtxConfigure_Click);
            // 
            // miSep2
            // 
            this.miSep2.Name = "miSep2";
            this.miSep2.Size = new System.Drawing.Size(184, 6);
            // 
            // miShowGroups
            // 
            this.miShowGroups.Name = "miShowGroups";
            this.miShowGroups.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.G)));
            this.miShowGroups.Size = new System.Drawing.Size(187, 22);
            this.miShowGroups.Text = "Show &Groups";
            this.miShowGroups.Click += new System.EventHandler(this.miShowGroups_Click);
            // 
            // miSep1
            // 
            this.miSep1.Name = "miSep1";
            this.miSep1.Size = new System.Drawing.Size(184, 6);
            // 
            // miRefresh
            // 
            this.miRefresh.Name = "miRefresh";
            this.miRefresh.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.miRefresh.Size = new System.Drawing.Size(187, 22);
            this.miRefresh.Text = "&Refresh";
            this.miRefresh.Click += new System.EventHandler(this.miRefresh_Click);
            // 
            // imageList
            // 
            this.imageList.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList.TransparentColor = System.Drawing.Color.Fuchsia;
            // 
            // uiFarmView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lvStatus);
            this.Name = "uiFarmView";
            this.Size = new System.Drawing.Size(549, 303);
            this.ctxMenuView.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lvStatus;
        private System.Windows.Forms.ColumnHeader hdrMachine;
        private System.Windows.Forms.ColumnHeader hdrStatus;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.ColumnHeader hdrImage;
        private System.Windows.Forms.ColumnHeader hdrLatency;
        private System.Windows.Forms.ColumnHeader hdrFailures;
        private System.Windows.Forms.ColumnHeader hdrFailed;
        private System.Windows.Forms.ContextMenuStrip ctxMenuView;
        private System.Windows.Forms.ToolStripMenuItem miShowGroups;
        private System.Windows.Forms.ToolStripSeparator miSep1;
        private System.Windows.Forms.ToolStripMenuItem miRefresh;
        private System.Windows.Forms.ToolStripMenuItem miMachine;
        private System.Windows.Forms.ToolStripSeparator miSep2;
        private System.Windows.Forms.ToolStripMenuItem miMachineReboot;
        private System.Windows.Forms.ToolStripMenuItem miMachineRebootCold;
        private System.Windows.Forms.ToolStripMenuItem miCtxConfigure;
        private System.Windows.Forms.ColumnHeader hdrAvailable;        
    }
}
