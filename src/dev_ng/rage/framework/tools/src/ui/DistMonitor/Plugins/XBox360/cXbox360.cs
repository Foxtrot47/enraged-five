//
// File: cXbox360.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of cXbox360.cs class
//

using System;
using System.Drawing;
using System.IO;
using System.Net;
using System.Diagnostics;
using System.Reflection;
using System.Resources;
using System.Threading;

using MachineInterface;

using RSG.Base.Logging;

namespace XBox360
{

    /// <summary>
    /// XBox 360 Class
    /// </summary>
    /// For more details on the methods, rather than duplicate documentation,
    /// see the iMachine interface.
    public class cXbox360 : iMachine, RSG.Base.Plugins.IPlugin
    {
        #region Constants
        public static readonly String NAME = "XBox 360";
        public static readonly String DESC = "Microsoft XBox 360 Machine Plugin";
        public static readonly String ARCH_NAME = "xenon";
        public static readonly String SDK_PATH = Environment.GetEnvironmentVariable("XEDK");
        public static readonly String SDK_PATH_BIN = SDK_PATH + Path.DirectorySeparatorChar + "bin" + Path.DirectorySeparatorChar + "win32";
        public static readonly String SDK_EXE_XBNETSTAT = SDK_PATH_BIN + Path.DirectorySeparatorChar + "xbnetstat";
        public static readonly String SDK_EXE_XBREBOOT = SDK_PATH_BIN + Path.DirectorySeparatorChar + "xbreboot";
        public static readonly String SDK_EXE_XBCP = SDK_PATH_BIN + Path.DirectorySeparatorChar + "xbcp";

        public static readonly int DELAY_REBOOT = 10000;
        public static readonly int DELAY_START = 60000;
        public static readonly int INTERVAL_START = 20000;
        public static readonly int DELAY_COPY = 10000;
        #endregion // Constants

        #region Events
        public event EventHandler<cMachineStatusEventArgs> MachineStatusChangeEvent;

        public event EventHandler<MachineInterface.cMachineEnabledEventArgs> MachineEnabledChangeEvent;
        #endregion // Events

        #region Private Locking Data
        /// <summary>
        /// XBox 360's from the same root directory cannot be started 
        /// simultaneously as it causes issues with the game core cache.dat
        /// file.  This locking variable is used to ensure only a single
        /// XBox starts up at any one time.
        /// </summary>
        private static object StartupLock = new Object();
        #endregion

        #region Properties and Associated Member Data
        public String Name
        {
            get { return NAME; }
        }

        public Version Version
        {
            get
            {
                Assembly assembly = Assembly.GetExecutingAssembly();
                return (assembly.GetName().Version);
            }
        }

        public String Description
        {
            get { return DESC; }
        }

        public String ArchName
        {
            get { return ARCH_NAME; }
        }

        public IPAddress IP
        {
            get { return m_IP; }
        }
        private IPAddress m_IP;

        public String HostName
        {
            get 
            {
                if (null != IP)
                {
                    // IPHostEntry host = System.Net.Dns.GetHostEntry(IP); DW - this fails now under .Net4 - see http://msdn.microsoft.com/en-us/library/ms143998.aspx and search for "don't use this function" - nice one Microsoft! 
                    IPHostEntry host = System.Net.Dns.Resolve(IP.ToString());                    
                    return (host.HostName);
                }
                return (String.Empty);
            }
        }

        public bool Enabled
        {
            get { return m_bEnabled; }
            set 
            { 
                m_bEnabled = value; 
                RaiseMachineEnabledChangeEvent(this, new cMachineEnabledEventArgs(this, this.m_bEnabled)); 
            }
        }
        private bool m_bEnabled;

        public bool Available
        {
            get { return m_bAvailable; }
            set
            {
                m_bAvailable = value;
                RaiseMachineEnabledChangeEvent(this, new cMachineEnabledEventArgs(this, this.m_bAvailable));
            }
        }
        private bool m_bAvailable;

        public Int32 Failures
        {
            get { return m_Failures; }
            set { m_Failures = value; }
        }
        private Int32 m_Failures = 0;

        public bool Failed
        {
            get { return m_Failed; }
            set { m_Failed = value; }
        }
        private bool m_Failed = false;

        public bool ForceDisconnectOnConnect
        {
            get { return m_bForceDisconnectOnConnect; }
            set { m_bForceDisconnectOnConnect = value; }
        }
        private bool m_bForceDisconnectOnConnect = false;

        public Bitmap Bitmap
        {
            get 
            {
                Assembly a = Assembly.GetExecutingAssembly();
                Stream imgStream = a.GetManifestResourceStream("XBox360.Data.xbox360.bmp");
                return (Bitmap.FromStream(imgStream) as Bitmap);
            }
        }

        public iDebugLog DebugLog
        {
            get { return m_DebugLog; }
            private set { m_DebugLog = value; }
        }
        private iDebugLog m_DebugLog;

        public MachineInterface.etMachineStatus Status
        {
            get { return m_eStatus; }
            private set { m_eStatus = value; }
        }
        private MachineInterface.etMachineStatus m_eStatus;
        #endregion

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public cXbox360()
        {
        }

        /// <summary>
        /// Constructor, specifying an IP Address
        /// </summary>
        /// <param name="ip"></param>
        public cXbox360(IPAddress ip)
        {
            Initialise(ip);
        }

        /// <summary>
        /// Constructor, specifying an IP Address and initial enabled state
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="enabled"></param>
        public cXbox360(IPAddress ip, bool enabled)
        {
            Initialise(ip, enabled);
        }
        #endregion

        #region Controller Methods
        #region Initialisation
        public void Initialise(IPAddress ip)
        {
            m_IP = ip;
            Enabled = true;
            Available = true;
            if (m_IP != null)
            {
                DebugLog = new cXbDebugLog();
                DebugLog.MachineStatusChangeEvent += OnDebugLogStateChange;
                DebugLog.ConnectTo(this);
            }
        }

        public void Initialise(IPAddress ip, bool enabled)
        {
            Initialise(ip);
            Enabled = enabled;
        }
        #endregion // Initialisation

        #region Game Start

        // sorry no default parameters in c# hence these overloads. ( might be a better way of doing this though? )
        public void Start(GameInterface.iGame game,
                          GameInterface.cConfiguration config,
                          String sArgs)
        {
            Start(game, config, sArgs, INTERVAL_START);
        }

        public void Start(GameInterface.iGame game,
                          GameInterface.cConfiguration config)
        {
            Start(game, config, "", INTERVAL_START);
        }

        public void Start(GameInterface.iGame game, 
                          GameInterface.cConfiguration config,
                          int delay)
        {
            Start(game, config, "", delay);
        }

        public void Start(GameInterface.iGame game, 
                          GameInterface.cConfiguration config,
                          String sArgs, 
                          int delay)
        {
            lock (StartupLock)
            {
                Log.Log__Warning(String.Format("{0} starting game {1}.", this.IP.ToString(), game.Name));
                CopyGame(game, config);
                StartGame(game, config, sArgs);

                Thread.Sleep(delay);
            }
        }

        #endregion // Game Start

        #region Reboot/connect/disconnect
        public void Connect()
        {
        }

        public void Disconnect()
        {
            m_DebugLog.Disconnect();
            Thread.Sleep(2000); // since the thred loop for the debuglog has it's own sleep.
        }
        
        public void Reboot()
        {
            Reboot(MachineInterface.etRebootType.Warm);
        }

        public void Reboot(MachineInterface.etRebootType type)
        {
            Log.Log__Warning(String.Format("{0} {1} rebooting.", this.IP.ToString(), type.ToString()));
                        
            String sCmd = SDK_EXE_XBREBOOT;
            String sArguments = String.Format("/X:{0}", IP.ToString());
            switch (type)
            {
                case MachineInterface.etRebootType.Cold:
                    sArguments += " /C";
                    break;
            }

            cMachineUtils.ExecuteWaitAndLogResult(sCmd, sArguments, DELAY_REBOOT / 1000);
        }
        #endregion

        #region Status
        public long Ping()
        {
            long ms_Latency = (MachineInterface.cMachineUtils.PingIP(this.IP));

            Log.Log__Debug(String.Format("Ping response from {0}: {1} ms.", IP.ToString(), ms_Latency));
            return (ms_Latency);
        }

        public void AddRunTimeParams( ref String sArgs)
        {
            
        }
        #endregion // Status
        #endregion // Controller Methods

        #region Object Overridden Methods
        /// <summary>
        /// Convert object to String representation
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return (String.Format("{0}:{1}", IP, NAME));
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Raise Machine Status Change Event (if we have listeners)
        /// </summary>
        private void RaiseMachineStatusChangeEvent(Object sender, cMachineStatusEventArgs e)
        {
            if (null != MachineStatusChangeEvent)
                MachineStatusChangeEvent(sender, e);
        }

        /// <summary>
        /// Raise Machine Enabled Change Event (if we have listeners)
        /// </summary>
        private void RaiseMachineEnabledChangeEvent(Object sender, cMachineEnabledEventArgs e)
        {
            if (null != MachineEnabledChangeEvent)
                MachineEnabledChangeEvent(sender, e);
        }

        /// <summary>
        /// Copy game to destination machine (used in Start / Resume methods)
        /// </summary>
        /// <param name="game">Game interface to copy</param>
        private void CopyGame(GameInterface.iGame game, GameInterface.cConfiguration config)
        {
            String sCmd = SDK_EXE_XBCP;
            String sRFSArguments = String.Format("/X:{0} /F /T {1}\\rfs.dat {2}\\", 
                                                 IP.ToString(), Environment.GetEnvironmentVariable("TEMP"), 
                                                 config.MachineRootDirectory);
            String sMapArguments = String.Format("/X:{0} /F /T {1}\\{2} {3}\\",
                                                 IP.ToString(), game.RootDirectory, 
                                                 config.Executable.Replace(".xex", ".map"),
                                                 config.MachineRootDirectory);
            String sCmpArguments = String.Format("/X:{0} /F /T {1}\\{2} {3}\\",
                                                            IP.ToString(), game.RootDirectory,
                                                            config.Executable.Replace(".xex", ".cmp"),
                                                            config.MachineRootDirectory);
            String sExeArguments = String.Format("/X:{0} /F /T {1}\\{2} {3}\\",
                                                 IP.ToString(), game.RootDirectory, 
                                                 config.Executable, 
                                                 config.MachineRootDirectory);


            cMachineUtils.ExecuteWaitAndLogResult(sCmd, sRFSArguments, DELAY_COPY / 1000);
            cMachineUtils.ExecuteWaitAndLogResult(sCmd, sMapArguments, DELAY_COPY / 1000);
            cMachineUtils.ExecuteWaitAndLogResult(sCmd, sCmpArguments, DELAY_COPY / 1000);
            cMachineUtils.ExecuteWaitAndLogResult(sCmd, sExeArguments, DELAY_COPY / 1000);
        }

        /// <summary>
        /// Start game on destination machine
        /// </summary>
        /// <param name="game">Game to start</param>
        /// <param name="config">Configuration to start</param>
        private void StartGame(GameInterface.iGame game, 
                               GameInterface.cConfiguration config,
                               String sArgs)
        {
            String sCmd = SDK_EXE_XBREBOOT;
            String sExeArguments;
            
            sExeArguments = String.Format("/X:{0} {1}\\{2} -rootdir={3} {4} {5}",
                                          this.IP.ToString(),
                                          config.MachineRootDirectory, config.Executable,
                                          game.RootDirectory, config.Arguments, sArgs);


            Console.WriteLine(sExeArguments);
            cMachineUtils.ExecuteWaitAndLogResult(sCmd, sExeArguments, 0);
        }
        #endregion

        #region Event Handlers
        /// <summary>
        /// Event handler for when cXbDebugLog raises MachineStatusChangeEvent
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnDebugLogStateChange(Object sender, cMachineStatusEventArgs e)
        {
            OnMonitorMachineStateChange(sender, e);
        }

        /// <summary>
        /// Event handler for when cMachineMonitor raises MachineStatusChangeEvent
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnMonitorMachineStateChange(Object sender, cMachineStatusEventArgs e)
        {
            if (this.Status != e.Status)
            {
                Status = e.Status;

                RaiseMachineStatusChangeEvent(sender, e);
            }
        }

        /// <summary>
        /// Event handler for when cMachineFarm needs to change Machine's Enabled state
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnMachineEnabledChange(Object sender, MachineInterface.cMachineEnabledEventArgs e)
        {
            if (this.Enabled != e.Enabled)
            {
                this.Enabled = e.Enabled;

                if (null != MachineEnabledChangeEvent)
                    MachineEnabledChangeEvent(this, e);
            }
        }
        #endregion
    }

} // End of DistMonitor.Machine namespace

// End of file
