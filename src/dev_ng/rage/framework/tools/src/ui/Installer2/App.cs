﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Installer.Resources;
using RSG.Base.Configuration;
using RSG.Base.Configuration.Installer;
using RSG.Editor.Controls;

namespace Installer
{

    /// <summary>
    /// Installer application class.
    /// </summary>
    public class App : RsApplication
    {
        #region Constants
        private static readonly String APPNAME = "Installer";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Default theme for user without running tools before.
        /// </summary>
        protected override ApplicationTheme DefaultTheme
        {
            get { return (ApplicationTheme.Light); }
        }

        /// <summary>
        /// Gets the unique name for this application that is used for mutex creation, and
        /// folder organisation inside the local app data folder and the registry.
        /// </summary>
        public override String UniqueName 
        {
            get { return (APPNAME); } 
        }

        /// <summary>
        /// Gets a semi-colon separate list of the application authors email addresses. These
        /// are the addresses which are used by the unhandled exception dialog to determine
        /// where the mail should be sent by default.
        /// e.g.
        /// *tools@rockstarnorth.com
        /// michael.taschler@rockstarnorth.com;dave.evans@rockstarnorth.com
        /// </summary>
        public override string AuthorEmailAddress
        {
            get { return "*tools@rockstarnorth.com"; }
        }
        #endregion Properties// Properties

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="App"/> class.
        /// </summary>
        public App()
        {
        }
        #endregion // Constructors

        #region Controller Methods
        /// <summary>
        /// Main entry point into the application. The first thing to get called.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            App app = new App();
            app.ShutdownMode = ShutdownMode.OnLastWindowClose;
            RsApplication.OnEntry(app, ApplicationMode.Single);
        }
        #endregion // Controller Methods

        #region RsApplication Overridden Methods
        /// <summary>
        /// Override to create the main window for the application.
        /// </summary>
        /// <returns>
        /// The System.Windows.Window that will be the main window for this application.
        /// </returns>
        protected override Window CreateMainWindow()
        {
            return new MainWindow();
        }

        /// <summary>
        /// Called immediately after the main windows Show method has been called.
        /// </summary>
        /// <param name="mainWindow">
        /// A reference to the main window that was shown.
        /// </param>
        protected async override Task OnMainWindowShown(Window mainWindow)
        {
            IWritableConfig config = null;
            await Task.Run(() =>
            {
                config = ConfigFactory.CreateWritableConfig();
            });
            mainWindow.DataContext = new ViewModel.InstallerViewModel(this, config);
        }

        /// <summary>
        /// Makes sure that the <see cref="HandleWindowsClosing"/> method is called if the
        /// main window is closing.
        /// </summary>
        /// <param name="windows">
        /// The windows that are to be tested.
        /// </param>
        /// <returns>
        /// True if the specified windows can be closed without user interaction; otherwise,
        /// false.
        /// </returns>
        public override bool CanCloseWithoutInteraction(ISet<Window> windows)
        {
            // Always need to handle the main window closing.
            if (windows.Contains(this.MainWindow))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Override to handle the command line arguments passed into the application from an
        /// external source after start-up.
        /// </summary>
        /// <param name="args">
        /// A list of command line arguments.
        /// </param>
        /// <returns>
        /// True if the command line arguments have been handled correctly; otherwise, false.
        /// </returns>
        protected override bool HandleExternalArgumentsCore(string[] args)
        {
            if (args.Length < 1)
            {
                return false;
            }

            string otherApplicationPath = Path.GetFullPath(args[0]);
            string thisApplicationPath = Path.GetFullPath(Environment.GetCommandLineArgs().First());
            if (!String.Equals(otherApplicationPath, thisApplicationPath, StringComparison.InvariantCultureIgnoreCase))
            {
                string message = String.Format(
                    "You've just tried to launch the installer from '{0}' while running the installer from '{1}'.\n\n" +
                    "This is not supported an you should consider closing the installer to make sure you're running the correct one.",
                    otherApplicationPath,
                    thisApplicationPath);
                RsMessageBox.Show(message, null, MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return true;
        }

        /// <summary>
        /// Handles the specified windows being closed. This is called after the method
        /// <see cref="CanCloseWithoutInteraction"/> returns false.
        /// </summary>
        /// <param name="windows">
        /// The windows to handle.
        /// </param>
        /// <param name="e">
        /// A instance of CancelEventArgs that can be used to cancel the closing of the
        /// specified windows.
        /// </param>
        public override void HandleWindowsClosing(ISet<Window> windows, CancelEventArgs e)
        {
            if (e.Cancel)
            {
                return;
            }

            MainWindow mainWindow = this.MainWindow as MainWindow;
            if (mainWindow == null)
            {
                Debug.Assert(mainWindow != null, "Incorrect cast of main window.");
                return;
            }

            if (mainWindow.IsFinishing)
            {
                e.Cancel = false;
                return;
            }
            
            MessageBoxResult result =
                RsMessageBox.Show(
                StringTable.InstallerCancelConfirm,
                this.ProductName,
                MessageBoxButton.YesNo,
                MessageBoxImage.Question,
                MessageBoxResult.No);

            if (result == MessageBoxResult.No)
            {
                e.Cancel = true;
                return;
            }
            
            Environment.ExitCode = 2;
        }
        #endregion // RsApplication Overridden Methods
    }

} // Installer namespace
