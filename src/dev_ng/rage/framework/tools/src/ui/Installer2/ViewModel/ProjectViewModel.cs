﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Editor;
using RSG.Platform;

namespace Installer.ViewModel
{

    /// <summary>
    /// Project view-model.
    /// </summary>
    internal class ProjectViewModel :
        Installer.ViewModel.ViewModelBase
    {
        #region Properties
        /// <summary>
        /// Friendly-name.
        /// </summary>
        public String FriendlyName
        {
            get { return (this.m_sFriendlyName); }
            private set
            {
                if (this.m_sFriendlyName != value)
                {
                    this.m_sFriendlyName = value;
                    NotifyPropertyChanged("FriendlyName");
                }
            }
        }
        private String m_sFriendlyName;

        /// <summary>
        /// Project key.
        /// </summary>
        public String Key
        {
            get { return (this.m_sKey); }
            private set
            {
                if (this.m_sKey != value)
                {
                    this.m_sKey = value;
                    NotifyPropertyChanged("Key");
                }
            }
        }
        private String m_sKey;

        /// <summary>
        /// Project branches.
        /// </summary>
        public ObservableCollection<BranchViewModel> Branches
        {
            get { return (this.m_Branches); }
            private set
            {
                if (this.m_Branches != value)
                {
                    this.m_Branches = value;
                    NotifyPropertyChanged("Branches");
                }
            }
        }
        private ObservableCollection<BranchViewModel> m_Branches;
        
        /// <summary>
        /// Project branch/target headers; for HeaderedItemsControl.
        /// </summary>
        public ObservableCollection<TargetHeaderViewModel> TargetHeaders
        {
            get { return (this.m_TargetHeaders); }
            private set
            {
                if (this.m_TargetHeaders != value)
                {
                    this.m_TargetHeaders = value;
                    NotifyPropertyChanged("TargetHeaders");
                }
            }
        }
        private ObservableCollection<TargetHeaderViewModel> m_TargetHeaders;

        /// <summary>
        /// Core project.
        /// </summary>
        public IProject CoreProject
        {
            get { return (this.m_CoreProject); }
            internal set
            {
                if (this.m_CoreProject != value)
                {
                    this.m_CoreProject = value;
                    NotifyPropertyChanged("CoreProject");
                }
            }
        }
        private IProject m_CoreProject;

        /// <summary>
        /// Default branch (user-selected).
        /// </summary>
        public BranchViewModel DefaultBranch
        {
            get { return (this.m_sDefaultBranch); }
            internal set
            {
                if (this.m_sDefaultBranch != value)
                {
                    this.m_sDefaultBranch = value;
                    NotifyPropertyChanged("DefaultBranch");
                }
            }
        }
        private BranchViewModel m_sDefaultBranch;
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// Constructor; from IProject (core-only).
        /// </summary>
        /// <param name="coreProject"></param>
        public ProjectViewModel(IProject coreProject)
        {
            Debug.Assert(!coreProject.IsDLC, "Unexpected DLC project!");
            this.CoreProject = coreProject;
            this.FriendlyName = coreProject.FriendlyName;
            this.Key = coreProject.Name;

            IEnumerable<RSG.Platform.Platform> allProjectPlatforms = this.CoreProject.Branches.
                SelectMany(b => b.Value.Targets).Select(t => t.Value.Platform).Distinct();
            List<RSG.Platform.Platform> sortedAllProjectPlatforms = allProjectPlatforms.ToList();
            sortedAllProjectPlatforms.Sort((x, y) => x.PlatformToFriendlyName().CompareTo(y.PlatformToFriendlyName()));

            List<String> sortedBranchNames = coreProject.Branches.Select(b => b.Key).ToList();
            sortedBranchNames.Sort((x, y) => x.CompareTo(y));
            this.Branches = new ObservableCollection<BranchViewModel>();
            this.Branches.AddRange(sortedBranchNames.Select(b => new BranchViewModel(this, b, allProjectPlatforms)));
            if (null != coreProject.DefaultBranch)
                this.DefaultBranch = this.Branches.Where(b => b.Key.Equals(coreProject.DefaultBranch.Name)).FirstOrDefault();
            else
                this.DefaultBranch = this.Branches.FirstOrDefault();

            List<TargetHeaderViewModel> headers = new List<TargetHeaderViewModel>();
            List<TargetViewModel> distinct = new List<TargetViewModel>();
            foreach (var branch in this.Branches)
            {
                foreach (var target in branch.Targets)
                {
                    bool found = false;
                    foreach (var current in distinct)
                    {
                        if (current.Name == target.Name)
                        {
                            found = true;
                            break;
                        }
                    }

                    if (!found)
                    {
                        distinct.Add(target);
                    }
                }
            }

            foreach (var t in distinct)
            {
                headers.Add(new TargetHeaderViewModel(t.Name, this.Branches, t.Platform, this));
            }

            this.TargetHeaders = new ObservableCollection<TargetHeaderViewModel>(headers);
        }
        #endregion // Constructor(s)

        #region Object Overridden Methods
        /// <summary>
        /// Return friendly name for display.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return (this.FriendlyName);
        }
        #endregion // Object Overidden Methods

        #region Controller Methods
        /// <summary>
        /// Copy settings from View-Model to writable-Config object.
        /// </summary>
        /// <param name="config"></param>
        public void SetupConfig(IConfig config)
        {
            foreach (BranchViewModel branchViewModel in this.Branches)
            {
                Debug.Assert(config.CoreProject.Branches.ContainsKey(branchViewModel.Key),
                    String.Format("Core project does not contain branch: '{0}'.", branchViewModel.Key));
                if (!config.CoreProject.Branches.ContainsKey(branchViewModel.Key))
                {
                    InstallerViewModel.Log.ToolError("Core project does not contain branch: '{0}'.", branchViewModel.Key);
                    continue; // Skip branch.
                }

                IBranch branch = config.CoreProject.Branches[branchViewModel.Key];
                foreach (TargetViewModel targetViewModel in branchViewModel.Targets)
                {
                    if (!(branch.Targets.ContainsKey(targetViewModel.Platform)))
                        continue; // Skip; perfectly normal to not have some platforms for some branches.
                    
                    ITarget target = branch.Targets[targetViewModel.Platform];
                    target.Enabled = targetViewModel.IsEnabled;
                }
            }
        }
        #endregion // Controller Methods
    }

} // Installer.ViewModel namespace
