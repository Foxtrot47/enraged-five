﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Configuration;
using RSG.Platform;

namespace Installer.ViewModel
{

    /// <summary>
    /// Branch view-model.
    /// </summary>
    internal class BranchViewModel : ViewModelBase
    {
        #region Properties
        /// <summary>
        /// Project key.
        /// </summary>
        public String Key
        {
            get { return (this.m_sKey); }
            private set
            {
                if (this.m_sKey != value)
                {
                    this.m_sKey = value;
                    NotifyPropertyChanged("Key");
                }
            }
        }
        private String m_sKey;

        /// <summary>
        /// Project target enabled states.
        /// </summary>
        public ObservableCollection<TargetViewModel> Targets
        {
            get { return (this.m_Targets); }
            private set
            {
                if (this.m_Targets != value)
                {
                    this.m_Targets = value;
                    NotifyPropertyChanged("Targets");
                }
            }
        }
        private ObservableCollection<TargetViewModel> m_Targets;

        public ProjectViewModel ProjectViewModel
        {
            get { return this.m_Parent; }
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Parent project.
        /// </summary>
        private ProjectViewModel m_Parent;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="branchName"></param>
        /// <param name="allProjectPlatforms"></param>
        public BranchViewModel(ProjectViewModel parent, String branchName, 
            IEnumerable<RSG.Platform.Platform> allProjectPlatforms)
        {
            Debug.Assert(parent.CoreProject.Branches.ContainsKey(branchName),
                String.Format("Invalid branch name key '{0}' for project {1}.", branchName, parent.CoreProject.Name));
            if (!parent.CoreProject.Branches.ContainsKey(branchName))
                throw (new ArgumentException(String.Format("Invalid branch name key '{0}' for project {1}.", branchName, this.m_Parent.CoreProject.Name), "branchName"));

            this.m_Parent = parent;
            this.Key = branchName;
            IBranch branch = this.m_Parent.CoreProject.Branches[branchName];

            this.Targets = new ObservableCollection<TargetViewModel>();
            foreach (RSG.Platform.Platform platform in allProjectPlatforms)
            {
                if (branch.Targets.ContainsKey(platform))
                {
                    ITarget target = branch.Targets[platform];
                    this.Targets.Add(new TargetViewModel(this, target));
                }
                else
                {
                    this.Targets.Add(new TargetViewModel(this, platform));
                }
            }
        }
        #endregion // Constructor(s)

        #region Object Overridden Methods
        /// <summary>
        /// Return friendly name for display.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return (this.Key);
        }
        #endregion // Object Overidden Methods
    }

} // Installer.ViewModel namespace
