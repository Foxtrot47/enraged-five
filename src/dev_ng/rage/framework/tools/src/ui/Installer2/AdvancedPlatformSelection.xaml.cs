﻿

namespace Installer
{
    using System.Windows;
    using System.Windows.Controls;
    using RSG.Editor.Controls;

    /// <summary>
    /// Interaction logic for AdvancedPlatformSelection.xaml
    /// </summary>
    public partial class AdvancedPlatformSelection : RsWindow
    {
        public AdvancedPlatformSelection()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
