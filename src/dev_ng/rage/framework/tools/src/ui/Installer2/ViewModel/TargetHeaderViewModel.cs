﻿using System.Collections.Generic;
using System;
using System.Linq;
using RSG.Platform;

namespace Installer.ViewModel
{

    /// <summary>
    /// Target header view-model.
    /// </summary>
    internal class TargetHeaderViewModel : ViewModelBase
    {
        #region Fields
        /// <summary>
        /// 
        /// </summary>
        private IEnumerable<BranchViewModel> _branches;

        /// <summary>
        /// 
        /// </summary>
        private bool? _isChecked;

        /// <summary>
        /// 
        /// </summary>
        private string _name;

        /// <summary>
        /// 
        /// </summary>
        private bool _updatingCheckedValue;

        /// <summary>
        /// 
        /// </summary>
        private ProjectViewModel _project;

        /// <summary>
        /// 
        /// </summary>
        private Platform _platform;
        #endregion Fields

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get { return (this._name); }
            set { this.SetProperty(ref this._name, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool? IsChecked
        {
            get
            {
                return this._isChecked;
            }

            set
            {
                if (this.SetProperty(ref this._isChecked, value))
                {
                    this._project.CoreProject.DefaultPlatforms[this._platform] = value ?? true;

                    if (value == null || this._updatingCheckedValue)
                    {
                        return;
                    }

                    foreach (BranchViewModel branchVm in this._branches)
                    {
                        foreach (TargetViewModel targetVm in branchVm.Targets)
                        {
                            if (!String.Equals(targetVm.Name, this._name))
                            {
                                continue;
                            }

                            targetVm.IsEnabled = (bool)value;
                            break;
                        }
                    }
                }
            }
        }
        #endregion Properties

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="branches"></param>
        public TargetHeaderViewModel(string name, IEnumerable<BranchViewModel> branches, Platform platform, ProjectViewModel project)
            : base()
        {
            this._name = name;
            this._branches = branches;
            this._project = project;
            this._platform = platform;
            List<bool> states = new List<bool>();
            foreach (BranchViewModel branchVm in branches)
            {
                foreach (TargetViewModel targetVm in branchVm.Targets)
                {
                    if (!String.Equals(targetVm.Name, name))
                    {
                        continue;
                    }

                    if (targetVm.IsReadOnly)
                    {
                        continue;
                    }

                    states.Add(targetVm.IsEnabled);
                }
            }

            if (this.AllEqual(states, true))
            {
                this._isChecked = true;
                this._project.CoreProject.DefaultPlatforms[this._platform] = true;
            }
            else if (this.AllEqual(states, false))
            {
                this._isChecked = false;
                this._project.CoreProject.DefaultPlatforms[this._platform] = false;
            }
            else
            {
                this._isChecked = null;
                this._project.CoreProject.DefaultPlatforms[this._platform] = true;
            }

        }
        #endregion Constructors

        #region Methods
        public void UpdateCheckedValue()
        {
            this._updatingCheckedValue = true;
            List<bool> states = new List<bool>();
            foreach (BranchViewModel branchVm in this._branches)
            {
                foreach (TargetViewModel targetVm in branchVm.Targets)
                {
                    if (!String.Equals(targetVm.Name, this._name))
                    {
                        continue;
                    }

                    if (targetVm.IsReadOnly)
                    {
                        continue;
                    }

                    states.Add(targetVm.IsEnabled);
                }
            }

            if (this.AllEqual(states, true))
            {
                this.IsChecked = true;
            }
            else if (this.AllEqual(states, false))
            {
                this.IsChecked = false;
            }
            else
            {
                this.IsChecked = null;
            }

            this._updatingCheckedValue = false;
        }

        private bool AllEqual(List<bool> items, bool value)
        {
            if (!items.Any())
            {
                return false;
            }

            bool firstProp = items.First();
            if (firstProp != value)
            {
                return false;
            }

            return !items.Any(x => x != firstProp);
        }
        #endregion Methods
    }
} // Installer.ViewModel namespace
