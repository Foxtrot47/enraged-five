﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Installer;

namespace Installer.ViewModel
{

    /// <summary>
    /// DCC ToolSet view-model.
    /// </summary>
    internal class DCCToolSetViewModel : ViewModelBase
    {
        #region Properties
        /// <summary>
        /// Tool set enum value.
        /// </summary>
        public DCCToolSet ToolSet
        {
            get { return (this.m_eToolSet); }
            private set
            {
                if (this.m_eToolSet != value)
                {
                    this.m_eToolSet = value;
                    NotifyPropertyChanged("ToolSet");
                }
            }
        }
        private DCCToolSet m_eToolSet;

        /// <summary>
        /// Friendly name (for UI display).
        /// </summary>
        public String FriendlyName
        {
            get { return (this.m_sFriendlyName); }
            private set
            {
                if (this.m_sFriendlyName != value)
                {
                    this.m_sFriendlyName = value;
                    NotifyPropertyChanged("FriendlyName");
                }
            }
        }
        private String m_sFriendlyName;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="toolSet"></param>
        public DCCToolSetViewModel(DCCToolSet toolSet)
        {
            this.ToolSet = toolSet;
            this.FriendlyName = toolSet.GetDisplayName();
        }
        #endregion // Constructor(s)
    }

} // Installer.ViewModel namespace
