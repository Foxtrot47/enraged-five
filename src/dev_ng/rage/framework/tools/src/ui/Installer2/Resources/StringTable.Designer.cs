﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Installer.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class StringTable {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal StringTable() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Installer.Resources.StringTable", typeof(StringTable).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Are you sure you want to cancel this install?
        ///
        ///No environment variables or settings changes will be saved..
        /// </summary>
        public static string InstallerCancelConfirm {
            get {
                return ResourceManager.GetString("InstallerCancelConfirm", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rockstar Games Tools Installer failed.
        ///
        ///Contact your local tools support group..
        /// </summary>
        public static string InstallerFailed {
            get {
                return ResourceManager.GetString("InstallerFailed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to https://devstar.rockstargames.com/wiki/index.php/Tools_Installer.
        /// </summary>
        public static string InstallerHelp {
            get {
                return ResourceManager.GetString("InstallerHelp", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Branch:.
        /// </summary>
        public static string InstallerProject_Branch {
            get {
                return ResourceManager.GetString("InstallerProject_Branch", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Default Branch:.
        /// </summary>
        public static string InstallerProject_DefaultBranch {
            get {
                return ResourceManager.GetString("InstallerProject_DefaultBranch", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to For each available project configure which platforms you are using.
        ///
        ///Platforms must be configured for each branch separately; some branches only support a restricted set of platforms..
        /// </summary>
        public static string InstallerProject_Instructions {
            get {
                return ResourceManager.GetString("InstallerProject_Instructions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Platforms:.
        /// </summary>
        public static string InstallerProject_Platforms {
            get {
                return ResourceManager.GetString("InstallerProject_Platforms", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Project:.
        /// </summary>
        public static string InstallerProject_Project {
            get {
                return ResourceManager.GetString("InstallerProject_Project", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Projects.
        /// </summary>
        public static string InstallerProjects_Header {
            get {
                return ResourceManager.GetString("InstallerProjects_Header", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Light.
        /// </summary>
        public static string InstallerSetting_ModernThemeDefault {
            get {
                return ResourceManager.GetString("InstallerSetting_ModernThemeDefault", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to DCC Packages:.
        /// </summary>
        public static string InstallerSettings_DCC {
            get {
                return ResourceManager.GetString("InstallerSettings_DCC", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Autodesk 3dsmax Toolset:.
        /// </summary>
        public static string InstallerSettings_DCCToolSet3dsmax {
            get {
                return ResourceManager.GetString("InstallerSettings_DCCToolSet3dsmax", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Autodesk MotionBuilder Toolset:.
        /// </summary>
        public static string InstallerSettings_DCCToolSetMotionBuilder {
            get {
                return ResourceManager.GetString("InstallerSettings_DCCToolSetMotionBuilder", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Install MotionBuilder Python Scripts:.
        /// </summary>
        public static string InstallerSettings_DCCToolSetMotionBuilderScripts {
            get {
                return ResourceManager.GetString("InstallerSettings_DCCToolSetMotionBuilderScripts", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Autodesk Mudbox Toolset:.
        /// </summary>
        public static string InstallerSettings_DCCToolSetMudbox {
            get {
                return ResourceManager.GetString("InstallerSettings_DCCToolSetMudbox", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tool User Interface:.
        /// </summary>
        public static string InstallerSettings_GUI {
            get {
                return ResourceManager.GetString("InstallerSettings_GUI", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Use Windows Modern interfaces (where applicable) .
        /// </summary>
        public static string InstallerSettings_GUIModern {
            get {
                return ResourceManager.GetString("InstallerSettings_GUIModern", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Settings.
        /// </summary>
        public static string InstallerSettings_Header {
            get {
                return ResourceManager.GetString("InstallerSettings_Header", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Log Level:.
        /// </summary>
        public static string InstallerSettings_LogLevel {
            get {
                return ResourceManager.GetString("InstallerSettings_LogLevel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Log:.
        /// </summary>
        public static string InstallerSettings_LogOptions {
            get {
                return ResourceManager.GetString("InstallerSettings_LogOptions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Trace information (for debugging only).
        /// </summary>
        public static string InstallerSettings_LogTrace {
            get {
                return ResourceManager.GetString("InstallerSettings_LogTrace", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Theme:.
        /// </summary>
        public static string InstallerSettings_ModernTheme {
            get {
                return ResourceManager.GetString("InstallerSettings_ModernTheme", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Use Xoreax XGE (when installed).
        /// </summary>
        public static string InstallerSettings_XGE {
            get {
                return ResourceManager.GetString("InstallerSettings_XGE", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enabled:.
        /// </summary>
        public static string InstallerSourceControl_Enabled {
            get {
                return ResourceManager.GetString("InstallerSourceControl_Enabled", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Source Control: Perforce.
        /// </summary>
        public static string InstallerSourceControl_Header {
            get {
                return ResourceManager.GetString("InstallerSourceControl_Header", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Check your Perforce settings; the installer attempts to auto-detect them..
        /// </summary>
        public static string InstallerSourceControl_Instructions {
            get {
                return ResourceManager.GetString("InstallerSourceControl_Instructions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Override Login:.
        /// </summary>
        public static string InstallerSourceControl_Override {
            get {
                return ResourceManager.GetString("InstallerSourceControl_Override", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Password:.
        /// </summary>
        public static string InstallerSourceControl_Password {
            get {
                return ResourceManager.GetString("InstallerSourceControl_Password", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Server:.
        /// </summary>
        public static string InstallerSourceControl_Server {
            get {
                return ResourceManager.GetString("InstallerSourceControl_Server", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Username:.
        /// </summary>
        public static string InstallerSourceControl_Username {
            get {
                return ResourceManager.GetString("InstallerSourceControl_Username", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Workspace:.
        /// </summary>
        public static string InstallerSourceControl_Workspace {
            get {
                return ResourceManager.GetString("InstallerSourceControl_Workspace", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rockstar Games Tools Installer finished successfully.
        ///
        ///The installer will now close..
        /// </summary>
        public static string InstallerSuccess {
            get {
                return ResourceManager.GetString("InstallerSuccess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &gt;Rockstar Games Tools Installer finished successfully.
        ///
        ///Changes were made to your system that requires a system restart; please do this at your earliest convenience.
        ///
        ///The installer will now close..
        /// </summary>
        public static string InstallerSuccessAndRestart {
            get {
                return ResourceManager.GetString("InstallerSuccessAndRestart", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Summary.
        /// </summary>
        public static string InstallerSummary_Header {
            get {
                return ResourceManager.GetString("InstallerSummary_Header", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The installation wizard is now complete.
        ///
        ///Click Finish to make the changes to your system..
        /// </summary>
        public static string InstallerSummary_Instructions {
            get {
                return ResourceManager.GetString("InstallerSummary_Instructions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to There are Windows Explorer processes running.  
        ///
        ///These will be closed and restarted.
        ///
        ///Are you ok to continue?.
        /// </summary>
        public static string InstallerSummary_RunningProcesses {
            get {
                return ResourceManager.GetString("InstallerSummary_RunningProcesses", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You currently have no enabled platforms for the selected branch. Do you wish to continue?.
        /// </summary>
        public static string InstallerTargets_Warning {
            get {
                return ResourceManager.GetString("InstallerTargets_Warning", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Domain:.
        /// </summary>
        public static string InstallerUserAccount_ActiveDirectoryDomain {
            get {
                return ResourceManager.GetString("InstallerUserAccount_ActiveDirectoryDomain", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email Address:.
        /// </summary>
        public static string InstallerUserAccount_EmailAddress {
            get {
                return ResourceManager.GetString("InstallerUserAccount_EmailAddress", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User Account.
        /// </summary>
        public static string InstallerUserAccount_Header {
            get {
                return ResourceManager.GetString("InstallerUserAccount_Header", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter your Windows login username and password and then click Next.
        ///
        ///Your email address will be synced from Active Directory..
        /// </summary>
        public static string InstallerUserAccount_Instructions {
            get {
                return ResourceManager.GetString("InstallerUserAccount_Instructions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Login.
        /// </summary>
        public static string InstallerUserAccount_Login {
            get {
                return ResourceManager.GetString("InstallerUserAccount_Login", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Password:.
        /// </summary>
        public static string InstallerUserAccount_Password {
            get {
                return ResourceManager.GetString("InstallerUserAccount_Password", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Studio:.
        /// </summary>
        public static string InstallerUserAccount_Studio {
            get {
                return ResourceManager.GetString("InstallerUserAccount_Studio", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Username:.
        /// </summary>
        public static string InstallerUserAccount_Username {
            get {
                return ResourceManager.GetString("InstallerUserAccount_Username", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User Role:.
        /// </summary>
        public static string InstallerUserAccount_UserRole {
            get {
                return ResourceManager.GetString("InstallerUserAccount_UserRole", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Title:.
        /// </summary>
        public static string InstallerUserAccount_UserTitle {
            get {
                return ResourceManager.GetString("InstallerUserAccount_UserTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Welcome.
        /// </summary>
        public static string InstallerWelcome_Header {
            get {
                return ResourceManager.GetString("InstallerWelcome_Header", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Welcome to the Rockstar Games Tools Installer.
        ///
        ///This installer will setup the tools environment for Rockstar Games projects.
        ///
        ///Click Next to continue..
        /// </summary>
        public static string InstallerWelcome_Text {
            get {
                return ResourceManager.GetString("InstallerWelcome_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tools Installer.
        /// </summary>
        public static string InstallerWindow_Title {
            get {
                return ResourceManager.GetString("InstallerWindow_Title", resourceCulture);
            }
        }
    }
}
