﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RSG.Editor.Controls;
using RSG.Editor.Controls.Wizard;
using RSG.SourceControl.Perforce;
using Installer.ViewModel;
using RSG.Interop.Microsoft.Windows;
using RSG.Interop.Microsoft;
using Installer.Resources;

namespace Installer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : RsWindow
    {
        #region Member Data
        /// <summary>
        /// Whether the installer is finishing; false if being cancelled.
        /// </summary>
        private bool _isFinishing;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            this._isFinishing = false;
            this.Wizard.ActivePage = this.Wizard.Pages.First();
            this.Wizard.PageChanging += WizardPageChanging;
            this.Wizard.PageChanged += WizardPageChanged;
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Gets a value indicating whether the installer is currently finishing.
        /// </summary>
        internal bool IsFinishing
        {
            get { return this._isFinishing; }
        }
        #endregion Properties

        #region Wizard Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WorkspaceRefreshClicked(object sender, RoutedEventArgs e)
        {
            InstallerViewModel vm = (InstallerViewModel)this.Wizard.DataContext;
            vm.TryGetPerforceWorkspaces();
            //if (vm.OverridePerforceLogin)
            //{

            //}
            //else
            //{

            //}
        }

        /// <summary>
        /// Wizard page changing event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WizardPageChanging(Object sender, WizardPageChangingEventArgs e)
        {
            InstallerViewModel vm = (InstallerViewModel)this.Wizard.DataContext;
            if (e.FromPage.Header == Installer.Resources.StringTable.InstallerUserAccount_Header)
            {
                if (e.ToPage.Header == Installer.Resources.StringTable.InstallerSourceControl_Header)
                {
                    // When trying to change from the User Account page we enforce
                    // validating the user account settings to ensure the user cannot
                    // proceed with invalid data.
                    if (!vm.TryLogin())
                        e.Cancel = true;
                }
            }
            else if (e.FromPage.Header == Installer.Resources.StringTable.InstallerSourceControl_Header)
            {
                if (e.ToPage.Header == Installer.Resources.StringTable.InstallerProjects_Header)
                {
                    if (vm.EnabledVersionControl)
                    {
                        // When trying to change from the Perforce Source Control page we
                        // enforce validating the settings to ensure user cannot proceed
                        // with invalid data.
                        if (!vm.TryPerforceSettings())
                            e.Cancel = true;
                    }
                }
            }
        }

        /// <summary>
        /// Wizard page changed event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WizardPageChanged(Object sender, WizardPageChangedEventArgs e)
        {
            InstallerViewModel vm = (InstallerViewModel)this.Wizard.DataContext;

            // Handle Available Perforce Workspaces population.
            if (e.ToPage.Header == Installer.Resources.StringTable.InstallerSourceControl_Header)
            {
                if (String.IsNullOrWhiteSpace(vm.PerforceUsername))
                {
                    vm.PerforceUsername = vm.Username;
                }

                if (String.IsNullOrWhiteSpace(vm.PerforcePassword))
                {
                    vm.PerforcePassword = vm.Password;
                }

                vm.TryGetPerforceWorkspaces();
            }
        }

        /// <summary>
        /// Opens the advance platform selection dialog.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AdvancedPlatformSelectionClicked(object sender, RoutedEventArgs e)
        {
            AdvancedPlatformSelection window = new AdvancedPlatformSelection();
            window.DataContext = this.DataContext;
            window.Owner = this;
            window.ShowDialog();
        }
        #endregion Wizard Event Handlers// Wizard Event Handlers

        #region Wizard Command Handlers
        /// <summary>
        /// Wizard next navigation.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WizardNext(Object sender, ExecutedRoutedEventArgs e)
        {
            if (this.Wizard.ActivePage.Header == Installer.Resources.StringTable.InstallerProjects_Header)
            {
                InstallerViewModel vm = (InstallerViewModel)this.Wizard.DataContext;
                if (vm.SelectedProject != null && vm.SelectedProject.DefaultBranch != null)
                {
                    if (!vm.SelectedProject.DefaultBranch.Targets.Any(t => t.IsEnabled))
                    {
                        MessageBoxResult result = RsMessageBox.Show(StringTable.InstallerTargets_Warning, "Installer", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                        if (result == MessageBoxResult.No)
                        {
                            return;
                        }
                    }
                }
            }

            if (this.Wizard.NavigateNext())
            {
                return;
            }

            if (this.Wizard.ActivePage == null)
            {
                return;
            }

            if (this.Wizard.ActivePage.Header == Installer.Resources.StringTable.InstallerSummary_Header)
            {
                WizardCommands.Finish.Execute(null, this.Wizard);
            }
        }

        private void WizardNextCanExecute(Object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        /// <summary>
        /// Wizard previous navigation.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WizardPrevious(Object sender, ExecutedRoutedEventArgs e)
        {
            this.Wizard.NavigatePrevious();
        }

        private void WizardPreviousCanExecute(Object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        /// <summary>
        /// Wizard cancellation.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WizardCancel(Object sender, ExecutedRoutedEventArgs e)
        {
            Close();
        }

        private void WizardCancelCanExecute(Object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        /// <summary>
        /// Wizard finish; run the installation process.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WizardFinish(Object sender, ExecutedRoutedEventArgs e)
        {
            // Check for running processes that we will restart.
            bool cancel = false;

            IList<IntPtr> explorerWindows = RSG.Interop.Microsoft.WindowsExplorer.GetAllTopLevelWindows();
            if (explorerWindows.Any())
            {
                if (MessageBoxResult.No == RsMessageBox.Show(this, Installer.Resources.StringTable.InstallerSummary_RunningProcesses,
                    this.Title, MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No))
                {
                    cancel = true;
                }
            }

            if (cancel)
                return;

            WindowsExplorer.CloseAllTopLevelWindows();

            InstallerViewModel vm = (InstallerViewModel)this.Wizard.DataContext;
            this._isFinishing = true;
            
            if (vm.Install())
            {
                // If "Launch folders in a separate process" is not set then we set
                // it and prompt for the user to restart their machine.
                if (!RSG.Interop.Microsoft.WindowsExplorer.IsSeparateProcessesSet())
                {
                    InstallerViewModel.Log.Message("Windows Explorer separate process flag not set.  Setting and prompting to restart.");
                    RSG.Interop.Microsoft.WindowsExplorer.SetSeparateProcess(true);

                    RsMessageBox.Show(this, Installer.Resources.StringTable.InstallerSuccessAndRestart,
                        this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    InstallerViewModel.Log.Message("Windows Explorer separate process flag already set.");

                    RsMessageBox.Show(this, Installer.Resources.StringTable.InstallerSuccess,
                        this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            else
            {
                RsMessageBox.Show(this, Installer.Resources.StringTable.InstallerFailed,
                    this.Title, MessageBoxButton.OK, MessageBoxImage.Error);
            }

            Close();
        }

        private void WizardFinishCanExecute(Object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = (this.Wizard.ActivePageIndex != 0);
        }
        #endregion // Wizard Command Handlers
        
        #region RsWindow Overridden Methods
        /// <summary>
        /// Handle user-click of help button.
        /// </summary>
        protected override void HandleContextHelp()
        {
            String helpUrl = Installer.Resources.StringTable.InstallerHelp;
            Process.Start(helpUrl);
        }
        #endregion // RsWindow Overidden Methods
    }

} // Installer.View namespace
