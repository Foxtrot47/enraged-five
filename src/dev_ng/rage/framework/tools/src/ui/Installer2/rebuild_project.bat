@echo off
pushd %~dp0
set started=%time% 
@echo STARTED : %started%

CALL setenv.bat >NUL

CALL %RS_TOOLSROOT%\script\util\projGen\sync.bat >NUL

CALL %RS_TOOLSROOT%\script\util\projGen\generate.bat %RS_TOOLSCONFIG%\projgen\toolsUIExe.build %RAGE_DIR%/framework/tools/src/ui/Installer2/Installer.makefile

:END
@echo STARTED  : %started% 
@echo FINISHED : %time% 

@echo Press a key...
pause