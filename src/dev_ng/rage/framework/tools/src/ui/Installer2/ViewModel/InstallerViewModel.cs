﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.DirectoryServices;
using System.IO;
using System.Linq;
using Microsoft.Win32;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using RSG.Base.Configuration;
using RSG.Base.Configuration.Bugstar;
using RSG.Base.Configuration.Installer;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Editor.Controls;
using RSG.Installer;
using RSG.Interop.Autodesk3dsmax;
using RSG.Interop.AutodeskMotionBuilder;
using RSG.SourceControl.Perforce;

namespace Installer.ViewModel
{

    /// <summary>
    /// Main Installer view-model.
    /// </summary>
    internal class InstallerViewModel :
        Installer.ViewModel.ViewModelBase
    {
        #region Constants
        private static readonly String LOG_CTX = "Installer";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Username.
        /// </summary>
        public String Username
        {
            get { return (this.m_sUsername); }
            set 
            {
                if (this.m_sUsername != value)
                {
                    this.m_sUsername = value;
                    NotifyPropertyChanged("Username");
                    this.HasLoginCredentials = (!String.IsNullOrWhiteSpace(this.m_sPassword) && 
                        !String.IsNullOrWhiteSpace(this.m_sUsername) &&
                        this.m_sPassword.Length > 0 && 
                        this.m_sUsername.Length > 0);
                }
            }
        }
        private String m_sUsername;

        /// <summary>
        /// Username.
        /// </summary>
        public String PerforceUsername
        {
            get { return (this.m_sPerforceUsername); }
            set
            {
                if (this.m_sPerforceUsername != value)
                {
                    this.m_sPerforceUsername = value;
                    NotifyPropertyChanged("PerforceUsername");
                    //this.TryGetPerforceWorkspaces();
                }
            }
        }
        private String m_sPerforceUsername;

        /// <summary>
        /// Password.
        /// </summary>
        public String Password
        {
            get { return (this.m_sPassword); }
            set
            {
                if (this.m_sPassword != value)
                {
                    this.m_sPassword = value;
                    NotifyPropertyChanged("Password");
                    this.HasLoginCredentials = (!String.IsNullOrWhiteSpace(this.m_sPassword) &&
                        !String.IsNullOrWhiteSpace(this.m_sUsername) &&
                        this.m_sPassword.Length > 0 &&
                        this.m_sUsername.Length > 0);
                }
            }
        }
        private String m_sPassword;

        /// <summary>
        /// Password.
        /// </summary>
        public String PerforcePassword
        {
            get { return (this.m_sPerforcePassword); }
            set
            {
                if (this.m_sPerforcePassword != value)
                {
                    this.m_sPerforcePassword = value;
                    NotifyPropertyChanged("PerforcePassword");
                    //this.TryGetPerforceWorkspaces();
                }
            }
        }
        private String m_sPerforcePassword;

        /// <summary>
        /// Whether the user has entered username and password (for Login button).
        /// </summary>
        public bool HasLoginCredentials
        {
            get { return (this.m_bHasLoginCredentials); }
            set
            {
                if (this.m_bHasLoginCredentials != value)
                {
                    this.m_bHasLoginCredentials = value;
                    NotifyPropertyChanged("HasLoginCredentials");
                }
            }
        }
        private bool m_bHasLoginCredentials;

        /// <summary>
        /// User has valid login with Bugstar.
        /// </summary>
        public bool ValidLogin
        {
            get { return (this.m_bValidLogin); }
            set
            {
                if (this.m_bValidLogin != value)
                {
                    this.m_bValidLogin = value;
                    NotifyPropertyChanged("ValidLogin");
                }
            }
        }
        private bool m_bValidLogin;

        /// <summary>
        /// User has valid Perforce settings.
        /// </summary>
        public bool ValidPerforceLogin
        {
            get { return (this.m_bValidPerforceLogin); }
            set
            {
                if (this.m_bValidPerforceLogin != value)
                {
                    this.m_bValidPerforceLogin = value;
                    NotifyPropertyChanged("ValidPerforceLogin");
                }
            }
        }
        private bool m_bValidPerforceLogin;
        
        /// <summary>
        /// User email address.
        /// </summary>
        public String UserEmailAddress
        {
            get { return (this.m_sUserEmailAddress); }
            set
            {
                if (this.m_sUserEmailAddress != value)
                {
                    this.m_sUserEmailAddress = value;
                    NotifyPropertyChanged("UserEmailAddress");
                }
            }
        }
        private String m_sUserEmailAddress;

        /// <summary>
        /// User studio friendly name.
        /// </summary>
        public String UserStudio
        {
            get { return (this.m_sUserStudio); }
            set
            {
                if (this.m_sUserStudio != value)
                {
                    this.m_sUserStudio = value;
                    NotifyPropertyChanged("UserStudio");
                }
            }
        }
        private String m_sUserStudio;

        /// <summary>
        /// User role selection.
        /// </summary>
        public IUsertype UserRole
        {
            get { return (this.m_sUserRole); }
            set
            {
                if (this.m_sUserRole != value)
                {
                    this.m_sUserRole = value;
                    NotifyPropertyChanged("UserRole");
                }
            }
        }
        private IUsertype m_sUserRole;

        /// <summary>
        /// Available user roles.
        /// </summary>
        public ObservableCollection<IUsertype> AvailableUserRoles
        {
            get { return (this.m_AvailableUserRoles); }
            set
            {
                if (this.m_AvailableUserRoles != value)
                {
                    this.m_AvailableUserRoles = value;
                    NotifyPropertyChanged("AvailableUserRoles");
                }
            }
        }
        private ObservableCollection<IUsertype> m_AvailableUserRoles;

        /// <summary>
        /// Whether Perforce integration is enabled.
        /// </summary>
        public bool EnabledVersionControl
        {
            get { return (this.m_bEnableVersionControl); }
            set
            {
                if (this.m_bEnableVersionControl != value)
                {
                    this.m_bEnableVersionControl = value;
                    if (!this.m_bEnableVersionControl)
                        this.ValidPerforceLogin = true;
                    NotifyPropertyChanged("EnabledVersionControl");
                }
            }
        }
        private bool m_bEnableVersionControl;

        /// <summary>
        /// Whether Perforce integration is enabled.
        /// </summary>
        public bool OverridePerforceLogin
        {
            get { return (this.m_bOverridePerforceLogin); }
            set
            {
                if (this.m_bOverridePerforceLogin != value)
                {
                    this.m_bOverridePerforceLogin = value;
                    NotifyPropertyChanged("OverridePerforceLogin");
                }
            }
        }
        private bool m_bOverridePerforceLogin;

        /// <summary>
        /// Perforce server.
        /// </summary>
        public String PerforceServer
        {
            get { return (this.m_sPerforceServer); }
            set
            {
                if (this.m_sPerforceServer != value)
                {
                    this.m_sPerforceServer = value;
                    NotifyPropertyChanged("PerforceServer");
                }
            }
        }
        private String m_sPerforceServer;

        /// <summary>
        /// Perforce workspaces that are valid for this host.
        /// </summary>
        public ObservableCollection<String> AvailablePerforceWorkspaces
        {
            get { return (this.m_AvailablePerforceWorkspaces); }
            set
            {
                if (this.m_AvailablePerforceWorkspaces != value)
                {
                    this.m_AvailablePerforceWorkspaces = value;
                    NotifyPropertyChanged("AvailablePerforceWorkspaces");
                }
            }
        }
        private ObservableCollection<String> m_AvailablePerforceWorkspaces;

        /// <summary>
        /// Perforce workspace.
        /// </summary>
        public String PerforceWorkspace
        {
            get { return (this.m_sPerforceWorkspace); }
            set
            {
                if (this.m_sPerforceWorkspace != value)
                {
                    this.m_sPerforceWorkspace = value;
                    NotifyPropertyChanged("PerforceWorkspace");
                }
            }
        }
        private String m_sPerforceWorkspace;

        /// <summary>
        /// Available projects.
        /// </summary>
        public ObservableCollection<ProjectViewModel> Projects
        {
            get { return (this.m_Projects); }
            set
            {
                if (this.m_Projects != value)
                {
                    this.m_Projects = value;
                    NotifyPropertyChanged("Projects");
                }
            }
        }
        private ObservableCollection<ProjectViewModel> m_Projects;

        /// <summary>
        /// Selected project.
        /// </summary>
        public ProjectViewModel SelectedProject
        {
            get { return (this.m_SelectedProject); }
            set
            {
                if (this.m_SelectedProject != value)
                {
                    this.m_SelectedProject = value;
                    NotifyPropertyChanged("SelectedProject");
                }
            }
        }
        private ProjectViewModel m_SelectedProject;

        #region Additional Settings
        /// <summary>
        /// Modern UI theme.
        /// </summary>
        public ApplicationTheme ModernTheme
        {
            get { return (this.m_sModernTheme); }
            set
            {
                if (this.m_sModernTheme != value)
                {
                    this.m_sModernTheme = value;
                    this.m_Application.SetTheme(value);
                    NotifyPropertyChanged("ModernTheme");
                }
            }
        }
        private ApplicationTheme m_sModernTheme;

        /// <summary>
        /// Available modern themes.
        /// </summary>
        public ObservableCollection<ApplicationTheme> AvailableThemes
        {
            get { return (this.m_AvailableThemes); }
            set
            {
                if (this.m_AvailableThemes != value)
                {
                    this.m_AvailableThemes = value;
                    NotifyPropertyChanged("AvailableThemes");
                }
            }
        }
        private ObservableCollection<ApplicationTheme> m_AvailableThemes;

        /// <summary>
        /// Whether the 3dsmax toolset selection is enabled.
        /// </summary>
        public bool CanSelectDCCToolSet3dsmax
        {
            get { return (this.m_CanSelectDCCToolSet3dsmax); }
            set { SetProperty(ref m_CanSelectDCCToolSet3dsmax, value); }
        }
        private bool m_CanSelectDCCToolSet3dsmax;

        /// <summary>
        /// Whether the MotionBuilder toolset selection is enabled.
        /// </summary>
        public bool CanSelectDCCToolSetMotionBuilder
        {
            get { return (this.m_CanSelectDCCToolSetMotionBuilder); }
            set { SetProperty(ref m_CanSelectDCCToolSetMotionBuilder, value); }
        }
        private bool m_CanSelectDCCToolSetMotionBuilder;

        /// <summary>
        /// Whether the Mudbox toolset selection is enabled.
        /// </summary>
        public bool CanSelectDCCToolSetMudbox
        {
            get { return (this.m_CanSelectDCCToolSetMudbox); }
            set { SetProperty(ref m_CanSelectDCCToolSetMudbox, value); }
        }
        private bool m_CanSelectDCCToolSetMudbox;

        /// <summary>
        /// Selected DCC Package Toolset for Autodesk 3dsmax.
        /// </summary>
        public DCCToolSetViewModel SelectedDCCToolSet3dsmax
        {
            get { return (this.m_SelectedDCCToolSet3dsmax); }
            set { SetProperty(ref m_SelectedDCCToolSet3dsmax, value); }
        }
        private DCCToolSetViewModel m_SelectedDCCToolSet3dsmax;

        /// <summary>
        /// Selected DCC Package Toolset for Autodesk MotionBuilder.
        /// </summary>
        public DCCToolSetViewModel SelectedDCCToolSetMotionBuilder
        {
            get { return (this.m_SelectedDCCToolSetMotionBuilder); }
            set { SetProperty(ref m_SelectedDCCToolSetMotionBuilder, value); }
        }
        private DCCToolSetViewModel m_SelectedDCCToolSetMotionBuilder;

        /// <summary>
        /// Selected DCC Package Toolset for Autodesk Mudbox.
        /// </summary>
        public DCCToolSetViewModel SelectedDCCToolSetMudbox
        {
            get { return (this.m_SelectedDCCToolSetMudbox); }
            set { SetProperty(ref m_SelectedDCCToolSetMudbox, value); }
        }
        private DCCToolSetViewModel m_SelectedDCCToolSetMudbox;

        /// <summary>
        /// Checked Install Mb python scripts
        /// </summary>
        public bool InstallDCCPythonScripts
        {
            get { return (this.m_InstallDCCPythonScripts); }
            set { SetProperty(ref m_InstallDCCPythonScripts, value); }
        }
        private bool m_InstallDCCPythonScripts;

        /// <summary>
        /// Available DCC Package Toolsets.
        /// </summary>
        public ObservableCollection<DCCToolSetViewModel> AvailableDCCToolSets
        {
            get { return (this.m_AvailableDCCToolSets); }
            set
            {
                if (this.m_AvailableDCCToolSets != value)
                {
                    this.m_AvailableDCCToolSets = value;
                    NotifyPropertyChanged("AvailableDCCToolSets");
                }
            }
        }
        private ObservableCollection<DCCToolSetViewModel> m_AvailableDCCToolSets;

        /// <summary>
        /// Available log levels.
        /// </summary>
        public ObservableCollection<LogLevel> AvailableLogLevels
        {
            get { return (this.m_AvailableLogLevels); }
            set
            {
                if (this.m_AvailableLogLevels != value)
                {
                    this.m_AvailableLogLevels = value;
                    NotifyPropertyChanged("AvailableLogLevels");
                }
            }
        }
        ObservableCollection<LogLevel> m_AvailableLogLevels;

        /// <summary>
        /// Log level user is interested in seeing.
        /// </summary>
        public LogLevel LogLevel
        {
            get { return (this.m_eLogLevel); }
            set
            {
                if (this.m_eLogLevel != value)
                {
                    this.m_eLogLevel = value;
                    NotifyPropertyChanged("LogLevel");
                }
            }
        }
        private LogLevel m_eLogLevel;

        /// <summary>
        /// Log tracing enabled flag.
        /// </summary>
        public bool LogTracing
        {
            get { return (this.m_bLogTracing); }
            set
            {
                if (this.m_bLogTracing != value)
                {
                    this.m_bLogTracing = value;
                    NotifyPropertyChanged("LogTracing");
                }
            }
        }
        private bool m_bLogTracing;
        #endregion Additional Settings// Additional Settings
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Root *writable* configuration object.
        /// </summary>
        IWritableConfig m_Config;

        /// <summary>
        /// 
        /// </summary>
        RsApplication m_Application;

        /// <summary>
        /// Installer log.
        /// </summary>
        public static RSG.Base.Logging.Universal.IUniversalLog Log;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public InstallerViewModel(RsApplication app, IWritableConfig config)
        {
            this.m_Config = config;
            this.m_Application = app;

            this.ValidLogin = false;
            this.HasLoginCredentials = false;
            this.ValidPerforceLogin = false;
            if (config.Studios.ThisStudio == null)
            {
                RsMessageBox.Show(
                    "Unable to continue, the installer was unable to determine the current studio from the list of subnets inside config.xml",
                    "Installer Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);

                Environment.Exit(1);
            }

            this.UserStudio = String.Format("{0} ({1})", config.Studios.ThisStudio.FriendlyName,
                config.Studios.ThisStudio.ActiveDirectoryDomain);
            this.PerforceServer = config.Studios.ThisStudio.DefaultPerforceServer;
            this.EnabledVersionControl = true;
            this.ModernTheme = app.CurrentTheme;
            this.AvailableThemes = new ObservableCollection<ApplicationTheme>(RsApplication.AvailableThemes);
            this.Projects = new ObservableCollection<ProjectViewModel>();
            this.Projects.Add(new ProjectViewModel(config.CoreProject));
            this.SelectedProject = this.Projects.FirstOrDefault();
            this.AvailablePerforceWorkspaces = new ObservableCollection<string>();

            InitConstants(config);
            LoadSettings(config);
        }

        /// <summary>
        /// Static constructor.
        /// </summary>
        static InstallerViewModel()
        {
            Log = LogFactory.CreateUniversalLog("Installer");
            LogFactory.CreateUniversalLogFile(Log);
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Reset all settings to defaults.
        /// </summary>
        public void ResetToDefaults(IConfig config)
        {
            this.ValidLogin = false;
            this.HasLoginCredentials = false;
            this.ValidPerforceLogin = false;
            this.Username = Environment.UserName;
            this.PerforceUsername = Environment.UserName;
            this.Password = String.Empty;
            this.PerforcePassword = String.Empty;
            this.UserEmailAddress = String.Empty;
            this.UserStudio = config.Studios.ThisStudio.FriendlyName;
            this.EnabledVersionControl = true;
            this.PerforceServer = config.Studios.ThisStudio.DefaultPerforceServer;
            this.PerforceWorkspace = String.Empty;
            this.ModernTheme = ApplicationTheme.Light;
            this.Projects = new ObservableCollection<ProjectViewModel>();
            if (this.AvailablePerforceWorkspaces == null)
            {
                this.AvailablePerforceWorkspaces = new ObservableCollection<string>();
            }
            else
            {
                this.AvailablePerforceWorkspaces.Clear();
            }

            InitConstants(config);
        }

        /// <summary>
        /// Attempt an Active Directory Login with currently specified credentials.
        /// </summary>
        public bool TryLogin()
        {
            try
            {
                this.ClearAllErrors("Username");
                this.ClearAllErrors("Password");

                IStudio studio = this.m_Config.Studios.ThisStudio;
                IDictionary<String, Object> userProperties;
                bool authenticated = RSG.Interop.Microsoft.ActiveDirectory.TryGetUserProperties(this.Username,
                    this.Password, studio.ActiveDirectoryDomain, out userProperties);

                if (authenticated)
                {
                    if (userProperties.ContainsKey("mail"))
                    {
                        ResultPropertyValueCollection mailCollection = (ResultPropertyValueCollection)userProperties["mail"];
                        this.UserEmailAddress = (String)(mailCollection[0]);
                    }
                    else
                    {
                        this.UserEmailAddress = "unknown";
                    }
                }
                else
                {
                    this.AddError("Username", Resources.StringTableErrors.Login, false);
                    this.AddError("Password", Resources.StringTableErrors.Login, false);
                    return (false);
                }

                return (true);
            }
            catch (Exception)
            {
                this.AddError("Username", Resources.StringTableErrors.Login, false);
                this.AddError("Password", Resources.StringTableErrors.Login, false);
                return (false);
            }
        }

        /// <summary>
        /// Attempt Perforce workspace query for current user and machine.
        /// </summary>
        /// <returns></returns>
        public bool TryGetPerforceWorkspaces()
        {
            try
            {
                this.AvailablePerforceWorkspaces.Clear();

                // Set default if we cannot get settings.
                if (String.IsNullOrWhiteSpace(this.PerforceUsername) ||
                    String.IsNullOrWhiteSpace(this.PerforcePassword) ||
                    String.IsNullOrWhiteSpace(this.PerforceServer))
                {
                    return true;
                }

                using (P4 p4 = new P4())
                {
                    p4.ExceptionLevel = P4API.P4ExceptionLevels.NoExceptionOnWarnings;
                    p4.Port = this.PerforceServer;
                    p4.User = this.PerforceUsername;
                    p4.Client = this.PerforceWorkspace;
                    p4.Connect(this.PerforcePassword);

                    P4API.P4RecordSet clients = p4.Run("clients", "-u", this.PerforceUsername);
                    string machineName = Environment.MachineName;
                    foreach (P4API.P4Record record in clients)
                    {
                        if (!record.Fields.ContainsKey("client"))
                        {
                            continue;
                        }

                        this.AvailablePerforceWorkspaces.Add(record["client"]);
                    }

                    if (String.IsNullOrWhiteSpace(this.PerforceWorkspace))
                    {
                        this.PerforceWorkspace =
                            this.AvailablePerforceWorkspaces.FirstOrDefault();
                    }

                    p4.Disconnect();
                }
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Attempt to verify Perforce settings (requires current login).
        /// </summary>
        /// <returns></returns>
        public bool TryPerforceSettings()
        {
            try
            {
                this.ClearAllErrors("PerforceServer");
                this.ClearAllErrors("PerforceWorkspace");

                if (String.IsNullOrWhiteSpace(this.PerforceServer))
                {
                    this.AddError("PerforceServer", Resources.StringTableErrors.PerforceNoServer, false);
                    return (false);
                }
                if (String.IsNullOrWhiteSpace(this.PerforceWorkspace))
                {
                    this.AddError("PerforceWorkspace", Resources.StringTableErrors.PerforceNoWorkspace, false);
                    return (false);
                }

                using (P4 p4 = new P4())
                {
                    p4.ExceptionLevel = P4API.P4ExceptionLevels.ExceptionOnBothErrorsAndWarnings;
                    p4.Port = this.PerforceServer;
                    p4.User = this.PerforceUsername;
                    p4.Client = this.PerforceWorkspace;
                    p4.Connect(this.PerforcePassword);
                    if (!p4.IsValidConnection(true, false))
                    {
                        this.AddError("PerforceServer", Resources.StringTableErrors.PerforceConnection.FormatCurrent(p4.Port, p4.User, p4.Client), false);
                        return (false);
                    }
                    else if (!p4.IsValidConnection(true, true))
                    {
                        this.AddError("PerforceWorkspace", Resources.StringTableErrors.PerforceWorkspace.FormatCurrent(p4.Port, p4.User, p4.Client), false);
                        return (false);
                    }
                    p4.Disconnect();
                    this.ValidPerforceLogin = true;
                }
            }
            catch (Exception ex)
            {
                this.AddError("PerforceServer", Resources.StringTableErrors.PerforceConnectionOnException.FormatCurrent(ex.Message), false);
                this.AddError("PerforceWorkspace", Resources.StringTableErrors.PerforceWorkspaceOnException.FormatCurrent(ex.Message), false);
       
                return (false);
            }
            return (true);
        }

        /// <summary>
        /// Run the installation.
        /// </summary>
        /// <returns></returns>
        public bool Install()
        {
            bool result = true;
            try
            {
                SetupConfig();
                ToolsInstaller installer = new ToolsInstaller(m_Config);

                DCCToolSet maxToolSet = DCCToolSet.DoNotInstall;
                if (this.CanSelectDCCToolSet3dsmax)
                    maxToolSet = this.SelectedDCCToolSet3dsmax.ToolSet;
                DCCToolSet mbToolSet = DCCToolSet.DoNotInstall;
                if (this.CanSelectDCCToolSetMotionBuilder)
                    mbToolSet = this.SelectedDCCToolSetMotionBuilder.ToolSet;
                DCCToolSet mudboxToolSet = DCCToolSet.DoNotInstall;
                if (this.CanSelectDCCToolSetMudbox)
                    mudboxToolSet = this.SelectedDCCToolSetMudbox.ToolSet;
                bool mbInstallPython = this.InstallDCCPythonScripts;

                String userBranch = this.SelectedProject.DefaultBranch.Key;
                result = installer.Install(Log, userBranch, maxToolSet, mbToolSet, mudboxToolSet, mbInstallPython);
                m_Application.SaveEditorValues();
            }
            catch (Exception e)
            {
                Log.ToolExceptionCtx(LOG_CTX, e, "Exception during installation");
                return false;
            }

            return (result);
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Initialise view-model constants.
        /// </summary>
        /// <param name="config"></param>
        private void InitConstants(IConfig config)
        {
            this.AvailableThemes = new ObservableCollection<ApplicationTheme>(RsApplication.AvailableThemes);
            this.AvailableLogLevels = new ObservableCollection<LogLevel>(Enum.GetValues(typeof(LogLevel)).Cast<RSG.Base.Logging.LogLevel>());
            this.AvailableDCCToolSets = new ObservableCollection<DCCToolSetViewModel>(
                Enum.GetValues(typeof(DCCToolSet)).Cast<DCCToolSet>().Select(t => new DCCToolSetViewModel(t)));

            List<IUsertype> sortedUserRoles = config.Usertypes.Where(t => !t.Pseudo).ToList();
            sortedUserRoles.Sort((x, y) => x.FriendlyName.CompareTo(y.FriendlyName));
            this.AvailableUserRoles = new ObservableCollection<IUsertype>();
            this.AvailableUserRoles.AddRange(sortedUserRoles);

            this.CanSelectDCCToolSet3dsmax = RSG.Interop.Autodesk3dsmax.Autodesk3dsmax.IsInstalled();
            this.CanSelectDCCToolSetMotionBuilder = RSG.Interop.AutodeskMotionBuilder.AutodeskMotionBuilder.IsInstalled();
            this.CanSelectDCCToolSetMudbox = RSG.Interop.AutodeskMudbox.AutodeskMudbox.IsInstalled();
        }

        /// <summary>
        /// Load our configured settings from disk.
        /// </summary>
        /// <returns></returns>
        private bool LoadSettings(IConfig config)
        {
            this.Username = config.Username;
            this.UserEmailAddress = config.EmailAddress;
            this.UserRole = config.Usertypes.Where(u => u.Flags == config.Usertype).FirstOrDefault();
            if (null == this.UserRole)
                this.UserRole = config.Usertypes.First();

            this.SelectedProject.DefaultBranch = this.SelectedProject.Branches.Where(b => b.Key.Equals(config.Project.DefaultBranchName)).FirstOrDefault();
            if (null == this.SelectedProject.DefaultBranch)
                this.SelectedProject.DefaultBranch = this.SelectedProject.Branches.First();

            this.EnabledVersionControl = config.VersionControlEnabled;
            if (config.VersionControlSettings.Any())
            {
                this.PerforceUsername = config.VersionControlSettings.First().Username;
                this.PerforceServer = config.VersionControlSettings.First().Server.ToString();
                this.PerforceWorkspace = config.VersionControlSettings.First().Workspace;
            }

            // Autodesk 3dsmax ToolSet.
            ToolsInstaller installer = new ToolsInstaller(m_Config);            
            IInstallerConfig installConfig = ConfigFactory.CreateInstallerConfig(m_Config.Project);
            if (this.CanSelectDCCToolSet3dsmax)
            {
                try
                {
                    DCCToolSet ts = installer.GetInstalledAutodesk3dsmaxToolSet(Log, installConfig);
                    this.SelectedDCCToolSet3dsmax = this.AvailableDCCToolSets.Where(d => d.ToolSet == ts).FirstOrDefault();
                }
                catch (Exception)
                {
                    Log.ErrorCtx(LOG_CTX, "Error reading Autodesk 3dsmax toolset setup.");
                    this.SelectedDCCToolSet3dsmax = this.AvailableDCCToolSets.Where(d => d.ToolSet == DCCToolSet.DoNotInstall).FirstOrDefault();
                }
            }
            else
            {
                this.SelectedDCCToolSet3dsmax = this.AvailableDCCToolSets.Where(d => d.ToolSet == DCCToolSet.DoNotInstall).FirstOrDefault();
            }

            // Autodesk MotionBuilder ToolSet.
            if (this.CanSelectDCCToolSetMotionBuilder)
            {
                try
                {
                    DCCToolSet ts = installer.GetInstalledAutodeskMotionBuilderToolSet(Log, installConfig);
                    this.SelectedDCCToolSetMotionBuilder = this.AvailableDCCToolSets.Where(d => d.ToolSet == ts).FirstOrDefault();
                    // If we have a motionbuilder toolset installed, use our preference to install py scripts, otherwise default true
                    if (ts == DCCToolSet.Current)
                    {
                        this.InstallDCCPythonScripts = installer.GetInstalledAutodeskMotionBuilderScripts(Log, installConfig);
                    }
                    else
                    {
                        this.InstallDCCPythonScripts = true;
                    }
                }
                catch (Exception)
                {
                    Log.ErrorCtx(LOG_CTX, "Error reading Autodesk MotionBuilder toolset setup.");
                    this.SelectedDCCToolSetMotionBuilder = this.AvailableDCCToolSets.Where(d => d.ToolSet == DCCToolSet.DoNotInstall).FirstOrDefault();
                }
            }
            else
            {
                this.SelectedDCCToolSetMotionBuilder = this.AvailableDCCToolSets.Where(d => d.ToolSet == DCCToolSet.DoNotInstall).FirstOrDefault();
            }

            // Autodesk Mudbox ToolSet.
            if (this.CanSelectDCCToolSetMudbox)
            {
                try
                {
                    DCCToolSet ts = installer.GetInstalledAutodeskMudboxToolSet(Log, installConfig);
                    this.SelectedDCCToolSetMudbox = this.AvailableDCCToolSets.Where(d => d.ToolSet == ts).FirstOrDefault();
                }
                catch (Exception)
                {
                    Log.ErrorCtx(LOG_CTX, "Error reading Autodesk Mudbox toolset setup.");
                    this.SelectedDCCToolSetMudbox = this.AvailableDCCToolSets.Where(d => d.ToolSet == DCCToolSet.DoNotInstall).FirstOrDefault();
                }
            }
            else
            {
                this.SelectedDCCToolSetMudbox = this.AvailableDCCToolSets.Where(d => d.ToolSet == DCCToolSet.DoNotInstall).FirstOrDefault();
            }

            return (true);
        }

        /// <summary>
        /// Copy settings from View-Model to writable-Config object.
        /// </summary>
        private void SetupConfig()
        {
            this.m_Config.Username = this.Username;
            this.m_Config.EmailAddress = this.UserEmailAddress;
            this.m_Config.Usertype = this.UserRole.Flags;
            this.m_Config.VersionControlEnabled = this.EnabledVersionControl;
            ((IWritableProject)this.m_Config.Project).DefaultBranchName = this.SelectedProject.DefaultBranch.Key;
            ((IWritableProject)this.m_Config.Project).DefaultBranch = this.m_Config.Project.Branches[this.SelectedProject.DefaultBranch.Key];
            
            IVersionControlSettings scm = new VersionControlSettings(this.PerforceServer,
                this.PerforceUsername, this.PerforceWorkspace);
            this.m_Config.VersionControlSettings = new IVersionControlSettings[] { scm };

            // Project Branch Config.
            foreach (ProjectViewModel vm in this.Projects)
                vm.SetupConfig(this.m_Config);
        }
        #endregion // Private Methods
    }

} // Installer.ViewModel namespace
