﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Configuration;
using RSG.Editor;

namespace Installer.ViewModel
{

    /// <summary>
    /// Target view-model.
    /// </summary>
    internal class TargetViewModel :
        Installer.ViewModel.ViewModelBase
    {
        #region Properties
        /// <summary>
        /// Platform (easy ITarget index).
        /// </summary>
        public RSG.Platform.Platform Platform
        {
            get { return (this.m_ePlatform); }
            private set
            {
                if (this.m_ePlatform != value)
                {
                    this.m_ePlatform = value;
                    NotifyPropertyChanged("Platform");
                }
            }
        }
        private RSG.Platform.Platform m_ePlatform;

        /// <summary>
        /// Platform-name.
        /// </summary>
        public String Name
        {
            get { return (this.m_sName); }
            private set
            {
                if (this.m_sName != value)
                {
                    this.m_sName = value;
                    NotifyPropertyChanged("Name");
                }
            }
        }
        private String m_sName;

        /// <summary>
        /// Enabled state.
        /// </summary>
        public bool IsEnabled
        {
            get { return (this.m_bIsEnabled); }
            internal set
            {
                if (this.SetProperty(ref this.m_bIsEnabled, value))
                {
                    foreach (TargetHeaderViewModel vm in this.BranchViewModel.ProjectViewModel.TargetHeaders)
                    {
                        vm.UpdateCheckedValue();
                    }
                }
            }
        }
        private bool m_bIsEnabled;

        /// <summary>
        /// Is target available on parent branch.
        /// </summary>
        public bool IsReadOnly
        {
            get { return (this.m_bIsReadOnly); }
            internal set
            {
                if (this.m_bIsReadOnly != value)
                {
                    this.m_bIsReadOnly = value;
                    NotifyPropertyChanged("IsReadOnly");
                }
            }
        }
        private bool m_bIsReadOnly;

        /// <summary>
        /// Invert IsReadOnly to save inverted bool->visibility converter.
        /// </summary>
        public bool NotReadOnly
        {
            get { return (!this.IsReadOnly); }
            set { this.IsReadOnly = !value; NotifyPropertyChanged("NotReadOnly"); }
        }

        public BranchViewModel BranchViewModel
        {
            get { return this.m_Parent; }
        }
        private BranchViewModel m_Parent;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor; from Platform (for display only).
        /// </summary>
        /// <param name="platform"></param>
        /// <param name="width"></param>
        public TargetViewModel(BranchViewModel branch, RSG.Platform.Platform platform)
        {
            this.m_Parent = branch;
            this.m_ePlatform = platform;
            this.m_sName = RSG.Platform.PlatformUtils.PlatformToFriendlyName(platform);
            this.m_bIsEnabled = false;
            this.m_bIsReadOnly = true;
        }

        /// <summary>
        /// Constructor; from ITarget.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="width"></param>
        public TargetViewModel(BranchViewModel branch, ITarget target)
        {
            this.m_Parent = branch;
            this.m_ePlatform = target.Platform;
            this.m_sName = RSG.Platform.PlatformUtils.PlatformToFriendlyName(target.Platform);
            this.m_bIsEnabled = target.Enabled;
            this.m_bIsReadOnly = false;
        }
        #endregion // Constructor(s)
    }

} // Installer.ViewModel namespace
