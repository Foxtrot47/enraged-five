﻿<rsg:RsWindow 
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
    xmlns:i="http://schemas.microsoft.com/expression/2010/interactivity"
    x:Class="Installer.MainWindow"    
    xmlns:rsg="http://schemas.rockstargames.com/2013/xaml/editor"
    xmlns:installer="clr-namespace:Installer"
    xmlns:res="clr-namespace:Installer.Resources"
    Icon="/Resources/Images/rockstar.png"
    Width="680" Height="500" 
    Title="{Binding Source={x:Static res:StringTable.InstallerWindow_Title}}" 
    WindowStartupLocation="CenterScreen"
    TextOptions.TextFormattingMode="Display"
    UseLayoutRounding="True"
    ShowHelpButton="True"
    CanMinimiseWindow="False"
    CanMaximiseWindow="False"
    ResizeMode="NoResize">

    <rsg:RsWindow.InputBindings>
        <KeyBinding Key="Enter" Command="rsg:WizardCommands.Next"/>
    </rsg:RsWindow.InputBindings>

    <!-- Resources -->
    <rsg:RsWindow.Resources>
        <BooleanToVisibilityConverter x:Key="BoolToVisibilityConverter" />
        <rsg:EnumValueToDisplayNameConverter x:Key="EnumValueToDisplayNameConverter"/>
    </rsg:RsWindow.Resources>
    
    <!-- Command Bindings -->
    <rsg:RsWindow.CommandBindings>
        <!-- Wizard Commands -->
        <CommandBinding Command="rsg:WizardCommands.Next" 
                        CanExecute="WizardNextCanExecute"
                        Executed="WizardNext" />
        <CommandBinding Command="rsg:WizardCommands.Previous" 
                        CanExecute="WizardPreviousCanExecute"
                        Executed="WizardPrevious" />
        <CommandBinding Command="rsg:WizardCommands.Cancel" 
                        CanExecute="WizardCancelCanExecute"
                        Executed="WizardCancel" />
        <CommandBinding Command="rsg:WizardCommands.Finish" 
                        CanExecute="WizardFinishCanExecute"
                        Executed="WizardFinish" />
    </rsg:RsWindow.CommandBindings>
    
    <!-- UI -->
    <Grid>
        <!-- Wizard -->
        <rsg:Wizard x:Name="Wizard" ShowPageProgress="False" Margin="0,0,7,5">
            <rsg:Wizard.Resources>
                <rsg:LogicalNotConverter x:Key="LogicalNotConverter" />
            </rsg:Wizard.Resources>
            <rsg:Wizard.Pages>
                <!-- Introduction -->
                <rsg:WizardPage Header="{Binding Source={x:Static res:StringTable.InstallerWelcome_Header}}" 
                                AllowPrevious="False" 
                                AllowNext="True">
                    <TextBlock Grid.Row="0" Text="{Binding Source={x:Static res:StringTable.InstallerWelcome_Text}}" TextWrapping="Wrap" />

                </rsg:WizardPage>
                
                <!-- Basic Installer Pages -->
                <rsg:WizardPage Header="{Binding Source={x:Static res:StringTable.InstallerUserAccount_Header}}"
                                AllowNext="{Binding HasLoginCredentials}">
                    <StackPanel Orientation="Vertical">
                        <TextBlock Text="{Binding Source={x:Static res:StringTable.InstallerUserAccount_Instructions}}" TextWrapping="Wrap" />
                        <Grid Margin="50,50,0,0">
                            <Grid.RowDefinitions>
                                <RowDefinition Height="Auto" />
                                <RowDefinition Height="5" /> <!-- Separator -->
                                <RowDefinition Height="Auto" />
                                <RowDefinition Height="5" /> <!-- Separator -->
                                <RowDefinition Height="Auto" />
                                <RowDefinition Height="5" /> <!-- Separator -->
                                <RowDefinition Height="Auto" />
                                <RowDefinition Height="5" /> <!-- Separator -->
                                <RowDefinition Height="Auto" />
                                <RowDefinition Height="5" /> <!-- Separator -->
                                <RowDefinition Height="Auto" />
                            </Grid.RowDefinitions>
                            <Grid.ColumnDefinitions>
                                <ColumnDefinition Width="100" />
                                <ColumnDefinition Width="300" />
                            </Grid.ColumnDefinitions>
                            <TextBlock Grid.Row="0" Grid.Column="0" VerticalAlignment="Center"
                                       Text="{Binding Source={x:Static res:StringTable.InstallerUserAccount_Username}}" />
                            <TextBox Grid.Row="0" Grid.Column="1" 
                                     Text="{Binding Username, NotifyOnValidationError=True}" />
                            <TextBlock Grid.Row="2" Grid.Column="0" VerticalAlignment="Center"
                                       Text="{Binding Source={x:Static res:StringTable.InstallerUserAccount_Password}}" />
                            <PasswordBox Grid.Row="2" Grid.Column="1" PasswordChar="*" 
                                         installer:PasswordBoxHelper.Attach="True"
                                         installer:PasswordBoxHelper.Password="{Binding Password, Mode=TwoWay, NotifyOnValidationError=True}">
                                <PasswordBox.InputBindings>
                                    <KeyBinding Key="Enter" Command="rsg:WizardCommands.Next"/>
                                </PasswordBox.InputBindings>
                            </PasswordBox>
                            <TextBlock Grid.Row="4" VerticalAlignment="Center"
                                       Text="{Binding Source={x:Static res:StringTable.InstallerUserAccount_UserRole}}" />
                            <ComboBox Grid.Row="4" Grid.Column="1" 
                                      ItemsSource="{Binding AvailableUserRoles}" 
                                      SelectedItem="{Binding UserRole}">
                                <ComboBox.ItemTemplate>
                                    <DataTemplate>
                                        <TextBlock Text="{Binding FriendlyName}" />
                                    </DataTemplate>
                                </ComboBox.ItemTemplate>
                            </ComboBox>
                            <Separator Grid.Row="6" Grid.Column="0" Grid.ColumnSpan="2" Width="0" />
                            <TextBlock Grid.Row="8" Grid.Column="0" VerticalAlignment="Center"
                                       Text="{Binding Source={x:Static res:StringTable.InstallerUserAccount_EmailAddress}}" />
                            <TextBlock Grid.Row="8" Grid.Column="1" VerticalAlignment="Center"
                                       Text="{Binding UserEmailAddress}" />
                            <TextBlock Grid.Row="10" Grid.Column="0" VerticalAlignment="Center"
                                       Text="{Binding Source={x:Static res:StringTable.InstallerUserAccount_Studio}}" />
                            <TextBlock Grid.Row="10" Grid.Column="1" VerticalAlignment="Center"
                                       Text="{Binding UserStudio}" />
                        </Grid>
                    </StackPanel>
                </rsg:WizardPage>
                                 
                <!-- Source Control Page -->
                <rsg:WizardPage Header="{Binding Source={x:Static res:StringTable.InstallerSourceControl_Header}}">
                    <StackPanel Orientation="Vertical">
                        <TextBlock Text="{Binding Source={x:Static res:StringTable.InstallerSourceControl_Instructions}}" TextWrapping="Wrap" />
                        <Grid Margin="50,50,0,0">
                            <Grid.RowDefinitions>
                                <RowDefinition Height="Auto" />
                                <RowDefinition Height="5" /> <!-- Separator -->
                                <RowDefinition Height="Auto" />
                                <RowDefinition Height="5" /> <!-- Separator -->
                                <RowDefinition Height="Auto" />
                                <RowDefinition Height="5" /> <!-- Separator -->
                                <RowDefinition Height="Auto" />
                                <RowDefinition Height="5" /> <!-- Separator -->
                                <RowDefinition Height="Auto" />
                                <RowDefinition Height="5" /> <!-- Separator -->
                                <RowDefinition Height="Auto" />
                                <RowDefinition Height="5" /> <!-- Separator -->
                                <RowDefinition Height="Auto" />
                            </Grid.RowDefinitions>
                            <Grid.ColumnDefinitions>
                                <ColumnDefinition Width="100" />
                                <ColumnDefinition Width="200"/>
                                <ColumnDefinition Width="Auto" />
                            </Grid.ColumnDefinitions>
                            
                            <TextBlock Grid.Row="0" Grid.Column="0"  VerticalAlignment="Center"
                                       Text="{Binding Source={x:Static res:StringTable.InstallerSourceControl_Enabled}}" />
                            <CheckBox Grid.Row="0" Grid.Column="1" 
                                      IsChecked="{Binding EnabledVersionControl}" />
                            
                            <TextBlock Grid.Row="2" Grid.Column="0"  VerticalAlignment="Center"
                                       Text="{Binding Source={x:Static res:StringTable.InstallerSourceControl_Server}}" />
                            <TextBox Grid.Row="2" Grid.Column="1" 
                                     IsEnabled="{Binding EnabledVersionControl}"
                                     Text="{Binding PerforceServer}" />

                            <TextBlock Grid.Row="4" Grid.Column="0"  VerticalAlignment="Center"
                                       Text="{Binding Source={x:Static res:StringTable.InstallerSourceControl_Workspace}}" />
                            <ComboBox Grid.Row="4" Grid.Column="1" IsEditable="True"
                                      IsEnabled="{Binding EnabledVersionControl}"
                                      SelectedItem="{Binding PerforceWorkspace}"                                     
                                      ItemsSource="{Binding AvailablePerforceWorkspaces}" />
                            <Button Grid.Row="4" Margin="7,0,0,0" Grid.Column="2" HorizontalAlignment="Left" Padding="0,0,0,0" Width="{Binding RelativeSource={RelativeSource Self}, Path=ActualHeight}" ToolTip="Refresh Available Workspaces" Click="WorkspaceRefreshClicked">
                                <Button.Content>
                                    <Image Source="{x:Static rsg:CommonIcons.Refresh}" Width="16" Height="16"/>
                                </Button.Content>
                            </Button>

                            <TextBlock Grid.Row="6" Grid.Column="0"  VerticalAlignment="Center"
                                       Text="{Binding Source={x:Static res:StringTable.InstallerSourceControl_Override}}" />
                            <CheckBox Grid.Row="6" Grid.Column="1" 
                                      IsChecked="{Binding OverridePerforceLogin}" />

                            <TextBlock Grid.Row="8" Grid.Column="0"  VerticalAlignment="Center"
                                       Text="{Binding Source={x:Static res:StringTable.InstallerSourceControl_Username}}" />
                            <TextBox Grid.Row="8" Grid.Column="1"
                                     IsEnabled="{Binding OverridePerforceLogin}"
                                     Text="{Binding PerforceUsername, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged}"/>

                            <TextBlock Grid.Row="10" Grid.Column="0"  VerticalAlignment="Center"
                                       Text="{Binding Source={x:Static res:StringTable.InstallerSourceControl_Password}}" />
                            <PasswordBox Grid.Row="10" Grid.Column="1" PasswordChar="*"
                                         IsEnabled="{Binding OverridePerforceLogin}"
                                         installer:PasswordBoxHelper.Attach="True"
                                         installer:PasswordBoxHelper.Password="{Binding PerforcePassword, Mode=TwoWay, NotifyOnValidationError=True}" />
                        </Grid>
                    </StackPanel>
                </rsg:WizardPage>
                
                <!-- Projects Page -->
                <rsg:WizardPage Header="{Binding Source={x:Static res:StringTable.InstallerProjects_Header}}">
                    <Grid>
                        <Grid.RowDefinitions>
                            <RowDefinition Height="Auto" />
                            <RowDefinition Height="*" />
                        </Grid.RowDefinitions>
                        <TextBlock Grid.Row="0" Text="{Binding Source={x:Static res:StringTable.InstallerProject_Instructions}}" TextWrapping="Wrap" />
                        <Grid Grid.Row="1" Margin="50,50,0,0">
                            <Grid.RowDefinitions>
                                <RowDefinition Height="Auto" />
                                <RowDefinition Height="5" />
                                <!-- Separator -->
                                <RowDefinition Height="Auto" />
                                <RowDefinition Height="5" />
                                <!-- Separator -->
                                <RowDefinition Height="*" />
                            </Grid.RowDefinitions>
                            <Grid.ColumnDefinitions>
                                <ColumnDefinition Width="Auto" />
                                <ColumnDefinition Width="Auto" />
                                <ColumnDefinition Width="*" />
                            </Grid.ColumnDefinitions>
                            
                            <TextBlock Grid.Row="0" Grid.Column="0"  VerticalAlignment="Center"
                                       Text="{Binding Source={x:Static res:StringTable.InstallerProject_Project}}" />
                            <ComboBox Grid.Row="0" Grid.Column="1" Width="200" Margin="150,0,0,0"  SelectedItem="{Binding SelectedProject}" ItemsSource="{Binding Projects}" />


                            <!-- Default Branch Selection -->
                            <TextBlock Grid.Row="2" Grid.Column="0" VerticalAlignment="Center"
                                       Text="{Binding Source={x:Static res:StringTable.InstallerProject_DefaultBranch}}" />
                            <ComboBox Grid.Row="2" Grid.Column="1" Width="200" Margin="150,0,0,0" SelectedItem="{Binding SelectedProject.DefaultBranch}" ItemsSource="{Binding SelectedProject.Branches}" />


                            <GroupBox Grid.Row="4" Grid.ColumnSpan="2" HorizontalContentAlignment="Stretch" VerticalContentAlignment="Stretch" Header="{Binding Source={x:Static res:StringTable.InstallerProject_Platforms}}">
                                <DockPanel HorizontalAlignment="Stretch">
                                    <Button DockPanel.Dock="Bottom" Margin="0,5,0,0" Content="Advanced Platform Selection" Click="AdvancedPlatformSelectionClicked" HorizontalAlignment="Right"/>
                                    <ScrollViewer VerticalScrollBarVisibility="Auto" DockPanel.Dock="Top"  Background="Transparent">
                                        <ItemsControl HorizontalAlignment="Stretch" ItemsSource="{Binding SelectedProject.TargetHeaders}">
                                            <ItemsControl.ItemTemplate>
                                                <DataTemplate>
                                                    <CheckBox Content="{Binding Name}" IsChecked="{Binding IsChecked}"/>
                                                </DataTemplate>
                                            </ItemsControl.ItemTemplate>
                                        </ItemsControl>
                                    </ScrollViewer>
                                </DockPanel>
                            </GroupBox>
                        </Grid>
                    </Grid>
                </rsg:WizardPage>

                <!-- Settings Page -->
                <rsg:WizardPage Header="{Binding Source={x:Static res:StringTable.InstallerSettings_Header}}">
                    <StackPanel Orientation="Vertical">
                        <GroupBox Header="{Binding Source={x:Static res:StringTable.InstallerSettings_GUI}}"
                                  Margin="5">
                            <StackPanel Orientation="Horizontal" Margin="5">
                                <TextBlock VerticalAlignment="Center" 
                                        Text="{Binding Source={x:Static res:StringTable.InstallerSettings_ModernTheme}}"/>
                                <Separator Height="0" Width="5" />
                                <ComboBox Margin="5,0,0,0" VerticalAlignment="Center"
                                          Width="150"
                                        SelectedItem="{Binding ModernTheme}"
                                        ItemsSource="{Binding AvailableThemes}">
                                    <ComboBox.ItemTemplate>
                                        <DataTemplate>
                                            <TextBlock Text="{Binding Converter={StaticResource EnumValueToDisplayNameConverter}}"/>
                                        </DataTemplate>
                                    </ComboBox.ItemTemplate>
                                </ComboBox>
                            </StackPanel>
                        </GroupBox>
                        <GroupBox Header="{Binding Source={x:Static res:StringTable.InstallerSettings_DCC}}"
                                  Margin="5">
                            <Grid>
                                <Grid.ColumnDefinitions>
                                    <ColumnDefinition Width="Auto" />
                                    <ColumnDefinition Width="5" />
                                    <ColumnDefinition Width="Auto" />
                                </Grid.ColumnDefinitions>
                                <Grid.RowDefinitions>
                                    <RowDefinition />
                                    <RowDefinition Height="5" />
                                    <RowDefinition />
                                    <RowDefinition Height="5" />
                                    <RowDefinition />
                                    <RowDefinition Height="5" />
                                    <RowDefinition />
                                </Grid.RowDefinitions>
                                <!-- Autodesk 3dsmax ToolSet -->
                                <TextBlock Grid.Row="0" Grid.Column="0" VerticalAlignment="Center"
                                           Text="{Binding Source={x:Static res:StringTable.InstallerSettings_DCCToolSet3dsmax}}" />
                                <ComboBox Grid.Row="0" Grid.Column="2" VerticalAlignment="Center"
                                          Width="Auto" 
                                          SelectedItem="{Binding SelectedDCCToolSet3dsmax}"
                                          ItemsSource="{Binding AvailableDCCToolSets}"
                                          IsHitTestVisible="{Binding CanSelectDCCToolSet3dsmax}"
                                          Focusable="{Binding CanSelectDCCToolSet3dsmax}">
                                    <ComboBox.ItemTemplate>
                                        <DataTemplate>
                                            <TextBlock Text="{Binding FriendlyName}" />
                                        </DataTemplate>
                                    </ComboBox.ItemTemplate>
                                </ComboBox>
                                
                                <!-- Autodesk MotionBuilder ToolSet -->
                                <TextBlock Grid.Row="2" Grid.Column="0" VerticalAlignment="Center"
                                           Text="{Binding Source={x:Static res:StringTable.InstallerSettings_DCCToolSetMotionBuilder}}" />
                                <ComboBox Grid.Row="2" Grid.Column="2" VerticalAlignment="Center"
                                          Width="Auto"
                                          SelectedItem="{Binding SelectedDCCToolSetMotionBuilder}"
                                          ItemsSource="{Binding AvailableDCCToolSets}"
                                          IsHitTestVisible="{Binding CanSelectDCCToolSetMotionBuilder}"
                                          Focusable="{Binding CanSelectDCCToolSetMotionBuilder}">
                                    <ComboBox.ItemTemplate>
                                        <DataTemplate>
                                            <TextBlock Text="{Binding FriendlyName}" />
                                        </DataTemplate>
                                    </ComboBox.ItemTemplate>
                                </ComboBox>
                                <!-- Motionbuilder Python Scripts -->
                                <TextBlock Grid.Row="4" Grid.Column="0" VerticalAlignment="Center"
                                           Text="{Binding Source={x:Static res:StringTable.InstallerSettings_DCCToolSetMotionBuilderScripts}}" />
                                <CheckBox Grid.Row="4" Grid.Column="2" VerticalAlignment="Center" IsChecked="{Binding InstallDCCPythonScripts}"/>
                                
                                <!-- Autodesk Mudbox ToolSet -->
                                <TextBlock Grid.Row="6" Grid.Column="0" VerticalAlignment="Center"
                                           Text="{Binding Source={x:Static res:StringTable.InstallerSettings_DCCToolSetMudbox}}" />
                                <ComboBox Grid.Row="6" Grid.Column="2" VerticalAlignment="Center"
                                          Width="Auto"
                                          SelectedItem="{Binding SelectedDCCToolSetMudbox}"
                                          ItemsSource="{Binding AvailableDCCToolSets}"
                                          IsHitTestVisible="{Binding CanSelectDCCToolSetMudbox}"
                                          Focusable="{Binding CanSelectDCCToolSetMudbox}">
                                    <ComboBox.ItemTemplate>
                                        <DataTemplate>
                                            <TextBlock Text="{Binding FriendlyName}" />
                                        </DataTemplate>
                                    </ComboBox.ItemTemplate>
                                </ComboBox>
                            </Grid>
                        </GroupBox>
                        
                        <!--
                            Log options currently disabled.
                        <GroupBox Header="{Binding Source={x:Static res:StringTable.InstallerSettings_LogOptions}}"
                                  Margin="5" 
                                  IsEnabled="False">
                            <StackPanel Orientation="Vertical" Margin="5">
                                <StackPanel Orientation="Horizontal">
                                    <TextBlock VerticalAlignment="Center"
                                               Text="{Binding Source={x:Static res:StringTable.InstallerSettings_LogLevel}}" />
                                    <Separator Height="0" Width="5" />
                                    <ComboBox ItemsSource="{Binding AvailableLogLevels}"
                                              SelectedItem="{Binding LogLevel}" />
                                </StackPanel>
                                <Separator Height="5" Width="0" />
                                
                                <CheckBox IsEnabled="False">
                                    <TextBlock VerticalAlignment="Center"
                                       Text="{Binding Source={x:Static res:StringTable.InstallerSettings_LogTrace}}" />
                                </CheckBox>
                            </StackPanel>
                        </GroupBox>
                        -->
                    </StackPanel>
                </rsg:WizardPage>

                <!-- Finish Installer Page -->
                <rsg:WizardPage Header="{Binding Source={x:Static res:StringTable.InstallerSummary_Header}}" 
                                AllowNext="False" AllowFinish="True">
                    <TextBlock Text="{Binding Source={x:Static res:StringTable.InstallerSummary_Instructions}}" TextWrapping="Wrap" />
                </rsg:WizardPage>
            </rsg:Wizard.Pages>
        </rsg:Wizard>
    </Grid>
</rsg:RsWindow>
