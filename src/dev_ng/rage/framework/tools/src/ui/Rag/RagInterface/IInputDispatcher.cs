﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace RagInterface
{
    /// <summary>
    /// Interface for objects that dispatch messages to the game on the event pipe.
    /// </summary>
    public interface IInputDispatcher
    {
        /// <summary>
        /// Checks and dispatches any queued input events.
        /// </summary>
        /// <returns>True if data was dispatched, otherwise false.</returns>
        bool ProcessQueue();

        /// <summary>
        /// Queues a keyboard event for the specified key.
        /// </summary>
        /// <param name="evt"></param>
        /// <param name="key"></param>
        void QueueKeyboardEvent(KeyboardEvent evt, Key key);

        /// <summary>
        /// Queues a mouse event.
        /// </summary>
        /// <param name="evt"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="wheelDelta"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        void QueueMouseEvent(MouseEvent evt, double x, double y, int wheelDelta, double width, double height);
    } // IInputDispatcher
}
