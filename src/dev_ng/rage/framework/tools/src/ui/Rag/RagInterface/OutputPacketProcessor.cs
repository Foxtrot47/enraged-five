﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Logging;
using RSG.Rag.Core;

namespace RagInterface
{
    /// <summary>
    /// 
    /// </summary>
    public class OutputPacketProcessor : IPacketProcessor
    {
        #region Member Data
        /// <summary>
        /// Pipe that this processor is for.
        /// </summary>
        private readonly INamedPipe _pipe;

        /// <summary>
        /// Log object.
        /// </summary>
        private readonly ILog _log;

        /// <summary>
        /// The thread-safe queue that contains the lines of output.
        /// </summary>
        private readonly ConcurrentQueue<TTYLine> _queue = new ConcurrentQueue<TTYLine>();

        private String _currentLine = String.Empty;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pipe"></param>
        /// <param name="log"></param>
        public OutputPacketProcessor(INamedPipe pipe, ILog log)
        {
            _pipe = pipe;
            _log = log;
        }
        #endregion // Constructor(s)

        #region IBankPacketProcessor Methods
        /// <summary>
        /// Reads data off of the game's bank pipe adding it to a queue for processing from a separate thread.
        /// Throws:
        /// DataException - Error during data decompression.
        /// </summary>
        /// <returns></returns>
        public bool Update()
        {
            bool readData = false;

            while (_pipe.HasData)
            {
                const int BUFFER_SIZE = 4 * 1024;
                Byte[] chars = new Byte[BUFFER_SIZE];
                int numRead = _pipe.ReadData(chars, BUFFER_SIZE, false);

                for (int i = 0; i < numRead; i++)
                {
                    Char charRead = Convert.ToChar(chars[i]);
                    if (charRead == 0)
                    {
                        if (_currentLine.Length > 0)
                        {
                            _queue.Enqueue(new TTYLine(_currentLine));
                        }

                        _currentLine = String.Empty;
                    }
                    else
                    {
                        _currentLine += charRead;
                    }
                }

                readData = true;
            }

            return readData;
        }

        /// <summary>
        /// Processes data of off the packet queue.
        /// </summary>
        /// <returns></returns>
        public bool Process()
        {
            bool processedData = false;
            TTYLine line;

            while (_queue.TryDequeue(out line))
            {
                // Currently don't do anything.
                processedData = true;
            }

            return processedData;
        }
        #endregion // IBankPacketProcessor Methods
    } // OutputPacketProcessor
}
