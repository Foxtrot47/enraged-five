﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Rag.Core;

namespace RagInterface
{
    /// <summary>
    /// Dispatches messages to the game on the event pipe.
    /// </summary>
    public class InputDispatcher : IInputDispatcher
    {
        #region Constants
        /// <summary>
        /// Maximum number of components that each packet can send.
        /// </summary>
        private const int COMPONENTS_PER_PACKET = 5;

        /// <summary>
        /// Size of a single packet (in bytes).
        /// </summary>
        private const int PACKET_SIZE = COMPONENTS_PER_PACKET * 4;
        #endregion // Constants

        #region Member Data
        /// <summary>
        /// Pipe to use to send data.
        /// </summary>
        private INamedPipe _pipe;

        /// <summary>
        /// Log object.
        /// </summary>
        private ILog _log;

        /// <summary>
        /// The thread-safe queue that contains the list of input events that need to be dispatched.
        /// </summary>
        private readonly ConcurrentQueue<byte[]> _dispatchQueue = new ConcurrentQueue<byte[]>();
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pipe"></param>
        /// <param name="log"></param>
        public InputDispatcher(INamedPipe pipe, ILog log)
        {
            _pipe = pipe;
            _log = log;
        }
        #endregion // Constructor(s)

        #region Dispatch Methods
        /// <summary>
        /// Checks and dispatches any queued input events.
        /// </summary>
        /// <returns>True if data was dispatched, otherwise false.</returns>
        public bool ProcessQueue()
        {
            bool dataDispatched = false;

            if (_dispatchQueue.Any())
            {
                // Estimated size as something could be added between initialising the
                // list and copying across the data.
                int estimatedDispatchBufferSize = _dispatchQueue.Count * PACKET_SIZE;
                IList<byte> allData = new List<byte>(estimatedDispatchBufferSize);

                byte[] packetData;
                while (_dispatchQueue.TryDequeue(out packetData))
                {
                    allData.AddRange(packetData);
                }

                // Send the data across the pipe.
                _pipe.WriteData(allData.ToArray(), allData.Count);
                dataDispatched = true;
            }

            return dataDispatched;
        }

        /// <summary>
        /// Queues a keyboard event for the specified key.
        /// </summary>
        /// <param name="evt"></param>
        /// <param name="key"></param>
        public void QueueKeyboardEvent(KeyboardEvent evt, Key key)
        {
            byte[] buffer = new byte[PACKET_SIZE];
            int i = 0;

            // event
            uint integer = (uint)evt;
            buffer[i++] = (byte)(integer);
            buffer[i++] = (byte)(integer >> 8);
            buffer[i++] = (byte)(integer >> 16);
            buffer[i++] = (byte)(integer >> 24);

            // key
            integer = (uint)KeyInterop.VirtualKeyFromKey(key);
            buffer[i++] = (byte)(integer);
            buffer[i++] = (byte)(integer >> 8);
            buffer[i++] = (byte)(integer >> 16);
            buffer[i++] = (byte)(integer >> 24);

            // Add it to the queue.
            _dispatchQueue.Enqueue(buffer);
        }

        /// <summary>
        /// Queues a mouse event.
        /// </summary>
        /// <param name="evt"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="wheelDelta"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public void QueueMouseEvent(MouseEvent evt, double x, double y, int wheelDelta, double width, double height)
        {
            byte[] buffer = new byte[PACKET_SIZE];
            int i = 0;

            // event
            uint integer = (uint)evt;
            buffer[i++] = (byte)(integer);
            buffer[i++] = (byte)(integer >> 8);
            buffer[i++] = (byte)(integer >> 16);
            buffer[i++] = (byte)(integer >> 24);

            // mouse:
            buffer[i++] = (byte)(integer);
            buffer[i++] = (byte)(integer >> 8);
            buffer[i++] = (byte)(integer >> 16);
            buffer[i++] = (byte)(integer >> 24);

            // x position:
            FloatToInt floatToInt = new FloatToInt((float)x / (float)width);
            buffer[i++] = (byte)(floatToInt.u);
            buffer[i++] = (byte)(floatToInt.u >> 8);
            buffer[i++] = (byte)(floatToInt.u >> 16);
            buffer[i++] = (byte)(floatToInt.u >> 24);

            // y position:
            floatToInt.f = (float)y / (float)height;
            buffer[i++] = (byte)(floatToInt.u);
            buffer[i++] = (byte)(floatToInt.u >> 8);
            buffer[i++] = (byte)(floatToInt.u >> 16);
            buffer[i++] = (byte)(floatToInt.u >> 24);

            // mouse wheel:
            ushort shortDelta = (ushort)wheelDelta;
            buffer[i++] = (byte)(shortDelta);
            buffer[i++] = (byte)(shortDelta >> 8);

            // Add it to the queue.
            _dispatchQueue.Enqueue(buffer);
        }
        #endregion // Dispatch Methods
    } // InputDispatcher
}
