﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using RSG.Editor.Controls;

namespace RagInterface
{
    /// <summary>
    /// 
    /// </summary>
    public class RagGameViewportHost : RsGameViewportHost
    {
        #region Fields
        /// <summary>
        /// Reference to the object in charge of sending input events across the
        /// event pipe.
        /// </summary>
        private IInputDispatcher _inputDispatcher;
        #endregion // Fields

        #region Properties
        /// <summary>
        /// Property for getting/setting the input dispatcher.
        /// </summary>
        public IInputDispatcher InputDispatcher
        {
            get { return _inputDispatcher; }
            set { _inputDispatcher = value; }
        }
        #endregion // Properties

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        protected override void OnWindowsKeyDownMessage(Key key)
        {
            if (_inputDispatcher != null)
            {
                _inputDispatcher.QueueKeyboardEvent(KeyboardEvent.KeyDown, key);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        protected override void OnWindowsKeyUpMessage(Key key)
        {
            if (_inputDispatcher != null)
            {
                _inputDispatcher.QueueKeyboardEvent(KeyboardEvent.KeyUp, key);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pos"></param>
        protected override void OnWindowsMouseMoveMessage(Point pos)
        {
            if (_inputDispatcher != null)
            {
                _inputDispatcher.QueueMouseEvent(MouseEvent.MouseMove, pos.X, pos.Y, 0, ActualWidth, ActualHeight);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pos"></param>
        /// <param name="delta"></param>
        protected override void OnWindowsMouseWheelMessage(Point pos, int delta)
        {
            if (_inputDispatcher != null)
            {
                _inputDispatcher.QueueMouseEvent(MouseEvent.MouseMove, pos.X, pos.Y, delta, ActualWidth, ActualHeight);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pos"></param>
        /// <param name="button"></param>
        protected override void OnWindowsMouseDownMessage(Point pos, MouseButton button)
        {
            if (_inputDispatcher != null)
            {
                if (button == MouseButton.Left)
                {
                    _inputDispatcher.QueueMouseEvent(MouseEvent.LeftButtonDown, pos.X, pos.Y, 0, ActualWidth, ActualHeight);
                }
                else if (button == MouseButton.Right)
                {
                    _inputDispatcher.QueueMouseEvent(MouseEvent.RightButtonDown, pos.X, pos.Y, 0, ActualWidth, ActualHeight);
                }
                else if (button == MouseButton.Middle)
                {
                    _inputDispatcher.QueueMouseEvent(MouseEvent.MiddleButtonDown, pos.X, pos.Y, 0, ActualWidth, ActualHeight);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pos"></param>
        /// <param name="button"></param>
        protected override void OnWindowsMouseUpMessage(Point pos, MouseButton button)
        {
            if (_inputDispatcher != null)
            {
                if (button == MouseButton.Left)
                {
                    _inputDispatcher.QueueMouseEvent(MouseEvent.LeftButtonUp, pos.X, pos.Y, 0, ActualWidth, ActualHeight);
                }
                else if (button == MouseButton.Right)
                {
                    _inputDispatcher.QueueMouseEvent(MouseEvent.RightButtonUp, pos.X, pos.Y, 0, ActualWidth, ActualHeight);
                }
                else if (button == MouseButton.Middle)
                {
                    _inputDispatcher.QueueMouseEvent(MouseEvent.MiddleButtonUp, pos.X, pos.Y, 0, ActualWidth, ActualHeight);
                }
            }
        }
        #endregion // Overrides
    } // RagGameViewportHost
}
