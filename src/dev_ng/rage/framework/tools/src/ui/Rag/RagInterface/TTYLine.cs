﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace RagInterface
{
#pragma warning disable 1591
    /// <summary>
    /// 
    /// </summary>
    public class TTYLine
    {
        public TTYLine(string text)
        {
            if (text.EndsWith(TTYLine.ColorPostfixCode))
            {
                m_text = text.Remove(text.LastIndexOf(TTYLine.ColorPostfixCode), TTYLine.ColorPostfixCode.Length);
                m_text += "\n";
            }
            else
            {
                m_text = text;
            }

            if (m_text.StartsWith(TTYLine.ColorCodeCharacter, StringComparison.Ordinal))
            {
                m_text = GetTextColors(m_text, ref m_foreColor, ref m_backColor);
            }

            string postText = m_text;
            m_severity = Severity.Display;
            for (int i = 0; i < TTYLine.SeverityPrefix.Length; ++i)
            {
                if ((TTYLine.SeverityPrefix[i].Length > 0) && m_text.StartsWith(TTYLine.SeverityPrefix[i], StringComparison.Ordinal))
                {
                    m_severity = (Severity)i;
                    postText = m_text.Remove(0, TTYLine.SeverityPrefix[i].Length);
                    break;
                }
                else if (TTYLine.SeverityText[i].Length > 0)
                {
                    int indexOf = m_text.IndexOf(TTYLine.SeverityText[i], StringComparison.Ordinal);
                    if (indexOf != -1)
                    {
                        m_severity = (Severity)i;
                        postText = m_text.Remove(0, indexOf + TTYLine.SeverityText[i].Length);
                        break;
                    }
                }
            }

            int indexOfFirstBracket = postText.IndexOf('[');
            if ((indexOfFirstBracket != -1) && (indexOfFirstBracket <= 16))
            {
                int indexOfLastBracket = postText.IndexOf(']', indexOfFirstBracket);
                if ((indexOfLastBracket != -1) && (indexOfLastBracket < postText.Length - 1) && (postText[indexOfLastBracket + 1] == ' '))
                {
                    int startIndex = indexOfFirstBracket + 1;
                    int endIndex = indexOfLastBracket - startIndex;
                    m_channelName = postText.Substring(startIndex, endIndex);

                    Color channelColor;
                    if ((m_severity >= Severity.Display) && TTYLine.ChannelColors.TryGetValue(m_channelName, out channelColor))
                    {
                        m_foreColor = channelColor;
                        m_backColor = SystemColors.WindowColor;
                    }
                }
            }
        }

        #region Enums
        public enum Severity
        {
            FatalError,
            Error,
            Warning,
            Display,
            Debug1,
            Debug2,
            Debug3
        }
        #endregion

        #region Variables
        private string m_text = string.Empty;
        private Severity m_severity = Severity.Display;
        private string m_channelName = string.Empty;
        private Color m_foreColor = SystemColors.WindowTextColor;
        private Color m_backColor = SystemColors.WindowColor;

        private static string sm_textWarningPrefix = "Warning: ";
        private static string sm_textErrorPrefix = "Error: ";
        private static string sm_textQuitPrefix = "Fatal Error: ";
        private static string sm_textDebug1Prefix = "Debug1: ";
        private static string sm_textDebug2Prefix = "Debug2: ";
        private static string sm_textDebug3Prefix = "Debug3: ";
        private static string sm_textScriptPrefix = "[Script] ";

        private static string sm_textColorCodePrefix = Convert.ToChar(027).ToString();
        private static string sm_textNormalCode = sm_textColorCodePrefix + "[0m";    // this code come from //rage/dev/rage/base/src/diag/output.h
        private static string sm_textRedCode = sm_textColorCodePrefix + "[31m";      // this code come from //rage/dev/rage/base/src/diag/output.h
        private static string sm_textYellowCode = sm_textColorCodePrefix + "[33m";   // this code come from //rage/dev/rage/base/src/diag/output.h
        private static string sm_textBlueCode = sm_textColorCodePrefix + "[34m";     // this code come from //rage/dev/rage/base/src/diag/output.h
        private static string sm_textOrangeCode = sm_textColorCodePrefix + "[37m";   // except this one.  this is made up

        private static string sm_textColorizedWarningPrefix = sm_textYellowCode + sm_textWarningPrefix;
        private static string sm_textColorizedErrorPrefix = sm_textRedCode + sm_textErrorPrefix;
        private static string sm_textColorizedQuitPrefix = sm_textRedCode + sm_textQuitPrefix;
        private static string sm_textColorizedDebug1Prefix = sm_textBlueCode + sm_textDebug1Prefix;
        private static string sm_textColorizedDebug2Prefix = sm_textBlueCode + sm_textDebug2Prefix;
        private static string sm_textColorizedDebug3Prefix = sm_textBlueCode + sm_textDebug3Prefix;
        private static string sm_textColorizedScriptPrefix = sm_textOrangeCode + sm_textScriptPrefix;

        private static int sm_textColorizedWarningPrefixLength = sm_textColorizedWarningPrefix.Length;
        private static int sm_textColorizedErrorPrefixLength = sm_textColorizedErrorPrefix.Length;
        private static int sm_textColorizedQuitPrefixLength = sm_textColorizedQuitPrefix.Length;
        private static int sm_textColorizedDebug1PrefixLength = sm_textColorizedDebug1Prefix.Length;
        private static int sm_textColorizedDebug2PrefixLength = sm_textColorizedDebug2Prefix.Length;
        private static int sm_textColorizedDebug3PrefixLength = sm_textColorizedDebug3Prefix.Length;
        private static int sm_textColorizedScriptPrefixLength = sm_textColorizedScriptPrefix.Length;

        private static Dictionary<string, Color> sm_foreColorCodes = BuildForeColorCodeDictionary();
        private static Dictionary<string, Color> sm_backColorCodes = BuildBackColorCodeDictionary();

        private static Dictionary<string, Color> sm_channelColors = BuildChannelColorsDictionary();
        #endregion

        #region Properties
        public static string[] SeverityPrefix = new string[]
            {
                "Fatal Error: ",
                "Error: ",
                "Warning: ",
                "",
                "Debug1: ",
                "Debug2: ",
                "Debug3: "
            };

        public static Color[] SeverityForeColor = new Color[]
            {
                Colors.Red,
                Colors.Red,
                Colors.Goldenrod,
                SystemColors.WindowTextColor,
                Colors.Blue,
                Colors.Blue,
                Colors.Blue
            };

        public static string[] SeverityText = new string[]
            {
                "): Fatal Error : ",
                "): Error : ",
                "): Warning : ",
                "",
                "): Debug1 : ",
                "): Debug2 : ",
                "): Debug3 : "
            };

        public static string ColorPostfixCode = sm_textNormalCode + "\n";

        public static string ColorCodeCharacter = Convert.ToChar(027).ToString();

        public static Dictionary<string, Color> ChannelColors
        {
            get
            {
                return sm_channelColors;
            }
        }

        public string Text
        {
            get
            {
                return m_text;
            }
        }

        public Severity TextSeverity
        {
            get
            {
                return m_severity;
            }
        }

        public string ChannelName
        {
            get
            {
                return m_channelName;
            }
        }

        public Color ForeColor
        {
            get
            {
                return m_foreColor;
            }
        }

        public Color BackColor
        {
            get
            {
                return m_backColor;
            }
        }
        #endregion

        #region Private Functions
        /// <summary>
        /// Checks the start of text for a color code and returns by references what the foreground and 
        /// background colors should be.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="foreColor">pass in the default foreground color</param>
        /// <param name="backColor">pass in the default background color</param>
        /// <returns>the string without the color code</returns>
        private string GetTextColors(string text, ref Color foreColor, ref Color backColor)
        {
            string prefix = text.Substring(0, text.Length < 5 ? text.Length : 5);
            if (prefix.StartsWith(sm_textNormalCode, StringComparison.Ordinal))
            {
                // normal, just trim the color code
                return text.Remove(0, sm_textNormalCode.Length);
            }

            Color c;

            // see if it's a regular color
            if (sm_foreColorCodes.TryGetValue(prefix, out c))
            {
                foreColor = c;
                return text.Remove(0, prefix.Length);
            }

            // see if it's an inverse color
            if (sm_backColorCodes.TryGetValue(prefix, out c))
            {
                backColor = c;
                return text.Remove(0, prefix.Length);
            }

            // see if it's a long inverse color
            if (text.Length >= 10)
            {
                prefix = text.Substring(0, 10);
                if (sm_backColorCodes.TryGetValue(prefix, out c))
                {
                    backColor = c;
                    return text.Remove(0, prefix.Length);
                }
            }

            return text;
        }

        private static Dictionary<string, Color> BuildForeColorCodeDictionary()
        {
            Dictionary<string, Color> colorCodes = new Dictionary<string, Color>();

            colorCodes.Add(sm_textColorCodePrefix + "[30m", Colors.Black);
            colorCodes.Add(sm_textRedCode, Colors.Red);
            colorCodes.Add(sm_textColorCodePrefix + "[32m", Colors.Green);
            colorCodes.Add(sm_textYellowCode, Colors.Goldenrod);
            colorCodes.Add(sm_textColorCodePrefix + "[34m", Colors.Blue);
            colorCodes.Add(sm_textColorCodePrefix + "[35m", Colors.Purple);
            colorCodes.Add(sm_textColorCodePrefix + "[36m", Colors.Cyan);

            colorCodes.Add(sm_textOrangeCode, Colors.Chocolate);

            return colorCodes;
        }

        private static Dictionary<string, Color> BuildBackColorCodeDictionary()
        {
            Dictionary<string, Color> colorCodes = new Dictionary<string, Color>();

            colorCodes.Add(sm_textColorCodePrefix + "[41m", Colors.Red);
            colorCodes.Add(sm_textColorCodePrefix + "[42m" + sm_textColorCodePrefix + "[30m", Colors.Green);
            colorCodes.Add(sm_textColorCodePrefix + "[43m" + sm_textColorCodePrefix + "[30m", Colors.Goldenrod);
            colorCodes.Add(sm_textColorCodePrefix + "[44m", Colors.Blue);
            colorCodes.Add(sm_textColorCodePrefix + "[45m", Colors.Purple);
            colorCodes.Add(sm_textColorCodePrefix + "[46m", Colors.Cyan);

            return colorCodes;
        }

        private static Dictionary<string, Color> BuildChannelColorsDictionary()
        {
            Dictionary<string, Color> channelColors = new Dictionary<string, Color>();

            channelColors.Add("Script", Colors.Chocolate);

            return channelColors;
        }
        #endregion
    } // TTYLine
#pragma warning restore 1591
}
