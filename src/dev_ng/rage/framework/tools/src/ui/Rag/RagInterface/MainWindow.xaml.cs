﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RSG.Editor.Controls;

namespace RagInterface
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : RsMainWindow
    {
        #region Fields
        /// <summary>
        /// Reference to the application.
        /// </summary>
        private readonly RagInterfaceApp _app;
        #endregion // Fields

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="application"></param>
        public MainWindow(RagInterfaceApp application)
            : base()
        {
            _app = application;
            _app.ProxyConnected += ApplicationProxyConnected;
            InitializeComponent();
            DataContext = new MainWindowDataContext();
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public RagGameViewportHost GameViewport
        {
            get
            {
                return this.TryFindResource("GameViewportBlah") as RagGameViewportHost;
            }
        }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Event handler for when the application has successfully established the connection to the proxy.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ApplicationProxyConnected(object sender, EventArgs e)
        {
            GameViewport.InputDispatcher = _app.InputDispatcher;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        private MainWindowDataContext DataContextResolver(ICommand command)
        {
            MainWindowDataContext dc = null;
            if (!this.Dispatcher.CheckAccess())
            {
                dc = this.Dispatcher.Invoke<MainWindowDataContext>(() => this.DataContext as MainWindowDataContext);
            }
            else
            {
                dc = this.DataContext as MainWindowDataContext;
            }

            if (dc == null)
            {
                Debug.Fail("Data context isn't valid.");
                throw new ArgumentException("Data context isn't valid.");
            }
            return dc;
        }
        #endregion // Methods
    } // MainWindow
}
