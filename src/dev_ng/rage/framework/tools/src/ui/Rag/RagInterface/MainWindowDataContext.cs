﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using RSG.Editor;
using System.Net;
using System.Net.Sockets;
using RSG.Rag.Core;
using System.Diagnostics;
using RSG.Base.Logging;
using RSG.Rag.Widgets;
using System.Windows;
using RSG.Rag.ViewModel;
using RSG.Rag.ViewModel.Widgets;
using RSG.Editor.Controls.Dock.ViewModel;
using RSG.Editor.Controls.Dock;

namespace RagInterface
{
    /// <summary>
    /// 
    /// </summary>
    internal sealed class MainWindowDataContext : NotifyPropertyChangedBase
    {
        #region Member Data
        /// <summary>
        /// Private field for the <see cref="Title"/> property.
        /// </summary>
        private String _title;

        /// <summary>
        /// Private reference to the root bank manager view model.
        /// </summary>
        private BankManagerViewModel _bankManagerViewModel;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="MainWindowDataContext"/> class.
        /// </summary>
        public MainWindowDataContext()
            : base()
        {
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// The main window's title.
        /// </summary>
        public String Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        /// <summary>
        /// Bank manager view model used by the bank widget view.
        /// </summary>
        public BankManagerViewModel BankManagerViewModel
        {
            get { return _bankManagerViewModel; }
            set { SetProperty(ref _bankManagerViewModel, value); }
        }
        #endregion // Properties
    } // MainWindowDataContext
}
