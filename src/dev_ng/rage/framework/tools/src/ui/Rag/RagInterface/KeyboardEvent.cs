﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RagInterface
{
    /// <summary>
    /// 
    /// </summary>
    public enum KeyboardEvent
    {
        /// <summary>
        /// WM_CHAR
        /// </summary>
        Char = 0x0102,

        /// <summary>
        /// WM_SYSCHAR
        /// </summary>
        SysChar = 0x0106,

        /// <summary>
        /// WM_KEYDOWN
        /// </summary>
        KeyDown = 0x0100,

        /// <summary>
        /// WM_SYSKEYDOWN
        /// </summary>
        SysKeyDown = 0x0104,

        /// <summary>
        /// WM_KEYDOWN
        /// </summary>
        KeyUp = 0x0101,

        /// <summary>
        /// WM_SYSKEYDOWN
        /// </summary>
        SysKeyUp = 0x0105,

        /// <summary>
        /// MK_SHIFT
        /// </summary>
        Shift = 0x0004,

        /// <summary>
        /// MK_CONTROL
        /// </summary>
        Control = 0x0008
    } // KeyboardEvent
}
