﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using RSG.Base.Logging;
using RSG.Rag.Compression;
using RSG.Rag.Widgets;

namespace RSG.Rag.Core
{
    /// <summary>
    /// 
    /// </summary>
    public class BankPacketProcessor : IPacketProcessor, IWidgetResolver
    {
        #region Constants
        /// <summary>
        /// Invalid widget id.
        /// </summary>
        public const uint INVALID_WIDGET_ID = 0;

        /// <summary>
        /// Log context for the packet processor.
        /// </summary>
        public const String PACKET_PROCESSOR_CTX = "Packet Processor";

        /// <summary>
        /// Log context for the widget resolver.
        /// </summary>
        public const String RESOLVER_CTX = "Widget Resolver";
        #endregion // Constants

        #region Member Data
        /// <summary>
        /// Pipe that this processor is for.
        /// </summary>
        private INamedPipe _pipe;

        /// <summary>
        /// Log object.
        /// </summary>
        private ILog _log;

        /// <summary>
        /// The thread-safe queue that contains the compression packets that require processing.
        /// </summary>
        private readonly ConcurrentQueue<CompressionPacket> _packetQueue = new ConcurrentQueue<CompressionPacket>();

        /// <summary>
        /// Compression factory for decompressing incoming packets.
        /// </summary>
        private readonly CompressionFactory _compressionFactory = new CompressionFactory();

        /// <summary>
        /// Partial read buffer from the previous update.
        /// </summary>
        private byte[] _partialReadBuffer;

        /// <summary>
        /// Mapping of widget id's to widgets.
        /// </summary>
        private readonly IDictionary<uint, IWidget> _remoteToLocal = new Dictionary<uint, IWidget>();

        /// <summary>
        /// Mapping of widget id's to paths of deleted widgets.
        /// </summary>
        private readonly IDictionary<uint, String> _deletedWidgets = new Dictionary<uint, String>();

        /// <summary>
        /// 
        /// </summary>
        private readonly WidgetFactory _widgetFactory;

        /// <summary>
        /// Bank packet dispatcher.
        /// </summary>
        private readonly IPacketDispatcher _bankPacketDispatcher;
        #endregion // Member Data

        #region Events
        /// <summary>
        /// 
        /// </summary>
        public WidgetEventHandler WidgetCreated;
        #endregion // Events

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pipe"></param>
        /// <param name="log"></param>
        public BankPacketProcessor(INamedPipe pipe, ILog log)
        {
            _pipe = pipe;
            _log = log;
            _widgetFactory = new WidgetFactory(log);
            _bankPacketDispatcher = new NamedPipePacketDispatcher(pipe);
        }
        #endregion // Constructor(s)

        #region IBankPacketProcessor Methods
        /// <summary>
        /// Reads data off of the game's bank pipe adding it to a queue for processing from a separate thread.
        /// Throws:
        /// DataException - Error during data decompression.
        /// </summary>
        /// <returns></returns>
        public bool Update()
        {
            if (_pipe.HasData)
            {
                byte[] pipeData = new byte[_pipe.NumBytesAvailable];
                int numRead = _pipe.ReadData(pipeData, pipeData.Length, false);
                Debug.Assert(pipeData.Length == numRead, "Mismatch between amount requested from pipe and amount read.");

                byte[] dataToProcess = pipeData;

                // Append what we read onto the end of the read buffer.
                if (_partialReadBuffer != null)
                {
                    byte[] combinedBuffer = new byte[_partialReadBuffer.Length + pipeData.Length];
                    Array.ConstrainedCopy(_partialReadBuffer, 0, combinedBuffer, 0, _partialReadBuffer.Length);
                    Array.ConstrainedCopy(pipeData, 0, combinedBuffer, _partialReadBuffer.Length, pipeData.Length);
                    dataToProcess = combinedBuffer;
                }

                // Try to convert the raw data stream into compression packets.
                //TODO: Wrap in try catch.
                int dataRead;
                IEnumerable<CompressionPacket> compressionPackets = _compressionFactory.Read(dataToProcess, out dataRead);
                foreach (CompressionPacket packet in compressionPackets)
                {
                    _packetQueue.Enqueue(packet);
                }
                
                // Update the partial read buffer with any left over data.
                if (dataRead == 0)
                {
                    _partialReadBuffer = dataToProcess;
                }
                else
                {
                    int remainingDataLength = dataToProcess.Length - dataRead;
                    if (remainingDataLength == 0)
                    {
                        _partialReadBuffer = null;
                    }
                    else
                    {
                        _partialReadBuffer = new byte[remainingDataLength];
                        Array.ConstrainedCopy(dataToProcess, dataRead, _partialReadBuffer, 0, remainingDataLength);
                    }
                }

                return true;
            }
            else
            {
                return false;
            }
        }
        
        /// <summary>
        /// Processes data of off the packet queue.
        /// </summary>
        /// <returns></returns>
        public bool Process()
        {
            bool processedData = false;
            CompressionPacket compresionPacket;
            while (_packetQueue.TryDequeue(out compresionPacket))
            {
                // TODO: Wrap in try catch.
                byte[] decompressedData = compresionPacket.DecompressData();

                int index = 0;
                while (index < decompressedData.Length)
                {
                    RemotePacket packet = new RemotePacket(decompressedData, index);
                    index += RemotePacket.HEADER_SIZE + packet.Length;

                    ProcessRemotePacket(packet);
                }

                processedData = true;
            }

            return processedData;
        }
        #endregion // IBankPacketProcessor Methods

        #region IWidgetResolver Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        public IWidget GetWidgetById(uint widgetId)
        {
            IWidget widget = null;

            if (widgetId == INVALID_WIDGET_ID)
            {
                _log.ErrorCtx(RESOLVER_CTX, "Trying to get widget with the invalid id.");
            }
            else if (!_remoteToLocal.TryGetValue(widgetId, out widget))
            {
                String path;
                if (_deletedWidgets.TryGetValue(widgetId, out path))
                {
                    _log.ErrorCtx(RESOLVER_CTX, "A widget with ID {0} does not exist anymore.  It was previously associated with '{1}'.", widgetId, path);
                }
                else
                {
                    _log.ErrorCtx(RESOLVER_CTX, "A widget with ID {0} does not exist.  It was never associated with a widget.", widgetId);
                }
            }

            return widget;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="widget"></param>
        public void TrackWidget(IWidget widget)
        {
            _remoteToLocal[widget.Id] = widget;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="widget"></param>
        public void WidgetDestroyed(IWidget widget)
        {
            _deletedWidgets.Add(widget.Id, widget.Path);
            _remoteToLocal.Remove(widget.Id);
        }
        #endregion // IWidgetResolver Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="packet"></param>
        private void ProcessRemotePacket(RemotePacket packet)
        {
            RemotePacketEventArgs e = new RemotePacketEventArgs(packet);
            IWidget widget = null;

            // Create is a special command type.
            if ((BankCommand)packet.Command == BankCommand.Create)
            {
                widget = ProcessCreateMessage(e);
            }
            else
            {
                // Attempt to get the widget this message is for.
                widget = GetWidgetById(packet.Id);
                if (widget == null)
                {
                    // TODO: Proper error notification.
                    _log.ErrorCtx(PACKET_PROCESSOR_CTX, "Unable to retrieve widget for id {0}.", packet.Id);
                }
                else
                {
                    // Destroy is handled separately as well.
                    if ((BankCommand)packet.Command == BankCommand.Destroy)
                    {
                        ProcessDestroyMessage(e, widget);
                    }
                    else
                    {
                        widget.ProcessMessage(e);
                    }
                }
            }

            // Make sure that all the data that was sent in the packet got read.
            if (!packet.WasAllDataRead())
            {
                e.AddErrorMessage(
                    "'{0}' message for '{1}' ('{2}') contained additional unread data.  Read offset: {3}, Header Length: {4}",
                    (BankCommand)packet.Command,
                    (widget != null ? widget.Path : "<<unknown widget>>"),
                    (widget != null ? widget.Guid.ToString() : "<<unknown guid>>"),
                    e.Packet.ReadOffset,
                    e.Packet.Length);
            }

            // Check if there are any errors reported for the processing of the message.
            if (!e.Handled)
            {
                foreach (String error in e.ErrorMessages)
                {
                    _log.ErrorCtx(PACKET_PROCESSOR_CTX, error);
                }
                //TODO notify the user
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        private IWidget ProcessCreateMessage(RemotePacketEventArgs e)
        {
            // Create the widget.
            Debug.Assert(e.Packet.Id != INVALID_WIDGET_ID, "Create message received the invalid id as the widget's id.");
            IWidget widget = _widgetFactory.CreateWidget((WidgetGuid)e.Packet.Guid, e.Packet.Id, this, _bankPacketDispatcher);

            // Let the widget handle the creation message.
            widget.ProcessCreateMessage(e);

            if (e.Handled)
            {
                // If we successfully created the widget, add it to it's parent and keep track of it.
                if (widget.Parent != null)
                {
                    widget.Parent.AddChild(widget);
                }
                else
                {
                    Debug.Assert(widget is BankManager, "Only bank manager's can have a null parent.");
                }
                TrackWidget(widget);
            }
            else
            {
                foreach (String error in e.ErrorMessages)
                {
                    _log.ErrorCtx(PACKET_PROCESSOR_CTX, error);
                }
                //TODO notify the user that something went wrong.
            }

            OnWidgetCreated(widget);
            return widget;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        /// <param name="widget"></param>
        private void ProcessDestroyMessage(RemotePacketEventArgs e, IWidget widget)
        {
            widget.Destroy();
        }

        /// <summary>
        /// Helper method for raising the WidgetCreated event.
        /// </summary>
        /// <param name="widget">The widget that was created.</param>
        private void OnWidgetCreated(IWidget widget)
        {
            if (WidgetCreated != null)
            {
                WidgetCreated(this, new WidgetEventArgs(widget));
            }
        }
        #endregion // Private Methods
    } // BankRemotePacketProcessor
}
