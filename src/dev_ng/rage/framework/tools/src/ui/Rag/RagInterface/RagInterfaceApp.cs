﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using RSG.Base.Configuration;
using RSG.Base.Configuration.Ports;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Base.OS;
using RSG.Editor;
using RSG.Editor.Controls;
using RSG.Rag.Core;
using RSG.Rag.ViewModel;
using RSG.Rag.ViewModel.Widgets;
using RSG.Rag.Widgets;

namespace RagInterface
{
    /// <summary>
    /// Defines the main entry point to the application and is responsible for creating and
    /// running the application.
    /// </summary>
    public class RagInterfaceApp : RsApplication
    {
        #region Commandline Options
        /// <summary>
        /// 
        /// </summary>
        private static readonly String OPT_PROXYPORT = "proxyport";
        #endregion // Commandline Options

        #region Constants
        /// <summary>
        /// Default base port to use when attempting to reserve the 50 ports.
        /// </summary>
        private const int DEFAULT_BASE_PIPE_PORT = 50000;

        /// <summary>
        /// Number of consecutive ports to reserve.
        /// </summary>
        private const int NUM_PORTS_TO_RESERVE = 50;
        #endregion // Constants 

        #region Fields
        /// <summary>
        /// Local log object.
        /// </summary>
        private readonly ILog _log;

        /// <summary>
        /// Cancellation token source for shutting down the rag interface.
        /// </summary>
        private readonly CancellationTokenSource _shutdownTokenSource;

        /// <summary>
        /// 
        /// </summary>
        private ProxyConnection _proxyConnection;

        /// <summary>
        /// 
        /// </summary>
        private int _basePort;

        /// <summary>
        /// List of reserved sockets for the various pipes.
        /// </summary>
        private TcpListener[] _reservedSockets = new TcpListener[NUM_PORTS_TO_RESERVE];

        /// <summary>
        /// Name of the bank pipe.
        /// </summary>
        private PipeId _bankPipeName;

        /// <summary>
        /// Name of the output pipe.
        /// </summary>
        private PipeId _outputPipeName;

        /// <summary>
        /// Name of the event pipe.
        /// </summary>
        private PipeId _eventPipeName;

        /// <summary>
        /// Named pipe for reading/sending widget related data.
        /// </summary>
        private INamedPipe _bankPipe;

        /// <summary>
        /// Named pipe for reading output data.
        /// </summary>
        private INamedPipe _outputPipe;

        /// <summary>
        /// Named pipe for sending input/mouse event data.
        /// </summary>
        private INamedPipe _eventPipe;

        /// <summary>
        /// Processor for reading/parsing data that comes from the bank pipe.
        /// </summary>
        private IPacketProcessor _bankPacketProcessor;

        /// <summary>
        /// Processor for reading/parsing data that comes from the output pipe.
        /// </summary>
        private IPacketProcessor _outputPacketProcessor;

        /// <summary>
        /// Object used for dispatching mouse/keyboard events over the event pipe.
        /// </summary>
        private IInputDispatcher _inputDispatcher;

        /// <summary>
        /// Reference to the game connection the app is connected to.
        /// </summary>
        private GameConnection _gameConnection;

        /// <summary>
        /// Factory for creating widget view models.
        /// </summary>
        private readonly WidgetViewModelFactory _widgetViewModelFactory;
        #endregion // Fields

        #region Events
        /// <summary>
        /// Event handler for when we are successfully connected to the proxy.
        /// </summary>
        public event EventHandler ProxyConnected;
        #endregion // Events

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="RagInterfaceApp"/> class.
        /// </summary>
        public RagInterfaceApp()
        {
            RegisterCommandlineArg(OPT_PROXYPORT, LongOption.ArgType.Required, "Port to connect against the RAG proxy.");

            LoadWidgetPlugins();
            InitialisePorts();

            _log = LogFactory.CreateUniversalLog("MAIN");
            _widgetViewModelFactory = new WidgetViewModelFactory(_log);
            _shutdownTokenSource = new CancellationTokenSource();
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the unique name for this application that is used for mutex creation, and
        /// folder organisation inside the local app data folder and the registry.
        /// </summary>
        public override string UniqueName
        {
            get { return "RagInterface"; }
        }

        /// <summary>
        /// Gets a value indicating whether the layout for the main window get serialised on
        /// exit and deserialised during loading.
        /// </summary>
        protected override bool MakeLayoutPersistent
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a semi-colon separate list of the application authors email addresses. These
        /// are the addresses which are used by the unhandled exception dialog to determine
        /// where the mail should be sent by default.
        /// e.g.
        /// *tools@rockstarnorth.com
        /// michael.taschler@rockstarnorth.com;dave.evans@rockstarnorth.com
        /// </summary>
        public override string AuthorEmailAddress
        {
            get { return "RDR3Tools@rockstarsandiego.com"; }
        }

        /// <summary>
        /// Retrieves a reference to the input dispatcher.
        /// </summary>
        public IInputDispatcher InputDispatcher
        {
            get { return _inputDispatcher; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Main entry point into the application. The first thing to get called.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            LogFactory.Initialize();

            RagInterfaceApp app = new RagInterfaceApp();
            app.ShutdownMode = ShutdownMode.OnMainWindowClose;
            RsApplication.OnEntry(app, ApplicationMode.Multiple);

            LogFactory.FlushApplicationLog();
            LogFactory.ApplicationShutdown();
        }

        /// <summary>
        /// Override to create the main window for the application.
        /// </summary>
        /// <returns>
        /// The System.Windows.Window that will be the main window for this application.
        /// </returns>
        protected override Window CreateMainWindow()
        {
            return new MainWindow(this);
        }

        /// <summary>
        /// Loads the widget plugins.
        /// </summary>
        /// <param name="e">
        /// The event data.
        /// </param>
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            // The proxy connection task initialises the connection with the RAG proxy.
            Task proxyConnectionTask = new Task(InitialiseProxyConnection, _shutdownTokenSource.Token);
            proxyConnectionTask.ContinueWith(
                t => HandleProxyConnectionException(t),
                _shutdownTokenSource.Token,
                TaskContinuationOptions.OnlyOnFaulted,
                TaskScheduler.FromCurrentSynchronizationContext());
            proxyConnectionTask.ContinueWith(
                t => RaiseProxyConnected(t),
                _shutdownTokenSource.Token,
                TaskContinuationOptions.OnlyOnRanToCompletion,
                TaskScheduler.FromCurrentSynchronizationContext());

            // Once the connection has been established, we add a set of long running tasks for reading/processing
            // the data that the proxy sends us on the various pipes.
            Task updateBankPipeTask = proxyConnectionTask.ContinueWith(
                t => UpdateBankPipe(),
                _shutdownTokenSource.Token,
                TaskContinuationOptions.LongRunning | TaskContinuationOptions.OnlyOnRanToCompletion,
                TaskScheduler.Default);
            updateBankPipeTask.ContinueWith(
                t => HandleException(t),
                _shutdownTokenSource.Token,
                TaskContinuationOptions.OnlyOnFaulted,
                TaskScheduler.FromCurrentSynchronizationContext());

            Task updateOutputPipeTask = proxyConnectionTask.ContinueWith(
                t => UpdateOutputPipe(),
                _shutdownTokenSource.Token,
                TaskContinuationOptions.LongRunning | TaskContinuationOptions.OnlyOnRanToCompletion,
                TaskScheduler.Default);
            updateOutputPipeTask.ContinueWith(
                t => HandleException(t),
                _shutdownTokenSource.Token,
                TaskContinuationOptions.OnlyOnFaulted,
                TaskScheduler.FromCurrentSynchronizationContext());

            Task processBankPipeTask = proxyConnectionTask.ContinueWith(
                t => ProcessBankPipe(),
                _shutdownTokenSource.Token,
                TaskContinuationOptions.LongRunning | TaskContinuationOptions.OnlyOnRanToCompletion,
                TaskScheduler.Default);
            processBankPipeTask.ContinueWith(
                t => HandleException(t),
                _shutdownTokenSource.Token,
                TaskContinuationOptions.OnlyOnFaulted,
                TaskScheduler.FromCurrentSynchronizationContext());

            Task processOutputPipeTask = proxyConnectionTask.ContinueWith(
                t => ProcessOutputPipe(),
                _shutdownTokenSource.Token,
                TaskContinuationOptions.LongRunning | TaskContinuationOptions.OnlyOnRanToCompletion,
                TaskScheduler.Default);
            processOutputPipeTask.ContinueWith(
                t => HandleException(t),
                _shutdownTokenSource.Token,
                TaskContinuationOptions.OnlyOnFaulted,
                TaskScheduler.FromCurrentSynchronizationContext());

            Task updateEventPipeTask = proxyConnectionTask.ContinueWith(
                t => UpdateEventPipe(),
                _shutdownTokenSource.Token,
                TaskContinuationOptions.LongRunning | TaskContinuationOptions.OnlyOnRanToCompletion,
                TaskScheduler.Default);
            updateEventPipeTask.ContinueWith(
                t => HandleException(t),
                _shutdownTokenSource.Token,
                TaskContinuationOptions.OnlyOnFaulted,
                TaskScheduler.FromCurrentSynchronizationContext());

            // Start the proxy connection task.
            proxyConnectionTask.Start();
        }

        /// <summary>
        /// Make sure we notify the long running tasks that they should shutdown.
        /// </summary>
        /// <param name="persistentDirectory"></param>
        protected override void HandleOnExiting(string persistentDirectory)
        {
            _shutdownTokenSource.Cancel();
            base.HandleOnExiting(persistentDirectory);
        }

        /// <summary>
        /// Loads any widget plugins and adds their core resource dictionary to the list of merged dictionaries for the application.
        /// </summary>
        private void LoadWidgetPlugins()
        {
            String applicationPath = AppDomain.CurrentDomain.BaseDirectory;
            foreach (String pluginFilepath in Directory.EnumerateFiles(applicationPath, "RSG.Rag.Widgets.*.dll"))
            {
                Assembly plugin = Assembly.LoadFrom(pluginFilepath);
                AssemblyName pluginName = plugin.GetName();

                ResourceDictionary pluginCoreDictionary = new ResourceDictionary();
                pluginCoreDictionary.Source = new Uri(String.Format(@"pack://application:,,,/{0};component/PluginResources.xaml", pluginName.Name));
                Resources.MergedDictionaries.Add(pluginCoreDictionary);
            }
        }

        /// <summary>
        /// Uses the config to detect overridden ports.
        /// </summary>
        private void InitialisePorts()
        {
            try
            {
                IConfig config = ConfigFactory.CreateConfig();
                IPortConfig portConfig = ConfigFactory.CreatePortConfig(config.Project);

                int hubConnectionServicesPort;
                if (portConfig.Ports.TryGetValue("RAGProxyHubConnectionService", out hubConnectionServicesPort))
                {
                    GameConnection.ConnectionServicePort = hubConnectionServicesPort;
                }
            }
            catch (Exception ex)
            {
                _log.ToolException(ex, "Configuration parse error.");
            }
        }
        #endregion // Methods

        #region Proxy Connection Methods
        /// <summary>
        /// 
        /// </summary>
        public void InitialiseProxyConnection()
        {
            CancellationToken token = _shutdownTokenSource.Token;

            // Query the connection service to get the game connection we should be connecting to.
            _gameConnection = GetCurrentGameConnection();
            if (_gameConnection == null)
            {
                throw new ArgumentNullException("Unable to retrieve an active game connection from the RAG proxy.  Is the game running and connected?");
            }

            token.ThrowIfCancellationRequested();

            // Create the proxy connection object.
            _proxyConnection = new ProxyConnection("Rag Proxy", _log);
            _proxyConnection.RemotePort = _gameConnection.Port;
            _proxyConnection.ProxyDisconnected += new EventHandler(ProxyDisconnected);

            // Connect to the proxy and attempt to reserve a port for the initial handshake communication.
            ProxyLock proxyLock = _proxyConnection.ConnectToProxy(60000);
            token.ThrowIfCancellationRequested();

            // Create the pipe and start trying to read information from it.
            PipeId ragPipeName = new PipeId("pipe_rag", IPAddress.Any, _proxyConnection.LocalPort, proxyLock.SurrenderListener());

            // Reserve the 50 ports that the named pipes make use of.
            _basePort = ReservePipePorts(ragPipeName.Address, token);

            // Take control of the rag pipe for the handshake.
            _log.Message("Waiting for client to connect to pipe '{0}' on port {1}...", ragPipeName.ToString(), ragPipeName.Port);

            INamedPipe ragPipe = new NamedPipeSocket(_log);
            bool result = ragPipe.Create(ragPipeName, true);
            token.ThrowIfCancellationRequested();
            if (!result)
            {
                _log.Error("Could not connect to pipe '{0}'!", ragPipeName);
                System.Windows.Application.Current.Shutdown(1);
            }
            else
            {
                _log.Message("Connected to pipe '{0}'.", ragPipeName);
                HandshakeResult handshakeData = PerformInitialHandshake(ragPipe);
                token.ThrowIfCancellationRequested();

                // Connect the pipes.
                ConnectPipes(handshakeData.MasterApplication);

                // Send the window handle packet
                SendWindowHandlePacket();

                // Create the packet processors.
                BankPacketProcessor bankProcessor = new BankPacketProcessor(_bankPipe, _log);
                bankProcessor.WidgetCreated += BankPacketProcessor_WidgetCreated;
                _bankPacketProcessor = bankProcessor;

                _outputPacketProcessor = new OutputPacketProcessor(_outputPipe, _log);
                _inputDispatcher = new InputDispatcher(_eventPipe, _log);
            }
        }

        /// <summary>
        /// Tries to set the current game connection.
        /// </summary>
        private GameConnection GetCurrentGameConnection()
        {
            GameConnection conn = null;
            int retryCount = 10;

            while (conn == null && retryCount > 0)
            {
                IList<GameConnection> games = GameConnection.AcquireGameList(_log);
                if (games.Any())
                {
                    IEnumerable<GameConnection> validConnections = games.Where(item => item.Connected);
                    int validChoices = validConnections.Count();

                    if (validChoices == 1)
                    {
                        conn = validConnections.First();
                        break;
                    }
                }

                if (conn == null)
                {
                    _log.Message("No games listed on proxy, retrying...");
                    Thread.Sleep(1000);
                }

                if (--retryCount < 0)
                {
                    /*
                    String msg = "RAG: Can not find any games to connect to on the proxy. Continue trying?";
                    if (MessageBox.Show(msg, "RAG: No games found", MessageBoxButtons.RetryCancel) == DialogResult.Retry)
                    {
                        retryCount = 20;
                    }
                    else
                    {
                        Shutdown();
                        break;
                    }
                    */
                    //Shutdown();
                    break;
                }
            }

            return conn;
        }

        /// <summary>
        /// Attempts to reserve the required number of pipe ports.
        /// </summary>
        /// <param name="address"></param>
        /// <param name="token"></param>
        /// <returns>Port of the first listener.</returns>
        private int ReservePipePorts(IPAddress address, CancellationToken token)
        {
            _log.Message("Reserving pipe ports.");
            int port = DEFAULT_BASE_PIPE_PORT;

            IList<TcpListener> listeners = new List<TcpListener>();

            do
            {
                try
                {
                    TcpListener listener = new TcpListener(address, port + listeners.Count);
                    listener.Start();
                    listeners.Add(listener);
                }
                catch (SocketException)
                {
                    _log.Warning("Could not reserve port {0}", port + listeners.Count);

                    // Release any ports that we have already reserved.
                    StopListeners(listeners);

                    // Update the port and reset the list.
                    port += listeners.Count + 1;
                    listeners.Clear();

                }
            } while (listeners.Count < NUM_PORTS_TO_RESERVE);

            _reservedSockets = listeners.ToArray();
            return ((IPEndPoint)_reservedSockets[0].LocalEndpoint).Port;
        }

        /// <summary>
        /// Stops a list of listeners.
        /// </summary>
        private void StopListeners(IList<TcpListener> listeners)
        {
            foreach (TcpListener listener in listeners)
            {
                listener.Stop();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private HandshakeResult PerformInitialHandshake(INamedPipe pipe)
        {
            _log.Message("Performing initial proxy handshake.");
            Handshaker theHandshaker = new Handshaker(pipe, _basePort, _log);
            HandshakeResult results = theHandshaker.PerformHandshake();

            UpdateTitle(results.MasterApplication);
            return results;
        }

        /// <summary>
        /// Updates the title.
        /// </summary>
        /// <param name="appInfo"></param>
        private void UpdateTitle(ApplicationInfo appInfo)
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => UpdateTitle(appInfo));
                return;
            }

            MainWindowDataContext dc = MainWindow.DataContext as MainWindowDataContext;
            if (dc != null)
            {
                dc.Title = String.Format("{0} @ {1}: {2} {3}", _gameConnection.Platform, _gameConnection.IPAddress, appInfo.Name, appInfo.Arguments);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void ConnectPipes(ApplicationInfo appInfo)
        {
            int offset = (int)SocketOffset.Bank;
            _bankPipeName = new PipeId(appInfo.BankPipeName, IPAddress.Any, _basePort + offset, ClaimSocket(offset));
            _bankPipe = new NamedPipeSocket(_log);
            if (!_bankPipe.Create(_bankPipeName, false))
            {
                throw new Exception("Unable to connect bank pipe.");
            }

            offset = (int)SocketOffset.Output;
            _outputPipeName = new PipeId(appInfo.OutputPipeName, IPAddress.Any, _basePort + offset, ClaimSocket(offset));
            _outputPipe = new NamedPipeSocket(_log);
            if (!_outputPipe.Create(_outputPipeName, false))
            {
                throw new Exception("Unable to connect output pipe.");
            }

            offset = (int)SocketOffset.Events;
            _eventPipeName = new PipeId(appInfo.EventPipeName, IPAddress.Any, _basePort + offset, ClaimSocket(offset));
            _eventPipe = new NamedPipeSocket(_log);
            if (!_eventPipe.Create(_eventPipeName, false))
            {
                throw new Exception("Unable to connect event pipe.");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void SendWindowHandlePacket()
        {
            WidgetGuidAttribute attribute =
                typeof(BankManager).GetCustomAttributes(typeof(WidgetGuidAttribute), false).FirstOrDefault() as WidgetGuidAttribute;
            Debug.Assert(attribute != null, "Bank manager doesn't have the WidgetGuidAttribute assigned as a class attribute.");

            RemotePacketBuilder builder = new RemotePacketBuilder((ushort)BankCommand.User3, attribute.Guid.ToInteger(), 0);
            builder.Write((uint)GetWindowHandle());

            IPacketDispatcher dispatcher = new NamedPipePacketDispatcher(_bankPipe);
            dispatcher.SendPacket(builder.ToRemotePacket());
        }

        /// <summary>
        /// Gets the game viewport window's handle.
        /// </summary>
        /// <returns>The game viewport host's window handle if it exists, otherwise 0.</returns>
        private int GetWindowHandle()
        {
            if (!Dispatcher.CheckAccess())
            {
                return Dispatcher.Invoke<int>(() => GetWindowHandle());
            }
            
            int handle = 0;

            MainWindow mainWindow = this.MainWindow as MainWindow;
            if (mainWindow != null)
            {
                RsGameViewportHost gameViewport = mainWindow.GameViewport;
                if (gameViewport != null)
                {
                    handle = (int)gameViewport.HostHandle;
                }
            }

            return handle;
        }

        /// <summary>
        /// Claims one of the reserved listener sockets.
        /// </summary>
        /// <param name="offset"></param>
        private TcpListener ClaimSocket(int offset)
        {
            Debug.Assert(offset < _reservedSockets.Length, "Requesting port that hasn't been reserved by the TrayApplication.");
            if (offset < 0 || offset >= _reservedSockets.Length)
            {
                throw new ArgumentOutOfRangeException("offset", "Requesting port that hasn't been reserved by the TrayApplication.");
            }

            // Check that the socket hasn't already been claimed.
            if (_reservedSockets[offset] == null)
            {
                throw new ArgumentNullException("offset", "Listener has already been claimed.");
            }

            // Pass back the requested listener.
            TcpListener listener = _reservedSockets[offset];
            _reservedSockets[offset] = null;
            return listener;
        }

        /// <summary>
        /// Task/thread method for updating the bank pipe.
        /// </summary>
        private void UpdateBankPipe()
        {
            _log.Message("Started to update the bank pipe.");
            CancellationToken token = _shutdownTokenSource.Token;

            while (!token.IsCancellationRequested)
            {
                if (!_bankPacketProcessor.Update())
                {
                    Task.Delay(16).Wait();
                }
            }

            token.ThrowIfCancellationRequested();
        }

        /// <summary>
        /// Task/thread method for updating the output pipe.
        /// </summary>
        private void UpdateOutputPipe()
        {
            _log.Message("Started to update the output pipe.");
            CancellationToken token = _shutdownTokenSource.Token;

            while (!token.IsCancellationRequested)
            {
                if (!_outputPacketProcessor.Update())
                {
                    Task.Delay(16).Wait();
                }
            }

            token.ThrowIfCancellationRequested();
        }

        /// <summary>
        /// Task/thread method for updating the event pipe.
        /// </summary>
        private void UpdateEventPipe()
        {
            _log.Message("Started to update the event pipe.");
            CancellationToken token = _shutdownTokenSource.Token;

            while (!token.IsCancellationRequested)
            {
                if (!_inputDispatcher.ProcessQueue())
                {
                    Task.Delay(16).Wait();
                }
            }

            token.ThrowIfCancellationRequested();
        }

        /// <summary>
        /// Task/thread method for processing data that was read off of the bank pipe.
        /// </summary>
        private void ProcessBankPipe()
        {
            _log.Message("Started to process the bank pipe.");
            CancellationToken token = _shutdownTokenSource.Token;

            while (!token.IsCancellationRequested)
            {
                if (!_bankPacketProcessor.Process())
                {
                    Task.Delay(16).Wait();
                }
            }

            token.ThrowIfCancellationRequested();
        }

        /// <summary>
        /// Task/thread method for processing data that was read off of the output pipe.
        /// </summary>
        private void ProcessOutputPipe()
        {
            _log.Message("Started to process the output pipe.");
            CancellationToken token = _shutdownTokenSource.Token;

            while (!token.IsCancellationRequested)
            {
                if (!_outputPacketProcessor.Process())
                {
                    Task.Delay(16).Wait();
                }
            }

            token.ThrowIfCancellationRequested();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="t"></param>
        private void HandleProxyConnectionException(Task t)
        {
            // Inform the user that something went wrong.
            _log.ToolException(t.Exception, "Exception during initial proxy connection!");

            IMessageBoxService msgService = this.GetService<IMessageBoxService>();
            if (msgService == null)
            {
                Debug.Fail("No message box service present.");
                _log.Error("No message box service registered.");
                return;
            }

            // Determine what it was that went wrong.
            String exceptionMessage;
            if (t.Exception is AggregateException)
            {
                exceptionMessage = ((AggregateException)t.Exception).InnerExceptions[0].Message;
            }
            else
            {
                exceptionMessage = t.Exception.Message;
            }

            msgService.Show(
                String.Format("The following error occurred while initialising the connection with the RAG proxy:\r\n\r\n{0}\r\n\r\n" +
                    "RAG is in an invalid state and will now shutdown.", exceptionMessage),
                "Proxy Connection Initialisation Failed",
                System.Windows.MessageBoxButton.OK,
                System.Windows.MessageBoxImage.Error);
            Shutdown(1);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="t"></param>
        private void HandleException(Task t)
        {
            // Inform the user that something went wrong.
            _log.ToolException(t.Exception, "Exception while updating/processing the pipe!");
            OnUnhandledException(t.Exception);

            // The app is no longer in a valid state.  Shut it down.
            Shutdown(1);
        }

        /// <summary>
        /// Utility method for raising the <see cref="ProxyConnected"/> event.
        /// </summary>
        private void RaiseProxyConnected(Task t)
        {
            _log.Message("Proxy connection complete.");

            if (ProxyConnected != null)
            {
                ProxyConnected(this, EventArgs.Empty);
            }
        }
        #endregion // Methods

        #region Event Handlers
        /// <summary>
        /// Event handler for when we get disconnected from the proxy.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProxyDisconnected(object sender, EventArgs e)
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => ProxyDisconnected(sender, e));
                return;
            }

            // Inform the user that we lost the connection to the proxy.
            IMessageBoxService msgService = this.GetService<IMessageBoxService>();
            if (msgService == null)
            {
                Debug.Fail("No message box service present.");
                _log.Error("No message box service registered.");
                return;
            }

            msgService.Show(
                "RAG has lost the connection to the game/proxy and will now close.",
                String.Empty,
                System.Windows.MessageBoxButton.OK,
                System.Windows.MessageBoxImage.Information);

            // Shutdown the application.
            Shutdown();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BankPacketProcessor_WidgetCreated(object sender, WidgetEventArgs e)
        {
            if (e.Widget is BankManager)
            {
                BankManagerViewModel vm = (BankManagerViewModel)_widgetViewModelFactory.CreateViewModel(e.Widget);
                UpdateBankManagerViewModel(vm);
            }
        }

        /// <summary>
        /// Updates the bank manager view model.
        /// </summary>
        /// <param name="vm"></param>
        private void UpdateBankManagerViewModel(BankManagerViewModel vm)
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => UpdateBankManagerViewModel(vm));
                return;
            }
            
            MainWindowDataContext dc = MainWindow.DataContext as MainWindowDataContext;
            if (dc != null)
            {
                Debug.Assert(dc.BankManagerViewModel == null, "Received more than one BankManager widget from the game.");
                dc.BankManagerViewModel = vm;
            }
        }
        #endregion // Proxy Connection Methods
    } // RagInterfaceApp
}
