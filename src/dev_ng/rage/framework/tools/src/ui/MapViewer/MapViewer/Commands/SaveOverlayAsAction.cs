﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml.Linq;
using RSG.Editor;
using RSG.Editor.Controls;
using RSG.MapViewer.Overlays;

namespace MapViewer.Commands
{
    /// <summary>
    /// Action for saving an overlay document as a new template.
    /// </summary>
    public class SaveOverlayAsAction : ButtonAction<IOverlay>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SaveOverlayAsAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public SaveOverlayAsAction(ParameterResolverDelegate<IOverlay> resolver)
            : base(resolver)
        {
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="overlay">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(IOverlay overlay)
        {
            return overlay != null;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="overlay">
        /// The main window.
        /// </param>
        public override void Execute(IOverlay overlay)
        {
            IMessageBoxService messageService = this.GetService<IMessageBoxService>();
            if (messageService == null)
            {
                throw new SmartArgumentNullException(() => messageService);
            }

            ICommonDialogService dialogService = this.GetService<ICommonDialogService>();
            if (dialogService == null)
            {
                throw new SmartArgumentNullException(() => dialogService);
            }

            string filter = "Overlay Template Files (*.overlay)|*.overlay|All Files (*.*)|*.*";
            string saveDestination;
            if (!dialogService.ShowSaveFile(null, null, filter, 0, out saveDestination))
            {
                return;
            }

            // Save the overlay template.
            Util.SaveOverlayTemplate(overlay, saveDestination, messageService);
        }
        #endregion
    }
}
