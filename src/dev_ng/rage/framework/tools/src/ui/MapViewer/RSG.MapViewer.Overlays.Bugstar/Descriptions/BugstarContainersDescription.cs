﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.MapViewer.Overlays.Bugstar.Overlays;

namespace RSG.MapViewer.Overlays.Bugstar.Descriptions
{
    /// <summary>
    /// Overlay description of the bugstar grids overlay.
    /// </summary>
    [Export(typeof(IOverlayDescription))]
    public class BugstarContainersDescription : OverlayDescription<BugstarContainersOverlay>
    {
        #region Constants
        /// <summary>
        /// Unique guid for this overlay definition.
        /// </summary>
        private static readonly Guid Id = new Guid("02D123D1-4AE5-4EA9-8892-F5170DEC1ED3");
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="BugstarContainersDescription"/> class.
        /// </summary>
        public BugstarContainersDescription()
            : base(Id, "Bugstar Containers", "Bugstar", "Shows the maps (with their associated grids) that are available for a configurable bugstar project.")
        {
        }
        #endregion
    }
}
