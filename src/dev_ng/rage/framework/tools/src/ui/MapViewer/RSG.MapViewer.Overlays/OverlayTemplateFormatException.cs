﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.MapViewer.Overlays
{
    /// <summary>
    /// Exception that gets thrown when loading an overlay template and the format isn't
    /// correct.
    /// </summary>
    public class OverlayTemplateFormatException : Exception
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="OverlayTemplateFormatException"/> class using the specified
        /// element name.
        /// </summary>
        /// <param name="elementName"></param>
        public OverlayTemplateFormatException(string elementName)
            : base(String.Format("Overlay template file is missing the '{0}' element.", elementName))
        {
            Data.Add("Element Name", elementName);
        }

        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="OverlayTemplateFormatException"/> class using the specified
        /// element name and message.
        /// </summary>
        /// <param name="elementName"></param>
        /// <param name="message"></param>
        public OverlayTemplateFormatException(string elementName, string message)
            : base(message)
        {
            Data.Add("Element Name", elementName);
        }
        #endregion
    }
}
