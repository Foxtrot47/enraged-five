﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using RSG.Editor.Controls;

namespace MapViewer
{
    /// <summary>
    /// Interaction logic for AddNewOverlayWindow.xaml
    /// </summary>
    public partial class AddNewOverlayWindow : RsWindow
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="AddNewOverlayWindow"/> class.
        /// </summary>
        public AddNewOverlayWindow()
        {
            InitializeComponent();

            this.GroupTreeview.SelectedItemChanged += this.GroupTreeview_SelectionChanged;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Called whenever the selection on the treeview changes.
        /// </summary>
        /// <param name="sender">
        /// The object that this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Controls.SelectionChangedEventArgs data for this event.
        /// </param>
        private void GroupTreeview_SelectionChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            AddNewOverlayViewModel vm = DataContext as AddNewOverlayViewModel;
            if (vm == null)
            {
                throw new ArgumentException("DataContext isn't an AddNewOverlayViewModel object");
            }

            vm.SelectedGroup = (OverlayDescriptionGroupViewModel)e.NewValue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
        #endregion
    }
}
