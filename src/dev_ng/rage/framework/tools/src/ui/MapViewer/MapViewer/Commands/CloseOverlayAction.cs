﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml.Linq;
using RSG.Editor;
using RSG.MapViewer.Overlays;

namespace MapViewer.Commands
{
    /// <summary>
    /// Action which closes an open overlay document.
    /// </summary>
    public class CloseOverlayAction : ButtonAction<OverlayDocumentViewModel>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CloseOverlayAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public CloseOverlayAction(ParameterResolverDelegate<OverlayDocumentViewModel> resolver)
            : base(resolver)
        {
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Create a new command definition for each command associated with this
        /// implementation.
        /// </summary>
        /// <param name="command">
        /// The command to create the definition for.
        /// </param>
        /// <returns>
        /// The new command definition created for the specified command.
        /// </returns>
        public override bool CanExecute(OverlayDocumentViewModel commandParameter)
        {
            return commandParameter != null;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="overlayDocumentVm">
        /// The main window.
        /// </param>
        public override void Execute(OverlayDocumentViewModel overlayDocumentVm)
        {
            IMessageBoxService messageService = this.GetService<IMessageBoxService>();
            if (messageService == null)
            {
                throw new SmartArgumentNullException(() => messageService);
            }

            IOverlay overlay = overlayDocumentVm.Overlay;

            if (overlay.IsModified)
            {
                MessageBoxResult result =
                    messageService.Show(
                    "The overlay has been modified do you want to save it before closing?",
                    null,
                    MessageBoxButton.YesNoCancel);

                if (result == MessageBoxResult.Cancel)
                {
                    return;
                }
                else if (result == MessageBoxResult.Yes)
                {
                    // Save the file.
                    string saveDestination = overlay.FilePath;
                    if (String.IsNullOrEmpty(overlay.FilePath))
                    {
                        ICommonDialogService dialogService = this.GetService<ICommonDialogService>();
                        if (dialogService == null)
                        {
                            throw new SmartArgumentNullException(() => dialogService);
                        }

                        string filter = "Overlay Template Files (*.overlay)|*.overlay|All Files (*.*)|*.*";
                        if (!dialogService.ShowSaveFile(null, null, filter, 0, out saveDestination))
                        {
                            return;
                        }
                    }

                    // Save the overlay template.
                    if (!Util.SaveOverlayTemplate(overlay, saveDestination, messageService))
                    {
                        return;
                    }
                }
            }

            // Remove the overlay from the view.
            MainWindowViewModel mainWindowVm = overlayDocumentVm.MainWindowViewModel;
            mainWindowVm.RemoveOverlayDocument(overlayDocumentVm);
        }
        #endregion
    }
}
