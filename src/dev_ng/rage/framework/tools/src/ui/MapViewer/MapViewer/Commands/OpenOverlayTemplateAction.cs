﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Editor;
using RSG.Editor.Model;
using RSG.MapViewer.Overlays;

namespace MapViewer.Commands
{
    /// <summary>
    /// Action which opens an overlay from a saved out template.
    /// </summary>
    public class OpenOverlayTemplateAction : ButtonAction<MainWindowViewModel>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="OpenOverlayTemplateAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public OpenOverlayTemplateAction(ParameterResolverDelegate<MainWindowViewModel> resolver)
            : base(resolver)
        {
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="mainWindowVm">
        /// The main window.
        /// </param>
        public override void Execute(MainWindowViewModel mainWindowVm)
        {
            // Get the services we want to make use of.
            IMessageBoxService messageService = GetService<IMessageBoxService>();
            if (messageService == null)
            {
                throw new SmartArgumentNullException(() => messageService);
            }

            ICommonDialogService dialogService = this.GetService<ICommonDialogService>();
            if (dialogService == null)
            {
                throw new SmartArgumentNullException(() => dialogService);
            }

            // Let the user choose the file to open.
            string filter = "Overlay Template Files (*.overlay)|*.overlay|All Files (*.*)|*.*";
            string filePath;
            if (!dialogService.ShowOpenFile(null, null, filter, 0, out filePath))
            {
                return;
            }

            // Make sure the file exists on disk.
            FileInfo info = new FileInfo(filePath);
            if (!info.Exists)
            {
                messageService.ShowStandardErrorBox(
                    String.Format("The selected file '{0}' doesn't exist on disk.  " +
                        "Please check the file path to ensure it's correct before trying again.",
                        filePath),
                    null);

                return;
            }

            // Open the overlay template.
            Util.OpenOverlayTemplate(filePath, mainWindowVm, ServiceProvider, messageService);
        }
        #endregion
    }
}
