﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using RSG.Editor;
using RSG.MapViewer.Overlays;
using System.Windows;
using System.ComponentModel;
using RSG.Editor.Controls.MapViewport;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Input;
using RSG.Editor.Model;

namespace MapViewer
{
    /// <summary>
    /// 
    /// </summary>
    public class OverlayDocumentViewModel : NotifyPropertyChangedBase, IWeakEventListener
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="MainWindowViewModel"/> property.
        /// </summary>
        private readonly MainWindowViewModel _mainWindowViewModel;

        /// <summary>
        /// Private field for the <see cref="Overlay"/> property.
        /// </summary>
        private readonly IOverlay _overlay;

        /// <summary>
        /// Private field for the <see cref="ShowDetails"/> property.
        /// </summary>
        private bool _showDetails;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="OverlayDocumentViewModel"/> class
        /// using the specified overlay and background images.
        /// </summary>
        /// <param name="overlay"></param>
        /// <param name="mainWindowViewModel"></param>
        public OverlayDocumentViewModel(IOverlay overlay, MainWindowViewModel mainWindowViewModel)
        {
            _mainWindowViewModel = mainWindowViewModel;
            _overlay = overlay;
            _showDetails = true;

            PropertyChangedEventManager.AddListener(overlay as INotifyPropertyChanged, this, "FileName");
            PropertyChangedEventManager.AddListener(overlay as INotifyPropertyChanged, this, "OverlayDetailsControl");
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the main window view model.
        /// </summary>
        public MainWindowViewModel MainWindowViewModel
        {
            get { return _mainWindowViewModel; }
        }

        /// <summary>
        /// Gets the overlay that this document view model is for.
        /// </summary>
        public IOverlay Overlay
        {
            get { return _overlay; }
        }

        /// <summary>
        /// Retrieves the overlay
        /// </summary>
        public string OverlayName
        {
            get { return _overlay.FileName ?? String.Format("New {0} Overlay", _overlay.Description.Name); }
        }

        /// <summary>
        /// Gets a value indicating that we shouldn't show any overlay details.
        /// </summary>
        public bool HasDetails
        {
            get { return _overlay.OverlayDetailsControl != null; }
        }

        /// <summary>
        /// Gets or sets the value indicating whether the user wishes to see the overlay details on screen.
        /// </summary>
        public bool ShowDetails
        {
            get { return _showDetails; }
            set { SetProperty(ref _showDetails, value, "ShowDetails", "ShowDetailsOnBottom", "ShowDetailsOnRight"); }
        }

        /// <summary>
        /// Gets a value indicating whether we should display the overlay details on the
        /// bottom of the map viewport.
        /// </summary>
        public bool ShowDetailsOnBottom
        {
            get
            {
                return _overlay.OverlayDetailsControl != null &&
                    _showDetails &&
                    _overlay.OverlayDetailsPosition == OverlayDetailsPosition.Bottom;
            }
        }

        /// <summary>
        /// Gets a value indicating whether we should display the overlay details on the
        /// right of the map viewport.
        /// </summary>
        public bool ShowDetailsOnRight
        {
            get
            {
                return _overlay.OverlayDetailsControl != null &&
                    _showDetails &&
                    _overlay.OverlayDetailsPosition == OverlayDetailsPosition.Right;
            }
        }
        #endregion

        #region IWeakEventListener Implementation
        /// <summary>
        /// Receives events from the centralized event manager.
        /// </summary>
        /// <param name="managerType">
        /// The type of the System.Windows.WeakEventManager calling this method.
        /// </param>
        /// <param name="sender">
        /// Object that originated the event.
        /// </param>
        /// <param name="e">
        /// Event data.
        /// </param>
        /// <returns>
        /// True if the listener handled the event; otherwise, false.
        /// </returns>
        public bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
        {
            bool handled = false;

            if (managerType == typeof(PropertyChangedEventManager))
            {
                PropertyChangedEventArgs args = e as PropertyChangedEventArgs;
                if (args != null)
                {
                    if (args.PropertyName == "OverlayDetailsControl")
                    {
                        NotifyPropertyChanged("HideDetails", "ShowDetailsOnRight", "ShowDetailsOnBottom");
                        handled = true;
                    }
                    else if (args.PropertyName == "FileName")
                    {
                        NotifyPropertyChanged("OverlayName");
                        handled = true;
                    }
                }
            }

            return handled;
        }
        #endregion
    }
}
