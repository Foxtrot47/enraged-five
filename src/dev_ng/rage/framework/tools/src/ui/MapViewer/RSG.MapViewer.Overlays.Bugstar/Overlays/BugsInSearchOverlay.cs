﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Xml.Linq;
using RSG.Base.Logging;
using RSG.Editor.Model;
using RSG.Interop.Bugstar;
using RSG.Interop.Bugstar.Organisation;
using RSG.Interop.Bugstar.Search;
using RSG.MapViewer.Overlays.Bugstar.OverlayDetails;
using MapComponents = RSG.Editor.Controls.MapViewport.OverlayComponents;

namespace RSG.MapViewer.Overlays.Bugstar.Overlays
{
    /// <summary>
    /// Overlay that displays bugs that are for a particular mission.
    /// </summary>
    public class BugsInSearchOverlay : BugstarOverlayBase
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Searches"/> property.
        /// </summary>
        private IEnumerable<Search> _searches;

        /// <summary>
        /// Private field for the <see cref="SelectedSearch"/> property.
        /// </summary>
        private Search _selectedSearch;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="BugsInSearchOverlay"/> class
        /// using the specified creation arguments.
        /// </summary>
        /// <param name="creationArgs"></param>
        public BugsInSearchOverlay(OverlayCreationArgs creationArgs)
            : base(creationArgs)
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// The list of searches that the user has for the selected project.
        /// </summary>
        public IEnumerable<Search> Searches
        {
            get { return _searches; }
            private set { SetUndoableProperty(ref _searches, value); }
        }

        /// <summary>
        /// Control that contains all of the overlays details.
        /// </summary>
        public override FrameworkElement OverlayDetailsControl
        {
            get { return new BugsInSearchDetails(); }
        }

        /// <summary>
        /// The currently selected search.
        /// </summary>
        public Search SelectedSearch
        {
            get { return _selectedSearch; }
            set
            {
                if (SetUndoableProperty(ref _selectedSearch, value))
                {
                    OnSelectedSearchChanged();
                }
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Initialises a newly created overlay.
        /// </summary>
        /// <param name="log"></param>
        public override void Initialise(ILog log)
        {
            base.Initialise(log);
            UpdateSearches();
        }

        /// <summary>
        /// Gives overlays a chance to initialise their settings based on a xml element.
        /// </summary>
        /// <param name="settingsElement"></param>
        /// <param name="log"></param>
        protected override void InitialiseSettings(XElement settingsElement, ILog log)
        {
            base.InitialiseSettings(settingsElement, log);
            UpdateSearches();

            XElement searchElement = settingsElement.Element("Search");
            if (searchElement == null)
            {
                throw new OverlayTemplateFormatException("Search");
            }

            if (!String.IsNullOrEmpty(searchElement.Value))
            {
                _selectedSearch = _searches.FirstOrDefault(item => item.Name == searchElement.Value);
                if (_selectedSearch == null)
                {
                    log.Warning("The '{0}' search can't be found in the list returned from bugstar.  Clearing the selected search and continuing to load.",
                        searchElement.Value);
                }
                OnSelectedSearchChanged();
            }
        }

        /// <summary>
        /// Invoked when the currently selected search has changed.
        /// </summary>
        protected void OnSelectedSearchChanged()
        {
            Layers.Clear();

            if (SelectedSearch != null)
            {
                MapComponents.Layer bugsLayer = new MapComponents.Layer("Bug markers", true);

                IList<Bug> bugsInSearch = SelectedSearch.GetBugs("Id,Summary,X,Y,Z");
                foreach (Bug bug in bugsInSearch)
                {
                    MapComponents.Ellipse bugEllipse = new MapComponents.Ellipse(new Point(bug.Location.X, bug.Location.Y), BugMarkerRadius, BugMarkerRadius);
                    bugEllipse.FillBrush = Brushes.Red;
                    bugEllipse.LeftMouseDownCommand = SelectBugCommand;
                    bugEllipse.ScaleFactor = 0.33333333;
                    bugEllipse.Tag = bug;
                    bugEllipse.ToolTip = bug.Summary;

                    bugsLayer.Components.Add(bugEllipse);
                }

                Layers.Add(bugsLayer);
            }
        }

        /// <summary>
        /// Invoked when the currently selected project has changed.
        /// </summary>
        protected override void OnSelectedProjectChanged()
        {
            SelectedSearch = null;
            base.OnSelectedProjectChanged();
            UpdateSearches();
        }

        /// <summary>
        /// Allows overlays to serialise out any settings they wish.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        protected override XElement SettingsToXElement(string name)
        {
            XElement settingsElement = base.SettingsToXElement(name);
            string selectedSearchName = null;
            if (_selectedSearch != null)
            {
                selectedSearchName = _selectedSearch.Name;
            }
            settingsElement.Add(new XElement("Search", selectedSearchName));
            return settingsElement;
        }

        /// <summary>
        /// Updates the list of searches available for the user in the currently selected project.
        /// </summary>
        private void UpdateSearches()
        {
            IEnumerable<Search> searches;

            if (SelectedProject != null)
            {
                User currentUser = SelectedProject.CurrentUser;
                searches = currentUser.PerformSearch(SearchType.PublicSearch);
            }
            else
            {
                searches = Enumerable.Empty<Search>();
            }

            Searches = searches;

            // Set up sorting by name for the searches collection.
            ICollectionView searchesCollectionView = CollectionViewSource.GetDefaultView(Searches);
            searchesCollectionView.SortDescriptions.Add(new SortDescription("Name", ListSortDirection.Ascending));
        }
        #endregion
    }
}
