﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapViewer.Exceptions
{
    /// <summary>
    /// Exception that overlay factories can 
    /// </summary>
    public class OverlayNotFoundException : Exception
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="OverlayNotFoundException"/> class
        /// using the specified message.
        /// </summary>
        /// <param name="message"></param>
        public OverlayNotFoundException(string message, Guid overlayGuid)
            : base(message)
        {
            Data["OverlayGuid"] = overlayGuid;
        }
        #endregion
    }
}
