﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapViewer.Commands
{
    /// <summary>
    /// Action arguments for the <see cref="ToggleOvlerayDetailsAction"/> class.
    /// </summary>
    public class ToggleOverlayDetailsArgs
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="DocumentVm"/> property.
        /// </summary>
        private readonly OverlayDocumentViewModel _documentVm;

        /// <summary>
        /// Private field for the <see cref="ShowDetails"/> property.
        /// </summary>
        private readonly bool _showDetails;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ToggleOverlayDetailsArgs"/>
        /// class using the specified view model and show flag.
        /// </summary>
        /// <param name="vm"></param>
        /// <param name="show"></param>
        public ToggleOverlayDetailsArgs(OverlayDocumentViewModel vm, bool show)
        {
            _documentVm = vm;
            _showDetails = show;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the overlay document view model that we wish to toggle the details view
        /// for.
        /// </summary>
        public OverlayDocumentViewModel DocumentVm
        {
            get { return _documentVm; }
        }

        /// <summary>
        /// Gets the flag indicating whether we wish to show the details view.
        /// </summary>
        public bool ShowDetails
        {
            get { return _showDetails; }
        }
        #endregion
    }
}
