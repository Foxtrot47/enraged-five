﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Linq;
using RSG.Base.Logging;
using RSG.Editor.Controls.MapViewport;
using RSG.Editor.Controls.MapViewport.Annotations;
using RSG.Editor.Controls.MapViewport.OverlayComponents;
using RSG.Editor.Model;

namespace RSG.MapViewer.Overlays
{
    /// <summary>
    /// Defines the properties that an overlay needs to expose.
    /// </summary>
    public interface IOverlay : IUndoEngineProvider
    {
        #region Properties
        /// <summary>
        /// Collection of annotations that this overlay exposes.
        /// </summary>
        AnnotationCollection Annotations { get; }

        /// <summary>
        /// Background that are available for use with this overlay.
        /// </summary>
        MapBackgroundImageCollection BackgroundImages { get; }

        /// <summary>
        /// The description associated with this overlay.
        /// </summary>
        IOverlayDescription Description { get; }
        
        /// <summary>
        /// Retrieves the filename for this overlay (if it's a template).
        /// </summary>
        string FileName { get; }

        /// <summary>
        /// Retrieves the filepath to this overlay template on disk.
        /// </summary>
        string FilePath { get; set; }
        
        /// <summary>
        /// Gets or sets a value indicating whether this overlay currently contains
        /// modified unsaved data.
        /// </summary>
        bool IsModified { get; }

        /// <summary>
        /// Collection of layers that make up the data for this overlay.
        /// </summary>
        ObservableCollection<Layer> Layers { get; }

        /// <summary>
        /// Control that contains all of the overlays details.
        /// </summary>
        FrameworkElement OverlayDetailsControl { get; }

        /// <summary>
        /// Where the overlay details will be positioned in relation to the map viewport.
        /// </summary>
        OverlayDetailsPosition OverlayDetailsPosition { get; }

        /// <summary>
        /// The currently selected background image.
        /// </summary>
        MapBackgroundImage SelectedBackgroundImage { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Initialises a newly created overlay.
        /// </summary>
        /// <param name="log"></param>
        void Initialise(ILog log);

        /// <summary>
        /// Initialises an overlay that has been loaded from a template.
        /// </summary>
        /// <param name="overlayElement">
        /// The xml element from which to load the overlay data.
        /// </param>
        /// <param name="log"></param>
        void Initialise(XElement overlayElement, ILog log);

        /// <summary>
        /// Saves the overlay details options to an xml element.
        /// </summary>
        /// <returns></returns>
        XElement ToXElement(string name);
        #endregion
    }
}
