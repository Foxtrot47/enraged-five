﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.MapViewer.Overlays
{
    /// <summary>
    /// Abstract base class that overlay descriptions can make use of.
    /// </summary>
    public abstract class OverlayDescription<T> : IOverlayDescription where T : IOverlay
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Guid"/> property.
        /// </summary>
        private readonly Guid _guid;

        /// <summary>
        /// Private field for the <see cref="Name"/> property.
        /// </summary>
        private readonly string _name;

        /// <summary>
        /// Private field for the <see cref="Group"/> property.
        /// </summary>
        private readonly string _group;

        /// <summary>
        /// Private field for the <see cref="Description"/> property.
        /// </summary>
        private readonly string _description;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Overlay"/> class using the 
        /// provided path, description, guid and type.
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="name"></param>
        /// <param name="group"></param>
        /// <param name="description"></param>
        protected OverlayDescription(Guid guid, string name, string group, string description)
        {
            _guid = guid;
            _name = name;
            _group = group;
            _description = description;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Name of this overlay.
        /// </summary>
        public string Name
        {
            get { return _name; }
        }
        
        /// <summary>
        /// Group this overlay is in.
        /// e.g. /Generic/Maps
        /// </summary>
        public string Group
        {
            get { return _group; }
        }

        /// <summary>
        /// Description to display for this overlay.
        /// </summary>
        public string Description
        {
            get { return _description; }
        }

        /// <summary>
        /// Unique guid for this overlay.
        /// </summary>
        public Guid Guid
        {
            get { return _guid; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Creates a new overlay instance based off of this description.
        /// </summary>
        /// <returns></returns>
        public virtual IOverlay CreateOverlayInstance(OverlayCreationArgs creationArgs)
        {
            return (IOverlay)Activator.CreateInstance(typeof(T), creationArgs);
        }
        #endregion
    }
}
