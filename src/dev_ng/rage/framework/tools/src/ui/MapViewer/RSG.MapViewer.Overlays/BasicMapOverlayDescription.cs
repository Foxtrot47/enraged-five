﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.MapViewer.Overlays
{
    /// <summary>
    /// Definition for the basic map overlay.
    /// </summary>
    [Export(typeof(IOverlayDescription))]
    public sealed class BasicMapOverlayDescription : OverlayDescription<BasicMapOverlay>
    {
        #region Constants
        /// <summary>
        /// Unique guid for this overlay definition.
        /// </summary>
        private static readonly Guid Id = new Guid("0D89C394-DCBE-4DEF-8336-3E69020F2FCB");
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="BasicMapOverlayDescription"/> class.
        /// </summary>
        public BasicMapOverlayDescription()
            : base(Id, "Basic Map", "Standard", "Overlay that just shows the map.")
        {
        }
        #endregion
    }
}
