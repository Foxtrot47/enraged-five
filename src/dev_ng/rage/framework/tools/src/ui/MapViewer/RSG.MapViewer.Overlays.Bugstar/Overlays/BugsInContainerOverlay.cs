﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using MapComponents = RSG.Editor.Controls.MapViewport.OverlayComponents;
using RSG.Interop.Bugstar;
using RSG.Interop.Bugstar.Game;
using RSG.MapViewer.Overlays.Bugstar.OverlayDetails;
using RSG.Editor;
using System.Diagnostics;
using RSG.Base.Logging;
using System.Xml.Linq;

namespace RSG.MapViewer.Overlays.Bugstar.Overlays
{
    /// <summary>
    /// Overlay that displays bugs that are for a particular container.
    /// </summary>
    public class BugsInContainerOverlay : BugstarOverlayBase
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Containers"/> property.
        /// </summary>
        private IEnumerable<MapGrid> _containers;

        /// <summary>
        /// Private field for the <see cref="SelectedContainer"/> property.
        /// </summary>
        private MapGrid _selectedContainer;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="BugsInContainerOverlay"/> class
        /// using the specified creation arguments.
        /// </summary>
        /// <param name="creationArgs"></param>
        public BugsInContainerOverlay(OverlayCreationArgs creationArgs)
            : base(creationArgs)
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// The list of containers that are in the currently selected map.
        /// </summary>
        public IEnumerable<MapGrid> Containers
        {
            get { return _containers; }
            private set { SetUndoableProperty(ref _containers, value); }
        }

        /// <summary>
        /// Control that contains all of the overlays details.
        /// </summary>
        public override FrameworkElement OverlayDetailsControl
        {
            get { return new BugsInContainerDetails(); }
        }

        /// <summary>
        /// The currently selected container.
        /// </summary>
        public MapGrid SelectedContainer
        {
            get { return _selectedContainer; }
            set
            {
                if (SetUndoableProperty(ref _selectedContainer, value))
                {
                    OnSelectedContainerChanged();
                }
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Initialises a newly created overlay.
        /// </summary>
        /// <param name="log"></param>
        public override void Initialise(ILog log)
        {
            base.Initialise(log);
            OnSelectedBackgroundImageChanged(false);
        }

        /// <summary>
        /// Gives overlays a chance to initialise their settings based on a xml element.
        /// </summary>
        /// <param name="settingsElement"></param>
        /// <param name="log"></param>
        protected override void InitialiseSettings(XElement settingsElement, ILog log)
        {
            base.InitialiseSettings(settingsElement, log);
            OnSelectedBackgroundImageChanged(false);

            XElement containerElement = settingsElement.Element("Container");
            if (containerElement == null)
            {
                throw new OverlayTemplateFormatException("Container");
            }

            if (!String.IsNullOrEmpty(containerElement.Value))
            {
                _selectedContainer = _containers.FirstOrDefault(item => item.Name == containerElement.Value);
                if (_selectedContainer == null)
                {
                    log.Warning("The '{0}' container can't be found in the list returned from bugstar.  Clearing the selected container and continuing load.",
                        containerElement.Value);
                    return;
                }
                OnSelectedContainerChanged();
            }
        }

        /// <summary>
        /// Invoked when the currently selected bugstar group has changed.
        /// </summary>
        protected void OnSelectedContainerChanged()
        {
            Layers.Clear();

            if (SelectedContainer != null)
            {
                MapComponents.Layer bugsLayer = new MapComponents.Layer("Bug markers", true);

                List<Bug> bugsInContainer = SelectedContainer.GetBugs("Id,Summary,X,Y,Z");
                foreach (Bug bug in bugsInContainer)
                {
                    MapComponents.Ellipse bugEllipse = new MapComponents.Ellipse(new Point(bug.Location.X, bug.Location.Y), BugMarkerRadius, BugMarkerRadius);
                    bugEllipse.FillBrush = Brushes.Red;
                    bugEllipse.LeftMouseDownCommand = SelectBugCommand;
                    bugEllipse.ScaleFactor = 0.33333333;
                    bugEllipse.Tag = bug;
                    bugEllipse.ToolTip = bug.Summary;

                    bugsLayer.Components.Add(bugEllipse);
                }

                Layers.Add(bugsLayer);
            }
        }

        /// <summary>
        /// Updates the list of containers available for the currently selected project.
        /// </summary>
        /// <param name="performingUndoEvent"></param>
        protected override void OnSelectedBackgroundImageChanged(bool performingUndoEvent)
        {
            if (performingUndoEvent)
            {
                return;
            }

            SelectedContainer = null;
            base.OnSelectedBackgroundImageChanged(performingUndoEvent);
            UpdateContainers();
        }

        /// <summary>
        /// Invoked when the currently selected project has changed.
        /// </summary>
        protected override void OnSelectedProjectChanged()
        {
            SelectedContainer = null;
            base.OnSelectedProjectChanged();
        }

        /// <summary>
        /// Allows overlays to serialise out any settings they wish.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        protected override XElement SettingsToXElement(string name)
        {
            XElement settingsElement = base.SettingsToXElement(name);

            string selectedContainerName = null;
            if (_selectedContainer != null)
            {
                selectedContainerName = _selectedContainer.Name;
            }
            settingsElement.Add(new XElement("Container", selectedContainerName));

            return settingsElement;
        }

        /// <summary>
        /// Updates the list of containers available for the currently selected map.
        /// </summary>
        private void UpdateContainers()
        {
            IEnumerable<MapGrid> containers;

            BugstarMapBackgroundImage selectedBugstarBgImage = SelectedBackgroundImage as BugstarMapBackgroundImage;
            if (selectedBugstarBgImage != null)
            {
                containers = selectedBugstarBgImage.Map.Grids;
            }
            else
            {
                containers = Enumerable.Empty<MapGrid>();
            }

            Containers = containers;

            // Set up sorting by name for the containers collection.
            ICollectionView containersCollectionView = CollectionViewSource.GetDefaultView(Containers);
            containersCollectionView.SortDescriptions.Add(new SortDescription("Name", ListSortDirection.Ascending));
        }
        #endregion
    }
}
