﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.MapViewer.Overlays
{
    /// <summary>
    /// Enumeration for where the overlay details will be shown in relation to the map
    /// viewport.
    /// </summary>
    public enum OverlayDetailsPosition
    {
        /// <summary>
        /// Overlay details are positioned to the right of map.
        /// </summary>
        Right,

        /// <summary>
        /// Overlay details are positioned at the bottom of the map.
        /// </summary>
        Bottom
    }
}
