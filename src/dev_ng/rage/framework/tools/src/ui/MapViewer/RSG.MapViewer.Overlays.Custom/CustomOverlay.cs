﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.MapViewer.Overlays.Custom
{
    /// <summary>
    /// 
    /// </summary>
    public class CustomOverlay : Overlay
    {
        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="creationArgs"></param>
        public CustomOverlay(OverlayCreationArgs creationArgs)
            : base(creationArgs)
        {
        }
        #endregion
    }
}
