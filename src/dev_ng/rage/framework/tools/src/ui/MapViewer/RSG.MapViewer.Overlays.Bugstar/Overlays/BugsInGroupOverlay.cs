﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Xml.Linq;
using RSG.Base.Logging;
using RSG.Editor.Controls.MapViewport;
using RSG.Editor.Controls.MapViewport.OverlayComponents;
using RSG.Interop.Bugstar;
using RSG.Interop.Bugstar.Organisation;
using RSG.Interop.Bugstar.Search;
using RSG.MapViewer.Overlays.Bugstar.OverlayDetails;
using BugstarGame = RSG.Interop.Bugstar.Game;

namespace RSG.MapViewer.Overlays.Bugstar.Overlays
{
    /// <summary>
    /// Overlay that displays bugs in a group for a particular project/map.
    /// </summary>
    public class BugsInGroupOverlay : BugstarOverlayBase
    {
        #region Constants
        /// <summary>
        /// The bug fields to request when doing a query for the bugs that are part of
        /// the selected group.
        /// </summary>
        private static readonly BugField[] _bugstarRequestFields = new BugField[]
            {
                BugField.Id,
                BugField.Summary,
                BugField.Location,
                BugField.Grid,
                BugField.State
            };

        /// <summary>
        /// Colour to use when filling in containers that only contain fixed bugs.
        /// </summary>
        private static readonly Color _fixedColor = Color.FromArgb(128, 0, 255, 0);

        /// <summary>
        /// Brush to use when filling in containers that only contain fixed bugs.
        /// </summary>
        private static readonly Brush _fixedBrush = new SolidColorBrush(_fixedColor);

        /// <summary>
        /// Colour to use when filling in containers that contain open bugs.
        /// </summary>
        private static readonly Color _openColor = Color.FromArgb(128, 255, 0, 0);

        /// <summary>
        /// Brush to use when filling in containers that contain open bugs.
        /// </summary>
        private static readonly Brush _openBrush = new SolidColorBrush(_openColor);
        #endregion

        #region Fields
        /// <summary>
        /// Private field for the <see cref="Containers"/> property.
        /// </summary>
        private IEnumerable<BugstarGame.MapGrid> _containers;

        /// <summary>
        /// Private field fort he <see cref="GroupByContainer"/> property.
        /// </summary>
        private bool _groupByContainer;

        /// <summary>
        /// Private field for the <see cref="Groups"/> property.
        /// </summary>
        private IEnumerable<Group> _groups;

        /// <summary>
        /// Private field for the <see cref="SelectedGroup"/> property.
        /// </summary>
        private Group _selectedGroup;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="BugsInGroupOverlay"/> class
        /// using the specified creation arguments.
        /// </summary>
        /// <param name="creationArgs"></param>
        public BugsInGroupOverlay(OverlayCreationArgs creationArgs)
            : base(creationArgs)
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// The list of containers that are in the currently selected map.
        /// </summary>
        private IEnumerable<BugstarGame.MapGrid> Containers
        {
            get { return _containers; }
            set { SetUndoableProperty(ref _containers, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating that the user wishes to see colour coded
        /// containers instead of the bugs themselves.
        /// </summary>
        public bool GroupByContainer
        {
            get { return _groupByContainer; }
            set
            {
                if (SetUndoableProperty(ref _groupByContainer, value))
                {
                    OnSelectedGroupChanged();
                }
            }
        }

        /// <summary>
        /// The list of groups the user has available to them.
        /// </summary>
        public IEnumerable<Group> Groups
        {
            get { return _groups; }
            private set { SetUndoableProperty(ref _groups, value); }
        }

        /// <summary>
        /// Control that contains all of the overlays details.
        /// </summary>
        public override FrameworkElement OverlayDetailsControl
        {
            get { return new BugsInGroupDetails(); }
        }

        /// <summary>
        /// The currently selected group.
        /// </summary>
        public Group SelectedGroup
        {
            get { return _selectedGroup; }
            set
            {
                if (SetUndoableProperty(ref _selectedGroup, value))
                {
                    OnSelectedGroupChanged();
                }
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Initialises a newly created overlay.
        /// </summary>
        /// <param name="log"></param>
        public override void Initialise(ILog log)
        {
            base.Initialise(log);
            UpdateGroups();
            UpdateContainers();
        }

        /// <summary>
        /// Gives overlays a chance to initialise their settings based on a xml element.
        /// </summary>
        /// <param name="settingsElement"></param>
        /// <param name="log"></param>
        protected override void InitialiseSettings(XElement settingsElement, ILog log)
        {
            base.InitialiseSettings(settingsElement, log);
            UpdateGroups();
            UpdateContainers();

            XElement groupElement = settingsElement.Element("Group");
            if (groupElement == null)
            {
                throw new OverlayTemplateFormatException("Group");
            }

            _selectedGroup = _groups.FirstOrDefault(item => item.Name == groupElement.Value);
            if (_selectedGroup == null)
            {
                log.Warning("The '{0}' group can't be found in the list returned from bugstar.  Clearing the selected group and continuing load.",
                    groupElement.Value);
            }

            XElement groupByContainerElement = settingsElement.Element("GroupByContainer");
            if (groupByContainerElement == null)
            {
                throw new OverlayTemplateFormatException("GroupByContainer");
            }
            _groupByContainer = Boolean.Parse(groupByContainerElement.Value);
            
            OnSelectedGroupChanged();
        }

        /// <summary>
        /// Invoked when the currently selected bugstar group has changed.
        /// </summary>
        protected void OnSelectedGroupChanged()
        {
            Layers.Clear();

            if (SelectedGroup != null)
            {
                IList<Bug> bugsInGroup = SelectedGroup.GetBugs(_bugstarRequestFields);

                if (_groupByContainer)
                {
                    IDictionary<uint, BugstarGame.MapGrid> gridLookup = _containers.ToDictionary(item => item.Id);

                    Layer outlineLayer = new Layer("Container outlines", true);
                    Layer labelLayer = new Layer("Container labels", false);

                    foreach (IGrouping<uint, Bug> bugsPerContainer in bugsInGroup.GroupBy(item => item.Grid))
                    {
                        BugstarGame.MapGrid grid;
                        if (!gridLookup.TryGetValue(bugsPerContainer.Key, out grid))
                        {
                            continue;
                        }

                        bool allBugsFixed = !bugsPerContainer.Any(bug => bug.IsOpen);

                        // Calculate the center of the grid.
                        Point gridCenter = CalculateGridCenter(grid);
                        Text gridNameText = new Text(grid.Name, gridCenter, PositionOrigin.Center);
                        gridNameText.ScaleFactor = 0.33333333;
                        labelLayer.Components.Add(gridNameText);

                        Polygon gridOutline = new Polygon(grid.Points.Select(item => new Point(item.X, item.Y)));
                        gridOutline.FillBrush = (allBugsFixed ? _fixedBrush : _openBrush);
                        outlineLayer.Components.Add(gridOutline);
                    }

                    Layers.Add(outlineLayer);
                    Layers.Add(labelLayer);
                }
                else
                {
                    Layer bugsLayer = new Layer("Bug markers", true);

                    foreach (Bug bug in bugsInGroup)
                    {
                        Ellipse bugEllipse = new Ellipse(new Point(bug.Location.X, bug.Location.Y), BugMarkerRadius, BugMarkerRadius);
                        bugEllipse.FillBrush = Brushes.Red;
                        bugEllipse.LeftMouseDownCommand = SelectBugCommand;
                        bugEllipse.ScaleFactor = 0.33333333;
                        bugEllipse.Tag = bug;
                        bugEllipse.ToolTip = bug.Summary;

                        bugsLayer.Components.Add(bugEllipse);
                    }

                    Layers.Add(bugsLayer);
                }
            }
        }

        /// <summary>
        /// Invoked when the currently selected project has changed.
        /// </summary>
        protected override void OnSelectedProjectChanged()
        {
            SelectedGroup = null;
            base.OnSelectedProjectChanged();
            UpdateGroups();
            UpdateContainers();
        }

        /// <summary>
        /// Allows overlays to serialise out any settings they wish.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        protected override XElement SettingsToXElement(string name)
        {
            XElement settingsElement = base.SettingsToXElement(name);
            string selectedGroupName = null;
            if (_selectedGroup != null)
            {
                selectedGroupName = _selectedGroup.Name;
            }
            settingsElement.Add(new XElement("Group", selectedGroupName));
            settingsElement.Add(new XElement("GroupByContainer", _groupByContainer));
            return settingsElement;
        }

        /// <summary>
        /// Updates the list of containers available for the currently selected map.
        /// </summary>
        private void UpdateContainers()
        {
            IEnumerable<BugstarGame.MapGrid> containers;

            BugstarMapBackgroundImage selectedBugstarBgImage = SelectedBackgroundImage as BugstarMapBackgroundImage;
            if (selectedBugstarBgImage != null)
            {
                containers = selectedBugstarBgImage.Map.Grids;
            }
            else
            {
                containers = Enumerable.Empty<BugstarGame.MapGrid>();
            }

            Containers = containers;
        }

        /// <summary>
        /// Updates the list of groups available for the currently selected project.
        /// </summary>
        private void UpdateGroups()
        {
            IEnumerable<Group> groups;

            if (SelectedProject != null)
            {
                User currentUser = SelectedProject.CurrentUser;
                groups = currentUser.Groups;
            }
            else
            {
                groups = Enumerable.Empty<Group>();
            }

            Groups = groups;

            // Set up sorting by name for the projects collection.
            ICollectionView groupsCollectionView = CollectionViewSource.GetDefaultView(Groups);
            groupsCollectionView.SortDescriptions.Add(new SortDescription("Name", ListSortDirection.Ascending));
        }
        #endregion
    }
}
