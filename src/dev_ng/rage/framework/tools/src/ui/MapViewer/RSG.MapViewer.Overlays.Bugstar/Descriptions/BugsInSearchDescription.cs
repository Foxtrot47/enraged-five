﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.MapViewer.Overlays.Bugstar.Overlays;

namespace RSG.MapViewer.Overlays.Bugstar.Descriptions
{
    /// <summary>
    /// 
    /// </summary>
    [Export(typeof(IOverlayDescription))]
    public class BugsInSearchDescription : OverlayDescription<BugsInSearchOverlay>
    {
        #region Constants
        /// <summary>
        /// Unique guid for this overlay definition.
        /// </summary>
        private static readonly Guid Id = new Guid("E795FAAD-BCBE-4627-934E-1E3666B49EAB");
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="BugsInSearchDescription"/> class.
        /// </summary>
        public BugsInSearchDescription()
            : base(Id, "Bugs in Search", "Bugstar", "Shows the location of bugs from a specific bugstar search.")
        {
        }
        #endregion
    }
}
