﻿using RSG.Editor;
using RSG.Editor.Model;

namespace MapViewer.Commands
{
    /// <summary>
    /// The action logic that is responsible for executing the undo command on the active
    /// document.
    /// </summary>
    public class UndoOverlayAction : ButtonAction<IUndoEngineProvider>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="UndoOverlayAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public UndoOverlayAction(
            ParameterResolverDelegate<IUndoEngineProvider> resolver)
            : base(resolver)
        {
        }
        #endregion

        #region Methods
        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="parameter">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(IUndoEngineProvider parameter)
        {
            if (parameter == null)
            {
                return false;
            }

            return parameter.UndoEngine != null && parameter.UndoEngine.CanUndo;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="parameter">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(IUndoEngineProvider parameter)
        {
            parameter.UndoEngine.Undo();
        }
        #endregion
    }
}
