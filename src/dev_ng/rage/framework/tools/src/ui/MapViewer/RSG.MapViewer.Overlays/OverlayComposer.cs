﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace RSG.MapViewer.Overlays
{
    /// <summary>
    /// In charge of discovering and composing the overlay factories.
    /// </summary>
    public class OverlayComposer : IDisposable
    {
        #region Constants
        /// <summary>
        /// Directory relative to the application which contains the pluggable overlay assemblies.
        /// </summary>
        private const string _overlaysDirectory = "Overlays";
        #endregion

        #region Fields
        /// <summary>
        /// Composition container used by MEF for discovering overlay factories.
        /// </summary>
        private readonly CompositionContainer _compositionContainer;

#pragma warning disable 0649
        /// <summary>
        /// Private field for the <see cref="OverlayDescriptions"/> property.
        /// </summary>
        [ImportMany(typeof(IOverlayDescription))]
        private IEnumerable<IOverlayDescription> _overlayDescriptions;
#pragma warning restore 0649
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="OverlayComposer"/> class.
        /// </summary>
        public OverlayComposer()
        {
            Assembly executingAssembly = Assembly.GetExecutingAssembly();
            string assemblyDirecotry = Path.GetDirectoryName(executingAssembly.Location);
            string overlayAssemblyDirectory = Path.Combine(assemblyDirecotry, _overlaysDirectory);

            AggregateCatalog catalog = new AggregateCatalog();
            catalog.Catalogs.Add(new AssemblyCatalog(executingAssembly));

            if (Directory.Exists(overlayAssemblyDirectory))
            {
                catalog.Catalogs.Add(new DirectoryCatalog(overlayAssemblyDirectory));
            }

            _compositionContainer = new CompositionContainer(catalog);
            _compositionContainer.ComposeParts(this);
        }
        #endregion

        #region Properties
        /// <summary>
        /// List of supported overlay factories.
        /// </summary>
        public IEnumerable<IOverlayDescription> OverlayDescriptions
        {
            get { return _overlayDescriptions; }
        }
        #endregion

        #region IDisposable Interface
        /// <summary>
        /// Disposes of the composition container.
        /// </summary>
        public void Dispose()
        {
            _compositionContainer.Dispose();
        }
        #endregion
    }
}
