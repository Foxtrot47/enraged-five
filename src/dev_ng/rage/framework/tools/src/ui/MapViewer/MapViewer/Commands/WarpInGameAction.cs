﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using RSG.Editor;
using RSG.Editor.Controls.MapViewport;
using RSG.Rag.Clients;
using RSG.Rag.Contracts.Data;

namespace MapViewer.Commands
{
    /// <summary>
    /// The action logic responsible for warping to a particular location on the map
    /// viewport in the game instance connected to Rag.
    /// </summary>
    /// <remarks>
    /// Note that CanExecute isn't overridden due to the fact that it would need
    /// to make a round trip to Rag to determine if there were any connected games
    /// which is too slow to do in the Can method.
    /// </remarks>
    public class WarpInGameAction : ButtonAction<Point>
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="WarpInGameAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public WarpInGameAction(ParameterResolverDelegate<Point> resolver)
            : base(resolver)
        {
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="warpLocation">
        /// The location to warp to.
        /// </param>
        public override void Execute(Point warpLocation)
        {
            IMessageBoxService messageService = GetService<IMessageBoxService>();
            if (messageService == null)
            {
                throw new ArgumentNullException("messageService");
            }

            try
            {
                IEnumerable<GameConnection> gameConnections = RagClientFactory.GetGameConnections();
                GameConnection defaultConnection = gameConnections.FirstOrDefault(item => item.DefaultConnection);

                if (defaultConnection != null)
                {
                    WidgetClient widgetClient = RagClientFactory.CreateWidgetClient(defaultConnection);
                    widgetClient.WriteStringWidget("_LiveEdit_/Bugstar/Warp Player x y z h vx vy vz", String.Format("{0} {1}", warpLocation.X, warpLocation.Y));
                    widgetClient.PressButton("_LiveEdit_/Bugstar/Warp now");
                }
                else
                {
                    messageService.Show("Please ensure the game is running and connected to the Rag proxy.", "No game connections detected");
                }
            }
            catch (Exception)
            {
                messageService.Show("Please ensure the Rag proxy is running and a game is connected.", "Problem connecting to Rag");
            }
        }
        #endregion
    }
}
