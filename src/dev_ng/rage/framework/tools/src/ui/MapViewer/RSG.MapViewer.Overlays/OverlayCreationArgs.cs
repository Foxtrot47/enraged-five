﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Editor;
using RSG.Editor.Controls.MapViewport;
using RSG.Editor.Model;

namespace RSG.MapViewer.Overlays
{
    /// <summary>
    /// Cotnains the arguments that can be used when creating a new overlay via its
    /// description.
    /// </summary>
    public class OverlayCreationArgs
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="DefaultBackgroundImages"/> property.
        /// </summary>
        private readonly MapBackgroundImageCollection _defaultBackgroundImages;

        /// <summary>
        /// Private field for the <see cref="FilePath"/> property.
        /// </summary>
        private readonly string _filePath;

        /// <summary>
        /// Private field for the <see cref="OverlayDescription"/> property.
        /// </summary>
        private readonly IOverlayDescription _overlayDescription;

        /// <summary>
        /// Private field for the <see cref="ServiceProvider"/> property.
        /// </summary>
        private readonly ICommandServiceProvider _serviceProvider;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="OverlayCreationArgs"/> class
        /// using the provided undo redo engine.
        /// </summary>
        /// <param name="description"></param>
        /// <param name="serviceProvider"></param>
        /// <param name="defaultBackgroundImages"></param>
        /// <param name="filePath"></param>
        public OverlayCreationArgs(
            IOverlayDescription description,
            ICommandServiceProvider serviceProvider,
            MapBackgroundImageCollection defaultBackgroundImages,
            string filePath = null)
        {
            _defaultBackgroundImages = defaultBackgroundImages;
            _filePath = filePath;
            _overlayDescription = description;
            _serviceProvider = serviceProvider;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the default set of background images available for overlays to use.
        /// </summary>
        public MapBackgroundImageCollection DefaultBackgroundImages
        {
            get { return _defaultBackgroundImages; }
        }

        /// <summary>
        /// Gets the filepath that this overlay is going to be loaded from.
        /// </summary>
        public string FilePath
        {
            get { return _filePath; }
        }

        /// <summary>
        /// Gets the overlay description that we are using to create the overlay.
        /// </summary>
        public IOverlayDescription OverlayDescription
        {
            get { return _overlayDescription; }
        }

        /// <summary>
        /// Gets the service provider.
        /// </summary>
        public ICommandServiceProvider ServiceProvider
        {
            get { return _serviceProvider; }
        }
        #endregion
    }
}
