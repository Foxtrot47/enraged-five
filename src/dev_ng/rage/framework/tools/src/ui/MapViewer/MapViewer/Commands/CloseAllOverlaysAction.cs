﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using RSG.Editor;
using RSG.MapViewer.Overlays;

namespace MapViewer.Commands
{
    /// <summary>
    /// Action which closes all of the open overlays.
    /// </summary>
    public class CloseAllOverlaysAction : ButtonAction<MainWindowViewModel>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CloseAllOverlaysAction"/> class.
        /// </summary>
        /// <param name="serviceProvider">
        /// The service provider that is used by this action to perform application specific
        /// operations, for example selecting a file to open.
        /// </param>
        public CloseAllOverlaysAction(ICommandServiceProvider serviceProvider)
            : base(serviceProvider)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="CloseAllOverlaysAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public CloseAllOverlaysAction(ParameterResolverDelegate<MainWindowViewModel> resolver)
            : base(resolver)
        {
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Create a new command definition for each command associated with this
        /// implementation.
        /// </summary>
        /// <param name="mainWindowVm">
        /// The command to create the definition for.
        /// </param>
        /// <returns>
        /// The new command definition created for the specified command.
        /// </returns>
        public override bool CanExecute(MainWindowViewModel mainWindowVm)
        {
            return mainWindowVm.OverlayDocuments.Any();
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="mainWindowVm">
        /// The main window.
        /// </param>
        public override void Execute(MainWindowViewModel mainWindowVm)
        {
            IMessageBoxService messageService = this.GetService<IMessageBoxService>();
            if (messageService == null)
            {
                throw new SmartArgumentNullException(() => messageService);
            }

            ICommonDialogService dialogService = this.GetService<ICommonDialogService>();
            if (dialogService == null)
            {
                throw new SmartArgumentNullException(() => dialogService);
            }

            // Allow the user to save any open overlays.
            foreach (OverlayDocumentViewModel item in mainWindowVm.OverlayDocuments)
            {
                IOverlay overlay = item.Overlay;

                if (overlay.IsModified)
                {
                    MessageBoxResult result =
                        messageService.Show(
                        String.Format("Save overlay '{0}'?", item.OverlayName),
                        "Save",
                        MessageBoxButton.YesNoCancel);
                    if (result == MessageBoxResult.No)
                    {
                        continue;
                    }
                    else if (result == MessageBoxResult.Cancel)
                    {
                        throw new OperationCanceledException();
                    }

                    string saveDestination = overlay.FilePath;
                    if (String.IsNullOrEmpty(saveDestination))
                    {
                        string filter = "Overlay Template Files (*.overlay)|*.overlay|All Files (*.*)|*.*";
                        if (!dialogService.ShowSaveFile(null, null, filter, 0, out saveDestination))
                        {
                            throw new OperationCanceledException();
                        }
                    }

                    // Save the overlay template.
                    Util.SaveOverlayTemplate(overlay, saveDestination, messageService);
                }
            }

            // If we've got this far we are safe to close all the overlays.
            mainWindowVm.RemoveAllOverlayDocuments();
        }
        #endregion
    }
}
