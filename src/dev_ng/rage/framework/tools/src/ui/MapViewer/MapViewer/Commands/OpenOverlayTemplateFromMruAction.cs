﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using RSG.Editor;
using RSG.Editor.Controls;

namespace MapViewer.Commands
{
    /// <summary>
    /// Action which opens an overlay from a saved out template.
    /// </summary>
    public class OpenOverlayTemplateFromMruAction : MruCommandAction<MainWindowViewModel>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="OpenOverlayTemplateFromMruAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public OpenOverlayTemplateFromMruAction(ParameterResolverDelegate<MainWindowViewModel> resolver)
            : base(resolver)
        {
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="mainWindowVm">
        /// The command parameter that has been requested.
        /// </param>
        /// <param name="filePath">
        /// The path sent with the MRU command.
        /// </param>
        /// <param name="additional">
        /// The additional information for the path property sent with the command.
        /// </param>
        public override void Execute(MainWindowViewModel mainWindowVm, String filePath, String[] additional)
        {
            // Get the services we want to make use of.
            IMessageBoxService messageService = GetService<IMessageBoxService>();
            if (messageService == null)
            {
                throw new SmartArgumentNullException(() => messageService);
            }

            // Make sure the file exists on disk.
            FileInfo info = new FileInfo(filePath);
            if (!info.Exists)
            {
                messageService.ShowStandardErrorBox(
                    String.Format("Unable to open the overlay template as the file '{0}' no longer exist on disk.", filePath),
                    null);
                return;
            }

            // Open the overlay template.
            Util.OpenOverlayTemplate(filePath, mainWindowVm, ServiceProvider, messageService);
        }
        #endregion
    }
}
