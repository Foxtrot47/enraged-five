﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.MapViewer.Overlays.Bugstar.Overlays;

namespace RSG.MapViewer.Overlays.Bugstar.Descriptions
{
    /// <summary>
    /// 
    /// </summary>
    [Export(typeof(IOverlayDescription))]
    public class BugsInGroupDescription : OverlayDescription<BugsInGroupOverlay>
    {
        #region Constants
        /// <summary>
        /// Unique guid for this overlay definition.
        /// </summary>
        private static readonly Guid Id = new Guid("6FA062E7-B052-491D-9F2B-090862E4BA4C");
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="BugsInGroupDescription"/> class.
        /// </summary>
        public BugsInGroupDescription()
            : base(Id, "Bugs in Group", "Bugstar", "Shows the location of bugs from a specific bugstar group.")
        {
        }
        #endregion
    }
}
