﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using RSG.Editor;
using RSG.Editor.SharedCommands;

namespace MapViewer
{
    /// <summary>
    /// Contains the routed commands that can be used by the MapViewer application.
    /// </summary>
    public static class MapViewerCommands
    {
        #region Fields
        /// <summary>
        /// String table resource manager for getting at the command related strings.
        /// </summary>
        private static readonly CommandResourceManager _commandResourceManager =
            new CommandResourceManager(
                "MapViewer.Resources.CommandStringTable",
                typeof(MapViewerCommands).Assembly);

        /// <summary>
        /// The private cache of System.Windows.Input.RoutedCommand objects.
        /// </summary>
        private static readonly RockstarRoutedCommand[] _internalCommands =
            new RockstarRoutedCommand[Enum.GetValues(typeof(CommandId)).Length];
        #endregion

        #region Enumerations
        /// <summary>
        /// Defines all of the command identifiers for the different map viewer commands.
        /// </summary>
        private enum CommandId
        {
            /// <summary>
            /// Used to identifier the <see cref="MapViewerCommands.AddNewOverlay"/>
            /// routed command.
            /// </summary>
            AddNewOverlay,

            /// <summary>
            /// Used to identifier the <see cref="MapViewportCommands.HideOverlayDetails"/>
            /// routed command.
            /// </summary>
            HideOverlayDetails,

            /// <summary>
            /// Used to identifier the <see cref="MapViewerCommands.OpenOverlayTemplate"/>
            /// routed command.
            /// </summary>
            OpenOverlayTemplate,

            /// <summary>
            /// Used to identifier the <see cref="MapViewportCommands.ShowOverlayDetails"/>
            /// routed command.
            /// </summary>
            ShowOverlayDetails,

            /// <summary>
            /// Used to identifier the <see cref="MapViewerCommands.WarpInGame"/>
            /// routed command.
            /// </summary>
            WarpInGame
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the value that represents the add new overlay command.
        /// </summary>
        public static RockstarRoutedCommand AddNewOverlay
        {
            get { return EnsureCommandExists(CommandId.AddNewOverlay); }
        }

        /// <summary>
        /// Gets the value that represents the hide overlay details command.
        /// </summary>
        public static RockstarRoutedCommand HideOverlayDetails
        {
            get { return EnsureCommandExists(CommandId.HideOverlayDetails, MapViewerIcons.HideDetails); }
        }

        /// <summary>
        /// Gets the value that represents the open overlay template command.
        /// </summary>
        public static RockstarRoutedCommand OpenOverlayTemplate
        {
            get { return EnsureCommandExists(CommandId.OpenOverlayTemplate); }
        }

        /// <summary>
        /// Gets the value that represents the show overlay details command.
        /// </summary>
        public static RockstarRoutedCommand ShowOverlayDetails
        {
            get { return EnsureCommandExists(CommandId.ShowOverlayDetails, MapViewerIcons.ShowDetails); }
        }

        /// <summary>
        /// Gets the value that represents the warp in game command.
        /// </summary>
        public static RockstarRoutedCommand WarpInGame
        {
            get { return EnsureCommandExists(CommandId.WarpInGame); }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Creates a new System.Windows.Input.RoutedCommand object that is associated with the
        /// specified identifier.
        /// </summary>
        /// <param name="id">
        /// The identifier for the command to create.
        /// </param>
        /// <param name="provider">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        /// <returns>
        /// The newly create System.Windows.Input.RoutedCommand object.
        /// </returns>
        private static RockstarRoutedCommand CreateCommand(String id, BitmapSource icon)
        {
            string name = _commandResourceManager.GetName(id);
            string category = _commandResourceManager.GetCategory(id);
            string description = _commandResourceManager.GetDescription(id);
            InputGestureCollection gestures = _commandResourceManager.GetGestures(id);

            return new RockstarRoutedCommand(
                name, typeof(MapViewerCommands), gestures, category, description, icon);
        }

        /// <summary>
        /// Makes sure the internal value for the command associated with the specified
        /// identifier exists and returns it.
        /// </summary>
        /// <param name="commandId">
        /// The identifier for the command that should be created and returned.
        /// </param>
        /// <param name="icon">
        /// The icon that the command should be using.
        /// </param>
        /// <returns>
        /// The <see cref="RSG.Editor.RockstarRoutedCommand"/> object associated with the
        /// specified identifier.
        /// </returns>
        private static RockstarRoutedCommand EnsureCommandExists(CommandId commandId, BitmapSource icon = null)
        {
            int commandIndex = (int)commandId;
            if (commandIndex < -1 || commandIndex >= _internalCommands.Length)
            {
                return null;
            }

            RockstarRoutedCommand command = _internalCommands[commandIndex];
            if (command == null)
            {
                lock (_internalCommands)
                {
                    if (command == null)
                    {
                        command = CreateCommand(commandId.ToString(), icon);
                        _internalCommands[commandIndex] = command;
                    }
                }
            }

            return command;
        }
        #endregion
    }
}
