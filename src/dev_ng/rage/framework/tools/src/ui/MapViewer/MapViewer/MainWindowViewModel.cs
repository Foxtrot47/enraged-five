﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Math;
using RSG.Editor;
using RSG.Editor.Controls;
using RSG.Editor.Controls.Dock;
using RSG.Editor.Controls.Dock.ViewModel;
using RSG.Editor.Controls.MapViewport;
using RSG.Editor.Controls.MapViewport.Annotations;
using RSG.Editor.Controls.MapViewport.OverlayComponents;
using RSG.Editor.Model;
using RSG.MapViewer.Overlays;

namespace MapViewer
{
    /// <summary>
    /// 
    /// </summary>
    public class MainWindowViewModel : NotifyPropertyChangedBase
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="ActiveOverlay"/> property.
        /// </summary>
        private OverlayDocumentViewModel _activeOverlay;

        /// <summary>
        /// Private field for the <see cref="BackgrounImages"/> property.
        /// </summary>
        public MapBackgroundImageCollection _defaultBackgroundImages;

        /// <summary>
        /// Private field for the <see cref="OverlayDescriptions"/> property.
        /// </summary>
        public readonly IEnumerable<IOverlayDescription> _overlayDescriptions;

        /// <summary>
        /// Private field for the <see cref="OverlayDocuments"/> property.
        /// </summary>
        private readonly ObservableCollection<OverlayDocumentViewModel> _overlayDocuments;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MainWindowViewModel"/> class
        /// using the specified overlay descriptions.
        /// </summary>
        /// <param name="overlayDescriptions"></param>
        public MainWindowViewModel(IEnumerable<IOverlayDescription> overlayDescriptions)
        {
            CreateMapImagesFromConfig();

            _overlayDescriptions = overlayDescriptions;
            _overlayDocuments = new ObservableCollection<OverlayDocumentViewModel>();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the overlay that is currently shown in the map viewer.
        /// </summary>
        public OverlayDocumentViewModel ActiveOverlay
        {
            get { return _activeOverlay; }
            set { SetProperty(ref _activeOverlay, value); }
        }

        /// <summary>
        /// Gets or sets the default list of background images that overlays can make use of.
        /// </summary>
        public MapBackgroundImageCollection DefaultBackgroundImages
        {
            get { return _defaultBackgroundImages; }
            private set { SetProperty(ref _defaultBackgroundImages, value); }
        }

        /// <summary>
        /// Gets the list of overlay descriptions that the application knows about.
        /// </summary>
        public IEnumerable<IOverlayDescription> OverlayDescriptions
        {
            get { return _overlayDescriptions; }
        }

        /// <summary>
        /// Gets the list of overlay documents that are currently shown.
        /// </summary>
        public IEnumerable<OverlayDocumentViewModel> OverlayDocuments
        {
            get { return _overlayDocuments; }
        }

        /// <summary>
        /// Gets a value indicating whether we should display the help text.
        /// </summary>
        public bool ShowHelpText
        {
            get { return !_overlayDocuments.Any(); }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Adds an overlay document to the collection of open documents.
        /// </summary>
        /// <param name="document"></param>
        public void AddOverlayDocument(OverlayDocumentViewModel document)
        {
            _overlayDocuments.Add(document);
            ActiveOverlay = document;
            NotifyPropertyChanged("ShowHelpText");

            // Check whether we should add the file to the MRU list.
            if (!String.IsNullOrEmpty(document.Overlay.FilePath))
            {
                RsApplication app = Application.Current as RsApplication;
                if (app != null)
                {
                    app.AddToMruList("Overlay Templates", document.Overlay.FilePath, new string[0]);
                }
            }
        }

        /// <summary>
        /// Loads the default list of background images from the maps config file.
        /// </summary>
        private void CreateMapImagesFromConfig()
        {
            try
            {
                XDocument doc = XDocument.Load(@".\Data\maps.xml");
                _defaultBackgroundImages = new MapBackgroundImageCollection(doc.Element("maps"));
            }
            catch (System.Exception)
            {
                // TODO: Inform the user that we weren't able to read the maps.xml config file.
            }
        }

        /// <summary>
        /// Removes all open overlay documents.
        /// </summary>
        public void RemoveAllOverlayDocuments()
        {
            _overlayDocuments.Clear();
            NotifyPropertyChanged("ShowHelpText");
        }

        /// <summary>
        /// Removes an overlay document to the collection of open documents.
        /// </summary>
        /// <param name="document"></param>
        public void RemoveOverlayDocument(OverlayDocumentViewModel document)
        {
            _overlayDocuments.Remove(document);
            NotifyPropertyChanged("ShowHelpText");
        }
        #endregion
    }
}
