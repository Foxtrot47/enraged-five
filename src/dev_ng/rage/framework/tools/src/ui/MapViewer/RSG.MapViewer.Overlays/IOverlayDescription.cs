﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Editor.Model;

namespace RSG.MapViewer.Overlays
{
    /// <summary>
    /// Definition of an overlay.
    /// </summary>
    public interface IOverlayDescription
    {
        #region Properties
        /// <summary>
        /// Unique guid for this overlay.
        /// </summary>
        Guid Guid { get; }

        /// <summary>
        /// Name of this overlay.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Group this overlay is in.
        /// e.g. /Generic/Maps
        /// </summary>
        string Group { get; }

        /// <summary>
        /// Description to display for this overlay.
        /// </summary>
        string Description { get; }
        #endregion

        #region Methods
        /// <summary>
        /// Creates a new overlay instance based off of this description.
        /// </summary>
        /// <param name="creationArgs"></param>
        /// <returns></returns>
        IOverlay CreateOverlayInstance(OverlayCreationArgs creationArgs);
        #endregion
    }
}
