﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RSG.Editor.Controls;

namespace RSG.MapViewer.Overlays.Bugstar.OverlayDetails
{
    /// <summary>
    /// Interaction logic for BugsInGroupDetails.xaml
    /// </summary>
    public partial class BugsInGroupDetails : RsUserControl
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="BugsInGroupDetails"/> class.
        /// </summary>
        public BugsInGroupDetails()
        {
            InitializeComponent();
        }
        #endregion
    }
}
