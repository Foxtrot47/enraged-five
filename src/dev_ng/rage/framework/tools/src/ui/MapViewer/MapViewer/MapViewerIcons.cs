﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace MapViewer
{
    /// <summary>
    /// Provides static properties that give access to common icons that can be used throughout
    /// an application.
    /// </summary>
    public static class MapViewerIcons
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="HideDetails"/> property.
        /// </summary>
        private static BitmapSource _hideDetails;

        /// <summary>
        /// The private field used for the <see cref="ShowDetails"/> property.
        /// </summary>
        private static BitmapSource _showDetails;

        /// <summary>
        /// A generic object that provides thread safety access to the image sources.
        /// </summary>
        private static object _syncRoot = new object();
        #endregion

        #region Properties
        /// <summary>
        /// Gets the icon that shows an icon that represents layers.
        /// </summary>
        public static BitmapSource HideDetails
        {
            get
            {
                if (_hideDetails == null)
                {
                    lock (_syncRoot)
                    {
                        if (_hideDetails == null)
                        {
                            _hideDetails = EnsureLoaded("hideDetails16.png");
                        }
                    }
                }

                return _hideDetails;
            }
        }

        /// <summary>
        /// Gets the icon that shows an icon that represents layers.
        /// </summary>
        public static BitmapSource ShowDetails
        {
            get
            {
                if (_showDetails == null)
                {
                    lock (_syncRoot)
                    {
                        if (_showDetails == null)
                        {
                            _showDetails = EnsureLoaded("showDetails16.png");
                        }
                    }
                }

                return _showDetails;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Ensures that the specified image source is loaded with the specified resource name.
        /// </summary>
        /// <param name="source">
        /// The image source that should be loaded.
        /// </param>
        /// <param name="resourceName">
        /// The name of the resource the loaded image source should set as its source.
        /// </param>
        private static BitmapSource EnsureLoaded(String resourceName)
        {
            String assemblyName = typeof(MapViewerIcons).Assembly.GetName().Name;
            String bitmapPath = String.Format(CultureInfo.InvariantCulture,
                "pack://application:,,,/{0};component/Resources/{1}", assemblyName, resourceName);
            Uri uri = new Uri(bitmapPath, UriKind.RelativeOrAbsolute);
            return new BitmapImage(uri);
        }
        #endregion
    }
}
