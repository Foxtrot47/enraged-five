﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.MapViewer.Overlays;

namespace MapViewer
{
    /// <summary>
    /// 
    /// </summary>
    public class OverlayDescriptionViewModel
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Description"/> property.
        /// </summary>
        private readonly IOverlayDescription _description;
        #endregion

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        public OverlayDescriptionViewModel(IOverlayDescription description)
        {
            _description = description;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the name of the overlay.
        /// </summary>
        public string Name
        {
            get { return _description.Name; }
        }

        /// <summary>
        /// Gets the textual description of the overlay.
        /// </summary>
        public string Description
        {
            get { return _description.Description; }
        }

        /// <summary>
        /// Gets the overlay description itself.
        /// </summary>
        public IOverlayDescription OverlayDescription
        {
            get { return _description; }
        }
        #endregion
    }
}
