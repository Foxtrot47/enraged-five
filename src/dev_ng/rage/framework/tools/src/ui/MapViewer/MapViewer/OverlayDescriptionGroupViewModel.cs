﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapViewer
{
    /// <summary>
    /// 
    /// </summary>
    public class OverlayDescriptionGroupViewModel
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Name"/> property.
        /// </summary>
        private readonly string _name;

        /// <summary>
        /// Private field for the <see cref="FullPath"/> property.
        /// </summary>
        private readonly string _fullPath;

        /// <summary>
        /// Private field for the <see cref="SubGroups"/> property.
        /// </summary>
        private readonly Collection<OverlayDescriptionGroupViewModel> _subGroups;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="OverlayDescriptionGroupViewModel"/>
        /// class.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="path"></param>
        /// <param name="subPaths"></param>
        public OverlayDescriptionGroupViewModel(string name, string path, IEnumerable<string[]> subPaths)
        {
            _name = name;
            _fullPath = path;
            _subGroups = new Collection<OverlayDescriptionGroupViewModel>();

            foreach (IGrouping<string, string[]> group in subPaths.GroupBy(item => item[0]))
            {
                IList<string[]> subGroups =
                    group.Where(item => item.Length > 1)
                        .Select(item => item.Skip(1).ToArray())
                        .ToList();

                _subGroups.Add(new OverlayDescriptionGroupViewModel(
                    group.Key,
                    String.Format("{0}/{1}", _fullPath, group.Key),
                    subGroups));
            }
        }
        #endregion

        #region Properties
        /// <summary>
        /// Returns the groups name.
        /// </summary>
        public string Name
        {
            get { return _name; }
        }

        /// <summary>
        /// Returns the full path to the group.
        /// </summary>
        public string FullPath
        {
            get { return _fullPath; }
        }

        /// <summary>
        /// 
        /// </summary>
        public IReadOnlyCollection<OverlayDescriptionGroupViewModel> SubGroups
        {
            get { return _subGroups; }
        }
        #endregion
    }
}
