﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using RSG.Editor.Controls.MapViewport;
using RSG.Interop.Bugstar.Game;

namespace RSG.MapViewer.Overlays.Bugstar
{
    /// <summary>
    /// Custom bugstar based map background image.
    /// </summary>
    public class BugstarMapBackgroundImage : MapBackgroundImage
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Map"/> property.
        /// </summary>
        private readonly Map _map;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="BugstarMapBackgroundImage"/>
        /// class using the provided bugstar map.
        /// </summary>
        /// <param name="map"></param>
        public BugstarMapBackgroundImage(Map map)
            : base(CalculateWorldExtents(map), map.Image, map.Image, map.Name)
        {
            _map = map;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the bugstar map object that this background image is for.
        /// </summary>
        public Map Map
        {
            get { return _map; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns the world extents for the specified bugstar map.
        /// </summary>
        /// <param name="map"></param>
        /// <returns></returns>
        private static Rect CalculateWorldExtents(Map map)
        {
            Point lowerLeft = new Point(map.LowerLeft.X, map.LowerLeft.Y);
            Point upperRight = new Point(map.UpperRight.X, map.UpperRight.Y);
            return new Rect(lowerLeft, upperRight);
        }
        #endregion
    }
}
