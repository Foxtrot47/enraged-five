﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.MapViewer.Overlays;

namespace RSG.MapViewer.Overlays.Custom
{
    /// <summary>
    /// 
    /// </summary>
    [Export(typeof(IOverlayDescription))]
    public class CustomOverlayDescription : OverlayDescription<CustomOverlay>
    {
        #region Constants
        /// <summary>
        /// Unique guid for this overlay definition.
        /// </summary>
        private static readonly Guid Id = new Guid("0C05EE2F-1D97-403F-9934-AA3015AE3186");
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="CustomOverlayDescription"/> class.
        /// </summary>
        public CustomOverlayDescription()
            : base(Id, "Custom per Grid Colours", "Custom", "Creates a custom overlay.")
        {
        }
        #endregion
    }
}
