﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Editor;

namespace MapViewer.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public class ToggleOverlayDetailsAction : ButtonAction<ToggleOverlayDetailsArgs>
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="ForceTextureExportOptionAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public ToggleOverlayDetailsAction(ParameterResolverDelegate<ToggleOverlayDetailsArgs> resolver)
            : base(resolver)
        {
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(ToggleOverlayDetailsArgs args)
        {
            if (args.DocumentVm == null)
            {
                return false;
            }

            return args.DocumentVm.ShowDetails != args.ShowDetails;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(ToggleOverlayDetailsArgs args)
        {
            args.DocumentVm.ShowDetails = args.ShowDetails;
        }
        #endregion
    }
}
