﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Xml.Linq;
using RSG.Base.Logging;
using RSG.Interop.Bugstar;
using RSG.Interop.Bugstar.Game;
using RSG.MapViewer.Overlays.Bugstar.OverlayDetails;
using MapComponents = RSG.Editor.Controls.MapViewport.OverlayComponents;

namespace RSG.MapViewer.Overlays.Bugstar.Overlays
{
    /// <summary>
    /// Overlay that displays bugs that are for a particular mission.
    /// </summary>
    public class BugsInMissionOverlay : BugstarOverlayBase
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Missions"/> property.
        /// </summary>
        private IEnumerable<Mission> _missions;

        /// <summary>
        /// Private field for the <see cref="SelectedMission"/> property.
        /// </summary>
        private Mission _selectedMission;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="BugsInMissionOverlay"/> class
        /// using the specified creation arguments.
        /// </summary>
        /// <param name="creationArgs"></param>
        public BugsInMissionOverlay(OverlayCreationArgs creationArgs)
            : base(creationArgs)
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// The list of mission that exist for the selected project.
        /// </summary>
        public IEnumerable<Mission> Missions
        {
            get { return _missions; }
            private set { SetUndoableProperty(ref _missions, value); }
        }

        /// <summary>
        /// Control that contains all of the overlays details.
        /// </summary>
        public override FrameworkElement OverlayDetailsControl
        {
            get { return new BugsInMissionDetails(); }
        }

        /// <summary>
        /// The currently selected mission.
        /// </summary>
        public Mission SelectedMission
        {
            get { return _selectedMission; }
            set
            {
                if (SetUndoableProperty(ref _selectedMission, value))
                {
                    OnSelectedMissionChanged();
                }
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Initialises a newly created overlay.
        /// </summary>
        /// <param name="log"></param>
        public override void Initialise(ILog log)
        {
            base.Initialise(log);
            UpdateMissions();
        }

        /// <summary>
        /// Gives overlays a chance to initialise their settings based on a xml element.
        /// </summary>
        /// <param name="settingsElement"></param>
        /// <param name="log"></param>
        protected override void InitialiseSettings(XElement settingsElement, ILog log)
        {
            base.InitialiseSettings(settingsElement, log);
            UpdateMissions();

            XElement missionElement = settingsElement.Element("Mission");
            if (missionElement == null)
            {
                throw new OverlayTemplateFormatException("Mission");
            }

            _selectedMission = _missions.FirstOrDefault(item => item.Name == missionElement.Value);
            if (_selectedMission == null)
            {
                log.Warning("The '{0}' mission can't be found in the list returned from bugstar.  Clearing the selected mission and continuing load.",
                    missionElement.Value);
            }
            OnSelectedMissionChanged();
        }

        /// <summary>
        /// Invoked when the currently selected mission has changed.
        /// </summary>
        protected void OnSelectedMissionChanged()
        {
            Layers.Clear();

            if (SelectedMission != null)
            {
                MapComponents.Layer bugsLayer = new MapComponents.Layer("Bug markers", true);

                List<Bug> bugsInMission = SelectedMission.GetBugs("Id,Summary,X,Y,Z");
                foreach (Bug bug in bugsInMission)
                {
                    MapComponents.Ellipse bugEllipse = new MapComponents.Ellipse(new Point(bug.Location.X, bug.Location.Y), BugMarkerRadius, BugMarkerRadius);
                    bugEllipse.FillBrush = Brushes.Red;
                    bugEllipse.LeftMouseDownCommand = SelectBugCommand;
                    bugEllipse.ScaleFactor = 0.33333333;
                    bugEllipse.Tag = bug;
                    bugEllipse.ToolTip = bug.Summary;

                    bugsLayer.Components.Add(bugEllipse);
                }

                Layers.Add(bugsLayer);
            }
        }

        /// <summary>
        /// Invoked when the currently selected project has changed.
        /// </summary>
        protected override void OnSelectedProjectChanged()
        {
            SelectedMission = null;
            base.OnSelectedProjectChanged();
            UpdateMissions();
        }

        /// <summary>
        /// Allows overlays to serialise out any settings they wish.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        protected override XElement SettingsToXElement(string name)
        {
            XElement settingsElement = base.SettingsToXElement(name);
            string selectedMissionName = null;
            if (_selectedMission != null)
            {
                selectedMissionName = _selectedMission.Name;
            }
            settingsElement.Add(new XElement("Mission", selectedMissionName));
            return settingsElement;
        }

        /// <summary>
        /// Updates the list of missions available for the currently selected project.
        /// </summary>
        private void UpdateMissions()
        {
            IEnumerable<Mission> missions;

            if (SelectedProject != null)
            {
                missions = SelectedProject.Missions;
            }
            else
            {
                missions = Enumerable.Empty<Mission>();
            }

            Missions = missions;

            // Set up sorting by name for the missions collection.
            ICollectionView missionsCollectionView = CollectionViewSource.GetDefaultView(Missions);
            missionsCollectionView.SortDescriptions.Add(new SortDescription("Name", ListSortDirection.Ascending));
        }
        #endregion
    }
}
