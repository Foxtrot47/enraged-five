﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using RSG.Base.Math;
using RSG.Editor;
using RSG.Editor.Controls.Bugstar;
using RSG.Editor.Controls.MapViewport;
using MapComponents = RSG.Editor.Controls.MapViewport.OverlayComponents;
using RSG.Interop.Bugstar;
using RSG.Interop.Bugstar.Game;
using RSG.Interop.Bugstar.Organisation;
using System.Diagnostics;
using RSG.Base.Logging;
using System.Xml.Linq;
using RSG.Editor.Model;

namespace RSG.MapViewer.Overlays.Bugstar.Overlays
{
    /// <summary>
    /// Base class for bugstar related overlays.
    /// </summary>
    public abstract class BugstarOverlayBase : Overlay
    {
        #region Constants
        /// <summary>
        /// Radius to use when rendering a bug on the map viewport.
        /// </summary>
        protected const double BugMarkerRadius = 10.0;
        #endregion

        #region Fields
        /// <summary>
        /// Reference to the bugstar service.
        /// </summary>
        private readonly IBugstarService _bugstarService;

        /// <summary>
        /// Private field for the <see cref="Projects"/> property.
        /// </summary>
        private readonly IEnumerable<Project> _projects;

        /// <summary>
        /// Private field for the <see cref="SelectBugCommand"/> property.
        /// </summary>
        private readonly ICommand _selectBugCommand;

        /// <summary>
        /// Private field for the <see cref="SelectedProject"/> property.
        /// </summary>
        private Project _selectedProject;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="BugstarOvlerayBase"/> class
        /// using the specified creation arguments.
        /// </summary>
        /// <param name="creationArgs"></param>
        protected BugstarOverlayBase(OverlayCreationArgs creationArgs)
            : base(creationArgs)
        {
            _bugstarService = creationArgs.ServiceProvider.GetService<IBugstarService>();
            if (_bugstarService == null)
            {
                throw new SmartArgumentNullException(() => _bugstarService);
            }

            // Get the available list of projects and sort them in the UI by their name.
            _projects = _bugstarService.GetAvailableProjects();

            ICollectionView projectsCollectionView = CollectionViewSource.GetDefaultView(Projects);
            projectsCollectionView.SortDescriptions.Add(new SortDescription("Name", ListSortDirection.Ascending));

            // Create the command that gets fired when a bug is pressed.
            _selectBugCommand = new RelayCommand<MapComponents.Component>(OnSelectBug, CanSelectBug);
        }
        #endregion

        #region Properties
        /// <summary>
        /// List of bugstar projects that the user has access to.
        /// </summary>
        public IEnumerable<Project> Projects
        {
            get { return _projects; }
        }

        /// <summary>
        /// Gets the command that is executed when the user clicks on one of the bugs.
        /// </summary>
        protected ICommand SelectBugCommand
        {
            get { return _selectBugCommand; }
        }

        /// <summary>
        /// The currently selected bugstar project.
        /// </summary>
        public Project SelectedProject
        {
            get { return _selectedProject; }
            set
            {
                bool performingEvent = UndoEngine.PerformingEvent;

                using (new UndoRedoBatch(UndoEngine))
                {
                    if (SetUndoableProperty(ref _selectedProject, value) && !performingEvent)
                    {
                        OnSelectedProjectChanged();
                    }
                }
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Finds the center of the map grid that is passed in.
        /// </summary>
        /// <param name="grid"></param>
        /// <returns></returns>
        protected Point CalculateGridCenter(RSG.Interop.Bugstar.Game.MapGrid grid)
        {
            Rect bounds = Rect.Empty;
            foreach (Vector2f point in grid.Points)
            {
                bounds.Union(new Point(point.X, point.Y));
            }

            return new Point(bounds.X + bounds.Width / 2.0, bounds.Y + bounds.Height / 2.0);
        }

        /// <summary>
        /// Called to determine whether the accept edit command logic can be run.
        /// </summary>
        /// <param name="commandParameter"></param>
        /// <returns></returns>
        private bool CanSelectBug(MapComponents.Component commandParameter)
        {
            return commandParameter != null;
        }

        /// <summary>
        /// Initialises a newly created overlay.
        /// </summary>
        /// <param name="log"></param>
        public override void Initialise(ILog log)
        {
            _selectedProject = _projects.FirstOrDefault(item => item.Id == _bugstarService.ProjectId);

            // Shouldn't really happen, but just in case try and fall back on the first project.
            if (_selectedProject == null)
            {
                log.Warning("The project id ({0}) for the installed tools doesn't exist in the data returned from bugstar.  " +
                    "Either you don't have access to this project or it doesn't exist.", _bugstarService.ProjectId);
                _selectedProject = _projects.FirstOrDefault();
            }

            // Update the images that are available to the user.
            UpdateBackgroundImages();

            // Allow the base class to perform it's initialisation.
            base.Initialise(log);
        }

        /// <summary>
        /// Gives overlays a chance to initialise their settings based on a xml element.
        /// </summary>
        /// <param name="settingsElement"></param>
        /// <param name="log"></param>
        protected override void InitialiseSettings(XElement settingsElement, ILog log)
        {
            XElement projectElement = settingsElement.Element("Project");
            if (projectElement == null)
            {
                throw new OverlayTemplateFormatException("Project");
            }

            _selectedProject = _projects.FirstOrDefault(item => item.Name == projectElement.Value);
            if (_selectedProject == null)
            {
                log.Warning("The '{0}' project can't be found in the list returned from bugstar.  " +
                    "Either you don't have access to it or it doesn't exist.",
                    projectElement.Value);
                _selectedProject = _projects.FirstOrDefault(item => item.Id == _bugstarService.ProjectId);
            }

            // Update the list of available background images and call the base to select the appropriate one.
            UpdateBackgroundImages();
            base.InitialiseSettings(settingsElement, log);
        }

        /// <summary>
        /// Executed when the accept edit command is invoked.
        /// </summary>
        /// <param name="commandParameter"></param>
        private void OnSelectBug(MapComponents.Component commandParameter)
        {
            Bug bug = commandParameter.Tag as Bug;
            if (bug != null)
            {
                Process.Start(String.Format("url:bugstar:{0}", bug.Id));
            }
        }

        /// <summary>
        /// Invoked when the currently selected project has changed.
        /// </summary>
        protected virtual void OnSelectedProjectChanged()
        {
            SelectedBackgroundImage = null;
            UpdateBackgroundImages();
            SelectedBackgroundImage = BackgroundImages.FirstOrDefault();
        }

        /// <summary>
        /// Allows overlays to serialise out any settings they wish.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        protected override XElement SettingsToXElement(string name)
        {
            XElement settingsElement = base.SettingsToXElement(name);
            if (_selectedProject != null)
            {
                settingsElement.Add(new XElement("Project", _selectedProject.Name));
            }
            return settingsElement;
        }

        /// <summary>
        /// Updates the background images based on the project that is selected.
        /// </summary>
        protected void UpdateBackgroundImages()
        {
            MapBackgroundImageCollection collection = new MapBackgroundImageCollection();

            if (_selectedProject != null)
            {
                // Create a background image for each map.
                foreach (Map map in _selectedProject.Maps)
                {
                    MapBackgroundImage backgroundImage = new BugstarMapBackgroundImage(map);
                    collection.Add(backgroundImage);
                }
            }

            // Set the new collection of images and select the first.
            UndoableBackgroundImages = collection;
        }
        #endregion
    }
}
