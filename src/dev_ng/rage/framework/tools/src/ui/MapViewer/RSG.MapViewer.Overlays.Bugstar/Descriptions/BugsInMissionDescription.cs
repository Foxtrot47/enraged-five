﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.MapViewer.Overlays.Bugstar.Overlays;

namespace RSG.MapViewer.Overlays.Bugstar.Descriptions
{
    /// <summary>
    /// 
    /// </summary>
    [Export(typeof(IOverlayDescription))]
    public class BugsInMissionDescription : OverlayDescription<BugsInMissionOverlay>
    {
        #region Constants
        /// <summary>
        /// Unique guid for this overlay definition.
        /// </summary>
        private static readonly Guid Id = new Guid("8A57B094-D311-4B06-B830-0BB1F95D660B");
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="BugsInMissionDescription"/> class.
        /// </summary>
        public BugsInMissionDescription()
            : base(Id, "Bugs in Mission", "Bugstar", "Shows the location of bugs in a specific bugstar mission.")
        {
        }
        #endregion
    }
}
