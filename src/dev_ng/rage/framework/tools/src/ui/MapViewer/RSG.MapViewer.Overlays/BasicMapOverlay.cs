﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Editor.Controls.MapViewport;
using RSG.Editor.Controls.MapViewport.Annotations;
using RSG.Editor.Controls.MapViewport.OverlayComponents;

namespace RSG.MapViewer.Overlays
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class BasicMapOverlay : Overlay
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="BasicMapOverlay"/> class.
        /// </summary>
        /// <param name="creationArgs"></param>
        public BasicMapOverlay(OverlayCreationArgs creationArgs)
            : base(creationArgs)
        {
#if false
            // Layer/Geometry testing
            Layer layer1 = new Layer("Test geometry", true);
            layer1.Components.Add(new Image(new Rect(100, 100, 400, 400), MapViewportIcons.MapAnnotation));
            layer1.Components.Add(new Text("TestString Blah Blah\nLorem Ipsum Delores", new Point(300, -300), PositionOrigin.Center));

            Polyline line1 = new Polyline(new Point[] { new Point(0, -400), new Point(100, -600), new Point(300, -400) });
            line1.FillBrush = Brushes.Red;
            layer1.Components.Add(line1);

            Polyline line2 = new Polyline(new Point[] { new Point(900, -500), new Point(700, -400), new Point(400, -600) });
            line2.StrokeBrush = Brushes.DeepPink;
            line2.StrokeThickness = 3;
            layer1.Components.Add(line2);

            Polygon poly1 = new Polygon(new Point[] { new Point(-100, -400), new Point(-300, -600), new Point(-600, -400) });
            poly1.StrokeBrush = Brushes.DarkGoldenrod;
            poly1.StrokeThickness = 2;
            layer1.Components.Add(poly1);

            Polygon poly2 = new Polygon(new Point[] { new Point(-100, 0), new Point(-200, 100), new Point(-200, -100) });
            poly2.StrokeBrush = Brushes.Green;
            poly2.FillBrush = Brushes.Indigo;
            layer1.Components.Add(poly2);

            Ellipse e1 = new Ellipse(new Point(600, 600), 25, 25);
            e1.StrokeBrush = Brushes.Gold;
            e1.StrokeThickness = 5;
            layer1.Components.Add(e1);

            Ellipse e2 = new Ellipse(new Point(700, 700), 50, 25);
            e2.StrokeBrush = Brushes.Gold;
            e2.StrokeThickness = 5;
            e2.FillBrush = Brushes.Honeydew;
            layer1.Components.Add(e2);

            Ellipse e3 = new Ellipse(new Point(800, 800), 75, 150);
            e3.StrokeBrush = Brushes.OrangeRed;
            e3.FillBrush = Brushes.GhostWhite;
            layer1.Components.Add(e3);

            Layers.Add(layer1);


            Layer layer2 = new Layer("Shitload of circles", true);

            for (int x = -4000; x <= 5000; x += 100)
            {
                for (int y = -5000; y <= 8500; y += 100)
                {
                    layer2.Components.Add(new Ellipse(new Point(x, y), 20, 20));
                }
            }
            //Layers.Add(layer2);

            // Annotation testing.
            XDocument doc = XDocument.Load(@"X:\gta5\src\dev_ng\rage\framework\tools\src\ui\MapViewer\MapViewer\Data\test_annotations.xml");
            foreach (XElement annotationElem in doc.XPathSelectElements("/Annotations/Annotation"))
            {
                //Annotations.Add(new Annotation(annotationElem));
            }
            /*
            Annotation annotation = new Annotation(new Point(500, -700));
            annotation.Comments.Add(new AnnotationComment("michael.taschler", "First annotation!", DateTime.UtcNow));
            annotation.Comments.Add(new AnnotationComment("michael.taschler", "Testing another comment blah.", DateTime.UtcNow.AddHours(2)));
            Annotations.Add(annotation);

            Annotations.Add(new Annotation(new Point(-2000, 2000)));
            Annotations.Add(new Annotation(new Point(-2100, 2000)));
            Annotations.Add(new Annotation(new Point(-2200, 2000)));
            Annotations.Add(new Annotation(new Point(-2300, 2000)));
            Annotations.Add(new Annotation(new Point(-2000, 2100)));
            Annotations.Add(new Annotation(new Point(-2100, 2200)));
            Annotations.Add(new Annotation(new Point(-2200, 2300)));
            Annotations.Add(new Annotation(new Point(-2300, 2400)));
            */
#endif
        }
        #endregion
    }
}
