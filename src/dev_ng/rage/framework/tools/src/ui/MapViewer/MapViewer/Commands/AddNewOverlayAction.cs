﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Editor;
using RSG.Editor.Model;
using RSG.MapViewer.Overlays;

namespace MapViewer.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public class AddNewOverlayAction : ButtonAction<MainWindow>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="AddNewOverlayAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public AddNewOverlayAction(ParameterResolverDelegate<MainWindow> resolver)
            : base(resolver)
        {
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="mainWindow">
        /// The main window.
        /// </param>
        public override void Execute(MainWindow mainWindow)
        {
            MainWindowViewModel mainWindowVm = mainWindow.DataContext as MainWindowViewModel;
            if (mainWindowVm == null)
            {
                throw new ArgumentException("mainWindowVm");
            }

            AddNewOverlayViewModel addNewOverlayVm = new AddNewOverlayViewModel(mainWindowVm.OverlayDescriptions);

            AddNewOverlayWindow addOverlayWindow = new AddNewOverlayWindow();
            addOverlayWindow.DataContext = addNewOverlayVm;
            addOverlayWindow.Owner = mainWindow;

            // Display the dialog and make sure the user hasn't cancelled out of it.
            bool? returnCode = addOverlayWindow.ShowDialog();
            if (returnCode == true)
            {
                IUniversalLog log = LogFactory.CreateUniversalLog("Overlay Creation");

                // Create the overlay.
                try
                {
                    IOverlayDescription overlayDescription = addNewOverlayVm.SelectedOverlay.OverlayDescription;
                    OverlayCreationArgs creationArgs = new OverlayCreationArgs(overlayDescription, ServiceProvider, mainWindowVm.DefaultBackgroundImages);

                    IOverlay overlay = overlayDescription.CreateOverlayInstance(creationArgs);
                    using (new SuspendUndoEngine(overlay.UndoEngine))
                    {
                        overlay.Initialise(log);
                    }

                    OverlayDocumentViewModel overlayViewModel = new OverlayDocumentViewModel(overlay, mainWindowVm);
                    mainWindowVm.AddOverlayDocument(overlayViewModel);
                }
                catch (System.Exception ex)
                {
                    IMessageBoxService msgService = GetService<IMessageBoxService>();
                    if (msgService == null)
                    {
                        throw new ArgumentNullException("msgService");
                    }
                    msgService.Show("A problem occurred while trying to create a new instance of the " +
                        addNewOverlayVm.SelectedOverlay.Name + " overlay:\n\n" + ex.Message);
                }
            }
        }
        #endregion
    }
}
