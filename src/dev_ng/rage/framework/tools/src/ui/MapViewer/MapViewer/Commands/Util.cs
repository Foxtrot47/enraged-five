﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml.Linq;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Editor;
using RSG.Editor.Controls;
using RSG.Editor.Model;
using RSG.MapViewer.Overlays;

namespace MapViewer.Commands
{
    /// <summary>
    /// Common code that is shared between multiple actions.
    /// </summary>
    internal static class Util
    {
        /// <summary>
        /// Opens an existing overlay template.
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="mainWindowVm"></param>
        /// <param name="serviceProvider"></param>
        /// <param name="messageService"></param>
        internal static void OpenOverlayTemplate(
            string filePath,
            MainWindowViewModel mainWindowVm,
            ICommandServiceProvider serviceProvider,
            IMessageBoxService messageService)
        {
            // Check whether the overlay is already open.  If so, set it as the active document and return.
            foreach (OverlayDocumentViewModel documentVm in mainWindowVm.OverlayDocuments)
            {
                if (String.Equals(documentVm.Overlay.FilePath, filePath, StringComparison.InvariantCultureIgnoreCase))
                {
                    mainWindowVm.ActiveOverlay = documentVm;
                    return;
                }
            }

            IUniversalLog log = LogFactory.CreateUniversalLog("Overlay Creation");

            try
            {
                // Open the overlay template from the file.
                XDocument doc = XDocument.Load(filePath);
                XElement overlayElement = doc.Element("Overlay");
                if (overlayElement == null)
                {
                    throw new OverlayTemplateFormatException("Overlay");
                }

                XElement descriptionGuidElement = overlayElement.Element("DescriptionGuid");
                if (descriptionGuidElement == null)
                {
                    throw new OverlayTemplateFormatException("DescriptionGuid");
                }

                // Attempt to resolve the description guid.
                Guid descriptionGuid = Guid.Parse(descriptionGuidElement.Value);
                IOverlayDescription overlayDescription = mainWindowVm.OverlayDescriptions.FirstOrDefault(item => item.Guid == descriptionGuid);
                if (overlayDescription == null)
                {
                    messageService.ShowStandardErrorBox(
                        String.Format("Unable to resolve the overlay description guid '{0}'.  " +
                            "Please send the overlay template file you are trying to open to tools.",
                            filePath),
                        null);

                    return;
                }

                // Create/initialise the overlay.
                OverlayCreationArgs creationArgs = new OverlayCreationArgs(
                    overlayDescription,
                    serviceProvider,
                    mainWindowVm.DefaultBackgroundImages,
                    filePath);

                IOverlay overlay = overlayDescription.CreateOverlayInstance(creationArgs);
                using (new SuspendUndoEngine(overlay.UndoEngine))
                {
                    overlay.Initialise(overlayElement, log);
                }

                // Add the overlay to the UI.
                OverlayDocumentViewModel overlayViewModel = new OverlayDocumentViewModel(overlay, mainWindowVm);
                mainWindowVm.AddOverlayDocument(overlayViewModel);
            }
            catch (Exception ex)
            {
                string message = String.Format("A problem occurred while trying to open the {0} overlay template.", filePath);
                log.ToolException(ex, message);
                messageService.Show(message + "\n\n" + ex.Message);
            }
            finally
            {
                if (log.HasErrors || log.HasWarnings)
                {
                    //Process.Start(logFilename);
                }
            }
        }

        /// <summary>
        /// Saves an overlay to the specified template file.
        /// </summary>
        /// <param name="overlay"></param>
        /// <param name="filePath"></param>
        /// <param name="messageService"></param>
        internal static bool SaveOverlayTemplate(
            IOverlay overlay,
            string filePath,
            IMessageBoxService messageService)
        {
            bool success = false;

            try
            {
                XDocument document = new XDocument(overlay.ToXElement("Overlay"));
                document.Save(filePath);

                overlay.FilePath = filePath;
                overlay.UndoEngine.PushCleanDirtyState();

                RsApplication app = Application.Current as RsApplication;
                if (app != null)
                {
                    app.AddToMruList("Overlay Templates", filePath, new string[0]);
                }
                success = true;
            }
            catch (Exception ex)
            {
                messageService.Show("A problem occurred while trying to save the " +
                    overlay.Description.Name +
                    " overlay template:\n\n" +
                    ex.Message);
            }
            return success;
        }
    }
}
