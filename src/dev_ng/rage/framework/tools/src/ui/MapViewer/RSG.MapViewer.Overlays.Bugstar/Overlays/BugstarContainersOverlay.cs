﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Xml.Linq;
using RSG.Base.Logging;
using RSG.Base.Math;
using RSG.Editor.Controls.MapViewport;
using RSG.Editor.Controls.MapViewport.OverlayComponents;
using RSG.MapViewer.Overlays.Bugstar.OverlayDetails;
using BugstarGame = RSG.Interop.Bugstar.Game;

namespace RSG.MapViewer.Overlays.Bugstar.Overlays
{
    /// <summary>
    /// Overlay that displays the grids associated with a particular bugstar project/map.
    /// </summary>
    public class BugstarContainersOverlay : BugstarOverlayBase
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="BugstarContainersOverlay"/> class
        /// using the specified creation arguments.
        /// </summary>
        /// <param name="creationArgs"></param>
        public BugstarContainersOverlay(OverlayCreationArgs creationArgs)
            : base(creationArgs)
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// Control that contains all of the overlays details.
        /// </summary>
        public override FrameworkElement OverlayDetailsControl
        {
            get { return new BugstarContainersDetails(); }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Initialises a newly created overlay.
        /// </summary>
        /// <param name="log"></param>
        public override void Initialise(ILog log)
        {
            base.Initialise(log);
            OnSelectedBackgroundImageChanged(false);
        }

        /// <summary>
        /// Gives overlays a chance to initialise their settings based on a xml element.
        /// </summary>
        /// <param name="settingsElement"></param>
        /// <param name="log"></param>
        protected override void InitialiseSettings(XElement settingsElement, ILog log)
        {
            base.InitialiseSettings(settingsElement, log);
            OnSelectedBackgroundImageChanged(false);
        }

        /// <summary>
        /// Invoked when the currently selected background image has changed.
        /// </summary>
        /// <param name="performingUndoEvent"></param>
        protected override void OnSelectedBackgroundImageChanged(bool performingUndoEvent)
        {
            Layers.Clear();

            BugstarMapBackgroundImage image = SelectedBackgroundImage as BugstarMapBackgroundImage;
            if (image != null)
            {
                BugstarGame.Map map = image.Map;

                // Create two groups
                Layer outlineLayer = new Layer("Container outlines", true);
                Layer labelLayer = new Layer("Container labels", false);

                foreach (BugstarGame.MapGrid grid in map.Grids.Where(item => item.Active))
                {
                    // Calculate the center of the grid.
                    Point gridCenter = CalculateGridCenter(grid);
                    Text gridNameText = new Text(grid.Name, gridCenter, PositionOrigin.Center);
                    gridNameText.ScaleFactor = 0.33333333;
                    labelLayer.Components.Add(gridNameText);
                    
                    Polygon gridOutline = new Polygon(grid.Points.Select(item => new Point(item.X, item.Y)));
                    outlineLayer.Components.Add(gridOutline);
                }

                Layers.Add(outlineLayer);
                Layers.Add(labelLayer);
            }
        }
        #endregion
    }
}
