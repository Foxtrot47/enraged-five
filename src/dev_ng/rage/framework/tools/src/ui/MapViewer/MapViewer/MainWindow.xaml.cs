﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MapViewer.Commands;
using RSG.Base.Logging;
using RSG.Editor;
using RSG.Editor.Controls;
using RSG.Editor.Controls.MapViewport;
using RSG.Editor.Controls.ZoomCanvas;
using RSG.Editor.Controls.ZoomCanvas.Commands;
using RSG.Editor.Controls.Helpers;
using RSG.Editor.SharedCommands;
using RSG.MapViewer.Overlays;
using RSG.Editor.Controls.Dock;
using RSG.Editor.Controls.MapViewport.Commands;

namespace MapViewer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : RsMainWindow
    {
        #region Fields
        /// <summary>
        /// Local log object.
        /// </summary>
        private readonly ILog _log;

        /// <summary>
        /// The global identifier used for the edit menu.
        /// </summary>
        private static Guid _editMenu = new Guid("4CE8F6EE-8A52-46F7-B061-CF4B17B8EE20");

        /// <summary>
        /// The global identifier used for the overlays menu.
        /// </summary>
        private static Guid _overlaysMenu = new Guid("38133D69-E333-4084-BCE0-5D2E2FA1BCCA");
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MainWindow"/> class using the 
        /// provided log object.
        /// </summary>
        /// <param name="application"></param>
        /// <param name="log"></param>
        /// <param name="overlayDescriptions"></param>
        public MainWindow(RsApplication application, ILog log, IEnumerable<IOverlayDescription> overlayDescriptions)
        {
            _log = log;

            this.DataContext = new MainWindowViewModel(overlayDescriptions);
            InitializeComponent();

            AddCommandInstances(application);
            AttachCommandBindings();
        }
        #endregion

        #region Command Bindings
        /// <summary>
        /// Attaches all of the necessary command bindings to this instance.
        /// </summary>
        private void AttachCommandBindings()
        {
            new AddNewOverlayAction(ResolveMainWindow)
                .AddBinding(MapViewerCommands.AddNewOverlay, this);
            new OpenOverlayTemplateAction(ResolveViewModel)
                .AddBinding(MapViewerCommands.OpenOverlayTemplate, this);
            new OpenOverlayTemplateFromMruAction(ResolveViewModel)
                .AddBinding(RockstarCommands.OpenFileFromMru, this);
            new CloseOverlayAction(ResolveActiveOverlayDocument)
                .AddBinding(DockingCommands.CloseItem, this);
            new CloseAllOverlaysAction(ResolveViewModel)
                .AddBinding(DockingCommands.CloseAll, this);
            new SaveOverlayAction(ResolveActiveOverlay)
                .AddBinding(RockstarCommands.Save, this);
            new SaveOverlayAsAction(ResolveActiveOverlay)
                .AddBinding(RockstarCommands.SaveAs, this);
            new SaveAllOverlaysAction(ResolveViewModel)
                .AddBinding(RockstarCommands.SaveAll, this);

            new UndoOverlayAction(ResolveActiveOverlay)
                .AddBinding(RockstarCommands.Undo, this);
            new RedoOverlayAction(ResolveActiveOverlay)
                .AddBinding(RockstarCommands.Redo, this);

            new AddAnnotationAction(ResolveAddAnnotationArgs)
                .AddBinding(MapViewportCommands.AddAnnotation, this);
            new SaveToImageAction(ResolveMapViewport)
                .AddBinding(MapViewportCommands.SaveToImage, this);
            new WarpInGameAction(ResolveMapViewportLocation)
                .AddBinding(MapViewerCommands.WarpInGame, this);
            new ToggleOverlayDetailsAction(ResolveToggleOverlayDetailsArgs)
                .AddBinding(MapViewerCommands.ShowOverlayDetails, this);
            new ToggleOverlayDetailsAction(ResolveToggleOverlayDetailsArgs)
                .AddBinding(MapViewerCommands.HideOverlayDetails, this);
        }
        #endregion

        #region Command Instances
        /// <summary>
        /// Adds the commands instances and menu instances to the command manager.
        /// </summary>
        /// <param name="application">
        /// The current application instance.
        /// </param>
        private void AddCommandInstances(RsApplication application)
        {
            // Menu Command Instances
            AddFileMenuCommandInstances(application);
            AddEditMenuCommandInstances();
            AddOverlayMenuCommandInstances();

            // Toolbar Command Instances

            // Context Menu Command Instances
            AddMapViewportContextMenuCommandInstances();
        }

        /// <summary>
        /// Adds the command instances to the file menu.
        /// </summary>
        /// <param name="application"></param>
        private void AddFileMenuCommandInstances(RsApplication application)
        {
            ushort index = 0;

            RockstarCommandManager.AddCommandInstance(
                MapViewerCommands.AddNewOverlay,
                index++,
                false,
                "New Overlay",
                new Guid("537142FB-AA1B-4E8B-8B85-3351D4BCAB0A"),
                RockstarCommandManager.FileMenuId);

            RockstarCommandManager.AddCommandInstance(
                MapViewerCommands.OpenOverlayTemplate,
                index++,
                false,
                "Open Overlay Template",
                new Guid("CD4CB9BA-32B8-41EB-846F-567799B4CC4F"),
                RockstarCommandManager.FileMenuId);

            RockstarCommandManager.AddCommandInstance(
                DockingCommands.CloseItem,
                index++,
                true,
                "Close",
                new Guid("CA31EE2A-7D43-409B-B556-BC08EB16BF22"),
                RockstarCommandManager.FileMenuId);
            
            RockstarCommandManager.AddCommandInstance(
                DockingCommands.CloseAll,
                index++,
                false,
                "Close All",
                new Guid("78655E19-B52A-42B7-B455-3228F17A1769"),
                RockstarCommandManager.FileMenuId);
            
            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Save,
                index++,
                true,
                "Save",
                new Guid("A627C071-9813-4DD2-8864-1C556A758825"),
                RockstarCommandManager.FileMenuId);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.SaveAs,
                index++,
                false,
                "Save As...",
                new Guid("DA732EB8-29BB-4598-8E49-94240F9BDB72"),
                RockstarCommandManager.FileMenuId);
            /*
            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.SaveAll,
                index++,
                false,
                "Save All",
                new Guid("BB9A95DA-3827-4B6A-82EB-49EC7F0DB4EE"),
                RockstarCommandManager.FileMenuId);
            */
            application.RegisterMruList(
                "Overlay Templates",
                true,
                RockstarCommands.OpenFileFromMru,
                new Guid("4F11F08A-5535-415B-BAAD-B3D413F9607F"),
                new Guid("39758940-266E-4AFC-8FB3-F1DCDC86212E"),
                new Guid("9B83B75D-E3CB-403B-B894-61C9518067BF"));
        }

        /// <summary>
        /// Adds the command instances to the edit menu.
        /// </summary>
        private void AddEditMenuCommandInstances()
        {
            RockstarCommandManager.AddEditMenu(_editMenu);

            ushort index = 0;
            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Undo,
                index++,
                true,
                "Undo",
                new Guid("1ED63D85-7BE8-4EAD-96A4-57ABF67057BF"),
                _editMenu);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Redo,
                index++,
                false,
                "Redo",
                new Guid("AAEAB6F0-462C-47F6-829C-0962688BB688"),
                _editMenu);
        }

        /// <summary>
        /// Adds the command instances to the overlay menu.
        /// </summary>
        private void AddOverlayMenuCommandInstances()
        {
            RockstarCommandManager.AddTopLevelMenu(2, "Overlays", _overlaysMenu);

            ushort index = 0;
            RockstarCommandManager.AddCommandInstance(
                MapViewportCommands.AddAnnotation,
                index++,
                false,
                "Add Annotation",
                new Guid("4630CACE-E73B-49CF-80CC-410712A404A6"),
                _overlaysMenu);

            RockstarCommandManager.AddCommandInstance(
                MapViewportCommands.SaveToImage,
                index++,
                false,
                "Save to Image",
                new Guid("34E7971E-F176-4FC1-B4D4-CF60770C0990"),
                _overlaysMenu);

            RockstarCommandManager.AddCommandInstance(
                MapViewerCommands.ShowOverlayDetails,
                index++,
                false,
                "Show Overlay Details",
                new Guid("C56FF923-6714-42F0-94FB-45460A4D2941"),
                _overlaysMenu);

            RockstarCommandManager.AddCommandInstance(
                MapViewerCommands.HideOverlayDetails,
                index++,
                false,
                "Hide Overlay Details",
                new Guid("2F6A62F8-13DC-4113-89BD-825421E04A53"),
                _overlaysMenu);
        }

        /// <summary>
        /// Adds the command instances to the map viewports context menu.
        /// </summary>
        private void AddMapViewportContextMenuCommandInstances()
        {
            RockstarCommandManager.AddCommandInstance(
                MapViewportCommands.AddAnnotation,
                0,
                false,
                "Add Annotation",
                new Guid("D2D7BF03-626E-4053-85CB-898D4DA7DBC8"),
                MapViewport.ContextMenuId);

            RockstarCommandManager.AddCommandInstance(
                MapViewportCommands.SaveToImage,
                1,
                false,
                "Save to Image",
                new Guid("32F4885F-DA5C-4ACA-9119-79E5205092E5"),
                MapViewport.ContextMenuId);

            RockstarCommandManager.AddCommandInstance(
                MapViewerCommands.WarpInGame,
                2,
                false,
                "Warp to Location",
                new Guid("1DCD1BBC-E612-46BB-BBC7-B13146F86E21"),
                MapViewport.ContextMenuId);
        }
        #endregion

        #region Command Resolvers
        /// <summary>
        /// Retrieve the currently active overlay (if any).
        /// </summary>
        /// <param name="commandData"></param>
        /// <returns></returns>
        private IOverlay ResolveActiveOverlay(CommandData commandData)
        {
            MainWindowViewModel mainWindowViewModel = DataContext as MainWindowViewModel;
            if (mainWindowViewModel == null)
            {
                throw new ArgumentException("DataContext isn't valid.");
            }

            // Get the overlay to return.
            IOverlay overlay = null;
            if (mainWindowViewModel.ActiveOverlay != null)
            {
                overlay = mainWindowViewModel.ActiveOverlay.Overlay;
            }
            return overlay;
        }

        /// <summary>
        /// Retrieve the currently active overlay document (if any).
        /// </summary>
        /// <param name="commandData"></param>
        /// <returns></returns>
        private OverlayDocumentViewModel ResolveActiveOverlayDocument(CommandData commandData)
        {
            MainWindowViewModel mainWindowViewModel = DataContext as MainWindowViewModel;
            if (mainWindowViewModel == null)
            {
                throw new ArgumentException("DataContext isn't valid.");
            }

            return mainWindowViewModel.ActiveOverlay;
        }

        /// <summary>
        /// The action resolver that returns command args to use with the
        /// <see cref="AddAnnotationAction"/>.
        /// </summary>
        /// <param name="commandData"></param>
        /// <returns></returns>
        private AddAnnotationCommandArgs ResolveAddAnnotationArgs(CommandData commandData)
        {
            return new AddAnnotationCommandArgs(DocumentPane.GetVisualDescendent<MapViewport>());
        }

        /// <summary>
        /// Retrieves the main window.
        /// </summary>
        /// <param name="commandData"></param>
        /// <returns></returns>
        private MainWindow ResolveMainWindow(CommandData commandData)
        {
            return this;
        }

        /// <summary>
        /// Retrieves the map viewport control.
        /// </summary>
        /// <param name="commandData"></param>
        /// <returns></returns>
        private MapViewport ResolveMapViewport(CommandData commandData)
        {
            return DocumentPane.GetVisualDescendent<MapViewport>();
        }

        /// <summary>
        /// Retrieves the coordinates of the mouse over the map viewport.
        /// </summary>
        /// <param name="commandData"></param>
        /// <returns></returns>
        private Point ResolveMapViewportLocation(CommandData commandData)
        {
            MapViewport viewport = DocumentPane.GetVisualDescendent<MapViewport>();
            return viewport.ContextMenuWorldTriggerPosition;
        }

        /// <summary>
        /// Retrieves the command args to use with the
        /// <see cref="ToggleOverlayDetailsAction"/> class.
        /// </summary>
        /// <param name="commandData"></param>
        /// <returns></returns>
        private ToggleOverlayDetailsArgs ResolveToggleOverlayDetailsArgs(CommandData commandData)
        {
            MainWindowViewModel mainWindowViewModel = DataContext as MainWindowViewModel;
            if (mainWindowViewModel == null)
            {
                throw new ArgumentException("DataContext isn't valid.");
            }

            bool showDetails = (commandData.Command == MapViewerCommands.ShowOverlayDetails);
            return new ToggleOverlayDetailsArgs(mainWindowViewModel.ActiveOverlay, showDetails);
        }

        /// <summary>
        /// Retrieves the coordinates of the mouse over the map viewport.
        /// </summary>
        /// <param name="commandData"></param>
        /// <returns></returns>
        internal MainWindowViewModel ResolveViewModel(CommandData commandData)
        {
            MainWindowViewModel mainWindowViewModel = DataContext as MainWindowViewModel;
            if (mainWindowViewModel == null)
            {
                throw new ArgumentException("DataContext isn't valid.");
            }

            return mainWindowViewModel;
        }
        #endregion
    }
}
