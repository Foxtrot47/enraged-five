﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using MapViewer.Commands;
using RSG.Base.Logging;
using RSG.Base.OS;
using RSG.Configuration;
using RSG.Editor.Controls;
using RSG.Editor.Controls.Bugstar;
using RSG.MapViewer.Overlays;

namespace MapViewer
{
    /// <summary>
    /// Defines the main entry point to the application and is responsible for creating and
    /// running the map viewer application.
    /// </summary>
    public class MapViewerApp : RsApplication
    {
        #region Program Entry Point
        /// <summary>
        /// Main entry point into the application. The first thing to get called.
        /// </summary>
        /// <param name="args"></param>
        [STAThread]
        public static void Main(string[] args)
        {
            MapViewerApp app = new MapViewerApp(args);
            app.ShutdownMode = ShutdownMode.OnLastWindowClose;
            RsApplication.OnEntry(app, ApplicationMode.Single);
        }
        #endregion

        #region Fields
        /// <summary>
        /// The private reference to the bugstar service that this application exposes to the
        /// command actions.
        /// </summary>
        private IBugstarService _bugstarService;

        /// <summary>
        /// List of all the overlay descriptions that the application is aware of.
        /// </summary>
        private IList<IOverlayDescription> _overlayDescriptions;
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="MapViewerApp"/> class.
        /// </summary>
        /// <param name="args"></param>
        public MapViewerApp(string[] args)
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets a value indicating whether the layout for the main window get serialised on
        /// exit and deserialised during loading.
        /// </summary>
        protected override bool MakeLayoutPersistent
        {
            get { return true; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Determines whether the specified windows can close without the need for user
        /// interaction. For example unsaved data.
        /// </summary>
        /// <param name="windows">
        /// The windows that are to be tested.
        /// </param>
        /// <returns>
        /// True if the specified windows can be closed without user interaction; otherwise,
        /// false.
        /// </returns>
        public override bool CanCloseWithoutInteraction(ISet<Window> windows)
        {
            return false;
        }

        /// <summary>
        /// Gets the service object of the specified type making sure that before the base is
        /// called the <see cref="RSG.Project.ViewModel.IProjectAddViewService"/> interface is
        /// handled.
        /// </summary>
        /// <param name="serviceType">
        /// An object that specifies the type of service object to get.
        /// </param>
        /// <returns>
        /// The service of the specified type if found; otherwise, null.
        /// </returns>
        public override object GetService(Type serviceType)
        {
            if (serviceType == typeof(IBugstarService))
            {
                return _bugstarService;
            }

            return base.GetService(serviceType);
        }

        /// <summary>
        /// Handles the specified windows being closed. This is called after the method
        /// <see cref="CanCloseWithoutInteraction"/> returns false.
        /// </summary>
        /// <param name="windows">
        /// The windows to handle.
        /// </param>
        /// <param name="e">
        /// A instance of CancelEventArgs that can be used to cancel the closing of the
        /// specified windows.
        /// </param>
        public override void HandleWindowsClosing(ISet<Window> windows, CancelEventArgs e)
        {
            MainWindow window = this.MainWindow as MainWindow;
            if (window == null)
            {
                return;
            }

            if (!windows.Contains(window))
            {
                return;
            }

            try
            {
                CloseAllOverlaysAction action = new CloseAllOverlaysAction(this);
                action.Execute(window.ResolveViewModel(null));
            }
            catch (OperationCanceledException)
            {
                e.Cancel = true;
                return;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void StartupCore(StartupEventArgs e)
        {
            // Create the bugstar service.
            _bugstarService = new BugstarService(Log, CommandOptions.Project);

            // Load the available overlay descriptions.
            OverlayComposer overlayComposer = null;
            try
            {
                overlayComposer = new OverlayComposer();
                _overlayDescriptions = overlayComposer.OverlayDescriptions.ToList();
            }
            catch (Exception)
            {
                //TODO: Display message box.
            }
            finally
            {
                if (overlayComposer != null)
                {
                    overlayComposer.Dispose();
                }
            }

            base.StartupCore(e);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="persistentDirectory"></param>
        protected override void HandleOnExiting(string persistentDirectory)
        {
            base.HandleOnExiting(persistentDirectory);
        }

        /// <summary>
        /// Override to create the main window for the application.
        /// </summary>
        /// <returns>
        /// The System.Windows.Window that will be the main window for this application.
        /// </returns>
        protected override Window CreateMainWindow()
        {
            return new MainWindow(this, Log, _overlayDescriptions);
        }
        #endregion
    }
}
