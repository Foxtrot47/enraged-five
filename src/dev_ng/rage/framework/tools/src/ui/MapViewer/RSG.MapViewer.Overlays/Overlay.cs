﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using RSG.Editor.Controls.MapViewport.Annotations;
using RSG.Editor.Controls.MapViewport.OverlayComponents;
using RSG.Editor;
using System.Windows;
using RSG.Editor.Controls.MapViewport;
using RSG.Editor.Model;
using System.IO;
using System.Xml.Linq;
using RSG.Base.Logging;
using System.Runtime.CompilerServices;

namespace RSG.MapViewer.Overlays
{
    /// <summary>
    /// Abstract base class from which overlays can inherit from.
    /// </summary>
    public abstract class Overlay : UndoablePropertyChangedBase, IOverlay
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Annotations"/> property.
        /// </summary>
        private AnnotationCollection _annotations;

        /// <summary>
        /// Private field for the <see cref="BackgroundImages"/> property.
        /// </summary>
        private MapBackgroundImageCollection _backgroundImages;

        /// <summary>
        /// Private field for the <see cref="Description"/> property.
        /// </summary>
        private readonly IOverlayDescription _description;

        /// <summary>
        /// Private field for the <see cref="FilePath"/> property.
        /// </summary>
        private string _filePath;

        /// <summary>
        /// Private field for the <see cref="IsModified"/> property.
        /// </summary>
        private bool _isModified;

        /// <summary>
        /// Private field for the <see cref="Layers"/> property.
        /// </summary>
        private ObservableCollection<Layer> _layers;

        /// <summary>
        /// Private field for the <see cref="SelectedBackgroundImage"/> property.
        /// </summary>
        private MapBackgroundImage _selectedBackgroundImage;

        /// <summary>
        /// Private field for the <see cref="UndoEngine"/> property.
        /// </summary>
        private readonly UndoEngine _undoEngine;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Overlay"/> class using the
        /// provided creation arguments.
        /// </summary>
        /// <param name="creationArgs"></param>
        protected Overlay(OverlayCreationArgs creationArgs)
        {
            _annotations = new AnnotationCollection(this);
            _backgroundImages = creationArgs.DefaultBackgroundImages;
            _description = creationArgs.OverlayDescription;
            _filePath = creationArgs.FilePath;
            _layers = new ObservableCollection<Layer>();
            _selectedBackgroundImage = _backgroundImages.FirstOrDefault();
            _undoEngine = new UndoEngine();
            _undoEngine.DirtyStateChanged += UndoEngineDirtyStateChanged;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Collection of annotations that this overlay exposes.
        /// </summary>
        public AnnotationCollection Annotations
        {
            get { return _annotations; }
        }

        /// <summary>
        /// Background that are available for use with this overlay.
        /// </summary>
        public MapBackgroundImageCollection BackgroundImages
        {
            get { return _backgroundImages; }
        }

        /// <summary>
        /// The description associated with this overlay.
        /// </summary>
        public IOverlayDescription Description
        {
            get { return _description; }
        }

        /// <summary>
        /// Retrieves the filename for this overlay (if it's a template).
        /// </summary>
        public string FileName
        {
            get
            {
                if (!String.IsNullOrEmpty(_filePath))
                {
                    return Path.GetFileNameWithoutExtension(_filePath);
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Retrieves the filepath to this overlay template on disk.
        /// </summary>
        public string FilePath
        {
            get { return _filePath; }
            set
            {
                if (SetProperty(ref _filePath, value, "FilePath", "FileName"))
                {
                    IsModified = _undoEngine.DirtyState || _filePath == null;
                }
            }
        }
        
        /// <summary>
        /// Gets or sets a value indicating whether this overlay currently contains
        /// modified unsaved data.
        /// </summary>
        public bool IsModified
        {
            get { return _isModified; }
            protected set { SetProperty(ref _isModified, value); }
        }

        /// <summary>
        /// Collection of layers that make up the data for this overlay.
        /// </summary>
        public ObservableCollection<Layer> Layers
        {
            get { return _layers; }
            protected set { SetProperty(ref _layers, value); }
        }

        /// <summary>
        /// Control that contains all of the overlays details.
        /// </summary>
        public virtual FrameworkElement OverlayDetailsControl
        {
            get { return null; }
        }

        /// <summary>
        /// Where the overlay details will be positioned in relation to the map viewport.
        /// </summary>
        public virtual OverlayDetailsPosition OverlayDetailsPosition
        {
            get { return OverlayDetailsPosition.Right; }
        }

        /// <summary>
        /// The currently selected background image.
        /// </summary>
        public MapBackgroundImage SelectedBackgroundImage
        {
            get { return _selectedBackgroundImage; }
            set
            {
                bool performingUndoEvent = UndoEngine.PerformingEvent;

                using (new UndoRedoBatch(UndoEngine))
                {
                    if (SetUndoableProperty(ref _selectedBackgroundImage, value))
                    {
                        OnSelectedBackgroundImageChanged(performingUndoEvent);
                    }
                }
            }
        }

        /// <summary>
        /// Undoable version of <see cref="BackgroundImages"/>.
        /// </summary>
        protected MapBackgroundImageCollection UndoableBackgroundImages
        {
            get { return _backgroundImages; }
            set { SetUndoableProperty(ref _backgroundImages, value, "UndoableBackgroundImages", "BackgroundImages"); }
        }

        /// <summary>
        /// Undo engine associated with this overlay.
        /// </summary>
        public override UndoEngine UndoEngine
        {
            get { return _undoEngine; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Initialises a newly created overlay.
        /// </summary>
        /// <param name="log"></param>
        public virtual void Initialise(ILog log)
        {
            _isModified = true;
            _selectedBackgroundImage = _backgroundImages.FirstOrDefault();
        }

        /// <summary>
        /// Initialises an overlay that has been loaded from a template.
        /// </summary>
        /// <param name="overlayElement">
        /// The xml element from which to load the overlay data.
        /// </param>
        /// <param name="log"></param>
        public void Initialise(XElement overlayElement, ILog log)
        {
            // Load annotations first.
            XElement annotationsElement = overlayElement.Element("Annotations");
            if (annotationsElement == null)
            {
                throw new OverlayTemplateFormatException("Annotations");
            }

            try
            {
                _annotations = new AnnotationCollection(annotationsElement, this);
            }
            catch (Exception ex)
            {
                log.Warning("A problem occurred while trying to load the annotations.  No annotations will be loaded for this overlay.\n\n{0}", ex.Message);
            }

            // Load the overlay settings next.
            XElement settingsElement = overlayElement.Element("Settings");
            if (settingsElement == null)
            {
                throw new OverlayTemplateFormatException("Settings");
            }
            InitialiseSettings(settingsElement, log);
        }

        /// <summary>
        /// Gives overlays a chance to initialise their settings based on a xml element.
        /// </summary>
        /// <param name="settingsElement"></param>
        /// <param name="log"></param>
        protected virtual void InitialiseSettings(XElement settingsElement, ILog log)
        {
            // Try and restore the same background image.
            XElement backgroundImageElement = settingsElement.Element("BackgroundImage");
            if (backgroundImageElement == null)
            {
                throw new OverlayTemplateFormatException("BackgroundImage");
            }

            string backgroundImageName = backgroundImageElement.Value;
            _selectedBackgroundImage = _backgroundImages.FirstOrDefault(item => item.DisplayName == backgroundImageName);

            if (_selectedBackgroundImage == null)
            {
                log.Warning("Unable to restore the previously selected background image ('{0}').  Using the default image instead.", backgroundImageName);
                _selectedBackgroundImage = _backgroundImages.FirstOrDefault();
            }
        }

        /// <summary>
        /// Invoked when the currently selected background image has changed.
        /// </summary>
        /// <param name="performingUndoEvent"></param>
        protected virtual void OnSelectedBackgroundImageChanged(bool performingUndoEvent)
        {
        }

        /// <summary>
        /// Saves the overlay details options to an xml element.
        /// </summary>
        /// <returns></returns>
        public XElement ToXElement(string name)
        {
            return new XElement(name,
                new XElement("DescriptionGuid", _description.Guid),
                Annotations.ToXElement("Annotations"),
                SettingsToXElement("Settings"));
        }

        /// <summary>
        /// Allows overlays to serialise out any settings they wish.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        protected virtual XElement SettingsToXElement(string name)
        {
            return new XElement(name,
                new XElement("BackgroundImage", _selectedBackgroundImage.DisplayName));
        }

        /// <summary>
        /// Called whenever the undo engine signals that its dirty state has been changed.
        /// </summary>
        /// <param name="sender">
        /// The undo engine attached to this handler.
        /// </param>
        /// <param name="e">
        /// The System.EventArgs containing the event data.
        /// </param>
        private void UndoEngineDirtyStateChanged(object sender, EventArgs e)
        {
            IsModified = _undoEngine.DirtyState || _filePath == null;
        }
        #endregion
    }
}
