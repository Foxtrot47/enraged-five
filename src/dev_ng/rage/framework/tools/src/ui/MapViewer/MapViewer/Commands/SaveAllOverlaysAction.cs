﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Editor;

namespace MapViewer.Commands
{
    /// <summary>
    /// Action for saving all overlay documents as templates.
    /// </summary>
    public class SaveAllOverlaysAction : ButtonAction<MainWindowViewModel>
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="SaveAllOverlaysAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public SaveAllOverlaysAction(ParameterResolverDelegate<MainWindowViewModel> resolver)
            : base(resolver)
        {
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="mainWindowVm">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(MainWindowViewModel mainWindowVm)
        {
            return mainWindowVm.OverlayDocuments.Any();
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="mainWindowVm">
        /// The main window.
        /// </param>
        public override void Execute(MainWindowViewModel mainWindowVm)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
