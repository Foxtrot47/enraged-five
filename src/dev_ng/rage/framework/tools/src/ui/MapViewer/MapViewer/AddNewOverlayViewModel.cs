﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using RSG.Editor;
using RSG.MapViewer.Overlays;

namespace MapViewer
{
    /// <summary>
    /// 
    /// </summary>
    public class AddNewOverlayViewModel : NotifyPropertyChangedBase
    {
        #region Fields
        /// <summary>
        /// The private field for the <see cref="Descriptions"/> property.
        /// </summary>
        private readonly IEnumerable<IOverlayDescription> _descriptions;

        /// <summary>
        /// Private field for the <see cref="OverlayGroups"/> property.
        /// </summary>
        private readonly Collection<OverlayDescriptionGroupViewModel> _overlayGroups;

        /// <summary>
        /// Private field for the <see cref="Overlays"/> property.
        /// </summary>
        private readonly Collection<OverlayDescriptionViewModel> _overlays;

        /// <summary>
        /// Private field for the <see cref="SelectedGroup"/> property.
        /// </summary>
        private OverlayDescriptionGroupViewModel _selectedGroup;

        /// <summary>
        /// Private field for the <see cref="SelectedOverlay"/> property.
        /// </summary>
        private OverlayDescriptionViewModel _selectedOverlay;
        #endregion

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="factories"></param>
        public AddNewOverlayViewModel(IEnumerable<IOverlayDescription> descriptions)
        {
            _descriptions = descriptions;
            _overlayGroups = new Collection<OverlayDescriptionGroupViewModel>();
            _overlays = new Collection<OverlayDescriptionViewModel>();
            _selectedGroup = null;
            _selectedOverlay = null;

            // Populate the group hierarchy.
            IList<string> uniquePaths =
                descriptions.Select(item => item.Group)
                    .Distinct()
                    .OrderBy(item => item).ToList();
            IList<string[]> pathComponents = uniquePaths.Select(item => item.Split(new char[] { '/' })).ToList();

            _overlayGroups.Add(new OverlayDescriptionGroupViewModel("All Overlays", "", pathComponents));
            
            // Populate the list of all overlays.
            foreach (IOverlayDescription description in descriptions)
            {
                _overlays.Add(new OverlayDescriptionViewModel(description));
            }
            _selectedOverlay = _overlays.FirstOrDefault();

            ICollectionView collectionView = CollectionViewSource.GetDefaultView(Overlays);
            collectionView.SortDescriptions.Add(new SortDescription("Name", ListSortDirection.Ascending));
        }
        #endregion

        #region Properties
        /// <summary>
        /// Root level overlay groups.
        /// </summary>
        public IReadOnlyCollection<OverlayDescriptionGroupViewModel> OverlayGroups
        {
            get { return _overlayGroups; }
        }

        /// <summary>
        /// The overlay group that the user selected.
        /// </summary>
        public OverlayDescriptionGroupViewModel SelectedGroup
        {
            get { return _selectedGroup; }
            set
            {
                if (SetProperty(ref _selectedGroup, value))
                {
                    ForceFilter();

                    ICollectionView items = CollectionViewSource.GetDefaultView(Overlays);
                    if (!items.Contains(SelectedOverlay))
                    {
                        IEnumerable<OverlayDescriptionViewModel> overlayItems = items.Cast<OverlayDescriptionViewModel>();
                        SelectedOverlay = overlayItems.FirstOrDefault();
                    }
                }
            }
        }

        /// <summary>
        /// The set of overlays that are in the group or sub-groups of the selected group.
        /// </summary>
        public IReadOnlyCollection<OverlayDescriptionViewModel> Overlays
        {
            get { return _overlays; }
        }

        /// <summary>
        /// The overlay that the user selected.
        /// </summary>
        public OverlayDescriptionViewModel SelectedOverlay
        {
            get { return _selectedOverlay; }
            set { SetProperty(ref _selectedOverlay, value, "SelectedOverlay", "IsOverlaySelected"); }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsOverlaySelected
        {
            get { return _selectedOverlay != null; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Forces a re-filter of the shown items.
        /// </summary>
        private void ForceFilter()
        {
            ICollectionView items = CollectionViewSource.GetDefaultView(Overlays);
            if (items != null)
            {
                items.Filter = this.FilterItem;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private bool FilterItem(object item)
        {
            if (_selectedGroup == null)
            {
                return false;
            }

            OverlayDescriptionViewModel viewModel = item as OverlayDescriptionViewModel;
            if (viewModel == null)
            {
                return false;
            }

            // Add extra slash at the end so we match the full group name and not partial names.
            // e.g.
            // Bugstar/Bugs/Per mission
            // Bugstar/Bugs & Other/Blah
            // without the slash "Bugstar/Bugs" would match both groups.
            string groupFullPath = _selectedGroup.FullPath + "/";
            string descriptionFullPath = String.Format("/{0}/{1}", viewModel.OverlayDescription.Group, viewModel.OverlayDescription.Name);
            
            return descriptionFullPath.StartsWith(groupFullPath);
        }
        #endregion
    }
}
