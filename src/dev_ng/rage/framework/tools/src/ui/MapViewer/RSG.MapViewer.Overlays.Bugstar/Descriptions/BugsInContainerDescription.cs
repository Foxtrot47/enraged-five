﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.MapViewer.Overlays.Bugstar.Overlays;

namespace RSG.MapViewer.Overlays.Bugstar.Descriptions
{
    /// <summary>
    /// 
    /// </summary>
    [Export(typeof(IOverlayDescription))]
    public class BugsInContainerDescription : OverlayDescription<BugsInContainerOverlay>
    {
        #region Constants
        /// <summary>
        /// Unique guid for this overlay definition.
        /// </summary>
        private static readonly Guid Id = new Guid("080FCF37-BBA0-4450-9505-A72C6BE5BA6B");
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="BugsInContainerDescription"/> class.
        /// </summary>
        public BugsInContainerDescription()
            : base(Id, "Bugs in Container", "Bugstar", "Shows the location of bugs in a specific bugstar container.")
        {
        }
        #endregion
    }
}
