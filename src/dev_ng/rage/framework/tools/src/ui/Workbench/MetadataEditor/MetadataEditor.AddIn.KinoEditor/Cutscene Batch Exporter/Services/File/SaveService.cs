﻿using System;
using System.Diagnostics;
using RSG.Base.Editor;
using RSG.Base.Logging;
using Workbench.AddIn;
using Workbench.AddIn.Services.File;
using System.Collections.Generic;
using Workbench.AddIn.UI.Layout;

namespace MetadataEditor.AddIn.KinoEditor.Cutscene_Batch_Exporter.Services.File
{

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.SaveService, typeof(ISaveService))]
    class SaveService : ISaveService
    {
        #region Constants

        private readonly Guid GUID = new Guid("E6DAACC7-6C28-464A-9064-580CAF0E3D94");

        #endregion // Constants

        #region Properties

        /// <summary>
        /// Service GUID.
        /// </summary>
        public Guid ID
        {
            get { return (GUID); }
        }

        /// <summary>
        /// Open dialog filter string.
        /// </summary>
        public String Filter
        {
            get { return "Batch Exporter documents (.batchxml)|*.batchxml"; }
        }

        /// <summary>
        /// Open dialog default filter index.
        /// </summary>
        public int DefaultFilterIndex
        {
            get { return 0; }
        }

        /// <summary>
        /// Array of support document GUIDs.
        /// </summary>
        public Guid[] SupportedContent
        {
            get
            {
                return m_supportedContent;
            }
        }
        private Guid[] m_supportedContent = { MetadataEditor.AddIn.KinoEditor.Cutscene_Batch_Exporter.CutsceneBatchExporterToolWindowView.GUID };

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Default constructor.
        /// </summary>
        public SaveService()
        {
        }

        #endregion // Constructors(s)

        #region Methods

        /// <summary>
        /// Returns a value indicating if the save service
        /// can currently be executed
        /// </summary>
        public Boolean CanExecuteSave()
        {
            return true;
        }

        /// <summary>
        /// Returns a value indicating if the save service
        /// can currently be executed
        /// </summary>
        public Boolean CanExecuteSaveAs()
        {
            return true;
        }

        /// <summary>
        /// Save the specified model data to disk file.
        /// </summary>
        public bool Save(IModel saveModel, String filename, int filterIndex, ref Boolean setPathToSavePath)
        {
            setPathToSavePath = true;
            if (saveModel is CutsceneBatchExportToolWindowViewModel)
            {
                (saveModel as CutsceneBatchExportToolWindowViewModel).Save(filename);
                return true;
            }
            return false;
        }

        #endregion // Methods
    }

}
