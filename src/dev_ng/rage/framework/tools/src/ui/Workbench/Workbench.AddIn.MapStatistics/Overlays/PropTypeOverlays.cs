﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Media;
using RSG.Base.Collections;
using RSG.Base.Math;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using RSG.Base.Windows.Helpers;
using RSG.Model.Common.Map;
using WidgetEditor.AddIn;
using Workbench.AddIn.Services;

namespace Workbench.AddIn.MapStatistics.Overlays
{
    /// <summary>
    /// 
    /// </summary>
    public class PropTypeOverlayBase : StatisticOverlayBase
    {
        #region Constants
        /// <summary>
        /// Rag warp widgets
        /// </summary>
        private const String RAG_WARPCOORDINATES = "Debug/Warp Player x y z h vx vy vz";
        private const String RAG_WARPNOWBUTTON = "Debug/Warp now";
        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// MEF import for proxy UI
        /// </summary>
        [ImportExtension(WidgetEditor.AddIn.CompositionPoints.ProxyService, typeof(IProxyService))]
        public Lazy<IProxyService> ProxyService { get; set; }

        /// <summary>
        /// MEF import for message box service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.MessageService, typeof(Workbench.AddIn.Services.IMessageService))]
        private Lazy<IMessageService> MessageService { get; set; }

        #endregion // MEF Imports

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<IArchetype> ValidArchetypes
        {
            get { return m_validArchetypes; }
            set
            {
                SetPropertyValue(value, () => ValidArchetypes,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_validArchetypes = (ObservableCollection<IArchetype>)newValue;
                        }
                ));
            }
        }
        private ObservableCollection<IArchetype> m_validArchetypes;

        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<IEntity> ValidEntities
        {
            get { return m_validEntities; }
            set
            {
                SetPropertyValue(value, () => ValidEntities,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_validEntities = (ObservableCollection<IEntity>)newValue;
                            OnRenderOptionChanged();
                        }
                ));
            }
        }
        private ObservableCollection<IEntity> m_validEntities;

        /// <summary>
        /// 
        /// </summary>
        public IEntity SelectedEntity
        {
            get { return m_selectedEntity; }
            set
            {
                SetPropertyValue(value, () => SelectedEntity,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_selectedEntity = (IEntity)newValue;
                        }
                ));
            }
        }
        private IEntity m_selectedEntity;

        /// <summary>
        /// Flag indicating whether we are connected to the game via the rag proxy
        /// </summary>
        public bool IsGameConnected
        {
            get
            {
                return ProxyService.Value.IsGameConnected;
            }
        }

        /// <summary>
        /// Command that gets fired off when the "Warp to in Game" button is pressed for the selected map instance
        /// </summary>
        public RelayCommand WarpToInGameCommand
        {
            get
            {
                if (m_warpToInGameCommand == null)
                {
                    m_warpToInGameCommand = new RelayCommand(param => WarpToInGame(param));
                }

                return m_warpToInGameCommand;
            }
        }
        private RelayCommand m_warpToInGameCommand;

        /// <summary>
        /// Command that gets fired off when the "Export to .csv File" button is pressed.
        /// </summary>
        public RelayCommand ExportToCSV
        {
            get
            {
                if (m_ExportToCsvFile == null)
                {
                    m_ExportToCsvFile = new RelayCommand(param => ExportToCsvFile(param), param => CanExportToCsvFile(param));
                }

                return m_ExportToCsvFile;
            }
        }
        private RelayCommand m_ExportToCsvFile;
        #endregion // Properties

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public PropTypeOverlayBase(string name, string description)
            : base(name, description)
        {
            m_includePropGroup = true;

            m_validArchetypes = new ObservableCollection<IArchetype>();
            m_validEntities = new ObservableCollection<IEntity>();
        }
        #endregion // Constructor

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="validSections"></param>
        /// <param name="validSectionsAndPropGroups"></param>
        protected override void RefreshGeometry(IDictionary<IMapSection, IMapSection> validSections)
        {
            // Get the updated list of valid entities
            this.ValidEntities.BeginUpdate();
            this.ValidEntities.Clear();

            foreach (KeyValuePair<IMapSection, IMapSection> pair in validSections)
            {
                ValidEntities.AddRange(GetValidEntities(pair.Key));
                ValidEntities.AddRange(GetValidEntities(pair.Value));
            }

            this.ValidEntities.EndUpdate();

            // Update the geometry
            this.Geometry.BeginUpdate();
            this.Geometry.Clear();

            foreach (IEntity entity in ValidEntities)
            {
                Vector2f position = new Vector2f(entity.Position.X, entity.Position.Y);
                Viewport2DCircle geometry = new Viewport2DCircle(etCoordSpace.World, "", position, 15.0f, Colors.Red, 15);
                this.Geometry.Add(geometry);
            }

            this.Geometry.EndUpdate();

            // Update the list of valid archetypes
            this.ValidArchetypes.BeginUpdate();
            this.ValidArchetypes.Clear();
            this.ValidArchetypes.AddRange(this.ValidEntities.Where(item => item.ReferencedArchetype != null).Select(item => item.ReferencedArchetype).Distinct());
            this.ValidArchetypes.EndUpdate();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        protected virtual IEnumerable<IEntity> GetValidEntities(IMapSection section)
        {
            return Enumerable.Empty<IEntity>();
        }

        /// <summary>
        /// Gets the control that should be shown in the overlay details panel
        /// </summary>
        public override System.Windows.Controls.Control GetOverlayControl()
        {
            return new Workbench.AddIn.MapDebugging.OverlayViews.ObjectLocationOverlayView();
        }
        #endregion // Overrides

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="instance"></param>
        private void WarpToInGame(object entityObj)
        {
            IEntity entity = entityObj as IEntity;
            if (entity != null)
            {
                Vector3f position = new Vector3f(entity.Position);
                position.X -= 2.0f;
                position.Z += 1.0f;

                float heading = -(float)Math.PI / 2.0f;

                // Check whether the RAG proxy is connected
                if (!ProxyService.Value.IsGameConnected)
                {
                    MessageService.Value.Show("RAG is not connected. Is the game running?");
                }
                else
                {
                    ProxyService.Value.Console.WriteStringWidget(RAG_WARPCOORDINATES, String.Format("{0} {1} {2} {3}", position.X, position.Y, position.Z, heading));
                    ProxyService.Value.Console.PressWidgetButton(RAG_WARPNOWBUTTON);
                }
            }
        }

        private bool CanExportToCsvFile(object parameter)
        {
            return this.ValidEntities.Count > 0;
        }

        private void ExportToCsvFile(object parameter)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.Filter = "CSV (Comma delimited)|*.csv";
            Nullable<bool> result = dlg.ShowDialog();
            if (false == result)
            {
                return;
            }

            using (StreamWriter sw = new StreamWriter(dlg.FileName))
            {
                string header = "Name, Section, Parent Area ,Grandparent Area, Coords";
                sw.WriteLine(header);

                foreach (var entity in this.ValidEntities)
                {
                    string sectionName = "n/a";
                    string parentName = "n/a";
                    string grandParentName = "n/a";
                    IMapSection section = entity.Parent as IMapSection;
                    IMapArea parent = null;
                    IMapArea grandParent = null;
                    if (section != null)
                    {
                        sectionName = section.Name;
                        parent = section.ParentArea;
                        if (parent != null)
                        {
                            parentName = parent.Name;
                            grandParent = parent.ParentArea;
                            if (grandParent != null)
                            {
                                grandParentName = grandParent.Name;
                            }
                        }
                    }

                    string location = string.Format("\"{0}, {1}, {2}\"", entity.Position.X, entity.Position.Y, entity.Position.Z);
                    string record = string.Format("{0},{1},{2},{3},{4}", entity.Name, sectionName, parentName, grandParentName, location);
                    sw.WriteLine(record);
                }
            }
        }
        #endregion // Private Methods
    } // PropTypeOverlayBase

    [ExportExtension(ExtensionPoints.PropTypeStatisticsOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class TreePropTypeOverlay : PropTypeOverlayBase
    {
        #region Constants
        /// <summary>
        /// 
        /// </summary>
        private const string c_name = "Trees";
        private const string c_description = "Shows all tree entities on the map.";

        /// <summary>
        /// 
        /// </summary>
        private static readonly string[] c_HdTreeShaders = new string[]
        {
            "tree.sps",
            "trees_normal.sps",
            "trees_normal_spec.sps",
            "trees_tnt.sps",
            "trees.sps",
        };

        /// <summary>
        /// 
        /// </summary>
        private static readonly string[] c_LodTreeShaders = new string[]
        {
            "tree_lod.sps",
            "tree_lod2.sps",
        };
        #endregion // Constants

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public TreePropTypeOverlay()
            : base(c_name, c_description)
        {
        }
        #endregion // Constructor

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        protected override IEnumerable<IEntity> GetValidEntities(IMapSection section)
        {
            if (section == null)
            {
                yield break;
            }

            foreach (IEntity entity in section.ChildEntities)
            {
                if (entity.ReferencedArchetype != null && !(entity.ReferencedArchetype is IInteriorArchetype))
                {
                    IMapArchetype mapArchetype = (IMapArchetype)entity.ReferencedArchetype;

                    // Check if any of the map archetype shaders is one of the hd tree shaders
                    if (mapArchetype.Shaders.Select(item => item.Name).Intersect(c_HdTreeShaders).Any())
                    {
                        yield return entity;
                    }
                }
            }
        }
        #endregion // Overrides
    } // TreePropTypeOverlay

    [ExportExtension(ExtensionPoints.PropTypeStatisticsOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class DynamicPropTypeOverlay : PropTypeOverlayBase
    {
        #region Constants
        /// <summary>
        /// 
        /// </summary>
        private const string c_name = "Dynamic";
        private const string c_description = "Shows all dynamic entities on the map.";
        #endregion // Constants

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public DynamicPropTypeOverlay()
            : base(c_name, c_description)
        {
        }
        #endregion // Constructor

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        protected override IEnumerable<IEntity> GetValidEntities(IMapSection section)
        {
            if (section == null)
            {
                yield break;
            }

            foreach (IEntity entity in section.ChildEntities)
            {
                if (entity.ReferencedArchetype is ISimpleMapArchetype)
                {
                    ISimpleMapArchetype drawable = (ISimpleMapArchetype)entity.ReferencedArchetype;

                    if (drawable.IsDynamic == true)
                    {
                        yield return entity;
                    }
                }
            }
        }
        #endregion // Overrides
    } // DynamicPropTypeOverlay

    [ExportExtension(ExtensionPoints.PropTypeStatisticsOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class FragPropTypeOverlay : PropTypeOverlayBase
    {
        #region Constants
        /// <summary>
        /// 
        /// </summary>
        private const string c_name = "Fragment";
        private const string c_description = "Shows all fragment entities on the map.";
        #endregion // Constants

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public FragPropTypeOverlay()
            : base(c_name, c_description)
        {
        }
        #endregion // Constructor

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        protected override IEnumerable<IEntity> GetValidEntities(IMapSection section)
        {
            if (section == null)
            {
                yield break;
            }

            foreach (IEntity entity in section.ChildEntities)
            {
                if (entity.ReferencedArchetype is IFragmentArchetype)
                {
                    yield return entity;
                }
            }
        }
        #endregion // Overrides
    } // FragPropTypeOverlay

    [ExportExtension(ExtensionPoints.PropTypeStatisticsOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class ExplosivePropTypeOverlay : PropTypeOverlayBase
    {
        #region Constants
        /// <summary>
        /// 
        /// </summary>
        private const string c_name = "Explosive";
        private const string c_description = "Shows all explosive entities on the map.";
        #endregion // Constants

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public ExplosivePropTypeOverlay()
            : base(c_name, c_description)
        {
        }
        #endregion // Constructor

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        protected override IEnumerable<IEntity> GetValidEntities(IMapSection section)
        {
            if (section == null)
            {
                yield break;
            }

            foreach (IEntity entity in section.ChildEntities)
            {
                if (entity.ReferencedArchetype != null && !(entity.ReferencedArchetype is IInteriorArchetype))
                {
                    IMapArchetype mapArchetype = (IMapArchetype)entity.ReferencedArchetype;

                    if (mapArchetype.HasExplosiveEffect == true)
                    {
                        yield return entity;
                    }
                }
            }
        }
        #endregion // Overrides
    } // ExplosivePropTypeOverlay

    [ExportExtension(ExtensionPoints.PropTypeStatisticsOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class AttachedLightPropTypeOverlay : PropTypeOverlayBase
    {
        #region Constants
        /// <summary>
        /// 
        /// </summary>
        private const string c_name = "Has Attached Lights";
        private const string c_description = "Shows all entities that have lights attached to them on the map.";
        #endregion // Constants

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public AttachedLightPropTypeOverlay()
            : base(c_name, c_description)
        {
        }
        #endregion // Constructor

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        protected override IEnumerable<IEntity> GetValidEntities(IMapSection section)
        {
            if (section == null)
            {
                yield break;
            }

            foreach (IEntity entity in section.ChildEntities)
            {
                if (entity.ReferencedArchetype != null && !(entity.ReferencedArchetype is IInteriorArchetype))
                {
                    IMapArchetype mapArchetype = (IMapArchetype)entity.ReferencedArchetype;

                    if (mapArchetype.HasAttachedLight == true)
                    {
                        yield return entity;
                    }
                }
            }
        }
        #endregion // Overrides
    } // AttachedLightPropTypeOverlay

    [ExportExtension(ExtensionPoints.PropTypeStatisticsOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class ClothPropTypeOverlay : PropTypeOverlayBase
    {
        #region Constants
        /// <summary>
        /// 
        /// </summary>
        private const string c_name = "Cloth";
        private const string c_description = "Shows all cloth entities on the map.";
        #endregion // Constants

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public ClothPropTypeOverlay()
            : base(c_name, c_description)
        {
        }
        #endregion // Constructor

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        protected override IEnumerable<IEntity> GetValidEntities(IMapSection section)
        {
            if (section == null)
            {
                yield break;
            }

            foreach (IEntity entity in section.ChildEntities)
            {
                if (entity.ReferencedArchetype is IFragmentArchetype)
                {
                    IFragmentArchetype fragment = (IFragmentArchetype)entity.ReferencedArchetype;

                    if (fragment.IsCloth == true)
                    {
                        yield return entity;
                    }
                }
            }
        }
        #endregion // Overrides
    } // ClothPropTypeOverlay
} // Workbench.AddIn.MapStatistics.Overlays
