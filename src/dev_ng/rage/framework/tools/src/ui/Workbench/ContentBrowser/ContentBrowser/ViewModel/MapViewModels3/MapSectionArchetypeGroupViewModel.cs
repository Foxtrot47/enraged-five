﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common.Map;
using ContentBrowser.ViewModel.GridViewModels;

namespace ContentBrowser.ViewModel.MapViewModels3
{
    /// <summary>
    /// 
    /// </summary>
    public class MapSectionArchetypeGroupViewModel : ContainerViewModelBase
    {
        #region Properties
        /// <summary>
        /// The main name property which by default will be shown as the header for a browser item
        /// </summary>
        public override String Name
        {
            get
            {
                return "Archetypes";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private IMapSection Section
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        private bool HasDrawables
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        private bool HasFragments
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        private bool HasInteriors
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        private bool HasStatedAnims
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public MapSectionArchetypeGroupViewModel(IMapSection section)
            : base(section)
        {
            Section = section;

            HasDrawables = (section.Archetypes.OfType<IDrawableArchetype>().Any());
            HasFragments = (section.Archetypes.OfType<IFragmentArchetype>().Any());
            HasInteriors = (section.Archetypes.OfType<IInteriorArchetype>().Any());
            HasStatedAnims = (section.Archetypes.OfType<IStatedAnimArchetype>().Any());
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// This gets called whenever the selection state for this view model
        /// has changed
        /// </summary>
        protected override void OnSelectionChanged(Boolean oldValue, Boolean newValue)
        {
            GridAssets.Clear();
            if (newValue == true)
            {
                if (HasDrawables)
                {
                    GridAssets.Add(new DummyAssetContainerGridViewModel("Drawables", null));
                }

                if (HasFragments)
                {
                    GridAssets.Add(new DummyAssetContainerGridViewModel("Fragments", null));
                }

                if (HasInteriors)
                {
                    GridAssets.Add(new DummyAssetContainerGridViewModel("Interiors", null));
                }

                if (HasStatedAnims)
                {
                    GridAssets.Add(new DummyAssetContainerGridViewModel("Stated Anims", null));
                }
            }
        }

        /// <summary>
        /// This gets called whenever the expansion state for this view model
        /// has changed
        /// </summary>
        protected override void OnExpansionChanged(Boolean oldValue, Boolean newValue)
        {
            AssetChildren.Clear();
            if (newValue == true)
            {
                if (HasDrawables)
                {
                    AssetChildren.Add(new ArchetypeGroupViewModel<IDrawableArchetype>("Drawables", Section));
                }

                if (HasFragments)
                {
                    AssetChildren.Add(new ArchetypeGroupViewModel<IFragmentArchetype>("Fragments", Section));
                }

                if (HasInteriors)
                {
                    AssetChildren.Add(new ArchetypeGroupViewModel<IInteriorArchetype>("Interiors", Section));
                }

                if (HasStatedAnims)
                {
                    AssetChildren.Add(new ArchetypeGroupViewModel<IStatedAnimArchetype>("Stated Anims", Section));
                }
            }
            else
            {
                AssetChildren.Add(new AssetDummyViewModel());
            }
        }
        #endregion // Overrides
    } // MapSectionArchetypeGroupViewModel
}
