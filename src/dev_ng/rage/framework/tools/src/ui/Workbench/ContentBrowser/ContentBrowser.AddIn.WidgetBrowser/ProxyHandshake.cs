﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ragCore;
using System.Threading;
using RSG.Base.Logging;
using System.Diagnostics;
using System.Net.Sockets;

namespace ContentBrowser.AddIn.WidgetBrowser
{
    public class ProxyHandshake
    {
        #region Constants
        /// <summary>
        /// Handshake value to help ensure that Rag code remains synchronized with Rage bank code.  Increment this value 
        /// whenever there's a change in Rag code that requires a change in Rage code.  Keep this in sync with 
        /// RAG_VERSION in rage/base/tools/rag/RageApplication.cs.
        /// </summary>
        public const float RAG_VERSION = 1.972f;
        #endregion

        public enum EnumPacketType
        {
            HANDSHAKE,
            NUM_APPS,
            APP_NAME,
            APP_VISIBLE_NAME,
            APP_ARGS,
            PIPE_NAME_BANK,
            PIPE_NAME_OUTPUT,
            PIPE_NAME_EVENTS,
            END_OUTPUT,
            WINDOW_HANDLE,
            PLATFORM_INFO,
            PS3_TARGET_ADDRESS
        };

        public class HandshakeResult
        {
            public uint MasterIndex;
            public List<TcpListener> ReservedSockets;

            public PipeID PipeNameBank;
            public PipeID PipeNameOutput;
            public PipeID PipeNameEvents;

            /// <summary>
            /// The display name of the Application.  Typically displayed as the Application Window's title.
            /// </summary>
            public string VisibleName;

            /// <summary>
            /// A unique identifier for this Application.
            /// </summary>
            public string Guid;

            /// <summary>
            /// The path of the Application.
            /// </summary>
            public string ExePath;

            /// <summary>
            /// The name of the Application
            /// </summary>
            public string ExeName;

            /// <summary>
            /// The arguments pass to the Application.
            /// </summary>
            public string Args;

            /// <summary>
            /// Whether the Application (one that is not the Main Application) should start when the Main Application starts.
            /// </summary>
            public bool ShouldStart;

            /// <summary>
            /// Whether the Application is running or not.
            /// </summary>
            public bool IsStarted;

            /// <summary>
            /// Whether the Application was stopped or not.
            /// </summary>
            public bool IsStopped;

            public string Path;

            public string PlatformString;

            public int PortNumber;
        }



        public static bool DoHandshake( INamedPipe pipe, out bool timedOut, ref HandshakeResult result )
        {
            RemotePacket p = new RemotePacket( pipe );

            timedOut = false;
            int retries = 40;   // 4 seconds

            while ( true )
            {
                if ( !pipe.HasData() )
                {
                    --retries;
                    if ( retries == 0 )
                    {
                        timedOut = true;
                        return false;
                    }

                    Thread.Sleep( 100 );
                    continue;
                }


                retries = 40;   // reset
                while ( pipe.HasData() )
                {
                    p.ReceiveHeader( pipe );
                    if ( p.Length > 0 )
                    {
                        p.ReceiveStorage( pipe );
                    }

                    // see if we're done parsing:
                    EnumPacketType appCommand = (EnumPacketType)p.Command;
                    //Log.Log__Debug( appCommand.ToString() );
                    if ( appCommand == EnumPacketType.END_OUTPUT )
                    {
                        return true;
                    }
                    else if ( !ReadPacket( p, ref result ) )
                    {
                        return false;
                    }
                }
            }
        }


        public static bool ReadPacket( RemotePacket packet, ref HandshakeResult result )
        {
            uint index;
            bool success = true;

            EnumPacketType appCommand = (EnumPacketType)packet.Command;

            // our initial handshake packet containing the game's rag version number
            if ( appCommand == EnumPacketType.HANDSHAKE )
            {
                packet.Begin();
                float ragVersion = ragVersion = packet.Read_float(); ;
                packet.End();

                if ( ragVersion != RAG_VERSION )
                {
                    string title;
                    string msg;
                    if ( ragVersion == 0.0f )
                    {
                        title = "Rag Version Number Not Received";
                        msg = "Rag and Rage bank code are out of sync.  Need " + RAG_VERSION + " from Bank.\n\nPlease update your bank code.";
                    }
                    else
                    {
                        title = "Rag Version Mismatch";
                        msg = "Rag and Rage bank code are out of sync.  Expected " + RAG_VERSION + " but received " + ragVersion + "from Bank.\n\nPlease update " + ((RAG_VERSION < ragVersion) ? "Rag." : "your bank code.");
                    }

                    // todo rageMessageBox.ShowError( msg, title );
                    Log.Log__Error( msg );
                    return false;
                }
            }
            // # of applications to create:
            else if ( appCommand == EnumPacketType.NUM_APPS )
            {
                packet.Begin();
                uint numApps = packet.Read_u32();
                result.MasterIndex = packet.Read_u32();
                packet.End();

                Debug.Assert( result.MasterIndex == 0, "m_MasterIndex < numApps" ); // Only support one "app"
                Debug.Assert( result.MasterIndex < numApps, "m_MasterIndex < numApps" );



                // try to retrieve the ip address of the application we're connected to
                if ( packet.Pipe is NamedPipeSocket )
                {
                    NamedPipeSocket namedPipe = packet.Pipe as NamedPipeSocket;
                    Sensor.MainApplicationIpAddress = namedPipe.GetRemoteIpAddress();
                }
            }
            // the path of an application:
            else if (  appCommand == EnumPacketType.APP_NAME )
            {
                packet.Begin();
                index = packet.Read_u32();
                result.Path += packet.Read_const_char();
                packet.End();
            }
            // the visible name of an application:
            else if (  appCommand == EnumPacketType.APP_VISIBLE_NAME )
            {
                packet.Begin();
                index = packet.Read_u32();
                result.VisibleName += packet.Read_const_char();
                packet.End();
            }
            // the application args:
            else if (  appCommand == EnumPacketType.APP_ARGS )
            {
                packet.Begin();
                index = packet.Read_u32();
                result.Args += packet.Read_const_char();
                packet.End();
            }
            // the name of the pipe for bank communications:
            else if (  appCommand == EnumPacketType.PIPE_NAME_BANK )
            {
                packet.Begin();
                index = packet.Read_u32();
                result.PipeNameBank.AddString( packet.Read_const_char() );
                packet.End();
            }
            // the name of the pipe for text output communications:
            else if (  appCommand == EnumPacketType.PIPE_NAME_OUTPUT )
            {
                packet.Begin();
                index = packet.Read_u32();
                result.PipeNameOutput.AddString( packet.Read_const_char() );
                packet.End();
            }
            // the name of the pipe for event handling communications:
            else if (  appCommand == EnumPacketType.PIPE_NAME_EVENTS )
            {
                packet.Begin();
                index = packet.Read_u32();
                result.PipeNameEvents.AddString( packet.Read_const_char() );
                packet.End();
            }
            else if (  appCommand == EnumPacketType.PLATFORM_INFO )
            {
                packet.Begin();
                index = packet.Read_u32();
                result.PlatformString = packet.Read_const_char();
                packet.End();
            }
            else if (  appCommand == EnumPacketType.PS3_TARGET_ADDRESS )
            {
                packet.Begin();
                index = packet.Read_u32();
                string ps3TargetAddr = packet.Read_const_char();
                Sensor.PS3TargetIpAddress = ps3TargetAddr;
                packet.End();
            }
            else
            {
                success = false;
            }

            return success;
        }

    }

}
