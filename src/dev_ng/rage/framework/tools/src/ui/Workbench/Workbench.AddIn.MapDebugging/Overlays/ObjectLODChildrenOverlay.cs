﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading;
using System.Windows.Media;
using RSG.Base.Collections;
using RSG.Base.Math;
using RSG.Base.Tasks;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using RSG.Base.Windows.Dialogs;
using RSG.Model.Common.Extensions;
using RSG.Model.Common.Map;
using Workbench.AddIn.MapDebugging.OverlayViews;
using RSG.Model.Common.Util;
using MapViewport.AddIn;
using RSG.Model.Common;
using Workbench.AddIn.Services.Model;

namespace Workbench.AddIn.MapDebugging.Overlays
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension("Workbench.AddIn.MapDebugging.OverlayGroups.MapDebug", typeof(Viewport.AddIn.ViewportOverlay))]
    public class ObjectLODChildrenOverlay : LevelDependentViewportOverlay
    {
        #region Constants
        /// <summary>
        /// Overlay name/description.
        /// </summary>
        private const String c_name = "Object LOD Children";
        private const String c_description = "Displays the location of the selected object on the map. If a definition is selected it will" +
                                             " show all instances of that definition on the map.";
        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// A reference to view model for the map viewport
        /// </summary>
        [ImportExtension(Viewport.AddIn.CompositionPoints.MapViewport, typeof(Viewport.AddIn.IMapViewport))]
        public Viewport.AddIn.IMapViewport MapViewport { get; set; }
        
        /// <summary>
        /// A reference to view model for the map viewport
        /// </summary>
        [ImportExtension(Viewport.AddIn.CompositionPoints.MapViewport, typeof(Viewport.AddIn.IMapViewport))]
        public Viewport.AddIn.IMapViewport MapViewportProxy { get; set; }
        #endregion // MEF Imports

        #region Member Variables
        /// <summary>
        /// Statistics on all of the map instances
        /// </summary>
        private List<LODChildrenStatistics> m_allStatistics;
        #endregion

        #region Helper Class
        /// <summary>
        /// 
        /// </summary>
        public class LODChildrenStatistics
        {
            public event EventHandler DisplayChanged;
            public bool Display
            {
                get { return m_display; }
                set
                {
                    if (m_display != value)
                    {
                        m_display = value;

                        if (DisplayChanged != null)
                        {
                            DisplayChanged(this, EventArgs.Empty);
                        }
                    }
                }
            }
            private bool m_display;

            public int NumLODChildren { get; set; }
            public IList<IEntity> Entities { get; protected set; }

            public LODChildrenStatistics()
                : this(false, 0)
            {
            }

            public LODChildrenStatistics(bool display, int occurrences)
            {
                Display = display;
                NumLODChildren = occurrences;
                Entities = new List<IEntity>();
            }
        }
        #endregion

        #region Properties
        /// <summary>
        /// A list of statistics that are filtered based on the minimum child lod count
        /// </summary>
        public ObservableCollection<LODChildrenStatistics> FilteredStatistics
        {
            get;
            protected set;
        }

        /// <summary>
        /// Minimum number of LOD children to display in the listview
        /// </summary>
        public uint MinLODChildrenCount
        {
            get { return m_minLODChildrenCount; }
            set
            {
                SetPropertyValue(value, () => MinLODChildrenCount,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_minLODChildrenCount = (uint)newValue;
                            UpdateFilteredStatistics();
                            UpdateDisplayedGeometry();
                        }
                ));
            }
        }
        private uint m_minLODChildrenCount;
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public ObjectLODChildrenOverlay()
            : base(c_name, c_description)
        {
            m_allStatistics = new List<LODChildrenStatistics>();
            m_minLODChildrenCount = 0;
            FilteredStatistics = new ObservableCollection<LODChildrenStatistics>();
        }
        #endregion // Constructors

        #region Overrides
        /// <summary>
        /// Gets called when the user wants to view this overlay
        /// </summary>
        public override void Activated()
        {
            base.Activated();

            // Make sure that the sections have the required data loaded
            CancellationTokenSource cts = new CancellationTokenSource();
            LevelTaskContext context = new LevelTaskContext(cts.Token, LevelBrowserProxy.Value.SelectedLevel);

            ITask loadDataTask = new ActionTask("Load Data", (ctx, progress) => EnsureDataLoaded((LevelTaskContext)ctx, progress));

            TaskExecutionDialog dialog = new TaskExecutionDialog();
            TaskExecutionViewModel vm = new TaskExecutionViewModel("Generating Overlay", loadDataTask, cts, context);
            vm.AutoCloseOnCompletion = true;
            dialog.DataContext = vm;
            Nullable<bool> selectionResult = dialog.ShowDialog();
            if (false == selectionResult)
            {
                return;
            }
            
            // Iterate over all the map sections populating the instance list
            IDictionary<int, LODChildrenStatistics> entityMap = new Dictionary<int, LODChildrenStatistics>();

            IMapHierarchy hiearchy = LevelBrowserProxy.Value.SelectedLevel.GetMapHierarchy();

            if (hiearchy != null)
            {
                foreach (IMapSection section in hiearchy.AllSections)
                {
                    foreach (IEntity entity in section.ChildEntities)
                    {
                        if (!entityMap.ContainsKey(entity.LodChildren.Count))
                        {
                            entityMap[entity.LodChildren.Count] = new LODChildrenStatistics(false, entity.LodChildren.Count);
                        }
                        entityMap[entity.LodChildren.Count].Entities.Add(entity);
                    }
                }
            }

            // Clear out any existing instances and populate with the ones we just found
            m_allStatistics.Clear();
            foreach (KeyValuePair<int, LODChildrenStatistics> pair in entityMap)
            {
                pair.Value.DisplayChanged += OccurrenceStatsDisplayChanged;
                m_allStatistics.Add(pair.Value);
            }

            // Sort the data by occurrence count
            m_allStatistics.Sort(delegate(LODChildrenStatistics a, LODChildrenStatistics b)
                             {
                                 if (a == null)
                                 {
                                     return (b == null ? 0 : 1);
                                 }
                                 else
                                 {
                                     return (b == null ? -1 : b.NumLODChildren.CompareTo(a.NumLODChildren));
                                 }
                             });

            // Update the filtered list and the geometry that is displayed on screen
            UpdateFilteredStatistics();
            UpdateDisplayedGeometry();
        }

        /// <summary>
        /// Gets called when the user turns off this overlay
        /// </summary>
        public override void Deactivated()
        {
            foreach (LODChildrenStatistics stat in m_allStatistics)
            {
                stat.DisplayChanged -= OccurrenceStatsDisplayChanged;
            }
            m_allStatistics.Clear();
            FilteredStatistics.Clear();

            base.Deactivated();
        }

        /// <summary>
        /// Gets the control that should be shown in the overlay details panel
        /// </summary>
        public override System.Windows.Controls.Control GetOverlayControl()
        {
            return new ObjectLODChildrenOverlayView();
        }
        #endregion // Overrides

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="progress"></param>
        private void EnsureDataLoaded(LevelTaskContext context, IProgress<TaskProgress> progress)
        {
            IMapHierarchy hierarchy = context.Level.GetMapHierarchy();

            if (hierarchy != null)
            {
                float increment = 1.0f / hierarchy.AllSections.Count();

                // Load all the map section data
                foreach (IMapSection section in hierarchy.AllSections)
                {
                    context.Token.ThrowIfCancellationRequested();

                    // Update the progress information
                    string message = String.Format("Loading {0}", section.Name);
                    progress.Report(new TaskProgress(increment, true, message));

                    // Only request that the entities for this section are loaded
                    section.LoadStats(new StreamableStat[] { StreamableMapSectionStat.Entities });

                    // Request the locations of all the entities
                    /*
                    foreach (IEntity entity in section.ChildEntities)
                    {
                        entity.RequestStatistics(new StreamableEntityStat[] { StreamableEntityStat.LodChildren, StreamableEntityStat.LodDistance, StreamableEntityStat.Position}, false);
                    }
                    */
                }
            }
        }

        /// <summary>
        /// Updates the list of filtered statistics based on the min lod children count
        /// </summary>
        private void UpdateFilteredStatistics()
        {
            FilteredStatistics.BeginUpdate();
            FilteredStatistics.Clear();

            foreach (LODChildrenStatistics stat in m_allStatistics)
            {
                if (stat.NumLODChildren >= MinLODChildrenCount)
                {
                    FilteredStatistics.Add(stat);
                }
            }

            FilteredStatistics.EndUpdate();
        }

        /// <summary>
        /// 
        /// </summary>
        private void OccurrenceStatsDisplayChanged(object sender, EventArgs e)
        {
            UpdateDisplayedGeometry();
        }

        /// <summary>
        /// 
        /// </summary>
        private void UpdateDisplayedGeometry()
        {
            if (LevelBrowserProxy.Value.SelectedLevel != null)
            {
                Vector2i size = new Vector2i(966, 627);
                Vector2f pos = new Vector2f(-483.0f, 627.0f);

                if (LevelBrowserProxy.Value.SelectedLevel.ImageBounds != null)
                {
                    size = new RSG.Base.Math.Vector2i((int)(LevelBrowserProxy.Value.SelectedLevel.ImageBounds.Max.X - LevelBrowserProxy.Value.SelectedLevel.ImageBounds.Min.X), (int)(LevelBrowserProxy.Value.SelectedLevel.ImageBounds.Max.Y - LevelBrowserProxy.Value.SelectedLevel.ImageBounds.Min.Y));
                    pos = new RSG.Base.Math.Vector2f(LevelBrowserProxy.Value.SelectedLevel.ImageBounds.Min.X, LevelBrowserProxy.Value.SelectedLevel.ImageBounds.Max.Y);
                }

                Viewport2DImageOverlay image = new Viewport2DImageOverlay(etCoordSpace.World, "", new RSG.Base.Math.Vector2i((int)(size.X * 0.25f), (int)(size.Y * 0.25f)), pos, new RSG.Base.Math.Vector2f(size.X, size.Y));

                this.Geometry.BeginUpdate();
                this.Geometry.Clear();
                image.BeginUpdate();


                //// Clear the geometry
                //this.Geometry.BeginUpdate();
                //this.Geometry.Clear();

                int lowestCount = 0;
                int highestCount = 0;
                int difference = 0;

                if (FilteredStatistics.Count > 0)
                {
                    lowestCount = m_allStatistics[m_allStatistics.Count - 1].NumLODChildren;
                    highestCount = m_allStatistics[0].NumLODChildren;
                    difference = highestCount - lowestCount;
                }

                // Add a filled circle for all selected stats
                foreach (LODChildrenStatistics stat in FilteredStatistics)
                {
                    if (stat.NumLODChildren >= MinLODChildrenCount && stat.Display)
                    {
                        float percentage = (float)stat.NumLODChildren / (float)difference;
                        byte r = (byte)(255.0f * percentage);
                        byte g = (byte)(255.0f - (255.0f * percentage));
                        System.Drawing.Color bgColor = System.Drawing.Color.FromArgb(128, r, g, 0);

                        foreach (IEntity entity in stat.Entities)
                        {
                            image.RenderCircle(new Vector2f(entity.Position.X, entity.Position.Y), (float)entity.LodDistance, bgColor, 2.0f);
                        }
                    }
                }


                image.EndUpdate();
                this.Geometry.Add(image);
                this.Geometry.EndUpdate();
            }
        }
        #endregion
    } // ObjectLODChildrenOverlay
} // Workbench.AddIn.MapDebugging.Overlays

