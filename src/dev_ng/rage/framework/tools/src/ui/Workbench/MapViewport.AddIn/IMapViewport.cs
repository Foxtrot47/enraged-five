﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using RSG.Base.Collections;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using RSG.Base.Windows.Controls.WpfViewport2D.Controls;
using RSG.Base.Math;


namespace Viewport.AddIn
{
    /// <summary>
    /// 
    /// </summary>
    public interface IMapViewport
    {
        #region Events

        event OverlayChangedEventHandler OnOverlayChanged;
        event ViewportClickEventHandler ViewportClicked;
        event EventHandler BeforeSaveOverlay;

        #endregion // Events

        #region Properties

        /// <summary>
        /// The current overlay that the viewport is currently showing
        /// </summary>
        Viewport.AddIn.ViewportOverlay SelectedOverlay { get; set; }

        /// <summary>
        /// The geoemtry that is currently selected in the viewport
        /// </summary>
        ObservableCollection<Object> SelectedOverlayGeometry { get; }

        #endregion // Properties

        #region Methods

        /// <summary>
        /// Sets up the overlays for the map viewport
        /// </summary>
        void SetOverlays(List<Viewport.AddIn.IViewportOverlayGroup> overlayGroups);
        
        /// <summary>
        /// Clears the selected pieces of geometry
        /// </summary>
        void ClearSelectedGeometry();

        /// <summary>
        /// Sets a particular piece of geometry as selected
        /// </summary>
        /// <param name="selectedGeometry"></param>
        void SetGeometryAsSelected(Viewport2DGeometry selectedGeometry);

        /// <summary>
        /// Makes the viewport center its view on a particular position
        /// </summary>
        /// <param name="position"></param>
        void CenterViewportOn(Vector2f position);

        /// <summary>
        /// Zoom's the viewport in to ensure that a particular bounding box is centered
        /// </summary>
        /// <param name="bbox"></param>
        void ZoomViewportToFit(BoundingBox2f bbox);

        /// <summary>
        /// Shows the tool tip to the map viewport.
        /// </summary>
        void ShowToolTip(Point location);

        /// <summary>
        /// Hides the tool tip in the map viewport.
        /// </summary>
        void HideToolTip();

        #endregion // Methods
    } // IMapViewport
} // Viewport.AddIn
