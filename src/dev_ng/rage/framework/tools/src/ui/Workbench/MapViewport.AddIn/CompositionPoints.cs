﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Viewport.AddIn
{
    /// <summary>
    /// 
    /// </summary>
    public static class CompositionPoints
    {
        /// <summary>
        /// 
        /// </summary>
        public const String MapViewport = "E39EC0EF-DF2B-44F1-9C70-9061205317CF";

        /// <summary>
        /// Viewport settings composition point
        /// </summary>
        public const String MapViewportSettings = "A2D1A069-992F-41B0-A3E6-279763E6AA54";

        /// <summary>
        /// Parentizer settings composition point
        /// </summary>
        public const String ParentizerSettings = "A8B50C8D-655A-4762-9AE2-CE9EFEC1AEBB";
    }
} // MapViewport.AddIn
