﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Map3;
using RSG.Model.Common.Map;
using System.IO;
using System.Xml;
using Workbench.AddIn;

namespace LevelBrowser.AddIn.MapBrowser.BatchExporter.Model
{
    [ExportExtension(ExtensionPoints.ExportSettingsModel, typeof(IBatchExportCollection))]
    public class BatchSectionDictionary : IBatchExportCollection
    {
        #region Private member fields

        private Dictionary<string, MapSection> m_sections;

        #endregion

        #region Public properties

        /// <summary>
        /// Returns true if the collection contains at least one item.
        /// </summary>
        public bool HasItems
        {
            get { return m_sections.Count > 0; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        public BatchSectionDictionary()
        {
            m_sections = new Dictionary<string, MapSection>();
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Add a map section to the internal collection.
        /// </summary>
        /// <param name="mapName">Map name.</param>
        /// <param name="mapSection">Map section.</param>
        public void Add(string mapName, MapSection mapSection)
        {
            if (!m_sections.ContainsKey(mapName))
            {
                m_sections.Add(mapName, mapSection);
            }
        }

        /// <summary>
        /// Remove all map sections from the internal collection.
        /// </summary>
        public void RemoveAll()
        {
            m_sections.Clear();
        }

        /// <summary>
        /// Remove a single item from the list.
        /// </summary>
        /// <param name="section">Section.</param>
        public void Remove(MapSection section)
        {
            m_sections.Remove(section.Name);
        }

        /// <summary>
        /// Enumerates the internal collection of MapSection instances.
        /// </summary>
        /// <returns>An enumeration of MapSection instances.</returns>
        public IEnumerable<MapSection> GetSections()
        {
            foreach (MapSection section in m_sections.Values)
            {
                yield return section;
            }
        }

        /// <summary>
        /// Enumerates the key value pair of section name and section data.
        /// </summary>
        /// <returns>An enumeration of string and MapSection key value pairs.</returns>
        public IEnumerable<KeyValuePair<string, MapSection>> GetAll()
        {
            foreach (var kvp in m_sections)
            {
                yield return kvp;
            }
        }

        #endregion

        #region Serialization methods

        /// <summary>
        /// Populate map section data from the map hierarchy.
        /// </summary>
        /// <param name="mapHierarchy">Map hierarchy containing MapSection data.</param>
        public void PopulateMapSections(IMapHierarchy mapHierarchy)
        {
            List<KeyValuePair<string, MapSection>> newSections = new List<KeyValuePair<string, MapSection>>();

            foreach (var kvp in m_sections)
            {
                if (kvp.Value == null)
                {
                    MapSection mapSection = (MapSection)mapHierarchy.AllSections.Where(section => section.Name.Equals(kvp.Key)).FirstOrDefault();
                    if (mapSection != null)
                    {
                        newSections.Add(new KeyValuePair<string, MapSection>(kvp.Key, mapSection));
                    }
                }
            }

            foreach (var kvp in newSections)
            {
                m_sections[kvp.Key] = kvp.Value;
            }
        }

        /// <summary>
        /// Deserializes the file stored on disk that contains the list of MapSections.
        /// </summary>
        /// <param name="pathToFile">Path to the file.</param>
        /// <param name="mapHierarchy">The current level's map hierarchy.</param>
        /// <returns></returns>
        public void Deserialize(string pathToFile, IMapHierarchy mapHierarchy)
        {
            if (File.Exists(pathToFile))
            {
                using (Stream s = File.Open(pathToFile, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(s);

                    XmlNodeList nodes = doc.SelectNodes("//section");
                    foreach (XmlNode node in nodes)
                    {
                        string name = node.Attributes["name"].Value;
                        MapSection mapSection = mapHierarchy != null ? mapHierarchy.AllSections.Where(section => section.Name == name).FirstOrDefault() as MapSection : null;
                        Add(name, mapSection);
                    }
                }
            }
            else
            {
                System.Windows.MessageBox.Show(Path.GetFileName(pathToFile) + " not found.");
            }
        }

        /// <summary>
        /// Serializes the MapSection collection to disk.
        /// </summary>
        /// <param name="pathToFile">Path to the file where the data will be stored.</param>
        public void Serialize(string pathToFile)
        {
            if (File.Exists(pathToFile))
            {
                try
                {
                    File.Delete(pathToFile);
                }
                catch (IOException)
                {
                    System.Windows.MessageBox.Show("Error writing to file. Could not overwrite " + Path.GetFileName(pathToFile) + ". File not saved.");
                    return;
                }
            }

            using (Stream s = File.Open(pathToFile, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Read))
            {
                using (TextWriter writer = new StreamWriter(s))
                {
                    using (XmlTextWriter xml = new XmlTextWriter(writer))
                    {
                        xml.WriteStartDocument();

                        xml.WriteStartElement("sections");

                        foreach (MapSection section in GetSections())
                        {
                            xml.WriteStartElement("section");

                            xml.WriteStartAttribute("name");
                            xml.WriteValue(section.Name);
                            xml.WriteEndAttribute();

                            xml.WriteEndElement();
                        }

                        xml.WriteEndElement();

                        xml.WriteEndDocument();
                    }
                }
            }
        }

        #endregion

    }
}
