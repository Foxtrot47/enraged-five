﻿using System;
using System.Windows.Input;
using System.Collections.Generic;
using System.Drawing;
using RSG.Base.Editor;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.Services;

namespace Workbench.AddIn.Services.File
{

    /// <summary>
    /// Service to open an existing IModel from disk file.
    /// </summary>
    public interface IOpenService : IService
    {
        #region Properties

        /// <summary>
        /// Open service header string for UI display.
        /// </summary>
        String Header { get;  }

        /// <summary>
        /// Open service Bitmap for UI display.
        /// </summary>
        Bitmap Image { get; }

        /// <summary>
        /// Open dialog filter string.
        /// </summary>
        String Filter { get; }

        /// <summary>
        /// Determines if the open dialog allows multiple items selected
        /// </summary>
        Boolean AllowMultiSelect { get; }

        /// <summary>
        /// Open dialog default filter index.
        /// </summary>
        /// For when you have multiple filter options you can have a non-zero
        /// default if required.
        int DefaultFilterIndex { get; }

        /// <summary>
        /// The keybinding that is placed on this command.
        /// This binding is added to the applications bindings.
        /// (Only used if this is a main menu item)
        /// </summary>
        KeyGesture KeyGesture { get; }

        /// <summary>
        /// Command line key to open filenames with this particular service.
        /// E.g. "meta".
        /// </summary>
        String CommandLineKey { get; }
        #endregion // Properties

        #region Methods

        /// <summary>
        /// Open the specified file and store in-memory IModel.
        /// </summary>
        bool Open(out IDocumentBase doc, out IModel model, String filename, int filterIndex);

        /// <summary>
        /// Open the specified file and store in-memory IModel.
        /// </summary>
        bool Open(out Dictionary<IDocumentBase, List<String>> docs, out List<IModel> models, List<String> filename, int filterIndex);

        /// <summary>
        /// Creates a new document based on the specified model
        /// </summary>
        /// <param name="model">
        /// The model that determines what document is created.
        /// </param>
        /// <returns>
        /// A new document that has its model/viewmodel setup to represent the specified model.
        /// </returns>
        IDocumentBase Open(IModel model);
        #endregion // Methods
    } // IOpenService

} // Workbench.AddIn.Services.File namespace
