﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetadataEditor.AddIn
{

    /// <summary>
    /// Metadata Editor composition point String GUIDs contracts.
    /// </summary>
    public static class CompositionPoints
    {

        /// <summary>
        /// Metadata Editor Controller.
        /// </summary>
        public const String StructureDictionary =
            "C6F45C4D-B18E-4AB4-B328-9B2A48C8A11E";
    
        /// <summary>
        /// Metadata Editor Property Inspector tool window.
        /// </summary>
        public const String PropertyInspector =
            "E634CE66-01E4-4C3E-B7B3-7B98ECDEB0A8";

        /// <summary>
        /// 
        /// </summary>
        public const String MetadataOpenService =
            "0FACA4D2-6F8B-423A-94FF-A5E53BA60EC5";

        /// <summary>
        /// 
        /// </summary>
        public const String LiveEditingService =
            "E5F817BA-A28D-4DA4-85BE-1D3AAD027112";
    }

} // MetadataEditor.AddIn.CompositionPoints
