﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Workbench.AddIn.Bugstar.OverlayViews
{
    /// <summary>
    /// Interaction logic for BugRadiusDetailsView.xaml
    /// </summary>
    public partial class BugRadiusDetailsView : UserControl
    {
        public BugRadiusDetailsView()
        {
            InitializeComponent();
        }
    }
}
