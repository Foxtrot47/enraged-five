﻿using System;
using System.Windows.Input;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Windows;
using RSG.Base.Editor;
using Workbench.AddIn;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.Services;
using Workbench.AddIn.Services.File;

namespace MetadataEditor.AddIn.KinoEditor.Cutscene_Batch_Exporter.Services.File
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.NewService, typeof(INewService))]
    class NewService : INewService
    {
        #region Constants

        private readonly Guid GUID = new Guid("2B21FF8B-45A8-4F5D-A6A7-EB61BC217B5C");

        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// MEF import for core workbench services.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.CoreServicesProvider,
            typeof(Workbench.AddIn.Services.ICoreServiceProvider))]
        private Lazy<Workbench.AddIn.Services.ICoreServiceProvider> CoreServiceProvider { get; set; }

        /// <summary>
        /// The perforce sync service
        /// </summary>
        [Workbench.AddIn.ImportExtension(Workbench.AddIn.CompositionPoints.PerforceSyncService, 
            typeof(Workbench.AddIn.Services.IPerforceSyncService))]
        private Lazy<Workbench.AddIn.Services.IPerforceSyncService> PerforceSyncService { get; set; }

        /// <summary>
        /// MEF import for save service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.WorkbenchSaveService, 
            typeof(Workbench.AddIn.Services.File.IWorkbenchSaveCurrentService))]
        private Lazy<Workbench.AddIn.Services.File.IWorkbenchSaveCurrentService> SaveService { get; set; }
        #endregion // MEF Imports

        #region Properties

        public Guid ID
        {
            get { return GUID; }
        }

        /// <summary>
        /// New service header string for UI display.
        /// </summary>
        public String Header
        {
            get;
            private set;
        }

        /// <summary>
        /// New service Bitmap for UI display.
        /// </summary>
        public Bitmap Image
        {
            get;
            private set;
        }

        /// <summary>
        /// The keybinding that is placed on this command.
        /// This binding is added to the applications bindings.
        /// (Only used if this is a main menu item)
        /// </summary>
        public KeyGesture KeyGesture
        {
            get { return null; }
        }

        #endregion // Properties

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public NewService()
        {
            this.Header = "New Cutscene Batch Exporter File...";
            this.Image = null;
        }

        #endregion // Constructor

        #region Methods

        /// <summary>
        /// 
        /// </summary>
        public bool PreNew(out Object param)
        {
            param = null;
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool New(out IDocumentBase document, out IModel model, String filename, Object param)
        {
            document = new CutsceneBatchExporterToolWindowView(filename, this.CoreServiceProvider.Value.TaskProgressService);
            model = (document as DocumentBase<CutsceneBatchExportToolWindowViewModel>).ViewModel;

            return true;
        }

        #endregion // Methods
    }
}
