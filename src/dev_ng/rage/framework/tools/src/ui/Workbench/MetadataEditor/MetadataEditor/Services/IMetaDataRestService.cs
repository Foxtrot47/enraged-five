﻿using System.ServiceModel;
using System.ServiceModel.Web;
using Workbench.AddIn.REST;
using MetadataEditor.RestData;

namespace MetadataEditor.Services
{
    [ServiceContract]
    public interface IMetaDataRestService : IRestService
    {
        /// <summary>
        /// Opens up the requested file
        /// </summary>
        /// <param name="file"></param>
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "OpenFile?name={file}")]
        void OpenFile(string file);

        /// <summary>
        /// Opens up the requested file(s)
        /// </summary>
        /// <param name="file"></param>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "OpenFiles")]
        void OpenFiles(FileList fileList);

        /// <summary>
        /// Attempts to figure out what file we should open based on information we received from RAG
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "OpenFileFromGame")]
        void OpenFileFromGame(MetadataInfo info);
    }
}
