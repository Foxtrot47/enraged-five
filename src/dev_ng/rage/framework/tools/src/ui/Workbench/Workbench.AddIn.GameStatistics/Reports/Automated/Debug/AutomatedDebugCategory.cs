﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Report;
using Report.AddIn;

namespace Workbench.AddIn.GameStatistics.Reports.Automated.Debug
{
    /// <summary>
    /// Category for debug reports based on the stats gathered from the automated tests that run on cruise.
    /// </summary>
    [ExportExtension(ExtensionPoints.AutomatedReport, typeof(IReport))]
    public class AutomatedDebugCategory : ReportCategory
    {
        #region Constants
        private const String c_name = "Debug";
        private const String c_description = "Debug reports based on the stats gathered from the automated tests that run on cruise.";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Imported reports for this category.
        /// </summary>
        [ImportManyExtension(ExtensionPoints.AutomatedDebugReport, typeof(IReport))]
        public override IEnumerable<IReportItem> Reports
        {
            get;
            protected set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public AutomatedDebugCategory()
            : base(c_name, c_description)
        {
        }
        #endregion // Constructor(s)
    } // AutomatedDebugCategory
}
