﻿using System;
using System.ComponentModel.Composition;
using System.Diagnostics;
using Workbench.AddIn.Services;
using RSG.Base.Logging;
using RSG.Interop.Bugstar;
using RSG.Interop.Bugstar.Organisation;
using System.Collections.Generic;
using RSG.Interop.Bugstar.Search;
using RSG.Base.Net;
using RSG.Base.Extensions;
using RSG.Base.Windows.Dialogs;
using System.Windows;

namespace Workbench.AddIn.Bugstar
{
    /// <summary>
    /// Bugstar Service implementation.
    /// </summary>
    [ExportExtension(Workbench.AddIn.CompositionPoints.BugstarService,
        typeof(Workbench.AddIn.Services.IBugstarService))]
    class BugstarService : Workbench.AddIn.Services.IBugstarService
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Connection"/> property.
        /// </summary>
        private BugstarConnection connection;

        /// <summary>
        /// User bugstar connection.
        /// </summary>
        private BugstarConnection _userConnection;

        /// <summary>
        /// The private field used for the <see cref="Project"/> property.
        /// </summary>
        private Project project;

        /// <summary>
        /// The private field used for the <see cref="User"/> property.
        /// </summary>
        private User user;

        /// <summary>
        /// The private field used for the <see cref="ProjectUsers"/> property.
        /// </summary>
        private User[] projectUsers;
        #endregion

        #region Properties
        /// <summary>
        /// Core Bugstar connection (may be read-only if not logged in).
        /// </summary>
        public BugstarConnection Connection
        {
            get
            {
                if (this.connection == null)
                {
                    this.InitialiseConnection();
                }

                return this.connection;
            }
        }

        /// <summary>
        /// Bugstar Project (for current tools branch).
        /// </summary>
        public Project Project
        {
            get
            {
                if (this.connection == null)
                {
                    this.InitialiseConnection();
                }
                
                return this.project;
            }
        }

        /// <summary>
        /// Bugstar Current User (if logged in; null otherwise).
        /// </summary>
        public User User
        {
            get
            {
                if (this.connection == null)
                {
                    this.InitialiseConnection();
                }
                
                return this.user;
            }
        }

        /// <summary>
        /// List of users that the current project has.
        /// </summary>
        public User[] ProjectUsers
        {
            get
            {
                if (this.connection == null)
                {
                    this.InitialiseConnection();
                }
                
                return this.projectUsers;
            }
        }

        /// <summary>
        /// Login service MEF import.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LoginService,
            typeof(Workbench.AddIn.Services.ILoginService))]
        private ILoginService LoginService { get; set; }

        /// <summary>
        /// Configuration service MEF import.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService,
            typeof(Workbench.AddIn.Services.IConfigurationService))]
        private IConfigurationService ConfigurationService { get; set; }
        #endregion
        
        #region Methods
        /// <summary>
        /// Queries bugstar for a list of searches that the current user has.
        /// </summary>
        /// <returns>List of searches for the current user or an empty list of the user hasn't logged into bugstar.</returns>
        public IList<Search> GetUserSearches()
        {
            IList<Search> searches = new List<Search>();
            if (this.User != null)
            {
                searches.AddRange(User.PerformSearch(SearchType.PublicSearch));
            }

            return searches;
        }

        /// <summary>
        /// Queries bugstar for a list of reports that the current user has.
        /// </summary>
        /// <returns>List of reports for the current user or an empty list of the user hasn't logged into bugstar.</returns>
        public IList<RSG.Interop.Bugstar.Search.Report> GetUserReports()
        {
            IList<RSG.Interop.Bugstar.Search.Report> reports = new List<RSG.Interop.Bugstar.Search.Report>();
            if (this.User != null)
            {
                reports.AddRange(User.Reports);
            }

            return reports;
        }

        /// <summary>
        /// Queries bugstar for a list of graphs that the current user has.
        /// </summary>
        /// <returns>List of graphs for the current user or an empty list of the user hasn't logged into bugstar.</returns>
        public IList<Graph> GetUserGraphs()
        {
            IList<Graph> graphs = new List<Graph>();
            if (this.User != null)
            {
                graphs.AddRange(User.Graphs);
            }

            return graphs;
        }

        /// <summary>
        /// 
        /// </summary>
        private void InitialiseConnection()
        {
            try
            {
                bool loginSuccesful = false;
                Application.Current.Dispatcher.Invoke((Action)delegate()
                    {
                        if (_userConnection == null || !_userConnection.IsValidConnection())
                        {
                            loginSuccesful = LoginService.Login(this.LoginCheck);
                        }
                    });

                if (loginSuccesful)
                {
                    this.connection = _userConnection;
                }
                else
                {
                    Uri restServiceUri = ConfigurationService.BugstarConfig.RESTService;
                    Uri attachmentServiceUri = ConfigurationService.BugstarConfig.AttachmentService;
                    this.connection = new BugstarConnection(restServiceUri, attachmentServiceUri);
                    this.connection.Login(ConfigurationService.BugstarConfig.ReadOnlyUsername, ConfigurationService.BugstarConfig.ReadOnlyPassword, "");
                }

                uint projectId = this.ConfigurationService.BugstarConfig.ProjectId;
                this.project = Project.GetProjectById(this.Connection, projectId);
                this.user = this.Project.CurrentUser;
                this.projectUsers = this.Project.Users;
            }
            catch (System.Exception ex)
            {
                Log.Log__Exception(ex, "Exception while connecting to Bugstar.");
                this.projectUsers = new User[] { };
            }
        }

        /// <summary>
        /// Method used to check whether a user succesfully logged in.
        /// </summary>
        /// <param name="details"></param>
        /// <returns></returns>
        private LoginResults LoginCheck(LoginDetails details)
        {
            LoginResults results = new LoginResults();
            try
            {
                Uri restServiceUri = ConfigurationService.BugstarConfig.RESTService;
                Uri attachmentServiceUri = ConfigurationService.BugstarConfig.AttachmentService;
                _userConnection = new BugstarConnection(restServiceUri, attachmentServiceUri);
                _userConnection.Login(details.Username, details.Password, ConfigurationService.BugstarConfig.UserDomain);
                results.Success = true;
            }
            catch (InvalidLoginException)
            {
                results.Success = false;
                results.Message = "Login failed. Please check your login details.";
                Log.Log__Warning("Bugstar login details were incorrect.");
            }
            catch (NotFoundException)
            {
                results.Success = false;
                results.Message = "Login failed. Server is currently unavailable.";
                Log.Log__Warning("Bugstar server not found.");
            }
            catch (InternalServerException)
            {
                results.Success = false;
                results.Message = "Login failed. Internal server error.";
                Log.Log__Warning("Bugstar login internal server error.");
            }
            catch (Exception ex)
            {
                results.Success = false;
                results.Message = "Unexpected error occurred while attempting to login.  Is the bugstar rest service running?";
                Log.Log__Warning("Generic bugstar login exception: {0}.", ex.Message);
            }
            return results;
        }
        #endregion
    } // BugstarService
} // Workbench.AddIn.Bugstar namespace
