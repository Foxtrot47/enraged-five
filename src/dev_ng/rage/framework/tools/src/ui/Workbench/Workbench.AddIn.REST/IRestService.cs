﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace Workbench.AddIn.REST
{
    /// <summary>
    /// 
    /// </summary>
    public interface IRestService
    {
        /// <summary>
        /// Location at which to attach this REST service to the web host
        /// i.e. http://localhost:8080/<EndPOintName>
        /// </summary>
        string EndPointName
        {
            get;
        }
    } // IRestService
} // Workbench.AddIn.REST
