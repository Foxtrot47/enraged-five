﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using RSG.Base.Collections;
using ragCore;
using ragWidgets;
using System.Diagnostics;
using RSG.Base.Editor.Command;

namespace WidgetEditor.ViewModel
{
    public enum WidgetType
    {
        Bank = 0,
        Group,
        Widget
    }

    public class WidgetModel<WidgetType> :IModel
    {
        public Widget ActualWidget { get; set; }

        public event ModifiedStateChangedEventHandler ModifiedStateChanged;

        public bool IsModified
        {
            get
            {
                return ActualWidget.IsModified();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool IsBatching
        {
            get
            {
                return false;
            }
            set
            {
            }
        }

        public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        public void SendPropertyChangedEvent(EditorPropertyChangedEventArgs e)
        {
        }

        public void SendCompositePropertyChangedEvent(EditorCompositeChangedEventArgs e)
        {
        }

        public void SendCollectionChangedEvent(EditorCollectionChangedEventArgs e)
        {
        }

        public void SendModifiedStateChangedEvent(ModifiedStateChangedEventArgs e)
        {
        }

    }



    public class WidgetViewModel : HierarchicalViewModelBase
    {
        #region Properties
        public Widget Model { get; set; }
        /*
        public WidgetType Type
        {
            get { return m_Type; }
            set
            {
                SetPropertyValue( value, () => this.Type,
                                new PropertySetDelegate( delegate( Object newValue ) { m_Type = (WidgetType)newValue; } ) );
            }
        }
        */
        private string m_Title;
        public string Title
        {
            get { return m_Title; }
            set
            {
                SetPropertyValue( value, () => this.Title,
                                new PropertySetDelegate( delegate( Object newValue ) { m_Title = (string)newValue; } ) );
            }
        }
        #endregion

        #region Variables

        
        #endregion



        public WidgetViewModel( Widget model )
        {            
            ////Model = new WidgetModel();
            Model = model;

            Model.WidgetAddedInstance += new WidgetEventHandler( model_WidgetAdded );
            Model.ValueChanged += new EventHandler( model_ValueChanged );

            Title = Model.Title;
        }

        void model_ValueChanged( object sender, EventArgs e )
        {
            bool isModified = Model.IsModified();

            if ( IsModified != isModified )
            {
                IsModified = isModified;
                if (Parent != null)
                {
                    WidgetViewModel parent = (Parent as WidgetViewModel);
                    if ( isModified && !parent.IsModified )
                    {
                        parent.IsModified = true;
                    }
                    else if ( !isModified && parent.IsModified )
                    {
                        // We went from modified to unmodified, and our parent
                        // thinks it's modified, so it needs to recheck all
                        // its children.
                        parent.UpdateIsModified();                                
                    }
                }
            }
        }

        void model_WidgetAdded( object sender, WidgetEventArgs e )
        {
            Dispatcher.Invoke( (Action)delegate()
            {
                WidgetViewModel child = new WidgetViewModel( e.Widget );
                child.Parent = this;
                Children.Add( child );
            } );
        }

        public void UpdateIsModified()
        {
            foreach ( WidgetViewModel child in Children )
            {
                if ( child.IsModified )
                {
                    IsModified = true;
                    (Parent as WidgetViewModel).UpdateIsModified();
                    return;
                }
            }

            IsModified = Model.IsModified();
            if (Parent != null)
            {
                (Parent as WidgetViewModel).UpdateIsModified();
            }
        }
    }
}
