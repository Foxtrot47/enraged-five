﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;

namespace MetadataEditor.AddIn.KinoEditor
{
    public class KinoEditorViewModel : ViewModelBase
    {
        #region Constants
        public const String DOCUMENT_NAME = "KinoMetaEditor";
        public const String DOCUMENT_TITLE = "Metadata";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public KinoEditorViewModel()
        {
        }
        #endregion // Constructor(s)
    }
}
