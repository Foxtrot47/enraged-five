﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using MetadataEditor.AddIn.MetaEditor.BasicViewModel;
using MetadataEditor.AddIn.MetaEditor.ViewModel;
using RSG.Metadata.Parser;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using RSG.Metadata.Data;
using System.Diagnostics;
using System.Linq;
using System.Windows.Media;

namespace MetadataEditor.AddIn.MetaEditor.View
{
    /// <summary>
    /// Interaction logic for TreeViewEditor.xaml
    /// </summary>
    public partial class TreeViewEditor : UserControl
    {
        public TreeViewEditor()
        {
            InitializeComponent();
            this.DataContextChanged += new DependencyPropertyChangedEventHandler(TreeViewEditor_DataContextChanged);
        }

        void TreeViewEditor_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (e.OldValue is MetaFileViewModel)
            {
                (e.NewValue as MetaFileViewModel).ItemSelected -= ItemSelected;
                (e.NewValue as MetaFileViewModel).UnSelectAllRequested -= UnSelectAll;
            }
            if (e.NewValue is MetaFileViewModel)
            {
                (e.NewValue as MetaFileViewModel).ItemSelected += ItemSelected;
                (e.NewValue as MetaFileViewModel).UnSelectAllRequested += UnSelectAll;
            }

        }

        void UnSelectAll(object sender, EventArgs e)
        {
            trMembers.UnselectAll();
        }

        void ItemSelected(object sender, ItemSelectedEventArgs e)
        {
            if (MetaEditorBasicView.SelectingSearchResult)
            {
                trMembers.ScrollIntoView(e.ItemSelected);
            }
        }

        void OnMouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (!ReferenceEquals(sender, e.Source))
                return;

            if (!((sender as ListBoxItemTree).Content as ITunableViewModel).IsExpanded)
                ((sender as ListBoxItemTree).Content as ITunableViewModel).IsExpanded = true;
            e.Handled = true;
        }

        void OnMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (!(sender is ListBoxItemTree))
                return;
            if (!(e.OriginalSource is DependencyObject))
                return;
            if ((sender as ListBoxItemTree).IsSelected == false)
                return;

            DependencyObject parent = System.Windows.Media.VisualTreeHelper.GetParent(e.OriginalSource as DependencyObject);
            if (!(parent is ToggleButton))
                return;

            bool expand = !((sender as ListBoxItemTree).Content as ITunableViewModel).IsExpanded;
            ((sender as ListBoxItemTree).Content as ITunableViewModel).IsExpanded = expand;
            e.Handled = true;
        }

        private void trMembers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            MetaFileViewModel vm = this.DataContext as MetaFileViewModel;
            Debug.Assert(vm != null, "Invalid DataContext");

            // We need to manually keep the selected items in the view model up to date with what the view has.
            // (No binding to the SelectedItems property :[ )

            foreach (ITunableViewModel itemVm in e.RemovedItems.OfType<ITunableViewModel>())
            {
                vm.SelectedItems.Remove(itemVm);
                itemVm.IsSelected = false;
            }

            foreach (ITunableViewModel itemVm in e.AddedItems.OfType<ITunableViewModel>())
            {
                vm.SelectedItems.Add(itemVm);
                itemVm.IsSelected = true;
            }
        }

        private void OnListBoxItemContainerFocused(object sender, RoutedEventArgs e)
        {
            ListBoxItem item = sender as ListBoxItem;
            if (item == null)
            {
                return;
            }

            ListBox box = this.FindAncestor<ListBox>(item);
            if (box == null)
            {
                return;
            }

            box.SelectedItem = item.DataContext;
        }

        private T FindAncestor<T>(DependencyObject from) where T : DependencyObject
        {
            if (from == null)
            {
                return null;
            }

            T candidate = from as T;
            if (candidate != null)
            {
                return candidate;
            }

            return FindAncestor<T>(VisualTreeHelper.GetParent(from));
        }
    }

    public class ListBoxTree : ListBox
    {
        enum DirectionKey
        {
            None,
            Left,
            Right,
            Up,
            Down
        }

        private DirectionKey m_lastKey;
        private List<ITunableViewModel> m_copiedItems;
        private int m_swapItemsLoop;

        private ITunableViewModel m_lastSelection;


        public ListBoxTree()
        {
            m_copiedItems = new List<ITunableViewModel>();
            m_lastKey = DirectionKey.None;
            SelectionChanged += new SelectionChangedEventHandler(ListBoxTree_SelectionChanged);
        }

        protected override void OnPreviewMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            m_swapItemsLoop = 0;
            m_lastKey = DirectionKey.None;
            m_lastSelection = null;
            
            base.OnPreviewMouseLeftButtonDown(e);
        }

        void ListBoxTree_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (m_lastKey == DirectionKey.Left || ((Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control && (m_lastKey == DirectionKey.Up || m_lastKey == DirectionKey.Down)))
            {
                if (m_lastSelection != null)
                {
                    SelectedItem = m_lastSelection;

                    if (m_swapItemsLoop <= 0)
                    {
                        m_lastKey = DirectionKey.None;
                        m_lastSelection = null;
                    }
                    else
                    {
                        m_swapItemsLoop--;
                    }
                }
                else if (e.RemovedItems.Count > 0 && e.AddedItems.Count > 0)
                {
                    var vm = e.RemovedItems[0] as ITunableViewModel;
                    if (vm.Parent != e.AddedItems[0])
                    {
                        m_swapItemsLoop = 2;
                        m_lastSelection = vm.Parent;
                        SelectedItem = m_lastSelection;
                        ScrollIntoView(vm.Parent);
                        InvalidateVisual();
                    }
                }
            }
            else
            {
                m_swapItemsLoop = 0;
                m_lastKey = DirectionKey.None;
                m_lastSelection = null;
            }
        }

        /// <summary>
        /// Navigation handler.
        /// </summary>
        /// <param name="e">Event arguments.</param>
        protected override void OnKeyUp(KeyEventArgs e)
        {
            if (e.OriginalSource.GetType() == typeof(TextBox))
            {
                e.Handled = true;
                return;
            }

            bool handled = false;
            m_lastKey = DirectionKey.None;

            if ((this.DataContext as MetaFileViewModel).SelectedItems == null)
            {
                return;
            }

            if (SelectedItems.Count != 1)
            {
                return;
            }

            var item = SelectedItem as ITunableViewModel;

            if (item != null)
            {
                if (Keyboard.Modifiers == ModifierKeys.None)
                {
                    if (e.Key == Key.Right && !item.IsExpanded)
                    {
                        m_lastKey = DirectionKey.Right;
                        item.IsExpanded = true;
                        handled = true;
                    }
                    else if (e.Key == Key.Right && item.IsExpanded)
                    {
                        if (item is HierarchicalTunableViewModel && item.IsExpanded)
                        {
                            var htvm = (item as HierarchicalTunableViewModel);
                            if (htvm.Members.Count >= 0)
                            {
                                m_lastKey = DirectionKey.Right;
                                SelectedItems.Clear();
                                SelectedItems.Add(htvm.Members[0]);
                            }
                        }
                    }
                    else if (e.Key == Key.Left && item.IsExpanded)
                    {
                        m_lastKey = DirectionKey.Left;
                        item.IsExpanded = false;
                        handled = true;
                    }
                    else if (e.Key == Key.Left && !item.IsExpanded && item.Parent != null)
                    {
                        m_lastKey = DirectionKey.Left;
                        SelectedItems.Clear();
                        SelectedItems.Add(item.Parent);
                        handled = true;
                    }
                    else if (e.Key == Key.Down)
                    {
                        var viewModel = GetNextViewModel(item);
                        if (viewModel != null)
                        {
                            m_lastKey = DirectionKey.Down;
                            SelectedItem = viewModel;
                            viewModel.IsSelected = true;
                        }
                        handled = true;
                    }
                    else if (e.Key == Key.Up)
                    {
                        var viewModel = GetPreviousViewModel(item);
                        if (viewModel != null)
                        {
                            m_lastKey = DirectionKey.Up;
                            SelectedItem = viewModel;
                            viewModel.IsSelected = true;
                        }

                        handled = true;
                    }
                }
                else if ((Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
                {
                    if (e.Key == Key.Up)
                    {
                        m_lastKey = DirectionKey.Up;
                        MoveItemUp(item);
                    }
                    else if (e.Key == Key.Down)
                    {
                        m_lastKey = DirectionKey.Down;
                        MoveItemDown(item);
                    }
                    else if (e.Key == Key.C)
                    {
                        m_copiedItems.Clear();

                        if (SelectedItems.Count == 0)
                        {
                            return;
                        }

                        foreach (var selected in SelectedItems)
                        {
                            if (selected is ArrayTunableViewModel || selected is StructureTunableViewModel)
                            {
                                m_copiedItems.Add(selected as TunableViewModel);
                            }
                        }
                    }
                    else if (e.Key == Key.X)
                    {
                        m_copiedItems.Clear();

                        if (SelectedItems.Count == 0)
                        {
                            return;
                        }

                        Dictionary<HierarchicalTunableViewModel, List<TunableViewModel>> killList = new Dictionary<HierarchicalTunableViewModel, List<TunableViewModel>>();

                        foreach (var selected in SelectedItems)
                        {
                            if (selected is ArrayTunableViewModel || selected is StructureTunableViewModel)
                            {
                                var parent = (selected as TunableViewModel).Parent as HierarchicalTunableViewModel;

                                if (parent != null)
                                {
                                    m_copiedItems.Add(selected as TunableViewModel);

                                    if (!killList.ContainsKey(parent))
                                    {
                                        killList.Add(parent, new List<TunableViewModel>());
                                    }

                                    killList[parent].Add(selected as TunableViewModel);
                                }
                            }
                        }

                        foreach (var kvp in killList)
                        {
                            foreach (var childModel in kvp.Value)
                            {
                                kvp.Key.Members.Remove(childModel);
                            }
                        }
                    }
                    else if (e.Key == Key.V && m_copiedItems.Count > 0)
                    {
                        var copy = (m_copiedItems[0] as TunableViewModel);
                        if (copy != null)
                        {
                            copy.RequestDuplicateCommand(m_copiedItems);
                        }
                    }
                }
            }

            e.Handled = true;
        }

        /// <summary>
        /// Move the item down in the members list.
        /// </summary>
        /// <param name="item">Item.</param>
        private void MoveItemDown(ITunableViewModel item)
        {
            if (item.Parent is ArrayTunableViewModel)
            {
                int oldIndex = (item.Parent as ArrayTunableViewModel).Members.IndexOf(item);
                if (oldIndex == (item.Parent as ArrayTunableViewModel).Members.Count - 1)
                {
                    return;
                }
                int newIndex = oldIndex++;
                ((item.Parent as ArrayTunableViewModel).Model as ArrayTunable).Move(oldIndex, newIndex);
            }
            else if (item.Parent is MapTunableViewModel)
            {
                int oldIndex = (item.Parent as MapTunableViewModel).Members.IndexOf(item);
                if (oldIndex == (item.Parent as ArrayTunableViewModel).Members.Count - 1)
                {
                    return;
                }
                int newIndex = oldIndex++;
                ((item.Parent as MapTunableViewModel).Model as MapTunable).Move(oldIndex, newIndex);
            }

            ScrollIntoView(item);
            SelectedItem = item;
        }

        /// <summary>
        /// Move the item up in the members list.
        /// </summary>
        /// <param name="item">Item.</param>
        private void MoveItemUp(ITunableViewModel item)
        {
            if (item.Parent is ArrayTunableViewModel)
            {
                int oldIndex = (item.Parent as ArrayTunableViewModel).Members.IndexOf(item);
                if (oldIndex == 0)
                {
                    return;
                }
                int newIndex = oldIndex--;
                ((item.Parent as ArrayTunableViewModel).Model as ArrayTunable).Move(oldIndex, newIndex);
            }
            else if (item.Parent is MapTunableViewModel)
            {
                int oldIndex = (item.Parent as MapTunableViewModel).Members.IndexOf(item);
                if (oldIndex == 0)
                {
                    return;
                }
                int newIndex = oldIndex--;
                ((item.Parent as MapTunableViewModel).Model as MapTunable).Move(oldIndex, newIndex);
            }

            ScrollIntoView(item);
            SelectedItem = item;
        }

        /// <summary>
        /// Get the next view model in the hierarchy.
        /// </summary>
        /// <param name="viewModel">Current view model.</param>
        /// <returns>The next view model.</returns>
        private ITunableViewModel GetNextViewModel(ITunableViewModel viewModel)
        {
            ITunableViewModel foundModel = null;

            if (viewModel is HierarchicalTunableViewModel && viewModel.IsExpanded)
            {
                var thisModel = (viewModel as HierarchicalTunableViewModel);
                if (thisModel.Members.Count > 0)
                {
                    foundModel = thisModel.Members[0];
                }
            }

            if (foundModel == null && viewModel.Parent is HierarchicalTunableViewModel)
            {
                var str = viewModel.Parent as HierarchicalTunableViewModel;
                bool found = false;
                foreach (var child in str.Members)
                {
                    if (found)
                    {
                        foundModel = child;
                        break;
                    }

                    if (child == viewModel)
                    {
                        found = true;
                    }
                }
            }

            return foundModel;
        }

        /// <summary>
        /// Get the previous model in the hierarchy.
        /// </summary>
        /// <param name="viewModel">Current model.</param>
        /// <returns>Previous model.</returns>
        private ITunableViewModel GetPreviousViewModel(ITunableViewModel viewModel)
        {
            ITunableViewModel foundModel = null;

            var currentModel = viewModel.Parent == null ? viewModel : viewModel.Parent;

            if (currentModel is HierarchicalTunableViewModel)
            {
                var str = currentModel as HierarchicalTunableViewModel;

                if (viewModel == str.Members[0])
                {
                    // Early out if we're jumping back to the parent.
                    return viewModel.Parent;
                }

                bool found = false;
                for (int i = str.Members.Count - 1; i >= 0; i--)
                {
                    var child = str.Members[i];
                    if (found)
                    {
                        foundModel = child;
                        break;
                    }

                    if (child == viewModel)
                    {
                        found = true;
                    }
                }
            }

            return foundModel;
        }
    }

    public class ListBoxItemTree : ListBoxItem
    {
        public ListBoxItemTree(){}

        #region Events
        /// <summary>
        /// Gets fired when the drag area for the item gets a PreviewMouseLeftButtonDown event
        /// </summary>
        public static readonly RoutedEvent ExpandedEvent = EventManager.RegisterRoutedEvent(
            "Expanded", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(ListBoxItemTree));

        public event RoutedEventHandler Expanded
        {
            add { AddHandler(ExpandedEvent, value); }
            remove { RemoveHandler(ExpandedEvent, value); }
        }
        #endregion

        #region Properties
        public static DependencyProperty IsExpandedProperty = DependencyProperty.Register("IsExpanded", typeof(bool),
            typeof(ListBoxItemTree), new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, OnExpandedChanged));

        private static void OnExpandedChanged(DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue == true && (bool)e.OldValue == false)
            {
                (s as ListBoxItemTree).FireExpandedEvent();
            }
        }

        public bool IsExpanded
        {
            get { return (bool)GetValue(IsExpandedProperty); }
            set { SetValue(IsExpandedProperty, value); }
        }
        #endregion

        protected override void OnKeyUp(System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Right && !IsExpanded)
            {
                FireExpandedEvent();
            }

            base.OnKeyUp(e);
        }

        #region Handlers
        private void FireExpandedEvent()
        {
            RoutedEventArgs newEventArgs = new RoutedEventArgs(ExpandedEvent);
            RaiseEvent(newEventArgs);
        }
        #endregion
    }

    public class ItemTemplateSelector : DataTemplateSelector
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <param name="container"></param>
        /// <returns></returns>
        public override DataTemplate SelectTemplate(Object item, DependencyObject container)
        {
            DataTemplate dt = null;
            ContentPresenter cp = container as ContentPresenter;
            if (item != null && !(item is HierarchicalTunableViewModel))
            {
                if (item is TunableViewModel)
                {
                    IMember member = (item as TunableViewModel).Model.Definition;
                    if (member != null)
                    {
                        if (member is StringMember)
                        {
                            if ((member as StringMember).SourceType == StringMember.StringSourceType.EnumDefinition)
                            {
                                dt = cp.FindResource("StringTunableTypeWithEnumValues") as DataTemplate;
                            }
                            else if ((member as StringMember).SourceType == StringMember.StringSourceType.Directory)
                            {
                                dt = cp.FindResource("StringTunableTypeWithFilenamesValues") as DataTemplate;
                            }
                            else if ((member as StringMember).SourceType == StringMember.StringSourceType.ParentMap)
                            {
                                dt = cp.FindResource("StringTunableTypeWithFilenamesValues") as DataTemplate;
                            }
                            else
                            {
                                dt = cp.FindResource("StringTunableType") as DataTemplate;
                            }
                        }
                        else if (member is BoolMember)
                        {
                            dt = cp.FindResource("BooleanTunableType") as DataTemplate;
                        }
                        else if (member is U8Member || member is U16Member ||
                                    member is S8Member || member is CharMember ||
                                    member is S16Member || member is ShortMember ||
                                    member is S32Member || member is IntMember)
                        {
                            dt = cp.FindResource("IntegerTunableType") as DataTemplate;
                        }
                        else if (member is U32Member)
                        {
                            if ((member as U32Member).IntegerType == U32Member.UInt32Type.Normal)
                            {
                                dt = cp.FindResource("UnsignedIntegerTunableType") as DataTemplate;
                            }
                            else
                            {
                                dt = cp.FindResource("IntColourTunableType") as DataTemplate;
                            }
                        }
                        else if (member is FloatMember)
                        {
                            dt = cp.FindResource("FloatTunableType") as DataTemplate;
                        }
                        else if (member is Float16Member)
                        {
                            dt = cp.FindResource("FloatTunableType") as DataTemplate;
                        }
                        else if (member is Color32Member)
                        {
                            dt = cp.FindResource("PureColourTunableType") as DataTemplate;
                        }
                        else if (member is EnumMember)
                        {
                            dt = cp.FindResource("EnumTunableType") as DataTemplate;
                        }
                        else if (member is Vector2Member || member is Vec2VMember)
                        {
                            dt = cp.FindResource("Vector2TunableType") as DataTemplate;
                        }
                        else if (member is Vector3Member || member is Vec3VMember)
                        {
                            if ((member as Vector3MemberBase).VectorType == Vector3MemberBase.Vector3Type.Normal)
                            {
                                dt = cp.FindResource("Vector3TunableType") as DataTemplate;
                            }
                            else
                            {
                                dt = cp.FindResource("VectorColourTunableType") as DataTemplate;
                            }
                        }
                        else if (member is Vector4Member || member is Vec4VMember)
                        {
                            dt = cp.FindResource("Vector4TunableType") as DataTemplate;
                        }
                        else if (member is BitsetMember)
                        {
                            if ((member as BitsetMember).Values != null)
                            {
                                dt = cp.FindResource("BitsetTunableType") as DataTemplate;
                            }
                            else
                            {
                                dt = cp.FindResource("StringTunableType") as DataTemplate;
                            }
                        }
                        else if (member is VecBoolVMember)
                        {
                            dt = cp.FindResource("VecBoolVType") as DataTemplate;
                        }
                        else if (member is Matrix33MemberBase)
                        {
                            dt = cp.FindResource("Matrix33TunableType") as DataTemplate;
                        }
                        else if (member is Matrix34MemberBase)
                        {
                            dt = cp.FindResource("Matrix34TunableType") as DataTemplate;
                        }
                        else if (member is Matrix44MemberBase)
                        {
                            dt = cp.FindResource("Matrix44TunableType") as DataTemplate;
                        }
                        else if (member is PointerMember)
                        {
                            dt = cp.FindResource("StringTunableType") as DataTemplate;
                        }
                    }
                }

            }
            else if (item != null && item is StructureTunableViewModel)
            {
                if ((item as StructureTunableViewModel).Parent is PointerTunableViewModel)
                {
                    PointerTunableViewModel pointer = (item as StructureTunableViewModel).Parent as PointerTunableViewModel;
                    RSG.Metadata.Data.PointerTunable tunable = pointer.Model as RSG.Metadata.Data.PointerTunable;
                    if (tunable.Value is RSG.Metadata.Data.StructureTunable)
                    {
                        RSG.Metadata.Parser.Structure str = ((tunable.Value as RSG.Metadata.Data.StructureTunable).Definition as StructMember).Definition;
                        if (str == RSG.Metadata.Model.StructureDictionary.NullStructure)
                        {
                            dt = cp.FindResource("NullPointerTunableValue") as DataTemplate;
                            return dt;
                        }
                    }
                }

                if (((item as StructureTunableViewModel).Model as RSG.Metadata.Data.StructureTunable).TunableKey is RSG.Metadata.Data.EnumTunable)
                {
                    dt = cp.FindResource("StructureTunableEnumValue") as DataTemplate;
                }
                else
                {
                    dt = cp.FindResource("StructureTunableValue") as DataTemplate;
                }
            }
            else if (item != null && item is MapItemTunableViewModel)
            {
                if ((item as MapItemTunableViewModel).Parent is MapTunableViewModel)
                {
                    MapTunableViewModel map = (item as MapItemTunableViewModel).Parent as MapTunableViewModel;
                    RSG.Metadata.Data.MapTunable tunable = map.Model as RSG.Metadata.Data.MapTunable;
                    if (tunable.IsIntegerKey)
                    {
                        dt = cp.FindResource("MapItemTunableWithInteger") as DataTemplate;
                        return dt;
                    }
                }

                dt = cp.FindResource("MapItemTunableWithString") as DataTemplate;
            }
            else if (item != null && item is XiTunableViewModel)
            {
                dt = cp.FindResource("XiIncludeTunableType") as DataTemplate;
            }

            if (dt == null)
                dt = cp.FindResource("EmptyTunableType") as DataTemplate;
            return dt;
        }
    } // ItemTemplateSelector
}
