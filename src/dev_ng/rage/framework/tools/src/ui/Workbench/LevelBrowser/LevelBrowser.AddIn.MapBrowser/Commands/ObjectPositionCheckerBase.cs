﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using RSG.Base.Logging;
using RSG.Base.Math;
using RSG.Interop.Autodesk3dsmax;
using RSG.Model.Common.Map;
using Workbench.AddIn;
using Workbench.AddIn.Commands;
using Workbench.AddIn.Services;
using Workbench.AddIn.Services.Model;
using WidgetEditor.AddIn;

namespace LevelBrowser.AddIn.MapBrowser.Commands
{

    /// <summary>
    /// Base abstract Object Position Checker command.
    /// </summary>
    abstract class ObjectPositionCheckerBase : WorkbenchMenuItemBase
    {
        #region Private helper classes / enums

        /// <summary>
        /// Enumeration value used to determine the state of the task when it completes.
        /// </summary>
        enum PositionCheckerResult
        {
            /// <summary>
            /// The task completed successfully.
            /// </summary>
            Success,
            /// <summary>
            /// The connection timed out. The checker is still running on the console,
            /// but there is no communication between WB and the game.
            /// </summary>
            Timeout,

            /// <summary>
            /// There is no widget.
            /// </summary>
            NoWidget
        }

        /// <summary>
        /// Process arguments. Used to pass the output folder and map sections to the task.
        /// </summary>
        class ProcessArguments
        {
            #region Public properties

            /// <summary>
            /// Output folder.
            /// </summary>
            public string OutputFolder { get; private set; }

            /// <summary>
            /// Map sections.
            /// </summary>
            public IMapSection[] Sections { get; private set; }

            #endregion

            #region Constructor

            /// <summary>
            /// Constructor.
            /// </summary>
            /// <param name="outputFolder">Output folder.</param>
            /// <param name="sections">Map sections.</param>
            public ProcessArguments(string outputFolder, IMapSection[] sections)
            {
                OutputFolder = outputFolder;
                Sections = sections;
            }

            #endregion
        }

        #endregion

        #region Private member fields

        RSG.Base.Tasks.ITask loadAssetsTask;

        private string batFilename;

        #endregion

        #region Delegates

        delegate void FunctionHandler(RSG.Base.Tasks.ITaskContext context, RSG.Base.Tasks.IProgress<RSG.Base.Tasks.TaskProgress> progress);

        #endregion

        #region Constants

        private const int WAIT_PAUSE = 2000; // Two second pause.
        private const int WAIT_CONNECTIONTIMEOUT = 60; // 1 minute time out. Allows the game to boot up and the proxy to connect with the game.

        public readonly String RAG_SECTORTOOLS = "Sector Tools/Physical Object Position Checker";
        public readonly String RAG_ENDXYZWORLD = "Sector Tools/End X Y Z World Coordinates:";
        public readonly String RAG_STARTXYZWORLD = "Sector Tools/Start X Y Z World Coordinates:";
        public readonly String RAG_SCANANDEXPORTAREA = "Sector Tools/Physical Object Position Checker/Scan and Export Area";
        public readonly String RAG_SCANINPROGRESS = "Sector Tools/Physical Object Position Checker/Scan In Progress (read-only)";
        
        #endregion

        #region Properties
        /// <summary>
        /// MEF import for core workbench services.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.CoreServicesProvider,
            typeof(ICoreServiceProvider))]
        protected Lazy<ICoreServiceProvider> CoreServiceProvider { get; set; }

        /// <summary>
        /// Content Browser.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LevelBrowser, typeof(ILevelBrowser))]
        protected ILevelBrowser LevelBrowserProxy { get; set; }
        #endregion // MEF Imports

        #region Constructor
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ObjectPositionCheckerBase(Guid id, string header, string batFilename)
        {
            this.batFilename = batFilename;
            this.Header = header;
            this.IsDefault = false;
            this.ID = id;
            this.RelativeID = new Guid(ExportMapCommand.GUID);
            this.Direction = Direction.After;
        }
        #endregion // Constructor

        #region ICommand Implementation
        /// <summary>
        /// Gets called when deciding whether this menu items command can be 
        /// executed or not.
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return (Autodesk3dsmax.IsInstalled());
        }

        /// <summary>
        /// Gets called when executing the command for this menu item.
        /// </summary>
        public override void Execute(Object parameter)
        {
            if (parameter is IList)
            {
                IMapSection[] maps = new IMapSection[(parameter as IList).Count];
                (parameter as IList).CopyTo(maps, 0);
                this.Run(maps);
            }
            else
            {
                IMapSection[] maps = new IMapSection[] { parameter as IMapSection };
                this.Run(maps);
            }
        }
        #endregion // ICommand Implementation
        
        #region Private Methods
        /// <summary>
        /// Invoke the game to run the object position checker.
        /// </summary>
        /// <param name="sections"></param>
        private void Run(IMapSection[] sections)
        {
            if (loadAssetsTask != null)
            {
                IUserInterfaceService uiService = this.CoreServiceProvider.Value.UserInterfaceService;
                uiService.Show("There is currently a position check in progress.");
                return;
            }

            if (this.CoreServiceProvider.Value.ProxyService.IsGameConnected)
            {
                StartTask(this.PositionCheckerAction, sections);
            }
            else
            {
                string outputFolder = String.Empty;
                System.Windows.Forms.FolderBrowserDialog dlg = new System.Windows.Forms.FolderBrowserDialog();
                dlg.Description = "Select output directory for Object Position Checker files.";
                if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    if (!String.IsNullOrEmpty(dlg.SelectedPath) && System.IO.Directory.Exists(dlg.SelectedPath))
                    {
                        outputFolder = dlg.SelectedPath;
                        StartTask(this.StartProcess, new ProcessArguments(outputFolder, sections));
                    }
                }
            }
        }

        /// <summary>
        /// Start the position checker task.
        /// </summary>
        /// <param name="processFunction">Delegate for task method.</param>
        /// <param name="sections">Sections.</param>
        private void StartTask(FunctionHandler processFunction, object sections)
        {
            loadAssetsTask = new RSG.Base.Tasks.ActionTask("Object Position Checker", (context, progress) => processFunction(context, progress));
            loadAssetsTask.OnTaskCompleted += new RSG.Base.Tasks.TaskCompletedEventHandler(PositionChecker_OnTaskCompleted);
            this.CoreServiceProvider.Value.TaskProgressService.Add(loadAssetsTask, new RSG.Base.Tasks.TaskContext(new System.Threading.CancellationToken(), sections), TaskPriority.Background);
        }

        /// <summary>
        /// Task completion event handler.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        void PositionChecker_OnTaskCompleted(object sender, RSG.Base.Tasks.TaskCompletedEventArgs e)
        {
            PositionCheckerResult success = e.ReturnValue == null ? PositionCheckerResult.Success : (PositionCheckerResult)e.ReturnValue;
            IUserInterfaceService uiService = this.CoreServiceProvider.Value.UserInterfaceService;

            switch (success)
            {
                case PositionCheckerResult.Success:
                    uiService.Show("Finished! The Object Position Checker process has finished.");
                    break;
                case PositionCheckerResult.NoWidget:
                    uiService.Show("The Object Position Checker '" + RAG_SECTORTOOLS + "' widget group could not be found in RAG.");
                    break;
                case PositionCheckerResult.Timeout:
                    uiService.Show("The Object Position Checker could not connect to the game in a timely manner.");
                    break;

            }

            loadAssetsTask.OnTaskCompleted -= PositionChecker_OnTaskCompleted;
            loadAssetsTask = null;
        }

        /// <summary>
        /// Talks through RAG to the game to set the start and end scan co-ordinates and then starts the scan.
        /// </summary>
        /// <param name="context">Context.</param>
        /// <param name="progress">Progress.</param>
        private void PositionCheckerAction(RSG.Base.Tasks.ITaskContext context, RSG.Base.Tasks.IProgress<RSG.Base.Tasks.TaskProgress> progress)
        {
            var sections = context.Argument as IMapSection[];
            double increment = sections.Length == 0 ? 0 : 1 / sections.Length;
            double count = 0;

            foreach (var section in sections)
            {
                progress.Report(new RSG.Base.Tasks.TaskProgress(count, "Parsing " + section.Name));

                float minx, miny, maxx, maxy;
                minx = miny = maxx = maxy = 0f;

                GetExtents(section, ref minx, ref miny, ref maxx, ref maxy);

                this.CoreServiceProvider.Value.ProxyService.Console.WriteStringWidget(RAG_STARTXYZWORLD, String.Format("{0} {1} {2}", minx, miny, 0f));
                this.CoreServiceProvider.Value.ProxyService.Console.WriteStringWidget(RAG_ENDXYZWORLD, String.Format("{0} {1} {2}", maxx, maxy, 0f));
                this.CoreServiceProvider.Value.ProxyService.Console.PressWidgetButton(RAG_SCANANDEXPORTAREA);

                context.ReturnValue = PauseTillDone();
            }
        }

        /// <summary>
        /// Start the external process that launches the game and with it the position checker.
        /// </summary>
        /// <param name="sections">Map sections.</param>
        private void StartProcess(RSG.Base.Tasks.ITaskContext context, RSG.Base.Tasks.IProgress<RSG.Base.Tasks.TaskProgress> progress)
        {
            var processArguments = context.Argument as ProcessArguments;
            var sections = processArguments.Sections;
            string outputDir = processArguments.OutputFolder;

            String buildDir = this.CoreServiceProvider.Value.ConfigurationService.GameConfig.BuildDir;
            String sectorFilename = Path.Combine(buildDir, "object_position_sectors.txt");
            CreateSectorFile(sectorFilename, sections);

            String args = String.Format("{0} -sectortools_input=\"{1}\"",
                Properties.Settings.Default.ObjectPositionCheckerArgs,
                sectorFilename);
            if (!String.IsNullOrEmpty(outputDir))
                args += String.Format(" -sectortools_output=\"{0}/\"", outputDir);

            Process process = new Process();
            process.StartInfo.FileName = Path.Combine(buildDir, this.batFilename);
            process.StartInfo.Arguments = args;
            process.StartInfo.WorkingDirectory = buildDir;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardError = false;
            process.StartInfo.RedirectStandardOutput = false;
            process.StartInfo.RedirectStandardInput = false;

            Log.Log__Message("Running the following command: {0} {1}", process.StartInfo.FileName, process.StartInfo.Arguments);

            if (!process.Start())
            {
                String message = String.Format("Error invoking game: {0}{1}{1}Arguments: {2}.",
                    process.StartInfo.FileName, Environment.NewLine, process.StartInfo.Arguments);
                Log.Log__Error(message);
                this.CoreServiceProvider.Value.UserInterfaceService.Show(message, MessageBoxButton.OK, MessageBoxImage.Error);
            }

            context.ReturnValue = PauseTillDone();
        }

        /// <summary>
        /// Keeps the thread active while we're waiting for the operation to complete.
        /// </summary>
        private PositionCheckerResult PauseTillDone()
        {
            bool ready = false;

            DateTime startTime = DateTime.Now;

            do // wait until the game has connected
            {
                if (!this.CoreServiceProvider.Value.ProxyService.IsProxyConnected)
                {
                    this.CoreServiceProvider.Value.ProxyService.RefreshGameConnections();
                }

                System.Threading.Thread.Sleep(WAIT_PAUSE);
                ready = this.CoreServiceProvider.Value.ProxyService.IsProxyConnected && this.CoreServiceProvider.Value.ProxyService.IsGameConnected;

                TimeSpan diff = DateTime.Now - startTime;
                if (!ready && diff.TotalSeconds > WAIT_CONNECTIONTIMEOUT)
                {
                    return PositionCheckerResult.Timeout;
                }

            } while (!ready);

            if (!this.CoreServiceProvider.Value.ProxyService.Console.WidgetExists(RAG_SECTORTOOLS))
            {
                return PositionCheckerResult.NoWidget;
            }

            // The first while statement is used to wait until the flag gets set to TRUE

            ready = IsScanRunning();
            while (!ready)
            {
                System.Threading.Thread.Sleep(WAIT_PAUSE);
                ready = IsScanRunning();
            }

            // The second while statement is used to wait until the flag gets set to FALSE

            bool running = IsScanRunning();
            while (running)
            {
                System.Threading.Thread.Sleep(WAIT_PAUSE);
                running = IsScanRunning();
            }

            return PositionCheckerResult.Success;
        }

        /// <summary>
        /// Checks the status of the scan complete flag game side.
        /// </summary>
        /// <returns>Returns true if the scan complete flag is set.</returns>
        private bool IsScanRunning()
        {

            bool inProgress = false;
            try
            {
                inProgress = this.CoreServiceProvider.Value.ProxyService.Console.ReadBoolWidget(RAG_SCANINPROGRESS);
            }
            catch
            {

            }

            return inProgress;
        }

        /// <summary>
        /// Get the extents of the given section.
        /// </summary>
        /// <param name="section">Section.</param>
        /// <param name="minX">Minimum X.</param>
        /// <param name="minY">Minimum Y.</param>
        /// <param name="maxX">Maximum X.</param>
        /// <param name="maxY">Maximum Y.</param>
        private void GetExtents(IMapSection section, ref float minX, ref float minY, ref float maxX, ref float maxY)
        {
            BoundingBox2f containerBound = new BoundingBox2f();
            foreach (Vector2f v in section.VectorMapPoints)
            {
                containerBound.Expand(v);
            }

            minX = containerBound.Min.X;
            minY = containerBound.Min.Y;
            maxX = containerBound.Max.X;
            maxY = containerBound.Max.Y;
        }

        /// <summary>
        /// Create the sector tools input sector list file.
        /// </summary>
        /// <param name="filename"></param>
        public void CreateSectorFile(String filename, IMapSection[] sections)
        {
            using (TextWriter tw = new StreamWriter(filename))
            {
                foreach (IMapSection section in sections)
                {
                    if (section.VectorMapPoints == null)
                    {
                        Log.Log__Error("No vector map for map container: {0}.", section.Name);
                        continue;
                    }

                    BoundingBox2f containerBound = new BoundingBox2f();
                    foreach (Vector2f v in section.VectorMapPoints)
                    {
                        containerBound.Expand(v);
                    }

                    tw.WriteLine("{0} {1} {2} {3}",
                        containerBound.Min.X, containerBound.Min.Y,
                        containerBound.Max.X, containerBound.Max.Y);
                }
                tw.Flush();
            }
        }
        #endregion // Private Methods
    }
}
