﻿using RSG.Model.Report;

namespace Workbench.AddIn.MapStatistics.Reports
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Report.AddIn.ExtensionPoints.MapStatsReport, typeof(IReport))]
    public class LodChildReport : RSG.Model.Report.Reports.Map.LodChildReport
    {
    } // LodChildReport
} // Workbench.AddIn.MapStatistics.Reports
