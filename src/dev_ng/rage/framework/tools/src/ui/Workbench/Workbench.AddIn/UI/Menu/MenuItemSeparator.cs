﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Workbench.AddIn.UI.Menu
{

    /// <summary>
    /// Menu item separator class; add instances to menus for separator 
    /// goodness.
    /// </summary>
    public class MenuItemSeparator : MenuItemBase
    {
        #region Constructor(s)

        /// <summary>
        /// Default constructor.
        /// </summary>
        public MenuItemSeparator()
            : base("")
        {
            this.IsSeparator = true;
        }

        #endregion // Constructor(s)

        #region ICommand Implementation

        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
        }

        #endregion // ICommand Implementation
    }

} // Workbench.AddIn.UI.Menu namespace
