﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Windows.Helpers;
using ragCore;

namespace WidgetEditor.AddIn
{
    /// <summary>
    /// 
    /// </summary>
    public class GameConnectionChangedEventArgs : ItemChangedEventArgs<GameConnection>
    {
        public GameConnectionChangedEventArgs(GameConnection oldConnection, GameConnection newConnection)
            : base(oldConnection, newConnection)
        {
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="args"></param>
    public delegate void GameConnectionChangedEventHandler(Object sender, GameConnectionChangedEventArgs args);
}
