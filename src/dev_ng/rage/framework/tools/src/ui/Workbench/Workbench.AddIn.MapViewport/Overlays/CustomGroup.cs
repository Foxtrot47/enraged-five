﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using RSG.Base.Configuration;
using RSG.Base.Math;
using RSG.Metadata.Model;
using RSG.Model.Common;
using RSG.Model.Common.Map;
using ContentBrowser.AddIn;
using Workbench.AddIn.Services;
using Workbench.AddIn.Services.Model;

namespace Workbench.AddIn.MapViewport.Overlays
{
    /// <summary>
    /// The Custom overlay group. This is located in the map viewport project 
    /// as it should be seen as a standard overlay that always shows. The 
    /// overlays are taken from a specific directory.
    /// </summary>
    [ExportExtension(Viewport.AddIn.ExtensionPoints.OverlayGroup, typeof(Viewport.AddIn.IViewportOverlayGroup))]
    public class CustomGroup : Viewport.AddIn.ViewportOverlayGroup, IPartImportsSatisfiedNotification
    {
        #region Constants
        /// <summary>
        /// Group name.
        /// </summary>
        private static readonly string GroupName = "Custom";
        /// <summary>
        /// The high level directory to the saved custom overlays
        /// </summary>
        private static readonly string directory = "workbench\\custom_overlays";
        #endregion

        #region MEF Imports
        /// <summary>
        /// The main config service for the workbench.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(Workbench.AddIn.Services.IConfigurationService))]
        public IConfigurationService Configurations { get; set; }

        /// <summary>
        /// Level Browser
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LevelBrowser, typeof(ILevelBrowser))]
        public ILevelBrowser LevelBrowserProxy { get; set; }

        /// <summary>
        /// The perforce sync service
        /// </summary>
        [Workbench.AddIn.ImportExtension(Workbench.AddIn.CompositionPoints.PerforceSyncService, typeof(Workbench.AddIn.Services.IPerforceSyncService))]
        private IPerforceSyncService PerforceSyncService { get; set; }

        /// <summary>
        /// MEF import for message box service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.MessageService, typeof(Workbench.AddIn.Services.IMessageService))]
        private IMessageService MessageService { get; set; }

        /// <summary>
        /// MEF import for perforce service.
        /// </summary>
        [ImportExtension(PerforceBrowser.AddIn.CompositionPoints.PerforceService, typeof(PerforceBrowser.AddIn.IPerforceService))]
        public PerforceBrowser.AddIn.IPerforceService PerforceService { get; set; }

        [ImportExtension(Viewport.AddIn.CompositionPoints.MapViewport, typeof(Viewport.AddIn.IMapViewport))]
        private Viewport.AddIn.IMapViewport MapViewportViewModel { get; set; }
        #endregion

        #region Properties
        /// <summary>
        /// A list of all the mapsections and their outline points that can be added to a
        /// custom overlay map.
        /// </summary>
        public SortedDictionary<string, IList<Vector2f>> MapSections
        {
            get;
            set;
        }

        /// <summary>
        /// The path to the directory the custom overlays are stored in.
        /// </summary>
        public string DirectoryPath
        {
            get;
            set;
        }

        public StructureDictionary DefinitionDictionary;
        #endregion

        #region Constructor
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="Workbench.AddIn.MapViewport.Overlays.CustomGroup"/>
        /// class.
        /// </summary>
        public CustomGroup()
            : base(CustomGroup.GroupName)
        {
            this.MapSections = new SortedDictionary<string, IList<Vector2f>>();
        }
        #endregion

        #region IPartImportsSatisfiedNotification Methods
        /// <summary>
        /// Occurs when all stated MEF imports have been successfully imported.
        /// </summary>
        public void OnImportsSatisfied()
        {
            LevelBrowserProxy.LevelChanged += OnLevelChanged;
            CreateAvailableSections();
        }
        #endregion // IPartImportsSatisfiedNotification Methods

        #region Overrides
        /// <summary>
        /// Called when the overlay group becomes the active group
        /// </summary>
        public override void Activated()
        {
            base.Activated();

            // Make sure the files required for custom overlays are synced
            string customOverlaysDef = Configurations.GameConfig.MetadataDir + @"\definitions\workbench\CustomOverlays.psc";

            List<string> perforceFiles = new List<string>();
            perforceFiles.Add(customOverlaysDef);
            perforceFiles.Add(Configurations.GameConfig.MetadataDir + @"\workbench\custom_overlays\...");
            perforceFiles.Add(Configurations.Config.ToolsConfig + @"\content\levels\...");

            int syncedFiles = 0;
            PerforceSyncService.Show("The workbench metadata definitions are currently out of date.\n" +
                                     "Would you like to grab latest now?\n\n" +
                                     "You can select which files to grab using the table below.\n",
                                     perforceFiles.ToArray(), ref syncedFiles, false, true);

            // Check that the user has the latest version of the //depot/gta5/assets/metadata/definitions/workbench/CustomOverlays.psc
            // file as without it the custom overlay functionality won't work
            try
            {
                P4API.P4RecordSet results = PerforceService.PerforceConnection.Run("fstat", customOverlaysDef);
                bool hasLatest = true;

                if (results.Records.Length == 1)
                {
                    if (results.Records[0].Fields.ContainsKey("headRev") &&
                        results.Records[0].Fields.ContainsKey("haveRev"))
                    {
                        hasLatest = (results.Records[0].Fields["headRev"] == results.Records[0].Fields["haveRev"]);
                    }
                    else
                    {
                        hasLatest = false;
                    }
                }

                if (!hasLatest)
                {
                    System.Windows.MessageBoxResult result =
                        MessageService.Show("You don't appear to have the latest of the custom overlay metadata definitions file.  " + 
                            "Please note that without the latest version of this file the custom overlays functionality may not work as expected.\n\n" +
                            "Do you wish to get latest of this file?", System.Windows.MessageBoxButton.YesNo, System.Windows.MessageBoxImage.Warning);

                    if (result == System.Windows.MessageBoxResult.Yes)
                    {
                        PerforceService.PerforceConnection.Run("sync", customOverlaysDef);
                    }
                }
            }
            catch (System.Exception)
            {
            }

            // Create the list of custom overlays
            RefreshDirectoryList();
        }
        #endregion // Overrides

        #region Methods
        /// <summary>
        /// Occurs when the level changes and a different set of vector maps are available.
        /// </summary>
        /// <param name="sender">
        ///The instance of the object that this handler is attached to. 
        /// </param>
        /// <param name="args">
        /// The event data.
        /// </param>
        private void OnLevelChanged(object sender, LevelChangedEventArgs args)
        {
            CreateAvailableSections(args.NewItem);
        }

        /// <summary>
        /// Uses the selected level as a root and finds all the available sections 
        /// in it that have valid vector maps and can be added to a custom overlay.
        /// </summary>
        internal void CreateAvailableSections()
        {
            CreateAvailableSections(LevelBrowserProxy.SelectedLevel);
        }

        /// <summary>
        /// 
        /// </summary>
        internal void LoadDefinitionsDictionary()
        {
            if (DefinitionDictionary == null)
            {
                IBranch branch = this.Configurations.Config.Project.DefaultBranch;
                String path = Path.Combine(branch.Metadata, "definitions", "workbench");
                DefinitionDictionary = new StructureDictionary();
                DefinitionDictionary.Load(branch, path);
            }
        }

        /// <summary>
        /// Refreshes the list of custom overlays from the directory.
        /// </summary>
        internal void RefreshDirectoryList()
        {
            this.Overlays.Clear();

            DirectoryPath = Path.Combine(Configurations.GameConfig.MetadataDir, directory);
            if (Directory.Exists(DirectoryPath))
            {
                string[] filenames = Directory.GetFiles(DirectoryPath, "*.meta");
                foreach (string filename in filenames)
                {
                    this.Overlays.Add(new CustomOutlineOverlay(filename, MapViewportViewModel, this));
                }
            }
            else
            {
                Directory.CreateDirectory(DirectoryPath);
            }
            this.Overlays.Add(new CustomOutlineOverlay(MapViewportViewModel, this));
            this.Overlays.Add(new RefreshCustomOutlineOverlay(this));
        }

        /// <summary>
        /// Uses the specified level as a root and finds all the avaliable sections in it
        /// that have valid vector maps and can be added to a custom overlay.
        /// </summary>
        /// <param name="level">
        /// The level to use as the map root to find the valid sections.
        /// </param>
        private void CreateAvailableSections(ILevel level)
        {
            MapSections.Clear();
            if (level == null)
            {
                return;
            }

            IMapHierarchy hierarchy = level.MapHierarchy;
            if (hierarchy == null)
            {
                return;
            }

            foreach (IMapSection section in hierarchy.AllSections)
            {
                if (section.VectorMapPoints == null || MapSections.ContainsKey(section.Name.ToLower()))
                {
                    continue;
                }

                this.MapSections.Add(section.Name.ToLower(), section.VectorMapPoints);
            }
        }
        #endregion
    } // CustomGroup
} // Workbench.AddIn.MapViewport.Overlays
