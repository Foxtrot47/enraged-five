﻿using System;
using System.Windows.Input;
using System.Diagnostics;
using System.Drawing;
using System.Windows;
using RSG.Base.Editor;
using System.Collections.Generic;
using RSG.Base.Logging;
using Workbench.AddIn;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.Services.File;
using MetadataEditor.AddIn;
using MetadataEditor.AddIn.View;
using RSG.Metadata.Parser;
using RSG.Metadata.Model;
using MetadataEditor.AddIn.DefinitionEditor.ViewModel;

namespace MetadataEditor.Services.File
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.NewService, typeof(INewService))]
    [ExportExtension(Workbench.AddIn.ExtensionPoints.ToolbarStandard, typeof(INewService))]
    class NewService : INewService
    {
        #region Constants
        private readonly Guid GUID =
            new Guid("E0BBD5ED-F876-466B-9D02-1A30F134395B");
        #endregion // Constants

        #region MEF Imports

        /// <summary>
        /// 
        /// </summary>
        [ImportManyExtension(MetadataEditor.AddIn.ExtensionPoints.MetadataDocument,
            typeof(IMetadataDocumentProxy))]
        private List<IMetadataDocumentProxy> MetadataDocuments { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(MetadataEditor.AddIn.ExtensionPoints.DefaultMetadataDocument,
            typeof(IMetadataDocumentProxy))]
        private Lazy<IMetadataDocumentProxy> DefaultMetadataDocument { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(MetadataEditor.AddIn.CompositionPoints.StructureDictionary,
            typeof(IStructureDictionary))]
        private Lazy<IStructureDictionary> Structures { get; set; }

        #endregion // MEF Imports

        #region Properties
        /// <summary>
        /// Service GUID.
        /// </summary>
        public Guid ID
        {
            get { return (GUID); }
        }

        /// <summary>
        /// New service header string for UI display.
        /// </summary>
        public String Header
        {
            get { return (Properties.Settings.Default.MetadataFileNewHeader); }
        }

        /// <summary>
        /// New service Bitmap for UI display.
        /// </summary>
        public Bitmap Image
        {
            get { return (Resources.Images.NEW); }
        }

        /// <summary>
        /// The keybinding that is placed on this command.
        /// This binding is added to the applications bindings.
        /// (Only used if this is a main menu item)
        /// </summary>
        public KeyGesture KeyGesture
        {
            get { return new KeyGesture(Key.N, ModifierKeys.Control); }
        }

        #endregion // Properties

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public NewService()
        {
        }

        #endregion // Constructor

        #region Methods

        /// <summary>
        /// 
        /// </summary>
        public bool PreNew(out Object param)
        {
            UI.View.DefinitionWindow win = new UI.View.DefinitionWindow("", this.Structures.Value);
            StructureDictionaryViewModel vm = win.DataContext as StructureDictionaryViewModel;
            win.Owner = Application.Current.MainWindow;

            Nullable<bool> res = win.ShowDialog();
            param = win.SelectedStructure;

            return (res.Value && (null != param));
        }

        /// <summary>
        /// 
        /// </summary>
        public bool New(out IDocumentBase document, out IModel model, String filename, Object param)
        {
            // Ensure that the IMetadataModelView is constructed.
            document = null;
            model = null;

            String rootDataType = (param as StructureViewModel).Model.DataType;

            // Loop through our supported metadata types and see if we have
            // a more convenient one than the default editor.  Use the
            // first one that matches; otherwise default.
            Boolean result = false;
            foreach (IMetadataDocumentProxy proxy in this.MetadataDocuments)
            {
                List<String> types = new List<String>(proxy.SupportedMetadataTypes);
                if (types.Contains(rootDataType))
                {
                    result = proxy.CreateNewDocument(out document, out model, (param as StructureViewModel).Model, Structures.Value.Structures);
                    if (result == true)
                    {
                        document.Title = filename;
                        document.IsModified = true;
                        document.IsReadOnly = false;
                        return true;
                    }
                }
            }
            // If we haven't created our IMetadataDocument yet then lets
            // fallback to our default view.
            if (document == null)
            {
                result = this.DefaultMetadataDocument.Value.CreateNewDocument(out document, out model, (param as StructureViewModel).Model, Structures.Value.Structures);
            }

            if (result == true && document != null)
            {
                document.Title = filename;
                document.IsModified = true;
                document.IsReadOnly = false;
                return true;
            }
            return false;
        }

        #endregion // Methods
    }
} // MetadataEditor.Services.File namespace
