﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn.Services.File;
using Workbench.AddIn;
using Workbench.AddIn.Services;
using System.Windows.Input;
using Workbench.AddIn.UI.Layout;
using RSG.Base.Editor;
using System.Windows;
using System.Drawing;

namespace MetadataEditor.AddIn.KinoEditor.Cutscene_Batch_Exporter.Services.File
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.OpenService, typeof(IOpenService))]
    class OpenService : IOpenService
    {
        #region Constants
        private readonly Guid GUID = new Guid("F953DF95-CD04-4F86-87BA-4210496E7AF4");
        private readonly String COMMAND_LINE_KEY = String.Empty;
        #endregion // Constants

        #region Imports
        /// <summary>
        /// MEF import for core workbench services.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.CoreServicesProvider,
            typeof(Workbench.AddIn.Services.ICoreServiceProvider))]
        private Lazy<Workbench.AddIn.Services.ICoreServiceProvider> CoreServiceProvider { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.WebBrowserService, typeof(IWebBrowserService))]
        private IWebBrowserService WebBrowser { get; set; }
        #endregion

        #region Properties
        /// <summary>
        /// Service GUID.
        /// </summary>
        public Guid ID
        {
            get { return (GUID); }
        }

        /// <summary>
        /// Open service header string for UI display.
        /// </summary>
        public String Header
        {
            get { return "Open Cutscene Batch Exporter Files..."; }
        }

        /// <summary>
        /// Open service Bitmap for UI display.
        /// </summary>
        public Bitmap Image
        {
            get { return null; }
        }

        /// <summary>
        /// Open dialog filter string.
        /// </summary>
        public String Filter
        {
            get { return "Batch exporter files (.batchxml)|*.batchxml"; }
        }

        /// <summary>
        /// Determines if the open dialog allows multiple items selected
        /// </summary>
        public Boolean AllowMultiSelect
        {
            get { return false; }
        }

        /// <summary>
        /// Open dialog default filter index.
        /// </summary>
        public int DefaultFilterIndex
        {
            get { return 0; }
        }

        /// <summary>
        /// The keybinding that is placed on this command.
        /// This binding is added to the applications bindings.
        /// (Only used if this is a main menu item)
        /// </summary>
        public KeyGesture KeyGesture
        {
            get { return null; }
        }

        /// <summary>
        /// Command line key.
        /// </summary>
        public String CommandLineKey
        {
            get { return COMMAND_LINE_KEY; }
        }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        public bool Open(out IDocumentBase document, out IModel model, String filename, int filterIndex)
        {
            document = new CutsceneBatchExporterToolWindowView(filename, this.CoreServiceProvider.Value.TaskProgressService);
            model = (document as DocumentBase<CutsceneBatchExportToolWindowViewModel>).ViewModel;
            if (document != null && model != null)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documents"></param>
        /// <param name="models"></param>
        /// <param name="filenames"></param>
        /// <param name="filterIndex"></param>
        /// <returns></returns>
        public bool Open(out Dictionary<IDocumentBase, List<String>> documents, out List<IModel> models, List<String> filenames, int filterIndex)
        {
            documents = new Dictionary<IDocumentBase, List<String>>();
            models = new List<IModel>();
            foreach (string filename in filenames)
            {
                var document = new CutsceneBatchExporterToolWindowView(filename, this.CoreServiceProvider.Value.TaskProgressService);
                var model = (document as DocumentBase<CutsceneBatchExportToolWindowViewModel>).ViewModel;

                documents.Add(document, new List<string>() { filename });
                models.Add(model);
            }
            return documents.Count >= 1;
        }

        /// <summary>
        /// Creates a new document based on the specified model
        /// </summary>
        /// <param name="model">
        /// The model that determines what document is created.
        /// </param>
        /// <returns>
        /// A new document that has its model/viewmodel setup to represent the specified model.
        /// </returns>
        public IDocumentBase Open(IModel model)
        {
            throw new NotImplementedException();
        }
        #endregion // Methods
    } // OpenService
}
