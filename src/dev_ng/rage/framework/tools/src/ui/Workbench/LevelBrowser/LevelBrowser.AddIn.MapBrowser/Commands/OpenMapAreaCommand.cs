﻿using System;
using System.ComponentModel.Composition;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using RSG.Base.Logging;
using RSG.Model.Common.Map;
using Workbench.AddIn;
using Workbench.AddIn.Services;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.Commands;

namespace LevelBrowser.AddIn.MapBrowser.Commands
{

    /// <summary>
    /// Command that opens a map area folder.
    /// </summary>
    [ExportExtension(Extensions.MapAreaCommands, typeof(IWorkbenchCommand))]
    class OpenMapAreaCommand : WorkbenchMenuItemBase
    {
        #region Constants
        public static readonly String GUID = "FE0345ED-7CDF-4349-83BC-415E1D3B3F12";
        #endregion // Constants

        #region MEF Imports

        [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager, typeof(ILayoutManager))]
        private ILayoutManager LayoutManager { get; set; }

        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(IConfigurationService))]
        private IConfigurationService ConfigServiceImport { get; set; }


        #endregion // MEF Imports

        #region Constructor

        /// <summary>
        /// Default constructor.
        /// </summary>
        public OpenMapAreaCommand()
        {
            this.Header = "_Open";
            this.IsDefault = true;
            this.ID = new Guid(GUID);
        }

        #endregion // Constructor

        #region ICommand Implementation
        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
            if (parameter is IList)
            {
                foreach (Object area in (parameter as IList))
                {
                    Debug.Assert(area is IMapArea, "Invalid Map Area selection.");
                    this.OpenMapArea((IMapArea)area);
                }
            }
            else
            {
                Debug.Assert(parameter is IMapArea, "Invalid Map Area selection.");
                this.OpenMapArea((IMapArea)parameter);
            }
        }
        #endregion // ICommand Implementation

        #region Private Methods
        /// <summary>
        /// Open a map area folder.
        /// </summary>
        /// <param name="area"></param>
        private void OpenMapArea(IMapArea area)
        {
            Debug.Assert(null != area, "Map area is null.  Folder cannot be opened.  Internal error.");
            if (null == area)
            {
                Log.Log__Error("Map area is null.  Folder cannot be opened.  Internal error.");
                return;
            }
            
            if (!System.IO.Directory.Exists(area.ExportDataPath))
            {
                Log.Log__Warning("Map area folder {0} does not exist.  Cannot open in Explorer.",
                    area.ExportDataPath);
                return;
            }

            ProcessStartInfo runExplorer = new System.Diagnostics.ProcessStartInfo();
            runExplorer.FileName = "explorer.exe";
            runExplorer.Arguments = @"/root," + area.ExportDataPath;
            System.Diagnostics.Process.Start(runExplorer);
        }
        #endregion // Private Methods
    } // OpenMapSectionCommand

} // LevelBrowser.AddIn.MapBrowser.Commands
