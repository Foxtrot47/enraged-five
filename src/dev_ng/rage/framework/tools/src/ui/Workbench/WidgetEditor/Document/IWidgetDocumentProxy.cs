﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn.UI.Layout;

namespace WidgetEditor.Document
{

    public interface IWidgetDocumentProxy
    {
        /// <summary>
        /// Array of String Metadata types this Metadata Document can view
        /// (e.g. "*", "::rage::cutfCutSceneFile2").
        /// </summary>
        String[] SupportedMetadataTypes { get; }

        /// <summary>
        /// Type of this document.
        /// </summary>
        WidgetDocumentType DocumentType { get; }
        
        /// <summary>
        /// Array of support document GUIDs.
        /// </summary>
        Guid[] SupportedContent { get; }

        /// <summary>
        /// Used to create a new instance of this document type from the given model
        /// </summary>
        Boolean OpenDocument(out IDocumentBase document, RSG.Base.Editor.IModel vm);
    }
}
