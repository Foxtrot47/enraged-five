﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Report;

namespace Workbench.AddIn.MapStatistics.Reports
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Report.AddIn.ExtensionPoints.MapStatsReport, typeof(IReport))]
    public class DuplicatedItemReport : RSG.Model.Report.Reports.Map.DuplicatedItemReport
    {
    } // DuplicatedItemReport
} // Workbench.AddIn.MapStatistics.Reports

