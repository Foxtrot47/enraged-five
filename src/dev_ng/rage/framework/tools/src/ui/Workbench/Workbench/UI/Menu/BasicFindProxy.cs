﻿using System;
using System.ComponentModel.Composition;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn;
using Workbench.AddIn.Services;
using Workbench.AddIn.Commands;
using Workbench.AddIn.UI.Layout;
using Workbench.UI.Toolbar;

namespace Workbench.UI.Menu
{
    /// <summary>
    /// 
    /// </summary>
    public class BasicFindProxy : IToolWindowProxy
    {
        #region Properties
       
        /// <summary>
        /// Layout manager reference.
        /// </summary>
        private ILayoutManager LayoutManager { get; set; }

        /// <summary>
        /// 
        /// </summary>
        private IMessageService MessageService { get; set; }

        /// <summary>
        /// 
        /// </summary>
        private List<string> Tags { get; set; }

        /// <summary>
        /// 
        /// </summary>
        private SearchResultWindowViewModel SearchResultsViewModel { get; set; }

        /// <summary>
        /// 
        /// </summary>
        private SearchResultWindowProxy SearchResultWindowProxy { get; set; }
        #endregion // Properties

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public BasicFindProxy()
        {
            this.Name = BasicFindViewModel.BASICFIND_NAME;
            this.Header = BasicFindViewModel.BASICFIND_TITLE;
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public BasicFindProxy(ILayoutManager layoutManager, IMessageService messageService, SearchResultWindowViewModel searchResultsViewModel, SearchResultWindowProxy searchResultWindowProxy, IEnumerable<ISearchableTags> tags)
        {
            this.LayoutManager = layoutManager;
            this.MessageService = messageService;
            this.SearchResultsViewModel = searchResultsViewModel;
            this.SearchResultWindowProxy = searchResultWindowProxy;
            this.Name = BasicFindViewModel.BASICFIND_NAME;
            this.Header = BasicFindViewModel.BASICFIND_TITLE;
            this.Tags = new List<string>();
            foreach (var tag in tags)
            {
                foreach (string tagComponent in tag.Tags)
                {
                    if (!this.Tags.Contains(tagComponent))
                        this.Tags.Add(tagComponent);
                }
            }
        }

        #endregion // Constructor

        #region Public Functions

        /// <summary>
        /// A abstract function that is overriden by the plugin to create the
        /// actual instance of the tool window and pass it back as a out paramter
        /// </summary>
        protected override Boolean CreateToolWindow(out IToolWindowBase toolWindow)
        {
            toolWindow = new BasicFindView(this.LayoutManager, this.MessageService, this.SearchResultsViewModel, SearchResultWindowProxy, Tags);
            return true;
        }
        
        #endregion // Public Functions
    } // PropertyInspectorProxy
} // Workbench.UI
