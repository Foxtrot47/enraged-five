﻿using System;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Windows.Media.Imaging;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Threading;
using ContentBrowser.AddIn;
using RSG.Base.ConfigParser;
using RSG.Model.Common;
using RSG.Model.Perforce;
using RSG.SourceControl.Perforce;
using Workbench.AddIn;
using Workbench.AddIn.Services;

namespace ContentBrowser.AddIn.Perforce
{

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(ContentBrowser.AddIn.ExtensionPoints.Browser, typeof(ContentBrowser.AddIn.BrowserBase))]
    class PerforceBrowser :
        ContentBrowser.AddIn.BrowserBase
    {
        #region Constants
        private const String NAME = "Perforce Browser";
        #endregion // Constants

        #region Members
        private BackgroundWorker m_asyncWorker = new BackgroundWorker();
        private List<Depot> m_depots;
        #endregion // Members

        #region MEF Imports
        /// <summary>
        /// The main content browser export object; will also be used as the view model
        /// for the content browsers main window
        /// </summary>
        [ImportExtension(ContentBrowser.AddIn.CompositionPoints.ContentBrowser, typeof(ContentBrowser.AddIn.IContentBrowser))]
        private Lazy<ContentBrowser.AddIn.IContentBrowser> ContentBrowserImport { get; set; }

        /// <summary>
        /// The main content browser export object; will also be used as the view model
        /// for the content browsers main window
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(Workbench.AddIn.Services.IConfigurationService))]
        private Lazy<IConfigurationService> ConfigurationService { get; set; }
        #endregion // MEF Imports

        #region Properties

        /// <summary>
        /// The icon that will be displayed to represent this browser
        /// </summary>
        public override BitmapImage BrowserIcon
        {
            get
            {
                Uri uriSource = new Uri("pack://application:,,,/ContentBrowser.AddIn.PerforceBrowser;component/Resources/Images/BrowserIcon.png");
                return new BitmapImage(uriSource);
            }
        }

        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public PerforceBrowser()
            : base(NAME)
        {
            this.m_asyncWorker = new BackgroundWorker();
            this.m_asyncWorker.WorkerReportsProgress = false;
            this.m_asyncWorker.WorkerSupportsCancellation = true;
            this.m_asyncWorker.DoWork += new DoWorkEventHandler(this.RefreshDoWork);
            this.m_asyncWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(this.RefreshCompleted);

            this.ID = ContentBrowser.AddIn.CompositionPoints.PerforceBrowserGuid;
            this.RelativeID = ContentBrowser.AddIn.CompositionPoints.MetadataBrowserGuid;
            this.Direction = Workbench.AddIn.Services.Direction.After;
        }
        #endregion // Constructor(s)

        /// <summary>
        /// Override to create the commands that will be attached to
        /// this browser
        /// </summary>
        protected override ObservableCollection<ContentBrowser.AddIn.ContentBrowserCommand> CreateCommands()
        {
            ObservableCollection<ContentBrowser.AddIn.ContentBrowserCommand> commands = base.CreateCommands();
            commands.Add(this.CreateRefreshCommand());
            return commands;
        }


        #region Command Creation

        /// <summary>
        /// Creates the refresh command, used to refresh the level browser data
        /// </summary>
        private ContentBrowser.AddIn.ContentBrowserCommand CreateRefreshCommand()
        {
            Action<Object> execute = param => ExecuteRefreshPerforce();
            Predicate<Object> canExecute = param => CanExecuteRefreshPerforce();
            String toolTip = "Refresh Perforce";
            BitmapImage image = null;
            try
            {
                Uri uriSource = new Uri("pack://application:,,,/LevelBrowser;component/Resources/Images/refresh.png");
                image = new BitmapImage(uriSource);
            }
            catch
            {
            }

            return new ContentBrowser.AddIn.ContentBrowserCommand(execute, canExecute, toolTip, image);
        }
        #endregion // Command Creation

        #region Command Callbacks

        /// <summary>
        /// 
        /// </summary>
        private bool CanExecuteRefreshPerforce()
        {
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        private void ExecuteRefreshPerforce()
        {
            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += new DoWorkEventHandler(RefreshDoWork);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(RefreshCompleted);
            worker.RunWorkerAsync();
        }
        #endregion // Command Callbacks



        /// <summary>
        /// Called when the map data needs to be refreshed (i.e
        /// re-created).
        /// </summary>
        public void OnRefresh(List<Depot> levels)
        {
            this.m_depots = levels;

            if (this.m_asyncWorker.IsBusy)
            {
                this.m_asyncWorker.CancelAsync();
            }
            else
            {
                this.m_asyncWorker.RunWorkerAsync();
            }
        }

        #region Private Methods

        /// <summary>
        /// Work handler for the loading thread
        /// </summary>
        private void RefreshDoWork(Object sender, DoWorkEventArgs e)
        {
            BackgroundWorker bwAsync = sender as BackgroundWorker;
            P4 p4;
            Workspace ws = RSG.Model.Perforce.Util.Creation.GetWorkspace(ConfigurationService.Value.Config, out p4);
            this.AssetHierarchy.Add(ws);
        }

        /// <summary>
        /// Completed handler for the loading thread
        /// </summary>
        private void RefreshCompleted(Object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                OnRefresh(this.m_depots);
            }
        }

        #endregion // Private Methods

    }
     
} // ContentBrowser.AddIn.Perforce namespace
