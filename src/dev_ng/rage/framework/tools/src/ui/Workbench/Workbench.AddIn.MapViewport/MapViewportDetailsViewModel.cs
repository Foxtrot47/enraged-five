﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Collections;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using RSG.Base.Windows.Controls.WpfViewport2D;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using Workbench.AddIn.Services.Model;

namespace Workbench.AddIn.MapViewport
{
    public class MapViewportDetailsViewModel : ViewModelBase
    {
        #region Members

        private ILevelBrowser m_levelBrowser;
        private Viewport.AddIn.IMapViewport m_mapViewport;
        private String m_description;
        private Control m_overlayControl;

        #endregion // Members

        #region Properties

        /// <summary>
        /// A reference to the content browser interface
        /// </summary>
        public ILevelBrowser LevelBrowserProxy
        {
            get { return m_levelBrowser; }
            set
            {
                if (m_levelBrowser == value)
                    return;

                SetPropertyValue(value, () => LevelBrowserProxy,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_levelBrowser = (ILevelBrowser)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// A reference to the map viewport instance
        /// </summary>
        public Viewport.AddIn.IMapViewport MapViewport
        {
            get { return m_mapViewport; }
            set
            {
                if (m_mapViewport == value)
                    return;

                Viewport.AddIn.IMapViewport oldValue = m_mapViewport;

                SetPropertyValue(value, () => MapViewport,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_mapViewport = (Viewport.AddIn.IMapViewport)newValue;
                        }
                ));

                OnViewportChanged(oldValue, value);
            }
        }

        /// <summary>
        /// The description for the selected overlay
        /// </summary>
        public String Description
        {
            get { return m_description; }
            set
            {
                if (m_description == value)
                    return;

                SetPropertyValue(value, () => Description,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_description = (String)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The control that is shown for the selected overlay
        /// </summary>
        public Control OverlayControl
        {
            get { return m_overlayControl; }
            set
            {
                if (m_overlayControl == value)
                    return;

                SetPropertyValue(value, () => OverlayControl,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_overlayControl = (Control)newValue;
                        }
                ));
            }
        }

        #endregion // Properties

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public MapViewportDetailsViewModel(ILevelBrowser levelBrowser, Viewport.AddIn.IMapViewport mapViewport)
        {
            this.LevelBrowserProxy = levelBrowser;
            this.MapViewport = mapViewport;
            if (mapViewport.SelectedOverlay != null)
            {
                this.Description = mapViewport.SelectedOverlay.Description;
                this.OverlayControl = mapViewport.SelectedOverlay.GetOverlayControl();
                if (this.OverlayControl != null)
                    this.OverlayControl.DataContext = mapViewport.SelectedOverlay;
            }
        }

        #endregion // Constructor

        #region Property Changed Handlers

        private void OnViewportChanged(Viewport.AddIn.IMapViewport oldValue, Viewport.AddIn.IMapViewport newValue)
        {
            if (oldValue != null)
            {
                oldValue.OnOverlayChanged -= OnOverlayChanged;
            }
            if (newValue != null)
            {
                newValue.OnOverlayChanged += OnOverlayChanged;
            }
        }

        void OnOverlayChanged(Object sender, Viewport.AddIn.OverlayChangedEventArgs args)
        {
            if (args.NewOverlay != null)
            {
                this.Description = args.NewOverlay.Description;
                this.OverlayControl = args.NewOverlay.GetOverlayControl();
                if (this.OverlayControl != null)
                    this.OverlayControl.DataContext = args.NewOverlay;
            }
            else
            {
                this.Description = null;
                this.OverlayControl = null;
            }
        }

        #endregion // Property Changed Handlers
    } // MapViewportViewModel
} // Workbench.AddIn.MapViewport
