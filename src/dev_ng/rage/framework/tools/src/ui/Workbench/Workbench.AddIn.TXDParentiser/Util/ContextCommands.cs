﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn.TXDParentiser.ViewModel;

namespace Workbench.AddIn.TXDParentiser.Util
{
    public static class ContextCommands
    {
        private static Object syncRoot = new Object();

        #region Add Command

        private static GenericCommand<TXDParentiserViewModel> m_addCommand = null;
        public static GenericCommand<TXDParentiserViewModel> AddCommand
        {
            get
            {
                lock (syncRoot)
                {
                    if (null == m_addCommand)
                    {
                        m_addCommand = new GenericCommand<TXDParentiserViewModel>
                            (param => AddExecute(param), param => AddCanExecute(param));
                    }
                }
                return m_addCommand;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static Boolean AddCanExecute(TXDParentiserViewModel parameter)
        {
            return parameter != null ? parameter.AddCanExecute(null) : true;
        }

        /// <summary>
        /// 
        /// </summary>
        public static void AddExecute(TXDParentiserViewModel parameter)
        {
            if (parameter != null)
            {
                parameter.AddExecute(null);
            }
        }

        #endregion // Add Command

        #region Add Global

        private static GenericCommand<TXDParentiserViewModel> m_addGlobalCommand = null;
        public static GenericCommand<TXDParentiserViewModel> AddGlobalCommand
        {
            get
            {
                lock (syncRoot)
                {
                    if (null == m_addGlobalCommand)
                    {
                        m_addGlobalCommand = new GenericCommand<TXDParentiserViewModel>
                            (param => AddGlobalExecute(param), param => AddGlobalCanExecute(param));
                    }
                }
                return m_addGlobalCommand;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static Boolean AddGlobalCanExecute(TXDParentiserViewModel parameter)
        {
            return parameter != null ? parameter.AddGlobalCanExecute(null) : true;
        }

        /// <summary>
        /// 
        /// </summary>
        public static void AddGlobalExecute(TXDParentiserViewModel parameter)
        {
            if (parameter != null)
            {
                parameter.AddGlobalExecute(null);
            }
        }

        #endregion // Add Command

        #region Add New Parent Command

        private static GenericCommand<TXDParentiserViewModel> m_addNewParentCommand = null;
        public static GenericCommand<TXDParentiserViewModel> AddNewParentCommand
        {
            get
            {
                lock (syncRoot)
                {
                    if (null == m_addNewParentCommand)
                    {
                        m_addNewParentCommand = new GenericCommand<TXDParentiserViewModel>
                            (param => AddNewParentExecute(param), param => AddNewParentCanExecute(param));
                    }
                }
                return m_addNewParentCommand;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static Boolean AddNewParentCanExecute(TXDParentiserViewModel parameter)
        {
            return parameter != null ? parameter.AddNewParentCanExecute(null) : true;
        }

        /// <summary>
        /// 
        /// </summary>
        public static void AddNewParentExecute(TXDParentiserViewModel parameter)
        {
            if (parameter != null)
            {
                parameter.AddNewParentExecute(null);
            }
        }

        #endregion // Add Command

        #region Add Sibling Command

        private static GenericCommand<TXDParentiserViewModel> m_addNewSiblingCommand = null;
        public static GenericCommand<TXDParentiserViewModel> AddNewSiblingCommand
        {
            get
            {
                lock (syncRoot)
                {
                    if (null == m_addNewSiblingCommand)
                    {
                        m_addNewSiblingCommand = new GenericCommand<TXDParentiserViewModel>
                            (param => AddNewSiblingExecute(param), param => AddNewSiblingCanExecute(param));
                    }
                }
                return m_addNewSiblingCommand;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static Boolean AddNewSiblingCanExecute(TXDParentiserViewModel parameter)
        {
            return parameter != null ? parameter.AddNewSiblingCanExecute(null) : true;
        }

        /// <summary>
        /// 
        /// </summary>
        public static void AddNewSiblingExecute(TXDParentiserViewModel parameter)
        {
            if (parameter != null)
            {
                parameter.AddNewSiblingExecute(null);
            }
        }

        #endregion // Add Command

        #region Delete Command

        private static GenericCommand<TXDParentiserViewModel> m_deleteCommand = null;
        public static GenericCommand<TXDParentiserViewModel> DeleteCommand
        {
            get
            {
                lock (syncRoot)
                {
                    if (null == m_deleteCommand)
                    {
                        m_deleteCommand = new GenericCommand<TXDParentiserViewModel>
                            (param => DeleteExecute(param), param => DeleteCanExecute(param));
                    }
                }
                return m_deleteCommand;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static Boolean DeleteCanExecute(TXDParentiserViewModel parameter)
        {
            return parameter != null ? parameter.DeleteCanExecute(null) : true;
        }

        /// <summary>
        /// 
        /// </summary>
        public static void DeleteExecute(TXDParentiserViewModel parameter)
        {
            if (parameter != null)
            {
                parameter.DeleteExecute(null);
            }
        }

        #endregion // Delete Command

        #region Delete Just Parent Command

        private static GenericCommand<TXDParentiserViewModel> m_deleteJustParentCommand = null;
        public static GenericCommand<TXDParentiserViewModel> DeleteJustParentCommand
        {
            get
            {
                lock (syncRoot)
                {
                    if (null == m_deleteJustParentCommand)
                    {
                        m_deleteJustParentCommand = new GenericCommand<TXDParentiserViewModel>
                            (param => DeleteJustParentExecute(param), param => DeleteJustParentCanExecute(param));
                    }
                }
                return m_deleteJustParentCommand;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static Boolean DeleteJustParentCanExecute(TXDParentiserViewModel parameter)
        {
            return parameter != null ? parameter.DeleteJustParentCanExecute(null) : true;
        }

        /// <summary>
        /// 
        /// </summary>
        public static void DeleteJustParentExecute(TXDParentiserViewModel parameter)
        {
            if (parameter != null)
            {
                parameter.DeleteJustParentExecute(null);
            }
        }

        #endregion // Delete Command

        #region Rename Command

        private static GenericCommand<TXDParentiserViewModel> m_renameCommand = null;
        public static GenericCommand<TXDParentiserViewModel> RenameCommand
        {
            get
            {
                lock (syncRoot)
                {
                    if (null == m_renameCommand)
                    {
                        m_renameCommand = new GenericCommand<TXDParentiserViewModel>
                            (param => RenameExecute(param), param => RenameCanExecute(param));
                    }
                }
                return m_renameCommand;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static Boolean RenameCanExecute(TXDParentiserViewModel parameter)
        {
            return parameter != null ? parameter.RenameCanExecute(null) : true;
        }

        /// <summary>
        /// 
        /// </summary>
        public static void RenameExecute(TXDParentiserViewModel parameter)
        {
            if (parameter != null)
            {
                parameter.RenameExecute(null);
            }
        }

        #endregion // Delete Command

        #region Select Low Instance Textures

        private static GenericCommand<TXDParentiserViewModel> m_selectLowCommand = null;
        public static GenericCommand<TXDParentiserViewModel> SelectLowCommand
        {
            get
            {
                lock (syncRoot)
                {
                    if (null == m_selectLowCommand)
                    {
                        m_selectLowCommand = new GenericCommand<TXDParentiserViewModel>
                            (param => SelectLowExecute(param), param => SelectLowCanExecute(param));
                    }
                }
                return m_selectLowCommand;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static Boolean SelectLowCanExecute(TXDParentiserViewModel parameter)
        {
            return parameter != null ? parameter.SelectLowCanExecute(null) : true;
        }

        /// <summary>
        /// 
        /// </summary>
        public static void SelectLowExecute(TXDParentiserViewModel parameter)
        {
            if (parameter != null)
            {
                parameter.SelectLowExecute(null);
            }
        }

        #endregion // Select Low Instance Textures

        #region Select Medium Instance Textures

        private static GenericCommand<TXDParentiserViewModel> m_selectMediumCommand = null;
        public static GenericCommand<TXDParentiserViewModel> SelectMediumCommand
        {
            get
            {
                lock (syncRoot)
                {
                    if (null == m_selectMediumCommand)
                    {
                        m_selectMediumCommand = new GenericCommand<TXDParentiserViewModel>
                            (param => SelectMediumExecute(param), param => SelectMediumCanExecute(param));
                    }
                }
                return m_selectMediumCommand;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static Boolean SelectMediumCanExecute(TXDParentiserViewModel parameter)
        {
            return parameter != null ? parameter.SelectMediumCanExecute(null) : true;
        }

        /// <summary>
        /// 
        /// </summary>
        public static void SelectMediumExecute(TXDParentiserViewModel parameter)
        {
            if (parameter != null)
            {
                parameter.SelectMediumExecute(null);
            }
        }

        #endregion // Select Medium Instance Textures

        #region Select High Instance Textures

        private static GenericCommand<TXDParentiserViewModel> m_selectHighCommand = null;
        public static GenericCommand<TXDParentiserViewModel> SelectHighCommand
        {
            get
            {
                lock (syncRoot)
                {
                    if (null == m_selectHighCommand)
                    {
                        m_selectHighCommand = new GenericCommand<TXDParentiserViewModel>
                            (param => SelectHighExecute(param), param => SelectHighCanExecute(param));
                    }
                }
                return m_selectHighCommand;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static Boolean SelectHighCanExecute(TXDParentiserViewModel parameter)
        {
            return parameter != null ? parameter.SelectHighCanExecute(null) : true;
        }

        /// <summary>
        /// 
        /// </summary>
        public static void SelectHighExecute(TXDParentiserViewModel parameter)
        {
            if (parameter != null)
            {
                parameter.SelectHighExecute(null);
            }
        }

        #endregion // Select Medium Instance Textures

        #region Set WIP Flag

        private static GenericCommand<TXDParentiserViewModel> m_wipCommand = null;
        public static GenericCommand<TXDParentiserViewModel> WIPCommand
        {
            get
            {
                lock (syncRoot)
                {
                    if (null == m_wipCommand)
                    {
                        m_wipCommand = new GenericCommand<TXDParentiserViewModel>
                            (param => WIPExecute(param), param => WIPCanExecute(param));
                    }
                }
                return m_wipCommand;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static Boolean WIPCanExecute(TXDParentiserViewModel parameter)
        {
            return parameter != null ? parameter.WIPCanExecute(null) : true;
        }

        /// <summary>
        /// 
        /// </summary>
        public static void WIPExecute(TXDParentiserViewModel parameter)
        {
            if (parameter != null)
            {
                parameter.WIPExecute(null);
            }
        }
        #endregion // Set WIP Flag

        #region Unset WIP Flag Command

        private static GenericCommand<TXDParentiserViewModel> m_unWipCommand = null;
        public static GenericCommand<TXDParentiserViewModel> UnWIPCommand
        {
            get
            {
                lock (syncRoot)
                {
                    if (null == m_unWipCommand)
                    {
                        m_unWipCommand = new GenericCommand<TXDParentiserViewModel>
                            (param => UnWIPExecute(param), param => UnWIPCanExecute(param));
                    }
                }
                return m_unWipCommand;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static Boolean UnWIPCanExecute(TXDParentiserViewModel parameter)
        {
            return parameter != null ? parameter.UnWIPCanExecute(null) : true;
        }

        /// <summary>
        /// 
        /// </summary>
        public static void UnWIPExecute(TXDParentiserViewModel parameter)
        {
            if (parameter != null)
            {
                parameter.UnWIPExecute(null);
            }
        }
        #endregion // Unset WIP Flag Command

        #region Promote Command

        private static GenericCommand<TXDParentiserViewModel> m_promoteCommand = null;
        public static GenericCommand<TXDParentiserViewModel> PromoteCommand
        {
            get
            {
                lock (syncRoot)
                {
                    if (null == m_promoteCommand)
                    {
                        m_promoteCommand = new GenericCommand<TXDParentiserViewModel>
                            (param => PromoteExecute(param), param => PromoteCanExecute(param));
                    }
                }
                return m_promoteCommand;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static Boolean PromoteCanExecute(TXDParentiserViewModel parameter)
        {
            return parameter != null ? parameter.PromoteCanExecute(null) : true;
        }

        /// <summary>
        /// 
        /// </summary>
        public static void PromoteExecute(TXDParentiserViewModel parameter)
        {
            if (parameter != null)
            {
                parameter.PromoteExecute(null);
            }
        }

        #endregion // Promote Command


        #region Validate Command

        private static GenericCommand<TXDParentiserViewModel> m_validateCommand = null;
        public static GenericCommand<TXDParentiserViewModel> ValidateCommand
        {
            get
            {
                lock (syncRoot)
                {
                    if (null == m_validateCommand)
                    {
                        m_validateCommand = new GenericCommand<TXDParentiserViewModel>
                            (param => ValidateExecute(param), param => ValidateCanExecute(param));
                    }
                }
                return m_validateCommand;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static Boolean ValidateCanExecute(TXDParentiserViewModel parameter)
        {
            return parameter != null ? parameter.ValidateCanExecute(null) : true;
        }

        /// <summary>
        /// 
        /// </summary>
        public static void ValidateExecute(TXDParentiserViewModel parameter)
        {
            if (parameter != null)
            {
                parameter.ValidateExecute(null);
            }
        }

        #endregion // Validate Command
    }
}
