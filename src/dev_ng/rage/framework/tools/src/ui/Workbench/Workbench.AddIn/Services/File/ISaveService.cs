﻿using System;
using RSG.Base.Editor;
using Workbench.AddIn.Services;

namespace Workbench.AddIn.Services.File
{

    /// <summary>
    /// Service to save an existing IModel disk file.
    /// </summary>
    public interface ISaveService : IService
    {
        #region Properties

        /// <summary>
        /// Save dialog filter string.
        /// </summary>
        /// E.g. "XML File (.xml)|*.xml" or
        ///      "XML File (.xml)|*.xml|DAT File (.dat)|*.dat"
        String Filter { get; }

        /// <summary>
        /// Save dialog default filter index.
        /// </summary>
        /// For when you have multiple filter options you can have a non-zero
        /// default if required.
        int DefaultFilterIndex { get; }

        /// <summary>
        /// Array of support document GUIDs.
        /// </summary>
        Guid[] SupportedContent { get; }

        #endregion // Properties

        #region Methods

        /// <summary>
        /// Returns a value indicating if the save service
        /// can currently be executed
        /// </summary>
        Boolean CanExecuteSave();

        /// <summary>
        /// Returns a value indicating if the save service
        /// can currently be executed for a save as event
        /// </summary>
        Boolean CanExecuteSaveAs();

        /// <summary>
        /// Save the specified model data to disk file.
        /// </summary>
        Boolean Save(IModel saveModel, String filename, int filterIndex, ref Boolean setPathToSavePath);

        #endregion // Methods
    }

} // Workbench.AddIn.Services.File namespace
