﻿using System;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using RSG.Base.Logging;
using Workbench.AddIn;
using Workbench.AddIn.UI.Layout;
using Editor.Utilities;
using RSG.Metadata.Parser;
using RSG.Metadata.Model;
using RSG.Base.Editor.Command;
using MetadataEditor.AddIn.DefinitionEditor.ViewModel;

namespace MetadataEditor.AddIn.DefinitionEditor
{
    /// <summary>
    /// Definition Class View tool window ViewModel.
    /// </summary>
    /// This is a dockable window component.
    public class ClassViewToolWindowViewModel : 
        RSG.Base.Editor.ViewModelBase
    {
        #region Constants
        public const String TOOLWINDOW_NAME = "Definition_Class_View";
        public const String TOOLWINDOW_TITLE = "Definition Class View";
        #endregion // Constants

        #region Members

        private RelayCommand m_refreshCommand;
        private RelayCommand m_errorCommand;
        private IStructureDictionary m_structureDictionary;
        private StructureDictionaryViewModel m_viewModel;
        private BitmapSource m_errorIconSource;

        #endregion // Members

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public StructureDictionaryViewModel Structures
        {
            get { return m_viewModel; }
            set
            {
                SetPropertyValue(value, () => this.Structures,
                    new PropertySetDelegate(delegate(Object newValue) { m_viewModel = (StructureDictionaryViewModel)newValue; }));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public IStructureDictionary StructureDictionary
        {
            get { return m_structureDictionary; }
            private set
            {
                if (m_structureDictionary == value)
                    return;

                IStructureDictionary oldValue = m_structureDictionary;

                SetPropertyValue(value, () => this.StructureDictionary,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_structureDictionary = (IStructureDictionary)newValue;
                        }
                ));

                this.OnStructureDictionaryChanged(oldValue, value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public BitmapSource ErrorIconSource
        {
            get { return m_errorIconSource; }
            set
            {
                SetPropertyValue(value, () => this.ErrorIconSource,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_errorIconSource = (BitmapSource)newValue;
                        }
                ));
            }
        }

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Default constructor.
        /// </summary>
        public ClassViewToolWindowViewModel(IStructureDictionary structures)
        {
            this.StructureDictionary = structures;
            System.Drawing.Icon icon = System.Drawing.SystemIcons.Warning;
            this.ErrorIconSource = System.Windows.Interop.Imaging.CreateBitmapSourceFromHIcon(icon.Handle, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
        }

        #endregion // Constructor(s)

        #region Commands

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand RefreshCommand
        {
            get
            {
                if (m_refreshCommand == null)
                {
                    m_refreshCommand = new RelayCommand(param => this.Refresh(param), param => this.CanFresh(param));
                }
                return m_refreshCommand;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand ErrorCommand
        {
            get
            {
                if (m_errorCommand == null)
                {
                    m_errorCommand = new RelayCommand(param => this.ShowErrors(param), param => this.CanShowErrors(param));
                }
                return m_errorCommand;
            }
        }

        #endregion // Commands

        #region Command Callbacks

        /// <summary>
        /// 
        /// </summary>
        private Boolean CanFresh(Object param)
        {
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        private void Refresh(Object param)
        {
            this.StructureDictionary.RefreshDefinitions();
        }

        /// <summary>
        /// 
        /// </summary>
        private Boolean CanShowErrors(Object param)
        {
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        private void ShowErrors(Object param)
        {
            this.StructureDictionary.ShowDefinitionErrorView();
        }

        #endregion // Command Callbacks

        #region Property Changed Callbacks

        /// <summary>
        /// 
        /// </summary>
        private void OnStructureDictionaryChanged(IStructureDictionary oldValue, IStructureDictionary newValue)
        {
            if (oldValue != null)
                oldValue.DefinitionsRefreshed -= new EventHandler(OnDefinitionsRefreshed);

            if (newValue != null)
            {
                CreateStructuresViewModel(newValue);
                newValue.DefinitionsRefreshed += new EventHandler(OnDefinitionsRefreshed);
            }
            else
                this.Structures = new StructureDictionaryViewModel();
        }

        #endregion // Property Changed Callbacks

        #region Private Functions

        void CreateStructuresViewModel(MetadataEditor.AddIn.IStructureDictionary structures)
        {
            this.Structures = new StructureDictionaryViewModel(structures.Structures);
        }

        void OnDefinitionsRefreshed(Object sender, EventArgs e)
        {
            CreateStructuresViewModel(sender as MetadataEditor.AddIn.IStructureDictionary);
        }

        #endregion // Private Functions
    } // ClassViewToolWindowViewModel
} // MetadataEditor.AddIn.DefinitionEditorViewModel
