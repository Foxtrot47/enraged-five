﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Windows.Input;
using Workbench.AddIn;
using Workbench.AddIn.Commands;
using Workbench.AddIn.Services;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.UI.Menu;
using Workbench.UI.Toolbar;

namespace Workbench.UI.Menu
{
    /// <summary>
    /// Basic Text Find menu item.
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.MenuFind, typeof(IWorkbenchCommand))]
    public class BasicFindItem : WorkbenchMenuItemBase
    {
        #region Constants

        public static readonly Guid GUID = new Guid("60C469A8-76FA-4441-BD54-EFB7FA9CF679");

        #endregion // Constants

        #region MEF Imports
        [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager, typeof(ILayoutManager))]
        private ILayoutManager LayoutManager { get; set; }

        [ImportExtension(Workbench.AddIn.CompositionPoints.MessageService, typeof(IMessageService))]
        private IMessageService MessageService { get; set; }

        [ImportExtension("Workbench.UI.SearchResultWindowViewModel", typeof(SearchResultWindowViewModel))]
        private SearchResultWindowViewModel SearchResultsViewModel { get; set; }

        [ImportExtension("Workbench.UI.SearchResultWindowProxy", typeof(SearchResultWindowProxy))]
        private SearchResultWindowProxy SearchResultWindowProxy { get; set; }

        [ImportManyExtension(Workbench.AddIn.ExtensionPoints.SearchableTags, typeof(ISearchableTags))]
        private IEnumerable<ISearchableTags> SearchableTags { get; set; }
        #endregion // MEF Imports

        #region Properties

        private BasicFindProxy ViewProxy { get; set; }

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Default constructor.
        /// </summary>
        public BasicFindItem()
        {
            this.Header = "Quick _Find";
            this.ID = GUID;
            this.KeyGesture = new System.Windows.Input.KeyGesture(Key.F, ModifierKeys.Control);
        }

        #endregion // Constructor(s)

        #region ICommand Implementation

        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
            if (ViewProxy == null)
            {
                ViewProxy = new BasicFindProxy(this.LayoutManager, this.MessageService, SearchResultsViewModel, SearchResultWindowProxy, SearchableTags);
            }

            IToolWindowBase toolWindow = this.ViewProxy.GetToolWindow();
            if (toolWindow != null)
            {
                if (this.LayoutManager.IsVisible(toolWindow))
                    this.LayoutManager.HideToolWindow(toolWindow);
                else
                {
                    if (toolWindow.WasFloatingWindowWhenHidden == true)
                        this.LayoutManager.ShowAsFloatingWindow(toolWindow);
                    else
                        this.LayoutManager.ShowToolWindow(toolWindow);

                    if (toolWindow.FloatByDefault == true && toolWindow.HasBeenShown == false)
                    {
                        toolWindow.FloatingWindowSize = toolWindow.DefaultFloatSize;
                        this.LayoutManager.ShowAsFloatingWindow(toolWindow);
                    }
                }
            }
        }

        #endregion // ICommand Implementation
    } // BasicFindItem
} // Workbench.UI.Menu
