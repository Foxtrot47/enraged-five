﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Workbench.AddIn.UI.Layout;

namespace Workbench.AddIn.GameStatistics.Reports.Automated
{
    /// <summary>
    /// Interaction logic for AutomatedTestView.xaml
    /// </summary>
    public partial class AutomatedTestView : DocumentBase<AutomatedTestReport>
    {
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="report"></param>
        public AutomatedTestView(AutomatedTestReport report)
            : base(report.Name.Replace(" ", ""), report)
        {
            InitializeComponent();
            this.Title = String.Format("Report: {0}", report.Name);
        }
        #endregion // Constructor(s)
    } // AutomatedTestView
}
