﻿using System;
using System.Windows.Input;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Windows;
using RSG.Base.Editor;
using Workbench.AddIn;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.Services;
using Workbench.AddIn.Services.File;
using RSG.Model.GlobalTXD;
using System.ComponentModel;
using Workbench.AddIn.Services.Model;

namespace Workbench.AddIn.TXDParentiser.Services.File
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.OpenService, typeof(IOpenService))]
    class OpenService : IOpenService
    {
        #region Constants
        private readonly Guid GUID = new Guid("FA92215B-A764-40FC-9571-8B0DD700F6EB");
        private readonly String COMMAND_LINE_KEY = String.Empty;
        #endregion // Constants

        #region MEF Imports

        /// <summary>
        /// The configuration service that has in it the game view
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(IConfigurationService))]
        Lazy<IConfigurationService> ConfigService
        {
            get;
            set;
        }

        /// <summary>
        /// The configuration service that has in it the game view
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.MessageService, typeof(IMessageService))]
        Lazy<IMessageService> MessageService
        {
            get;
            set;
        }

        /// <summary>
        /// A reference to the level browser interface
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LevelBrowser, typeof(ILevelBrowser))]
        private Lazy<ILevelBrowser> LevelBrowserProxy { get; set; }

        /// <summary>
        /// MEF import for perforce service.
        /// </summary>
        [ImportExtension(PerforceBrowser.AddIn.CompositionPoints.PerforceService, typeof(PerforceBrowser.AddIn.IPerforceService))]
        private Lazy<PerforceBrowser.AddIn.IPerforceService> PerforceService { get; set; }

        /// <summary>
        /// The perforce sync service
        /// </summary>
        [Workbench.AddIn.ImportExtension(Workbench.AddIn.CompositionPoints.PerforceSyncService, typeof(Workbench.AddIn.Services.IPerforceSyncService))]
        private Lazy<Workbench.AddIn.Services.IPerforceSyncService> PerforceSyncService { get; set; }

        /// <summary>
        /// The progress service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ProgressService, typeof(Workbench.AddIn.Services.IProgressService))]
        private Lazy<Workbench.AddIn.Services.IProgressService> ProgressService { get; set; }

        /// <summary>
        /// MEF import for login service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.WorkbenchSaveService, typeof(Workbench.AddIn.Services.File.IWorkbenchSaveCurrentService))]
        public Lazy<Workbench.AddIn.Services.File.IWorkbenchSaveCurrentService> SaveService { get; set; }

        #endregion // MEF Imports

        #region Properties

        /// <summary>
        /// Service GUID.
        /// </summary>
        public Guid ID
        {
            get { return (GUID); }
        }

        /// <summary>
        /// Open service header string for UI display.
        /// </summary>
        public String Header
        {
            get { return "Open TXD Parentiser File..."; }
        }

        /// <summary>
        /// Open service Bitmap for UI display.
        /// </summary>
        public Bitmap Image
        {
            get { return null; }
        }

        /// <summary>
        /// Open dialog filter string.
        /// </summary>
        public String Filter
        {
            get { return "TXD documents (.xml)|*.xml"; }
        }

        /// <summary>
        /// Determines if the open dialog allows multiple items selected
        /// </summary>
        public Boolean AllowMultiSelect
        {
            get { return false; }
        }

        /// <summary>
        /// Open dialog default filter index.
        /// </summary>
        public int DefaultFilterIndex
        {
            get { return 0; }
        }

        /// <summary>
        /// The keybinding that is placed on this command.
        /// This binding is added to the applications bindings.
        /// (Only used if this is a main menu item)
        /// </summary>
        public KeyGesture KeyGesture
        {
            get { return null; }
        }

        /// <summary>
        /// Command line key.
        /// </summary>
        public String CommandLineKey
        {
            get { return COMMAND_LINE_KEY; }
        }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        public bool Open(out IDocumentBase document, out IModel model, String filename, int filterIndex)
        {
            bool link = false;

            document = null;
            model = null;

            document = new TXDParentiserView(new Uri(filename, UriKind.Absolute), this.ConfigService.Value, this.PerforceService.Value, this.PerforceSyncService.Value, this.ProgressService.Value, LevelBrowserProxy.Value, SaveService.Value, link);
            model = (document as DocumentBase<TXDParentiserViewModel>).ViewModel;
            if (document != null && model != null)
            {
                document.OpenService = this;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Creates a new document based on the specified model
        /// </summary>
        /// <param name="model">
        /// The model that determines what document is created.
        /// </param>
        /// <returns>
        /// A new document that has its model/viewmodel setup to represent the specified model.
        /// </returns>
        public IDocumentBase Open(IModel model)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documents"></param>
        /// <param name="models"></param>
        /// <param name="filenames"></param>
        /// <param name="filterIndex"></param>
        /// <returns></returns>
        public bool Open(out Dictionary<IDocumentBase, List<String>> documents, out List<IModel> models, List<String> filenames, int filterIndex)
        {
            bool link = false;

            documents = new Dictionary<IDocumentBase, List<String>>();
            models = new List<IModel>();

            foreach (String filename in filenames)
            {
                IDocumentBase document = new TXDParentiserView(new Uri(filename, UriKind.Absolute), this.ConfigService.Value, this.PerforceService.Value, this.PerforceSyncService.Value, this.ProgressService.Value, this.LevelBrowserProxy.Value, SaveService.Value, link);
                IModel model = (document as DocumentBase<TXDParentiserViewModel>).ViewModel;
                if (document != null && model != null)
                {
                    document.OpenService = this;
                    List<String> files = new List<String>();
                    files.Add(filename);
                    documents.Add(document, files);
                    models.Add(model);
                }
            }
            return documents.Count >= 1;
        }
        #endregion // Methods
    }
}
