﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Report;

namespace Workbench.AddIn.MapStatistics.Reports
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Report.AddIn.ExtensionPoints.MapStatsReport, typeof(IReport))]
    public class MissingTextureReport : RSG.Model.Report.Reports.Map.MissingTextureReport
    {
    } // TextureNameConflictReport
} // Workbench.AddIn.MapStatistics.Reports
