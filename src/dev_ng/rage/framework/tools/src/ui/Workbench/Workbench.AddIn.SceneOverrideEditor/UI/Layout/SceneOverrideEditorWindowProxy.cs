﻿using System;
using System.Windows;
using System.ComponentModel.Composition;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PerforceBrowser.AddIn;
using WidgetEditor.AddIn;
using Workbench.AddIn.Services;
using Workbench.AddIn.UI.Layout;

namespace Workbench.AddIn.SceneOverrideEditor.UI.Layout
{
    [Export(Workbench.AddIn.ExtensionPoints.ToolWindowProxy, typeof(IToolWindowProxy))]
    public class SceneOverrideEditorWindowProxy : IToolWindowProxy
    {
        #region MEF Imports

        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(IConfigurationService))]
        private IConfigurationService ConfigurationService { get; set; }

        [ImportExtension(Workbench.AddIn.CompositionPoints.MessageService, typeof(IMessageService))]
        private IMessageService MessageService { get; set; }

        [ImportExtension(PerforceBrowser.AddIn.CompositionPoints.PerforceService, typeof(IPerforceService))]
        private IPerforceService PerforceService { get; set; }

        [ImportExtension(CompositionPoints.ProgressService, typeof(IProgressService))]
        private IProgressService ProgressService { get; set; }

        [ImportExtension(WidgetEditor.AddIn.CompositionPoints.ProxyService, typeof(IProxyService))]
        public IProxyService ProxyService { get; set; }

        #endregion // MEF Imports

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public SceneOverrideEditorWindowProxy()
        {
            this.Name = Resources.Strings.SceneOverrideEditor_Name;
            this.Header = Resources.Strings.SceneOverrideEditor_Title;
        }

        #endregion // Constructor

        #region Public Functions

        /// <summary>
        /// A abstract function that is overriden by the plugin to create the
        /// actual instance of the tool window and pass it back as a out paramter
        /// </summary>
        protected override Boolean CreateToolWindow(out IToolWindowBase toolWindow)
        {
            toolWindow = new SceneOverrideEditorView(ConfigurationService, MessageService, PerforceService, ProgressService, ProxyService);
            return true;
        }

        #endregion // Public Functions
    } // MapViewportWindowProxy
} // Workbench.AddIn.MapViewport.UI.Layout
