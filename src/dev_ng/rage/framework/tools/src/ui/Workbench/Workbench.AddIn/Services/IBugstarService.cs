﻿using System;
using RSG.Interop.Bugstar;
using RSG.Interop.Bugstar.Organisation;
using RSG.Interop.Bugstar.Search;
using System.Collections.Generic;

namespace Workbench.AddIn.Services
{

    /// <summary>
    /// Bugstar Service; for accessing Bugstar in a friendly way throughout
    /// the Workbench application.
    /// </summary>
    public interface IBugstarService
    {
        #region Properties
        /// <summary>
        /// Core Bugstar connection (may be read-only if not logged in).
        /// </summary>
        BugstarConnection Connection { get; }

        /// <summary>
        /// Bugstar Project (for current tools branch).
        /// </summary>
        Project Project { get; }

        /// <summary>
        /// Bugstar Current User (if logged in; null otherwise).
        /// </summary>
        User User { get; }

        /// <summary>
        /// List of users that the current project has.
        /// </summary>
        User[] ProjectUsers { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Queries bugstar for a list of searches that the current user has.
        /// </summary>
        /// <returns>List of searches for the current user or an empty list of the user hasn't logged into bugstar.</returns>
        IList<Search> GetUserSearches();

        /// <summary>
        /// Queries bugstar for a list of reports that the current user has.
        /// </summary>
        /// <returns>List of reports for the current user or an empty list of the user hasn't logged into bugstar.</returns>
        IList<Report> GetUserReports();

        /// <summary>
        /// Queries bugstar for a list of graphs that the current user has.
        /// </summary>
        /// <returns>List of graphs for the current user or an empty list of the user hasn't logged into bugstar.</returns>
        IList<Graph> GetUserGraphs();
        #endregion // Methods
    }

} // Workbench.AddIn.Services namespace
