﻿using System;
using System.ComponentModel.Composition;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn.Commands;
using Workbench.AddIn;
using RSG.Base.Logging;

namespace ContentBrowser.Commands
{
    /// <summary>
    /// Contains all the commands used in the content browser and
    /// the assets
    /// </summary>
    [ExportExtension("ContentBrowser.Commands.CommandContainer", typeof(CommandContainer))]
    public class CommandContainer : IPartImportsSatisfiedNotification
    {
        #region MEF Imports

        [ImportManyExtension(ContentBrowser.AddIn.ExtensionPoints.AssetCommandContainer, typeof(ContentBrowser.AddIn.IAssetCommandContainer))]
        IEnumerable<ContentBrowser.AddIn.IAssetCommandContainer> CommandContainers
        {
            get;
            set;
        }

        #endregion // MEF Imports

        #region Properties

        /// <summary>
        /// All the commands indexed by the type their are attached to
        /// </summary>
        public Dictionary<Type, List<IWorkbenchCommand>> AssetCommands
        {
            get;
            set;
        }

        public Dictionary<Type, IWorkbenchCommand> DefaultAssetCommands
        {
            get;
            set;
        }

        #endregion // Properties

        #region IPartImportsSatisfiedNotification Interface

        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {
            Log.Log__Profile("CommandContainer OnImportsSatisfied()");

            this.AssetCommands = new Dictionary<Type, List<IWorkbenchCommand>>();
            this.DefaultAssetCommands = new Dictionary<Type,IWorkbenchCommand>();
            foreach (ContentBrowser.AddIn.IAssetCommandContainer container in CommandContainers)
            {
                System.Diagnostics.Debug.Assert(container.AssetType != null, "Command container doesn't have any asset type set.");
                if (container.AssetType != null)
                {
                    if (!this.AssetCommands.ContainsKey(container.AssetType))
                    {
                        AssetCommands.Add(container.AssetType, new List<IWorkbenchCommand>());
                        foreach (IWorkbenchCommand command in container.Commands)
                        {
                            AssetCommands[container.AssetType].Add(command);
                            if (command.IsDefault == true)
                            {
                                if (!this.DefaultAssetCommands.ContainsKey(container.AssetType))
                                {
                                    this.DefaultAssetCommands.Add(container.AssetType, command);
                                }
                            }
                        }
                    }
                }
            }

            Log.Log__ProfileEnd();
        }

        #endregion // IPartImportsSatisfiedNotification Interface
    } // CommandContainer
} // ContentBrowser.Commands
