﻿using System;
using System.Windows;
using Workbench.AddIn;
using MetadataEditor.AddIn.View;

namespace MetadataEditor.AddIn.KinoEditor
{
    /// <summary>
    /// DefinitionEditorView class code.
    /// </summary>
    public partial class KinoEditorView : Workbench.AddIn.UI.Layout.DocumentBase<KinoEditorViewModel>
    {
        //#region Properties and Associated Member Data
        ///// <summary>
        ///// Array of String Metadata types this Metadata Document can view
        ///// (e.g. "*", "::rage::cutfCutSceneFile2").
        ///// </summary>
        //public override String[] SupportedMetadataTypes
        //{
        //    get { return new String[] { "::rage::cutfCutsceneFile2" }; }
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        //public override MetadataDocumentType DocumentType
        //{
        //    get { return MetadataDocumentType.TypeSpecific; }
        //}
        //#endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public KinoEditorView()
        {
            InitializeComponent();
            this.ID = new Guid(CompositionPoints.KinoEditor);
            this.Title = "KinoEditor";
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public KinoEditorView(KinoEditorViewModel viewModel)
            : base(KinoEditor.DOCUMENT_NAME, viewModel)
        {
            InitializeComponent();
            this.ID = new Guid(CompositionPoints.KinoEditor);
            this.Title = "KinoEditor";
        }
        #endregion // Constructor(s)
    }
} // MetadataEditor.AddIn.KinoEditor namespace
