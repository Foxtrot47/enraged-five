﻿using MetadataEditor.AddIn.MetaEditor.ViewModel;
using RSG.Metadata.Data;

namespace MetadataEditor.AddIn.MetaEditor.GridViewModel
{
    /// <summary>
    /// The view model that wraps around a  <see cref="VecBoolVTunable"/> object. Used for the
    /// grid view.
    /// </summary>
    public class VecBoolVTunableViewModel : GridTunableViewModel
    {
        #region Constructor
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="MetadataEditor.AddIn.MetaEditor.GridViewModel.VecBoolVTunableViewModel"/>
        /// class.
        /// </summary>
        public VecBoolVTunableViewModel(GridTunableViewModel parent, VecBoolVTunable m, MetaFileViewModel root)
            : base(parent, m, root)
        {
        }
        #endregion // Constructor
    } // VecBoolVTunableViewModel
} // MetadataEditor.AddIn.MetaEditor.GridViewModel