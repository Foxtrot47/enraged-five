﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using RSG.Base.Editor;
using RSG.Base.Logging;
using Workbench.AddIn;
using Workbench.AddIn.Services.File;
using Workbench.AddIn.UI.Layout;
using MetadataEditor.AddIn;
using MetadataEditor.AddIn.View;
using RSG.Metadata.Model;
using System.Windows;

namespace MetadataEditor.Services.File
{
    [ExportExtension(Workbench.AddIn.ExtensionPoints.SaveService, 
        typeof(ISaveService))]
    class SaveService : ISaveService
    {
        #region Constants
        private readonly Guid GUID =
            new Guid("1166F090-9DE9-4251-9104-0835FDC15037");
        #endregion // Constants

        #region MEF Imports

        /// <summary>
        /// 
        /// </summary>
        [ImportManyExtension(MetadataEditor.AddIn.ExtensionPoints.MetadataDocument,
            typeof(IMetadataDocumentProxy))]
        private List<IMetadataDocumentProxy> MetadataDocuments { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager,
            typeof(Workbench.AddIn.UI.Layout.ILayoutManager))]
        private Workbench.AddIn.UI.Layout.ILayoutManager LayoutManager { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(MetadataEditor.AddIn.ExtensionPoints.DefaultMetadataDocument,
            typeof(IMetadataDocumentProxy))]
        private Lazy<IMetadataDocumentProxy> DefaultMetadataDocument { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(MetadataEditor.AddIn.CompositionPoints.StructureDictionary,
            typeof(IStructureDictionary))]
        private Lazy<IStructureDictionary> Structures { get; set; }

        #endregion // MEF Imports

        #region Properties

        /// <summary>
        /// Service GUID.
        /// </summary>
        public Guid ID
        {
            get { return (GUID); }
        }

        /// <summary>
        /// Save dialog filter string.
        /// </summary>
        public String Filter
        {
            get { return (Properties.Settings.Default.MetadataFileFilter); }
        }

        /// <summary>
        /// Save dialog default filter index.
        /// </summary>
        public int DefaultFilterIndex
        {
            get { return (Properties.Settings.Default.MetadataFileFilterIndex); }
        }

        /// <summary>
        /// Array of support document GUIDs.
        /// </summary>
        public Guid[] SupportedContent
        {
            get
            {
                List<Guid> array = new List<Guid>();
                foreach (IMetadataDocumentProxy proxy in this.MetadataDocuments)
                {
                    array.AddRange(proxy.SupportedContent);
                }
                array.AddRange(DefaultMetadataDocument.Value.SupportedContent);
                return array.ToArray();
            }
        }

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Default constructor.
        /// </summary>
        public SaveService()
        {
        }

        #endregion // Constructors(s)

        #region Methods

        /// <summary>
        /// Returns a value indicating if the save service
        /// can currently be executed
        /// </summary>
        public Boolean CanExecuteSave()
        {
            return true;
        }

        /// <summary>
        /// Returns a value indicating if the save service
        /// can currently be executed for a save as event
        /// </summary>
        public Boolean CanExecuteSaveAs()
        {
            IContentBase content = this.LayoutManager.GetActiveContent();
            if (content != null)
            {
                if (content.SaveModel is IMetaFile)
                {
                    if (content.SaveModel is MultiMetaFiles)
                    {
                        return false;
                    }
    
                    return !(content.SaveModel as IMetaFile).HasInternalFiles;
                }
            }

            return false;
        }

        /// <summary>
        /// Save the specified model data to disk file.
        /// </summary>
        public bool Save(IModel saveModel, String filename, int filterIndex, ref Boolean setPathToSavePath)
        {
            Debug.Assert(saveModel is IMetaFile, "Invalid model passed to Metadata SaveModel.");
            if (!(saveModel is IMetaFile))
                return false;

            foreach (string internalFilename in (saveModel as IMetaFile).GetInternalFilenames)
            {
                System.IO.FileInfo fi = new System.IO.FileInfo(internalFilename);
                if (fi.Exists == true && fi.Attributes.HasFlag(System.IO.FileAttributes.ReadOnly))
                {
                    string msg = string.Format("Unable to save '{0}' due to the following internal file being marked as read-only:\n", filename);
                    msg += internalFilename;
                    MessageBox.Show(msg, "Error Saving!", MessageBoxButton.OK, MessageBoxImage.Error);
                    return false;
                }
            }

            bool result = true;
            try
            {
                IMetaFile mf = (saveModel as IMetaFile);
                if (mf != null)
                {
                    mf.Serialise(filename);
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex, "Exception during Metadata SaveService");
                result = false;
            }

            return (result);
        }

        #endregion // Methods
    }

#if false
    /// <summary>
    /// Service to save an existing Metadata model disk file.
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.SaveService,
        typeof(ISaveService))]
    class SaveService : ISaveService
    {
        #region Constants
        private readonly Guid GUID =
            new Guid("1166F090-9DE9-4251-9104-0835FDC15037");
        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// MEF import for Documents (IDocument).
        /// </summary>
        [ImportManyExtension(MetadataEditor.AddIn.ExtensionPoints.MetadataDocument,
            typeof(IMetadataDocument))]
        private IEnumerable<IMetadataDocument> Documents { get; set; }
        #endregion // MEF Imports

        #region Properties
        /// <summary>
        /// Service GUID.
        /// </summary>
        public Guid ID
        {
            get { return (GUID); }
        }

        /// <summary>
        /// Array of supported document GUIDs.
        /// </summary>
        public Guid[] SupportedDocuments
        {
            get
            {
                List<Guid> docs = new List<Guid>();
                foreach (IMetadataDocument d in this.Documents)
                    docs.Add(d.ID);
                return (docs.ToArray());
            }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public SaveService()
        {
        }
        #endregion // Constructors(s)

        #region Methods
        /// <summary>
        /// Save the specified model data to disk file.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool Save(IModel model)
        {
            Debug.Assert(model is MetaFile, "Invalid model passed to Metadata SaveModel.");
            if (!(model is MetaFile))
                return (false);

            bool result = true;
            try
            {
                MetaFile mf = (model as MetaFile);
                mf.Serialise(mf.Filename);            
            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex, "Exception during Metadata SaveService");
                result = false;
            }

            return (result);
        }
        #endregion // Methods
    }
#endif
} // MetadataEditor.Services.File namespace
