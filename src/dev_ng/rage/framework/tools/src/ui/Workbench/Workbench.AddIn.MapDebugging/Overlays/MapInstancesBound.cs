﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Media;
using RSG.Base.Windows.Controls.WpfViewport2D;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using RSG.Base.Math;
using RSG.Model.Common;
using RSG.Model.Common.Map;
using System.Threading;
using RSG.Base.Tasks;
using RSG.Base.Windows.Dialogs;
using RSG.Model.Common.Util;
using MapViewport.AddIn;

namespace Workbench.AddIn.MapDebugging.Overlays
{

    /// <summary>
    /// Overlay to display bounds for map section instance positions.
    /// </summary>
    [ExportExtension("Workbench.AddIn.MapDebugging.OverlayGroups.MapDebug", typeof(Viewport.AddIn.ViewportOverlay))]
    class MapInstancesBound : LevelDependentViewportOverlay
    {
        #region Constants
        private const String c_name = "Map Entities Bound";
        private const String c_description = "Displays bounding box for map entities based off of the current selected section(s).";
        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// A reference to the content browser interface
        /// </summary>
        [ImportExtension(ContentBrowser.AddIn.CompositionPoints.ContentBrowser, typeof(ContentBrowser.AddIn.IContentBrowser))]
        Lazy<ContentBrowser.AddIn.IContentBrowser> ContentBrowserProxy { get; set; }
        #endregion // MEF Imports

        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public MapInstancesBound()
            : base(c_name, c_description)
        {
        }
        #endregion // Constructors

        #region Controller Methods
        /// <summary>
        /// Activate overlay.
        /// </summary>
        public override void Activated()
        {
            base.Activated();
            this.ContentBrowserProxy.Value.GridSelectionChanged += GridSelectionChanged;
            this.SelectionChanged(this.ContentBrowserProxy.Value.SelectedGridItems);
        }

        /// <summary>
        /// Deactivate overlay.
        /// </summary>
        public override void Deactivated()
        {
            this.ContentBrowserProxy.Value.GridSelectionChanged -= GridSelectionChanged;
            base.Deactivated();
        }
        #endregion // Controller Methods

        #region Event Handling
        /// <summary>
        /// Get called when the items selected in the content browser change
        /// </summary>
        void GridSelectionChanged(Object sender, ContentBrowser.AddIn.GridSelectionChangedEventArgs args)
        {
            this.SelectionChanged(args.NewItem);
        }

        /// <summary>
        /// Gets called when the items selected in the content browser change and when
        /// the overlay is first shown
        /// </summary>
        private void SelectionChanged(IEnumerable<IAsset> selectedAssets)
        {
            this.Geometry.BeginUpdate();
            this.Geometry.Clear();

            // Check if any map sections are selected
            IEnumerable<IMapSection> sections = selectedAssets.OfType<IMapSection>();
            if (sections.Any())
            {
                // Make sure that the sections have the required data loaded
                CancellationTokenSource cts = new CancellationTokenSource();
                SectionTaskContext context = new SectionTaskContext(cts.Token, sections);

                ITask loadDataTask = new ActionTask("Load Data", (ctx, progress) => EnsureDataLoaded((SectionTaskContext)ctx, progress));

                TaskExecutionDialog dialog = new TaskExecutionDialog();
                TaskExecutionViewModel vm = new TaskExecutionViewModel("Generating Overlay", loadDataTask, cts, context);
                vm.AutoCloseOnCompletion = true;
                dialog.DataContext = vm;
                Nullable<bool> selectionResult = dialog.ShowDialog();
                if (false == selectionResult)
                {
                    return;
                }

                // Create the geometry for the sections
                foreach (IMapSection section in sections)
                {
                    BoundingBox2f bounds = new BoundingBox2f();
                    foreach (IEntity entity in section.ChildEntities)
                    {
                        bounds.Expand(new Vector2f(entity.Position.X, entity.Position.Y));
                    }

                    if (!bounds.IsEmpty)
                    {
                        Vector2f[] points = new Vector2f[]
                        {
                            new Vector2f(bounds.Min.X, bounds.Min.Y),
                            new Vector2f(bounds.Min.X, bounds.Max.Y),
                            new Vector2f(bounds.Max.X, bounds.Max.Y),
                            new Vector2f(bounds.Max.X, bounds.Min.Y)                    
                        };

                        Viewport2DShape newGeometry = new Viewport2DShape(etCoordSpace.World, "Section", points, Colors.Black, 1, Colors.White);
                        newGeometry.UserText = section.Name;
                        this.Geometry.Add(newGeometry);
                    }
                }
            }

            this.Geometry.EndUpdate();
        }
        #endregion // Event Handling

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="progress"></param>
        private void EnsureDataLoaded(SectionTaskContext context, IProgress<TaskProgress> progress)
        {
            float increment = 1.0f / context.Sections.Count();

            // Load all the map section data
            foreach (IMapSection section in context.Sections)
            {
                context.Token.ThrowIfCancellationRequested();

                // Update the progress information
                string message = String.Format("Loading {0}", section.Name);
                progress.Report(new TaskProgress(increment, true, message));

                // Only request that the entities for this section are loaded
                section.LoadStats(new StreamableStat[] { StreamableMapSectionStat.Entities });
            }
        }
        #endregion // Protected Functions
    } // MapInstancesBound

} // Workbench.AddIn.MapDebugging.Overlays namespace
