﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common.Map;
using RSG.Model.Common;
using System.ComponentModel;
using System.Windows;

namespace ContentBrowser.ViewModel.MapViewModels3
{
    /// <summary>
    /// 
    /// </summary>
    public class InteriorArchetypeViewModel : ObservableContainerViewModelBase<IRoom>, IWeakEventListener, IDisposable
    {
        #region Members
        /// <summary>
        /// Thread synchronisation helper object
        /// </summary>
        private object m_syncObject = new object();
        #endregion // Members

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        private IInteriorArchetype InteriorArchetype
        {
            get
            {
                return (IInteriorArchetype)Model;
            }
        }
        #endregion // Properties

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public InteriorArchetypeViewModel(IInteriorArchetype interior)
            : base(interior, interior.Rooms)
        {
            PropertyChangedEventManager.AddListener(InteriorArchetype, this, "Rooms");
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// This gets called whenever the selection state for this view model
        /// has changed
        /// </summary>
        protected override void OnSelectionChanged(Boolean oldValue, Boolean newValue)
        {
            if (IsSelected)
            {
                lock (m_syncObject)
                {
                    InteriorArchetype.RequestAllStatistics(true);
                }
            }

            base.OnSelectionChanged(oldValue, newValue);
        }

        /// <summary>
        /// This gets called whenever the expansion state for this view model
        /// has changed
        /// </summary>
        protected override void OnExpansionChanged(Boolean oldValue, Boolean newValue)
        {
            if (IsExpanded)
            {
                lock (m_syncObject)
                {
                    InteriorArchetype.RequestStatistics(new StreamableStat[] { StreamableInteriorArchetypeStat.Rooms }, true);
                }
            }

            base.OnExpansionChanged(oldValue, newValue);
        }
        #endregion // Overrides

        #region IWeakEventListener Implementation
        /// <summary>
        /// 
        /// </summary>
        public bool ReceiveWeakEvent(Type managerType, Object sender, EventArgs e)
        {
            if (Application.Current != null)
            {
                Application.Current.Dispatcher.Invoke(
                    new Action
                    (
                        delegate()
                        {
                            MonitoredCollection = InteriorArchetype.Rooms;
                            OnExpansionChanged(!IsExpanded, IsExpanded);
                            OnSelectionChanged(!IsSelected, IsSelected);
                        }
                    ),
                    System.Windows.Threading.DispatcherPriority.ContextIdle
                );
            }
            else
            {
                MonitoredCollection = InteriorArchetype.Rooms;
            }
            return true;
        }
        #endregion // IWeakEventListener

        #region IDisposable Implementation
        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            PropertyChangedEventManager.RemoveListener(InteriorArchetype, this, "Rooms");
        }
        #endregion // IDisposable Implementation
    } // InteriorArchetypeViewModel
}
