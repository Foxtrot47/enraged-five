﻿using System;

namespace MetadataEditor.AddIn.DefinitionEditor.ViewModel
{

    /// <summary>
    /// ViewModel for the hierarchical IMember series of classes.
    /// </summary>
    public class HierarchicalMemberViewModel :
        MemberViewModel
    {
        #region Properties
        /// <summary>
        /// IsExpanded property for this member being expanded.
        /// </summary>
        public bool Expanded { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="m"></param>
        public HierarchicalMemberViewModel(IMemberViewModel parent, RSG.Metadata.Parser.IMember m)
            : base(parent, m)
        {
        }
        #endregion // Constructor(s)
    }
    
} // RSG.Metadata.ViewModel namespace
