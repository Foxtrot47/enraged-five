﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Report;
using RSG.Model.Report.Reports;

namespace Workbench.AddIn.MapStatistics.Reports
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Report.AddIn.ExtensionPoints.MapStatsReport, typeof(IReport))]
    public class ShaderReport : RSG.Model.Report.Reports.Map.ShaderReport
    {
    } // ShaderReport
} // Workbench.AddIn.MapStatistics.Reports
