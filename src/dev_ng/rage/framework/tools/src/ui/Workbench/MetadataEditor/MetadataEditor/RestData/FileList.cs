﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace MetadataEditor.RestData
{
    /// <summary>
    /// A list of files
    /// </summary>
    [CollectionDataContract(Name = "Files", ItemName = "File", Namespace = "")]
    public class FileList : List<string>
    {
        public FileList() { }
        public FileList(IEnumerable<string> files) : base(files) { }
    } // FileList
} // MetadataEditor.RestData
