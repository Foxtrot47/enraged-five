﻿using MetadataEditor.AddIn.MetaEditor.ViewModel;
using RSG.Metadata.Data;

namespace MetadataEditor.AddIn.MetaEditor.GridViewModel
{
    /// <summary>
    /// The view model that wraps around a  <see cref="Vector2TunableBase"/> object. Used for
    /// the grid view.
    /// </summary>
    public class Vector2TunableViewModel : GridTunableViewModel
    {
        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="tunable"></param>
        public Vector2TunableViewModel(GridTunableViewModel parent, Vector2TunableBase m, MetaFileViewModel root)
            : base(parent, m, root)
        {
        }
        #endregion // Constructor
    } // Vector2TunableViewModel

    /// <summary>
    /// The view model that wraps around a  <see cref="Vector3TunableBase"/> object. Used for
    /// the grid view.
    /// </summary>
    public class Vector3TunableViewModel : GridTunableViewModel
    {
        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="tunable"></param>
        public Vector3TunableViewModel(GridTunableViewModel parent, Vector3TunableBase m, MetaFileViewModel root)
            : base(parent, m, root)
        {
        }
        #endregion // Constructor
    } // Vector3TunableViewModel

    /// <summary>
    /// The view model that wraps around a  <see cref="Vector4TunableBase"/> object. Used for
    /// the grid view.
    /// </summary>
    public class Vector4TunableViewModel : GridTunableViewModel
    {
        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="tunable"></param>
        public Vector4TunableViewModel(GridTunableViewModel parent, Vector4TunableBase m, MetaFileViewModel root)
            : base(parent, m, root)
        {
        }
        #endregion // Constructor
    } // Vector4TunableViewModel
} // MetadataEditor.AddIn.MetaEditor.GridViewModel
