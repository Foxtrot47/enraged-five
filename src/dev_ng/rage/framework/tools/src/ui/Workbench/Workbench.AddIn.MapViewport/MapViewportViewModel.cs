﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Collections;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using RSG.Base.Windows.Controls.WpfViewport2D;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using RSG.Base.Windows.Controls.WpfViewport2D.Controls;
using System.Windows.Input;
using RSG.Base.Logging;
using System.Windows.Media.Imaging;
using System.IO;
using RSG.Base.Math;
using System.Windows.Media;
using System.ComponentModel;
using RSG.Model.Common;
using System.ComponentModel.Composition;
using ContentBrowser.AddIn;
using WidgetEditor.AddIn;
using RSG.Base.Windows.Helpers;
using Workbench.AddIn.Services;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using Workbench.AddIn.MapViewport.MapSupport;
using RSG.Base.Configuration;
using MapViewport.AddIn;
using RSG.Model.Common.Util;
using RSG.Base.Tasks;
using RSG.Base.Windows.Dialogs;
using System.Threading;
using Workbench.AddIn.Services.Model;

namespace Workbench.AddIn.MapViewport
{
    [ExportExtension(Viewport.AddIn.CompositionPoints.MapViewport, typeof(Viewport.AddIn.IMapViewport))]
    public class MapViewportViewModel : ViewModelBase, Viewport.AddIn.IMapViewport, IPartImportsSatisfiedNotification, IDisposable, IWeakEventListener
    {
        #region Private member fields

        private MapContainer m_mapContainer;

        #endregion

        #region Constants

        private const String RAG_WARPCOORDINATES = "Debug/Warp Player x y z h vx vy vz";
        private const String RAG_WARPNOWBUTTON = "Debug/Warp now";
        private const int ToolTip_MaxWidth = 500;
        private const int ToolTip_MaxHeight = 200;
        #endregion // Constants

        #region Events

        public event Viewport.AddIn.OverlayChangedEventHandler OnOverlayChanged;

        public event ViewportClickEventHandler ViewportClicked;

        public event EventHandler BeforeSaveOverlay;

        #endregion // Events

        #region MEF Imports
        /// <summary>
        /// Game config service
        /// </summary>
        [ImportExtension(CompositionPoints.ConfigurationService, typeof(Workbench.AddIn.Services.IConfigurationService))]
        Lazy<Workbench.AddIn.Services.IConfigurationService> Config { get; set; }

        /// <summary>
        /// MEF import for login service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LoginService, typeof(Workbench.AddIn.Services.ILoginService))]
        Lazy<Workbench.AddIn.Services.ILoginService> LoginService { get; set; }

        /// <summary>
        /// MEF import for login service.
        /// </summary>
        [ImportExtension(Viewport.AddIn.CompositionPoints.MapViewportSettings, typeof(Viewport.AddIn.IMapViewportSettings))]
        Viewport.AddIn.IMapViewportSettings MapViewportSettings { get; set; }

        /// <summary>
        /// A reference to the level browser interface
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LevelBrowser, typeof(ILevelBrowser))]
        ILevelBrowser LevelBrowserProxy { get; set; }

        /// <summary>
        /// MEF import for proxy UI
        /// </summary>
        [ImportExtension(WidgetEditor.AddIn.CompositionPoints.ProxyService, typeof(IProxyService))]
        public Lazy<IProxyService> ProxyService { get; set; }

        /// <summary>
        /// MEF import for message box service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.MessageService, typeof(Workbench.AddIn.Services.IMessageService))]
        private Lazy<IMessageService> MessageService { get; set; }

        /// <summary>
        /// The data source browser
        /// </summary>
        [Workbench.AddIn.ImportExtension(Workbench.AddIn.CompositionPoints.DataSourceBrowser, typeof(IDataSourceBrowser))]
        private Lazy<IDataSourceBrowser> DataSourceBrowser { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.DataContextProvider, typeof(IDataContextProvider))]
        private Lazy<IDataContextProvider> DataContextProvider { get; set; }
        #endregion // MEF Imports

        #region Members

        private Viewport2D m_viewport;
        private Viewport2DImage m_backgroundImage = null;
        private ObservableCollection<Viewport.AddIn.ViewportOverlayGroup> m_overlayGroups;
        private Viewport.AddIn.ViewportOverlay m_selectedOverlay;
        private Viewport.AddIn.IViewportOverlayGroup m_selectedGroup;
        private ObservableCollection<Object> m_selectedOverlayGeometry;
        private ObservableCollection<Viewport2DGeometry> m_gridGeometry;
        private Control m_overlayViewportControl;
        private Control m_overlayToolTipControl;
        private RelayCommand m_warpToInGameCommand;
        private RelayCommand m_saveOverlayCommand;

        #endregion // Members

        #region Properties

        /// <summary>
        /// The image to use as the background for the imported overlays
        /// </summary>
        public Viewport2DImage BackgroundImage
        {
            get { return m_backgroundImage; }
            set
            {
                SetPropertyValue(value, () => this.BackgroundImage,
                    new PropertySetDelegate(delegate(Object newValue) { m_backgroundImage = (Viewport2DImage)newValue; }));
            }
        }

        /// <summary>
        /// The list of map names. Maps allow users to select different map backdrops to the overlays.
        /// </summary>
        public ObservableCollection<string> Maps
        {
            get { return m_maps; }
            set
            {
                SetPropertyValue(value, () => this.Maps,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_maps = (ObservableCollection<string>)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The name of the selected map.
        /// </summary>
        public string SelectedMap
        {
            get { return m_selectedMap; }
            set
            {
                SetPropertyValue(value, () => this.SelectedMap,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_selectedMap = (string)newValue;
                            OnSelectedMapChanged();
                        }
                ));
            }
        }

        private string m_selectedMap;

        private ObservableCollection<string> m_maps;

        /// <summary>
        /// This is the geometry that is used to render the grid overlay
        /// </summary>
        public ObservableCollection<Viewport2DGeometry> GridGeometry
        {
            get { return m_gridGeometry; }
            set
            {
                SetPropertyValue(value, () => this.GridGeometry,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_gridGeometry = (ObservableCollection<Viewport2DGeometry>)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// A reference to the viewport in the view
        /// </summary>
        public Viewport2D ViewportControl
        {
            get { return m_viewport; }
            set
            {
                if (m_viewport == value)
                    return;

                Viewport2D oldViewport = m_viewport;

                SetPropertyValue(value, () => ViewportControl,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_viewport = (Viewport2D)newValue;
                        }
                ));

                OnViewportChanged(oldViewport, value);
            }
        }

        /// <summary>
        /// The collection of overlay groups that have been import from external plugins
        /// </summary>
        public ObservableCollection<Viewport.AddIn.ViewportOverlayGroup> OverlayGroups
        {
            get { return m_overlayGroups; }
            set
            {
                SetPropertyValue(value, () => OverlayGroups,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_overlayGroups = (ObservableCollection<Viewport.AddIn.ViewportOverlayGroup>)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The current overlay that the viewport is currently showing
        /// </summary>
        public Viewport.AddIn.ViewportOverlay SelectedOverlay
        {
            get { return m_selectedOverlay; }
            set
            {
                SetPropertyValue(value, () => SelectedOverlay,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            Viewport.AddIn.ViewportOverlay oldOverlay = m_selectedOverlay;
                            m_selectedOverlay = (Viewport.AddIn.ViewportOverlay)newValue;
                            OnSelectedOverlayChanged(oldOverlay, m_selectedOverlay);
                        }
                ));
            }
        }

        /// <summary>
        /// The current overlay group that the viewport currently has selected
        /// </summary>
        public Viewport.AddIn.IViewportOverlayGroup SelectedGroup
        {
            get { return m_selectedGroup; }
            set
            {
                SetPropertyValue(value, m_selectedGroup, () => SelectedGroup,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            Viewport.AddIn.IViewportOverlayGroup oldGroup = m_selectedGroup;
                            m_selectedGroup = (Viewport.AddIn.IViewportOverlayGroup)newValue;
                            OnSelectedGroupChanged(oldGroup, m_selectedGroup);
                        }
                ));
            }
        }

        /// <summary>
        /// The geoemtry that is currently selected in the viewport
        /// </summary>
        public ObservableCollection<Object> SelectedOverlayGeometry
        {
            get { return m_selectedOverlayGeometry; }
            set
            {
                if (m_selectedOverlayGeometry == value)
                    return;

                Viewport.AddIn.ViewportOverlay oldOverlay = m_selectedOverlay;

                SetPropertyValue(value, () => SelectedOverlayGeometry,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_selectedOverlayGeometry = (ObservableCollection<Object>)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The control that is on the map viewport shown for the selected overlay
        /// </summary>
        public Control OverlayViewportControl
        {
            get { return m_overlayViewportControl; }
            set
            {
                SetPropertyValue(value, () => OverlayViewportControl,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_overlayViewportControl = (Control)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The control that is on the map viewport shown for the selected overlay
        /// </summary>
        public Control OverlayToolTipControl
        {
            get { return m_overlayToolTipControl; }
            set
            {
                SetPropertyValue(value, () => OverlayToolTipControl,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_overlayToolTipControl = (Control)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool OverlayEnabled
        {
            get { return m_overlayEnabled; }
            set
            {
                SetPropertyValue(value, () => OverlayEnabled,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_overlayEnabled = (bool)newValue;
                        }
                ));
            }
        }
        private bool m_overlayEnabled;

        /// <summary>
        /// 
        /// </summary>
        public String OverlayMessage
        {
            get { return m_overlayMessage; }
            set
            {
                SetPropertyValue(value, () => OverlayMessage,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_overlayMessage = (String)newValue;
                        }
                ));
            }
        }
        private String m_overlayMessage;

        /// <summary>
        /// 
        /// </summary>
        public MouseSelectionControl MouseSelectControl
        {
            get;
            set;
        }

        #region Tool Tip Bindings
        /// <summary>
        /// Maximum height of the tool tip.
        /// </summary>
        public int MaxToolTipHeight
        {
            get { return m_maxToolTipHeight; }
            set
            {
                SetPropertyValue(value, () => MaxToolTipHeight,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_maxToolTipHeight = (int)newValue;
                        }
                ));
            }
        }
        private int m_maxToolTipHeight;

        /// <summary>
        /// Maximum width of the tool tip.
        /// </summary>
        public int MaxToolTipWidth
        {
            get { return m_maxToolTipWidth; }
            set
            {
                SetPropertyValue(value, () => MaxToolTipWidth,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_maxToolTipWidth = (int)newValue;
                        }
                ));
            }
        }
        private int m_maxToolTipWidth;

        /// <summary>
        /// Size of the tool tip.
        /// </summary>
        public Rect ToolTipRect
        {
            get { return m_toolTipRect; }
            set
            {
                SetPropertyValue(value, () => ToolTipRect,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_toolTipRect = (Rect)newValue;
                        }
                ));
            }
        }
        private Rect m_toolTipRect;

        /// <summary>
        /// Whether the tool tip is showing.
        /// </summary>
        public bool IsToolTipOpen
        {
            get { return m_isToolTipOpen; }
            set
            {
                SetPropertyValue(value, () => IsToolTipOpen,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_isToolTipOpen = (bool)newValue;
                        }
                ));
            }
        }
        private bool m_isToolTipOpen;

        /// <summary>
        /// The child UI element of the popup.
        /// </summary>
        public UIElement ToolTipChild
        {
            get { return m_toolTipChild; }
            set
            {
                SetPropertyValue(value, () => ToolTipChild,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_toolTipChild = (UIElement)newValue;
                        }
                ));
            }
        }
        private UIElement m_toolTipChild;
        #endregion 

        /// <summary>
        /// Flag indicating whether we should show the grid or not
        /// </summary>
        public bool GridEnabled
        {
            get { return MapViewportSettings.DisplayGrid; }
            set
            {
                if (MapViewportSettings.DisplayGrid == value)
                    return;


                SetPropertyValue(value, () => GridEnabled,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            MapViewportSettings.DisplayGrid = (bool)newValue;
                        }
                ));

                MapViewportSettings.Apply();
            }
        }

        /// <summary>
        /// Command that gets fired off when the "Warp to in Game" button is pressed for the selected map instance
        /// </summary>
        public RelayCommand WarpToInGameCommand
        {
            get
            {
                if (m_warpToInGameCommand == null)
                {
                    m_warpToInGameCommand = new RelayCommand(param => WarpToInGame(param));
                }

                return m_warpToInGameCommand;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand SaveOverlayCommand
        {
            get
            {
                if (m_saveOverlayCommand == null)
                {
                    m_saveOverlayCommand = new RelayCommand(param => SaveOverlay(), param => CanSaveOverlay());
                }

                return m_saveOverlayCommand;
            }
        }

        #endregion // Properties

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public MapViewportViewModel()
        {
            this.OverlayGroups = new ObservableCollection<Viewport.AddIn.ViewportOverlayGroup>();
            this.SelectedOverlayGeometry = new ObservableCollection<Object>();
            this.GridGeometry = new ObservableCollection<Viewport2DGeometry>();
            this.Maps = new ObservableCollection<string>();
            this.MaxToolTipHeight = ToolTip_MaxHeight;
            this.MaxToolTipWidth = ToolTip_MaxWidth;
            this.OverlayEnabled = true;

            Properties.Settings.Default.PropertyChanged += OnViewportPropertyChanged;
        }

        #endregion // Constructor

        #region Public Functions

        /// <summary>
        /// Sets up the overlays for the map viewport
        /// </summary>
        public void SetOverlays(List<Viewport.AddIn.IViewportOverlayGroup> overlayGroups)
        {
            // Sort the overlay groups before adding them
            overlayGroups.Sort();

            this.OverlayGroups.BeginUpdate();
            this.OverlayGroups.Clear();
            this.OverlayGroups.Add(new Viewport.AddIn.ViewportOverlayGroup("None", null));
            this.OverlayGroups.AddRange(overlayGroups.Cast<Viewport.AddIn.ViewportOverlayGroup>());
            this.OverlayGroups.EndUpdate();

            // Set the selected overlay based on the settings
            this.SelectedGroup = this.OverlayGroups.FirstOrDefault(item => item.Name == MapViewportSettings.SelectedOverlayGroup);
            if (this.SelectedGroup != null)
            {
                Viewport.AddIn.ViewportOverlay toSelect = (Viewport.AddIn.ViewportOverlay)this.SelectedGroup.Overlays.FirstOrDefault(item => item.Name == MapViewportSettings.SelectedOverlay);
                if (toSelect != null && toSelect.RestoreIfOpen)
                {
                    this.SelectedOverlay = toSelect;
                }
            }
        }

        public void ClearSelectedGeometry()
        {
            IsToolTipOpen = false;
            this.MouseSelectControl.SelectedGeometry.Clear();
            this.MouseSelectControl.SelectedGeometryItems.Clear();
            foreach (var geometry in this.ViewportControl.GeometrySource)
            {
                geometry.IsSelected = false;
            }
        }


        public void SetGeometryAsSelected(Viewport2DGeometry selectedGeometry)
        {
            if (selectedGeometry.IsSelected == false)
            {
                foreach (Viewport2DGeometry geometry in this.ViewportControl.GeometrySource)
                {
                    if (geometry == selectedGeometry)
                    {
                        geometry.IsSelected = true;
                        this.MouseSelectControl.SelectedGeometry.Add(geometry);
                        this.MouseSelectControl.SelectedGeometryItems.Add(geometry.PickData);
                         
                        //Display a tool tip?

                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Displays a set of controls as part of a grid in the tool tip.
        /// </summary>
        /// <param name="controls"></param>
        /// <param name="location"></param>
        public void ShowToolTip(Point location)
        {
            OverlayToolTipControl = m_selectedOverlay.GetOverlayToolTipControl();
            if (OverlayToolTipControl == null)
            {
                HideToolTip();
                return; //No tool tip to show.
            }
			         
            ToolTipChild = OverlayToolTipControl;
            this.OverlayToolTipControl.InvalidateMeasure();

            double actualWidth = this.OverlayToolTipControl.ActualWidth;
            double actualHeight = this.OverlayToolTipControl.ActualHeight;

            if (actualHeight > ToolTip_MaxHeight)
            {
                actualHeight = this.MaxToolTipHeight;
            }
 
            if (actualWidth > ToolTip_MaxWidth)
            {
                actualWidth = this.MaxToolTipWidth;
            }

            ToolTipRect = new Rect(location, new Size(actualWidth, actualHeight));
            IsToolTipOpen = true;
        }

        /// <summary>
        /// Hides the tool tip.
        /// </summary>
        public void HideToolTip()
        {
            if (m_selectedOverlay == null)
                return;

            OverlayToolTipControl = m_selectedOverlay.GetOverlayToolTipControl();
            if (OverlayToolTipControl == null)
                return; //No tool tip to hide.

            if (IsToolTipOpen)
                IsToolTipOpen = false;
        }

        public void CenterViewportOn(Vector2f position)
        {
            HideToolTip();

            ViewportControl.WorldLookAt = new System.Windows.Point(position.X, position.Y);
        }

        public void ZoomViewportToFit(BoundingBox2f bbox)
        {
            HideToolTip();

            ViewportControl.ZoomFactor = ViewportControl.GetZoomFactorToFit(bbox) + 0.1f;
        }

        public void OnViewportClicked(object sender, ViewportClickEventArgs e)
        {
            if (ViewportClicked != null)
            {
                ViewportClicked(sender, e);
            }
        }

        public void OnMouseMoved(object sender, MouseMoveEventArgs e)
        {
            if (this.SelectedOverlay != null)
            {
                this.SelectedOverlay.OnMouseMoved(e.ViewPosition, e.WorldPosition);
            }
        }

        public void OnLostTooltipFocus(object sender, EventArgs e)
        {
            HideToolTip();
        }

        #endregion // Public Functions

        #region Property Changed Handlers

        private void LevelChanged(Object sender, LevelChangedEventArgs args)
        {
            HideToolTip();

            OnSelectedLevelChanged(args.OldItem, args.NewItem);
        }

        private void DataContextChanged(Object sender, DataContextChangedEventArgs args)
        {
            HideToolTip();

            if (args.OldItem.DataSource != args.NewItem.DataSource ||
                args.OldItem.Build != args.NewItem.Build)
            {
                if (SelectedOverlay != null && (!args.NewItem.Level.Initialised && (SelectedOverlay is LevelDependentViewportOverlay)))
                {
                    // Disable the selected overlay until the level is initialised again.
                    SelectedOverlay.Deactivated();
                }
            }

            if (args.OldItem.Level != args.NewItem.Level)
            {
                OnSelectedLevelChanged(args.OldItem.Level, args.NewItem.Level);
            }
        }

        /// <summary>
        /// Handle map changes.
        /// </summary>
        private void OnSelectedMapChanged()
        {
            if (m_mapContainer == null)
            {
                return;
            }

            AlternativeMap selectedMap = m_mapContainer.DefaultMap;
            if (SelectedMap != null && m_mapContainer.Maps.ContainsKey(SelectedMap))
            {
                selectedMap = m_mapContainer.Maps[SelectedMap];
            }

            RSG.Base.Math.Vector2f size = new RSG.Base.Math.Vector2f(966.0f, 627.0f);
            RSG.Base.Math.Vector2f pos = new RSG.Base.Math.Vector2f(-483.0f, 627.0f);
            if (selectedMap.ImageBounds != null)
            {
                size = new RSG.Base.Math.Vector2f(selectedMap.ImageBounds.Max.X - selectedMap.ImageBounds.Min.X, selectedMap.ImageBounds.Max.Y - selectedMap.ImageBounds.Min.Y);
                pos = new RSG.Base.Math.Vector2f(selectedMap.ImageBounds.Min.X, selectedMap.ImageBounds.Max.Y);
            }

            BackgroundImage = new Viewport2DImage(etCoordSpace.World, String.Format("Image for level - {0}", selectedMap.Name), selectedMap.Image, pos, size);
        }

        private void OnSelectedLevelChanged(ILevel oldLevel, ILevel newLevel)
        {
            // Get rid of the current background image
            this.BackgroundImage = null;

            if (oldLevel != null)
            {
                PropertyChangedEventManager.RemoveListener(oldLevel, this, "Initialised");
            }

            if (newLevel != null && newLevel.ImageBounds != null && this.ViewportControl != null)
            {
                PropertyChangedEventManager.AddListener(newLevel, this, "Initialised");

                try
                {
                    SelectedMap = "";
                    Maps.Clear();

                    m_mapContainer = new MapContainer(newLevel, Path.Combine(Config.Value.Config.ToolsRoot, "etc", "config", "altmap"));
                    Maps.AddRange(m_mapContainer.GetMapNames());
                    SelectedMap = m_mapContainer.DefaultMap == null ? String.Empty : m_mapContainer.DefaultMap.Name;

                    RSG.Base.Math.Vector2f size = new RSG.Base.Math.Vector2f(966.0f, 627.0f);
                    RSG.Base.Math.Vector2f pos = new RSG.Base.Math.Vector2f(-483.0f, 627.0f);
                    if (newLevel.ImageBounds != null && !newLevel.ImageBounds.IsEmpty)
                    {
                        size = new RSG.Base.Math.Vector2f(newLevel.ImageBounds.Max.X - newLevel.ImageBounds.Min.X, newLevel.ImageBounds.Max.Y - newLevel.ImageBounds.Min.Y);
                        pos = new RSG.Base.Math.Vector2f(newLevel.ImageBounds.Min.X, newLevel.ImageBounds.Max.Y);
                    }
                    
                    BackgroundImage = new Viewport2DImage(etCoordSpace.World, String.Format("Image for level - {0}", newLevel.Name), newLevel.Image, pos, size);
                    BackgroundImage.PickData = newLevel;
                    BackgroundImage.UserText = String.Empty;

                    if (this.ViewportControl != null)
                    {
                        float originX = 0.0f;
                        float originY = 0.0f;
                        if (newLevel.ImageBounds != null)
                        {
                            originX = newLevel.ImageBounds.Min.X + ((newLevel.ImageBounds.Max.X - newLevel.ImageBounds.Min.X) / 2.0f);
                            originY = newLevel.ImageBounds.Min.Y + ((newLevel.ImageBounds.Max.Y - newLevel.ImageBounds.Min.Y) / 2.0f);
                        }
                        this.ViewportControl.WorldOrigin = new System.Windows.Point(originX, originY);
                        this.ViewportControl.WorldLookAt = new System.Windows.Point(originX, originY);
                        this.ViewportControl.ZoomFactor = this.ViewportControl.GetZoomFactorToFit() + 0.1f;
                    }
                    
                    // Update the grid overlay
                    UpdateGrid(newLevel);
                }
                catch
                {
                }
            }
        }

        private void OnViewportChanged(Viewport2D oldViewport, Viewport2D newViewport)
        {
            HideToolTip();

            this.OnSelectedLevelChanged(null, this.LevelBrowserProxy.SelectedLevel);
        }

        private void OnSelectedOverlayChanged(Viewport.AddIn.ViewportOverlay oldValue, Viewport.AddIn.ViewportOverlay newValue)
        {
            if (oldValue != null && oldValue.IsCurrentlyActive)
            {
                try
                {
                    oldValue.Deactivated();
                }
                catch (System.Exception ex)
                {
                    Log.Log__Exception(ex, "Exception occurred while deactivating the '{0}' overlay.", oldValue.Name);
                }
            }
            if (newValue != null)
            {
                // Can the new overlay be activated?
                if (newValue.DataSourceModes[DataSourceBrowser.Value.SelectedSource])
                {
                    if (this.LevelBrowserProxy.SelectedLevel.Initialised || !(newValue is LevelDependentViewportOverlay))
                    {
                        OverlayEnabled = true;
                        OverlayMessage = "";
                    }
                    else
                    {
                        OverlayEnabled = false;
                        OverlayMessage = "Level has not been initialised yet.  This overlay will auto refresh once the level has been initialised.";
                    }
                }
                else
                {
                    OverlayEnabled = false;
                    OverlayMessage = "Overlay is not available in the selected data source mode.";
                }
            }
            else
            {
                OverlayEnabled = true;
                OverlayMessage = "Please select an overlay.";
            }

            if (OverlayEnabled && newValue != null)
            {
                // Load data required by the overlay.
                bool dataLoaded = true;

                if (newValue.RequiredStats.Any())
                {
                    /*
                    CancellationTokenSource cts = new CancellationTokenSource();
                    LevelTaskContext context = new LevelTaskContext(cts.Token, LevelBrowserProxy.SelectedLevel);

                    ActionTask dataLoadTask = new ActionTask("Load Data", (ctx, progress) => EnsureDataLoaded((LevelTaskContext)ctx, progress));
                    
                    TaskExecutionDialog dialog = new TaskExecutionDialog();
                    TaskExecutionViewModel vm = new TaskExecutionViewModel("Generating Overlay", dataLoadTask, cts, context);
                    vm.AutoCloseOnCompletion = true;
                    dialog.DataContext = vm;
                    Nullable<bool> selectionResult = dialog.ShowDialog();
                    if (false == selectionResult)
                    {
                        dataLoaded = false;
                    }
                    */
                }

                // Make sure the data was successfully loaded.
                if (dataLoaded)
                {
                    try
                    {
                        newValue.Activated();
                    }
                    catch (System.Exception ex)
                    {
                        Log.Log__Exception(ex, "Exception occurred while activating the '{0}' overlay.", oldValue.Name);
                    }

                    // Update the viewport overlay control
                    OverlayViewportControl = newValue.GetViewportControl();
                    if (OverlayViewportControl != null)
                    {
                        OverlayViewportControl.DataContext = newValue;
                    }

                    if (this.ViewportControl != null)
                    {
                        System.Windows.Data.BindingOperations.GetMultiBindingExpression(this.ViewportControl, Viewport2D.GeometrySourceProperty).UpdateTarget();
                    }

                    if (this.OnOverlayChanged != null)
                    {
                        this.OnOverlayChanged(this, new Viewport.AddIn.OverlayChangedEventArgs(oldValue, newValue));
                    }
                }
            }
            else
            {
                OverlayViewportControl = null;

                if (this.OnOverlayChanged != null)
                {
                    this.OnOverlayChanged(this, new Viewport.AddIn.OverlayChangedEventArgs(oldValue, null));
                }
            }

            MapViewportSettings.SelectedOverlay = (newValue == null ? "" : newValue.Name);
            MapViewportSettings.Apply();
        }

        private void OnSelectedGroupChanged(Viewport.AddIn.IViewportOverlayGroup oldValue, Viewport.AddIn.IViewportOverlayGroup newValue)
        {
            if (oldValue != null)
            {
                oldValue.Deactivated();
            }
            if (newValue != null)
            {
                newValue.Activated();
            }

            MapViewportSettings.SelectedOverlayGroup = (newValue == null ? "" : newValue.Name);
            MapViewportSettings.Apply();
        }

        private void OnViewportPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "DisplayGrid")
            {
                OnPropertyChanged("GridEnabled");
            }

            if (LevelBrowserProxy != null && e.PropertyName == "DisplayGrid" || e.PropertyName == "MajorLineSpacing" || e.PropertyName == "MinorLineSpacing")
            {
                UpdateGrid(LevelBrowserProxy.SelectedLevel);
            }
        }

        #endregion // Property Changed Handlers

        #region Private Methods
        /// <summary>
        /// Helper function for refreshing the grid lines
        /// </summary>
        /// <param name="level"></param>
        private void UpdateGrid(ILevel level)
        {
            GridGeometry.BeginUpdate();
            GridGeometry.Clear();

            if (level != null && level.ImageBounds != null && MapViewportSettings.DisplayGrid)
            {
                float minX = level.ImageBounds.Min.X;
                float maxX = level.ImageBounds.Max.X;
                float minY = level.ImageBounds.Min.Y;
                float maxY = level.ImageBounds.Max.Y;

                // Draw the minor lines first so major lines overlap them
                GenerateMinorLines(minX, maxX, minY, maxY);
                GenerateMajorLines(minX, maxX, minY, maxY);
            }

            GridGeometry.EndUpdate();
        }

        /// <summary>
        /// Generates the major grid lines
        /// </summary>
        /// <param name="minX"></param>
        /// <param name="maxX"></param>
        /// <param name="minY"></param>
        /// <param name="maxY"></param>
        private void GenerateMajorLines(float minX, float maxX, float minY, float maxY)
        {
            Color color = Colors.Gray;

            for (float x = 0.0f; x <= maxX; x += (float)MapViewportSettings.MajorLineSpacing)
            {
                Vector2f[] points = new Vector2f[] { new Vector2f(x, minY), new Vector2f(x, maxY) };
                GridGeometry.Add(new Viewport2DMultiLine(etCoordSpace.World, "", points, false, color, 1));
            }

            for (float x = -(float)MapViewportSettings.MajorLineSpacing; x >= minX; x -= (float)MapViewportSettings.MajorLineSpacing)
            {
                Vector2f[] points = new Vector2f[] { new Vector2f(x, minY), new Vector2f(x, maxY) };
                GridGeometry.Add(new Viewport2DMultiLine(etCoordSpace.World, "", points, false, color, 1));
            }

            for (float y = 0; y <= maxY; y += (float)MapViewportSettings.MajorLineSpacing)
            {
                Vector2f[] points = new Vector2f[] { new Vector2f(minX, y), new Vector2f(maxX, y) };
                GridGeometry.Add(new Viewport2DMultiLine(etCoordSpace.World, "", points, false, color, 1));
            }

            for (float y = -(float)MapViewportSettings.MajorLineSpacing; y >= minY; y -= (float)MapViewportSettings.MajorLineSpacing)
            {
                Vector2f[] points = new Vector2f[] { new Vector2f(minX, y), new Vector2f(maxX, y) };
                GridGeometry.Add(new Viewport2DMultiLine(etCoordSpace.World, "", points, false, color, 1));
            }
        }
        
        /// <summary>
        /// Generates the minor grid lines
        /// </summary>
        /// <param name="minX"></param>
        /// <param name="maxX"></param>
        /// <param name="minY"></param>
        /// <param name="maxY"></param>
        private void GenerateMinorLines(float minX, float maxX, float minY, float maxY)
        {
            Color color = Colors.LightGray;

            for (float x = 0.0f; x <= maxX; x += (float)MapViewportSettings.MinorLineSpacing)
            {
                if (x % (float)MapViewportSettings.MajorLineSpacing != 0)
                {
                    Vector2f[] points = new Vector2f[] { new Vector2f(x, minY), new Vector2f(x, maxY) };
                    GridGeometry.Add(new Viewport2DMultiLine(etCoordSpace.World, "", points, false, color, 1));
                }
            }

            for (float x = -(float)MapViewportSettings.MinorLineSpacing; x >= minX; x -= (float)MapViewportSettings.MinorLineSpacing)
            {
                if (x % (float)MapViewportSettings.MajorLineSpacing != 0)
                {
                    Vector2f[] points = new Vector2f[] { new Vector2f(x, minY), new Vector2f(x, maxY) };
                    GridGeometry.Add(new Viewport2DMultiLine(etCoordSpace.World, "", points, false, color, 1));
                }
            }

            for (float y = 0; y <= maxY; y += (float)MapViewportSettings.MinorLineSpacing)
            {
                if (y % (float)MapViewportSettings.MajorLineSpacing != 0)
                {
                    Vector2f[] points = new Vector2f[] { new Vector2f(minX, y), new Vector2f(maxX, y) };
                    GridGeometry.Add(new Viewport2DMultiLine(etCoordSpace.World, "", points, false, color, 1));
                }
            }

            for (float y = -(float)MapViewportSettings.MinorLineSpacing; y >= minY; y -= (float)MapViewportSettings.MinorLineSpacing)
            {
                if (y % (float)MapViewportSettings.MajorLineSpacing != 0)
                {
                    Vector2f[] points = new Vector2f[] { new Vector2f(minX, y), new Vector2f(maxX, y) };
                    GridGeometry.Add(new Viewport2DMultiLine(etCoordSpace.World, "", points, false, color, 1));
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="instance"></param>
        private void WarpToInGame(object param)
        {
            Point mouseViewPoint = (Point)param;
            Point mouseWorldPoint = ViewportControl.ViewToWorld(mouseViewPoint);

            // Check whether the RAG proxy is connected
            if (!ProxyService.Value.IsGameConnected)
            {
                MessageService.Value.Show("RAG is not connected. Is the game running?");
            }
            else
            {
                ProxyService.Value.Console.WriteStringWidget(RAG_WARPCOORDINATES, String.Format("{0} {1}", (float)mouseWorldPoint.X, (float)mouseWorldPoint.Y));
                ProxyService.Value.Console.PressWidgetButton(RAG_WARPNOWBUTTON);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void SaveOverlay()
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.Filter = "PNG|*.png";
            Nullable<bool> result = dlg.ShowDialog();
            if (false == result)
            {
                return;
            }

            if (BeforeSaveOverlay != null)
            {
                BeforeSaveOverlay(this, EventArgs.Empty);
            }

            BoundingBox2f imageBounds = LevelBrowserProxy.SelectedLevel.ImageBounds;
            int width = (int)((imageBounds.Max.X - imageBounds.Min.X) * 0.5);
            int height = (int)((imageBounds.Max.Y - imageBounds.Min.Y) * 0.5);

            ViewportControl.SaveGeometryToImage(dlg.FileName, LevelBrowserProxy.SelectedLevel.ImageBounds);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool CanSaveOverlay()
        {
            return (SelectedOverlay != null && LevelBrowserProxy.SelectedLevel != null && LevelBrowserProxy.SelectedLevel.ImageBounds != null);
        }
        #endregion // Private Methods

        #region IPartImportsSatisfiedNotification Methods
        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {
            // Add the callback for when the data context changes.
            DataContextProvider.Value.DataContextChanged += DataContextChanged;

            // Load in the settings.
            MapViewportSettings.Load();

            // Force an update of the grid.
            UpdateGrid(LevelBrowserProxy.SelectedLevel);
        }
        #endregion // IPartImportsSatisfiedNotification Methods

        #region IWeakEventListener Implementation
        /// <summary>
        /// 
        /// </summary>
        public bool ReceiveWeakEvent(Type managerType, Object sender, EventArgs e)
        {
            if (managerType == typeof(PropertyChangedEventManager))
            {
                PropertyChangedEventArgs eventArgs = e as PropertyChangedEventArgs;
                if (eventArgs.PropertyName == "Initialised")
                {

                    if (Application.Current != null)
                    {
                        Application.Current.Dispatcher.Invoke(
                            new Action
                            (
                                delegate()
                                {
                                    if (SelectedOverlay != null && !SelectedOverlay.IsCurrentlyActive)
                                    {
                                        OnSelectedOverlayChanged(null, SelectedOverlay);
                                    }
                                }
                            ),
                            System.Windows.Threading.DispatcherPriority.ContextIdle
                        );
                    }
                    else
                    {
                        if (SelectedOverlay != null && !SelectedOverlay.IsCurrentlyActive)
                        {
                            OnSelectedOverlayChanged(null, SelectedOverlay);
                        }
                    }
                }
            }

            return true;
        }
        #endregion // IWeakEventListener

        #region IDisposable Implementation
        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            if (SelectedOverlay != null)
            {
                SelectedOverlay.Deactivated();
            }
        }
        #endregion // IDisposable Implementation
    } // MapViewportViewModel
} // Workbench.AddIn.MapViewport
