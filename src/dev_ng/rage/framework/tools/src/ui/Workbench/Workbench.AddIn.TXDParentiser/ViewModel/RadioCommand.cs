﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Workbench.AddIn.TXDParentiser.ViewModel
{
    public class RadioCommand
    {
        #region Fields

        private readonly Action<Object> m_onSelected;
        private readonly Action<Object> m_onUnSelected;

        #endregion // Fields

        #region Constructors

        /// <summary>
        /// Creates a new command that can always execute.
        /// </summary>
        public RadioCommand(Action<Object> onSelected)
            : this(onSelected, null)
        {
        }

        /// <summary>
        /// Creates a new command.
        /// </summary>
        public RadioCommand(Action<Object> onSelected, Action<Object> onUnSelected)
        {
            m_onSelected = onSelected;
            m_onUnSelected = onUnSelected;
        }

        #endregion // Constructors

        #region Events

        /// <summary>
        /// 
        /// </summary>
        public void OnSelected(Object parameter)
        {
            if (m_onSelected != null)
                m_onSelected(parameter);
        }

        /// <summary>
        /// 
        /// </summary>
        public void Execute(Object parameter)
        {
            if (m_onUnSelected != null)
                m_onUnSelected(parameter);
        }

        #endregion // Events
    } // RadioCommand
} // Workbench.AddIn.TXDParentiser.ViewModel
