﻿using System;
using System.ComponentModel;
using RSG.Base.Tasks;
using System.Threading;

namespace Workbench.AddIn.Services
{
    /// <summary>
    /// Service for showing a generic progress dialog box
    /// </summary>
    public interface IProgressService
    {
        #region Don't use these anymore...
        [Obsolete]
        string Title
        {
            get;
            set;
        }

        [Obsolete]
        string Message
        {
            get;
            set;
        }

        [Obsolete]
        string Status
        {
            get;
            set;
        }

        [Obsolete]
        object WorkerArguments
        {
            get;
            set;
        }

        [Obsolete]
        bool Show(BackgroundWorker worker);
        [Obsolete]
        bool Show(BackgroundWorker worker, string title, string message);
        [Obsolete]
        bool Show(BackgroundWorker worker, string title, string message, bool indeterminate);
        [Obsolete]
        bool Show(BackgroundWorker worker, object arguments, string title, string message, bool indeterminate);
        #endregion //

        /// <summary>
        /// Shows the task execution dialog and executes the task on a seperate thread.
        /// </summary>
        /// <param name="task">Task to execute.</param>
        /// <param name="context">Task context.</param>
        /// <param name="cts">Task cancellation token source.</param>
        /// <param name="title">Title of the window that will be displayed.</param>
        /// <returns>Whether the task successfully completed.</returns>
        bool Show(ITask task, ITaskContext context, CancellationTokenSource cts, string title);
    } // IProgressService
}
