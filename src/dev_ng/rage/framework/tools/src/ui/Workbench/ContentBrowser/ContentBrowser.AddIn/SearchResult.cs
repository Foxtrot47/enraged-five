﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn.Services;
using ContentBrowser.AddIn;
using RSG.Model.Common;
using Workbench.AddIn.UI.Layout;

namespace ContentBrowser.AddIn
{
    /// <summary>
    /// 
    /// </summary>
    public class SearchResult : ISearchResult
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public object Data
        { 
            get { return this.Asset; }
        }

        /// <summary>
        /// Represents whether the user can jump to the search result
        /// to see it in the view.
        /// </summary>
        public bool CanJumpTo 
        { 
            get { return true; }
        }

        /// <summary>
        /// Represents what to show the user in the search results
        /// window for this result.
        /// </summary>
        public string DisplayText
        {
            get;
            private set;
        }

        /// <summary>
        /// The browser that the attached
        /// asset was found in.
        /// </summary>
        public BrowserBase Browser
        {
            get;
            private set;
        }

        /// <summary>
        /// The asset model attached to this result
        /// </summary>
        public IAsset Asset
        {
            get;
            private set;
        }

        /// <summary>
        /// A reference to the content
        /// the search result was found in.
        /// </summary>
        public IContentBase Content
        {
            get;
            set;
        }
        #endregion

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="displayText"></param>
        public SearchResult(string displayText, BrowserBase browser, IAsset asset)
        {
            this.DisplayText = displayText;
            this.Browser = browser;
            this.Asset = asset;
        }
        #endregion
    } // SearchResult
} // LevelBrowser
