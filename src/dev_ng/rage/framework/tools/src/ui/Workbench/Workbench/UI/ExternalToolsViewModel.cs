﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Text;
using System.Xml;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using RSG.Base.Logging;
using Workbench.Model;

namespace Workbench.UI
{

    /// <summary>
    /// 
    /// </summary>
    internal class ExternalToolCollectionViewModel : ViewModelBase
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<ExternalToolViewModel> Tools
        {
            get { return m_Tools; }
            set
            {
                SetPropertyValue(value, () => this.Tools,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_Tools = (ObservableCollection<ExternalToolViewModel>)newValue;
                        }
                ));
            }
        }
        private ObservableCollection<ExternalToolViewModel> m_Tools;

        /// <summary>
        /// 
        /// </summary>
        public ExternalToolViewModel SelectedTool
        {
            get { return m_SelectedTool; }
            set
            {
                SetPropertyValue(value, () => this.SelectedTool,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_SelectedTool = (ExternalToolViewModel)newValue;
                        }
                ));

                if (null == SelectedTool)
                {
                    this.CanMoveUp = false;
                    this.CanMoveDown = false;
                }
                else
                {
                    int index = this.Tools.IndexOf(this.SelectedTool);
                    this.CanMoveUp = (index > 0);
                    this.CanMoveDown = (index < (this.Tools.Count - 1));
                }
            }
        }
        private ExternalToolViewModel m_SelectedTool;

        /// <summary>
        /// Flag to determine if we can move selected tool up.
        /// </summary>
        public bool CanMoveUp
        {
            get { return m_bCanMoveUp; }
            set
            {
                SetPropertyValue(value, () => this.CanMoveUp,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_bCanMoveUp = (bool)newValue;
                        }
                ));
            
            }
        }
        private bool m_bCanMoveUp;

        /// <summary>
        /// Flag to determine if we can move selected tool down.
        /// </summary>
        public bool CanMoveDown
        {
            get { return m_bCanMoveDown; }
            set
            {
                SetPropertyValue(value, () => this.CanMoveDown,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_bCanMoveDown = (bool)newValue;
                        }
                ));

            }
        }
        private bool m_bCanMoveDown;
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ExternalToolCollectionViewModel()
        {
            this.Tools = new ObservableCollection<ExternalToolViewModel>();
            
            // Read from application settings.
            if (null != Properties.Settings.Default.ExternalTools)
            {
                try
                {
                    foreach (String input in Properties.Settings.Default.ExternalTools)
                    {
                        ExternalTool tool = new ExternalTool(input);
                        this.Tools.Add(new ExternalToolViewModel(tool));
                    }
                }
                catch (Exception ex)
                {
                    Log.Log__Exception(ex, "Invalid external tool configuration. Resetting.");
                    Properties.Settings.Default.ExternalTools.Clear();
                    Properties.Settings.Default.Save();
                }
            }
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        public void Save()
        {
            if (null == Properties.Settings.Default.ExternalTools)
                Properties.Settings.Default.ExternalTools = new StringCollection();
            StringCollection toolsSetting = Properties.Settings.Default.ExternalTools;
            toolsSetting.Clear();
            XmlDocument xmlDoc = new XmlDocument();
            foreach (ExternalToolViewModel vm in this.Tools)
            {
                toolsSetting.Add(vm.Model.Serialise());
            }
            Properties.Settings.Default.Save();
        }
        #endregion // Controller Methods
    }

    /// <summary>
    /// 
    /// </summary>
    internal class ExternalToolViewModel : ViewModelBase
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Model reference.
        /// </summary>
        public ExternalTool Model
        {
            get { return m_Model; }
            set
            {
                SetPropertyValue(value, () => this.Model,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_Model = (ExternalTool)newValue;
                        }
                ));
            }
        }
        private ExternalTool m_Model;
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="model"></param>
        public ExternalToolViewModel(ExternalTool model)
        {
            this.Model = model;
        }
        #endregion // Constructor(s)
    }

} // Workbench.UI namespace
