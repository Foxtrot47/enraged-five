﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Workbench.Services
{
    /// <summary>
    /// Interaction logic for SaveContentWindow.xaml
    /// </summary>
    public partial class SaveContentWindow : Window
    {
        public MessageBoxResult Result
        {
            get;
            set;
        }

        public List<String> Files
        {
            get;
            set;
        }

        public SaveContentWindow()
        {
            InitializeComponent();
        }

        public SaveContentWindow(List<String> files)
        {
            this.Files = files;
            this.Result = MessageBoxResult.Cancel;
            InitializeComponent();
        }
        
        private void OnYes(Object sender, RoutedEventArgs e)
        {
            Result = MessageBoxResult.Yes;
            this.DialogResult = false;
        }

        private void OnNo(Object sender, RoutedEventArgs e)
        {
            Result = MessageBoxResult.No;
            this.DialogResult = false;
        }

        private void OnCancel(Object sender, RoutedEventArgs e)
        {
            Result = MessageBoxResult.Cancel;
            this.DialogResult = false;
        }
    }
}
