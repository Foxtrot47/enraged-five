﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Windows;
using System.Windows.Controls;
using RSG.Base.Collections;
using RSG.Base.Editor;
using RSG.Base.Logging;
using MetadataEditor.AddIn.DefinitionEditor.ViewModel;
using RSG.Metadata.Model;

namespace MetadataEditor.UI.View
{
    /// <summary>
    /// Interaction logic for DefinitionWindow.xaml
    /// </summary>
    public partial class DefinitionWindow : Window    
    {        
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public StructureDictionaryViewModel Structures
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public StructureViewModel SelectedStructure
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public DefinitionWindow(String metadataDir, MetadataEditor.AddIn.IStructureDictionary structures)
        {
            InitializeComponent();
            this.Structures = new StructureDictionaryViewModel(structures.Structures);
            if (this.Structures.RootNamespaces.Count > 0)
                this.Structures.RootNamespaces[0].IsExpanded = true;
            this.DataContext = this.Structures;
        }
        #endregion // Constructor(s)

        #region Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TreeView_SelectedItemChanged(Object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            StructureViewModel vm =
                ((sender as TreeView).SelectedItem as StructureViewModel);
            this.SelectedStructure = vm;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(Object sender, RoutedEventArgs e)
        {
            if (this.SelectedStructure is StructureViewModel)
            {
                this.DialogResult = true;
                Close();
            }
        }
        #endregion // Event Handlers
    }

} // MetadataEditor.UI.View namespace
