﻿using RSG.Model.Report;

namespace Workbench.AddIn.WeaponStatistics.Reports
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(ExtensionPoints.Report, typeof(IReport))]
    public class WeaponStatsReport : RSG.Model.Report.Reports.Weapons.WeaponStatsReport
    {
    } // WeaponCSVExportReport
}
