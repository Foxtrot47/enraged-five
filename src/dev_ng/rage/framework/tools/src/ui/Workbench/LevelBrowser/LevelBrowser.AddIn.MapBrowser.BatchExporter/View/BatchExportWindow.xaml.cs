﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Documents;
using LevelBrowser.AddIn.MapBrowser.BatchExporter.ViewModel;
using Workbench.AddIn.UI.Layout;
using RSG.Model.Map3;
using System.Windows.Input;

namespace LevelBrowser.AddIn.MapBrowser.BatchExporter.View
{
    /// <summary>
    /// Interaction logic for BatchExportWindow.xaml
    /// </summary>
    public partial class BatchExportWindow : ToolWindowBase<BatchExportWindowViewModel>
    {
        #region Public constants

        public static readonly Guid GUID = new Guid("23CE7441-3D19-4d6e-9567-1A2A3211D60B");

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        public BatchExportWindow()
            : base("BATCH_EXPORT_WINDOW",
                  new BatchExportWindowViewModel())
        {
            InitializeComponent();
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="viewModel">View model that provides the data context.</param>
        public BatchExportWindow(BatchExportWindowViewModel viewModel)
            : base("BATCH_EXPORT_WINDOW", 
                  viewModel)
        {
            Name = "Batch_Export";
            InitializeComponent();

            ID = GUID;
            SaveModel = viewModel;
        }

        #endregion

        #region Protected drag and drop handlers

        /// <summary>
        /// Handle the OnDragEnter event.
        /// </summary>
        /// <param name="e">Drag and drop event arguments.</param>
        protected override void OnDragEnter(DragEventArgs e)
        {
            HandleDragEvent(this, e);
        }

        /// <summary>
        /// Handle the OnDragOver event.
        /// </summary>
        /// <param name="e">Drag and drop event arguments.</param>
        protected override void OnDragOver(DragEventArgs e)
        {
            HandleDragEvent(this, e);
        }

        /// <summary>
        /// Handles the OnDrop event.
        /// </summary>
        /// <param name="e">Drag and drop event arguments.</param>
        protected override void OnDrop(DragEventArgs e)
        {
            string[] formats = e.Data.GetFormats();
            if (formats.Length == 1)
            {
                List<MapSection> sections = new List<MapSection>();
                object payload = e.Data.GetData(formats[0]);
                if (payload.GetType().GetGenericArguments().Length > 0)
                {
                    sections.AddRange(payload as List<MapSection>);
                }
                else
                {
                    sections.Add(payload as MapSection);
                }

                ViewModel.AddSections(sections);
            }

            CommandManager.InvalidateRequerySuggested();
        }

        #endregion

        #region Private helper methods


        /// <summary>
        /// Event handler for the ScrollViewer drag and drop events.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void HandleDragEvent(object sender, DragEventArgs e)
        {
            if (!IsValidData(e))
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// Determines what is / isn't valid data during a drag and drop operation.
        /// </summary>
        /// <param name="e">Drag event arguments.</param>
        /// <returns>True if the data contained in the event arguments is valid.</returns>
        private bool IsValidData(DragEventArgs e)
        {
            string[] formats = e.Data.GetFormats();
            if (formats.Length != 1)
            {
                e.Effects = DragDropEffects.None;
                return false;
            }

            object payload = e.Data.GetData(formats[0]);
            if (payload.GetType() == typeof(MapSection) || payload.GetType() == typeof(List<MapSection>))
            {
                e.Effects = DragDropEffects.Copy | DragDropEffects.Move;
                return true;
            }

            e.Effects = DragDropEffects.None;
            return false;
        }

        #endregion
    }
}
