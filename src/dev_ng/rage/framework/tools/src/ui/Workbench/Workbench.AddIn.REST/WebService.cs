﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Web;
using RSG.Base.Logging;
using WidgetEditor.AddIn;
using Workbench.AddIn.Services;

namespace Workbench.AddIn.REST
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Workbench.AddIn.CompositionPoints.WebService, typeof(IWebService))]
    class WebService : IWebService, IServiceBehavior, IDispatchMessageInspector
    {
        #region Static Members
        private static readonly object c_httpAccessDenied = new object();
        private static readonly object c_accessDenied = new object();
        #endregion // Static Members

        #region MEF Imports
        /// <summary>
        /// MEF import for proxy
        /// </summary>
        [ImportExtension(WidgetEditor.AddIn.CompositionPoints.ProxyService, typeof(IProxyService))]
        public Lazy<IProxyService> ProxyService { get; set; }
        
        /// <summary>
        /// MEF Import for all the rest services to run
        /// </summary>
        [ImportManyExtension(ExtensionPoints.RESTService, typeof(IRestService))]
        private IEnumerable<IRestService> RESTServices { get; set; }
        #endregion // MEF Imports

        #region Member Data
        /// <summary>
        /// List of the web service hosts that have been started up
        /// </summary>
        private List<WebServiceHost> m_webServiceHosts = new List<WebServiceHost>();
        #endregion // Member Data

        #region IWebService Implementation
        /// <summary>
        /// Starts the web service
        /// </summary>
        public void Start()
        {
            foreach (IRestService service in RESTServices)
            {
                WebServiceHost serviceHost = null;

                try
                {
                    Uri endPointUri = new Uri(String.Format("http://localhost:12357/{0}", service.EndPointName));
                    serviceHost = new WebServiceHost(service, endPointUri);
                    serviceHost.Description.Behaviors.Add(this);
                    serviceHost.Open();
                    m_webServiceHosts.Add(serviceHost);
                }
                catch (System.Exception ex)
                {
                    Log.Log__Exception(ex, "Unexpected exception occurred while attempting to open the " + serviceHost.BaseAddresses + " rest service.");
                    serviceHost.Abort();
                }
            }
        }

        /// <summary>
        /// Stops the web service
        /// </summary>
        public void Stop()
        {
            foreach (WebServiceHost serviceHost in m_webServiceHosts)
            {
                try
                {
                    serviceHost.Close();
                }
                catch (System.Exception ex)
                {
                    Log.Log__Exception(ex, "Unexpected exception occurred while attempting to close the " + serviceHost.BaseAddresses + " rest service.");
                    serviceHost.Abort();
                }
            }
        }
        #endregion // IWebService Implementation

        #region IServiceBehaviour Implementation
        /// <summary>
        /// Provides the ability to pass custom data to binding elements to support the contract implementation.
        /// </summary>
        /// <param name="serviceDescription">The service description of the service.</param>
        /// <param name="serviceHostBase">The host of the service.</param>
        /// <param name="endpoints">The service endpoints.</param>
        /// <param name="bindingParameters">Custom objects to which binding elements have access.</param>
        public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters)
        {
        }

        /// <summary>
        /// Provides the ability to change run-time property values or insert custom
        /// extension objects such as error handlers, message or parameter interceptors,
        /// security extensions, and other custom extension objects.
        /// </summary>
        /// <param name="serviceDescription">The service description.</param>
        /// <param name="serviceHostBase">The host that is currently being built.</param>
        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            foreach (ChannelDispatcher channelDispatcher in serviceHostBase.ChannelDispatchers)
            {
                foreach (EndpointDispatcher endpointDispatcher in channelDispatcher.Endpoints)
                {
                    endpointDispatcher.DispatchRuntime.MessageInspectors.Add(this);
                }
            }  
        }
        
        /// <summary>
        /// Provides the ability to inspect the service host and the service description
        /// to confirm that the service can run successfully.
        /// </summary>
        /// <param name="serviceDescription">The service description.</param>
        /// <param name="serviceHostBase">The service host that is currently being constructed.</param>
        public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
        }
        #endregion // IServiceBehaviour Implementation

        #region IDispatchMessageInspector Implementation
        /// <summary>
        /// Called after an inbound message has been received but before the message
        /// is dispatched to the intended operation.
        /// </summary>
        /// <param name="request">The request message.</param>
        /// <param name="channel">The incoming channel.</param>
        /// <param name="instanceContext">The current service instance.</param>
        /// <returns>The object used to correlate state. This object is passed back in the BeforeSendReply method.</returns>
        public object AfterReceiveRequest(ref Message request, IClientChannel channel, InstanceContext instanceContext)
        {
            // Log the incoming request
            if (request.Properties.ContainsKey("UriMatched") && (bool)request.Properties["UriMatched"])
            {
                // Uri matched a template and will be successfully processed
                UriTemplateMatch match = request.Properties["UriTemplateMatchResults"] as UriTemplateMatch;
                Log.Log__Message("Processing incoming REST request: {0}", match.RequestUri.ToString());

                HttpRequestMessageProperty httpRequest = request.Properties[HttpRequestMessageProperty.Name] as HttpRequestMessageProperty;
                if (httpRequest.Method == "POST" || httpRequest.Method == "PUT")
                {
                    Log.Log__Debug("{0} message body:\n{1}", httpRequest.Method, request);
                }
            }
            else
            {
                // Uri wasn't matched log the failed match and bail
                Log.Log__Error("Unknown uri template for incoming REST request: {0}.", request.Properties.Via);
            }

            // The address is a string so we have to parse to get as a "number"
            RemoteEndpointMessageProperty remoteEndpoint = request.Properties[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;
            IPAddress remoteAddress = IPAddress.Parse(remoteEndpoint.Address);

            // Determine the IP address of the game the rag Proxy is connected to.
            IPAddress gameAddress = IPAddress.None;

            IList<ragCore.GameConnection> gameConnections = ragCore.GameConnection.AcquireGameList(LogFactory.ApplicationLog);
            ragCore.GameConnection currentConnection = gameConnections.FirstOrDefault(item => item.Connected == true);
            if (currentConnection != null)
            {
                gameAddress = IPAddress.Parse(currentConnection.IPAddress);
            }

            // If IP address is denied clear the request message so service method does not get execute
            if (!(IPAddress.IsLoopback(remoteAddress) || remoteAddress.Equals(gameAddress)))
            {
                Log.Log__Error("The REST request did not come from a valid source so it will be ignored. Source IP: {0}", remoteAddress.ToString());
                request = null;
                return (channel.LocalAddress.Uri.Scheme.Equals(Uri.UriSchemeHttp) || channel.LocalAddress.Uri.Scheme.Equals(Uri.UriSchemeHttps)) ? c_httpAccessDenied : c_accessDenied;
            }

            // Everything was fine
            return null;
        }

        /// <summary>
        /// Called after the operation has returned but before the reply message is sent.
        /// </summary>
        /// <param name="reply">The reply message. This value is null if the operation is one way.</param>
        /// <param name="correlationState">The correlation object returned from the AfterReceiveRequest method.</param>
        public void BeforeSendReply(ref Message reply, object correlationState)
        {
            if (correlationState == c_httpAccessDenied)
            {
                HttpResponseMessageProperty responseProperty = new HttpResponseMessageProperty();
                responseProperty.StatusCode = HttpStatusCode.Unauthorized;
                reply.Properties["httpResponse"] = responseProperty;
            }
        }
        #endregion // IDispatchMessageInspector Implementation
    } // WebService
} // Workbench.AddIn.REST
