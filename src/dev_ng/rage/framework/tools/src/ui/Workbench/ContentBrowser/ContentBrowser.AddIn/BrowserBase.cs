﻿using System;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using RSG.Model.Common;
using System.Text.RegularExpressions;
using Workbench.AddIn.Services;
using Workbench.AddIn.UI.Layout;

namespace ContentBrowser.AddIn
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class BrowserBase : Workbench.AddIn.Services.ExtensionBase, IBrowser
    {
        #region Members

        private String m_name;
        private ObservableCollection<ContentBrowserCommand> m_commands;
        private ILevel m_selectedLevel;
        private ObservableCollection<IAsset> m_assetHierarchy;

        #endregion // Members

        #region Properties

        /// <summary>
        /// The name of the browser (needs to be unique)
        /// </summary>
        public String Name
        {
            get { return m_name; }
            set
            {
                if (m_name == value)
                    return;

                SetPropertyValue(value, () => Name,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_name = (String)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The list of commands on this browser
        /// </summary>
        public ObservableCollection<ContentBrowserCommand> Commands
        {
            get { return m_commands; }
            set
            {
                if (m_commands == value)
                    return;

                SetPropertyValue(value, () => Commands,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_commands = (ObservableCollection<ContentBrowserCommand>)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The main property for a browser, this contains the
        /// top level of the asset hierarchy for this browser
        /// </summary>
        public ObservableCollection<IAsset> AssetHierarchy
        {
            get { return m_assetHierarchy; }
            set
            {
                if (m_assetHierarchy == value)
                    return;

                SetPropertyValue(value, () => AssetHierarchy,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_assetHierarchy = (ObservableCollection<IAsset>)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Determines if this is the default browser for the
        /// content browser.
        /// </summary>
        public virtual Boolean DefaultBrowser
        {
            get { return false; }
        }

        /// <summary>
        /// The icon that will be displayed to represent this browser
        /// </summary>
        public virtual BitmapImage BrowserIcon
        {
            get { return null; }
        }
        
        /// <summary>
        /// Returns the control to use for the
        /// browser header in the view or null to
        /// use the default one.
        /// </summary>
        public virtual Control HeaderControl
        {
            get { return null; }
        }

        #endregion // Properties

        #region Constructor

        /// <summary>
        /// Default Constructor
        /// </summary>
        public BrowserBase(String name)
        {
            this.Name = name;
            this.Commands = this.CreateCommands();
            this.AssetHierarchy = new ObservableCollection<IAsset>();
        }

        #endregion // Constructor

        #region Virtual Methods

        /// <summary>
        /// Override to create the commands that will be attached to
        /// this browser
        /// </summary>
        protected virtual ObservableCollection<ContentBrowserCommand> CreateCommands()
        {
            return new ObservableCollection<ContentBrowserCommand>();
        }

        /// <summary>
        /// Gets called once the browser collection system has fully loaded
        /// </summary>
        public virtual void Initialise()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public virtual List<ISearchResult> FindResults(ISearchQuery query, IAsset rootObject)
        {
            return new List<ISearchResult>();
        }
        #endregion // Virtual Methods
    } // BrowserBase
} // ContentBrowser.AddIn
