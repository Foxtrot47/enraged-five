﻿using RSG.Model.Report;
using RSG.Model.Report.Reports;

namespace Workbench.AddIn.MapStatistics.Reports
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Report.AddIn.ExtensionPoints.MapStatsReport, typeof(IReport))]
    class PropReplacementReport : RSG.Model.Report.Reports.Map.PropReplacementReport
    {
    } // PropReplacementReport
} // Workbench.AddIn.MapStatistics.Reports
