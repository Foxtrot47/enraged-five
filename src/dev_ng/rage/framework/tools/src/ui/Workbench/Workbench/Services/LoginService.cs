﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn;
using Workbench.AddIn.Services;
using RSG.Base.ConfigParser;
using System.Windows;
using RSG.Base.Windows.Dialogs;

namespace Workbench.Services
{
    /// <summary>
    /// 
    /// </summary>
    [Obsolete("Use ICoreServiceProvider.UserInterfaceService instead.")]
    [ExportExtension(Workbench.AddIn.CompositionPoints.LoginService,
        typeof(Workbench.AddIn.Services.ILoginService))]
    class LoginService : ILoginService
    {
        #region Member data
        /// <summary>
        /// 
        /// </summary>
        private string m_loginCredentials;
        #endregion
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public LoginService()
        {
        }
        #endregion // Constructor(s)
        
        #region Methods
        /// <summary>
        /// Prompts the user for his login details
        /// </summary>
        /// <param name="loginPredicate"></param>
        /// <returns>true if everything is fine, false if an error was found</returns>
        public bool Login(Func<LoginDetails, LoginResults> loginPredicate)
        {
            return Login(loginPredicate, "Login Dialog", "Please enter your login details below:");
        }

        /// <summary>
        /// Prompts the user for his login details with the supplied dialog title/message
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public bool Login(Func<LoginDetails, LoginResults> loginPredicate, string title, string message)
        {
            // Check whether the user saved their details from their previous session
            if (!String.IsNullOrEmpty(m_loginCredentials))
            {
                string usernamePassword = new ASCIIEncoding().GetString(Convert.FromBase64String(m_loginCredentials));
                int colonIndex = usernamePassword.IndexOf(':');

                if (colonIndex != -1)
                {
                    String username = usernamePassword.Substring(0, colonIndex);
                    String password = usernamePassword.Substring(colonIndex + 1);

                    // Check whether they are still valid
                    LoginResults results = loginPredicate.Invoke(new LoginDetails { Username = username, Password = password });
                    if (results.Success)
                    {
                        return true;
                    }
                    else
                    {
                        m_loginCredentials = null;
                    }
                }
            }

            // Show the login dialog
            // Temporarily disable the remember me functionality and always remember the user (url:bugstar:617700).
            LoginViewModel vm = new LoginViewModel(loginPredicate, title, message, new Uri("pack://application:,,,/Workbench;component/Data/p4bugstar.png"));
            vm.Username = Environment.GetEnvironmentVariable("USERNAME");
            vm.IsUsernameReadOnly = true;

            LoginWindow loginWindow = new LoginWindow(vm);
            bool? result = loginWindow.ShowDialog();

            if (result == true)
            {
                string usernamePassword = String.Format("{0}:{1}", vm.Username, vm.Password);
                m_loginCredentials = Convert.ToBase64String(new ASCIIEncoding().GetBytes(usernamePassword));
                return true;
            }

            m_loginCredentials = null;
            return false;
        }
        #endregion
    } // LoginService
} // Workbench.Services
