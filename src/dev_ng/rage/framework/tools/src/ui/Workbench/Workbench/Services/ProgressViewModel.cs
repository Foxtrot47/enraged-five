﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using RSG.Base.Editor;
using System.Windows.Input;
using System.Windows;
using RSG.Base.Windows.Helpers;

namespace Workbench.Services
{
    public class ProgressViewModel : ViewModelBase
    {
        #region Members
        private BackgroundWorker m_worker = null;
        private Object m_sync = new object();
        #endregion // Members

        #region Properties and associated member data
        /// <summary>
        /// 
        /// </summary>
        public String Status
        {
            get { return m_status; }
            set
            {
                SetPropertyValue(value, m_status, () => this.Status,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_status = (String)newValue;
                        }
                    ));
            }
        }
        private string m_status;

        /// <summary>
        /// 
        /// </summary>
        public String Title
        {
            get { return m_title; }
            set
            {
                SetPropertyValue(value, m_title, () => this.Title,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_title = (String)newValue;
                        }
                    ));
            }
        }
        private string m_title;

        /// <summary>
        /// 
        /// </summary>
        public String Message
        {
            get { return m_message; }
            set
            {
                SetPropertyValue(value, m_message, () => this.Message,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_message = (String)newValue;
                        }
                    ));
            }
        }
        private string m_message;

        /// <summary>
        /// 
        /// </summary>
        public int Progress
        {
            get { return m_progress; }
            set
            {
                SetPropertyValue(value, m_progress, () => this.Progress,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_progress = (int)newValue;
                        }
                    ));
            }
        }
        private int m_progress;

        /// <summary>
        /// 
        /// </summary>
        public bool IsIndeterminate
        {
            get { return m_isIndeterminate; }
            set
            {
                SetPropertyValue(value, m_isIndeterminate, () => this.IsIndeterminate,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_isIndeterminate = (bool)newValue;
                        }
                    ));
            }
        }
        private bool m_isIndeterminate;

        /// <summary>
        /// 
        /// </summary>
        public Object WorkerArguments
        {
            get { return m_workerArguments; }
            set
            {
                SetPropertyValue(value, m_workerArguments, () => this.IsIndeterminate,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_workerArguments = newValue;
                        }
                    ));
            }
        }
        private Object m_workerArguments;

        /// <summary>
        /// 
        /// </summary>
        public Boolean? DialogState
        {
            get { return m_dialogState; }
            set
            {
                SetPropertyValue(value, m_dialogState, () => this.DialogState,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_dialogState = (Boolean?)newValue;
                        }
                    ));
            }
        }
        private Boolean? m_dialogState;

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                lock (m_sync)
                {
                    if (m_cancelCommand == null)
                    {
                        m_cancelCommand = new RelayCommand(param => this.Cancel(param), param => this.CanCancel(param));
                    }
                }
                return m_cancelCommand;
            }
        }
        private RelayCommand m_cancelCommand;

        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="worker"></param>
        public ProgressViewModel(BackgroundWorker worker)
            : this(worker, null, "", "", false)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="worker"></param>
        /// <param name="title"></param>
        /// <param name="message"></param>
        /// <param name="indeterminate"></param>
        public ProgressViewModel(BackgroundWorker worker, string title, string message, bool indeterminate)
            : this(worker, null, "", "", false)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="worker"></param>
        public ProgressViewModel(BackgroundWorker worker, object workerArguments, string title, string message, bool indeterminate)
        {
            m_dialogState = null;
            Title = title;
            Message = message;
            IsIndeterminate = indeterminate;

            m_worker = worker;
            m_workerArguments = workerArguments;
            m_worker.WorkerSupportsCancellation = true;
            m_worker.WorkerReportsProgress = !indeterminate;
            m_worker.ProgressChanged += WorkerProgressChanged;
            m_worker.RunWorkerCompleted += WorkerCompleted;
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        internal void StartWorker()
        {
            if (m_worker == null)
            {
                return;
            }

            m_worker.RunWorkerAsync(m_workerArguments);
        }

        /// <summary>
        /// 
        /// </summary>
        internal void CancelWorker()
        {
            m_worker.CancelAsync();
        }

        /// <summary>
        /// Update the progress bar to show the new progress
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WorkerProgressChanged(Object sender, ProgressChangedEventArgs e)
        {
            this.Progress = e.ProgressPercentage;
        }

        /// <summary>
        /// Set the dialog state dependending on whether the worker was cancelled or not
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WorkerCompleted(Object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled == true)
            {
            }
            else
            {
                this.DialogState = true;
            }
        }
        #endregion // Controller Methods

        #region Commands
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        private void Cancel(Object parameter)
        {
            m_worker.CancelAsync();
            this.DialogState = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        private Boolean CanCancel(Object parameter)
        {
            return this.m_worker != null && m_worker.IsBusy;
        }
        #endregion // Commands
    } // ProgressViewModel
} // Workbench.Services
