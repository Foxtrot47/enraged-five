﻿using System;
using MetadataEditor.AddIn.MetaEditor.ViewModel;
using RSG.Base.Editor.Command;
using RSG.Metadata.Data;

namespace MetadataEditor.AddIn.MetaEditor.GridViewModel
{
    public class StringValueViewModel : GridTunableViewModel
    {
        #region Constructor
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="MetadataEditor.AddIn.MetaEditor.GridViewModel.StringValueViewModel"/>
        /// class.
        /// </summary>
        public StringValueViewModel(GridTunableViewModel parent, PointerTunable m, MetaFileViewModel root)
            : base(parent, m, root)
        {
        }
        #endregion // Constructor
    }

    /// <summary>
    /// The view model that wraps around a 
    /// <see cref="StringTunable"/> object. Used
    /// for the grid view.
    /// </summary>
    public class StringTunableViewModel : GridTunableViewModel
    {
        #region Constructor
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="MetadataEditor.AddIn.MetaEditor.GridViewModel.StringTunableViewModel"/>
        /// class.
        /// </summary>
        public StringTunableViewModel(GridTunableViewModel parent, StringTunable m, MetaFileViewModel root)
            : base(parent, m, root)
        {
        }
        #endregion // Constructor
    } // StringTunableViewModel
} // MetadataEditor.AddIn.MetaEditor.GridViewModel
