﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;

using RSG.Model.Common.Map;
using RSG.Base.Editor;
using RSG.Base.Math;

using WidgetEditor.AddIn;
using Workbench.AddIn.MapDebugging.Overlays;

namespace Workbench.AddIn.MapDebugging.ToolTips
{
    /// <summary>
    /// Displays additional information about a specific SpawnPoint.
    /// </summary>
    public class SpawnPointDisplayChild : ViewModelBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public string Header
        {
            get { return m_header; }
            set
            {
                SetPropertyValue(value, () => this.Header,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_header = (string)newValue;
                        }
                ));
            }
        }
        private string m_header;

        public SpawnPointDisplay Parent
        {
            get;
            private set;
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="text"></param>
        public SpawnPointDisplayChild(SpawnPointDisplay parent, string text)
        {
            Parent = parent;
            Header = text;
        }
        #endregion
    }

    /// <summary>
    /// Displays information about a spawn point.
    /// </summary>
    public class SpawnPointDisplay : ViewModelBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public string Header
        {
            get { return m_header; }
            set
            {
                SetPropertyValue(value, () => this.Header,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_header = (string)newValue;
                        }
                ));
            }
        }
        private string m_header;

        /// <summary>
        /// 
        /// </summary>
        public IList<SpawnPointDisplayChild> Children
        {
            get { return m_children; }
            set
            {
                SetPropertyValue(value, () => this.Children,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_children = (IList<SpawnPointDisplayChild>)newValue;
                        }
                ));
            }
        }
        private IList<SpawnPointDisplayChild> m_children;

        /// <summary>
        /// 
        /// </summary>
        public SpawnPointInstance Instance
        {
            get { return m_spawnPointInstance; }
            set
            {
                SetPropertyValue(value, () => this.Instance,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_spawnPointInstance = (SpawnPointInstance)newValue;
                        }
                ));
            }
        }
        private SpawnPointInstance m_spawnPointInstance;

        private string m_source;
        private string m_entity;
        private string m_group;
        private string m_time;
        private string m_modelSet;
        private string m_availability;
        #endregion

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="spawnPointInstance"></param>
        public SpawnPointDisplay(SpawnPointInstance spawnPointInstance)
        {
            Instance = spawnPointInstance;

            ISpawnPoint spawnPoint = spawnPointInstance.SpawnPoint;
            m_header = spawnPoint.SpawnType;

            m_source = "Source: ";
            if (spawnPoint.Archetype == null)
            {
                m_source += Path.GetFileName(spawnPoint.SourceFile);
            }
            else
            {
                m_source += spawnPoint.Archetype.Name + ", " + spawnPoint.Archetype.CollisionGroup;
            }

            if (spawnPointInstance.Entity != null)
            {
                m_entity = "Instance: " + spawnPointInstance.Entity.Parent.Name;
            }
            else
            {
                m_entity = null;
            }

            if (String.IsNullOrWhiteSpace(spawnPoint.SpawnGroup) == false)
            {
                m_group = "Group: " + spawnPoint.SpawnGroup;
                m_group.Trim();
            }
            else
            {
                m_group = null;
            }

            string time = null;
            if (spawnPoint.StartTime != null && spawnPoint.EndTime != null)
                time = "Start Time: " + spawnPoint.StartTime + "\tEnd Time: " + spawnPoint.EndTime;
            else if (spawnPoint.StartTime != null)
                time = "Start Time: " + spawnPoint.StartTime;
            else if (spawnPoint.EndTime != null)
                time = "End Time: " + spawnPoint.EndTime;

            m_time = time;

            if (String.IsNullOrWhiteSpace(spawnPoint.ModelSet) == false)
            {
                m_modelSet = "Model Set: " + spawnPoint.ModelSet;
            }
            else
            {
                m_modelSet = null;
            }

            if (spawnPoint.AvailabilityMode != SpawnPointAvailableModes.kUnknown)
            {
                m_availability = "Availability: " + spawnPoint.AvailabilityMode.ToString();
            }
            else
            {
                m_availability = null;
            }

            Children = new List<SpawnPointDisplayChild>();
            
            if (m_entity != null)
                Children.Add(new SpawnPointDisplayChild(this, m_entity));
            if (m_group != null)
                Children.Add(new SpawnPointDisplayChild(this, m_group));
            
            if (spawnPoint.SourceFile != null)
            {
                string region = Path.GetFileNameWithoutExtension(spawnPoint.SourceFile);
                Children.Add(new SpawnPointDisplayChild(this, string.Format("Region: {0}", region)));
            }

            if (m_time != null)
                Children.Add(new SpawnPointDisplayChild(this, m_time));
            if (m_modelSet != null)
                Children.Add(new SpawnPointDisplayChild(this, m_modelSet));
            if (m_availability != null)
                Children.Add(new SpawnPointDisplayChild(this, m_availability));
        }
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    public class ChainingNodeDisplay : ViewModelBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public string Header
        {
            get { return m_header; }
            set
            {
                SetPropertyValue(value, () => this.Header,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_header = (string)newValue;
                        }
                ));
            }
        }
        private string m_header;

        /// <summary>
        /// 
        /// </summary>
        public IChainingGraphNode ChainingNode
        {
            get;
            private set;
        }
        #endregion

        #region Constructors
        public ChainingNodeDisplay(int count, IChainingGraphNode chainingNode)
        {
            Header = count + ": " + chainingNode.ScenarioType;
            ChainingNode = chainingNode;
        }
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    public class ChainingGraphDisplay : ViewModelBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public string Header
        {
            get { return m_header; }
            set
            {
                SetPropertyValue(value, () => this.Header,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_header = (string)newValue;
                        }
                ));
            }
        }
        private string m_header;

        /// <summary>
        /// 
        /// </summary>
        public IList<ChainingNodeDisplay> ChainedNodes
        {
            get { return m_chainedNodes; }
            set
            {
                SetPropertyValue(value, () => this.ChainedNodes,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_chainedNodes = (List<ChainingNodeDisplay>)newValue;
                        }
                ));
            }
        }
        private List<ChainingNodeDisplay> m_chainedNodes;

        /// <summary>
        /// 
        /// </summary>
        public IChainingGraph Instance
        {
            get { return m_chainingGraphInstance; }
            set
            {
                SetPropertyValue(value, () => this.Instance,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_chainingGraphInstance = (IChainingGraph)newValue;
                        }
                ));
            }
        }
        private IChainingGraph m_chainingGraphInstance;
        #endregion

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="spawnPointInstance"></param>
        public ChainingGraphDisplay(IChainingGraph chainingGraph)
        {
            Instance = chainingGraph;
            Header = "Count: " + chainingGraph.ChainedNodes.Count + " : " + Path.GetFileName(chainingGraph.SourceFile);
            ChainedNodes = new List<ChainingNodeDisplay>();
            int count = 0;
            foreach (IChainingGraphNode node in chainingGraph.ChainedNodes)
            {
                ChainedNodes.Add(new ChainingNodeDisplay(count, node));
                count++;
            }
        }
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    public class SpawnPointOverlayToolTipViewModel : ViewModelBase
    {
        #region MEF Imports
        /// <summary>
        /// MEF import for proxy UI
        /// </summary>
        [ImportExtension(WidgetEditor.AddIn.CompositionPoints.ProxyService, typeof(IProxyService))]
        public Lazy<IProxyService> ProxyService { get; set; }
        #endregion

        #region Constants
        private const string RAG_WARPCOORDINATES = "Debug/Warp Player x y z h vx vy vz";
        private const string RAG_WARPNOWBUTTON = "Debug/Warp now";
        #endregion 

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<SpawnPointDisplay> SpawnPoints
        {
            get { return m_spawnPoints; }
            set
            {
                SetPropertyValue(value, () => this.SpawnPoints,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_spawnPoints = (ObservableCollection<SpawnPointDisplay>)newValue;
                        }
                ));
            }
        }
        private ObservableCollection<SpawnPointDisplay> m_spawnPoints;

        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<ChainingGraphDisplay> ChainingGraphs
        {
            get { return m_chainingGraphs; }
            set
            {
                SetPropertyValue(value, () => this.ChainingGraphs,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_chainingGraphs = (ObservableCollection<ChainingGraphDisplay>)newValue;
                        }
                ));
            }
        }
        private ObservableCollection<ChainingGraphDisplay> m_chainingGraphs;

        /// <summary>
        /// 
        /// </summary>
        public bool IsGameConnected
        {
            get { return ProxyService.Value.IsGameConnected; }
        }
        
        #endregion

        #region Constructors
        public SpawnPointOverlayToolTipViewModel(Lazy<IProxyService> service, List<SpawnPointInstance> selectedSpawnPoints, List<IChainingGraph> selectedChainingGraphs)
        {
            ProxyService = service;

            ObservableCollection<SpawnPointDisplay> spawnPointList = new ObservableCollection<SpawnPointDisplay>();
            foreach (SpawnPointInstance spawnPoint in selectedSpawnPoints)
            {
                spawnPointList.Add(new SpawnPointDisplay(spawnPoint));
            }

            SpawnPoints = spawnPointList;

            ObservableCollection<ChainingGraphDisplay> chainingGraphList = new ObservableCollection<ChainingGraphDisplay>();
            foreach (IChainingGraph chainingGraph in selectedChainingGraphs)
            {
                chainingGraphList.Add(new ChainingGraphDisplay(chainingGraph));
            }

            ChainingGraphs = chainingGraphList;
        }
        #endregion

        #region Event Callbacks

        /// <summary>
        /// Returns whether the context menus should be enabled.
        /// </summary>
        /// <returns></returns>
        public bool IsContextMenuEnabled()
        {
            return ProxyService.Value.IsProxyConnected && ProxyService.Value.IsGameConnected;
        }

        /// <summary>
        /// Notify the runtime, if active, to warp and select the specified spawn point.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void WarpToSpawnPoint(SpawnPointInstance spawnPoint)
        {
            if (ProxyService.Value.IsProxyConnected)
            {
                Vector3f spawnPointWorldPosition = spawnPoint.Position;

                float heading = -(float)Math.PI / 2.0f;
                spawnPointWorldPosition.Z += 2.0f;  //Pad the camera so it's not on the point.
                ProxyService.Value.Console.WriteStringWidget(RAG_WARPCOORDINATES, String.Format("{0} {1} {2} {3}", spawnPointWorldPosition.X, spawnPointWorldPosition.Y, spawnPointWorldPosition.Z, heading));
                ProxyService.Value.Console.PressWidgetButton(RAG_WARPNOWBUTTON);
            }
        }

        /// <summary>
        /// Notify the runtime, if active, to warp and select the specified spawn point.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void WarpToChainingNode(IChainingGraphNode chainingNode)
        {
            if (ProxyService.Value.IsProxyConnected)
            {
                float heading = -(float)Math.PI / 2.0f;
                Vector3f cameraPosition = chainingNode.Position;
                cameraPosition.Z += 2.0f;  //Pad the camera so it's not on the point.
                ProxyService.Value.Console.WriteStringWidget(RAG_WARPCOORDINATES, String.Format("{0} {1} {2} {3}", cameraPosition.X, cameraPosition.Y, cameraPosition.Z, heading));
                ProxyService.Value.Console.PressWidgetButton(RAG_WARPNOWBUTTON);
            }
        }
        #endregion
    }
}
