﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using RSG.Model.GlobalTXD;

namespace Workbench.AddIn.TXDParentiser.ViewModel
{
    public class SourceTextureDictionaryViewModel : RSG.Base.Editor.HierarchicalViewModelBase, ITextureContainerViewModel, IDictionaryChildViewModel
    {
        #region Properties

        /// <summary>
        /// If this is true it means that source texture dictionaries can be added to
        /// this container as children
        /// </summary>
        public Boolean CanAcceptTextures
        {
            get { return this.Model.CanAcceptTextures; }
        }

        /// <summary>
        /// The name of the texture dictionary, this name will be exported and used at runtime,
        /// which means that this needs to be unique.
        /// </summary>
        public String Name
        {
            get { return this.Model.Name; }
            set { this.Model.Name = value; }
        }

        /// <summary>
        /// The model reference that this view model represents.
        /// </summary>
        public SourceTextureDictionary Model
        {
            get { return m_model; }
            set { m_model = value; }
        }
        private SourceTextureDictionary m_model;

        /// <summary>
        /// The parent global dictionary if it has one
        /// </summary>
        public new IDictionaryContainerViewModel Parent
        {
            get { return m_parent; }
            set
            {
                this.SetPropertyValue(ref this.m_parent, value, "Parent");
            }
        }
        private IDictionaryContainerViewModel m_parent;

        /// <summary>
        /// The global root object that this dictionary belongs to
        /// </summary>
        public GlobalRootViewModel Root
        {
            get { return m_root; }
            set
            {
                this.SetPropertyValue(ref this.m_root, value, "Root");
            }
        }
        private GlobalRootViewModel m_root;

        /// <summary>
        /// The user data that can be attached to this view model
        /// </summary>
        public RSG.Base.Collections.UserData ViewModelUserData
        {
            get { return m_viewModelUserData; }
            set { m_viewModelUserData = value; }
        }
        private RSG.Base.Collections.UserData m_viewModelUserData;

        /// <summary>
        /// Respresents where the UIElement bound to this object currently has focus
        /// </summary>
        public bool IsFocused
        {
            get { return m_isFocused; }
            set
            {
                this.SetPropertyValue(ref this.m_isFocused, value, "IsFocused");
            }
        }
        private bool m_isFocused = false;

        /// <summary>
        /// Returns the texture with the given streamname if it exists and null otherwise
        /// </summary>
        public SourceTextureViewModel this[String name]
        {
            get
            {
                foreach (SourceTextureViewModel texture in this.Children)
                {
                    if (texture.Model.StreamName == name)
                    {
                        return texture;
                    }
                }
                return null;
            }
        }

        public Boolean CreatingTextures
        {
            get;
            set;
        }

        public Boolean TexturesCreated
        {
            get;
            set;
        }

        /// <summary>
        /// A dictionary of uniquely named source texture dictionaries that are children of this global root
        /// The dictionaries are indexed by names and these names need to be unique.
        /// </summary>
        IEnumerable<ITextureChildViewModel> ITextureContainerViewModel.Textures
        {
            get { return this.Children.OfType<ITextureChildViewModel>(); }
        }

        public void RemoveTexture(ITextureChildViewModel texture)
        {
            this.Children.Remove(texture as SourceTextureViewModel);
        }

        public bool IsDropTarget
        {
            get { return false; }
        }
        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        public SourceTextureDictionaryViewModel(SourceTextureDictionary model, GlobalTextureDictionaryViewModel parent)
        {
            this.Model = model;
            this.Parent = parent;
            this.Root = parent.Root;
            this.ViewModelUserData = new RSG.Base.Collections.UserData();

            foreach (SourceTexture texture in model.Textures.Values)
            {
                this.Children.Add(new SourceTextureViewModel(texture, this));
            }
        }

        #endregion // Constructors

        #region ITextureContainerViewModel Implementation

        /// <summary>
        /// Promotes the given texture (source or global) to here and makes sure that
        /// validation is done so that the texture is removed elsewhere
        /// </summary>
        public Boolean PromoteTextureHere(SourceTextureViewModel texture)
        {
            return this.Model.PromoteTextureHere(texture.Model);
        }

        /// <summary>
        /// Moves the given global texture from its previous global dictionary to this 
        /// global dictionary
        /// </summary>
        public Boolean MoveTextureHere(GlobalTextureViewModel texture)
        {
            return false;
        }

        /// <summary>
        /// Moves the given global texture from this dictionary before it gets
        /// added to another dictionary
        /// </summary>
        public Boolean MoveTextureFromHere(GlobalTextureViewModel texture)
        {
            return false;
        }

        #endregion // ITextureContainerViewModel Implementation

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="results"></param>
        public IEnumerable<IViewModel> FindAll(Regex expression)
        {
            foreach (SourceTextureViewModel texture in Children)
            {
                if (expression.IsMatch(texture.Model.StreamName))
                    yield return texture;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="results"></param>
        public IEnumerable<IViewModel> GetAll()
        {
            foreach (SourceTextureViewModel texture in Children)
            {
                yield return texture;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Expand()
        {
            this.IsExpanded = true;
        }

        /// <summary>
        /// 
        /// </summary>
        public void ExpandedTo()
        {
            IDictionaryContainerViewModel parent = this.Parent;
            while (parent != null)
            {
                parent.Expand();
                parent = parent.Parent;
            }
        }
        #endregion
    }
} // Workbench.AddIn.TXDParentiser.ViewModel
