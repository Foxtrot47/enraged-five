﻿using System.Collections.ObjectModel;

namespace MetadataEditor.AddIn.KinoEditor.Subtitle_Editor
{
    /// <summary>
    /// Represents a single Event Track that has a collection of events in it all with a start
    /// position and length.
    /// </summary>
    public class EventTrackViewModel : ViewModelBase
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Name"/> property.
        /// </summary>
        private string name;

        /// <summary>
        /// The private field used for the <see cref="Events"/> property.
        /// </summary>
        private ObservableCollection<EventViewModel> events = new ObservableCollection<EventViewModel>();
        #endregion
        
        /// <summary>
        /// Initialises a new instance of the <see cref="MainWindowViewModel"/> class.
        /// </summary>
        public EventTrackViewModel()
        {
        }

        /// <summary>
        /// Get or sets the name for this track.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.SetProperty(ref this.name, value, "Name"); }
        }

        public ObservableCollection<EventViewModel> Events
        {
            get { return this.events; }
        }
    }
}
