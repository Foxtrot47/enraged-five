﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Xsl;
using RSG.Base.Logging;
using RSG.Model.Report;
using RSG.Base.Tasks;

namespace Workbench.AddIn.Bugstar.Reports
{

    /// <summary>
    /// A ReportUri and a Builder Report concrete class - MEF constructed
    /// </summary>
    class BugstarReport :
        HTMLReport,
        IDynamicReport
    {
        #region Constants
        private static readonly String DESC = "Bugstar Report";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Task to run for generating this report.
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (m_task == null)
                {
                    m_task = new ActionTask("Generating report", (context, progress) => GenerateReport((DynamicReportContext)context, progress));
                }
                return m_task;
            }
        }
        private ITask m_task;
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private RSG.Interop.Bugstar.Search.Report m_report;
        #endregion

        #region Constructor(s)
        public BugstarReport(RSG.Interop.Bugstar.Search.Report report)
            : base(report.Name, DESC)
        {
            m_report = report;
        }
        #endregion // Constructor(s)

        #region IDynamicReport Interface
        /// <summary>
        /// Generates the report dynamically
        /// </summary>
        private void GenerateReport(DynamicReportContext context, IProgress<TaskProgress> progress)
        {
            try
            {
                // Get the xml and xsl streams
                XmlTextReader xmlReader = new XmlTextReader(m_report.DataStream);
                XmlTextReader xslReader = new XmlTextReader(m_report.StylesheetStream);

                XslCompiledTransform transformer = new XslCompiledTransform();
                transformer.Load(xslReader);

                MemoryStream resultsStream = new MemoryStream();
                transformer.Transform(xmlReader, null, resultsStream);

                resultsStream.Position = 0;
                Stream = resultsStream;
            }
            catch (System.Exception ex)
            {
                Log.Log__Exception(ex, "Unhandled exception while processing the {0} report.", Name);
            }
        }
        #endregion // DynamicReportStream
    } // class BugstarReport 
} // Workbench.AddIn.Bugstar.Reports namespace
