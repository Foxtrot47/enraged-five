﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Report.Reports.GameStatsReports.Tests;
using RSG.Base.Editor.Command;

namespace Workbench.AddIn.GameStatistics.Reports.Automated.ViewModels
{
    /// <summary>
    /// 
    /// </summary>
    public class ScriptMemorySmokeTestViewModel : SmokeTestViewModelBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public IList<ScriptMemSmokeTest.ScriptMemInfo> ScriptMemResults
        {
            get { return m_scriptMemResults; }
            set
            {
                SetPropertyValue(value, () => this.ScriptMemResults,
                          new PropertySetDelegate(delegate(object newValue) { m_scriptMemResults = (IList<ScriptMemSmokeTest.ScriptMemInfo>)newValue; }));
            }
        }
        private IList<ScriptMemSmokeTest.ScriptMemInfo> m_scriptMemResults;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ScriptMemorySmokeTestViewModel(ScriptMemSmokeTest smokeTest, string testName)
            : base(smokeTest, testName)
        {
            if (smokeTest.GroupedMemStatInfo.ContainsKey(testName))
            {
                ScriptMemResults = smokeTest.GroupedMemStatInfo[testName];
            }
        }
        #endregion // Constructor(s)
    } // ScriptMemorySmokeTestViewModel
}
