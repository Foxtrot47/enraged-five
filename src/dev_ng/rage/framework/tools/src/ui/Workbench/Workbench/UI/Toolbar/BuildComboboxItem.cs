﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn.Commands;
using RSG.Statistics.Common.Dto.GameAssets;

namespace Workbench.UI.Toolbar
{
    /// <summary>
    /// 
    /// </summary>
    public class BuildComboboxItem : WorkbenchCommandComboBoxItem
    {
        #region Constants
        /// <summary>
        /// 
        /// </summary>
        public static readonly Guid GUID = new Guid("0B1BDB02-54F7-4BA7-A499-C0E6B9CE80AE");
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Build this label is for
        /// </summary>
        public BuildDto Build
        {
            get;
            protected set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="platform"></param>
        public BuildComboboxItem(BuildDto build)
            : base()
        {
            this.ID = GUID;
            this.Header = build.Identifier;
            this.CustomDataTemplateName = "CustomBuildItem";
            this.CustomSelectedDataTemplateName = "CustomSelectedBuildItem";
            Build = build;
        }
        #endregion // Constructor(s)
    } // ContentBrowserToolbarBuildLabel
} // ContentBrowser.UI.Toolbar
