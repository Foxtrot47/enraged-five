﻿using MetadataEditor.AddIn.MetaEditor.ViewModel;
using RSG.Metadata.Data;


namespace MetadataEditor.AddIn.MetaEditor.GridViewModel
{
    /// <summary>
    /// The view model that wraps around a  <see cref="ColourTunable"/> object. Used for the
    /// grid view.
    /// </summary>
    public class ColourTunableViewModel : GridTunableViewModel
    {
        #region Constructor
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="MetadataEditor.AddIn.MetaEditor.GridViewModel.ColourTunableViewModel"/>
        /// class.
        /// </summary>
        public ColourTunableViewModel(GridTunableViewModel parent, Color32Tunable m, MetaFileViewModel root)
            : base(parent, m, root)
        {
        }
        #endregion // Constructor
    } // ColourTunableViewModel
} // MetadataEditor.AddIn.MetaEditor.GridViewModel