﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Workbench.AddIn.UI.Layout
{
    public interface ISearchableTags
    {
        #region Properties
        List<string> Tags { get; }
        #endregion //  Properties
    }
}
