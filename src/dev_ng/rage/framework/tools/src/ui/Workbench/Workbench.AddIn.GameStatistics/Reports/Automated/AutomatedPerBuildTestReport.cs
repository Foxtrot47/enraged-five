﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Report;
using RSG.Model.Report.Reports.GameStatsReports.Tests;
using System.ComponentModel.Composition;
using System.IO;
using RSG.SourceControl.Perforce;

namespace Workbench.AddIn.GameStatistics.Reports.Automated
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(ExtensionPoints.AutomatedBuildReport, typeof(IReport))]
    public class AutomatedPerBuildTestReport : AutomatedTestReport, IPartImportsSatisfiedNotification
    {
        #region Constants
        /// <summary>
        /// 
        /// </summary>
        private const string c_name = "Per Build Report";

        /// <summary>
        /// Report description.
        /// </summary>
        private const string c_description = "Generates a report showing information for a single automated mission's test results .";

        /// <summary>
        /// Directory where cached version of the p4 files are stored.
        /// </summary>
        private const string c_cacheDir = @"cache:\Reports\Automated\PerBuild";
        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// MEF import for login service.
        /// </summary>
        [ImportExtension(PerforceBrowser.AddIn.CompositionPoints.PerforceService, typeof(PerforceBrowser.AddIn.IPerforceService))]
        public PerforceBrowser.AddIn.IPerforceService PerforceService { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(Workbench.AddIn.Services.IConfigurationService))]
        protected Workbench.AddIn.Services.IConfigurationService Config { get; set; }
        #endregion // MEF Imports
        
        #region Private Member Data
        /// <summary>
        /// Place to save cached p4 files to.
        /// </summary>
        protected override string CacheDir
        {
            get
            {
                return c_cacheDir;
            }
        }
        #endregion // Private Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="graph"></param>
        public AutomatedPerBuildTestReport()
            : base(c_name, c_description)
        {
            SmokeTests.Add(new FpsSmokeTest());
            SmokeTests.Add(new CpuSmokeTest());
            SmokeTests.Add(new GpuSmokeTest());
            SmokeTests.Add(new MemorySmokeTest());
            SmokeTests.Add(new StreamingMemorySmokeTest());
            SmokeTests.Add(new ScriptMemSmokeTest());
            SmokeTests.Add(new ThreadSmokeTest());
            SmokeTests.Add(new DrawListSmokeTest());
        }
        #endregion // Constructor(s)

        #region Methods
        protected override P4 GetPerforceConnection()
        {
            return PerforceService.PerforceConnection;
        }
        #endregion

        #region IPartImportsSatisfiedNotification
        /// <summary>
        /// Called when a part's imports have been satisfied and it is safe to use.
        /// </summary>
        public void OnImportsSatisfied()
        {
            // Set up the depot file path
            DepotFile = Path.GetFullPath(Config.ReportsConfig.AutomatedReports.PerBuildStatsFile.Replace("$(assets)", Config.GameConfig.AssetsDir));
        }
        #endregion // IPartImportsSatisfiedNotification
    } // AutomatedPerBuildTestReport
}
