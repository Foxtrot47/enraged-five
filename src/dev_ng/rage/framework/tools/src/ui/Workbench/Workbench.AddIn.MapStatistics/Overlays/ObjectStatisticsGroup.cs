﻿using System;
using System.ComponentModel.Composition;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Workbench.AddIn.MapStatistics.Overlays
{
    [ExportExtension(Viewport.AddIn.ExtensionPoints.OverlayGroup, typeof(Viewport.AddIn.IViewportOverlayGroup))]
    public class ObjectStatisticsGroup : Viewport.AddIn.ViewportOverlayGroup, IPartImportsSatisfiedNotification
    {
        #region MEF Imports

        /// <summary>
        /// 
        /// </summary>
        [ImportManyExtension(Workbench.AddIn.MapStatistics.ExtensionPoints.ObjectStatisticsOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
        IEnumerable<Viewport.AddIn.IViewportOverlay> ImportedOverlays
        {
            get;
            set;
        }

        #endregion // MEF Imports

        #region Constructor

        public ObjectStatisticsGroup()
            : base("Count Statistics")
        {
        }

        #endregion // Constructor

        #region IPartImportsSatisfiedNotification

        public void OnImportsSatisfied()
        {
            foreach (Viewport.AddIn.IViewportOverlay overlay in this.ImportedOverlays)
            {
                this.Overlays.Add(overlay);
            }   
        }

        #endregion // IPartImportsSatisfiedNotification
    } // CostStatisticGroup
} // Workbench.AddIn.MapStatistics.Overlays
