﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn;
using Workbench.AddIn.Commands;
using RSG.Base.Extensions;
using RSG.Model.Common;
using ContentBrowser.AddIn;
using Workbench.AddIn.Services;
using System.ComponentModel.Composition;
using RSG.Statistics.Client.ServiceClients;
using Workbench.AddIn.Services.Model;
using RSG.Base.Logging;

namespace Workbench.UI.Toolbar
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.DataModeToolbar, typeof(IWorkbenchCommand))]
    [ExportExtension(Workbench.AddIn.CompositionPoints.DataSourceBrowser, typeof(IDataSourceBrowser))]
    class DataSourceCombobox : WorkbenchCommandCombobox, IDataSourceBrowser
    {
        #region Constants
        /// <summary>
        /// Unique GUID
        /// </summary>
        public static readonly Guid GUID = new Guid("3E2B6624-61F0-44B2-9DDE-ACD4B7B07713");
        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(Workbench.AddIn.Services.IConfigurationService))]
        Lazy<Workbench.AddIn.Services.IConfigurationService> Config { get; set; }

        /// <summary>
        /// MEF import for message box service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.MessageService, typeof(Workbench.AddIn.Services.IMessageService))]
        public IMessageService MessageService { get; set; }

        /// <summary>
        /// MEF import for content browser settings
        /// </summary>
        [ImportExtension(ContentBrowser.AddIn.CompositionPoints.ContentBrowserSettings, typeof(ContentBrowser.AddIn.IContentBrowserSettings))]
        ContentBrowser.AddIn.IContentBrowserSettings Settings { get; set; }
        #endregion // MEF Imports

        #region Events
        /// <summary>
        /// Gets called when the current data source changes
        /// </summary>
        public event DataSourceChangedEventHandler DataSourceChanged;
        #endregion // Events

        #region Properties
        /// <summary>
        /// The currently selected item in the data source collection
        /// </summary>
        public DataSource SelectedSource
        {
            get
            {
                DataSourceComboboxItem label = SelectedItem as DataSourceComboboxItem;
                return (label != null ? label.Source : DataSource.SceneXml);
            }
            set
            {
                if (SelectedSource != value)
                {
                    SelectedItem = (DataSourceComboboxItem)Items.FirstOrDefault(item => (item as DataSourceComboboxItem).Source == value);
                }
            }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public DataSourceCombobox()
            : base()
        {
            this.ID = GUID;
            this.RelativeToolBarID = DataSourceLabel.GUID;
            this.Direction = Direction.After;
        }
        #endregion // Constructor(s)

        #region IDataSourceBrowser Implementation
        /// <summary>
        /// 
        /// </summary>
        public void UpdateDataSources()
        {
            // Create the list of child combo box items
            List<IWorkbenchCommand> allItems = new List<IWorkbenchCommand>();
            foreach (DataSource value in Enum.GetValues(typeof(DataSource)))
            {
                if (value == DataSource.Database && !Config.Value.GameConfig.StatisticsEnabled)
                {
                    continue;
                }

                allItems.Add(new DataSourceComboboxItem(value));
            }
            this.Items = allItems;

            // Try and restore the previously selected datasource
            DataSource previousSource = DataSource.SceneXml;
            if (!String.IsNullOrEmpty(Settings.DataSource))
            {
                try
                {
                    previousSource = (DataSource)Enum.Parse(typeof(DataSource), Settings.DataSource);
                }
                catch (System.Exception)
                {
                    Log.Log__Warning("Previously selected datasource '{0}' isn't a valid datasource.", Settings.DataSource);
                }
            }

            IWorkbenchCommand previousComboItem = allItems.Cast<DataSourceComboboxItem>().FirstOrDefault(item => item.Source == previousSource);
            if (previousComboItem == null)
            {
                Log.Log__Warning("Previously selected datasource '{0}' is no longer available.");
                previousComboItem = allItems.FirstOrDefault();
            }
            this.SelectedItem = previousComboItem;
        }
        #endregion // IDataSourceBrowser Implementation

        #region WorkbenchCommandCombobox Overrides
        /// <summary>
        /// Called prior to the selected item changing
        /// </summary>
        /// <param name="oldValue"></param>
        /// <param name="newValue"></param>
        /// <returns>True if the change is allowed, false otherwise</returns>
        protected override bool PreSelectedItemChanged(IWorkbenchCommand oldValue, IWorkbenchCommand newValue)
        {
            DataSourceComboboxItem newLabel = newValue as DataSourceComboboxItem;
            DataSource? newSource = (newLabel != null ? (DataSource?)newLabel.Source : null);

            // If the user switched to the database mode, check that the server is up and running
            if (newSource == DataSource.Database)
            {
                try
                {
                    // Run a platform query
                    using (EnumClient client = new EnumClient())
                    {
                        client.GetPlatforms();
                    }
                }
                catch (System.Exception)
                {
                    MessageService.Show("The statistics server is currently down for maintenance and thus the Database mode is unavailable.  Please try again in a short while.");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Callback for whenever the selected item changes
        /// </summary>
        /// <param name="oldValue"></param>
        /// <param name="newValue"></param>
        protected override void PostSelectedItemChanged(IWorkbenchCommand oldValue, IWorkbenchCommand newValue)
        {
            DataSourceComboboxItem oldLabel = oldValue as DataSourceComboboxItem;
            DataSourceComboboxItem newLabel = newValue as DataSourceComboboxItem;

            DataSource? oldSource = (oldLabel != null ? (DataSource?)oldLabel.Source : null);
            DataSource? newSource = (newLabel != null ? (DataSource?)newLabel.Source : null);

            if (newSource != null)
            {
                Settings.DataSource = newSource.Value.ToString();
                Settings.Apply();
            }

            if (DataSourceChanged != null)
            {
                DataSourceChanged(this, new DataSourceChangedEventArgs(oldSource, newSource));
            }
        }
        #endregion
    } // ContentBrowserToolbarDataSourceCombobox
} // ContentBrowser.UI.Toolbar
