﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn.Services;
using Workbench.AddIn;
using System.Collections.Concurrent;
using RSG.Base.Tasks;
using System.Threading;
using System.Threading.Tasks;
using RSG.Base.Windows.Dialogs;
using RSG.Base.Logging;
using RSG.Base.Threading;
using System.Diagnostics;

namespace Workbench.Services
{
    /// <summary>
    /// The task progress service allows callers to inform subscribers of the current percentage completion of a task. Tasks can be 
    /// run in either foreground or background mode. In foreground mode, a dedicated dialog box shows the progress of the task. In 
    /// background mode, the progress is displayed in the status bar of the main Workbench window. Background tasks can be queued and
    /// will execute in the order they are received. 
    /// </summary>
    [ExportExtension(Workbench.AddIn.CompositionPoints.TaskProgressService, typeof(Workbench.AddIn.Services.ITaskProgressService))]
    public class TaskProgressService : ITaskProgressService
    {
        #region Private helper class

        /// <summary>
        /// Queued task contains the task and the context the caller gave it.
        /// </summary>
        class QueuedTask
        {
            #region Properties

            /// <summary>
            /// Task.
            /// </summary>
            public ITask Task { get; set; }

            /// <summary>
            /// Task context.
            /// </summary>
            public TaskContext Context { get; set; }

            #endregion

            #region Constructor

            /// <summary>
            /// Constructor.
            /// </summary>
            /// <param name="task">Task.</param>
            /// <param name="context">Task context.</param>
            public QueuedTask(ITask task, TaskContext context)
            {
                Task = task;
                Context = context;
            }

            #endregion
        }

        #endregion

        #region Private member fields
        /// <summary>
        /// 
        /// </summary>
        ILog m_log;

        /// <summary>
        /// The queue containing the tasks that need processing
        /// </summary>
        private BlockingCollection<QueuedTask> TaskQueue { get; set; }

        /// <summary>
        /// The source for cancelling tasks that are queued up
        /// </summary>
        protected CancellationTokenSource TaskCancellationSouce { get; private set; }
        #endregion

        #region Public events

        /// <summary>
        /// Current task progress changed event.
        /// </summary>
        public event TaskProgressEventHandler ProgressChanged;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        public TaskProgressService()
        {
            m_log = LogFactory.CreateUniversalLog("TaskProgressService");
            this.TaskCancellationSouce = new CancellationTokenSource();
            this.TaskQueue = new BlockingCollection<QueuedTask>();
            this.ThreadEntry();
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Stop the service.
        /// </summary>
        public void Stop()
        {
            TaskCancellationSouce.Cancel();
        }

        /// <summary>
        /// Add a task to the internal queue.
        /// </summary>
        /// <param name="task">Task.</param>
        /// <param name="context">Task context.</param>
        /// <param name="priority">Task priority.</param>
        /// <param name="cts">Cancellation token source required when TaskPriority is foreground.</param>
        public void Add(ITask task, TaskContext context, TaskPriority priority, CancellationTokenSource cts = null)
        {
            if (priority == TaskPriority.Foreground)
            {
                Debug.Assert(cts != null, "Must provide the cancellation token source when adding a foreground task.");
                if (cts == null)
                {
                    throw new ArgumentNullException("cts");
                }

                m_log.Profile("Executing '{0}' in the foreground.", task.Name);

                TaskExecutionDialog dialog = new TaskExecutionDialog();
                TaskExecutionViewModel taskVm = new TaskExecutionViewModel("Running Task", task, cts, context);
                taskVm.AutoCloseOnCompletion = true;
                dialog.DataContext = taskVm;
                dialog.ShowDialog();

                m_log.ProfileEnd();
            }
            else
            {
                m_log.Message("Queuing '{0}'.", task.Name);
                TaskQueue.Add(new QueuedTask(task, context));
            }
        }

        #endregion

        #region Private helper methods

        /// <summary>
        /// Thread entry point.
        /// </summary>
        private void ThreadEntry()
        {
            Task.Factory.StartNew((o) => ProcessTasks(o), this.TaskCancellationSouce);
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void ProcessTasks(object dummy)
        {
            foreach (QueuedTask task in TaskQueue.GetConsumingEnumerable())
            {
                try
                {
                    this.ProcessTask(task);
                    Thread.Sleep(30);
                }
                catch (System.Exception ex)
                {
                    Log.Log__Exception(ex, "Unhandled thread exception");
                }
            }
        }

        /// <summary>
        /// Process a single task.
        /// </summary>
        /// <param name="obj">Task and context in the form of a QueuedTask instance.</param>
        private void ProcessTask(object obj)
        {
            var item = obj as QueuedTask;

            bool hasEvent = false;

            if (item.Task.Progress is EventProgress<TaskProgress>)
            {
                var evtProgress = item.Task.Progress as EventProgress<TaskProgress>;
                evtProgress.ProgressChanged += new EventHandler<TaskProgress>(evtProgress_ProgressChanged);
                hasEvent = true;
            }

            UpdateProgressBar(0, String.Empty);
            m_log.Profile("Executing '{0}'.", item.Task.Name);
            item.Task.Execute(item.Context);
            m_log.ProfileEnd();
            UpdateProgressBar(0, String.Empty);

            if (hasEvent)
            {
                var evtProgress = item.Task.Progress as EventProgress<TaskProgress>;
                evtProgress.ProgressChanged -= evtProgress_ProgressChanged;
            }
        }

        /// <summary>
        /// Update the progress bar with a new value.
        /// </summary>
        /// <param name="completion">Percentage completion (0..100 inclusive).</param>
        /// <param name="message">Text message giving additional detail on the current task.</param>
        private void UpdateProgressBar(int completion, string message)
        {
            if (ProgressChanged != null)
            {
                ProgressChanged(this, new TaskProgressServiceArgs(completion, message));
            }
        }

        #endregion

        #region Event handlers

        /// <summary>
        /// Current event progress changed event handler.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Task progress event arguments.</param>
        void evtProgress_ProgressChanged(object sender, TaskProgress e)
        {
            int complete = (int)(e.Progress * 100);
            UpdateProgressBar(complete, e.Message);
        }

        #endregion
    }
}
