﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Windows.Helpers;
using RSG.Base.Editor;

namespace Workbench.AddIn.TXDParentiser.AddIn
{
    /// <summary>
    /// 
    /// </summary>
    public class TxdSelectionChangedEventArgs : ItemChangedEventArgs<IEnumerable<ModelBase>>
    {
        public TxdSelectionChangedEventArgs(IEnumerable<ModelBase> oldSelection, IEnumerable<ModelBase> newSelection)
            : base(oldSelection, newSelection)
        {
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="args"></param>
    public delegate void TxdSelectionChangedEventHandler(Object sender, TxdSelectionChangedEventArgs args);
}
