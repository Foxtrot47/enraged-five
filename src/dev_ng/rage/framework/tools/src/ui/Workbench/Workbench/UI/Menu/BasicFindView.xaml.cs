﻿using System;
using System.Windows;
using Workbench.AddIn;
using Workbench.AddIn.Services;
using Workbench.AddIn.UI.Layout;
using System.Collections.Generic;

namespace Workbench.UI.Menu
{
    /// <summary>
    /// 
    /// </summary>
    public partial class BasicFindView : ToolWindowBase<BasicFindViewModel>
    {
        #region Properties
        public override Size DefaultFloatSize
        {
            get
            {
                return new Size(380, 295);
            }
        }
        
        public override bool FloatByDefault
        {
            get
            {
                return true;
            }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public BasicFindView()
            : base(BasicFindViewModel.BASICFIND_NAME, new BasicFindViewModel())
        {
            InitializeComponent();
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public BasicFindView(ILayoutManager layoutManager, IMessageService messageService, SearchResultWindowViewModel searchResultWindowViewModel, SearchResultWindowProxy searchResultWindowProxy, List<string> tags)
            : base(BasicFindViewModel.BASICFIND_NAME, new BasicFindViewModel(layoutManager, messageService, searchResultWindowViewModel, searchResultWindowProxy, tags))
        {
            InitializeComponent();
            this.ViewModel.FindContent = this;
            layoutManager.ContentOpened += new ContentOpenedHandler(layoutManager_ContentOpened);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="openedContent"></param>
        void layoutManager_ContentOpened(IContentBase openedContent)
        {
            if (openedContent == this)
            {
                if (this.FindText != null)
                {
                    this.FindText.SelectAll();
                    this.FindText.Focus();
                }
            }
        }
        #endregion // Constructor(s)

        protected override void OnKeyDown(System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter || e.Key == System.Windows.Input.Key.F3)
            {
                if (this.ViewModel.CanExecuteFindCommand(null))
                {
                    this.ViewModel.Find(SearchParameter.FindNext);
                }
            }

            base.OnKeyDown(e);
        }
    
    } // PropertyInspectorView
} // Workbench.UI
