﻿using System;
using System.Collections.Generic;
using RSG.Base.Collections;
using RSG.Model.Common;
using RSG.Model.Report;
using System.ComponentModel.Composition;
using RSG.Base.Editor.Command;

namespace Report.AddIn
{

    /// <summary>
    /// Abstract report category class.
    /// </summary>
    public abstract class ReportCategory : 
        AssetContainerBase,
        IReportCategory,
        IPartImportsSatisfiedNotification
    {
        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public ReportCategory()
            : this("", "")
        {            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="description"></param>
        public ReportCategory(string name, string description)
            : base(name)
        {
            Description = description;
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Report description string.
        /// </summary>
        public String Description
        {
            get { return m_sDescription; }
            protected set
            {
                SetPropertyValue(value, () => this.Description,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_sDescription = (String)newValue;
                        }
                ));
            }
        }
        private String m_sDescription;

        /// <summary>
        /// Category report objects.
        /// This is abstract since we wish your class to implement it with the appropriate attribute ( as defined by your class ) for MEF imports.
        /// </summary>
        public abstract IEnumerable<IReportItem> Reports
        {
            get;
            protected set;
        }
        #endregion // Properties

        #region Public Methods
        /// <summary>
        /// Method that gets called to intialise the category's reports.
        /// </summary>
        public virtual void InitialiseReports()
        {
        }
        #endregion // Public Methods

        #region IPartImportsSatisfiedNotification Interface
        /// <summary>
        /// 
        /// </summary>
        public virtual void OnImportsSatisfied()
        {
            this.AssetChildren.AddRange(Reports);
        }
        #endregion // IPartImportsSatisfiedNotification Interface
    } // ReportCategory

} // Report.AddIn namespace
