﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Workbench.AddIn;
using Workbench.AddIn.Services;

namespace Workbench.AddIn.UI.ToolBar
{
    /// <summary>
    /// Interaction logic for ToolBarButtonBaseView.xaml
    /// </summary>
    public partial class ToolBarButtonBaseView : ResourceDictionary
    {
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ToolBarButtonBaseView()
        {
            InitializeComponent();
        }
        #endregion // Constructor(s)
    }
     
} // Workbench.AddIn.UI.ToolBar namespace
