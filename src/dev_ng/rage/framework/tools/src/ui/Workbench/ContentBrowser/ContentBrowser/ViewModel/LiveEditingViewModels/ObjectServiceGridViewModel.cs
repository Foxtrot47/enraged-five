﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ContentBrowser.ViewModel.GridViewModels;
using RSG.Model.LiveEditing;

namespace ContentBrowser.ViewModel.LiveEditingViewModels
{
    /// <summary>
    /// 
    /// </summary>
    internal class ObjectServiceGridViewModel : GridViewModelBase
    {
        #region Constructor(s)

        /// <summary>
        /// Default constructor
        /// </summary>
        public ObjectServiceGridViewModel(IObjectService service)
            : base(service)
        {
        }
        #endregion // Constructor(s)
    } // ObjectServiceGridViewModel
}
