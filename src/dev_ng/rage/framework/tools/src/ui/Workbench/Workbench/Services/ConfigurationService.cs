﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Windows;
using RSG.Base.ConfigParser;
using RSG.Base.Configuration;
using RSG.Base.Configuration.Automation;
using RSG.Base.Configuration.Reports;
using RSG.Base.Logging;
using Workbench.AddIn;
using Workbench.AddIn.Services;
using RSG.Base.Configuration.Bugstar;

namespace Workbench.Services
{

    /// <summary>
    /// Workbench configuration service implementation.
    /// </summary>
    [ExportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService,
        typeof(IConfigurationService))]
    class ConfigurationService : 
        IConfigurationService,
        IPartImportsSatisfiedNotification
    {
        #region Properties
        /// <summary>
        /// Game and tools configuration data (OLD, will become obsolete).
        /// </summary>
        [Obsolete("Use IConfigurationService.Config instead.")]
        public ConfigGameView GameConfig
        {
            get;
            private set;
        }

        /// <summary>
        /// Game and tools configuration data.
        /// </summary>
        public IConfig Config
        {
            get;
            private set;
        }

        /// <summary>
        /// Reports configuration data.
        /// </summary>
        public IReportsConfig ReportsConfig
        {
            get;
            private set;
        }

        /// <summary>
        /// Bugstar configuration data.
        /// </summary>
        public IBugstarConfig BugstarConfig
        {
            get;
            private set;
        }

        /// <summary>
        /// Automation Core Services configuration data.
        /// </summary>
        public IAutomationCoreServices AutomationCoreServices
        {
            get;
            private set;
        }

        /// <summary>
        /// Recent files list; Filenames and IOpenService GUID.
        /// </summary>
        public List<KeyValuePair<String, Guid>> RecentFiles
        {
            get;
            private set;
        }
        #endregion // Properties

        #region MEF Imports
        /// <summary>
        /// MEF import for Workbench messaging service.
        /// </summary>
        [ImportExtension(CompositionPoints.MessageService, typeof(IMessageService))]
        private IMessageService MessageService { get; set; }
        #endregion // MEF Imports

        #region Static Member Data
        /// <summary>
        /// 
        /// </summary>
        private static int NewFileCounter = 1;
        #endregion // Static Member Data

        #region Constructor(s) / Destructor
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ConfigurationService()
        {
            this.GameConfig = new RSG.Base.ConfigParser.ConfigGameView();
        }

        /// <summary>
        /// Destructor.
        /// </summary>
        ~ConfigurationService()
        {
            if (null != this.GameConfig)
                this.GameConfig.Dispose();
        }
        #endregion // Constructor(s) / Destructor

        #region IPartImportsSatisfiedNotification Interface Methods
        /// <summary>
        /// MEF imports satisfied.
        /// </summary>
        public void OnImportsSatisfied()
        {
            Log.Log__Profile("ConfigurationService OnImportsSatisfied()");

            try
            {
                this.Config = ConfigFactory.CreateConfig();
                this.ReportsConfig = ConfigFactory.CreateReportConfig(this.Config.Project);
                this.BugstarConfig = ConfigFactory.CreateBugstarConfig(this.Config);
                this.AutomationCoreServices = ConfigFactory.CreateAutomationConfig(this.Config.Project);
            }
            catch (ConfigurationVersionException ex)
            {
                Log.Log__Exception(ex, "Configuration version error: run tools installer.  Installed version: {0}, expected version: {1}.", 
                    ex.ActualVersion, ex.ExpectedVersion);
                MessageService.Show(String.Format("An important update has been made to the tool chain.  Install version: {0}, expected version: {1}.{2}{2}Sync latest or labelled tools, run {3} and then restart the application.",
                    ex.ActualVersion, ex.ExpectedVersion, Environment.NewLine, 
                    Path.Combine(Environment.GetEnvironmentVariable("RS_TOOLSROOT"), "install.bat")),
                    MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1);
            }
            catch (ConfigurationException ex)
            {
                Log.Log__Exception(ex, "Configuration parse error.");
                MessageService.Show(String.Format("There was an error initialising configuration data.{0}{0}Sync latest or labelled tools and restart the application.",
                    Environment.NewLine), MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1);
            }

            Log.Log__ProfileEnd();
        }
        #endregion // IPartImportsSatisfiedNotification Interface Methods

        #region Methods
        /// <summary>
        /// Returns next new file filename (basename).
        /// </summary>
        /// <returns></returns>
        public String GetNewFilename()
        {
            return (String.Format("{0}{1}", 
                Properties.Settings.Default.NewFilePrefix, NewFileCounter++));
        }
        #endregion // Methods
    }

} // Workbench.Services namespace
