﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Report.Reports.GameStatsReports.Tests;
using RSG.Model.Statistics.Captures;
using RSG.Base.Editor.Command;

namespace Workbench.AddIn.GameStatistics.Reports.Automated.ViewModels
{
    public class PedVehicleSmokeTestViewModel : SmokeTestViewModelBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public IList<PedAndVehicleResult> LatestResults
        {
            get { return m_latestResults; }
            set
            {
                SetPropertyValue(value, () => this.LatestResults,
                          new PropertySetDelegate(delegate(object newValue) { m_latestResults = (IList<PedAndVehicleResult>)newValue; }));
            }
        }
        private IList<PedAndVehicleResult> m_latestResults;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public PedVehicleSmokeTestViewModel(PedAndVehicleSmokeTest smokeTest, string testName)
            : base(smokeTest, testName)
        {
            if (smokeTest.LatestStats.ContainsKey(testName))
            {
                LatestResults = smokeTest.LatestStats[testName];
            }
        }
        #endregion // Constructor(s)

    }
}
