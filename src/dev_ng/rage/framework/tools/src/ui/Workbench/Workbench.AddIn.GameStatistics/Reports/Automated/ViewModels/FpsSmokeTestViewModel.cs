﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Report.Reports.GameStatsReports.Tests;
using RSG.Base.Editor.Command;

namespace Workbench.AddIn.GameStatistics.Reports.Automated.ViewModels
{
    /// <summary>
    /// 
    /// </summary>
    public class FpsSmokeTestViewModel : SmokeTestViewModelBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public IList<FpsSmokeTest.FpsInfo> FpsResults
        {
            get { return m_fpsResults; }
            set
            {
                SetPropertyValue(value, () => this.FpsResults,
                          new PropertySetDelegate(delegate(object newValue) { m_fpsResults = (IList<FpsSmokeTest.FpsInfo>)newValue; }));
            }
        }
        private IList<FpsSmokeTest.FpsInfo> m_fpsResults;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public FpsSmokeTestViewModel(FpsSmokeTest smokeTest, string testName)
            : base(smokeTest, testName)
        {
            if (smokeTest.GroupedFpsInfo.ContainsKey(testName))
            {
                FpsResults = smokeTest.GroupedFpsInfo[testName];
            }
        }
        #endregion // Constructor(s)
    } // FpsSmokeTestViewModel
}
