﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;

namespace MetadataEditor.AddIn.KinoEditor.Subtitle_Editor
{
    /// <summary>
    /// Interaction logic for EventTimeLine.xaml
    /// </summary>
    public partial class EventTimeLine : UserControl
    {
        public EventTimeLine()
        {
            InitializeComponent();
            InvalidateVisual();
        }

        protected override void OnRender(DrawingContext dc)
        {
            Pen pen = new Pen();
            pen.Brush = Brushes.Gray;

            for (int i = 0; i < ActualWidth; i += 30) // Every Second
            {
                dc.DrawLine(pen, new Point(i, 0), new Point(i, 5));
            }

            //pen.Brush = Brushes.Black;

            for (int i = 0; i < ActualWidth; i+=60) // Every 2 Seconds
            {
                dc.DrawLine(pen, new Point(i, 0), new Point(i, 15));

                FormattedText formattedText = new FormattedText(
                        (i/30).ToString(),
                        CultureInfo.GetCultureInfo("en-us"),
                        FlowDirection.LeftToRight,
                        new Typeface("Segoe UI"),
                        12,
                        Brushes.Black
                        );

                dc.DrawText(formattedText, new Point(i+3, 3));
            }

            

            //base.OnRender(dc);
        }
    }
}
