﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WidgetEditor.ViewModel;
using ragWidgets;
using RSG.Base.Logging;

namespace WidgetEditor.View
{
    /// <summary>
    /// Interaction logic for TreeViewEditor.xaml
    /// </summary>
    public partial class WidgetList : UserControl
    {
        public WidgetList()
        {
            InitializeComponent();
        }
    }

    public class ItemTemplateSelector : DataTemplateSelector
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <param name="container"></param>
        /// <returns></returns>
        public override DataTemplate SelectTemplate(Object item, DependencyObject container)
        {
            ContentPresenter cp = container as ContentPresenter;

            WidgetViewModel vm = item as WidgetViewModel;
            Type widgetType = vm.Model.GetType();
            string typeName = widgetType.Name;

            DataTemplate dt = cp.TryFindResource( typeName ) as DataTemplate;
            if ( dt != null )
                return dt;

            //    Log.Log__Debug( "Didn't find resource for widget {0}, type={1}", vm.Model.ActualWidget.Path, typeName );

            
            if ( vm.Model is WidgetGroupBase )
            {
                dt = cp.FindResource( "WidgetGroupBase2" ) as DataTemplate;
            }
            else if ( vm.Model is WidgetToggle )
            {
                dt = cp.FindResource( "WidgetToggle" ) as DataTemplate;
            }
            else
            {
                dt = cp.FindResource( "GenericWidget" ) as DataTemplate;
            }

            
            return dt;
        }
    } // ItemTemplateSelector


}
