﻿using RSG.Model.Report;

namespace Workbench.AddIn.VehicleStatistics.Reports
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(ExtensionPoints.Report, typeof(IReport))]
    class VehicleTextureReport : RSG.Model.Report.Reports.Vehicles.VehicleTextureReport
    {
    } // VehicleTextureReport
}
