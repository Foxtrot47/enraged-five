﻿using System;
using System.Windows;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Threading;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using RSG.Model.Common;
using RSG.Base.Windows.DragDrop;

namespace ContentBrowser.ViewModel
{
    /// <summary>
    /// View model class for assets that implement the IHasAssetChildren interface
    /// </summary>
    public class AssetContainerViewModelBase : ObservableContainerViewModelBase<IAsset>
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// A reference to the asset container base model
        /// that this view model wraps
        /// </summary>
        public IHasAssetChildren HasAssetChildrenModel
        {
            get { return m_hasAssetChildrenModel; }
            set
            {
                if (m_hasAssetChildrenModel == value)
                    return;

                if (this.HasAssetChildrenModel != null)
                {
                    PropertyChangedEventManager.RemoveListener(this.HasAssetChildrenModel, this, "AssetChildren");
                }
                SetPropertyValue(value, () => HasAssetChildrenModel,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_hasAssetChildrenModel = (IHasAssetChildren)newValue;
                            MonitoredCollection = m_hasAssetChildrenModel.AssetChildren;
                        }
                ));

                if (value != null)
                {
                    PropertyChangedEventManager.AddListener(value, this, "AssetChildren");
                }
            }
        }
        private IHasAssetChildren m_hasAssetChildrenModel;
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public AssetContainerViewModelBase(IHasAssetChildren model)
            : base(model, model.AssetChildren)
        {
            HasAssetChildrenModel = model;
        }
        #endregion // Constructor(s)

        #region IWeakEventListener
        /// <summary>
        /// 
        /// </summary>
        public override Boolean ReceiveWeakEvent(Type managerType, Object sender, EventArgs e)
        {
            if (managerType == typeof(PropertyChangedEventManager))
            {
                PropertyChangedEventArgs eventArgs = e as PropertyChangedEventArgs;
                if (eventArgs.PropertyName == "AssetChildren")
                {
                    if (Application.Current != null)
                    {
                        Application.Current.Dispatcher.Invoke(
                            new Action
                            (
                                delegate()
                                {
                                    MonitoredCollection = this.HasAssetChildrenModel.AssetChildren;
                                    this.OnExpansionChanged(!this.IsExpanded, this.IsExpanded);
                                    this.OnSelectionChanged(!this.IsSelected, this.IsSelected);
                                }
                            ),
                            System.Windows.Threading.DispatcherPriority.ContextIdle
                        );
                    }
                    else
                    {
                        MonitoredCollection = this.HasAssetChildrenModel.AssetChildren;
                        this.OnExpansionChanged(!this.IsExpanded, this.IsExpanded);
                        this.OnSelectionChanged(!this.IsSelected, this.IsSelected);
                    }
                }
            }

            return base.ReceiveWeakEvent(managerType, sender, e);
        }
        #endregion // IWeakEventListener
    } // AssetContainerViewModelBase
}
