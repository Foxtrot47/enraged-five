﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.GlobalTXD;

namespace Workbench.AddIn.TXDParentiser.ViewModel
{
    public interface IDictionaryChildViewModel
    {
        /// <summary>
        /// The parent container that conatins this object
        /// </summary>
        IDictionaryContainerViewModel Parent { get; }

        /// <summary>
        /// The root object for this object
        /// </summary>
        GlobalRootViewModel Root { get; }

        void ExpandedTo();
    }
} // Workbench.AddIn.TXDParentiser.ViewModel
