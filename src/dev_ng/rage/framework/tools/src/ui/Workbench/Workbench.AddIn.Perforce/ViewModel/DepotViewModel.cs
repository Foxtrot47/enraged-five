﻿using System;
using RSG.Base.Editor;
using RSG.SourceControl.Perforce;

namespace Workbench.AddIn.Perforce.ViewModel
{

    /// <summary>
    /// 
    /// </summary>
    internal class DepotViewModel : ViewModelBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public String Name
        {
            get { return m_Depot.Name; }
        }

        /// <summary>
        /// 
        /// </summary>
        private Depot m_Depot;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="depot"></param>
        public DepotViewModel(Depot depot)
        {
            this.m_Depot = depot;
        }
        #endregion // Constructor(s)
    }

} // Workbench.AddIn.Perforce.ViewModel
