﻿using System;
using RSG.Base.Tasks;
using RSG.Model.Common;
using RSG.Model.Weapon;
using Workbench.AddIn.Services;

namespace ContentBrowser.ViewModel.WeaponViewModels
{
    /// <summary>
    /// 
    /// </summary>
    internal class WeaponViewModel : AssetContainerViewModelBase
    {
        #region Fields
        private object m_syncObject = new object();
        private bool m_created = false;

        /// <summary>
        /// 
        /// </summary>
        private ITaskProgressService TaskProgressService { get; set; }
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public WeaponViewModel(IWeapon weapon, ITaskProgressService taskProgressService)
            : base(weapon)
        {
            TaskProgressService = taskProgressService;
        }
        #endregion // Constructor(s)
        
        /// <summary>
        /// This gets called whenever the expansion state for this view model
        /// has changed
        /// </summary>
        protected override void OnExpansionChanged(Boolean oldValue, Boolean newValue)
        {
            lock (m_syncObject)
            {
                if (!m_created)
                {
                    m_created = true;
                    ITask actionTask = new ActionTask("Weapon Load", (context, progress) => (this.Model as IWeapon).LoadAllStats());
                    TaskProgressService.Add(actionTask, new TaskContext(), TaskPriority.Background);
                }
            }
            base.OnExpansionChanged(oldValue, newValue);
        }

        /// <summary>
        /// Get called when a level is selected. When this happens need to create the file asset
        /// view models for the grid view to display. Since this needs to happen dynamically when it
        /// gets deselected need to destroy them completely.
        /// </summary>
        protected override void OnSelectionChanged(Boolean oldValue, Boolean newValue)
        {
            lock (m_syncObject)
            {
                if (!m_created)
                {
                    m_created = true;
                    ITask actionTask = new ActionTask("Weapon Load", (context, progress) => (this.Model as IWeapon).LoadAllStats());
                    TaskProgressService.Add(actionTask, new TaskContext(), TaskPriority.Background);
                }
            }
            base.OnSelectionChanged(oldValue, newValue);
        }

    } // WeaponViewModel
} // ContentBrowser.ViewModel.WeaponViewModels
