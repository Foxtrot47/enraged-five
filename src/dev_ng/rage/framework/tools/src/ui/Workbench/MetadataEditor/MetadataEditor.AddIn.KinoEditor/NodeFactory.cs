﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace MetadataEditor.AddIn.KinoEditor
{
    class NodeFactory
    {
        #region Data

        DragCanvas _dragCanvas = null;
       
        #endregion

        #region Constructor
        
        public NodeFactory(DragCanvas canvas)
        {
            _dragCanvas = canvas;
        }

        #endregion

        #region Interface

        public void CreateChild(UIElement child, Point position)
        {
            _dragCanvas.Children.Add(child);
            DragCanvas.SetLeft(child, position.X);
            DragCanvas.SetTop(child, position.Y);
        }

        #endregion
    }
}
