﻿using System;
using System.Diagnostics;
using System.ComponentModel;
using System.Windows.Input;

namespace ContentBrowser.AddIn
{
    /// <summary>
    /// Interface for a content browser command
    /// </summary>
    public interface IContentBrowserCommand : ICommand, INotifyPropertyChanged
    {
        #region Properties

        String Description { get; }

        Object Image { get; }

        #endregion // Properties
    } // IContentBrowserCommand
} // ContentBrowser.AddIn
