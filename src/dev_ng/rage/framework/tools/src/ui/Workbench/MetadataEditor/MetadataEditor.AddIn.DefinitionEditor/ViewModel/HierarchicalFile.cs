﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using SIO = System.IO;
using System.Windows.Media.Imaging;
using RSG.Metadata.Model;
using RSG.Metadata.Parser;

namespace MetadataEditor.AddIn.DefinitionEditor.ViewModel
{

    /// <summary>
    /// 
    /// </summary>
    internal class HierarchicalFile :
        RSG.Base.Editor.HierarchicalViewModelBase
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// 
        /// </summary>
        public String Path { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public String Alias { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<Object> Children { get; set; }
        #endregion // Properties and Associated Member Data
    
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="defDict"></param>
        public HierarchicalFile(String path)
        {
            String[] parts = path.Split(
                new char[] { SIO.Path.DirectorySeparatorChar, SIO.Path.AltDirectorySeparatorChar },
                StringSplitOptions.RemoveEmptyEntries);
            String alias = parts[parts.Length - 1];
            Initialise(path, alias);
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="alias"></param>
        /// <param name="defDict"></param>
        public HierarchicalFile(String path, String alias)
        {
            Initialise(path, alias);
        }
        #endregion // Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(Object obj)
        {
            if (!(obj is HierarchicalFile))
                return (false);
            HierarchicalFile other = (obj as HierarchicalFile);
            return (0 == String.Compare(this.Path, other.Path, true));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public HierarchicalFile FindFile(String path)
        {
            // Base case.
            if (0 == String.Compare(Path, path))
                return (this);

            foreach (Object item in this.Children)
            {
                if (!(item is HierarchicalFile))
                    continue;

                HierarchicalFile folder = (item as HierarchicalFile);
                HierarchicalFile found = (folder.FindFile(path));
                if (null != found)
                    return (found);
            }

            // Not found.
            return (null);
        }

        /// <summary>
        /// Create root HierarchicalFolder object for path.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static HierarchicalFile Create(String path, StructureDictionary defDict)
        {
            Debug.Assert(SIO.File.Exists(path),
                String.Format("File {0} does not exist.", path));

            return (new HierarchicalFile(path));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <param name="alias"></param>
        /// <param name="defDict"></param>
        private void Initialise(String path, String alias)
        {
            this.Children = new ObservableCollection<Object>();
            this.Path = (path.Clone() as String);
            this.Alias = (alias.Clone() as String);
        }
    }

} // MetadataEditor.AddIn.DefinitionEditorViewModel.ViewModel namespace
