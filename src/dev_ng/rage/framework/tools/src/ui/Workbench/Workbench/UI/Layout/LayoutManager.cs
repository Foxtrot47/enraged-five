﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using RSG.Base.Logging;
using Workbench.AddIn;
using Workbench.AddIn.UI.Layout;
using AvalonDock;
using System.IO;
using System.Text;
using System.Xml;

namespace Workbench.UI.Layout
{
    /// <summary>
    /// Workbench window layout manager.
    /// </summary>
    [ExportExtension(Workbench.AddIn.CompositionPoints.LayoutManager,
        typeof(ILayoutManager))]
    class LayoutManager : ILayoutManager
    {
        #region Constants

        public static readonly String DefaultLayoutID = "6AF187A4-37A2-4598-868B-E4C0B3ED2D76";
        public static readonly String DefaultLayout =
            "<DockingManager version=\"1.3.0\">" +
                "<ResizingPanel ResizeWidth=\"*\" ResizeHeight=\"*\" EffectiveSize=\"0,0\" Orientation=\"Vertical\">" +
                    "<ResizingPanel ResizeWidth=\"*\" ResizeHeight=\"*\" EffectiveSize=\"1600,923.02\" Orientation=\"Horizontal\">" +
                        "<DocumentPane IsMain=\"true\" ResizeWidth=\"*\" ResizeHeight=\"*\" EffectiveSize=\"1600,923.02\" />" +
                            "<DockablePane ResizeWidth=\"200\" ResizeHeight=\"*\" EffectiveSize=\"200,686.04\" ID=\"55b4e51f-6b95-481e-88d1-72633530eac6\" Anchor=\"Right\" IsAutoHidden=\"false\" />" +
                     "</ResizingPanel>" +
                    "<DockablePane ResizeWidth=\"*\" ResizeHeight=\"205.02\" EffectiveSize=\"1600,205.02\" ID=\"433fdf38-b93b-4b27-be6f-a3abd8f7f0ea\" Anchor=\"Bottom\" IsAutoHidden=\"false\">" +
                        "<DockableContent Name=\"Universal_Log_Output\" FloatingWindowSize=\"250,400\" ChildIndex=\"0\" Width=\"200\" Height=\"686.04\" Anchor=\"Right\" State=\"Docked\" ContainerPaneID=\"55b4e51f-6b95-481e-88d1-72633530eac6\" />" +
                    "</DockablePane>" +
                "</ResizingPanel>" +
                "<Hidden />" +
                "<Windows />" +
            "</DockingManager>";

        public static readonly String ClearedLayoutID = "353199FD-9221-4B47-A2D5-365BFEF43E9D";
        public static readonly String ClearedLayout =
            "<DockingManager version=\"1.3.0\">" +
                "<DocumentPane IsMain=\"true\" ResizeWidth=\"*\" ResizeHeight=\"*\" EffectiveSize=\"1243,1134.04\" />" +
                "<Hidden />" +
                "<Windows />" +
            "</DockingManager>";

        #endregion // Constants

        #region Events

        /// <summary>
        /// 
        /// </summary>
        public event EventHandler Loaded;

        /// <summary>
        /// 
        /// </summary>
        public event EventHandler Unloaded;

        /// <summary>
        /// 
        /// </summary>
        public event EventHandler LayoutUpdated;

        /// <summary>
        /// 
        /// </summary>
        public event EventHandler ActiveDocumentChanged;

        /// <summary>
        /// 
        /// </summary>
        public event EventHandler ActiveDocumentChanging;

        /// <summary>
        /// 
        /// </summary>
        public event EventHandler ActiveContentChanged;

        /// <summary>
        /// 
        /// </summary>
        public event ContentOpenedHandler ContentOpened;

        /// <summary>
        /// 
        /// </summary>
        public event ContentClosedHandler ContentClosed;

        /// <summary>
        /// 
        /// </summary>
        public event NewDocumentCreatedHandler DocumentCreated;
        
        /// <summary>
        /// 
        /// </summary>
        public event DocumentClosed DocumentClosed;

        /// <summary>
        /// 
        /// </summary>
        public event EventHandler Exiting;

        #endregion // Events
        
        #region Properties and Associated Member Data

        /// <summary>
        /// Avalon Docking Manager object.
        /// </summary>
        public AvalonDock.DockingManager DockManager
        {
            get { return m_DockManager; }
        }
        private AvalonDock.DockingManager m_DockManager = new AvalonDock.DockingManager();

        /// <summary>
        /// Collection of workbench Documents
        /// </summary>
        public ReadOnlyCollection<IDocumentBase> Documents 
        {
            get { return new ReadOnlyCollection<IDocumentBase>(m_documents); } 
        }
        private readonly List<IDocumentBase> m_documents = new List<IDocumentBase>();

        /// <summary>
        /// Collection of workbench Toolwindows
        /// </summary>
        public ReadOnlyCollection<IToolWindowBase> ToolWindows
        {
            get { return new ReadOnlyCollection<IToolWindowBase>(m_ToolWindows.ToList()); }
        }
        private readonly List<IToolWindowBase> m_ToolWindows = new List<IToolWindowBase>();

        /// <summary>
        /// Tool window proxys, so that we can create tool windows that are being deserialized
        /// </summary>
        [ImportManyExtension(Workbench.AddIn.ExtensionPoints.ToolWindowProxy, typeof(IToolWindowProxy))]
        private IEnumerable<IToolWindowProxy> ToolWindowProxies { get; set; }

        /// <summary>
        /// A dictionary of the saved layouts from the settings file that can be used to create the
        /// window layout buttons.
        /// </summary>
        public Dictionary<String, String> SavedLayouts
        {
            get { return m_savedLayouts; }
            set { m_savedLayouts = value; }
        }
        Dictionary<String, String> m_savedLayouts = new Dictionary<String, String>();

        #endregion // Properties and Associated Member Data

        #region Member Data

        /// <summary>
        /// Document pane for Documents.
        /// </summary>
        private DocumentPane m_DocPane = new DocumentPane();
        
        /// <summary>
        /// Resizable panel for tool windows.
        /// </summary>
        private ResizingPanel m_ToolPanel = new ResizingPanel();

        #endregion // Member Data

        #region Constructor(s)

        /// <summary>
        /// Default constructor.
        /// </summary>
        public LayoutManager()
        {
            m_DocPane.Name = "DocumentPane";
            m_ToolPanel.Orientation = System.Windows.Controls.Orientation.Vertical;
            m_ToolPanel.Name = "ToolPanel";
            m_ToolPanel.Children.Add(m_DocPane);
            DockManager.Content = m_ToolPanel;
            DockManager.Loaded += this.DockManager_Loaded;
            DockManager.Unloaded += this.DockManager_Unloaded;
            DockManager.LayoutUpdated += this.DockManager_LayoutUpdated;
            DockManager.ActiveDocumentChanged += this.DockManager_ActiveDocumentChanged;
            DockManager.ActiveContentChanged += this.DockManager_ActiveContentChanged;
            DockManager.DeserializationCallback = DeserializationCallback;

            if (Properties.Settings.Default.SavedLayouts != null)
            {
                String[] savedLayouts = new String[Properties.Settings.Default.SavedLayouts.Count];
                Properties.Settings.Default.SavedLayouts.CopyTo(savedLayouts, 0);
                Properties.Settings.Default.SavedLayouts.Clear();
                foreach (String layout in savedLayouts)
                {
                    if (layout.StartsWith("<name=\""))
                    {
                        int length = layout.IndexOf("\">") - 7;
                        String key = layout.Substring(7, length);

                        int valueStart = layout.IndexOf("<", 7);
                        String value = layout.Substring(valueStart);

                        if (!this.SavedLayouts.ContainsKey(key))
                        {
                            this.SavedLayouts[key] = value;
                            Properties.Settings.Default.SavedLayouts.Add(layout);
                        }
                    }
                }
                Properties.Settings.Default.Save();
            }
        }

        #endregion // Constructor(s)

        #region Controller Methods

        #region Content Methods

        public IContentBase GetActiveContent()
        {
            if (DockManager.ActiveContent != null)
                return (DockManager.ActiveContent as IContentBase);
            else
                return null;
        }

        #endregion // Content Methods

        #region ToolWindow Methods

        /// <summary>
        /// Show the specified IToolWindowBase
        /// </summary>
        public void ShowToolWindow(IToolWindowBase window)
        {
            if (!ToolWindows.Contains(window))
            {
                m_ToolWindows.Add(window);
                DockablePane pane = new DockablePane();
                pane.Items.Add(window);
                m_ToolPanel.Children.Add(pane);

                window.Closed += ToolWindow_Closed;
                window.GotFocus += window.OnFocus;
                window.LostFocus += window.OnLostFocus;
            }

            DockManager.Show(window);
            DockManager.ActiveContent = window;
            if (this.ContentOpened != null)
                this.ContentOpened(window);
        }

        /// <summary>
        /// Show the specified IToolWindowBase
        /// </summary>
        public void ShowAsFloatingWindow(IToolWindowBase window)
        {
            if (!ToolWindows.Contains(window))
            {
                window.Closed += ToolWindow_Closed;
                window.GotFocus += window.OnFocus;
                window.LostFocus += window.OnLostFocus;
            }

            window.ShowAsFloatingWindow(true);
            DockManager.ActiveContent = window;
            if (this.ContentOpened != null)
                this.ContentOpened(window);
        }

        /// <summary>
        /// Show the specified IToolWindowBase
        /// </summary>
        public void ShowAsDocumentWindow(IToolWindowBase window)
        {
            if (!ToolWindows.Contains(window))
            {
                window.Closed += ToolWindow_Closed;
                window.GotFocus += window.OnFocus;
                window.LostFocus += window.OnLostFocus;
                m_ToolWindows.Add(window);
            }

            window.ShowAsDocument(this.DockManager);
            DockManager.ActiveContent = window;
            if (this.ContentOpened != null)
                this.ContentOpened(window);
        }

        /// <summary>
        /// Hide the specified IToolWindowBase.
        /// </summary>
        public void HideToolWindow(IToolWindowBase window)
        {
            Debug.Assert(ToolWindows.Contains(window), "Internal error: layout manager not aware of tool windows to be hidden.");

            if (!ToolWindows.Contains(window))
                return;

            DockManager.Hide(window);
        }

        /// <summary>
        /// Determine whether the specified IToolWindowBase is visible.
        /// </summary>
        public bool IsVisible(IToolWindowBase window)
        {
            if (ToolWindows.Contains(window))
            {
                return (DockableContentState.Hidden != window.State);
            }
            return false;
        }

        /// <summary>
        /// Hide all IToolWindowBases
        /// </summary>
        public void HideAllToolWindows()
        {
            foreach (IToolWindowBase window in ToolWindows)
            {
                DockManager.Hide(window);
            }
        }

        #endregion // ToolWindow Methods

        #region Document Methods

        /// <summary>
        /// Show the specified IDocument.
        /// </summary>
        public void ShowDocument(IDocumentBase document)
        {
            Debug.Assert(document != null, "Invalid document specified.");
            if (document == null)
                return;

            if (!String.IsNullOrEmpty(document.Path))
            {
                foreach (IDocumentBase openedDocument in m_documents)
                {
                    if (openedDocument.Path == document.Path)
                    {
                        this.DockManager.ActiveDocument = openedDocument;
                        this.DockManager.ActiveContent = openedDocument;
                        return;
                    }
                }
            }

            // Not currently opened; needs to be a whole new document
            bool newDocument = false;
            if (!this.m_documents.Contains(document))
            {
                newDocument = true;
                DocumentContent content = document;
                m_documents.Add(document);
                this.m_DocPane.Items.Add(content);

                content.Closing += document.OnClosing;
                content.Closed += Document_Closed;
                content.GotFocus += document.OnFocus;
                content.LostFocus += document.OnLostFocus;

                document.OnOpened(content, EventArgs.Empty);
            }

            if (this.m_documents.Contains(document))
            {
                document.Show(this.DockManager);
                this.DockManager.ActiveDocument = document;
                this.DockManager.ActiveContent = document;
            }

            if (newDocument)
            {
                if (this.ContentOpened != null)
                    this.ContentOpened(document);
                if (this.DocumentCreated != null)
                    this.DocumentCreated(document);
            }
        }
        
        /// <summary>
        /// Close the specified IDocument.
        /// </summary>
        public void CloseDocument(IDocumentBase document)
        {
            Debug.Assert(m_documents.Contains(document), "Internal error: layout manager not aware of document to be closed.");
            if (!m_documents.Contains(document))
                return;

            document.Close();
        }

        /// <summary>
        /// Close all opened documents.
        /// </summary>
        public void CloseAllDocuments()
        {
            List<IDocumentBase> cache = new List<IDocumentBase>(this.m_documents);
            foreach (IDocumentBase document in cache)
            {
                document.Close();
            }

            if (!this.m_documents.Contains(this.DockManager.ActiveDocument))
            {
                this.DockManager.ActiveDocument = null;
            }
        }

        /// <summary>
        /// Return the currently active IDocument.
        /// </summary>
        public IDocumentBase ActiveDocument()
        {
            if (DockManager.ActiveDocument != null)
                return (DockManager.ActiveDocument as IDocumentBase);
            else
                return null;
        }

        /// <summary>
        /// Determine whether the specified IDocument is active.
        /// </summary>
        public bool IsActiveDocument(IDocumentBase document)
        {
            return document.IsActiveDocument;
        }

        public bool IsActiveContent(IDocumentBase document)
        {
            return document.IsActiveContent;
        }

        /// <summary>
        /// Determine whether the specified IDocument is visible.
        /// </summary>
        public bool IsVisible(IDocumentBase document)
        {
            if (m_documents.Contains(document))
            {
                return document.IsVisible;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Create a new horizontal tab group for the specified document.
        /// </summary>
        /// <param name="document"></param>
        public void NewHorizontalTabGroup(IDocumentBase document)
        {
            if (this.Documents.Count >= 2 && this.Documents.Contains(document))
            {
                ManagedContent activeContent = document;
                DocumentPane newContainerPane = new DocumentPane();

                int indexOfDocumentInItsContainer = activeContent.ContainerPane.Items.IndexOf(activeContent);
                activeContent.ContainerPane.RemoveContent(indexOfDocumentInItsContainer);
                newContainerPane.Items.Add(activeContent);

                this.DockManager.Anchor(newContainerPane, DockManager.MainDocumentPane, AnchorStyle.Right);
                this.DockManager.ActiveDocument = document;
            }
        }

        /// <summary>
        /// Create a new vertical tab group for the specified document.
        /// </summary>
        /// <param name="document"></param>
        public void NewVerticalTabGroup(IDocumentBase document)
        {
            if (this.Documents.Count >= 2 && this.Documents.Contains(document))
            {
                ManagedContent activeContent = document;
                DocumentPane newContainerPane = new DocumentPane();

                int indexOfDocumentInItsContainer = activeContent.ContainerPane.Items.IndexOf(activeContent);
                activeContent.ContainerPane.RemoveContent(indexOfDocumentInItsContainer);
                newContainerPane.Items.Add(activeContent);

                this.DockManager.Anchor(newContainerPane, DockManager.MainDocumentPane, AnchorStyle.Bottom);
                this.DockManager.ActiveDocument = document;
            }
        }

        #endregion // Document Methods

        #region Layout Serialisation
        
        /// <summary>
        /// 
        /// </summary>
        public String SaveLayout(String name)
        {
            StringBuilder builder = new StringBuilder(String.Empty);
            StringWriter writer = new StringWriter(builder);
            DockManager.SaveLayout(writer);

            this.SavedLayouts[name] = builder.ToString();
            SaveLayouts();

            return (builder.ToString());
        }

        /// <summary>
        /// Saves the currently saved layouts in the user settings
        /// </summary>
        public void SaveLayouts()
        {
            StringCollection layouts = new StringCollection();
            foreach (KeyValuePair<String, String> layout in this.SavedLayouts)
            {
                String layoutString = "<name=\"" + layout.Key + "\">\r\n" + layout.Value;
                layouts.Add(layoutString);
            }

            Properties.Settings.Default.SavedLayouts = layouts;
            Properties.Settings.Default.Save();
        }

        /// <summary>
        /// 
        /// </summary>
        public void RestoreLayout(String blob)
        {
            try
            {
                StringReader reader = new StringReader(blob);
                DockManager.RestoreLayout(reader);
            }
            catch
            {
                RSG.Base.Logging.Log.Log__Error("Unable to load dock layout going to default.");
                StringReader reader = new StringReader(DefaultLayout);
                DockManager.RestoreLayout(reader);
            }
        }

        /// <summary>
        /// Opens the tool windows defined the the specified blob.
        /// </summary>
        /// <param name="blob"></param>
        public void MergeToolWindows(String blob)
        {
            try
            {
                List<string> toolWindows = new List<string>();
                using (StringReader stringReader = new StringReader(blob))
                using (XmlReader reader = XmlReader.Create(stringReader))
                {
                    while (reader.MoveToContent() != XmlNodeType.None)
                    {
                        if (!reader.IsStartElement() || reader.Name != "DockableContent")
                        {
                            reader.Read();
                            continue;
                        }

                        string name = reader.GetAttribute("Name");
                        if (string.IsNullOrWhiteSpace(name))
                        {
                            continue;
                        }

                        foreach (IToolWindowProxy proxy in this.ToolWindowProxies.Where(p => p.Name == name))
                        {
                            IToolWindowBase window = proxy.GetToolWindow();
                            if (window != null)
                            {
                                this.ShowAsDocumentWindow(window);
                                break;
                            }
                        }

                        reader.Read();
                    }
                }
            }
            catch
            {
                RSG.Base.Logging.Log.Log__Error("Unable to merge dock layout.");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void RestoreSavedLayout(String name)
        {
            try
            {
                if (this.SavedLayouts.ContainsKey(name))
                {
                    StringReader reader = new StringReader(this.SavedLayouts[name]);
                    DockManager.RestoreLayout(reader);
                }
            }
            catch
            {
                RSG.Base.Logging.Log.Log__Error("Unable to load dock layout going to default.");
                StringReader reader = new StringReader(DefaultLayout);
                DockManager.RestoreLayout(reader);
            }
        }

        /// <summary>
        /// Returns the layout as it would be having been
        /// serialised.
        /// </summary>
        public String GetLayoutSerialization()
        {
            StringBuilder builder = new StringBuilder(String.Empty);
            StringWriter writer = new StringWriter(builder);
            DockManager.SaveLayout(writer);

            return builder.ToString();
        }

        #endregion // Layout Serialisation

        /// <summary>
        /// 
        /// </summary>
        public void OnExit()
        {
            if (this.Exiting != null)
                this.Exiting(this, EventArgs.Empty);
        }

        #endregion // Controller Methods

        #region Private Methods

        #region DockManager Event Handlers

        private void DeserializationCallback(Object sender, DeserializationCallbackEventArgs e)
        {
            string name = e.Name;
            if (name == "Output")
                name = "Universal_Log_Output";

            foreach (IToolWindowProxy proxy in this.ToolWindowProxies.Where(p => p.Name == name))
            {
                IToolWindowBase window = proxy.GetToolWindow();
                if (window != null)
                {
                    // Just take the first valid one
                    e.Content = window;
                    if (!ToolWindows.Contains(window))
                    {
                        m_ToolWindows.Add(window);

                        window.Closed += ToolWindow_Closed;
                        window.GotFocus += window.OnFocus;
                        window.LostFocus += window.OnLostFocus;
                    }

                    if (this.ContentOpened != null)
                        this.ContentOpened(window);
                    break;
                }
            }
        }

        /// <summary>
        /// Pass through the 'Loaded' event.
        /// </summary>
        private void DockManager_Loaded(Object sender, RoutedEventArgs e)
        {
            if (null != Loaded)
                Loaded(this, e);
        }

        /// <summary>
        /// Pass through the 'Unloaded' event.
        /// </summary>
        private void DockManager_Unloaded(Object sender, RoutedEventArgs e)
        {
            if (null != Unloaded)
                Unloaded(this, e);
        }

        /// <summary>
        /// Pass through the 'LayoutUpdated' event.
        /// </summary>
        private void DockManager_LayoutUpdated(Object sender, EventArgs e)
        {
            if (null != LayoutUpdated)
                LayoutUpdated(this, e);
        }

        /// <summary>
        /// Pass through the 'ActiveDocumentChanged' event.
        /// </summary>
        private void DockManager_ActiveDocumentChanged(Object sender, EventArgs e)
        {
            if (null != ActiveDocumentChanged)
                ActiveDocumentChanged(this, e);
        }

        /// <summary>
        /// Pass through the 'ActiveDocumentChanging' event.
        /// </summary>
        private void DockManager_ActiveDocumentChanging(Object sender, EventArgs e)
        {
            if (null != ActiveDocumentChanging)
                ActiveDocumentChanging(this, e);
        }

        /// <summary>
        /// Pass through the 'ActiveContentChanged' event.
        /// </summary>
        private void DockManager_ActiveContentChanged(Object sender, EventArgs e)
        {
            System.Windows.Input.CommandManager.InvalidateRequerySuggested();

            if (null != ActiveContentChanged)
                ActiveContentChanged(this, e);
        }

        #endregion // DockManager Event Handlers

        #region Tool Window Event Handlers

        /// <summary>
        /// Tool Window closed handler;
        /// </summary>
        private void ToolWindow_Closed(Object sender, EventArgs e)
        {
            IToolWindowBase window = (sender as IToolWindowBase);

            if (this.ContentClosed != null)
                this.ContentClosed(window);
        }

        #endregion // Tool Window Event Handlers

        #region Document Event Handlers

        /// <summary>
        /// Document closed handler; removing documents from our internal data 
        /// structures, unsubscribe event handlers etc.
        /// </summary>
        private void Document_Closed(Object sender, EventArgs e)
        {
            IDocumentBase document = (sender as IDocumentBase);

            document.Closing -= document.OnClosing;
            document.Closed -= Document_Closed;
            document.GotFocus -= document.OnFocus;
            document.LostFocus -= document.OnLostFocus;

            if (m_documents.Contains(document))
                m_documents.Remove(document);

            if (this.DocumentClosed != null)
                this.DocumentClosed(document);

            document.OnClosed(sender, e);
            if (this.ContentClosed != null)
                this.ContentClosed(document);
        }
  
        #endregion // Document Event Handlers

        #endregion // Private Methods
    }

} // Workbench.UI.Layout namespace
