﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using RSG.Model.Report;
using ReportBrowser.Addin;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using RSG.Model.Report.Reports.GameStatsReports.Tests;
using Workbench.AddIn.UI.Layout;
using RSG.Base.Collections;
using Workbench.AddIn.GameStatistics.Reports.Automated.ViewModels;
using RSG.SourceControl.Perforce;
using RSG.Base.ConfigParser;
using RSG.Model.Statistics.Captures;
using System.ComponentModel;
using System.IO;
using RSG.Base.Logging;
using RSG.Base.Tasks;

namespace Workbench.AddIn.GameStatistics.Reports.Automated
{
    /// <summary>
    /// 
    /// </summary>
    public class TestFilterViewModel : ViewModelBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public string TestName
        {
            get { return m_testName; }
            set
            {
                SetPropertyValue(value, () => this.TestName,
                          new PropertySetDelegate(delegate(object newValue) { m_testName = (string)newValue; }));
            }
        }
        private string m_testName;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="testName"></param>
        public TestFilterViewModel(string testName)
            : base()
        {
            TestName = testName;
        }
        #endregion // Constructor(s)
    }

    /// <summary>
    /// 
    /// </summary>
    public class SmokeTestTypeFilterViewModel : ViewModelBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get { return m_name; }
            set
            {
                SetPropertyValue(value, () => this.Name,
                          new PropertySetDelegate(delegate(object newValue) { m_name = (string)newValue; }));
            }
        }
        private string m_name;

        /// <summary>
        /// 
        /// </summary>
        public ISmokeTest SmokeTest
        {
            get { return m_smokeTest; }
            set
            {
                SetPropertyValue(value, () => this.SmokeTest,
                          new PropertySetDelegate(delegate(object newValue) { m_smokeTest = (ISmokeTest)newValue; }));
            }
        }
        private ISmokeTest m_smokeTest;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="testName"></param>
        public SmokeTestTypeFilterViewModel(string name)
            : base()
        {
            m_name = name;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="testName"></param>
        public SmokeTestTypeFilterViewModel(ISmokeTest test)
            : this(test.SmokeTestName)
        {
            m_smokeTest = test;
        }
        #endregion // Constructor(s)
    } // SmokeTestTypeFilterViewModel

    /// <summary>
    /// 
    /// </summary>
    public abstract class AutomatedTestReport : RSG.Model.Report.Report, IReportDocumentProvider, IDynamicReport, IWeakEventListener
    {
        #region Fields
        /// <summary>
        /// Default values for the customizable parameters.
        /// </summary>
        private const int c_maxGraphResults = 20;
        private const int c_maxTableResults = 10;
        private P4 perforceConnection;
        #endregion

        #region Properties
        /// <summary>
        /// Task to run for generating this report.
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (m_task == null)
                {
                    m_task = new ActionTask("Generating report", (context, progress) => GenerateReport((DynamicReportContext)context, progress));
                }
                return m_task;
            }
        }
        private ITask m_task;
        
        /// <summary>
        /// Stream containing the report data
        /// </summary>
        public IDocumentBase Document { get; protected set; }

        /// <summary>
        /// Number of entries the graph will show
        /// </summary>
        public uint MaxGraphResults { get; set; }

        /// <summary>
        /// Number of results to show in the tables
        /// </summary>
        public uint MaxTableResults { get; set; }

        /// <summary>
        /// Names of all the test components that were run.
        /// </summary>
        public ObservableCollection<TestFilterViewModel> TestNameFilters
        {
            get { return m_testNameFilters; }
            set
            {
                SetPropertyValue(value, () => this.TestNameFilters,
                          new PropertySetDelegate(delegate(object newValue) { m_testNameFilters = (ObservableCollection<TestFilterViewModel>)newValue; }));
            }
        }
        private ObservableCollection<TestFilterViewModel> m_testNameFilters;

        /// <summary>
        /// Names of all the smoke tests that were run.
        /// </summary>
        public ObservableCollection<SmokeTestTypeFilterViewModel> SmokeTestFilters
        {
            get { return m_smokeTestFilters; }
            set
            {
                SetPropertyValue(value, () => this.SmokeTestFilters,
                          new PropertySetDelegate(delegate(object newValue) { m_smokeTestFilters = (ObservableCollection<SmokeTestTypeFilterViewModel>)newValue; }));
            }
        }
        private ObservableCollection<SmokeTestTypeFilterViewModel> m_smokeTestFilters;

        /// <summary>
        /// View model to display based on the user's selection.
        /// </summary>
        public SmokeTestViewModelBase CurrentViewModel
        {
            get { return m_currentViewModel; }
            set
            {
                SetPropertyValue(value, () => this.CurrentViewModel,
                          new PropertySetDelegate(delegate(object newValue) { m_currentViewModel = (SmokeTestViewModelBase)newValue; }));
            }
        }
        private SmokeTestViewModelBase m_currentViewModel;

        /// <summary>
        /// 
        /// </summary>
        public bool NonSummaryMode
        {
            get
            {
                return (CurrentViewModel != null && CurrentViewModel.SmokeTestName != "Summary");
            }
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Path to the depot file that contains the information for this particular mission.
        /// </summary>
        protected string DepotFile { get; set; }

        /// <summary>
        /// The perforce connection object to use for p4 queries
        /// </summary>
        protected P4 PerforceConnection
        {
            get
            {
                if (this.perforceConnection != null)
                {
                    return this.perforceConnection;
                }

                this.perforceConnection = this.GetPerforceConnection();
                return this.perforceConnection;
            }
        }

        /// <summary>
        /// List of smoke tests to run as part of this report.
        /// </summary>
        protected IList<ISmokeTest> SmokeTests { get; private set; }

        /// <summary>
        /// Place to save cached p4 files to.
        /// </summary>
        protected abstract string CacheDir { get; }
        #endregion // Private Member Data
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <param name="depotFile"></param>
        /// <param name="perforceConnection"></param>
        public AutomatedTestReport(string name, string description)
            : this(name, description, null, null)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <param name="depotFile"></param>
        /// <param name="perforceConnection"></param>
        public AutomatedTestReport(string name, string description, string depotFile, P4 perforceConnection)
            : base(name, description)
        {
            DepotFile = depotFile;
            this.perforceConnection = perforceConnection;

            MaxGraphResults = c_maxGraphResults;
            MaxTableResults = c_maxTableResults;

            SmokeTests = new List<ISmokeTest>();
        }
        #endregion // Constructor(s)

        #region IDynamicReport Interface
        /// <summary>
        /// Generates the report dynamically
        /// </summary>
        private void GenerateReport(DynamicReportContext context, IProgress<TaskProgress> progress)
        {
            // Get the p4 records that will be used for generating the report
            P4API.P4RecordSet recordSet = PerforceConnection.Run("changes", "-m", Math.Max(MaxGraphResults, MaxTableResults).ToString(), DepotFile);

            // Retrieve the contents of the files and cache their contents
            string cacheDir = CacheDir.Replace("cache:", context.GameView.ToolsCacheDir);
            EnsureStatsFilesAreSynced(recordSet, cacheDir);

            // Extract the information that we are interested in
            IList<TestSession> testSessions = ProcessHistoricalStatsFiles(recordSet, cacheDir);

            // Create the view models for the filters (smoke tests, test names)
            if (testSessions.Any())
            {
                // Run the smoke tests
                foreach (ISmokeTest test in SmokeTests)
                {
                    test.RunTest(testSessions);
                }
                
                CreateFilterViewModels(testSessions.First().TestResults.Select(item => item.Name));
            }

            // Finally create a new view and set it as the document for this report (invoke this on the UI thread).
            Application.Current.Dispatcher.Invoke(
                    new Action
                    (
                        delegate()
                        {
                            Document = new AutomatedTestView(this);
                        }
                    ),
                    System.Windows.Threading.DispatcherPriority.ContextIdle
                );
        }
        #endregion // IDynamicReport Interface

        #region Methods
        #region Gathering Data
        /// <summary>
        /// 
        /// </summary>
        /// <param name="recordSet"></param>
        /// <param name="reportsCacheDir"></param>
        protected void EnsureStatsFilesAreSynced(P4API.P4RecordSet recordSet, string reportsCacheDir)
        {
            // Cache the contents of the files to the tools cache directory
            if (!Directory.Exists(reportsCacheDir))
            {
                Directory.CreateDirectory(reportsCacheDir);
            }

            // If we don't have a particular revision of the stats file retrieve and cache it
            foreach (P4API.P4Record record in recordSet)
            {
                string tempFilename = String.Format("{0}@{1}", Path.Combine(reportsCacheDir, Path.GetFileName(DepotFile)), record["change"]);
                DateTime timestamp;

                // If the file doesn't exist get the contents of the file from p4
                if (!File.Exists(tempFilename))
                {
                    P4API.P4RecordSet printRecordSet = PerforceConnection.Run("print", String.Format("{0}@{1}", DepotFile, record["change"]));

                    using (FileStream stream = File.Create(tempFilename))
                    {
                        using (TextWriter writer = new StreamWriter(stream))
                        {
                            foreach (string message in printRecordSet.Messages)
                            {
                                writer.Write(message);
                            }
                            writer.Flush();
                        }
                    }

                    // Update the creation time to the time the file was submitted to p4
                    timestamp = new DateTime(1970, 1, 1);
                    timestamp = timestamp.AddSeconds(UInt32.Parse(record["time"]));
                    File.SetCreationTime(tempFilename, timestamp);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="recordSet"></param>
        /// <param name="reportsCacheDir"></param>
        /// <returns></returns>
        protected IList<TestSession> ProcessHistoricalStatsFiles(P4API.P4RecordSet recordSet, string reportsCacheDir)
        {
            List<TestSession> testSessions = new List<TestSession>();

            foreach (P4API.P4Record record in recordSet)
            {
                string tempFilename = String.Format("{0}@{1}", Path.Combine(reportsCacheDir, Path.GetFileName(DepotFile)), record["change"]);

                // The file should really exist
                if (File.Exists(tempFilename))
                {
                    DateTime timestamp = File.GetCreationTime(tempFilename);

                    // Open up the file and add it to our list
                    using (Stream stream = File.OpenRead(tempFilename))
                    {
                        try
                        {
                            testSessions.Add(new TestSession(stream, timestamp));
                        }
                        catch (System.Exception)
                        {
                            Log.Log__Warning("Unable to parse test session data in {0}.", tempFilename);
                        }
                    }
                }
            }

            return testSessions;
        }
        #endregion // Gathering Data

        /// <summary>
        /// 
        /// </summary>
        /// <param name="testNames"></param>
        protected void CreateFilterViewModels(IEnumerable<string> testNames)
        {
            // Check to see if we need to remove any event handlers
            if (TestNameFilters != null && TestNameFilters.Any())
            {
                foreach (TestFilterViewModel vm in TestNameFilters)
                {
                    PropertyChangedEventManager.RemoveListener(vm, this, "IsSelected");
                }
            }

            // Create the new collection and populate the items
            TestNameFilters = new ObservableCollection<TestFilterViewModel>();
            TestNameFilters.AddRange(testNames.Select(item => new TestFilterViewModel(item)));

            // Check if there is one to select one by default
            if (TestNameFilters.Any())
            {
                TestNameFilters.First().IsSelected = true;
            }

            // Hook up new event handlers
            foreach (TestFilterViewModel vm in TestNameFilters)
            {
                PropertyChangedEventManager.AddListener(vm, this, "IsSelected");
            }

            // Create the new collection and populate the items
            SmokeTestFilters = new ObservableCollection<SmokeTestTypeFilterViewModel>();
            SmokeTestFilters.Add(new SmokeTestTypeFilterViewModel("Summary"));
            SmokeTestFilters.AddRange(SmokeTests.Select(item => new SmokeTestTypeFilterViewModel(item)));

            // Check if there is one to select one by default
            if (SmokeTestFilters.Any())
            {
                SmokeTestFilters.First().IsSelected = true;
            }

            // Hook up new event handlers
            foreach (SmokeTestTypeFilterViewModel vm in SmokeTestFilters)
            {
                PropertyChangedEventManager.AddListener(vm, this, "IsSelected");
            }

            OnFilterChanged(SmokeTestFilters.FirstOrDefault(item => item.IsSelected), TestNameFilters.FirstOrDefault(item => item.IsSelected));
        }

        protected abstract P4 GetPerforceConnection();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="smokeTest"></param>
        /// <returns></returns>
        protected SmokeTestViewModelBase CreateViewModel(ISmokeTest smokeTest, string testName)
        {
            if (smokeTest == null)
            {
                return new SummaryViewModel(SmokeTests);
            }
            else if (smokeTest is FpsSmokeTest)
            {
                return new FpsSmokeTestViewModel((FpsSmokeTest)smokeTest, testName);
            }
            else if (smokeTest is CpuSmokeTest)
            {
                return new CpuSmokeTestViewModel((CpuSmokeTest)smokeTest, testName);
            }
            else if (smokeTest is GpuSmokeTest)
            {
                return new GpuSmokeTestViewModel((GpuSmokeTest)smokeTest, testName);
            }
            else if (smokeTest is ScriptMemSmokeTest)
            {
                return new ScriptMemorySmokeTestViewModel((ScriptMemSmokeTest)smokeTest, testName);
            }
            else if (smokeTest is ThreadSmokeTest)
            {
                return new ThreadSmokeTestViewModel((ThreadSmokeTest)smokeTest, testName);
            }
            else if (smokeTest is MemorySmokeTest)
            {
                return new MemorySmokeTestViewModel((MemorySmokeTest)smokeTest, testName);
            }
            else if (smokeTest is StreamingMemorySmokeTest)
            {
                return new StreamingMemorySmokeTestViewModel((StreamingMemorySmokeTest)smokeTest, testName);
            }
            else if (smokeTest is DrawListSmokeTest)
            {
                return new DrawListSmokeTestViewModel((DrawListSmokeTest)smokeTest, testName);
            }
            else if(smokeTest is PedAndVehicleSmokeTest)
            {
                return new PedVehicleSmokeTestViewModel((PedAndVehicleSmokeTest)smokeTest, testName);
            }

            return null;
        }


        /// <summary>
        /// Called whenever one of the filters change
        /// </summary>
        private void OnFilterChanged(SmokeTestTypeFilterViewModel typeVm, TestFilterViewModel testVm)
        {
            if (testVm != null && typeVm != null)
            {
                CurrentViewModel = CreateViewModel(typeVm.SmokeTest, testVm.TestName);
            }
            else
            {
                CurrentViewModel = null;
            }

            // Force an update of the "NonSummaryMode" property (for enabling/disabling the test name selection buttons)
            OnPropertyChanged("NonSummaryMode");
        }
        #endregion // Private Methods

        #region IWeakEventListener Implementation
        /// <summary>
        /// 
        /// </summary>
        public bool ReceiveWeakEvent(Type managerType, Object sender, EventArgs e)
        {
            PropertyChangedEventArgs eventArgs = e as PropertyChangedEventArgs;

            // Only do work when an item is selected
            if (sender is TestFilterViewModel)
            {
                TestFilterViewModel vm = (TestFilterViewModel)sender;
                if (vm.IsSelected)
                {
                    Action action = () => OnFilterChanged(SmokeTestFilters.FirstOrDefault(item => item.IsSelected), vm);
                    Application.Current.Dispatcher.BeginInvoke(action);
                }
            }
            else if (sender is SmokeTestTypeFilterViewModel)
            {
                SmokeTestTypeFilterViewModel vm = (SmokeTestTypeFilterViewModel)sender;
                if (vm.IsSelected)
                {
                    Action action = () => OnFilterChanged(vm, TestNameFilters.FirstOrDefault(item => item.IsSelected));
                    Application.Current.Dispatcher.BeginInvoke(action);
                }
            }

            return true;
        }
        #endregion // IWeakEventListener
    } // AutomatedTestReport
}
