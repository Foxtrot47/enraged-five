﻿using RSG.Base.Editor;
using RSG.Model.Map3;
using LevelBrowser.AddIn.MapBrowser.BatchExporter.Model;
using RSG.Model.Common.Map;

namespace LevelBrowser.AddIn.MapBrowser.BatchExporter.ViewModel
{
    /// <summary>
    /// View model for MapSection instance.
    /// </summary>
    public class SectionExportItemViewModel : ViewModelBase
    {
        #region Private member fields

        private bool m_isChecked;
        private string m_name;
        private MapSection m_section;
        private string m_fullPath;

        #endregion

        #region Public properties

        /// <summary>
        /// MapSection instance.
        /// </summary>
        public MapSection MapSection
        {
            get { return m_section; }
            set
            {
                m_section = value;
                FullPath = GetSectionFullPath(m_section);
                OnPropertyChanged("IsLoaded");
            }
        }

        /// <summary>
        /// True if the view model has been selected in the UI.
        /// </summary>
        public bool IsChecked
        {
            get { return m_isChecked; }
            set
            {
                m_isChecked = value;
                OnPropertyChanged("IsChecked");
            }
        }

        /// <summary>
        /// Returns true if the map section is not null.
        /// </summary>
        public bool IsLoaded
        {
            get { return MapSection != null; }
        }

        /// <summary>
        /// The full path to the MapSection.
        /// </summary>
        public string FullPath
        {
            get 
            {
                return m_fullPath;
            }
            set
            {
                m_fullPath = value;
                OnPropertyChanged("FullPath");
            }
        }

        /// <summary>
        /// The section name.
        /// </summary>
        public string SectionName
        {
            get { return m_name; }
            set
            {
                m_name = value;
                OnPropertyChanged("SectionName");
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="sectionName">Section name.</param>
        /// <param name="section">MapSection instance.</param>
        public SectionExportItemViewModel(string sectionName, MapSection section)
        {
            m_section = section;
            SectionName = section == null ? sectionName : section.Name;
            IsChecked = true;

            FullPath = GetSectionFullPath(section);
        }

        #endregion

        #region Private helper methods

        /// <summary>
        /// Walks back up the map hierarchy to determine the path to the root element from the given MapSection.
        /// </summary>
        /// <param name="section">MapSection instance.</param>
        /// <returns>The path to the root element of the current level's map hierarchy.</returns>
        private string GetSectionFullPath(MapSection section)
        {
            if (section == null)
            {
                return "...";
            }

            string fullPath = section.Name;
            IMapArea parent = section.ParentArea;

            while (parent != null)
            {
                fullPath = parent.Name + "/" + fullPath;
                parent = parent.ParentArea;
            }

            return fullPath;
        }

        #endregion
    }
}
