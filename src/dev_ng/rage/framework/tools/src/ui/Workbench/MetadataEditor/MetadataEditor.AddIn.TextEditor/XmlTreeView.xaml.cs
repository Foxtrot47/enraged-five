﻿using System;
using System.Collections.Generic;
using System.Windows;
using Editor.AddIn;
using Editor.Workbench.AddIn;
using MetadataEditor.AddIn.TextEditor.Controls;

namespace MetadataEditor.AddIn.TextEditor
{
    /// <summary>
    /// Interaction logic for XmlTreeView.xaml
    /// </summary>
    [ExportExtension(Editor.AddIn.ExtensionPoints.Views,
        typeof(ResourceDictionary))]
    public partial class XmlTreeView : ResourceDictionary
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public XmlTreeView()
        {
            InitializeComponent();
        }
        #endregion // Constructor(s)
    }

} // MetadataEditor.AddIn.TextEditor namespace
