﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Controls;
using Ionic.Zip;
using MapViewport.AddIn;
using RSG.Base.Configuration;
using RSG.Base.Drawing;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Base.Math;
using RSG.Base.Tasks;
using RSG.Base.Windows.Controls.WpfViewport2D.Controls;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using RSG.Base.Windows.Extensions;
using RSG.Base.Windows.Helpers;
using RSG.Model.Common;
using RSG.Pipeline.Content;
using RSG.Pipeline.Core;
using RSG.Pipeline.Processor.Map;
using RSG.Pipeline.Services;
using RSG.Platform;
using Workbench.AddIn.MapDebugging.OverlayViews;
using Workbench.AddIn.Services;

namespace Workbench.AddIn.MapDebugging.Overlays
{

    /// <summary>
    /// 
    /// </summary>
    public class IMAPFileComparer : ListViewCustomComparer<IMAPFile>
    {
        /// <summary>
        /// Compares the specified x to y.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <returns></returns>
        public override int Compare(IMAPFile x, IMAPFile y)
        {
            try
            {
                switch (SortBy)
                {
                    default:
                    case "IMAP Name":
                        if (SortDirection.Equals(ListSortDirection.Ascending))
                        {
                            return String.Compare(x.Name, y.Name);
                        }
                        else
                        {
                            return (-1) * String.Compare(x.Name, y.Name);
                        }
                    case "Entity Count":
                        if (SortDirection.Equals(ListSortDirection.Ascending))
                        {
                            return x.EntityCount.CompareTo(y.EntityCount);
                        }
                        else
                        {
                            return (-1) * x.EntityCount.CompareTo(y.EntityCount);
                        }
                }
            }
            catch (Exception)
            {
            }
            return 0;
        }
    }


    /// <summary>
    /// 
    /// </summary>
    [ExportExtension("Workbench.AddIn.MapDebugging.OverlayGroups.MapDebug", typeof(Viewport.AddIn.ViewportOverlay))]
    public class EntityIMAPOverlay : LevelDependentViewportOverlay
    {
        #region Constants
        private const String c_name = "Entity IMAP";
        private const String c_description = "Overlay showing number of entities that will be loaded in various areas of the map.";
        private const String c_processor_mapDummyMetadataMerge = "RSG.Pipeline.Processor.Map.DummyMetadataMergeProcessor";
        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// The perforce sync service.
        /// </summary>
        [ImportExtension(CompositionPoints.PerforceSyncService, typeof(IPerforceSyncService))]
        private Lazy<IPerforceSyncService> PerforceSyncService { get; set; }

        /// <summary>
        /// MEF import for message box service.
        /// </summary>
        [ImportExtension(CompositionPoints.TaskProgressService, typeof(ITaskProgressService))]
        private Lazy<ITaskProgressService> TaskProgressService { get; set; }

        /// <summary>
        /// A reference to view model for the map viewport.
        /// </summary>
        [ImportExtension(Viewport.AddIn.CompositionPoints.MapViewport, typeof(Viewport.AddIn.IMapViewport))]
        private Lazy<Viewport.AddIn.IMapViewport> MapViewportProxy { get; set; }
        #endregion // MEF Imports

        #region Properties
        #region Stats
        /// <summary>
        /// Minimum value.
        /// </summary>
        public uint MinValue
        {
            get { return m_minValue; }
            set { SetPropertyValue(ref m_minValue, value, "MinValue"); }
        }
        private uint m_minValue;

        /// <summary>
        /// Maximum value.
        /// </summary>
        public uint MaxValue
        {
            get { return m_maxValue; }
            set { SetPropertyValue(ref m_maxValue, value, "MaxValue"); }
        }
        private uint m_maxValue;

        /// <summary>
        /// Average value.
        /// </summary>
        public double AverageValue
        {
            get { return m_averageValue; }
            set { SetPropertyValue(ref m_averageValue, value, "AverageValue"); }
        }
        private double m_averageValue;
        #endregion // Stats

        #region Render Options
        /// <summary>
        /// 
        /// </summary>
        [OverlayParameter(100, FriendlyName = "Resolution")]
        public int Resolution
        {
            get { return m_resolution; }
            set
            {
                if (SetPropertyValue(ref m_resolution, value, "Resolution"))
                {
                    // HACK :/
                    if (IsCurrentlyActive)
                    {
                        ResolutionChanged = true;
                    }
                }
            }
        }
        private int m_resolution;

        /// <summary>
        /// 
        /// </summary>
        public bool MouseClickMode
        {
            get { return m_mouseClickMode; }
            set
            {
                if (SetPropertyValue(ref m_mouseClickMode, value, "MouseClickMode") && value == false && IsCurrentlyActive)
                {
                    ResolutionChanged = true;
                }
            }
        }
        private bool m_mouseClickMode;

        /// <summary>
        /// Flag that gets set whent he resolution has changed.
        /// </summary>
        [OverlayParameter(false, FriendlyName = "Resolution Changed")]
        public bool ResolutionChanged
        {
            get { return m_resolutionChanged; }
            set { SetPropertyValue(ref m_resolutionChanged, value, "ResolutionChanged"); }
        }
        private bool m_resolutionChanged;

        /// <summary>
        /// 
        /// </summary>
        [OverlayParameter(0u, FriendlyName = "Low Limit")]
        public uint LowValue
        {
            get { return m_lowValue; }
            set
            {
                if (SetPropertyValue(ref m_lowValue, value, "LowValue"))
                {
                    OnColoursChanged();
                }
            }
        }
        private uint m_lowValue;

        /// <summary>
        /// 
        /// </summary>
        [OverlayParameter(100u, FriendlyName = "High Limit")]
        public uint HighValue
        {
            get { return m_highValue; }
            set
            {
                if (SetPropertyValue(ref m_highValue, value, "HighValue"))
                {
                    OnColoursChanged();
                }
            }
        }
        private uint m_highValue;

        /// <summary>
        /// 
        /// </summary>
        [OverlayParameter(65.0f, FriendlyName = "Opacity")]
        public float OverlayOpacity
        {
            get { return m_overlayOpacity; }
            set
            {
                if (SetPropertyValue(ref m_overlayOpacity, value, "OverlayOpacity"))
                {
                    OnColoursChanged();
                }
            }
        }
        private float m_overlayOpacity;

        /// <summary>
        /// 
        /// </summary>
        public System.Windows.Media.Color LowColor
        {
            get { return m_lowColor; }
            set
            {
                if (SetPropertyValue(ref m_lowColor, value, "LowColor"))
                {
                    OnColoursChanged();
                }
            }
        }
        private System.Windows.Media.Color m_lowColor;

        /// <summary>
        /// Used for serialisation of the low color
        /// </summary>
        [OverlayParameter(4278255360u, FriendlyName = "Low Color")]
        public uint LowColor_SerialisationSurrogate
        {
            get
            {
                return LowColor.ToInteger();
            }
            set
            {
                LowColor = ColorExtensions.FromInteger(value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public System.Windows.Media.Color HighColor
        {
            get { return m_highColor; }
            set
            {
                if (SetPropertyValue(ref m_highColor, value, "HighColor"))
                {
                    OnColoursChanged();
                }
            }
        }
        private System.Windows.Media.Color m_highColor;

        /// <summary>
        /// Used for serialisation of the high color
        /// </summary>
        [OverlayParameter(4294901760u, FriendlyName = "High Color")]
        public uint HighColor_SerialisationSurrogate
        {
            get
            {
                return HighColor.ToInteger();
            }
            set
            {
                HighColor = ColorExtensions.FromInteger(value);
            }
        }
        #endregion // Render Options

        #region Legend
        /// <summary>
        /// 
        /// </summary>
        public System.Windows.Media.Brush GradientLegend
        {
            get { return m_gradientLegend; }
            set { SetPropertyValue(ref m_gradientLegend, value, "GradientLegend"); }
        }
        private System.Windows.Media.Brush m_gradientLegend;
        #endregion // Legend

        #region Commands
        /// <summary>
        /// Command for refreshing the overlay.
        /// </summary>
        public RelayCommand RefreshCommand
        {
            get
            {
                if (m_relayCommand == null)
                {
                    m_relayCommand = new RelayCommand(param => RefreshOverlay(), param => ResolutionChanged);
                }
                return m_relayCommand;
            }
        }
        private RelayCommand m_relayCommand;
        #endregion // Commands

        #region Mouse Based Info
        /// <summary>
        /// The value that is currently under the mouse.
        /// </summary>
        public uint? ValueUnderMouse
        {
            get { return m_valueUnderMouse; }
            set { SetPropertyValue(ref m_valueUnderMouse, value, "ValueUnderMouse"); }
        }
        private uint? m_valueUnderMouse;

        /// <summary>
        /// Information based off of what the user clicked on.
        /// </summary>
        public IList<IMAPFile> SelectionImaps
        {
            get { return m_selectionImaps; }
            set { SetPropertyValue(ref m_selectionImaps, value, "SelectionImaps"); }
        }
        private IList<IMAPFile> m_selectionImaps = new List<IMAPFile>();

        /// <summary>
        /// The item the user selected in the list.
        /// </summary>
        public IMAPFile SelectedImap
        {
            get { return m_selectedImap; }
            set
            {
                if (SetPropertyValue(ref m_selectedImap, value, "SelectedImap"))
                {
                    UpdateHighlightedBox();
                }
            }
        }
        private IMAPFile m_selectedImap;
        #endregion // Mouse Based Info
        #endregion // Properties

        #region Member Data
        private IList<IMAPFile> ImapFiles { get; set; }
        private IDictionary<BoundingBox2f, uint> CachedStats { get; set; }

        private ITask LoadContentTreeTask { get; set; }
        private ITask SyncFilesTask { get; set; }
        private ITask LoadDataTask { get; set; }
        private ITask GenerateGeometryTask { get; set; }
        private ITask GenerateGeometryUnderMouseTask { get; set; }
        private ITask UpdateOverlayTask { get; set; }

        private Viewport2DMultiLine HighlightedBox { get; set; }
        #endregion // Member Data

        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public EntityIMAPOverlay()
            : base(c_name, c_description)
        {
            LoadContentTreeTask = new ActionTask("Loading Content Tree", (ctx, progress) => LoadContentTreeAction(ctx, progress));
            SyncFilesTask = new ActionTask("Syncing Files", (ctx, progress) => SyncFilesAction(ctx, progress));
            LoadDataTask = new ActionTask("Loading Data", (ctx, progress) => LoadOverlayDataAction(ctx, progress));
            GenerateGeometryTask = new ActionTask("Generating Geometry", (ctx, progress) => GenerateGeometryAction(ctx, progress));
            GenerateGeometryUnderMouseTask = new ActionTask("Generating Geometry", (ctx, progress) => GenerateGeometryUnderMouseAction(ctx, progress));
            UpdateOverlayTask = new ActionTask("Updating Overlay", (ctx, progress) => UpdateOverlayGeometryAction(ctx, progress));
        }
        #endregion // Constructors

        #region Overrides
        /// <summary>
        /// Gets called when the user wants to view this overlay
        /// </summary>
        public override void Activated()
        {
            base.Activated();
            MapViewportProxy.Value.ViewportClicked += ViewportClicked;
            UpdateLegend();

            // Create a task for loading the data required for the overlay.
            CancellationTokenSource cts = new CancellationTokenSource();
            TaskContext context = new TaskContext(cts.Token);

            CompositeTask compositeTask = new CompositeTask("Updating Overlay");
            compositeTask.AddSubTask(LoadContentTreeTask);
            compositeTask.AddSubTask(SyncFilesTask);
            compositeTask.AddSubTask(LoadDataTask);
            compositeTask.AddSubTask(GenerateGeometryTask);
            compositeTask.AddSubTask(UpdateOverlayTask);

            TaskProgressService.Value.Add(compositeTask, context, TaskPriority.Foreground, cts);
        }

        /// <summary>
        /// Gets called when the user turns off this overlay
        /// </summary>
        public override void Deactivated()
        {
            MapViewportProxy.Value.ViewportClicked -= ViewportClicked;
            base.Deactivated();
        }

        /// <summary>
        /// Gets the control that should be shown in the overlay details panel
        /// </summary>
        public override Control GetOverlayControl()
        {
            return new EntityIMAPOverlayView();
        }

        /// <summary>
        /// Gets the control that shows information at the bottom right of the viewport.
        /// </summary>
        public override Control GetViewportControl()
        {
            return new ArchetypeITYPViewportOverlayView();
        }

        /// <summary>
        /// 
        /// </summary>
        public override void OnMouseMoved(System.Windows.Point viewPosition, System.Windows.Point worldPosition)
        {
            // Try and find the value under the mouse
            Vector2f point = new Vector2f((float)worldPosition.X, (float)worldPosition.Y);

            if (CachedStats != null)
            {
                lock (CachedStats)
                {
                    if (CachedStats.Any(item => item.Key.Contains(point)))
                    {
                        KeyValuePair<BoundingBox2f, uint> pairUnderMouse = CachedStats.First(item => item.Key.Contains(point));
                        ValueUnderMouse = pairUnderMouse.Value;
                    }
                    else
                    {
                        ValueUnderMouse = null;
                    }
                }
            }
            else
            {
                ValueUnderMouse = null;
            }
        }
        #endregion // Overrides

        #region Private Methods
        /// <summary>
        /// Loads the context tree.
        /// </summary>
        private void LoadContentTreeAction(ITaskContext context, IProgress<TaskProgress> progress)
        {
            // Get the processed zip file paths that we will need to process.

            CommandOptions options = new CommandOptions();
            IContentTree tree = Factory.CreateTree(Config.Value.Config.Project.DefaultBranch);

            IEnumerable<IProcess> metadataMergeProcesses =
                tree.Processes.Where(item => item.ProcessorClassName.Equals(c_processor_mapDummyMetadataMerge, StringComparison.OrdinalIgnoreCase));

            ISet<IContentNode> outputNodes = new HashSet<IContentNode>();
            outputNodes.AddRange(metadataMergeProcesses.SelectMany(item => item.Outputs));

            context.Argument = outputNodes.OfType<IFilesystemNode>().Select(item => item.AbsolutePath);
        }

        /// <summary>
        /// Checks whether we need to sync any files.
        /// </summary>
        private void SyncFilesAction(ITaskContext context, IProgress<TaskProgress> progress)
        {
            IEnumerable<String> zipPaths = (IEnumerable<String>)context.Argument;

            // Make sure the user has them synced.
            int syncedFiles = 0;
            PerforceSyncService.Value.Show("You current have some processed metadata zip files that are out of date.\n" +
                                           "Would you like to grab latest now?\n\n" +
                                           "You can select which files to grab using the table below.\n",
                                           zipPaths, ref syncedFiles, false, true);
        }

        /// <summary>
        /// Rips through the processed zip files getting the information we need.
        /// </summary>
        private void LoadOverlayDataAction(ITaskContext context, IProgress<TaskProgress> progress)
        {
            IEnumerable<String> zipPaths = (IEnumerable<String>)context.Argument;
            String imapExtension = FileType.IMAP.GetExportExtension();
            double progressIncrement = 1.0 / zipPaths.Count();

            ImapFiles = new List<IMAPFile>();
            zipPaths.AsParallel().ForAll(zipPath =>
            {
                try
                {
                    if (System.IO.File.Exists(zipPath))
                    {
                        using (ZipFile file = ZipFile.Read(zipPath))
                        {
                            // Extract information about all the imaps
                            List<IMAPFile> tempImapFiles = new List<IMAPFile>();
                            foreach (ZipEntry entry in file.Entries.Where(item => item.FileName.EndsWith(imapExtension)))
                            {
                                using (MemoryStream zipStream = new MemoryStream())
                                {
                                    entry.Extract(zipStream);
                                    zipStream.Position = 0;

                                    IMAPFile imapFile = IMAPFile.CreateFromStream(zipStream);
                                    if (imapFile != null)
                                    {
                                        tempImapFiles.Add(imapFile);
                                    }
                                }
                            }
                            lock (ImapFiles)
                            {
                                ImapFiles.AddRange(tempImapFiles);
                            }
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    Log.Log__Exception(ex, "Error parsing {0} zip file.", zipPath);
                }

                context.Token.ThrowIfCancellationRequested();
                progress.Report(new TaskProgress(progressIncrement, true, ""));
            });

            context.Token.ThrowIfCancellationRequested();
        }

        /// <summary>
        /// Action for parsing the IMAP/ITYP data generating the geometry to be displayed on the overlay.
        /// </summary>
        private void GenerateGeometryAction(ITaskContext context, IProgress<TaskProgress> progress)
        {
            // Gather the readings across the map.
            ILevel level = LevelBrowserProxy.Value.SelectedLevel;

            int startX = (int)level.ImageBounds.Min.X / Resolution;
            int endX = (int)level.ImageBounds.Max.X / Resolution;
            int startY = (int)level.ImageBounds.Min.Y / Resolution;
            int endY = (int)level.ImageBounds.Max.X / Resolution;

            CachedStats = new Dictionary<BoundingBox2f, uint>();

            int xSteps = endX - startX;
            double progressIncrement = 1.0 / xSteps;

            Enumerable.Range(startX, xSteps).AsParallel().ForAll(x =>
            {
                IList<KeyValuePair<BoundingBox2f, uint>> tempValues = new List<KeyValuePair<BoundingBox2f, uint>>();

                for (int y = startY; y < endY; ++y)
                {
                    BoundingBox2f bbox = new BoundingBox2f(
                        new Vector2f(x * Resolution, y * Resolution),
                        new Vector2f(x * Resolution + Resolution, y * Resolution + Resolution));

                    // Get the IMAP files that this bounding box intersects.
                    IEnumerable<IMAPFile> relevantImaps = ImapFiles.Where(item =>
                    {
                        BoundingBox2f twodBox = new BoundingBox2f(
                            new Vector2f(item.StreamingExtents.Min.X, item.StreamingExtents.Min.Y),
                            new Vector2f(item.StreamingExtents.Max.X, item.StreamingExtents.Max.Y));
                        return twodBox.Intersects(bbox);
                    });

                    uint entities = (uint)relevantImaps.Sum(item => item.EntityCount);
                    tempValues.Add(new KeyValuePair<BoundingBox2f, uint>(bbox, entities));
                }

                lock (CachedStats)
                {
                    CachedStats.AddRange(tempValues);
                }

                context.Token.ThrowIfCancellationRequested();
                progress.Report(new TaskProgress(progressIncrement, true, ""));
            });

            if (CachedStats.Any())
            {
                MinValue = CachedStats.Min(item => item.Value);
                MaxValue = CachedStats.Max(item => item.Value);
                AverageValue = CachedStats.Average(item => item.Value);
            }
            else
            {
                MinValue = 0;
                MaxValue = 0;
                AverageValue = 0.0;
            }

            ResolutionChanged = false;
            SelectionImaps = new List<IMAPFile>();
            SelectedImap = null;
            UpdateHighlightedBox();

            context.Token.ThrowIfCancellationRequested();
        }

        /// <summary>
        /// Action for updating the geometry rendered on the overlay.
        /// </summary>
        private void UpdateOverlayGeometryAction(ITaskContext context, IProgress<TaskProgress> progress)
        {
            // Create the overlay.
            Viewport2DImageOverlay overlay = CreateOverlayImage();
            overlay.BitmapScalingMode = System.Windows.Media.BitmapScalingMode.NearestNeighbor;
            overlay.BeginUpdate();

            int total = CachedStats.Count;
            int current = 0;
            int reportAt = total / 100;

            foreach (KeyValuePair<BoundingBox2f, uint> pair in CachedStats)
            {
                Vector2f[] points = new Vector2f[4];
                points[0] = pair.Key.Min;
                points[1] = new Vector2f(pair.Key.Max.X, pair.Key.Min.Y);
                points[2] = pair.Key.Max;
                points[3] = new Vector2f(pair.Key.Min.X, pair.Key.Max.Y);

                overlay.RenderPolygon(points, CreateColorForValue(pair.Value));
                current++;

                if (context != null && current % reportAt == 0)
                {
                    context.Token.ThrowIfCancellationRequested();
                    progress.Report(new TaskProgress((double)current / total, null));
                }
            }

            overlay.UpdateImageOpacity((byte)((OverlayOpacity / 100.0f) * 255));
            overlay.EndUpdate();

            this.Geometry.BeginUpdate();
            this.Geometry.Clear();
            this.Geometry.Add(overlay);
            this.Geometry.EndUpdate();

            context.Token.ThrowIfCancellationRequested();
        }

        /// <summary>
        /// Refreshes the overlay.
        /// </summary>
        private void RefreshOverlay()
        {
            CancellationTokenSource cts = new CancellationTokenSource();
            TaskContext context = new TaskContext(cts.Token);

            CompositeTask compositeTask = new CompositeTask("Updating Overlay");
            compositeTask.AddSubTask(GenerateGeometryTask);
            compositeTask.AddSubTask(UpdateOverlayTask);
            TaskProgressService.Value.Add(compositeTask, context, TaskPriority.Foreground, cts);
        }

        /// <summary>
        /// 
        /// </summary>
        private void OnColoursChanged()
        {
            // HACK :/
            if (IsCurrentlyActive)
            {
                UpdateLegend();
                UpdateOverlayGeometryAction(null, null);
            }
        }

        /// <summary>
        /// Updates the brush used for rendering the gradient legend.
        /// </summary>
        private void UpdateLegend()
        {
            // Update the gradient legend.
            System.Windows.Media.LinearGradientBrush brush = new System.Windows.Media.LinearGradientBrush();
            brush.StartPoint = new System.Windows.Point(0.0, 0.5);
            brush.EndPoint = new System.Windows.Point(1.0, 0.5);

            double hueStart = RSG.Base.Windows.Controls.ColorUtilities.ConvertRgbToHsv(LowColor.R, LowColor.G, LowColor.B).H / 360.0;
            double hueEnd = RSG.Base.Windows.Controls.ColorUtilities.ConvertRgbToHsv(HighColor.R, HighColor.G, HighColor.B).H / 360.0;
            double hueRange = (hueEnd - hueStart) / 0.6666;

            double size = 1.0 / hueRange;
            double start = -(hueStart / 0.6666) * size;
            double increment = size / 4;

            brush.GradientStops = new System.Windows.Media.GradientStopCollection();
            brush.GradientStops.Add(new System.Windows.Media.GradientStop(System.Windows.Media.Colors.Red, start + (increment * 0)));
            brush.GradientStops.Add(new System.Windows.Media.GradientStop(System.Windows.Media.Colors.Yellow, start + (increment * 1)));
            brush.GradientStops.Add(new System.Windows.Media.GradientStop(System.Windows.Media.Color.FromRgb(0, 255, 0), start + (increment * 2)));
            brush.GradientStops.Add(new System.Windows.Media.GradientStop(System.Windows.Media.Colors.Cyan, start + (increment * 3)));
            brush.GradientStops.Add(new System.Windows.Media.GradientStop(System.Windows.Media.Colors.Blue, start + (increment * 4)));
            GradientLegend = brush;
        }

        /// <summary>
        /// Converts a value to a colour based on the selected render settings.
        /// </summary>
        private Color CreateColorForValue(uint value)
        {
            // Use HSV method
            // 0 -> red
            // 0.3 -> green
            uint minValue = LowValue;
            uint maxValue = HighValue;

            if (value < minValue)
            {
                value = minValue;
            }
            // Clamp values above max to white.
            if (value > maxValue)
            {
                return Color.White;
            }

            // Convert it to a 0->1 scale
            value -= minValue;
            double percent = (double)value / (maxValue - minValue);

            double lowHue = RSG.Base.Windows.Controls.ColorUtilities.ConvertRgbToHsv(LowColor.R, LowColor.G, LowColor.B).H / 360.0;
            double highHue = RSG.Base.Windows.Controls.ColorUtilities.ConvertRgbToHsv(HighColor.R, HighColor.G, HighColor.B).H / 360.0;
            double hueDiff = highHue - lowHue;

            byte h = (byte)((lowHue + (percent * hueDiff)) * 255);
            byte s = (byte)(0.9 * 255);
            byte b = (byte)(0.9 * 255);

            return ColourSpaceConv.HSVtoColor(new HSV(h, s, b));
        }

        /// <summary>
        /// 
        /// </summary>
        private void UpdateHighlightedBox()
        {
            if (HighlightedBox != null)
            {
                this.Geometry.Remove(HighlightedBox);
            }

            if (SelectedImap != null)
            {
                Vector2f[] points = new Vector2f[] {
                    new Vector2f(SelectedImap.StreamingExtents.Min.X, SelectedImap.StreamingExtents.Min.Y),
                    new Vector2f(SelectedImap.StreamingExtents.Max.X, SelectedImap.StreamingExtents.Min.Y),
                    new Vector2f(SelectedImap.StreamingExtents.Max.X, SelectedImap.StreamingExtents.Max.Y),
                    new Vector2f(SelectedImap.StreamingExtents.Min.X, SelectedImap.StreamingExtents.Max.Y),
                    new Vector2f(SelectedImap.StreamingExtents.Min.X, SelectedImap.StreamingExtents.Min.Y)
                };
                HighlightedBox = new Viewport2DMultiLine(etCoordSpace.World, "", points, true, System.Windows.Media.Colors.Black, 1);
                this.Geometry.Add(HighlightedBox);
            }
            else
            {
                HighlightedBox = null;
            }
        }

        /// <summary>
        /// Gets called when the user clicked somewhere on the viewport other than on geometry
        /// </summary>
        private void ViewportClicked(object sender, ViewportClickEventArgs e)
        {
            // Get the imap files that would be loaded at the point where the user clicked.
            Vector2f worldPosition = new Vector2f((float)e.WorldPosition.X, (float)e.WorldPosition.Y);
            SelectionImaps = ImapFiles.Where(item =>
            {
                BoundingBox2f twodBox = new BoundingBox2f(
                    new Vector2f(item.StreamingExtents.Min.X, item.StreamingExtents.Min.Y),
                    new Vector2f(item.StreamingExtents.Max.X, item.StreamingExtents.Max.Y));
                return twodBox.Contains(worldPosition);
            }).ToList();

            // If we are in mouse click mode, update the overlay
            if (MouseClickMode)
            {
                CancellationTokenSource cts = new CancellationTokenSource();
                TaskContext context = new TaskContext(cts.Token);

                CompositeTask compositeTask = new CompositeTask("Updating Overlay");
                compositeTask.AddSubTask(GenerateGeometryUnderMouseTask);
                compositeTask.AddSubTask(UpdateOverlayTask);
                TaskProgressService.Value.Add(compositeTask, context, TaskPriority.Foreground, cts);
            }
        }

        /// <summary>
        /// Action for parsing the IMAP/ITYP data generating the geometry to be displayed on the overlay.
        /// </summary>
        private void GenerateGeometryUnderMouseAction(ITaskContext context, IProgress<TaskProgress> progress)
        {
            IEnumerable<IMAPFile> validImaps = (IEnumerable<IMAPFile>)context.Argument;

            // Gather the readings across the map.
            ILevel level = LevelBrowserProxy.Value.SelectedLevel;

            int startX = (int)level.ImageBounds.Min.X / Resolution;
            int endX = (int)level.ImageBounds.Max.X / Resolution;
            int startY = (int)level.ImageBounds.Min.Y / Resolution;
            int endY = (int)level.ImageBounds.Max.X / Resolution;

            CachedStats = new Dictionary<BoundingBox2f, uint>();

            int xSteps = endX - startX;
            double progressIncrement = 1.0 / xSteps;

            Enumerable.Range(startX, xSteps).AsParallel().ForAll(x =>
            {
                IList<KeyValuePair<BoundingBox2f, uint>> tempValues = new List<KeyValuePair<BoundingBox2f, uint>>();

                for (int y = startY; y < endY; ++y)
                {
                    BoundingBox2f bbox = new BoundingBox2f(
                        new Vector2f(x * Resolution, y * Resolution),
                        new Vector2f(x * Resolution + Resolution, y * Resolution + Resolution));

                    // Get the IMAP files that this bounding box intersects.
                    IEnumerable<IMAPFile> relevantImaps = ImapFiles.Where(item =>
                    {
                        BoundingBox2f twodBox = new BoundingBox2f(
                            new Vector2f(item.StreamingExtents.Min.X, item.StreamingExtents.Min.Y),
                            new Vector2f(item.StreamingExtents.Max.X, item.StreamingExtents.Max.Y));
                        return twodBox.Intersects(bbox);
                    });

                    uint entities = (uint)relevantImaps.Intersect(SelectionImaps).Sum(item => item.EntityCount);
                    tempValues.Add(new KeyValuePair<BoundingBox2f, uint>(bbox, entities));
                }

                lock (CachedStats)
                {
                    CachedStats.AddRange(tempValues);
                }

                context.Token.ThrowIfCancellationRequested();
                progress.Report(new TaskProgress(progressIncrement, true, ""));
            });

            if (CachedStats.Any())
            {
                MinValue = CachedStats.Min(item => item.Value);
                MaxValue = CachedStats.Max(item => item.Value);
                AverageValue = CachedStats.Average(item => item.Value);
            }
            else
            {
                MinValue = 0;
                MaxValue = 0;
                AverageValue = 0.0;
            }

            ResolutionChanged = false;
            SelectedImap = null;
            UpdateHighlightedBox();

            context.Token.ThrowIfCancellationRequested();
        }
        #endregion // Private Methods
    } // EntityIMAPOverlay
}
