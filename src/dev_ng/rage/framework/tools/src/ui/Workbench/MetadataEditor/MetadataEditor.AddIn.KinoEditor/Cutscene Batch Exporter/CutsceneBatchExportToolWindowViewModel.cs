﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Diagnostics;
using RSG.Metadata.Model;
using MetadataEditor.AddIn.View;
using RSG.Base.Editor.Command;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Media;
using Microsoft.Win32;
using Workbench.AddIn;
using Workbench.AddIn.UI.Layout;
using RSG.Base.ConfigParser;
using System.Collections;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Base.Configuration.Automation;
using RSG.Base.Configuration;
using RSG.Pipeline.Automation.Common;
using Workbench.AddIn.Services;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Automation.Consumers;
using RSG.Base.Logging;
using RSG.SourceControl.Perforce;
using RSG.GraphML;
using RSG.Pipeline.Core;
using System.Windows.Data;

namespace MetadataEditor.AddIn.KinoEditor.Cutscene_Batch_Exporter
{

    /// <summary>
    /// 
    /// </summary>
    public class CutsceneBatchExportToolWindowViewModel :
        RSG.Base.Editor.ViewModelBase,
        IPartImportsSatisfiedNotification
    {
        #region Constants
        public const String TOOLWINDOW_NAME = "Cutscene_Batch_Exporter";
        public const String TOOLWINDOW_TITLE = "Cutscene Batch Exporter";
        public const String LOG_CTX = "Cutscene Network Export";
        const string DEFAULT_ENTRY = "Blank";
        private const String MOTIONBUILDER_PROCESSOR = "RSG.Pipeline.Processor.Common.DccMotionbuilderExportProcessor";

        //const string MOTIONBUILDER_EXE = "C:\\Program Files\\Autodesk\\MotionBuilder 2010\\bin\\x64\\motionbuilder.exe";
        #endregion // Constants

        private ITaskProgressService m_taskService;

        #region Properties and Associated Member Data
        /// <summary>
        /// Batch export entries.
        /// </summary>
        private List<BatchEntry> Entries
        {
            get { return m_Entries; }
            set
            {
                SetPropertyValue(value, () => this.Entries,
                    new PropertySetDelegate(delegate(Object newValue) { m_Entries = (List<BatchEntry>)newValue; }));
            }
        }
        private List<BatchEntry> m_Entries = null;

        /// <summary>
        /// Batch export entries.
        /// </summary>
        public ListCollectionView GroupedEntries
        {
            get
            {
                return m_groupedEntries;
            }
            private set
            {
                SetPropertyValue(value, () => this.GroupedEntries,
                    new PropertySetDelegate(delegate(Object newValue) { m_groupedEntries = (ListCollectionView)newValue; }));
            }
        }

        private ListCollectionView m_groupedEntries;

        public int SelectedIndex
        {
            get { return m_selectedIndex; }
            set
            {
                m_selectedIndex = value;
            }
        }
        private int m_selectedIndex = -1;

        public static String CutsceneScenePath
        {
            get { return m_cutsceneScenePath; }
        }
        private static String m_cutsceneScenePath;

        public static String CutsceneCutsPath
        {
            get { return m_cutsceneCutsPath; }
        }
        private static String m_cutsceneCutsPath;

        #endregion // Properties and Associated Member Data

        #region Constructor(s)

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="taskService">Task service.</param>
        public CutsceneBatchExportToolWindowViewModel(ITaskProgressService taskService)
            : this(taskService, null)
        {

        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="taskService">Task service.</param>
        /// <param name="filename">File name to load.</param>
        public CutsceneBatchExportToolWindowViewModel(ITaskProgressService taskService, string filename)
        {
            m_taskService = taskService;

            this.Entries = new List<BatchEntry>();
            IConfig config = ConfigFactory.CreateConfig();
            
            m_cutsceneScenePath = System.IO.Path.Combine(config.Project.DefaultBranch.Art, "animation", "cutscene", "!!scenes");
            m_cutsceneCutsPath = System.IO.Path.Combine(config.Project.DefaultBranch.Assets, "cuts");

            RSG.Base.Tasks.ITask primaryTask = null;

            if (!Path.IsPathRooted(filename))
            {
                filename = null;
            }

            RSG.Base.Tasks.ITask loadAssetsTask = new RSG.Base.Tasks.ActionTask("Load Assets", (context, progress) => this.LoadCutSceneData(context, progress));
            if (String.IsNullOrEmpty(filename))
            {
                primaryTask = loadAssetsTask;
            }
            else
            {
                var compositeTask = new RSG.Base.Tasks.CompositeTask("Cutscene Batch Loader");
                compositeTask.AddSubTask(loadAssetsTask);

                RSG.Base.Tasks.ITask loadFileTask = new RSG.Base.Tasks.ActionTask("Loading the batch file", (context, progress) => this.LoadAction(context));
                compositeTask.AddSubTask(loadFileTask);
                primaryTask = compositeTask;
            }

            primaryTask.OnTaskCompleted += OnRefreshTaskCompleted;
            m_taskService.Add(primaryTask, new RSG.Base.Tasks.TaskContext(new System.Threading.CancellationToken(), filename), TaskPriority.Background);
        }

        private void LoadContentFile(IConfig config, IBranch branch, String contentFile, String artDir, String assetDir, string groupName)
        {
            var graphs = new GraphCollection(contentFile, branch);
            //progress.Report(new RSG.Base.Tasks.TaskProgress(0.33, "Creating the tree"));
            var contentTree = RSG.Pipeline.Content.Factory.CreateTree(branch, graphs);
            //progress.Report(new RSG.Base.Tasks.TaskProgress(0.66, "Creating the UI"));

            const string dataCutPart = "data.cutpart";

            foreach (var process in contentTree.Processes.Where(p => p.ProcessorClassName.Equals(MOTIONBUILDER_PROCESSOR, StringComparison.OrdinalIgnoreCase)))  //   p.Processor.Name.IndexOf("Motionbuilder", StringComparison.OrdinalIgnoreCase) >= 0))
            {
                string mission = "";
                foreach (var output in process.Outputs)
                {
                    if (output is RSG.Pipeline.Content.Directory)
                    {
                        var missionDirectory = output as RSG.Pipeline.Content.Directory;
                        mission = missionDirectory.Name;
                    }
                }

                foreach (var input in process.Inputs)
                {
                    if (input is RSG.Pipeline.Content.File)
                    {
                        var fileNode = input as RSG.Pipeline.Content.File;

                        string cutData = null;
                        IEnumerable<IProcess> fbxOutputProcesses = RSG.Pipeline.Content.Algorithm.Search.FindProcessesWithOutput(contentTree, fileNode);
                        if (fbxOutputProcesses.Count() > 0)
                        {
                            foreach (IProcess fbxOutputProcess in fbxOutputProcesses)
                            {
                                foreach (IContentNode contentNode in fbxOutputProcess.Inputs)
                                {
                                    if (contentNode is RSG.Pipeline.Content.File)
                                    {
                                        RSG.Pipeline.Content.File fileContent = contentNode as RSG.Pipeline.Content.File;
                                        if (fileContent.AbsolutePath.Contains(dataCutPart) == true)
                                        {
                                            cutData = branch.Environment.Subst(fileContent.AbsolutePath);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            string cutsFolder = Path.Combine(branch.Assets, "cuts");
                            string subFolder = Path.Combine(cutsFolder, fileNode.Basename);

                            cutData = Path.Combine(subFolder, dataCutPart);
                        }

                        if (fileNode.AbsolutePath.ToLower().Contains(dataCutPart)) continue;

                        BatchEntry entry = new BatchEntry(fileNode.AbsolutePath, cutData, mission, false, false, groupName);
                        Entries.Add(entry);
                    }
                }
            }
        }

        /// <summary>
        /// Load the cut scene data.
        /// </summary>
        /// <param name="context">Task context.</param>
        /// <param name="progress">Task progress.</param>
        private void LoadCutSceneData(RSG.Base.Tasks.ITaskContext context, RSG.Base.Tasks.IProgress<RSG.Base.Tasks.TaskProgress> progress)
        {
            IConfig config = ConfigFactory.CreateConfig();

            LoadContentFile(config, config.Project.DefaultBranch, Path.Combine(config.ToolsConfig, @"content\content_animation_cutscene_fbx.xml"), config.Project.DefaultBranch.Art, config.Project.DefaultBranch.Assets, "GTA V");

            foreach (KeyValuePair<String, IProject> pair in config.DLCProjects)
            {
                IBranch branch = pair.Value.Branches[pair.Value.DefaultBranchName];
                // Get the branch for the dlc, we look for the core branch in the dlc. If this doesnt exist we drop back to its default branch name
                if(pair.Value.Branches.ContainsKey(config.CoreProject.DefaultBranchName))
                {
                    branch = pair.Value.Branches[config.CoreProject.DefaultBranchName];
                }

                String fbx_content = Path.Combine(branch.Project.Root, "content_animation_cutscene_fbx.xml");
                if (File.Exists(fbx_content))
                {
                    LoadContentFile(config, branch, fbx_content, branch.Art, branch.Assets, pair.Value.Name);
                }
            }

            progress.Report(new RSG.Base.Tasks.TaskProgress(1, "Finished"));
        }

        #endregion // Constructor(s)

        #region IPartImportsSatisfiedNotification Interface
        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {

        }
        #endregion // IPartImportsSatisfiedNotification Interface

        #region Functionality

        private string CreateTempPythonFileName()
        {
            return Path.Combine(System.IO.Path.GetTempPath(),Guid.NewGuid().ToString()) + ".py";
        }

        public bool WritePythonFile(string filename, string fbxfile, string cutfile, bool importcut)
        {
            try
            {
                TextWriter tw = new StreamWriter(filename);

                tw.WriteLine("from ctypes import *");
                tw.WriteLine("from pyfbsdk import *");
                tw.WriteLine("from pyfbsdk_additions import *");
                tw.WriteLine("import os");
                tw.WriteLine("");
                tw.WriteLine("try:");
                tw.WriteLine("\tlogPath = os.getenv(\"RS_TOOLSROOT\") + \"\\logs\\cutscene\\workbench_batch.log\"");
                tw.WriteLine("\tLOGFILE = open(logPath,\"a\")");
                tw.WriteLine("");
                tw.WriteLine("\tif FBApplication().FileOpen( \"" + fbxfile.Replace("\\", "/") + "\" ) == True:");
                tw.WriteLine("\t\tif cdll.rexMBRage.ExportBuildCutscene_Py( \"" + cutfile.Replace("\\", "/") + "\",False," + ((importcut) ? "True" : "False") + ",False,True) == False:");
                tw.WriteLine("\t\t\tLOGFILE.write(\"" + fbxfile.Replace("\\", "/") + " - " + cutfile.Replace("\\", "/") + " failed\\n\")");
                tw.WriteLine("\t\telse:");
                tw.WriteLine("\t\t\tLOGFILE.write(\"" + fbxfile.Replace("\\", "/") + " - " + cutfile.Replace("\\", "/") + " success\\n\")");
                tw.WriteLine("\telse:");
                tw.WriteLine("\t\tLOGFILE.write(\"" + fbxfile.Replace("\\", "/") + " - Unable to open file\\n\")");
                tw.WriteLine("");
                tw.WriteLine("\tLOGFILE.close()");
                tw.WriteLine("except IOError:");
                tw.WriteLine("  print \"An error has occurred within the Python script.\"");
                tw.WriteLine("FBApplication().FileExit()");

                tw.Close();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void CreatePythonScripts()
        {
            System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Array.ForEach(Directory.GetFiles(dialog.SelectedPath), delegate(string path) { File.Delete(path); });

                foreach (BatchEntry be in m_Entries)
                {
                    if (be.FBXFile == DEFAULT_ENTRY || be.CutFile == DEFAULT_ENTRY) continue; // ignore any entries not set

                    string filename = Path.Combine(dialog.SelectedPath, Guid.NewGuid().ToString()) + ".py";

                    WritePythonFile(filename, be.FBXFile, be.CutFile, be.ImportCutFile);
                }
            }
        }

        /// <summary>
        /// Submits the entry to the Automation Server.
        /// </summary>
        /// <param name="entry"></param>
        /// <returns></returns>
        public bool SubmitEntry(BatchEntry entry, IAutomationCoreServices automationConfig, IConfig config)
        {
            if (entry.IsSelected == false) return false;

            Job jobby = new Job();
            jobby.Role = CapabilityType.CutsceneExportNetwork;
            jobby.ID = Guid.NewGuid();

            String serviceName = "Cutscene Network Export"; //This is a friendly name that corresponds to XML configuration data.
            if (config.CoreProject.DefaultBranchName == "dev_ng") serviceName = "Cutscene Network Export NG"; // We have two network exporters, one for cg and one for ng so we switch servers here

            IAutomationServiceParameters serviceParameter = automationConfig.GetServiceParameters(serviceName);
            if (serviceParameter == null)
            {
                Log.Log__ErrorCtx(LOG_CTX, String.Format("Unable to find service parameter named {0}.  Missing configuration data.", serviceName));
                return false;
            }

            // Bit dirty but we need to get the bool down from the workbench so just tag it on the front and remove it ":true:"
            jobby.UserData = ":importCutFile:" + entry.ImportCutFile.ToString().ToLower()
                + ":useSourceControlFiles:" + entry.UseSourceControlFiles.ToString().ToLower()
                + ":concat:" + entry.Concat.ToString().ToLower()
                + ":files:" + entry.FBXFile + "," + entry.CutFile; 

            if (entry.UseSourceControlFiles == false)
            {
                if (!File.Exists(entry.FBXFile))
                {
                    Log.Log__ErrorCtx(LOG_CTX, String.Format("Export queue failed. FBX source file {0} does not exist.", entry.FBXFile));
                    return false;
                }

                if (!File.Exists(entry.CutFile))
                {
                    Log.Log__ErrorCtx(LOG_CTX, String.Format("Export queue failed. Cutpart source file {0} does not exist.", entry.CutFile));
                    return false;
                }

                // If a range changes this is stored in the shot cutxml files so we need to push them over.
                // We need to read the datacutpart for the shot files
                string[] shotFiles = GatherShotFiles(entry.CutFile);
                jobby.UserData += "," + String.Join(",", shotFiles);

                FileTransferServiceConsumer fileTransfer = new FileTransferServiceConsumer(config, serviceParameter.FileTransferService);

                if (!fileTransfer.UploadFile(jobby, entry.FBXFile))
                {
                    Log.Log__ErrorCtx(LOG_CTX, String.Format("Export queue failed. FBX source file {0} failed to upload.", entry.FBXFile));
                    return false;
                }

                if (!fileTransfer.UploadFile(jobby, entry.CutFile))
                {
                    Log.Log__ErrorCtx(LOG_CTX, String.Format("Export queue failed. Cutpart source file {0} failed to upload.", entry.CutFile));
                    return false;
                }

                for (int i = 0; i < shotFiles.Length; ++i)
                {
                    if (!fileTransfer.UploadFile(jobby, shotFiles[i]))
                    {
                        Log.Log__ErrorCtx(LOG_CTX, String.Format("Export queue failed. Shot source file {0} failed to upload.", shotFiles[i]));
                        return false;
                    }
                }
            }

            Debug.Assert(null == jobby.Trigger);

            List<string> files = new List<string>();
            files.Add(entry.FBXFile);

            jobby.Trigger = new UserRequestTrigger(files);

            AutomationServiceConsumer temp = new AutomationServiceConsumer(serviceParameter.AutomationService);
            bool queueResult = temp.EnqueueJob(jobby);
            if (!queueResult)
            {
                Log.Log__ErrorCtx(LOG_CTX, String.Format("Export queue failed. Do you already have something in the queue?  Or have {0} been queued by another user?", entry.FBXFile));
            }
            else
            {
                Log.Log__MessageCtx(LOG_CTX, String.Format("Export queued successfully. {0}", entry.FBXFile));
            }

            return true;
        }

        /// <summary>
        /// Toggle the visibility of the un-selected items in the view.
        /// </summary>
        /// <param name="visible">True if only selected items are to be shown.</param>
        public void ToggleVisibility(bool visible)
        {
            if (GroupedEntries == null) return;

            if (GroupedEntries.IsAddingNew)
            {
                GroupedEntries.CommitNew();
            }
            
            if (GroupedEntries.IsEditingItem)
            {
                GroupedEntries.CommitEdit();
            }

            if (visible)
            {
                using (GroupedEntries.DeferRefresh())
                {
                    GroupedEntries.Filter = new Predicate<object>((o) => { return ((BatchEntry)o).IsSelected; });
                }
            }
            else
            {
                using (GroupedEntries.DeferRefresh())
                {
                    GroupedEntries.Filter = null;
                }
            }
        }

        /// <summary>
        /// Toggle the selected check box value for each item.
        /// </summary>
        public void ToggleSelection()
        {
            if (GroupedEntries == null) return;

            if (GroupedEntries.IsAddingNew)
            {
                GroupedEntries.CommitNew();
            }

            if (GroupedEntries.IsEditingItem)
            {
                GroupedEntries.CommitEdit();
            }

            using (GroupedEntries.DeferRefresh())
            {
                foreach (BatchEntry item in Entries)
                {
                    item.IsSelected = !item.IsSelected;
                }
            }
        }

        public void SendJobs()
        {
            IAutomationCoreServices automationConfig = ConfigFactory.CreateAutomationConfig();
            IConfig config = ConfigFactory.CreateConfig();

            String message = String.Format("Are you sure want to queue all selected jobs for Cutscene Export?");
            MessageBoxResult result = MessageBox.Show(message, "Cutscene Batch Exporter", MessageBoxButton.YesNo, MessageBoxImage.Question);

            switch (result)
            {
                case MessageBoxResult.Yes:
                    {
                        int count = 0;
                        foreach (BatchEntry entry in m_Entries.Where(entry => entry.IsSelected == true))
                        {
                            SubmitEntry(entry, automationConfig, config);
                            count++;
                        }

                        Log.Log__MessageCtx(LOG_CTX, String.Format("{0} Export jobs queued.  Use the Automation Monitor to monitor status.",
                                    count));
                    }
                    break;
                default:
                    MessageBox.Show("Network Export request cancelled.");
                    break;
            }
        }

        private string[] GatherShotFiles(string strCutPart)
        {
            List<string> lstFiles = new List<string>();

            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            doc.Load(strCutPart);
            System.Xml.XmlElement root = doc.DocumentElement;
            System.Xml.XmlNodeList lst = root.GetElementsByTagName("filename");

            foreach (System.Xml.XmlNode n in lst)
            {
                lstFiles.Add(n.InnerText);
            }

            return lstFiles.ToArray();
        }

        public void CreateAndExecutePythonScripts()
        {
            string strMotionbuilderPath = @"C:\Program Files\Autodesk\MotionBuilder 2012 (64-bit)\bin\x64\motionbuilder.exe";

            if (!String.IsNullOrWhiteSpace(strMotionbuilderPath) && File.Exists(strMotionbuilderPath))
            {
                string strLogPath = Environment.GetEnvironmentVariable("RS_TOOLSROOT") + "\\logs\\cutscene\\workbench_batch.log";

                try
                {
                    string strDirectoryPath = Path.GetDirectoryName(strLogPath);
                    if ( Directory.Exists( strDirectoryPath ) == false )
                    {
                        Directory.CreateDirectory(strDirectoryPath);
                    }

                    File.Create(strLogPath).Dispose();

                    foreach (BatchEntry be in m_Entries)
                    {
                        if (be.FBXFile == DEFAULT_ENTRY || be.CutFile == DEFAULT_ENTRY || !be.IsSelected ) continue; // ignore any entries not set

                        string filename = CreateTempPythonFileName();
                        
                        if (WritePythonFile(filename, be.FBXFile, be.CutFile, be.ImportCutFile) == true)
                        {
                            System.Diagnostics.Process pExe = new System.Diagnostics.Process();
                            pExe.StartInfo.FileName = strMotionbuilderPath;
                            pExe.StartInfo.Arguments = filename;
                            pExe.StartInfo.CreateNoWindow = true;
                            pExe.StartInfo.UseShellExecute = false;
                            pExe.Start();

                            pExe.WaitForExit();
                            File.Delete(filename);
                        }
                        else
                        {
                            MessageBox.Show("Unable to open stream to write to " + filename + ".");
                        }
                    }

                    MessageBox.Show("Scene(s) Exported. Check log file: " + strLogPath + " for results.");
                }
                catch (Exception)
                {
                    MessageBox.Show("Unable to create log file for Python scripts at " + strLogPath + ".");
                }
            }
        }

        /// <summary>
        /// Load the specified file.
        /// </summary>
        /// <param name="context">Context containing the specified file name.</param>
        private void LoadAction(RSG.Base.Tasks.ITaskContext context)
        {
            Load(context.Argument.ToString());
        }

        /// <summary>
        /// Load the specified file.
        /// </summary>
        /// <param name="filename">File name.</param>
        public void Load(string filename)
        {
            foreach (var entry in Entries)
            {
                entry.IsSelected = false;
            }

            XPathDocument document = new XPathDocument(filename);

            XPathNavigator navigator = document.CreateNavigator();
            XPathNodeIterator iterator = navigator.Select("//Entries/Entry");

            while (iterator.MoveNext())
            {
                string strFBX = String.Empty;
                string strCUT = String.Empty;
                bool bImportCut = false;
                bool bUseSourceControlFiles = false;
                bool bConcat = false;

                XPathNodeIterator attributeIter = iterator.Current.Select("FBXFile");
                if (attributeIter.Count > 0)
                {
                    attributeIter.MoveNext();
                    strFBX = attributeIter.Current.Value;
                }

                try
                {
                    attributeIter = iterator.Current.Select("ImportCutFile");
                    if (attributeIter.Count > 0)
                    {
                        attributeIter.MoveNext();
                        bImportCut = Convert.ToBoolean(attributeIter.Current.Value);
                    }

                    attributeIter = iterator.Current.Select("Concat");
                    if (attributeIter.Count > 0)
                    {
                        attributeIter.MoveNext();
                        bConcat = Convert.ToBoolean(attributeIter.Current.Value);
                    }

                    attributeIter = iterator.Current.Select("UseSourceControlFiles");
                    if (attributeIter.Count > 0)
                    {
                        attributeIter.MoveNext();
                        bUseSourceControlFiles = Convert.ToBoolean(attributeIter.Current.Value);
                    }
                }
                catch (Exception) { }

                var batchEntry = Entries.FirstOrDefault(ent => ent.FBXFile.IndexOf(strFBX, StringComparison.OrdinalIgnoreCase) >= 0);
                if (batchEntry != null)
                {
                    batchEntry.IsSelected = true;
                    batchEntry.Concat = bConcat;
                    batchEntry.UseSourceControlFiles = bUseSourceControlFiles;
                    batchEntry.ImportCutFile = bImportCut;
                }
                else
                {
                    Log.Log__Warning("Entry '{0}' does not exist in content tree", strFBX);
                }
            }
        }

        /// <summary>
        /// Get a boolean value from an XmlNode.
        /// </summary>
        /// <param name="node">Node.</param>
        /// <param name="defaultValue">Default value.</param>
        /// <returns>The value of the XmlNode.</returns>
        private bool GetBoolValue(XmlNode node, bool defaultValue)
        {
            bool result = defaultValue;

            if (node != null)
            {
                bool temp = false;
                if (bool.TryParse(node.InnerText, out temp))
                {
                    defaultValue = temp;
                }
            }

            return defaultValue;
        }

        public void Save(string filename)
        {
            XmlTextWriter textWriter = new XmlTextWriter(filename, null);
            textWriter.Formatting = Formatting.Indented;

            textWriter.WriteStartDocument();
            textWriter.WriteStartElement("Entries");

            foreach (BatchEntry be in m_Entries)
            {
                if (!be.IsSelected)
                {
                    continue;
                }

                textWriter.WriteStartElement("Entry");
                textWriter.WriteStartElement("FBXFile");
                textWriter.WriteString(be.FBXFile);
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("Concat");
                textWriter.WriteString(be.Concat.ToString());
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("ImportCutFile");
                textWriter.WriteString(be.ImportCutFile.ToString());
                textWriter.WriteEndElement();
                textWriter.WriteStartElement("UseSourceControlFiles");
                textWriter.WriteString(be.UseSourceControlFiles.ToString());
                textWriter.WriteEndElement();
                textWriter.WriteEndElement();
            }

            textWriter.WriteEndElement();
            textWriter.WriteEndDocument();

            textWriter.Close();
        }

        /// <summary>
        /// When the task has completed, use the dispatcher to fill in the entries property.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void OnRefreshTaskCompleted(object sender, RSG.Base.Tasks.TaskCompletedEventArgs e)
        {
            Application.Current.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(
            () =>
            {
                GroupedEntries = new ListCollectionView(Entries);
                GroupedEntries.GroupDescriptions.Add(new PropertyGroupDescription("GroupName"));
            }
            ));
        }

        #endregion
    }

} // MetadataEditor.AddIn.KinoEditor.Cutscene_Batch_Exporter
