﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using RSG.Base.Configuration;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using RSG.Metadata.Model;
using RSG.Metadata.Parser;

namespace MetadataEditor.AddIn.DefinitionEditor.ViewModel
{

    /// <summary>
    /// 
    /// </summary>
    public class StructureDictionaryViewModel : 
        ViewModelBase
    {
        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public StructureDictionary Model
        {
            get { return (m_Model); }
            set
            {
                SetPropertyValue(value, () => this.Model,
                    new PropertySetDelegate(delegate(Object newValue) { m_Model = (StructureDictionary)newValue; }));
            }
        }
        private StructureDictionary m_Model;

        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<NamespaceViewModel> RootNamespaces
        {
            get;
            private set;
        }

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Default constructor.
        /// </summary>
        public StructureDictionaryViewModel()
        {
            this.Model = new StructureDictionary();
            this.RootNamespaces = new ObservableCollection<NamespaceViewModel>();
        }

        /// <summary>
        /// Constructor, specifying StructureDictionary model.
        /// </summary>
        /// <param name="dictionary"></param>
        public StructureDictionaryViewModel(StructureDictionary dictionary)
        {
            this.Model = dictionary;
            this.RootNamespaces = new ObservableCollection<NamespaceViewModel>();

            ObservableCollection<Namespace> TempRootNamespaces = new ObservableCollection<Namespace>();
            TempRootNamespaces.Add(new Namespace());
            List<Structure> baseStructures = GetBaseStructures(this.Model);
            foreach (Structure model in baseStructures)
            {
                Namespace n = TempRootNamespaces[0].Find(model.DataType);
                if (null == n)
                {
                    String ns = Namespace.GetNamespaceName(model.DataType);

                    // Check to see if our "ns" is really a class; for classes
                    // declared within classes.
                    if (!this.Model.ContainsKey(ns))
                    {
                        n = Namespace.Create(ns, TempRootNamespaces[0]);
                        TempRootNamespaces[0].Children.Add(n);
                    }
                    else
                        n = TempRootNamespaces[0];
                }

                n.Children.Add(model);
            }

            foreach (Namespace n in TempRootNamespaces)
            {
                this.RootNamespaces.Add(new NamespaceViewModel(n));
            }
        }

        #endregion // Constructor(s)

        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="path"></param>
        public void Load(IBranch branch, String path)
        {
            Load(branch, path, true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="path"></param>
        /// <param name="recurse"></param>
        public void Load(IBranch branch, String path, bool recurse)
        {
            this.Model.Load(branch, path, recurse);
            OnPropertyChanged("Model");
            this.RootNamespaces.Clear();

            ObservableCollection<Namespace> TempRootNamespaces = new ObservableCollection<Namespace>();
            TempRootNamespaces.Add(new Namespace());
            System.Collections.Generic.List<String> addedNamespaces = new System.Collections.Generic.List<String>();
            foreach (Structure model in this.Model.Values.Where(s => s.ImmediateDescendants.Count() > 0))
            {
                if (addedNamespaces.Contains(model.DataType))
                    continue;

                Namespace n = TempRootNamespaces[0].Find(model.DataType);
                if (null == n)
                {
                    String ns = Namespace.GetNamespaceName(model.DataType);

                    // Check to see if our "ns" is really a class; for classes
                    // declared within classes.
                    if (!this.Model.ContainsKey(ns))
                    {
                        n = Namespace.Create(ns, TempRootNamespaces[0]);
                        TempRootNamespaces[0].Children.Add(n);
                    }
                    else
                        n = TempRootNamespaces[0];
                }
                foreach (Structure child in model.ImmediateDescendants)
                {
                    addedNamespaces.Add(child.DataType);
                }

                n.Children.Add(model);
                addedNamespaces.Add(model.DataType);
            }
            foreach (Structure model in this.Model.Values.Where(s => s.ImmediateDescendants.Count() == 0))
            {
                if (addedNamespaces.Contains(model.DataType))
                    continue;

                Namespace n = TempRootNamespaces[0].Find(model.DataType);
                if (null == n)
                {
                    String ns = Namespace.GetNamespaceName(model.DataType);

                    // Check to see if our "ns" is really a class; for classes
                    // declared within classes.
                    if (!this.Model.ContainsKey(ns))
                    {
                        n = Namespace.Create(ns, TempRootNamespaces[0]);
                        TempRootNamespaces[0].Children.Add(n);
                    }
                    else
                        n = TempRootNamespaces[0];
                }
                foreach (Structure child in model.ImmediateDescendants)
                {
                    addedNamespaces.Add(child.DataType);
                }

                n.Children.Add(model);
                addedNamespaces.Add(model.DataType);
            }
            foreach (Namespace n in TempRootNamespaces)
            {
                this.RootNamespaces.Add(new NamespaceViewModel(n));
            }
        }

        #endregion // Controller Methods

        #region Static Methods

        private List<Structure> GetBaseStructures(StructureDictionary defDict)
        {
            return new List<Structure>(defDict.Values.Where(s => s.Base == null));
        }

        #endregion // Static Methods
    } // StructureDictionaryViewModel
} // RSG.Metadata.ViewModel namespace
