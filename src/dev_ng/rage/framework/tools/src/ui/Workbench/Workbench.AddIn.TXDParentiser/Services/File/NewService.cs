﻿using System;
using System.Windows.Input;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Windows;
using RSG.Base.Editor;
using Workbench.AddIn;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.Services;
using Workbench.AddIn.Services.File;
using RSG.Model.GlobalTXD;
using WidgetEditor.AddIn;
using Workbench.AddIn.Services.Model;

namespace Workbench.AddIn.TXDParentiser.Services.File
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.NewService, typeof(INewService))]
    class NewService : INewService
    {
        #region Constants

        private readonly Guid GUID = new Guid("BA268265-9202-41D0-88E1-B7F5E353C2AE");

        #endregion // Constants

        #region MEF Imports

        /// <summary>
        /// The configuration service that has in it the game view
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(IConfigurationService))]
        Lazy<IConfigurationService> ConfigService
        {
            get;
            set;
        }

        /// <summary>
        /// A reference to the level browser interface
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LevelBrowser, typeof(ILevelBrowser))]
        private Lazy<ILevelBrowser> LevelBrowserProxy { get; set; }

        /// <summary>
        /// MEF import for perforce service.
        /// </summary>
        [ImportExtension(PerforceBrowser.AddIn.CompositionPoints.PerforceService, typeof(PerforceBrowser.AddIn.IPerforceService))]
        private Lazy<PerforceBrowser.AddIn.IPerforceService> PerforceService { get; set; }

        /// <summary>
        /// The perforce sync service
        /// </summary>
        [Workbench.AddIn.ImportExtension(Workbench.AddIn.CompositionPoints.PerforceSyncService, typeof(Workbench.AddIn.Services.IPerforceSyncService))]
        private Lazy<Workbench.AddIn.Services.IPerforceSyncService> PerforceSyncService { get; set; }

        /// <summary>
        /// The progress service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ProgressService, typeof(Workbench.AddIn.Services.IProgressService))]
        private Lazy<Workbench.AddIn.Services.IProgressService> ProgressService { get; set; }

        /// <summary>
        /// MEF import for login service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.WorkbenchSaveService, typeof(Workbench.AddIn.Services.File.IWorkbenchSaveCurrentService))]
        public Lazy<Workbench.AddIn.Services.File.IWorkbenchSaveCurrentService> SaveService { get; set; }

        [ImportExtension(WidgetEditor.AddIn.CompositionPoints.ProxyService, typeof(IProxyService))]
        private IProxyService ProxyService { get; set; }
        #endregion // MEF Imports

        #region Properties

        public Guid ID
        {
            get { return GUID; }
        }

        /// <summary>
        /// New service header string for UI display.
        /// </summary>
        public String Header
        {
            get;
            private set;
        }

        /// <summary>
        /// New service Bitmap for UI display.
        /// </summary>
        public Bitmap Image
        {
            get;
            private set;
        }

        /// <summary>
        /// The keybinding that is placed on this command.
        /// This binding is added to the applications bindings.
        /// (Only used if this is a main menu item)
        /// </summary>
        public KeyGesture KeyGesture
        {
            get { return null; }
        }

        #endregion // Properties

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public NewService()
        {
            this.Header = "New TXD Parentiser File...";
            this.Image = null;
        }

        #endregion // Constructor

        #region Methods

        /// <summary>
        /// 
        /// </summary>
        public bool PreNew(out Object param)
        {
            param = null;
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool New(out IDocumentBase document, out IModel model, String filename, Object param)
        {
            document = new TXDParentiserView(filename, this.ConfigService.Value, this.PerforceService.Value, this.PerforceSyncService.Value, this.ProgressService.Value, this.LevelBrowserProxy.Value, SaveService.Value, ProxyService, false);
            model = (document as DocumentBase<TXDParentiserViewModel>).ViewModel;

            return true;
        }

        #endregion // Methods
    }
}
