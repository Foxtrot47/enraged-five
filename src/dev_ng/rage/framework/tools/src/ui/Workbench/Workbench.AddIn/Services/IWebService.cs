﻿namespace Workbench.AddIn.Services
{
    /// <summary>
    /// 
    /// </summary>
    public interface IWebService
    {
        /// <summary>
        /// Starts the web service
        /// </summary>
        void Start();

        /// <summary>
        /// Stops the web service
        /// </summary>
        void Stop();
    } // IWebService
} // Workbench.AddIn.Services
