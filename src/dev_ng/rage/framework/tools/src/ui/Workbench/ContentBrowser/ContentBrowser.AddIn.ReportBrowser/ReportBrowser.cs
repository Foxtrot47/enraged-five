﻿using System;
using System.ComponentModel.Composition;
using System.Windows.Media.Imaging;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Workbench.AddIn;
using ContentBrowser.AddIn;
using RSG.Model.Common;
using RSG.Model.Report;
using RSG.Base.Extensions;
using ContentBrowser.AddIn.ReportBrowser.Reports;
using RSG.Base.Configuration;
using RSG.Base.Configuration.Reports;

namespace ContentBrowser.AddIn.ReportBrowser
{
    /// <summary>
    /// Report Browser Proxy class.
    /// </summary>
    [ExportExtension(ExtensionPoints.Browser, typeof(BrowserBase))]
    class ReportBrowser : 
        ContentBrowser.AddIn.BrowserBase,
        IPartImportsSatisfiedNotification
    {
        #region MEF Imports
        /// <summary>
        /// The main content browser export object; will also be used as the view model
        /// for the content browsers main window
        /// </summary>
        [Workbench.AddIn.ImportExtension(ContentBrowser.AddIn.CompositionPoints.ContentBrowser, typeof(ContentBrowser.AddIn.IContentBrowser))]
        private Lazy<ContentBrowser.AddIn.IContentBrowser> ContentBrowserImport { get; set; }
        
        /// <summary>
        /// The main content browser export object; will also be used as the view model
        /// for the content browsers main window
        /// </summary>
        [Workbench.AddIn.ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(Workbench.AddIn.Services.IConfigurationService))]
        private Lazy<Workbench.AddIn.Services.IConfigurationService> ConfigurationService { get; set; }
        
        /// <summary>
        /// Import all exported reports in the system.
        /// </summary>
        [ImportManyExtension(Report.AddIn.ExtensionPoints.ReportCategory, typeof(IReportCategory))]
        private IEnumerable<IReportCategory> ReportCategories { get; set; }
        #endregion // MEF Imports

        #region Properties

        /// <summary>
        /// The icon that will be displayed to represent this browser
        /// </summary>
        public override BitmapImage BrowserIcon
        {
            get
            {
                Uri uriSource = new Uri("pack://application:,,,/ContentBrowser.AddIn.ReportBrowser;component/Resources/Images/BrowserIcon.png");
                return new BitmapImage(uriSource);
            }
        }

        #endregion // Properties
   
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ReportBrowser()
            : base("Report Browser")
        {
            this.ID = ContentBrowser.AddIn.CompositionPoints.ReportBrowserGuid;
            this.RelativeID = ContentBrowser.AddIn.CompositionPoints.PerforceBrowserGuid;
            this.Direction = Workbench.AddIn.Services.Direction.After;
        }
        #endregion // Constructor

        #region IPartImportsSatisfiedNotification Interface
        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {
            // Add any imported reports
            AssetHierarchy.AddRange(ReportCategories);

            // Add the custom reports from the reports config
            foreach (ICustomReportGroup group in ConfigurationService.Value.ReportsConfig.ReportGroups)
            {
                AssetHierarchy.Add(new CustomReportCategory(group));
            }
        }
        #endregion // IPartImportsSatisfiedNotification Interface
    } // ReportBrowserProxy
    
} // ContentBrowser.AddIn.ReportBrowser
