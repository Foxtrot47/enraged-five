﻿using System;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Diagnostics;
using RSG.Metadata.Model;
using Workbench.AddIn;
using Workbench.AddIn.Services;
using Workbench.AddIn.UI.Layout;
using Editor.Utilities;
using MetadataEditor.AddIn.View;
using RSG.Base.Editor.Command;

namespace MetadataEditor.AddIn.DefinitionEditor
{
    /// <summary>
    /// Definition Explorer tool window.
    /// </summary>
    /// This is a dockable window component.
    public class DefinitionExplorerToolWindowViewModel : 
        RSG.Base.Editor.ViewModelBase
    {
        #region Constants
        public const String TOOLWINDOW_NAME = "Definition_Explorer";
        public const String TOOLWINDOW_TITLE = "Definition Explorer";
        #endregion // Constants

        #region Members

        private ObservableCollection<Object> m_Roots = null;
        private RelayCommand m_refreshCommand;
        private RelayCommand m_errorCommand;
        private IStructureDictionary m_structureDictionary;
        private BitmapSource m_errorIconSource;

        #endregion // Members

        #region Properties and Associated Member Data

        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<Object> Roots
        {
            get { return m_Roots; }
            private set 
            { 
                SetPropertyValue(value, () => this.Roots,
                    new PropertySetDelegate(delegate(Object newValue) { m_Roots = (ObservableCollection<Object>)newValue;}));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public IStructureDictionary StructureDictionary
        {
            get { return m_structureDictionary; }
            private set
            {
                if (m_structureDictionary == value)
                    return;

                IStructureDictionary oldValue = m_structureDictionary;

                SetPropertyValue(value, () => this.StructureDictionary,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        { 
                            m_structureDictionary = (IStructureDictionary)newValue;
                        }
                ));

                this.OnStructureDictionaryChanged(oldValue, value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public BitmapSource ErrorIconSource
        {
            get { return m_errorIconSource; }
            set
            {
                SetPropertyValue(value, () => this.ErrorIconSource,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_errorIconSource = (BitmapSource)newValue;
                        }
                ));
            }
        }

        #endregion // Properties and Associated Member Data

        #region Constructor(s)

        /// <summary>
        /// Default constructor.
        /// </summary>
        public DefinitionExplorerToolWindowViewModel(IConfigurationService config, MetadataEditor.AddIn.IStructureDictionary structures)
        {
            this.StructureDictionary = structures;
            System.Drawing.Icon icon = System.Drawing.SystemIcons.Warning;
            this.ErrorIconSource = System.Windows.Interop.Imaging.CreateBitmapSourceFromHIcon(icon.Handle, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
        }
       
        #endregion // Constructor(s)

        #region Commands

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand RefreshCommand
        {
            get
            {
                if (m_refreshCommand == null)
                {
                    m_refreshCommand = new RelayCommand(param => this.Refresh(param), param => this.CanFresh(param));
                }
                return m_refreshCommand;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand ErrorCommand
        {
            get
            {
                if (m_errorCommand == null)
                {
                    m_errorCommand = new RelayCommand(param => this.ShowErrors(param), param => this.CanShowErrors(param));
                }
                return m_errorCommand;
            }
        }

        #endregion // Commands

        #region Command Callbacks

        /// <summary>
        /// 
        /// </summary>
        private Boolean CanFresh(Object param)
        {
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        private void Refresh(Object param)
        {
            this.StructureDictionary.RefreshDefinitions();
        }

        /// <summary>
        /// 
        /// </summary>
        private Boolean CanShowErrors(Object param)
        {
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        private void ShowErrors(Object param)
        {
            this.StructureDictionary.ShowDefinitionErrorView();
        }

        #endregion // Command Callbacks

        #region Property Changed Callbacks

        /// <summary>
        /// 
        /// </summary>
        private void OnStructureDictionaryChanged(IStructureDictionary oldValue, IStructureDictionary newValue)
        {
            if (oldValue != null)
                oldValue.DefinitionsRefreshed -= new EventHandler(OnDefinitionsRefreshed);

            if (newValue != null)
            {
                CreateHierchicalFolders(newValue);
                newValue.DefinitionsRefreshed += new EventHandler(OnDefinitionsRefreshed);
            }
            else
                this.Roots.Clear();
        }

        #endregion // Property Changed Callbacks

        #region Private Functions

        /// <summary>
        /// 
        /// </summary>
        void CreateHierchicalFolders(MetadataEditor.AddIn.IStructureDictionary structures)
        {
            String path = structures.DefinitionRootLocation;
            ViewModel.HierarchicalFolder root = ViewModel.HierarchicalFolder.Create(path, structures.Structures);
            root.IsExpanded = true;

            this.Roots = new ObservableCollection<Object>();
            this.Roots.Add(root);
        }

        /// <summary>
        /// 
        /// </summary>
        void OnDefinitionsRefreshed(Object sender, EventArgs e)
        {
            CreateHierchicalFolders(sender as MetadataEditor.AddIn.IStructureDictionary);
        }

        #endregion // Private Functions
    } // DefinitionExplorerToolWindowViewModel

    /// <summary>
    /// A command whose sole purpose is to  relay its functionality 
    /// to other objects by invoking delegates. The default return 
    /// value for the CanExecute method is 'true'.
    /// </summary>
    public class RelayCommand : System.Windows.Input.ICommand
    {
        #region Fields

        private readonly Action<Object> m_execute;
        private readonly Predicate<Object> m_canExecute;

        #endregion // Fields

        #region Constructors

        /// <summary>
        /// Creates a new command that can always execute.
        /// </summary>
        public RelayCommand(Action<Object> execute)
            : this(execute, null)
        {
        }

        /// <summary>
        /// Creates a new command.
        /// </summary>
        public RelayCommand(Action<Object> execute, Predicate<Object> canExecute)
        {
            if (execute == null)
                throw new ArgumentNullException("execute");

            m_execute = execute;
            m_canExecute = canExecute;
        }

        #endregion // Constructors

        #region ICommand Members

        /// <summary>
        /// Gets called when the manager is deciding if this
        /// command can be executed. This returns true be default
        /// </summary>
        public bool CanExecute(Object parameter)
        {
            if (m_canExecute != null)
                return m_canExecute(parameter);

            return true;
        }

        /// <summary>
        /// Gets called when the command is executed. This just
        /// relays the execution to the delegate function
        /// </summary>
        public void Execute(Object parameter)
        {
            if (m_execute != null)
                m_execute(parameter);
        }

        /// <summary>
        /// Makes sure that this command is hooked up into the
        /// command manager
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add { System.Windows.Input.CommandManager.RequerySuggested += value; }
            remove { System.Windows.Input.CommandManager.RequerySuggested -= value; }
        }

        #endregion // ICommand Members
    } // RelayCommand
} // MetadataEditor.AddIn.DefinitionEditorViewModel namespace
