﻿using System;
using System.ComponentModel.Composition;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Windows.Controls.WpfViewport2D;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using RSG.Base.Math;
using RSG.Model.Map.Statistics;
using Map.AddIn.Services;

namespace Workbench.AddIn.MapStatistics.Overlays
{
    //[ExportExtension("Workbench.AddIn.MapStatistics.OverlayGroups.CostStatisticsGroup", typeof(ViewportOverlay))]
    //public class TotalCostOverlay : ViewportOverlay, IPartImportsSatisfiedNotification
    //{
    //    #region MEF Imports

    //    /// <summary>
    //    /// Map Browser
    //    /// </summary>
    //    [ImportExtension(Map.AddIn.Services.CompositionPoints.MapBrowser, typeof(IMapBrowser))]
    //    Lazy<IMapBrowser> MapBrowser
    //    {
    //        get;
    //        set;
    //    }

    //    /// <summary>
    //    /// Map Browser
    //    /// </summary>
    //    [ImportExtension("Workbench.AddIn.MapStatistics.MapStatisticsViewModel", typeof(MapStatisticsViewModel))]
    //    MapStatisticsViewModel ViewModel
    //    {
    //        get;
    //        set;
    //    }

    //    #endregion // MEF Imports

    //    #region Constructors

    //    /// <summary>
    //    /// Default Constructor
    //    /// </summary>
    //    public TotalCostOverlay()
    //    {
    //        this.Name = "Total Cost";
    //        this.Description = "Shows the indiviual containers for the map in a colour that ranages from red to green that represents " + 
    //                                            "the total size of that container compared with the other containers on the map.";
    //    }

    //    #endregion // Constructors

    //    #region Overrides

    //    /// <summary>
    //    /// Gets called when the user wants to view this overlay
    //    /// </summary>
    //    public override void Activated()
    //    {
    //        if (MapBrowser.Value.LevelGeometry != null && MapBrowser.Value.SelectedLevel != null)
    //        {
    //            int totalStat = 0;
    //            int statCount = 0;
    //            int minValue = int.MaxValue;
    //            int maxValue = int.MinValue; 
    //            float range = 0;
    //            Dictionary<MapSectionViewModel, List<Vector2f>> statistics = new Dictionary<MapSectionViewModel, List<Vector2f>>();
    //            Dictionary<MapSectionViewModel, int> statValues = new Dictionary<MapSectionViewModel, int>();
    //            if (this.ViewModel.RootStatisticArea != null)
    //            {
    //                foreach (KeyValuePair<MapSectionViewModel, List<Vector2f>> kvp in MapBrowser.Value.LevelGeometry)
    //                {
    //                    if (ViewModelUtilities.ContainsComponent(this.ViewModel.RootStatisticArea, kvp.Key.Name, true) ||
    //                        ViewModelUtilities.ContainsComponent(this.ViewModel.RootStatisticArea2, kvp.Key.Name, true))
    //                    {
    //                        Boolean unique = ViewModel.IncludeJustUniqueReferences;
    //                        int stat = 0;
    //                        if (ViewModel.IncludeHighDetailObjects)
    //                            stat += (kvp.Key.Model).GetTotalCost(RSG.Model.Map.InstanceType.HD, unique);
    //                        if (ViewModel.IncludeLodObjects)
    //                            stat += (kvp.Key.Model).GetTotalCost(RSG.Model.Map.InstanceType.LOD, unique);
    //                        if (ViewModel.IncludeSLodObjects)
    //                            stat += (kvp.Key.Model).GetTotalCost(RSG.Model.Map.InstanceType.SLOD, unique);
    //                        if (ViewModel.IncludeReferences)
    //                            stat += (kvp.Key.Model).GetTotalCost(RSG.Model.Map.InstanceType.REF, unique);

    //                        if (stat != 0)
    //                        {
    //                            totalStat += stat;

    //                            if (stat > maxValue)
    //                                maxValue = stat;
    //                            if (stat < minValue)
    //                                minValue = stat;

    //                            statCount++;
    //                            statValues.Add(kvp.Key, stat);
    //                            statistics.Add(kvp.Key, kvp.Value);
    //                        }
    //                    }
    //                }
    //            }
    //            range = (maxValue - minValue);

    //            System.Windows.Media.Brush brush = System.Windows.Media.Brushes.Black;
    //            SortedDictionary<int, List<Viewport2DGeometry>> orderedGeometry = new SortedDictionary<int,List<Viewport2DGeometry>>();
    //            foreach (KeyValuePair<MapSectionViewModel, List<Vector2f>> kvp in statistics)
    //            {
    //                float percentage = statValues[kvp.Key];
    //                percentage /= range;
    //                if (percentage > 1.0f)
    //                {
    //                    percentage = 1.0f;
    //                }
    //                else if (percentage < 0.0f)
    //                {
    //                    percentage = 0.0f;
    //                }
    //                Vector3i difference = new Vector3i(127, -127, 0);
    //                byte r = (byte)(((int)(difference.X * percentage)));
    //                byte g = (byte)(((int)(difference.Y * percentage)) + 127);
    //                byte b = (byte)(((int)(difference.Z * percentage)));
    //                System.Windows.Media.Color colour = System.Windows.Media.Color.FromArgb(255, r, g, b);

    //                if (!orderedGeometry.ContainsKey(statValues[kvp.Key]))
    //                {
    //                    orderedGeometry.Add(statValues[kvp.Key], new List<Viewport2DGeometry>());
    //                }
    //                Viewport2DShape newGeometry = new Viewport2DShape(etCoordSpace.World, "Filled shape for section", kvp.Value.ToArray(), brush, 1, colour);
    //                newGeometry.UserText = kvp.Key.Name;
    //                newGeometry.PickData = kvp.Key;
    //                orderedGeometry[statValues[kvp.Key]].Add(newGeometry);
    //            }

    //            int index = 0;
    //            this.Geometry.BeginUpdate();
    //            this.Geometry.Clear();
    //            foreach (var statValue in orderedGeometry.Reverse())
    //            {
    //                if (++index > this.ViewModel.WorstCount && this.ViewModel.ShowWorst == true)
    //                {
    //                    break;
    //                }
    //                foreach (var geo in statValue.Value)
    //                {
    //                    this.Geometry.Add(geo);
    //                }
    //            }
    //            this.Geometry.EndUpdate();
    //        }
    //        base.Activated();
    //    }

    //    /// <summary>
    //    /// Gets called when the user turns off this overlay
    //    /// </summary>
    //    public override void Deactivated()
    //    {
    //        this.Geometry = new RSG.Base.Collections.ObservableCollection<Viewport2DGeometry>();
    //        base.Deactivated();
    //    }

    //    #endregion // Overrides

    //    #region IPartImportsSatisfiedNotification

    //    /// <summary>
    //    /// Gets called once all the imports have been satisfied
    //    /// </summary>
    //    public void OnImportsSatisfied()
    //    {
    //        ViewModel.PropertyChanged += OnPropertyChanged;
    //    }

    //    /// <summary>
    //    /// Gets called whenever a property on the map browser changes
    //    /// </summary>
    //    void OnPropertyChanged(Object sender, System.ComponentModel.PropertyChangedEventArgs e)
    //    {
    //        if (this.IsCurrentlyActive == true)
    //        {
    //            if (e.PropertyName == "IncludeHighDetailObjects")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "IncludeLodObjects")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "IncludeSLodObjects")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "IncludeReferences")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "IncludeJustUniqueReferences")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "ShowWorst")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "WorstCount")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "RootStatisticArea")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "RootStatisticArea2")
    //            {
    //                this.Activated();
    //            }
    //        }
    //    }

    //    #endregion // IPartImportsSatisfiedNotification
    //} // TotalCostOverlay

    //[ExportExtension("Workbench.AddIn.MapStatistics.OverlayGroups.CostStatisticsGroup", typeof(ViewportOverlay))]
    //public class GeometryCostOverlay : ViewportOverlay, IPartImportsSatisfiedNotification
    //{
    //    #region MEF Imports

    //    /// <summary>
    //    /// Map Browser
    //    /// </summary>
    //    [ImportExtension(Map.AddIn.Services.CompositionPoints.MapBrowser, typeof(IMapBrowser))]
    //    Lazy<IMapBrowser> MapBrowser
    //    {
    //        get;
    //        set;
    //    }

    //    /// <summary>
    //    /// Map Browser
    //    /// </summary>
    //    [ImportExtension("Workbench.AddIn.MapStatistics.MapStatisticsViewModel", typeof(MapStatisticsViewModel))]
    //    MapStatisticsViewModel ViewModel
    //    {
    //        get;
    //        set;
    //    }

    //    #endregion // MEF Imports

    //    #region Constructors

    //    /// <summary>
    //    /// Default Constructor
    //    /// </summary>
    //    public GeometryCostOverlay()
    //    {
    //        this.Name = "Geometry Cost";
    //        this.Description = "Shows the indiviual containers for the map in a colour that ranages from red to green that represents " +
    //                                            "the geometry size of that container compared with the other containers on the map.";
    //    }

    //    #endregion // Constructors

    //    #region Overrides

    //    /// <summary>
    //    /// Gets called when the user wants to view this overlay
    //    /// </summary>
    //    public override void Activated()
    //    {
    //        if (MapBrowser.Value.LevelGeometry != null && MapBrowser.Value.SelectedLevel != null)
    //        {
    //            int totalStat = 0;
    //            int statCount = 0;
    //            int minValue = int.MaxValue;
    //            int maxValue = int.MinValue;
    //            float range = 0;
    //            Dictionary<MapSectionViewModel, List<Vector2f>> statistics = new Dictionary<MapSectionViewModel, List<Vector2f>>();
    //            Dictionary<MapSectionViewModel, int> statValues = new Dictionary<MapSectionViewModel, int>();
    //            if (this.ViewModel.RootStatisticArea != null)
    //            {
    //                foreach (KeyValuePair<MapSectionViewModel, List<Vector2f>> kvp in MapBrowser.Value.LevelGeometry)
    //                {
    //                    if (ViewModelUtilities.ContainsComponent(this.ViewModel.RootStatisticArea, kvp.Key.Name, true) ||
    //                        ViewModelUtilities.ContainsComponent(this.ViewModel.RootStatisticArea2, kvp.Key.Name, true))
    //                    {
    //                        Boolean unique = ViewModel.IncludeJustUniqueReferences;
    //                        int stat = 0;
    //                        if (ViewModel.IncludeHighDetailObjects)
    //                            stat += (kvp.Key.Model).GetGeometryCost(RSG.Model.Map.InstanceType.HD, unique);
    //                        if (ViewModel.IncludeLodObjects)
    //                            stat += (kvp.Key.Model).GetGeometryCost(RSG.Model.Map.InstanceType.LOD, unique);
    //                        if (ViewModel.IncludeSLodObjects)
    //                            stat += (kvp.Key.Model).GetGeometryCost(RSG.Model.Map.InstanceType.SLOD, unique);
    //                        if (ViewModel.IncludeReferences)
    //                            stat += (kvp.Key.Model).GetGeometryCost(RSG.Model.Map.InstanceType.REF, unique);

    //                        if (stat != 0)
    //                        {
    //                            totalStat += stat;

    //                            if (stat > maxValue)
    //                                maxValue = stat;
    //                            if (stat < minValue)
    //                                minValue = stat;

    //                            statCount++;
    //                            statValues.Add(kvp.Key, stat);
    //                            statistics.Add(kvp.Key, kvp.Value);
    //                        }
    //                    }
    //                }
    //            }
    //            range = (maxValue - minValue);

    //            System.Windows.Media.Brush brush = System.Windows.Media.Brushes.Black;
    //            SortedDictionary<int, List<Viewport2DGeometry>> orderedGeometry = new SortedDictionary<int, List<Viewport2DGeometry>>();
    //            foreach (KeyValuePair<MapSectionViewModel, List<Vector2f>> kvp in statistics)
    //            {
    //                float percentage = statValues[kvp.Key];
    //                percentage /= range;
    //                if (percentage > 1.0f)
    //                {
    //                    percentage = 1.0f;
    //                }
    //                else if (percentage < 0.0f)
    //                {
    //                    percentage = 0.0f;
    //                }
    //                Vector3i difference = new Vector3i(127, -127, 0);
    //                byte r = (byte)(((int)(difference.X * percentage)));
    //                byte g = (byte)(((int)(difference.Y * percentage)) + 127);
    //                byte b = (byte)(((int)(difference.Z * percentage)));
    //                System.Windows.Media.Color colour = System.Windows.Media.Color.FromArgb(255, r, g, b);

    //                if (!orderedGeometry.ContainsKey(statValues[kvp.Key]))
    //                {
    //                    orderedGeometry.Add(statValues[kvp.Key], new List<Viewport2DGeometry>());
    //                }
    //                Viewport2DShape newGeometry = new Viewport2DShape(etCoordSpace.World, "Filled shape for section", kvp.Value.ToArray(), brush, 1, colour);
    //                newGeometry.UserText = kvp.Key.Name;
    //                newGeometry.PickData = kvp.Key;
    //                orderedGeometry[statValues[kvp.Key]].Add(newGeometry);
    //            }

    //            int index = 0;
    //            this.Geometry.BeginUpdate();
    //            this.Geometry.Clear();
    //            foreach (var statValue in orderedGeometry.Reverse())
    //            {
    //                if (++index > this.ViewModel.WorstCount && this.ViewModel.ShowWorst == true)
    //                {
    //                    break;
    //                }
    //                foreach (var geo in statValue.Value)
    //                {
    //                    this.Geometry.Add(geo);
    //                }
    //            }
    //            this.Geometry.EndUpdate();
    //        }
    //        base.Activated();
    //    }

    //    /// <summary>
    //    /// Gets called when the user turns off this overlay
    //    /// </summary>
    //    public override void Deactivated()
    //    {
    //        this.Geometry = new RSG.Base.Collections.ObservableCollection<Viewport2DGeometry>();
    //        base.Deactivated();
    //    }

    //    #endregion // Overrides

    //    #region IPartImportsSatisfiedNotification

    //    /// <summary>
    //    /// Gets called once all the imports have been satisfied
    //    /// </summary>
    //    public void OnImportsSatisfied()
    //    {
    //        ViewModel.PropertyChanged += OnPropertyChanged;
    //    }

    //    /// <summary>
    //    /// Gets called whenever a property on the map browser changes
    //    /// </summary>
    //    void OnPropertyChanged(Object sender, System.ComponentModel.PropertyChangedEventArgs e)
    //    {
    //        if (this.IsCurrentlyActive == true)
    //        {
    //            if (e.PropertyName == "IncludeHighDetailObjects")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "IncludeLodObjects")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "IncludeSLodObjects")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "IncludeReferences")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "IncludeJustUniqueReferences")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "ShowWorst")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "WorstCount")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "RootStatisticArea")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "RootStatisticArea2")
    //            {
    //                this.Activated();
    //            }
    //        }
    //    }

    //    #endregion // IPartImportsSatisfiedNotification
    //} // GeometryCostOverlay

    //[ExportExtension("Workbench.AddIn.MapStatistics.OverlayGroups.CostStatisticsGroup", typeof(ViewportOverlay))]
    //public class TXDCostOverlay : ViewportOverlay, IPartImportsSatisfiedNotification
    //{
    //    #region MEF Imports

    //    /// <summary>
    //    /// Map Browser
    //    /// </summary>
    //    [ImportExtension(Map.AddIn.Services.CompositionPoints.MapBrowser, typeof(IMapBrowser))]
    //    Lazy<IMapBrowser> MapBrowser
    //    {
    //        get;
    //        set;
    //    }

    //    /// <summary>
    //    /// Map Browser
    //    /// </summary>
    //    [ImportExtension("Workbench.AddIn.MapStatistics.MapStatisticsViewModel", typeof(MapStatisticsViewModel))]
    //    MapStatisticsViewModel ViewModel
    //    {
    //        get;
    //        set;
    //    }

    //    #endregion // MEF Imports

    //    #region Constructors

    //    /// <summary>
    //    /// Default Constructor
    //    /// </summary>
    //    public TXDCostOverlay()
    //    {
    //        this.Name = "TXD Cost";
    //        this.Description = "Shows the indiviual containers for the map in a colour that ranages from red to green that represents " +
    //                                            "the txd size of that container compared with the other containers on the map.";
    //    }

    //    #endregion // Constructors

    //    #region Overrides

    //    /// <summary>
    //    /// Gets called when the user wants to view this overlay
    //    /// </summary>
    //    public override void Activated()
    //    {
    //        if (MapBrowser.Value.LevelGeometry != null && MapBrowser.Value.SelectedLevel != null)
    //        {
    //            int totalStat = 0;
    //            int statCount = 0;
    //            int minValue = int.MaxValue;
    //            int maxValue = int.MinValue;
    //            float range = 0;
    //            Dictionary<MapSectionViewModel, List<Vector2f>> statistics = new Dictionary<MapSectionViewModel, List<Vector2f>>();
    //            Dictionary<MapSectionViewModel, int> statValues = new Dictionary<MapSectionViewModel, int>();
    //            if (this.ViewModel.RootStatisticArea != null)
    //            {
    //                foreach (KeyValuePair<MapSectionViewModel, List<Vector2f>> kvp in MapBrowser.Value.LevelGeometry)
    //                {
    //                    if (ViewModelUtilities.ContainsComponent(this.ViewModel.RootStatisticArea, kvp.Key.Name, true) ||
    //                        ViewModelUtilities.ContainsComponent(this.ViewModel.RootStatisticArea2, kvp.Key.Name, true))
    //                    {
    //                        int stat = 0;
    //                        if (ViewModel.IncludeHighDetailObjects)
    //                            stat += (kvp.Key.Model).GetTXDCost(RSG.Model.Map.InstanceType.HD);
    //                        if (ViewModel.IncludeLodObjects)
    //                            stat += (kvp.Key.Model).GetTXDCost(RSG.Model.Map.InstanceType.LOD);
    //                        if (ViewModel.IncludeSLodObjects)
    //                            stat += (kvp.Key.Model).GetTXDCost(RSG.Model.Map.InstanceType.SLOD);
    //                        if (ViewModel.IncludeReferences)
    //                            stat += (kvp.Key.Model).GetTXDCost(RSG.Model.Map.InstanceType.REF);

    //                        if (stat != 0)
    //                        {
    //                            totalStat += stat;

    //                            if (stat > maxValue)
    //                                maxValue = stat;
    //                            if (stat < minValue)
    //                                minValue = stat;

    //                            statCount++;
    //                            statValues.Add(kvp.Key, stat);
    //                            statistics.Add(kvp.Key, kvp.Value);
    //                        }
    //                    }
    //                }
    //            }
    //            range = (maxValue - minValue);

    //            System.Windows.Media.Brush brush = System.Windows.Media.Brushes.Black;
    //            SortedDictionary<int, List<Viewport2DGeometry>> orderedGeometry = new SortedDictionary<int, List<Viewport2DGeometry>>();
    //            foreach (KeyValuePair<MapSectionViewModel, List<Vector2f>> kvp in statistics)
    //            {
    //                float percentage = statValues[kvp.Key];
    //                percentage /= range;
    //                if (percentage > 1.0f)
    //                {
    //                    percentage = 1.0f;
    //                }
    //                else if (percentage < 0.0f)
    //                {
    //                    percentage = 0.0f;
    //                }
    //                Vector3i difference = new Vector3i(127, -127, 0);
    //                byte r = (byte)(((int)(difference.X * percentage)));
    //                byte g = (byte)(((int)(difference.Y * percentage)) + 127);
    //                byte b = (byte)(((int)(difference.Z * percentage)));
    //                System.Windows.Media.Color colour = System.Windows.Media.Color.FromArgb(255, r, g, b);

    //                if (!orderedGeometry.ContainsKey(statValues[kvp.Key]))
    //                {
    //                    orderedGeometry.Add(statValues[kvp.Key], new List<Viewport2DGeometry>());
    //                }
    //                Viewport2DShape newGeometry = new Viewport2DShape(etCoordSpace.World, "Filled shape for section", kvp.Value.ToArray(), brush, 1, colour);
    //                newGeometry.UserText = kvp.Key.Name;
    //                newGeometry.PickData = kvp.Key;
    //                orderedGeometry[statValues[kvp.Key]].Add(newGeometry);
    //            }

    //            int index = 0;
    //            this.Geometry.BeginUpdate();
    //            this.Geometry.Clear();
    //            foreach (var statValue in orderedGeometry.Reverse())
    //            {
    //                if (++index > this.ViewModel.WorstCount && this.ViewModel.ShowWorst == true)
    //                {
    //                    break;
    //                }
    //                foreach (var geo in statValue.Value)
    //                {
    //                    this.Geometry.Add(geo);
    //                }
    //            }
    //            this.Geometry.EndUpdate();
    //        }
    //        base.Activated();
    //    }

    //    /// <summary>
    //    /// Gets called when the user turns off this overlay
    //    /// </summary>
    //    public override void Deactivated()
    //    {
    //        this.Geometry = new RSG.Base.Collections.ObservableCollection<Viewport2DGeometry>();
    //        base.Deactivated();
    //    }

    //    #endregion // Overrides

    //    #region IPartImportsSatisfiedNotification

    //    /// <summary>
    //    /// Gets called once all the imports have been satisfied
    //    /// </summary>
    //    public void OnImportsSatisfied()
    //    {
    //        ViewModel.PropertyChanged += OnPropertyChanged;
    //    }

    //    /// <summary>
    //    /// Gets called whenever a property on the map browser changes
    //    /// </summary>
    //    void OnPropertyChanged(Object sender, System.ComponentModel.PropertyChangedEventArgs e)
    //    {
    //        if (this.IsCurrentlyActive == true)
    //        {
    //            if (e.PropertyName == "IncludeHighDetailObjects")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "IncludeLodObjects")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "IncludeSLodObjects")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "IncludeReferences")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "ShowWorst")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "WorstCount")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "RootStatisticArea")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "RootStatisticArea2")
    //            {
    //                this.Activated();
    //            }
    //        }
    //    }

    //    #endregion // IPartImportsSatisfiedNotification
    //} // TXDCostOverlay

    //[ExportExtension("Workbench.AddIn.MapStatistics.OverlayGroups.ObjectStatisticsGroup", typeof(ViewportOverlay))]
    //public class GeometryCountOverlay : ViewportOverlay, IPartImportsSatisfiedNotification
    //{
    //    #region MEF Imports

    //    /// <summary>
    //    /// Map Browser
    //    /// </summary>
    //    [ImportExtension(Map.AddIn.Services.CompositionPoints.MapBrowser, typeof(IMapBrowser))]
    //    Lazy<IMapBrowser> MapBrowser
    //    {
    //        get;
    //        set;
    //    }

    //    /// <summary>
    //    /// Map Browser
    //    /// </summary>
    //    [ImportExtension("Workbench.AddIn.MapStatistics.MapStatisticsViewModel", typeof(MapStatisticsViewModel))]
    //    MapStatisticsViewModel ViewModel
    //    {
    //        get;
    //        set;
    //    }

    //    #endregion // MEF Imports

    //    #region Constructors

    //    /// <summary>
    //    /// Default Constructor
    //    /// </summary>
    //    public GeometryCountOverlay()
    //    {
    //        this.Name = "Geometry Count";
    //        this.Description = "Shows the indiviual containers for the map in a colour that ranages from red to green that represents " +
    //                                            "the geometry count of that container compared with the other containers on the map.";
    //    }

    //    #endregion // Constructors

    //    #region Overrides

    //    /// <summary>
    //    /// Gets called when the user wants to view this overlay
    //    /// </summary>
    //    public override void Activated()
    //    {
    //        if (MapBrowser.Value.LevelGeometry != null && MapBrowser.Value.SelectedLevel != null)
    //        {
    //            int totalStat = 0;
    //            int statCount = 0;
    //            int minValue = int.MaxValue;
    //            int maxValue = int.MinValue;
    //            float range = 0;
    //            Dictionary<MapSectionViewModel, List<Vector2f>> statistics = new Dictionary<MapSectionViewModel, List<Vector2f>>();
    //            Dictionary<MapSectionViewModel, int> statValues = new Dictionary<MapSectionViewModel, int>();
    //            if (this.ViewModel.RootStatisticArea != null)
    //            {
    //                foreach (KeyValuePair<MapSectionViewModel, List<Vector2f>> kvp in MapBrowser.Value.LevelGeometry)
    //                {
    //                    if (ViewModelUtilities.ContainsComponent(this.ViewModel.RootStatisticArea, kvp.Key.Name, true) ||
    //                        ViewModelUtilities.ContainsComponent(this.ViewModel.RootStatisticArea2, kvp.Key.Name, true))
    //                    {
    //                        Boolean unique = ViewModel.IncludeJustUniqueReferences;
    //                        int stat = 0;
    //                        if (ViewModel.IncludeHighDetailObjects)
    //                            stat += (kvp.Key.Model).GetGeometryCount(RSG.Model.Map.InstanceType.HD, unique);
    //                        if (ViewModel.IncludeLodObjects)
    //                            stat += (kvp.Key.Model).GetGeometryCount(RSG.Model.Map.InstanceType.LOD, unique);
    //                        if (ViewModel.IncludeSLodObjects)
    //                            stat += (kvp.Key.Model).GetGeometryCount(RSG.Model.Map.InstanceType.SLOD, unique);
    //                        if (ViewModel.IncludeReferences)
    //                            stat += (kvp.Key.Model).GetGeometryCount(RSG.Model.Map.InstanceType.REF, unique);
                                
    //                        if (stat != 0)
    //                        {
    //                            totalStat += stat;

    //                            if (stat > maxValue)
    //                                maxValue = stat;
    //                            if (stat < minValue)
    //                                minValue = stat;

    //                            statCount++;
    //                            statValues.Add(kvp.Key, stat);
    //                            statistics.Add(kvp.Key, kvp.Value);
    //                        }
    //                    }
    //                }
    //            }
    //            range = (maxValue - minValue);

    //            System.Windows.Media.Brush brush = System.Windows.Media.Brushes.Black;
    //            SortedDictionary<int, List<Viewport2DGeometry>> orderedGeometry = new SortedDictionary<int, List<Viewport2DGeometry>>();
    //            foreach (KeyValuePair<MapSectionViewModel, List<Vector2f>> kvp in statistics)
    //            {
    //                float percentage = statValues[kvp.Key];
    //                percentage /= range;
    //                if (percentage > 1.0f)
    //                {
    //                    percentage = 1.0f;
    //                }
    //                else if (percentage < 0.0f)
    //                {
    //                    percentage = 0.0f;
    //                }
    //                Vector3i difference = new Vector3i(127, -127, 0);
    //                byte r = (byte)(((int)(difference.X * percentage)));
    //                byte g = (byte)(((int)(difference.Y * percentage)) + 127);
    //                byte b = (byte)(((int)(difference.Z * percentage)));
    //                System.Windows.Media.Color colour = System.Windows.Media.Color.FromArgb(255, r, g, b);

    //                if (!orderedGeometry.ContainsKey(statValues[kvp.Key]))
    //                {
    //                    orderedGeometry.Add(statValues[kvp.Key], new List<Viewport2DGeometry>());
    //                }
    //                Viewport2DShape newGeometry = new Viewport2DShape(etCoordSpace.World, "Filled shape for section", kvp.Value.ToArray(), brush, 1, colour);
    //                newGeometry.UserText = kvp.Key.Name;
    //                newGeometry.PickData = kvp.Key;
    //                orderedGeometry[statValues[kvp.Key]].Add(newGeometry);
    //            }

    //            int index = 0;
    //            this.Geometry.BeginUpdate();
    //            this.Geometry.Clear();
    //            foreach (var statValue in orderedGeometry.Reverse())
    //            {
    //                if (++index > this.ViewModel.WorstCount && this.ViewModel.ShowWorst == true)
    //                {
    //                    break;
    //                }
    //                foreach (var geo in statValue.Value)
    //                {
    //                    this.Geometry.Add(geo);
    //                }
    //            }
    //            this.Geometry.EndUpdate();
    //        }
    //        base.Activated();
    //    }

    //    /// <summary>
    //    /// Gets called when the user turns off this overlay
    //    /// </summary>
    //    public override void Deactivated()
    //    {
    //        this.Geometry = new RSG.Base.Collections.ObservableCollection<Viewport2DGeometry>();
    //        base.Deactivated();
    //    }

    //    #endregion // Overrides

    //    #region IPartImportsSatisfiedNotification

    //    /// <summary>
    //    /// Gets called once all the imports have been satisfied
    //    /// </summary>
    //    public void OnImportsSatisfied()
    //    {
    //        ViewModel.PropertyChanged += OnPropertyChanged;
    //    }

    //    /// <summary>
    //    /// Gets called whenever a property on the map browser changes
    //    /// </summary>
    //    void OnPropertyChanged(Object sender, System.ComponentModel.PropertyChangedEventArgs e)
    //    {
    //        if (this.IsCurrentlyActive == true)
    //        {
    //            if (e.PropertyName == "IncludeHighDetailObjects")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "IncludeLodObjects")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "IncludeSLodObjects")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "IncludeReferences")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "IncludeJustUniqueReferences")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "ShowWorst")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "WorstCount")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "RootStatisticArea")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "RootStatisticArea2")
    //            {
    //                this.Activated();
    //            }
    //        }
    //    }

    //    #endregion // IPartImportsSatisfiedNotification
    //} // GeometryCountOverlay

    //[ExportExtension("Workbench.AddIn.MapStatistics.OverlayGroups.ObjectStatisticsGroup", typeof(ViewportOverlay))]
    //public class TXDCountOverlay : ViewportOverlay, IPartImportsSatisfiedNotification
    //{
    //    #region MEF Imports

    //    /// <summary>
    //    /// Map Browser
    //    /// </summary>
    //    [ImportExtension(Map.AddIn.Services.CompositionPoints.MapBrowser, typeof(IMapBrowser))]
    //    Lazy<IMapBrowser> MapBrowser
    //    {
    //        get;
    //        set;
    //    }

    //    /// <summary>
    //    /// Map Browser
    //    /// </summary>
    //    [ImportExtension("Workbench.AddIn.MapStatistics.MapStatisticsViewModel", typeof(MapStatisticsViewModel))]
    //    MapStatisticsViewModel ViewModel
    //    {
    //        get;
    //        set;
    //    }

    //    #endregion // MEF Imports

    //    #region Constructors

    //    /// <summary>
    //    /// Default Constructor
    //    /// </summary>
    //    public TXDCountOverlay()
    //    {
    //        this.Name = "TXD Count";
    //        this.Description = "Shows the indiviual containers for the map in a colour that ranages from red to green that represents " +
    //                                            "the txd count of that container compared with the other containers on the map.";
    //    }

    //    #endregion // Constructors

    //    #region Overrides

    //    /// <summary>
    //    /// Gets called when the user wants to view this overlay
    //    /// </summary>
    //    public override void Activated()
    //    {
    //        if (MapBrowser.Value.LevelGeometry != null && MapBrowser.Value.SelectedLevel != null)
    //        {
    //            int totalStat = 0;
    //            int statCount = 0;
    //            int minValue = int.MaxValue;
    //            int maxValue = int.MinValue;
    //            float range = 0;
    //            Dictionary<MapSectionViewModel, List<Vector2f>> statistics = new Dictionary<MapSectionViewModel, List<Vector2f>>();
    //            Dictionary<MapSectionViewModel, int> statValues = new Dictionary<MapSectionViewModel, int>();
    //            if (this.ViewModel.RootStatisticArea != null)
    //            {
    //                foreach (KeyValuePair<MapSectionViewModel, List<Vector2f>> kvp in MapBrowser.Value.LevelGeometry)
    //                {
    //                    if (ViewModelUtilities.ContainsComponent(this.ViewModel.RootStatisticArea, kvp.Key.Name, true) ||
    //                        ViewModelUtilities.ContainsComponent(this.ViewModel.RootStatisticArea2, kvp.Key.Name, true))
    //                    {
    //                        int stat = 0;
    //                        if (ViewModel.IncludeHighDetailObjects)
    //                            stat += (kvp.Key.Model).GetTXDCount(RSG.Model.Map.InstanceType.HD);
    //                        if (ViewModel.IncludeLodObjects)
    //                            stat += (kvp.Key.Model).GetTXDCount(RSG.Model.Map.InstanceType.LOD);
    //                        if (ViewModel.IncludeSLodObjects)
    //                            stat += (kvp.Key.Model).GetTXDCount(RSG.Model.Map.InstanceType.SLOD);
    //                        if (ViewModel.IncludeReferences)
    //                            stat += (kvp.Key.Model).GetTXDCount(RSG.Model.Map.InstanceType.REF);

    //                        if (stat != 0)
    //                        {
    //                            totalStat += stat;

    //                            if (stat > maxValue)
    //                                maxValue = stat;
    //                            if (stat < minValue)
    //                                minValue = stat;

    //                            statCount++;
    //                            statValues.Add(kvp.Key, stat);
    //                            statistics.Add(kvp.Key, kvp.Value);
    //                        }
    //                    }
    //                }
    //            }
    //            range = (maxValue - minValue);

    //            System.Windows.Media.Brush brush = System.Windows.Media.Brushes.Black;
    //            SortedDictionary<int, List<Viewport2DGeometry>> orderedGeometry = new SortedDictionary<int, List<Viewport2DGeometry>>();
    //            foreach (KeyValuePair<MapSectionViewModel, List<Vector2f>> kvp in statistics)
    //            {
    //                float percentage = statValues[kvp.Key];
    //                percentage /= range;
    //                if (percentage > 1.0f)
    //                {
    //                    percentage = 1.0f;
    //                }
    //                else if (percentage < 0.0f)
    //                {
    //                    percentage = 0.0f;
    //                }
    //                Vector3i difference = new Vector3i(127, -127, 0);
    //                byte r = (byte)(((int)(difference.X * percentage)));
    //                byte g = (byte)(((int)(difference.Y * percentage)) + 127);
    //                byte b = (byte)(((int)(difference.Z * percentage)));
    //                System.Windows.Media.Color colour = System.Windows.Media.Color.FromArgb(255, r, g, b);

    //                if (!orderedGeometry.ContainsKey(statValues[kvp.Key]))
    //                {
    //                    orderedGeometry.Add(statValues[kvp.Key], new List<Viewport2DGeometry>());
    //                }
    //                Viewport2DShape newGeometry = new Viewport2DShape(etCoordSpace.World, "Filled shape for section", kvp.Value.ToArray(), brush, 1, colour);
    //                newGeometry.UserText = kvp.Key.Name;
    //                newGeometry.PickData = kvp.Key;
    //                orderedGeometry[statValues[kvp.Key]].Add(newGeometry);
    //            }

    //            int index = 0;
    //            this.Geometry.BeginUpdate();
    //            this.Geometry.Clear();
    //            foreach (var statValue in orderedGeometry.Reverse())
    //            {
    //                if (++index > this.ViewModel.WorstCount && this.ViewModel.ShowWorst == true)
    //                {
    //                    break;
    //                }
    //                foreach (var geo in statValue.Value)
    //                {
    //                    this.Geometry.Add(geo);
    //                }
    //            }
    //            this.Geometry.EndUpdate();
    //        }
    //        base.Activated();
    //    }

    //    /// <summary>
    //    /// Gets called when the user turns off this overlay
    //    /// </summary>
    //    public override void Deactivated()
    //    {
    //        this.Geometry = new RSG.Base.Collections.ObservableCollection<Viewport2DGeometry>();
    //        base.Deactivated();
    //    }

    //    #endregion // Overrides

    //    #region IPartImportsSatisfiedNotification

    //    /// <summary>
    //    /// Gets called once all the imports have been satisfied
    //    /// </summary>
    //    public void OnImportsSatisfied()
    //    {
    //        ViewModel.PropertyChanged += OnPropertyChanged;
    //    }

    //    /// <summary>
    //    /// Gets called whenever a property on the map browser changes
    //    /// </summary>
    //    void OnPropertyChanged(Object sender, System.ComponentModel.PropertyChangedEventArgs e)
    //    {
    //        if (this.IsCurrentlyActive == true)
    //        {
    //            if (e.PropertyName == "IncludeHighDetailObjects")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "IncludeLodObjects")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "IncludeSLodObjects")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "IncludeReferences")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "ShowWorst")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "WorstCount")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "RootStatisticArea")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "RootStatisticArea2")
    //            {
    //                this.Activated();
    //            }
    //        }
    //    }

    //    #endregion // IPartImportsSatisfiedNotification
    //} // TXDCountOverlay

    //[ExportExtension("Workbench.AddIn.MapStatistics.OverlayGroups.ObjectStatisticsGroup", typeof(ViewportOverlay))]
    //public class PolycountCountOverlay : ViewportOverlay, IPartImportsSatisfiedNotification
    //{
    //    #region MEF Imports

    //    /// <summary>
    //    /// Map Browser
    //    /// </summary>
    //    [ImportExtension(Map.AddIn.Services.CompositionPoints.MapBrowser, typeof(IMapBrowser))]
    //    Lazy<IMapBrowser> MapBrowser
    //    {
    //        get;
    //        set;
    //    }

    //    /// <summary>
    //    /// Map Browser
    //    /// </summary>
    //    [ImportExtension("Workbench.AddIn.MapStatistics.MapStatisticsViewModel", typeof(MapStatisticsViewModel))]
    //    MapStatisticsViewModel ViewModel
    //    {
    //        get;
    //        set;
    //    }

    //    #endregion // MEF Imports

    //    #region Constructors

    //    /// <summary>
    //    /// Default Constructor
    //    /// </summary>
    //    public PolycountCountOverlay()
    //    {
    //        this.Name = "Polygon Count";
    //        this.Description = "Shows the indiviual containers for the map in a colour that ranages from red to green that represents " +
    //                                            "the polygon count of that container compared with the other containers on the map.";
    //    }

    //    #endregion // Constructors

    //    #region Overrides

    //    /// <summary>
    //    /// Gets called when the user wants to view this overlay
    //    /// </summary>
    //    public override void Activated()
    //    {
    //        if (MapBrowser.Value.LevelGeometry != null && MapBrowser.Value.SelectedLevel != null)
    //        {
    //            int totalStat = 0;
    //            int statCount = 0;
    //            int minValue = int.MaxValue;
    //            int maxValue = int.MinValue;
    //            float range = 0;
    //            Dictionary<MapSectionViewModel, List<Vector2f>> statistics = new Dictionary<MapSectionViewModel, List<Vector2f>>();
    //            Dictionary<MapSectionViewModel, int> statValues = new Dictionary<MapSectionViewModel, int>();
    //            if (this.ViewModel.RootStatisticArea != null)
    //            {
    //                foreach (KeyValuePair<MapSectionViewModel, List<Vector2f>> kvp in MapBrowser.Value.LevelGeometry)
    //                {
    //                    if (ViewModelUtilities.ContainsComponent(this.ViewModel.RootStatisticArea, kvp.Key.Name, true) ||
    //                        ViewModelUtilities.ContainsComponent(this.ViewModel.RootStatisticArea2, kvp.Key.Name, true))
    //                    {
    //                        Boolean unique = ViewModel.IncludeJustUniqueReferences;
    //                        int stat = 0;
    //                        if (ViewModel.IncludeHighDetailObjects)
    //                            stat += (kvp.Key.Model).GetPolycount(RSG.Model.Map.InstanceType.HD, unique);
    //                        if (ViewModel.IncludeLodObjects)
    //                            stat += (kvp.Key.Model).GetPolycount(RSG.Model.Map.InstanceType.LOD, unique);
    //                        if (ViewModel.IncludeSLodObjects)
    //                            stat += (kvp.Key.Model).GetPolycount(RSG.Model.Map.InstanceType.SLOD, unique);
    //                        if (ViewModel.IncludeReferences)
    //                            stat += (kvp.Key.Model).GetPolycount(RSG.Model.Map.InstanceType.REF, unique);

    //                        if (stat != 0)
    //                        {
    //                            totalStat += stat;

    //                            if (stat > maxValue)
    //                                maxValue = stat;
    //                            if (stat < minValue)
    //                                minValue = stat;

    //                            statCount++;
    //                            statValues.Add(kvp.Key, stat);
    //                            statistics.Add(kvp.Key, kvp.Value);
    //                        }
    //                    }
    //                }
    //            }
    //            range = (maxValue - minValue);

    //            System.Windows.Media.Brush brush = System.Windows.Media.Brushes.Black;
    //            SortedDictionary<int, List<Viewport2DGeometry>> orderedGeometry = new SortedDictionary<int, List<Viewport2DGeometry>>();
    //            foreach (KeyValuePair<MapSectionViewModel, List<Vector2f>> kvp in statistics)
    //            {
    //                float percentage = statValues[kvp.Key];
    //                percentage /= range;
    //                if (percentage > 1.0f)
    //                {
    //                    percentage = 1.0f;
    //                }
    //                else if (percentage < 0.0f)
    //                {
    //                    percentage = 0.0f;
    //                }
    //                Vector3i difference = new Vector3i(127, -127, 0);
    //                byte r = (byte)(((int)(difference.X * percentage)));
    //                byte g = (byte)(((int)(difference.Y * percentage)) + 127);
    //                byte b = (byte)(((int)(difference.Z * percentage)));
    //                System.Windows.Media.Color colour = System.Windows.Media.Color.FromArgb(255, r, g, b);

    //                if (!orderedGeometry.ContainsKey(statValues[kvp.Key]))
    //                {
    //                    orderedGeometry.Add(statValues[kvp.Key], new List<Viewport2DGeometry>());
    //                }
    //                Viewport2DShape newGeometry = new Viewport2DShape(etCoordSpace.World, "Filled shape for section", kvp.Value.ToArray(), brush, 1, colour);
    //                newGeometry.UserText = kvp.Key.Name;
    //                newGeometry.PickData = kvp.Key;
    //                orderedGeometry[statValues[kvp.Key]].Add(newGeometry);
    //            }

    //            int index = 0;
    //            this.Geometry.BeginUpdate();
    //            this.Geometry.Clear();
    //            foreach (var statValue in orderedGeometry.Reverse())
    //            {
    //                if (++index > this.ViewModel.WorstCount && this.ViewModel.ShowWorst == true)
    //                {
    //                    break;
    //                }
    //                foreach (var geo in statValue.Value)
    //                {
    //                    this.Geometry.Add(geo);
    //                }
    //            }
    //            this.Geometry.EndUpdate();
    //        }
    //        base.Activated();
    //    }

    //    /// <summary>
    //    /// Gets called when the user turns off this overlay
    //    /// </summary>
    //    public override void Deactivated()
    //    {
    //        this.Geometry = new RSG.Base.Collections.ObservableCollection<Viewport2DGeometry>();
    //        base.Deactivated();
    //    }

    //    #endregion // Overrides

    //    #region IPartImportsSatisfiedNotification

    //    /// <summary>
    //    /// Gets called once all the imports have been satisfied
    //    /// </summary>
    //    public void OnImportsSatisfied()
    //    {
    //        ViewModel.PropertyChanged += OnPropertyChanged;
    //    }

    //    /// <summary>
    //    /// Gets called whenever a property on the map browser changes
    //    /// </summary>
    //    void OnPropertyChanged(Object sender, System.ComponentModel.PropertyChangedEventArgs e)
    //    {
    //        if (this.IsCurrentlyActive == true)
    //        {
    //            if (e.PropertyName == "IncludeHighDetailObjects")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "IncludeLodObjects")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "IncludeSLodObjects")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "IncludeReferences")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "IncludeJustUniqueReferences")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "ShowWorst")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "WorstCount")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "RootStatisticArea")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "RootStatisticArea2")
    //            {
    //                this.Activated();
    //            }
    //        }
    //    }

    //    #endregion // IPartImportsSatisfiedNotification
    //} // PolycountCountOverlay

    //[ExportExtension("Workbench.AddIn.MapStatistics.OverlayGroups.ObjectStatisticsGroup", typeof(ViewportOverlay))]
    //public class PolycountDensityOverlay : ViewportOverlay, IPartImportsSatisfiedNotification
    //{
    //    #region MEF Imports

    //    /// <summary>
    //    /// Map Browser
    //    /// </summary>
    //    [ImportExtension(Map.AddIn.Services.CompositionPoints.MapBrowser, typeof(IMapBrowser))]
    //    Lazy<IMapBrowser> MapBrowser
    //    {
    //        get;
    //        set;
    //    }

    //    /// <summary>
    //    /// Map Browser
    //    /// </summary>
    //    [ImportExtension("Workbench.AddIn.MapStatistics.MapStatisticsViewModel", typeof(MapStatisticsViewModel))]
    //    MapStatisticsViewModel ViewModel
    //    {
    //        get;
    //        set;
    //    }

    //    #endregion // MEF Imports

    //    #region Constructors

    //    /// <summary>
    //    /// Default Constructor
    //    /// </summary>
    //    public PolycountDensityOverlay()
    //    {
    //        this.Name = "Polygon Density";
    //        this.Description = "Shows the indiviual containers for the map in a colour that ranages from red to green that represents " +
    //                                            "the polygon density of that container compared with the other containers on the map.";
    //    }

    //    #endregion // Constructors

    //    #region Overrides

    //    /// <summary>
    //    /// Gets called when the user wants to view this overlay
    //    /// </summary>
    //    public override void Activated()
    //    {
    //        if (MapBrowser.Value.LevelGeometry != null && MapBrowser.Value.SelectedLevel != null)
    //        {
    //            float totalStat = 0;
    //            int statCount = 0;
    //            float minValue = float.MaxValue;
    //            float maxValue = float.MinValue;
    //            float range = 0;
    //            Dictionary<MapSectionViewModel, List<Vector2f>> statistics = new Dictionary<MapSectionViewModel, List<Vector2f>>();
    //            Dictionary<MapSectionViewModel, float> statValues = new Dictionary<MapSectionViewModel, float>();
    //            if (this.ViewModel.RootStatisticArea != null)
    //            {
    //                foreach (KeyValuePair<MapSectionViewModel, List<Vector2f>> kvp in MapBrowser.Value.LevelGeometry)
    //                {
    //                    if (ViewModelUtilities.ContainsComponent(this.ViewModel.RootStatisticArea, kvp.Key.Name, true) ||
    //                        ViewModelUtilities.ContainsComponent(this.ViewModel.RootStatisticArea2, kvp.Key.Name, true))
    //                    {
    //                        Boolean unique = ViewModel.IncludeJustUniqueReferences;
    //                        float stat = 0.0f;
    //                        if (ViewModel.IncludeHighDetailObjects)
    //                            stat += (kvp.Key.Model).GetPolycountDensity(RSG.Model.Map.InstanceType.HD, unique);
    //                        if (ViewModel.IncludeLodObjects)
    //                            stat += (kvp.Key.Model).GetPolycountDensity(RSG.Model.Map.InstanceType.LOD, unique);
    //                        if (ViewModel.IncludeSLodObjects)
    //                            stat += (kvp.Key.Model).GetPolycountDensity(RSG.Model.Map.InstanceType.SLOD, unique);
    //                        if (ViewModel.IncludeReferences)
    //                            stat += (kvp.Key.Model).GetPolycountDensity(RSG.Model.Map.InstanceType.REF, unique);

    //                        if (stat != 0.0f)
    //                        {
    //                            totalStat += stat;

    //                            if (stat > maxValue)
    //                                maxValue = stat;
    //                            if (stat < minValue)
    //                                minValue = stat;

    //                            statCount++;
    //                            statValues.Add(kvp.Key, stat);
    //                            statistics.Add(kvp.Key, kvp.Value);
    //                        }
    //                    }
    //                }
    //            }
    //            range = (maxValue - minValue);

    //            System.Windows.Media.Brush brush = System.Windows.Media.Brushes.Black;
    //            SortedDictionary<float, List<Viewport2DGeometry>> orderedGeometry = new SortedDictionary<float, List<Viewport2DGeometry>>();
    //            foreach (KeyValuePair<MapSectionViewModel, List<Vector2f>> kvp in statistics)
    //            {
    //                float percentage = statValues[kvp.Key];
    //                percentage /= range;
    //                if (percentage > 1.0f)
    //                {
    //                    percentage = 1.0f;
    //                }
    //                else if (percentage < 0.0f)
    //                {
    //                    percentage = 0.0f;
    //                }
    //                Vector3i difference = new Vector3i(127, -127, 0);
    //                byte r = (byte)(((int)(difference.X * percentage)));
    //                byte g = (byte)(((int)(difference.Y * percentage)) + 127);
    //                byte b = (byte)(((int)(difference.Z * percentage)));
    //                System.Windows.Media.Color colour = System.Windows.Media.Color.FromArgb(255, r, g, b);

    //                if (!orderedGeometry.ContainsKey(statValues[kvp.Key]))
    //                {
    //                    orderedGeometry.Add(statValues[kvp.Key], new List<Viewport2DGeometry>());
    //                }
    //                Viewport2DShape newGeometry = new Viewport2DShape(etCoordSpace.World, "Filled shape for section", kvp.Value.ToArray(), brush, 1, colour);
    //                newGeometry.UserText = kvp.Key.Name;
    //                newGeometry.PickData = kvp.Key;
    //                orderedGeometry[statValues[kvp.Key]].Add(newGeometry);
    //            }

    //            int index = 0;
    //            this.Geometry.BeginUpdate();
    //            this.Geometry.Clear();
    //            foreach (var statValue in orderedGeometry.Reverse())
    //            {
    //                if (++index > this.ViewModel.WorstCount && this.ViewModel.ShowWorst == true)
    //                {
    //                    break;
    //                }
    //                foreach (var geo in statValue.Value)
    //                {
    //                    this.Geometry.Add(geo);
    //                }
    //            }
    //            this.Geometry.EndUpdate();
    //        }
    //        base.Activated();
    //    }

    //    /// <summary>
    //    /// Gets called when the user turns off this overlay
    //    /// </summary>
    //    public override void Deactivated()
    //    {
    //        this.Geometry = new RSG.Base.Collections.ObservableCollection<Viewport2DGeometry>();
    //        base.Deactivated();
    //    }

    //    #endregion // Overrides

    //    #region IPartImportsSatisfiedNotification

    //    /// <summary>
    //    /// Gets called once all the imports have been satisfied
    //    /// </summary>
    //    public void OnImportsSatisfied()
    //    {
    //        ViewModel.PropertyChanged += OnPropertyChanged;
    //    }

    //    /// <summary>
    //    /// Gets called whenever a property on the map browser changes
    //    /// </summary>
    //    void OnPropertyChanged(Object sender, System.ComponentModel.PropertyChangedEventArgs e)
    //    {
    //        if (this.IsCurrentlyActive == true)
    //        {
    //            if (e.PropertyName == "IncludeHighDetailObjects")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "IncludeLodObjects")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "IncludeSLodObjects")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "IncludeReferences")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "IncludeJustUniqueReferences")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "ShowWorst")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "WorstCount")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "RootStatisticArea")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "RootStatisticArea2")
    //            {
    //                this.Activated();
    //            }
    //        }
    //    }

    //    #endregion // IPartImportsSatisfiedNotification
    //} // PolycountDensityOverlay

    //[ExportExtension("Workbench.AddIn.MapStatistics.OverlayGroups.CollisionStatisticsGroup", typeof(ViewportOverlay))]
    //public class CollisionPolycountCountOverlay : ViewportOverlay, IPartImportsSatisfiedNotification
    //{
    //    #region MEF Imports

    //    /// <summary>
    //    /// Map Browser
    //    /// </summary>
    //    [ImportExtension(Map.AddIn.Services.CompositionPoints.MapBrowser, typeof(IMapBrowser))]
    //    Lazy<IMapBrowser> MapBrowser
    //    {
    //        get;
    //        set;
    //    }

    //    /// <summary>
    //    /// Map Browser
    //    /// </summary>
    //    [ImportExtension("Workbench.AddIn.MapStatistics.MapStatisticsViewModel", typeof(MapStatisticsViewModel))]
    //    MapStatisticsViewModel ViewModel
    //    {
    //        get;
    //        set;
    //    }

    //    #endregion // MEF Imports

    //    #region Constructors

    //    /// <summary>
    //    /// Default Constructor
    //    /// </summary>
    //    public CollisionPolycountCountOverlay()
    //    {
    //        this.Name = "Collision Polygon Count";
    //        this.Description = "Shows the indiviual containers for the map in a colour that ranages from red to green that represents " +
    //                                            "the collision polygon count of that container compared with the other containers on the map.";
    //    }

    //    #endregion // Constructors

    //    #region Overrides

    //    /// <summary>
    //    /// Gets called when the user wants to view this overlay
    //    /// </summary>
    //    public override void Activated()
    //    {
    //        if (MapBrowser.Value.LevelGeometry != null && MapBrowser.Value.SelectedLevel != null)
    //        {
    //            int totalStat = 0;
    //            int statCount = 0;
    //            int minValue = int.MaxValue;
    //            int maxValue = int.MinValue;
    //            float range = 0;
    //            Dictionary<MapSectionViewModel, List<Vector2f>> statistics = new Dictionary<MapSectionViewModel, List<Vector2f>>();
    //            Dictionary<MapSectionViewModel, int> statValues = new Dictionary<MapSectionViewModel, int>();
    //            if (this.ViewModel.RootStatisticArea != null)
    //            {
    //                foreach (KeyValuePair<MapSectionViewModel, List<Vector2f>> kvp in MapBrowser.Value.LevelGeometry)
    //                {
    //                    if (ViewModelUtilities.ContainsComponent(this.ViewModel.RootStatisticArea, kvp.Key.Name, true) ||
    //                        ViewModelUtilities.ContainsComponent(this.ViewModel.RootStatisticArea2, kvp.Key.Name, true))
    //                    {
    //                        Boolean unique = ViewModel.IncludeJustUniqueReferences;
    //                        int stat = 0;
    //                        if (ViewModel.IncludeHighDetailObjects)
    //                            stat += (kvp.Key.Model).GetCollisionPolycount(RSG.Model.Map.InstanceType.HD, unique);
    //                        if (ViewModel.IncludeReferences)
    //                            stat += (kvp.Key.Model).GetCollisionPolycount(RSG.Model.Map.InstanceType.REF, unique);

    //                        if (stat != 0)
    //                        {
    //                            totalStat += stat;

    //                            if (stat > maxValue)
    //                                maxValue = stat;
    //                            if (stat < minValue)
    //                                minValue = stat;

    //                            statCount++;
    //                            statValues.Add(kvp.Key, stat);
    //                            statistics.Add(kvp.Key, kvp.Value);
    //                        }
    //                    }
    //                }
    //            }
    //            range = (maxValue - minValue);

    //            System.Windows.Media.Brush brush = System.Windows.Media.Brushes.Black;
    //            SortedDictionary<int, List<Viewport2DGeometry>> orderedGeometry = new SortedDictionary<int, List<Viewport2DGeometry>>();
    //            foreach (KeyValuePair<MapSectionViewModel, List<Vector2f>> kvp in statistics)
    //            {
    //                float percentage = statValues[kvp.Key];
    //                percentage /= range;
    //                if (percentage > 1.0f)
    //                {
    //                    percentage = 1.0f;
    //                }
    //                else if (percentage < 0.0f)
    //                {
    //                    percentage = 0.0f;
    //                }
    //                Vector3i difference = new Vector3i(127, -127, 0);
    //                byte r = (byte)(((int)(difference.X * percentage)));
    //                byte g = (byte)(((int)(difference.Y * percentage)) + 127);
    //                byte b = (byte)(((int)(difference.Z * percentage)));
    //                System.Windows.Media.Color colour = System.Windows.Media.Color.FromArgb(255, r, g, b);

    //                if (!orderedGeometry.ContainsKey(statValues[kvp.Key]))
    //                {
    //                    orderedGeometry.Add(statValues[kvp.Key], new List<Viewport2DGeometry>());
    //                }
    //                Viewport2DShape newGeometry = new Viewport2DShape(etCoordSpace.World, "Filled shape for section", kvp.Value.ToArray(), brush, 1, colour);
    //                newGeometry.UserText = kvp.Key.Name;
    //                newGeometry.PickData = kvp.Key;
    //                orderedGeometry[statValues[kvp.Key]].Add(newGeometry);
    //            }

    //            int index = 0;
    //            this.Geometry.BeginUpdate();
    //            this.Geometry.Clear();
    //            foreach (var statValue in orderedGeometry.Reverse())
    //            {
    //                if (++index > this.ViewModel.WorstCount && this.ViewModel.ShowWorst == true)
    //                {
    //                    break;
    //                }
    //                foreach (var geo in statValue.Value)
    //                {
    //                    this.Geometry.Add(geo);
    //                }
    //            }
    //            this.Geometry.EndUpdate();
    //        }
    //        base.Activated();
    //    }

    //    /// <summary>
    //    /// Gets called when the user turns off this overlay
    //    /// </summary>
    //    public override void Deactivated()
    //    {
    //        this.Geometry = new RSG.Base.Collections.ObservableCollection<Viewport2DGeometry>();
    //        base.Deactivated();
    //    }

    //    #endregion // Overrides

    //    #region IPartImportsSatisfiedNotification

    //    /// <summary>
    //    /// Gets called once all the imports have been satisfied
    //    /// </summary>
    //    public void OnImportsSatisfied()
    //    {
    //        ViewModel.PropertyChanged += OnPropertyChanged;
    //    }

    //    /// <summary>
    //    /// Gets called whenever a property on the map browser changes
    //    /// </summary>
    //    void OnPropertyChanged(Object sender, System.ComponentModel.PropertyChangedEventArgs e)
    //    {
    //        if (this.IsCurrentlyActive == true)
    //        {
    //            if (e.PropertyName == "IncludeHighDetailObjects")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "IncludeReferences")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "ShowWorst")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "WorstCount")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "RootStatisticArea")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "RootStatisticArea2")
    //            {
    //                this.Activated();
    //            }
    //        }
    //    }

    //    #endregion // IPartImportsSatisfiedNotification
    //} // CollisionPolycountCountOverlay

    //[ExportExtension("Workbench.AddIn.MapStatistics.OverlayGroups.CollisionStatisticsGroup", typeof(ViewportOverlay))]
    //public class CollisionPolycountDensityOverlay : ViewportOverlay, IPartImportsSatisfiedNotification
    //{
    //    #region MEF Imports

    //    /// <summary>
    //    /// Map Browser
    //    /// </summary>
    //    [ImportExtension(Map.AddIn.Services.CompositionPoints.MapBrowser, typeof(IMapBrowser))]
    //    Lazy<IMapBrowser> MapBrowser
    //    {
    //        get;
    //        set;
    //    }

    //    /// <summary>
    //    /// Map Browser
    //    /// </summary>
    //    [ImportExtension("Workbench.AddIn.MapStatistics.MapStatisticsViewModel", typeof(MapStatisticsViewModel))]
    //    MapStatisticsViewModel ViewModel
    //    {
    //        get;
    //        set;
    //    }

    //    #endregion // MEF Imports

    //    #region Constructors

    //    /// <summary>
    //    /// Default Constructor
    //    /// </summary>
    //    public CollisionPolycountDensityOverlay()
    //    {
    //        this.Name = "Collision Polygon Density";
    //        this.Description = "Shows the indiviual containers for the map in a colour that ranages from red to green that represents " +
    //                                            "the collision polygon density of that container compared with the other containers on the map.";
    //    }

    //    #endregion // Constructors

    //    #region Overrides

    //    /// <summary>
    //    /// Gets called when the user wants to view this overlay
    //    /// </summary>
    //    public override void Activated()
    //    {
    //        if (MapBrowser.Value.LevelGeometry != null && MapBrowser.Value.SelectedLevel != null)
    //        {
    //            float totalStat = 0;
    //            int statCount = 0;
    //            float minValue = float.MaxValue;
    //            float maxValue = float.MinValue;
    //            float range = 0;
    //            Dictionary<MapSectionViewModel, List<Vector2f>> statistics = new Dictionary<MapSectionViewModel, List<Vector2f>>();
    //            Dictionary<MapSectionViewModel, float> statValues = new Dictionary<MapSectionViewModel, float>();
    //            if (this.ViewModel.RootStatisticArea != null)
    //            {
    //                foreach (KeyValuePair<MapSectionViewModel, List<Vector2f>> kvp in MapBrowser.Value.LevelGeometry)
    //                {
    //                    if (ViewModelUtilities.ContainsComponent(this.ViewModel.RootStatisticArea, kvp.Key.Name, true) ||
    //                        ViewModelUtilities.ContainsComponent(this.ViewModel.RootStatisticArea2, kvp.Key.Name, true))
    //                    {
    //                        Boolean unique = ViewModel.IncludeJustUniqueReferences;
    //                        float stat = 0.0f;
    //                        if (ViewModel.IncludeHighDetailObjects)
    //                            stat += (kvp.Key.Model).GetCollisionPolycountDensity(RSG.Model.Map.InstanceType.HD, unique);
    //                        if (ViewModel.IncludeReferences)
    //                            stat += (kvp.Key.Model).GetCollisionPolycountDensity(RSG.Model.Map.InstanceType.REF, unique);

    //                        if (stat != 0)
    //                        {
    //                            totalStat += stat;

    //                            if (stat > maxValue)
    //                                maxValue = stat;
    //                            if (stat < minValue)
    //                                minValue = stat;

    //                            statCount++;
    //                            statValues.Add(kvp.Key, stat);
    //                            statistics.Add(kvp.Key, kvp.Value);
    //                        }
    //                    }
    //                }
    //            }
    //            range = (maxValue - minValue);

    //            System.Windows.Media.Brush brush = System.Windows.Media.Brushes.Black;
    //            SortedDictionary<float, List<Viewport2DGeometry>> orderedGeometry = new SortedDictionary<float, List<Viewport2DGeometry>>();
    //            foreach (KeyValuePair<MapSectionViewModel, List<Vector2f>> kvp in statistics)
    //            {
    //                float percentage = statValues[kvp.Key];
    //                percentage /= range;
    //                if (percentage > 1.0f)
    //                {
    //                    percentage = 1.0f;
    //                }
    //                else if (percentage < 0.0f)
    //                {
    //                    percentage = 0.0f;
    //                }
    //                Vector3i difference = new Vector3i(127, -127, 0);
    //                byte r = (byte)(((int)(difference.X * percentage)));
    //                byte g = (byte)(((int)(difference.Y * percentage)) + 127);
    //                byte b = (byte)(((int)(difference.Z * percentage)));
    //                System.Windows.Media.Color colour = System.Windows.Media.Color.FromArgb(255, r, g, b);

    //                if (!orderedGeometry.ContainsKey(statValues[kvp.Key]))
    //                {
    //                    orderedGeometry.Add(statValues[kvp.Key], new List<Viewport2DGeometry>());
    //                }
    //                Viewport2DShape newGeometry = new Viewport2DShape(etCoordSpace.World, "Filled shape for section", kvp.Value.ToArray(), brush, 1, colour);
    //                newGeometry.UserText = kvp.Key.Name;
    //                newGeometry.PickData = kvp.Key;
    //                orderedGeometry[statValues[kvp.Key]].Add(newGeometry);
    //            }

    //            int index = 0;
    //            this.Geometry.BeginUpdate();
    //            this.Geometry.Clear();
    //            foreach (var statValue in orderedGeometry.Reverse())
    //            {
    //                if (++index > this.ViewModel.WorstCount && this.ViewModel.ShowWorst == true)
    //                {
    //                    break;
    //                }
    //                foreach (var geo in statValue.Value)
    //                {
    //                    this.Geometry.Add(geo);
    //                }
    //            }
    //            this.Geometry.EndUpdate();
    //        }
    //        base.Activated();
    //    }

    //    /// <summary>
    //    /// Gets called when the user turns off this overlay
    //    /// </summary>
    //    public override void Deactivated()
    //    {
    //        this.Geometry = new RSG.Base.Collections.ObservableCollection<Viewport2DGeometry>();
    //        base.Deactivated();
    //    }

    //    #endregion // Overrides

    //    #region IPartImportsSatisfiedNotification

    //    /// <summary>
    //    /// Gets called once all the imports have been satisfied
    //    /// </summary>
    //    public void OnImportsSatisfied()
    //    {
    //        ViewModel.PropertyChanged += OnPropertyChanged;
    //    }

    //    /// <summary>
    //    /// Gets called whenever a property on the map browser changes
    //    /// </summary>
    //    void OnPropertyChanged(Object sender, System.ComponentModel.PropertyChangedEventArgs e)
    //    {
    //        if (this.IsCurrentlyActive == true)
    //        {
    //            if (e.PropertyName == "IncludeHighDetailObjects")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "IncludeReferences")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "ShowWorst")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "WorstCount")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "RootStatisticArea")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "RootStatisticArea2")
    //            {
    //                this.Activated();
    //            }
    //        }
    //    }

    //    #endregion // IPartImportsSatisfiedNotification
    //} // CollisionPolycountDensityOverlay

    //[ExportExtension("Workbench.AddIn.MapStatistics.OverlayGroups.CollisionStatisticsGroup", typeof(ViewportOverlay))]
    //public class CollisionPolycountRatioOverlay : ViewportOverlay, IPartImportsSatisfiedNotification
    //{
    //    #region MEF Imports

    //    /// <summary>
    //    /// Map Browser
    //    /// </summary>
    //    [ImportExtension(Map.AddIn.Services.CompositionPoints.MapBrowser, typeof(IMapBrowser))]
    //    Lazy<IMapBrowser> MapBrowser
    //    {
    //        get;
    //        set;
    //    }

    //    /// <summary>
    //    /// Map Browser
    //    /// </summary>
    //    [ImportExtension("Workbench.AddIn.MapStatistics.MapStatisticsViewModel", typeof(MapStatisticsViewModel))]
    //    MapStatisticsViewModel ViewModel
    //    {
    //        get;
    //        set;
    //    }

    //    #endregion // MEF Imports

    //    #region Constructors

    //    /// <summary>
    //    /// Default Constructor
    //    /// </summary>
    //    public CollisionPolycountRatioOverlay()
    //    {
    //        this.Name = "Collision Polygon Ratio";
    //        this.Description = "Shows the indiviual containers for the map in a colour that ranages from red to green that represents " +
    //                                            "the collision polygon ratio of that container compared with the other containers on the map.";
    //    }

    //    #endregion // Constructors

    //    #region Overrides

    //    /// <summary>
    //    /// Gets called when the user wants to view this overlay
    //    /// </summary>
    //    public override void Activated()
    //    {
    //        if (MapBrowser.Value.LevelGeometry != null && MapBrowser.Value.SelectedLevel != null)
    //        {
    //            float totalStat = 0;
    //            int statCount = 0;
    //            float minValue = float.MaxValue;
    //            float maxValue = float.MinValue;
    //            float range = 0;
    //            Dictionary<MapSectionViewModel, List<Vector2f>> statistics = new Dictionary<MapSectionViewModel, List<Vector2f>>();
    //            Dictionary<MapSectionViewModel, float> statValues = new Dictionary<MapSectionViewModel, float>();
    //            if (this.ViewModel.RootStatisticArea != null)
    //            {
    //                foreach (KeyValuePair<MapSectionViewModel, List<Vector2f>> kvp in MapBrowser.Value.LevelGeometry)
    //                {
    //                    if (kvp.Key.Model.SectionStatistics is MapContainer)
    //                    {
    //                        if (ViewModelUtilities.ContainsComponent(this.ViewModel.RootStatisticArea, kvp.Key.Name, true) ||
    //                            ViewModelUtilities.ContainsComponent(this.ViewModel.RootStatisticArea2, kvp.Key.Name, true))
    //                        {
    //                            Boolean unique = ViewModel.IncludeJustUniqueReferences;
    //                            float stat = 0.0f;
    //                            if (ViewModel.IncludeHighDetailObjects)
    //                                stat += (kvp.Key.Model).GetCollisionRatio(RSG.Model.Map.InstanceType.HD, unique);
    //                            if (ViewModel.IncludeReferences)
    //                                stat += (kvp.Key.Model).GetCollisionRatio(RSG.Model.Map.InstanceType.REF, unique);

    //                            if (stat != 0.0f)
    //                            {
    //                                totalStat += stat;

    //                                if (stat > maxValue)
    //                                    maxValue = stat;
    //                                if (stat < minValue)
    //                                    minValue = stat;

    //                                statCount++;
    //                                statValues.Add(kvp.Key, stat);
    //                                statistics.Add(kvp.Key, kvp.Value);
    //                            }
    //                        }
    //                    }
    //                }
    //            }
    //            range = (maxValue - minValue);

    //            System.Windows.Media.Brush brush = System.Windows.Media.Brushes.Black;
    //            SortedDictionary<float, List<Viewport2DGeometry>> orderedGeometry = new SortedDictionary<float, List<Viewport2DGeometry>>();
    //            foreach (KeyValuePair<MapSectionViewModel, List<Vector2f>> kvp in statistics)
    //            {
    //                float percentage = statValues[kvp.Key];
    //                percentage /= range;
    //                if (percentage > 1.0f)
    //                {
    //                    percentage = 1.0f;
    //                }
    //                else if (percentage < 0.0f)
    //                {
    //                    percentage = 0.0f;
    //                }
    //                Vector3i difference = new Vector3i(127, -127, 0);
    //                byte r = (byte)(((int)(difference.X * percentage)));
    //                byte g = (byte)(((int)(difference.Y * percentage)) + 127);
    //                byte b = (byte)(((int)(difference.Z * percentage)));
    //                System.Windows.Media.Color colour = System.Windows.Media.Color.FromArgb(255, r, g, b);

    //                if (!orderedGeometry.ContainsKey(statValues[kvp.Key]))
    //                {
    //                    orderedGeometry.Add(statValues[kvp.Key], new List<Viewport2DGeometry>());
    //                }
    //                Viewport2DShape newGeometry = new Viewport2DShape(etCoordSpace.World, "Filled shape for section", kvp.Value.ToArray(), brush, 1, colour);
    //                newGeometry.UserText = kvp.Key.Name;
    //                newGeometry.PickData = kvp.Key;
    //                orderedGeometry[statValues[kvp.Key]].Add(newGeometry);
    //            }

    //            int index = 0;
    //            this.Geometry.BeginUpdate();
    //            this.Geometry.Clear();
    //            foreach (var statValue in orderedGeometry.Reverse())
    //            {
    //                if (++index > this.ViewModel.WorstCount && this.ViewModel.ShowWorst == true)
    //                {
    //                    break;
    //                }
    //                foreach (var geo in statValue.Value)
    //                {
    //                    this.Geometry.Add(geo);
    //                }
    //            }
    //            this.Geometry.EndUpdate();
    //        }
    //        base.Activated();
    //    }

    //    /// <summary>
    //    /// Gets called when the user turns off this overlay
    //    /// </summary>
    //    public override void Deactivated()
    //    {
    //        this.Geometry = new RSG.Base.Collections.ObservableCollection<Viewport2DGeometry>();
    //        base.Deactivated();
    //    }

    //    #endregion // Overrides

    //    #region IPartImportsSatisfiedNotification

    //    /// <summary>
    //    /// Gets called once all the imports have been satisfied
    //    /// </summary>
    //    public void OnImportsSatisfied()
    //    {
    //        ViewModel.PropertyChanged += OnPropertyChanged;
    //    }

    //    /// <summary>
    //    /// Gets called whenever a property on the map browser changes
    //    /// </summary>
    //    void OnPropertyChanged(Object sender, System.ComponentModel.PropertyChangedEventArgs e)
    //    {
    //        if (this.IsCurrentlyActive == true)
    //        {
    //            if (e.PropertyName == "IncludeHighDetailObjects")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "IncludeReferences")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "ShowWorst")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "WorstCount")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "RootStatisticArea")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "RootStatisticArea2")
    //            {
    //                this.Activated();
    //            }
    //        }
    //    }

    //    #endregion // IPartImportsSatisfiedNotification
    //} // CollisionPolycountRatioOverlay

    //[ExportExtension("Workbench.AddIn.MapStatistics.OverlayGroups.LodDistanceStatisticsGroup", typeof(ViewportOverlay))]
    //public class HighLodDistanceRatioOverlay : ViewportOverlay, IPartImportsSatisfiedNotification
    //{
    //    #region MEF Imports

    //    /// <summary>
    //    /// Map Browser
    //    /// </summary>
    //    [ImportExtension(Map.AddIn.Services.CompositionPoints.MapBrowser, typeof(IMapBrowser))]
    //    Lazy<IMapBrowser> MapBrowser
    //    {
    //        get;
    //        set;
    //    }

    //    /// <summary>
    //    /// Map Browser
    //    /// </summary>
    //    [ImportExtension("Workbench.AddIn.MapStatistics.MapStatisticsViewModel", typeof(MapStatisticsViewModel))]
    //    MapStatisticsViewModel ViewModel
    //    {
    //        get;
    //        set;
    //    }

    //    #endregion // MEF Imports

    //    #region Constructors

    //    /// <summary>
    //    /// Default Constructor
    //    /// </summary>
    //    public HighLodDistanceRatioOverlay()
    //    {
    //        this.Name = "High Detail Lod Distance";
    //        this.Description = "Shows the indiviual containers for the map in a colour that ranages from red to green that represents " +
    //                                            "the high deatil lod distance of that container compared with the other containers on the map.";
    //    }

    //    #endregion // Constructors

    //    #region Overrides

    //    /// <summary>
    //    /// Gets called when the user wants to view this overlay
    //    /// </summary>
    //    public override void Activated()
    //    {
    //        if (MapBrowser.Value.LevelGeometry != null && MapBrowser.Value.SelectedLevel != null)
    //        {
    //            float totalStat = 0;
    //            int statCount = 0;
    //            float minValue = float.MaxValue;
    //            float maxValue = float.MinValue;
    //            float range = 0;
    //            Dictionary<MapSectionViewModel, List<Vector2f>> statistics = new Dictionary<MapSectionViewModel, List<Vector2f>>();
    //            Dictionary<MapSectionViewModel, float> statValues = new Dictionary<MapSectionViewModel, float>();
    //            if (this.ViewModel.RootStatisticArea != null)
    //            {
    //                foreach (KeyValuePair<MapSectionViewModel, List<Vector2f>> kvp in MapBrowser.Value.LevelGeometry)
    //                {
    //                    if (ViewModelUtilities.ContainsComponent(this.ViewModel.RootStatisticArea, kvp.Key.Name, true) ||
    //                        ViewModelUtilities.ContainsComponent(this.ViewModel.RootStatisticArea2, kvp.Key.Name, true))
    //                    {
    //                        float stat = (kvp.Key.Model).GetLodDistance(RSG.Model.Map.InstanceType.HD);
    //                        if (stat != 0)
    //                        {
    //                            totalStat += stat;

    //                            if (stat > maxValue)
    //                                maxValue = stat;
    //                            if (stat < minValue)
    //                                minValue = stat;

    //                            statCount++;
    //                            statValues.Add(kvp.Key, stat);
    //                            statistics.Add(kvp.Key, kvp.Value);
    //                        }
    //                    }
    //                }
    //            }
    //            range = (maxValue - minValue);

    //            System.Windows.Media.Brush brush = System.Windows.Media.Brushes.Black;
    //            SortedDictionary<float, List<Viewport2DGeometry>> orderedGeometry = new SortedDictionary<float, List<Viewport2DGeometry>>();
    //            foreach (KeyValuePair<MapSectionViewModel, List<Vector2f>> kvp in statistics)
    //            {
    //                float percentage = statValues[kvp.Key];
    //                percentage /= range;
    //                if (percentage > 1.0f)
    //                {
    //                    percentage = 1.0f;
    //                }
    //                else if (percentage < 0.0f)
    //                {
    //                    percentage = 0.0f;
    //                }
    //                Vector3i difference = new Vector3i(127, -127, 0);
    //                byte r = (byte)(((int)(difference.X * percentage)));
    //                byte g = (byte)(((int)(difference.Y * percentage)) + 127);
    //                byte b = (byte)(((int)(difference.Z * percentage)));
    //                System.Windows.Media.Color colour = System.Windows.Media.Color.FromArgb(255, r, g, b);

    //                if (!orderedGeometry.ContainsKey(statValues[kvp.Key]))
    //                {
    //                    orderedGeometry.Add(statValues[kvp.Key], new List<Viewport2DGeometry>());
    //                }
    //                Viewport2DShape newGeometry = new Viewport2DShape(etCoordSpace.World, "Filled shape for section", kvp.Value.ToArray(), brush, 1, colour);
    //                newGeometry.UserText = kvp.Key.Name;
    //                newGeometry.PickData = kvp.Key;
    //                orderedGeometry[statValues[kvp.Key]].Add(newGeometry);
    //            }

    //            int index = 0;
    //            this.Geometry.BeginUpdate();
    //            this.Geometry.Clear();
    //            foreach (var statValue in orderedGeometry.Reverse())
    //            {
    //                if (++index > this.ViewModel.WorstCount && this.ViewModel.ShowWorst == true)
    //                {
    //                    break;
    //                }
    //                foreach (var geo in statValue.Value)
    //                {
    //                    this.Geometry.Add(geo);
    //                }
    //            }
    //            this.Geometry.EndUpdate();
    //        }
    //        base.Activated();
    //    }

    //    /// <summary>
    //    /// Gets called when the user turns off this overlay
    //    /// </summary>
    //    public override void Deactivated()
    //    {
    //        this.Geometry = new RSG.Base.Collections.ObservableCollection<Viewport2DGeometry>();
    //        base.Deactivated();
    //    }

    //    #endregion // Overrides

    //    #region IPartImportsSatisfiedNotification

    //    /// <summary>
    //    /// Gets called once all the imports have been satisfied
    //    /// </summary>
    //    public void OnImportsSatisfied()
    //    {
    //        ViewModel.PropertyChanged += OnPropertyChanged;
    //    }

    //    /// <summary>
    //    /// Gets called whenever a property on the map browser changes
    //    /// </summary>
    //    void OnPropertyChanged(Object sender, System.ComponentModel.PropertyChangedEventArgs e)
    //    {
    //        if (this.IsCurrentlyActive == true)
    //        {
    //            if (e.PropertyName == "ShowWorst")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "WorstCount")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "RootStatisticArea")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "RootStatisticArea2")
    //            {
    //                this.Activated();
    //            }
    //        }
    //    }

    //    #endregion // IPartImportsSatisfiedNotification
    //} // HighLodDistanceRatioOverlay

    //[ExportExtension("Workbench.AddIn.MapStatistics.OverlayGroups.LodDistanceStatisticsGroup", typeof(ViewportOverlay))]
    //public class LodLodDistanceRatioOverlay : ViewportOverlay, IPartImportsSatisfiedNotification
    //{
    //    #region MEF Imports

    //    /// <summary>
    //    /// Map Browser
    //    /// </summary>
    //    [ImportExtension(Map.AddIn.Services.CompositionPoints.MapBrowser, typeof(IMapBrowser))]
    //    Lazy<IMapBrowser> MapBrowser
    //    {
    //        get;
    //        set;
    //    }

    //    /// <summary>
    //    /// Map Browser
    //    /// </summary>
    //    [ImportExtension("Workbench.AddIn.MapStatistics.MapStatisticsViewModel", typeof(MapStatisticsViewModel))]
    //    MapStatisticsViewModel ViewModel
    //    {
    //        get;
    //        set;
    //    }

    //    #endregion // MEF Imports

    //    #region Constructors

    //    /// <summary>
    //    /// Default Constructor
    //    /// </summary>
    //    public LodLodDistanceRatioOverlay()
    //    {
    //        this.Name = "LOD Detail Lod Distance";
    //        this.Description = "Shows the indiviual containers for the map in a colour that ranages from red to green that represents " +
    //                                            "the LOD deatil lod distance of that container compared with the other containers on the map.";
    //    }

    //    #endregion // Constructors

    //    #region Overrides

    //    /// <summary>
    //    /// Gets called when the user wants to view this overlay
    //    /// </summary>
    //    public override void Activated()
    //    {
    //        if (MapBrowser.Value.LevelGeometry != null && MapBrowser.Value.SelectedLevel != null)
    //        {
    //            float totalStat = 0;
    //            int statCount = 0;
    //            float minValue = float.MaxValue;
    //            float maxValue = float.MinValue;
    //            float range = 0;
    //            Dictionary<MapSectionViewModel, List<Vector2f>> statistics = new Dictionary<MapSectionViewModel, List<Vector2f>>();
    //            Dictionary<MapSectionViewModel, float> statValues = new Dictionary<MapSectionViewModel, float>();
    //            if (this.ViewModel.RootStatisticArea != null)
    //            {
    //                foreach (KeyValuePair<MapSectionViewModel, List<Vector2f>> kvp in MapBrowser.Value.LevelGeometry)
    //                {
    //                    if (ViewModelUtilities.ContainsComponent(this.ViewModel.RootStatisticArea, kvp.Key.Name, true) ||
    //                        ViewModelUtilities.ContainsComponent(this.ViewModel.RootStatisticArea2, kvp.Key.Name, true))
    //                    {
    //                        float stat = (kvp.Key.Model).GetLodDistance(RSG.Model.Map.InstanceType.LOD);
    //                        if (stat != 0)
    //                        {
    //                            totalStat += stat;

    //                            if (stat > maxValue)
    //                                maxValue = stat;
    //                            if (stat < minValue)
    //                                minValue = stat;

    //                            statCount++;
    //                            statValues.Add(kvp.Key, stat);
    //                            statistics.Add(kvp.Key, kvp.Value);
    //                        }
    //                    }
    //                }
    //            }
    //            range = (maxValue - minValue);

    //            System.Windows.Media.Brush brush = System.Windows.Media.Brushes.Black;
    //            SortedDictionary<float, List<Viewport2DGeometry>> orderedGeometry = new SortedDictionary<float, List<Viewport2DGeometry>>();
    //            foreach (KeyValuePair<MapSectionViewModel, List<Vector2f>> kvp in statistics)
    //            {
    //                float percentage = statValues[kvp.Key];
    //                percentage /= range;
    //                if (percentage > 1.0f)
    //                {
    //                    percentage = 1.0f;
    //                }
    //                else if (percentage < 0.0f)
    //                {
    //                    percentage = 0.0f;
    //                }
    //                Vector3i difference = new Vector3i(127, -127, 0);
    //                byte r = (byte)(((int)(difference.X * percentage)));
    //                byte g = (byte)(((int)(difference.Y * percentage)) + 127);
    //                byte b = (byte)(((int)(difference.Z * percentage)));
    //                System.Windows.Media.Color colour = System.Windows.Media.Color.FromArgb(255, r, g, b);

    //                if (!orderedGeometry.ContainsKey(statValues[kvp.Key]))
    //                {
    //                    orderedGeometry.Add(statValues[kvp.Key], new List<Viewport2DGeometry>());
    //                }
    //                Viewport2DShape newGeometry = new Viewport2DShape(etCoordSpace.World, "Filled shape for section", kvp.Value.ToArray(), brush, 1, colour);
    //                newGeometry.UserText = kvp.Key.Name;
    //                newGeometry.PickData = kvp.Key;
    //                orderedGeometry[statValues[kvp.Key]].Add(newGeometry);
    //            }

    //            int index = 0;
    //            this.Geometry.BeginUpdate();
    //            this.Geometry.Clear();
    //            foreach (var statValue in orderedGeometry.Reverse())
    //            {
    //                if (++index > this.ViewModel.WorstCount && this.ViewModel.ShowWorst == true)
    //                {
    //                    break;
    //                }
    //                foreach (var geo in statValue.Value)
    //                {
    //                    this.Geometry.Add(geo);
    //                }
    //            }
    //            this.Geometry.EndUpdate();
    //        }
    //        base.Activated();
    //    }

    //    /// <summary>
    //    /// Gets called when the user turns off this overlay
    //    /// </summary>
    //    public override void Deactivated()
    //    {
    //        this.Geometry = new RSG.Base.Collections.ObservableCollection<Viewport2DGeometry>();
    //        base.Deactivated();
    //    }

    //    #endregion // Overrides

    //    #region IPartImportsSatisfiedNotification

    //    /// <summary>
    //    /// Gets called once all the imports have been satisfied
    //    /// </summary>
    //    public void OnImportsSatisfied()
    //    {
    //        ViewModel.PropertyChanged += OnPropertyChanged;
    //    }

    //    /// <summary>
    //    /// Gets called whenever a property on the map browser changes
    //    /// </summary>
    //    void OnPropertyChanged(Object sender, System.ComponentModel.PropertyChangedEventArgs e)
    //    {
    //        if (this.IsCurrentlyActive == true)
    //        {
    //            if (e.PropertyName == "ShowWorst")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "WorstCount")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "RootStatisticArea")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "RootStatisticArea2")
    //            {
    //                this.Activated();
    //            }
    //        }
    //    }

    //    #endregion // IPartImportsSatisfiedNotification
    //} // LodLodDistanceRatioOverlay

    //[ExportExtension("Workbench.AddIn.MapStatistics.OverlayGroups.LodDistanceStatisticsGroup", typeof(ViewportOverlay))]
    //public class SLodLodDistanceRatioOverlay : ViewportOverlay, IPartImportsSatisfiedNotification
    //{
    //    #region MEF Imports

    //    /// <summary>
    //    /// Map Browser
    //    /// </summary>
    //    [ImportExtension(Map.AddIn.Services.CompositionPoints.MapBrowser, typeof(IMapBrowser))]
    //    Lazy<IMapBrowser> MapBrowser
    //    {
    //        get;
    //        set;
    //    }

    //    /// <summary>
    //    /// Map Browser
    //    /// </summary>
    //    [ImportExtension("Workbench.AddIn.MapStatistics.MapStatisticsViewModel", typeof(MapStatisticsViewModel))]
    //    MapStatisticsViewModel ViewModel
    //    {
    //        get;
    //        set;
    //    }

    //    #endregion // MEF Imports

    //    #region Constructors

    //    /// <summary>
    //    /// Default Constructor
    //    /// </summary>
    //    public SLodLodDistanceRatioOverlay()
    //    {
    //        this.Name = "SLOD Detail Lod Distance";
    //        this.Description = "Shows the indiviual containers for the map in a colour that ranages from red to green that represents " +
    //                                            "the SLOD deatil lod distance of that container compared with the other containers on the map.";
    //    }

    //    #endregion // Constructors

    //    #region Overrides

    //    /// <summary>
    //    /// Gets called when the user wants to view this overlay
    //    /// </summary>
    //    public override void Activated()
    //    {
    //        if (MapBrowser.Value.LevelGeometry != null && MapBrowser.Value.SelectedLevel != null)
    //        {
    //            float totalStat = 0;
    //            int statCount = 0;
    //            float minValue = float.MaxValue;
    //            float maxValue = float.MinValue;
    //            float range = 0;
    //            Dictionary<MapSectionViewModel, List<Vector2f>> statistics = new Dictionary<MapSectionViewModel, List<Vector2f>>();
    //            Dictionary<MapSectionViewModel, float> statValues = new Dictionary<MapSectionViewModel, float>();
    //            if (this.ViewModel.RootStatisticArea != null)
    //            {
    //                foreach (KeyValuePair<MapSectionViewModel, List<Vector2f>> kvp in MapBrowser.Value.LevelGeometry)
    //                {
    //                    if (ViewModelUtilities.ContainsComponent(this.ViewModel.RootStatisticArea, kvp.Key.Name, true) ||
    //                        ViewModelUtilities.ContainsComponent(this.ViewModel.RootStatisticArea2, kvp.Key.Name, true))
    //                    {
    //                        float stat = (kvp.Key.Model).GetLodDistance(RSG.Model.Map.InstanceType.SLOD);
    //                        if (stat != 0)
    //                        {
    //                            totalStat += stat;

    //                            if (stat > maxValue)
    //                                maxValue = stat;
    //                            if (stat < minValue)
    //                                minValue = stat;

    //                            statCount++;
    //                            statValues.Add(kvp.Key, stat);
    //                            statistics.Add(kvp.Key, kvp.Value);
    //                        }
    //                    }
    //                }
    //            }
    //            range = (maxValue - minValue);

    //            System.Windows.Media.Brush brush = System.Windows.Media.Brushes.Black;
    //            SortedDictionary<float, List<Viewport2DGeometry>> orderedGeometry = new SortedDictionary<float, List<Viewport2DGeometry>>();
    //            foreach (KeyValuePair<MapSectionViewModel, List<Vector2f>> kvp in statistics)
    //            {
    //                float percentage = statValues[kvp.Key];
    //                percentage /= range;
    //                if (percentage > 1.0f)
    //                {
    //                    percentage = 1.0f;
    //                }
    //                else if (percentage < 0.0f)
    //                {
    //                    percentage = 0.0f;
    //                }
    //                Vector3i difference = new Vector3i(127, -127, 0);
    //                byte r = (byte)(((int)(difference.X * percentage)));
    //                byte g = (byte)(((int)(difference.Y * percentage)) + 127);
    //                byte b = (byte)(((int)(difference.Z * percentage)));
    //                System.Windows.Media.Color colour = System.Windows.Media.Color.FromArgb(255, r, g, b);

    //                if (!orderedGeometry.ContainsKey(statValues[kvp.Key]))
    //                {
    //                    orderedGeometry.Add(statValues[kvp.Key], new List<Viewport2DGeometry>());
    //                }
    //                Viewport2DShape newGeometry = new Viewport2DShape(etCoordSpace.World, "Filled shape for section", kvp.Value.ToArray(), brush, 1, colour);
    //                newGeometry.UserText = kvp.Key.Name;
    //                newGeometry.PickData = kvp.Key;
    //                orderedGeometry[statValues[kvp.Key]].Add(newGeometry);
    //            }

    //            int index = 0;
    //            this.Geometry.BeginUpdate();
    //            this.Geometry.Clear();
    //            foreach (var statValue in orderedGeometry.Reverse())
    //            {
    //                if (++index > this.ViewModel.WorstCount && this.ViewModel.ShowWorst == true)
    //                {
    //                    break;
    //                }
    //                foreach (var geo in statValue.Value)
    //                {
    //                    this.Geometry.Add(geo);
    //                }
    //            }
    //            this.Geometry.EndUpdate();
    //        }
    //        base.Activated();
    //    }

    //    /// <summary>
    //    /// Gets called when the user turns off this overlay
    //    /// </summary>
    //    public override void Deactivated()
    //    {
    //        this.Geometry = new RSG.Base.Collections.ObservableCollection<Viewport2DGeometry>();
    //        base.Deactivated();
    //    }

    //    #endregion // Overrides

    //    #region IPartImportsSatisfiedNotification

    //    /// <summary>
    //    /// Gets called once all the imports have been satisfied
    //    /// </summary>
    //    public void OnImportsSatisfied()
    //    {
    //        ViewModel.PropertyChanged += OnPropertyChanged;
    //    }

    //    /// <summary>
    //    /// Gets called whenever a property on the map browser changes
    //    /// </summary>
    //    void OnPropertyChanged(Object sender, System.ComponentModel.PropertyChangedEventArgs e)
    //    {
    //        if (this.IsCurrentlyActive == true)
    //        {
    //            if (e.PropertyName == "ShowWorst")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "WorstCount")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "RootStatisticArea")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "RootStatisticArea2")
    //            {
    //                this.Activated();
    //            }
    //        }
    //    }

    //    #endregion // IPartImportsSatisfiedNotification
    //} // SLodLodDistanceRatioOverlay

    //[ExportExtension("Workbench.AddIn.MapStatistics.OverlayGroups.LodDistanceStatisticsGroup", typeof(ViewportOverlay))]
    //public class ReferenceLodDistanceRatioOverlay : ViewportOverlay, IPartImportsSatisfiedNotification
    //{
    //    #region MEF Imports

    //    /// <summary>
    //    /// Map Browser
    //    /// </summary>
    //    [ImportExtension(Map.AddIn.Services.CompositionPoints.MapBrowser, typeof(IMapBrowser))]
    //    Lazy<IMapBrowser> MapBrowser
    //    {
    //        get;
    //        set;
    //    }

    //    /// <summary>
    //    /// Map Browser
    //    /// </summary>
    //    [ImportExtension("Workbench.AddIn.MapStatistics.MapStatisticsViewModel", typeof(MapStatisticsViewModel))]
    //    MapStatisticsViewModel ViewModel
    //    {
    //        get;
    //        set;
    //    }

    //    #endregion // MEF Imports

    //    #region Constructors

    //    /// <summary>
    //    /// Default Constructor
    //    /// </summary>
    //    public ReferenceLodDistanceRatioOverlay()
    //    {
    //        this.Name = "Lod Distance For References";
    //        this.Description = "Shows the indiviual containers for the map in a colour that ranages from red to green that represents " +
    //                                            "the lod distance for the references of that container compared with the other containers on the map.";
    //    }

    //    #endregion // Constructors

    //    #region Overrides

    //    /// <summary>
    //    /// Gets called when the user wants to view this overlay
    //    /// </summary>
    //    public override void Activated()
    //    {
    //        if (MapBrowser.Value.LevelGeometry != null && MapBrowser.Value.SelectedLevel != null)
    //        {
    //            float totalStat = 0;
    //            int statCount = 0;
    //            float minValue = float.MaxValue;
    //            float maxValue = float.MinValue;
    //            float range = 0;
    //            Dictionary<MapSectionViewModel, List<Vector2f>> statistics = new Dictionary<MapSectionViewModel, List<Vector2f>>();
    //            Dictionary<MapSectionViewModel, float> statValues = new Dictionary<MapSectionViewModel, float>();
    //            if (this.ViewModel.RootStatisticArea != null)
    //            {
    //                foreach (KeyValuePair<MapSectionViewModel, List<Vector2f>> kvp in MapBrowser.Value.LevelGeometry)
    //                {
    //                    if (ViewModelUtilities.ContainsComponent(this.ViewModel.RootStatisticArea, kvp.Key.Name, true) ||
    //                        ViewModelUtilities.ContainsComponent(this.ViewModel.RootStatisticArea2, kvp.Key.Name, true))
    //                    {
    //                        float stat = (kvp.Key.Model).GetLodDistance(RSG.Model.Map.InstanceType.REF);
    //                        if (stat != 0)
    //                        {
    //                            totalStat += stat;

    //                            if (stat > maxValue)
    //                                maxValue = stat;
    //                            if (stat < minValue)
    //                                minValue = stat;

    //                            statCount++;
    //                            statValues.Add(kvp.Key, stat);
    //                            statistics.Add(kvp.Key, kvp.Value);
    //                        }
    //                    }
    //                }
    //            }
    //            range = (maxValue - minValue);

    //            System.Windows.Media.Brush brush = System.Windows.Media.Brushes.Black;
    //            SortedDictionary<float, List<Viewport2DGeometry>> orderedGeometry = new SortedDictionary<float, List<Viewport2DGeometry>>();
    //            foreach (KeyValuePair<MapSectionViewModel, List<Vector2f>> kvp in statistics)
    //            {
    //                float percentage = statValues[kvp.Key];
    //                percentage /= range;
    //                if (percentage > 1.0f)
    //                {
    //                    percentage = 1.0f;
    //                }
    //                else if (percentage < 0.0f)
    //                {
    //                    percentage = 0.0f;
    //                }
    //                Vector3i difference = new Vector3i(127, -127, 0);
    //                byte r = (byte)(((int)(difference.X * percentage)));
    //                byte g = (byte)(((int)(difference.Y * percentage)) + 127);
    //                byte b = (byte)(((int)(difference.Z * percentage)));
    //                System.Windows.Media.Color colour = System.Windows.Media.Color.FromArgb(255, r, g, b);

    //                if (!orderedGeometry.ContainsKey(statValues[kvp.Key]))
    //                {
    //                    orderedGeometry.Add(statValues[kvp.Key], new List<Viewport2DGeometry>());
    //                }
    //                Viewport2DShape newGeometry = new Viewport2DShape(etCoordSpace.World, "Filled shape for section", kvp.Value.ToArray(), brush, 1, colour);
    //                newGeometry.UserText = kvp.Key.Name;
    //                newGeometry.PickData = kvp.Key;
    //                orderedGeometry[statValues[kvp.Key]].Add(newGeometry);
    //            }

    //            int index = 0;
    //            this.Geometry.BeginUpdate();
    //            this.Geometry.Clear();
    //            foreach (var statValue in orderedGeometry.Reverse())
    //            {
    //                if (++index > this.ViewModel.WorstCount && this.ViewModel.ShowWorst == true)
    //                {
    //                    break;
    //                }
    //                foreach (var geo in statValue.Value)
    //                {
    //                    this.Geometry.Add(geo);
    //                }
    //            }
    //            this.Geometry.EndUpdate();
    //        }
    //        base.Activated();
    //    }

    //    /// <summary>
    //    /// Gets called when the user turns off this overlay
    //    /// </summary>
    //    public override void Deactivated()
    //    {
    //        this.Geometry = new RSG.Base.Collections.ObservableCollection<Viewport2DGeometry>();
    //        base.Deactivated();
    //    }

    //    #endregion // Overrides

    //    #region IPartImportsSatisfiedNotification

    //    /// <summary>
    //    /// Gets called once all the imports have been satisfied
    //    /// </summary>
    //    public void OnImportsSatisfied()
    //    {
    //        ViewModel.PropertyChanged += OnPropertyChanged;
    //    }

    //    /// <summary>
    //    /// Gets called whenever a property on the map browser changes
    //    /// </summary>
    //    void OnPropertyChanged(Object sender, System.ComponentModel.PropertyChangedEventArgs e)
    //    {
    //        if (this.IsCurrentlyActive == true)
    //        {
    //            if (e.PropertyName == "ShowWorst")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "WorstCount")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "RootStatisticArea")
    //            {
    //                this.Activated();
    //            }
    //            else if (e.PropertyName == "RootStatisticArea2")
    //            {
    //                this.Activated();
    //            }
    //        }
    //    }

    //    #endregion // IPartImportsSatisfiedNotification
    //} // ReferenceLodDistanceRatioOverlay
}
