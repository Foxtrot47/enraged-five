﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Viewport.AddIn;
using Workbench.AddIn.UI.Layout;
using System.ComponentModel.Composition;
using System.Windows;
using System.Collections.Specialized;
using ContentBrowser.AddIn;
using Workbench.AddIn.TXDParentiser.ViewModel;
using RSG.Model.GlobalTXD;
using RSG.Model.Common;
using RSG.Model.Common.Extensions;
using Workbench.AddIn.TXDParentiser.AddIn;
using RSG.Base.Editor;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using RSG.Base.Math;
using System.Drawing;
using RSG.Model.Common.Map;
using System.Threading;
using RSG.Model.Common.Util;
using RSG.Base.Tasks;
using RSG.Base.Windows.Dialogs;
using MapViewport.AddIn;
using Workbench.AddIn.Services.Model;

namespace Workbench.AddIn.TXDParentiser.Overlays
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(ExtensionPoints.TxdParentiserOverlayGroup, typeof(IViewportOverlay))]
    public class ParentTXDTextureUsageOverlay : LevelDependentViewportOverlay
    {
        #region Constants
        private const String NAME = "Parent TXD Texture Usage";
        private const String DESC = "Shows the visibility bounds of a texture or TXD based on what is selected in a TXD file.";
        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// MEF import for TXD parentiser
        /// </summary>
        [ImportExtension(Workbench.AddIn.TXDParentiser.AddIn.CompositionPoints.TxdParentiser, typeof(ITXDParentiserService))]
        private ITXDParentiserService TxdParentiserService { get; set; }

        /// <summary>
        /// Level Browser
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LevelBrowser, typeof(ILevelBrowser))]
        private ILevelBrowser LevelBrowserProxy { get; set; }
        #endregion // MEF Imports

        #region Properties
        /// <summary>
        /// Overlay image that is used for rendering to texture
        /// </summary>
        protected Dictionary<ModelBase, Viewport2DImageOverlay> OverlayImages
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        protected Dictionary<ModelBase, Color> OverlayColours
        {
            get;
            set;
        }

        /// <summary>
        /// List of overlays that were previously used and that can be reused
        /// </summary>
        protected LinkedList<Viewport2DImageOverlay> OverlaysToReuse
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        protected Vector2i OverlayImageSize
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        protected Vector2f OverlayImageRenderSize
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        protected Vector2f OverlayImagePosition
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public ParentTXDTextureUsageOverlay()
            : base(NAME, DESC)
        {
            OverlayImages = new Dictionary<ModelBase, Viewport2DImageOverlay>();
            OverlayColours = new Dictionary<ModelBase, Color>();
            OverlaysToReuse = new LinkedList<Viewport2DImageOverlay>();
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// Gets called when the user wants to view this overlay
        /// </summary>
        public override void Activated()
        {
            base.Activated();

            CancellationTokenSource cts = new CancellationTokenSource();
            LevelTaskContext context = new LevelTaskContext(cts.Token, LevelBrowserProxy.SelectedLevel);
            IMapHierarchy hierarchy = context.MapHierarchy.AllSections.First().MapHierarchy;

            StreamableStat[] statsToLoad = new StreamableStat[] {
                StreamableMapSectionStat.Archetypes,
                StreamableMapSectionStat.Entities,
                StreamableArchetypeStat.LodDistance,
                StreamableMapArchetypeStat.Shaders,
                StreamableMapArchetypeStat.TxdExportSizes,
                StreamableEntityStat.LodDistanceOverride };
            ITask dataLoadTask = hierarchy.CreateStatLoadTask(context.MapHierarchy.AllSections, statsToLoad);

            TaskExecutionDialog dialog = new TaskExecutionDialog();
            TaskExecutionViewModel vm = new TaskExecutionViewModel("Loading Map Data", dataLoadTask, cts, context);
            vm.AutoCloseOnCompletion = true;
            dialog.DataContext = vm;
            Nullable<bool> selectionResult = dialog.ShowDialog();
            if (false == selectionResult)
            {
                return;
            }

            // Make sure a level is selected
            if (LevelBrowserProxy.SelectedLevel != null)
            {
                // Setup the overlay image
                Vector2i size = new Vector2i(966, 627);
                Vector2f pos = new Vector2f(-483.0f, 627.0f);

                ILevel level = LevelBrowserProxy.SelectedLevel;
                if (level.ImageBounds != null)
                {
                    size = new Vector2i((int)(level.ImageBounds.Max.X - level.ImageBounds.Min.X), (int)(level.ImageBounds.Max.Y - level.ImageBounds.Min.Y));
                    pos = new Vector2f(level.ImageBounds.Min.X, level.ImageBounds.Max.Y);
                }

                float imageScaleFactor = 0.1f;
                OverlayImageSize = new Vector2i((int)(size.X * imageScaleFactor), (int)(size.Y * imageScaleFactor));
                OverlayImageRenderSize = new Vector2f(size.X, size.Y);
                OverlayImagePosition = pos;
            }

            TxdParentiserService.SelectionChanged += TxdParentiserService_SelectionChanged;
            SelectionChanged(TxdParentiserService.SelectedItems);
        }

        /// <summary>
        /// Gets called when the user turns off this overlay
        /// </summary>
        public override void Deactivated()
        {
            TxdParentiserService.SelectionChanged -= TxdParentiserService_SelectionChanged;
            Geometry.Clear();
            OverlayImages.Clear();
            OverlayColours.Clear();
            OverlaysToReuse.Clear();
            base.Deactivated();
        }

        /// <summary>
        /// Gets the control that should be shown in the overlay details panel
        /// </summary>
        public override System.Windows.Controls.Control GetOverlayControl()
        {
            return null;
        }
        #endregion // Overrides

        #region Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void TxdParentiserService_SelectionChanged(object sender, TxdSelectionChangedEventArgs args)
        {
            SelectionChanged(args.NewItem);
        }
        #endregion // Event Handlers

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="progress"></param>
        private void EnsureDataLoaded(LevelTaskContext context, IProgress<TaskProgress> progress)
        {
            float increment = 1.0f / context.MapHierarchy.AllSections.Count();

            // Load all the map section data
            foreach (IMapSection section in context.MapHierarchy.AllSections)
            {
                context.Token.ThrowIfCancellationRequested();

                // Update the progress information
                string message = String.Format("Loading {0}", section.Name);
                progress.Report(new TaskProgress(increment, true, message));

                section.RequestAllStatistics(false);
            }
        }

        /// <summary>
        /// Called whenever the selection has changed
        /// </summary>
        private void SelectionChanged(IEnumerable<ModelBase> newSelection)
        {
            // Try to be smart about which overlay images to reuse and which to discard
            List<ModelBase> addedItems = new List<ModelBase>();
            foreach (ModelBase model in newSelection)
            {
                if (!OverlayImages.ContainsKey(model))
                {
                    addedItems.Add(model);
                }
            }

            List<ModelBase> removedItems = new List<ModelBase>();
            foreach (ModelBase model in OverlayImages.Keys)
            {
                if (!newSelection.Contains(model))
                {
                    removedItems.Add(model);
                }
            }

            // Remove the overlays that are no longer needed keeping track of them in case we wish to reuse them
            foreach (ModelBase model in removedItems)
            {
                if (OverlayImages.ContainsKey(model))
                {
                    OverlaysToReuse.AddLast(OverlayImages[model]);
                    OverlayImages.Remove(model);
                }
                if (OverlayColours.ContainsKey(model))
                {
                    OverlayColours.Remove(model);
                }
            }

            // Get the list of colours that we can use
            IEnumerable<Color> colours = VaryingColors(1, 128).Take(newSelection.Count()).Except(OverlayColours.Values);

            int idx = 0;
            foreach (ModelBase model in newSelection)
            {
                // Only update those overlays that haven't been created yet
                if (!OverlayImages.ContainsKey(model))
                {
                    Viewport2DImageOverlay overlay = null;

                    if (OverlaysToReuse.Count > 0)
                    {
                        overlay = OverlaysToReuse.First.Value;
                        OverlaysToReuse.RemoveFirst();
                    }
                    else
                    {
                        overlay = new Viewport2DImageOverlay(etCoordSpace.World, "", OverlayImageSize, OverlayImagePosition, OverlayImageRenderSize);
                    }

                    // Update the overlay
                    overlay.ClearImage();
                    overlay.BeginUpdate();
                    RenderGeometry(model, overlay, colours.ElementAt(idx));
                    overlay.UpdateImageOpacity(128);
                    overlay.EndUpdate();

                    OverlayImages.Add(model, overlay);
                    OverlayColours.Add(model, colours.ElementAt(idx++));
                }
            }

            // Refresh the geometry based on the overlay images
            Geometry.BeginUpdate();
            Geometry.Clear();
            Geometry.AddRange(OverlayImages.Values);
            Geometry.EndUpdate();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        private void RenderGeometry(ModelBase model, Viewport2DImageOverlay overlay, Color fillColour)
        {
            if (model is GlobalTexture)
            {
                GlobalTexture texture = (GlobalTexture)model;
                GlobalTextureDictionary parentTxd = (GlobalTextureDictionary)texture.Parent;
                RenderTextureGeometry(texture.Filename, texture.AlphaTextureName, parentTxd.Name, overlay, fillColour);
            }
            else if (model is SourceTexture)
            {
                SourceTexture texture = (SourceTexture)model;
                SourceTextureDictionary parentTxd = (SourceTextureDictionary)texture.Parent;
                RenderTextureGeometry(texture.Filename, texture.AlphaTextureName, parentTxd.Name, overlay, fillColour);
            }
            else if (model is GlobalTextureDictionary)
            {
                GlobalTextureDictionary txd = (GlobalTextureDictionary)model;
                RenderGlobalTxdGeometry(txd, overlay, fillColour);
            }
            else if (model is SourceTextureDictionary)
            {
                SourceTextureDictionary txd = (SourceTextureDictionary)model;
                RenderSourceTxdGeometry(txd, overlay, fillColour);
            }
        }

        /// <summary>
        /// Updates the geometry that is rendered when a single texture has been selected
        /// </summary>
        /// <param name="textureName"></param>
        /// <param name="alphaName"></param>
        private void RenderTextureGeometry(string textureName, string alphaName, string parentTxdName, Viewport2DImageOverlay overlay, Color fillColour)
        {
            if (String.IsNullOrEmpty(alphaName))
            {
                alphaName = null;
            }

            List<IEntity> validEntities = new List<IEntity>();
            foreach (IMapArchetype archetype in this.GetAllMapDefinitions())
            {
                if (archetype.TxdExportSizes.Count != 1)
                {
                    continue;
                }

                string txdName = archetype.TxdExportSizes.FirstOrDefault().Key.ToLower();
                if (parentTxdName.ToLower() != txdName)
                {
                    continue;
                }

                foreach (ITexture texture in archetype.Textures)
                {
                    if (texture.Filename == textureName && texture.AlphaFilename == alphaName)
                    {
                        validEntities.AddRange(archetype.Entities);
                    }
                }
            }

            // Iterate over the list of instances we found rendering them to the overlay
            foreach (IEntity instance in validEntities)
            {
                RenderMapInstanceBounds(instance.GetStreamingExtents(), overlay, fillColour);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="txd"></param>
        /// <param name="fillColour"></param>
        private void RenderSourceTxdGeometry(SourceTextureDictionary txd, Viewport2DImageOverlay overlay, Color fillColour)
        {
            List<IEntity> validEntities = new List<IEntity>();
            foreach (IMapArchetype archetype in this.GetAllMapDefinitions())
            {
                if (archetype.TxdExportSizes.Count != 1)
                {
                    continue;
                }

                string txdName = archetype.TxdExportSizes.FirstOrDefault().Key.ToLower();
                if (txd.Name.ToLower() != txdName)
                {
                    continue;
                }

                validEntities.AddRange(archetype.Entities);
            }

            foreach (IEntity instance in validEntities)
            {
                RenderMapInstanceBounds(instance.GetStreamingExtents(), overlay, fillColour);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="txd"></param>
        /// <param name="fillColour"></param>
        private void RenderGlobalTxdGeometry(GlobalTextureDictionary txd, Viewport2DImageOverlay overlay, Color fillColour)
        {
            // Recurse onto the child global txd
            foreach (GlobalTextureDictionary globalChild in txd.GlobalTextureDictionaries.Values)
            {
                RenderGlobalTxdGeometry(globalChild, overlay, fillColour);
            }

            // Recurse onto the child source txd
            foreach (SourceTextureDictionary sourceChild in txd.SourceTextureDictionaries.Values)
            {
                RenderSourceTxdGeometry(sourceChild, overlay, fillColour);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bounds"></param>
        private void RenderMapInstanceBounds(BoundingBox3f bounds, Viewport2DImageOverlay overlay, Color fillColour)
        {
            Vector2f[] points = new Vector2f[4];
            points[0] = new Vector2f(bounds.Min.X, bounds.Min.Y);
            points[1] = new Vector2f(bounds.Min.X, bounds.Max.Y);
            points[2] = new Vector2f(bounds.Max.X, bounds.Max.Y);
            points[3] = new Vector2f(bounds.Max.X, bounds.Min.Y);

            overlay.RenderPolygon(points, fillColour);
        }

        /// <summary>
        /// Returns all the map definitions that exist in the map data
        /// </summary>
        /// <returns></returns>
        private IEnumerable<IMapArchetype> GetAllMapDefinitions()
        {
            IMapHierarchy mapHierarchy = LevelBrowserProxy.SelectedLevel.GetMapHierarchy();
            return from section in mapHierarchy.AllSections
                   from archetype in section.Archetypes
                   select archetype;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="seedIndex"></param>
        /// <returns></returns>
        private IEnumerable<Color> VaryingColors(int seedIndex, int alpha)
        {
            int maxValue = 1 << 24;
            int index = seedIndex % maxValue;

            while (true)
            {
                byte r = 0;
                byte g = 0;
                byte b = 0;

                for (int i = 0; i < 24; i++)
                {
                    if ((index & (1 << i)) != 0)
                    {
                        switch (i % 3)
                        {
                            case 0: r |= (byte)(1 << (23 - i) / 3); break;
                            case 1: g |= (byte)(1 << (23 - i) / 3); break;
                            case 2: b |= (byte)(1 << (23 - i) / 3); break;
                        }
                    }
                }

                yield return Color.FromArgb(alpha, r, g, b);

                index = (index + 1) % maxValue;
            }
        }
        #endregion // Private Methods
    } // TextureUsageOverlay
}
