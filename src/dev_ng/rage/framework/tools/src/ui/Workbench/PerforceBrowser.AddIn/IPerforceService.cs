﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.SourceControl.Perforce;

namespace PerforceBrowser.AddIn
{
    /// <summary>
    /// 
    /// </summary>
    public interface IPerforceService
    {
        #region Properties
        /// <summary>
        /// Valid perforce connection
        /// </summary>
        P4 PerforceConnection
        {
            get;
        }

        /// <summary>
        /// Toggle for login checks in the PerforceConnection get method.
        /// </summary>
        bool EnableLoginChecks
        {
            get;
            set;
        }
        #endregion // Properties
    } // IPerforceService
} // PerforceBrowser.AddIn
