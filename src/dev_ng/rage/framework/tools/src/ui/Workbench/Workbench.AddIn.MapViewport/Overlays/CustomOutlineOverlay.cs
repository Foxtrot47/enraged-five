﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using RSG.Base.Editor;
using System.Windows.Media;
using RSG.Base.Windows.DragDrop;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using RSG.Base.Math;
using System.Windows.Input;
using Microsoft.Win32;
using RSG.Metadata.Model;
using RSG.Metadata.Parser;
using RSG.Metadata.Data;
using System.Windows;
using System.Collections.Specialized;
using RSG.Base.Collections;
using RSG.Base.Windows.Helpers;
using MapViewport.AddIn;
using System.ComponentModel;
using System.Xml.Linq;
using System.Xml.XPath;

namespace Workbench.AddIn.MapViewport.Overlays
{
    /// <summary>
    /// 
    /// </summary>
    public enum VectorMapMode
    {
        Default,
        Overridden
    }

    /// <summary>
    /// Represents a map section that has been selected by the user to show on the map.
    /// </summary>
    public class ShownMapSection : ViewModelBase, IDropTarget
    {
        #region Fields
        /// <summary>
        /// Private field for the SectionText property that supports change events.
        /// </summary>
        private string m_sectionText;
        /// <summary>
        /// Private field for the SectionText property that supports change events.
        /// </summary>
        private Color m_sectionColour;
        /// <summary>
        /// Private field for the SectionName property that supports change events.
        /// </summary>
        private string m_sectionName;
        /// <summary>
        /// The group that this section belongs to.
        /// </summary>
        private ShownMapSectionGroup group;
        #endregion

        #region Properties
        /// <summary>
        /// The colour of the section when it is rendered onto the map.
        /// </summary>
        public Color Colour
        {
            get { return m_sectionColour; }
            set
            {
                if (m_sectionColour == value)
                    return;

                this.m_sectionColour = value;
                OnPropertyChanged("Colour");
            }
        }

        /// <summary>
        /// The text that appears in the tooltip when it is rendered onto the map.
        /// </summary>
        public string Text
        {
            get { return m_sectionText; }
            set
            {
                if (m_sectionText == value)
                    return;

                this.m_sectionText = value;
                OnPropertyChanged("Text");
            }
        }

        /// <summary>
        /// The name of the section
        /// </summary>
        public string Name
        {
            get { return m_sectionName; }
            set
            {
                if (m_sectionName == value)
                    return;

                this.m_sectionName = value;
                OnPropertyChanged("Name");
            }
        }

        /// <summary>
        /// The group this section belongs to.
        /// </summary>
        public ShownMapSectionGroup Group
        {
            get { return this.group; }
            set
            {
                if (this.group == value)
                    return;

                this.group = value;
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="Workbench.AddIn.MapViewport.Overlays.ShownMapSection"/>
        /// class.
        /// </summary>
        /// <param name="sectionName">
        /// The name of the shown section. 
        /// </param>
        /// <param name="colour">
        /// The initialise colour the shown section should be rendered in. 
        /// </param>
        public ShownMapSection(string sectionName, Color colour, ShownMapSectionGroup group)
        {
            this.Name = sectionName;
            this.Colour = colour;
            this.group = group;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Called when a object is dragged over the control for the selected sections.
        /// </summary>
        /// <param name="dropInfo">
        /// The info got from the dragged data.
        /// </param>
        public void DragOver(DropInfo dropInfo)
        {
            if (dropInfo.Data is string)
            {
                string name = dropInfo.Data.ToString();
                if (!this.group.Overlay.SectionsToUse.ContainsKey(name))
                    return;

                bool addedAlready = false;
                foreach (ShownMapSection sec in this.group.Overlay.ShownMapSections)
                {
                    if (string.Compare(sec.Name, name, true) != 0)
                        continue;

                    addedAlready = true;
                    break;
                }
                if (!addedAlready)
                    dropInfo.Effects = System.Windows.DragDropEffects.Move;
            }
            else if (dropInfo.Data is IEnumerable<string>)
            {
                bool valid = false;
                foreach (string name in dropInfo.Data as IEnumerable<string>)
                {
                    if (!this.group.Overlay.SectionsToUse.ContainsKey(name))
                        continue;

                    bool addedAlready = false;
                    foreach (ShownMapSection sec in this.group.Overlay.ShownMapSections)
                    {
                        if (string.Compare(sec.Name, name, true) != 0)
                            continue;

                        addedAlready = true;
                        break;
                    }
                    if (addedAlready)
                        continue;

                    valid = true;
                    break;
                }
                if (valid)
                    dropInfo.Effects = System.Windows.DragDropEffects.Move;
            }
        }

        /// <summary>
        /// Called when a object is dropped into the control for the selected sections.
        /// </summary>
        /// <param name="dropInfo">
        /// The info got from the dragged data.
        /// </param>
        public void Drop(DropInfo dropInfo)
        {
            if (dropInfo.Data is string)
            {
                ShownMapSection section = new ShownMapSection(dropInfo.Data.ToString(), this.Colour, this.group);
                int index = this.group.ShownMapSections.IndexOf(this);
                if (index != -1)
                {
                    this.group.ShownMapSections.Insert(index, section);
                }
                else
                {
                    this.group.ShownMapSections.Add(section);
                }
            }
            else if (dropInfo.Data is IEnumerable<string>)
            {
                int index = this.group.ShownMapSections.IndexOf(this);
                foreach (string name in dropInfo.Data as IEnumerable<string>)
                {
                    ShownMapSection section = new ShownMapSection(name, Colour, this.group);
                    if (index != -1)
                    {
                        this.group.ShownMapSections.Insert(index, section);
                    }
                    else
                    {
                        this.group.ShownMapSections.Add(section);
                    }
                }
            }
            else if (dropInfo.Data is ShownMapSection)
            {
                ShownMapSection section = (dropInfo.Data as ShownMapSection);
                section.Colour = this.Group.Colour;
                int index = this.group.ShownMapSections.IndexOf(this);
                section.Group.ShownMapSections.Remove(section);
                section.Group = this.Group;
                if (index != -1)
                {
                    this.group.ShownMapSections.Insert(index, section);
                }
                else
                {
                    this.group.ShownMapSections.Add(section);
                }
            }
            else if (dropInfo.Data is IEnumerable<ShownMapSection>)
            {
                int index = this.group.ShownMapSections.IndexOf(this);
                foreach (ShownMapSection section in dropInfo.Data as IEnumerable<ShownMapSection>)
                {
                    section.Colour = this.Group.Colour;
                    section.Group.ShownMapSections.Remove(section);
                    section.Group = this.Group;
                    if (index != -1)
                    {
                        if (index > this.group.ShownMapSections.Count + 1)
                        {
                            this.group.ShownMapSections.Add(section);
                        }
                        else
                        {
                            this.group.ShownMapSections.Insert(index++, section);
                        }
                    }
                    else
                    {
                        this.group.ShownMapSections.Add(section);
                    }
                }
            }
        }
        #endregion
    }

    /// <summary>
    /// Represents a map section that has been selected by the user to show on the map.
    /// </summary>
    public class ShownMapSectionGroup : ViewModelBase, IDropTarget
    {
        #region Fields
        /// <summary>
        /// Private field for the SectionText property that supports change events.
        /// </summary>
        private string m_sectionText;
        /// <summary>
        /// Private field for the SectionText property that supports change events.
        /// </summary>
        private Color m_groupColour;
        /// <summary>
        /// Private field for the SectionName property that supports change events.
        /// </summary>
        private string m_groupName;
        /// <summary>
        /// 
        /// </summary>
        private CustomOutlineOverlay m_overlay;
        /// <summary>
        /// Private field for the SectionText property that supports change events.
        /// </summary>
        private string m_isSelected;

        /// <summary>
        /// 
        /// </summary>
        private bool m_isVisibleOnMap;
        #endregion

        #region Properties
        /// <summary>
        /// Always returns false
        /// </summary>
        public new bool IsSelected
        {
            get { return false; }
            set { }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsVisibleOnMap
        {
            get
            {
                return this.m_isVisibleOnMap;
            }
            set
            {
                if (m_isVisibleOnMap == value)
                    return;

                this.m_isVisibleOnMap = value;
                OnPropertyChanged("IsVisibleOnMap");
            }
        }

        /// <summary>
        /// The colour of the section when it is rendered onto the map.
        /// </summary>
        public Color Colour
        {
            get { return m_groupColour; }
            set
            {
                if (m_groupColour == value)
                    return;

                this.m_groupColour = value;
                OnPropertyChanged("Colour");
                foreach (ShownMapSection section in ShownMapSections)
                {
                    section.Colour = m_groupColour;
                }
            }
        }

        /// <summary>
        /// The text that appears in the tooltip when it is rendered onto the map.
        /// </summary>
        public string Text
        {
            get { return m_sectionText; }
            set
            {
                if (m_sectionText == value)
                    return;

                this.m_sectionText = value;
                OnPropertyChanged("Text");
            }
        }

        /// <summary>
        /// The name of the section
        /// </summary>
        public string Name
        {
            get { return m_groupName; }
            set
            {
                if (m_groupName == value)
                    return;

                this.m_groupName = value;
                OnPropertyChanged("Name");
            }
        }

        /// <summary>
        /// The map sections that have been chosen by the user to be rendered
        /// onto the overlay.
        /// </summary>
        public ObservableCollection<ShownMapSection> ShownMapSections
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the command used to delete shown sections from the overlay.
        /// </summary>
        public RelayCommand RemoveCommand
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the command used to delete shown sections from the overlay.
        /// </summary>
        public RelayCommand DeleteGroupCommand
        {
            get;
            set;
        }

        public CustomOutlineOverlay Overlay
        {
            get { return this.m_overlay; }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="Workbench.AddIn.MapViewport.Overlays.ShownMapSection"/>
        /// class.
        /// </summary>
        /// <param name="sectionName">
        /// The name of the shown section. 
        /// </param>
        /// <param name="colour">
        /// The initialise colour the shown section should be rendered in. 
        /// </param>
        public ShownMapSectionGroup(string name, Color colour, CustomOutlineOverlay overlay)
        {
            this.m_isVisibleOnMap = true;
            this.ShownMapSections = new ObservableCollection<ShownMapSection>();
            this.Name = name;
            this.Colour = colour;
            this.m_overlay = overlay;

            RemoveCommand = new RelayCommand(this.Remove);
            DeleteGroupCommand = new RelayCommand(this.DeleteGroup);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Called when a object is dragged over the control for the selected sections.
        /// </summary>
        /// <param name="dropInfo">
        /// The info got from the dragged data.
        /// </param>
        public void DragOver(DropInfo dropInfo)
        {
            if (dropInfo.Data is string)
            {
                string name = dropInfo.Data.ToString();
                if (!this.m_overlay.SectionsToUse.ContainsKey(name))
                    return;

                bool addedAlready = false;
                foreach (ShownMapSection sec in this.m_overlay.ShownMapSections)
                {
                    if (string.Compare(sec.Name, name, true) != 0)
                        continue;

                    addedAlready = true;
                    break;
                }
                if (!addedAlready)
                    dropInfo.Effects = System.Windows.DragDropEffects.Move;
            }
            else if (dropInfo.Data is IEnumerable<string>)
            {
                bool valid = false;
                foreach (string name in dropInfo.Data as IEnumerable<string>)
                {
                    if (!this.m_overlay.SectionsToUse.ContainsKey(name))
                        continue;

                    bool addedAlready = false;
                    foreach (ShownMapSection sec in this.m_overlay.ShownMapSections)
                    {
                        if (string.Compare(sec.Name, name, true) != 0)
                            continue;

                        addedAlready = true;
                        break;
                    }
                    if (addedAlready)
                        continue;

                    valid = true;
                    break;
                }
                if (valid)
                    dropInfo.Effects = System.Windows.DragDropEffects.Move;
            }
            else if (dropInfo.Data is ShownMapSection)
            {
                dropInfo.Effects = System.Windows.DragDropEffects.Move;
            }
            else if (dropInfo.Data is IEnumerable<ShownMapSection>)
            {
                dropInfo.Effects = System.Windows.DragDropEffects.Move;
            }
        }

        /// <summary>
        /// Called when a object is dropped into the control for the selected sections.
        /// </summary>
        /// <param name="dropInfo">
        /// The info got from the dragged data.
        /// </param>
        public void Drop(DropInfo dropInfo)
        {
            if (dropInfo.Data is string)
            {
                ShownMapSection section = new ShownMapSection(dropInfo.Data.ToString(), this.Colour, this);
                this.ShownMapSections.Add(section);
            }
            else if (dropInfo.Data is IEnumerable<string>)
            {
                foreach (string name in dropInfo.Data as IEnumerable<string>)
                {
                    ShownMapSection section = new ShownMapSection(name, Colour, this);
                    this.ShownMapSections.Add(section);
                }
            }
            else if (dropInfo.Data is ShownMapSection)
            {
                ShownMapSection section = (dropInfo.Data as ShownMapSection);
                section.Colour = this.Colour;
                section.Group.ShownMapSections.Remove(section);
                section.Group = this;
                this.ShownMapSections.Add(section);
            }
            else if (dropInfo.Data is IEnumerable<ShownMapSection>)
            {
                foreach (ShownMapSection section in dropInfo.Data as IEnumerable<ShownMapSection>)
                {
                    section.Group.ShownMapSections.Remove(section);
                    section.Colour = this.Colour;
                    section.Group = this;
                    this.ShownMapSections.Add(section);
                }
            }
        }

        /// <summary>
        /// Removes all of the selected shown sections from the overlay.
        /// </summary>
        /// <param name="param">
        ///The command parameter. 
        /// </param>
        private void Remove(object param)
        {
            List<ShownMapSection> selected = new List<ShownMapSection>();
            foreach (ShownMapSection section in this.ShownMapSections)
            {
                if (section.IsSelected == true)
                    selected.Add(section);
            }
            foreach (ShownMapSection section in selected)
            {
                section.IsSelected = false;
                this.ShownMapSections.Remove(section);
            }
        }

        /// <summary>
        /// Removes all of the selected shown sections from the overlay.
        /// </summary>
        /// <param name="param">
        ///The command parameter. 
        /// </param>
        private void DeleteGroup(object param)
        {
            this.m_overlay.RemoveGroup(this);
        }
        #endregion
    }

    /// <summary>
    /// Custom outline overlay that gets created by the custom group and can be serialised
    /// out to a file for sharing.
    /// </summary>
    public class CustomOutlineOverlay : LevelDependentViewportOverlay
    {
        #region MEF Imports

        /// <summary>
        /// A reference to view model for the map viewport
        /// </summary>
        private Viewport.AddIn.IMapViewport MapViewportViewModel { get; set; }

        #endregion

        #region Constants
        /// <summary>
        /// The default colour to make every section outline before the user picks
        /// a unique colour.
        /// </summary>
        private Color DefaultColour = Color.FromArgb(255, 0, 127, 0);
        #endregion

        #region Properties
        /// <summary>
        /// The group that this custom overlay belongs to.
        /// </summary>
        public CustomGroup OwningGroup
        {
            get;
            set;
        }

        public ObservableCollection<string> AvailableSections
        {
            get;
            set;
        }

        /// <summary>
        /// The filename that this custom overlay is associated to.
        /// </summary>
        private string Filename
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public VectorMapMode VectorMapMode
        {
            get { return _vectorMapMode; }
            set
            {
                if (SetPropertyValue(ref _vectorMapMode, value, "VectorMapMode", "CustomVectorMapFileInvalid"))
                {
                    UpdateAvailableSections();
                    UpdateGeometry(null);
                }
            }
        }
        private VectorMapMode _vectorMapMode = VectorMapMode.Default;

        /// <summary>
        /// 
        /// </summary>
        public String VectorMapFilename
        {
            get { return _vectorMapFilename; }
            set
            {
                if (SetPropertyValue(ref _vectorMapFilename, value, "VectorMapFilename"))
                {
                    // Try and parse the vector map.
                    UpdateCustomMapSections();
                    UpdateAvailableSections();
                    UpdateGeometry(null);
                }
            }
        }
        private String _vectorMapFilename;

        /// <summary>
        /// 
        /// </summary>
        public bool CustomVectorMapFileInvalid
        {
            get { return _customVectorMapFileInvalid && _vectorMapMode == VectorMapMode.Overridden; }
            set { SetPropertyValue(ref _customVectorMapFileInvalid, value, "CustomVectorMapFileInvalid"); }
        }
        private bool _customVectorMapFileInvalid = true;

        /// <summary>
        /// 
        /// </summary>
        public IDictionary<String, IList<Vector2f>> SectionsToUse
        {
            get
            {
                if (VectorMapMode == VectorMapMode.Default)
                {
                    return OwningGroup.MapSections;
                }
                else
                {
                    return _customMapSections;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private SortedDictionary<string, IList<Vector2f>> _customMapSections = new SortedDictionary<string, IList<Vector2f>>();

        /// <summary>
        /// The map sections that have been chosen by the user to be rendered
        /// onto the overlay.
        /// </summary>
        public IEnumerable<ShownMapSection> ShownMapSections
        {
            get
            {
                foreach (var group in ShownMapSectionGroups)
                {
                    foreach (var section in group.ShownMapSections)
                        yield return section;
                }
            }
        }

        /// <summary>
        /// The map sections that have been chosen by the user to be rendered
        /// onto the overlay.
        /// </summary>
        public ObservableCollection<ShownMapSectionGroup> ShownMapSectionGroups
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the ui control that will act as a detailed view for this overlay, which acts
        /// as the controls view model.
        /// </summary>
        /// <returns></returns>
        public override System.Windows.Controls.Control GetOverlayControl()
        {
            return new CustomOverlayDetailsView();
        }

        /// <summary>
        /// The command that is fired when the user wants to update the geometry on the
        /// viewport with the selected sections.
        /// </summary>
        public RelayCommand UpdateGeometryCommand
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the command used to save the overlay out to a metadata file.
        /// </summary>
        public RelayCommand SaveCommand
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the command used to delete the overlay.
        /// </summary>
        public RelayCommand DeleteCommand
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the command used to delete the overlay.
        /// </summary>
        public RelayCommand AddGroupCommand
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand BrowseCommand
        {
            get;
            private set;
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Initialises a new instance of the 
        /// <see cref="Workbench.AddIn.MapViewport.Overlays.CustomOutlineOverlay"/>
        /// class.
        /// </summary>
        /// <param name="filename">
        /// The filename points to the location of the overlays metadata file.
        /// </param>
        /// <param name="group">
        ///The group that owns and created this overlay. 
        /// </param>
        public CustomOutlineOverlay(string filename, Viewport.AddIn.IMapViewport viewPort, CustomGroup group)
            : base(Path.GetFileNameWithoutExtension(filename), "Custom User Created Overlay.")
        {
            this.OwningGroup = group;
            this.Filename = filename;
            this.ShownMapSectionGroups = new ObservableCollection<ShownMapSectionGroup>();
            this.MapViewportViewModel = viewPort;

            UpdateGeometryCommand = new RelayCommand(this.UpdateGeometry);
            SaveCommand = new RelayCommand(this.SaveOverlay);
            DeleteCommand = new RelayCommand(this.DeleteOverlay, this.CanDeleteOverlay);
            AddGroupCommand = new RelayCommand(this.AddGroup);
            BrowseCommand = new RelayCommand(this.BrowseForVectorMapFile);

            this.AvailableSections = new ObservableCollection<string>();
            foreach (string sections in this.OwningGroup.MapSections.Keys)
                this.AvailableSections.Add(sections);

            foreach (ShownMapSectionGroup shownGroup in this.ShownMapSectionGroups)
            {
                shownGroup.ShownMapSections.CollectionChanged += ShownMapSectionsChanged;
            }

            // Add the callback for when the level changes
            OwningGroup.LevelBrowserProxy.LevelChanged += LevelChanged;

            if (OwningGroup.LevelBrowserProxy.SelectedLevel != null)
            {
                PropertyChangedEventManager.AddListener(OwningGroup.LevelBrowserProxy.SelectedLevel, this, "Initialised");
                OnPropertyChanged("CanActivate");
            }
        }

        /// <summary>
        /// Initialises a new instance of the 
        /// <see cref="Workbench.AddIn.MapViewport.Overlays.CustomOutlineOverlay"/>
        /// class.
        /// </summary>
        /// <param name="group">
        ///The group that owns and created this overlay. 
        /// </param>
        public CustomOutlineOverlay(Viewport.AddIn.IMapViewport viewPort, CustomGroup group)
            : base("Create new overlay...", "Create a new custom overlay.")
        {
            this.Filename = null;
            this.OwningGroup = group;
            this.ShownMapSectionGroups = new ObservableCollection<ShownMapSectionGroup>();
            this.ShownMapSectionGroups.Add(new ShownMapSectionGroup("New Group", DefaultColour, this));
            this.MapViewportViewModel = viewPort;

            UpdateGeometryCommand = new RelayCommand(this.UpdateGeometry);
            SaveCommand = new RelayCommand(this.SaveOverlay);
            DeleteCommand = new RelayCommand(this.DeleteOverlay, this.CanDeleteOverlay);
            AddGroupCommand = new RelayCommand(this.AddGroup);
            BrowseCommand = new RelayCommand(this.BrowseForVectorMapFile);

            this.AvailableSections = new ObservableCollection<string>();
            foreach (string sections in this.OwningGroup.MapSections.Keys)
                this.AvailableSections.Add(sections);

            foreach (ShownMapSectionGroup shownGroup in this.ShownMapSectionGroups)
            {
                shownGroup.ShownMapSections.CollectionChanged += ShownMapSectionsChanged;
            }

            // Add the callback for when the level changes
            OwningGroup.LevelBrowserProxy.LevelChanged += LevelChanged;

            if (OwningGroup.LevelBrowserProxy.SelectedLevel != null)
            {
                PropertyChangedEventManager.AddListener(OwningGroup.LevelBrowserProxy.SelectedLevel, this, "Initialised");
                OnPropertyChanged("CanActivate");
            }
        }
        #endregion

        #region Overridden Methods
        /// <summary>
        /// Occurs whenever the overlay is displayed to the user.
        /// </summary>
        public override void Activated()
        {
            this.OwningGroup.LoadDefinitionsDictionary();
            this.OwningGroup.CreateAvailableSections();

            MapViewportViewModel.BeforeSaveOverlay -= BeforeSaveOverlay;
            MapViewportViewModel.BeforeSaveOverlay+=new EventHandler(BeforeSaveOverlay);

            if (!string.IsNullOrWhiteSpace(Filename) && File.Exists(Filename))
            {
                Deserialise(Filename);
            }

            UpdateCustomMapSections();
            UpdateAvailableSections();

            foreach (ShownMapSectionGroup group in this.ShownMapSectionGroups)
            {
                group.ShownMapSections.CollectionChanged += ShownMapSectionsChanged;
            }

            UpdateGeometry(null);
            base.Activated();
        }

        /// <summary>
        /// Occurs when the overlay is hidden from the user.
        /// </summary>
        public override void Deactivated()
        {
            this.Geometry.Clear();

            foreach (ShownMapSectionGroup group in this.ShownMapSectionGroups)
            {
                group.ShownMapSections.CollectionChanged -= ShownMapSectionsChanged;
            }

            MapViewportViewModel.BeforeSaveOverlay -= BeforeSaveOverlay;

            base.Deactivated();
        }
        #endregion

        #region Methods
        private void ShownMapSectionsChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                if (e.NewItems != null)
                {
                    foreach (var newItem in e.NewItems)
                    {
                        if (!(newItem is ShownMapSection))
                        {
                            continue;
                        }

                        this.AvailableSections.Remove((newItem as ShownMapSection).Name);
                    }
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                if (e.OldItems != null)
                {
                    List<string> items = new List<string>();
                    foreach (string sections in this.AvailableSections)
                        items.Add(sections);

                    foreach (var oldItem in e.OldItems)
                    {
                        if (!(oldItem is ShownMapSection))
                        {
                            continue;
                        }

                        if (!items.Contains((oldItem as ShownMapSection).Name))
                            items.Add((oldItem as ShownMapSection).Name);
                    }

                    items.Sort();
                    foreach (var oldItem in e.OldItems)
                    {
                        if (!(oldItem is ShownMapSection))
                        {
                            continue;
                        }

                        string name = (oldItem as ShownMapSection).Name;
                        if (!this.AvailableSections.Contains(name))
                        {
                            int index = items.IndexOf(name);
                            index = Math.Min(index, this.AvailableSections.Count);
                            this.AvailableSections.Insert(index, name);
                        }
                    }
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Reset)
            {
                UpdateAvailableSections();
            }
        }

        private void BeforeSaveOverlay(object sender, EventArgs e)
        {
            UpdateGeometry(null);
        }

        /// <summary>
        /// Updates the geometry of the overlay, using the shown map sections as the
        /// base for which sections get rendered.
        /// </summary>
        /// <param name="param">
        /// The command parameter.
        /// </param>
        private void UpdateGeometry(object param)
        {
            ObservableCollection<Viewport2DGeometry> geometry = new ObservableCollection<Viewport2DGeometry>();

            foreach (ShownMapSectionGroup group in ShownMapSectionGroups)
            {
                if (group.IsVisibleOnMap == false)
                {
                    continue;
                }

                foreach (ShownMapSection section in group.ShownMapSections)
                {
                    if (!this.SectionsToUse.ContainsKey(section.Name))
                        continue;

                    IList<Vector2f> points = this.SectionsToUse[section.Name];

                    Viewport2DShape geo = new Viewport2DShape(etCoordSpace.World, string.Empty, points.ToArray(), Colors.Black, 1, section.Colour);
                    string text = section.Text;
                    if (string.IsNullOrWhiteSpace(text))
                        text = null;
                    geo.ToolTip = text;
                    geo.UserText = section.Name;
                    geometry.Add(geo);
                }
            }
            this.Geometry = geometry;
        }

        /// <summary>
        /// Saves the overlay to a .metadata file specified by the user, and automatically
        /// adds it in perforce in the default changelist.
        /// </summary>
        /// <param name="param">
        /// The command parameter.
        /// </param>
        private void SaveOverlay(object param)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Filter = "Metadata file|*.meta";
            dlg.InitialDirectory = this.OwningGroup.DirectoryPath;
            bool? result = dlg.ShowDialog();
            if ((bool)result == false)
                return;

            SaveOverlay(dlg.FileName);
        }

        /// <summary>
        /// Saves the overlay to the specified .metadata file, and automatically
        /// adds it in perforce in the default change-list.
        /// </summary>
        /// <param name="filename">
        /// The filename to save the overlay to.
        /// </param>
        private void SaveOverlay(string filename)
        {
            try
            {
                string structureType = "workbench::CustomOverlay";
                string groupType = "workbench::CustomOverlayGroup";
                string sectionType = "workbench::CustomOverlaySection";

                if (!this.OwningGroup.DefinitionDictionary.ContainsKey(structureType))
                {
                    return;
                }

                if (!this.OwningGroup.DefinitionDictionary.ContainsKey(groupType))
                {
                    return;
                }
                
                if (!this.OwningGroup.DefinitionDictionary.ContainsKey(sectionType))
                {
                    return;
                }

                Structure structure = this.OwningGroup.DefinitionDictionary[structureType];
                Structure groupStructure = this.OwningGroup.DefinitionDictionary[groupType];
                Structure sectionStructure = this.OwningGroup.DefinitionDictionary[sectionType];

                MetaFile file = new MetaFile(structure);
                RSG.Metadata.Data.StructureTunable root = file.Members.First().Value as RSG.Metadata.Data.StructureTunable;

                if (root == null || !root.ContainsKey("VisibleSections"))
                    return;

                ArrayTunable array = root["VisibleGroups"] as ArrayTunable;
                if (array == null)
                    return;

                (root["OverrideVectorMap"] as BoolTunable).Value = (VectorMapMode == VectorMapMode.Overridden);
                (root["VectorMapFilename"] as StringTunable).Value = _vectorMapFilename;

                IMember arrayType = (array.Definition as ArrayMember).ElementType;
                foreach (ShownMapSectionGroup group in this.ShownMapSectionGroups)
                {
                    StructureTunable newTunable = TunableFactory.Create(array, arrayType) as StructureTunable;
                    if (newTunable == null)
                        continue;

                    (newTunable["Name"] as StringTunable).Value = group.Name;
                    (newTunable["Colour"] as Color32Tunable).Value = group.Colour;
                    ArrayTunable sectionArray = newTunable["VisibleSections"] as ArrayTunable;
                    if (sectionArray == null)
                        continue;

                    IMember sectionArrayType = (sectionArray.Definition as ArrayMember).ElementType;
                    foreach (ShownMapSection section in group.ShownMapSections)
                    {
                        StructureTunable newSectionTunable = TunableFactory.Create(sectionArray, sectionArrayType) as StructureTunable;

                        (newSectionTunable["SectionName"] as StringTunable).Value = section.Name;
                        (newSectionTunable["SectionColour"] as Color32Tunable).Value = section.Colour;
                        (newSectionTunable["SectionText"] as StringTunable).Value = section.Text;

                        sectionArray.Add(newSectionTunable);
                    }
                    
                    array.Add(newTunable);
                }

                file.Serialise(filename);
            }
            catch
            {
                MessageBox.Show("Unknown error while saving overlay.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (this.Filename == null)
            {
                if (!File.Exists(filename))
                    return;

                OwningGroup.Overlays.BeginUpdate();
                string name = Path.GetFileNameWithoutExtension(filename);
                Viewport.AddIn.IViewportOverlay remove = null;
                int index = -1;
                for (int i = 0; i < OwningGroup.Overlays.Count - 1; i++)
                {
                    if (OwningGroup.Overlays[i].Name != name)
                        continue;

                    remove = OwningGroup.Overlays[i];
                    index = i;
                    break;
                }
                if (remove != null)
                    OwningGroup.Overlays.Remove(remove);

                CustomOutlineOverlay newOverlay = new CustomOutlineOverlay(filename, MapViewportViewModel, this.OwningGroup);
                if (index != -1)
                    OwningGroup.Overlays.Insert(index, newOverlay);
                else
                    OwningGroup.Overlays.Insert(OwningGroup.Overlays.Count - 1, newOverlay);

                OwningGroup.Overlays.EndUpdate();
            }
            else
            {
                UpdateGeometry(null);
            }
        }

        /// <summary>
        /// Determines whether the selected custom overlay (this overlay) can be
        /// delete by the user.
        /// </summary>
        /// <param name="parameter">
        /// The command parameter. 
        /// </param>
        /// <returns>
        ///  True if the DeleteCommand can be executed by the user; otherwise false.
        /// </returns>
        private bool CanDeleteOverlay(object parameter)
        {
            if (string.IsNullOrWhiteSpace(Filename))
                return false;

            return true;
        }

        /// <summary>
        /// Saves the overlay to a .metadata file specified by the user, and automatically
        /// marks it for delete in perforce in the default changelist if apart of the source
        /// control.
        /// </summary>
        /// <param name="param">
        /// The command parameter.
        /// </param>
        private void DeleteOverlay(object param)
        {
            if (string.IsNullOrWhiteSpace(Filename))
                return;

            MessageBoxResult result = MessageBox.Show(string.Format("'{0}'  will be deleted permanently", this.Name), "Rockstar Workbench", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Cancel)
                return;

            if (File.Exists(Filename))
                File.Delete(Filename);

            string name = Path.GetFileNameWithoutExtension(Filename);
            Viewport.AddIn.IViewportOverlay remove = null;
            foreach (Viewport.AddIn.IViewportOverlay overlay in this.OwningGroup.Overlays)
            {
                if (overlay.Name == name)
                {
                    remove = overlay;
                    break;
                }
            }
            if (remove != null)
                this.OwningGroup.Overlays.Remove(remove);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param">
        /// </param>
        private void AddGroup(object param)
        {
            ShownMapSectionGroup group = new ShownMapSectionGroup("New Group", DefaultColour, this);
            this.ShownMapSectionGroups.Add(group);
            group.ShownMapSections.CollectionChanged += ShownMapSectionsChanged;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        private void BrowseForVectorMapFile(object param)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.Filter = "Xml documents (.xml)|*.xml";
            dlg.FileName = _vectorMapFilename;

            if (dlg.ShowDialog() == true)
            {
                VectorMapFilename = dlg.FileName;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param">
        /// </param>
        internal void RemoveGroup(ShownMapSectionGroup group)
        {
            group.ShownMapSections.Clear();
            group.ShownMapSections.CollectionChanged -= ShownMapSectionsChanged;
            this.ShownMapSectionGroups.Remove(group);
        }

        /// <summary>
        /// Initialises this overlay by deserialising the specified metadata file and
        /// creating the shown sections from it.
        /// </summary>
        /// <param name="filename"></param>
        private void Deserialise(string filename)
        {
            try
            {
                this.ShownMapSectionGroups.Clear();
                string structureType = "workbench::CustomOverlay";
                string groupType = "workbench::CustomOverlayGroup";
                string sectionType = "workbench::CustomOverlaySection";

                Structure structure = this.OwningGroup.DefinitionDictionary[structureType];
                Structure groupStructure = this.OwningGroup.DefinitionDictionary[groupType];
                Structure sectionStructure = this.OwningGroup.DefinitionDictionary[sectionType];
                if (structure == null || groupStructure == null || sectionStructure == null)
                    return;

                if (!File.Exists(filename))
                    return;

                MetaFile file = new MetaFile(filename, structure);
                RSG.Metadata.Data.StructureTunable root = file.Members.First().Value as RSG.Metadata.Data.StructureTunable;
                if (root == null)
                    return;
                
                StringTunable vectorMapFilenameTunable = root["VectorMapFilename"] as StringTunable;
                if (vectorMapFilenameTunable != null)
                {
                    VectorMapFilename = vectorMapFilenameTunable.Value;
                }

                BoolTunable overrideTunable = root["OverrideVectorMap"] as BoolTunable;
                if (overrideTunable != null)
                {
                    bool overrideVectorMap = (bool)overrideTunable.Value;
                    VectorMapMode = (overrideVectorMap ? VectorMapMode.Overridden : VectorMapMode.Default);
                }

                ArrayTunable oldArray = root["VisibleSections"] as ArrayTunable;
                if (oldArray != null)
                {
                    foreach (StructureTunable tunable in oldArray.Items.OfType<StructureTunable>())
                    {
                        string name = (tunable["SectionName"] as StringTunable).Value;
                        Color colour = (tunable["SectionColour"] as Color32Tunable).Value;
                        string text = (tunable["SectionText"] as StringTunable).Value;
                        if (string.IsNullOrWhiteSpace(text))
                            text = null;

                        if (this.ShownMapSectionGroups.Count == 0)
                            this.ShownMapSectionGroups.Add(new ShownMapSectionGroup("New Group", DefaultColour, this));

                        ShownMapSection section = new ShownMapSection(name, colour, this.ShownMapSectionGroups[0]);
                        section.Text = text;

                        this.ShownMapSectionGroups[0].ShownMapSections.Add(section);
                    }
                }

                ArrayTunable array = root["VisibleGroups"] as ArrayTunable;
                if (array != null)
                {
                    foreach (StructureTunable tunable in array.Items.OfType<StructureTunable>())
                    {
                        string name = (tunable["Name"] as StringTunable).Value;
                        Color colour = (tunable["Colour"] as Color32Tunable).Value;

                        ShownMapSectionGroup group = new ShownMapSectionGroup(name, colour, this);
                        ArrayTunable sections = tunable["VisibleSections"] as ArrayTunable;
                        if (sections != null)
                        {
                            foreach (StructureTunable section in sections.Items.OfType<StructureTunable>())
                            {
                                string sectionName = (section["SectionName"] as StringTunable).Value;
                                Color sectionColour = (section["SectionColour"] as Color32Tunable).Value;
                                string sectionText = (section["SectionText"] as StringTunable).Value;
                                if (string.IsNullOrWhiteSpace(sectionText))
                                    sectionText = null;

                                ShownMapSection newSection = new ShownMapSection(sectionName, sectionColour, group);
                                newSection.Text = sectionText;

                                group.ShownMapSections.Add(newSection);
                            }
                        }

                        this.ShownMapSectionGroups.Add(group);
                    }
                }
            }
            catch
            {
                this.ShownMapSectionGroups.Clear();
                MessageBox.Show("Unknown error while loading overlay.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
        }

        private void UpdateAvailableSections()
        {
            this.AvailableSections.Clear();

            foreach (string sections in this.SectionsToUse.Keys)
                this.AvailableSections.Add(sections);

            foreach (ShownMapSectionGroup group in this.ShownMapSectionGroups)
            {
                foreach (var section in group.ShownMapSections)
                {
                    this.AvailableSections.Remove(section.Name);
                }
            }
        }

        private void UpdateCustomMapSections()
        {
            _customMapSections.Clear();

            // Make sure a file was specified and that it exists.
            if (String.IsNullOrWhiteSpace(_vectorMapFilename) ||
                !File.Exists(_vectorMapFilename))
            {
                CustomVectorMapFileInvalid = true;
                return;
            }

            // Try and parse the file.
            try
            {
                XDocument doc = XDocument.Load(_vectorMapFilename);

                foreach (XElement sectionElem in doc.XPathSelectElements("/vector_map/section"))
                {
                    XAttribute nameAtt = sectionElem.Attribute("name");
                    if (nameAtt != null)
                    {
                        String sectionName = nameAtt.Value;
                        IList<Vector2f> spline = new List<Vector2f>();

                        foreach (XElement pointElem in sectionElem.XPathSelectElements("spline/point"))
                        {
                            XAttribute xAtt = pointElem.Attribute("x");
                            XAttribute yAtt = pointElem.Attribute("y");
                            if (xAtt != null && yAtt != null)
                            {
                                float x, y;
                                if (Single.TryParse(xAtt.Value, out x) &&
                                    Single.TryParse(yAtt.Value, out y))
                                {
                                    spline.Add(new Vector2f(x, y));
                                }
                            }
                        }

                        if (spline.Any())
                        {
                            _customMapSections.Add(sectionName, spline);
                        }
                    }
                }

                CustomVectorMapFileInvalid = !_customMapSections.Any();
            }
            catch (System.Exception ex)
            {
                CustomVectorMapFileInvalid = true;
            }
        }
        #endregion
    } // CustomOutlineOverlay
    
    /// <summary>
    /// Custom outline overlay that gets created by the custom group and can be serialised
    /// out to a file for sharing.
    /// </summary>
    public class RefreshCustomOutlineOverlay : Viewport.AddIn.ViewportOverlay
    {
        #region Fields
        /// <summary>
        /// Represents whether this overlay has been initially deserialised.
        /// </summary>
        private CustomGroup group;
        #endregion

        #region Constructor
        /// <summary>
        /// Initialises a new instance of the 
        /// <see cref="Workbench.AddIn.MapViewport.Overlays.RefreshCustomOutlineOverlay"/>
        /// class.
        /// </summary>
        /// <param name="group">
        ///The group that owns and created this overlay. 
        /// </param>
        public RefreshCustomOutlineOverlay(CustomGroup group)
            : base("Refresh custom overlay list...", "Refresh custom overlay list.")
        {
            this.group = group;
        }
        #endregion

        #region Overridden Methods
        /// <summary>
        /// Occurs whenever the overlay is displayed to the user.
        /// </summary>
        public override void Activated()
        {
            if (this.group != null)
            {
                this.group.RefreshDirectoryList();
            }

            base.Activated();
        }

        /// <summary>
        /// Occurs when the overlay is hidden from the user.
        /// </summary>
        public override void Deactivated()
        {
            base.Deactivated();
        }
        #endregion
    }

    /// <summary>
    /// Comparer to make sure the same section doesn't get added twice.
    /// </summary>
    public class ShownMapComparer : IEqualityComparer<ShownMapSection>
    {
        #region IEqualityComparer<ShownMapSection> Members
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bool Equals(ShownMapSection x, ShownMapSection y)
        {
            return string.Compare(x.Name, y.Name, true) == 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int GetHashCode(ShownMapSection obj)
        {
            return obj.Name.GetHashCode();
        }
        #endregion
    } // ShownMapComparer

} // Workbench.AddIn.MapViewport.Overlays
