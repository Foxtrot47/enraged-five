﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn.Services;
using Workbench.AddIn;
using System.Net;
using Viewport.AddIn;
using System.ComponentModel;

namespace Workbench.AddIn.MapViewport
{
    /// <summary>
    /// Settings for Metadata Live Editing.
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.Settings, typeof(ISettings))]
    [ExportExtension(Viewport.AddIn.CompositionPoints.MapViewportSettings, typeof(IMapViewportSettings))]
    public class MapViewportSettings : SettingsBase, IMapViewportSettings
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        [System.ComponentModel.DisplayName("Display Grid")]
        [System.ComponentModel.Category("Grid")]
        [System.ComponentModel.Description("Flag specifying whether the grid should be shown or not.")]
        public bool DisplayGrid
        {
            get { return Properties.Settings.Default.DisplayGrid; }
            set { Properties.Settings.Default.DisplayGrid = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [System.ComponentModel.DisplayName("Major Line Spacing (m)")]
        [System.ComponentModel.Category("Grid")]
        [System.ComponentModel.Description("The interval (in meters) at which major lines should be shown in the map viewport.")]
        public uint MajorLineSpacing
        {
            get { return Properties.Settings.Default.MajorLineSpacing; }
            set { Properties.Settings.Default.MajorLineSpacing = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [System.ComponentModel.DisplayName("Minor Line Spacing (m)")]
        [System.ComponentModel.Category("Grid")]
        [System.ComponentModel.Description("The interval (in meters) at which minor lines should be shown in the map viewport.")]
        public uint MinorLineSpacing
        {
            get { return Properties.Settings.Default.MinorLineSpacing; }
            set { Properties.Settings.Default.MinorLineSpacing = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public string SelectedOverlayGroup
        {
            get { return Properties.Settings.Default.SelectedOverlayGroup; }
            set { Properties.Settings.Default.SelectedOverlayGroup = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public string SelectedOverlay
        {
            get { return Properties.Settings.Default.SelectedOverlay; }
            set { Properties.Settings.Default.SelectedOverlay = value; }
        }

        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public MapViewportSettings()
            : base("Map Viewport")
        {
        }
        #endregion // Constructor(s)

        #region Methods
        /// <summary>
        /// Loads settings
        /// </summary>
        public void Load()
        {
        }

        /// <summary>
        /// Applies settings
        /// </summary>
        public void Apply()
        {
            Properties.Settings.Default.Save();
        }

        /// <summary>
        /// Checks its current state for any invalid data.
        /// </summary>
        /// <param name="errors">The service will fill this up with one string per 
        /// error it finds in its data.</param>
        /// <returns>true if everything is fine, false if an error was found</returns>
        public bool Validate(List<String> errors)
        {
            // No validation needed
            return true;
        }
        #endregion // Methods
    } // MapViewportSettings
} // Workbench.AddIn.MapViewport
