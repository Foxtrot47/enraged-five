﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Workbench.AddIn.TXDParentiser
{
    /// <summary>
    /// Lists the importing GUIDs for the workbench to pickup
    /// </summary>
    internal static class CompositionPoints
    {
        #region Document GUID(s)

        /// <summary>
        /// The main GUID for the TXD Parentiser document
        /// </summary>
        public const String TXDParentiserDocument = "0D71E88B-BAA3-4324-9039-058FF554C087";

        #endregion // Document GUID(s)

    } // CompositionPoints
} // Workbench.AddIn.TXDParentiser
