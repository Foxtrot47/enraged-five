﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn;
using Workbench.AddIn.Services;
using Workbench.AddIn.UI.Layout;

namespace Workbench.AddIn.PropViewer
{
    [ExportExtension(Map.AddIn.Services.ExtensionPoints.PropViewer, typeof(Map.AddIn.Services.IPropViewer))]
    class PropViewerProxy : Map.AddIn.Services.IPropViewer
    {
        #region MEF Imports

        /// <summary>
        /// Layout manager reference.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager, typeof(ILayoutManager))]
        private Lazy<ILayoutManager> LayoutManager { get; set; }

        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(IConfigurationService))]
        Lazy<IConfigurationService> ConfigurationService
        {
            get;
            set;
        }

        #endregion // MEF Imports

        /// <summary>
        /// Creates a new document using a map secion to display the ide props
        /// on.
        /// </summary>
        public void CreateViewerDocument(RSG.Model.Map.MapSection section)
        {
            PropViewerView newView = new PropViewerView(section, this.ConfigurationService.Value);
            this.LayoutManager.Value.ShowDocument(newView);
        }
    }
}
