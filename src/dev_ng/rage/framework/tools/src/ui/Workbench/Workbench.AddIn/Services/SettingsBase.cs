﻿using System;
using System.ComponentModel;

namespace Workbench.AddIn.Services
{

    /// <summary>
    /// Base Settings class.
    /// </summary>
    public abstract class SettingsBase : ExtensionBase
    {
        #region Properties
        /// <summary>
        /// Descriptive string for UI display.
        /// </summary>
        [Browsable(false)]
        public String Title { get; protected set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public SettingsBase()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="title"></param>
        public SettingsBase(string title)
        {
            Title = title;
        }
        #endregion // Constructor(s)
    } // SettingsBase

} // Workbench.AddIn.Services
