﻿using System;
using System.Windows.Media;
using System.Linq;
using System.Text;
using RSG.Base.Windows.Controls.WpfViewport2D;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using RSG.Base.Math;
using System.ComponentModel.Composition;
using Workbench.AddIn.Bugstar.OverlayViews;
using RSG.Interop.Bugstar;
using RSG.Base.Logging;
using System.IO;
using System.Xml;
using System.Collections.Specialized;
using System.Windows.Input;
using RSG.Base.Windows.Controls.WpfViewport2D.Controls;
using RSG.Interop.Bugstar.Game;
using RSG.Interop.Bugstar.Organisation;
using RSG.Base.Collections;

namespace Workbench.AddIn.Bugstar.Overlays
{
    [ExportExtension(ExtensionPoints.Overlay, typeof(Viewport.AddIn.IViewportOverlay))]
    public class BugsByMissionOverlay : BugsOverlayBase
    {
        #region Constants
        private const String NAME = "Bugs by Mission";
        private const String DESC = "Shows all the bugs that are for a particular mission.";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<Mission> Missions
        {
            get { return m_missions; }
            set
            {
                if (m_missions == value)
                    return;

                SetPropertyValue(value, () => Missions,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_missions = (ObservableCollection<Mission>)newValue;
                        }
                ));
            }
        }
        private ObservableCollection<Mission> m_missions;

        /// <summary>
        /// 
        /// </summary>
        public Mission SelectedMission
        {
            get { return m_selectedMission; }
            set
            {
                if (m_selectedMission == value)
                    return;

                SetPropertyValue(value, () => SelectedMission,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_selectedMission = (Mission)newValue;
                        }
                ));

                OnSelectedMissionChanged();
            }
        }
        private Mission m_selectedMission;
        #endregion

        #region Constructors
        /// <summary>
        /// Default Constructor
        /// </summary>
        public BugsByMissionOverlay()
            : base(NAME, DESC)
        {
            Missions = new ObservableCollection<Mission>();
        }
        #endregion // Constructors

        #region Overrides
        /// <summary>
        /// Gets called when the user wants to view this overlay
        /// </summary>
        public override void Activated()
        {
            base.Activated();

            // Get the grids that are available for the current project
            if (Missions.Count == 0 && BugstarService.Project != null)
            {
                Missions.AddRange(BugstarService.Project.Missions);
            }
        }

        /// <summary>
        /// Gets called when the user turns off this overlay
        /// </summary>
        public override void Deactivated()
        {
            base.Deactivated();
        }

        /// <summary>
        /// Gets the control that should be shown in the overlay details panel
        /// </summary>
        public override System.Windows.Controls.Control GetOverlayControl()
        {
            return new BugMissionDetailsView();
        }
        #endregion // Overrides

        #region Event Callbacks
        /// <summary>
        /// 
        /// </summary>
        protected void OnSelectedMissionChanged()
        {
            Refresh();
        }
        #endregion // Event Callbacks

        #region BugOverlayBase Overrides
        /// <summary>
        /// 
        /// </summary>
        protected override void RefreshBugList()
        {
            // Get the new list of bugs
            BugList.Clear();
            SelectedBug = null;

            if (SelectedMission != null)
            {
                try
                {
                    BugList.AddRange(SelectedMission.GetBugs("Id,Summary,Description,X,Y,Z,Developer,Tester,State,Component,DueDate,Priority,Category"));
                }
                catch (System.Exception ex)
                {
                    Log.Log__Exception(ex, "Unexpected exception while attempting to retrieve the bug list for mission '{0}'.", SelectedMission.Name);
                }
            }
        }
        #endregion // Private Methods
    } // BugstarBugOverlay
} // Workbench.AddIn.Bugstar.Overlays
