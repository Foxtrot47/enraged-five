﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn;
using Workbench.AddIn.Commands;
using System.ComponentModel.Composition;
using Workbench.AddIn.Services;

namespace MetadataEditor.UI
{

    /// <summary>
    /// Metadata editor toolbar.
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.ToolbarTray, typeof(IToolbar))]
    class MetadataToolbar : ToolBarBase, IPartImportsSatisfiedNotification
    {
        #region MEF Imports
        /// <summary>
        /// Toolbar subitems.
        /// </summary>
        [ImportManyExtension(MetadataEditor.AddIn.ExtensionPoints.MetadataToolbar, 
            typeof(IWorkbenchCommand))]
        private IEnumerable<IWorkbenchCommand> ImportedItems { get; set; }
        #endregion // MEF Imports

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public MetadataToolbar()
        {
        }
        #endregion // Constructor(s)

        #region IPartImportsSatisfiedNotification Methods
        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {
            List<IWorkbenchCommand> allItems = new List<IWorkbenchCommand>();
            allItems.AddRange(this.SortToolBarItems(ImportedItems));
            this.Items = allItems;
        }
        #endregion // IPartImportsSatisfiedNotification Methods
    }

} // MetadataEditor.UI namespace
