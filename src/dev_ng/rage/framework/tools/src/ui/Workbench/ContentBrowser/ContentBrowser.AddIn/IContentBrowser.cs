﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace ContentBrowser.AddIn
{
    /// <summary>
    /// The interface for a content browser object.
    /// Import this if you need to know/manipulate the 
    /// selection of the content browser
    /// </summary>
    public interface IContentBrowser : INotfiyPanelSelectionChanged, INotfiyGridSelectionChanged
    {
        #region Properties
        
        /// <summary>
        /// The currently selected item in the browser collection
        /// </summary>
        RSG.Model.Common.IAsset SelectedItem { get; }

        /// <summary>
        /// The items that are currently selected in the grid panel
        /// of the content browser
        /// </summary>
        IEnumerable<RSG.Model.Common.IAsset> SelectedGridItems { get; }

        #endregion // Properties

        #region Methods
        
        /// <summary>
        /// Returns a enumerable object around any currently selected
        /// items that have the given type
        /// </summary>
        IEnumerable<T> GetGridSelectionForType<T>() where T : RSG.Model.Common.IAsset;

        /// <summary>
        /// Initialises all the attached browsers
        /// </summary>
        void InitialiseBrowsers();

        #endregion // Methods
    } // IContentBrowser
} // ContentBrowser.AddIn
