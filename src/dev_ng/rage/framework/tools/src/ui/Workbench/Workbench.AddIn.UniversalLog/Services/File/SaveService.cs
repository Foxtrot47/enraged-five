﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using Workbench.AddIn.Services.File;

namespace Workbench.AddIn.UniversalLog.Services.File
{
    [ExportExtension(Workbench.AddIn.ExtensionPoints.SaveService, typeof(ISaveService))]
    class SaveService : ISaveService
    {
        #region Constants

        private readonly Guid GUID = new Guid("82EBCCEF-0DAA-4C3A-A301-EC9F7DADDC42");

        #endregion // Constants

        #region Properties

        /// <summary>
        /// Service GUID.
        /// </summary>
        public Guid ID
        {
            get { return (GUID); }
        }

        /// <summary>
        /// Open dialog filter string.
        /// </summary>
        public String Filter
        {
            get { return "ULog documents (.ulog)|*.ulog"; }
        }

        /// <summary>
        /// Open dialog default filter index.
        /// </summary>
        public int DefaultFilterIndex
        {
            get { return 0; }
        }

        /// <summary>
        /// Array of support document GUIDs.
        /// </summary>
        public Guid[] SupportedContent
        {
            get
            {
                return m_supportedContent;
            }
        }
        private Guid[] m_supportedContent = { UniversalLogView.GUID };

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Default constructor.
        /// </summary>
        public SaveService()
        {
        }

        #endregion // Constructors(s)

        #region Methods

        /// <summary>
        /// Returns a value indicating if the save service
        /// can currently be executed
        /// </summary>
        public Boolean CanExecuteSave()
        {
            return true;
        }

        /// <summary>
        /// Returns a value indicating if the save service
        /// can currently be executed
        /// </summary>
        public Boolean CanExecuteSaveAs()
        {
            return true;
        }

        /// <summary>
        /// Save the specified model data to disk file.
        /// </summary>
        public bool Save(IModel saveModel, String filename, int filterIndex, ref Boolean setPathToSavePath)
        {
            setPathToSavePath = true;
            UniversalLogViewModel viewModel = saveModel as UniversalLogViewModel;
            if (viewModel != null)
            {
                viewModel.Save(filename);
                return true;
            }

            return false;
        }

        #endregion // Methods
    }
}
