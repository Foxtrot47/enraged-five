﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.GlobalTXD;

namespace Workbench.AddIn.TXDParentiser.ViewModel
{
    public interface ITextureContainerViewModel
    {
        IEnumerable<ITextureChildViewModel> Textures { get; }

        /// <summary>
        /// If this is true it means that source textures can be added to
        /// this container as children
        /// </summary>
        Boolean CanAcceptTextures { get; }

        /// <summary>
        /// Promotes the given texture (source or global) to here and makes sure that
        /// validation is done so that the texture is removed elsewhere
        /// </summary>
        Boolean PromoteTextureHere(SourceTextureViewModel texture);

        /// <summary>
        /// Moves the given global texture from its previous global dictionary to this 
        /// global dictionary
        /// </summary>
        Boolean MoveTextureHere(GlobalTextureViewModel texture);

        /// <summary>
        /// Moves the given global texture from this dictionary before it gets
        /// added to another dictionary
        /// </summary>
        Boolean MoveTextureFromHere(GlobalTextureViewModel texture);

        void RemoveTexture(ITextureChildViewModel texture);

        void Expand();
    }
} // Workbench.AddIn.TXDParentiser.ViewModel
