﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Report;
using Workbench.AddIn.Services;

namespace Workbench.AddIn.MapStatistics.Reports
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Report.AddIn.ExtensionPoints.MapStatsReport, typeof(IReport))]
    public class IsolatedReferenceReport : RSG.Model.Report.Reports.Map.IsolatedReferenceReport
    {
    } // IsolatedReferenceReport
} // Workbench.AddIn.MapStatistics.Reports
