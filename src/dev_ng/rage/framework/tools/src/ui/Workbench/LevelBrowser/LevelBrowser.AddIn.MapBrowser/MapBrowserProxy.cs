﻿using System;
using System.Xml;
using System.IO;
using System.Xml.XPath;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using LevelBrowser.AddIn;
using RSG.Model.Common;
using RSG.Base.ConfigParser;
using RSG.SceneXml;
using RSG.Model.Common.Map;
using ContentBrowser.AddIn;
using System.ComponentModel.Composition;
using RSG.Base.Logging;
using Workbench.AddIn.Services.Model;
using RSG.Model.Asset.Level;
using Workbench.AddIn.Services;

namespace LevelBrowser.AddIn.MapBrowser
{
    /// <summary>
    /// The exported class to hook into the level
    /// browser
    /// </summary>
    [Workbench.AddIn.ExportExtension(ExtensionPoints.LevelBrowserComponent, typeof(ILevelBrowserComponent))]
    public class MapBrowserProxy : ILevelBrowserComponent, IPartImportsSatisfiedNotification, IDisposable
    {
        #region MEF Imports
        /// <summary>
        /// The data source browser
        /// </summary>
        [Workbench.AddIn.ImportExtension(Workbench.AddIn.CompositionPoints.DataSourceBrowser, typeof(IDataSourceBrowser))]
        private Lazy<IDataSourceBrowser> DataSourceBrowser { get; set; }

        /// <summary>
        /// The build browser
        /// </summary>
        [Workbench.AddIn.ImportExtension(Workbench.AddIn.CompositionPoints.BuildBrowser, typeof(IBuildBrowser))]
        private Lazy<IBuildBrowser> BuildBrowser { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Workbench.AddIn.ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(Workbench.AddIn.Services.IConfigurationService))]
        Lazy<Workbench.AddIn.Services.IConfigurationService> ConfigService { get; set; }

        /// <summary>
        /// Bugstar Service MEF import.
        /// </summary>
        [Workbench.AddIn.ImportExtension(Workbench.AddIn.CompositionPoints.BugstarService, typeof(IBugstarService))]
        private Lazy<IBugstarService> BugstarService { get; set; }
        #endregion // MEF Imports

        #region Member Data
        /// <summary>
        /// Reference to the list of levels.
        /// </summary>
        private ILevelCollection Levels { get; set; }

        /// <summary>
        /// Factory for creating the appropriate hierarchy (based off of the data source).
        /// </summary>
        private IMapFactory MapFactory { get; set; }

        /// <summary>
        /// Background worker for loading the map data.
        /// </summary>
        private BackgroundWorker AsyncWorker { get; set; }
        #endregion // Member Data

        #region Constructors

        /// <summary>
        /// Default constructors
        /// </summary>
        public MapBrowserProxy()
        {
            this.AsyncWorker = new BackgroundWorker();
            this.AsyncWorker.WorkerReportsProgress = false;
            this.AsyncWorker.WorkerSupportsCancellation = true;
            this.AsyncWorker.DoWork += new DoWorkEventHandler(this.RefreshDoWork);
            this.AsyncWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(this.RefreshCompleted);
        }

        #endregion // Constructors

        #region Methods

        /// <summary>
        /// Called when the map data needs to be refreshed (i.e
        /// re-created).
        /// </summary>
        public void OnRefresh(ILevelCollection levels)
        {
            this.Levels = levels;

            if (this.AsyncWorker.IsBusy)
            {
                this.AsyncWorker.CancelAsync();
            }
            else
            {
                this.AsyncWorker.RunWorkerAsync();
            }
        }
        
        /// <summary>
        /// Returns all of the paths that need checking and syncing before the user can start up the
        /// level browser.
        /// </summary>
        public String[] GetFilesForSync(ConfigGameView gv)
        {
            List<String> files = new List<String>();
            /// Grab the map xml files
            files.Add(System.IO.Path.Combine(gv.ExportDir, "levels....xml"));
            /// Grab the prop renders
            files.Add(System.IO.Path.Combine(gv.AssetsDir, "maps", "propRenders....jpg"));

            return files.ToArray();
        }

        #endregion // Methods

        #region Private Methods

        /// <summary>
        /// Work handler for the loading thread
        /// </summary>
        private void RefreshDoWork(Object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            IEnumerable<StreamableStat> statsToLoad = new StreamableStat[]
                { StreamableMapSectionStat.ExportEntities, StreamableMapSectionStat.ExportArchetypes, StreamableMapSectionStat.Archetypes };

            foreach (ILevel level in Levels)
            {
                if (level.Name != "None")
                {
                    String buildIdentifier = (BuildBrowser.Value.SelectedBuild != null ? BuildBrowser.Value.SelectedBuild.Identifier : null);

                    level.MapHierarchy = MapFactory.CreateHierarchy(DataSourceBrowser.Value.SelectedSource, level, buildIdentifier);

                    // Is a cancellation pending?
                    if (worker.CancellationPending == true)
                    {
                        e.Cancel = true;
                        break;
                    }

                    if (level.MapHierarchy != null)
                    {
                        // Load the archetypes for all the prop/interior sections (do this in two steps)

                        Log.Log__Profile("Loading prop statistics.");
                        IEnumerable<IMapSection> propSections =
                            level.MapHierarchy.AllSections.Where(item => item.Type == RSG.Model.Common.Map.SectionType.Prop);

                        if (level is LocalLevel)
                        {
                            propSections.AsParallel().ForAll(section => section.LoadStats(statsToLoad));
                        }
                        else
                        {
                            level.MapHierarchy.RequestStatisticsForSections(propSections, statsToLoad, false);
                        }
                        Log.Log__ProfileEnd();

                        // Is a cancellation pending?
                        if (worker.CancellationPending == true)
                        {
                            e.Cancel = true;
                            break;
                        }

                        Log.Log__Profile("Loading interior statistics.");
                        IEnumerable<IMapSection> interiorSections =
                            level.MapHierarchy.AllSections.Where(item => item.Type == RSG.Model.Common.Map.SectionType.Interior);

                        if (level is LocalLevel)
                        {
                            interiorSections.AsParallel().ForAll(section => section.LoadStats(statsToLoad));
                        }
                        else
                        {
                            level.MapHierarchy.RequestStatisticsForSections(interiorSections, statsToLoad, false);
                        }
                        Log.Log__ProfileEnd();
                    }
                }

                // Is a cancellation pending?
                if (worker.CancellationPending == true)
                {
                    e.Cancel = true;
                    break;
                }

                level.Initialised = true;
            }
        }

        /// <summary>
        /// Completed handler for the loading thread
        /// </summary>
        private void RefreshCompleted(Object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                OnRefresh(this.Levels);
            }
        }
        #endregion // Private Methods

        #region IPartImportsSatisfiedNotification Interface
        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {
            MapFactory = new RSG.Model.Map3.MapFactory(ConfigService.Value.Config, ConfigService.Value.BugstarConfig, BugstarService.Value.Connection);
        }
        #endregion // IPartImportsSatisfiedNotification Interface

        #region IDisposable Implementation
        /// <summary>
        /// Correctly dispose of the weapon factory.
        /// </summary>
        public void Dispose()
        {
            MapFactory.Dispose();
        }
        #endregion // IDisposable Implementation
    } // MapBrowserProxy
} // LevelBrowser.AddIn.MapBrowser
