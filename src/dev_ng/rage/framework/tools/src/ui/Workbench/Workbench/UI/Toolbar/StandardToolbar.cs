﻿using System;
using System.ComponentModel.Composition;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn;
using Workbench.AddIn.Commands;
using Workbench.AddIn.Services;
using Workbench.AddIn.Services.File;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.UI.Menu;
using Workbench.UI.Menu;

namespace Workbench.UI.Toolbar
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.ToolbarTray, typeof(IToolbar))] 
    public class StandardToolbar : ToolBarBase, IPartImportsSatisfiedNotification
    {
        #region MEF Imports

        /// <summary>
        /// Extensions service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ExtensionService)]
        private IExtensionService ExtensionService { get; set; }

        /// <summary>
        /// Window menu subitems.
        /// </summary>
        [ImportManyExtension(Workbench.AddIn.ExtensionPoints.ToolbarStandard, typeof(IWorkbenchCommand))]
        private IEnumerable<IWorkbenchCommand> items { get; set; }

        /// <summary>
        /// toolbar new services
        /// </summary>
        [ImportManyExtension(Workbench.AddIn.ExtensionPoints.ToolbarStandard, typeof(INewService))]
        private IEnumerable<INewService> NewServices { get; set; }

        /// <summary>
        /// toolbar new services
        /// </summary>
        [ImportManyExtension(Workbench.AddIn.ExtensionPoints.ToolbarStandard, typeof(IOpenService))]
        private IEnumerable<IOpenService> OpenServices { get; set; }

        /// <summary>
        /// MEF import for workbench layout manager.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager, typeof(ILayoutManager))]
        private ILayoutManager LayoutManager { get; set; }

        /// <summary>
        /// MEF import for workbench configuration service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(IConfigurationService))]
        private IConfigurationService ConfigurationService { get; set; }

        /// <summary>
        /// MEF import for workbench configuration service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.WorkbenchOpenService, typeof(IWorkbenchOpenService))]
        private IWorkbenchOpenService WorkbenchOpenService { get; set; }

        #endregion // MEF Imports
        
        #region Constructor(s)

        /// <summary>
        /// Default constructor.
        /// </summary>
        public StandardToolbar()
        {
        }

        #endregion // Constructor(s)

        #region IPartImportsSatisfiedNotification Methods

        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {
            List<IWorkbenchCommand> allItems = new List<IWorkbenchCommand>();
            List<IWorkbenchCommand> newServices = new List<IWorkbenchCommand>();
            List<IWorkbenchCommand> openServices = new List<IWorkbenchCommand>();
            Guid previousId = Guid.NewGuid();
            foreach (INewService os in this.NewServices)
            {
                NewFileServiceMenuItem mi = new NewFileServiceMenuItem(os, previousId, this.LayoutManager, this.ConfigurationService);
                previousId = mi.ID;
                newServices.Add(mi);
            }
            foreach (IOpenService os in this.OpenServices)
            {
                OpenFileServiceMenuItem mi = new OpenFileServiceMenuItem(os, previousId, this.LayoutManager, this.WorkbenchOpenService);
                previousId = mi.ID;
                openServices.Add(mi);
            }
            allItems.AddRange(newServices);
            allItems.AddRange(openServices);
            allItems.AddRange(this.SortToolBarItems(items));
            this.Items = allItems;
        }

        #endregion // IPartImportsSatisfiedNotification Methods
    } // StandardToolbar

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.ToolbarStandard, typeof(IWorkbenchCommand))]
    class StandardToolbarSep1 : WorkbenchCommandSeparator
    {
        #region Constants

        public static readonly Guid GUID = new Guid("3EBBAE34-9CD6-40AF-88DE-BCD8FAFA651A");

        #endregion // Constants

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public StandardToolbarSep1()
        {
            this.ID = GUID;
            this.IsSeparator = true;
            this.RelativeToolBarID = new Guid(Workbench.AddIn.CompositionPoints.MenuFileSaveAll);
            this.Direction = Direction.After;
        }

        #endregion // Constructor(s)

        #region Public Members

        public void SetPlacement(Guid relative, Direction direction)
        {
            this.RelativeID = relative;
            this.Direction = direction;
        }

        #endregion // Public Members
    } // StandardToolbarSep1

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.ToolbarStandard, typeof(IWorkbenchCommand))]
    class StandardToolbarSep2 : WorkbenchCommandSeparator
    {
        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public StandardToolbarSep2()
        {
            this.ID = new Guid(Workbench.AddIn.CompositionPoints.ToolWindowSeparator);
            this.IsSeparator = true;
            this.RelativeToolBarID = RedoMenuItem.GUID;
            this.Direction = Direction.After;
        }

        #endregion // Constructor(s)

        #region Public Members

        public void SetPlacement(Guid relative, Direction direction)
        {
            this.RelativeID = relative;
            this.Direction = direction;
        }

        #endregion // Public Members
    } // StandardToolbarSep2
} // Workbench.UI.Toolbar
