﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using RSG.Base.Logging;
using Workbench.AddIn;
using Workbench.AddIn.Commands;
using Workbench.AddIn.Services;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.UI.Menu;

namespace Workbench.AddIn.Devstar.UI.Menu
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.MenuHelp, typeof(IWorkbenchCommand))]
    [ExportExtension(Workbench.AddIn.CompositionPoints.WebBrowserService, typeof(IWebBrowserService))]
    class DevstarMenuItem : WorkbenchMenuItemBase, IWebBrowserService
    {
        #region Constants

        public static readonly Guid GUID = new Guid("2444D91B-8668-464E-9393-EF970EEE3108");

        #endregion // Constants

        #region MEF Imports

        [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager, typeof(ILayoutManager))]
        private ILayoutManager LayoutManager { get; set; }

        #endregion // MEF Imports

        #region Constructor(s)

        /// <summary>
        /// Default constructor.
        /// </summary>
        public DevstarMenuItem()
        {
            this.Header = "_Devstar";
            this.ID = GUID;
        }

        #endregion // Constructor(s)

        #region ICommandControl Interface

        public override Boolean CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        public override void Execute(Object parameter)
        {
            Log.Log__Debug("DevstarAction::Execute()");

            DevstarView view = new DevstarView();
            this.LayoutManager.ShowDocument(view);
        }

        public void OpenNewTabAtAddress(string address)
        {
            DevstarView view = new DevstarView(address);
            this.LayoutManager.ShowDocument(view);
        }

        #endregion // ICommandControl Interface
    } // DevstarMenuItem
} // Workbench.AddIn.Devstar.UI.Menu namespace
