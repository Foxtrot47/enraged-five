﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.LiveEditing;

namespace ContentBrowser.ViewModel.LiveEditingViewModels
{
    /// <summary>
    /// 
    /// </summary>
    internal class ObjectServiceViewModel : AssetViewModelBase
    {
        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public ObjectServiceViewModel(IObjectService service)
            : base(service)
        {
        }
        #endregion // Constructor(s)
    }
}
