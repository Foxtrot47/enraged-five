﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Report.AddIn;
using RSG.Model.Report;

namespace Workbench.AddIn.GameStatistics.Reports.Automated.Build
{
    /// <summary>
    /// Category for per build reports based on the stats gathered from the automated tests that run on cruise.
    /// </summary>
    [ExportExtension(ExtensionPoints.AutomatedReport, typeof(IReport))]
    public class AutomatedBuildCategory : ReportCategory
    {
        #region Constants
        private const String c_name = "Build";
        private const String c_description = "Per build reports based on the stats gathered from the automated tests that run on cruise.";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Imported reports for this category.
        /// </summary>
        [ImportManyExtension(ExtensionPoints.AutomatedBuildReport, typeof(IReport))]
        public override IEnumerable<IReportItem> Reports
        {
            get;
            protected set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public AutomatedBuildCategory()
            : base(c_name, c_description)
        {
        }
        #endregion // Constructor(s)
    } // AutomatedBuildCategory
}
