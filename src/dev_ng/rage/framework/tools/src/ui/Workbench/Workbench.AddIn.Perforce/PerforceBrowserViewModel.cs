﻿using System;
using System.Collections.ObjectModel;
using RSG.Base.Editor;
using Workbench.AddIn.Perforce.ViewModel;

namespace Workbench.AddIn.Perforce
{

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(CompositionPoints.PerforceBrowser, typeof(IPerforceBrowser))]
    class PerforceBrowserViewModel : 
        HierarchicalViewModelBase,
        IPerforceBrowser
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<ConnectionViewModel> Connections
        {
            get;
            set;
        }
        #endregion // Properties and Associated Member Data
    }

} // Workbench.AddIn.Perforce namespace
