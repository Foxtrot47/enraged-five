﻿using System;
using System.Linq;
using System.ComponentModel.Composition;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Workbench.AddIn;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.Commands;
using RSG.Model.Report;
using RSG.Model.Report.Reports;
using Workbench.AddIn.ReportBrowser;
using Workbench.AddIn.Services;
using RSG.Base.Logging;
using ReportBrowser.Addin;
using RSG.Base.Threading;
using System.Threading;
using RSG.Base.Windows.Dialogs;
using RSG.Base.Tasks;
using RSG.Model.Common.Map;
using Workbench.AddIn.Services.Model;

namespace ContentBrowser.AddIn.ReportBrowser.Commands
{
    [ExportExtension(ContentBrowser.AddIn.ExtensionPoints.ReportAssetCommands, typeof(IWorkbenchCommand))]
    public class OpenReportCommand : WorkbenchMenuItemBase
    {
        #region MEF Imports

        [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager, typeof(ILayoutManager))]
        private ILayoutManager LayoutManager { get; set; }

        /// <summary>
        /// A reference to the level browser interface
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LevelBrowser, typeof(ILevelBrowser))]
        private Lazy<ILevelBrowser> LevelBrowserProxy { get; set; }

        /// <summary>
        /// The platform browser
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.PlatformBrowser, typeof(IPlatformBrowser))]
        private Lazy<IPlatformBrowser> PlatformBrowser { get; set; }

        /// <summary>
        /// Application startup window.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(IConfigurationService))]
        public IConfigurationService ConfigService { get; set; }

        /// <summary>
        /// The data source browser
        /// </summary>
        [Workbench.AddIn.ImportExtension(Workbench.AddIn.CompositionPoints.DataSourceBrowser, typeof(IDataSourceBrowser))]
        private Lazy<IDataSourceBrowser> DataSourceBrowser { get; set; }

        private List<IReport> generatedReports;
        #endregion // MEF Imports

        #region Constructor

        /// <summary>
        /// Default constructor.
        /// </summary>
        public OpenReportCommand()
        {
            this.Header = "_Open";
            this.IsDefault = true;
            this.generatedReports = new List<IReport>();
        }

        #endregion // Constructor

        #region ICommand Implementation

        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            IDynamicLevelReport dynamicLevelReport = parameter as IDynamicLevelReport;
            if (dynamicLevelReport != null)
            {
                if (dynamicLevelReport.DataSourceModes[DataSourceBrowser.Value.SelectedSource])
                {
                    return this.LevelBrowserProxy.Value.SelectedLevel.Initialised;
                }
                else
                {
                    return false;
                }
            }
            else if (parameter is IDynamicSectionReport)
            {
                return this.LevelBrowserProxy.Value.SelectedLevel.Initialised;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
            if (parameter is IEnumerable)
            {
                foreach (Object report in (parameter as IEnumerable))
                {
                    this.OpenReport(report as RSG.Model.Report.IReport);
                }
            }
            else
            {
                this.OpenReport(parameter as RSG.Model.Report.IReport);
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="report"></param>
        private void OpenReport(RSG.Model.Report.IReport report)
        {
            Debug.Assert(null != report, "Invalid report.  Ignoring.");
            if (null == report)
                return;

            // If the report generates a file figure out where the user wants to save it
            if (report is IReportFileProvider)
            {
                IReportFileProvider reportFile = report as IReportFileProvider;

                Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
                dlg.Filter = reportFile.Filter;
                Nullable<bool> result = dlg.ShowDialog();
                if (false == result)
                    return;

                reportFile.Filename = dlg.FileName;
            }

            // If this is a dynamic report
            IDynamicReport dynamicReport = report as IDynamicReport;
            if (dynamicReport != null && dynamicReport.GenerationTask != null)
            {
                // Create the context to use for generating this report.
                CancellationTokenSource cts = new CancellationTokenSource();
                DynamicReportContext reportContext = null;

                if (report is IDynamicLevelReport)
                {
                    reportContext = new DynamicLevelReportContext(cts.Token, ConfigService.GameConfig, ConfigService.Config, LevelBrowserProxy.Value.SelectedLevel, PlatformBrowser.Value.PlatformCollection);
                }
                else if (report is IDynamicSectionReport)
                {
                    IDynamicSectionReport dynamicSectionReport = report as IDynamicSectionReport;

                    IList<IMapSection> sections = new List<IMapSection>();
                    SectionSelection.SectionSelection selection = new SectionSelection.SectionSelection();
                    SectionSelection.SectionSelectionViewModel vm = new SectionSelection.SectionSelectionViewModel(LevelBrowserProxy.Value.SelectedLevel);
                    selection.DataContext = vm;
                    Nullable<bool> selectionResult = selection.ShowDialog();
                    if (false == selectionResult)
                    {
                        return;
                    }

                    vm.GetSelectedSections(ref sections);
                    if (sections.Count < 2)
                    {
                        return;
                    }

                    reportContext = new DynamicSectionReportContext(cts.Token, ConfigService.GameConfig, ConfigService.Config, sections);
                }
                else if (report is IDynamicReport)
                {
                    reportContext = new DynamicReportContext(cts.Token, ConfigService.GameConfig, ConfigService.Config);
                }
                else
                {
                    Debug.Assert(false, "Unknown dynamic report type encountered.");
                }

                // Check what kind of data the report requires for generation (if any).
                ITask dataLoadTask = null;

                if (report is IDynamicLevelReport)
                {
                    IDynamicLevelReport dynamicLevelReport = (IDynamicLevelReport)report;
                    if (dynamicLevelReport.RequiredStats != null && dynamicLevelReport.RequiredStats.Any())
                    {
                    }
                }

                bool generate = true;
                if (!(report is IReportFileProvider))
                {
                    if (this.generatedReports.Contains(report))
                    {
                        generate = false;
                    }
                }

                if (generate)
                {
                    // Create the task that we will be running (based on whether we need to load data).
                    ITask taskToRun = dynamicReport.GenerationTask;
                    if (dataLoadTask != null)
                    {
                        CompositeTask compositeTask = new CompositeTask("Generating report");
                        compositeTask.AddSubTask(dataLoadTask);
                        compositeTask.AddSubTask(dynamicReport.GenerationTask);
                        taskToRun = compositeTask;
                    }

                    // Show the task execution dialog to begin executing the task.
                    TaskExecutionDialog dialog = new TaskExecutionDialog();
                    TaskExecutionViewModel taskVm = new TaskExecutionViewModel("Generating Report", taskToRun, cts, reportContext);
                    taskVm.AutoCloseOnCompletion = true;
                    dialog.DataContext = taskVm;
                    Nullable<bool> taskResult = dialog.ShowDialog();
                    if (false == taskResult)
                    {
                        return;
                    }

                    this.generatedReports.Add(report);
                }
            }

            // Check if the report generated a uri or has a stream to be displayed in the web browser.
            if (report is IReportUriProvider)
            {
                ReportUriView view = new ReportUriView(new ReportUriViewModel(report));
                this.LayoutManager.ShowDocument(view);
            }
            else if (report is IReportStreamProvider)
            {
                ReportUriView view = new ReportUriView(new ReportUriViewModel(report));
                this.LayoutManager.ShowDocument(view);
            }
            else if (report is IReportDocumentProvider)
            {
                this.LayoutManager.ShowDocument((report as IReportDocumentProvider).Document);
            }

            // Perform final cleanup.
            if (report is IDisposable)
            {
                (report as IDisposable).Dispose();
            }
        }

        #endregion // ICommand Implementation
    } // OpenReportCommand
} // ContentBrowser.AddIn.ReportBrowser.Commands
