﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Workbench.AddIn.MapStatistics
{
    /// <summary>
    /// 
    /// </summary>
    public static class StatisticsUtil  
    {
        /// <summary>
        /// Turns a integer size in bytes to a string representation
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        public static String SizeToString(ulong size)
        {
            String prettySize = String.Empty;
            ulong bytes = size;
            if (bytes < 1024 * 1024)
            {
                ulong kbytes = bytes >> 10;
                ulong kbyte = 1024;
                ulong leftover = bytes - (kbytes * kbyte);

                if (leftover > 0)
                {
                    prettySize = String.Format("{0} KB", ((double)bytes / (double)kbyte).ToString("F1"));
                }
                else
                {
                    prettySize = String.Format("{0} KB", bytes >> 10);
                }
            }
            else
            {
                ulong mbytes = bytes >> 20;
                ulong mbyte = 1024 * 1024;
                ulong leftover = bytes - (mbytes * mbyte);

                if (leftover > 0)
                {
                    prettySize = String.Format("{0} MB", ((double)bytes / (double)mbyte).ToString("F1"));
                }
                else
                {
                    prettySize = String.Format("{0} MB", bytes >> 20);
                }
            }
            return prettySize;
        }
    }
}
