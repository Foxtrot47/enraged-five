﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Windows.Helpers;
using RSG.Model.Common;

namespace Workbench.AddIn.Services.Model
{
    /// <summary>
    /// 
    /// </summary>
    public interface IDataSourceBrowser
    {
        #region Events
        /// <summary>
        /// Gets called when the current build changes
        /// </summary>
        event DataSourceChangedEventHandler DataSourceChanged;
        #endregion // Events

        #region Properties
        /// <summary>
        /// The currently selected item in the data source collection
        /// </summary>
        DataSource SelectedSource { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Updates the data sources.
        /// </summary>
        void UpdateDataSources();
        #endregion // Methods
    } // IDataSourceBrowser

    /// <summary>
    /// DataSource changed
    /// </summary>
    public class DataSourceChangedEventArgs : ItemChangedEventArgs<DataSource?>
    {
        public DataSourceChangedEventArgs(DataSource? oldSource, DataSource? newSource)
            : base(oldSource, newSource)
        {
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="args"></param>
    public delegate void DataSourceChangedEventHandler(Object sender, DataSourceChangedEventArgs args);
}
