﻿using System;
using System.Windows.Media.Imaging;

namespace Workbench.AddIn
{

    /// <summary>
    /// 
    /// </summary>
    public interface IPluginDetails
    {
        /// <summary>
        /// 
        /// </summary>
        BitmapSource Image { get; }
    
        /// <summary>
        /// 
        /// </summary>
        String Title { get; }

        /// <summary>
        /// 
        /// </summary>
        String Version { get; }

        /// <summary>
        /// 
        /// </summary>
        String Author { get; }

        /// <summary>
        /// 
        /// </summary>
        String Copyright { get; }
    }

} // Workbench.AddIn namespace
