﻿using MetadataEditor.AddIn.MetaEditor.ViewModel;
using RSG.Metadata.Data;

namespace MetadataEditor.AddIn.MetaEditor.GridViewModel
{
    public static class TunableViewModelFactory
    {
        public static GridTunableViewModel Create(GridTunableViewModel parent, ITunable m, MetaFileViewModel root)
        {
            if (m is StructureTunable)
            {
                return new StructureTunableViewModel(parent, m as StructureTunable, root);
            }
            if (m is ArrayTunable)
            {
                return new ArrayTunableViewModel(parent, m as ArrayTunable, root);
            }
            else if (m is StringTunable)
            {
                return new StringTunableViewModel(parent, m as StringTunable, root);
            }
            else if (m is PointerTunable)
            {
                return new PointerTunableViewModel(parent, m as PointerTunable, root);
            }
            else if (m is FloatTunable)
            {
                return new FloatTunableViewModel(parent, m as FloatTunable, root);
            }
            else if (m is Float16Tunable)
            {
                return new Float16TunableViewModel(parent, m as Float16Tunable, root);
            }
            else if (m is BoolTunable)
            {
                return new BoolTunableViewModel(parent, m as BoolTunable, root);
            }
            else if (m is IntegerTunable)
            {
                return new IntegerTunableViewModel(parent, m as IntegerTunable, root);
            }
            else if (m is Color32Tunable)
            {
                return new ColourTunableViewModel(parent, m as Color32Tunable, root);
            }
            else if (m is EnumTunable)
            {
                return new EnumTunableViewModel(parent, m as EnumTunable, root);
            }
            else if (m is VecBoolVTunable)
            {
                return new VecBoolVTunableViewModel(parent, m as VecBoolVTunable, root);
            }
            else if (m is Vector2TunableBase)
            {
                return new Vector2TunableViewModel(parent, m as Vector2TunableBase, root);
            }
            else if (m is Vector3TunableBase)
            {
                return new Vector3TunableViewModel(parent, m as Vector3TunableBase, root);
            }
            else if (m is Vector4TunableBase)
            {
                return new Vector4TunableViewModel(parent, m as Vector4TunableBase, root);
            }
            else if (m is Matrix33TunableBase)
            {
                return new Matrix33TunableViewModel(parent, m as Matrix33TunableBase, root);
            }
            else if (m is Matrix34TunableBase)
            {
                return new Matrix34TunableViewModel(parent, m as Matrix34TunableBase, root);
            }
            else if (m is Matrix44TunableBase)
            {
                return new Matrix44TunableViewModel(parent, m as Matrix44TunableBase, root);
            }
            else if (m is BitsetTunable)
            {
                return new BitsetTunableViewModel(parent, m as BitsetTunable, root);
            }
            else if(m is MapTunable)
            {
                return new MapTunableViewModel(parent, m as MapTunable, root);
            }

            return new GridTunableViewModel(parent, m, root);
        }
    }
}
