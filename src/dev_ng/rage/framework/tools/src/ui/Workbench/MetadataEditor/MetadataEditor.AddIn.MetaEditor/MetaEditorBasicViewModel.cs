﻿using System;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Diagnostics;
using Workbench.AddIn;
using Workbench.AddIn.UI.Layout;
using MetadataEditor.AddIn.View;
using RSG.Base.Editor;
using MetadataEditor.AddIn.MetaEditor.ViewModel;

namespace MetadataEditor.AddIn.MetaEditor
{

    /// <summary>
    /// Simple ViewModel for the core Metadata Editor(s).
    /// </summary>
    class MetaEditorBasicViewModel : ViewModelBase
    {
        #region Constants
        public const String DOCUMENT_NAME = "MetaEditorBasicViewModel";
        public const String DOCUMENT_TITLE = "Metadata";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public MetaEditorBasicViewModel(MetaFileViewModel viewModel)
        {
        }
        #endregion // Constructor(s)
    }

} // MetadataEditor.AddIn.MetaEditor namespace
