﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Media;
using RSG.Base.Collections;
using Workbench.AddIn.Services;
using RSG.Base.ConfigParser;
using Workbench.AddIn.MapDebugging.OverlayViews;
using System.ComponentModel.Composition;
using RSG.Base.Editor;
using RSG.Base.Math;
using RSG.Base.Windows.Controls.WpfViewport2D.Controls;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using RSG.Model.Common;
using RSG.Model.Common.Map;
using RSG.Base.Extensions;
using RSG.Base.Tasks;
using RSG.Base.Windows.Dialogs;
using RSG.Model.Common.Extensions;
using RSG.Model.Common.Util;
using WidgetEditor.AddIn;
using MapViewport.AddIn;
using Workbench.AddIn.Services.Model;
using Workbench.AddIn.MapDebugging.ToolTips;
using System.Windows.Input;

namespace Workbench.AddIn.MapDebugging.Overlays
{    
    /// <summary>
    /// Overlay to display bounds for map section instance positions.
    /// </summary>
    [ExportExtension("Workbench.AddIn.MapDebugging.OverlayGroups.MapDebug", typeof(Viewport.AddIn.ViewportOverlay))]
    class RageLightInstanceOverlay : LevelDependentViewportOverlay
    {
        #region Fields
        /// <summary>
        /// The name of the overlay.
        /// </summary>
        private const String c_name = "Rage Light Instances";

        /// <summary>
        /// The description for this overlay.
        /// </summary>
        private const String c_description = "Displays the entities that has any amount of Rage Light Instances attached to them.";

        /// <summary>
        /// Scale of the overlay image in relation to the level's background image.
        /// </summary>
        private const float c_overlayImageScale = 0.25f;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public RageLightInstanceOverlay()
            : base(c_name, c_description)
        {
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Gets called when the user wants to view this overlay
        /// </summary>
        public override void Activated()
        {
            base.Activated();
            if (LevelBrowserProxy.Value.SelectedLevel == null)
            {
                return;
            }

            // Make sure that the required data is loaded
            CancellationTokenSource cts = new CancellationTokenSource();
            LevelTaskContext context = new LevelTaskContext(cts.Token, LevelBrowserProxy.Value.SelectedLevel);

            ITask loadDataTask = new ActionTask("Load Data", (ctx, progress) => EnsureDataLoaded((LevelTaskContext)ctx, progress));

            TaskExecutionDialog dialog = new TaskExecutionDialog();
            TaskExecutionViewModel vm = new TaskExecutionViewModel("Generating Overlay", loadDataTask, cts, context);
            vm.AutoCloseOnCompletion = true;
            dialog.DataContext = vm;
            Nullable<bool> selectionResult = dialog.ShowDialog();
            if (false == selectionResult)
            {
                return;
            }

            UpdateOverlayImage(LevelBrowserProxy.Value.SelectedLevel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        private void UpdateOverlayImage(ILevel level)
        {
            Geometry.Clear();

            // Update the overlay image
            if (level != null)
            {
                Vector2i size = new Vector2i(966, 627);
                Vector2f pos = new Vector2f(-483.0f, 627.0f);

                if (level.ImageBounds != null)
                {
                    size = new RSG.Base.Math.Vector2i((int)(level.ImageBounds.Max.X - level.ImageBounds.Min.X), (int)(level.ImageBounds.Max.Y - level.ImageBounds.Min.Y));
                    pos = new RSG.Base.Math.Vector2f(level.ImageBounds.Min.X, level.ImageBounds.Max.Y);
                }

                Viewport2DImageOverlay overlayImage = new Viewport2DImageOverlay(etCoordSpace.World, "", new RSG.Base.Math.Vector2i((int)(size.X * c_overlayImageScale), (int)(size.Y * c_overlayImageScale)), pos, new RSG.Base.Math.Vector2f(size.X, size.Y));

                overlayImage.BeginUpdate();
                RenderRageLightInstances(overlayImage);
                overlayImage.EndUpdate();
                Geometry.Add(overlayImage);
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        private void RenderRageLightInstances(Viewport2DImageOverlay overlayImage)
        {
            IMapHierarchy hierarchy = LevelBrowserProxy.Value.SelectedLevel.MapHierarchy;
            System.Drawing.Color spawnTypeColor = System.Drawing.Color.Red;
            foreach (IMapSection section in hierarchy.AllSections)
            {
                if (section == null || section.ChildEntities == null)
                {
                    continue;
                }

                foreach (IEntity entity in section.ChildEntities)
                {
                    if (entity == null || entity.RageLightInstanceCount == 0)
                    {
                        continue;
                    }

                    overlayImage.RenderCircle(entity.Position, 5.0f, spawnTypeColor);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="progress"></param>
        private void EnsureDataLoaded(LevelTaskContext context, IProgress<TaskProgress> progress)
        {
            // Compute the total number of steps to process
            int steps = context.MapHierarchy.AllSections.Count() + 1;
            float increment = 1.0f / steps;

            // Load all the map section data
            foreach (IMapSection section in context.MapHierarchy.AllSections)
            {
                context.Token.ThrowIfCancellationRequested();

                // Update the progress information
                string message = String.Format("Loading {0}", section.Name);
                progress.Report(new TaskProgress(increment, true, message));

                // Only request that the archetypes for this section are loaded
                section.LoadStats(new StreamableStat[] { StreamableMapSectionStat.Archetypes, StreamableMapSectionStat.Entities });
            }
        }

        /// <summary>
        /// Gets called when the user turns off this overlay
        /// </summary>
        public override void Deactivated()
        {
            this.Geometry.Clear();
            base.Deactivated();
        }
        #endregion Methods
    }
}
