﻿using System;
using System.ComponentModel;

namespace Workbench.AddIn
{

    /// <summary>
    /// Workbench extension point GUID string constants.
    /// </summary>
    public static class ExtensionPoints
    {
        #region Extension Points

        /// <summary>
        /// 
        /// </summary>
        public const String StartupCommand =
            "73821EA6-0B08-4ACC-8C97-A4D0B23A8F5B";

        /// <summary>
        /// 
        /// </summary>
        public const String ShutdownCommand =
            "8FCF641D-B629-4DFB-AAD6-FFF2BF641598";

        /// <summary>
        /// 
        /// </summary>
        public const String Void =
            "6596B3CC-AA9A-421E-8DA2-AC7B54D2A290";

        /// <summary>
        /// 
        /// </summary>
        public const String ToolWindowProxy = "E2FC17B2-1DFE-4E79-AA10-2E456CBE2D99";
        
        /// <summary>
        /// Settings view model extension; ISettings interface.
        /// </summary>
        public const String Settings =
            "B27E8C7D-854B-4692-AD99-2BE63C5CA646";
        
        /// <summary>
        /// Resident service extensions.
        /// </summary>
        public const String ResidentService =
            "8AF76048-077B-4996-AAE6-3B0F86C3F097";

        /// <summary>
        /// Plugin information; IPluginDetails interface.
        /// </summary>
        public const String PluginInformation =
            "278C5136-8A38-45E4-BA95-FC0B4710D094";

        public const String LoggingOutput =
            "D7BCAB38-2059-4920-943B-89D131068DAF";

        public const String SearchableTags =
            "0A003A18-8ED8-4917-AED4-241F365F6BCC";

        #region File Services
        /// <summary>
        /// New service; INewService interface.
        /// </summary>
        public const String NewService =
            "76EF39C1-D90A-4339-8FAC-BE442C41B68D";

        /// <summary>
        /// Open service; IOpenService interface.
        /// </summary>
        public const String OpenService =
            "A9281C9B-7A43-4DB3-91D9-D13D5AAE68DB";

        /// <summary>
        /// Save service; ISaveService interface.
        /// </summary>
        public const String SaveService =
            "4D4B0ED4-BA76-43E2-A3C9-EAF47E701CCD";

        #endregion // File Services

        #region Menus
        /// <summary>
        /// Main menu item; IMenuItem interface.
        /// </summary>
        public const String MenuMain =
            "49D0F07C-B34A-4C1E-A269-64430C242BE7";

        /// <summary>
        /// File menu item; IMenuItem interface.
        /// </summary>
        public const String MenuFile =
            "842F4ED9-EC08-4A1F-8606-4E9F3ED03B9E";

        /// <summary>
        /// File recently used files; IMenuItem interface.
        /// </summary>
        public const String MenuFileRecentFiles =
            "563A5F98-E2E9-4A3F-89C9-B3A973C03A0E";

        /// <summary>
        /// Edit menu item; IMenuItem interface.
        /// </summary>
        public const String MenuEdit =
            "41B56958-FAB7-43DC-A778-C69C86441044";

        /// <summary>
        /// Tools menu item; IMenuItem interface.
        /// </summary>
        public const String MenuTools =
            "BC70920F-F689-4F28-AFC5-D643CFB9225E";
        
        /// <summary>
        /// Window menu item; IMenuItem interface.
        /// </summary>
        public const String MenuWindow =
            "F83B1B0E-E969-4C3B-8C12-4DA6380CD69C";

        /// <summary>
        /// Help menu item; IMenuItem interface.
        /// </summary>
        public const String MenuHelp =
            "D90FA3CE-0AF7-4B3C-B333-6CC5D87F8EFA";

        /// <summary>
        /// Find menu item; IMenuItem interface.
        /// </summary>
        public const String MenuFind =
            "3FFA0806-17D6-41FD-BE7F-E513D5A1C46F";

        /// <summary>
        /// Find menu item; IMenuItem interface.
        /// </summary>
        public const String OpenMenu =
            "81945480-02F2-4A70-A54E-7373EB936806";
        #endregion // Menus

        #region ToolBars

        public const String ToolbarTray =
            "40660ABF-3961-480A-AEFA-7A31EE8E47A2";

        /// <summary>
        /// Standard toolbar.
        /// </summary>
        public const String ToolbarStandard =
            "DBC17D93-1CE5-495F-8B91-A250C143761C";

        /// <summary>
        /// Custom resource for a toolbar
        /// </summary>
        public const String ToolbarResource =
            "83F30781-4EC0-4034-9567-DAD2135AEBAD";

        /// <summary>
        /// Data mode toolbar.
        /// </summary>
        public const String DataModeToolbar =
            "33B7A6BC-E641-4ECA-B5B5-D907024C0486";

        #endregion // ToolBars
        
        #endregion // Extension Points
    }
        
} // Workbench namespace
