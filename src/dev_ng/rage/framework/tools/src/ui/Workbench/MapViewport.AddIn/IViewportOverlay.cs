﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Collections;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using System.ComponentModel;
using System.Windows;
using RSG.Model.Report;
using RSG.Model.Common;

namespace Viewport.AddIn
{
    /// <summary>
    /// 
    /// </summary>
    public interface IViewportOverlay : IReport
    {
        #region Properties

        /// <summary>
        /// The geometry collection that the overlay is made up on
        /// </summary>
        ObservableCollection<Viewport2DGeometry> Geometry { get; }

        /// <summary>
        /// Represents whether this overlay for the viewport is currently the active overlay
        /// </summary>
        Boolean IsCurrentlyActive { get; }

        /// <summary>
        /// Flag indicating whether the overlay should be restored to be the open one the next time the workbench is reopened.
        /// </summary>
        Boolean RestoreIfOpen { get; }

        /// <summary>
        /// List of specific streamable stats that are required to generate this report.
        /// </summary>
        IEnumerable<StreamableStat> RequiredStats { get; }

        #endregion // Properties
        
        #region Functions

        /// <summary>
        /// Gets called when the user wants to view this overlay
        /// </summary>
        void Activated();

        /// <summary>
        /// Gets called when the user turns off this overlay
        /// </summary>
        void Deactivated();

        /// <summary>
        /// Gets the control that should be shown in the overlay details panel
        /// </summary>
        System.Windows.Controls.Control GetOverlayControl();

        /// <summary>
        /// Gets the control that shows information at the bottom right of the viewport.
        /// </summary>
        /// <returns></returns>
        System.Windows.Controls.Control GetViewportControl();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewPosition"></param>
        /// <param name="worldPosition"></param>
        void OnMouseMoved(Point viewPosition, Point worldPosition);

        #endregion // Virtual Functions
    } // IViewportOverlay
} // Viewport.AddIn
