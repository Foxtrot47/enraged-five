﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Workbench.AddIn.UI.Layout;

namespace Workbench.UI.Menu
{
    /// <summary>
    /// Interaction logic for SavedLayoutName.xaml
    /// </summary>
    public partial class SavedLayoutName : Window
    {
        public ILayoutManager LayoutManager
        {
            get;
            set;
        }

        public String LayoutName
        {
            get { return m_layoutName; }
            set { m_layoutName = value; }
        }
        private String m_layoutName = String.Empty;

        public SavedLayoutName()
        {
            InitializeComponent();
            this.TextInputBox.Focus();
            this.DataContext = this;
        }

        private void OnCancelClick(Object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void OnConfirmClick(Object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void OnLayoutNameChanged(Object sender, TextChangedEventArgs e)
        {
            this.Confirm.IsEnabled = true;
            if (LayoutName.Length == 0)
            {
                this.Confirm.IsEnabled = false;
                return;
            }
            foreach (String currentName in this.LayoutManager.SavedLayouts.Keys)
            {
                if (this.LayoutName == currentName)
                {
                    this.Confirm.IsEnabled = false;
                    return;
                }
            }
        }
    }
}
