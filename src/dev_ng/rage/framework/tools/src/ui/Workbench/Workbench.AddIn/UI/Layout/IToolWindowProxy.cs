﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn.Commands;

namespace Workbench.AddIn.UI.Layout
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class IToolWindowProxy : WorkbenchMenuItemBase
    {
        #region Properties

        /// <summary>
        /// The name of the tool window (FrameworkElement.Name)
        /// this is used in the serialisation/deserialisation of the
        /// docking manager
        /// </summary>
        public String Name
        {
            get;
            set;
        }
        
        /// <summary>
        /// A flag that determines if the tool window will be created when
        /// the menu item is created (i.e on compose) or dealyed to
        /// be created the the window is first visible
        /// </summary>
        public Boolean DelayCreation
        {
            get { return m_dealyCreation; }
            protected set { m_dealyCreation = value; }
        }
        private Boolean m_dealyCreation = true;

        /// <summary>
        /// Represents whether this tool window has been created
        /// yet or needs to be created
        /// </summary>
        public Boolean HasBeenCreated
        {
            get { return ToolWindow != null; }
        }

        /// <summary>
        /// A reference to the actual tool window, until this has been created
        /// it will be null. This should be the only instance of a tool
        /// window at all times, however this can be overriden in a plugin
        /// </summary>
        protected IToolWindowBase ToolWindow
        {
            get { return m_toolWindow; }
            set { m_toolWindow = value; }
        }
        protected IToolWindowBase m_toolWindow = null;

        #endregion Properties

        #region Public Functions

        /// <summary>
        /// This returns the tool window, if it hasn't been created yet
        /// this creates it
        /// </summary>
        public IToolWindowBase GetToolWindow()
        {
            if (this.ToolWindow == null)
            {
                IToolWindowBase toolWindow = null;
                Boolean creationResult = this.CreateToolWindow(out toolWindow);
                if (creationResult == true)
                {
                    this.ToolWindow = toolWindow;
                    if (this.ToolWindow != null)
                    {
                        if (this.ToolWindow.Name != this.Name)
                        {
                            System.Diagnostics.Debug.Assert(this.ToolWindow.Name == this.Name,
                                "The name of the tool window " + this.ToolWindow.Title + " doesn't match the name in the proxy to it");

                            this.ToolWindow.Name = this.Name;
                        }
                    }
                }
            }

            return this.ToolWindow;
        }

        /// <summary>
        /// A abstract function that is overriden by the plugin to create the
        /// actual instance of the tool window and pass it back as a out paramter
        /// </summary>
        protected abstract Boolean CreateToolWindow(out IToolWindowBase toolWindow);

        #endregion // Public Functions

        #region ICommand Implementation

        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
        }

        #endregion // ICommand Implementation
    } // IToolWindowProxy
}
