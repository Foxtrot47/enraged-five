﻿using System;
using System.ComponentModel.Composition;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using RSG.Base.Logging;
using RSG.Base.Windows;
using Workbench.AddIn;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.Services;

namespace Workbench.AddIn.Devstar
{
    /// <summary>
    /// Interaction logic for DevstarView.xaml
    /// </summary>
    public partial class DevstarView : IDocumentBase
    {
        #region Properties

        private DevstarViewModel ViewModel
        {
            get;
            set;
        }

        #endregion // Properties

        #region Constructors

        public DevstarView()
        {
            InitializeComponent();
            this.ViewModel = new DevstarViewModel();
            DataContext = this.ViewModel;
        }

        public DevstarView(string initialAddress)
        {
            InitializeComponent();
            this.ViewModel = new DevstarViewModel(initialAddress);
            DataContext = this.ViewModel;
        }

        #endregion // Constructor

        #region Private Methods

        #region Event Handlers

        /// <summary>
        /// 
        /// </summary>
        private void btnGo_Click(Object sender, RoutedEventArgs e)
        {
            Button btnGo = (sender as Button);
            Debug.Assert(null != btnGo, "Invalid sender object.");
            if (null == btnGo)
                return;

            if (this.ViewModel == null)
                return;

            this.ViewModel.Go();
        }

        /// <summary>
        /// 
        /// </summary>
        private void webBrowser_Navigated(Object sender, NavigationEventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        private void webBrowser_Navigating(Object sender, NavigatingCancelEventArgs e)
        {
            WebBrowser webBrowser = (sender as WebBrowser);
            Debug.Assert(null != webBrowser, "Invalid sender object.");
            if (null == webBrowser)
                return;

            if (this.ViewModel == null)
                return;

            this.ViewModel.IsNavigating = true;
            this.ViewModel.Address = e.Uri;
        }

        private void webBrowser_LoadCompleted(object sender, NavigationEventArgs e)
        {
            WebBrowser webBrowser = (sender as WebBrowser);
            Debug.Assert(null != webBrowser, "Invalid sender object.");
            if (null == webBrowser)
                return;

            if (this.ViewModel == null)
                return;

            this.ViewModel.IsNavigating = false;
        }

        #endregion // Event Handlers

        #endregion // Private Methods

    }
} // Workbench.AddIn.Devstar namespace
