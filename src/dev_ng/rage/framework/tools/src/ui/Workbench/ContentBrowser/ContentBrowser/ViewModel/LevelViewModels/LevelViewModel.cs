﻿using System;
using System.Windows;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using RSG.Model.Common;
using RSG.Model.GlobalTXD;
using RSG.Base.Configuration;
using System.IO;
using RSG.Model.Asset;

namespace ContentBrowser.ViewModel.LevelViewModels
{
    /// <summary>
    /// The view model for the RSG.Model.Common.Level.Level object
    /// </summary>
    public class LevelViewModel : ContainerViewModelBase, IAssetViewModel, IWeakEventListener
    {
        #region Fields
        private GlobalRoot global;
        #endregion

        #region Properties
        /// <summary>
        /// A reference to the types model that this
        /// view model is wrapping
        /// </summary>
        public new ILevel Model
        {
            get { return m_model; }
            set
            {
                SetPropertyValue(value, () => Model,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            if (m_model != null)
                            {
                                PropertyChangedEventManager.RemoveListener(this.Model, this, "MapHierarchy");
                                PropertyChangedEventManager.RemoveListener(this.Model, this, "Characters");
                                PropertyChangedEventManager.RemoveListener(this.Model, this, "Vehicles");
                                PropertyChangedEventManager.RemoveListener(this.Model, this, "Weapons");
                            }

                            m_model = (ILevel)newValue;

                            if (m_model != null)
                            {
                                PropertyChangedEventManager.AddListener(this.Model, this, "MapHierarchy");
                                PropertyChangedEventManager.AddListener(this.Model, this, "Characters");
                                PropertyChangedEventManager.AddListener(this.Model, this, "Vehicles");
                                PropertyChangedEventManager.AddListener(this.Model, this, "Weapons");
                            }
                        }
                ));
            }
        }
        private ILevel m_model = null;

        /// <summary>
        /// The name of the level, this needs to be unique
        /// throughout all the loaded levels
        /// </summary>
        public override String Name
        {
            get { return this.Model.Name; }
        }
        #endregion // Properties

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public LevelViewModel(ILevel level)
            : base(level)
        {
            this.Model = level;
        }

        #endregion // Constructor

        #region Methods
        internal ParentedState GetParentedState(TextureDictionary dictionary)
        {
            if (this.global == null)
            {
                IConfig config = ConfigFactory.CreateConfig();
                IBranch branch = config.Project.DefaultBranch;

                string filename = Path.Combine(branch.Assets, "maps", "ParentTxds.xml");
                string textures = Path.Combine(branch.Assets, "maps", "Textures");
                if (File.Exists(filename))
                {
                    this.global = new GlobalRoot(filename, textures, ValidationOptions.DontValidate);
                }
                else
                {
                    this.global = new GlobalRoot();
                }

                return new ParentedState(dictionary, false, false);
            }

            if (this.global == null)
            {
                return new ParentedState(dictionary, false, false);
            }

            bool parented = false;
            bool savingMemory = false;

            SourceTextureDictionary source = this.ContainsSourceDictionary(dictionary.Name, this.global);
            if (source != null)
            {
                parented = true;
                savingMemory = !source.IsSavingLessThanParent;
            }

            return new ParentedState(dictionary, parented, savingMemory);
        }

        public SourceTextureDictionary ContainsSourceDictionary(String name, IDictionaryContainer root)
        {
            if (root.CanAcceptSourceDictionaries == true)
            {
                foreach (SourceTextureDictionary sourceDictionary in root.SourceTextureDictionaries.Values)
                {
                    if (String.Compare(sourceDictionary.Name, name, true) == 0)
                    {
                        return sourceDictionary;
                    }
                }
            }

            if (root.CanAcceptGlobalDictionaries == true)
            {
                foreach (GlobalTextureDictionary globalDictionary in root.GlobalTextureDictionaries.Values)
                {
                    SourceTextureDictionary sourceDictionary = this.ContainsSourceDictionary(name, globalDictionary);
                    if (sourceDictionary != null)
                    {
                        return sourceDictionary;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// This gets called whenever the selection state for this view model
        /// has changed
        /// </summary>
        protected override void OnSelectionChanged(Boolean oldValue, Boolean newValue)
        {
            RefreshTreeViewModels();
            RefreshGridViewModels();
        }

        /// <summary>
        /// This gets called whenever the expansion state for this view model
        /// has changed
        /// </summary>
        protected override void OnExpansionChanged(Boolean oldValue, Boolean newValue)
        {
            RefreshTreeViewModels();
            RefreshGridViewModels();
        }

        /// <summary>
        /// 
        /// </summary>
        private void RefreshGridViewModels()
        {
            GridAssets.BeginUpdate();
            GridAssets.Clear();
            if (IsSelected)
            {
                if (Model.MapHierarchy != null)
                {
                    GridAssets.Add(ViewModelFactory.CreateGridViewModel(Model.MapHierarchy));
                }
                if (Model.Characters != null)
                {
                    GridAssets.Add(ViewModelFactory.CreateGridViewModel(Model.Characters));
                }
                if (Model.Vehicles != null)
                {
                    GridAssets.Add(ViewModelFactory.CreateGridViewModel(Model.Vehicles));
                }
                if (Model.Weapons != null)
                {
                    GridAssets.Add(ViewModelFactory.CreateGridViewModel(Model.Weapons));
                }
            }
            GridAssets.EndUpdate();
        }

        /// <summary>
        /// 
        /// </summary>
        private void RefreshGridViewModelsThreaded()
        {
            if (Application.Current != null)
            {
                Application.Current.Dispatcher.Invoke(
                    new Action
                    (
                        delegate()
                        {
                            RefreshGridViewModels();
                        }
                    ),
                    System.Windows.Threading.DispatcherPriority.ContextIdle
                );
            }
            else
            {
                RefreshGridViewModels();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void RefreshTreeViewModels()
        {
            AssetChildren.BeginUpdate();
            AssetChildren.Clear();
            if (IsExpanded)
            {
                if (Model.MapHierarchy != null)
                {
                    AssetChildren.Add(ViewModelFactory.CreateViewModel(Model.MapHierarchy));
                }
                if (Model.Characters != null)
                {
                    AssetChildren.Add(ViewModelFactory.CreateViewModel(Model.Characters));
                }
                if (Model.Vehicles != null)
                {
                    AssetChildren.Add(ViewModelFactory.CreateViewModel(Model.Vehicles));
                }
                if (Model.Weapons != null)
                {
                    AssetChildren.Add(ViewModelFactory.CreateViewModel(Model.Weapons));
                }
            }
            else
            {
                AssetChildren.Add(new AssetDummyViewModel());
            }
            AssetChildren.EndUpdate();
        }

        /// <summary>
        /// 
        /// </summary>
        private void RefreshTreeViewModelsThreaded()
        {
            if (Application.Current != null)
            {
                Application.Current.Dispatcher.Invoke(
                    new Action
                    (
                        delegate()
                        {
                            RefreshTreeViewModels();
                        }
                    ),
                    System.Windows.Threading.DispatcherPriority.ContextIdle
                );
            }
            else
            {
                RefreshTreeViewModels();
            }
        }
        #endregion

        #region IWeakEventListener Implementation
        /// <summary>
        /// Called when the contents of the container we are monitoring changes
        /// </summary>
        public virtual Boolean ReceiveWeakEvent(Type managerType, Object sender, EventArgs e)
        {
            if (managerType == typeof(PropertyChangedEventManager))
            {
                RefreshTreeViewModelsThreaded();
                RefreshGridViewModelsThreaded();
            }

            return true;
        }
        #endregion // IWeakEventListener

        #region Classes
        /// <summary>
        /// Describes the global texture parented state of a associated texture dictionary.
        /// </summary>
        internal class ParentedState
        {
            #region Fields
            /// <summary>
            /// The private field used for the <see cref="SavingMemory"/> property.
            /// </summary>
            private bool savingMemory;

            /// <summary>
            /// The private field used for the <see cref="Parented"/> property.
            /// </summary>
            private bool parented;

            /// <summary>
            /// The private field used for the <see cref="Dictionary"/> property.
            /// </summary>
            private TextureDictionary dictionary;
            #endregion

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="ParentedState"/> class.
            /// </summary>
            /// <param name="dictionary">
            /// The dictionary that the state is being returned on.
            /// </param>
            /// <param name="parented">
            /// A value indicating whether the associated dictionary is in the global texture
            /// dictionary tree view.
            /// </param>
            /// <param name="savingMemory">
            /// A value indicating whether the dictionary is curent saving memory or costing
            /// while being in the global texture dictionary tree view.
            /// </param>
            public ParentedState(TextureDictionary dictionary, bool parented, bool savingMemory)
            {
                this.parented = parented;
                this.savingMemory = savingMemory;
            }
            #endregion

            #region Properties
            /// <summary>
            /// Gets a value indicating whether the dictionary is curent saving memory or
            /// costing while being in the global texture dictionary tree view.
            /// </summary>
            public bool SavingMemory
            {
                get { return this.savingMemory; }
            }

            /// <summary>
            /// Gets a value indicating whether the associated dictionary is in the global
            /// texture dictionary tree view.
            /// </summary>
            public bool Parented
            {
                get { return this.parented; }
            }

            /// <summary>
            /// Gets the associated dictionary whose parented state is described by this
            /// object.
            /// </summary>
            public TextureDictionary Dictionary
            {
                get { return this.dictionary; }
            }
            #endregion
        }
        #endregion
    } // LevelViewModel
} // ContentBrowser.ViewModel.Level
