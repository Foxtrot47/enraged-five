﻿using System;
using System.ComponentModel.Composition;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn;
using Workbench.AddIn.Commands;
using Workbench.AddIn.Services;
using Workbench.AddIn.Services.File;
using Workbench.AddIn.UI.Layout;
using WidgetEditor.AddIn;

namespace Workbench.AddIn.Automation.UI
{

    /// <summary>
    /// Map Network Export Monitor Tool Window Proxy.
    /// </summary>
    [ExportExtension(ExtensionPoints.ToolWindowProxy, typeof(IToolWindowProxy))]
    class AutomationMonitorProxy
        : IToolWindowProxy
    {
        #region MEF Imports
        /// <summary>
        /// MEF Import for core services
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.CoreServicesProvider,
            typeof(ICoreServiceProvider))]
        private ICoreServiceProvider CoreServices { get; set; }

        /// <summary>
        /// MEF imports for Workbench Open services.
        /// </summary>
        [ImportManyExtension(ExtensionPoints.OpenService, typeof(IOpenService))]
        private IEnumerable<IOpenService> OpenServices { get; set; }

        /// <summary>
        /// MEF import for automation monitor settings
        /// </summary>
        [ImportExtension(Workbench.AddIn.Automation.AutomationCompositionPoints.AutomationMonitorSettings, typeof(IAutomationMonitorSettings))]
        public IAutomationMonitorSettings Settings { get; set; }
        #endregion // MEF Imports

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public AutomationMonitorProxy()
        {
            this.IsToggle = false;
            this.Name = Resources.Strings.AutomationMonitor_Name;
            this.Header = Resources.Strings.AutomationMonitor_Title;
            this.DelayCreation = true;
        }
        #endregion // Constructor(s)

        #region IToolWindowProxy Methods
        /// <summary>
        /// Create and initialise the tool window.
        /// </summary>
        protected override bool CreateToolWindow(out IToolWindowBase toolWindow)
        {
            IOpenService ulogOpenService = this.OpenServices.Where(os =>
                os.Filter.Contains("*.ulog")).FirstOrDefault();
            toolWindow = new AutomationMonitor(this.CoreServices, ulogOpenService, this.Settings);
            return true;
        }
        #endregion // IToolWindowProxy Methods
    }

} // Workbench.AddIn.Automation.UI namespace
