﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Report.Reports.GameStatsReports.Tests;
using RSG.Base.Editor.Command;

namespace Workbench.AddIn.GameStatistics.Reports.Automated.ViewModels
{
    /// <summary>
    /// 
    /// </summary>
    public class GpuSmokeTestViewModel : SmokeTestViewModelBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public IList<GpuSmokeTest.GpuInfo> LatestGpuResults
        {
            get { return m_latestGpuResults; }
            set
            {
                SetPropertyValue(value, () => this.LatestGpuResults,
                          new PropertySetDelegate(delegate(object newValue) { m_latestGpuResults = (IList<GpuSmokeTest.GpuInfo>)newValue; }));
            }
        }
        private IList<GpuSmokeTest.GpuInfo> m_latestGpuResults;

        /// <summary>
        /// 
        /// </summary>
        public IDictionary<GpuSmokeTest.GpuMetricKey, IList<GpuSmokeTest.GpuInfo>> HistoricalGpuStats
        {
            get { return m_historicalGpuResults; }
            set
            {
                SetPropertyValue(value, () => this.HistoricalGpuStats,
                          new PropertySetDelegate(delegate(object newValue) { m_historicalGpuResults = (IDictionary<GpuSmokeTest.GpuMetricKey, IList<GpuSmokeTest.GpuInfo>>)newValue; }));
            }
        }
        private IDictionary<GpuSmokeTest.GpuMetricKey, IList<GpuSmokeTest.GpuInfo>> m_historicalGpuResults;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public GpuSmokeTestViewModel(GpuSmokeTest smokeTest, string testName)
            : base(smokeTest, testName)
        {
            LatestGpuResults = smokeTest.GroupedLatestBuildStats.Where(item => item.Key == testName).Select(item => item.Value).FirstOrDefault();
            HistoricalGpuStats = smokeTest.GroupedHistoricalStats.Where(item => item.Key.TestName == testName).ToDictionary(item => item.Key, item => item.Value);
        }
        #endregion // Constructor(s)
    } // GpuSmokeTestViewModel
}
