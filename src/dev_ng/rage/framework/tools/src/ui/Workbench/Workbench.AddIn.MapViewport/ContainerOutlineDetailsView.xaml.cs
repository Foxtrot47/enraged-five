﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Workbench.AddIn.MapViewport.Overlays;
using System.ComponentModel;
using System.Collections.Specialized;

namespace Workbench.AddIn.MapViewport
{
    /// <summary>
    /// Interaction logic for ContainerOutlineDetailsView.xaml
    /// </summary>
    public partial class ContainerOutlineDetailsView : UserControl
    {
        public ContainerOutlineDetailsView()
        {
            this.DataContextChanged += OnDataContextChanged;
            InitializeComponent();
        }

        void OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            ContainerOutlineOverlay vm = e.NewValue as ContainerOutlineOverlay;
            if (vm == null)
                return;

            //vm.Filters.CollectionChanged += CollectionChanged;

            //CreateColumns(vm);
        }

        void CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            //CreateColumns(this.DataContext as ContainerOutlineOverlay);
        }

        //private void CreateColumns(ContainerOutlineOverlay vm)
        //{
        //    if (vm == null)
        //        return;

        //    this.List.Columns.Clear();
        //    DataGridTextColumn nameColumn = new DataGridTextColumn();
        //    nameColumn.Header = "Name";
        //    nameColumn.IsReadOnly = true;
        //    nameColumn.Binding = new Binding("Section.Name") { Mode = BindingMode.OneWay };
        //    this.List.Columns.Add(nameColumn);

        //    foreach (var filter in vm.Filters)
        //    {
        //        DataGridCheckBoxColumn filterColumn = new DataGridCheckBoxColumn();
        //        filterColumn.Header = filter.Name;
        //        filterColumn.IsReadOnly = true;
        //        filterColumn.Binding = new Binding(string.Format("[{0}]", filter.Name)) { Mode = BindingMode.OneWay };
        //        this.List.Columns.Add(filterColumn);
        //    }
        //}
    }
}
