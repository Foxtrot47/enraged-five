﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PerforceBrowser.AddIn;
using Workbench.AddIn;
using RSG.SourceControl.Perforce;
using System.ComponentModel.Composition;
using RSG.Base.Logging;
using RSG.Base.Configuration;
using RSG.Base.Windows.Dialogs;

namespace ContentBrowser.AddIn.PerforceBrowser
{
    /// <summary>
    /// Perforce service implementation.
    /// </summary>
    [ExportExtension(global::PerforceBrowser.AddIn.CompositionPoints.PerforceService,
        typeof(IPerforceService))]
    public class PerforceService :
        IPerforceService,
        IDisposable
    {
        #region MEF Imports
        /// <summary>
        /// MEF import for login service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LoginService, 
            typeof(Workbench.AddIn.Services.ILoginService))]
        private Lazy<Workbench.AddIn.Services.ILoginService> LoginService { get; set; }

        /// <summary>
        /// MEF import for Configuration Service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService,
            typeof(Workbench.AddIn.Services.IConfigurationService))]
        private Lazy<Workbench.AddIn.Services.IConfigurationService> ConfigService { get; set; }
        #endregion // MEF Imports

        #region Member Data
        private P4 m_connection;
        private bool m_enableLoginChecks;
        #endregion // Member Data

        #region Properties
        /// <summary>
        /// Valid perforce connection
        /// </summary>
        public P4 PerforceConnection
        {
            get
            {
                if (this.ConfigService.Value.Config.VersionControlEnabled == false)
                    return null;

                // If we don't have a valid connection, attempt to connect
                if (!m_connection.IsValidConnection(m_enableLoginChecks, true))
                {
                    if (ConnectToPerforce() == true)
                        return m_connection;

                    return null;
                }
                
                m_connection.Connect();
                
                // Return the connection object only if its valid
                return (m_connection.IsValidConnection(m_enableLoginChecks, true) ? m_connection : null);
            }
        }
        
        /// <summary>
        /// Enable the login checks temporarily.
        /// </summary>
        public bool EnableLoginChecks
        {
            get
            {
                return m_enableLoginChecks;
            }
            set
            {
                m_enableLoginChecks = value;
            }
        }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public PerforceService()
        {
            m_enableLoginChecks = false;
            m_connection = new P4();
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// Attempts to create a connection with perforce by using the login prompt for the user's details
        /// </summary>
        private bool ConnectToPerforce()
        {
            return LoginService.Value.Login(LoginCheck);
        }

        /// <summary>
        /// Method to use to check if the user was able to succesfully connect to p4.
        /// </summary>
        /// <param name="details"></param>
        /// <returns></returns>
        private LoginResults LoginCheck(LoginDetails details)
        {
            LoginResults results = new LoginResults();
            try
            {
                m_connection.Connect(details.Password);
                Log.Log__Message("Perforce connection successfully established.");
                results.Success = true;
            }
            catch (P4API.Exceptions.InvalidLogin)
            {
                results.Success = false;
                results.Message = "Login failed. Please check your login details.";
                Log.Log__Warning("Perforce login details were incorrect.");
            }
            catch (P4API.Exceptions.P4APIExceptions ex)
            {
                results.Success = false;
                results.Message = "Login failed. A generic perforce error occurred.";
                Log.Log__Exception(ex, "Unhandled Perforce exception");
            }
            return results;
        }
        #endregion

        #region IDisposable Implementation
        /// <summary>
        /// Disconnect from perforce
        /// </summary>
        public void Dispose()
        {
            m_connection.Disconnect();
        }
        #endregion
    }
}
