﻿using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using RSG.Base.Editor;
using Workbench.AddIn.Services;
using Workbench.AddIn.UI.Layout;
using System.Collections.Generic;
using RSG.UniversalLog;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System;
using System.Xml;
using RSG.UniversalLog.ViewModel;
using RSG.Base.Windows.Helpers;
using RSG.Base.Logging.Universal;
using System.Xml.Linq;
using System.Net.Mail;

namespace Workbench.AddIn.UniversalLog
{
    /// <summary>
    /// 
    /// </summary>
    public class UniversalLogViewModel : ViewModelBase
    {
        #region Fields
        private RelayCommand sendEmailCommand;
        private RelayCommand clearLogCommand;
        private KeyValuePair<string, IUniversalLogListener> m_selectedListener;
        private IConfigurationService m_config;
        private IWebBrowserService m_webBrowser;
        private ObservableCollection<UniversalLogComponentViewModel> m_messageCollection;
        private ObservableCollection<UniversalLogComponentViewModel> m_allMessageCollection;
        private bool showErrors;
        private bool showWarnings;
        private bool showMessages;
        private bool showDebugMessages;
        #endregion

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public RelayCommand SendEmailCommand
        {
            get
            {
                if (this.sendEmailCommand == null)
                {
                    this.sendEmailCommand = new RelayCommand(this.SendEmail);
                }

                return this.sendEmailCommand;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand ClearLogCommand
        {
            get
            {
                if (this.clearLogCommand == null)
                {
                    this.clearLogCommand = new RelayCommand(this.Clear);
                }

                return this.clearLogCommand;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, IUniversalLogListener> Listeners
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public KeyValuePair<string, IUniversalLogListener> SelectedListener
        {
            get { return m_selectedListener; }
            set
            {
                if (Object.ReferenceEquals(m_selectedListener, value))
                    return;

                m_selectedListener = value;
                OnSelectedListenerChanged();
            }
        }

        public bool ShowErrors
        {
            get
            {
                return this.showErrors;
            }

            set
            {
                if (this.showErrors == value)
                {
                    return;
                }

                this.showErrors = value;
                this.OnPropertyChanged("ShowErrors");
                this.OnFilterOptionChanged();
            }
        }

        public bool ShowWarnings
        {
            get
            {
                return this.showWarnings;
            }

            set
            {
                if (this.showWarnings == value)
                {
                    return;
                }

                this.showWarnings = value;
                this.OnPropertyChanged("ShowWarnings");
                this.OnFilterOptionChanged();
            }
        }

        public bool ShowMessages
        {
            get
            {
                return this.showMessages;
            }

            set
            {
                if (this.showMessages == value)
                {
                    return;
                }

                this.showMessages = value;
                this.OnPropertyChanged("ShowMessages");
                this.OnFilterOptionChanged();
            }
        }

        public bool ShowDebugMessages
        {
            get
            {
                return this.showDebugMessages;
            }

            set
            {
                if (this.showDebugMessages == value)
                {
                    return;
                }

                this.showDebugMessages = value;
                this.OnPropertyChanged("ShowDebugMessages");
                this.OnFilterOptionChanged();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<UniversalLogComponentViewModel> MessageCollection
        {
            get { return m_messageCollection; }
            set { m_messageCollection = value; }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="config"></param>
        /// <param name="webBrowser"></param>
        public UniversalLogViewModel(string filename, IConfigurationService config, IWebBrowserService webBrowser)
        {
            this.m_config = config;
            this.m_webBrowser = webBrowser;
            this.m_allMessageCollection = new ObservableCollection<UniversalLogComponentViewModel>();
            this.MessageCollection = new ObservableCollection<UniversalLogComponentViewModel>();
            this.SelectedListener = new KeyValuePair<string, IUniversalLogListener>(filename, new UniversalLogFileListener(filename));
            this.ShowErrors = true;
            this.ShowMessages = true;
            this.ShowWarnings = true;
        }
        #endregion // Constructor

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        public void Clear(object parameter)
        {
            this.m_allMessageCollection.Clear();
            this.MessageCollection.Clear();
            if (this.SelectedListener.Value != null)
            {
                this.SelectedListener.Value.Clear();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void SendEmail(object parameter)
        {
            string[] receivers = this.m_config.GameConfig.ConfigCoreView.GetLogMailAddresses();
            if (this.SelectedListener.Value != null)
            {
                List<Attachment> att = new List<Attachment>();

                string messagebody = "See attachment\n\n\n";
                string subject = "Universal Log Report from " + Environment.UserName;
                using (MemoryStream stream = new MemoryStream())
                {
                    using (XmlWriter writer = XmlWriter.Create(stream))
                    {
                        writer.WriteStartDocument(true);
                        writer.WriteStartElement("ulog");
                        writer.WriteAttributeString("version", "1.0");
                        writer.WriteAttributeString("name", Environment.UserName);
                        writer.WriteAttributeString("machine", Environment.MachineName);

                        this.SelectedListener.Value.Serialise(writer);

                        writer.WriteEndElement();
                        writer.WriteEndDocument();
                    }

                    stream.Position = 0;
                    att.Add(new Attachment(stream, "WorkbenchULogReport.ulog"));
                    RSG.UniversalLog.Util.Email.SendEmail(m_config.Config, receivers, subject, messagebody, att);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnUniversalBufferChanged(object sender, UniversalBufferChangedEventArgs e)
        {
            if (!ReferenceEquals(sender, this.SelectedListener.Value))
            {
                return;
            }

            if (Application.Current != null)
            {
                Application.Current.Dispatcher.BeginInvoke(
                    new Action(
                        delegate
                        {
                            if (e.Added)
                            {
                                foreach (var data in e.Data)
                                {
                                    var newVm = UniversalLogComponentViewModel.Create(data);
                                    this.m_allMessageCollection.Add(newVm);
                                    if (this.IsValid(newVm))
                                    {
                                        this.MessageCollection.Add(newVm);
                                    }
                                }
                            }
                        }
                    ), System.Windows.Threading.DispatcherPriority.ApplicationIdle,
                    null
                    );
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void OnSelectedListenerChanged()
        {
            if (this.SelectedListener.Value == null)
            {
                this.MessageCollection.Clear();
                return;
            }

            this.m_allMessageCollection.Clear();
            foreach (var message in this.SelectedListener.Value.Buffer)
            {
                this.m_allMessageCollection.Add(UniversalLogComponentViewModel.Create(message));
            }

            this.FilterMessageCollection(true);
        }

        /// <summary>
        /// 
        /// </summary>
        private void OnFilterOptionChanged()
        {
            this.FilterMessageCollection(false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        internal void Save(string filename)
        {
            ICollection<UniversalLogFileBufferedMessage> collection = new Collection<UniversalLogFileBufferedMessage>(
                    (from m in this.m_allMessageCollection select m.Data as UniversalLogFileBufferedMessage).ToList());

            XDocument document = UniversalLogFile.TransformToXDocument(collection, null);
            document.Save(filename);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="clear"></param>
        private void FilterMessageCollection(bool clear)
        {
            if (clear)
            {
                this.MessageCollection.Clear();
            }

            int validCount = 0;
            foreach (var message in this.m_allMessageCollection)
            {
                bool valid = this.IsValid(message);
                if (valid && !this.MessageCollection.Contains(message))
                {
                    this.MessageCollection.Insert(validCount, message);
                }
                else if (!valid && this.MessageCollection.Contains(message))
                {
                    this.MessageCollection.Remove(message);
                }

                if (valid)
                {
                    validCount++;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        private bool IsValid(UniversalLogComponentViewModel viewModel)
        {
            if (viewModel.ComponentType == UniversalLogComponentTypes.Profiling)
            {
                return false;
            }

            if (viewModel.ComponentType == UniversalLogComponentTypes.Error && this.ShowErrors == false)
            {
                return false;
            }

            if (viewModel.ComponentType == UniversalLogComponentTypes.Warning && this.ShowWarnings == false)
            {
                return false;
            }

            if (viewModel.ComponentType == UniversalLogComponentTypes.DebugMessage && this.ShowDebugMessages == false)
            {
                return false;
            }

            if (viewModel.ComponentType == UniversalLogComponentTypes.Message && this.ShowMessages == false)
            {
                return false;
            }

            return true;
        }
        #endregion
    } // UniversalLogViewModel
} // Workbench.AddIn.UniversalLog
