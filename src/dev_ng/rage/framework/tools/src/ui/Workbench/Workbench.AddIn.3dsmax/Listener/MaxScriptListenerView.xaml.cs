﻿using System;
using System.Windows;
using System.Windows.Data;
using System.Windows.Controls;
using Workbench.AddIn;
using Workbench.AddIn.Commands;
using Workbench.AddIn.UI.Layout;

namespace Workbench.AddIn._3dsmax.Listener
{
    /// <summary>
    /// MaxScript Listener Code-behind.
    /// </summary>
    public partial class MaxScriptListenerView : 
        ToolWindowBase<MaxScriptListenerViewModel>
    {
        #region Constants
        public static readonly Guid GUID = new Guid("E9C52956-B52C-4C3B-8675-6CDF373B0CA7");
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public MaxScriptListenerView()
            : base(Workbench.AddIn._3dsmax.Resources.Strings.Listener_Name, new MaxScriptListenerViewModel())
        {
            InitializeComponent();

            this.ID = GUID;
        }
        #endregion // Constructor(s)

        #region Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RefreshButton_Click(Object sender, RoutedEventArgs e)
        {
            this.ViewModel.RefreshMaxInstances();
        }
        #endregion // Event Handlers
    }

} // Workbench.AddIn._3dsmax.Listener namespace
