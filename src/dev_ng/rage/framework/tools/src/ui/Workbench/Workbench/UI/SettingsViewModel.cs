﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using System.Windows;
using Workbench.UI.Menu;
using Workbench.AddIn.Services;
using RSG.Base.Collections;
using System.Reflection;
using System.ComponentModel;

namespace Workbench.UI
{
    public class PreferencesViewModel : ViewModelBase
    {
        #region Members

        private RelayCommand m_cancelCommand;
        private RelayCommand m_saveCommand;
        private Window m_windowView;

        public ISettings SelectedService { get; set; }

        #endregion // Members

        #region Properties

        public ObservableCollection<ISettings> PreferenceServices { get; set; }

        #endregion // Properties

        #region Constructor

        public PreferencesViewModel( Window window, IEnumerable<ISettings> services )
        {
            m_windowView = window;

            PreferenceServices = new ObservableCollection<ISettings>();
            PreferenceServices.BeginUpdate();

            // Load all the settings, but only add those to the observable collection that have a
            // configurable property.
            foreach (ISettings item in services)
            {
                item.Load();

                if (HasDisplayableProperties(item))
                {
                    PreferenceServices.Add(item);
                }
            }

            PreferenceServices.EndUpdate();
            SelectedService = PreferenceServices.FirstOrDefault();
        }

        #endregion // Constructor

        #region Private Methods
        /// <summary>
        /// Returns whether the passed in object has any properties that are browsable true.
        /// </summary>
        /// <param name="settings"></param>
        /// <returns></returns>
        private bool HasDisplayableProperties(ISettings settings)
        {
            bool hasDisplayableProps = false;

            Type type = settings.GetType();
            PropertyInfo[] pInfos = type.GetProperties();
            foreach (PropertyInfo pInfo in pInfos)
            {
                BrowsableAttribute[] atts = (BrowsableAttribute[])pInfo.GetCustomAttributes(typeof(BrowsableAttribute), true);
                if (!atts.Any() || atts.First().Browsable)
                {
                    hasDisplayableProps = true;
                    break;
                }
            }

            return hasDisplayableProps;
        }
        #endregion // Private Methods

        #region Commands

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (m_cancelCommand == null)
                {
                    m_cancelCommand = new RelayCommand(param => this.Cancel(param), param => this.CanCancel(param));
                }
                return m_cancelCommand;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand SaveCommand
        {
            get
            {
                if (m_saveCommand == null)
                {
                    m_saveCommand = new RelayCommand(param => this.Save(param), param => this.CanSave(param));
                }
                return m_saveCommand;
            }
        }


        #endregion // Commands

        #region Command Callbacks

        /// <summary>
        /// 
        /// </summary>
        private Boolean CanCancel(Object param)
        {
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        private void Cancel(Object param)
        {
            this.m_windowView.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        private Boolean CanSave(Object param)
        {
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        private void Save(Object param)
        {
            foreach ( var item in PreferenceServices )
            {
                List<string> errors = new List<string>();
                if ( !item.Validate(errors) )
                {
                    string msg = "There were some invalid preferences for: " + item.Title;
                    msg += Environment.NewLine + Environment.NewLine;
                    msg += errors.Aggregate( ( i, j ) => i + Environment.NewLine + "* " + j );
                    MessageBox.Show( msg );
                    return;
                }
            }

            foreach ( var item in PreferenceServices )
            {
                item.Apply();
            }

            this.m_windowView.Close();
        }


        #endregion // Command Callbacks
    }

    /// <summary>
    /// A command whose sole purpose is to  relay its functionality 
    /// to other objects by invoking delegates. The default return 
    /// value for the CanExecute method is 'true'.
    /// </summary>
    public class RelayCommand : System.Windows.Input.ICommand
    {
        #region Fields

        private readonly Action<Object> m_execute;
        private readonly Predicate<Object> m_canExecute;

        #endregion // Fields

        #region Constructors

        /// <summary>
        /// Creates a new command that can always execute.
        /// </summary>
        public RelayCommand(Action<Object> execute)
            : this(execute, null)
        {
        }

        /// <summary>
        /// Creates a new command.
        /// </summary>
        public RelayCommand(Action<Object> execute, Predicate<Object> canExecute)
        {
            if (execute == null)
                throw new ArgumentNullException("execute");

            m_execute = execute;
            m_canExecute = canExecute;
        }

        #endregion // Constructors

        #region ICommand Members

        /// <summary>
        /// Gets called when the manager is deciding if this
        /// command can be executed. This returns true be default
        /// </summary>
        public bool CanExecute(Object parameter)
        {
            if (m_canExecute != null)
                return m_canExecute(parameter);

            return true;
        }

        /// <summary>
        /// Gets called when the command is executed. This just
        /// relays the execution to the delegate function
        /// </summary>
        public void Execute(Object parameter)
        {
            if (m_execute != null)
                m_execute(parameter);
        }

        /// <summary>
        /// Makes sure that this command is hooked up into the
        /// command manager
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add { System.Windows.Input.CommandManager.RequerySuggested += value; }
            remove { System.Windows.Input.CommandManager.RequerySuggested -= value; }
        }

        #endregion // ICommand Members
    } // RelayCommand
}
