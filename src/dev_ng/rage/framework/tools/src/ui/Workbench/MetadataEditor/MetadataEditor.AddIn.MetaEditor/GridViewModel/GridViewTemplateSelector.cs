﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows;

namespace MetadataEditor.AddIn.MetaEditor.GridViewModel
{
    /// <summary>
    /// Grid view selector to choose when to use the inner structure version of the StructureTunable view model, or to just use 
    /// the default (toggleable) view model.
    /// </summary>
    public class GridViewTemplateSelector : DataTemplateSelector
    {
        #region Private member fields

        private ResourceDictionary m_resources;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        public GridViewTemplateSelector()
        {
            m_resources = new ResourceDictionary();
            m_resources.Source = new Uri(@"pack://application:,,,/MetadataEditor.AddIn.MetaEditor;component/View/GridViewEditorResources.xaml");
        }

        #endregion

        #region Public overrides

        /// <summary>
        /// Select the appropriate template for the item. If the item is a StructureTunableViewModel and it has a parent, we choose the InnerStructure
        /// data template (See GridViewEditorResources.xaml) otherwise, we return the default data template for the item which is usually null.
        /// </summary>
        /// <param name="item">Item to choose the data template.</param>
        /// <param name="container">Container.</param>
        /// <returns>The data template. Can be null.</returns>
        public override System.Windows.DataTemplate SelectTemplate(object item, System.Windows.DependencyObject container)
        {
            DataTemplate dataTemplate = null;

            if (item is StructureTunableViewModel)
            {
                var viewModel = item as StructureTunableViewModel;
                if (viewModel.Parent != null)
                {
                    dataTemplate = m_resources["InnerStructure"] as DataTemplate;
                }
            }

            if (dataTemplate == null)
            {
                dataTemplate = base.SelectTemplate(item, container);
            }

            return dataTemplate;

        }

        #endregion
    }
}
