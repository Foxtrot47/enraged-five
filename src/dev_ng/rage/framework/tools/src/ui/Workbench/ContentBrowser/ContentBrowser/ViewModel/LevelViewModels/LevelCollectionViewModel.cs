﻿using System;
using System.Windows;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using RSG.Model.Asset.Level;

namespace ContentBrowser.ViewModel.LevelViewModels
{
    /*
    /// <summary>
    /// The view model for the RSG.Model.Common.Level.LevelDictionary object
    /// </summary>
    public class LevelCollectionViewModel : ModelBase, IWeakEventListener
    {
        #region Members

        private LevelCollection m_model = null;
        private ObservableCollection<LevelViewModel> m_levels;

        #endregion // Members

        #region Properties

        /// <summary>
        /// A reference to the types model that this
        /// view model is wrapping
        /// </summary>
        public LevelCollection Model
        {
            get { return m_model; }
            set
            {
                if (m_model == value)
                    return;

                SetPropertyValue(value, () => Model,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_model = (LevelCollection)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The collection of levels that are currently
        /// loaded in this dictionary
        /// </summary>
        public ObservableCollection<LevelViewModel> Levels
        {
            get { return m_levels; }
            set
            {
                if (m_levels == value)
                    return;

                SetPropertyValue(value, () => Levels,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_levels = (ObservableCollection<LevelViewModel>)newValue;
                        }
                ));
            }
        }

        #endregion // Properties
        
        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public LevelCollectionViewModel(LevelCollection model)
        {
            this.Model = model;
            this.Levels = new ObservableCollection<LevelViewModel>();

            CollectionChangedEventManager.AddListener(this.Model.Levels, this);
            PropertyChangedEventManager.AddListener(this.Model, this, "Levels");

            this.SyncLevelsToModel();
        }

        #endregion // Constructor

        #region Private Methods

        /// <summary>
        /// Makes sure that the level viewmodel collection is in sync
        /// with the model collection
        /// </summary>
        private void SyncLevelsToModel()
        {
            this.Levels.Clear();
            foreach (Level level in this.Model.Levels)
            {
                this.Levels.Add(new LevelViewModel(level));
            }
        }

        #endregion // Private Methods

        #region IWeakEventListener Implementation

        /// <summary>
        /// Listens to the model, making sure that the collections stay in sync
        /// </summary>
        public Boolean ReceiveWeakEvent(Type managerType, Object sender, EventArgs e)
        {
            if (managerType == typeof(CollectionChangedEventManager))
            {
                SyncLevelsToModel();
            }
            else if (managerType == typeof(PropertyChangedEventManager))
            {
                CollectionChangedEventManager.AddListener(this.Model.Levels, this);
                SyncLevelsToModel();
            }

            return true;
        }

        #endregion // IWeakEventListener Implementation
    } // LevelDictionaryViewModel
    */
} // ContentBrowser.ViewModel.LevelViewModel
