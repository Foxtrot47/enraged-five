﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Workbench.AddIn.UI.ToolBar;
using Workbench.AddIn;

namespace ContentBrowser.View
{
    /// <summary>
    /// Interaction logic for ToolbarResources.xaml
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.ToolbarResource, typeof(ResourceDictionary))] 
    public partial class ToolbarResources : ResourceDictionary
    {
        public ToolbarResources()
        {
            InitializeComponent();
        }
    }
}
