﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Collections;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using System.ComponentModel;

namespace MetadataEditor.AddIn.KinoEditor.Cutscene_Event_Def_Editor
{
    public class EventCollection : ObservableCollection<Event>
    {
        protected override void InsertItem(int index, Event item)
        {
            base.InsertItem(index, item);
        }

        protected override void RemoveItem(int index)
        {
            base.RemoveItem(index);
        }

        protected override void SetItem(int index, Event item)
        {
            base.SetItem(index, item);
        }
    }

    public class OppositeEventType
    {
        public OppositeEventType(string name, int id)
        {
            Name = name;
            TypeID = id;
        }

        public int TypeID { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }

    public class Event : RSG.Base.Editor.ModelBase
    {
        public Event()
        {
            Rank = "255";
        }

        public string EventName
        {
            get
            {
                return _EventName;
            }

            set
            {
                SetPropertyValue(value, () => this.EventName,
                    new PropertySetDelegate(delegate(Object newValue) { _EventName = (string)newValue; }));
                FullEventName = "CUTSCENE_" + ((string)value).ToUpper().Replace(" ", "_") + "_EVENT";
            }
        }
        
        public string FullEventName 
        {
            get
            {
                return _FullEventName;
            }
            set
            {
                SetPropertyValue(value, () => this.FullEventName,
                   new PropertySetDelegate(delegate(Object newValue) { _FullEventName = (string)newValue; }));
            }
        }

        public string Rank 
        {
            get
            {
                return _Rank;
            }
            set
            {
                SetPropertyValue(value, () => this.Rank,
                    new PropertySetDelegate(delegate(Object newValue) { _Rank = (string)newValue; }));
            }
        }

        public Event OppositeEvent 
        {
            get
            {
                return _OppositeEvent;
            }
            set
            {
                SetPropertyValue(value, () => this.OppositeEvent,
                    new PropertySetDelegate(delegate(Object newValue) { _OppositeEvent = (Event)newValue; }));
            }
        }

        public EventArg AssociatedEventArg 
        {
            get
            {
                return _AssociatedEventArg;
            }
            set
            {
                SetPropertyValue(value, () => this.AssociatedEventArg,
                    new PropertySetDelegate(delegate(Object newValue) { _AssociatedEventArg = (EventArg)newValue; }));
            }
        }

        private string _EventName = String.Empty;
        private string _FullEventName = String.Empty;
        private string _Rank = String.Empty;
        private Event _OppositeEvent = null;
        private EventArg _AssociatedEventArg = null;


        public override string ToString()
        {
            return FullEventName;
        }
    }
}
