﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Workbench.AddIn.MapStatistics.Overlays;
using System.Reflection;
//using RSG.Model.Map.Statistics;

namespace Workbench.AddIn.MapStatistics
{
    /// <summary>
    /// Interaction logic for OverlayDetailsView.xaml
    /// </summary>
    public partial class OverlayDetailsView : UserControl
    {
        public OverlayDetailsView()
        {
            InitializeComponent();
        }
    }
    
    public class StatValueConverter : IValueConverter
    {
        #region IValueConverter Members

        /// <summary>
        /// 
        /// </summary>
        public Object Convert(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            if (parameter is String && value is SectionStatViewModelBase)
            {
                SectionStatViewModelBase statVm = (SectionStatViewModelBase)value;

                double? statValue = 0.0;
                statValue = this.GetStatValue((parameter as String), statVm);
                if (statValue.HasValue == false)
                {
                    return null;
                }

                String convertedValue = String.Empty;
                switch (statVm.StatisticType)
                {
                    case SectionStatisticType.Size:
                        convertedValue = StatisticsUtil.SizeToString((ulong)statValue);
                        break;

                    case SectionStatisticType.Double:
                        convertedValue = String.Format("{0:0.0000}", (double)statValue);
                        break;

                    case SectionStatisticType.Percentage:
                        convertedValue = String.Format("{0:0.00%}", (double)statValue * 0.01);
                        break;

                    case SectionStatisticType.Integer:
                        convertedValue = String.Format("{0}", (int)statValue);
                        break;

                    case SectionStatisticType.Distance:
                        convertedValue = String.Format("{0:0m}", (double)statValue);
                        break;

                    default:
                        break;
                }

                return convertedValue;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Converts a Color to a unsigned integer value.
        /// </summary>
        public Object ConvertBack(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the statistic for the specific statistic class and parameter name
        /// </summary>
        private double? GetStatValue(String parameter, SectionStatViewModelBase statistic)
        {
            double? statValue = 0.0;

            PropertyInfo propInfo = statistic.GetType().GetProperty(String.Format("{0}Statistic", parameter));
            if (propInfo != null)
            {
                statValue = System.Convert.ToDouble(propInfo.GetValue(statistic, null));
            }
            return statValue;
            /*
            if ((parameter as String) == "Total")
            {
                if (statistic is DensityStatisticBase)
                {
                    statValue = (statistic as DensityStatisticBase).TotalStatistic;
                }
                else if (statistic is RatioStatisticBase)
                {
                    statValue = (statistic as RatioStatisticBase).TotalStatistic;
                }
                else if (statistic is DistanceStatisticBase)
                {
                    statValue = (statistic as DistanceStatisticBase).TotalStatistic;
                }
                else
                {
                    statValue = (statistic as MapSectionStatistic).TotalStatistic;
                }
            }
            else if ((parameter as String) == "HighDynamic")
            {
                if (statistic is DensityStatisticBase)
                {
                    statValue = (statistic as DensityStatisticBase).HighDynamicStatistic;
                }
                else if (statistic is RatioStatisticBase)
                {
                    statValue = (statistic as RatioStatisticBase).HighDynamicStatistic;
                }
                else if (statistic is DistanceStatisticBase)
                {
                    statValue = (statistic as DistanceStatisticBase).HighDynamicStatistic;
                }
                else
                {
                    statValue = (statistic as MapSectionStatistic).HighDynamicStatistic;
                }
            }
            else if ((parameter as String) == "HighNonDynamic")
            {
                if (statistic is DensityStatisticBase)
                {
                    statValue = (statistic as DensityStatisticBase).HighNonDynamicStatistic;
                }
                else if (statistic is RatioStatisticBase)
                {
                    statValue = (statistic as RatioStatisticBase).HighNonDynamicStatistic;
                }
                else if (statistic is DistanceStatisticBase)
                {
                    statValue = (statistic as DistanceStatisticBase).HighNonDynamicStatistic;
                }
                else
                {
                    statValue = (statistic as MapSectionStatistic).HighNonDynamicStatistic;
                }
            }
            else if ((parameter as String) == "LodDynamic")
            {
                if (statistic is DensityStatisticBase)
                {
                    statValue = (statistic as DensityStatisticBase).LodDynamicStatistic;
                }
                else if (statistic is RatioStatisticBase)
                {
                    statValue = (statistic as RatioStatisticBase).LodDynamicStatistic;
                }
                else if (statistic is DistanceStatisticBase)
                {
                    statValue = (statistic as DistanceStatisticBase).LodDynamicStatistic;
                }
                else
                {
                    statValue = (statistic as MapSectionStatistic).LodDynamicStatistic;
                }
            }
            else if ((parameter as String) == "LodNonDynamic")
            {
                if (statistic is DensityStatisticBase)
                {
                    statValue = (statistic as DensityStatisticBase).LodNonDynamicStatistic;
                }
                else if (statistic is RatioStatisticBase)
                {
                    statValue = (statistic as RatioStatisticBase).LodNonDynamicStatistic;
                }
                else if (statistic is DistanceStatisticBase)
                {
                    statValue = (statistic as DistanceStatisticBase).LodNonDynamicStatistic;
                }
                else
                {
                    statValue = (statistic as MapSectionStatistic).LodNonDynamicStatistic;
                }
            }
            else if ((parameter as String) == "SlodDynamic")
            {
                if (statistic is DensityStatisticBase)
                {
                    statValue = (statistic as DensityStatisticBase).SlodDynamicStatistic;
                }
                else if (statistic is RatioStatisticBase)
                {
                    statValue = (statistic as RatioStatisticBase).SlodDynamicStatistic;
                }
                else if (statistic is DistanceStatisticBase)
                {
                    statValue = (statistic as DistanceStatisticBase).SlodDynamicStatistic;
                }
                else
                {
                    statValue = (statistic as MapSectionStatistic).SlodDynamicStatistic;
                }
            }
            else if ((parameter as String) == "SlodNonDynamic")
            {
                if (statistic is DensityStatisticBase)
                {
                    statValue = (statistic as DensityStatisticBase).SlodNonDynamicStatistic;
                }
                else if (statistic is RatioStatisticBase)
                {
                    statValue = (statistic as RatioStatisticBase).SlodNonDynamicStatistic;
                }
                else if (statistic is DistanceStatisticBase)
                {
                    statValue = (statistic as DistanceStatisticBase).SlodNonDynamicStatistic;
                }
                else
                {
                    statValue = (statistic as MapSectionStatistic).SlodNonDynamicStatistic;
                }
            }
            return statValue;
            */
        }

        #endregion
    } // StatValueConverter
    /*
    public class CombinedStatisticsConverter : IMultiValueConverter
    {
        #region IMultiValueConverter Members

        public Object Convert(Object[] values, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            RawSectionStatistics stats = null;
            float area = 0.0f;
            List<MapSectionStatistic> statistics = new List<MapSectionStatistic>();
            if (values.Where(i => i is RSG.Model.Map.MapSection == true).Count() == 1)
            {
                var section = (from v in values.OfType<RSG.Model.Map.MapSection>()
                               select v).FirstOrDefault();
                if (section == null)
                    return statistics;

                area = section.Area;
                stats = section.RawSectionStatistics;
            }
            else
            {
                var sections = (from s in values.OfType<RSG.Model.Map.MapSection>()
                               select s).ToList();

                stats = new RawSectionStatistics(sections);
                foreach (var section in sections)
                {
                    stats.TopLevelCollision += stats.TopLevelCollision;
                    stats.TopLevelMoverCollision += stats.TopLevelMoverCollision;
                    stats.TopLevelWeaponCollision += stats.TopLevelWeaponCollision;
                    stats.TopLevelCameraCollision += stats.TopLevelCameraCollision;
                    stats.TopLevelRiverCollision += stats.TopLevelRiverCollision;
                }
                if (sections.Count > 0)
                {
                    area = (from s in values.OfType<RSG.Model.Map.MapSection>()
                            select s.Area).Max();
                }
            }

            //statistics.Add(new TotalCostStatistic(section.RawSectionStatistics));
            //statistics.Add(new GeometryCostStatistic(section.RawSectionStatistics));
            //statistics.Add(new TextureDictionaryCostStatistic(section.RawSectionStatistics));
            statistics.Add(new DrawableCountStatistic(stats));
            statistics.Add(new DrawableInstanceCountStatistic(stats));
            statistics.Add(new NonDrawableInstanceCountStatistic(stats));
            statistics.Add(new InteriorInstanceCountStatistic(stats));
            statistics.Add(new TxdCountStatistic(stats));
            statistics.Add(new ShaderCountStatistic(stats));
            statistics.Add(new PolygonCountStatistic(stats));
            statistics.Add(new CollisionPolygonCountStatistic(stats));
            statistics.Add(new MoverPolygonCountStatistic(stats));
            statistics.Add(new ShooterPolygonCountStatistic(stats));
            statistics.Add(new CollisionPolygonRatioStatistic(stats));
            statistics.Add(new MoverShooterPolygonRatioStatistic(stats));
            statistics.Add(new DrawableDensityStatistic(stats, area));
            statistics.Add(new DrawableInstanceDensityStatistic(stats, area));
            statistics.Add(new NonDrawableDensityStatistic(stats, area));
            statistics.Add(new TxdDensityStatistic(stats, area));
            statistics.Add(new ShaderDensityStatistic(stats, area));
            statistics.Add(new PolygonDensityStatistic(stats, area));
            statistics.Add(new CollisionPolygonDensityStatistic(stats, area));
            statistics.Add(new LodDistanceStatistic(stats));

            return statistics;
        }

        public Object[] ConvertBack(Object value, Type[] targetTypes, Object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
        
        #endregion // IMultiValueConverter Members
    } // CombinedStatisticsConverter
    */
    public class CombinedStatisticsContentConverter : IMultiValueConverter
    {
        #region IMultiValueConverter Members

        public Object Convert(Object[] values, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            if (values.Length == 2)
                return String.Format("Show Stats Table For Combined ({0} and {1})", values[0], values[1]);

            return String.Empty;
        }

        public Object[] ConvertBack(Object value, Type[] targetTypes, Object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion // IMultiValueConverter Members
    } // CombinedStatisticsConverter

    /// <summary>
    /// From:
    /// http://stackoverflow.com/questions/397556/how-to-bind-radiobuttons-to-an-enum
    /// </summary>
    public class EnumToBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value.Equals(parameter);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value.Equals(true) ? parameter : Binding.DoNothing;
        }
    }
} // Workbench.AddIn.MapStatistics
