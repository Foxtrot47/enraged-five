﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace Workbench.Services
{

    /// <summary>
    /// Recent files service.
    /// </summary>
    /// This is an internally used Workbench service; it is not exposed using
    /// MEF because its only the Open Service that utilises it.
    internal static class RecentFileService
    {
        /// <summary>
        /// Recently used file object.
        /// </summary>
        public class RecentlyUsedFile
        {
            public String Filename { get; private set; }
            public Guid OpenService { get; private set; }

            /// <summary>
            /// Constructor.
            /// </summary>
            /// <param name="filename"></param>
            /// <param name="os"></param>
            public RecentlyUsedFile(String filename, Guid os)
            {
                Filename = (filename.Clone() as String);
                OpenService = os;
            }
        }

        #region Static Controller Methods

        /// <summary>
        /// Deserialise the MRU list from the settings String.
        /// </summary>
        /// <returns></returns>
        public static RecentlyUsedFile[] GetFiles()
        {
            List<RecentlyUsedFile> mru = new List<RecentlyUsedFile>();
            String[] parts = Properties.Settings.Default.RecentlyUsedFiles.Split(
                new String[] { MRU_ENTRY_SEPARATOR }, StringSplitOptions.RemoveEmptyEntries);

            // Loop through each part "FILENAME|OS_GUID" pair.
            foreach (String part in parts)
            {
                String[] pair = part.Split(new String[] { MRU_FILE_GUID_SEPARATOR }, StringSplitOptions.RemoveEmptyEntries);
                Debug.Assert(2 == pair.Length, "MRU deserialisation issue!");
                if (2 != pair.Length)
                    continue;

                Guid os = new Guid(pair[1]);
                RecentlyUsedFile entry = new RecentlyUsedFile(pair[0], os);
                mru.Add(entry);
            }

            return (mru.ToArray());
        }

        /// <summary>
        /// Add a file to the recently used file service.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="os"></param>
        public static void AddFile(String filename, Guid os)
        {
            RecentlyUsedFile[] mru = GetFiles();
            List<RecentlyUsedFile> mrul = new List<RecentlyUsedFile>(mru);

            // Search existing list; if our entry is a dupe then remove it
            // and the insert will promote it to the top.
            int index = 0;
            for (index = 0; index < mrul.Count; ++index)
            {
                String entryFilename = mrul[index].Filename;
                if (0 == String.Compare(entryFilename, filename, true))
                {
                    mrul.RemoveAt(index);
                    break;
                }
            }
            // Insert new file at the beginning.
            mrul.Insert(0, new RecentlyUsedFile(filename, os));

            // Check we have not overrun our entry count; remove last entry if
            // we have overrun.
            if (mrul.Count > Properties.Settings.Default.RecentlyUsedFileCount)
                mrul.RemoveAt(mrul.Count - 1);

            String mrulist = SerialiseMRU(mrul.ToArray());
            Properties.Settings.Default.RecentlyUsedFiles = mrulist;
            Properties.Settings.Default.Save();
        }

        /// <summary>
        /// Remove all files that no longer exist from the MRU list.
        /// </summary>
        public static void RemoveNonExistantFiles()
        {
            RecentlyUsedFile[] mru = GetFiles();
            List<RecentlyUsedFile> mrul = new List<RecentlyUsedFile>(mru);

            // reverse so we don't affect indices as items are removed.
            int index = 0;
            for (index = mrul.Count - 1; index >= 0; --index)
            {
                String entryFilename = mrul[index].Filename;
                if (!File.Exists(entryFilename))
                    mrul.RemoveAt(index);
            }
        }

        /// <summary>
        /// Clear out our MRU.
        /// </summary>
        public static void Clear()
        {
            Properties.Settings.Default.RecentlyUsedFiles = String.Empty;
            Properties.Settings.Default.Save();
        }
        #endregion // Static Controller Methods

        #region Private Implementation
        private const String MRU_ENTRY_SEPARATOR = ";";
        private const String MRU_FILE_GUID_SEPARATOR = "|"; 

        /// <summary>
        /// Serialise an array of RecentlyUsedFile objects to a String.
        /// </summary>
        /// <param name="mru"></param>
        /// <returns></returns>
        private static String SerialiseMRU(RecentlyUsedFile[] mru)
        {
            StringBuilder sb = new StringBuilder();
            foreach (RecentlyUsedFile entry in mru)
            {
                sb.AppendFormat("{0}{1}{2}{3}",
                    entry.Filename,
                    MRU_FILE_GUID_SEPARATOR,
                    entry.OpenService.ToString(),
                    MRU_ENTRY_SEPARATOR);
            }
            return (sb.ToString());
        }
        #endregion // Private Implementation
    }

} // Workbench.Services
