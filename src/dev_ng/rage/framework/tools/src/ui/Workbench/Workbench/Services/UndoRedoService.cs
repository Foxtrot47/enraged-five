﻿using System;
using System.IO;
using System.Diagnostics;
using System.Reflection;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Collections.Generic;
using System.Collections.Specialized;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using Workbench.AddIn;
using Workbench.AddIn.Services;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.Services.File;

namespace Workbench.Services
{
    /// <summary>
    /// Workbench undo/redo management service
    /// </summary>
    /// This is a core workbench service that provides the application with
    /// storage between the Model and ViewModel undo/redo managers and the
    /// 
    /// Its somewhat perverse, but this is a IViewModel itself.  This allows
    /// us to register ourselves for undo/redo the selection!
    [ExportExtension(Workbench.AddIn.CompositionPoints.UndoRedoService, typeof(IUndoRedoService))]
    class UndoRedoService : ModelBase, IUndoRedoService
    {        
        #region Members

        private Dictionary<IContentBase, CommandManager> m_content;

        #endregion // Members

        #region Member Data

        /// <summary>
        /// Dictionary of managed content and their models and CommandManager.
        /// </summary>
        protected Dictionary<IContentBase, CommandManager> Contents
        {
            get { return m_content; }
            set { m_content = value; }
        }

        #endregion // Member Data

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public UndoRedoService()
        {
            this.Contents = new Dictionary<IContentBase, CommandManager>();
        }

        #endregion // Constructor(s)

        #region Controller Methods

        /// <summary>
        /// Register a Model with the service.
        /// </summary>
        /// <param name="model"></param>
        public void RegisterContent(IContentBase content, List<IModel> models)
        {
            if (!this.Contents.ContainsKey(content))
            {
                this.Contents.Add(content, new CommandManager());
                this.Contents[content].RegisterModels(models);
            }
            else
            {
                this.Contents[content].RegisterModels(models);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void RegisterModels(IContentBase content, List<IModel> models)
        {
            if (!this.Contents.ContainsKey(content))
            {
                this.Contents.Add(content, new CommandManager());
                this.Contents[content].RegisterModels(models);
            }
            else
            {
                this.Contents[content].RegisterModels(models);
            }
        }

        /// <summary>
        /// Unregister a Model with the service.
        /// </summary>
        /// <param name="model"></param>
        public void UnregisterContent(IContentBase content)
        {
            if (!this.Contents.ContainsKey(content))
                return;

            this.Contents[content].UnregisterAllModels();
            this.Contents.Remove(content);
        }

        /// <summary>
        /// Unregister a Model with the service.
        /// </summary>
        public void UnregisterModels(IContentBase content, List<IModel> models)
        {
            if (this.Contents.ContainsKey(content))
            {
                this.Contents[content].UnRegisterModels(models);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Boolean ContainsContent(IContentBase content)
        {
            if (content != null)
            {
                return this.Contents != null && this.Contents.ContainsKey(content);
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        public int UndoCountForContent(IContentBase content)
        {
            if (content != null)
            {
                if (this.Contents != null && this.Contents.ContainsKey(content))
                {
                    return this.Contents[content].UndoStack.Count;
                }
            }
            return -1;
        }

        /// <summary>
        /// 
        /// </summary>
        public int RedoCountForContent(IContentBase content)
        {
            if (content != null)
            {
                if (this.Contents != null && this.Contents.ContainsKey(content))
                {
                    return this.Contents[content].RedoStack.Count;
                }
            }
            return -1;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Undo(IContentBase content)
        {
            if (content != null && this.Contents != null)
            {
                if (this.Contents.ContainsKey(content))
                {
                    CommandManager manager = this.Contents[content];
                    manager.Undo();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Redo(IContentBase content)
        {
            if (content != null && this.Contents != null)
            {
                if (this.Contents.ContainsKey(content))
                {
                    CommandManager manager = this.Contents[content];
                    manager.Redo();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void OnDocumentSaved(IDocumentBase document)
        {
            foreach (CommandManager manager in this.Contents.Values)
            {
                manager.Saved = true;
                foreach (var command in manager.UndoStack)
                {
                    command.ModifiedState = true;
                }
                foreach (var command in manager.RedoStack)
                {
                    command.ModifiedState = true;
                }
            }
        }

        #endregion // Controller Methods

        #region IWeakEventListener

        /// <summary>
        /// Receives a property changed event from one of the models or a collection change
        /// event from one of the models and needs to make sure the approirate document gets
        /// the right settings set.
        /// </summary>
        public Boolean ReceiveWeakEvent(Type managerType, Object sender, EventArgs e)
        {
            if (managerType == typeof(PropertyChangedEventManager))
            {
                //// A property has changed so set the document as having been modified.
                //IDocumentBase document = GetDocumentForModel(sender);
                //if (document != null)
                //    document.IsModified = true;

                // Also make sure if this property implements the INotifyCollectionChanged or
                // INotifyPropertyChanged interfaces we re-register it for listening.
                String propertyName = (e as PropertyChangedEventArgs).PropertyName;
                Type senderType = sender.GetType();
                PropertyInfo property = senderType.GetProperty(propertyName);
                Debug.Assert(property != null, String.Format("Unable to get the property with name {0} on object type {1}", propertyName, senderType.Name));
                if (property != null)
                {
                    Object propertyValue = property.GetValue(sender, null);
                    if (propertyValue is INotifyCollectionChanged)
                    {

                    }
                    else if (propertyValue is INotifyPropertyChanged)
                    {

                    }
                }
            }
            else if (managerType == typeof(CollectionChangedEventManager))
            {

            }

            return true;
        }

        #endregion // IWeakEventListener
    } // UndoRedoAndSelectionService
} // Workbench.Services namespace
