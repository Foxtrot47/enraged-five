﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LevelBrowser.AddIn.MapBrowser.Commands
{
    public static class Extensions
    {
        #region Commands

        public const String MapSectionCommands = "23477B9A-4B0F-4750-9F1B-FE3665CD465F";

        public const String MapAreaCommands = "2AE32044-CAE7-40C0-BDBF-7E5C44FF0513";

        public const String MapDefinitionCommands = "A54C199C-55CC-461B-BEC0-57A50A1205AA";

        public const String TextureDictionaryCommands = "051F876D-A590-4D34-9F75-69782277C24A";
        
        #endregion // Commands
    } // Extensions
} // LevelBrowser.AddIn.MapBrowser.Commands

