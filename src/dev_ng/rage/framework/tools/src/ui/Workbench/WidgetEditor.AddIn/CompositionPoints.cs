﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WidgetEditor.AddIn
{
    /// <summary>
    /// Contants a set of static strings that are used so that other plugins can use the
    /// proxy service.
    /// </summary>
    public static class CompositionPoints
    {
        /// <summary>
        /// The point to use to get the proxy service
        /// </summary>
        public const String ProxyService = "2245727F-192A-472A-B3CC-099300F8543F";
    } // CompositionPoints
}
