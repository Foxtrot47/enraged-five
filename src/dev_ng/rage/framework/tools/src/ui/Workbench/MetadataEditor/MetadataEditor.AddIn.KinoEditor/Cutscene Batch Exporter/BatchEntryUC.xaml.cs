﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MetadataEditor.AddIn.KinoEditor.Cutscene_Batch_Exporter
{
    /// <summary>
    /// Interaction logic for BatchEntry.xaml
    /// </summary>
    public partial class BatchEntryUC : UserControl
    {
        public BatchEntryUC()
        {
            InitializeComponent();
        }

        private void FBXFile_Button_Click(object sender, EventArgs e)
        {
            if (this.DataContext is BatchEntry)
            {
                Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
                dlg.Title = "Open FBX File";
                dlg.DefaultExt = ".fbx"; // Default file extension
                dlg.Filter = "FBX files (.fbx)|*.fbx"; // Filter files by extension
                dlg.InitialDirectory = CutsceneBatchExportToolWindowViewModel.CutsceneScenePath;

                // Show open file dialog box
                Nullable<bool> result = dlg.ShowDialog();

                // Process open file dialog box results
                if (result == true)
                {
                    // Open document
                    (this.DataContext as BatchEntry).FBXFile = dlg.FileName;
                }
            }
        }

        private void CutFile_Button_Click(object sender, EventArgs e)
        {
            if (this.DataContext is BatchEntry)
            {
                Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
                dlg.Title = "Open Cut Part File";
                dlg.DefaultExt = ".cutpart"; // Default file extension
                dlg.Filter = "Cut part files (.cutpart)|*.cutpart"; // Filter files by extension
                dlg.InitialDirectory = CutsceneBatchExportToolWindowViewModel.CutsceneCutsPath;

                // Show open file dialog box
                Nullable<bool> result = dlg.ShowDialog();

                // Process open file dialog box results
                if (result == true)
                {
                    // Open document
                    (this.DataContext as BatchEntry).CutFile = dlg.FileName;
                }
            }
        }
    }
}
