﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;

namespace Workbench.AddIn.Services
{

    /// <summary>
    /// 
    /// </summary>
    public abstract class ExtensionBase : ViewModelBase, IExtension
    {
        #region IExtension Properties
        /// <summary>
        /// ExtensionService GUID.
        /// </summary>
        [Browsable(false)]
        public Guid ID 
        { 
            get { return m_ID; }
            protected set 
            {
                SetPropertyValue(value, () => this.ID,
                    new PropertySetDelegate(delegate(Object newValue) { m_ID = (Guid)newValue; }));
            }
        }
        private Guid m_ID = Guid.Empty;
        
        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public Guid RelativeID 
        {
            get { return m_eRelativeID; }
            protected set
            {
                SetPropertyValue(value, () => this.RelativeID,
                    new PropertySetDelegate(delegate(Object newValue) { m_eRelativeID = (Guid)newValue; }));
            }
        }
        private Guid m_eRelativeID = Guid.Empty;

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public Direction Direction 
        {
            get { return m_eDirection; } 
            protected set
            {
                SetPropertyValue(value, () => this.Direction,
                    new PropertySetDelegate(delegate(Object newValue) { m_eDirection = (Direction)newValue; }));
            }
        }
        private Direction m_eDirection = Direction.After;
        #endregion // IExtension Properties
    }

} // Workbench.AddIn.Services namespace
