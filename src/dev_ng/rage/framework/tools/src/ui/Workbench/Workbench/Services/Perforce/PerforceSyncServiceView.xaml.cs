﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Interop;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Workbench.Services.Perforce
{
    /// <summary>
    /// Interaction logic for PreforceSyncServiceView.xaml
    /// </summary>
    public partial class PerforceSyncServiceView : Window
    {
        #region Constructor

        public PerforceSyncServiceView()
        {
            InitializeComponent();
        }

        #endregion // Constructor

        #region Private Functions

        /// <summary>
        /// 
        /// </summary>
        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            PerforceSyncServiceViewModel vm = this.DataContext as PerforceSyncServiceViewModel;

            // Check whether we need to abort an ongoing sync
            if (vm.IsSyncing)
            {
                if (vm.CanAbortActiveSync(null))
                {
                    vm.AbortActiveSync(null);
                    base.OnClosing(e);
                }
                else
                {
                    e.Cancel = true;
                }
            }
            else
            {
                base.OnClosing(e);
            }
        }
        #endregion // Private Functions
    } // PreforceSyncServiceView
} // Workbench.Services.Perforce
