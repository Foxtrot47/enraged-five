﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using RSG.Base.Logging;
using RSG.Base.Editor;
using Workbench.AddIn;
using Workbench.AddIn.UI.Menu;
using Workbench.AddIn.Services;

namespace Workbench
{
    /// <summary>
    /// Metadata Editor Workbench view window.
    /// </summary>
    public partial class WorkbenchView : Window
    {
        #region Properties
        public IApplicationArgumentsService ApplicationArgumentsService { get; set; }
        #endregion

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public WorkbenchView()
        {
            InitializeComponent();
            this.StateChanged += new EventHandler(WorkbenchView_StateChanged);
            if (Properties.Settings.Default.Maximised)
                this.WindowState = System.Windows.WindowState.Maximized;
            else
                this.WindowState = System.Windows.WindowState.Normal;
        }

        void WorkbenchView_StateChanged(object sender, EventArgs e)
        {
            if (this.WindowState == System.Windows.WindowState.Maximized)
                Properties.Settings.Default.Maximised = true;
            else if (this.WindowState == System.Windows.WindowState.Normal)
                Properties.Settings.Default.Maximised = false;
        }

        #endregion // Constructor(s)
        
        #region Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Window_Loaded(Object sender, RoutedEventArgs e)
        {
            // Install WndProc handler.
            HwndSource source = HwndSource.FromHwnd(new WindowInteropHelper(this).Handle);
            source.AddHook(new HwndSourceHook(WndProc)); 
 
            // Process command line arguments.
            if (this.ApplicationArgumentsService != null)
                ApplicationArgumentsService.TriggerProcessStartupArguments();
        }
        
        /// <summary>
        /// WndProc handler.
        /// </summary>
        /// <param name="hwnd"></param>
        /// <param name="msg"></param>
        /// <param name="wParam"></param>
        /// <param name="lParam"></param>
        /// <param name="handled"></param>
        /// <returns></returns>
        private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            const int WM_MOUSEACTIVATE = 0x0021;
            const int MA_NOACTIVATE = 3;

            if (msg == WM_MOUSEACTIVATE)
            {
                if (this.IsFocused == false)
                {
                    Boolean menuActivated = false;
                    Boolean toolbarActivated = false;
                    for (int i = 0; i < this.MainMenuControl.Items.Count; i++)
                    {
                        DependencyObject item = this.MainMenuControl.ItemContainerGenerator.ContainerFromIndex(i);
                        UIElement element = item as UIElement;
                        if (element != null && element.IsMouseOver)
                        {
                            menuActivated = true;
                            break;
                        }
                    }
                    if (menuActivated == false)
                    {
                        foreach (ToolBar toolBar in this.ToolBarTrayControl.ToolBars)
                        {
                            for (int i = 0; i < toolBar.Items.Count; i++)
                            {
                                DependencyObject item = toolBar.ItemContainerGenerator.ContainerFromIndex(i);
                                UIElement element = item as UIElement;
                                if (element != null && element.IsMouseOver)
                                {
                                    toolbarActivated = true;
                                    break;
                                }
                            }
                        }
                    }
                    if (menuActivated == true || toolbarActivated == true)
                    {
                        handled = true;
                        return new IntPtr(MA_NOACTIVATE);
                    }
                }
            }
            return IntPtr.Zero;
        }
        #endregion // Event Handlers
    }

} // Workbench namespace
