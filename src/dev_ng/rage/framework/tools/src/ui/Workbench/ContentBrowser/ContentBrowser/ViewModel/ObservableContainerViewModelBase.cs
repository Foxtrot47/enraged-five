﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Collections;
using RSG.Model.Common;
using System.Collections.Specialized;
using System.Windows;
using System.Windows.Threading;
using System.ComponentModel;
using ContentBrowser.Util;

namespace ContentBrowser.ViewModel
{
    /// <summary>
    /// Abstract base class for viewm model's that wish to monitor the contents of a particular observable collection
    /// </summary>
    public abstract class ObservableContainerViewModelBase<T> : ContainerViewModelBase, IWeakEventListener where T : IAsset
    {
        #region Event
        /// <summary>
        /// 
        /// </summary>
        public event EventHandler ViewModelChildrenCreated;
        #endregion

        #region Properties and Associated Member Data
        /// <summary>
        /// A reference to the asset container base model
        /// that this view model wraps
        /// </summary>
        public ObservableCollection<T> MonitoredCollection
        {
            get { return m_monitoredCollection; }
            set
            {
                SetPropertyValue(value, () => MonitoredCollection,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            if (m_monitoredCollection != null)
                            {
                                CollectionChangedEventManager.RemoveListener(m_monitoredCollection, this);
                            }

                            m_monitoredCollection = (ObservableCollection<T>)newValue;

                            if (newValue != null)
                            {
                                CollectionChangedEventManager.AddListener(m_monitoredCollection, this);
                            }
                        }
                ));
            }
        }
        private ObservableCollection<T> m_monitoredCollection;

        /// <summary>
        /// Background worker used to create the viewmodels for
        /// the asset children shown on expansion
        /// </summary>
        protected virtual BackgroundWorker AssetChildWorker
        {
            get { return m_asyncWorker; }
            set
            {
                SetPropertyValue(value, () => AssetChildWorker,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_asyncWorker = (BackgroundWorker)newValue;
                        }
                ));
            }
        }
        protected BackgroundWorker m_asyncWorker;

        /// <summary>
        /// Background worker used to create the viewmodels for
        /// the grid assets shown on selection
        /// </summary>
        protected virtual BackgroundWorker GridAssetWorker
        {
            get { return m_fileAsyncWorker; }
            set
            {
                SetPropertyValue(value, () => GridAssetWorker,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_fileAsyncWorker = (BackgroundWorker)newValue;
                        }
                ));
            }
        }
        protected BackgroundWorker m_fileAsyncWorker;

        /// <summary>
        /// The number of viewmodels to create before displaying them on the
        /// screen. Less than or equal to 0 implies no batching
        /// </summary>
        protected virtual uint AssetCreationBatchSize
        {
            get
            {
                return 0;
            }
        }

        /// <summary>
        /// The number of viewmodels to create before displaying them on the
        /// screen. Less than or equal to 0 implies no batching
        /// </summary>
        protected virtual uint GridCreationBatchSize
        {
            get
            {
                return 0;
            }
        }
        #endregion // Properties and Associated Member Data
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="collection"></param>
        public ObservableContainerViewModelBase(ObservableCollection<T> collection)
            : this(null, collection)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public ObservableContainerViewModelBase(IAsset model, ObservableCollection<T> collection)
            : base(model)
        {
            AssetChildWorker = new BackgroundWorker();
            AssetChildWorker.WorkerReportsProgress = false;
            AssetChildWorker.WorkerSupportsCancellation = true;
            AssetChildWorker.DoWork += new DoWorkEventHandler(OnExpansion);
            AssetChildWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(OnExpansionCompleted);

            GridAssetWorker = new BackgroundWorker();
            GridAssetWorker.WorkerReportsProgress = false;
            GridAssetWorker.WorkerSupportsCancellation = true;
            GridAssetWorker.DoWork += new DoWorkEventHandler(OnSelection);
            GridAssetWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(OnSelectionCompleted);

            MonitoredCollection = collection;
        }
        #endregion // Constructor(s)

        #region Virtual Functions
        /// <summary>
        /// This gets called whenever the selection state for this view model
        /// has changed
        /// </summary>
        protected override void OnSelectionChanged(Boolean oldValue, Boolean newValue)
        {
            if (this.GridAssetWorker.IsBusy)
            {
                this.GridAssetWorker.CancelAsync();
            }
            else
            {
                ResetGridAssets();
                if (newValue == true)
                {
                    this.GridAssetWorker.RunWorkerAsync();
                }
            }
        }

        /// <summary>
        /// This gets called whenever the expansion state for this view model
        /// has changed
        /// </summary>
        protected override void OnExpansionChanged(Boolean oldValue, Boolean newValue)
        {
            if (this.AssetChildWorker.IsBusy)
            {
                this.m_asyncWorker.CancelAsync();
            }
            else
            {
                ResetChildAssets();
                if (newValue == true)
                {
                    this.AssetChildWorker.RunWorkerAsync();
                }
                else
                {
                    this.AssetChildren.Add(new AssetDummyViewModel());
                }
            }
        }

        /// <summary>
        /// Resets the asset children for this view model by clearing the
        /// current list and clearing the memory
        /// </summary>
        protected virtual void ResetChildAssets()
        {
            lock (this.AssetChildren)
            {
                this.AssetChildren.Clear();
            }
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
        }

        /// <summary>
        /// Resets the grid assets for this view model by clearing the
        /// current list and clearing the memory
        /// </summary>
        protected virtual void ResetGridAssets()
        {
            lock (this.GridAssets)
            {
                this.GridAssets.Clear();
            }
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
        }

        /// <summary>
        /// Gets called in a background thread when the view model is expanded. This 
        /// creates the child asset view models
        /// </summary>
        protected virtual void OnExpansion(Object sender, DoWorkEventArgs e)
        {
            if (MonitoredCollection == null)
            {
                return;
            }

            BackgroundWorker bwAsync = sender as BackgroundWorker;

            // Create the copied list of assets that we'll work with here
            List<T> copiedAssets = new List<T>();
            lock (MonitoredCollection)
            {
                copiedAssets = new List<T>(MonitoredCollection);
            }

            // Create all the view models
            List<IAssetViewModel> viewModels = new List<IAssetViewModel>();
            foreach (IAsset preAsset in copiedAssets)
            {
                // Add any new distinct items that are valid for adding and that we don't already have as children
                foreach (IAsset asset in this.PreprocessAsset(preAsset).Distinct().Where(item => IsAssetValid(item) && !ContainsAssetChild(item)))
                {
                    IAssetViewModel newViewModel = ViewModelFactory.CreateViewModel(asset);
                    viewModels.Add(newViewModel);

                    // Check whether we have reached the batch size
                    if (viewModels.Count >= this.AssetCreationBatchSize)
                    {
                        AssetChildren.AddRangeThreaded(viewModels);
                        viewModels.Clear();
                    }

                    if (bwAsync.CancellationPending)
                    {
                        // Set the e.Cancel flag so that the WorkerCompleted event
                        // knows that the process was cancelled.
                        e.Cancel = true;
                        return;
                    }
                }
            }

            if (viewModels.Count > 0)
            {
                AssetChildren.AddRangeThreaded(viewModels);
            }
        }

        /// <summary>
        /// Called after the background thread to create the asset children has finished,
        /// either through completing the thread or cancelling the thread
        /// </summary>
        protected virtual void OnExpansionCompleted(Object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                this.OnExpansionChanged(!this.IsExpanded, this.IsExpanded);
            }
            else
            {
                FireChildrenCreatedEvent();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected void FireChildrenCreatedEvent()
        {
            if (ViewModelChildrenCreated != null)
                this.ViewModelChildrenCreated(this, EventArgs.Empty);
        }

        /// <summary>
        /// Gets called in a background thread when the view model is selected. This
        /// creates the grid asset view models
        /// </summary>
        protected virtual void OnSelection(Object sender, DoWorkEventArgs e)
        {
            if (MonitoredCollection == null)
            {
                return;
            }

            BackgroundWorker bwAsync = sender as BackgroundWorker;

            // Create the copied list of assets that we'll work with here
            List<T> copiedAssets = new List<T>();
            lock (MonitoredCollection)
            {
                copiedAssets = new List<T>(MonitoredCollection);
            }

            // Create the view models
            List<IGridViewModel> viewModels = new List<IGridViewModel>();
            foreach (IAsset preAsset in copiedAssets)
            {
                // Add any new distinct items that are valid for adding and that we don't already have as children
                IList<IAsset> distinct = this.PreprocessGridAsset(preAsset).Distinct().ToList();
                IList<IAsset> valid = distinct.Where(item => IsGridAssetValid(item)).ToList();
                IList<IAsset> notChild = valid.Where(item => !ContainsAssetChild(item)).ToList();

                foreach (IAsset asset in this.PreprocessGridAsset(preAsset).Distinct().Where(item => IsGridAssetValid(item) && !ContainsGridAsset(item)))
                {
                    IGridViewModel newViewModel = ViewModelFactory.CreateGridViewModel(asset);
                    viewModels.Add(newViewModel);

                    // Have we reached the batch size?
                    if (viewModels.Count >= this.GridCreationBatchSize)
                    {
                        GridAssets.AddRangeThreaded(viewModels);
                        viewModels.Clear();
                    }

                    if (bwAsync.CancellationPending)
                    {
                        // Set the e.Cancel flag so that the WorkerCompleted event
                        // knows that the process was cancelled.
                        e.Cancel = true;
                        return;
                    }
                }
            }

            if (viewModels.Count > 0)
            {
                GridAssets.AddRangeThreaded(viewModels);
            }
        }

        /// <summary>
        /// Called after the background thread to create the grid assets has finished,
        /// either through completing the thread or cancelling the thread
        /// </summary>
        protected virtual void OnSelectionCompleted(Object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                this.OnSelectionChanged(!this.IsSelected, this.IsSelected);
            }
        }

        /// <summary>
        /// Gets called if the models asset children changes. This should be used to make sure the
        /// model and the view model stay in sync with each other.
        /// </summary>
        protected virtual void OnModelAssetsChanged(NotifyCollectionChangedEventArgs e)
        {
            if (this.IsExpanded == true || this.IsSelected == true)
            {
                List<IAsset> added = new List<IAsset>();
                List<IAsset> gridAdded = new List<IAsset>();
                List<IAssetViewModel> assetRemoved = new List<IAssetViewModel>();
                List<IGridViewModel> gridAssetRemoved = new List<IGridViewModel>();
                if (!(e.Action == NotifyCollectionChangedAction.Move) && !(e.Action == NotifyCollectionChangedAction.Reset))
                {
                    if (e.NewItems != null)
                    {
                        foreach (Object add in e.NewItems)
                        {
                            if (add is IAsset)
                            {
                                if (this.IsAssetValid(add as IAsset))
                                {
                                    added.Add(add as IAsset);
                                }
                                if (this.IsGridAssetValid(add as IAsset))
                                {
                                    gridAdded.Add(add as IAsset);
                                }
                            }
                        }
                    }
                    if (e.OldItems != null)
                    {
                        foreach (Object remove in e.OldItems)
                        {
                            if (remove is IAsset)
                            {
                                IAssetViewModel viewModel = this.GetAssetChild(remove as IAsset);
                                if (viewModel != null)
                                {
                                    assetRemoved.Add(viewModel);
                                }
                                IGridViewModel gridViewModel = this.GetGridAsset(remove as IAsset);
                                if (gridViewModel != null)
                                {
                                    gridAssetRemoved.Add(gridViewModel);
                                }
                            }
                        }
                    }
                }
                else if (e.Action == NotifyCollectionChangedAction.Reset)
                {
                    this.OnExpansionChanged(!this.IsExpanded, this.IsExpanded);
                    this.OnSelectionChanged(!this.IsSelected, this.IsSelected);
                    return;
                }

                if (this.IsExpanded == true)
                {
                    // Remove any children first
                    AssetChildren.RemoveRangeThreaded(assetRemoved);

                    // Add any new assets added
                    List<IAssetViewModel> viewModels = new List<IAssetViewModel>();
                    foreach (IAsset preAsset in added)
                    {
                        // Add any new distinct items that are valid for adding and that we don't already have as children
                        foreach (IAsset asset in this.PreprocessAsset(preAsset).Distinct().Where(item => IsAssetValid(item) && !ContainsAssetChild(item)))
                        {
                            IAssetViewModel newViewModel = ViewModelFactory.CreateViewModel(asset);
                            viewModels.Add(newViewModel);

                            // Check whether we have reached the batch size
                            if (viewModels.Count >= this.AssetCreationBatchSize)
                            {
                                AssetChildren.AddRangeThreaded(viewModels);
                                viewModels.Clear();
                            }
                        }
                    }

                    if (viewModels.Count > 0)
                    {
                        AssetChildren.AddRangeThreaded(viewModels);
                    }
                }
                if (this.IsSelected == true)
                {
                    // Remove any children grid vms first
                    GridAssets.RemoveRangeThreaded(gridAssetRemoved);

                    // Add any new assets added
                    List<IGridViewModel> viewModels = new List<IGridViewModel>();
                    foreach (IAsset preAsset in gridAdded)
                    {
                        // Add any new distinct items that are valid for adding and that we don't already have as children
                        foreach (IAsset asset in this.PreprocessGridAsset(preAsset).Distinct().Where(item => IsGridAssetValid(item) && !ContainsGridAsset(item)))
                        {
                            IGridViewModel newViewModel = ViewModelFactory.CreateGridViewModel(asset);
                            viewModels.Add(newViewModel);

                            // Have we reached the batch size?
                            if (viewModels.Count >= this.GridCreationBatchSize)
                            {
                                GridAssets.AddRangeThreaded(viewModels.Where(item => !GridAssets.Contains(item)));
                                viewModels.Clear();
                            }
                        }
                    }

                    if (viewModels.Count > 0)
                    {
                        GridAssets.AddRangeThreaded(viewModels);
                    }
                }
            }
        }

        /// <summary>
        /// Determines whether a model asset should be displayed as a
        /// child view model asset and a grid asset. Default is always true
        /// </summary>
        protected virtual Boolean IsAssetValid(IAsset asset)
        {
            return true;
        }

        /// <summary>
        /// Determines whether a model asset should be displayed as a
        /// grid asset. Default is always true
        /// </summary>
        protected virtual Boolean IsGridAssetValid(IAsset asset)
        {
            return true;
        }

        /// <summary>
        /// Preprocess an asset to get a list of assets to display in the hierarchy
        /// </summary>
        protected virtual IEnumerable<IAsset> PreprocessAsset(IAsset asset)
        {
            yield return asset;
        }

        /// <summary>
        /// Preprocess an asset to get a list of assets to display in the grid
        /// </summary>
        protected virtual IEnumerable<IAsset> PreprocessGridAsset(IAsset asset)
        {
            yield return asset;
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual void ExpandAndCreateChildren()
        {
            if (this.AssetChildWorker.IsBusy)
            {
                this.m_asyncWorker.CancelAsync();
            }
            else
            {
                this.AssetChildren.Clear();
                List<IAssetViewModel> viewModels = new List<IAssetViewModel>();
                foreach (IAsset preAsset in MonitoredCollection)
                {
                    foreach (IAsset asset in this.PreprocessAsset(preAsset).Distinct().Where(item => IsAssetValid(item) && !ContainsAssetChild(item)))
                    {
                        if (this.IsAssetValid(asset))
                        {
                            IAssetViewModel newViewModel = ViewModelFactory.CreateViewModel(asset);
                            this.AssetChildren.Add(newViewModel);
                        }
                    }
                }
                IsExpanded = true;
                OnPropertyChanged("IsExpanded");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void CollapseAndDestroyChildren()
        {
            if (this.AssetChildWorker.IsBusy)
            {
                this.m_asyncWorker.CancelAsync();
            }
            else
            {
                ResetChildAssets();
                this.AssetChildren.Add(new AssetDummyViewModel());
                IsExpanded = false;
                OnPropertyChanged("IsExpanded");
            }
        }
        #endregion // Virtual Functions

        #region IWeakEventListener Implementation
        /// <summary>
        /// Called when the contents of the container we are monitoring changes
        /// </summary>
        public virtual Boolean ReceiveWeakEvent(Type managerType, Object sender, EventArgs e)
        {
            if (managerType == typeof(CollectionChangedEventManager))
            {
                if (Application.Current != null)
                {
                    Application.Current.Dispatcher.Invoke(
                        new Action
                        (
                            delegate()
                            {
                                OnModelAssetsChanged(e as NotifyCollectionChangedEventArgs);
                            }
                        ),
                        System.Windows.Threading.DispatcherPriority.ContextIdle
                    );
                }
                else
                {
                    OnModelAssetsChanged(e as NotifyCollectionChangedEventArgs);
                }
            }

            return true;
        }
        #endregion // IWeakEventListener
    } // ObservableContainerViewModelBase<T>
}
