﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common.Map;

namespace ContentBrowser.ViewModel.MapViewModels3
{
    /// <summary>
    /// 
    /// </summary>
    public class MapSectionEntityGroupViewModel : ObservableContainerViewModelBase<IEntity>
    {
        #region Properties
        /// <summary>
        /// The main name property which by default will be shown as the header for a browser item
        /// </summary>
        public override String Name
        {
            get
            {
                return "Entities";
            }
        }

        /// <summary>
        /// The number of viewmodels to create before displaying them on the
        /// screen. Less than or equal to 0 implies no batching
        /// </summary>
        protected override uint AssetCreationBatchSize
        {
            get
            {
                return 250;
            }
        }

        /// <summary>
        /// The number of viewmodels to create before displaying them on the
        /// screen. Less than or equal to 0 implies no batching
        /// </summary>
        protected override uint GridCreationBatchSize
        {
            get
            {
                return 250;
            }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public MapSectionEntityGroupViewModel(IMapSection section)
            : base(section, section.ChildEntities)
        {
        }
        #endregion // Constructor(s)
    } // MapSectionEntityGroupViewModel
}
