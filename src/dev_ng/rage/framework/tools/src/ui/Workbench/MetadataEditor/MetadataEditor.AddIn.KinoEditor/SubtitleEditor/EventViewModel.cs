﻿using System;
using System.Windows.Input;
using RSG.Base.Editor.Command;
using RSG.Base.Windows.Helpers;
namespace MetadataEditor.AddIn.KinoEditor.Subtitle_Editor
{
    /// <summary>
    /// Represents a single Event that has a start value and a length value in some non
    /// specifiec units.
    /// </summary>
    public class EventViewModel : ViewModelBase
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Start"/> property.
        /// </summary>
        private double start;

        /// <summary>
        /// The private field used for the <see cref="UnitLength"/> property.
        /// </summary>
        private double unitLength;

        /// <summary>
        /// The private field used for the <see cref="String"/> property.
        /// </summary>
        private string text;

        /// <summary>
        /// The private field used for the <see cref="Speaker"/> property.
        /// </summary>
        private string speaker;

        /// <summary>
        /// The private field used for the <see cref="RemoveTimelineCommand"/> property.
        /// </summary>
        private RelayCommand removeTimeLineCommand;

        /// <summary>
        /// The private reference to the subtitle editor view model that constructed this
        /// event.
        /// </summary>
        internal SubtitleEditorViewModel subtitleEditor;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="EventViewModel"/> class.
        /// </summary>
        public EventViewModel()
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="EventViewModel"/> class.
        /// </summary>
        /// <param name="subtitleEditor">
        /// The subtitle editor that is the owner for this event.
        /// </param>
        public EventViewModel(SubtitleEditorViewModel subtitleEditor)
        {
            this.subtitleEditor = subtitleEditor;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the unit value that this event starts at.
        /// </summary>
        public double Start
        {
            get { return this.start; }
            set { this.SetProperty(ref this.start, value, "Start"); }
        }

        /// <summary>
        /// Gets or sets the number of units that this event goes on for.
        /// </summary>
        public double UnitLength
        {
            get { return this.unitLength; }
            set { this.SetProperty(ref this.unitLength, value, "UnitLength"); }
        }

        /// <summary>
        /// Gets or sets the event text.
        /// </summary>
        public string String
        {
            get { return this.text; }
            set { this.SetProperty(ref this.text, value, "String"); }
        }

        /// <summary>
        /// Gets or sets the speaker of the event.
        /// </summary>
        public string Speaker
        {
            get { return this.speaker; }
            set { this.SetProperty(ref this.speaker, value, "Speaker"); }
        }

        /// <summary>
        /// Gets or sets the identifier of the event.
        /// </summary>
        public string Identifier
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the identifier of the event.
        /// </summary>
        public string Filename
        {
            get;
            set;
        }


        /// <summary>
        /// Gets or sets if the event is present on the timeline.
        /// </summary>
        public bool OnTimeline
        {
            get;
            set;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter cannot be cast to ThreeDPoint return false:
            EventViewModel p = obj as EventViewModel;
            if ((object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return Identifier == p.Identifier && Speaker == p.Speaker && String == p.String;
        }

        public void ResetScale()
        {
            UnitLength = 20;
            Start = 1;
        }

        /// <summary>
        /// Gets the command that removes this event from the timeline.
        /// </summary>
        public ICommand RemoveTimeLineCommand
        {
            get
            {
                if (this.removeTimeLineCommand == null)
                {
                    this.removeTimeLineCommand =
                        new RelayCommand(param => this.RemoveFromTimeLine());
                }

                return this.removeTimeLineCommand;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Removes this event from the time line.
        /// </summary>
        private void RemoveFromTimeLine()
        {
            if (!this.OnTimeline)
            {
                return;
            }

            this.subtitleEditor.RemoveEventFromTimeLine(this);
        }
        #endregion
    }
}
