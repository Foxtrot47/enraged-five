﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Collections;
using System.Collections.Specialized;

namespace Viewport.AddIn
{
    /// <summary>
    /// Contains a group of overlays to show on the viewport
    /// this is so that the viewport can sub categrise the overlays
    /// </summary>
    public class ViewportOverlayGroup : Viewport.AddIn.IViewportOverlayGroup, INotifyPropertyChanged
    {
        #region Events

        /// <summary>
        /// Property changed event fired when the name changes so that the 
        /// binding can be dynamic.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Create the OnPropertyChanged method to raise the event
        /// </summary>
        /// <param name="name"></param>
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// A collection of all the overlays in this group
        /// </summary>
        public ObservableCollection<IViewportOverlay> Overlays
        {
            get { return m_overlays; }
            set { m_overlays = value;  }
        }
        private ObservableCollection<IViewportOverlay> m_overlays;

        /// <summary>
        /// The name of this overlay group, this will be the name shown OnPropertyChanged("Overlays");
        /// in the viewport overlay drop down menu
        /// </summary>
        public String Name
        {
            get { return m_name; }
            set
            {
                if (m_name != value)
                {
                    m_name = value;
                    OnPropertyChanged("Name");
                }
            }
        }
        private String m_name;

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public ViewportOverlayGroup(String name)
            : this(name, null)
        {
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="geometryList"></param>
        public ViewportOverlayGroup(String name, IEnumerable<ViewportOverlay> overlayList)
        {
            this.Name = name;
            this.Overlays = new ObservableCollection<IViewportOverlay>();

            if (overlayList != null)
            {
                this.Overlays.BeginUpdate();
                this.Overlays.AddRange(overlayList);
                this.Overlays.EndUpdate();
            }
        }
        #endregion // Constructor(s)

        #region Virtual Function(s)
        /// <summary>
        /// Called when the overlay group becomes the active group
        /// </summary>
        public virtual void Activated()
        {
        }

        /// <summary>
        /// Called when the overlay group is no longer the active group
        /// </summary>
        public virtual void Deactivated()
        {
        }
        #endregion // Virtual Function(s)

        #region IComparable<IViewportOverlayGroup> Implementation
        /// <summary>
        /// 
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(IViewportOverlayGroup other)
        {
            return this.Name.CompareTo(other.Name);
        }
        #endregion // IComparable<IViewportOverlayGroup> Implementation
    } // ViewportOverlayGroup
} // Viewport.AddIn
