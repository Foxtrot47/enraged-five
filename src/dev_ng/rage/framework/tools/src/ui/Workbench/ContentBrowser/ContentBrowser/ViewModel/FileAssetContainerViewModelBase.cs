﻿using System;
using System.Windows;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Threading;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using RSG.Model.Common;

namespace ContentBrowser.ViewModel
{
    /// <summary>
    /// 
    /// </summary>
    public class FileAssetContainerViewModelBase : AssetContainerViewModelBase, IFileAssetViewModel
    {
        #region Properties
        /// <summary>
        /// The main filename property
        /// </summary>
        public virtual String Filename
        {
            get { return "Unknown Filename For File Asset"; }
        }
        #endregion // Properties

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public FileAssetContainerViewModelBase(IHasAssetChildren model)
            : base(model)
        {
        }
        #endregion // Constructor
    } // FileAssetContainerViewModelBase
} // ContentBrowser.ViewModel
