﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Media;
using RSG.Base.Drawing;
using RSG.Base.Extensions;
using RSG.Base.Math;
using RSG.Base.Tasks;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using RSG.Base.Windows.Dialogs;
using RSG.Model.Common;
using RSG.Model.Common.Extensions;
using RSG.Model.Common.Map;
using RSG.Model.Common.Util;
using MapViewport.AddIn;
using Workbench.AddIn.Services.Model;

namespace Workbench.AddIn.MapStatistics.Overlays
{

    /// <summary>
    /// 
    /// </summary>
    public enum SectionStatsType
    {
        Section,
        PropGroup,
        Combined
    }

    /// <summary>
    /// 
    /// </summary>
    public class StatisticOverlayBase : LevelDependentViewportOverlay
    {
        #region Members

        protected Boolean m_includePropGroup;
        protected Boolean m_includeDrawableEntities;
        protected Boolean m_includeNonDrawableEntities;
        protected Boolean m_dontShowEntityTypeOption;
        protected Boolean m_includeHighDetailed;
        protected Boolean m_includeLod;
        protected Boolean m_includeSuperLod;
        protected Boolean m_includeReferenced;
        protected Boolean m_includeInterior;
        protected Boolean m_includeDynamic;
        protected Boolean m_includeNonDynamic;
        protected Boolean m_includeOnlyUniqueReferenced;
        protected Boolean m_onlyWorstCases;
        protected int m_worstCaseCount;
        protected int m_maximumGeometryCount;
        protected Boolean m_onlyGreaterThan;
        protected Boolean m_onlyGreaterThanAverage;
        protected double m_average;
        protected float m_greaterThanLimit;
        protected IMapSection m_selectedSection;
        protected Boolean m_dontShowObjectOptions;
        protected Boolean m_dontShowReferenceOptions;
        protected Boolean m_dontShowPropOptions;
        protected Boolean m_dontShowInteriorOptions;
        protected Boolean m_dontShowLodOptions;
        protected Boolean m_dontShowSlod1Options;
        protected Boolean m_dontShowUniqueOptions;

        private IDictionary<IMapSection, IDictionary<StatKey, IMapSectionStatistic>> m_cachedSectionStats;
        private IDictionary<IMapSection, IDictionary<StatKey, IMapSectionStatistic>> m_cachedCombinedStats;

        #endregion // Members

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public Boolean IncludePropGroup
        {
            get { return m_includePropGroup; }
            set
            {
                SetPropertyValue(value, () => IncludePropGroup,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_includePropGroup = (Boolean)newValue;
                            OnRenderOptionChanged();
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Boolean IncludeDrawableEntities
        {
            get { return m_includeDrawableEntities; }
            set
            {
                SetPropertyValue(value, m_includeDrawableEntities, () => IncludeDrawableEntities,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_includeDrawableEntities = (Boolean)newValue;
                            OnRenderOptionChanged();
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Boolean IncludeNonDrawableEntities
        {
            get { return m_includeNonDrawableEntities; }
            set
            {
                SetPropertyValue(value, m_includeNonDrawableEntities, () => IncludeNonDrawableEntities,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_includeNonDrawableEntities = (Boolean)newValue;
                            OnRenderOptionChanged();
                        }
                ));
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public Boolean DontShowEntityTypeOption
        {
            get { return m_dontShowEntityTypeOption; }
            set
            {
                SetPropertyValue(value, m_dontShowEntityTypeOption, () => DontShowEntityTypeOption,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_dontShowEntityTypeOption = (Boolean)newValue;
                            OnRenderOptionChanged();
                        }
                ));
            }
        }      

        /// <summary>
        /// 
        /// </summary>
        public Boolean IncludeHighDetailed
        {
            get { return m_includeHighDetailed; }
            set
            {
                SetPropertyValue(value, () => IncludeHighDetailed,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_includeHighDetailed = (Boolean)newValue;
                            OnRenderOptionChanged();
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Boolean IncludeLod
        {
            get { return m_includeLod; }
            set
            {
                SetPropertyValue(value, () => IncludeLod,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_includeLod = (Boolean)newValue;
                            OnRenderOptionChanged();
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Boolean IncludeSuperLod
        {
            get { return m_includeSuperLod; }
            set
            {
                SetPropertyValue(value, () => IncludeSuperLod,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_includeSuperLod = (Boolean)newValue;
                            OnRenderOptionChanged();
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Boolean IncludeReferenced
        {
            get { return m_includeReferenced; }
            set
            {
                SetPropertyValue(value, () => IncludeReferenced,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_includeReferenced = (Boolean)newValue;
                            OnRenderOptionChanged();
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Boolean IncludeInterior
        {
            get { return m_includeInterior; }
            set
            {
                SetPropertyValue(value, () => IncludeInterior,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_includeInterior = (Boolean)newValue;
                            OnRenderOptionChanged();
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Boolean IncludeDynamic
        {
            get { return m_includeDynamic; }
            set
            {
                SetPropertyValue(value, () => IncludeDynamic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_includeDynamic = (Boolean)newValue;
                            OnRenderOptionChanged();
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Boolean IncludeNonDynamic
        {
            get { return m_includeNonDynamic; }
            set
            {
                SetPropertyValue(value, () => IncludeNonDynamic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_includeNonDynamic = (Boolean)newValue;
                            OnRenderOptionChanged();
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Boolean IncludeOnlyUniqueReferenced
        {
            get { return m_includeOnlyUniqueReferenced; }
            set
            {
                SetPropertyValue(value, () => IncludeOnlyUniqueReferenced,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_includeOnlyUniqueReferenced = (Boolean)newValue;
                            OnRenderOptionChanged();
                        }
                ));
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public Boolean OnlyWorstCases
        {
            get { return m_onlyWorstCases; }
            set
            {
                SetPropertyValue(value, () => OnlyWorstCases,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_onlyWorstCases = (Boolean)newValue;
                            OnRenderOptionChanged();
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int WorstCaseCount
        {
            get { return m_worstCaseCount; }
            set
            {
                SetPropertyValue(value, () => WorstCaseCount,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_worstCaseCount = (int)newValue;

                            if (OnlyWorstCases)
                            {
                                OnRenderOptionChanged();
                            }
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int MaximumGeometryCount
        {
            get { return m_maximumGeometryCount; }
            set
            {
                SetPropertyValue(value, () => MaximumGeometryCount,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_maximumGeometryCount = (int)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Boolean OnlyGreaterThan
        {
            get { return m_onlyGreaterThan; }
            set
            {
                SetPropertyValue(value, () => OnlyGreaterThan,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_onlyGreaterThan = (Boolean)newValue;
                            OnRenderOptionChanged();
                        }
                ));
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public Boolean OnlyGreaterThanAverage
        {
            get { return m_onlyGreaterThanAverage; }
            set
            {
                SetPropertyValue(value, () => OnlyGreaterThanAverage,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_onlyGreaterThanAverage = (Boolean)newValue;
                            OnRenderOptionChanged();
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public double Average
        {
            get { return m_average; }
            set
            {
                SetPropertyValue(value, () => Average,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_average = (double)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public float GreaterThanLimit
        {
            get { return m_greaterThanLimit; }
            set
            {
                SetPropertyValue(value, () => GreaterThanLimit,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_greaterThanLimit = (float)newValue;

                            if (OnlyGreaterThan)
                            {
                                OnRenderOptionChanged();
                            }
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public IMapSection SelectedSection
        {
            get { return m_selectedSection; }
            set
            {
                SetPropertyValue(value, () => SelectedSection,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_selectedSection = (IMapSection)newValue;
                            OnSelectedSectionChanged();
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Boolean DontShowObjectOptions
        {
            get { return m_dontShowObjectOptions; }
            set
            {
                SetPropertyValue(value, () => DontShowObjectOptions,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_dontShowObjectOptions = (Boolean)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Boolean DontShowReferenceOptions
        {
            get { return m_dontShowReferenceOptions; }
            set
            {
                SetPropertyValue(value, () => DontShowReferenceOptions,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_dontShowReferenceOptions = (Boolean)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Boolean DontShowPropOptions
        {
            get { return m_dontShowPropOptions; }
            set
            {
                SetPropertyValue(value, () => DontShowPropOptions,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_dontShowPropOptions = (Boolean)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Boolean DontShowInteriorOptions
        {
            get { return m_dontShowInteriorOptions; }
            set
            {
                SetPropertyValue(value, () => DontShowInteriorOptions,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_dontShowInteriorOptions = (Boolean)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Boolean DontShowLodOptions
        {
            get { return m_dontShowLodOptions; }
            set
            {
                SetPropertyValue(value, () => DontShowLodOptions,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_dontShowLodOptions = (Boolean)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Boolean DontShowSlod1Options
        {
            get { return m_dontShowSlod1Options; }
            set
            {
                SetPropertyValue(value, () => DontShowSlod1Options,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_dontShowSlod1Options = (Boolean)newValue;
                        }
                ));
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public Boolean DontShowUniqueOptions
        {
            get { return m_dontShowUniqueOptions; }
            set
            {
                SetPropertyValue(value, () => DontShowUniqueOptions,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_dontShowUniqueOptions = (Boolean)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public SectionStatsType SectionStatsType
        {
            get { return m_statTypeFilter; }
            set
            {
                SetPropertyValue(value, () => SectionStatsType,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_statTypeFilter = (SectionStatsType)newValue;
                            OnSelectedSectionChanged();
                        }
                ));
            }
        }
        private SectionStatsType m_statTypeFilter;

        /// <summary>
        /// 
        /// </summary>
        public SectionStatsViewModel StatsViewModel
        {
            get { return m_statsViewModel; }
            set
            {
                SetPropertyValue(value, () => StatsViewModel,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_statsViewModel = (SectionStatsViewModel)newValue;
                        }
                ));
            }
        }
        private SectionStatsViewModel m_statsViewModel;
        
        /// <summary>
        /// List of stats that are required to generate this report.
        /// </summary>
        public override IEnumerable<StreamableStat> RequiredStats
        {
            get
            {
                if (m_requiredStats == null)
                {
                    m_requiredStats = new StreamableStat[] { StreamableMapSectionStat.AggregatedStats };
                }
                return m_requiredStats;
            }
        }

        #endregion // Properties

        #region MEF Imports

        /// <summary>
        /// Content Browser
        /// </summary>
        [ImportExtension(ContentBrowser.AddIn.CompositionPoints.ContentBrowser, typeof(ContentBrowser.AddIn.IContentBrowser))]
        protected ContentBrowser.AddIn.IContentBrowser ContentBrowserProxy { get; set; }

        /// <summary>
        /// A reference to view model for the map viewport
        /// </summary>
        [ImportExtension(Viewport.AddIn.CompositionPoints.MapViewport, typeof(Viewport.AddIn.IMapViewport))]
        public Viewport.AddIn.IMapViewport MapViewport { get; set; }

        #endregion // MEF Imports

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public StatisticOverlayBase(string name, string description)
            : base(name, description)
        {
            m_includePropGroup = true;
            m_includeDrawableEntities = true;
            m_includeNonDrawableEntities = true;
            m_includeHighDetailed = true;
            m_includeLod = true;
            m_includeSuperLod = true;
            m_includeReferenced = false;
            m_includeInterior = false;
            m_includeDynamic = false;
            m_onlyWorstCases = false;
            m_worstCaseCount = 1;
            m_maximumGeometryCount = 1;

            m_cachedSectionStats = new Dictionary<IMapSection, IDictionary<StatKey, IMapSectionStatistic>>();
            m_cachedCombinedStats = new Dictionary<IMapSection, IDictionary<StatKey, IMapSectionStatistic>>();
            m_statsViewModel = null;

            this.DataSourceModes[DataSource.Database] = true;
        }

        #endregion // Constructor

        #region Overrides

        /// <summary>
        /// Gets called when the user wants to view this overlay
        /// </summary>
        public override void Activated()
        {
            base.Activated();
            this.ContentBrowserProxy.GridSelectionChanged += GridSelectionChanged;
            this.SelectionChanged(this.ContentBrowserProxy.SelectedGridItems);
        }
        
        /// <summary>
        /// Gets called when the user turns off this overlay
        /// </summary>
        public override void Deactivated()
        {
            this.ContentBrowserProxy.GridSelectionChanged -= GridSelectionChanged;
            base.Deactivated();

            m_cachedSectionStats.Clear();
        }

        /// <summary>
        /// Gets the control that should be shown in the overlay details panel
        /// </summary>
        public override System.Windows.Controls.Control GetOverlayControl()
        {
            return new OverlayDetailsView();
        }

        #endregion // Overrides

        #region Event Handling

        /// <summary>
        /// Gets called when any stat render option changes and we need to redraw the geometry
        /// </summary>
        protected void OnRenderOptionChanged()
        {
            RefreshGeometry(GetValidSectionSelection(this.ContentBrowserProxy.SelectedGridItems));
        }

        /// <summary>
        /// Get called when the items selected in the content browser change
        /// </summary>
        void GridSelectionChanged(Object sender, ContentBrowser.AddIn.GridSelectionChangedEventArgs args)
        {
            this.SelectionChanged(args.NewItem);
        }

        /// <summary>
        /// Get called when the items selected in the content browser change and when
        /// the overlay is first shown
        /// </summary>
        private void SelectionChanged(IEnumerable<IAsset> selectedAssets)
        {
            if (this.ContentBrowserProxy.SelectedGridItems != null)
            {
                IDictionary<IMapSection, IMapSection> validSections = GetValidSectionSelection(this.ContentBrowserProxy.SelectedGridItems);
                IList<IMapSection> combinedSections = validSections.Keys.Union(validSections.Values).Where(item => item != null).ToList();
                
                ISet<IMapSection> sections = new HashSet<IMapSection>();
                sections.AddRange(combinedSections);
                sections.AddRange(combinedSections.SelectMany(item => item.Neighbours[(int)NeighbourMode.Distance]));

                if (sections.Any())
                {
                    CancellationTokenSource cts = new CancellationTokenSource();
                    SectionTaskContext context = new SectionTaskContext(cts.Token, sections);

                    StreamableStat[] statsToLoad = new StreamableStat[] {
                    StreamableMapSectionStat.AggregatedStats };

                    IMapHierarchy hierarchy = sections.First().MapHierarchy;
                    ITask dataLoadTask = hierarchy.CreateStatLoadTask(sections, statsToLoad);

                    if (dataLoadTask != null)
                    {
                        TaskExecutionDialog dialog = new TaskExecutionDialog();
                        TaskExecutionViewModel vm = new TaskExecutionViewModel("Generating Overlay", dataLoadTask, cts, context);
                        vm.AutoCloseOnCompletion = true;
                        dialog.DataContext = vm;
                        Nullable<bool> selectionResult = dialog.ShowDialog();
                        if (false == selectionResult)
                        {
                            return;
                        }
                    }
                }
                
                RefreshGeometry(validSections);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="selectedAssets"></param>
        /// <returns></returns>
        private IDictionary<IMapSection, IMapSection> GetValidSectionSelection(IEnumerable<IAsset> selectedAssets)
        {
            // Gather the sections that the user has selected by drilling down the hierarchy
            ISet<IMapSection> selectedSections = new HashSet<IMapSection>();

            foreach (IAsset asset in selectedAssets)
            {
                if (asset is ILevel)
                {
                    selectedSections.AddRange((asset as ILevel).GetMapHierarchy().AllSections);
                }
                else if (asset is IMapArea)
                {
                    selectedSections.AddRange((asset as IMapArea).AllDescendentSections);
                }
                else if (asset is IMapSection)
                {
                    selectedSections.Add(asset as IMapSection);
                }
            }

            // Generate a mapping between sections and prop groups based on the selected sections
            IDictionary<IMapSection, IMapSection> validSections = new Dictionary<IMapSection, IMapSection>();

            foreach (IMapSection section in selectedSections)
            {
                if (section.PropGroup != null)
                {
                    validSections.Add(section, section.PropGroup);
                }
                else if (section.PropGroup == null && section.Name.ToLower().EndsWith("_props"))
                {
                    // Make sure we haven't already added (or won't be adding) the section as part of the non-prop section
                    if (selectedSections.FirstOrDefault(item => item.Name == section.Name.ToLower().Replace("_props", "")) == null)
                    {
                        IMapSection ownerSection = section.MapHierarchy.AllSections.FirstOrDefault(item => item.PropGroup == section);
                        if (ownerSection != null)
                        {
                            validSections.Add(ownerSection, section);
                        }
                        else
                        {
                            validSections.Add(section, null);
                        }
                    }
                }
                else if (section.PropGroup == null)
                {
                    validSections.Add(section, null);
                }
            }

            return validSections;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="validSections"></param>
        protected virtual void RefreshGeometry(IDictionary<IMapSection, IMapSection> validSections)
        {
            // Keep track of which sections were selected prior to clearing the selected geometry
            ISet<IMapSection> previouslySelectedSections = new HashSet<IMapSection>();
            if (MapViewport.SelectedOverlayGeometry != null)
            {
                foreach (IMapSection section in MapViewport.SelectedOverlayGeometry.OfType<IMapSection>())
                {
                    if (section == null)
                    {
                        continue;
                    }

                    previouslySelectedSections.Add(section);
                }
            }

            MapViewport.ClearSelectedGeometry();

            // Update the geometry
            this.Geometry.BeginUpdate();
            this.Geometry.Clear();
            this.MaximumGeometryCount = 0;

            // Gather the stats we are after
            IDictionary<double, IList<IMapSection>> sections = new SortedDictionary<double, IList<IMapSection>>();
            double maximumValue = Double.MinValue;
            double minimumValue = Double.MaxValue;

            foreach (KeyValuePair<IMapSection, IMapSection> pair in validSections)
            {
                double stat = 0.0;
                if (this.IncludePropGroup == true)
                {
                    stat = GetStatisticValue(pair.Key, pair.Value);
                }
                else
                {
                    stat = GetStatisticValue(pair.Key, null);
                }

                if (!sections.ContainsKey(stat))
                {
                    sections.Add(stat, new List<IMapSection>());
                }

                if (pair.Key != null)
                {
                    sections[stat].Add(pair.Key);
                }
                else if (pair.Value != null && this.IncludePropGroup == true)
                {
                    sections[stat].Add(pair.Key);
                }

                if (stat > maximumValue)
                {
                    maximumValue = stat;
                }
                if (stat < minimumValue)
                {
                    minimumValue = stat;
                }
            }

            // Calculate the average
            double totalStat = sections.Sum(item => item.Key * item.Value.Count);
            int totalCount = sections.Sum(item => item.Value.Count);
            this.Average = totalStat / totalCount;

            if (sections.Count > 0)
            {
                Color colour = Colors.White;
                double statRange = maximumValue - minimumValue;
                int sectionRenderedCount = 0;

                foreach (KeyValuePair<double, IList<IMapSection>> sectionValue in sections.Reverse())
                {
                    if (sectionValue.Key == maximumValue && sectionValue.Key == minimumValue)
                    {
                        colour = Colors.White;
                    }
                    else if (sectionValue.Key == 0)
                    {
                        colour = Colors.White;
                    }
                    else
                    {
                        double percentage = (1.0 - (sectionValue.Key - minimumValue) / statRange);

                        // Uses green -> red range (dividing by 3 below is same as *0.33333 which equates to red in hues between 0 and 1)
                        byte h = (byte)((percentage / 3.0) * 255);
                        byte s = (byte)(0.9 * 255);
                        byte v = (byte)(0.9 * 255);
                        System.Drawing.Color drawingColor = ColourSpaceConv.HSVtoColor(new HSV(h, s, v));
                        colour = Color.FromRgb(drawingColor.R, drawingColor.G, drawingColor.B);
                    }

                    if (this.OnlyGreaterThanAverage)
                    {
                        if (sectionValue.Key > this.Average)
                        {
                            foreach (IMapSection section in sectionValue.Value)
                            {
                                if (section.VectorMapPoints != null)
                                {
                                    CreateGeometryForSection(section, Colors.Black, colour, sectionValue.Key);
                                }
                            }
                        }
                    }
                    else if (this.OnlyGreaterThan == false || (this.OnlyGreaterThan == true && this.GreaterThanLimit < sectionValue.Key))
                    {
                        foreach (IMapSection section in sectionValue.Value)
                        {
                            if (section.VectorMapPoints != null)
                            {
                                CreateGeometryForSection(section, Colors.Black, colour, sectionValue.Key);
                                sectionRenderedCount++;
                            }
                            if (this.OnlyWorstCases == true && sectionRenderedCount == this.WorstCaseCount)
                            {
                                break;
                            }
                        }
                        if (this.OnlyWorstCases == true && sectionRenderedCount == this.WorstCaseCount)
                        {
                            break;
                        }
                    }
                }
            }

            this.MaximumGeometryCount = System.Math.Max(1, sections.SelectMany(item => item.Value).Select(item => item.VectorMapPoints != null).Count());
            this.Geometry.EndUpdate();
            
            // Restore the previously selected sections
            if (previouslySelectedSections.Count > 0)
            {
                foreach (Viewport2DGeometry geom in Geometry.Where(item => previouslySelectedSections.Contains(item.PickData)))
                {
                    MapViewport.SetGeometryAsSelected(geom);
                }
            }
        }
        #endregion // Event Handling

        #region Virtual Functions
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="propGroup"></param>
        /// <returns></returns>
        protected virtual double GetStatisticValue(IMapSection section, IMapSection propGroup)
        {
            return 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sectionStat"></param>
        /// <returns></returns>
        protected virtual String GeometryUserText(KeyValuePair<double, IMapSection> sectionStat)
        {
            return sectionStat.Key.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="outlineColour"></param>
        /// <param name="fillColour"></param>
        /// <param name="value"></param>
        protected virtual void CreateGeometryForSection(IMapSection section, Color outlineColour, Color fillColour, double value)
        {
            IList<Vector2f> points = section.VectorMapPoints;
            Viewport2DShape newGeometry = new Viewport2DShape(etCoordSpace.World, String.Empty, points.ToArray(), outlineColour, 1, fillColour);
            newGeometry.PickData = section;
            newGeometry.UserText = this.GeometryUserText(new KeyValuePair<double, IMapSection>(value, section));
            this.Geometry.Add(newGeometry);
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void OnSelectedSectionChanged()
        {
            IDictionary<StatKey, IMapSectionStatistic> stats = null;
            float area = 0.0f;

            if (SelectedSection != null)
            {
                if (SectionStatsType == SectionStatsType.Section ||
                    SectionStatsType == SectionStatsType.PropGroup)
                {
                    IMapSection section = SelectedSection;
                    if (SectionStatsType == SectionStatsType.PropGroup)
                    {
                        section = SelectedSection.PropGroup;
                    }

                    if (section != null)
                    {
                        if (!m_cachedSectionStats.ContainsKey(section))
                        {
                            m_cachedSectionStats[section] = GatherSectionStats(section);
                        }

                        stats = m_cachedSectionStats[section];
                        area = section.Area;
                    }
                }
                else if (SectionStatsType == SectionStatsType.Combined)
                {
                    if (SelectedSection.PropGroup != null)
                    {
                        if (!m_cachedCombinedStats.ContainsKey(SelectedSection))
                        {
                            m_cachedCombinedStats[SelectedSection] = GatherCombinedStats(SelectedSection);
                        }

                        stats = m_cachedCombinedStats[SelectedSection];
                        area = SelectedSection.Area;
                        if (SelectedSection.PropGroup.Area > area)
                        {
                            area = SelectedSection.PropGroup.Area;
                        }
                    }
                    else
                    {
                        if (!m_cachedSectionStats.ContainsKey(SelectedSection))
                        {
                            m_cachedSectionStats[SelectedSection] = GatherSectionStats(SelectedSection);
                        }

                        stats = m_cachedSectionStats[SelectedSection];
                        area = SelectedSection.Area;
                    }
                }
            }

            if (stats != null)
            {
                StatsViewModel = new SectionStatsViewModel(stats, area);
            }
            else
            {
                StatsViewModel = null;
            }
        }
        #endregion // Virtual Functions

        #region Helpers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        private IDictionary<StatKey, IMapSectionStatistic> GatherSectionStats(IMapSection section)
        {
            return section.AggregatedStats.Where(item => !item.Key.IncludesPropGroup).ToDictionary(item => item.Key, item => item.Value);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        private IDictionary<StatKey, IMapSectionStatistic> GatherCombinedStats(IMapSection section)
        {
            return section.AggregatedStats.Where(item => item.Key.IncludesPropGroup).ToDictionary(item => item.Key, item => item.Value);
        }
        #endregion // Helpers
    } // StatisticOverlayBase
} // Workbench.AddIn.MapStatistics.Overlays
