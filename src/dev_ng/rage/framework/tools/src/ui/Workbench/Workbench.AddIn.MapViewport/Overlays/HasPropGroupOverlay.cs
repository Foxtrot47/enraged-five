﻿using System;
using System.Windows.Media;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Windows.Controls.WpfViewport2D;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using RSG.Base.Math;
using RSG.Base.ConfigParser;
using RSG.Model.Common.Map;
using MapViewport.AddIn;
using Workbench.AddIn.Services.Model;

namespace Workbench.AddIn.MapViewport.Overlays
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Viewport.AddIn.ExtensionPoints.MiscellaneousOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class HasPropGroupOverlay : LevelDependentViewportOverlay
    {
        #region Constants
        /// <summary>
        /// Name/Description for the overlay
        /// </summary>
        private const String c_name = "Has Prop Group";
        private const String c_description = "Shows the individual containers for the map with the containers that include a prop group coloured in green and the onces that don't in red.";

        /// <summary>
        /// Colours to use for the various map sections
        /// </summary>
        private static readonly Color[] c_colours = {
                                                      Color.FromArgb(255, 128, 0, 0),
                                                      Color.FromArgb(255, 0, 128, 0),
                                                    };
        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// Level Browser
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LevelBrowser, typeof(ILevelBrowser))]
        Lazy<ILevelBrowser> LevelBrowserProxy { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(CompositionPoints.ConfigurationService, typeof(Workbench.AddIn.Services.IConfigurationService))]
        Lazy<Workbench.AddIn.Services.IConfigurationService> Config { get; set; }
        #endregion // MEF Imports

        #region Constructors
        /// <summary>
        /// Default Constructor
        /// </summary>
        public HasPropGroupOverlay()
            : base(c_name, c_description)
        {
        }
        #endregion // Constructors

        #region Overrides

        /// <summary>
        /// Gets called when the user wants to view this overlay
        /// </summary>
        public override void Activated()
        {
            this.Geometry.BeginUpdate();
            this.Geometry.Clear();

            if (LevelBrowserProxy.Value.SelectedLevel != null)
            {
                IMapHierarchy hierarchy = LevelBrowserProxy.Value.SelectedLevel.MapHierarchy;

                if (hierarchy != null)
                {
                    IDictionary<string, IMapSection> allSections = hierarchy.AllSections.ToDictionary(item => item.Name.ToLower());

                    foreach (KeyValuePair<string, IMapSection> pair in allSections)
                    {
                        if (pair.Value.VectorMapPoints != null)
                        {
                            String propGroupName = pair.Key + "_props";

                            Color colour = c_colours[0];
                            if (allSections.Keys.Contains(propGroupName))
                            {
                                colour = c_colours[1];
                            }

                            Viewport2DShape newGeometry = new Viewport2DShape(etCoordSpace.World, "Filled shape for section", pair.Value.VectorMapPoints.ToArray(), Colors.Black, 1, colour);
                            newGeometry.UserText = pair.Key;
                            this.Geometry.Add(newGeometry);
                        }
                    }
                }
            }

            this.Geometry.EndUpdate();
        }

        /// <summary>
        /// Gets called when the user turns off this overlay
        /// </summary>
        public override void Deactivated()
        {
            this.Geometry.Clear();
        }
        #endregion // Overrides
    } // HasPropGroupOverlay
} // Workbench.AddIn.MapViewport.Overlays
