﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LevelBrowser.AddIn.MapBrowser.Commands
{
    public class MaxVersionPickerViewModel
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, string> Versions
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public string SelectedVersion
        {
            get;
            set;
        }
        #endregion

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="versions"></param>
        public MaxVersionPickerViewModel(IDictionary<string, string> versions)
        {
            this.Versions = new Dictionary<string, string>(versions);
        }

        #endregion
    }
}
