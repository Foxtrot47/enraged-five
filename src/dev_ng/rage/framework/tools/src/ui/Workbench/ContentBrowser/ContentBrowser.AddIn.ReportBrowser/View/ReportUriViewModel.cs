﻿using System;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using RSG.Model.Report;

using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Input;
using System.IO;

namespace Workbench.AddIn.ReportBrowser
{
    /// <summary>
    /// View model for the ReportUri
    /// </summary>
    public class ReportUriViewModel 
        : ViewModelBase
    {
        #region Properties and associated member data
        /// <summary>
        /// 
        /// </summary>
        public bool IsNavigating
        {
            get { return m_isNavigating; }
            set
            {
                m_isNavigating = value;
                OnPropertyChanged("IsNavigating");
            }
        }
        private bool m_isNavigating;

        /// <summary>
        /// Name of the report
        /// </summary>
        public string Name
        {
            get { return m_name; }
        }
        private string m_name;

        /// <summary>
        /// Address the web browser should navigate to
        /// </summary>
        public string Address
        {
            get { return m_address; }
            set
            {
                if (m_address != value)
                {
                    m_address = value;
                    OnPropertyChanged("Address");
                }
            }
        }
        private string m_address;

        /// <summary>
        /// Stream of data
        /// </summary>
        public Stream Stream
        {
            get { return m_stream; }
        }
        private Stream m_stream;
        #endregion // Properties and associated member data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ReportUriViewModel(IReport report)
        {
            // Create model
            if (report is IReportUriProvider)
            {
                Uri uri = (report as IReportUriProvider).Uri;
                m_address = (uri == null ? null : uri.OriginalString);
            }
            else if (report is IReportStreamProvider)
            {
                m_stream = new MemoryStream();
                if ((report as IReportStreamProvider).Stream != null)
                {
                    (report as IReportStreamProvider).Stream.CopyTo(m_stream);
                }

                m_stream.Position = 0;
            }

            m_name = (report as IReport).Name;

            IsNavigating = true;
        }
        #endregion // Constructor(s)
    } // ReportUriViewModel
} // Workbench.AddIn.ReportBrowser
