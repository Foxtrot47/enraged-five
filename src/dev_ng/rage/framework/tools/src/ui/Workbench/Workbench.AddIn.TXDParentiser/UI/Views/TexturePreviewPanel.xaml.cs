﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Workbench.AddIn.TXDParentiser;
using RSG.Model.GlobalTXD.ViewModel;

namespace Workbench.AddIn.TXDParentiser.UI.Views
{
    ///// <summary>
    ///// Interaction logic for TexturePreviewPanel.xaml
    ///// </summary>
    //public partial class TexturePreviewPanel : UserControl, INotifyPropertyChanged
    //{
    //    #region Property change events

    //    /// <summary>
    //    /// Property changed event fired when the any of the properties change
    //    /// </summary>
    //    public event PropertyChangedEventHandler PropertyChanged;

    //    /// <summary>
    //    /// Create the OnPropertyChanged method to raise the event
    //    /// </summary>
    //    /// <param name="name"></param>
    //    protected void OnPropertyChanged(string name)
    //    {
    //        PropertyChangedEventHandler handler = PropertyChanged;
    //        if (handler != null)
    //        {
    //            handler(this, new PropertyChangedEventArgs(name));
    //        }
    //    }

    //    #endregion // Property change events

    //    public String TextureName
    //    {
    //        get { return m_textureName; }
    //        set { m_textureName = value; OnPropertyChanged("TextureName"); }
    //    }
    //    private String m_textureName;

    //    public String StreamName
    //    {
    //        get { return m_streamName; }
    //        set { m_streamName = value; OnPropertyChanged("StreamName"); }
    //    }
    //    private String m_streamName;

    //    public float TextureSize
    //    {
    //        get { return m_textureSize; }
    //        set { m_textureSize = value; OnPropertyChanged("TextureSize"); }
    //    }
    //    private float m_textureSize;

    //    public TextureViewModel Source
    //    {
    //        get { return m_Source; }
    //        set { m_Source = value; OnPropertyChanged("Source"); }
    //    }
    //    private TextureViewModel m_Source;


    //    public TexturePreviewPanel()
    //    {
    //        InitializeComponent();

    //        this.TextureSize = 0.0f;
    //    }

    //    private void OnDataContextChanged(Object sender, DependencyPropertyChangedEventArgs e)
    //    {
    //        if (!(e.NewValue is TextureViewModel))
    //            return;

    //        this.Source = e.NewValue as TextureViewModel;

    //        this.TextureName = System.IO.Path.GetFileNameWithoutExtension(this.Source.Filename);

    //        if (String.IsNullOrEmpty(this.Source.Model.StreamName))
    //        {
    //            this.StreamName = "Not found in stream";
    //        }
    //        else
    //        {
    //            this.StreamName = this.Source.Model.StreamName;
    //            this.TextureSize = this.Source.TextureSize;
    //        }
    //    }
    //}
}
