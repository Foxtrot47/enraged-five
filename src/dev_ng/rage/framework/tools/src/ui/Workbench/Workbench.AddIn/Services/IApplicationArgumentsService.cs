﻿using System;
using System.Collections.Generic;

namespace Workbench.AddIn.Services
{

    /// <summary>
    /// Application arguments service interface.
    /// </summary>
    /// This service interface provides other modules with information about 
    /// the arguments used to startup the application.
    /// 
    public interface IApplicationArgumentsService
    {
        #region Events
        /// <summary>
        /// Event raised whenever the system wants the startup arguments processed.
        /// There may be multiple handlers for this in various components
        /// around the system.
        /// </summary>
        event EventHandler<ArgumentsEventArgs> ProcessStartupArguments;

        /// <summary>
        /// Event raised whenever new arguments are injected into the Workbench
        /// application (if single instance set).
        /// </summary>
        event EventHandler<ArgumentsEventArgs> InjectedArgumentsChanged;
        #endregion // Events

        #region Properties
        /// <summary>
        /// Dictionary hash of startup application arguments.
        /// </summary>
        Dictionary<String, Object> StartupArguments { get; }

        /// <summary>
        /// Array of application trailing arguments.
        /// </summary>
        String[] StartupTrailingArguments { get; }

        /// <summary>
        /// Latest injected arguments from another instance (if single instance set).
        /// </summary>
        Dictionary<String, Object> InjectedArguments { get; }

        /// <summary>
        /// Latest injected trailing arguments from another instance (if single instance set).
        /// </summary>
        String[] InjectedTrailingArguments { get; }
        #endregion // Properties

        #region Controller Methods
        /// <summary>
        /// External trigger for application to notify other systems to process
        /// the startup arguments (via the ProcessStartupArguments event).
        /// </summary>
        void TriggerProcessStartupArguments();

        /// <summary>
        /// Invoke an injection event after processing and updating the internal
        /// injected argument data structures.
        /// </summary>
        /// <param name="injectedArgs"></param>
        void InjectArguments(String[] injectedArgs);
        #endregion // Controller Methods
    }

    /// <summary>
    /// Arguments event args class.  Used for startup and injection events.
    /// </summary>
    public class ArgumentsEventArgs : EventArgs
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<String, Object> Arguments { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public String[] TrailingArguments { get; set; }
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public ArgumentsEventArgs(Dictionary<String, Object> args, String[] trailing)
        {
            this.Arguments = args;
            this.TrailingArguments = trailing;
        }
        #endregion // Constructor(s)
    }

} // Workbench.AddIn.Services
