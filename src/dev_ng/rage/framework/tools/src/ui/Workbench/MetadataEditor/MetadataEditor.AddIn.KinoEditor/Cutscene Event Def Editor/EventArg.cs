﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Collections;
using System.ComponentModel;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;

namespace MetadataEditor.AddIn.KinoEditor.Cutscene_Event_Def_Editor
{
    public class EventArgCollection : ObservableCollection<EventArg>
    {
    }

    public class AttributeCollection : ObservableCollection<Attribute>
    {
        public AttributeCollection Clone()
        {
            AttributeCollection copy = new AttributeCollection();
            for (int i = 0; i < this.Items.Count; ++i)
            {
                Attribute attr = new Attribute();

                attr.Name = this.Items[i].Name;
                attr.Type = this.Items[i].Type;
                attr.DefaultValue = this.Items[i].DefaultValue;

                copy.Add(attr);
            }

            return copy;
        }
    }

    public class Attribute
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string DefaultValue { get; set; }

        #region INotifyPropertyChanged Members

        /// <summary>
        /// Raised when a property on this object has a new value.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises this object's PropertyChanged event.
        /// </summary>
        /// <param name="propertyName">The property that has a new value.</param>
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName);
                handler(this, e);
            }
        }

        #endregion // INotifyPropertyChanged Members
    }

    public class AttributeType
    {
        public AttributeType(string name, int id)
        {
            Name = name;
            TypeID = id;
        }

        public int TypeID { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }

    public class EventArg : RSG.Base.Editor.ModelBase
    {
        public EventArg()
        {
            Attributes = new AttributeCollection();
        }

        public AttributeCollection Attributes
        {
            get;
            set;
        }

        public string Name 
        { 
            get
            {
                return _Name;
            } 

            set
            {
                SetPropertyValue(value, () => this.Name,
                    new PropertySetDelegate(delegate(Object newValue) { _Name = (string)newValue; }));
            } 
        }
        private string _Name = String.Empty;

        public override string ToString()
        {
            return Name;
        }
    }
}
