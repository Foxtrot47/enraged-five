﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Report;

namespace Workbench.AddIn.MapStatistics.Reports
{
    [ExportExtension(Report.AddIn.ExtensionPoints.MapStatsReport, typeof(IReport))]
    class AssetTypeCountCSVExportReport : RSG.Model.Report.Reports.Map.AssetTypeCountReport
    {
    } 


}
