﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using RSG.Base.Collections;
using RSG.Base.Windows.Controls;
using Workbench.AddIn.MapDebugging.Overlays;

namespace Workbench.AddIn.MapDebugging.OverlayViews
{
    /// <summary>
    /// Interaction logic for SpawnPointOverlayView.xaml
    /// </summary>
    public partial class SpawnPointOverlayView : UserControl
    {
        public SpawnPointOverlayView()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Handles when the selection of spawn point filters has changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SpawnPointFilters_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var listBox = sender as ListBox;
            if (listBox == null) return;

            SpawnPointOverlay viewModel = listBox.DataContext as SpawnPointOverlay;
            if (viewModel == null) return;

            viewModel.FilterTypes.Clear();

            ObservableCollection<SpawnPointFilterItem> selectedItems = new ObservableCollection<SpawnPointFilterItem>();
            foreach (SpawnPointFilterItem spawnPointType in viewModel.SpawnPointTypes)
            {
                foreach (SpawnPointFilterItem selectedItem in listBox.SelectedItems)
                {
                    if (String.Compare(spawnPointType.Name, selectedItem.Name, true) == 0)
                    {
                        selectedItems.Add(spawnPointType);
                    }
                }
            }

            viewModel.FilterTypes = selectedItems;
        }

        /// <summary>
        /// Handles when the selection of spawn point model types has changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SpawnPointModelTypes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var listBox = sender as ListBox;
            if (listBox == null) return;

            SpawnPointOverlay viewModel = listBox.DataContext as SpawnPointOverlay;
            if (viewModel == null) return;

            viewModel.FilterTypes.Clear();
            viewModel.FilterModelTypes.Clear();

            ObservableCollection<SpawnPointFilterItem> selectedItems = new ObservableCollection<SpawnPointFilterItem>();
            foreach (SpawnPointFilterItem spawnPointGroup in viewModel.SpawnPointModelTypes)
            {
                foreach (SpawnPointFilterItem selectedItem in listBox.SelectedItems)
                {
                    if (String.Compare(spawnPointGroup.Name, selectedItem.Name, true) == 0)
                    {
                        selectedItems.Add(spawnPointGroup);
                    }
                }
            }

            viewModel.FilterModelTypes = selectedItems;
        }

        /// <summary>
        /// Handles when the selection of spawn point groups has changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SpawnPointGroups_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var listBox = sender as ListBox;
            if (listBox == null) return;

            SpawnPointOverlay viewModel = listBox.DataContext as SpawnPointOverlay;
            if (viewModel == null) return;

            viewModel.FilterTypes.Clear();

            ObservableCollection<SpawnPointGroupItem> selectedItems = new ObservableCollection<SpawnPointGroupItem>();
            foreach (SpawnPointGroupItem spawnPointGroup in viewModel.SpawnPointGroups)
            {
                foreach (SpawnPointGroupItem selectedItem in listBox.SelectedItems)
                {
                    if (String.Compare(spawnPointGroup.Name, selectedItem.Name, true) == 0)
                    {
                        selectedItems.Add(spawnPointGroup);
                    }
                }
            }

            viewModel.GroupTypes = selectedItems;
        }


        /// <summary>
        /// Keeps both filters (types and groups) mutually exclusive.
        /// </summary>
        private void OnFilterTypeToggled(object sender, RoutedEventArgs e)
        {
            //FilterGroupCheckBox.IsChecked = !FilterTypeCheckBox.IsChecked;
        }

        /// <summary>
        /// Keeps both filters (types and groups) mutually exclusive.
        /// </summary>
        private void OnFilterGroupToggled(object sender, RoutedEventArgs e)
        {
            //FilterTypeCheckBox.IsChecked = !FilterGroupCheckBox.IsChecked;
        }

        private void OnFilterModelTypeToggled(object sender, RoutedEventArgs e)
        {
            
        }

        /// <summary>
        /// Handles the selected item changed event on the model set combo box
        /// so we know when to commit data to the view-model.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void ModelSet_SelectionChanged(object sender, RoutedEventArgs e)
        {
            var comboBox = sender as ComboBox;
            if (comboBox == null || comboBox.SelectedItem == null || String.IsNullOrWhiteSpace(comboBox.SelectedItem.ToString()))
            {
                return;
            }

            SpawnPointOverlay viewModel = this.DataContext as SpawnPointOverlay;
            if (viewModel == null) return;

            viewModel.ModelSetFilter = comboBox.SelectedItem.ToString();
        }

        /// <summary>
        /// Handles when the color changes on any color picker.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ColorPicker_SelectedColorChanged(object sender, RoutedEventArgs e)
        {
            SpawnPointOverlay viewModel = this.DataContext as SpawnPointOverlay;
            if (viewModel == null) return;

            viewModel.EnableUpdateOverlay = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdateOverlay_Clicked(object sender, RoutedEventArgs e)
        {
            SpawnPointOverlay viewModel = this.DataContext as SpawnPointOverlay;
            if (viewModel == null) return;

            viewModel.ForceUpdateFilter = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExportReport_Clicked(object sender, RoutedEventArgs e)
        {
            SpawnPointOverlay viewModel = this.DataContext as SpawnPointOverlay;
            if (viewModel == null) return;

            viewModel.ExportReport();
        }

        /// <summary>
        /// Click event for the model type totals export button.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void ModelTypeTotals_Click(object sender, RoutedEventArgs e)
        {
            SpawnPointOverlay viewModel = this.DataContext as SpawnPointOverlay;
            if (viewModel == null) return;

            viewModel.ExportModelTypeTotals();
        }
    }
}
