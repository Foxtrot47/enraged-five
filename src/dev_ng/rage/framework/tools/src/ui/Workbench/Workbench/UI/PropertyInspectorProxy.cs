﻿using System;
using System.ComponentModel.Composition;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn;
using Workbench.AddIn.Commands;
using Workbench.AddIn.UI.Layout;
using Workbench.UI.Toolbar;

namespace Workbench.UI
{
    [ExportExtension(Workbench.AddIn.ExtensionPoints.ToolWindowProxy, typeof(IToolWindowProxy))]
    [ExportExtension(Workbench.AddIn.CompositionPoints.PropertyInspectorService, typeof(Workbench.AddIn.Services.IPropertyInspector))]
    [ExportExtension(Workbench.AddIn.ExtensionPoints.ToolbarStandard, typeof(IWorkbenchCommand))]
    class PropertyInspectorProxy : IToolWindowProxy, Workbench.AddIn.Services.IPropertyInspector, IPartImportsSatisfiedNotification
    {
        #region MEF Imports
        
        /// <summary>
        /// Layout manager reference.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager, typeof(ILayoutManager))]
        private ILayoutManager LayoutManager { get; set; }
        
        #endregion // MEF Imports

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public PropertyInspectorProxy()
        {
            this.Name = PropertyInspectorViewModel.PROPERTYINSPECTOR_NAME;
            this.Header = PropertyInspectorViewModel.PROPERTYINSPECTOR_TITLE;
            SetImageFromBitmap(Workbench.Resources.Images.Properties);
            this.RelativeToolBarID = new Guid(Workbench.AddIn.CompositionPoints.ToolWindowSeparator);
            this.Direction = AddIn.Services.Direction.After;
            this.ToolTip = "Properties Window";
            this.IsToggle = true;
            this.KeyGesture = new System.Windows.Input.KeyGesture(System.Windows.Input.Key.F4);
            this.DelayCreation = false;
        }

        #endregion // Constructor

        #region Public Functions

        /// <summary>
        /// A abstract function that is overriden by the plugin to create the
        /// actual instance of the tool window and pass it back as a out paramter
        /// </summary>
        protected override Boolean CreateToolWindow(out IToolWindowBase toolWindow)
        {
            toolWindow = new PropertyInspectorView();
            return true;
        }

        #endregion // Public Functions

        #region IPropertyInspector Interface

        /// <summary>
        /// Pushes a object onto the property grid
        /// </summary>
        public void Push(Object item)
        {
            PropertyInspectorView view = this.GetToolWindow() as PropertyInspectorView;
            if (view != null && view.PropertyGrid != null)
            {
                view.PropertyGrid.Push(item);
            }
        }

        /// <summary>
        /// Pushes the given collection onto the property grid
        /// </summary>
        public void PushRange(IEnumerable<Object> collection)
        {
            PropertyInspectorView view = this.GetToolWindow() as PropertyInspectorView;
            if (view != null && view.PropertyGrid != null)
            {
                view.PropertyGrid.PushRange(collection);
            }
        }

        /// <summary>
        /// Pops an object off the property grid
        /// </summary>
        public void Pop(Object item)
        {
            PropertyInspectorView view = this.GetToolWindow() as PropertyInspectorView;
            if (view != null && view.PropertyGrid != null)
            {
                view.PropertyGrid.Pop(item);
            }
        }

        /// <summary>
        /// Pops the given collection off the property grid
        /// </summary>
        public void PopRange(IEnumerable<Object> collection)
        {
            PropertyInspectorView view = this.GetToolWindow() as PropertyInspectorView;
            if (view != null && view.PropertyGrid != null)
            {
                view.PropertyGrid.PopRange(collection);
            }
        }

        /// <summary>
        /// Clears the property grid
        /// </summary>
        public void Clear()
        {
            PropertyInspectorView view = this.GetToolWindow() as PropertyInspectorView;
            if (view != null && view.PropertyGrid != null)
            {
                view.PropertyGrid.Clear();
            }
        }

        #endregion // IPropertyInspector Interface

        #region IPartImportsSatisfiedNotification

        public void OnImportsSatisfied()
        {
            this.LayoutManager.LayoutUpdated += new EventHandler(LayoutManager_LayoutUpdated);
        }

        private void LayoutManager_LayoutUpdated(Object sender, EventArgs e)
        {
            if (this.HasBeenCreated == true)
            {
                this.IsChecked = LayoutManager.IsVisible(this.GetToolWindow());
            }
        }

        #endregion // IPartImportsSatisfiedNotification

        #region ICommand Implementation

        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
            IToolWindowBase toolWindow = this.GetToolWindow();
            if (toolWindow != null)
            {
                if (this.LayoutManager.IsVisible(toolWindow))
                    toolWindow.Hide();
                else
                {
                    if (toolWindow.WasFloatingWindowWhenHidden == true)
                        toolWindow.ShowAsFloatingWindow(true);
                    else
                        this.LayoutManager.ShowToolWindow(toolWindow);

                    if (toolWindow.FloatByDefault == true && toolWindow.HasBeenShown == false)
                    {
                        toolWindow.FloatingWindowSize = toolWindow.DefaultFloatSize;
                        toolWindow.ShowAsFloatingWindow(true);
                    }
                }
            }
        }

        #endregion // ICommand Implementation
    } // PropertyInspectorProxy
} // Workbench.UI
