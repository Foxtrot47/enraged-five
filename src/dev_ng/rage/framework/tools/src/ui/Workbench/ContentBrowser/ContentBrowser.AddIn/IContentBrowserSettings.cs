﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn.Services;

namespace ContentBrowser.AddIn
{
    /// <summary>
    /// 
    /// </summary>
    public interface IContentBrowserSettings : ISettings
    {
        string DataSource { get; set; }
        string Build { get; set; }
        string Level { get; set; }
        string Platform { get; set; }
        bool PreviewPaneVisible { get; set; }
    } // IContentBrowserSettings
}
