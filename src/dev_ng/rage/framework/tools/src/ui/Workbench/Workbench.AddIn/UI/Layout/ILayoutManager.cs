﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using AvalonDock;

namespace Workbench.AddIn.UI.Layout
{
    public delegate void ContentOpenedHandler(IContentBase openedContent);
    public delegate void ContentClosedHandler(IContentBase closedContent);
    public delegate void NewDocumentCreatedHandler(IDocumentBase document);
    public delegate Boolean DocumentClosing(IDocumentBase document);
    public delegate void DocumentClosed(IDocumentBase document);
    
    /// <summary>
    /// 
    /// </summary>
    public interface ILayoutManager
    {
        #region Events

        /// <summary>
        /// 
        /// </summary>
        event EventHandler Loaded;

        /// <summary>
        /// 
        /// </summary>
        event EventHandler Unloaded;

        /// <summary>
        /// 
        /// </summary>
        event EventHandler LayoutUpdated;

        /// <summary>
        /// 
        /// </summary>
        event EventHandler ActiveDocumentChanged;

        /// <summary>
        /// 
        /// </summary>
        event EventHandler ActiveDocumentChanging;

        /// <summary>
        /// 
        /// </summary>
        event EventHandler ActiveContentChanged;

        /// <summary>
        /// 
        /// </summary>
        event ContentOpenedHandler ContentOpened;

        /// <summary>
        /// 
        /// </summary>
        event ContentClosedHandler ContentClosed;

        /// <summary>
        /// 
        /// </summary>
        event NewDocumentCreatedHandler DocumentCreated;
        
        /// <summary>
        /// 
        /// </summary>
        event DocumentClosed DocumentClosed;

        /// <summary>
        /// 
        /// </summary>
        event EventHandler Exiting;

        #endregion // Events

        #region Properties

        /// <summary>
        /// All currently open IDocument's.
        /// </summary>
        ReadOnlyCollection<IDocumentBase> Documents { get; }

        /// <summary>
        /// All currently available IToolWindow's.
        /// </summary>
        ReadOnlyCollection<IToolWindowBase> ToolWindows { get; }

        /// <summary>
        /// A dictionary of the saved layouts from the settings file that can be used to create the
        /// window layout buttons.
        /// </summary>
        Dictionary<String, String> SavedLayouts { get; }

        #endregion // Properties

        #region Controller Methods

        #region Content Methods

        IContentBase GetActiveContent();

        #endregion // Content Methods

        #region ToolWindow Methods

        void ShowToolWindow(IToolWindowBase window);

        void ShowAsFloatingWindow(IToolWindowBase window);

        void HideToolWindow(IToolWindowBase window);

        bool IsVisible(IToolWindowBase window);

        void HideAllToolWindows();

        #endregion // ToolWindow Methods

        #region Document Methods

        void ShowDocument(IDocumentBase document);

        void CloseDocument(IDocumentBase document);

        void CloseAllDocuments();

        /// <summary>
        /// Currently active IDocument.
        /// </summary>
        IDocumentBase ActiveDocument();

        bool IsActiveDocument(IDocumentBase document);

        bool IsActiveContent(IDocumentBase document);

        bool IsVisible(IDocumentBase document);

        /// <summary>
        /// Create a new horizontal tab group for the specified document.
        /// </summary>
        void NewHorizontalTabGroup(IDocumentBase document);

        /// <summary>
        /// Create a new vertical tab group for the specified document.
        /// </summary>
        void NewVerticalTabGroup(IDocumentBase document);

        #endregion // Document Methods

        #region UI.Layout Serialisation

        /// <summary>
        /// Save layout; returning string serialised representation.
        /// </summary>
        /// <returns></returns>
        String SaveLayout(String name);

        /// <summary>
        /// Saves the currently saved layouts in the user settings
        /// </summary>
        void SaveLayouts();

        /// <summary>
        /// Restore layout from serialised representation.
        /// </summary>
        /// <param name="blob"></param>
        void RestoreLayout(String blob);

        /// <summary>
        /// Opens the tool windows defined the the specified blob.
        /// </summary>
        /// <param name="blob"></param>
        void MergeToolWindows(String blob);
        
        /// <summary>
        /// Restores the layout to a saved layout by using the
        /// given name
        /// </summary>
        void RestoreSavedLayout(String name);

        /// <summary>
        /// Returns the layout as it would be having been
        /// serialised.
        /// </summary>
        String GetLayoutSerialization();

        #endregion // UI.Layout Serialisation

        void OnExit();

        #endregion // Controller Methods
    }

} // Workbench.AddIn.UI.Layout namespace
