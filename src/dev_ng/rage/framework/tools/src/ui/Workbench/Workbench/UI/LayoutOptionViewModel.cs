﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Workbench.UI
{
    public class LayoutOptionViewModel
    {
        #region Properties
        public string Name { get; private set; }
        public FontWeight FontWeight { get; private set; }
        public string Xml { get; set; }
        #endregion

        public LayoutOptionViewModel(string name, string xml, bool isBold)
        {
            this.Name = name;
            this.Xml = xml;
            if (isBold)
            {
                this.FontWeight = FontWeights.Bold;
            }
            else
            {
                this.FontWeight = FontWeights.Normal;
            }
        }
    }
}
