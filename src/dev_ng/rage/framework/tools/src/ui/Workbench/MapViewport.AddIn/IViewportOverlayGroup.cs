﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using RSG.Base.Collections;

namespace Viewport.AddIn
{
    /// <summary>
    /// 
    /// </summary>
    public interface IViewportOverlayGroup : IComparable<IViewportOverlayGroup>
    {
        #region Properties
        /// <summary>
        /// Name of the overlay group.
        /// </summary>
        String Name { get; }

        /// <summary>
        /// List of overlays that are part of this group.
        /// </summary>
        ObservableCollection<IViewportOverlay> Overlays { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Called when the overlay group becomes the active group
        /// </summary>
        void Activated();

        /// <summary>
        /// Called when the overlay group is no longer the active group
        /// </summary>
        void Deactivated();
        #endregion // Methods
    } // IViewportOverlayGroup
}
