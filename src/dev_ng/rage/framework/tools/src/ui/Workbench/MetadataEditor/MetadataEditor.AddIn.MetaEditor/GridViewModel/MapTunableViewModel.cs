﻿using System;
using RSG.Metadata.Data;
using RSG.Metadata.Parser;
using RSG.Base.Editor.Command;
using System.ComponentModel;
using System.Collections.ObjectModel;
using MetadataEditor.AddIn.MetaEditor.ViewModel;
using System.Collections.Specialized;

namespace MetadataEditor.AddIn.MetaEditor.GridViewModel
{
    /// <summary>
    /// Tunable map view model.
    /// </summary>
    public class MapTunableViewModel : GridTunableViewModel
    {
        #region Private member fields

        private System.Collections.ObjectModel.ObservableCollection<GridTunableViewModel> m_members;

        #endregion

        #region Properties

        /// <summary>
        /// Members.
        /// </summary>
        public System.Collections.ObjectModel.ObservableCollection<GridTunableViewModel> Members
        {
            get { return m_members; }
            set
            {
                SetPropertyValue(value, () => this.Members,
                    new PropertySetDelegate(delegate(Object newValue) { m_members = (System.Collections.ObjectModel.ObservableCollection<GridTunableViewModel>)newValue; }));
            }
        }

        /// <summary>
        /// FriendlyTypeName
        /// </summary>
        public new String FriendlyTypeName
        {
            get
            {
                if ((this.Model.Definition as MapMember).ElementType is PointerMember)
                {
                    return ((this.Model.Definition as MapMember).ElementType as PointerMember).ObjectTypeName;
                }
                else
                {
                    return (this.Model.Definition as MapMember).ElementType.FriendlyTypeName;
                }
            }
        }

        #endregion

        #region Constructor(s)

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="parent">Parent view model.</param>
        /// <param name="tunable">Map tunable.</param>
        /// <param name="root">Root meta file view model.</param>
        public MapTunableViewModel(GridTunableViewModel parent, MapTunable tunable, MetaFileViewModel root)
            : base(parent, tunable, root)
        {
            CollectionChangedEventManager.AddListener((tunable as MapTunable).Items, this);
            m_members = new ObservableCollection<GridTunableViewModel>();

            foreach (var t in tunable.Items)
            {
                if (t.Value.Definition.HideWidgets == false)
                {
                    GridTunableViewModel vm = TunableViewModelFactory.Create(this, t.Value, root);
                    Members.Add(vm);
                }
            }

            if (Model.ParentModel is PointerTunable)
            {
                PropertyChangedEventManager.AddListener(Model.ParentModel as ITunable, this, "Value");
            }
        }

        #endregion 
    } 
}
