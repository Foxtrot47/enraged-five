﻿using MetadataEditor.AddIn.MetaEditor.ViewModel;
using RSG.Metadata.Data;

namespace MetadataEditor.AddIn.MetaEditor.GridViewModel
{
    /// <summary>
    /// The view model that wraps around a  <see cref="EnumTunable"/> object. Used for the
    /// grid view.
    /// </summary>
    public class EnumTunableViewModel : GridTunableViewModel
    {
        #region Constructor
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="MetadataEditor.AddIn.MetaEditor.GridViewModel.EnumTunableViewModel"/>
        /// class.
        /// </summary>
        public EnumTunableViewModel(GridTunableViewModel parent, EnumTunable m, MetaFileViewModel root)
            : base(parent, m, root)
        {
        }
        #endregion // Constructor
    } // EnumTunableViewModel
} // MetadataEditor.AddIn.MetaEditor.GridViewModel