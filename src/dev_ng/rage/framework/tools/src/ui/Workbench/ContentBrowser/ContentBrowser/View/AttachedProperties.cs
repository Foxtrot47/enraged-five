﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace ContentBrowser.View
{
    public static class DoubleClickCommand
    {
        public static DependencyProperty DoubleClickCommandParameterProperty = DependencyProperty.RegisterAttached(
            "DoubleClickCommandParameter",
            typeof(Object),
            typeof(DoubleClickCommand));

        public static void SetDoubleClickCommandParameter(DependencyObject element, Object value)
        {
            element.SetValue(DoubleClickCommandParameterProperty, value);
        }

        public static Object GetDoubleClickCommandParameter(DependencyObject element)
        {
            return (Object)element.GetValue(DoubleClickCommandParameterProperty);
        }

        public static DependencyProperty DoubleClickCommandProperty = DependencyProperty.RegisterAttached(
            "DoubleClickCommand",
            typeof(System.Windows.Input.ICommand),
            typeof (DoubleClickCommand),
            new PropertyMetadata(OnDoubleClickCommandPropertyChanged));

        public static void SetDoubleClickCommand(DependencyObject element, System.Windows.Input.ICommand value)
        {
            element.SetValue(DoubleClickCommandProperty, value);
        }

        public static System.Windows.Input.ICommand GetDoubleClickCommand(DependencyObject element)
        {
            return (System.Windows.Input.ICommand)element.GetValue(DoubleClickCommandProperty);
        }

        private static void OnDoubleClickCommandPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            if (sender is System.Windows.UIElement)
            {
                if (e.OldValue is System.Windows.Input.ICommand)
                {
                    (sender as System.Windows.UIElement).PreviewMouseLeftButtonDown -= DoubleClickCommand_MouseLeftButtonDown;
                }

                if (e.NewValue is System.Windows.Input.ICommand)
                {
                    (sender as System.Windows.UIElement).PreviewMouseLeftButtonDown += DoubleClickCommand_MouseLeftButtonDown;
                }
            }
        }

        static void DoubleClickCommand_MouseLeftButtonDown(Object sender, MouseButtonEventArgs e)
        {
            System.Diagnostics.Debug.Print("{0}", e.ClickCount.ToString());
            if (e.ClickCount >= 2)
            {
                if (sender is System.Windows.UIElement)
                {
                    Object parameter = GetDoubleClickCommandParameter(sender as System.Windows.UIElement);
                    System.Windows.Input.ICommand command = GetDoubleClickCommand(sender as DependencyObject);
                    if (command != null)
                    {
                        if (command.CanExecute(parameter))
                        {
                            command.Execute(parameter);
                        }
                    }
                }
            }
        }
    }
}
