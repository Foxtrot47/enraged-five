﻿using System;
using System.Windows.Media.Imaging;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ContentBrowser.AddIn;
using System.Text.RegularExpressions;
using Workbench.AddIn.Services;
using RSG.Model.Common;
using Workbench.AddIn.UI.Layout;

namespace ContentBrowser.AddIn
{
    /// <summary>
    /// 
    /// </summary>
    public interface IBrowserViewModel : IWeakEventListener, INotifyPropertyChanged
    {
        #region Properties

        BrowserBase Model { get; }
        
        String Name { get; }
        
        Boolean IsSelected { get; set; }

        Object SelectedItem { get; }

        /// <summary>
        /// The icon that will be displayed to represent this browser
        /// </summary>
        BitmapImage BrowserIcon { get; }

        #endregion // Properties

        #region Methods
        /// <summary>
        /// Clears the current selection in the tree view of assets
        /// </summary>
        void ClearSelection();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        List<ISearchResult> FindResults(ISearchQuery query);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="asset"></param>
        void SelectAsset(IAsset asset);
        #endregion // Methods
    } // IBrowserViewModel
} // ContentBrowser.ViewModel
