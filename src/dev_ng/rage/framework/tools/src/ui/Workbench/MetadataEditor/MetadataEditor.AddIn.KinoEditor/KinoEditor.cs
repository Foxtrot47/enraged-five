﻿using System;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Diagnostics;
using Workbench.AddIn;
using Workbench.AddIn.UI.Layout;
using MetadataEditor.AddIn.View;

namespace MetadataEditor.AddIn.KinoEditor
{
    public class KinoEditor : RSG.Base.Editor.ViewModelBase
    {
        #region Constants
        public static readonly String METADATA_TYPE = "rage::cutfCutsceneFile2";

        public const String DOCUMENT_NAME = "Cutscene";
        #endregion // Constants
    }

} // MetadataEditor.AddIn.KinoEditor
