﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;

namespace Workbench.Services.Perforce
{
    public class PerforceItemViewModel : ViewModelBase
    {
        #region Members

        private PerforceItem m_model; 

        #endregion // Members

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public PerforceItem Model
        {
            get { return m_model; }
            set
            {
                SetPropertyValue(value, () => this.Model,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_model = (PerforceItem)newValue;
                        }
                    )
                );
            }
        }

        /// <summary>
        /// Represents where this item should be synced or not
        /// </summary>
        public Boolean SyncItem
        {
            get { return this.Model.SyncItem; }
            set { this.Model.SyncItem = value; }
        }

        /// <summary>
        /// The filename to this perforce
        /// sync item.
        /// </summary>
        public String DepotFile
        {
            get { return this.Model.DepotFile; }
        }

        /// <summary>
        /// The filename to this perforce
        /// sync item.
        /// </summary>
        public String DepotFilename
        {
            get { return System.IO.Path.GetFileName(this.Model.DepotFile); }
        }

        /// <summary>
        /// The filename to this perforce
        /// sync item.
        /// </summary>
        public String DepotFilepath
        {
            get { return System.IO.Path.GetDirectoryName(this.Model.DepotFile); }
        }

        /// <summary>
        /// The size of the file as reported by
        /// the perforce record else as reported
        /// by the .Net framework
        /// </summary>
        public int FileSize
        {
            get { return this.Model.FileSize; }
        }
    
        #endregion // Properties

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public PerforceItemViewModel(PerforceItem model)
        {
            this.Model = model;
        }

        #endregion // Constructor
    } // PerforceItemViewModel
} // Workbench.Services.Perforce
