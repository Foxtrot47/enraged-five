﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn;
using System.ComponentModel;
using System.Windows;
using RSG.Base.Tasks;
using System.Threading;
using RSG.Base.Windows.Dialogs;

namespace Workbench.Services
{
    /// <summary>
    /// Service for showing a generic progress dialog box
    /// </summary>
    [ExportExtension(Workbench.AddIn.CompositionPoints.ProgressService,
           typeof(Workbench.AddIn.Services.IProgressService))]
    class ProgressService : Workbench.AddIn.Services.IProgressService
    {
        ProgressViewModel m_vm;

        /// <summary>
        /// Default constructor.
        /// </summary>
        public ProgressService()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        [Obsolete]
        public string Title
        {
            get
            {
                return m_vm.Title;
            }
            set
            {
                m_vm.Title = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Obsolete]
        public string Message
        {
            get
            {
                return m_vm.Message;
            }
            set
            {
                m_vm.Message = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Obsolete]
        public string Status
        {
            get
            {
                return m_vm.Status;
            }
            set
            {
                m_vm.Status = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Obsolete]
        public bool IsIndeterminate
        {
            get
            {
                return m_vm.IsIndeterminate;
            }
            set
            {
                m_vm.IsIndeterminate = IsIndeterminate;
            }
        }

        /// <summary>
        /// Arguments passed into the BackgroundWorker.
        /// </summary>
        [Obsolete]
        public Object WorkerArguments
        {
            get
            {
                return m_vm.WorkerArguments;
            }
            set
            {
                m_vm.WorkerArguments = WorkerArguments;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="worker"></param>
        /// <returns></returns>
        [Obsolete]
        public bool Show(BackgroundWorker worker)
        {
            m_vm = new ProgressViewModel(worker);
            ProgressWindow progressWindow = new ProgressWindow(m_vm);
            progressWindow.Owner = Application.Current.MainWindow;
            return (progressWindow.ShowDialog() != false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="worker"></param>
        /// <param name="title"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        [Obsolete]
        public bool Show(BackgroundWorker worker, string title, string message)
        {
            return Show(worker, title, message, false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="worker"></param>
        /// <param name="title"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        [Obsolete]
        public bool Show(BackgroundWorker worker, string title, string message, bool indeterminate)
        {
            m_vm = new ProgressViewModel(worker, title, message, indeterminate);
            ProgressWindow progressWindow = new ProgressWindow(m_vm);
            progressWindow.Owner = Application.Current.MainWindow;
            return (progressWindow.ShowDialog() != false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="worker"></param>
        /// <param name="arguments"></param>
        /// <param name="title"></param>
        /// <param name="message"></param>
        /// <param name="indeterminate"></param>
        /// <returns></returns>
        [Obsolete]
        public bool Show(BackgroundWorker worker, object arguments, string title, string message, bool indeterminate)
        {
            m_vm = new ProgressViewModel(worker, arguments, title, message, indeterminate);
            ProgressWindow progressWindow = new ProgressWindow(m_vm);
            progressWindow.Owner = Application.Current.MainWindow;
            return (progressWindow.ShowDialog() != false);
        }
        
        /// <summary>
        /// Shows the task execution dialog and executes the task on a seperate thread.
        /// </summary>
        /// <param name="task">Task to execute.</param>
        /// <param name="context">Task context</param>
        /// <returns>Whether the task successfully completed.</returns>
        public bool Show(ITask task, ITaskContext context, CancellationTokenSource cts, string title)
        {
            TaskExecutionDialog dialog = new TaskExecutionDialog();
            TaskExecutionViewModel vm = new TaskExecutionViewModel(title, task, cts, context);
            vm.AutoCloseOnCompletion = true;
            dialog.DataContext = vm;
            Nullable<bool> selectionResult = dialog.ShowDialog();
            if (false == selectionResult)
            {
                return false;
            }

            return true;
        }
    }
}
