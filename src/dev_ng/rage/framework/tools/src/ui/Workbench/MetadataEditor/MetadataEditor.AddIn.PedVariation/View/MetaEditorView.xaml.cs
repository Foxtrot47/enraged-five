﻿using System;
using MetadataEditor.AddIn.PedVariation.ViewModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Data;
using System.Globalization;

namespace MetadataEditor.AddIn.PedVariation.View
{
    /// <summary>
    /// Interaction logic for MetaEditorBasicView.xaml
    /// </summary>
    public partial class MetaEditorView : Workbench.AddIn.UI.Layout.DocumentBase<MetaFileViewModel>, IMetadataView
    {
        #region Constants
        public static Guid CONSTANT_ID = new Guid("C78A10AE-9495-4357-A49D-1F61FAB62D80");
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public MetaEditorView()
            : base(MetaEditorViewModel.DOCUMENT_NAME, new MetaFileViewModel())
        {
            InitializeComponent();
            this.ID = CONSTANT_ID;
            this.Name = MetaEditorViewModel.DOCUMENT_NAME;
            this.SaveModel = this.ViewModel.Model;
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public MetaEditorView(MetaFileViewModel viewModel)
            : base(MetaEditorViewModel.DOCUMENT_NAME, viewModel)
        {
            InitializeComponent();
            this.ID = CONSTANT_ID;
            this.Name = String.Format("{0}_{1}", MetaEditorViewModel.DOCUMENT_NAME, viewModel.GetHashCode().ToString());

            String[] filenames = viewModel.Model.Filename.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (filenames.Length == 1)
                this.Title = System.IO.Path.GetFileName(filenames[0]);
            else if (filenames.Length > 1)
                this.Title = "Multi Document";
            this.SaveModel = viewModel.Model;
        }
        #endregion // Constructor(s)

        #region Methods
        /// <summary>
        /// Gets the view model as a IMetadataViewModel to support live editing a generic
        /// metadata file.
        /// </summary>
        public IMetadataViewModel GetMetadataViewModel()
        {
            return this.ViewModel;
        }

        /// <summary>
        /// Stops a list box automatically scrolling a partially visible item into view
        /// when the user selects it.
        /// </summary>
        /// <param name="sender">
        /// The object that this event handler is attached to.
        /// </param>
        /// <param name="e">
        /// The data for this event.
        /// </param>
        void listBoxItem_RequestBringIntoView(object sender, RequestBringIntoViewEventArgs e)
        {
            e.Handled = true;
        }
        #endregion

        private void TextBox_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            e.Handled = !IsHexCharacterKey(e.Key) && !IsActionKey(e.Key);
            if ((Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
            {
                e.Handled = false;
            }
        }

        private bool IsHexCharacterKey(Key inKey)
        {
            if (inKey < Key.D0 || inKey > Key.D9)
            {
                if (inKey < Key.NumPad0 || inKey > Key.NumPad9)
                {
                    if (!IsHexKey(inKey))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        private bool IsHexKey(Key inKey)
        {
            if (inKey < Key.A || inKey > Key.F)
            {
                return false;
            }

            return true;
        }

        private bool IsActionKey(Key inKey)
        {
            return inKey == Key.Delete || inKey == Key.Back || inKey == Key.Tab || inKey == Key.Return || Keyboard.Modifiers.HasFlag(ModifierKeys.Alt);
        }

        private string LeaveOnlyHexCharacters(String inString)
        {
            String tmp = inString;
            foreach (char c in inString.ToCharArray())
            {
                char test = char.ToUpper(c);
                if (!char.IsDigit(test))
                {
                    if (test != 'A' || test != 'B' || test != 'C' || test != 'D' || test != 'E' || test != 'F')
                    {
                        tmp = tmp.Replace(c.ToString(), "");
                    }
                }
            }

            return tmp;
        }

        private void EditSeletionSets_Click(object sender, RoutedEventArgs e)
        {
            SelectionSetEditor editor = new SelectionSetEditor();
            editor.DataContext = this.ViewModel.SelectionSets;
            editor.ShowDialog();
        }
    }

    public class HexStringConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string stringValue = String.Format("{0:X}", (uint)value);
            return stringValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return 0;
            }

            uint uintValue = 0;
            uint.TryParse((string)value, System.Globalization.NumberStyles.HexNumber, CultureInfo.CurrentCulture, out uintValue);
            return uintValue;
        }
    }
} // MetadataEditor.AddIn.MetaEditor namespace
