﻿using System;
using System.Windows.Media;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using RSG.Base.Windows.Helpers;
using Workbench.AddIn.Services;
using RSG.Base.Math;
using RSG.Model.Common;
using WidgetEditor.AddIn;
using RSG.Base.Tasks;
using RSG.Model.Common.Map;
using System.Threading;
using RSG.Base.Windows.Dialogs;
using RSG.Model.Common.Util;
using MapViewport.AddIn;
using System.IO;
using Workbench.AddIn.Services.Model;

namespace Workbench.AddIn.MapDebugging.Overlays
{
    [ExportExtension("Workbench.AddIn.MapDebugging.OverlayGroups.MapDebug",
        typeof(Viewport.AddIn.ViewportOverlay))]
    public class ObjectLocationOverlay : LevelDependentViewportOverlay
    {
        #region Constants

        private const String NAME = "Asset Location";
        private const String DESC = "Displays the location of the selected asset on the map. If a definition or texture is selected it will"
                                    + " show all instances where that asset is used on the map.";

        private const String RAG_WARPCOORDINATES = "Debug/Warp Player x y z h vx vy vz";
        private const String RAG_WARPNOWBUTTON = "Debug/Warp now";

        #endregion // Constants

        #region MEF Imports

        /// <summary>
        /// Content Browser
        /// </summary>
        [ImportExtension(ContentBrowser.AddIn.CompositionPoints.ContentBrowser, typeof(ContentBrowser.AddIn.IContentBrowser))]
        public Lazy<ContentBrowser.AddIn.IContentBrowser> ContentBrowserProxy { get; set; }

        /// <summary>
        /// A reference to view model for the map viewport
        /// </summary>
        [ImportExtension(Viewport.AddIn.CompositionPoints.MapViewport, typeof(Viewport.AddIn.IMapViewport))]
        public Lazy<Viewport.AddIn.IMapViewport> MapViewportProxy { get; set; }

        /// <summary>
        /// MEF import for proxy UI
        /// </summary>
        [ImportExtension(WidgetEditor.AddIn.CompositionPoints.ProxyService, typeof(IProxyService))]
        public Lazy<IProxyService> ProxyService { get; set; }

        /// <summary>
        /// MEF import for message box service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.MessageService, typeof(Workbench.AddIn.Services.IMessageService))]
        private Lazy<IMessageService> MessageService { get; set; }

        /// <summary>
        /// MEF import for message box service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.TaskProgressService, typeof(Workbench.AddIn.Services.ITaskProgressService))]
        private Lazy<ITaskProgressService> TaskProgressService { get; set; }

        #endregion // MEF Imports

        #region Members

        private ObservableCollection<IEntity> m_validEntities;
        private ObservableCollection<object> m_validArchetypes;
        private IEntity m_selectedEntity;
        private object m_currentSelection;
        private RelayCommand m_warpToInGameCommand;
        private RelayCommand m_ExportToCsvFile;

        #endregion // Members

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<object> ValidArchetypes
        {
            get { return m_validArchetypes; }
            set
            {
                SetPropertyValue(value, () => ValidArchetypes,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_validArchetypes = (ObservableCollection<object>)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<IEntity> ValidEntities
        {
            get { return m_validEntities; }
            set
            {
                SetPropertyValue(value, () => ValidEntities,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_validEntities = (ObservableCollection<IEntity>)newValue;
                            OnRenderOptionChanged();
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public IEntity SelectedEntity
        {
            get { return m_selectedEntity; }
            set
            {
                SetPropertyValue(value, () => SelectedEntity,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_selectedEntity = (IEntity)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Flag indicating whether we are connected to the game via the rag proxy
        /// </summary>
        public bool IsGameConnected
        {
            get
            {
                return ProxyService.Value.IsGameConnected;
            }
        }

        /// <summary>
        /// Command that gets fired off when the "Warp to in Game" button is pressed for the selected map instance
        /// </summary>
        public RelayCommand WarpToInGameCommand
        {
            get
            {
                if (m_warpToInGameCommand == null)
                {
                    m_warpToInGameCommand = new RelayCommand(param => WarpToInGame(param));
                }

                return m_warpToInGameCommand;
            }
        }

        /// <summary>
        /// Command that gets fired off when the "Export to .csv File" button is pressed.
        /// </summary>
        public RelayCommand ExportToCSV
        {
            get
            {
                if (m_ExportToCsvFile == null)
                {
                    m_ExportToCsvFile = new RelayCommand(param => ExportToCsvFile(param), param => CanExportToCsvFile(param));
                }

                return m_ExportToCsvFile;
            }
        }

        public object SelectedMapObject
        {
            get
            {
                return this.selectedMapObject;
            }

            set
            {
                this.selectedMapObject = value;
                if (LevelBrowserProxy == null || LevelBrowserProxy.Value.SelectedLevel == null)
                {
                    return;
                }

                if (LevelBrowserProxy.Value.SelectedLevel.MapHierarchy == null)
                {
                    return;
                }

                this.CreateGeometry(LevelBrowserProxy.Value.SelectedLevel.MapHierarchy);
            }
        }
        private object selectedMapObject;
        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Default Constructor.
        /// </summary>
        public ObjectLocationOverlay()
            : base(NAME, DESC)
        {
            this.ValidArchetypes = new ObservableCollection<object>();
            this.ValidEntities = new ObservableCollection<IEntity>();
        }

        #endregion // Constructors

        #region Overrides

        /// <summary>
        /// Gets called when the user wants to view this overlay
        /// </summary>
        public override void Activated()
        {
            base.Activated();
            this.m_currentSelection = null;
            this.ContentBrowserProxy.Value.GridSelectionChanged += GridSelectionChanged;
            
            CancellationTokenSource cts = new CancellationTokenSource();
            LevelTaskContext context = new LevelTaskContext(cts.Token, LevelBrowserProxy.Value.SelectedLevel);
            IMapHierarchy hierarchy = context.MapHierarchy.AllSections.First().MapHierarchy;

            StreamableStat[] statsToLoad = new StreamableStat[] {
                StreamableMapSectionStat.Archetypes,
                StreamableMapSectionStat.Entities,
                StreamableMapArchetypeStat.Shaders,
                StreamableEntityStat.Position };
            ITask dataLoadTask = hierarchy.CreateStatLoadTask(context.MapHierarchy.AllSections, statsToLoad);

            if (dataLoadTask != null)
            {
                TaskExecutionDialog dialog = new TaskExecutionDialog();
                TaskExecutionViewModel vm = new TaskExecutionViewModel("Generating Overlay", dataLoadTask, cts, context);
                vm.AutoCloseOnCompletion = true;
                dialog.DataContext = vm;
                Nullable<bool> selectionResult = dialog.ShowDialog();
                if (false == selectionResult)
                {
                    return;
                }
            }

            if (dataLoadTask == null || dataLoadTask.Status == TaskStatus.Completed)
            {
                this.CreateGeometry(LevelBrowserProxy.Value.SelectedLevel.MapHierarchy);
            }
        }

        /// <summary>
        /// Gets called when the user turns off this overlay
        /// </summary>
        public override void Deactivated()
        {
            this.ContentBrowserProxy.Value.GridSelectionChanged -= GridSelectionChanged;
            this.ValidEntities.Clear();
            base.Deactivated();
        }

        /// <summary>
        /// Gets the control that should be shown in the overlay details panel
        /// </summary>
        public override System.Windows.Controls.Control GetOverlayControl()
        {
            return new OverlayViews.ObjectLocationOverlayView();
        }

        #endregion // Overrides

        #region Event Handling

        /// <summary>
        /// Gets called when any stat render option changes and we need to redraw the geometry
        /// </summary>
        void OnRenderOptionChanged()
        {
            if (LevelBrowserProxy == null || LevelBrowserProxy.Value.SelectedLevel == null)
            {
                return;
            }

            if (LevelBrowserProxy.Value.SelectedLevel.MapHierarchy == null)
            {
                return;
            }

            this.CreateGeometry(LevelBrowserProxy.Value.SelectedLevel.MapHierarchy);
        }
        
        /// <summary>
        /// Get called when the items selected in the content browser change
        /// </summary>
        void GridSelectionChanged(Object sender, ContentBrowser.AddIn.GridSelectionChangedEventArgs args)
        {
            this.SelectionChanged();
        }

        /// <summary>
        /// Get called when the items selected in the content browser change and when
        /// the overlay is first shown
        /// </summary>
        private void SelectionChanged()
        {
            if (this.MapViewportProxy == null)
            {
                return;
            }

            this.MapViewportProxy.Value.SelectedOverlayGeometry.Clear();
            if (this.ContentBrowserProxy.Value.SelectedGridItems == null || this.ContentBrowserProxy.Value.SelectedGridItems.Count() < 1)
            {
                return;
            }

            this.ValidArchetypes.Clear();
            m_currentSelection = this.ContentBrowserProxy.Value.SelectedGridItems.First();
            if (this.ContentBrowserProxy.Value.SelectedGridItems != null)
            {
                foreach (object selection in this.ContentBrowserProxy.Value.SelectedGridItems)
                {
                    this.ValidArchetypes.Add(selection);
                }
            }
        }

        /// <summary>
        /// Creates the viewport geometry for the given map sections
        /// </summary>
        protected virtual void CreateGeometry(IMapHierarchy hierarchy)
        {
            this.Geometry.BeginUpdate();
            this.ValidEntities.Clear();
            this.Geometry.Clear();

            // Create the overlay image
            RSG.Base.Math.Vector2i size = new RSG.Base.Math.Vector2i(966, 627);
            RSG.Base.Math.Vector2f pos = new RSG.Base.Math.Vector2f(-483.0f, 627.0f);
            ILevel level = LevelBrowserProxy.Value.SelectedLevel;
            if (level.ImageBounds != null)
            {
                size = new RSG.Base.Math.Vector2i((int)(level.ImageBounds.Max.X - level.ImageBounds.Min.X), (int)(level.ImageBounds.Max.Y - level.ImageBounds.Min.Y));
                pos = new RSG.Base.Math.Vector2f(level.ImageBounds.Min.X, level.ImageBounds.Max.Y);
            }
            Viewport2DImageOverlay image = new Viewport2DImageOverlay(etCoordSpace.World, "", new RSG.Base.Math.Vector2i((int)(size.X * 0.2f), (int)(size.Y * 0.2f)), pos, new RSG.Base.Math.Vector2f(size.X, size.Y));
            image.BeginUpdate();

            // Update the data rendered to the overlay
            if (this.SelectedMapObject != null)
            {
                IList<System.Drawing.Color> colours = RSG.Base.Drawing.Colours.VaryingColors(1, 255).Take(ContentBrowserProxy.Value.SelectedGridItems.Count()).ToList();
                int colourIdx = 0;

                if (this.SelectedMapObject is IArchetype)
                {
                    foreach (IEntity instance in (this.SelectedMapObject as IArchetype).Entities)
                    {
                        image.RenderCircle(new RSG.Base.Math.Vector2f(instance.Position.X, instance.Position.Y), 10.0f, colours[colourIdx]);
                        this.ValidEntities.Add(instance);
                    }
                }
                else if (this.SelectedMapObject is IEntity)
                {
                    IEntity instance = (this.SelectedMapObject as IEntity);
                    image.RenderCircle(new RSG.Base.Math.Vector2f(instance.Position.X, instance.Position.Y), 10.0f, colours[colourIdx]);
                    this.ValidEntities.Add(instance);
                }
                else if (this.SelectedMapObject is ITexture)
                {
                    string textureName = (this.SelectedMapObject as ITexture).Name;
                    IEnumerable<IMapArchetype> test = from s in hierarchy.AllSections
                                                      from a in s.Archetypes
                                                      where a.Textures.Any(texture => texture.Name == textureName)
                                                      select a;

                    foreach (IMapArchetype definition in test)
                    {
                        foreach (IEntity instance in definition.Entities)
                        {
                            image.RenderCircle(new RSG.Base.Math.Vector2f(instance.Position.X, instance.Position.Y), 10.0f, colours[colourIdx]);
                            this.ValidEntities.Add(instance);
                        }
                    }
                }
                else if (this.SelectedMapObject is IMapSection)
                {
                    foreach (IArchetype definition in (this.SelectedMapObject as IMapSection).Archetypes)
                    {
                        foreach (IEntity instance in definition.Entities)
                        {
                            image.RenderCircle(new RSG.Base.Math.Vector2f(instance.Position.X, instance.Position.Y), 10.0f, colours[colourIdx]);
                            this.ValidEntities.Add(instance);
                        }
                    }
                }
            }

            image.EndUpdate();
            this.Geometry.Add(image);
            this.Geometry.EndUpdate();
        }

        private bool CanExportToCsvFile(object parameter)
        {
            return SelectedMapObject != null && this.ValidEntities.Count > 0;
        }

        private void ExportToCsvFile(object parameter)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.Filter = "CSV (Comma delimited)|*.csv";
            Nullable<bool> result = dlg.ShowDialog();
            if (false == result)
            {
                return;
            }

            using (StreamWriter sw = new StreamWriter(dlg.FileName))
            {
                string header = "Name, Section, Parent Area ,Grandparent Area, Coords";
                sw.WriteLine(header);

                foreach (var entity in this.ValidEntities)
                {
                    string sectionName = "n/a";
                    string parentName = "n/a";
                    string grandParentName = "n/a";
                    IMapSection section = entity.Parent as IMapSection;
                    IMapArea parent = null;
                    IMapArea grandParent = null;
                    if (section != null)
                    {
                        sectionName = section.Name;
                        parent = section.ParentArea;
                        if (parent != null)
                        {
                            parentName = parent.Name;
                            grandParent = parent.ParentArea;
                            if (grandParent != null)
                            {
                                grandParentName = grandParent.Name;
                            }
                        }
                    }

                    string location = string.Format("\"{0}, {1}, {2}\"", entity.Position.X, entity.Position.Y, entity.Position.Z);
                    string record = string.Format("{0},{1},{2},{3},{4}", entity.Name, sectionName, parentName, grandParentName, location);
                    sw.WriteLine(record);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="instance"></param>
        private void WarpToInGame(object instanceObj)
        {
            IEntity instance = instanceObj as IEntity;
            if (instance != null)
            {
                Vector3f position = new Vector3f(instance.Position);
                position.X -= 2.0f;
                position.Z += 1.0f;

                float heading = -(float)Math.PI/2.0f;

                // Check whether the RAG proxy is connected
                if (!ProxyService.Value.IsGameConnected)
                {
                    MessageService.Value.Show("RAG is not connected. Is the game running?");
                }
                else
                {
                    ProxyService.Value.Console.WriteStringWidget(RAG_WARPCOORDINATES, String.Format("{0} {1} {2} {3}", position.X, position.Y, position.Z, heading));
                    ProxyService.Value.Console.PressWidgetButton(RAG_WARPNOWBUTTON);
                }
            }
        }
        #endregion // Event Handling
    } // MapInstancePriorityLevel
} // Workbench.AddIn.MapDebugging.Overlays
