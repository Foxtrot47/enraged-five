﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Map.ViewModel;
using RSG.Base.Collections;
using RSG.Base.Math;

namespace Map.AddIn.Services
{
    public interface IMapBrowser
    {
        #region Events

        /// <summary>
        /// Gets fired whenever the selected level changes to something different
        /// </summary>
        event EventHandler SelectedLevelChanged;

        /// <summary>
        /// Gets fired whenever a item is double clicked in the map browser
        /// tree view
        /// </summary>
        event RoutedEventHandler ComponentDoubleClicked;

        /// <summary>
        /// Gets fired whenever a item gets expanded
        /// </summary>
        event RoutedEventHandler ComponentExpanded;

        #endregion // Events

        #region Properties

        /// <summary>
        /// The selected level in the map browser, there can only be one at a
        /// time selected
        /// </summary>
        LevelViewModel SelectedLevel { get; }

        /// <summary>
        /// A collection of objects currently selected in the map browser treeview
        /// </summary>
        ObservableCollection<Object> SelectedItems { get; }

        /// <summary>
        /// A collection of the map sections currently selected in the map browser treeview
        /// </summary>
        ObservableCollection<IMapSectionViewModel> SelectedSections { get; }

        /// <summary>
        /// A list containing all the map sections that are in the currently
        /// selected level
        /// </summary>
        List<MapSectionViewModel> MapSections { get; }

        /// <summary>
        /// A list containing all the prop group sections that are in the currently
        /// selected level
        /// </summary>
        List<PropGroupSectionViewModel> PropGroupSections { get; }

        /// <summary>
        /// A list containing all the interior sections that are in the currently
        /// selected level
        /// </summary>
        List<InteriorSectionViewModel> InteriorSections { get; }

        /// <summary>
        /// A list containing all the prop sections that are in the currently
        /// selected level
        /// </summary>
        List<PropSectionViewModel> PropSections { get; }
        
        /// <summary>
        /// Contains the points for all the selected level map section geometry
        /// </summary>
        Dictionary<MapSectionViewModel, List<Vector2f>> LevelGeometry { get; }

        #endregion // Properties

        #region Methods

        /// <summary>
        /// Clears the selection in the treeview
        /// </summary>
        void ClearSelection();

        #endregion // Methods
    }
}
