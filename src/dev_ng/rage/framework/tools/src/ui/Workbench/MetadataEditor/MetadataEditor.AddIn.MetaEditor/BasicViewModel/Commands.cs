﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RSG.Metadata.Parser;

namespace MetadataEditor.AddIn.MetaEditor.BasicViewModel
{
    /// <summary>
    /// A command whose sole purpose is to 
    /// relay its functionality to other
    /// objects by invoking delegates. The
    /// default return value for the CanExecute
    /// method is 'true'.
    /// </summary>
    public class RelayCommand : System.Windows.Input.ICommand
    {
        #region Fields

        public Action<Object> _execute;
        public Predicate<Object> _canExecute;

        #endregion // Fields

        #region Constructors

        /// <summary>
        /// Creates a new command that can always execute.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        public RelayCommand()
            : this(null, null)
        {
        }

        /// <summary>
        /// Creates a new command that can always execute.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        public RelayCommand(Action<object> execute)
            : this(execute, null)
        {
        }

        /// <summary>
        /// Creates a new command.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        /// <param name="canExecute">The execution status logic.</param>
        public RelayCommand(Action<object> execute, Predicate<object> canExecute)
        {
            _execute = execute;
            _canExecute = canExecute;
        }

        #endregion // Constructors

        #region ICommand Members

        public bool CanExecute(Object parameter)
        {
            return _canExecute == null ? true : _canExecute(parameter);
        }

        public event EventHandler CanExecuteChanged
        {
            add { System.Windows.Input.CommandManager.RequerySuggested += value; }
            remove { System.Windows.Input.CommandManager.RequerySuggested -= value; }
        }

        public void Execute(Object parameter)
        {
            if (_execute != null)
                _execute(parameter);
        }

        #endregion // ICommand Members
    } // RelayCommand

    /// <summary>
    /// Represents an actionable item displayed by a View.
    /// </summary>
    public class CommandViewModel : INotifyPropertyChanged
    {
        #region Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion // Events

        #region Properties

        public System.Collections.ObjectModel.ObservableCollection<CommandViewModel> Children
        {
            get;
            set;
        }

        public Boolean IsEnabled
        {
            get
            {
                if (this.Children.Count == 0 && this.Command._execute == null)
                {
                    return false;
                }
                Boolean enabled = false;
                if (this.Children.Count == 0)
                {
                    enabled = true;
                }
                foreach (var child in this.Children)
                {
                    if (child.Command.CanExecute(null) == true)
                    {
                        enabled = true;
                        break;
                    }
                }
                return enabled;
            }
        }

        public String DisplayName
        {
            get { return _DisplayName; }
            set
            {
                if (_DisplayName != value)
                {
                    _DisplayName = value;
                    if (PropertyChanged != null)
                        PropertyChanged(this, new PropertyChangedEventArgs("DisplayName"));
                }
            }
        }
        private String _DisplayName;

        public RelayCommand Command { get; private set; }

        public IMember CommandTag { get; set; }

        #endregion // Properties

        #region Constructors

        public CommandViewModel(String displayName, IMember tag, RelayCommand command)
        {
            if (command == null)
                throw new ArgumentNullException("command");

            this.DisplayName = displayName;
            this.Command = command;
            this.Children = new System.Collections.ObjectModel.ObservableCollection<CommandViewModel>();
            this.CommandTag = tag;
        }

        #endregion // Constructors

        #region Public Functions

        public void RefreshIsEnabled()
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs("IsEnabled"));

            foreach (var child in this.Children)
            {
                child.RefreshIsEnabled();
            }
        }

        #endregion // Public Functions
    } // CommandViewModel
} // MetadataEditor.AddIn.ViewModel
