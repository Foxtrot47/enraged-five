﻿using System;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Controls;
using System.ComponentModel;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using ContentBrowser.AddIn;
using ContentBrowser.Model;
using ContentBrowser.View;
using RSG.Base.Editor;
using ContentBrowser.ViewModel.LevelViewModels;
using System.Windows.Input;

namespace ContentBrowser.ViewModel
{
    /// <summary>
    /// 
    /// </summary>
    public class BrowserCollectionViewModel : ViewModelBase, IWeakEventListener
    {
        #region Events
        public event EventHandler HideShowChanged;
        #endregion

        #region Members

        private BrowserCollection m_model;
        private ObservableCollection<IBrowserViewModel> m_browsers;
        public IBrowserViewModel m_selectedBrowser;

        #endregion // Members

        #region Properties
        /// <summary>
        /// The observable collection of browsers
        /// </summary>
        public BrowserCollection Model
        {
            get { return m_model; }
            set
            {
                if (m_model == value)
                    return;

                SetPropertyValue(value, () => Model,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_model = (BrowserCollection)newValue;
                        }
                ));

                this.SyncLevelsToModel();
            }
        }

        /// <summary>
        /// The observable collection of browsers
        /// </summary>
        public ObservableCollection<IBrowserViewModel> BrowserItems
        {
            get { return m_browsers; }
            set 
            {
                if (m_browsers == value)
                    return;

                SetPropertyValue(value, () => BrowserItems,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_browsers = (ObservableCollection<IBrowserViewModel>)newValue;
                        }
                ));
            }
        }
 
        /// <summary>
        /// The browser that is currently selected and shown to the
        /// user.
        /// </summary>
        public IBrowserViewModel SelectedBrowser
        {
            get { return m_selectedBrowser; }
            set
            {
                if (m_selectedBrowser == value)
                    return;

                IBrowserViewModel oldBrowser = m_selectedBrowser;

                SetPropertyValue(value, () => SelectedBrowser,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_selectedBrowser = (IBrowserViewModel)newValue;
                        }
                ));

                OnBrowserChanged(oldBrowser, value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand HideShowGridCommand
        {
            get
            {
                if (m_HideShowGridCommand == null)
                {
                    m_HideShowGridCommand = new RelayCommand(this.OnHideShowExecuted);
                }
                return m_HideShowGridCommand;
            }
        }
        RelayCommand m_HideShowGridCommand;

        /// <summary>
        /// 
        /// </summary>
        public bool ShowGrid
        {
            get { return m_showGrid; }
            private set
            {
                if (m_showGrid == value)
                    return;

                SetPropertyValue(value, m_showGrid, () => ShowGrid,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_showGrid = (bool)newValue;
                        }
                ));
            }
        }
        private bool m_showGrid;
        #endregion // Properties

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public BrowserCollectionViewModel(BrowserCollection browserCollection)
        {
            this.ShowGrid = true;
            this.Model = browserCollection;
            this.BrowserItems = new ObservableCollection<IBrowserViewModel>();
            foreach (BrowserBase browser in browserCollection.Browsers.Values)
            {
                IBrowserViewModel browserViewModel = ViewModelFactory.CreateBrowserViewModel(browser);
                PropertyChangedEventManager.AddListener(browserViewModel, this, "IsSelected");
                this.BrowserItems.Add(browserViewModel);
            }
            GenericBrowserViewModel defaultBrowser = null;
            foreach (GenericBrowserViewModel vm in this.BrowserItems)
            {
                if (vm.Model.DefaultBrowser == true)
                {
                    if (defaultBrowser == null)
                        defaultBrowser = vm;
                    else
                        System.Diagnostics.Debug.Assert(defaultBrowser == null, "Multiple browsers have the default property set to true. Can only have one default browser");
                }
            }
            if (defaultBrowser != null)
                defaultBrowser.IsSelected = true;
            else if (this.BrowserItems.Count > 0)
                this.BrowserItems[0].IsSelected = true;
        }

        #endregion // Constructor

        #region Property Changed Callbacks

        /// <summary>
        /// 
        /// </summary>
        private void OnBrowserChanged(IBrowserViewModel oldBrowser, IBrowserViewModel newBrowser)
        {
            if (oldBrowser != null)
                oldBrowser.ClearSelection();
        }

        #endregion // Property Changed Callbacks

        #region Private Methods

        /// <summary>
        /// Makes sure that the level viewmodel collection is in sync
        /// with the model collection
        /// </summary>
        private void SyncLevelsToModel()
        {
            this.OnPropertyChanged("Levels");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        private void OnHideShowExecuted(object parameter)
        {
            this.ShowGrid = !this.ShowGrid;
            if (HideShowChanged != null)
                HideShowChanged(this, EventArgs.Empty);
        }
        #endregion // Private Methods

        #region IWeakEventListener Implementation

        /// <summary>
        /// Listens to the model, making sure that the collections stay in sync
        /// </summary>
        public Boolean ReceiveWeakEvent(Type managerType, Object sender, EventArgs e)
        {
            if (managerType == typeof(CollectionChangedEventManager))
            {
                SyncLevelsToModel();
            }
            else if (managerType == typeof(PropertyChangedEventManager))
            {
                if (sender is IBrowserViewModel)
                {
                    if ((sender as IBrowserViewModel).IsSelected == true)
                    {
                        this.SelectedBrowser = (sender as IBrowserViewModel);
                    }
                }
            }

            return true;
        }

        #endregion // IWeakEventListener Implementation
    } // BrowserCollectionViewModel


    /// <summary>
    /// A command whose sole purpose is to  relay its functionality 
    /// to other objects by invoking delegates. The default return 
    /// value for the CanExecute method is 'true'.
    /// </summary>
    public class RelayCommand : ICommand
    {
        #region Fields

        private readonly Action<Object> m_execute;
        private readonly Predicate<Object> m_canExecute;

        #endregion // Fields

        #region Constructors

        /// <summary>
        /// Creates a new command that can always execute.
        /// </summary>
        public RelayCommand(Action<Object> execute)
            : this(execute, null)
        {
        }

        /// <summary>
        /// Creates a new command.
        /// </summary>
        public RelayCommand(Action<Object> execute, Predicate<Object> canExecute)
        {
            if (execute == null)
                throw new ArgumentNullException("execute");

            m_execute = execute;
            m_canExecute = canExecute;
        }

        #endregion // Constructors

        #region ICommand Members

        /// <summary>
        /// Gets called when the manager is deciding if this
        /// command can be executed. This returns true be default
        /// </summary>
        public bool CanExecute(Object parameter)
        {
            if (m_canExecute != null)
                return m_canExecute(parameter);

            return true;
        }

        /// <summary>
        /// Gets called when the command is executed. This just
        /// relays the execution to the delegate function
        /// </summary>
        public void Execute(Object parameter)
        {
            if (m_execute != null)
                m_execute(parameter);
        }

        /// <summary>
        /// Makes sure that this command is hooked up into the
        /// command manager
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        #endregion // ICommand Members
    } // RelayCommand
} // ContentBrowser.ViewModel
