﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Collections;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using RSG.Model.Asset;
using RSG.Model.Common;

namespace Workbench.AddIn.TXDParentiser.ViewModel
{
    public class TextureDictionaryViewModel : RSG.Base.Editor.HierarchicalViewModelBase
    {
        #region Properties

        /// <summary>
        /// The model reference that this view model represents.
        /// </summary>
        public TextureDictionary Model
        {
            get { return m_model; }
            set { m_model = value; }
        }
        private TextureDictionary m_model;

        /// <summary>
        /// A list of textures that this definition has attached to
        /// it. This list only has unique textures in it, no duplicates
        /// </summary>
        public ObservableCollection<TextureViewModel> Textures
        {
            get { return m_textures; }
            set
            {
                this.SetPropertyValue(ref this.m_textures, value, "Textures");
            }
        }
        private ObservableCollection<TextureViewModel> m_textures;

        #endregion // Properties

        #region Constructor

        /// <summary>
        /// Default Constructor
        /// </summary>
        public TextureDictionaryViewModel(IViewModel parent, TextureDictionary model)
        {
            this.Parent = parent;
            this.Model = model;
            this.Textures = new ObservableCollection<TextureViewModel>();
            this.Textures.Add(new TextureViewModel());
        }

        #endregion // Constructor

        #region Override Functions

        protected override void OnFirstExpanded()
        {
            if (this.Model != null)
            {
                ObservableCollection<TextureViewModel> textures = new ObservableCollection<TextureViewModel>();
                foreach (Texture texture in this.Model.AssetChildren.OfType<ITexture>())
                {
                    textures.Add(new TextureViewModel(this, texture));
                }
                this.Textures = textures;
            }
        }

        public override IViewModel GetChildWithString(String name)
        {
            foreach (TextureViewModel child in this.Textures)
            {
                if (String.Compare(child.Model.Name, name, true) == 0)
                {
                    return child as IViewModel;
                }
            }

            return null;
        }

        #endregion // Override Functions
    } // TextureDictionaryViewModel
} // RSG.Model.Map.ViewModel
