﻿using System;
using System.Collections.Generic;
using RSG.Model.Perforce;

namespace ContentBrowser.ViewModel.PerforceViewModels
{

    /// <summary>
    /// 
    /// </summary>
    internal class DepotViewModel : AssetContainerViewModelBase
    {
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public DepotViewModel(Depot depot)
            : base(depot)
        {
        }
        #endregion // Constructor(s)
    }

} // ContentBrowser.ViewModel.PerforceViewModels namespace
