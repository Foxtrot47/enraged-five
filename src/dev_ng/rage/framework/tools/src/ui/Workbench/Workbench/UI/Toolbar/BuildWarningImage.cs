﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn.Commands;
using Workbench.AddIn;
using Workbench.AddIn.Services;
using ContentBrowser.AddIn;
using System.ComponentModel.Composition;
using RSG.Model.Common;
using RSG.Statistics.Common.Dto.GameAssets;
using Workbench.AddIn.Services.Model;

namespace Workbench.UI.Toolbar
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.DataModeToolbar, typeof(IWorkbenchCommand))]
    public class BuildWarningImage : WorkbenchCommandImage, IPartImportsSatisfiedNotification
    {
        #region Constants
        /// <summary>
        /// 
        /// </summary>
        public static readonly Guid GUID = new Guid("312820F2-B267-4015-B54D-9ACE530235C2");
        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.DataSourceBrowser, typeof(IDataSourceBrowser))]
        Lazy<IDataSourceBrowser> DataSourceBrowser { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.BuildBrowser, typeof(IBuildBrowser))]
        Lazy<IBuildBrowser> BuildBrowser { get; set; }
        #endregion // MEF Imports
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public BuildWarningImage()
            : base()
        {
            this.ID = GUID;
            this.RelativeToolBarID = BuildCombobox.GUID;
            this.Direction = Direction.After;
            this.Visible = false;

            SetImageFromBitmap(Workbench.Resources.Images.db_warning);
        }
        #endregion // Constructor(s)
        
        #region IPartImportsSatisfiedNotification Methods
        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {
            // Set up the callback that monitor the datasource
            DataSourceBrowser.Value.DataSourceChanged += OnDataSourceChanged;
            BuildBrowser.Value.BuildChanged += OnBuildChanged;
        }
        #endregion // IPartImportsSatisfiedNotification Methods

        #region Event Callbacks
        /// <summary>
        /// Callback for when the datasource has changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnDataSourceChanged(Object sender, DataSourceChangedEventArgs e)
        {
            UpdateVisibility();
        }

        /// <summary>
        /// Callback for when the build has changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnBuildChanged(Object sender, BuildChangedEventArgs e)
        {
            UpdateVisibility();
        }
        #endregion // Event Callbacks

        #region Private Methods
        /// <summary>
        /// Updates the image's visibility based on the current datasource and build
        /// </summary>
        private void UpdateVisibility()
        {
            ToolTip = null;
            
            // If the source is the scene xml, always hide the image, otherwise base it off of
            // the builds stats flags.
            if (DataSourceBrowser.Value.SelectedSource == DataSource.SceneXml)
            {
                Visible = false;
            }
            else
            {
                BuildDto currentBuild = BuildBrowser.Value.SelectedBuild;

                if (currentBuild != null)
                {
                    Visible = !currentBuild.HasAssetStats ||
                        !(currentBuild.HasAutomatedMapOnlyStats || currentBuild.HasAutomatedEverythingStats ||
                        currentBuild.HasAutomatedNighttimeMapOnlyStats || currentBuild.HasMagDemoStats ||
                        currentBuild.HasMemShortfallStats || currentBuild.HasShapeTestStats);

                    if (!currentBuild.HasAssetStats)
                    {
                        ToolTip = "The selected build doesn't have any asset stats associated with it.";
                    }
                    if (!(currentBuild.HasAutomatedMapOnlyStats || currentBuild.HasAutomatedEverythingStats ||
                        currentBuild.HasAutomatedNighttimeMapOnlyStats || currentBuild.HasMagDemoStats ||
                        currentBuild.HasMemShortfallStats || currentBuild.HasShapeTestStats))
                    {
                        ToolTip = "The selected build doesn't have any telemetry stats associated with it.";
                    }
                }
                else
                {
                    Visible = true;
                    ToolTip = "No build selected.";
                }
            }
        }
        #endregion // Private Methods
    } // ContentBrowserToolbarBuildWarningImage
}
