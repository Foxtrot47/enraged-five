﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Windows.Helpers;
using RSG.Model.Common;
using RSG.Statistics.Common.Dto.GameAssets;

namespace Workbench.AddIn.Services.Model
{
    /// <summary>
    /// Public interface for the main level browser in the workbench.
    /// Use this level browser as a central place to keep the selected level
    /// and loaded level collection.
    /// </summary>
    public interface ILevelBrowser
    {
        #region Events
        /// <summary>
        /// Fired when the selected level changes
        /// </summary>
        event LevelChangedEventHandler LevelChanged;
        #endregion // Events

        #region Properties
        /// <summary>
        /// Contains the level that is currently selected
        /// as the level to show
        /// </summary>
        ILevel SelectedLevel { get; }

        /// <summary>
        /// The main level collection that contains the current levels
        /// </summary>
        ILevelCollection LevelCollection { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Updates the levels.
        /// </summary>
        void UpdateLevels(DataSource source, BuildDto build = null);

        /// <summary>
        /// Changes the selection of the content
        /// browser to the given level if it exists.
        /// </summary>
        void SelectLevel(ILevel level);

        /// <summary>
        /// Changes the selection of the content
        /// browser to the given level name if it exists.
        /// </summary>
        void SelectLevel(String levelName);
        #endregion // Methods
    } // ILevelBrowser


    /// <summary>
    /// Level changed
    /// </summary>
    public class LevelChangedEventArgs : ItemChangedEventArgs<ILevel>
    {
        public LevelChangedEventArgs(ILevel oldLevel, ILevel newLevel)
            : base(oldLevel, newLevel)
        {
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="args"></param>
    public delegate void LevelChangedEventHandler(Object sender, LevelChangedEventArgs args);
}
