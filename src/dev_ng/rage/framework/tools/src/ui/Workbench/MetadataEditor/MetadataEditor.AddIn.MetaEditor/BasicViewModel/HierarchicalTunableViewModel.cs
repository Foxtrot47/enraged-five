﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Collections;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Data;
using RSG.Base.Collections;
using RSG.Base.Editor.Command;
using RSG.Metadata.Parser;
using RSG.Metadata.Data;
using MetadataEditor.AddIn.MetaEditor.ViewModel;

namespace MetadataEditor.AddIn.MetaEditor.BasicViewModel
{
    /// <summary>
    /// ViewModel for the hierarchical ITunable series of classes.
    /// </summary>
    public abstract class HierarchicalTunableViewModel : TunableViewModel, IEnumerable<ITunableViewModel>, System.Windows.IWeakEventListener
    {
        #region Properties
        public System.Collections.ObjectModel.ObservableCollection<ITunableViewModel> Members
        {
            get { return m_Members; }
            set
            {
                SetPropertyValue(value, () => this.Members,
                    new PropertySetDelegate(delegate(Object newValue) { m_Members = (System.Collections.ObjectModel.ObservableCollection<ITunableViewModel>)newValue; }));
            }
        }
        private System.Collections.ObjectModel.ObservableCollection<ITunableViewModel> m_Members;

        /// <summary>
        /// 
        /// </summary>
        public new Boolean? InheritParent
        {
            get { return this.Model.InheritParent; }
            set
            {
                if (value == this.Model.InheritParent)
                    return;

                this.Model.SetInheritParent(value);
                foreach (CommandViewModel command in this.Commands)
                {
                    command.RefreshIsEnabled();
                }
                foreach (ITunableViewModel child in this.Members)
                {
                    child.InheritParent = value;
                }
            }
        }

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public HierarchicalTunableViewModel(ITunableViewModel parent, ITunableStructure tunable, MetaFileViewModel root)
            : base(parent, tunable, root)
        {
            this.Parent = parent;
            this.Members = new System.Collections.ObjectModel.ObservableCollection<ITunableViewModel>();
            this.Members.CollectionChanged += new NotifyCollectionChangedEventHandler(Members_CollectionChanged);
            foreach (System.Collections.Generic.KeyValuePair<String, ITunable> t in tunable)
            {
                if (t.Value.Definition.HideWidgets == false)
                {
                    ITunableViewModel vm = TunableViewModelFactory.Create(this, t.Value, root);
                    if (vm is PointerTunableViewModel)
                    {
                        this.Members.Add((vm as PointerTunableViewModel).Members[0]);
                    }
                    else
                    {
                        this.Members.Add(vm);
                    }
                }
            }
            if (this.Model.ParentModel is PointerTunable)
            {
                PropertyChangedEventManager.AddListener(this.Model.ParentModel as ITunable, this, "Value");
            }

            m_PreviousMembers = new System.Collections.ObjectModel.ObservableCollection<ITunableViewModel>();
            foreach (ITunableViewModel vm in m_Members)
                m_PreviousMembers.Add(vm);
        }

        private void Members_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (this.Members.Count > 0)
            {
                this.HasItems = true;
            }
            if (this.IsExpanded)
            {
                foreach (ITunableViewModel vm in m_PreviousMembers)
                {
                    if (!m_Members.Contains(vm) && RootViewModel.AllTunableViewModels.Contains(vm))
                        RootViewModel.AllTunableViewModels.Remove(vm);

                    vm.IsExpanded = false;
                }
                foreach (ITunableViewModel vm in m_Members)
                {
                    vm.IsExpanded = false;
                }

                int index = this.RootViewModel.AllTunableViewModels.IndexOf(this);
                if (index != -1)
                {
                    index++;
                    foreach (ITunableViewModel vm in m_Members)
                    {
                        if (!RootViewModel.AllTunableViewModels.Contains(vm))
                            RootViewModel.AllTunableViewModels.Insert(index, vm);

                        index++;
                    }

                    index = this.RootViewModel.AllTunableViewModels.IndexOf(this) + 1;
                    foreach (ITunableViewModel vm in m_Members)
                    {
                        int vmIndex = RootViewModel.AllTunableViewModels.IndexOf(vm);
                        if (vmIndex != index)
                        {
                            RootViewModel.AllTunableViewModels.Remove(vm);
                            RootViewModel.AllTunableViewModels.Insert(index, vm);
                        }
                        index++;
                    }
                }
            }

            m_PreviousMembers.Clear();
            foreach (ITunableViewModel vm in m_Members)
                m_PreviousMembers.Add(vm);
        }

        /// <summary>
        /// 
        /// </summary>
        public HierarchicalTunableViewModel(ITunableViewModel parent, ITunableArray tunable, MetaFileViewModel root)
            : base(parent, tunable, root)
        {
            this.Parent = parent;
            this.Members = new System.Collections.ObjectModel.ObservableCollection<ITunableViewModel>();
            this.Members.CollectionChanged += new NotifyCollectionChangedEventHandler(Members_CollectionChanged);
            foreach (ITunable t in tunable)
            {
                if (t.Definition.HideWidgets == false)
                {
                    if (t is PointerTunable)
                    {
                        if ((t.Definition as PointerMember).Policy == PointerMember.PointerPolicy.ExternalNamed ||
                            (t.Definition as PointerMember).Policy == PointerMember.PointerPolicy.Link ||
                            (t.Definition as PointerMember).Policy == PointerMember.PointerPolicy.LazyLink)
                        {
                            ITunableViewModel vm = new TunableViewModel(parent, t, root);
                            this.Members.Add(vm);
                        }
                        else if ((t as PointerTunable).Value is ITunable)
                        {
                            ITunableViewModel vm = TunableViewModelFactory.Create(this, ((t as PointerTunable).Value as ITunable), root);
                            this.Members.Add(vm);
                        }
                    }
                    else
                    {
                        ITunableViewModel vm = TunableViewModelFactory.Create(this, t, root);
                        this.Members.Add(vm);
                    }
                }
            }
            if (this.Model.ParentModel is PointerTunable)
            {
                PropertyChangedEventManager.AddListener(this.Model.ParentModel as ITunable, this, "Value");
            }

            m_PreviousMembers = new System.Collections.ObjectModel.ObservableCollection<ITunableViewModel>();
            foreach (ITunableViewModel vm in m_Members)
                m_PreviousMembers.Add(vm);
        }

        /// <summary>
        /// 
        /// </summary>
        public HierarchicalTunableViewModel(ITunableViewModel parent, MapTunable tunable, MetaFileViewModel root)
            : base(parent, tunable, root)
        {
            this.Parent = parent;
            this.Members = new System.Collections.ObjectModel.ObservableCollection<ITunableViewModel>();
            this.Members.CollectionChanged += new NotifyCollectionChangedEventHandler(Members_CollectionChanged);
            foreach (MapItemTunable mapItem in tunable.Items)
            {
                this.Members.Add(new MapItemTunableViewModel(this, mapItem, root));
            }

            m_PreviousMembers = new System.Collections.ObjectModel.ObservableCollection<ITunableViewModel>();
            foreach (ITunableViewModel vm in m_Members)
                m_PreviousMembers.Add(vm);
        }

        /// <summary>
        /// 
        /// </summary>
        public HierarchicalTunableViewModel(ITunableViewModel parent, MapItemTunable tunable, MetaFileViewModel root)
            : base(parent, tunable, root)
        {
            this.Parent = parent;
            this.Members = new System.Collections.ObjectModel.ObservableCollection<ITunableViewModel>();
            this.Members.CollectionChanged += new NotifyCollectionChangedEventHandler(Members_CollectionChanged);
            ITunable t = tunable.Value;
            if (t.Definition.HideWidgets == false)
            {
                if (t is PointerTunable)
                {
                    if ((t as PointerTunable).Value is ITunable)
                    {
                        ITunableViewModel vm = TunableViewModelFactory.Create(this, ((t as PointerTunable).Value as ITunable), root);
                        this.Members.Add(vm);
                    }
                }
                else
                {
                    ITunableViewModel vm = TunableViewModelFactory.Create(this, t, root);
                    this.Members.Add(vm);
                }
            }
            if (this.Model.ParentModel is PointerTunable)
            {
                PropertyChangedEventManager.AddListener(this.Model.ParentModel as ITunable, this, "Value");
            }

            m_PreviousMembers = new System.Collections.ObjectModel.ObservableCollection<ITunableViewModel>();
            foreach (ITunableViewModel vm in m_Members)
                m_PreviousMembers.Add(vm);
        }

        /// <summary>
        /// 
        /// </summary>
        public HierarchicalTunableViewModel(ITunableViewModel parent, PointerTunable tunable, MetaFileViewModel root)
            : base(parent, tunable, root)
        {
            this.Parent = parent;
            this.Members = new System.Collections.ObjectModel.ObservableCollection<ITunableViewModel>();
            this.Members.CollectionChanged += new NotifyCollectionChangedEventHandler(Members_CollectionChanged);
            if (tunable.Definition.HideWidgets == false)
            {
                if ((tunable.Definition as PointerMember).Policy == PointerMember.PointerPolicy.Owner ||
                    (tunable.Definition as PointerMember).Policy == PointerMember.PointerPolicy.SimpleOwner)
                {
                    if ((tunable.Definition as PointerMember).ObjectType == null)
                    {
                        // We cannot load a meta file with an unresolved pointer type. This would produce
                        // incorrect data and mean that serialising the file back out would create dangerous data
                        RSG.Base.Logging.Log.Log__Error("Unresolved pointer type with name {0}, couldn't find the definition anywhere.", (tunable.Definition as PointerMember).ObjectTypeName);
                        throw new NotSupportedException("We cannot load a meta file with an unresolved pointer type");
                    }

                    if (tunable.Value is ITunable)
                    {
                        this.Members.Add(TunableViewModelFactory.Create(this, (tunable.Value) as ITunable, root));
                    }
                    else
                    {
                        Structure itemStruct = (tunable.Definition as PointerMember).ObjectType;
                        StructMember sm = new StructMember(itemStruct, (tunable.Definition as PointerMember).ParentStructure);
                        tunable.Value = TunableFactory.Create(tunable, sm);
                        this.Members.Add(TunableViewModelFactory.Create(this, (tunable.Value) as ITunable, root));
                    }
                }
            }
            if (this.Model.ParentModel is PointerTunable)
            {
                PropertyChangedEventManager.AddListener(this.Model.ParentModel as ITunable, this, "Value");
            }


            m_PreviousMembers = new System.Collections.ObjectModel.ObservableCollection<ITunableViewModel>();
            foreach (ITunableViewModel vm in m_Members)
                m_PreviousMembers.Add(vm);
        }

        /// <summary>
        /// 
        /// </summary>
        public HierarchicalTunableViewModel(ITunableViewModel parent, XIncludeTunable tunable, MetaFileViewModel root)
            : base(parent, tunable, root)
        {
            this.Parent = parent;
            this.Members = new System.Collections.ObjectModel.ObservableCollection<ITunableViewModel>();
            this.Members.CollectionChanged += new NotifyCollectionChangedEventHandler(Members_CollectionChanged);
            if (tunable.Definition.HideWidgets == false)
            {
                this.Members.Add(TunableViewModelFactory.Create(this, tunable.Data, root));
            }
            if (this.Model.ParentModel is PointerTunable)
            {
                PropertyChangedEventManager.AddListener(this.Model.ParentModel as ITunable, this, "Value");
            }


            m_PreviousMembers = new System.Collections.ObjectModel.ObservableCollection<ITunableViewModel>();
            foreach (ITunableViewModel vm in m_Members)
                m_PreviousMembers.Add(vm);
        }
        #endregion // Constructor(s)

        #region IEnumerable<ITunable> Interface

        /// <summary>
        /// 
        /// </summary>
        public IEnumerator<ITunableViewModel> GetEnumerator()
        {
            return (this.Members.GetEnumerator());
        }

        /// <summary>
        /// 
        /// </summary>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return (GetEnumerator());
        }

        #endregion // IEnumerable<ITunable> Interface

        #region Commands

        #region RequestAddCommand

        protected override void RequestAddCommand(Object parameter)
        {
            if (this is ArrayTunableViewModel)
            {
                (this as ArrayTunableViewModel).AddElement(this.Members.Count, parameter as IMember);
            }
            else if (this is MapTunableViewModel)
            {
                (this as MapTunableViewModel).AddElement(this.Members.Count, parameter as IMember);
            }
        }

        #endregion // RequestAddCommand

        #region InsertCommand

        protected override void RequestInsertCommand(Object parameter)
        {
            if (this.Parent != null && this.Parent is ArrayTunableViewModel)
            {
                ArrayTunableViewModel parent = this.Parent as ArrayTunableViewModel;
                parent.AddElement(parent.Members.IndexOf(this), parameter as IMember);
            }
            else if (this.Parent != null && this.Parent is MapTunableViewModel)
            {
                MapTunableViewModel parent = this.Parent as MapTunableViewModel;
                parent.AddElement(parent.Members.IndexOf(this), parameter as IMember);
            }
        }

        #endregion // InsertCommand

        #endregion // Commands

        #region IWeakEventListener

        /// <summary>
        /// true if the listener handled the event. It is considered an error by the
        /// System.Windows.WeakEventManager handling in WPF to register a listener for 
        /// an event that the listener does not handle. Regardless, the method should
        /// return false if it receives an event that it does not recognize or handle.
        /// </summary>
        public Boolean ReceiveWeakEvent(Type managerType, Object sender, EventArgs e)
        {
            if (e is EditorPropertyChangedEventArgs)
            {
                EditorPropertyChangedEventArgs args = e as EditorPropertyChangedEventArgs;
                if (args.Model != args.OriginalModel)
                    return true;
            }

            if (managerType == typeof(System.ComponentModel.PropertyChangedEventManager))
            {
                // Pointer value has changed
                if (this.Parent is ArrayTunableViewModel)
                {
                    if ((this.Model.ParentModel as PointerTunable).Value is ITunable)
                    {
                        ITunable tunable = (this.Model.ParentModel as PointerTunable).Value as ITunable;
                        int thisIndex = (this.Parent as ArrayTunableViewModel).Members.IndexOf(this);

                        (this.Parent as ArrayTunableViewModel).Members.Insert(thisIndex, TunableViewModelFactory.Create(this.Parent, tunable, this.RootViewModel));
                        (this.Parent as ArrayTunableViewModel).Members.Remove(this);
                        PropertyChangedEventManager.RemoveListener(this.Model.ParentModel as ITunable, this, "Value");
                    }
                }
                else if (this.Parent is PointerTunableViewModel)
                {
                    if ((this.Model.ParentModel as PointerTunable).Value is ITunable)
                    {
                        ITunable tunable = (this.Model.ParentModel as PointerTunable).Value as ITunable;

                        System.Collections.ObjectModel.ObservableCollection<ITunableViewModel> members = new System.Collections.ObjectModel.ObservableCollection<ITunableViewModel>();
                        ITunableViewModel vm = TunableViewModelFactory.Create(this.Parent, tunable, this.RootViewModel);
                        vm.Name = this.Name;
                        if ((tunable.Definition as StructMember).Definition == RSG.Metadata.Model.StructureDictionary.NullStructure)
                        {
                            vm.FriendlyTypeName = ((this.Parent as PointerTunableViewModel).Model.Definition as PointerMember).ObjectTypeName;
                        }
                        members.Add(vm);
                        (this.Parent as PointerTunableViewModel).Members = members;

                        if ((this.Parent as PointerTunableViewModel).Parent is StructureTunableViewModel)
                        {
                            int thisIndex = ((this.Parent as PointerTunableViewModel).Parent as StructureTunableViewModel).Members.IndexOf(this);
                            ((this.Parent as PointerTunableViewModel).Parent as StructureTunableViewModel).Members.RemoveAt(thisIndex);
                            ((this.Parent as PointerTunableViewModel).Parent as StructureTunableViewModel).Members.Insert(thisIndex, (this.Parent as PointerTunableViewModel).Members[0]);
                        }
                        PropertyChangedEventManager.RemoveListener(this.Model.ParentModel as ITunable, this, "Value");
                    }
                }
            }
            else if (this is ArrayTunableViewModel && managerType == typeof(CollectionChangedEventManager))
            {
                if ((e as NotifyCollectionChangedEventArgs).NewItems != null)
                {
                    foreach (var newItem in (e as NotifyCollectionChangedEventArgs).NewItems)
                    {
                        if (newItem is ITunable)
                        {
                            // Find model index 
                            int index = (this.Model as ArrayTunable).Items.IndexOf(newItem as ITunable);
                            if (index >= 0)
                            {
                                if (newItem is PointerTunable)
                                {
                                    ITunableViewModel vm = TunableViewModelFactory.Create(this, ((newItem as PointerTunable).Value as ITunable), this.RootViewModel);
                                    this.Members.Insert(index, vm);
                                }
                                else
                                {
                                    ITunableViewModel vm = TunableViewModelFactory.Create(this, newItem as ITunable, this.RootViewModel);
                                    this.Members.Insert(index, vm);
                                }
                            }
                        }
                    }
                    this.OnPropertyChanged("FriendlyTypeName");
                }
                if ((e as NotifyCollectionChangedEventArgs).OldItems != null)
                {
                    foreach (var oldItem in (e as NotifyCollectionChangedEventArgs).OldItems)
                    {
                        if (oldItem is ITunable)
                        {
                            // Find model index 
                            int index = 0;
                            Boolean foundViewModel = false;
                            foreach (ITunableViewModel vm in this.Members)
                            {
                                if (oldItem is PointerTunable)
                                {
                                    if (vm.Model == (oldItem as PointerTunable).Value)
                                    {
                                        foundViewModel = true;
                                        break;
                                    }
                                }
                                else
                                {
                                    if (vm.Model == oldItem as ITunable)
                                    {
                                        foundViewModel = true;
                                        break;
                                    }
                                }
                                index++;
                            }
                            if (foundViewModel)
                            {
                                this.Members.RemoveAt(index);
                            }
                        }
                    }
                    this.OnPropertyChanged("FriendlyTypeName");
                }
            }
            else if (this is MapTunableViewModel && managerType == typeof(CollectionChangedEventManager))
            {
                if ((e as NotifyCollectionChangedEventArgs).NewItems != null)
                {
                    foreach (var newItem in (e as NotifyCollectionChangedEventArgs).NewItems)
                    {
                        if (newItem is ITunable)
                        {
                            // Find model index 
                            int index = (this.Model as MapTunable).Items.IndexOf(newItem as MapItemTunable);
                            if (index >= 0)
                            {
                                if (newItem is MapItemTunable)
                                {
                                    this.Members.Insert(index, new MapItemTunableViewModel(this, newItem as MapItemTunable, this.RootViewModel));
                                }
                            }
                        }
                    }
                    this.OnPropertyChanged("FriendlyTypeName");
                }
                if ((e as NotifyCollectionChangedEventArgs).OldItems != null)
                {
                    foreach (var oldItem in (e as NotifyCollectionChangedEventArgs).OldItems)
                    {
                        if (oldItem is ITunable)
                        {
                            // Find model index 
                            int index = 0;
                            Boolean foundViewModel = false;
                            foreach (ITunableViewModel vm in this.Members)
                            {
                                if (oldItem is PointerTunable)
                                {
                                    if (vm.Model == (oldItem as PointerTunable).Value)
                                    {
                                        foundViewModel = true;
                                        break;
                                    }
                                }
                                else
                                {
                                    if ((vm as TunableViewModel).RawModel == oldItem as ITunable)
                                    {
                                        foundViewModel = true;
                                        break;
                                    }
                                }
                                index++;
                            }
                            if (foundViewModel)
                            {
                                this.Members.RemoveAt(index);
                            }
                        }
                    }
                    this.OnPropertyChanged("FriendlyTypeName");
                }
            }
            return true;
        }

        #endregion // IWeakEventListener

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        internal static ITunableViewModel SearchStart = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public override Boolean IsMatch(Regex expression)
        {
            if (this is MapItemTunableViewModel)
            {
                if (this.RawModel is MapItemTunable)
                {
                    string key = (this.RawModel as MapItemTunable).Key;
                    if (expression.IsMatch(key))
                    {
                        return true;
                    }
                }

                return false;
            }
            if (this is ArrayTunableViewModel || this is MapTunableViewModel)
            {

                string typeName = null;
                if (this is ArrayTunableViewModel)
                    typeName = (this as ArrayTunableViewModel).FriendlyTypeName;
                else if (this is MapTunableViewModel)
                    typeName = (this as MapTunableViewModel).FriendlyTypeName;

                if (typeName == null)
                    return false;

                int index = typeName.IndexOf('[');
                if (index == -1)
                    return false;

                typeName = typeName.Substring(0, index);
                if (expression.IsMatch(typeName))
                {
                    return true;
                }
                return false;
            }

            if (this.Model is StructureTunable)
            {
                // See whether or not the type is a match
                if (expression.IsMatch(this.FriendlyTypeName))
                {
                    return true;
                }

                if ((this.Model as StructureTunable).TunableKey is StringTunable)
                {
                    if (((this.Model as StructureTunable).TunableKey as StringTunable).Value == null)
                        return false;

                    if (expression.IsMatch(((this.Model as StructureTunable).TunableKey as StringTunable).Value))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        internal override ITunableViewModel FindNextInChildren(Regex expression)
        {
            for (int i = 0; i < this.Members.Count; i++)
            {
                ITunableViewModel result = FindNextInChildren(expression, i, false);
                if (result != null)
                    return result;
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        internal ITunableViewModel FindNextInChildren(Regex expression, ITunableViewModel start, bool checkParents)
        {
            bool canSearch = false;
            for (int i = 0; i < this.Members.Count; i++)
            {
                if (canSearch)
                {
                    ITunableViewModel result = FindNextInChildren(expression, i, false);
                    if (result != null)
                        return result;
                }
                else if (this.Members[i] == start)
                {
                    canSearch = true;
                }
            }

            if (this.Parent is HierarchicalTunableViewModel && checkParents)
            {
                ITunableViewModel result = (this.Parent as HierarchicalTunableViewModel).FindNextInChildren(expression, this, checkParents);
                if (result != null)
                    return result;
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        private ITunableViewModel FindNextInChildren(Regex expression, int index, bool checkParents)
        {
            if (this.Members.Count <= index)
                return null;

            ITunableViewModel child = this.Members[index];
            if (child.IsMatch(expression))
                return child;

            return child.FindNext(expression, checkParents);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="start"></param>
        /// <param name="checkParents"></param>
        /// <param name="checkChildren"></param>
        /// <returns></returns>
        internal ITunableViewModel FindPreviousInChildren(Regex expression)
        {
            for (int i = this.Members.Count - 1; i >= 0; i--)
            {
                if (this.Members[i] is HierarchicalTunableViewModel)
                {
                    ITunableViewModel result = (this.Members[i] as HierarchicalTunableViewModel).FindPreviousInChildren(expression);
                    if (result != null)
                        return result;
                }

                if (this.Members[i].IsMatch(expression))
                    return this.Members[i];      
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="start"></param>
        /// <param name="checkParents"></param>
        /// <returns></returns>
        internal ITunableViewModel FindPreviousInChildren(Regex expression, ITunableViewModel start, bool checkParents, bool checkChildren)
        {
            bool canSearch = false;
            for (int i = this.Members.Count - 1; i >= 0; i--)
            {
                if (canSearch)
                {
                    if (checkChildren && this.Members[i] is HierarchicalTunableViewModel)
                    {
                        ITunableViewModel result = (this.Members[i] as HierarchicalTunableViewModel).FindPreviousInChildren(expression);
                        if (result != null)
                            return result;
                    }
                    if (this.Members[i].IsMatch(expression))
                        return this.Members[i];
                }
                else if (this.Members[i] == start)
                {
                    canSearch = true;
                }
            }

            if (this.Parent is HierarchicalTunableViewModel && checkParents)
            {
                ITunableViewModel result = (this.Parent as HierarchicalTunableViewModel).FindPreviousInChildren(expression, this, checkParents, checkChildren);
                if (result != null)
                    return result;
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ITunableViewModel> GetAllChildren()
        {
            foreach (var child in this.Members)
            {
                yield return child;
                if (child is HierarchicalTunableViewModel)
                {
                    foreach (var grandChild in (child as HierarchicalTunableViewModel).GetAllChildren())
                    {
                        yield return grandChild;
                    }
                }
            }
        }
        #endregion // Methods
    } // HierarchicalTunableViewModel

    /// <summary>
    /// 
    /// </summary>
    public class ArrayTunableViewModel : HierarchicalTunableViewModel
    {
        #region Properties

        public new String FriendlyTypeName
        {
            get 
            {
                if ((this.Model.Definition as ArrayMember).ElementType is PointerMember)
                {
                    String elementName = ((this.Model.Definition as ArrayMember).ElementType as PointerMember).ObjectTypeName;
                    return String.Format("{0}*[{1}]", elementName, this.Members.Count.ToString());
                }
                else
                {
                    String elementName = (this.Model.Definition as ArrayMember).ElementType.FriendlyTypeName;
                    return String.Format("{0}[{1}]", (this.Model.Definition as ArrayMember).ElementType.FriendlyTypeName, this.Members.Count.ToString());
                }
            }
        }

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public ArrayTunableViewModel(ITunableViewModel parent, ITunableArray tunable, MetaFileViewModel root)
            : base(parent, tunable, root)
        {
            CollectionChangedEventManager.AddListener((tunable as ArrayTunable).Items, this);
        }

        #endregion // Constructor(s)

        #region Commands

        protected override bool CanAddCommand()
        {
            if ( this.Model.Definition is ArrayMember )
            {
                ArrayMember model = (ArrayMember)this.Model.Definition;
                if ( model.IsFixedSize() )
                {
                    return false;
                }
            }

            return base.CanAddCommand();
        }

        protected override List<CommandViewModel> ChildElementCommands()
        {
            if ((this.Model.Definition as ArrayMember).ElementType is PointerMember)
            {
                SortedList<string, CommandViewModel> children = new SortedList<string, CommandViewModel>();
                Structure objectType = ((this.Model.Definition as ArrayMember).ElementType as PointerMember).ObjectType;
                if (objectType == null)
                    return children.Values.ToList();

                foreach (Structure structure in GetChildren(objectType, true, true))
                {
                    if (structure.Constuctable == true)
                    {
                        children.Add(structure.DataType,
                            new CommandViewModel(
                                String.Format("{0}", structure.DataType),
                                new StructMember(structure, this.Model.Definition.ParentStructure),
                                new RelayCommand())
                        );
                    }
                }
                if (this.Model.ParentModel is PointerTunable)
                {
                    children.Add(RSG.Metadata.Model.StructureDictionary.NullStructure.DataType,
                        new CommandViewModel(
                            String.Format("{0}", RSG.Metadata.Model.StructureDictionary.NullStructure.DataType),
                            new StructMember(RSG.Metadata.Model.StructureDictionary.NullStructure, this.Model.Definition.ParentStructure),
                            new RelayCommand())
                    );
                }
                return children.Values.ToList();
            }
            else
            {
                return new List<CommandViewModel>
                {
                    new CommandViewModel(
                        String.Format("{0}", (this.Model.Definition as ArrayMember).ElementType.FriendlyTypeName), 
                        this.Model.Definition,
                        new RelayCommand())     
                };
            }
        }

        private IEnumerable<Structure> GetChildren(Structure root, Boolean includeSelf, Boolean recursive)
        {
            if (includeSelf == true)
                yield return root;

            foreach (Structure child in root.ImmediateDescendants)
            {
                yield return child;
                if (recursive == true)
                {
                    foreach (Structure grandChild in GetChildren(child, false, true))
                    {
                        yield return grandChild;
                    }
                }
            }
        }

        public void AddElement(int index, IMember member)
        {
            using (BatchUndoRedoBlock block = new BatchUndoRedoBlock(CommandManager.CommandManagerForModel(this.RootViewModel.Model)))
            {
                if ((this.Model.Definition as ArrayMember).ElementType is PointerMember)
                {
                    ITunable newTunable = TunableFactory.Create(this.Model, ((this.Model.Definition as ArrayMember).ElementType as PointerMember));
                    if (newTunable is PointerTunable)
                    {
                        IMember valueMember = member;
                        if (valueMember == null)
                            valueMember = new StructMember(((this.Model.Definition as ArrayMember).ElementType as PointerMember).ObjectType, this.Model.Definition.ParentStructure);

                        (newTunable as PointerTunable).Value = TunableFactory.Create(newTunable, valueMember);
                    }
                    (this.Model as ArrayTunable).Insert(index, newTunable);
                    this.IsExpanded = true;
                    this.RootViewModel.UnSelectAll();
                    if (this.Members[index] is TunableViewModel)
                    {
                        (this.Members[index] as TunableViewModel).IsSelected = true;
                        (this.Members[index] as TunableViewModel).IsExpanded = true;
                    }
                }
                else
                {
                    ITunable newTunable = TunableFactory.Create(this.Model, (this.Model.Definition as ArrayMember).ElementType);
                    (this.Model as ArrayTunable).Insert(index, newTunable);
                    this.IsExpanded = true;
                    this.RootViewModel.UnSelectAll();
                    if (this.Members[index] is TunableViewModel)
                    {
                        (this.Members[index] as TunableViewModel).IsSelected = true;
                        (this.Members[index] as TunableViewModel).IsExpanded = true;
                    }
                }
                this.OnPropertyChanged("FriendlyTypeName");
            }
        }

        public void AddElement(int index, IMember member, ITunable source)
        {
            if ((this.Model.Definition as ArrayMember).ElementType is PointerMember)
            {
                ITunable newTunable = TunableFactory.Create(this.Model, ((this.Model.Definition as ArrayMember).ElementType as PointerMember));
                if (newTunable is PointerTunable)
                {
                    IMember valueMember = member;
                    if (valueMember == null)
                        valueMember = new StructMember(((this.Model.Definition as ArrayMember).ElementType as PointerMember).ObjectType, this.Model.Definition.ParentStructure);

                    (newTunable as PointerTunable).Value = TunableFactory.Create(newTunable, valueMember);
                    ((newTunable as PointerTunable).Value as ITunable).SetTunableValue(source);
                }
                else
                {
                    newTunable.SetTunableValue(source);
                }
                (this.Model as ArrayTunable).Insert(index, newTunable);
                this.IsExpanded = true;
                this.RootViewModel.UnSelectAll();
                if (this.Members[index] is HierarchicalTunableViewModel)
                {
                    (this.Members[index] as HierarchicalTunableViewModel).IsSelected = true;
                    (this.Members[index] as HierarchicalTunableViewModel).IsExpanded = true;
                }
            }
            else
            {
                ITunable newTunable = TunableFactory.Create(this.Model, (this.Model.Definition as ArrayMember).ElementType);
                newTunable.SetTunableValue(source);
                (this.Model as ArrayTunable).Insert(index, newTunable);
                this.IsExpanded = true;
                this.RootViewModel.UnSelectAll();
                if (this.Members[index] is HierarchicalTunableViewModel)
                {
                    (this.Members[index] as HierarchicalTunableViewModel).IsSelected = true;
                    (this.Members[index] as HierarchicalTunableViewModel).IsExpanded = true;
                }
            }
            this.OnPropertyChanged("FriendlyTypeName");
        }

        public void RemoveElement(int index)
        {
            (this.Model as ArrayTunable).RemoveAt(index);
            this.OnPropertyChanged("FriendlyTypeName");
        }

        #region RequestChangeCommand

        protected override void RequestChangeCommand(Object parameter)
        {
            if (this.Parent is ArrayTunableViewModel)
            {
                int thisIndex = (this.Parent as ArrayTunableViewModel).Members.IndexOf(this);
                ITunable newTunable = TunableFactory.Create(this.Model.ParentModel as ITunable, parameter as IMember);

                (this.Model.ParentModel as PointerTunable).Value = newTunable;
                (this.Parent as ArrayTunableViewModel).Members.Insert(thisIndex, TunableViewModelFactory.Create(this.Parent, newTunable, this.RootViewModel));
                (this.Parent as ArrayTunableViewModel).Members.Remove(this);
            }
        }

        #endregion // RequestChangeCommand

        #endregion // Commands
    } // ArrayTunableViewModel

    /// <summary>
    /// 
    /// </summary>
    public class MapTunableViewModel : HierarchicalTunableViewModel
    {
        #region Properties

        public new String FriendlyTypeName
        {
            get
            {
                if ((this.Model.Definition as MapMember).ElementType is PointerMember)
                {
                    String elementName = ((this.Model.Definition as MapMember).ElementType as PointerMember).ObjectTypeName;
                    return String.Format("{0}*[{1}]", elementName, this.Members.Count.ToString());
                }
                else
                {
                    String elementName = (this.Model.Definition as MapMember).ElementType.FriendlyTypeName;
                    return String.Format("{0}[{1}]", (this.Model.Definition as MapMember).ElementType.FriendlyTypeName, this.Members.Count.ToString());
                }
            }
        }

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public MapTunableViewModel(ITunableViewModel parent, MapTunable tunable, MetaFileViewModel root)
            : base(parent, tunable, root)
        {
            CollectionChangedEventManager.AddListener((tunable as MapTunable).Items, this);
        }

        #endregion // Constructor(s)

        #region Commands
        protected override bool CanAddCommand()
        {
            return true;
        }

        protected override List<CommandViewModel> ChildElementCommands()
        {
            if ((this.Model.Definition as MapMember).ElementType is PointerMember)
            {
                SortedList<string, CommandViewModel> children = new SortedList<string, CommandViewModel>();
                Structure objectType = ((this.Model.Definition as MapMember).ElementType as PointerMember).ObjectType;
                foreach (Structure structure in GetChildren(objectType, true, true))
                {
                    if (structure.Constuctable == true)
                    {
                        children.Add(structure.DataType,
                            new CommandViewModel(
                                String.Format("{0}", structure.DataType),
                                new StructMember(structure, this.Model.Definition.ParentStructure),
                                new RelayCommand())
                        );
                    }
                }
                if (this.Model.ParentModel is PointerTunable)
                {
                    children.Add(RSG.Metadata.Model.StructureDictionary.NullStructure.DataType,
                        new CommandViewModel(
                            String.Format("{0}", RSG.Metadata.Model.StructureDictionary.NullStructure.DataType),
                            new StructMember(RSG.Metadata.Model.StructureDictionary.NullStructure, this.Model.Definition.ParentStructure),
                            new RelayCommand())
                    );
                }
                return children.Values.ToList();
            }
            else
            {
                return new List<CommandViewModel>
                {
                    new CommandViewModel(
                        String.Format("{0}", (this.Model.Definition as MapMember).ElementType.FriendlyTypeName), 
                        this.Model.Definition,
                        new RelayCommand())     
                };
            }
        }

        private IEnumerable<Structure> GetChildren(Structure root, Boolean includeSelf, Boolean recursive)
        {
            if (includeSelf == true)
                yield return root;

            foreach (Structure child in root.ImmediateDescendants)
            {
                yield return child;
                if (recursive == true)
                {
                    foreach (Structure grandChild in GetChildren(child, false, true))
                    {
                        yield return grandChild;
                    }
                }
            }
        }

        public void AddElement(int index, IMember member)
        {
            using (BatchUndoRedoBlock block = new BatchUndoRedoBlock(CommandManager.CommandManagerForModel(this.RootViewModel.Model)))
            {
                if ((this.Model.Definition as MapMember).ElementType is PointerMember)
                {
                    ITunable newTunable = TunableFactory.Create(this.Model, ((this.Model.Definition as MapMember).ElementType as PointerMember));
                    if (newTunable is PointerTunable)
                    {
                        IMember valueMember = member;
                        if (valueMember == null)
                            valueMember = new StructMember(((this.Model.Definition as MapMember).ElementType as PointerMember).ObjectType, this.Model.Definition.ParentStructure);

                        (newTunable as PointerTunable).Value = TunableFactory.Create(newTunable, valueMember);
                    }
                    (this.Model as MapTunable).Insert(index, new MapItemTunable(newTunable, this.Model));
                    this.IsExpanded = true;
                    this.RootViewModel.UnSelectAll();
                    if (this.Members[index] is TunableViewModel)
                    {
                        (this.Members[index] as TunableViewModel).IsSelected = true;
                        (this.Members[index] as TunableViewModel).IsExpanded = true;
                    }
                }
                else
                {
                    ITunable newTunable = TunableFactory.Create(this.Model, (this.Model.Definition as MapMember).ElementType);
                    (this.Model as MapTunable).Insert(index, new MapItemTunable(newTunable, this.Model));
                    this.IsExpanded = true;
                    this.RootViewModel.UnSelectAll();
                    if (this.Members[index] is TunableViewModel)
                    {
                        (this.Members[index] as TunableViewModel).IsSelected = true;
                        (this.Members[index] as TunableViewModel).IsExpanded = true;
                    }
                }
                this.OnPropertyChanged("FriendlyTypeName");
            }
        }

        public void AddElement(int index, IMember member, ITunable source)
        {
            if ((this.Model.Definition as MapMember).ElementType is PointerMember)
            {
                ITunable newTunable = TunableFactory.Create(this.Model, ((this.Model.Definition as MapMember).ElementType as PointerMember));
                if (newTunable is PointerTunable)
                {
                    IMember valueMember = member;
                    if (valueMember == null)
                        valueMember = new StructMember(((this.Model.Definition as MapMember).ElementType as PointerMember).ObjectType, this.Model.Definition.ParentStructure);

                    if (source is MapItemTunable && (source as MapItemTunable).Value is PointerTunable)
                    {
                        ITunable valueToCopy = ((source as MapItemTunable).Value as PointerTunable);
                        (newTunable as PointerTunable).SetTunableValue(valueToCopy);
                    }
                }
                else
                {
                    newTunable.SetTunableValue(source);
                }

                MapItemTunable newMapTunable = new MapItemTunable(newTunable, this.Model);
                if (source is MapItemTunable)
                    newMapTunable.Key = (source as MapItemTunable).Key;

                (this.Model as MapTunable).Insert(index, newMapTunable);
            }
            else
            {
                ITunable newTunable = TunableFactory.Create(this.Model, (this.Model.Definition as MapMember).ElementType);
                if (source is MapItemTunable && (source as MapItemTunable).Value.GetType() == newTunable.GetType()) 
                {
                    ITunable valueToCopy = ((source as MapItemTunable).Value as ITunable);
                    newTunable.SetTunableValue(valueToCopy);
                }
                else
                {
                    newTunable.SetTunableValue(source);
                }

                MapItemTunable newMapTunable = new MapItemTunable(newTunable, this.Model);
                if (source is MapItemTunable)
                    newMapTunable.Key = (source as MapItemTunable).Key;

                (this.Model as MapTunable).Insert(index, newMapTunable);
            }
            this.OnPropertyChanged("FriendlyTypeName");
        }

        public void RemoveElement(int index)
        {
            (this.Model as MapTunable).RemoveAt(index);
            this.OnPropertyChanged("FriendlyTypeName");
        }
        #endregion
    } // MapTunableViewModel

    /// <summary>
    /// 
    /// </summary>
    public class MapItemTunableViewModel : HierarchicalTunableViewModel
    {
        #region Properties

        public new String FriendlyTypeName
        {
            get
            {
                return "Map Entry";
            }
        }

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public MapItemTunableViewModel(ITunableViewModel parent, MapItemTunable tunable, MetaFileViewModel root)
            : base(parent, tunable, root)
        {
        }

        #endregion // Constructor(s)
    } // MapTunableViewModel
    /// <summary>
    /// 
    /// </summary>
    public class StructureTunableViewModel : HierarchicalTunableViewModel
    {
        #region Properties

        public new String FriendlyTypeName
        {
            get
            {
                if (this.Model.ParentModel is PointerTunable)
                {
                    if (this.Model.Definition is StructMember && (this.Model.Definition as StructMember).Definition == RSG.Metadata.Model.StructureDictionary.NullStructure)
                    {
                        return ((this.Model.ParentModel as PointerTunable).Definition as PointerMember).ObjectTypeName;
                    }
                }
                return this.Model.Definition.FriendlyTypeName;
            }
        }

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public StructureTunableViewModel(ITunableViewModel parent, ITunableStructure tunable, MetaFileViewModel root)
            : base(parent, tunable, root)
        {
        }

        #endregion // Constructor(s)

        #region Commands

        protected override List<CommandViewModel> ChildElementCommands()
        {
            if (this.Parent != null && this.Parent is ArrayTunableViewModel)
            {
                if ((this.Parent.Model.Definition as ArrayMember).ElementType is PointerMember)
                {
                    SortedList<string, CommandViewModel> children = new SortedList<string, CommandViewModel>();
                    Structure objectType = ((this.Parent.Model.Definition as ArrayMember).ElementType as PointerMember).ObjectType;
                    foreach (Structure structure in GetChildren(objectType, true, true))
                    {
                        if (structure.Constuctable == true)
                        {
                            children.Add(structure.DataType,
                                new CommandViewModel(
                                    String.Format("{0}", structure.DataType),
                                    new StructMember(structure, this.Model.Definition.ParentStructure),
                                    new RelayCommand())
                            );
                        }
                    }
                    if (this.Model.ParentModel is PointerTunable)
                    {
                        children.Add(RSG.Metadata.Model.StructureDictionary.NullStructure.DataType,
                            new CommandViewModel(
                                String.Format("{0}", RSG.Metadata.Model.StructureDictionary.NullStructure.DataType),
                                new StructMember(RSG.Metadata.Model.StructureDictionary.NullStructure, this.Model.Definition.ParentStructure),
                                new RelayCommand())
                        );
                    }
                    return children.Values.ToList();
                }
                else
                {
                    return new List<CommandViewModel>
                    {
                        new CommandViewModel(
                            String.Format("{0}", (this.Model.Definition as StructMember).FriendlyTypeName), 
                            this.Model.Definition,
                            new RelayCommand())     
                    };
                }
            }
            else if (this.Parent is PointerTunableViewModel)
            {
                SortedList<string, CommandViewModel> children = new SortedList<string, CommandViewModel>();
                Structure objectType = (this.Parent.Model.Definition as PointerMember).ObjectType;
                foreach (Structure structure in GetChildren(objectType, true, true))
                {
                    if (structure.Constuctable == true)
                    {
                        children.Add(structure.DataType,
                            new CommandViewModel(
                                String.Format("{0}", structure.DataType),
                                new StructMember(structure, this.Model.Definition.ParentStructure),
                                new RelayCommand())
                        );
                    }
                }
                if (this.Model.ParentModel is PointerTunable)
                {
                    children.Add(RSG.Metadata.Model.StructureDictionary.NullStructure.DataType,
                        new CommandViewModel(
                            String.Format("{0}", RSG.Metadata.Model.StructureDictionary.NullStructure.DataType),
                            new StructMember(RSG.Metadata.Model.StructureDictionary.NullStructure, this.Model.Definition.ParentStructure),
                            new RelayCommand())
                    );
                }
                return children.Values.ToList();
            }
            else
            {
                return new List<CommandViewModel>
                {
                };
            }
        }

        private IEnumerable<Structure> GetChildren(Structure root, Boolean includeSelf, Boolean recursive)
        {
            if (includeSelf == true)
                yield return root;

            foreach (Structure child in root.ImmediateDescendants)
            {
                yield return child;
                if (recursive == true)
                {
                    foreach (Structure grandChild in GetChildren(child, false, true))
                    {
                        yield return grandChild;
                    }
                }
            }
        }

        #region RequestChangeCommand

        protected override void RequestChangeCommand(Object parameter)
        {
            if (this.Parent is ArrayTunableViewModel)
            {
                int thisIndex = (this.Parent as ArrayTunableViewModel).Members.IndexOf(this);
                ITunable newTunable = TunableFactory.Create(this.Model.ParentModel as ITunable, parameter as IMember);

                (this.Model.ParentModel as PointerTunable).Value = newTunable;
                this.OnPropertyChanged("FriendlyTypeName");
            }
            else if (this.Parent is PointerTunableViewModel)
            {
                ITunable newTunable = TunableFactory.Create(this.Parent.Model, parameter as IMember);
                (this.Parent.Model as PointerTunable).Value = newTunable;
                this.OnPropertyChanged("FriendlyTypeName");
            }
        }

        #endregion // RequestChangeCommand

        #endregion // Commands
    } // StructureTunableViewModel

    /// <summary>
    /// 
    /// </summary>
    public class PointerTunableViewModel : HierarchicalTunableViewModel
    {
        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public PointerTunableViewModel(ITunableViewModel parent, PointerTunable tunable, MetaFileViewModel root)
            : base(parent, tunable, root)
        {
            if (this.Members != null)
            {
                foreach (ITunableViewModel member in this.Members)
                {
                    member.Name = this.Name;
                    if ((member.Model.Definition as StructMember).Definition == RSG.Metadata.Model.StructureDictionary.NullStructure)
                    {
                        member.FriendlyTypeName = (tunable.Definition as PointerMember).ObjectTypeName;
                    }
                }
            }
        }

        #endregion // Constructor(s)

        #region Commands
        
        protected override List<CommandViewModel> ChildElementCommands()
        {
            SortedList<string, CommandViewModel> children = new SortedList<string, CommandViewModel>();
            Structure objectType = (this.Model.Definition as PointerMember).ObjectType;
            foreach (Structure structure in GetChildren(objectType, true, true))
            {
                if (structure.Constuctable == true)
                {
                    children.Add(structure.DataType,
                        new CommandViewModel(
                            String.Format("{0}", structure.DataType),
                            new StructMember(structure, this.Model.Definition.ParentStructure),
                            new RelayCommand())
                    );
                }
            }
            if (this.Model.ParentModel is PointerTunable)
            {
                children.Add(RSG.Metadata.Model.StructureDictionary.NullStructure.DataType,
                    new CommandViewModel(
                        String.Format("{0}", RSG.Metadata.Model.StructureDictionary.NullStructure.DataType),
                        new StructMember(RSG.Metadata.Model.StructureDictionary.NullStructure, this.Model.Definition.ParentStructure),
                        new RelayCommand())
                );
            }

            return children.Values.ToList();
        }

        private IEnumerable<Structure> GetChildren(Structure root, Boolean includeSelf, Boolean recursive)
        {
            if (includeSelf == true)
                yield return root;

            foreach (Structure child in root.ImmediateDescendants)
            {
                yield return child;
                if (recursive == true)
                {
                    foreach (Structure grandChild in GetChildren(child, false, true))
                    {
                        yield return grandChild;
                    }
                }
            }
        }

        #region RequestChangeCommand

        protected override void RequestChangeCommand(Object parameter)
        {
            if (this.Parent is ArrayTunableViewModel)
            {
                int thisIndex = (this.Parent as ArrayTunableViewModel).Members.IndexOf(this);
                ITunable newTunable = TunableFactory.Create(this.Model.ParentModel as ITunable, parameter as IMember);

                (this.Model.ParentModel as PointerTunable).Value = newTunable;
                (this.Parent as ArrayTunableViewModel).Members.Insert(thisIndex, TunableViewModelFactory.Create(this.Parent, newTunable, this.RootViewModel));
                (this.Parent as ArrayTunableViewModel).Members.Remove(this);
            }
        }

        #endregion // RequestChangeCommand

        #endregion // Commands
    } // StructureTunableViewModel

    /// <summary>
    /// 
    /// </summary>
    public class XiTunableViewModel : HierarchicalTunableViewModel
    {
        #region Properties
        public new String FriendlyTypeName
        {
            get
            {
                return "External Metadata File";
            }
        }
        #endregion

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public XiTunableViewModel(ITunableViewModel parent, XIncludeTunable tunable, MetaFileViewModel root)
            : base(parent, tunable, root)
        {
        }

        #endregion
    } // StructureTunableViewModel
} // MetadataEditor.AddIn.MetaEditor.ViewModel
