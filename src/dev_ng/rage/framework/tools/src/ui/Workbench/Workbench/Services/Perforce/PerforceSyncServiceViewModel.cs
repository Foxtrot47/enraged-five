﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using Workbench.AddIn.Services;
using RSG.SourceControl.Perforce;
using System.Collections.ObjectModel;
using System.ComponentModel;
using RSG.Base.Windows.Helpers;
using RSG.Base.Tasks;
using System.Threading;
using System.IO;

namespace Workbench.Services.Perforce
{
    public class PerforceSyncServiceViewModel : ViewModelBase
    {
        #region Constants

        private const uint FILE_SYNC_BATCH_SIZE = 25;

        #endregion // Constants

        #region Members
        private P4 m_p4;
        private ITaskProgressService m_taskProgressService;
        private CancellationTokenSource m_cts;
        #endregion // Members

        #region Properties
        /// <summary>
        /// State of the dialog.
        /// </summary>
        public bool? DialogState
        {
            get { return m_dialogState; }
            set
            {
                SetPropertyValue(value, m_dialogState, () => this.DialogState,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_dialogState = (bool?)newValue;
                        }
                    ));
            }
        }
        private bool? m_dialogState;

        /// <summary>
        /// Whether we are currently performing the sync preview.
        /// </summary>
        public bool PerformingSyncPreview
        {

            get { return m_performingSyncPreview; }
            set
            {
                SetPropertyValue(value, m_performingSyncPreview, () => this.PerformingSyncPreview,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_performingSyncPreview = (bool)newValue;
                        }
                    ));
            }
        }
        private bool m_performingSyncPreview;

        /// <summary>
        /// 
        /// </summary>
        public PerforceItemCollectionViewModel PerforceCollection
        {
            get { return m_perforceCollection; }
            set
            {
                SetPropertyValue(value, () => this.PerforceCollection,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_perforceCollection = (PerforceItemCollectionViewModel)newValue;
                        }
                    )
                );
            }
        }
        private PerforceItemCollectionViewModel m_perforceCollection;

        /// <summary>
        /// Message to display above the files that need syncing.
        /// </summary>
        public String SyncMessage
        {
            get { return m_syncMessage; }
            set
            {
                SetPropertyValue(value, () => this.SyncMessage,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_syncMessage = (String)newValue;
                        }
                    )
                );
            }
        }
        private String m_syncMessage;

        /// <summary>
        /// Whether the user is forced to sync the data.
        /// </summary>
        public Boolean ForceSync
        {
            get { return m_forceSync; }
            set
            {
                SetPropertyValue(value, () => this.ForceSync,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_forceSync = (Boolean)newValue;
                        }
                    )
                );
            }
        }
        private Boolean m_forceSync;

        /// <summary>
        /// Whether we allow the user to cancel the sync.
        /// </summary>
        public Boolean AllowCancel
        {
            get { return m_allowCancel; }
            set
            {
                SetPropertyValue(value, () => this.AllowCancel,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_allowCancel = (Boolean)newValue;
                        }
                    )
                );
            }
        }
        private Boolean m_allowCancel;

        /// <summary>
        /// Whether the user chose an option.
        /// </summary>
        internal bool? OptionChosen { get; set; }

        /// <summary>
        /// Flag indicating whether we are currently syncing data.
        /// </summary>
        public bool IsSyncing
        {
            get { return m_isSyncing; }
            set
            {
                SetPropertyValue(value, m_isSyncing, () => this.IsSyncing,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_isSyncing = (bool)newValue;
                        }
                    )
                );
            }
        }
        private bool m_isSyncing;

        /// <summary>
        /// Flag indicating whether we are syncing less than 20 items.  This is used
        /// to control the type of progress indicator that is shown.
        /// </summary>
        public bool IsSyncingLessThan20Items
        {
            get { return m_isSyncingLessThan20Items; }
            set
            {
                SetPropertyValue(value, m_isSyncingLessThan20Items, () => this.IsSyncingLessThan20Items,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_isSyncingLessThan20Items = (bool)newValue;
                        }
                    )
                );
            }
        }
        private bool m_isSyncingLessThan20Items;

        /// <summary>
        /// How fa
        /// </summary>
        public int SyncProgress
        {
            get { return m_syncProgress; }
            set
            {
                SetPropertyValue(value, m_syncProgress, () => this.SyncProgress,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_syncProgress = (int)newValue;
                        }
                    )
                );
            }
        }
        private int m_syncProgress;

        /// <summary>
        /// Cursor to use for the view.
        /// </summary>
        public Cursor Cursor
        {
            get { return m_cursor; }
            set
            {
                SetPropertyValue(value, m_cursor, () => this.Cursor,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_cursor = (Cursor)newValue;
                        }));
            }
        }
        private Cursor m_cursor;

        /// <summary>
        /// Result of the sync process.
        /// </summary>
        public PerforceSyncResult SyncResult { get; set; }
        #endregion // Properties

        #region Commands
        /// <summary>
        /// 
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (m_cancelCommand == null)
                {
                    m_cancelCommand = new RelayCommand(param => this.Cancel(param));
                }
                return m_cancelCommand;
            }
        }
        private RelayCommand m_cancelCommand;

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand GrabLatestCommand
        {
            get
            {
                if (m_grabLatestCommand == null)
                {
                    m_grabLatestCommand = new RelayCommand(param => this.GrabLatest(param), param => this.CanGrabLatest(param));
                }
                return m_grabLatestCommand;
            }
        }
        private RelayCommand m_grabLatestCommand;

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand ContinueCommand
        {
            get
            {
                if (m_continueCommand == null)
                {
                    m_continueCommand = new RelayCommand(param => this.Continue(param), param => this.CanContinue(param));
                }
                return m_continueCommand;
            }
        }
        private RelayCommand m_continueCommand;

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand AbortSyncCommand
        {
            get
            {
                if (m_abortActiveSync == null)
                {
                    m_abortActiveSync = new RelayCommand(param => this.AbortActiveSync(param), param => this.CanAbortActiveSync(param));
                }
                return m_continueCommand;
            }
        }
        private RelayCommand m_abortActiveSync;
        #endregion // Commands

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public PerforceSyncServiceViewModel(IEnumerable<String> filenames, String syncMessage, Boolean forceSync, Boolean allowCancel, P4 p4, ITaskProgressService taskProgressService)
        {
            m_syncMessage = syncMessage;
            m_forceSync = forceSync;
            m_allowCancel = allowCancel;
            m_p4 = p4;
            m_cursor = Cursors.Arrow;
            m_taskProgressService = taskProgressService;
            SyncResult = PerforceSyncResult.Ok;

            // Kick off a background task that checks which files need syncing.
            m_performingSyncPreview = true;
            m_cts = new CancellationTokenSource();

            TaskContext context = new TaskContext(m_cts.Token);
            context.Argument = filenames.ToArray();

            ITask actionTask = new ActionTask("P4 Sync Preview", (ctx, progress) => SyncPreviewAction(ctx));
            actionTask.OnTaskCompleted += SyncPreviewCompleted;
            m_taskProgressService.Add(actionTask, context, TaskPriority.Background);
        }

        #endregion // Constructor

        #region Command Callbacks
        /// <summary>
        /// Command for cancelling the sync.
        /// </summary>
        private void Cancel(Object param)
        {
            m_cts.Cancel();
            this.OptionChosen = false;
            this.DialogState = false;
        }

        /// <summary>
        /// 
        /// </summary>
        private Boolean CanGrabLatest(Object param)
        {
            if (this.PerforceCollection != null)
            {
                return this.PerforceCollection.Items.Any(item => item.SyncItem);
            }
            
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        private void GrabLatest(Object param)
        {
            this.Cursor = Cursors.Wait;

            this.IsSyncing = true;
            this.IsSyncingLessThan20Items = (PerforceCollection.Items.Where(item => item.SyncItem == true).Count() <= FILE_SYNC_BATCH_SIZE);
            this.OptionChosen = true;
            
            TaskContext context = new TaskContext(m_cts.Token);
            ITask actionTask = new ActionTask("P4 Sync", (ctx, progress) => SyncAction(ctx, progress));
            actionTask.ReportsProgress = !this.IsSyncingLessThan20Items;
            actionTask.OnTaskCompleted += SyncCompleted;

            if (actionTask.Progress is EventProgress<TaskProgress>)
            {
                ((EventProgress<TaskProgress>)actionTask.Progress).ProgressChanged += new EventHandler<TaskProgress>(SyncProgressChanged);
            }

            m_taskProgressService.Add(actionTask, context, TaskPriority.Background);
        }

        /// <summary>
        /// 
        /// </summary>
        private Boolean CanContinue(Object param)
        {
            return !this.ForceSync;
        }

        /// <summary>
        /// 
        /// </summary>
        private void Continue(Object param)
        {
            this.OptionChosen = null;
            this.DialogState = true;
        }

        /// <summary>
        /// Returns whether the user is allowed to abort an active sync
        /// </summary>
        /// <returns></returns>
        public bool CanAbortActiveSync(Object param)
        {
            return (!this.ForceSync && this.AllowCancel);
        }

        /// <summary>
        /// 
        /// </summary>
        public void AbortActiveSync(Object param)
        {
            if (m_isSyncing)
            {
                m_cts.Cancel();
                OptionChosen = false;
                SyncResult = PerforceSyncResult.Cancelled;
            }
        }
        #endregion // Command Callbacks

        #region Private Helpers
        /// <summary>
        /// 
        /// </summary>
        private void SyncPreviewAction(ITaskContext context)
        {
            String[] localFilenames = (String[])context.Argument;

            // Convert the local file paths to depot ones
            FileMapping[] fileMappings = FileMapping.Create(m_p4, localFilenames);
            String[] depotFilenames = fileMappings.Select(mapping => mapping.DepotFilename).ToArray();
            context.Token.ThrowIfCancellationRequested();

            // Make sure some of the files exist in the depot.
            // If we didn't do this check, it would run a "p4 sync -n" command which would shows us all the files in the depot we
            // needed to sync resulting in it using a ludicrous amount of memory.
            if (depotFilenames.Any())
            {
                // Collapse the array into one string so that we only make one server request.
                List<String> perforceArgs = new List<String>();
                perforceArgs.Add("-n");
                perforceArgs.AddRange(depotFilenames);
                
                // From the given filenames determine which onces are not up-to-date with the head revision
                String currentDirectory = Directory.GetCurrentDirectory();

                try
                {
                    // Set the working directory.
                    String directoryName = Path.GetDirectoryName(localFilenames[0]);
                    if (!Directory.Exists(directoryName))
                    {
                        Directory.CreateDirectory(directoryName);
                    }
                    Directory.SetCurrentDirectory(directoryName);

                    List<P4API.P4Record> recordSet = new List<P4API.P4Record>();
                    P4API.P4RecordSet result = m_p4.Run("sync", perforceArgs.ToArray());
                    recordSet.AddRange(result.Records);

                    if (recordSet.Count > 0)
                    {
                        PerforceItemCollection model = new PerforceItemCollection(recordSet);
                        context.ReturnValue = new PerforceItemCollectionViewModel(model);
                    }
                    else
                    {
                        SyncResult = PerforceSyncResult.SyncedAll;
                    }
                }
                catch (P4API.Exceptions.P4APIExceptions ex)
                {
                    RSG.Base.Logging.Log.Log__Exception(ex, "Unhandled Perforce exception");
                    SyncResult = PerforceSyncResult.Failed;
                }
                finally
                {
                    Directory.SetCurrentDirectory(currentDirectory);
                }
            }
            else
            {
                SyncResult = PerforceSyncResult.Ok;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void SyncPreviewCompleted(object sender, TaskCompletedEventArgs e)
        {
            if (e.Result == TaskStatus.Completed)
            {
                if (e.ReturnValue == null)
                {
                    DialogState = (SyncResult == PerforceSyncResult.Ok) || (SyncResult == PerforceSyncResult.SyncedAll);
                }
                else
                {
                    PerforceCollection = (PerforceItemCollectionViewModel)e.ReturnValue;
                }
            }
            else
            {
                SyncResult = PerforceSyncResult.Failed;
                DialogState = false;
            }

            PerformingSyncPreview = false;
        }

        /// <summary>
        /// 
        /// </summary>
        private void SyncAction(ITaskContext context, IProgress<TaskProgress> progress)
        {
            IEnumerable<String> filesToSync = PerforceCollection.Items.Where(item => item.SyncItem == true).Select(item => item.DepotFile);
            List<String> fileBatch = new List<String>();
            uint filesSynced = 0;
            uint numFilesToSync = (uint)filesToSync.Count();

            // Loop over all the files we need to sync batching them up
            foreach (String item in filesToSync)
            {
                fileBatch.Add(item);

                // If we have reached the batch size
                if (fileBatch.Count == FILE_SYNC_BATCH_SIZE)
                {
                    filesSynced += SyncFileBatch(fileBatch);
                    fileBatch.Clear();

                    // Report the progress
                    progress.Report(new TaskProgress((double)filesSynced / (double)numFilesToSync, "Sync in progress."));
                }

                context.Token.ThrowIfCancellationRequested();
            }

            // Sync the last partially full batch of files
            if (fileBatch.Count > 0)
            {
                filesSynced += SyncFileBatch(fileBatch);
            }
            progress.Report(new TaskProgress(1.0, "Sync complete."));

            // Update the result of the sync
            if (numFilesToSync == PerforceCollection.Items.Count && filesSynced == numFilesToSync)
                this.SyncResult = PerforceSyncResult.SyncedAll;
            else
                this.SyncResult = PerforceSyncResult.SyncedSome;
        }

        /// <summary>
        /// 
        /// </summary>
        private void SyncProgressChanged(object sender, TaskProgress e)
        {
            if (e.Relative)
            {
                SyncProgress += (int)(e.Progress * 100);
            }
            else
            {
                SyncProgress = (int)(e.Progress * 100);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void SyncCompleted(object sender, TaskCompletedEventArgs e)
        {
            this.IsSyncing = false;
            this.Cursor = Cursors.Arrow;

            if (e.Result != TaskStatus.Completed)
            {
                this.SyncResult = PerforceSyncResult.SyncedSome;
            }
            else
            {
                this.DialogState = true;
            }
        }

        /// <summary>
        /// Sync a batch of files
        /// </summary>
        /// <param name="files"></param>
        /// <returns></returns>
        private uint SyncFileBatch(List<string> files)
        {
            uint filesSynced = 0;

            P4API.P4ExceptionLevels oldLevel = m_p4.ExceptionLevel;
            m_p4.ExceptionLevel = P4API.P4ExceptionLevels.NoExceptionOnErrors;

            P4API.P4RecordSet syncResult = m_p4.Run("sync", files.ToArray());
            OutputRecordSetErrors(syncResult);

            m_p4.ExceptionLevel = oldLevel;

            filesSynced += (uint)syncResult.Records.Length;

            return filesSynced;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="recordSet"></param>
        /// <returns></returns>
        private void OutputRecordSetErrors(P4API.P4BaseRecordSet recordSet)
        {
            foreach (string error in recordSet.Errors)
            {
                RSG.Base.Logging.Log.Log__Error(error);
            }

            foreach (string warning in recordSet.Warnings)
            {
                RSG.Base.Logging.Log.Log__Warning(warning);
            }
        }

        #endregion // Private Helpers

    } // PerforceSyncServiceViewModel
} // Workbench.Services.Perforce
