﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using RSG.Base.Tasks;
using RSG.Model.Report;
using Workbench.AddIn.Services;

namespace ContentBrowser.ViewModel.ReportViewModels
{
    /// <summary>
    /// 
    /// </summary>
    public class ReportCategoryViewModel : ContainerViewModelBase, IDisposable, IWeakEventListener
    {
        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private IReportCategory Category { get; set; }

        /// <summary>
        /// 
        /// </summary>
        private ITaskProgressService TaskProgressService { get; set; }

        /// <summary>
        /// Thread synchronisation helper object
        /// </summary>
        private object m_syncObject = new object();
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public ReportCategoryViewModel(IReportCategory category, ITaskProgressService taskProgressService)
            : base(category)
        {
            Category = category;
            TaskProgressService = taskProgressService;
            PropertyChangedEventManager.AddListener(Category, this, "Reports");
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// This gets called whenever the selection state for this view model
        /// has changed
        /// </summary>
        protected override void OnSelectionChanged(Boolean oldValue, Boolean newValue)
        {
            if (IsSelected)
            {
                lock (m_syncObject)
                {
                    if (Category.Reports == null)
                    {
                        ITask actionTask = new ActionTask("Initialise Reports", (context, progress) => Category.InitialiseReports());
                        TaskProgressService.Add(actionTask, new TaskContext(), TaskPriority.Background);
                    }
                }
            }

            RefreshGrid();
        }

        /// <summary>
        /// This gets called whenever the expansion state for this view model
        /// has changed
        /// </summary>
        protected override void OnExpansionChanged(Boolean oldValue, Boolean newValue)
        {
            if (IsExpanded)
            {
                lock (m_syncObject)
                {
                    if (Category.Reports == null)
                    {
                        ITask actionTask = new ActionTask("Initialise Reports", (context, progress) => Category.InitialiseReports());
                        TaskProgressService.Add(actionTask, new TaskContext(), TaskPriority.Background);
                    }
                }
            }

            RefreshHierarchy();
        }
        #endregion // Overrides

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void RefreshHierarchy()
        {
            this.AssetChildren.BeginUpdate();
            this.AssetChildren.Clear();
            if (IsExpanded)
            {
                if (Category.Reports == null)
                {
                    this.AssetChildren.Add(new AssetDummyViewModel("loading..."));
                }
                else
                {
                    this.AssetChildren.AddRange(Category.Reports.Select(texture => ViewModelFactory.CreateViewModel(texture)));
                }
            }
            else
            {
                this.AssetChildren.Add(new AssetDummyViewModel());
            }
            this.AssetChildren.EndUpdate();
        }

        /// <summary>
        /// 
        /// </summary>
        private void RefreshGrid()
        {
            this.GridAssets.BeginUpdate();
            this.GridAssets.Clear();
            if (IsSelected)
            {
                if (Category.Reports != null)
                {
                    this.GridAssets.AddRange(Category.Reports.Select(texture => ViewModelFactory.CreateGridViewModel(texture)));
                }
            }
            this.GridAssets.EndUpdate();
        }
        #endregion // Private Methods

        #region IWeakEventListener Implementation
        /// <summary>
        /// 
        /// </summary>
        public bool ReceiveWeakEvent(Type managerType, Object sender, EventArgs e)
        {
            if (Application.Current != null)
            {
                Application.Current.Dispatcher.Invoke(
                    new Action
                    (
                        delegate()
                        {
                            RefreshHierarchy();
                            RefreshGrid();
                        }
                    ),
                    System.Windows.Threading.DispatcherPriority.ContextIdle
                );
            }
            else
            {
                RefreshHierarchy();
                RefreshGrid();
            }
            return true;
        }
        #endregion // IWeakEventListener

        #region IDisposable Implementation
        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            PropertyChangedEventManager.RemoveListener(Category, this, "Reports");
        }
        #endregion // IDisposable Implementation
    } // ReportCategoryViewModel
}
