﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Report;
using RSG.Model.Report.Reports;
using Workbench.AddIn.Services;

namespace Workbench.AddIn.MapStatistics.Reports
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Report.AddIn.ExtensionPoints.MapStatsReport, typeof(IReport))]
    public class SectionReport : RSG.Model.Report.Reports.Map.SectionReport
    {
    } // SectionReport
} // Workbench.AddIn.MapStatistics.Reports
