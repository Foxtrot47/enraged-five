﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Report;
using System.ComponentModel.Composition;
using RSG.Model.Report.Reports.GameStatsReports.Tests;
using RSG.Base.ConfigParser;
using RSG.SourceControl.Perforce;

namespace Workbench.AddIn.GameStatistics.Reports.Automated.Build
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(ExtensionPoints.AutomatedBuildReport, typeof(IReport))]
    public class MemoryGameStatReport : RSG.Model.Report.Reports.GameStatsReports.HtmlGameStatReport, IPartImportsSatisfiedNotification
    {
        #region Constants
        private const String NAME = "Memory Report";
        private const String DESC = "Generates a report that shows information regarding the memory changes on a per build basis.";
        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// MEF import for login service.
        /// </summary>
        [ImportExtension(PerforceBrowser.AddIn.CompositionPoints.PerforceService, typeof(PerforceBrowser.AddIn.IPerforceService))]
        public PerforceBrowser.AddIn.IPerforceService PerforceService { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(Workbench.AddIn.Services.IConfigurationService))]
        protected Workbench.AddIn.Services.IConfigurationService Config { get; set; }
        #endregion // MEF Imports
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public MemoryGameStatReport()
            : base(NAME, DESC, new MemorySmokeTest())
        {
        }
        #endregion // Constructor(s)
        
        #region IPartImportsSatisfiedNotification
        /// <summary>
        /// Called when a part's imports have been satisfied and it is safe to use.
        /// </summary>
        public void OnImportsSatisfied()
        {
            ReportsConfig = Config.ReportsConfig;
        }
        #endregion // IPartImportsSatisfiedNotification

        #region Methods
        protected override P4 GetPerforceConnection()
        {
            return PerforceService.PerforceConnection;
        }
        #endregion
    } // MemoryGameStatReport
}
