﻿using System;
using System.Linq;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows;
using PerforceBrowser.AddIn;
using RSG.Base.ConfigParser;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Tasks;
using RSG.Base.Windows.Dialogs;
using RSG.Model.Common.Map;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Export;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Automation.Common.Utils;
using RSG.Pipeline.Automation.Consumers;
using RSG.Pipeline.Core;
using RSG.SourceControl.Perforce;
using Workbench.AddIn;
using Workbench.AddIn.Automation;
using Workbench.AddIn.Commands;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.Services;
using LevelBrowser.AddIn.MapBrowser.Commands;
using RSG.Base.Configuration.Automation;

namespace Workbench.AddIn.Automation.Commands
{
    
    /// <summary>
    /// Map Network Export Command; fire-and-forget-for-a-while-map-exporting
    /// for environment artists.
    /// </summary>
    [ExportExtension(Extensions.MapSectionCommands, typeof(IWorkbenchCommand))]
    class MapNetworkExportCommand : 
        WorkbenchMenuItemBase,
        IPartImportsSatisfiedNotification
    {
        #region Constants
        public static readonly String GUID = "8A660A0E-C195-42BC-A0C0-E8F2D470AB0C";

        /// <summary>
        /// Automation-service Friendly-Name.
        /// </summary>
        private static readonly String SERVICE_NAME = "Map Network Export";

        private bool _initialised;
        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// Workbench messaging service import.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.MessageService, 
            typeof(IMessageService))]
        private IMessageService MessageService { get; set; }

        /// <summary>
        /// Configuration Service MEF import.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, 
            typeof(IConfigurationService))]
        private IConfigurationService ConfigService { get; set; }

        /// <summary>
        /// MEF import for perforce service.
        /// </summary>
        [ImportExtension(PerforceBrowser.AddIn.CompositionPoints.PerforceService,
            typeof(PerforceBrowser.AddIn.IPerforceService))]
        public PerforceBrowser.AddIn.IPerforceService PerforceService { get; set; }
        #endregion // MEF Imports

        #region Member Data
        /// <summary>
        /// Visible flag; from configuration data.
        /// </summary>
        private bool m_bVisible;

        /// <summary>
        /// Map Network Export consumer.
        /// </summary>
        private FileTransferServiceConsumer m_FileTransferConsumer;

        /// <summary>
        /// Automation services consumer.
        /// </summary>
        private AutomationServiceConsumer m_AutomationConsumer;

        /// <summary>
        /// Automation Admin Service Consumer.
        /// </summary>
        private AutomationAdminConsumer m_AutomationAdminConsumer;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public MapNetworkExportCommand()
        {
            this.Header = "Queue _Network Export...";
            this.IsDefault = false;
            this.ID = new Guid(GUID);
            this.Direction = Direction.After;
        }
        #endregion // Constructor(s)

        #region IPartImportsSatisfiedNotification Interface Methods
        /// <summary>
        /// MEF import composition complete callback.
        /// </summary>
        public void OnImportsSatisfied()
        {
        }

        private void Initialise()
        {
            this._initialised = true;
            IAutomationServiceParameters serviceParams = this.ConfigService.AutomationCoreServices.GetServiceParameters(SERVICE_NAME);
            if (serviceParams == null)
            {
                Log.Log__Error("Map Network Export command initialisation failed; no automation service '{0}'. Check Automation Configuration.",
                    SERVICE_NAME);
                return;
            }

            this.m_bVisible = serviceParams.WorkbenchVisible;
            this.m_FileTransferConsumer = new FileTransferServiceConsumer(this.ConfigService.Config, serviceParams.FileTransferService);
            this.m_AutomationConsumer = new AutomationServiceConsumer(serviceParams.AutomationService);
            this.m_AutomationAdminConsumer = new AutomationAdminConsumer(serviceParams.AutomationAdminService);
        }
        #endregion // IPartImportsSatisfiedNotification Interface Methods

        #region ICommand Implementation
        /// <summary>
        /// Gets called when deciding whether this menu items command can be 
        /// executed or not.
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            if (!_initialised)
            {
                this.Initialise();
            }

#warning DHM FIX ME: authenticate user against Bugstar role.
            return (m_bVisible);
        }

        /// <summary>
        /// Gets called when executing the command for this menu item.
        /// </summary>
        public override void Execute(Object parameter)
        {
            if (!_initialised)
            {
                this.Initialise();
            }

            // Get the list of map sections that we will be processing
            IEnumerable<IMapSection> mapSections;
            if (parameter is IList)
                mapSections = (parameter as IList).Cast<IMapSection>();
            else
                mapSections = new IMapSection[] { parameter as IMapSection };

            String message = String.Format("Are you sure want to queue Network Exports for {0} maps?{1}{1}NOTE: The Scene xml on your computer is used to get the textures to send for export.",
                                            mapSections.Count(), Environment.NewLine);
            MessageBoxResult result = this.MessageService.Show(message, 
                MessageBoxButton.YesNo, MessageBoxImage.Question);

            switch (result)
            {
                case MessageBoxResult.Yes:
                    {
                        // Determine DCC Source and Export Files.
                        IEnumerable<String> mapNames = GetMapNames(mapSections);

                        foreach (String mapName in mapNames)
                        {
                            // Changed to be one job per map section rather than multiple map sections in a job if multiple
                            // sections selected.  Left the constructors wanting an IEnumerable for now even though it's just
                            // taking 1 map name, just incase we need to revert in future.
                            MapExportJob job = this.CreateJob(mapName);

                            // Upload DCC Source Files.
                            if (!this.UploadDCCSourceFiles(job))
                            {
                                this.MessageService.Show("Export queue failed.  DCC Source Files failed to upload.",
                                    MessageBoxButton.OK, MessageBoxImage.Error);
                                return;
                            }

                            try
                            {
                                if (!this.UploadTextureSourceFiles(job))
                                {
                                    this.MessageService.Show("Export queue failed.  Texture Source Files failed to upload - check the universal log listener",
                                        MessageBoxButton.OK, MessageBoxImage.Error);                                                                     

                                    return;
                                }
                            }
                            catch (Exception ex)
                            {
                                Log.Log__Exception(ex, "Error uploading texture source files for job {0}", job.ID);
                            }

                            Debug.Assert(null == job.Trigger);
                            IEnumerable<String> files = job.ExportProcesses.Select(p =>
                                p.DCCSourceFilename);
                            job.Trigger = new UserRequestTrigger(files);

                            bool queueResult = this.m_AutomationConsumer.EnqueueJob(job);
                            if (queueResult)
                            {
                                this.MessageService.Show(
                                    String.Format("Export job {0} queued.  Use the Automation Monitor to monitor status.",
                                    job.ID));
                            }
                            else
                            {
                                this.MessageService.Show("Export queue failed!",
                                    MessageBoxButton.OK, MessageBoxImage.Error);
                            }
                        }
                    }
                    break;
                default:
                    this.MessageService.Show("Network Export request canceled.");
                    break;
            }
        }
        #endregion // ICommand Implementation

        #region Private Methods
        /// <summary>
        /// Return map names from a series of MapSections.
        /// </summary>
        /// <param name="maps"></param>
        /// <returns></returns>
        private IEnumerable<String> GetMapNames(IEnumerable<IMapSection> maps)
        {
            IEnumerable<String> mapNames = maps.Select(m => m.Name);
            return (mapNames);
        }

        /// <summary>
        /// UPload DCC Source Files to automation server.
        /// </summary>
        /// <param name="job"></param>
        /// <returns>true iff successful; false otherwise.</returns>
        private MapExportJob CreateJob(String mapName)
        {
            MapExportJob job = new MapExportJob();
            CancellationTokenSource cts = new CancellationTokenSource();
            ITaskContext context = new TaskContext(cts.Token);
            ITask findSourceTexturesTask = new ActionTask("Finding textures used in map section",
                (ctx, progress) => CreateJobAction(ref job, mapName));
            findSourceTexturesTask.ReportsProgress = false;

            TaskExecutionDialog dialog = new TaskExecutionDialog();
            TaskExecutionViewModel vm = new TaskExecutionViewModel("Finding textures used in map section",
                findSourceTexturesTask, cts, context);
            vm.AutoCloseOnCompletion = true;
            dialog.DataContext = vm;
            dialog.ShowDialog();

            return job;
        }

        /// <summary>
        /// Creates a MapExportJob.  Currently 1 map section per job
        /// </summary>
        /// <param name="?"></param>
        /// <param name="mapNames"></param>
        /// <returns></returns>
        private void CreateJobAction(ref MapExportJob job, String mapName)
        {
            // Create map export job to submit.
            IConfig config = ConfigFactory.CreateConfig();
            IContentTree tree = RSG.Pipeline.Content.Factory.CreateTree(config.Project.DefaultBranch);
            RSG.Pipeline.Content.ContentTreeHelper helper = new RSG.Pipeline.Content.ContentTreeHelper(tree);
            IEnumerable<IContentNode> mapExportNodes = helper.GetAllMapExportNodes();

            String dccFilename = String.Empty;
            foreach (RSG.Pipeline.Content.File node in mapExportNodes)
            {
                if (node.Basename.Equals(mapName, StringComparison.OrdinalIgnoreCase))
                {
                    dccFilename = node.AbsolutePath;
                    break;
                }
            }
            if (dccFilename == String.Empty)
            {
                Log.Log__Error("Couldn't find filename for map: {0}", mapName);
                return;
            }

            job = MapExportJobFactory.Create(config.Project.DefaultBranch, tree, PerforceService.PerforceConnection, dccFilename, true);
        }

        /// <summary>
        /// Upload DCC Source Files to automation server (max and scene xml)
        /// </summary>
        /// <param name="job"></param>
        /// <returns>true iff successful; false otherwise.</returns>
        private bool UploadDCCSourceFiles(IJob job)
        {
            CancellationTokenSource cts = new CancellationTokenSource();
            ITaskContext context = new TaskContext(cts.Token, job);
            ITask uploadDataTask = new ActionTask("Uploading DCC Source Files",
                (ctx, progress) => UploadDCCSourceFilesAction(ctx, progress));
            uploadDataTask.ReportsProgress = true;

            TaskExecutionDialog dialog = new TaskExecutionDialog();
            TaskExecutionViewModel vm = new TaskExecutionViewModel("Uploading DCC Source Files",
                uploadDataTask, cts, context);
            vm.AutoCloseOnCompletion = true;
            dialog.DataContext = vm;
            bool? result = dialog.ShowDialog();
            bool taskReturnCode = false;
            // If the upload is cancelled by user, context.ReturnValue is null
            if (context.ReturnValue != null)
            {
                taskReturnCode = (bool)context.ReturnValue;
            }

            return (result.HasValue ? result.Value && taskReturnCode : false);
        }

        /// <summary>
        /// Upload DCC Source Files background thread worker.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="progress"></param>
        private void UploadDCCSourceFilesAction(ITaskContext context, IProgress<TaskProgress> progress)
        {
            Debug.Assert(null != context.Argument,
                "No argument for asynchronous upload task.  Internal error.");
            if (null == context.Argument)
            {
                Log.Log__Error("No argument for asynchronous upload task.  Internal error.");
                return;
            }
            Debug.Assert(context.Argument is MapExportJob,
                "Invalid argument for asynchronous upload task.  Internal error.");
            if (!(context.Argument is MapExportJob))
            {
                Log.Log__Error("Invalid argument for asynchronous upload task.  Internal error.");
                return;
            }

            MapExportJob mapJob = (MapExportJob)context.Argument;
            List<String> dccSourceFiles = mapJob.ExportProcesses.Select(p => p.DCCSourceFilename).ToList();
            // Upload scene xml file so source texture usage can be sync'd by client worker
            IEnumerable<String> sceneXMLFiles = mapJob.ExportProcesses.SelectMany(p => p.ExportFiles.Where(dccFile => Path.GetExtension(dccFile).Contains("xml")));
            if (sceneXMLFiles != null)
                dccSourceFiles.AddRange(sceneXMLFiles);
            int dccSourceFileCount = dccSourceFiles.Count();
            int currentSourceFile = 0;
            bool result = true;

            // Upload each file in turn; reporting progress as we go.
            float percentage = 0.0f;
            foreach (String dccSourceFile in dccSourceFiles)
            {
                // Progress update.
                percentage = (1.0f / (float)dccSourceFileCount) * ++currentSourceFile;
                progress.Report(new TaskProgress(percentage,
                    String.Format("Uploading DCC Source File {0} ({1} of {2})...",
                        System.IO.Path.GetFileName(dccSourceFile),
                        currentSourceFile, dccSourceFileCount)));

                String dccSourceFilename;
                if (dccSourceFile.Contains(".xml"))
                {
                    // Scene xml file which we have full filename for
                    dccSourceFilename = dccSourceFile;                    
                }
                else
                {
                    dccSourceFilename = Path.ChangeExtension(dccSourceFile, ".maxc");
                    if (!File.Exists(dccSourceFilename))
                        dccSourceFilename = Path.ChangeExtension(dccSourceFile, ".max");                 
                }

                if (File.Exists(dccSourceFilename) == false)
                {
                    Log.Log__Error("Unable to find Max file for " + dccSourceFile + " locally.  Queuing for export failed.");
                    result = false;
                }
                else
                {
                    // Transfer file.
                    bool fileResult = this.m_FileTransferConsumer.UploadFile(mapJob, dccSourceFilename);
                    result &= fileResult;
                    if (!fileResult)
                    {
                        Log.Log__Error("Unable to upload Max file {0}.  Queuing for export failed.", dccSourceFilename);
                    }
                }
				
				 System.Threading.Thread.Sleep(1000);
            }
            context.ReturnValue = result;
        }
        
        /// <summary>
        /// UPload DCC Source Files to automation server.
        /// </summary>
        /// <param name="job"></param>
        /// <returns>true iff successful; false otherwise.</returns>
        private bool UploadTextureSourceFiles(IJob job)
        {
            CancellationTokenSource cts = new CancellationTokenSource();
            ITaskContext context = new TaskContext(cts.Token, job);
            ITask uploadDataTask = new ActionTask("Uploading Texture Source Files",
                (ctx, progress) => UploadTextureSourceFilesAction(ctx, progress));
            uploadDataTask.ReportsProgress = true;

            TaskExecutionDialog dialog = new TaskExecutionDialog();
            TaskExecutionViewModel vm = new TaskExecutionViewModel("Uploading Texture Source Files",
                uploadDataTask, cts, context);
            vm.AutoCloseOnCompletion = true;
            dialog.DataContext = vm;
            bool? result = dialog.ShowDialog();
            bool taskReturnCode = false;
            // If the upload is cancelled by user, context.ReturnValue is null
            if (context.ReturnValue != null)
            {
                taskReturnCode = (bool)context.ReturnValue;
            }

            return (result.HasValue ? result.Value && taskReturnCode : false);
        }

        /// <summary>
        /// Upload DCC Source Files background thread worker.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="progress"></param>
        private void UploadTextureSourceFilesAction(ITaskContext context, IProgress<TaskProgress> progress)
        {
            Debug.Assert(null != context.Argument,
                "No argument for asynchronous upload task.  Internal error.");
            if (null == context.Argument)
            {
                Log.Log__Error("No argument for asynchronous upload task.  Internal error.");
                return;
            }
            Debug.Assert(context.Argument is MapExportJob,
                "Invalid argument for asynchronous upload task.  Internal error.");
            if (!(context.Argument is MapExportJob))
            {
                Log.Log__Error("Invalid argument for asynchronous upload task.  Internal error.");
                return;
            }

            MapExportJob mapJob = (MapExportJob)context.Argument;
            IEnumerable<String> textureSourceFiles = mapJob.ExportProcesses.SelectMany(p => p.EditedSourceTextures);

            // No texture files to upload
            if (textureSourceFiles.Count() == 0)
            {
                context.ReturnValue = true;
                return;
            }

            List<String> sourceTexturesToSend = CommonFuncs.GetTexturesRequiredToSend(PerforceService.PerforceConnection, textureSourceFiles.ToList());
            int textureSourceFileCount = sourceTexturesToSend.Count();
            int currentTextureSourceFile = 0;
            bool result = true;
            // Upload each file in turn; reporting progress as we go.
            float percentage = 0.0f;
            foreach (String textureSourceFile in sourceTexturesToSend)
            {
                // Progress update.
                percentage = (1.0f / (float)textureSourceFileCount) * ++currentTextureSourceFile;
                progress.Report(new TaskProgress(percentage,
                    String.Format("Uploading Texture Source File {0} ({1} of {2})...",
                        textureSourceFile,
                        currentTextureSourceFile, textureSourceFileCount)));

                if (!File.Exists(textureSourceFile))
                {
                    Log.Log__Warning("Texture Source File {0} does not exist locally!", textureSourceFile);
                    continue;
                }

                // Transfer file.
                bool fileResult = this.m_FileTransferConsumer.UploadFile(mapJob, textureSourceFile);
                result &= fileResult;
                if (!fileResult)
                    Log.Log__Warning("Texture Source File upload of {0} failed.", textureSourceFile);

                System.Threading.Thread.Sleep(1000);
            }
            context.ReturnValue = result;
        }
        #endregion // Private Methods
    }

    /// <summary>
    /// 
    /// </summary>
    class UploadArgument
    {
        public IJob Job;

        public UploadArgument(IJob job)
        {
            this.Job = job;
        }
    }

} // Workbench.AddIn.Automation.Commands namespace
