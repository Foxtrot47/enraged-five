﻿using System;
using RSG.Model.Report;

namespace ContentBrowser.ViewModel.ReportViewModels
{

    /// <summary>
    /// 
    /// </summary>
    internal class ReportViewModel : AssetViewModelBase
    {
        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public ReportViewModel(IReport report)
            : base(report)
        {
        }
        #endregion // Constructor(s)
    }

} // ContentBrowser.ViewModel.ReportViewModels
