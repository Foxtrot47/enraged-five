﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Viewport.AddIn
{
    public class OverlayChangedEventArgs : EventArgs
    {
        public ViewportOverlay NewOverlay { get; private set; }
        public ViewportOverlay OldOverlay { get; private set; }

        public OverlayChangedEventArgs(ViewportOverlay oldOverlay, ViewportOverlay newverlay)
        {
            this.NewOverlay = newverlay;
            this.OldOverlay = oldOverlay;
        }
    }

    public delegate void OverlayChangedEventHandler(Object sender, OverlayChangedEventArgs args);
}
