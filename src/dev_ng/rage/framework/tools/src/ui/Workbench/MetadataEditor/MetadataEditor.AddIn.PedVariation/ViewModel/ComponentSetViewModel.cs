﻿namespace MetadataEditor.AddIn.PedVariation.ViewModel
{
    using System.Collections.Generic;
    using RSG.Base.Collections;
    using RSG.Base.Editor;
    using RSG.Metadata.Characters;

    /// <summary>
    /// 
    /// </summary>
    public class ComponentSetViewModel : ViewModelBase
    {
        #region Fields
        /// <summary>
        /// The private reference to the model data provider for this view model.
        /// </summary>
        private SelectionSet _selectionSet;

        /// <summary>
        /// The index for this particular component set inside the associated selection set.
        /// </summary>
        private int _index;

        /// <summary>
        /// A private dictionary used to map friendly names used for the drawable indices.
        /// </summary>
        private Dictionary<byte, string> _drawableMapping;

        /// <summary>
        /// The private field used for the <see cref="AvailableTextureNames"/> property.
        /// </summary>
        private SelectionSetCollectionViewModel _parent;

        /// <summary>
        /// A private dictionary used to map drawable indices to their random friendly name.
        /// </summary>
        internal static readonly Dictionary<int, string> _randomNames = InitialiseRandomNames();
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ComponentSetViewModel"/> class.
        /// </summary>
        /// <param name="selectionSet">
        /// The model that provides the data for this view model.
        /// </param>
        /// <param name="index">
        /// The index for this component set inside the associated selection set.
        /// </param>
        public ComponentSetViewModel(SelectionSet selectionSet, int index, SelectionSetCollectionViewModel parent)
        {
            this._selectionSet = selectionSet;
            this._index = index;
            this._parent = parent;

            this._drawableMapping = new Dictionary<byte, string>();
            this._drawableMapping.Add(255, _randomNames[index]);
            foreach (KeyValuePair<int, string> drawable in parent.Components[index])
            {
                this._drawableMapping.Add((byte)drawable.Key, drawable.Value);
            }
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the drawable name the is associated with the index used for this
        /// component set.
        /// </summary>
        public string SelectedDrawableName
        {
            get
            {
                byte drawbleIndex = this._selectionSet.GetDrawableId(this._index);
                return this._drawableMapping[drawbleIndex];
            }

            set
            {
                foreach (KeyValuePair<byte, string> kvp in this._drawableMapping)
                {
                    if (kvp.Value == value)
                    {
                        this._selectionSet.SetDrawableId(this._index, kvp.Key);
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the texture name the is associated with the index used for this
        /// component set.
        /// </summary>
        public string SelectedTextureName
        {
            get
            {
                byte textureIndex = this._selectionSet.GetTextureId(this._index);
                if (textureIndex == 255)
                {
                    return "Random";
                }

                int asciiValue = 97 + textureIndex;
                return ((char)asciiValue).ToString();
            }

            set
            {
                if (value == "Random" || value == null)
                {
                    this._selectionSet.SetTextureId(this._index, 255);
                }
                else
                {
                    int asciiValue = (int)value[0];
                    this._selectionSet.SetTextureId(this._index,(byte)(asciiValue - 97));
                }
            }
        }

        /// <summary>
        /// Gets a iterator around the available names of the drawable indices.
        /// </summary>
        public IEnumerable<string> AvailableDrawableNames
        {
            get { return this._drawableMapping.Values; }
        }

        /// <summary>
        /// Gets a iterator around the available names of the texture indices.
        /// </summary>
        public IEnumerable<string> AvailableTextureNames
        {
            get { return this._parent.ComponentTextures(this._index); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Initialise the static dictionary containing the mapping between drawable indices
        /// and their friendly random names.
        /// </summary>
        /// <returns>
        /// A dictionary containing the mapping between drawable indices and their friendly
        /// random names.
        /// </returns>
        private static Dictionary<int, string> InitialiseRandomNames()
        {
            Dictionary<int, string> randomNames = new Dictionary<int, string>();

            randomNames.Add(0, "Head - Random");
            randomNames.Add(1, "Berd - Random");
            randomNames.Add(2, "Hair - Random");
            randomNames.Add(3, "Uppr - Random");
            randomNames.Add(4, "Lowr - Random");
            randomNames.Add(5, "Hand - Random");
            randomNames.Add(6, "Feet - Random");
            randomNames.Add(7, "Teef - Random");
            randomNames.Add(8, "Accs - Random");
            randomNames.Add(9, "Task - Random");
            randomNames.Add(10, "Decl - Random");
            randomNames.Add(11, "Jbib - Random");

            return randomNames;
        }
        #endregion Methods
    } // MetadataEditor.AddIn.PedVariation.ViewModel.ComponentSetViewModel {Class}
} // MetadataEditor.AddIn.PedVariation.ViewModel {Namespace}
