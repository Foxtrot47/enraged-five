﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using RSG.Base.Logging;
using Workbench.AddIn;
using Workbench.AddIn.Services;
using Workbench.AddIn;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.UI.Menu;

namespace Workbench.AddIn.TXDParentiser.UI.Menu
{
    // Comment out since it now uses the services to create the TXD Parentiser document
    /// <summary>
    /// 
    /// </summary>
    //[ExportExtension(Workbench.AddIn.ExtensionPoints.MenuTools, 
    //    typeof(IMenuItem))]
    //class TXDParentiserToolsMenu : MenuItemBase
    //{
    //    #region MEF Imports

    //    [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager, typeof(ILayoutManager))]
    //    private ILayoutManager LayoutManager { get; set; }

    //    [ImportExtension(Workbench.AddIn.TXDParentiser.CompositionPoints.TXDParentiserDocument, typeof(TXDParentiserViewModel))]
    //    private TXDParentiserViewModel TXDParentiserViewer { get; set; }

    //    #endregion // MEF Imports

    //    #region Constructor(s)

    //    /// <summary>
    //    /// Default constructor.
    //    /// </summary>
    //    public TXDParentiserToolsMenu() : base("_TXD Parentiser")
    //    {
    //    }

    //    #endregion // Constructor(s)

    //    #region ICommandControl Interface

    //    /// <summary>
    //    /// This is called whenever the user selects the TXD Parentiser menu option.
    //    /// Just creates the main document and uses the layout manager to display it.
    //    /// </summary>
    //    /// <param name="parameter"></param>
    //    public override void Execute(Object parameter)
    //    {
    //        Log.Log__Debug("TXDParentiser::Execute()");

    //        IDocument TXDParentiserDoc = TXDParentiserViewer.CreateDocument("TXD Parentiser");
    //        LayoutManager.ShowDocument(TXDParentiserDoc);
    //    }
    //    #endregion // ICommandControl Interface

    //} // TXDParentiserToolsMenu

} // Workbench.AddIn.TXDParentiser.UI.Menu
