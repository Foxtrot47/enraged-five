﻿using System;
using System.Collections.Generic;
using PerforceBrowser.AddIn;
using RSG.Base.Editor;
using WidgetEditor.AddIn;
using Workbench.AddIn.Services.File;
using Workbench.AddIn.Services.Model;
using Workbench.AddIn.UI.Layout;

namespace Workbench.AddIn.Services
{

    /// <summary>
    /// Core Workbench Service Provider interface.
    /// </summary>
    /// This core service interface allows other extensions and services to 
    /// import all of the Workbench core services easily; greatly simplfying
    /// MEF composition.
    /// 
    /// This also pulls in the tools-wide services.
    /// 
    /// To add:
    ///     Core Tools Services (needs abstract into assembly for non-Workbench and Workbench apps).
    ///     Search Service
    /// 
    public interface ICoreServiceProvider
    {
#warning DHM TODO : this could be abstract into a separate assembly as many apps will use it.
        #region Tool Core Services
        /// <summary>
        /// Bugstar Service.
        /// </summary>
        IBugstarService BugstarService { get; }

        /// <summary>
        /// Perforce Service.
        /// </summary>
        IPerforceService PerforceService { get; }

        /// <summary>
        /// Game-RAG Proxy Service.
        /// </summary>
        IProxyService ProxyService { get; }
        #endregion // Tool Core Services

        #region Workbench Core Services
        /// <summary>
        /// Configuration Service.
        /// </summary>
        IConfigurationService ConfigurationService { get; }

        /// <summary>
        /// Layout Manager.
        /// </summary>
        ILayoutManager LayoutManager { get; }

        /// <summary>
        /// Workbench level open service.
        /// </summary>
        IWorkbenchOpenService OpenService { get; }

        /// <summary>
        /// User Interface Service.
        /// </summary>
        IUserInterfaceService UserInterfaceService { get; }

        /// <summary>
        /// Task progress service.
        /// </summary>
        ITaskProgressService TaskProgressService { get; }

        /// <summary>
        /// Task percentage complete. Value between 0 and 100 inclusive.
        /// </summary>
#warning DHM: WTF?  Should be part of ITaskProgressService.
        int TaskPercentageComplete { get; set; }

        /// <summary>
        /// Task message.
        /// </summary>
#warning DHM: WTF?  Should be part of ITaskProgressService.
        String TaskMessage { get; set; }
        #endregion // Workbench Core Services
    }

} // Workbench.AddIn.Services
