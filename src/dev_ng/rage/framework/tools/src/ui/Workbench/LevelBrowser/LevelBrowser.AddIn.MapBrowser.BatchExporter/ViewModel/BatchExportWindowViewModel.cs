﻿using System;
using System.Collections.Generic;
using System.Linq;
using RSG.Base.Editor;
using Workbench.AddIn;
using RSG.Model.Map3;
using Workbench.AddIn.Commands;
using LevelBrowser.AddIn.MapBrowser.BatchExporter.Model;
using System.ComponentModel;
using System.Threading;
using RSG.Base.Collections;
using RSG.Base.Windows.Helpers;
using System.Windows.Input;
using System.Windows.Forms;
using LevelBrowser.AddIn.MapBrowser.Commands;
using Workbench.AddIn.Services.Model;
using System.ComponentModel.Composition;

namespace LevelBrowser.AddIn.MapBrowser.BatchExporter.ViewModel
{
    /// <summary>
    /// View model for the batch exporter window.
    /// </summary>
    [ExportExtension("LevelBrowser.AddIn.MapBrowser.BatchExporter.ViewModel.BatchExportWindowViewModel", typeof(BatchExportWindowViewModel))]
    public class BatchExportWindowViewModel : ViewModelBase, IPartImportsSatisfiedNotification
    {
        #region Constants

        private readonly string FILE_FILTER = "Batch Export List (*.xml)|*.xml|All Files (*.*)|*.*";

        #endregion

        #region Private member fields

        private ObservableCollection<SectionExportItemViewModel> m_exportItems;

        private ICommand m_save;
        private ICommand m_load;
        private ICommand m_export;
        private ICommand m_clear;
        private ICommand m_removeSelection;

        private BackgroundWorker m_worker;
        private WaitHandle m_waitHandle;
        
        private bool m_toggleSelection;
        private string m_statusBarText;

        #endregion

        #region Public properties

        /// <summary>
        /// Level browser proxy.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LevelBrowser, typeof(ILevelBrowser))]
        public ILevelBrowser LevelBrowserProxy
        {
            get;
            set;
        }

        /// <summary>
        /// The model that represents the collection of MapSection instances.
        /// </summary>
        [ImportExtension(ExtensionPoints.ExportSettingsModel, typeof(IBatchExportCollection))]
        public IBatchExportCollection Model
        {
            get;
            set;
        }

        /// <summary>
        /// The save command.
        /// </summary>
        public ICommand SaveCommand
        {
            get { return m_save; }
            set
            {
                SetPropertyValue(value, m_save, () => this.SaveCommand,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(object newValue)
                        {
                            m_save = (ICommand)newValue;
                        }));
            }
        }

        /// <summary>
        /// The load command.
        /// </summary>
        public ICommand LoadCommand
        {
            get { return m_load; }
            set
            {
                SetPropertyValue(value, m_load, () => this.LoadCommand,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(object newValue)
                        {
                            m_load = (ICommand)newValue;
                        }));
            }
        }

        /// <summary>
        /// The clear list command.
        /// </summary>
        public ICommand ClearCommand
        {
            get { return m_clear; }
            set
            {
                SetPropertyValue(value, m_clear, () => this.ClearCommand,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(object newValue)
                        {
                            m_clear = (ICommand)newValue;
                        }));
            }
        }

        /// <summary>
        /// The export command.
        /// </summary>
        public ICommand ExportCommand
        {
            get { return m_export; }
            set
            {
                SetPropertyValue(value, m_export, () => this.ExportCommand,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(object newValue)
                        {
                            m_export = (ICommand)newValue;
                        }));
            }
        }

        /// <summary>
        /// The remove selection command.
        /// </summary>
                public ICommand RemoveSelectionCommand
        {
            get { return m_removeSelection; }
            set
            {
                SetPropertyValue(value, m_removeSelection, () => this.RemoveSelectionCommand,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(object newValue)
                        {
                            m_removeSelection = (ICommand)newValue;
                        }));
            }
        }

        /// <summary>
        /// The collection of view models that constitute the visual representation 
        /// of the list of MapSection instances to export.
        /// </summary>
        public ObservableCollection<SectionExportItemViewModel> ExportItems
        {
            get { return m_exportItems; }
            set
            {
                SetPropertyValue(value, m_exportItems, () => this.ExportItems,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(object newValue)
                        {
                            m_exportItems = (ObservableCollection<SectionExportItemViewModel>)newValue;
                        }));
            }
        }

        /// <summary>
        /// Toggle the section of all items.
        /// </summary>
        public bool SelectAll
        {
            get { return m_toggleSelection; }
            set
            {
                SetPropertyValue(value, m_toggleSelection, () => this.SelectAll,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(object newValue)
                        {
                            m_toggleSelection = (bool)newValue;
                            UpdateSelection(m_toggleSelection);
                            OnPropertyChanged("SelectAll");
                        }));
            }
        }

        /// <summary>
        /// Status bar text.
        /// </summary>
        public string StatusBarText
        {
            get { return m_statusBarText; }
            set
            {
                SetPropertyValue(value, m_statusBarText, () => this.StatusBarText,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(object newValue)
                        {
                            m_statusBarText = newValue.ToString();
                        }));
            }
        }


        /// <summary>
        /// Returns true if at least one item in the list of MapSections has been selected.
        /// </summary>
        public bool HasSelection
        {
            get { return ExportItems.Where(item => item.IsChecked).Any(); }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        public BatchExportWindowViewModel()
        {
            m_exportItems = new ObservableCollection<SectionExportItemViewModel>();
            SelectAll = true;
            ExportItems.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(ExportItems_CollectionChanged);
            StatusBarText = "0 items";
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Replace the current model with new data.
        /// </summary>
        /// <param name="sectionList">New model data.</param>
        public void ReplaceModel(string filename)
        {
            ExportItems.BeginUpdate();

            ExportItems.Clear();
            Model.RemoveAll();

            if (!String.IsNullOrEmpty(filename))
            {
                Model.Deserialize(filename, LevelBrowserProxy.SelectedLevel.MapHierarchy);

                foreach (var mapSection in Model.GetAll())
                {
                    AddSection(mapSection.Key, mapSection.Value);
                }
            }

            ExportItems.EndUpdate();
        }

        /// <summary>
        /// Add MapSection instances to the list of items to be exported.
        /// </summary>
        /// <param name="sections">MapSection instances.</param>
        public void AddSections(IEnumerable<MapSection> sections)
        {
            ExportItems.BeginUpdate();

            foreach (MapSection section in sections)
            {
                AddSection(section.Name, section);
            }

            ExportItems.EndUpdate();
        }

        /// <summary>
        /// Remove a list of sections from the list.
        /// </summary>
        /// <param name="sections">Sections.</param>
        public void RemoveSections(IEnumerable<SectionExportItemViewModel> sections)
        {
            ExportItems.BeginUpdate();

            foreach (var viewModel in sections)
            {
                ExportItems.Remove(viewModel);
                Model.Remove(viewModel.MapSection);
            }

            ExportItems.EndUpdate();
        }

        #endregion

        #region Background worker methods

        /// <summary>
        /// Initialise the background worker task.
        /// </summary>
        private void InitWorker()
        {
            m_worker = new BackgroundWorker();
            m_worker.WorkerReportsProgress = false;
            m_worker.DoWork += new DoWorkEventHandler(Worker_DoWork);
            m_worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(Worker_RunWorkerCompleted);
            m_worker.RunWorkerAsync(this);
        }

        /// <summary>
        /// Event handler for background worker completed.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void Worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (LevelBrowserProxy.SelectedLevel != null)
            {
                Model.PopulateMapSections(LevelBrowserProxy.SelectedLevel.MapHierarchy);
                foreach (var mapSection in Model.GetAll())
                {
                    SectionExportItemViewModel viewModel = ExportItems.Where(item => item.SectionName.Equals(mapSection.Key)).FirstOrDefault();
                    if (viewModel != null)
                    {
                        viewModel.MapSection = mapSection.Value;
                    }
                }

                System.Windows.Input.CommandManager.InvalidateRequerySuggested();
            }
        }

        /// <summary>
        /// Wait until the LevelBrowserProxy's map hierarchy becomes available. Does not affect UI interaction.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            bool hasLevel = false;
            var viewModel = e.Argument as BatchExportWindowViewModel;
            while (!hasLevel)
            {
                Thread.Sleep(1000);
                hasLevel = (viewModel.LevelBrowserProxy != null && viewModel.LevelBrowserProxy != null && viewModel.LevelBrowserProxy.SelectedLevel != null && viewModel.LevelBrowserProxy.SelectedLevel.MapHierarchy != null);
            }
        }

        #endregion

        #region Command methods

        /// <summary>
        /// Initialise the commands.
        /// </summary>
        private void InitCommands()
        {
            SaveCommand = new RelayCommand(Save, CanExecute);
            ClearCommand = new RelayCommand(ClearList, CanExecute);
            LoadCommand = new RelayCommand(Load);
            ExportCommand = new RelayCommand(ExportList, CanExport);
            RemoveSelectionCommand = new RelayCommand(RemoveSelected, CanRemoveItems);
        }

        /// <summary>
        /// Clear the list.
        /// </summary>
        /// <param name="parameter"></param>
        private void ClearList(object parameter)
        {
            var viewModel = parameter as BatchExportWindowViewModel;
            if (viewModel != null)
            {
                DialogResult result = MessageBox.Show("Are you sure you want to clear the list?", "Clear List", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    viewModel.ReplaceModel(null);
                }
            }
        }

        /// <summary>
        /// Load the export list from disk.
        /// </summary>
        /// <param name="parameter">Parameter.</param>
        private void Load(object parameter)
        {
            var viewModel = parameter as BatchExportWindowViewModel;
            if (viewModel == null)
            {
                return;
            }

            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = FILE_FILTER;
            ofd.Title = "Open Export List";

            DialogResult result = ofd.ShowDialog();

            if (result == DialogResult.OK)
            {
                viewModel.ReplaceModel(ofd.FileName);
            }
        }

        /// <summary>
        /// Remove the selection from the list.
        /// </summary>
        /// <param name="parameter">Parameter.</param>
        private void RemoveSelected(object parameter)
        {
            var viewModel = parameter as BatchExportWindowViewModel;
            IEnumerable<SectionExportItemViewModel> matches = viewModel.ExportItems.Where(item => item.IsChecked);
            List<SectionExportItemViewModel> viewModels = new List<SectionExportItemViewModel>(matches);
            viewModel.RemoveSections(viewModels);
        }

        /// <summary>
        /// Save the list of map sections.
        /// </summary>
        /// <param name="parameter">Parameter.</param>
        private void Save(object parameter)
        {
            if (!(parameter is BatchExportWindowViewModel))
            {
                return;
            }

            var viewModel = parameter as BatchExportWindowViewModel;

            SaveFileDialog ofd = new SaveFileDialog();
            ofd.Filter = FILE_FILTER;
            ofd.Title = "Save Export List";
            DialogResult result = ofd.ShowDialog();
            if (result == DialogResult.OK)
            {
                viewModel.Model.Serialize(ofd.FileName);
            }
        }

        private void ExportList(object parameter)
        {
            var viewModel = parameter as BatchExportWindowViewModel;
            if (viewModel != null)
            {
                ExportMapCommand cmd = new ExportMapCommand();
                List<MapSection> sections = new List<MapSection>();
                sections.AddRange(viewModel.Model.GetSections());
                cmd.Execute(sections);
            }
        }

        /// <summary>
        /// Returns true if the command can execute with the given parameter.
        /// </summary>
        /// <param name="parameter">Parameter.</param>
        /// <returns>True if the command can execute with the given parameter.</returns>
        private bool CanExecute(object parameter)
        {
            bool isOK = parameter != null && parameter is BatchExportWindowViewModel;
            if (isOK)
            {
                var viewModel = parameter as BatchExportWindowViewModel;
                isOK = viewModel.Model.HasItems;
            }

            return isOK;
        }

        /// <summary>
        /// Returns true if there is at least one item in the export list.
        /// </summary>
        /// <param name="parameter">Parameter.</param>
        /// <returns>True if there is at least one item in the export list.</returns>
        private bool CanExport(object parameter)
        {
            bool baseCanExecute = CanExecute(parameter);
            var viewModel = parameter as BatchExportWindowViewModel;

            if (!baseCanExecute)
            {
                return false;
            }

            bool hierarchyValid = (viewModel.LevelBrowserProxy != null && viewModel.LevelBrowserProxy != null && viewModel.LevelBrowserProxy.SelectedLevel.MapHierarchy != null);
            bool isComplete = true;
            foreach (var item in viewModel.Model.GetAll())
            {
                if (item.Value == null)
                {
                    isComplete = false;
                    break;
                }
            }

            return hierarchyValid && isComplete;
        }

        /// <summary>
        /// Returns true if there is a list and it has at least one item selected.
        /// </summary>
        /// <param name="parameter">Parameter.</param>
        /// <returns>True if the export list contains at least one selected item.</returns>
        private bool CanRemoveItems(object parameter)
        {
            var viewModel = parameter as BatchExportWindowViewModel;
            return CanExecute(parameter) && viewModel.HasSelection;
        }

        #endregion

        #region Event handlers

        /// <summary>
        /// Update the status bar text to show the number of items in the list.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void ExportItems_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            string format = ExportItems.Count == 1 ? "{0} item" : "{0} items";
            StatusBarText = String.Format(format, ExportItems.Count);
        }

        #endregion

        #region Private helper methods

        /// <summary>
        /// Adds a MapSection to the collection.
        /// </summary>
        /// <param name="sectionName">The name of the map section.</param>
        /// <param name="section">MapSection instance to add.</param>
        private void AddSection(string sectionName, MapSection section)
        {
            if (!ExportItems.Where(se => se.SectionName.Equals(sectionName)).Any())
            {
                SectionExportItemViewModel vm = new SectionExportItemViewModel(sectionName, section);
                ExportItems.Add(vm);
                Model.Add(sectionName, section);
            }
        }

        /// <summary>
        /// Toggle the selection.
        /// </summary>
        /// <param name="selection">Checked value of the items.</param>
        private void UpdateSelection(bool selection)
        {
            foreach (var vm in m_exportItems)
            {
                vm.IsChecked = selection;
            }
        }

        #endregion

        public void OnImportsSatisfied()
        {
            InitCommands();
            InitWorker();
        }
    }
}