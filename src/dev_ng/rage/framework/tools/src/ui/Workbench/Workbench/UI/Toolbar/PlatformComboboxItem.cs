﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn.Commands;
using RSG.Model.Common;
using RSG.Platform;

namespace Workbench.UI.Toolbar
{
    /// <summary>
    /// 
    /// </summary>
    public class PlatformComboboxItem : WorkbenchCommandLabel
    {
        #region Constants
        /// <summary>
        /// 
        /// </summary>
        public static readonly Guid GUID = new Guid("0B1BDB02-54F7-4BA7-A499-C0E6B9CE80AE");
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Platform this label is for
        /// </summary>
        public RSG.Platform.Platform Platform { get; protected set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="platform"></param>
        public PlatformComboboxItem(RSG.Platform.Platform platform)
            : base()
        {
            this.ID = GUID;
            this.Header = platform.PlatformToFriendlyName();
            Platform = platform;
        }
        #endregion // Constructor(s)
    } // ContentBrowserToolbarPlatformLabel
} // ContentBrowser.UI.Toolbar
