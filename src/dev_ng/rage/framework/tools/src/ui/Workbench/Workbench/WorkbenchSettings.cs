﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using Workbench.AddIn.Services;
using Workbench.AddIn;

namespace Workbench
{
    /// <summary>
    /// Settings for Metadata Definitions
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.Settings, typeof(ISettings))]
    public class WorkbenchSettings : Workbench.AddIn.Services.SettingsBase, ISettings
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        [Description("Only allow a single instance of the Workbench.")]
        public bool SingleInstance
        {
            get { return Properties.Settings.Default.SingleInstance; }
            set { Properties.Settings.Default.SingleInstance = value; }
        }

        /// <summary>
        /// Minimum IE version
        /// </summary>
        [Description("Minimum supported version of Internet Explorer")]
        public int MinimumIEVersion
        {
            get { return Properties.Settings.Default.IEMinimumVersion; }
            set { Properties.Settings.Default.IEMinimumVersion = value; }
        }

        /// <summary>
        /// The version of Internet Explorer the web browser control will use.
        /// </summary>
        [Description("The version of Internet Explorer the web browser control will use.")]
        public string WebBrowserVersion
        {
            get { return Properties.Settings.Default.IEWebBrowser; }
            set { Properties.Settings.Default.IEWebBrowser = value; }
        }

        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public WorkbenchSettings()
        {
            this.Title = "Core";
        }
        #endregion // Constructor(s)

        #region Methods
        /// <summary>
        /// Checks its current state for any invalid data.
        /// </summary>
        /// <param name="errors">The service will fill this up with one string per 
        /// error it finds in its data.</param>
        /// <returns>true if everything is fine, false if an error was found</returns>
        public Boolean Validate(List<String> errors)
        {
            return true;
        }

        /// <summary>
        /// Loads settings
        /// </summary>
        public void Load()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public void Apply()
        {
            Properties.Settings.Default.Save();
        }
        #endregion // Methods
    } 

} // Workbench
