﻿using MetadataEditor.AddIn.MetaEditor.ViewModel;
using RSG.Metadata.Data;

namespace MetadataEditor.AddIn.MetaEditor.GridViewModel
{
    /// <summary>
    /// The view model that wraps around a 
    /// <see cref="IntegerTunable"/> object. Used
    /// for the grid view.
    /// </summary>
    public class IntegerTunableViewModel : GridTunableViewModel
    {
        #region Constructor
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="MetadataEditor.AddIn.MetaEditor.GridViewModel.IntegerTunableViewModel"/>
        /// class.
        /// </summary>
        public IntegerTunableViewModel(GridTunableViewModel parent, IntegerTunable m, MetaFileViewModel root)
            : base(parent, m, root)
        {
        }
        #endregion // Constructor
    } // IntegerTunableViewModel
} // MetadataEditor.AddIn.MetaEditor.GridViewModel
