﻿using RSG.Model.Common.Map;

namespace Workbench.AddIn.MapStatistics.Overlays
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(ExtensionPoints.DensityStatisticsOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class DrawableInstanceDensityOverlay : StatisticOverlayBase
    {
        #region Constants
        private const string c_name = "Drawable Entity Density";
        private const string c_description = "Shows the density of all the drawable entities in the map container by the map containers area.  This statistic answers the question “How many drawable entities per meter squared are there in the map container”.";
        #endregion // Constants

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public DrawableInstanceDensityOverlay()
            : base(c_name, c_description)
        {
            m_includeNonDynamic = true;
            m_includeDynamic = true;
            m_includeHighDetailed = true;
            m_includeLod = true;
            m_includeSuperLod = true;
            m_includeInterior = false;
            m_dontShowInteriorOptions = true;
            m_dontShowUniqueOptions = true;
            m_dontShowEntityTypeOption = true;
        }
        #endregion // Constructor

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        protected uint GetStatisticValue(IMapSection section)
        {
            if (!this.IncludeDynamic && !this.IncludeNonDynamic)
                return 0;

            StatGroup group = StatGroup.DrawableEntities;
            StatEntityType type = StatEntityType.Total;
            if (this.IncludeDynamic && !this.IncludeNonDynamic)
            {
                type = StatEntityType.Dynamic;
            }
            else if (!this.IncludeDynamic && this.IncludeNonDynamic)
            {
                type = StatEntityType.NonDynamic;
            }

            // Gather the information
            uint stat = 0;

            if (this.IncludeHighDetailed)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Hd, type)];
                stat += stats.TotalCount;
            }
            if (this.IncludeLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Lod, type)];
                stat += stats.TotalCount;
            }
            if (this.IncludeSuperLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.SLod1, type)];
                stat += stats.TotalCount;
            }

            return stat;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="propgroup"></param>
        /// <returns></returns>
        protected override double GetStatisticValue(IMapSection section, IMapSection propgroup)
        {
            if (section.Area == 0.0f)
                return 0.0;

            uint objectCount = 0;
            if (section != null)
                objectCount += GetStatisticValue(section);
            if (propgroup != null)
                objectCount += GetStatisticValue(propgroup);

            return objectCount / section.Area;
        }
        #endregion // Overrides
    } // DrawableInstanceDensityOverlay

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(ExtensionPoints.DensityStatisticsOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class NonDrawableInstanceDensityOverlay : StatisticOverlayBase
    {
        #region Constants
        private const string c_name = "Non-Drawable Entity Density";
        private const string c_description = "Shows the density of all the non-drawable entities in the map container by the map containers area.  This statistic answers the question “How many non drawable entities per meter squared are there in the map container”.";
        #endregion // Constants

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public NonDrawableInstanceDensityOverlay()
            : base(c_name, c_description)
        {
            m_includeNonDynamic = true;
            m_includeDynamic = true;
            m_includeHighDetailed = true;
            m_includeLod = true;
            m_includeSuperLod = true;
            m_includeInterior = false;
            m_dontShowInteriorOptions = true;
            m_dontShowUniqueOptions = true;
            m_dontShowEntityTypeOption = true;
        }
        #endregion // Constructor

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        protected uint GetStatisticValue(IMapSection section)
        {
            if (!this.IncludeDynamic && !this.IncludeNonDynamic)
                return 0;

            StatGroup group = StatGroup.NonDrawableEntities;
            StatEntityType type = StatEntityType.Total;
            if (this.IncludeDynamic && !this.IncludeNonDynamic)
            {
                type = StatEntityType.Dynamic;
            }
            else if (!this.IncludeDynamic && this.IncludeNonDynamic)
            {
                type = StatEntityType.NonDynamic;
            }

            // Gather the information
            uint stat = 0;

            if (this.IncludeHighDetailed)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Hd, type)];
                stat += stats.TotalCount;
            }
            if (this.IncludeLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Lod, type)];
                stat += stats.TotalCount;
            }
            if (this.IncludeSuperLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.SLod1, type)];
                stat += stats.TotalCount;
            }

            return stat;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="propgroup"></param>
        /// <returns></returns>
        protected override double GetStatisticValue(IMapSection section, IMapSection propgroup)
        {
            if (section.Area == 0.0f)
                return 0.0;

            uint objectCount = 0;
            if (section != null)
                objectCount += GetStatisticValue(section);
            if (propgroup != null)
                objectCount += GetStatisticValue(propgroup);

            return objectCount / section.Area;
        }
        #endregion // Overrides
    } // NonDrawableInstanceDensityOverlay

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(ExtensionPoints.DensityStatisticsOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class TXDInstanceDensityOverlay : StatisticOverlayBase
    {
        #region Constants
        private const string c_name = "Entity TXD Density";
        private const string c_description = "Shows the density of all the unique txds in the map container by the map containers area.  This statistic answers the question “How many non unique txds need loaded per meter squared are there in the map container”.";
        #endregion // Constants

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public TXDInstanceDensityOverlay()
            : base(c_name, c_description)
        {
            m_includeHighDetailed = true;
            m_includeLod = true;
            m_includeSuperLod = true;
            m_includeDynamic = true;
            m_includeNonDynamic = true;
            m_includeInterior = false;

            m_dontShowObjectOptions = false;
            m_dontShowReferenceOptions = false;
            m_dontShowInteriorOptions = true;
            m_dontShowUniqueOptions = true;
        }
        #endregion // Constructor

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        protected uint GetStatisticValue(IMapSection section)
        {
            if (!this.IncludeDynamic && !this.IncludeNonDynamic)
                return 0;
            if (!this.IncludeDrawableEntities && !this.IncludeNonDrawableEntities)
                return 0;

            StatEntityType type = StatEntityType.Total;
            if (this.IncludeDynamic && !this.IncludeNonDynamic)
            {
                type = StatEntityType.Dynamic;
            }
            else if (!this.IncludeDynamic && this.IncludeNonDynamic)
            {
                type = StatEntityType.NonDynamic;
            }

            uint stat = 0;
            if (this.IncludeDrawableEntities && !this.IncludeNonDrawableEntities)
            {
                stat += AddStatistic(section, StatGroup.DrawableEntities, type);
            }
            else if (!this.IncludeDrawableEntities && this.IncludeNonDrawableEntities)
            {
                stat += AddStatistic(section, StatGroup.NonDrawableEntities, type);
            }
            else
            {
                stat += AddStatistic(section, StatGroup.AllNonInteriorEntities, type);
            }
            return stat;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stat"></param>
        /// <param name="stats"></param>
        /// <param name="group"></param>
        /// <param name="type"></param>
        private uint AddStatistic(IMapSection section, StatGroup group, StatEntityType type)
        {
            uint stat = 0;

            if (this.IncludeHighDetailed)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Hd, type)];
                stat += stats.TXDCount;
            }
            if (this.IncludeLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Lod, type)];
                stat += stats.TXDCount;
            }
            if (this.IncludeSuperLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.SLod1, type)];
                stat += stats.TXDCount;
            }

            return stat;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="propgroup"></param>
        /// <returns></returns>
        protected override double GetStatisticValue(IMapSection section, IMapSection propgroup)
        {
            if (section.Area == 0.0f)
                return 0.0;

            uint txdCount = 0;
            if (section != null)
                txdCount += GetStatisticValue(section);
            if (propgroup != null)
                txdCount += GetStatisticValue(propgroup);

            return txdCount / section.Area;
        }
        #endregion // Overrides
    } // TXDInstanceDensityOverlay

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(ExtensionPoints.DensityStatisticsOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class ShaderInstanceDensityOverlay : StatisticOverlayBase
    {
        #region Constants
        private const string c_name = "Entity Shader Density";
        private const string c_description = "Shows the density of all the unique textures in the map container by the map containers area. This statistic answers the question “How many non unique shaders need loaded per meter squared are there in the map container”.";
        #endregion // Constants

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public ShaderInstanceDensityOverlay()
            : base(c_name, c_description)
        {
            m_includeHighDetailed = true;
            m_includeLod = true;
            m_includeSuperLod = true;
            m_includeDynamic = true;
            m_includeNonDynamic = true;
            m_includeInterior = false;

            m_dontShowObjectOptions = false;
            m_dontShowReferenceOptions = false;
            m_dontShowInteriorOptions = true;
            m_dontShowUniqueOptions = true;
        }
        #endregion // Constructor

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        protected uint GetStatisticValue(IMapSection section)
        {
            if (!this.IncludeDynamic && !this.IncludeNonDynamic)
                return 0;
            if (!this.IncludeDrawableEntities && !this.IncludeNonDrawableEntities)
                return 0;

            StatEntityType type = StatEntityType.Total;
            if (this.IncludeDynamic && !this.IncludeNonDynamic)
            {
                type = StatEntityType.Dynamic;
            }
            else if (!this.IncludeDynamic && this.IncludeNonDynamic)
            {
                type = StatEntityType.NonDynamic;
            }

            uint stat = 0;
            if (this.IncludeDrawableEntities && !this.IncludeNonDrawableEntities)
            {
                stat += AddStatistic(section, StatGroup.DrawableEntities, type);
            }
            else if (!this.IncludeDrawableEntities && this.IncludeNonDrawableEntities)
            {
                stat += AddStatistic(section, StatGroup.NonDrawableEntities, type);
            }
            else
            {
                stat += AddStatistic(section, StatGroup.AllNonInteriorEntities, type);
            }
            return stat;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stat"></param>
        /// <param name="stats"></param>
        /// <param name="group"></param>
        /// <param name="type"></param>
        private uint AddStatistic(IMapSection section, StatGroup group, StatEntityType type)
        {
            uint stat = 0;

            if (this.IncludeHighDetailed)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Hd, type)];
                stat += stats.ShaderCount;
            }
            if (this.IncludeLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Lod, type)];
                stat += stats.ShaderCount;
            }
            if (this.IncludeSuperLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.SLod1, type)];
                stat += stats.ShaderCount;
            }

            return stat;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="propgroup"></param>
        /// <returns></returns>
        protected override double GetStatisticValue(IMapSection section, IMapSection propgroup)
        {
            if (section.Area == 0.0f)
                return 0.0;

            uint shaderCount = 0;
            if (section != null)
                shaderCount += GetStatisticValue(section);
            if (propgroup != null)
                shaderCount += GetStatisticValue(propgroup);

            return shaderCount / section.Area;
        }
        #endregion // Overrides
    } // ShaderInstanceDensityOverlay

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(ExtensionPoints.DensityStatisticsOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class TextureInstanceDensityOverlay : StatisticOverlayBase
    {
        #region Constants
        private const string c_name = "Entity Texture Density";
        private const string c_description = "Shows the density of the unique textures in the map container by the map containers area.  This statistic answers the question “How many non unique textures need loaded per meter squared are there in the map container”.";
        #endregion // Constants

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public TextureInstanceDensityOverlay()
            : base(c_name, c_description)
        {
            m_includeHighDetailed = true;
            m_includeLod = true;
            m_includeSuperLod = true;
            m_includeDynamic = true;
            m_includeNonDynamic = true;
            m_includeInterior = false;

            m_dontShowObjectOptions = false;
            m_dontShowReferenceOptions = false;
            m_dontShowInteriorOptions = true;
            m_dontShowUniqueOptions = true;
        }
        #endregion // Constructor

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        protected uint GetStatisticValue(IMapSection section)
        {
            if (!this.IncludeDynamic && !this.IncludeNonDynamic)
                return 0;
            if (!this.IncludeDrawableEntities && !this.IncludeNonDrawableEntities)
                return 0;

            StatEntityType type = StatEntityType.Total;
            if (this.IncludeDynamic && !this.IncludeNonDynamic)
            {
                type = StatEntityType.Dynamic;
            }
            else if (!this.IncludeDynamic && this.IncludeNonDynamic)
            {
                type = StatEntityType.NonDynamic;
            }

            uint stat = 0;
            if (this.IncludeDrawableEntities && !this.IncludeNonDrawableEntities)
            {
                stat = AddStatistic(section, StatGroup.DrawableEntities, type);
            }
            else if (!this.IncludeDrawableEntities && this.IncludeNonDrawableEntities)
            {
                stat = AddStatistic(section, StatGroup.NonDrawableEntities, type);
            }
            else
            {
                stat = AddStatistic(section, StatGroup.AllNonInteriorEntities, type);
            }
            return stat;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stat"></param>
        /// <param name="stats"></param>
        /// <param name="group"></param>
        /// <param name="type"></param>
        private uint AddStatistic(IMapSection section, StatGroup group, StatEntityType type)
        {
            uint stat = 0;

            if (this.IncludeHighDetailed)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Hd, type)];
                stat += stats.TextureCount;
            }
            if (this.IncludeLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Lod, type)];
                stat += stats.TextureCount;
            }
            if (this.IncludeSuperLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.SLod1, type)];
                stat += stats.TextureCount;
            }

            return stat;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="propgroup"></param>
        /// <returns></returns>
        protected override double GetStatisticValue(IMapSection section, IMapSection propgroup)
        {
            if (section.Area == 0.0f)
                return 0.0;

            uint textureCount = 0;
            if (section != null)
                textureCount += GetStatisticValue(section);
            if (propgroup != null)
                textureCount += GetStatisticValue(propgroup);

            return textureCount / section.Area;
        }
        #endregion // Overrides
    } // TextureInstanceDensityOverlay

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(ExtensionPoints.DensityStatisticsOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class PolygonInstanceDensityOverlay : StatisticOverlayBase
    {
        #region Constants
        private const string c_name = "Entity Polygon Density";
        private const string c_description = "Shows the density of the number of polygons in the map container by the map containers area. This statistic answers the question “How many polygons per meter squared are there in the map container”.";
        #endregion // Constants

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public PolygonInstanceDensityOverlay()
            : base(c_name, c_description)
        {
            m_includeHighDetailed = true;
            m_includeLod = true;
            m_includeSuperLod = true;
            m_includeDynamic = true;
            m_includeNonDynamic = true;
            m_includeInterior = false;

            m_dontShowObjectOptions = false;
            m_dontShowReferenceOptions = false;
            m_dontShowInteriorOptions = true;
            m_dontShowUniqueOptions = true;
        }
        #endregion // Constructor

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        protected uint GetStatisticValue(IMapSection section)
        {
            if (!this.IncludeDynamic && !this.IncludeNonDynamic)
                return 0;
            if (!this.IncludeDrawableEntities && !this.IncludeNonDrawableEntities)
                return 0;

            StatEntityType type = StatEntityType.Total;
            if (this.IncludeDynamic && !this.IncludeNonDynamic)
            {
                type = StatEntityType.Dynamic;
            }
            else if (!this.IncludeDynamic && this.IncludeNonDynamic)
            {
                type = StatEntityType.NonDynamic;
            }

            uint stat = 0;
            if (this.IncludeDrawableEntities && !this.IncludeNonDrawableEntities)
            {
                stat = AddStatistic(section, StatGroup.DrawableEntities, type);
            }
            else if (!this.IncludeDrawableEntities && this.IncludeNonDrawableEntities)
            {
                stat = AddStatistic(section, StatGroup.NonDrawableEntities, type);
            }
            else
            {
                stat = AddStatistic(section, StatGroup.AllNonInteriorEntities, type);
            }
            return stat;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="stat"></param>
        /// <param name="stats"></param>
        /// <param name="group"></param>
        /// <param name="type"></param>
        private uint AddStatistic(IMapSection section, StatGroup group, StatEntityType type)
        {
            uint stat = 0;

            if (this.IncludeHighDetailed)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Hd, type)];
                stat += stats.PolygonCount;
            }
            if (this.IncludeLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Lod, type)];
                stat += stats.PolygonCount;
            }
            if (this.IncludeSuperLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.SLod1, type)];
                stat += stats.PolygonCount;
            }

            return stat;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="propgroup"></param>
        /// <returns></returns>
        protected override double GetStatisticValue(IMapSection section, IMapSection propgroup)
        {
            if (section.Area == 0.0f)
                return 0.0;

            uint polygonCount = 0;
            if (section != null)
                polygonCount += GetStatisticValue(section);
            if (propgroup != null)
                polygonCount += GetStatisticValue(propgroup);

            return polygonCount / section.Area;
        }
        #endregion // Overrides
    } // PolygonInstanceDensityOverlay

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(ExtensionPoints.DensityStatisticsOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class CollisionInstanceDensityOverlay : StatisticOverlayBase
    {
        #region Constants
        private const string c_name = "Entity Collision Polygon Density";
        private const string c_description = "Shows the density of the number of collision polygons in the map container by the map containers area.  This statistic answers the question “How many collision polygons per meter squared are there in the map container”.";
        #endregion // Constants

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public CollisionInstanceDensityOverlay()
            : base(c_name, c_description)
        {
            m_includeHighDetailed = true;
            m_includeLod = true;
            m_includeSuperLod = true;
            m_includeDynamic = true;
            m_includeNonDynamic = true;
            m_includeInterior = false;

            m_dontShowObjectOptions = false;
            m_dontShowReferenceOptions = false;
            m_dontShowInteriorOptions = true;
            m_dontShowUniqueOptions = true;
        }
        #endregion // Constructor

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        protected uint GetStatisticValue(IMapSection section)
        {
            if (!this.IncludeDynamic && !this.IncludeNonDynamic)
                return 0;
            if (!this.IncludeDrawableEntities && !this.IncludeNonDrawableEntities)
                return 0;

            StatEntityType type = StatEntityType.Total;
            if (this.IncludeDynamic && !this.IncludeNonDynamic)
            {
                type = StatEntityType.Dynamic;
            }
            else if (!this.IncludeDynamic && this.IncludeNonDynamic)
            {
                type = StatEntityType.NonDynamic;
            }

            uint stat = 0;
            if (this.IncludeDrawableEntities && !this.IncludeNonDrawableEntities)
            {
                stat = AddStatistic(section, StatGroup.DrawableEntities, type);
            }
            else if (!this.IncludeDrawableEntities && this.IncludeNonDrawableEntities)
            {
                stat = AddStatistic(section, StatGroup.NonDrawableEntities, type);
            }
            else
            {
                stat = AddStatistic(section, StatGroup.AllNonInteriorEntities, type);
            }
            return stat;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="stat"></param>
        /// <param name="stats"></param>
        /// <param name="group"></param>
        /// <param name="type"></param>
        private uint AddStatistic(IMapSection section, StatGroup group, StatEntityType type)
        {
            uint stat = 0;

            if (this.IncludeHighDetailed)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Hd, type)];
                stat += stats.CollisionCount;
            }
            if (this.IncludeLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Lod, type)];
                stat += stats.CollisionCount;
            }
            if (this.IncludeSuperLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.SLod1, type)];
                stat += stats.CollisionCount;
            }

            return stat;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="propgroup"></param>
        /// <returns></returns>
        protected override double GetStatisticValue(IMapSection section, IMapSection propgroup)
        {
            if (section.Area == 0.0f)
                return 0.0;

            uint collisionCount = 0;
            if (section != null)
                collisionCount += GetStatisticValue(section);
            if (propgroup != null)
                collisionCount += GetStatisticValue(propgroup);

            return collisionCount / section.Area;
        }
        #endregion // Overrides
    } // CollisionInstanceDensityOverlay
} // Workbench.AddIn.MapStatistics.Overlays
 
