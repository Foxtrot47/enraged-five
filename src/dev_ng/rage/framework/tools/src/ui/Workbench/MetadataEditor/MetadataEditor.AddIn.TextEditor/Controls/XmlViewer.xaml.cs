﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;

namespace MetadataEditor.AddIn.TextEditor.Controls
{
    /// <summary>
    /// Interaction logic for XmlViewer.xaml
    /// </summary>
    public partial class XmlViewer : UserControl
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// 
        /// </summary>
        public XmlDocument Document
        {
            get { return m_Document; }
            set { m_Document = value; BindXmlDocument(); }
        }
        private XmlDocument m_Document;
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public XmlViewer()
        {
            InitializeComponent();
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void BindXmlDocument()
        {
            if (null == this.Document)
            {
                xmlTree.ItemsSource = null;
                return;
            }
            XmlDataProvider provider = new XmlDataProvider();
            provider.Document = this.Document;
            Binding binding = new Binding();
            binding.Source = provider;
            binding.XPath = "child::nodes()";
            xmlTree.SetBinding(TreeView.ItemsSourceProperty, binding);
        }
        #endregion // Private Methods
    }

} // MetadataEditor.AddIn.TextEditor namespace
