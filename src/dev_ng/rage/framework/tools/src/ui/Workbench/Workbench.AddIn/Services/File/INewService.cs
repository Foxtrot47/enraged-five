﻿using System;
using System.Windows.Input;
using System.Drawing;
using RSG.Base.Editor;
using Workbench.AddIn.Services;
using Workbench.AddIn.UI.Layout;

namespace Workbench.AddIn.Services.File
{

    /// <summary>
    /// Service to create a new IModel in-memory.
    /// </summary>
    public interface INewService : IService
    {
        #region Properties
        /// <summary>
        /// New service header string for UI display.
        /// </summary>
        String Header { get; }
        
        /// <summary>
        /// New service Bitmap for UI display.
        /// </summary>
        Bitmap Image { get; }

        /// <summary>
        /// The keybinding that is placed on this command.
        /// This binding is added to the applications bindings.
        /// (Only used if this is a main menu item)
        /// </summary>
        KeyGesture KeyGesture { get; }

        #endregion // Properties

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        bool PreNew(out Object param);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="document"></param>
        /// <param name="model"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        bool New(out IDocumentBase document, out IModel model, String filename, Object param);
        #endregion // Methods
    }

} // Workbench.AddIn.Services.File namespace
