﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Report;
using Workbench.AddIn.Services;
using RSG.Model.Common;
using System.ComponentModel;
using RSG.Base.ConfigParser;
using RSG.SceneXml;
using System.IO;
using RSG.Base.Logging;

namespace Workbench.AddIn.MapStatistics.Reports
{
    /// <summary>
    /// Note that this report is disabled in the workbench due to it being very slow
    /// </summary>
    [ExportExtension(Report.AddIn.ExtensionPoints.MapStatsReport, typeof(IReport))]
    public class SmallBoundsReport : RSG.Model.Report.Reports.Map.SmallBoundsReport
    {
    } // SmallBoundsReport
}
