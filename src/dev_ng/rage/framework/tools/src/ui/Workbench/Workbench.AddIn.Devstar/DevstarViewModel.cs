﻿using System;
using System.ComponentModel.Composition;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Workbench.AddIn;
using Workbench.AddIn.UI.Layout;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;

namespace Workbench.AddIn.Devstar
{
    public class DevstarViewModel : ViewModelBase
    {
        #region Constants

        private readonly Uri DEFAULT_ADDRESS = new Uri("https://devstar.rockstargames.com/wiki/index.php/Main_Page");

        #endregion // Constants

        #region Properties

        /// <summary>
        /// Current browser address.
        /// </summary>
        public Uri Address
        {
            get { return m_uriAddress; }
            set
            {
                SetPropertyValue(value, () => this.Address,
                    new PropertySetDelegate(delegate(Object newValue) { m_uriAddress = (Uri)newValue; }));
            }
        }
        private Uri m_uriAddress;

        /// <summary>
        /// 
        /// </summary>
        public bool CanGoBack
        {
            get { return m_bCanGoBack; }
            set { m_bCanGoBack = value; }
        }
        private bool m_bCanGoBack;

        /// <summary>
        /// 
        /// </summary>
        public bool CanGoForward
        {
            get { return m_bCanGoForward; }
            set { m_bCanGoForward = value; }
        }
        private bool m_bCanGoForward;

        /// <summary>
        /// Navigating status.
        /// </summary>
        public bool IsNavigating
        {
            get { return m_bIsNavigating; }
            set
            {
                SetPropertyValue(value, () => this.IsNavigating,
                   new PropertySetDelegate(delegate(Object newValue) { m_bIsNavigating = (bool)newValue; }));

            }
        }
        private bool m_bIsNavigating;

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Default constructor.
        /// </summary>
        public DevstarViewModel()
        {
            this.Address = DEFAULT_ADDRESS;
        }

        /// <summary>
        /// Constructor, specifying web address.
        /// </summary>
        public DevstarViewModel(String address)
        {
            this.Address = new Uri(address);
        }

        /// <summary>
        /// Constructor, specifying web address.
        /// </summary>
        public DevstarViewModel(Uri address)
        {
            this.Address = new Uri(address.ToString());
        }

        #endregion // Constructor(s)

        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        public void Go(String address)
        {
            this.Address = new Uri(address);
            this.Go();
        }

        /// <summary>
        /// 
        /// </summary>
        public void Go(Uri address)
        {
            this.Address = new Uri(address.ToString());
            this.Go();
        }

        /// <summary>
        /// 
        /// </summary>
        public void Go()
        {
            RSG.Base.Logging.Log.Log__Debug("GO: {0}", this.Address);
        }

        /// <summary>
        /// 
        /// </summary>
        public void Home()
        {
            this.Go(DEFAULT_ADDRESS);
        }

        /// <summary>
        /// 
        /// </summary>
        public void Forward()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        public void Back()
        {

        }

        #endregion // Controller Methods
    }

    /// <summary>
    /// 
    /// </summary>
    /// We should be able to replace this by using the RSG.Base.Editor.Proxy
    /// generic class at some point.
    ///   http://www.weask.us/entry/databind-source-property-webbrowser-wpf
    ///   
    [Obsolete]
    public static class WebBrowserUtility
    {
        #region Source Property
        public static readonly DependencyProperty BindableSourceProperty =
            DependencyProperty.RegisterAttached("BindableSource", typeof(string), typeof(WebBrowserUtility), new UIPropertyMetadata(null, BindableSourcePropertyChanged));

        public static string GetBindableSource(DependencyObject obj)
        {
            return (string)obj.GetValue(BindableSourceProperty);
        }

        public static void SetBindableSource(DependencyObject obj, string value)
        {
            obj.SetValue(BindableSourceProperty, value);
        }

        public static void BindableSourcePropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            WebBrowser browser = o as WebBrowser;
            if (browser != null)
            {
                String uri = e.NewValue as String;
                if (!uri.StartsWith("http://") && !uri.StartsWith("https://"))
                {
                    uri = "http://" + e.NewValue as String;
                }
                try
                {
                    browser.Source = uri != null ? new Uri(uri) : null;
                }
                catch
                {
                }
            }
        }
        #endregion // Source Property
    }

} // Workbench.AddIn.Devstar namespace
