﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Workbench.Services
{
    public enum ReadOnlyResult
    {
        Cancel,
        SaveAs,
        Overwrite,
    }

    /// <summary>
    /// Interaction logic for SaveReadOnlyWindow.xaml
    /// </summary>
    public partial class SaveReadOnlyWindow : Window
    {
        public ReadOnlyResult Result
        {
            get;
            set;
        }

        public String File
        {
            get;
            set;
        }

        public SaveReadOnlyWindow()
        {
            this.File = "Unknown";
            this.Result = ReadOnlyResult.Cancel;
            InitializeComponent();
        }

        public SaveReadOnlyWindow(String file)
        {
            this.File = file;
            this.Result = ReadOnlyResult.Cancel;
            InitializeComponent();
        }

        private void OnOverwrite(Object sender, RoutedEventArgs e)
        {
            Result = ReadOnlyResult.Overwrite;
            this.DialogResult = true;
        }

        private void OnSaveAs(Object sender, RoutedEventArgs e)
        {
            Result = ReadOnlyResult.SaveAs;
            this.DialogResult = true;
        }

        private void OnCancel(Object sender, RoutedEventArgs e)
        {
            Result = ReadOnlyResult.Cancel;
            this.DialogResult = false;
        }
    } // SaveReadOnlyWindow
} // Workbench.Services
