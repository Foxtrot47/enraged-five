﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using RSG.Model.Common;

namespace ContentBrowser.ViewModel.MapViewModels3
{
    /// <summary>
    /// 
    /// </summary>
    public class TextureDetailsViewModel : ViewModelBase
    {
        #region Members
        private ITexture m_model = null;
        #endregion // Members

        #region Properties
        /// <summary>
        /// A reference to the types model that this
        /// view model is wrapping
        /// </summary>
        public ITexture Model
        {
            get { return m_model; }
            set
            {
                if (m_model == value)
                    return;

                SetPropertyValue(value, () => Model,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_model = (ITexture)newValue;
                        }
                ));
            }
        }
        #endregion // Properties

        public TextureDetailsViewModel(ITexture texture)
        {
            this.Model = texture;
        }
    }
}
