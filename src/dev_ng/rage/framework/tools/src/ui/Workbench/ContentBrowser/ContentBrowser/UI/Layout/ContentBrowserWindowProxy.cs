﻿using System;
using System.ComponentModel.Composition;
using System.Collections.Generic;
using Workbench.AddIn.UI.Layout;
using ContentBrowser.AddIn;

namespace ContentBrowser.UI.Layout
{
    /// <summary>
    /// The main proxy to the content browser, so that it is hooked into the workbench
    /// </summary>
    [Workbench.AddIn.ExportExtension(Workbench.AddIn.ExtensionPoints.ToolWindowProxy, typeof(IToolWindowProxy))]
    [Workbench.AddIn.ExportExtension(Workbench.AddIn.ExtensionPoints.ToolbarStandard, typeof(Workbench.AddIn.Commands.IWorkbenchCommand))]
    class ContentBrowserWindowProxy : IToolWindowProxy, IPartImportsSatisfiedNotification
    {
        #region MEF Imports

        /// <summary>
        /// The main content browser export object; will also be used as the view model
        /// for the content browsers main window
        /// </summary>
        [Workbench.AddIn.ImportExtension(CompositionPoints.ContentBrowser, typeof(IContentBrowser))]
        private Lazy<IContentBrowser> ContentBrowserViewModel { get; set; }

        /// <summary>
        /// Layout manager reference.
        /// </summary>
        [Workbench.AddIn.ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager, typeof(ILayoutManager))]
        private ILayoutManager LayoutManager { get; set; }

        #endregion // MEF Imports

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public ContentBrowserWindowProxy()
        {
            this.Name = "ContentBrowser";
            this.Header = "Content Browser";

            SetImageFromBitmap(ContentBrowser.Resources.Images.BrowserIcon);
            this.RelativeToolBarID = new Guid(Workbench.AddIn.CompositionPoints.ToolWindowSeparator);
            this.Direction = Workbench.AddIn.Services.Direction.After;
            this.ToolTip = "Content Browser";
            this.IsToggle = true;
            this.KeyGesture = new System.Windows.Input.KeyGesture(System.Windows.Input.Key.J, System.Windows.Input.ModifierKeys.Control | System.Windows.Input.ModifierKeys.Alt);
        }

        #endregion // Constructor

        #region IToolWindowProxy

        /// <summary>
        /// A abstract function that is overriden by the plugin to create the
        /// actual instance of the tool window and pass it back as a out paramter
        /// </summary>
        protected override Boolean CreateToolWindow(out IToolWindowBase toolWindow)
        {
            if (this.ContentBrowserViewModel.Value is ViewModel.ContentBrowserDataContext)
            {
                this.ContentBrowserViewModel.Value.InitialiseBrowsers();
                toolWindow = new ContentBrowser.View.ContentBrowserView(this.ContentBrowserViewModel.Value as ViewModel.ContentBrowserDataContext);
            }
            else
            {
                toolWindow = new ContentBrowser.View.ContentBrowserView();
            }
            return true;
        }

        #endregion // IToolWindowProxy

        #region IPartImportsSatisfiedNotification

        public void OnImportsSatisfied()
        {
            this.LayoutManager.LayoutUpdated += new EventHandler(LayoutManager_LayoutUpdated);
        }

        private void LayoutManager_LayoutUpdated(Object sender, EventArgs e)
        {
            if (this.HasBeenCreated == true)
            {
                this.IsChecked = LayoutManager.IsVisible(this.GetToolWindow());
            }
        }

        #endregion // IPartImportsSatisfiedNotification

        #region ICommand Implementation

        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
            IToolWindowBase toolWindow = this.GetToolWindow();
            if (toolWindow != null)
            {
                if (this.LayoutManager.IsVisible(toolWindow))
                    toolWindow.Hide();
                else
                {
                    if (toolWindow.WasFloatingWindowWhenHidden == true)
                        toolWindow.ShowAsFloatingWindow(true);
                    else
                        this.LayoutManager.ShowToolWindow(toolWindow);

                    if (toolWindow.FloatByDefault == true && toolWindow.HasBeenShown == false)
                    {
                        toolWindow.FloatingWindowSize = toolWindow.DefaultFloatSize;
                        toolWindow.ShowAsFloatingWindow(true);
                    }
                }
            }
        }

        #endregion // ICommand Implementation
    } // ContentBrowserWindowProxy
} // ContentBrowser.UI.Layout
