﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.SceneXml;
using RSG.Model.Map3;
using RSG.Base.Math;
using RSG.Base.Collections;

namespace Workbench.AddIn.MapStatistics.Overlays.RoadDensity
{
    /// <summary>
    /// Road density model.
    /// </summary>
    internal class RoadDensityModel
    {
        #region Private member fields

        private Dictionary<int, List<Chain>> m_chainBucket;
        private List<Chain> m_chains;
        private VehiclePath m_path;
        private Vector2f m_offset;

        #endregion

        #region Public properties

        /// <summary>
        /// Set to true if pedestrian crossing are to be ignored (i.e. won't be rendered).
        /// </summary>
        public bool IgnorePedCrossings { get; set; }

        /// <summary>
        /// Set to true if assisted movement routes are to be ignored (i.e. won't be rendered).
        /// </summary>
        public bool IgnoreAssistedMovementRoutes { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="sceneXml">Path to the Scene Xml file.</param>
        public RoadDensityModel(string sceneXml)
            : this(sceneXml, new Vector2f(0, 0), false)
        {

        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="sceneXml">Path to the Scene Xml file.</param>
        /// <param name="offset">Offset.</param>
        /// <param name="dontUseForNavigation">If true, links marked as don't use for navigation will not be included.</param>
        public RoadDensityModel(string sceneXml, Vector2f offset, bool dontUseForNavigation)
        {
            m_offset = offset;

            Scene scene = new Scene(sceneXml, LoadOptions.All, false);
            m_path = new VehiclePath(scene, dontUseForNavigation);

            m_chains = new List<Chain>();
            m_chainBucket = new Dictionary<int, List<Chain>>();

            IgnorePedCrossings = true;
            IgnoreAssistedMovementRoutes = true;
        }

        #endregion

        #region Public methods

        public void Parse()
        {
            List<VehicleNodeLink> linkPruneList = new List<VehicleNodeLink>(m_path.Links);

            while (linkPruneList.Count > 0)
            {
                VehicleNodeLink current = linkPruneList[0];
                List<VehicleNodeLink> walkedLinks = new List<VehicleNodeLink>();
                WalkLinks(walkedLinks, current, linkPruneList);
                if (walkedLinks.Count > 0)
                {
                    CreateChain(walkedLinks);
                }

                current = linkPruneList.Count > 0 ? linkPruneList[0] : null;
            }
        }
        
        /// <summary>
        /// Get the chains.
        /// </summary>
        /// <returns>Collection of chains of points.</returns>
        public IEnumerable<Chain> GetChains()
        {
            foreach (var chain in m_chains)
            {
                yield return chain;
            }
        }

        #endregion

        #region Private helper methods

        /// <summary>
        /// Walk the node link graph and create a list of links that connect with each other.
        /// </summary>
        /// <param name="links">Links.</param>
        /// <param name="link">Current link.</param>
        /// <param name="linkPruneList">Link prune list.</param>
        private void WalkLinks(List<VehicleNodeLink> links, VehicleNodeLink link, List<VehicleNodeLink> linkPruneList)
        {
            VehicleNode firstNode = link.FirstNode;
            VehicleNode secondNode = link.SecondNode;

            links.Add(link);
            linkPruneList.Remove(link);

            bool addLink = true;

            if (IsPedestrianWay(firstNode) && IsPedestrianWay(secondNode))
            {
                addLink = false;
            }

            VehicleNodeLink newLink = linkPruneList.Find(lnk => !lnk.IsShortCut && (lnk.FirstNode == secondNode || lnk.SecondNode == secondNode || link.SecondNode == firstNode));

            while (newLink != null)
            {
                linkPruneList.Remove(newLink);
                if (addLink)
                {
                    links.Add(newLink);
                }

                newLink = linkPruneList.Find(lnk => !lnk.IsShortCut && (lnk.FirstNode == secondNode || lnk.SecondNode == secondNode || link.SecondNode == firstNode));

                if (newLink != null)
                {
                    firstNode = newLink.FirstNode;
                    secondNode = newLink.SecondNode;
                    addLink = true;

                    if (IsPedestrianWay(firstNode) && IsPedestrianWay(secondNode))
                    {
                        addLink = false;
                    }
                }
            }
        }

        /// <summary>
        /// Create a chain of points from the supplied walkedLinks. Adds it to the internal list
        /// of chains.
        /// </summary>
        /// <param name="walkedLinks">Links.</param>
        private void CreateChain(List<VehicleNodeLink> walkedLinks)
        {
            Chain chain = new Chain(m_offset);

            foreach (var node in GetUniqueNodes(walkedLinks))
            {
                if (IsPedestrianWay(node))
                {
                    continue;
                }

                int density = node.IsDisabled ? 0 : node.Density;

                if (node.SpecialUse == VehicleNodeSpecialUse.PedCrossing)
                {
                    density = Chain.c_pedCrossingDensity;
                }
                else if (node.SpecialUse == VehicleNodeSpecialUse.PedAssistedMovement)
                {
                    density = Chain.c_pedAssistedMovementDensity;
                }

                chain.Add(node.Position.X, node.Position.Y, density, node.SpecialUse);
            }

            m_chains.Add(chain);
        }

        /// <summary>
        /// Create a list of unique nodes from the list of links.
        /// </summary>
        /// <param name="walkedLinks">Links.</param>
        /// <returns>Enumerable list of unique nodes from the supplied walkedLinks.</returns>
        private IEnumerable<VehicleNode> GetUniqueNodes(List<VehicleNodeLink> walkedLinks)
        {
            List<VehicleNode> nodes = new List<VehicleNode>();

            foreach (var link in walkedLinks)
            {
                if (!nodes.Contains(link.FirstNode))
                {
                    nodes.Add(link.FirstNode);
                }
                if (!nodes.Contains(link.SecondNode))
                {
                    nodes.Add(link.SecondNode);
                }
            }

            foreach (var node in nodes)
            {
                yield return node;
            }
        }

        /// <summary>
        /// Returns true if the node is a pedestrian or assisted movement way.
        /// </summary>
        /// <param name="node">Node.</param>
        /// <returns>True if the node is a pedestrian or assisted movement way.</returns>
        private bool IsPedestrianWay(VehicleNode node)
        {
            return (node.SpecialUse == VehicleNodeSpecialUse.PedCrossing && IgnorePedCrossings) ||
                   (node.SpecialUse == VehicleNodeSpecialUse.PedAssistedMovement && IgnoreAssistedMovementRoutes);
        }
        
        #endregion
    }
}
