﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Workbench.AddIn.SceneOverrideEditor
{
    public class AttributeEdit
    {
        public AttributeEdit(uint hash, uint guid, float posX, float posY, float posZ, 
                             String imapName, String modelName, 
                             bool dontCastShadows, bool dontRenderInShadows,
                             bool dontRenderInReflections, bool onlyRenderInReflections,
                             bool dontRenderInWaterReflections, bool onlyRenderInWaterReflections,
                             bool dontRenderInMirrorReflections, bool onlyRenderInMirrorReflections, 
                             bool streamingPriorityLow, int priority)
        {
            Guid = guid;
            Hash = hash;
            PosX = posX;
            PosY = posY;
            PosZ = posZ;
            IMAPName = imapName;
            ModelName = modelName;
            DontCastShadows = dontCastShadows;
            DontRenderInShadows = dontRenderInShadows;
            DontRenderInReflections = dontRenderInReflections;
            OnlyRenderInReflections = onlyRenderInReflections;
            DontRenderInWaterReflections = dontRenderInWaterReflections;
            OnlyRenderInWaterReflections = onlyRenderInWaterReflections;
            DontRenderInMirrorReflections = dontRenderInMirrorReflections;
            OnlyRenderInMirrorReflections = onlyRenderInMirrorReflections;
            StreamingPriorityLow = streamingPriorityLow;
            Priority = priority;
        }

        public uint Guid { get; private set; }
        public uint Hash { get; private set; }
        public float PosX { get; private set; }
        public float PosY { get; private set; }
        public float PosZ { get; private set; }
        public String IMAPName { get; private set; }
        public String ModelName { get; private set; }
        internal bool DontCastShadows { get; private set; }
        internal bool DontRenderInShadows { get; private set; }
        internal bool DontRenderInReflections { get; private set; }
        internal bool OnlyRenderInReflections { get; private set; }
        internal bool DontRenderInWaterReflections { get; private set; }
        internal bool OnlyRenderInWaterReflections { get; private set; }
        internal bool DontRenderInMirrorReflections { get; private set; }
        internal bool OnlyRenderInMirrorReflections { get; private set; }
        internal bool StreamingPriorityLow { get; private set; }
        internal int Priority { get; private set; }

        public String PositionText
        {
            get
            {
                return String.Format("[{0}, {1}, {2}]", PosX.ToString("0.00"), PosY.ToString("0.00"), PosZ.ToString("0.00"));
            }
        }

        public String DontCastShadowsText
        {
            get
            {
                return DontCastShadows.ToString();
            }
        }

        public String DontRenderInShadowsText
        {
            get
            {
                return DontRenderInShadows.ToString();
            }
        }

        public String DontRenderInReflectionsText
        {
            get
            {
                return DontRenderInReflections.ToString();
            }
        }

        public String OnlyRenderInReflectionsText
        {
            get
            {
                return OnlyRenderInReflections.ToString();
            }
        }

        public String DontRenderInWaterReflectionsText
        {
            get
            {
                return DontRenderInWaterReflections.ToString();
            }
        }

        public String OnlyRenderInWaterReflectionsText
        {
            get
            {
                return OnlyRenderInWaterReflections.ToString();
            }
        }

        public String DontRenderInMirrorReflectionsText
        {
            get
            {
                return DontRenderInMirrorReflections.ToString();
            }
        }

        public String OnlyRenderInMirrorReflectionsText
        {
            get
            {
                return OnlyRenderInMirrorReflections.ToString();
            }
        }

        public String StreamingPriorityLowText
        {
            get
            {
                return StreamingPriorityLow.ToString();
            }
        }

        public String PriorityText
        {
            get
            {
                return Priority.ToString();
            }
        }
    }
}
