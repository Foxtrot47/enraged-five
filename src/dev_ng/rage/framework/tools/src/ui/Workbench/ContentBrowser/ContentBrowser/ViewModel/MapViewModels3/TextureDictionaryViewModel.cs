﻿using System;
using System.Windows.Media;
using ContentBrowser.ViewModel.LevelViewModels;
using RSG.Base.Editor;
using RSG.Model.Asset;

namespace ContentBrowser.ViewModel.MapViewModels3
{
    public class TextureDictionaryViewModel : FileAssetContainerViewModelBase
    {
        #region Members
        private TextureDictionary m_model = null;

        private LevelViewModel level;

        /// <summary>
        /// The private field used for the <see cref="SavingMemory"/> property.
        /// </summary>
        private Nullable<bool> savingMemory;

        /// <summary>
        /// The private field used for the <see cref="Parented"/> property.
        /// </summary>
        private Nullable<bool> parented;

        private static object syncObject = new object();
        #endregion // Members

        #region Properties
        /// <summary>
        /// A reference to the types model that this
        /// view model is wrapping
        /// </summary>
        public TextureDictionary Model
        {
            get { return m_model; }
            set
            {
                if (m_model == value)
                    return;

                SetPropertyValue(value, () => Model,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_model = (TextureDictionary)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Gets a value indicating whether the dictionary is curent saving memory or
        /// costing while being in the global texture dictionary tree view.
        /// </summary>
        public bool SavingMemory
        {
            get
            {
                if (this.savingMemory == null)
                {
                    lock (syncObject)
                    {
                        if (this.savingMemory == null)
                        {
                            LevelViewModel.ParentedState state = this.level.GetParentedState(this.Model);
                            this.parented = state.Parented;
                            this.savingMemory = state.SavingMemory;
                        }
                    }
                }

                return (bool)this.savingMemory;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the associated dictionary is in the global
        /// texture dictionary tree view.
        /// </summary>
        public bool Parented
        {
            get
            {
                if (this.parented == null)
                {
                    lock (syncObject)
                    {
                        if (this.parented == null)
                        {
                            LevelViewModel.ParentedState state = this.level.GetParentedState(this.Model);
                            this.parented = state.Parented;
                            this.savingMemory = state.SavingMemory;
                        }
                    }
                }

                return (bool)this.parented;
            }
        }

        public string DisplayName
        {
            get
            {
                if (this.Parented)
                {
                    return this.Name + " (Member of global hierarchy)";
                }
                else
                {
                    return this.Name;
                }
            }
        }

        public Brush Foreground
        {
            get
            {
                if (this.Parented)
                {
                    if (this.SavingMemory)
                    {
                        return Brushes.Green;
                    }
                    else
                    {
                        return Brushes.Red;
                    }
                }
                else
                {
                    return Brushes.Black;
                }
            }
        }

        #endregion // Properties

        public TextureDictionaryViewModel(TextureDictionary textureDictionary, LevelViewModel level)
            : base(textureDictionary)
        {
            this.Model = textureDictionary;
            this.level = level;
        }
    }
}
