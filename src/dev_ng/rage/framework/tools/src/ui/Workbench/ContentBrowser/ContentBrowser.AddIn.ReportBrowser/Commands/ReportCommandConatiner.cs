﻿using System;
using System.ComponentModel.Composition;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn;
using Workbench.AddIn.Commands;

namespace ContentBrowser.AddIn.ReportBrowser.Commands
{
    /// <summary>
    /// Contains all the commands that are attached to the asset
    /// type IReport
    /// </summary>
    [ExportExtension(ContentBrowser.AddIn.ExtensionPoints.AssetCommandContainer, typeof(IAssetCommandContainer))]
    public class ReportCommandContainer : IAssetCommandContainer
    {
        #region MEF Import

        /// <summary>
        /// The imported commands
        /// </summary>
        [ImportManyExtension(ContentBrowser.AddIn.ExtensionPoints.ReportAssetCommands, typeof(IWorkbenchCommand))]
        public IEnumerable<IWorkbenchCommand> ImportedCommands
        {
            get;
            set;
        }
        
        /// <summary>
        /// Extensions service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ExtensionService, typeof(Workbench.AddIn.Services.IExtensionService))]
        private Workbench.AddIn.Services.IExtensionService ExtensionService
        { 
            get;
            set;
        }

        #endregion // MEF Import

        #region Properties

        /// <summary>
        /// The type that this command will be attached to.
        /// </summary>
        public Type AssetType 
        {
            get { return typeof(RSG.Model.Report.IReport); }
        }

        /// <summary>
        /// The list of workbench commands that are 
        /// attached to the asset type.
        /// </summary>
        public IEnumerable<IWorkbenchCommand> Commands
        {
            get;
            set;
        }
        
        #endregion // Properties

        #region IPartImportsSatisfiedNotification Interface

        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {
            this.Commands = ExtensionService.Sort(this.ImportedCommands);
        }

        #endregion // IPartImportsSatisfiedNotification Interface
    } // ReportCommandContainer
} // ContentBrowser.AddIn.ReportBrowser.Commands
