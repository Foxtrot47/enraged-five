﻿using System;
using System.Windows;

namespace MetadataEditor.AddIn.MetaEditor.GridViewModel
{
    class Expander : System.Windows.Controls.Expander
    {
        #region Dependency Properties

        public static readonly DependencyProperty CanToggleProperty =
            DependencyProperty.Register("CanToggle", typeof(Boolean),
            typeof(Expander), new PropertyMetadata(true));

        public Boolean CanToggle
        {
            get { return (Boolean)GetValue(Expander.CanToggleProperty); }
            set { SetValue(Expander.CanToggleProperty, value); }
        }

        #endregion // Dependency Properties
    } // Expander
} // MetadataEditor.AddIn.MetaEditor.GridViewModel
