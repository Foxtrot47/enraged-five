﻿using System;
using System.Collections.Generic;
using System.Linq;
using RSG.Model.Common.Map;
using System.Windows.Media;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using RSG.Model.Common;
using System.Threading;
using RSG.Base.Tasks;
using RSG.Model.Common.Util;
using RSG.Base.Windows.Dialogs;
using RSG.Base.Math;
using RSG.Base.Drawing;
using MapViewport.AddIn;
using Workbench.AddIn.Services.Model;

namespace Workbench.AddIn.MapViewport.Overlays
{
    [ExportExtension(Viewport.AddIn.ExtensionPoints.MiscellaneousOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class VegetationOverlay : LevelDependentViewportOverlay
    {
        #region Private internal classes

        /// <summary>
        /// Section pair helper class.
        /// </summary>
        class SectionPair
        {
            #region Public properties

            /// <summary>
            /// The main map section. 
            /// </summary>
            public IMapSection MainSection { get; private set; }

            /// <summary>
            /// The list version of the map sections.
            /// </summary>
            public IList<IMapSection> MapSections { get; private set; }

            #endregion

            #region Constructor

            /// <summary>
            /// Constructor.
            /// </summary>
            /// <param name="mainSection">The main map section.</param>
            /// <param name="propSection">The list version of the map sections.</param>
            internal SectionPair(IMapSection mainSection, IMapSection propSection)
            {
                MainSection = mainSection;
                MapSections = new List<IMapSection>() { mainSection, propSection };
            }

            #endregion
        }

        /// <summary>
        /// Map section render data helper class.
        /// </summary>
        class MapSectionRenderData
        {
            #region Public properties

            /// <summary>
            /// Number of entities that match the search criteria.
            /// </summary>
            public int EntityCount { get; set; }

            /// <summary>
            /// The section area.
            /// </summary>
            public Vector2f[] MapPoints { get; set; }

            /// <summary>
            /// The name of the map section.
            /// </summary>
            public string SectionName { get; set; }

            /// <summary>
            /// Ratio between the number of entities and the area of the map section.
            /// </summary>
            public double Density
            {
                get
                {
                    return m_area > 0 ? Math.Min(1, m_entityArea / m_area) : 0;
                }
            }

            #endregion

            #region Constructor

            /// <summary>
            /// Constructor.
            /// </summary>
            /// <param name="area">Area.</param>
            /// <param name="entityArea">The area taken up by the entity (references the entities bounding box).</param>
            public MapSectionRenderData(double area, double entityArea)
            {
                m_area = area;
                m_entityArea = entityArea;
            }

            #endregion

            #region Private member fields

            private double m_area;
            private double m_entityArea;

            #endregion
        }

        #endregion

        #region Constants

        /// <summary>
        /// Overlay name.
        /// </summary>
        private const String c_name = "Vegetation Per Container";

        /// <summary>
        /// Overlay description.
        /// </summary>
        private const String c_description = "Shows the vegetation count and density of individual containers for the map.";

        #endregion

        #region Static shader names

        /// <summary>
        /// Vegetation shader names.
        /// </summary>
        private static readonly string[] s_vegShaders = new string[]
        {
            "tree.sps",
            "trees_normal.sps",
            "trees_normal_spec.sps",
            "trees_tnt.sps",
            "trees.sps",

            // TODO: What other vegetation is here?
        };

        #endregion

        #region MEF Imports

        /// <summary>
        /// Level browser.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LevelBrowser, typeof(ILevelBrowser))]
        Lazy<ILevelBrowser> LevelBrowserProxy { get; set; }

        /// <summary>
        /// Content Browser
        /// </summary>
        [ImportExtension(ContentBrowser.AddIn.CompositionPoints.ContentBrowser, typeof(ContentBrowser.AddIn.IContentBrowser))]
        protected ContentBrowser.AddIn.IContentBrowser ContentBrowserProxy { get; set; }

        #endregion 

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        public VegetationOverlay() :
            base(c_name, c_description)
        {
            
        }

        #endregion

        #region Overrides

        /// <summary>
        /// Fill in the viewport when the overlay is activated.
        /// </summary>
        public override void Activated()
        {
            base.Activated();
            UpdateOverlay();
        }

        /// <summary>
        /// Remove all rendered components when the overlay is deactivated.
        /// </summary>
        public override void Deactivated()
        {
            base.Deactivated();
            Geometry.Clear();
        }

        #endregion

        #region Private helper methods

        /// <summary>
        /// Update the rendered data displayed on the overlay.
        /// </summary>
        private void UpdateOverlay()
        {
            Geometry.BeginUpdate();
            Geometry.Clear();

            OnActivate();

            Geometry.EndUpdate();
        }

        /// <summary>
        /// Get the valid section selection.
        /// </summary>
        /// <param name="selectedAssets">Selected map sections.</param>
        /// <returns>The validated section selection.</returns>
        private IDictionary<IMapSection, IMapSection> GetValidSectionSelection(IEnumerable<IMapSection> mapSections)
        {
            // Gather the sections that the user has selected by drilling down the hierarchy
            ISet<IMapSection> selectedSections = new HashSet<IMapSection>();

            foreach(var section in mapSections)
            {
                selectedSections.Add(section);
            }
            
            // Generate a mapping between sections and prop groups based on the selected sections
            IDictionary<IMapSection, IMapSection> validSections = new Dictionary<IMapSection, IMapSection>();

            foreach (IMapSection section in selectedSections)
            {
                if (section.PropGroup != null)
                {
                    validSections.Add(section, section.PropGroup);
                }
                else if (section.PropGroup == null && section.Name.ToLower().EndsWith("_props"))
                {
                    // Make sure we haven't already added (or won't be adding) the section as part of the non-prop section
                    if (selectedSections.FirstOrDefault(item => item.Name == section.Name.ToLower().Replace("_props", "")) == null)
                    {
                        IMapSection ownerSection = section.MapHierarchy.AllSections.FirstOrDefault(item => item.PropGroup == section);
                        if (ownerSection != null)
                        {
                            validSections.Add(ownerSection, section);
                        }
                        else
                        {
                            validSections.Add(section, null);
                        }
                    }
                }
                else if (section.PropGroup == null)
                {
                    validSections.Add(section, null);
                }
            }

            return validSections;
        }

        /// <summary>
        /// Get an enumerable of valid entities.
        /// </summary>
        /// <param name="section">Map section.</param>
        /// <returns>An enumerable containing a list of valid entities.</returns>
        private IEnumerable<IEntity> GetValidEntities(IMapSection section)
        {
            if (section == null || section.ChildEntities == null)
            {
                yield break;
            }

            foreach (IEntity entity in section.ChildEntities)
            {
                if (entity.ReferencedArchetype != null && !(entity.ReferencedArchetype is IInteriorArchetype))
                {
                    IMapArchetype mapArchetype = (IMapArchetype)entity.ReferencedArchetype;

                    // Check if any of the map archetype shaders is one of the hd tree shaders
                    if (mapArchetype.Shaders.Select(item => item.Name).Intersect(s_vegShaders).Any())
                    {
                        yield return entity;
                    }
                }
            }
        }


        /// <summary>
        /// Ensure that the section data has been loaded.
        /// </summary>
        /// <param name="context">Task context.</param>
        /// <param name="progress">Progress.</param>
        private void EnsureDataLoaded(SectionTaskContext context, IProgress<TaskProgress> progress)
        {
            float increment = 1.0f / context.Sections.Count();

            // Load all the map section data
            foreach (IMapSection section in context.Sections)
            {
                context.Token.ThrowIfCancellationRequested();

                // Update the progress information
                string message = String.Format("Loading {0}", section.Name);
                progress.Report(new TaskProgress(increment, true, message));

                section.RequestStatistics(new StreamableStat[] { StreamableMapSectionStat.Entities }, false);
            }
        }

        /// <summary>
        /// Get all sections.
        /// </summary>
        /// <returns>The list of all sections to parse.</returns>
        private IList<IMapSection> GetAllSections()
        {
            List<IMapSection> allSections = new List<IMapSection>();

            foreach (var obj in this.ContentBrowserProxy.SelectedGridItems)
            {
                if (obj is ILevel)
                {
                    allSections.AddRange((obj as ILevel).MapHierarchy.AllSections);
                }
                else if (obj is IMapArea)
                {
                    allSections.AddRange((obj as IMapArea).AllDescendentSections);
                }
                else if (obj is IMapSection)
                {
                    allSections.Add(obj as IMapSection);
                }
            }

            if (allSections.Count == 0)
            {
                allSections.AddRange(LevelBrowserProxy.Value.SelectedLevel.MapHierarchy.AllSections);
            }

            return allSections;
        }

        /// <summary>
        /// When activated, the overlay populates the viewport with container overlays representing the number of vegetation entities.
        /// </summary>
        private void OnActivate()
        {
            if (LevelBrowserProxy.Value.SelectedLevel == null || LevelBrowserProxy.Value.SelectedLevel.MapHierarchy == null)
            {
                return;
            }

            IDictionary<IMapSection, IMapSection> validSections = GetValidSectionSelection(GetAllSections());

            CancellationTokenSource cts = new CancellationTokenSource();
            SectionTaskContext context = new SectionTaskContext(cts.Token, validSections.Keys.Union(validSections.Values).Where(item => item != null).ToList());

            CompositeTask compositeTask = new CompositeTask("Updating Overlay");
            compositeTask.AddSubTask(new ActionTask("Load Data", (ctx, progress) => EnsureDataLoaded((SectionTaskContext)ctx, progress)));

            TaskExecutionDialog dialog = new TaskExecutionDialog();
            TaskExecutionViewModel vm = new TaskExecutionViewModel("Generating Overlay", compositeTask, cts, context);
            vm.AutoCloseOnCompletion = true;
            dialog.DataContext = vm;
            bool? selectionResult = dialog.ShowDialog();
            
            if (selectionResult.HasValue && !selectionResult.Value)
            {
                return;
            }

            List<SectionPair> mapSections = new List<SectionPair>();

            foreach(var pair in validSections)
            {
                mapSections.Add(new SectionPair(pair.Key, pair.Value));
            }

            OnActivate(mapSections);
        }

        /// <summary>
        /// Get the valid entity count for a section pair.
        /// </summary>
        /// <param name="pair">Section pair.</param>
        /// <param name="area">The total area consumed by the entities</param>
        /// <returns>The valid entity count for a section pair.</returns>
        private int GetValidEntityCount(SectionPair pair, out double area)
        {
            int count = 0;
            area = 0;

            foreach(IMapSection section in pair.MapSections)
            {
                if (section == null)
                {
                    continue;
                }

                foreach (var entity in GetValidEntities(section))
                {
                    Vector3f diff = entity.BoundingBox.Max - entity.BoundingBox.Min;
                    area = area + (diff.X * diff.Y);
                    count++;
                }
            }

            return count;
        }

        /// <summary>
        /// Create the viewport render data from the map section data.
        /// </summary>
        /// <param name="mapSections">Map section data.</param>
        /// <param name="maximum">Maximum entity count.</param>
        /// <param name="minimum">Minimum entity count.</param>
        /// <returns>A list of render data to add to the viewport overlay.</returns>
        private List<MapSectionRenderData> FillRenderData(List<SectionPair> mapSections, ref double maximum, ref double minimum)
        {
            minimum = double.MaxValue;
            maximum = double.MinValue;

            List<MapSectionRenderData> stuff = new List<MapSectionRenderData>();

            foreach (var mapSection in mapSections)
            {
                if (mapSection != null && mapSection.MainSection.VectorMapPoints != null)
                {
                    double entityArea = 0;
                    int count = GetValidEntityCount(mapSection, out entityArea);

                    if (count < minimum)
                    {
                        minimum = count;
                    }

                    if (count > maximum)
                    {
                        maximum = count;
                    }

                    MapSectionRenderData renderData = new MapSectionRenderData(mapSection.MainSection.Area, entityArea);
                    renderData.MapPoints = mapSection.MainSection.VectorMapPoints.ToArray();
                    renderData.EntityCount = count;
                    renderData.SectionName = mapSection.MainSection.Name;

                    stuff.Add(renderData);

                    if (count > 0)
                    {
                        if (count > maximum)
                        {
                            maximum = count;
                        }
                    }
                }
            }

            return stuff;
        }

        /// <summary>
        /// Calculate the colour value based upon the percentage.
        /// </summary>
        /// <param name="percentage">Percentage.</param>
        /// <returns>Colour value.</returns>
        private Color CalcColour(double percentage)
        {
            // Uses green -> red range (dividing by 3 below is same as *0.33333 which equates to red in hues between 0 and 1)
            byte h = (byte)((percentage / 3.0) * 255);
            byte s = (byte)(0.9 * 255);
            byte v = (byte)(0.9 * 255);
            System.Drawing.Color color = ColourSpaceConv.HSVtoColor(new HSV(h, s, v));
            return Color.FromRgb(color.R, color.G, color.B);
        }

        /// <summary>
        /// Fill the viewport with data.
        /// </summary>
        /// <param name="mapSections">Map sections.</param>
        private void OnActivate(List<SectionPair> mapSections)
        {
            double minimum = 0;
            double maximum = 0;

            List<MapSectionRenderData> renderData = FillRenderData(mapSections, ref maximum, ref minimum);

            if (renderData.Count == 0)
            {
                // No data, early-out.
                return;
            }

            double total = renderData.Sum<MapSectionRenderData>(rd => rd.EntityCount);
            double average = total / renderData.Count;
            double range = maximum - minimum;

            foreach (var data in renderData)
            {
                Color colour = Colors.White;

                double percentage = 1 - data.Density;
                colour = CalcColour(percentage);
                
                Viewport2DShape newGeometry = new Viewport2DShape(etCoordSpace.World, "Filled shape for section", data.MapPoints, Colors.Black, 1, colour);
                newGeometry.UserText = String.Format("{0} : {1} item(s)\n{2:0.00%} density", data.SectionName, data.EntityCount, data.Density);
                                
                Geometry.Add(newGeometry);
            }
        }

        #endregion
    }
}
