﻿using System;
using System.Collections.Generic;
using RSG.Base.Collections;
using RSG.Base.Math;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using RSG.Base.Windows.Helpers;
using RSG.Bounds;

namespace Workbench.AddIn.MapStatistics.Overlays.Bounds
{
    /// <summary>
    /// Enumeration of how we wish to match the collision type bit flags
    /// </summary>
    public enum MatchMode
    {
        Any,
        All
    }

    /// <summary>
    /// Shows an overlay of bounds extents for each streamable bounds.
    /// </summary>
    [ExportExtension(ExtensionPoints.BoundsStatisticsOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class BoundExtentsOverlay : BoundsOverlayBase
    {
        #region Constants
        private const string c_name = "Bound Extents";
        private const string c_description = "Shows an overlay of bounds extents for each streamable bounds.";
        #endregion

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<CollisionTypeHelper> CollisionTypes
        {
            get { return m_collisionTypes; }
            set
            {
                SetPropertyValue(value, () => CollisionTypes,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_collisionTypes = (ObservableCollection<CollisionTypeHelper>)newValue;
                        }
                ));
            }
        }
        private ObservableCollection<CollisionTypeHelper> m_collisionTypes;

        /// <summary>
        /// How the user wants to be matching the collision type bit flags
        /// </summary>
        public MatchMode MatchMode
        {
            get { return m_matchMode; }
            set
            {
                SetPropertyValue(value, () => MatchMode,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_matchMode = (MatchMode)newValue;
                        }
                ));
            }
        }
        private MatchMode m_matchMode;

        /// <summary>
        /// List of bound files that are loaded for the currently selected map section
        /// </summary>
        private List<BNDFile> BoundFiles
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default Constructor
        /// </summary>
        public BoundExtentsOverlay()
            : base(c_name, c_description)
        {
            m_matchMode = MatchMode.Any;
            BoundFiles = new List<BNDFile>();

            // Setup the collision types collection
            m_collisionTypes = new ObservableCollection<CollisionTypeHelper>();

            foreach (BNDFile.CollisionType type in Enum.GetValues(typeof(BNDFile.CollisionType)))
            {
                if (type != BNDFile.CollisionType.Default)
                {
                    CollisionTypes.Add(new CollisionTypeHelper(type));
                }
            }
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// Gets called when the user turns off this overlay
        /// </summary>
        public override void Deactivated()
        {
            BoundFiles.Clear();
            base.Deactivated();
        }

        /// <summary>
        /// Gets the control that should be shown in the overlay details panel
        /// </summary>
        public override System.Windows.Controls.Control GetOverlayControl()
        {
            return new BoundExtentsOverlayView();
        }

        /// <summary>
        /// Called when the selected map section changes
        /// </summary>
        protected override void OnSelectedMapSectionChanged()
        {
            // Clear out the existing data
            BoundFiles.Clear();

            // Make sure a map section was selected
            if (SelectedMapSection != null)
            {
                // Load in the relevant bounds files
                BoundFiles.AddRange(LoadBoundsDataForSection(SelectedMapSection));
            }

            // Update the overlay
            RefreshOverlay();
        }

        /// <summary>
        /// Function that is called when the user hits "Refresh Overlay" in the overlay details view
        /// </summary>
        protected override void RefreshOverlay()
        {
            // Update the overlay image
            OverlayImage.ClearImage();
            OverlayImage.BeginUpdate();

            BNDFile.CollisionType typeFlags = 0;
            foreach (CollisionTypeHelper helper in CollisionTypes)
            {
                if (helper.Enabled)
                {
                    typeFlags |= helper.Type;
                }
            }

            // Process all the bounds files
            foreach (BNDFile file in BoundFiles)
            {
                ProcessBoundObject(file.BoundObject, typeFlags);
            }

            OverlayImage.EndUpdate();

            // Kinda Hacky: Forces a collection changed notification to be sent which causes the UI to update
            Geometry.BeginUpdate();
            Geometry.EndUpdate();
        }
        #endregion // Overrides

        #region Private Methods
        /// <summary>
        /// Processes a single bound object
        /// </summary>
        /// <param name="obj"></param>
        private void ProcessBoundObject(BoundObject obj, BNDFile.CollisionType typeFlags)
        {
            if (obj is Composite)
            {
                Composite composite = (Composite)obj;

                foreach (CompositeChild child in composite.Children)
                {
                    if ((MatchMode == MatchMode.Any && (child.CollisionType & typeFlags) != 0) ||
                        (MatchMode == MatchMode.All && child.CollisionType == typeFlags))
                    {
                        ProcessBoundObject(child.BoundObject, typeFlags);
                    }
                }
            }
            else
            {
                // Convert the bound object to a BVH and then process
                BVH bvh = BoundObjectConverter.ToBVH(obj);
                ProcessBVH(bvh);
            }
        }

        /// <summary>
        /// Processes a single bvh
        /// </summary>
        /// <param name="bvh"></param>
        private void ProcessBVH(BVH bvh)
        {
            foreach (BVHPrimitive prim in bvh.Primitives)
            {
                if (prim is BVHTriangle)
                {
                    BVHTriangle tri = (BVHTriangle)prim;

                    Vector2f[] outline = new Vector2f[3];
                    outline[0] = new Vector2f(bvh.Verts[tri.Index0].Position.X, bvh.Verts[tri.Index0].Position.Y);
                    outline[1] = new Vector2f(bvh.Verts[tri.Index1].Position.X, bvh.Verts[tri.Index1].Position.Y);
                    outline[2] = new Vector2f(bvh.Verts[tri.Index2].Position.X, bvh.Verts[tri.Index2].Position.Y);

                    OverlayImage.RenderPolygon(outline, System.Drawing.Color.LightCoral);
                }
            }

            /*
            BoundingBox2f bbox = new BoundingBox2f();

            foreach (Vector3f pos in bvh.Verts)
            {
                Vector2f pos2d = new Vector2f(pos.X, pos.Y);
                bbox.Expand(pos2d);
            }

            Vector2f[] outline = new Vector2f[4];
            outline[0] = bbox.Min;
            outline[1] = new Vector2f(bbox.Max.X, bbox.Min.Y);
            outline[2] = bbox.Max;
            outline[3] = new Vector2f(bbox.Min.X, bbox.Max.Y);

            OverlayImage.RenderPolygon(outline, System.Drawing.Color.Red, 1);
            */
        }
        #endregion // Private Methods
    } // BoundExtentsOverlay
} // Workbench.AddIn.MapStatistics.Overlays.Bounds
