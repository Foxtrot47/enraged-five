﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Windows;
using RSG.Base.Editor;
using RSG.Base.Logging;
using Workbench.AddIn;
using Workbench.AddIn.Commands;
using Workbench.AddIn.Services;
using Workbench.AddIn.Services.File;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.UI.Menu;

namespace Workbench.UI.Menu
{
    /// <summary>
    /// File New menu item.
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.MenuFile, typeof(IWorkbenchCommand))]
    class FileNewMenuItem : WorkbenchMenuItemBase, IPartImportsSatisfiedNotification
    {
        #region MEF Imports
        /// <summary>
        /// Extensions service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ExtensionService)]
        private IExtensionService ExtensionService { get; set; }

        /// <summary>
        /// MEF import for workbench layout manager.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager,
            typeof(ILayoutManager))]
        private ILayoutManager LayoutManager { get; set; }

        /// <summary>
        /// MEF import for workbench configuration service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService,
            typeof(IConfigurationService))]
        private IConfigurationService ConfigurationService { get; set; }

        /// <summary>
        /// MEF import for workbench file new services exposed by plugins.
        /// </summary>
        [ImportManyExtension(Workbench.AddIn.ExtensionPoints.NewService,
            typeof(INewService), AllowRecomposition = true)]
        private IEnumerable<INewService> NewServices { get; set; }
        #endregion // MEF Imports

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public FileNewMenuItem()
        {
            this.Header = "_New";
            this.ID = new Guid(Workbench.AddIn.CompositionPoints.MenuFileNew);
        }
        #endregion // Constructor(s)

        #region IPartImportsSatisfiedNotification Methods
        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {
            List<IWorkbenchCommand> newItems = new List<IWorkbenchCommand>();
            Guid previousId = Guid.NewGuid();
            foreach (INewService ns in this.NewServices)
            {
                NewFileServiceMenuItem mi = new NewFileServiceMenuItem(ns, previousId, this.LayoutManager, this.ConfigurationService);
                if (ns.KeyGesture != null)
                    mi.KeyGesture = ns.KeyGesture;
                previousId = mi.ID;
                newItems.Add(mi);
            }
            if (0 == newItems.Count)
            {
                newItems.Add(new NewFileServiceMenuItem());
            }
            this.Items = ExtensionService.Sort(newItems);
        }
        #endregion // IPartImportsSatisfiedNotification Methods

        #region ICommand Implementation

        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
        }

        #endregion // ICommand Implementation
    }

    #region NewFileServiceMenuItem Class

    /// <summary>
    /// 
    /// </summary>
    class NewFileServiceMenuItem : WorkbenchMenuItemBase
    {
        #region Properties

        /// <summary>
        /// Associated INewService.
        /// </summary>
        private INewService Service;

        /// <summary>
        /// Layout manager for showing the results.
        /// </summary>
        private ILayoutManager LayoutManager;

        /// <summary>
        /// Configuration service for getting new filename.
        /// </summary>
        private IConfigurationService ConfigurationService;

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="os"></param>
        /// <param name="prevId"></param>
        /// <param name="lm"></param>
        public NewFileServiceMenuItem(INewService ns, Guid prevId, ILayoutManager lm, IConfigurationService cs)
        {
            this.Header = ns.Header;
            this.ID = Guid.NewGuid();
            this.RelativeID = prevId;
            this.Direction = Direction.After;
            this.Service = ns;
            this.ConfigurationService = cs;
            this.SetImageFromBitmap(ns.Image);
            this.LayoutManager = lm;
        }

        /// <summary>
        /// Default constructor (used to display no services available).
        /// </summary>
        public NewFileServiceMenuItem()
        {
            this.Header = "[No New Services Loaded]";
            this.ID = Guid.NewGuid();
            this.Service = null;
        }

        #endregion // Constructor(s)

        #region ICommand Implementation

        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return this.Service != null;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
            Log.Log__Debug("NewFileServiceMenuItem::Execute() {0}", this.Header);

            Object param = null;
            bool preResult = this.Service.PreNew(out param);
            if (!preResult)
            {
                Log.Log__Debug("NewService PreNew decided not to continue.");
                return;
            }

            IModel model = null;
            IDocumentBase document = null;
            String filename = this.ConfigurationService.GetNewFilename();
            bool newResult = this.Service.New(out document, out model, filename, param);
            Debug.Assert(newResult && null != document && null != model, String.Format("NewService {0} failed.", this.Service.ID));
            if (newResult)
            {
                Log.Log__Debug("NewService {0} successful.", this.Service.ID);
                if (this.LayoutManager != null)
                    this.LayoutManager.ShowDocument(document);
            }
            else
            {
                Log.Log__Error("NewService {0} failed.", this.Service.ID);
            }
        }

        #endregion // ICommand Implementation
    } // NewFileServiceMenuItem
    #endregion // OpenFileServiceMenuItem Class
} // Workbench.UI.Menu
