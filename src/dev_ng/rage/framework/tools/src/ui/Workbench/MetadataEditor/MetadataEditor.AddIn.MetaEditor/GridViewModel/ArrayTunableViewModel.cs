﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using MetadataEditor.AddIn.MetaEditor.ViewModel;
using RSG.Metadata.Data;
using RSG.Metadata.Parser;
using System.Text.RegularExpressions;
using MetadataEditor.AddIn.MetaEditor.BasicViewModel;

namespace MetadataEditor.AddIn.MetaEditor.GridViewModel
{
    /// <summary>
    /// The view model that wraps around a <see cref="ArrayTunable"/> object.
    /// </summary>
    public class ArrayTunableViewModel : GridTunableViewModel
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="EditCommand"/> property.
        /// </summary>
        private RelayCommand editCommand;

        /// <summary>
        /// Private dictionary used to as a lookup to the visibility of the array columns.
        /// </summary>
        private Dictionary<string, bool> visibility;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="MetadataEditor.AddIn.MetaEditor.GridViewModel.ArrayTunableViewModel"/>
        /// class.
        /// </summary>
        public ArrayTunableViewModel(GridTunableViewModel parent, ArrayTunable m, MetaFileViewModel root)
            : base(parent, m, root)
        {
            this.Children.CollectionChanged += this.ChildrenChanged;
            foreach (ITunable t in m)
            {
                if (t.Definition.HideWidgets == true)
                {
                    continue;
                }

                GridTunableViewModel vm = TunableViewModelFactory.Create(this, t, root);
                this.Children.Add(vm);
            }

            if (m.Length == 0)
            {
                this.ChildrenChanged(null, null);
            }

            ArrayMember member = m.Definition as ArrayMember;
            visibility = new Dictionary<string, bool>();
            if (member == null)
            {
                return;
            }

            IMember elementType = member.ElementType as IMember;
            Structure structure = null;
            if (elementType is PointerMember)
            {
                PointerMember pointerMember = elementType as PointerMember;
                structure = pointerMember.ObjectType;
            }
            else if (elementType is StructMember)
            {
                structure = (elementType as StructMember).Definition;
            }

            if (structure != null)
            {
                int index = 0;
                foreach (var kvp in structure.Members)
                {
                    visibility.Add(kvp.Key, true);
                    sortedMembers.Add(kvp.Key, new KeyValuePair<int, IMember>(index, kvp.Value));
                    index++;
                }

                this.contextMenu = new ContextMenu();
                foreach (var kvp in structure.Members)
                {
                    MenuItem item = new MenuItem
                    {
                        Header = kvp.Key,
                        IsCheckable = true,
                        IsChecked = visibility[kvp.Key],
                    };

                    item.Click += new RoutedEventHandler(this.OnVisibilityItemClicked);
                    this.contextMenu.Items.Add(item);
                }
            }
        }
        #endregion

        #region Properties
        ContextMenu contextMenu;
        Dictionary<string, KeyValuePair<int, IMember>> sortedMembers = new Dictionary<string, KeyValuePair<int, IMember>>();
        Dictionary<string, DataGridColumn> currentColumns = new Dictionary<string, DataGridColumn>();

        /// <summary>
        /// Columns to display in the table.
        /// </summary>
        public Collection<DataGridColumn> Columns
        {
            get
            {
                Collection<DataGridColumn> columns = new Collection<DataGridColumn>();
                currentColumns.Clear();
                foreach (var columnMember in sortedMembers)
                {
                    if (this.visibility[columnMember.Key])
                    {
                        KeyValuePair<string, IMember> kvp = new KeyValuePair<string, IMember>(columnMember.Key, columnMember.Value.Value);
                        DataGridColumn column = CreateColumn(kvp, columnMember.Value.Key);
                        if (column != null)
                        {
                            columns.Add(column);
                            currentColumns.Add(columnMember.Key, column);
                        }
                    }
                }

                return columns;
            }
        }

        void OnVisibilityItemClicked(object sender, RoutedEventArgs e)
        {
            string key = (sender as MenuItem).Header as string;
            bool current = this.visibility[key];
            this.visibility[key] = !current;

            DataGridColumn anyColumn = this.currentColumns.Values.FirstOrDefault();
            if (anyColumn == null)
            {
                return;
            }

            DataGrid grid = anyColumn.GetValue(FrameworkElement.TagProperty) as DataGrid;
            bool visible = !current;
            if (visible)
            {
                KeyValuePair<int, IMember> columnMember = sortedMembers[key];
                KeyValuePair<string, IMember> kvp = new KeyValuePair<string, IMember>(key, columnMember.Value);

                DataGridColumn newColumn = this.CreateColumn(kvp, columnMember.Key);
                newColumn.SetValue(FrameworkElement.TagProperty, grid);
                int insertIndex = columnMember.Key;
                int originalIndex = columnMember.Key;
                foreach (KeyValuePair<string, KeyValuePair<int, IMember>> memberCache in this.sortedMembers)
                {
                    if (memberCache.Value.Key < originalIndex)
                    {
                        if (!visibility[memberCache.Key])
                        {
                            insertIndex--;
                        }
                    }
                }
                
                grid.Columns.Insert(insertIndex, newColumn);
                this.currentColumns.Add(key, newColumn);
            }
            else
            {
                DataGridColumn column = this.currentColumns[key];
                grid.Columns.Remove(column);
                this.currentColumns.Remove((sender as MenuItem).Header as string);
            }
        }

        /// <summary>
        /// Gets the command used to edit the array by drilling down into it.
        /// </summary>
        public RelayCommand EditCommand
        {
            get
            {
                if (this.editCommand == null)
                {
                    this.editCommand = new RelayCommand(this.OnEdit);
                }

                return this.editCommand;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Occurs whenever the children for this array change.
        /// </summary>
        /// <param name="sender">
        /// The child collection this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Collections.Specialized.NotifyCollectionChangedEventArgs data for
        /// this event.
        /// </param>
        private void ChildrenChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            IMember elementType = (this.Model.Definition as ArrayMember).ElementType;
            string count = this.Children.Count.ToString();
            if (elementType is PointerMember)
            {
                string elementName = (elementType as PointerMember).ObjectTypeName;
                this.FriendlyTypeName = string.Format("{0}*[{1}]", elementName, count);
            }
            else
            {
                string elementName = elementType.FriendlyTypeName;
                this.FriendlyTypeName = string.Format("{0}[{1}]", elementName, count);
            }
        }

        /// <summary>
        /// Find the next item that matches the expression.
        /// </summary>
        /// <param name="expression">Expression.</param>
        /// <returns>Matched item.</returns>
        internal override ITunableViewModel FindNextInChildren(Regex expression)
        {
            foreach(var child in Children)
            {
                if (child is StructureTunableViewModel)
                {
                    StructureTunableViewModel stvm = child as StructureTunableViewModel;
                    foreach (var stvmChild in stvm.Children)
                    {
                        if (stvmChild.IsMatch(expression))
                        {
                            return stvmChild;
                        }
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        private void OnEdit(object parameter)
        {
            if (this.RootViewModel == null)
            {
                return;
            }

            if (this.RootViewModel.BreadCrumbs.Contains(this))
            {
                return;
            }

            this.RootViewModel.BreadCrumbs.Add(this);
            this.RootViewModel.BreadCrumbSelection = this;
        }

        private DataGridColumn CreateColumn(KeyValuePair<string, IMember> member, int index)
        {
            ArrayMember arrayMember = (this.Model as ArrayTunable).Definition as ArrayMember;
            if (arrayMember == null)
            {
                return null;
            }

            IMember elementType = arrayMember.ElementType as IMember;


            DataGridTemplateColumn memberColumn = new DataGridTemplateColumn();
            memberColumn.IsReadOnly = true;
            if (member.Value is StructMember)
            {
                memberColumn.Width = DataGridLength.SizeToHeader;
            }
            else
            {
                memberColumn.Width = DataGridLength.Auto;
            }

            TextBlock header = new TextBlock();
            header.Text = member.Key;
            header.ContextMenu = this.contextMenu;
            memberColumn.Header = header;

            DataTemplate cbdt = new DataTemplate();
            FrameworkElementFactory cbFactory = new FrameworkElementFactory(typeof(ContentControl));
            Binding binding = null;
            if (elementType is PointerMember)
            {
                binding = new Binding(string.Format("Value.Children[{0}]", index));
            }
            else
            {
                binding = new Binding(string.Format("Children[{0}]", index));
            }

            cbFactory.SetBinding(ContentControl.ContentProperty, binding);
            cbdt.VisualTree = cbFactory;

            memberColumn.CellTemplate = cbdt;
            return memberColumn;
        }
        #endregion
    } // MetadataEditor.AddIn.MetaEditor.GridViewModel.ArrayTunableViewModel
} // MetadataEditor.AddIn.MetaEditor.GridViewModel
