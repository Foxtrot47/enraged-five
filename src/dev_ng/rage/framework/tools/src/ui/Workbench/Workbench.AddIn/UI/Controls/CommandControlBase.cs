﻿using System;
using System.Collections.Generic;
using System.Windows;
using Workbench.AddIn.Services;

namespace Workbench.AddIn.UI.Controls
{

    /// <summary>
    /// Base CommandControl class.
    /// </summary>
    public abstract class CommandControlBase :
        ControlBase,
        ICommandControl
    {
        #region MEF Imports
        /// <summary>
        /// MEF import for message box service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.UserInterfaceService,
            typeof(Workbench.AddIn.Services.IUserInterfaceService))]
        protected IUserInterfaceService UserInterfaceService { get; set; }
        #endregion // MEF Imports

        #region ICommand Interface
        
        /// <summary>
        /// 
        /// </summary>
        public event EventHandler CanExecuteChanged 
        { 
            add { System.Windows.Input.CommandManager.RequerySuggested += value; }
            remove { System.Windows.Input.CommandManager.RequerySuggested -= value; }
        } 

        /// <summary>
        /// 
        /// </summary>
        public abstract Boolean CanExecute(Object parameter);

        /// <summary>
        /// Command execute method.
        /// </summary>
        public abstract void Execute(Object parameter);

        #endregion // ICommand Interface
    }

} // Workbench.AddIn.UI.Controls namespace
