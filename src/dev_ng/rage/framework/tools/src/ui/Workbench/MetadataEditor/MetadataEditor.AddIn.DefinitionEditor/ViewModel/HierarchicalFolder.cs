﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using SIO = System.IO;
using System.Windows.Media.Imaging;
using RSG.Metadata.Model;
using RSG.Metadata.Parser;
using RSG.Base.Editor;

namespace MetadataEditor.AddIn.DefinitionEditor.ViewModel
{
    /// <summary>
    /// 
    /// </summary>
    internal class HierarchicalFolder :
        RSG.Base.Editor.HierarchicalViewModelBase
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// 
        /// </summary>
        public bool Selected 
        {
            get { return m_selected; }
            set { m_selected = value; }
        }
        private bool m_selected;

        /// <summary>
        /// 
        /// </summary>
        public bool Expanded
        {
            get { return m_expanded; }
            set { m_expanded = value; }
        }
        private bool m_expanded;

        /// <summary>
        /// 
        /// </summary>
        public String Path { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public String Alias { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public new ObservableCollection<Object> Children { get; set; }
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="defDict"></param>
        public HierarchicalFolder(String path, IViewModel parent)
        {
            String[] parts = path.Split(
                new char[] { SIO.Path.DirectorySeparatorChar, SIO.Path.AltDirectorySeparatorChar },
                StringSplitOptions.RemoveEmptyEntries);
            String alias = parts[parts.Length - 1];
            Initialise(path, alias);
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="alias"></param>
        /// <param name="defDict"></param>
        public HierarchicalFolder(String path, String alias, IViewModel parent)
        {
            Initialise(path, alias);
        }
        #endregion // Constructor(s)

        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(Object obj)
        {
            if (!(obj is HierarchicalFolder))
                return (false);
            HierarchicalFolder other = (obj as HierarchicalFolder);
            return (0 == String.Compare(this.Path, other.Path, true));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public HierarchicalFolder FindFolder(String path)
        {
            // Base case.
            if (0 == String.Compare(Path, path))
                return (this);

            foreach (Object item in this.Children)
            {
                if (!(item is HierarchicalFolder))
                    continue;

                HierarchicalFolder folder = (item as HierarchicalFolder);
                HierarchicalFolder found = (folder.FindFolder(path));
                if (null != found)
                    return (found);
            }

            // Not found.
            return (null);
        }

        /// <summary>
        /// Create root HierarchicalFolder object for path.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static HierarchicalFolder Create(String path, StructureDictionary defDict)
        {
            HierarchicalFolder root = new HierarchicalFolder(path, path, null);

            if (SIO.Directory.Exists(path))
            {
                IEnumerable<String> dirsEnumerable = SIO.Directory.EnumerateDirectories(
                    path, "*", SIO.SearchOption.AllDirectories);
                List<String> dirs = new List<String>(dirsEnumerable);
                dirs.Sort();
                foreach (String dir in dirs)
                {
                    HierarchicalFolder parent = root;
                    String[] relativeParts = dir.Replace(path, "").Split(
                        new char[] { SIO.Path.DirectorySeparatorChar, SIO.Path.AltDirectorySeparatorChar },
                        StringSplitOptions.RemoveEmptyEntries);

                    foreach (String part in relativeParts)
                    {
                        HierarchicalFolder child = new HierarchicalFolder(SIO.Path.Combine(parent.Path, part), parent);
                        if (!parent.Children.Contains(child))
                        {
                            parent.Children.Add(child);
                            parent = child;
                        }
                        else
                        {
                            int index = parent.Children.IndexOf(child);
                            parent = (parent.Children[index] as HierarchicalFolder);
                        }
                    }
                }

                // Loop through adding to the relevant HierarchicalFolder.
                List<Structure> baseStructures = GetBaseStructures(defDict);
                foreach (Structure model in baseStructures)
                {
                    if (!String.IsNullOrEmpty(model.Filename))
                    {
                        String pathname = SIO.Path.GetDirectoryName(model.Filename);
                        Uri uri = new Uri(pathname);
                        HierarchicalFolder folder = root.FindFolder(SIO.Path.GetFullPath(uri.AbsolutePath));
                        if (folder == null)
                        {
                            RSG.Base.Logging.Log.Log__Error("Unable to find folder in definition explorer with filename " + pathname);
                            continue;
                        }
                        StructureViewModel vm = new StructureViewModel(model, folder);
                        folder.Children.Add(vm);
                    }
                }
            }
            return (root);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <param name="alias"></param>
        /// <param name="defDict"></param>
        private void Initialise(String path, String alias)
        {
            this.Children = new ObservableCollection<Object>();
            this.Path = (path.Clone() as String);
            this.Alias = (alias.Clone() as String);

            this.Selected = false;
            this.Expanded = false;
        }

        private static List<Structure> GetBaseStructures(StructureDictionary defDict)
        {
           return new List<Structure>(defDict.Values.Where(s => string.IsNullOrWhiteSpace(s.BaseDataType)));
        }
            
        #endregion // Controller Methods
    } // HierarchicalFolder
} // MetadataEditor.AddIn.DefinitionEditorViewModel.ViewModel namespace
