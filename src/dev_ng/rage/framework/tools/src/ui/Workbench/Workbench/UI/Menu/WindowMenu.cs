﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel.Composition;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using RSG.Base.Logging;
using Workbench.AddIn;
using Workbench.AddIn.Commands;
using Workbench.AddIn.Services;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.UI.Menu;

namespace Workbench.UI.Menu
{
    /// <summary>
    /// Workbench 'Window' menu.
    /// </summary>
    /// The workbench Window menu has some common tool and document window
    /// operations.
    /// 
    /// The workbench Window menu includes a dynamically constructed list of
    /// open IDocument windows in the interface.  These can be clicked on to
    /// bring focus to that document window.
    /// 
    [ExportExtension(Workbench.AddIn.ExtensionPoints.MenuMain, typeof(IWorkbenchCommand))]
    class WindowMenu : WorkbenchMenuItemBase, IPartImportsSatisfiedNotification
    {
        #region MEF Imports
        /// <summary>
        /// Extensions service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ExtensionService)]
        private IExtensionService ExtensionService { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager,
            typeof(ILayoutManager))]
        private ILayoutManager LayoutManager { get; set; }

        /// <summary>
        /// Window menu subitems.
        /// </summary>
        [ImportManyExtension(Workbench.AddIn.ExtensionPoints.MenuWindow,
            typeof(IWorkbenchCommand))]
        private IEnumerable<IWorkbenchCommand> items { get; set; }
        #endregion // MEF Imports

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public WindowMenu()
        {
            this.Header = "_Window";
            ID = new Guid(Workbench.AddIn.ExtensionPoints.MenuWindow);
            RelativeID = new Guid(Workbench.AddIn.ExtensionPoints.MenuHelp);
            Direction = Direction.Before;
        }
        #endregion // Constructor(s)

        #region IPartImportsSatisfiedNotification Methods
        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {
            this.Items = ExtensionService.Sort(items);
        }
        #endregion // IPartImportsSatisfiedNotification Methods

        #region Submenu Methods

        /// <summary>
        /// 
        /// </summary>
        protected override void OnIsSubmenuOpenChanged()
        {
            if (!IsSubmenuOpen)
                return;

            if (LayoutManager.Documents.Count > 0)
            {
                List<IWorkbenchCommand> newItems = new List<IWorkbenchCommand>();
                Guid previousId = Guid.Empty;
                foreach (IDocumentBase doc in LayoutManager.Documents)
                {
                    DocumentMenuItem docMenuItem = new DocumentMenuItem(doc, previousId, LayoutManager);
                    previousId = docMenuItem.ID;
                    newItems.Add(docMenuItem);
                }
                Items = ExtensionService.SortAndJoin(items, new WorkbenchCommandSeparator(), newItems);
            }
            else
            {
                List<IWorkbenchCommand> newItems = new List<IWorkbenchCommand>();
                DocumentMenuItem emptyMenuItem = new DocumentMenuItem("[No Open Documents]");
                newItems.Add(emptyMenuItem);
                Items = ExtensionService.SortAndJoin(items, new WorkbenchCommandSeparator(), newItems);
            }
        }

        #endregion // Submenu Methods

        #region ICommand Implementation

        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
        }

        #endregion // ICommand Implementation
    } // WindowMenu

    #region DocumentMenuItem Class

    /// <summary>
    /// DocumentMenuItem represents a IDocument and has a checked status.
    /// </summary>
    class DocumentMenuItem : WorkbenchMenuItemBase
    {
        #region Member Data

        private ILayoutManager LayoutManager;
        private IDocumentBase Document;

        #endregion // Member Data

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public DocumentMenuItem(IDocumentBase document, Guid previousId, ILayoutManager layoutManager)
        {
            this.Header = document.Title;
            this.ID = Guid.NewGuid();
            if (previousId != Guid.Empty)
            {
                this.RelativeID = previousId;
                this.Direction = Direction.After;
            }
            this.IsCheckable = true;
            this.IsChecked = (layoutManager.IsActiveDocument(document));
            this.LayoutManager = layoutManager;
            this.Document = document;
        }

        /// <summary>
        /// 
        /// </summary>
        public DocumentMenuItem(String header)
        {
            this.Header = header;
        }

        #endregion // Constructor(s)

        #region CommandControl Methods

        /// <summary>
        /// 
        /// </summary>
        public override bool CanExecute(Object parameter)
        {
            return (this.Document != null);
        }

        /// <summary>
        /// 
        /// </summary>
        public override void Execute(Object parameter)
        {
            Log.Log__Debug("DocumentMenuItem::Execute()");

            if (this.Document != null)
            {
                LayoutManager.ShowDocument(this.Document);
            }
        }

        #endregion // CommandControl Methods
    } // DocumentMenuItem

    #endregion // DocumentMenuItem Class

    #region Generic Window Operation Menu Classes

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.MenuWindow, typeof(IWorkbenchCommand))]
    class HorizontalTabMenuItem : WorkbenchMenuItemBase
    {
        #region Constants
        public static readonly Guid GUID =
            new Guid("D96A026D-9F01-4A8A-8988-14C1D662B385");
        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager,
            typeof(ILayoutManager))]
        private ILayoutManager LayoutManager { get; set; }
        #endregion // MEF Imports

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public HorizontalTabMenuItem()
        {
            this.Header = "New Hori_zontal Tab Group";
            this.ID = GUID;
            SetImageFromBitmap(Resources.Images.application_tile_horizontal);
        }
        #endregion // Constructor(s)

        #region IMenuItem Interface

        public override bool CanExecute(Object parameter)
        {
            IDocumentBase active = this.LayoutManager.ActiveDocument();
            return active != null && active.ContainerPane != null && active.ContainerPane.Items.Count >= 2;
        }

        /// <summary>
        /// 
        /// </summary>
        public override void Execute(Object parameter)
        {
            IDocumentBase document = this.LayoutManager.ActiveDocument();
            this.LayoutManager.NewHorizontalTabGroup(document);
        }

        #endregion // IMenuItem Interface
    } // HorizontalTabMenuItem

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.MenuWindow, typeof(IWorkbenchCommand))]
    class VerticalTabMenuItem : WorkbenchMenuItemBase
    {
        #region Constants
        public static readonly Guid GUID =
            new Guid("28DE6B8E-8CE4-4EAC-B5EC-B753BFE95720");
        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager,
            typeof(ILayoutManager))]
        private ILayoutManager LayoutManager { get; set; }
        #endregion // MEF Imports

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public VerticalTabMenuItem()
        {
            this.Header = "New _Vertical Tab Group";
            this.ID = GUID;
            this.RelativeID = HorizontalTabMenuItem.GUID;
            this.Direction = Direction.After;
            SetImageFromBitmap(Resources.Images.application_tile_vertical);
        }
        #endregion // Constructor(s)

        #region IMenuItem Interface

        public override bool CanExecute(Object parameter)
        {
            IDocumentBase active = this.LayoutManager.ActiveDocument();
            return active != null && active.ContainerPane != null && active.ContainerPane.Items.Count >= 2;
        }

        /// <summary>
        /// 
        /// </summary>
        public override void Execute(Object parameter)
        {
            IDocumentBase document = this.LayoutManager.ActiveDocument();
            this.LayoutManager.NewVerticalTabGroup(document);
        }

        #endregion // IMenuItem Interface
    } // VerticalTabMenuItem

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.MenuWindow, typeof(IWorkbenchCommand))]
    class CloseAllWindowsMenuItem : WorkbenchMenuItemBase
    {
        #region Constants
        public static readonly Guid GUID = 
            new Guid("686C3E9B-AA4C-454A-BF62-0DC26B12E110");
        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager,
            typeof(ILayoutManager))]
        private ILayoutManager LayoutManager { get; set; }
        #endregion // MEF Imports

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public CloseAllWindowsMenuItem()
        {
            this.Header = "C_lose All Documents";
            this.ID = GUID;
            this.RelativeID = VerticalTabMenuItem.GUID;
            this.Direction = Direction.After;
        }
        #endregion // Constructor(s)

        #region ICommand Implementation

        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
            Log.Log__Debug("CloseAllWindowsMenuItem::Execute()");
            this.LayoutManager.CloseAllDocuments();
        }

        #endregion // ICommand Implementation
    } // CloseAllWindowsMenuItem
    #endregion // Generic Window Operation Menu Classes

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.MenuWindow, typeof(IWorkbenchCommand))]
    class WindowSep1MenuItem : WorkbenchCommandSeparator
    {
        #region Constants
        public static readonly Guid GUID = 
            new Guid("F7BD38AF-E576-4E79-9BB3-E2C425A1A1FA");
        #endregion // Constants

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public WindowSep1MenuItem()
        {
            this.ID = GUID;
            this.IsSeparator = true;
            this.RelativeID = CloseAllWindowsMenuItem.GUID;
            this.Direction = Direction.After;
        }

        #endregion // Constructor(s)

        #region Public Members

        public void SetPlacement(Guid relative, Direction direction)
        {
            this.RelativeID = relative;
            this.Direction = direction;
        }

        #endregion // Public Members
    } // WindowSep1MenuItem

    /// <summary>
    /// 
    /// </summary>
    class SavedLayoutMenuItem : WorkbenchMenuItemBase
    {
        #region Properties

        private String LayoutName
        {
            get;
            set;
        }

        private String LayoutBlob
        {
            get;
            set;
        }

        private ILayoutManager LayoutManager
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public SavedLayoutMenuItem(String name, String blob, Guid guid, ILayoutManager layoutManager)
        {
            this.Header = name;
            this.ID = guid;
            this.LayoutName = name;
            this.LayoutBlob = blob;
            this.LayoutManager = layoutManager;
            this.IsCheckable = true;
        }
        #endregion // Constructor(s)

        #region ICommand Implementation

        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
            Log.Log__Debug("SavedLayoutMenuItem::Execute()");
            this.LayoutManager.RestoreLayout(this.LayoutBlob);
            Properties.Settings.Default.LastActiveLayout = this.LayoutName;
            Properties.Settings.Default.Save();
        }

        #endregion // ICommand Implementation

        #region Public Members

        public void SetPlacement(Guid relative, Direction direction)
        {
            this.RelativeID = relative;
            this.Direction = direction;
        }

        #endregion // Public Members
    } // SavedLayoutMenuItem

    /// <summary>
    /// Window -> Saved Layouts -> Override Current Layout
    /// </summary>
    class LayoutOverrideMenuItem : WorkbenchMenuItemBase
    {
        #region Constants
        public static readonly Guid GUID =
            new Guid("6A46C67B-F736-4E75-95D9-3F013FF16382");
        #endregion // Constants

        #region Properties

        private ILayoutManager LayoutManager
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public LayoutOverrideMenuItem(ILayoutManager layoutManager)
        {
            this.Header = "Override Current Layout";
            this.ID = GUID;
            this.LayoutManager = layoutManager;
            this.RelativeID = WindowSep1MenuItem.GUID;
            this.Direction = AddIn.Services.Direction.After;
        }
        #endregion // Constructor(s)

        #region ICommand Implementation

        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            String currentLayoutName = Properties.Settings.Default.LastActiveLayout;
            foreach (String layoutName in this.LayoutManager.SavedLayouts.Keys)
            {
                if (currentLayoutName == layoutName)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
            Log.Log__Debug("LayoutOverrideMenuItem::Execute()");
            this.LayoutManager.SaveLayout(Properties.Settings.Default.LastActiveLayout);
        }

        #endregion // ICommand Implementation
    } // LayoutOverrideMenuItem

    /// <summary>
    /// Window -> Saved Layouts -> Save Current Layout As...
    /// </summary>
    class LayoutSaveAsMenuItem : WorkbenchMenuItemBase
    {
        #region Constants
        public static readonly Guid GUID =
            new Guid("5FC46AB4-3353-45FD-A2AF-5B9FC90DF495");
        #endregion // Constants

        #region Properties

        private ILayoutManager LayoutManager
        {
            get;
            set;
        }

        private WindowSavedLayoutsMenuItem BaseMenuItem
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public LayoutSaveAsMenuItem(ILayoutManager layoutManager, WindowSavedLayoutsMenuItem baseMenu)
        {
            this.Header = "Save Current Layout As...";
            this.ID = GUID;
            this.LayoutManager = layoutManager;
            this.RelativeID = LayoutOverrideMenuItem.GUID;
            this.Direction = AddIn.Services.Direction.After;
            this.BaseMenuItem = baseMenu;
        }
        #endregion // Constructor(s)

        #region ICommand Implementation

        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
            Log.Log__Debug("LayoutSaveAsMenuItem::Execute()");
            // Create a new layout
            SavedLayoutName dialog = new SavedLayoutName();
            dialog.LayoutManager = this.LayoutManager;
            dialog.Owner = Application.Current.MainWindow;
            if ((Boolean)dialog.ShowDialog())
            {
                String newName = dialog.LayoutName;
                this.LayoutManager.SaveLayout(newName);
                Properties.Settings.Default.LastActiveLayout = newName;
                Properties.Settings.Default.Save();
                this.BaseMenuItem.RefreshSavedLayouts();
            }
        }

        #endregion // ICommand Implementation
    } // LayoutSaveAsMenuItem

    /// <summary>
    /// Window -> Saved Layouts -> Delete Current Layout
    /// </summary>
    class LayoutDeleteMenuItem : WorkbenchMenuItemBase
    {
        #region Constants
        public static readonly Guid GUID =
            new Guid("4444AFE2-781E-4FA9-86A9-1F29B205BA8C");
        #endregion // Constants

        #region Properties

        private ILayoutManager LayoutManager
        {
            get;
            set;
        }

        private WindowSavedLayoutsMenuItem BaseMenuItem
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public LayoutDeleteMenuItem(ILayoutManager layoutManager, WindowSavedLayoutsMenuItem baseMenu)
        {
            this.Header = "Delete Current Layout";
            this.ID = GUID;
            this.LayoutManager = layoutManager;
            this.RelativeID = LayoutSaveAsMenuItem.GUID;
            this.Direction = AddIn.Services.Direction.After;
            this.BaseMenuItem = baseMenu;
        }
        #endregion // Constructor(s)

        #region ICommand Implementation

        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            String currentLayoutName = Properties.Settings.Default.LastActiveLayout;
            foreach (String layoutName in this.LayoutManager.SavedLayouts.Keys)
            {
                if (currentLayoutName == layoutName)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
            Log.Log__Debug("LayoutDeleteMenuItem::Execute()");
            String currentLayoutName = Properties.Settings.Default.LastActiveLayout;
            if (this.LayoutManager.SavedLayouts.ContainsKey(currentLayoutName))
            {
                this.LayoutManager.SavedLayouts.Remove(currentLayoutName);
                this.LayoutManager.SaveLayouts();
                Properties.Settings.Default.LastActiveLayout = Workbench.UI.Layout.LayoutManager.DefaultLayoutID;
                Properties.Settings.Default.Save();
                this.BaseMenuItem.RefreshSavedLayouts();
            }
        }

        #endregion // ICommand Implementation
    } // LayoutDeleteMenuItem

    /// <summary>
    /// Window -> Saved Layouts
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.MenuWindow, typeof(IWorkbenchCommand))]
    class WindowSavedLayoutsMenuItem : WorkbenchMenuItemBase, IPartImportsSatisfiedNotification
    {
        #region Constants
        public static readonly Guid GUID =
            new Guid("C2F340A5-5DBF-41FF-99AF-B16051E93C8B");
        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager,
            typeof(ILayoutManager))]
        private ILayoutManager LayoutManager { get; set; }

        /// <summary>
        /// Generic extension service reference.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ExtensionService, typeof(IExtensionService))]
        private IExtensionService ExtensionService { get; set; }

        /// <summary>
        /// MEF Import for configuration service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(IConfigurationService))]
        public IConfigurationService ConfigService { get; set; }
        #endregion // MEF Imports

        #region Properties

        Dictionary<String, String> SavedLayouts = new Dictionary<String, String>();

        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public WindowSavedLayoutsMenuItem()
        {
            this.Header = "Saved Layout";
            this.ID = GUID;
            this.RelativeID = WindowSep1MenuItem.GUID;
            this.Direction = Direction.After;
        }
        #endregion // Constructor(s)

        #region Submenu Methods

        /// <summary>
        /// 
        /// </summary>
        protected override void OnIsSubmenuOpenChanged()
        {
            if (!IsSubmenuOpen)
                return;

            String layoutName = Properties.Settings.Default.LastActiveLayout;
            foreach (IWorkbenchCommand item in this.Items)
            {
                if (!(item is SavedLayoutMenuItem))
                    continue;

                if (item.Header != layoutName)
                {
                    item.IsChecked = false;
                }
                else
                {
                    item.IsChecked = true;
                }
            }
        }

        #endregion // Submenu Methods

        #region IPartImportsSatisfiedNotification Methods

        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {
            this.RefreshSavedLayouts();
        }

        #endregion // IPartImportsSatisfiedNotification Methods

        #region ICommand Implementation

        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
            Log.Log__Debug("WindowResetLayoutMenuItem::Execute()");
            this.LayoutManager.SaveLayout("OnlyOne");
            Properties.Settings.Default.LastActiveLayout = "OnlyOne";
            Properties.Settings.Default.Save();
        }

        #endregion // ICommand Implementation

        #region Public Functions

        public void RefreshSavedLayouts()
        {
            List<IWorkbenchCommand> items = new List<IWorkbenchCommand>();
            Guid previousGuid = Guid.Empty;
            string globalFile = Path.Combine(ConfigService.Config.ToolsBin, "workbench", "GlobalLayouts.xml");
            List<LayoutOptionViewModel> globalViews = WorkbenchViewModel.GetLayoutOptionViewModels(globalFile, this.LayoutManager);
            foreach (LayoutOptionViewModel globalView in globalViews)
            {
                Guid newGuid = Guid.NewGuid();
                SavedLayoutMenuItem newItem = new SavedLayoutMenuItem(globalView.Name, globalView.Xml, newGuid, LayoutManager);
                if (previousGuid != Guid.Empty)
                {
                    newItem.SetPlacement(previousGuid, AddIn.Services.Direction.After);
                }

                items.Add(newItem);
                previousGuid = newGuid;
            }

            if (this.LayoutManager.SavedLayouts.Count > 0)
            {
                WindowSep1MenuItem separator = new WindowSep1MenuItem();
                separator.SetPlacement(previousGuid, Direction.After);
                items.Add(separator);
            }

            items.Add(new LayoutOverrideMenuItem(this.LayoutManager));
            items.Add(new LayoutSaveAsMenuItem(this.LayoutManager, this));
            items.Add(new LayoutDeleteMenuItem(this.LayoutManager, this));
            Items = ExtensionService.Sort(items);
        }

        #endregion // Public Functions
    } // WindowSavedLayoutsMenuItem

    /// <summary>
    /// Window -> Reset Window Layout
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.MenuWindow, typeof(IWorkbenchCommand))]
    class WindowResetLayoutMenuItem : WorkbenchMenuItemBase
    {
        #region Constants

        public static readonly Guid GUID = new Guid("0534F604-8FF5-4B9F-9EAC-FA20858CD4CA");

        #endregion // Constants

        #region MEF Imports

        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager, typeof(ILayoutManager))]
        private ILayoutManager LayoutManager { get; set; }

        #endregion // MEF Imports

        #region Constructor(s)

        /// <summary>
        /// Reset window layout to the default value
        /// </summary>
        public WindowResetLayoutMenuItem()
        {
            this.Header = "Reset Window Layout";
            this.ID = GUID;
            this.RelativeID = WindowSavedLayoutsMenuItem.GUID;
            this.Direction = Direction.After;
        }

        #endregion // Constructor(s)

        #region ICommand Implementation

        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
            Log.Log__Debug("WindowResetLayoutMenuItem::Execute()");
            this.LayoutManager.RestoreLayout(Workbench.UI.Layout.LayoutManager.DefaultLayout);
            Properties.Settings.Default.LastActiveLayout = Workbench.UI.Layout.LayoutManager.DefaultLayoutID;
            Properties.Settings.Default.Save();
        }

        #endregion // ICommand Implementation
    } // WindowResetLayoutMenuItem

    /// <summary>
    /// Window -> Clear Window Layout
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.MenuWindow, typeof(IWorkbenchCommand))]
    class WindowClearLayoutMenuItem : WorkbenchMenuItemBase
    {
        #region Constants

        public static readonly Guid GUID = new Guid("E916EF0A-C1ED-4113-934C-61360B70B73F");

        #endregion // Constants

        #region MEF Imports

        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager, typeof(ILayoutManager))]
        private ILayoutManager LayoutManager { get; set; }

        #endregion // MEF Imports

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public WindowClearLayoutMenuItem()
        {
            this.Header = "Clear Window Layout";
            this.ID = GUID;
            this.RelativeID = WindowResetLayoutMenuItem.GUID;
            this.Direction = Direction.After;
        }

        #endregion // Constructor(s)

        #region ICommand Implementation

        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
            Log.Log__Debug("WindowClearLayoutMenuItem::Execute()");
            this.LayoutManager.RestoreLayout(Workbench.UI.Layout.LayoutManager.ClearedLayout);
            Properties.Settings.Default.LastActiveLayout = Workbench.UI.Layout.LayoutManager.ClearedLayoutID;
            Properties.Settings.Default.Save();
        }

        #endregion // ICommand Implementation
    } // WindowClearLayoutMenuItem

    /// <summary>
    /// Window -> Clear Window Layout
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.MenuWindow, typeof(IWorkbenchCommand))]
    class LayoutPickerLayoutMenuItem : WorkbenchMenuItemBase
    {
        #region Constants

        public static readonly Guid GUID = new Guid("A40BFC58-15D3-4A7C-840F-8DEC7F7C4F7F");

        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager, typeof(ILayoutManager))]
        private ILayoutManager LayoutManager { get; set; }

        /// <summary>
        /// MEF Import for configuration service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(IConfigurationService))]
        public IConfigurationService ConfigService { get; set; }
        #endregion // MEF Imports

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public LayoutPickerLayoutMenuItem()
        {
            this.Header = "Startup Layout Picker";
            this.ID = GUID;
            this.RelativeID = WindowClearLayoutMenuItem.GUID;
            this.Direction = Direction.After;
        }

        #endregion // Constructor(s)

        #region ICommand Implementation

        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
            string globalFile = Path.Combine(ConfigService.Config.ToolsConfig, "workbench", "GlobalLayouts.xml");
            List<LayoutOptionViewModel> globalViews = WorkbenchViewModel.GetLayoutOptionViewModels(globalFile, this.LayoutManager);
            if (globalViews.Count > 0)
            {
                LayoutOptionViewModel savedOption = null;
                if (!string.IsNullOrWhiteSpace(Properties.Settings.Default.LayoutPickerSavedOption))
                {
                    foreach (LayoutOptionViewModel layout in globalViews)
                    {
                        if (layout.Name == Properties.Settings.Default.LayoutPickerSavedOption)
                        {
                            savedOption = layout;
                            break;
                        }
                    }
                }

                LayoutPickerWindow window = new LayoutPickerWindow();
                window.AskOnStartup.Visibility = Visibility.Visible;
                window.AskOnStartup.IsChecked = Properties.Settings.Default.AskUserForLayout;
                window.LayoutList.ItemsSource = globalViews;
                window.Owner = Application.Current.MainWindow;
                if (savedOption != null)
                {
                    window.LayoutList.SelectedItem = savedOption;
                }

                if (window.ShowDialog() != false)
                {
                    LayoutOptionViewModel selected = window.LayoutList.SelectedItem as LayoutOptionViewModel;
                    bool noChange = Properties.Settings.Default.LayoutPickerSavedOption == selected.Name;

                    Properties.Settings.Default.LayoutPickerSavedOption = selected.Name;
                    Properties.Settings.Default.AskUserForLayout = (bool)window.AskOnStartup.IsChecked;

                    if (!noChange)
                    {
                        MessageBoxResult result = MessageBox.Show("Would you like to change your layout now?", "Layout Change", MessageBoxButton.YesNo);
                        if (result == MessageBoxResult.Yes)
                        {
                            this.LayoutManager.RestoreLayout(selected.Xml);
                            Properties.Settings.Default.LastActiveLayout = selected.Name;
                            this.LayoutManager.CloseAllDocuments();
                        }
                    }

                    Properties.Settings.Default.Save();


                    return;
                }
            }
        }
        #endregion // ICommand Implementation
    } // WindowClearLayoutMenuItem

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.MenuWindow, typeof(IWorkbenchCommand))]
    class WindowSep2MenuItem : WorkbenchCommandSeparator
    {
        #region Constants
        public static readonly Guid GUID =
            new Guid("BA4FC898-8803-4EAF-AF50-94A25B4282F5");
        #endregion // Constants

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public WindowSep2MenuItem()
        {
            this.ID = GUID;
            this.IsSeparator = true;
            this.RelativeID = LayoutPickerLayoutMenuItem.GUID;
            this.Direction = Direction.After;
        }

        #endregion // Constructor(s)

        #region ICommand Implementation

        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
        }

        #endregion // ICommand Implementation
    } // WindowSep2MenuItem

    #region ToolWindowMenuItem Class

    /// <summary>
    /// Menu item representing a tool window; so its visibility can be toggled.
    /// </summary>
    class ToolWindowMenuItem : WorkbenchMenuItemBase
    {
        #region Properties

        /// <summary>
        /// Associated IToolWindow instance.
        /// </summary>
        public IToolWindowProxy ToolWindowProxy
        {
            get;
            set;
        }

        private readonly ILayoutManager LayoutManager = null;

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public ToolWindowMenuItem(IToolWindowProxy proxy, ILayoutManager layoutManager)
        {
            this.Header = proxy.Header;
            this.ToolWindowProxy = proxy;
            this.LayoutManager = layoutManager;
            this.IsCheckable = true;
            this.KeyGesture = proxy.KeyGesture;
            if (proxy.DelayCreation == false)
            {
                IToolWindowBase toolWindow = this.ToolWindowProxy.GetToolWindow();
            }
        }

        #endregion // Constructor(s)

        #region ICommand Implementation

        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
            Log.Log__Debug("ToolWindowMenuItem::Execute()");
            IToolWindowBase toolWindow = this.ToolWindowProxy.GetToolWindow();
            if (toolWindow != null)
            {
                if (this.LayoutManager.IsVisible(toolWindow))
                    toolWindow.Hide();
                else
                {
                    if (toolWindow.WasFloatingWindowWhenHidden == true)
                        toolWindow.ShowAsFloatingWindow(true);
                    else
                        this.LayoutManager.ShowToolWindow(toolWindow);

                    if (toolWindow.FloatByDefault == true && toolWindow.HasBeenShown == false)
                    {
                        toolWindow.FloatingWindowSize = toolWindow.DefaultFloatSize;
                        toolWindow.ShowAsFloatingWindow(true);
                    }
                }
            }
        }

        #endregion // ICommand Implementation
    } // ToolWindowMenuItem

    #endregion // ToolWindowMenuItem Class

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.MenuWindow, typeof(IWorkbenchCommand))]
    class ToolWindowsMenuItem : WorkbenchMenuItemBase, IPartImportsSatisfiedNotification
    {
        #region MEF Imports

        /// <summary>
        /// Generic extension service reference.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ExtensionService, typeof(IExtensionService))]
        private IExtensionService ExtensionService { get; set; }

        /// <summary>
        /// Layout manager reference.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager, typeof(ILayoutManager))]
        private ILayoutManager LayoutManager { get; set; }

        /// <summary>
        /// Tool window proxys
        /// </summary>
        [ImportManyExtension(Workbench.AddIn.ExtensionPoints.ToolWindowProxy, typeof(IToolWindowProxy))]
        private IEnumerable<IToolWindowProxy> ToolWindows { get; set; }

        #endregion // MEF Imports

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ToolWindowsMenuItem()
        {
            this.Header = "Tool Windows";
            ID = new Guid(Workbench.AddIn.CompositionPoints.MenuWindowToolWindows);
            RelativeID = WindowSep2MenuItem.GUID;
            Direction = Direction.After;
        }
        #endregion // Constructor(s)

        #region IPartImportsSatisfiedNotification Methods

        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {
            List<IWorkbenchCommand> items = new List<IWorkbenchCommand>();
            foreach (IToolWindowProxy proxy in ToolWindows)
            {
                items.Add(new ToolWindowMenuItem(proxy, LayoutManager));
            }
            Items = ExtensionService.Sort(items);
        }

        #endregion // IPartImportsSatisfiedNotification Methods

        #region Submenu Methods

        /// <summary>
        /// 
        /// </summary>
        protected override void OnIsSubmenuOpenChanged()
        {
            if (!IsSubmenuOpen)
                return;

            foreach (IWorkbenchCommand item in this.Items)
            {
                if (!(item is ToolWindowMenuItem))
                    continue;

                ToolWindowMenuItem twitem = (item as ToolWindowMenuItem);
                if (twitem.ToolWindowProxy.HasBeenCreated == true)
                {
                    twitem.IsChecked = LayoutManager.IsVisible(twitem.ToolWindowProxy.GetToolWindow());
                }
                else
                {
                    twitem.IsChecked = false;
                }
            }
        }

        #endregion // Submenu Methods

        #region ICommand Implementation

        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
        }

        #endregion // ICommand Implementation
    } // ToolWindowsMenuItem
} // Workbench.UI.Menu namespace
