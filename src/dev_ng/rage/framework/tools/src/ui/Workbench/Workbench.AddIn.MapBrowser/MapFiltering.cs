﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Workbench.AddIn.MapBrowser
{
    /// <summary>
    /// The different filtering that can be put onto the map area children
    /// </summary>
    public enum MapAreaFilters
    {
        None,
        All,
        ShowMapSections,
        ShowPropGroups,
        ShowPropFiles,
        ShowInteriorFiles
    }

    /// <summary>
    /// Base class for the map filtering types
    /// </summary>
    public class BaseMapFiltering
    {
        #region Properties

        /// <summary>
        /// The name that this filter has as a display name
        /// </summary>
        public String Name
        {
            get { return m_name; }
            set { m_name = value; }
        }
        private String m_name;

        /// <summary>
        /// The type of map filtering
        /// </summary>
        public MapAreaFilters Type
        {
            get { return m_type; }
            set { m_type = value; }
        }
        private MapAreaFilters m_type;

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Default constructor
        /// </summary>
        public BaseMapFiltering()
        {
            this.Name = "Unknown";
            this.Type = MapAreaFilters.None;
        }

        /// <summary>
        /// Default constructor, that sets the displayname for this filter with
        /// the given name
        /// </summary>
        public BaseMapFiltering(String displayName)
        {
            this.Name = displayName;
            this.Type = MapAreaFilters.None;
        }

        #endregion // Constructor(s)

    }

    public class ShowMapSections :
        BaseMapFiltering
    {
        #region Constructor(s)

        /// <summary>
        /// Default constructor
        /// </summary>
        public ShowMapSections()
        {
            this.Name = "Show Map Sections";
            this.Type = MapAreaFilters.ShowMapSections;
        }

        #endregion // Constructor(s)
    }

    public class ShowPropGroups :
        BaseMapFiltering
    {
        #region Constructor(s)

        /// <summary>
        /// Default constructor
        /// </summary>
        public ShowPropGroups()
        {
            this.Name = "Show Prop Groups";
            this.Type = MapAreaFilters.ShowPropGroups;
        }

        #endregion // Constructor(s)
    }

    public class ShowPropFiles :
        BaseMapFiltering
    {
        #region Constructor(s)

        /// <summary>
        /// Default constructor
        /// </summary>
        public ShowPropFiles()
        {
            this.Name = "Show Prop Files";
            this.Type = MapAreaFilters.ShowPropFiles;
        }

        #endregion // Constructor(s)
    }

    public class ShowInteriors :
        BaseMapFiltering
    {
        #region Constructor(s)

        /// <summary>
        /// Default constructor
        /// </summary>
        public ShowInteriors()
        {
            this.Name = "Show Interiors";
            this.Type = MapAreaFilters.ShowInteriorFiles;
        }

        #endregion // Constructor(s)
    }

}
