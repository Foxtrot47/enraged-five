﻿using System;
using MetadataEditor.AddIn.MetaEditor.ViewModel;
using RSG.Base.Editor.Command;
using RSG.Metadata.Data;
using RSG.Metadata.Parser;

namespace MetadataEditor.AddIn.MetaEditor.GridViewModel
{
    /// <summary>
    /// The view model that wraps around a 
    /// <see cref="PointerTunable"/> object. Used
    /// for the grid view.
    /// </summary>
    public class PointerTunableViewModel : GridTunableViewModel
    {
        #region Members
        /// <summary>
        /// The private field used for the <see cref="Value"/> property.
        /// </summary>
        private GridTunableViewModel value;
        #endregion // Members

        #region Properties
        /// <summary>
        /// A reference to the value that this pointer tunable points to.
        /// </summary>
        public new GridTunableViewModel Value
        {
            get { return this.value; }
            set
            {
                SetPropertyValue(value, this.value, () => this.Value,
                    new PropertySetDelegate(delegate(Object newValue)
                    {
                        this.value = (GridTunableViewModel)newValue;
                    }
                    ));
            }
        }


        public bool IsStructure
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="MetadataEditor.AddIn.MetaEditor.GridViewModel.PointerTunableViewModel"/>
        /// class.
        /// </summary>
        public PointerTunableViewModel(GridTunableViewModel parent, PointerTunable m, MetaFileViewModel root)
            : base(parent, m, root)
        {
            if ((m.Definition as PointerMember).Policy == PointerMember.PointerPolicy.Link ||
                (m.Definition as PointerMember).Policy == PointerMember.PointerPolicy.LazyLink ||
                (m.Definition as PointerMember).Policy == PointerMember.PointerPolicy.ExternalNamed)
            {
                this.Value = new StringValueViewModel((GridTunableViewModel)this.Parent, m, root);
            }
            else if (m.Value is ITunable)
            {
                this.Value = TunableViewModelFactory.Create((GridTunableViewModel)this.Parent, m.Value as ITunable, root);
                this.IsStructure = true;
            }
        }

        #endregion // Constructor
    } // PointerTunableViewModel
} // MetadataEditor.AddIn.MetaEditor.GridViewModel
