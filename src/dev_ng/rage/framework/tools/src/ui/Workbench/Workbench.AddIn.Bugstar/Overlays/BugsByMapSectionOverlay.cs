﻿using System;
using System.Windows.Media;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Windows.Controls.WpfViewport2D;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using RSG.Base.Math;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using Workbench.AddIn.Bugstar.OverlayViews;
using RSG.Interop.Bugstar;
using RSG.Base.Logging;
using System.IO;
using System.Xml;
using System.Collections.Specialized;
using RSG.Interop.Bugstar.Game;
using RSG.Interop.Bugstar.Organisation;
using RSG.Model.Common.Map;
using Workbench.AddIn.Services.Model;

namespace Workbench.AddIn.Bugstar.Overlays
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(ExtensionPoints.Overlay, typeof(Viewport.AddIn.IViewportOverlay))]
    public class BugsByMapSectionOverlay : BugsOverlayBase
    {
        #region Constants
        private const String NAME = "Bugs by Map Section";
        private const String DESC = "Shows all the bugs that are in a particular map section.";
        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// Level Browser
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LevelBrowser, typeof(ILevelBrowser))]
        Lazy<ILevelBrowser> LevelBrowserProxy { get; set; }

        /// <summary>
        /// Content Browser
        /// </summary>
        [ImportExtension(ContentBrowser.AddIn.CompositionPoints.ContentBrowser, typeof(ContentBrowser.AddIn.IContentBrowser))]
        Lazy<ContentBrowser.AddIn.IContentBrowser> ContentBrowserProxy { get; set; }
        #endregion // MEF Imports
        
        #region Private Properties
        /// <summary>
        /// The list of map grids that bugstar has available
        /// </summary>
        private List<MapGrid> MapGrids
        {
            get;
            set;
        }
        #endregion // Private Properties

        #region Constructors
        /// <summary>
        /// Default Constructor
        /// </summary>
        public BugsByMapSectionOverlay()
            : base(NAME, DESC)
        {
        }
        #endregion // Constructors

        #region Overrides
        /// <summary>
        /// Gets called when the user wants to view this overlay
        /// </summary>
        public override void Activated()
        {
            base.Activated();

            // Get the grids that are available for the current project
            uint bugstarLevelId = Config.Value.BugstarConfig.GetLevelId(LevelBrowserProxy.Value.SelectedLevel.Name);

            Map currentMap = null;
            if (BugstarService.Project != null)
            {
                currentMap = Map.GetMapById(BugstarService.Project, bugstarLevelId);
            }

            if (currentMap != null)
            {
                MapGrids = new List<MapGrid>();
                MapGrids.AddRange(currentMap.Grids.Where(grid => grid.Active));
            }

            // Add callback for when the content browser item changes
            ContentBrowserProxy.Value.GridSelectionChanged += GridSelectionChanged;
        }

        /// <summary>
        /// Gets called when the user turns off this overlay
        /// </summary>
        public override void Deactivated()
        {
            base.Deactivated();

            MapGrids = null;

            ContentBrowserProxy.Value.GridSelectionChanged -= GridSelectionChanged;
        }

        /// <summary>
        /// Gets the control that should be shown in the overlay details panel
        /// </summary>
        public override System.Windows.Controls.Control GetOverlayControl()
        {
            return new BugMapSectionDetailsView();
        }
        #endregion // Overrides

        #region Event Callbacks
        /// <summary>
        /// Get called when the items selected in the content browser change
        /// </summary>
        void GridSelectionChanged(Object sender, ContentBrowser.AddIn.GridSelectionChangedEventArgs args)
        {
            Refresh();
        }
        #endregion // Event Callbacks

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        protected override void RefreshBugList()
        {
            // Get the new list of bugs
            BugList.Clear();

            // Attempt to get the MapGrid based on the currently selected
            if (ContentBrowserProxy.Value.SelectedGridItems != null && ContentBrowserProxy.Value.SelectedGridItems.Count() > 0)
            {
                // Use the first one for now
                IMapSection section = ContentBrowserProxy.Value.SelectedGridItems.First() as IMapSection;

                if (section != null)
                {
                    MapGrid grid = MapGrids.FirstOrDefault(item => item.Name.ToLower() == section.Name.ToLower());

                    if (grid != null)
                    {
                        try
                        {
                            BugList.AddRange(grid.GetBugs("Id,Summary,Description,X,Y,Z,Developer,Tester,State,Component,DueDate,Priority,Category"));
                        }
                        catch (System.Exception ex)
                        {
                            Log.Log__Exception(ex, "Unexpected exception while attempting to retrieve the bug list for section '{0}'.", section.Name);
                        }
                    }
                }
            }
        }
        #endregion // Private Methods
    } // BugstarBugOverlay
} // Workbench.AddIn.Bugstar.Overlays
