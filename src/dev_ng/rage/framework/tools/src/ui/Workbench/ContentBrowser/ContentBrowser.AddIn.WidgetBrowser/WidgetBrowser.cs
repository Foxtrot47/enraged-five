﻿

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using ragCore;
using ragWidgets;
using RSG.Base.Logging;
using RSG.Base.Network;
using RSG.Model.Common;
using RSG.Model.Common.Level;
using Workbench.AddIn;
using Workbench.AddIn.Services;
using ContentBrowser.AddIn.WidgetBrowser.Commands;
using WidgetEditor.ViewModel;
using WidgetEditor.Document;
using WidgetEditor.View;
using System.Linq;

namespace ContentBrowser.AddIn.WidgetBrowser
{

    /// <summary>
    /// 
    /// </summary>
    [Workbench.AddIn.ExportExtension( ContentBrowser.AddIn.ExtensionPoints.Browser, typeof( ContentBrowser.AddIn.BrowserBase ) )]
    class WidgetBrowser : ContentBrowser.AddIn.BrowserBase, IPartImportsSatisfiedNotification
    {

        #region Imports

        /// <summary>
        /// The main content browser export object; will also be used as the view model
        /// for the content browsers main window
        /// </summary>
        [Workbench.AddIn.ImportExtension( ContentBrowser.AddIn.CompositionPoints.ContentBrowser, typeof( ContentBrowser.AddIn.IContentBrowser ) )]
        private Lazy<ContentBrowser.AddIn.IContentBrowser> ContentBrowserImport { get; set; }


        /// <summary>
        /// MEF import for workbench UI.UI.Layout Manager component.
        /// </summary>
        [ImportExtension( Workbench.AddIn.CompositionPoints.LayoutManager, typeof( Workbench.AddIn.UI.Layout.ILayoutManager ) )]
        public Workbench.AddIn.UI.Layout.ILayoutManager LayoutManager { get; set; }

        /// <summary>
        /// MEF import for workbench configuration service.
        /// </summary>
        [ImportExtension( Workbench.AddIn.CompositionPoints.ConfigurationService,
            typeof( IConfigurationService ) )]
        private IConfigurationService ConfigurationService { get; set; }

        /// <summary>
        /// MEF import for acquiring proxy connection
        /// </summary>
        [ImportExtension( Workbench.AddIn.ExtensionPoints.ProxyService, typeof( IProxyService ) )]
        public IProxyService ProxyService { get; set; }
        #endregion


        #region Members

        private ProxyConnection m_ProxyConnection = new ProxyConnection( "Rag Proxy" );


        private bool m_ProxyInitialised = false;
        private static bool m_WidgetHandlersInitialised = false;
        private ObservableCollection<WidgetBank> m_Banks = new ObservableCollection<WidgetBank>();

        // Pipes
        private Thread m_PipeUpdateThread;
        private Thread m_OutputUpdateThread;
        private BankPipe m_PipeBank = new BankPipe();
        private INamedPipe m_PipeOutput = PipeID.CreateNamedPipe();
        private INamedPipe m_PipeEvents = PipeID.CreateNamedPipe();
        private BankRemotePacketProcessor m_PacketProcessor = new BankRemotePacketProcessor();

        // Commands
        private RefreshWidgetsCommand m_RefreshWidgetsCommand;


        #endregion // Members


        #region Properties

        #endregion // Properties



        #region Constructor

        /// <summary>
        /// Default Constructor
        /// </summary>
        public WidgetBrowser()
            : base( "Widget Browser" )
        {
            m_ProxyConnection.ProxyDisconnected += new EventHandler( m_ProxyConnection_ProxyDisconnected );


            // Set up event listeners and widget handlers
            Application.Current.Exit += new ExitEventHandler( Current_Exit );

            if ( !m_WidgetHandlersInitialised )
            {
                BankManager.AddHandlers( m_PacketProcessor );

                WidgetAngle.AddHandlers( m_PacketProcessor );
                WidgetBank.AddHandlers( m_PacketProcessor );
                WidgetButton.AddHandlers( m_PacketProcessor );
                WidgetColor.AddHandlers( m_PacketProcessor );
                WidgetCombo.AddHandlers( m_PacketProcessor );
                WidgetData.AddHandlers( m_PacketProcessor );
                WidgetGroup.AddHandlers( m_PacketProcessor );
                WidgetImageViewer.AddHandlers( m_PacketProcessor );
                ragWidgets.WidgetList.AddHandlers( m_PacketProcessor );
                WidgetMatrix.AddHandlers( m_PacketProcessor );
                WidgetSeparator.AddHandlers( m_PacketProcessor );
                WidgetSliderFloat.AddHandlers( m_PacketProcessor );
                WidgetSliderInt.AddHandlers( m_PacketProcessor );
                WidgetText.AddHandlers( m_PacketProcessor );
                WidgetTitle.AddHandlers( m_PacketProcessor );
                WidgetToggle.AddHandlers( m_PacketProcessor );
                WidgetTreeList.AddHandlers( m_PacketProcessor );
                WidgetVector.AddHandlers( m_PacketProcessor );

                m_WidgetHandlersInitialised = true;
            }

            WidgetBank.AddBankEvent += new WidgetBank.BankDelegate( WidgetBank_AddBankEvent );
            m_PacketProcessor.DestroyHandler = new BankRemotePacketProcessor.DestroyDelegate( Widget.DestroyHandler );
            BankRemotePacketProcessor.OnDisplayMessage += new BankRemotePacketProcessor.DisplayMessageEventHandler( BankRemotePacket_OnDisplayMessage );


            m_PipeUpdateThread = new Thread( new ThreadStart( ReadBankPipe ) );
            m_PipeUpdateThread.Name = "WidgetBrowser BankPipe thread";
            m_PipeUpdateThread.IsBackground = true;
            m_PipeUpdateThread.Start();

            m_OutputUpdateThread = new Thread( new ThreadStart( ReadOutputPipe ) );
            m_OutputUpdateThread.Name = "WidgetBrowser OutputPipe thread";
            m_OutputUpdateThread.IsBackground = true;
            m_OutputUpdateThread.Start();



            WidgetBank bank = new WidgetBank( m_PipeBank, "testbank" );
            AssetHierarchy.Add( new WidgetAsset( bank ) );


            Widget w2 = new WidgetBank( m_PipeBank, "testbank2" );
            AssetHierarchy.Add( new WidgetAsset( w2 ) );

            {
                //Widget w = new WidgetText( m_PipeBank, "text", "memo", "thetext", false, null );
                //bank.WidgetList.Add( w );
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {
            ContentBrowserImport.Value.PanelItemChanged += new SelectedPanelItemChangedHandler( Value_PanelItemChanged );
            m_RefreshWidgetsCommand.Context.Console = ProxyService.Console;
            ProxyService.Connected += new EventHandler( ProxyService_Connected );
            ProxyService.Disconnected += new EventHandler( ProxyService_Disconnected );
        }

        void ProxyService_Connected( object sender, EventArgs e )
        {
            m_RefreshWidgetsCommand.Context.ProxyConnection.ProxyPort = ProxyService.ProxyPort;
        }

        void ProxyService_Disconnected( object sender, EventArgs e )
        {
            CloseConnections();
        }

        #endregion // Constructor

        void Current_Exit( object sender, ExitEventArgs e )
        {
            CloseConnections();
        }

        void m_ProxyConnection_ProxyDisconnected( object sender, EventArgs e )
        {
            Log.Log__Message( "Widget editor disconnected from proxy" );
            m_ProxyInitialised = false;
        }





        #region Override Methods

        /// <summary>
        /// Override to create the commands that will be attached to
        /// this browser
        /// </summary>
        protected override ObservableCollection<ContentBrowser.AddIn.ContentBrowserCommand> CreateCommands()
        {
            ObservableCollection<ContentBrowser.AddIn.ContentBrowserCommand> commands = base.CreateCommands();
            commands.Add( CreateRefreshCommand() );
            return commands;
        }


        #endregion // Override Methods

        #region Command Creation

        /// <summary>
        /// Creates the refresh command, used to refresh the level browser data
        /// </summary>
        private ContentBrowser.AddIn.ContentBrowserCommand CreateRefreshCommand()
        {
            RefreshWidgetsCommand.CommandContext rwcContext = new RefreshWidgetsCommand.CommandContext();
            rwcContext.AssetHierarchy = AssetHierarchy;
            rwcContext.Banks = m_Banks;
            rwcContext.PipeBank = m_PipeBank;
            rwcContext.PipeOutput = m_PipeOutput;
            rwcContext.PipeEvents = m_PipeEvents;
            rwcContext.ProxyConnection = m_ProxyConnection;
            rwcContext.ProxyInitialised = false;
            rwcContext.ReadBankPipe = ReadBankPipe;
            rwcContext.ReadOutputPipe = ReadOutputPipe;
            rwcContext.OnSuccess = delegate( RefreshWidgetsCommand.Result result )
                {
                    Application.Current.Dispatcher.Invoke( (Action)delegate()
                    {
                        lock ( m_Banks )
                        {
                            var sortedBanks = from item in m_Banks
                                              orderby item.Title
                                              select item;

                            ObservableCollection<WidgetBank> tmp = new ObservableCollection<WidgetBank>();
                            foreach ( var bank in sortedBanks )
                            {
                                tmp.Add( bank );
                            }
                            m_Banks = tmp;

                            AssetHierarchy.Clear();
                            foreach ( var bank in m_Banks )
                            {
                                var bankAsset = new WidgetAsset( bank );
                                AssetHierarchy.Add( bankAsset );
                                SetupAssetHierarchy( bankAsset );
                            }

                            m_ProxyInitialised = true;
                        }
                    } );
                };
            rwcContext.OnFail = delegate( RefreshWidgetsCommand.Result result )
            {
            };
            rwcContext.OnStarted = delegate()
            {
                m_Banks.Clear();
                AssetHierarchy.Clear();
            };

            m_RefreshWidgetsCommand = new RefreshWidgetsCommand( rwcContext );

            return m_RefreshWidgetsCommand.Command;
        }



        private void SetupAssetHierarchy( WidgetAsset widgetAsset )
        {
            if ( widgetAsset.Model is WidgetGroupBase )
            {
                var groupWidget = widgetAsset.Model as WidgetGroupBase;
                foreach ( var item in groupWidget.WidgetList )
                {
                    WidgetAsset childAsset = new WidgetAsset( item );
                    widgetAsset.AssetChildren.Add( childAsset );
                    SetupAssetHierarchy( childAsset );
                }
            }
        }

        #endregion // Command Creation








        private void CloseConnections()
        {
            Log.Log__Message( "Widget editor closing connections" );

            if ( m_ProxyConnection != null && m_ProxyConnection.IsConnected() )
            {
                m_ProxyConnection.DisconnectFromProxy();

                // Allow proxy to shut it down peacefully before closing pipes
                if ( ProxyService.Console.IsConnected() )
                {
                    string command = "getProxyStatus " + m_ProxyConnection.ProxyUID;
                    for ( int i = 0; i < 20; i++ )
                    {
                        Thread.Sleep( 200 );
                        string proxyStatus = ProxyService.Console.SendCommand( command );
                        if ( proxyStatus.Contains( "Error" ) )
                        {
                            break;
                        }
                    }

                    ProxyService.Console.DisconnectConsole();
                }
                else
                {
                    Thread.Sleep( 1000 );
                }
            }

            m_PipeBank.Close();
            m_PipeOutput.Close();
            m_PipeEvents.Close();
        }























        void BankRemotePacket_OnDisplayMessage( object sender, BankRemotePacketProcessor.DisplayMessageEventArgs e )
        {
            if ( e.Error )
            {
                Log.Log__Error( e.Message );
            }
            else
            {
                Log.Log__Message( e.Message );
                e.YesOK = true; // todo fix
            }

        }


        private void ReadBankPipe()
        {
            while ( Application.Current != null )
            {
                if ( m_PipeBank.IsConnected() )
                {
                    m_PacketProcessor.Update( m_PipeBank );
                    m_PacketProcessor.Process( m_PipeBank );
                }
            }
        }

        private void ReadOutputPipe()
        {
            while ( Application.Current != null )
            {
                if ( m_PipeOutput.IsConnected() )
                {
                    string currentLine = string.Empty;
                    while ( m_PipeOutput.HasData() )
                    {
                        const int BUFFER_SIZE = 4 * 1024;
                        Byte[] chars = new Byte[BUFFER_SIZE];
                        int numRead = m_PipeOutput.ReadData( chars, BUFFER_SIZE, false );

                        for ( int i = 0; i < numRead; i++ )
                        {
                            Char charRead = Convert.ToChar( chars[i] );
                            if ( charRead == 0 )
                            {
                                if ( currentLine.Length > 0 )
                                {
                                    Log.Log__MessageCtx( "RAG", currentLine );
                                    //Console.WriteLine( "Enqueue Output: {0}", linesOutput.ToString() );
                                }

                                currentLine = string.Empty;
                            }
                            else
                            {
                                currentLine += charRead;
                            }
                        }
                    }
                }
            }

            Log.Log__Debug( "Listening to output pipe END" );
        }




        void WidgetBank_AddBankEvent( BankPipe pipe, WidgetBank bank )
        {
            Application.Current.Dispatcher.Invoke( (Action)delegate()
            {
                lock ( m_Banks )
                {
                    m_Banks.Add( bank );
                    if ( m_ProxyInitialised )
                    {
                        AssetHierarchy.Add( new WidgetAsset( bank ) );
                    }
                }
            } );
        }



















        #region Event handling


        void Value_GridSelectionChanged( object sender, GridSelectionChangedEventArgs args )
        {
            foreach ( var item in args.NewLevel )
            {
                //if ( item is RESTService )
                {
                    //OpenService((RESTService) item );                                    
                }
            }
        }


        static WidgetDocumentBase doc = null;
        void Value_PanelItemChanged( IAsset oldValue, IAsset newValue )
        {
            if ( !(newValue is WidgetAsset) )
                return;

            WidgetAsset asset = (WidgetAsset)newValue;
            Widget widget = asset.Model;


            WidgetDocumentViewModel docVM;
            if ( doc == null )
            {
                docVM = new WidgetDocumentViewModel();
                doc = new WidgetView( "test1", docVM );
            }

            docVM = doc.ViewModel;
            docVM.Widgets.Clear();
            docVM.Path = widget.Path;

            if ( widget is WidgetGroupBase )
            {
                WidgetGroupBase group = widget as WidgetGroupBase;
                docVM.RootWidget = (WidgetGroupViewModel)WidgetViewModelFactory.CreateViewModel( group );
                SetupViewModelHierarchy( docVM.RootWidget );
                /*
                foreach (Widget w in group.WidgetList)
                {
                    WidgetViewModel wvm = WidgetViewModelFactory.CreateViewModel( w );
                    docVM.RootWidget.Children.Add( wvm );
                    
                    docVM.Widgets.Add( wvm );
                }
                 * */
            }

            LayoutManager.ShowDocument( doc );

        }


        private void SetupViewModelHierarchy( WidgetGroupViewModel widgetGroupViewModel )
        {
            widgetGroupViewModel.IsExpanded = true;
            foreach ( Widget w in widgetGroupViewModel.WidgetList )
            {
                WidgetViewModel wvm = WidgetViewModelFactory.CreateViewModel( w );
                widgetGroupViewModel.Children.Add( wvm );
                wvm.Parent = widgetGroupViewModel;

                if ( wvm is WidgetGroupViewModel )
                {
                    SetupViewModelHierarchy( (WidgetGroupViewModel)wvm );
                }
            }
        }


        #endregion
    } // MetadataBrowser
} // ContentBrowser.AddIn.WidgetBrowser

