﻿using System;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Diagnostics;
using RSG.Base.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RSG.Model.Map;
using RSG.Model.Map.Statistics;
using RSG.Model.Map.Utilities;
using RSG.Model.Map.ViewModel;
using RSG.Base.Windows.Controls;
using RSG.Base.Editor;
using Workbench.AddIn.Services;
using Workbench.AddIn.UI.Layout;
using RSG.SourceControl.Perforce;
using Map.AddIn.Services;

namespace Workbench.AddIn.MapBrowser
{
    /// <summary>
    /// Interaction logic for MapBrowserView.xaml
    /// </summary>
    internal partial class MapBrowserView : ToolWindowBase<MapBrowserViewModel>
    {
        #region Constants

        public static readonly Guid GUID = new Guid("33A0CC63-ECAE-4A52-B6B9-7C0422180768");

        #endregion // Constants

        #region Properties

        /// <summary>
        /// A reference to the perforce object that is used to get the 
        /// latest independent data.
        /// </summary>
        public P4 PerforceObject
        {
            get { return m_perforceObject; }
            set { m_perforceObject = value; }
        }
        private P4 m_perforceObject = null;
        
        /// <summary>
        /// A list of map tool windows that should be shon when the
        /// user presses the map button
        /// </summary>
        private List<IToolWindowProxy> MapToolWindows
        { 
            get; 
            set;
        }

        /// <summary>
        /// A list of map statistic windows that should be shown when the
        /// user presses the statistics button
        /// </summary>
        private List<IToolWindowProxy> MapStatisticWindows
        {
            get;
            set;
        }

        /// <summary>
        /// A boolean value representing whether the statistics have been created
        /// for the current map data. Whent he map data gets refreshed so do the statistics
        /// </summary>
        private Boolean CreatedStatistics
        {
            get;
            set;
        }

        private Map.AddIn.Services.IPropViewer PropViewer
        {
            get;
            set;
        }

        #endregion // Properties

        #region Constructor

        public MapBrowserView(IEnumerable<IToolWindowProxy> mapToolWindows, IEnumerable<IToolWindowProxy> mapStatisticWindows,  IMapBrowser viewModel, IPropViewer propViewer)
            : base(Workbench.AddIn.MapBrowser.Resources.Strings.MapBrowser_Name, viewModel as MapBrowserViewModel)
        {
            InitializeComponent();

            this.ID = GUID;
            this.SaveModel = this.ViewModel;
            this.MapToolWindows = new List<IToolWindowProxy>(mapToolWindows);
            this.MapStatisticWindows = mapStatisticWindows.ToList();
            this.ViewModel.TreeView = this.MapTreeView;
            this.PropViewer = propViewer;
        }

        #endregion // Constructor

        #region Loading Methods

        /// <summary>
        /// Gets called when the user presses the refresh button, this is
        /// responsible for reloading all the map data
        /// </summary>
        private void RefreshMapData(Object sender, RoutedEventArgs e)
        {
            System.GC.Collect();
            try
            {
                this.LoadProgress.Value = 0;

                if (this.PerforceObject == null)
                {
                    this.PerforceObject = new RSG.SourceControl.Perforce.P4();
                }
                P4API.P4RecordSet syncResult = this.PerforceSyncExportLevelData(true, null);
                if (syncResult != null && syncResult.Records.Length > 0)
                {
                    Dialogs.PerforceSync dlg = new Dialogs.PerforceSync(syncResult);
                    dlg.Owner = Application.Current.MainWindow;
                    dlg.DataContext = syncResult;
                    dlg.PerforceObject = this.PerforceObject;
                    dlg.Config = this.ViewModel.ConfigurationService;

                    if ((Boolean)dlg.ShowDialog() == false)
                    {
                        this.LoadProgress.Value = 0;
                        this.ViewModel.RefreshingMapData = false;
                        dlg = null;
                        return;
                    }

                    dlg = null;
                }

                this.ViewModel.ContentLevel = null;
                this.ViewModel.SelectedLevel = null;
                LoadMapDataProcess loadProcess = new LoadMapDataProcess(this.ViewModel);
                loadProcess.Owner = Application.Current.MainWindow;
                loadProcess.ShowDialog();

                // After generic sections have been loaded dynamically load the
                // non generic sections
                BackgroundWorker thread = CreateNonGenericLoadThread();
                if (thread != null)
                {
                    thread.RunWorkerAsync();
                }
            }
            catch (Exception ex)
            {
                RSG.Base.Logging.Log.Log__Exception(ex, "Exception caught while refreshing map data");
            }
            finally
            {
            }
        }

        ///<summary>
        /// Gets the latest independent data if the preview is false and returns
        /// the number of files out of date if preview is true
        /// </summary>
        private P4API.P4RecordSet PerforceSyncExportLevelData(Boolean preview, P4API.P4RecordSet syncSet)
        {
            P4API.P4RecordSet result = null;
            String currentDirectory = System.IO.Directory.GetCurrentDirectory();
            try
            {
                System.IO.Directory.SetCurrentDirectory(System.IO.Path.Combine(ViewModel.ConfigurationService.GameConfig.ExportDir, "levels"));
                PerforceObject.Connect();
                if (preview)
                {
                    result = PerforceObject.Run("sync", "-n", "....xml");
                    Dictionary<String, int> filesizes = new Dictionary<String, int>();
                    foreach (P4API.P4Record record in result)
                    {
                        // Check if its a delete.
                        String action = String.Empty;
                        if (record.Fields.ContainsKey("action"))
                        {
                            action = (record.Fields["action"]);
                            if (0 == String.Compare("deleted", action))
                                continue;
                        }

                        int fileSize = -1;
                        if (record.Fields.ContainsKey("fileSize"))
                            fileSize = int.Parse(record.Fields["fileSize"]);
                        String filename = (record.Fields["depotFile"] as String);

                        Debug.Assert(fileSize >= 0, String.Format("Internal error; fileSize is 0 for {0} [action: {1}].", 
                            filename, action));
                        filesizes.Add(filename, fileSize);
                    }

                    //P4API.P4RecordSet result1 = PerforceObject.Run("fstat", "-T", "depotFile, haveRev, headRev", "....xml");
                    //foreach (P4API.P4Record record in result1)
                    //{
                    //    if (record.Fields.ContainsKey("haveRev"))
                    //    {
                    //        if (int.Parse(record.Fields["headRev"]) > int.Parse(record.Fields["haveRev"]))
                    //        {
                    //            int size = filesizes[record.Fields["depotFile"]];
                    //        }
                    //    }
                    //    else
                    //    {
                    //        int size = filesizes[record.Fields["depotFile"]];
                    //    }
                    //}
                }
            }
            catch (P4API.Exceptions.P4APIExceptions ex)
            {
                System.IO.Directory.SetCurrentDirectory(currentDirectory);
                RSG.Base.Logging.Log.Log__Exception(ex, "Unhandled Perforce exception");
            }
            finally
            {
                System.IO.Directory.SetCurrentDirectory(currentDirectory);
                PerforceObject.Disconnect();
            }

            return result;
        }

        /// <summary>
        /// Creates the thread that will be used to 
        /// create the non generic map sections after 
        /// the initial load and is done in the
        /// background of the main application, so that it 
        /// streams in dyamically
        /// </summary>
        private BackgroundWorker CreateNonGenericLoadThread()
        {
            if (ViewModel != null)
            {
                this.LoadProgress.Value = 0;
                BackgroundWorker backgroundWorker = new BackgroundWorker();
                backgroundWorker.WorkerReportsProgress = true;

                // what to do in the background thread
                backgroundWorker.DoWork += new DoWorkEventHandler
                (
                    delegate(Object o, DoWorkEventArgs args)
                    {
                        this.ViewModel.RefreshingMapData = true;
                        this.LoadNonGeneric(backgroundWorker);
                        this.ViewModel.RefreshingMapData = false;
                    }
                );
                backgroundWorker.ProgressChanged += new ProgressChangedEventHandler
                (
                    delegate(Object o, ProgressChangedEventArgs args)
                    {
                        Dispatcher.Invoke(
                            new Action(
                                delegate
                                {
                                    if (this.LoadProgress != null)
                                    {
                                        this.LoadProgress.Value = args.ProgressPercentage;
                                    }
                                }
                        ), System.Windows.Threading.DispatcherPriority.ApplicationIdle);
                    }
                );

                backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler
                (
                    delegate(Object o, RunWorkerCompletedEventArgs args)
                    {
                        this.LoadProgress.Value = 0;
                        this.ViewModel.RefreshingMapData = false;
                        this.ViewModel.ValidMapData = true;
                        Mouse.OverrideCursor = null;

                        if (this.ViewModel.GenericDefinitions.Missing.Count > 0)
                        {
                            System.Diagnostics.Debug.Print("The following definitions could not be found during load:");
                            RSG.Base.Logging.Log.Log__Error("The following definitions could not be found during load:");
                            foreach (KeyValuePair<String, KeyValuePair<String, List<String>>> def in this.ViewModel.GenericDefinitions.Missing)
                            {
                                if (def.Value.Value.Count > 0)
                                {
                                    RSG.Base.Logging.Log.Log__Error(def.Key + " - " + def.Value.Key);
                                }
                            }
                            this.ViewModel.GenericDefinitions.Missing = null;
                        }
                        foreach (KeyValuePair<String, List<String>> kvp in this.ViewModel.GenericDefinitions.DefinitionNames)
                        {
                            if (kvp.Value.Count > 1)
                            {
                                String message = "Object " + kvp.Key.ToLower() + " appears in multiple maps: ";
                                foreach (String container in kvp.Value)
                                {
                                    message += container + ", ";
                                }
                                message.Trim();
                                message.Trim(',');
                                System.Diagnostics.Debug.Print(message);
                                RSG.Base.Logging.Log.Log__Error(message);
                            }
                        }

                        this.ViewModel.GenericDefinitions.DefinitionNames = null;
                        this.ViewModel.GenericDefinitions = null;

                        SceneManager.Reset();
                        GC.Collect();
                        GC.WaitForPendingFinalizers();
                    }
                );

                return backgroundWorker;
            }
            return null;
        }

        /// <summary>
        /// Dynamically loads the non generic map section data.
        /// </summary>
        private void LoadNonGeneric(BackgroundWorker thread)
        {
            Dictionary<MapSectionViewModel, PropGroupSectionViewModel> sectionPairs = new Dictionary<MapSectionViewModel, PropGroupSectionViewModel>();
            List<PropGroupSectionViewModel> propGroups = new List<PropGroupSectionViewModel>(this.ViewModel.PropGroupSections);
            foreach (MapSectionViewModel section in this.ViewModel.MapSections)
            {
                PropGroupSectionViewModel foundPropGroup = null;
                foreach (PropGroupSectionViewModel propGroup in propGroups)
                {
                    if (String.Compare(section.Name + "_props", propGroup.Name) == 0)
                    {
                        if (propGroup.Model.Level == section.Model.Level)
                        {
                            foundPropGroup = propGroup;
                            break;
                        }
                    }
                }
                if (foundPropGroup != null)
                {
                    propGroups.Remove(foundPropGroup);
                    section.Model.PropGroup = foundPropGroup.Model;
                }
                sectionPairs.Add(section, foundPropGroup);
            }
            int totalNonGenericCount = propGroups.Count + sectionPairs.Count;
            int i = 0;
            foreach (PropGroupSectionViewModel propGroup in propGroups)
            {
                if (propGroup.Model != null)
                {
                    if (propGroup.Model.CreateExportPaths())
                    {
                        propGroup.Model.CreateDataFromSceneXml(this.ViewModel.GenericDefinitions);
                        propGroup.UpdateChildrenWithModel();
                    }
                    else
                    {
                        propGroup.Model.ValidMapData = false;
                        if (propGroup.Model.Container is Level)
                        {
                            this.Dispatcher.Invoke(
                                new Action(
                                    delegate
                                    {
                                        (propGroup.Model.Container as Level).MapComponents.Remove(propGroup.Name);
                                        (propGroup.Parent as LevelViewModel).Children.Remove(propGroup);
                                    }
                                ));
                        }
                        else if (propGroup.Model.Container is MapArea)
                        {
                            this.Dispatcher.Invoke(
                                new Action(
                                    delegate
                                    {
                                        (propGroup.Model.Container as MapArea).MapComponents.Remove(propGroup.Name);
                                        (propGroup.Parent as MapAreaViewModel).Children.Remove(propGroup);
                                    }
                                ));
                        }
                    }
                }
                ++i;
            }

            int percentageComplete = (int)(System.Math.Ceiling((100.0 / totalNonGenericCount) * i));
            thread.ReportProgress(percentageComplete);


            foreach (KeyValuePair<MapSectionViewModel, PropGroupSectionViewModel> sectionPair in sectionPairs)
            {
                MapSection mapSection = sectionPair.Key.Model;
                MapSection propGroup = null;
                if (sectionPair.Value != null)
                    propGroup = sectionPair.Value.Model;

                try
                {
                    if (propGroup != null)
                    {
                        if (propGroup.CreateExportPaths())
                        {
                            propGroup.CreateDataFromSceneXml(this.ViewModel.GenericDefinitions);
                            sectionPair.Value.UpdateChildrenWithModel();
                        }
                        else
                        {
                            propGroup.ValidMapData = false;
                            if (propGroup.Container is Level)
                            {
                                this.Dispatcher.Invoke(
                                    new Action(
                                        delegate
                                        {
                                            (propGroup.Container as Level).MapComponents.Remove(propGroup.Name);
                                            (sectionPair.Value.Parent as LevelViewModel).Children.Remove(sectionPair.Value);
                                        }
                                    ));
                            }
                            else if (propGroup.Container is MapArea)
                            {
                                this.Dispatcher.Invoke(
                                    new Action(
                                        delegate
                                        {
                                            (propGroup.Container as MapArea).MapComponents.Remove(propGroup.Name);
                                            (sectionPair.Value.Parent as MapAreaViewModel).Children.Remove(sectionPair.Value);
                                        }
                                    ));
                            }
                        }
                    }

                    if (mapSection.CreateExportPaths())
                    {
                        mapSection.CreateDataFromSceneXml(this.ViewModel.GenericDefinitions);
                        sectionPair.Key.UpdateChildrenWithModel();
                    }
                    else
                    {
                        mapSection.ValidMapData = false;
                        if (mapSection.Container is Level)
                        {
                            this.Dispatcher.Invoke(
                                new Action(
                                    delegate
                                    {
                                        (mapSection.Container as Level).MapComponents.Remove(mapSection.Name);
                                        (sectionPair.Key.Parent as LevelViewModel).Children.Remove(sectionPair.Key);
                                    }
                                ));
                        }
                        else if (mapSection.Container is MapArea)
                        {
                            this.Dispatcher.Invoke(
                                new Action(
                                    delegate
                                    {
                                        (mapSection.Container as MapArea).MapComponents.Remove(mapSection.Name);
                                        (sectionPair.Key.Parent as MapAreaViewModel).Children.Remove(sectionPair.Key);
                                    }
                                ));
                        }
                    }
                }
                catch
                {
                }

                ++i;
                if (i % 30 == 0)
                {
                    percentageComplete = (int)(System.Math.Ceiling((100.0 / totalNonGenericCount) * i));
                    thread.ReportProgress(percentageComplete);
                }
            }
        }

        #endregion // Loading Methods

        #region Event Handlers

        /// <summary>
        /// Gets called when the user clicks the map button. This just makes sure that all the
        /// map tool windows assicated with this browser are opened
        /// </summary>
        private void OnMapClick(Object sender, RoutedEventArgs e)
        {
            foreach (IToolWindowProxy mapProxy in this.MapToolWindows)
            {
                IToolWindowBase toolWindow = mapProxy.GetToolWindow();
                if (toolWindow != null)
                {
                    this.ViewModel.LayoutManager.ShowToolWindow(toolWindow);
                }
            }
        }

        /// <summary>
        /// Gets called when the user clicks the statistic button. This just makes sure that all the
        /// map statistic windows assicated with this browser are opened
        /// </summary>
        private void OnStatisticClick(Object sender, RoutedEventArgs e)
        {
            foreach (IToolWindowProxy mapProxy in this.MapStatisticWindows)
            {
                IToolWindowBase toolWindow = mapProxy.GetToolWindow();
                if (toolWindow != null)
                {
                    this.ViewModel.LayoutManager.ShowToolWindow(toolWindow);
                }
            }
        }

        /// <summary>
        /// When an item gets expanded we need to make sure that its map section children are initialised
        /// </summary>
        private void OnExpanded(Object sender, RoutedEventArgs e)
        {
            this.ViewModel.FireComponentExpandedEvent(e.OriginalSource, e);
        }

        /// <summary>
        /// Gets called when a menu item that represents the selected level
        /// is clicked.
        /// </summary>
        private void OnMenuItemChecked(Object sender, RoutedEventArgs e)
        {
            LevelViewModel checkedLevel = (sender as MenuItem).Header as LevelViewModel;
            if (checkedLevel != null)
            {
                if ((sender as MenuItem).IsChecked == false)
                {
                    (sender as MenuItem).IsChecked = true;
                    return;
                }
                foreach (LevelViewModel level in this.ViewModel.ContentLevel.Levels)
                {
                    if (level != checkedLevel)
                    {
                        level.IsSelected = false;
                    }
                    else
                    {
                        checkedLevel.IsSelected = true;
                        this.ViewModel.SelectedLevel = checkedLevel;
                    }
                }
            }
        }

        /// <summary>
        /// Gets called when any tree view item gets double clicked by the user
        /// </summary>
        private void OnDoubleClicked(Object sender, MouseButtonEventArgs e)
        {
            RSG.Base.Windows.Controls.MultiSelect.TreeViewItem source = this.GetVisualParent<RSG.Base.Windows.Controls.MultiSelect.TreeViewItem>(e.OriginalSource as DependencyObject);
            if (sender == source)
            {
                if (source.Header is MapSectionViewModel)
                {
                    if ((source.Header as MapSectionViewModel).Model.SectionType == SectionTypes.PROPS)
                    {
                        this.PropViewer.CreateViewerDocument((source.Header as MapSectionViewModel).Model);
                    }
                }

                this.ViewModel.FireComponentDoubleClickedEvent(sender, e);
            }
        }

        /// <summary>
        /// Need to make sure that the tree view item selected/expanded is actually in view. This is important because
        /// we can select a item in code without it being in view. 
        /// </summary>
        private void OnSelected(Object sender, RoutedEventArgs e)
        {
            if (e.OriginalSource == sender)
            {
                (e.OriginalSource as RSG.Base.Windows.Controls.MultiSelect.TreeViewItem).BringIntoView();
            }
        }

        /// <summary>
        /// Gets the first visual parent that is of type T
        /// </summary>
        private T GetVisualParent<T>(DependencyObject root) where T : DependencyObject
        {
            DependencyObject parent = VisualTreeHelper.GetParent(root);
            T result = parent as T;
            while (parent != null && result == null)
            {
                parent = VisualTreeHelper.GetParent(parent);
                result = parent as T;
            }

            return result;
        }

        #endregion // Event Handlers

        #region Drag & Drop

        Point DragStartPoint = new Point(0.0, 0.0);
        Object DragData = null;
        UIElement DragSource = null;
        Type DragFormat = null;
        Boolean IsDragging = false;
        UIElement DragScope = null;

        private void OnDragAreaMouseDown(Object sender, RoutedEventArgs e)
        {
            RSG.Base.Windows.Controls.MultiSelect.TreeViewItem item = e.OriginalSource as RSG.Base.Windows.Controls.MultiSelect.TreeViewItem;
            if (item != null)
            {
                this.IsDragging = false;

                DragStartPoint = Mouse.GetPosition(null);
                DragData = item.Header != null ? item.Header : item;
                DragSource = item;
                DragFormat = DragData.GetType();

                DragScope = Application.Current.MainWindow.Content as UIElement;
                System.Diagnostics.Debug.Assert(DragScope != null);

                DragScope.AllowDrop = true;
                DragScope.QueryContinueDrag += new QueryContinueDragEventHandler(DragScope_QueryContinueDrag);
            }
        }

        private void OnMouseMove(Object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed && !this.IsDragging && (DragStartPoint != new Point(0.0, 0.0)))
            {
                Point position = e.GetPosition(null);

                if (Math.Abs(position.X - DragStartPoint.X) > SystemParameters.MinimumHorizontalDragDistance ||
                    Math.Abs(position.Y - DragStartPoint.Y) > SystemParameters.MinimumVerticalDragDistance)
                {
                    StartDrag(e);
                }
            }
        }

        private void StartDrag(MouseEventArgs e)
        {
            if (DragSource != null && DragFormat != null && DragData != null)
            {
                this.IsDragging = true;

                DataObject data = new DataObject(this.DragFormat, this.DragData);
                DragDropEffects effects = DragDrop.DoDragDrop(this.DragSource, data, DragDropEffects.All);
            }
        }

        void DragScope_QueryContinueDrag(Object sender, QueryContinueDragEventArgs e)
        {
            if (e.KeyStates == DragDropKeyStates.None || e.EscapePressed == true)
            {
                DragScope.QueryContinueDrag -= new QueryContinueDragEventHandler(DragScope_QueryContinueDrag);

                e.Action = DragAction.Cancel;
                this.DragFinished();
            }
        }

        void DragFinished()
        {
            this.IsDragging = false;
            System.Diagnostics.Debug.Print("Drag Finished");
        }

        #endregion // Drag & Drop

        /// <summary>
        /// Gets called when the user types in the simple search box
        /// </summary>
        private void OnSimpleSearchChanged(Object sender, TextChangedEventArgs e)
        {
            List<ISearchable> searchResult = null;
            if ((sender as TextBox).Text.Length > 0)
                searchResult = Search.SimpleSearchFirst(this.ViewModel.SelectedLevel.Model, (sender as TextBox).Text);

            if (searchResult != null && searchResult.Count > 0)
            {
                foreach (ISearchable searchable in searchResult)
                {
                    System.Diagnostics.Debug.Print(searchable.Name);
                }
                this.MapTreeView.ClearSelection();
                IHierarchicalViewModel previous = null;
                int index = 1;
                foreach (ISearchable searchable in searchResult)
                {
                    if (searchable is Level)
                    {
                        if (index++ != searchResult.Count)
                        {
                            this.ViewModel.ContentLevel[searchResult[0].Name].IsExpanded = true;
                        }
                        else
                        {
                            this.ViewModel.ContentLevel[searchResult[0].Name].IsSelected = true;
                        }
                        previous = this.ViewModel.ContentLevel[searchResult[0].Name];
                    }
                    else if (previous != null)
                    {
                        IHierarchicalViewModel viewModel =  previous.GetChildWithString(searchable.Name) as IHierarchicalViewModel;
                        if (viewModel != null)
                        {
                            if (index++ != searchResult.Count)
                            {
                                viewModel.IsExpanded = true;
                            }
                            else
                            {
                                viewModel.IsSelected = true;
                            }
                            previous = viewModel;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                System.Diagnostics.Debug.Print("Found first result");
            }
            else
            {
                System.Diagnostics.Debug.Print("Couldn't find first result");
            }
        }
    }

    /// <summary>
    /// Takes a number of collections and combines them into one collection, making sure we use the current 
    /// </summary>
    /// <values>
    /// [0] List - A list of types that have been registered to be displayed on the treeview/>
    /// [1] List - A list of user data types that we are allowed to show through filtering
    /// [2] BaseMapFiltering - The map area filter option, use to filter out some of the map sections (IMapSectionViewModel)
    /// [3] ObservableCollection - The collection of child objects to be put into the final list
    /// [4] ObservableCollection - The collection of use data to be put into the final list
    /// </values>
    public class CollectionFilterConverter : IMultiValueConverter
    {
        public Object Convert(Object[] values, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            ObservableCollection<Object> combinedCollection = new ObservableCollection<Object>();

            ListCollectionView children = null;
            if (values.Length > 0 && values[0] != null && values[0] != DependencyProperty.UnsetValue)
            {
                children = new ListCollectionView(values[0] as System.Collections.IList);
            }
            ListCollectionView userData = null;
            if (values.Length > 1 && values[1] != null && values[1] != DependencyProperty.UnsetValue)
            {
                userData = new ListCollectionView(values[1] as System.Collections.IList);
            }
            ListCollectionView objects = null;
            if (values.Length > 2 && values[2] != null && values[2] != DependencyProperty.UnsetValue)
            {
                objects = new ListCollectionView(values[2] as System.Collections.IList);
            }

            if (children != null)
            {
                foreach (var item in children)
                {
                    combinedCollection.Add(item);
                }
            }
            if (userData != null)
            {
                foreach (var item in userData)
                {
                    combinedCollection.Add(item);
                }
            }
            if (objects != null)
            {
                foreach (var item in objects)
                {
                    combinedCollection.Add(item);
                }
            }

            return combinedCollection;
        }

        public Object[] ConvertBack(Object value, Type[] targetTypes, Object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    } // CollectionFilterConverter

    public class MapSectionChildren : IMultiValueConverter
    {
        public Object Convert(Object[] values, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            ObservableCollection<Object> combinedCollection = new ObservableCollection<Object>();

            ListCollectionView children = null;
            if (values.Length > 0 && values[0] != null && values[0] != DependencyProperty.UnsetValue)
            {
                children = new ListCollectionView(values[0] as System.Collections.IList);
            }
            ListCollectionView objects = null;
            if (values.Length > 1 && values[1] != null && values[1] != DependencyProperty.UnsetValue)
            {
                objects = new ListCollectionView(values[1] as System.Collections.IList);
            }
            TextureDictionarySetViewModel dictionaries = null;
            if (values.Length > 2 && values[2] != null && values[2] != DependencyProperty.UnsetValue)
            {
                if (values[2] is TextureDictionarySetViewModel)
                {
                    dictionaries = values[2] as TextureDictionarySetViewModel;
                }
            }

            if (children != null)
            {
                foreach (var item in children)
                {
                    combinedCollection.Add(item);
                }
            }
            if (objects != null)
            {
                foreach (var item in objects)
                {
                    combinedCollection.Add(item);
                }
            }
            if (dictionaries != null)
            {
                combinedCollection.Add(dictionaries);
            }

            return combinedCollection;
        }

        public Object[] ConvertBack(Object value, Type[] targetTypes, Object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    } // MapSectionChildren

}
