﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn.UI.Layout;

namespace Workbench.AddIn.Services
{
    /// <summary>
    /// 
    /// </summary>
    public interface ISearchResult
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        object Data { get; }

        /// <summary>
        /// Represents whether the user can jump to the search result
        /// to see it in the view.
        /// </summary>
        bool CanJumpTo { get; }

        /// <summary>
        /// Represents what to show the user in the search results
        /// window for this result.
        /// </summary>
        string DisplayText { get; }

        /// <summary>
        /// A reference to the content
        /// the search result was found in.
        /// </summary>
        IContentBase Content { get; set; }
        #endregion
    } // ISearchResult
} // Workbench.AddIn.Services
