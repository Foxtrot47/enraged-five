﻿namespace MetadataEditor.AddIn.PedVariation.ViewModel
{
    using System.Collections.Generic;
    using RSG.Base.Editor;
    using System.Linq;
    using RSG.Metadata.Characters;

    /// <summary>
    /// 
    /// </summary>
    public class PropSetViewModel : ViewModelBase
    {
        #region Fields
        /// <summary>
        /// The private reference to the model data provider for this view model.
        /// </summary>
        private SelectionSet _selectionSet;

        /// <summary>
        /// The index for this particular component set inside the associated selection set.
        /// </summary>
        private int _index;

        /// <summary>
        /// The private field used for the <see cref="AvailableTextureNames"/> property.
        /// </summary>
        private SelectionSetCollectionViewModel _parent;

        /// <summary>
        /// A private dictionary used to map drawable indices to their random friendly name.
        /// </summary>
        internal static readonly Dictionary<byte, string> _anchorPoints = InitialiseAnchorPointNames();
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="PropSetViewModel"/> class.
        /// </summary>
        /// <param name="selectionSet">
        /// The model that provides the data for this view model.
        /// </param>
        /// <param name="index">
        /// The index for this prop set inside the associated selection set.
        /// </param>
        public PropSetViewModel(SelectionSet selectionSet, int index, SelectionSetCollectionViewModel parent)
        {
            this._parent = parent;
            this._selectionSet = selectionSet;
            this._index = index;

            byte anchorIndex = this._selectionSet.GetAnchorId(this._index);
            int drawableIndex = this._selectionSet.GetPropDrawableId(this._index);
            List<int> drawableIndices = this._parent.PropsForIndex(anchorIndex).Keys.ToList();
            this.SelectedDrawableIndex = drawableIndices.IndexOf(drawableIndex);

            byte textureIndex = this._selectionSet.GetPropTextureId(this._index);
            List<byte> textureIndices = this._parent.PropTextures(anchorIndex).Keys.ToList();
            this.SelectedTextureIndex = textureIndices.IndexOf(textureIndex);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the texture name the is associated with the index used for this
        /// component set.
        /// </summary>
        public int SelectedDrawableIndex
        {
            get
            {
                return this._selectedDrawableIndex;
            }

            set
            {
                this.SetPropertyValue(ref this._selectedDrawableIndex, value, "SelectedDrawableIndex");
                if (value == -1)
                {
                    this._selectionSet.SetPropDrawableId(this._index, 255);
                    return;
                }

                byte anchorIndex = this._selectionSet.GetAnchorId(this._index);
                List<int> drawableIndices = this._parent.PropsForIndex(anchorIndex).Keys.ToList();
                if (drawableIndices.Count <= value)
                {
                    this._selectionSet.SetPropDrawableId(this._index, 255);
                    return;
                }

                this._selectionSet.SetPropDrawableId(this._index, (byte)drawableIndices[value]);
            }
        }

        private int _selectedDrawableIndex;

        /// <summary>
        /// Gets or sets the texture name the is associated with the index used for this
        /// component set.
        /// </summary>
        public int SelectedTextureIndex
        {
            get
            {
                return this._selectedTextureIndex;
            }

            set
            {
                this.SetPropertyValue(ref this._selectedTextureIndex, value, "SelectedTextureIndex");
                if (value == -1)
                {
                    this._selectionSet.SetPropTextureId(this._index, 255);
                    return;
                }

                byte anchorIndex = this._selectionSet.GetAnchorId(this._index);
                List<byte> textureIndices = this._parent.PropTextures(anchorIndex).Keys.ToList();
                if (textureIndices.Count <= value)
                {
                    this._selectionSet.SetPropTextureId(this._index, 255);
                    return;
                }

                this._selectionSet.SetPropTextureId(this._index, textureIndices[value]);
            }
        }

        private int _selectedTextureIndex;

        /// <summary>
        /// Gets or sets the texture name the is associated with the index used for this
        /// component set.
        /// </summary>
        public string SelectedAnchorPoint
        {
            get
            {
                byte anchorIndex = this._selectionSet.GetAnchorId(this._index);
                return _anchorPoints[anchorIndex];
            }

            set
            {
                if (value == this.SelectedAnchorPoint)
                {
                    return;
                }

                if (value == "Empty" || value == null)
                {
                    this._selectionSet.SetAnchorId(this._index, 255);
                }
                else
                {
                    foreach (KeyValuePair<byte, string> kvp in _anchorPoints)
                    {
                        if (kvp.Value == value)
                        {
                            this._selectionSet.SetAnchorId(this._index, kvp.Key);
                            break;
                        }
                    }
                }

                this.SelectedTextureIndex = 0;
                this.SelectedDrawableIndex = 0;
                this.NotifyPropertyChanged("AvailableDrawableNames");
                this.NotifyPropertyChanged("AvailableTextureNames");
            }
        }

        /// <summary>
        /// Gets a iterator around the available names of the texture indices.
        /// </summary>
        public IEnumerable<string> AvailableDrawableNames
        {
            get
            {
                byte anchorIndex = this._selectionSet.GetAnchorId(this._index);
                return this._parent.PropsForIndex(anchorIndex).Values;
            }
        }

        /// <summary>
        /// Gets a iterator around the available names of the texture indices.
        /// </summary>
        public IEnumerable<string> AvailableTextureNames
        {
            get
            {
                byte anchorIndex = this._selectionSet.GetAnchorId(this._index);
                return this._parent.PropTextures(anchorIndex).Values;
            }
        }

        /// <summary>
        /// Gets a iterator around the available names of the texture indices.
        /// </summary>
        public IEnumerable<string> AvailableAnchorPoints
        {
            get
            {
                return this._parent.AvailableAnchorPoints;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Initialise the static dictionary containing the mapping between anchor indices
        /// and their friendly point names.
        /// </summary>
        /// <returns>
        /// A dictionary containing the mapping between anchor indices and their friendly
        /// point names.
        /// </returns> 
        private static Dictionary<byte, string> InitialiseAnchorPointNames()
        {
            Dictionary<byte, string> anchorPoints = new Dictionary<byte, string>();

            anchorPoints.Add(0, "ANCHOR_HEAD");
            anchorPoints.Add(1, "ANCHOR_EYES");
            anchorPoints.Add(2, "ANCHOR_EARS");
            anchorPoints.Add(3, "ANCHOR_MOUTH");
            anchorPoints.Add(4, "ANCHOR_LEFT_HAND");
            anchorPoints.Add(5, "ANCHOR_RIGHT_HAND");
            anchorPoints.Add(6, "ANCHOR_LEFT_WRIST");
            anchorPoints.Add(7, "ANCHOR_RIGHT_WRIST");
            anchorPoints.Add(8, "ANCHOR_HIP");
            anchorPoints.Add(9, "ANCHOR_LEFT_FOOT");
            anchorPoints.Add(10, "ANCHOR_RIGHT_FOOT");
            anchorPoints.Add(11, "ANCHOR_PH_L_HAND");
            anchorPoints.Add(12, "ANCHOR_PH_R_HAND");
            anchorPoints.Add(255, "Empty");

            return anchorPoints;
        }
        #endregion Methods
    } // MetadataEditor.AddIn.PedVariation.ViewModel.PropSetViewModel {Class}
} // MetadataEditor.AddIn.PedVariation.ViewModel {Namespace}
