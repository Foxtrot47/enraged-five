﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using RSG.Base.Math;
using System.Windows.Media.Imaging;

namespace Workbench.AddIn.MapViewport.MapSupport
{
    /// <summary>
    /// Map model.
    /// </summary>
    public class AlternativeMap
    {
        #region Public properties

        /// <summary>
        /// The name of the map.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The image's bounding box.
        /// </summary>
        public BoundingBox2f ImageBounds { get; private set; }

        /// <summary>
        /// The image.
        /// </summary>
        public BitmapImage Image { get; private set; }

        #endregion

        #region Private properties

        /// <summary>
        /// The location of the bounds path.
        /// </summary>
        private string BoundsPath { get; set; }

        /// <summary>
        /// The location of the image file.
        /// </summary>
        private string ImagePath { get; set; }

        #endregion

        #region Private constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        private AlternativeMap()
        {
            // Prevents users accidentally not using one of the static factory construction methods
        }

        #endregion

        #region Static methods

        /// <summary>
        /// Deserialize the map data.
        /// </summary>
        /// <param name="stream">Stream.</param>
        /// <returns>An instance of the map.</returns>
        public static AlternativeMap Deserialize(string pathToFile)
        {
            string folder = Path.GetDirectoryName(pathToFile);

            XmlDocument doc = new XmlDocument();
            doc.Load(pathToFile);

            AlternativeMap map = new AlternativeMap();
            map.Name = GetAttribute(doc, "//map", "name");
            map.ImagePath = GetAttribute(doc, "//image", "path");
            map.BoundsPath = GetAttribute(doc, "//bounds", "path");

            if (!Path.IsPathRooted(map.ImagePath))
            {
                map.ImagePath = Path.GetFullPath(Path.Combine(folder, map.ImagePath));
            }

            if (!Path.IsPathRooted(map.BoundsPath))
            {
                map.BoundsPath = Path.GetFullPath(Path.Combine(folder, map.BoundsPath));
            }

            map.Make();

            return map;
        }

        /// <summary>
        /// Create the alternative map from an image and bounds data.
        /// </summary>
        /// <param name="image">Image.</param>
        /// <param name="bounds">Bounding box.</param>
        /// <returns>Alternative map instance.</returns>
        public static AlternativeMap Create(BitmapImage image, BoundingBox2f bounds)
        {
            AlternativeMap map = new AlternativeMap();
            map.Image = image;
            map.ImageBounds = bounds;
            return map;
        }

        /// <summary>
        /// Get an attribute from a given node.
        /// </summary>
        /// <param name="doc">Xml document.</param>
        /// <param name="xpath">Xpath query.</param>
        /// <param name="attribute">Attribute name.</param>
        /// <returns>The value of the attribute.</returns>
        private static string GetAttribute(XmlDocument doc, string xpath, string attribute)
        {
            var node = doc.SelectSingleNode(xpath);
            if (node == null)
            {
                return String.Empty;
            }

            if (node.Attributes[attribute] == null)
            {
                return String.Empty;
            }

            return node.Attributes[attribute].Value;
        }

        #endregion

        #region Private helper methods

        /// <summary>
        /// Create the image, bounding box and load the vector points for the map.
        /// </summary>
        private void Make()
        {
            if (File.Exists(ImagePath))
            {
                Stream imageStreamSource = new FileStream(ImagePath, FileMode.Open, FileAccess.Read, FileShare.Read);

                BitmapImage bImg = new BitmapImage();
                bImg.BeginInit();
                bImg.StreamSource = imageStreamSource;
                bImg.EndInit();

                this.Image = bImg;
                this.Image.Freeze();
            }

            ImageBounds = null;

            if (!String.IsNullOrEmpty(BoundsPath) && File.Exists(BoundsPath))
            {
                ImageBounds = new BoundingBox2f();
                XmlDocument doc = new XmlDocument();
                doc.Load(BoundsPath);

                XmlNode lowerLeftXNode = doc.SelectSingleNode("/bounds/lowerleft/x");
                XmlNode lowerLeftYNode = doc.SelectSingleNode("/bounds/lowerleft/y");
                ImageBounds.Min = new Vector2f(Single.Parse(lowerLeftXNode.InnerText), Single.Parse(lowerLeftYNode.InnerText));

                XmlNode upperRightXNode = doc.SelectSingleNode("/bounds/upperright/x");
                XmlNode upperRightYNode = doc.SelectSingleNode("/bounds/upperright/y");
                ImageBounds.Max = new Vector2f(Single.Parse(upperRightXNode.InnerText), Single.Parse(upperRightYNode.InnerText));
            }
        }

        #endregion
    }
}
