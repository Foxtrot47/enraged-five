﻿using Workbench.AddIn;
using System;

namespace Workbench.AddIn
{

    /// <summary>
    /// 
    /// </summary>
    public static class CompositionPoints
    {
        /// <summary>
        /// Workbench Core Services Provider.
        /// </summary>
        public const String CoreServicesProvider =
            "95BA69BD-CF89-46DE-834D-BE074FF7DF92";

        #region Services
        /// <summary>
        /// Workbench web browser service.
        /// </summary>
        public const String WebBrowserService =
            "972F1FC3-4431-4737-88C0-1DF9F34DC5DD";

        /// <summary>
        /// Workbench messaging service.
        /// </summary>
        public const String MessageService =
            "575829DF-214E-4A32-9D09-2B2D5D783678";

        /// <summary>
        /// Workbench progress information service.
        /// </summary>
        public const String ProgressService =
            "53B2B3B5-2F20-4401-9F65-675058166F45";

        /// <summary>
        /// ExtensionService extension.
        /// </summary>
        public const String ExtensionService =
            "ADA62BA0-3265-4CD9-8E49-BCC9753F67B2";

        /// <summary>
        /// Application arguments extension.
        /// </summary>
        public const String ApplicationArgumentsService =
            "83705BB4-36FC-4A93-B18F-7C68828B6332";

        /// <summary>
        /// Undo/redo workbench management service.
        /// </summary>
        public const String UndoRedoService =
            "6F49385D-D3BB-4556-8512-965AFBD36FE4";

        /// <summary>
        /// Workbench model selection service.
        /// </summary>
        public const String SelectionService =
            "2E9FED9E-2068-431D-B740-2748C9D77936";

        /// <summary>
        /// Workbench and game configuration service.
        /// </summary>
        public const String ConfigurationService =
            "5755A811-E85A-4A55-8B82-FBCB85B4D436";

        /// <summary>
        /// The workbench property inspector
        /// </summary>
        public const String PropertyInspectorService =
            "B9F1791F-5F4C-40E9-8809-A63A0D7F58BF";

        /// <summary>
        /// 
        /// </summary>
        public const String PerforceSyncService =
            "1DEBEEFF-4CDE-4E64-8EED-8D79F05C7952";

        /// <summary>
        /// 
        /// </summary>
        public const String ExternalToolService =
            "E5BCB0AC-D197-4581-A7F0-9D6D2231927E";

        /// <summary>
        /// 
        /// </summary>
        public const String WorkbenchFileManager =
            "92E2E3A0-5F13-4DDC-A5E7-AC2AE8C1F272";

        /// <summary>
        /// 
        /// </summary>
        public const String WorkbenchOpenService =
            "EDA8F2A8-C7D8-4910-9031-36B5B55D2A3C";
        
        /// <summary>
        /// 
        /// </summary>
        public const String WorkbenchSaveService =
            "AFF97EE1-F028-4B20-AAAD-3D74A9D9880E";

        /// <summary>
        /// 
        /// </summary>
        public const String LoginService =
            "DDC14FB1-1D78-4EDE-98D6-82025C788C9B";
        
        /// <summary>
        /// 
        /// </summary>
        public const String WebService =
            "349BAAFC-BCB2-4F71-87A2-1F7F9C678169";

        /// <summary>
        /// Bugstar Service.
        /// </summary>
        public const String BugstarService =
            "3F60FC75-7088-4F79-918C-EE61D5B4D148";

        /// <summary>
        /// Task progress service.
        /// </summary>
        public const String TaskProgressService =
            "712D45FD-7A7A-4df6-912C-0E83E4C0265F";

        /// <summary>
        /// Model data provider.
        /// </summary>
        public const String ModelDataProvider =
            "3876B07C-F9C8-405D-9536-99531E1FBE5A";

        /// <summary>
        /// 
        /// </summary>
        public const String DataSourceBrowser =
            "7F3250F8-E2BA-47BB-8A42-8C437C981E95";

        /// <summary>
        /// 
        /// </summary>
        public const String BuildBrowser =
            "E8BBB066-A9CA-4DD1-9B89-ACDA1D6F91D5";

        /// <summary>
        /// The point to use to get the level browser
        /// </summary>
        public const String LevelBrowser =
            "65AD7380-57F3-456D-BCF0-CED9F7408B42";

        /// <summary>
        /// 
        /// </summary>
        public const String PlatformBrowser =
            "9569B86E-50E7-4489-A5E0-ED8497D484CF";

        /// <summary>
        /// Data context provider.
        /// </summary>
        public const String DataContextProvider =
            "D7A09DD3-0813-43A0-A880-E5E25DAD51DC";

        #endregion // Services

#warning DHM FIX ME: should be moved somewhere for internal use - so we can't import it
        #region Core Services
        /// <summary>
        /// User-Interface Service.
        /// </summary>
        public const String UserInterfaceService =
            "F8D36F31-950F-4B72-BB1E-CE4190E916F2";

        /// <summary>
        /// Workbench layout management service.
        /// </summary>
        public const String LayoutManager =
            "186B9586-B392-4706-A125-FF9196544A41";
        #endregion // Core Services

        #region Menus
        /// <summary>
        /// File new menu item.
        /// </summary>
        public const String MenuFileNew =
            "E6A1B4B5-683F-4D78-93CC-39F47974CB0B";

        /// <summary>
        /// File open menu item.
        /// </summary>
        public const String MenuFileOpen =
            "5730E254-94EE-420B-BB28-DE1C1FBD80F6";

        /// <summary>
        /// File save menu item.
        /// </summary>
        public const String MenuFileSave =
            "26375279-277E-45AC-B73F-552240DE9DC4";

        /// <summary>
        /// File save as menu item.
        /// </summary>
        public const String MenuFileSaveAs =
        "8A651A9D-0209-49FB-9AA3-EB34767C2E57";

        /// <summary>
        /// File save all menu item.
        /// </summary>
        public const String MenuFileSaveAll =
            "BE54D3B9-93C3-4DEE-A024-E81444C32911";

        /// <summary>
        /// File close menu item.
        /// </summary>
        public const String MenuFileClose =
            "113F3CD5-DAAC-47BD-98B1-4E530C840E40";

        /// <summary>
        /// File close menu item.
        /// </summary>
        public const String MenuFileCloseAll =
            "9DB33C25-3D6F-4C69-86F4-9771FBDAE73F";

        /// <summary>
        /// File recent files menu item.
        /// </summary>
        public const String MenuFileRecentFiles =
            "63BAA43C-9964-4039-AAFE-DF5A213C4E1A";

        /// <summary>
        /// File exit menu item.
        /// </summary>
        public const String MenuFileExit =
            "EC1DF0B8-5B3F-4028-9EC9-E9D1C04E67D4";

        /// <summary>
        /// Window menu, Tool windows submenu.
        /// </summary>
        public const String MenuWindowToolWindows =
            "BF5FC34A-2E85-4D77-BFF3-17EF2F49130E";
        #endregion // Menus

        #region Tool Bars

        /// <summary>
        /// Separate before tool window toggle buttons
        /// </summary>
        public const String ToolWindowSeparator = "1BD16885-7613-49B1-B25D-0110732E68E5";

        #endregion // Tool Bars
    }
    
} // Workbench.AddIn namespace
