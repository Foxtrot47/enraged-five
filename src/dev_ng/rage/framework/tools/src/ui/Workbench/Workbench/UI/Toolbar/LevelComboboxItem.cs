﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn.Commands;
using RSG.Model.Common;

namespace Workbench.UI.Toolbar
{
    /// <summary>
    /// 
    /// </summary>
    public class LevelComboboxItem : WorkbenchCommandLabel
    {
        #region Constants
        /// <summary>
        /// 
        /// </summary>
        public static readonly Guid GUID = new Guid("1CC22442-E477-4B80-9F06-FE8C403E21BD");
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Level this label is for
        /// </summary>
        public ILevel Level
        {
            get;
            protected set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="platform"></param>
        public LevelComboboxItem(ILevel level)
            : base()
        {
            this.ID = GUID;
            this.Header = level.Name;
            this.Level = level;
        }
        #endregion // Constructor(s)
    } // ContentBrowserToolbarLevelItem
}
