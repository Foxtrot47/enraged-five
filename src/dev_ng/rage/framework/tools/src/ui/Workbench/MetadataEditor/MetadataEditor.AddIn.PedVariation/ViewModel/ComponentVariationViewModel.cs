﻿using System.Collections.Generic;
using RSG.Base.Editor;
using RSG.Metadata.Characters;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using RSG.Metadata.Data;
using MetadataEditor.AddIn.PedVariation.View;
using RSG.Metadata.Parser;
using System.Linq;
using System;
using System.Globalization;

namespace MetadataEditor.AddIn.PedVariation.ViewModel
{
    /// <summary>
    /// 
    /// </summary>
    public class ComponentVariationViewModel : ViewModelBase
    {
        #region Classes
        /// <summary>
        /// 
        /// </summary>
        public class FloatExpression : ViewModelBase
        {
            #region Properties
            /// <summary>
            /// The variation model that this expression viewmodel
            /// is attached to.
            /// </summary>
            private PedComponentVariation Model
            {
                get;
                set;
            }

            /// <summary>
            /// The value of this user expression
            /// </summary>
            public float Value
            {
                get { return Model.Expressions[ExpressionIndex]; }
                set 
                { 
                    Model.SetUserExpressionIdentifier(value, ExpressionIndex);
                    OnPropertyChanged("Value");
                }
            }

            /// <summary>
            /// The index of this expression in the expression
            /// array.
            /// </summary>
            private int ExpressionIndex
            {
                get;
                set;
            }
            #endregion

            #region Constructor
            /// <summary>
            /// Initialises a new instance of the 
            /// <see cref="FloatExpression"/> class.
            /// </summary>
            /// <param name="value"></param>
            /// <param name="index"></param>
            /// <param name="model"></param>
            public FloatExpression(float value, int index, PedComponentVariation model)
            {
                this.ExpressionIndex = index;
                this.Model = model;
            }
            #endregion
        }

        /// <summary>
        /// 
        /// </summary>
        public class NewBitFlag : ViewModelBase
        {
            #region Constants
            private static List<string> FlagNames = new List<string>()
            {
                "Bulky",
                "Job",
                "Sunny",
                "Wet",
                "Cold",
                "Not In Car",
                "Bike Only",
                "Not Indoors",
                "Fire Retardent",
                "Armoured",
                "Lightly Armoured",
                "High Detail",
                "Default Helmet",
                "Random Helmet",
                "Script Helmet",
                "Flight Helmet",
                "Hide In First Person",
                "Use Physics Hat",
                "Wet More Wet",
                "Wet Less Wet",
                "Pilot Helmet",
            };

            private static Dictionary<string, string> FlagNameLookup = new Dictionary<string, string>
            {
                {"PV_FLAG_BULKY", "Bulky"},
                {"PV_FLAG_JOB", "Job"},
                {"PV_FLAG_SUNNY", "Sunny"},
                {"PV_FLAG_WET", "Wet"},
                {"PV_FLAG_COLD", "Cold"},
                {"PV_FLAG_NOT_IN_CAR", "Not In Car"},
                {"PV_FLAG_BIKE_ONLY", "Bike Only"},
                {"PV_FLAG_NOT_INDOORS", "Not Indoors"},
                {"PV_FLAG_FIRE_RETARDENT", "Fire Retardent"},
                {"PV_FLAG_ARMOURED", "Armoured"},
                {"PV_FLAG_LIGHTLY_ARMOURED", "Lightly Armoured"},
                {"PV_FLAG_HIGH_DETAIL", "High Detail"},
                {"PV_FLAG_DEFAULT_HELMET", "Default Helmet"},
                {"PV_FLAG_RANDOM_HELMET", "Random Helmet"},
                {"PV_FLAG_SCRIPT_HELMET", "Script Helmet"},
                {"PV_FLAG_FLIGHT_HELMET", "Flight Helmet"},
                {"PV_FLAG_HIDE_IN_FIRST_PERSON", "Hide In First Person"},
                {"PV_FLAG_USE_PHYSICS_HAT_2", "Use Physics Hat"},
                {"PV_FLAG_WET_MORE_WET", "Wet More Wet"},
                {"PV_FLAG_WET_LESS_WET", "Wet Less Wet"},
                {"PV_FLAG_PILOT_HELMET", "Pilot Helmet"},
            };
            #endregion

            #region Properties
            /// <summary>
            /// The variation model that this bit flag viewmodel
            /// is attached to.
            /// </summary>
            private IFlagVariation Model
            {
                get;
                set;
            }

            /// <summary>
            /// The value of this bit flag
            /// </summary>
            public bool Value
            {
                get { return Model.GetNewFlag(FlagIndex); }
                set
                {
                    Model.SetNewFlag(FlagIndex, value);
                    OnPropertyChanged("Value");
                }
            }

            /// <summary>
            /// The index of this expression in the expression
            /// array.
            /// </summary>
            private int FlagIndex
            {
                get;
                set;
            }

            /// <summary>
            /// The name of this bit flag.
            /// </summary>
            public string Name
            {
                get
                {
                    string name = null;
                    if (!FlagNameLookup.TryGetValue(this._name, out name))
                    {
                        return this._name;
                    }

                    return name;
                }
            }
            private string _name;
            #endregion

            #region Constructor
            /// <summary>
            /// Initialises a new instance of the 
            /// <see cref="FloatExpression"/> class.
            /// </summary>
            /// <param name="index"></param>
            /// <param name="model"></param>
            public NewBitFlag(int index, string flagName, IFlagVariation model)
            {
                this._name = flagName;
                this.FlagIndex = index;
                this.Model = model;
            }
            #endregion
        }

        /// <summary>
        /// 
        /// </summary>
        public class BitFlag : ViewModelBase
        {
            #region Constants
            private static List<string> FlagNames = new List<string>()
            {
                "Bulky",
                "Job",
                "Sunny",
                "Wet",
                "Cold",
                "Not In Car",
                "Bike Only",
                "Not Indoors",
                "Fire Retardent",
                "Armoured",
                "Lightly Armoured",
                "High Detail",
                "Default Helmet",
                "Random Helmet",
                "Script Helmet",
                "Flight Helmet"
            };

            private static Dictionary<string, string> FlagNameLookup = new Dictionary<string, string>
            {
                {"PV_FLAG_BULKY", "Bulky"},
                {"PV_FLAG_JOB", "Job"},
                {"PV_FLAG_SUNNY", "Sunny"},
                {"PV_FLAG_WET", "Wet"},
                {"PV_FLAG_COLD", "Cold"},
                {"PV_FLAG_NOT_IN_CAR", "Not In Car"},
                {"PV_FLAG_BIKE_ONLY", "Bike Only"},
                {"PV_FLAG_NOT_INDOORS", "Not Indoors"},
                {"PV_FLAG_FIRE_RETARDENT", "Fire Retardent"},
                {"PV_FLAG_ARMOURED", "Armoured"},
                {"PV_FLAG_LIGHTLY_ARMOURED", "Lightly Armoured"},
                {"PV_FLAG_HIGH_DETAIL", "High Detail"},
                {"PV_FLAG_DEFAULT_HELMET", "Default Helmet"},
                {"PV_FLAG_RANDOM_HELMET", "Random Helmet"},
                {"PV_FLAG_SCRIPT_HELMET", "Script Helmet"},
                {"PV_FLAG_FLIGHT_HELMET", "Flight Helmet"},
                {"PV_FLAG_HIDE_IN_FIRST_PERSON", "Hide In First Person"},
                {"PV_FLAG_USE_PHYSICS_HAT_2", "Use Physics Hat"},
                {"PV_FLAG_WET_MORE_WET", "Wet More Wet"},
                {"PV_FLAG_WET_LESS_WET", "Wet Less Wet"},
                {"PV_FLAG_PILOT_HELMET", "Pilot Helmet"},
            };
            #endregion

            #region Properties
            /// <summary>
            /// The variation model that this bit flag viewmodel
            /// is attached to.
            /// </summary>
            private IFlagVariation Model
            {
                get;
                set;
            }

            /// <summary>
            /// The value of this bit flag
            /// </summary>
            public bool Value
            {
                get { return Model.GetFlag(FlagIndex); }
                set 
                { 
                    Model.SetFlag(FlagIndex, value);
                    OnPropertyChanged("Value");
                }
            }

            /// <summary>
            /// The index of this expression in the expression
            /// array.
            /// </summary>
            private int FlagIndex
            {
                get;
                set;
            }

            /// <summary>
            /// The name of this bit flag.
            /// </summary>
            public string Name
            {
                get
                {
                    string name = null;
                    if (!FlagNameLookup.TryGetValue(this._name, out name))
                    {
                        return this._name;
                    }

                    return name;
                }
            }
            private string _name;
            #endregion

            #region Constructor
            /// <summary>
            /// Initialises a new instance of the 
            /// <see cref="FloatExpression"/> class.
            /// </summary>
            /// <param name="value"></param>
            /// <param name="index"></param>
            /// <param name="model"></param>
            public BitFlag(bool value, int index, string flagName, IFlagVariation model)
            {
                this._name = flagName;
                this.FlagIndex = index;
                this.Model = model;
            }
            #endregion
        }

        /// <summary>
        /// 
        /// </summary>
        public class PropMask : ViewModelBase
        {
            #region Constants
            private static List<string> FlagNames = new List<string>()
            {
                "PV_DRAWBL_ACTIVE",
                "PV_DRAWBL_ALPHA",
                "PV_DRAWBL_DECAL",
                "PV_DRAWBL_CUTOUT",
                "PV_DRAWBL_RACE_TEX",
                "PV_DRAWBL_MATCH_PREV",
                "PV_DRAWBL_PALETTE",
                "PV_DRAWBL_PROXY_TEX"
            };
            #endregion

            #region Properties
            /// <summary>
            /// The variation model that this bit flag viewmodel
            /// is attached to.
            /// </summary>
            private IPropMaskVariation Model
            {
                get;
                set;
            }

            /// <summary>
            /// The value of this bit flag
            /// </summary>
            public bool Value
            {
                get { return Model.GetPropMaskFlag(FlagIndex); }
                set
                {
                    Model.SetPropMaskFlag(FlagIndex, value);
                    OnPropertyChanged("Value");
                }
            }

            /// <summary>
            /// The index of this expression in the expression
            /// array.
            /// </summary>
            private int FlagIndex
            {
                get;
                set;
            }

            /// <summary>
            /// The name of this bit flag.
            /// </summary>
            public string Name
            {
                get { return FlagNames[FlagIndex]; }
            }
            #endregion

            #region Constructor
            /// <summary>
            /// Initialises a new instance of the 
            /// <see cref="FloatExpression"/> class.
            /// </summary>
            /// <param name="value"></param>
            /// <param name="index"></param>
            /// <param name="model"></param>
            public PropMask(bool value, int index, IPropMaskVariation model)
            {
                this.FlagIndex = index;
                this.Model = model;
            }
            #endregion
        }

        /// <summary>
        /// 
        /// </summary>
        public class TextureVariationViewModel : ViewModelBase
        {
            #region Fields
            /// <summary>
            /// Private member for the distribution value to support
            /// conversion between float and integer.
            /// </summary>
            private float m_distribution;
            /// <summary>
            /// Flag to tell the view model not to update the internal
            /// distribution value when the model changes.
            /// </summary>
            private bool m_syncingDistribution;
            #endregion
            
            #region Properties
            /// <summary>
            /// The variation model that this expression viewmodel
            /// is attached to.
            /// </summary>
            private ComponentTextureVariation Model
            {
                get;
                set;
            }

            /// <summary>
            /// The identifiers for all of the texture variations.
            /// </summary>
            public int TextureIdentifier
            {
                get { return Model.TextureIdentifier; }
                set
                {
                    Model.SetTextureIdentifier(value);
                    OnPropertyChanged("TextureIdentifier");
                }
            }

            /// <summary>
            /// The distribution rate for this variation (spawn probability).
            /// This is represented with a float value where as in the model
            /// it is a integer so a conversion is needed.
            /// </summary>
            public float Distribution
            {
                get { return m_distribution; }
                set
                {
                    m_distribution = value;
                    m_syncingDistribution = true;
                    Model.SetDistribution((int)System.Math.Round(value * 2.55f));
                    m_syncingDistribution = false;
                    OnPropertyChanged("Distribution");
                }
            }

            /// <summary>
            /// The index of this expression in the expression
            /// array.
            /// </summary>
            public int TextureIndex
            {
                get;
                set;
            }
            #endregion

            #region Constructor
            /// <summary>
            /// Initialises a new instance of the 
            /// <see cref="TextureVariationViewModel"/> class.
            /// </summary>
            /// <param name="value"></param>
            /// <param name="index"></param>
            /// <param name="model"></param>
            public TextureVariationViewModel(int index, ComponentTextureVariation model)
            {
                this.TextureIndex = index;
                this.Model = model;
                m_distribution = Model.Distribution / 2.55f;

                model.TextureIdentifierChanged += (s, e) => { OnPropertyChanged("TextureIdentifier"); };
                model.DistributionValueChanged += (s, e) => 
                {
                    if (!m_syncingDistribution)
                        m_distribution = Model.Distribution / 2.55f;
                    OnPropertyChanged("Distribution");
                };
            }
            #endregion
        }
        #endregion

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        private PedComponentVariation Model
        {
            get;
            set;
        }

        /// <summary>
        /// The name of this variation
        /// </summary>
        public string Name
        {
            get { return Model.Name; }
        }

        /// <summary>
        /// The index of the component that this is a 
        /// variation of.
        /// </summary>
        public int ComponentIndex
        {
            get { return Model.ComponentIndex; }
        }

        /// <summary>
        /// The index of the variation for the specific component
        /// this variation represents.
        /// </summary>
        public int VariationIndex
        {
            get { return Model.VariationIndex; }
        }

        /// <summary>
        /// The id to specify the inclusion set for this variation.
        /// </summary>
        public int InclusionIdentifier
        {
            get { return Model.InclusionIdentifier; }
            set
            {
                Model.SetInclusionIdentifier(value);
                OnPropertyChanged("InclusionIdentifier");
            }
        }

        /// <summary>
        /// The id to specify the exclusion set for this variation.
        /// </summary>
        public int ExclusionIdentifier
        {
            get { return Model.ExclusionIdentifier; }
            set
            {
                Model.SetExclusionIdentifier(value);
                OnPropertyChanged("ExclusionIdentifier");
            }
        }

        public BitsetTunableViewModel InclusionSet
        {
            get { return this.inclusionSet; }
            set
            {
                this.inclusionSet = value;
                OnPropertyChanged("InclusionSet");
            }
        }
        private BitsetTunableViewModel inclusionSet;


        public BitsetTunableViewModel ExclusionSet
        {
            get { return this.exclusionSet; }
            set
            {
                this.exclusionSet = value;
                OnPropertyChanged("ExclusionSet");
            }
        }
        private BitsetTunableViewModel exclusionSet;

        /// <summary>
        /// The id to specify the audio setting for this variation.
        /// </summary>
        public string AudioIdentifier
        {
            get { return Model.FirstAudioIdentifier; }
            set
            {
                Model.SetFirstAudioIdentifier(value);
                OnPropertyChanged("AudioIdentifier");
            }
        }

        /// <summary>
        /// The id to specify the audio setting for this variation.
        /// </summary>
        public string SecondAudioIdentifier
        {
            get { return Model.SecondAudioIdentifier; }
            set
            {
                Model.SetSecondAudioIdentifier(value);
                OnPropertyChanged("SecondAudioIdentifier");
            }
        }

        /// <summary>
        /// The flag that represents whether this variation is cloth.
        /// </summary>
        public bool HasClothFlag
        {
            get { return Model.HasCloth; }
            set
            {
                Model.SetHasClothFlag(value);
                OnPropertyChanged("HasClothFlag");
            }
        }

        /// <summary>
        /// Gets or sets a set of values the audio identifier can be set to.
        /// </summary>
        public string[] ValidAudioIdentifiers
        {
            get { return this.Model.ValidAudioValues; }
        }

        /// <summary>
        /// An array of a specific type that represents the bit
        /// fields that can be set.
        /// </summary>
        public ObservableCollection<PropMask> PropMasks
        {
            get;
            set;
        }
        
        /// <summary>
        /// An array of a specific type that represents the user
        /// expressions.
        /// </summary>
        public ObservableCollection<FloatExpression> Expressions
        {
            get;
            set;
        }

        /// <summary>
        /// An array of a specific type that represents the bit
        /// fields that can be set.
        /// </summary>
        public ObservableCollection<BitFlag> Flags
        {
            get;
            set;
        }

        /// <summary>
        /// An array of a specific type that represents the bit
        /// fields that can be set.
        /// </summary>
        public ObservableCollection<NewBitFlag> NewFlags
        {
            get;
            set;
        }

        /// <summary>
        /// An array of a specific type that represents the bit
        /// fields that can be set.
        /// </summary>
        public List<TextureVariationViewModel> Textures
        {
            get;
            set;
        }

        /// <summary>
        /// Gets a value that indicates whether this view model is in a odd position in
        /// its list.
        /// </summary>
        public bool IsOdd
        {
            get;
            private set;
        }

        public BitsetTunableViewModel VfxComponents
        {
            get;
            set;
        }
        #endregion

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        public ComponentVariationViewModel(PedComponentVariation model, bool isOdd)
        {
            this.Model = model;
            this.IsOdd = isOdd;

            this.VfxComponents = new BitsetTunableViewModel(model.VfxComponents, false);
            this.InclusionSet = new BitsetTunableViewModel(model.InclusionSetTunable, true);
            this.ExclusionSet = new BitsetTunableViewModel(model.ExclusionSetTunable, true);
            this.Expressions = new ObservableCollection<FloatExpression>();
            this.Textures = new List<TextureVariationViewModel>();
            this.Flags = new ObservableCollection<BitFlag>();
            this.NewFlags = new ObservableCollection<NewBitFlag>();
            this.PropMasks = new ObservableCollection<PropMask>();
            CreateExpressions();
            CreateFlags();
            CreateNewFlags();
            CreatePropMasks();

            int index = 0;
            foreach (ITextureVariation texture in model.TextureVariations)
            {
                if (texture is ComponentTextureVariation)
                {
                    Textures.Add(new TextureVariationViewModel(index++, texture as ComponentTextureVariation));
                }
            }

            model.FirstAudioIdentifierChanged += (s, e) => { OnPropertyChanged("AudioIdentifier"); };
            model.SecondAudioIdentifierChanged += (s, e) => { OnPropertyChanged("SecondAudioIdentifier"); };
            model.FlagValueChanged += (s, e) => { CreateFlags(); OnPropertyChanged("Flags"); };
            model.NewFlagValueChanged += (s, e) => { CreateNewFlags(); OnPropertyChanged("NewFlags"); };
            model.PropMaskValueChanged += (s, e) => { CreatePropMasks(); OnPropertyChanged("PropMask"); };
            model.ExpressionValueChanged += (s, e) => { CreateExpressions(); OnPropertyChanged("Expressions"); };
        }
        #endregion

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        private void CreateFlags()
        {
            this.Flags.Clear();
            for (int i = 0; i < this.Model.FlagCount; i++)
            {
                this.Flags.Add(new BitFlag(Model.GetFlag(i), i, this.Model.ComponentFlagName(i), Model));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void CreateNewFlags()
        {
            this.NewFlags.Clear();
            for (int i = 0; i < this.Model.NewFlagCount; i++)
            {
                this.NewFlags.Add(new NewBitFlag(i, this.Model.ComponentFlagName(i), Model));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void CreatePropMasks()
        {
            this.PropMasks.Clear();
            for (int i = 0; i < this.Model.PropMaskCount; i++)
            {
                this.PropMasks.Add(new PropMask(Model.GetPropMaskFlag(i), i, Model));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void CreateExpressions()
        {
            this.Expressions.Clear();
            for (int i = 0; i < Model.ExpressionCount; i++)
            {
                this.Expressions.Add(new FloatExpression(Model.Expressions[i], i, Model));
            }
        }
        #endregion
    } // ComponentVariationViewModel

    public class BitsetTunableValueViewModel :
        ViewModelBase, IComboWithCheckboxesItem
    {
        #region Properties

        public string DisplayName
        {
            get;
            set;
        }

        public bool IsChecked
        {
            get { return m_isChecked; }
            set
            {
                if (value == m_isChecked)
                    return;

                m_isChecked = value;
                OnPropertyChanged("IsChecked");
                this.Parent.ChildInclusionChanged();
            }
        }
        private bool m_isChecked;

        BitsetTunableViewModel Parent
        {
            get;
            set;
        }

        #endregion // Properties

        #region Constructor

        public BitsetTunableValueViewModel(BitsetTunableViewModel parent, string displayName, bool isChecked)
        {
            this.m_isChecked = isChecked;
            this.Parent = parent;
            this.DisplayName = displayName;
        }

        #endregion // Constructor
    }

    public class BitsetTunableViewModel : ViewModelBase, System.Windows.IWeakEventListener
    {
        #region Properties
        private BitsetTunable Model
        {
            get;
            set;
        }

        public ObservableCollection<BitsetTunableValueViewModel> Values
        {
            get;
            set;
        }

        public bool _updatingTunableValue = false;

        public string DisplayText
        {
            get { return m_displayText; }
            set
            {
                m_displayText = value;
                OnPropertyChanged("DisplayText");
            }
        }
        private string m_displayText;

        #endregion // Properties

        #region Constructor
        public BitsetTunableViewModel(BitsetTunable t, bool useIndexForDisplay)
        {
            this.Model = t;
            if (t == null)
            {
                return;
            }

            System.ComponentModel.PropertyChangedEventManager.AddListener(t, this, "Value");
            this.Values = new ObservableCollection<BitsetTunableValueViewModel>();

            BitsetMember bitsetMemeber = t.Definition as BitsetMember;
            if (!useIndexForDisplay)
            {
                if (bitsetMemeber.Values == null)
                {
                    this.ChildInclusionChanged();
                    return;
                }

                IEnumerable<string> keys = from k in bitsetMemeber.Values.Keys
                                           orderby k ascending
                                           select k;

                foreach (string bitsetValue in keys)
                {
                    bool isChecked = false;
                    if (t.Value != null && t.Value.Contains(bitsetValue))
                    {
                        string[] valueParts = t.Value.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        if (valueParts.Contains(bitsetValue))
                        {
                            isChecked = true;
                        }
                    }

                    BitsetTunableValueViewModel newValue = new BitsetTunableValueViewModel(this, bitsetValue, isChecked);
                    this.Values.Add(newValue);
                }
            }
            else
            {
                string tunableValue = t.Value;
                uint value = 0;
                if (!uint.TryParse(tunableValue, out value))
                {
                    if (tunableValue.StartsWith("0x", true, CultureInfo.CurrentCulture))
                    {
                        tunableValue = tunableValue.Substring(2);
                        uint.TryParse(tunableValue, NumberStyles.HexNumber, CultureInfo.CurrentCulture, out value);
                    }
                }

                for (int i = 0; i < 32; i++)
                {
                    bool isChecked = false;
                    if ((value & (1 << i)) != 0)
                    {
                        isChecked = true;
                    }

                    this.Values.Add(new BitsetTunableValueViewModel(this, (i + 1).ToString(), isChecked));
                }
            }

            this.ChildInclusionChanged();
        }
        #endregion // Constructor

        #region IWeakEventListener
        /// <summary>
        /// true if the listener handled the event. It is considered an error by the
        /// System.Windows.WeakEventManager handling in WPF to register a listener for 
        /// an event that the listener does not handle. Regardless, the method should
        /// return false if it receives an event that it does not recognize or handle.
        /// </summary>
        public bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
        {
            if (managerType == typeof(System.ComponentModel.PropertyChangedEventManager))
            {
                if (_updatingTunableValue == true)
                    return true;
                else
                {
                    // Property on model changed, need to update the view models.
                    if (sender is BitsetTunable)
                    {
                        BitsetTunable bitsetTunable = sender as BitsetTunable;
                        string[] setValues = new string[] { };
                        if (!string.IsNullOrEmpty(bitsetTunable.Value))
                        {
                            setValues = bitsetTunable.Value.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                        }

                        if ((bitsetTunable.Definition as BitsetMember).Values != null)
                        {
                            this._updatingTunableValue = true;
                            foreach (BitsetTunableValueViewModel vm in this.Values)
                            {
                                vm.IsChecked = false;
                                if (setValues.Contains(vm.DisplayName))
                                {
                                    vm.IsChecked = true;
                                }
                            }
                            this._updatingTunableValue = false;
                        }

                        string value = string.Empty;
                        int index = 0;
                        foreach (BitsetTunableValueViewModel bitsetValue in this.Values)
                        {
                            if (bitsetValue.IsChecked == true)
                            {
                                if (index == 0)
                                {
                                    value += bitsetValue.DisplayName;
                                }
                                else
                                {
                                    value += " | " + bitsetValue.DisplayName;
                                }
                                index++;
                            }
                        }
                        this.DisplayText = value;
                    }
                }
            }

            return true;
        }
        #endregion // IWeakEventListener

        #region Methods

        public void ChildInclusionChanged()
        {
            if (this._updatingTunableValue == true)
                return;

            String value = String.Empty;
            int index = 0;
            uint bitValue = 0;
            foreach (BitsetTunableValueViewModel bitsetValue in this.Values)
            {
                if (bitsetValue.IsChecked == true)
                {
                    uint bitOffset = (uint)(1 << index);
                    bitValue |= bitOffset;

                    if (index == 0)
                    {
                        value += bitsetValue.DisplayName;
                    }
                    else
                    {
                        value += " " + bitsetValue.DisplayName;
                    }
                }

                index++;
            }

            this._updatingTunableValue = true;
            BitsetMember bitsetMemeber = this.Model.Definition as BitsetMember;
            if (bitsetMemeber.Values != null)
            {
                this.Model.Value = value;
            }
            else
            {
                this.Model.Value = "0x" + bitValue.ToString("X8");
            }

            this._updatingTunableValue = false;
        }
        #endregion // Methods
    }
} // MetadataEditor.AddIn.PedVariation.ViewModel
