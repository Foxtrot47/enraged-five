﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ContentBrowser.ViewModel
{
    /// <summary>
    /// Used to create a dummy view model inside a view
    /// model container to provide a method for streaming
    /// data on expansion
    /// </summary>
    class AssetDummyViewModel : AssetViewModelBase
    {
        #region Properties
        /// <summary>
        /// The main name property which by default
        /// will be shown as the header for a browser item
        /// </summary>
        public override String Name
        {
            get { return m_name; }
        }
        private String m_name;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public AssetDummyViewModel()
            : this("Dummy")
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public AssetDummyViewModel(String name)
            : base(null)
        {
            this.m_name = name;
        }
        #endregion // Constructor(s)
    } // AssetDummyViewModel
} // ContentBrowser.ViewModel
