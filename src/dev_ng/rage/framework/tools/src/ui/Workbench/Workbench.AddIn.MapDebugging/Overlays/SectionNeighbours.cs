﻿using System;
using System.Windows.Media;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using RSG.Model.Common.Map;
using MapViewport.AddIn;
using RSG.Model.Common;
using Workbench.AddIn.Services.Model;

namespace Workbench.AddIn.MapDebugging.Overlays
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension("Workbench.AddIn.MapDebugging.OverlayGroups.MapDebug", typeof(Viewport.AddIn.ViewportOverlay))]
    public class SectionNeighboursRadius : LevelDependentViewportOverlay
    {
        #region Constants
        /// <summary>
        /// Overlay name/description.
        /// </summary>
        private const String c_name = "Section Neighbours (Algorithm: Radius)";
        private const String c_description = "Displays the neighbour geometry picker for each section and upon selecting a " +
                                             "piece of geometry shows it's neighbours in red and it in green";
        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// A reference to view model for the map viewport, used to get the currently selected geometry
        /// </summary>
        [ImportExtension(Viewport.AddIn.CompositionPoints.MapViewport, typeof(Viewport.AddIn.IMapViewport))]
        public Viewport.AddIn.IMapViewport MapViewport { get; set; }
        #endregion // MEF Imports

        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public SectionNeighboursRadius()
            : base(c_name, c_description)
        {
            DataSourceModes[DataSource.Database] = true;
        }
        #endregion // Constructors

        #region Overrides
        /// <summary>
        /// Gets called when the user wants to view this overlay
        /// </summary>
        public override void Activated()
        {
            base.Activated();
            MapViewport.SelectedOverlayGeometry.CollectionChanged += SelectedOverlayGeometryChanged;

            this.Geometry.BeginUpdate();
            this.Geometry.Clear();

            if (LevelBrowserProxy.Value.SelectedLevel != null)
            {
                IMapHierarchy hierarchy = LevelBrowserProxy.Value.SelectedLevel.MapHierarchy;
                if (hierarchy != null)
                {
                    foreach (IMapSection section in hierarchy.AllSections.Where(item => item.VectorMapPoints != null))
                    {
                        Viewport2DShape newGeometry = new Viewport2DShape(etCoordSpace.World, "", section.VectorMapPoints.ToArray(), Colors.Black, 1, Colors.White);
                        newGeometry.UserText = section.Name;
                        newGeometry.PickData = section;
                        this.Geometry.Add(newGeometry);
                    }
                }
            }

            this.Geometry.EndUpdate();
        }

        /// <summary>
        /// Gets called when the user turns off this overlay
        /// </summary>
        public override void Deactivated()
        {
            MapViewport.SelectedOverlayGeometry.CollectionChanged -= SelectedOverlayGeometryChanged;
            base.Deactivated();
        }
        #endregion // Overrides

        #region Event Callbacks
        /// <summary>
        /// Gets called when map geometry selection changes
        /// </summary>
        private void SelectedOverlayGeometryChanged(Object sender, NotifyCollectionChangedEventArgs e)
        {
            IList<Viewport2DGeometry> selectedGeometry = new List<Viewport2DGeometry>();

            // Get the list of selected geometry updating the colours of all sections as we go along
            foreach (Viewport2DGeometry geometry in this.Geometry)
            {
                if (geometry is Viewport2DShape)
                {
                    if (geometry.IsSelected == true)
                    {
                        (geometry as Viewport2DShape).FillColour = Colors.Green;
                        selectedGeometry.Add(geometry);
                    }
                    else
                    {
                        (geometry as Viewport2DShape).FillColour = Colors.White;
                    }
                }
            }

            // Loop over the selected geometry updating the fill colours
            foreach (Viewport2DGeometry selected in selectedGeometry)
            {
                if (selected.PickData is IMapSection)
                {
                    IList<IMapSection> neighbours = (selected.PickData as IMapSection).Neighbours[(int)NeighbourMode.Radius];
                    foreach (Viewport2DGeometry geometry in this.Geometry)
                    {
                        if (geometry is Viewport2DShape && geometry.PickData is IMapSection)
                        {
                            if (neighbours.Contains(geometry.PickData as IMapSection))
                            {
                                (geometry as Viewport2DShape).FillColour = Colors.Red;
                            }
                        }
                    }
                }
            }
        }
        #endregion // Event Callbacks
    } // SectionNeighboursRadius

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension("Workbench.AddIn.MapDebugging.OverlayGroups.MapDebug", typeof(Viewport.AddIn.ViewportOverlay))]
    public class SectionNeighboursBox : LevelDependentViewportOverlay
    {
        #region Constants
        /// <summary>
        /// Overlay name/description.
        /// </summary>
        private const String c_name = "Section Neighbours (Algorithm: Box)";
        private const String c_description = "Displays the neighbour geometry picker for each section and upon selecting a " +
                                             "piece of geometry shows it's neighbours in red and it in green";
        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// A reference to view model for the map viewport, used to get the currently selected geometry
        /// </summary>
        [ImportExtension(Viewport.AddIn.CompositionPoints.MapViewport, typeof(Viewport.AddIn.IMapViewport))]
        public Viewport.AddIn.IMapViewport MapViewport { get; set; }
        #endregion // MEF Imports

        #region Constructors

        /// <summary>
        /// Default Constructor.
        /// </summary>
        public SectionNeighboursBox()
            : base(c_name, c_description)
        {
            DataSourceModes[DataSource.Database] = true;
        }

        #endregion // Constructors

        #region Overrides
        /// <summary>
        /// Gets called when the user wants to view this overlay
        /// </summary>
        public override void Activated()
        {
            base.Activated();
            MapViewport.SelectedOverlayGeometry.CollectionChanged += SelectedOverlayGeometryChanged;

            this.Geometry.BeginUpdate();
            this.Geometry.Clear();

            if (LevelBrowserProxy.Value.SelectedLevel != null)
            {
                IMapHierarchy hierarchy = LevelBrowserProxy.Value.SelectedLevel.MapHierarchy;
                if (hierarchy != null)
                {
                    foreach (IMapSection section in hierarchy.AllSections.Where(item => item.VectorMapPoints != null))
                    {
                        Viewport2DShape newGeometry = new Viewport2DShape(etCoordSpace.World, "", section.VectorMapPoints.ToArray(), Colors.Black, 1, Colors.White);
                        newGeometry.UserText = section.Name;
                        newGeometry.PickData = section;
                        this.Geometry.Add(newGeometry);
                    }
                }
            }

            this.Geometry.EndUpdate();
        }

        /// <summary>
        /// Gets called when the user turns off this overlay
        /// </summary>
        public override void Deactivated()
        {
            MapViewport.SelectedOverlayGeometry.CollectionChanged -= SelectedOverlayGeometryChanged;
            base.Deactivated();
        }
        #endregion // Overrides

        #region Event Callbacks

        /// <summary>
        /// Gets called when map geometry selection changes
        /// </summary>
        private void SelectedOverlayGeometryChanged(Object sender, NotifyCollectionChangedEventArgs e)
        {
            IList<Viewport2DGeometry> selectedGeometry = new List<Viewport2DGeometry>();

            // Get the list of selected geometry updating the colours of all sections as we go along
            foreach (Viewport2DGeometry geometry in this.Geometry)
            {
                if (geometry is Viewport2DShape)
                {
                    if (geometry.IsSelected == true)
                    {
                        (geometry as Viewport2DShape).FillColour = Colors.Green;
                        selectedGeometry.Add(geometry);
                    }
                    else
                    {
                        (geometry as Viewport2DShape).FillColour = Colors.White;
                    }
                }
            }

            // Loop over the selected geometry updating the fill colours
            foreach (Viewport2DGeometry selected in selectedGeometry)
            {
                if (selected.PickData is IMapSection)
                {
                    IList<IMapSection> neighbours = (selected.PickData as IMapSection).Neighbours[(int)NeighbourMode.Box];
                    foreach (Viewport2DGeometry geometry in this.Geometry)
                    {
                        if (geometry is Viewport2DShape && geometry.PickData is IMapSection)
                        {
                            if (neighbours.Contains(geometry.PickData as IMapSection))
                            {
                                (geometry as Viewport2DShape).FillColour = Colors.Red;
                            }
                        }
                    }
                }
            }
        }

        #endregion // Event Callbacks
    } // SectionNeighboursBox

    [ExportExtension("Workbench.AddIn.MapDebugging.OverlayGroups.MapDebug",
        typeof(Viewport.AddIn.ViewportOverlay))]
    public class SectionNeighboursDistance : LevelDependentViewportOverlay
    {
        #region Constants
        /// <summary>
        /// Overlay name/description.
        /// </summary>
        private const String c_name = "Section Neighbours (Algorithm: Distance)";
        private const String c_description = "Displays the neighbour geometry picker for each section and upon selecting a " +
                                             "piece of geometry shows it's neighbours in red and it in green";
        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// A reference to view model for the map viewport, used to get the currently selected geometry
        /// </summary>
        [ImportExtension(Viewport.AddIn.CompositionPoints.MapViewport, typeof(Viewport.AddIn.IMapViewport))]
        public Viewport.AddIn.IMapViewport MapViewport { get; set; }
        #endregion // MEF Imports

        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public SectionNeighboursDistance()
            : base(c_name, c_description)
        {
            DataSourceModes[DataSource.Database] = true;
        }
        #endregion // Constructors

        #region Overrides
        /// <summary>
        /// Gets called when the user wants to view this overlay
        /// </summary>
        public override void Activated()
        {
            base.Activated();
            MapViewport.SelectedOverlayGeometry.CollectionChanged += SelectedOverlayGeometryChanged;

            this.Geometry.BeginUpdate();
            this.Geometry.Clear();

            if (LevelBrowserProxy.Value.SelectedLevel != null)
            {
                IMapHierarchy hierarchy = LevelBrowserProxy.Value.SelectedLevel.MapHierarchy;
                if (hierarchy != null)
                {
                    foreach (IMapSection section in hierarchy.AllSections.Where(item => item.VectorMapPoints != null))
                    {
                        Viewport2DShape newGeometry = new Viewport2DShape(etCoordSpace.World, "", section.VectorMapPoints.ToArray(), Colors.Black, 1, Colors.White);
                        newGeometry.UserText = section.Name;
                        newGeometry.PickData = section;
                        this.Geometry.Add(newGeometry);
                    }
                }
            }

            this.Geometry.EndUpdate();
        }

        /// <summary>
        /// Gets called when the user turns off this overlay
        /// </summary>
        public override void Deactivated()
        {
            MapViewport.SelectedOverlayGeometry.CollectionChanged -= SelectedOverlayGeometryChanged;
            base.Deactivated();
        }
        #endregion // Overrides

        #region Event Callbacks
        /// <summary>
        /// Gets called when map geometry selection changes
        /// </summary>
        private void SelectedOverlayGeometryChanged(Object sender, NotifyCollectionChangedEventArgs e)
        {
            IList<Viewport2DGeometry> selectedGeometry = new List<Viewport2DGeometry>();

            // Get the list of selected geometry updating the colours of all sections as we go along
            foreach (Viewport2DGeometry geometry in this.Geometry)
            {
                if (geometry is Viewport2DShape)
                {
                    if (geometry.IsSelected == true)
                    {
                        (geometry as Viewport2DShape).FillColour = Colors.Green;
                        selectedGeometry.Add(geometry);
                    }
                    else
                    {
                        (geometry as Viewport2DShape).FillColour = Colors.White;
                    }
                }
            }

            // Loop over the selected geometry updating the fill colours
            foreach (Viewport2DGeometry selected in selectedGeometry)
            {
                if (selected.PickData is IMapSection)
                {
                    IList<IMapSection> neighbours = (selected.PickData as IMapSection).Neighbours[(int)NeighbourMode.Distance];
                    foreach (Viewport2DGeometry geometry in this.Geometry)
                    {
                        if (geometry is Viewport2DShape && geometry.PickData is IMapSection)
                        {
                            if (neighbours.Contains(geometry.PickData as IMapSection))
                            {
                                (geometry as Viewport2DShape).FillColour = Colors.Red;
                            }
                        }
                    }
                }
            }
        }
        #endregion // Event Callbacks
    } // SectionNeighboursDistance
} // Workbench.AddIn.MapDebugging.Overlays
