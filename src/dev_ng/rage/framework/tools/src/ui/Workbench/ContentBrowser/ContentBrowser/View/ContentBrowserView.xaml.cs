﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Windows;
using System.Globalization;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.Services;
using ContentBrowser.AddIn;
using ContentBrowser.ViewModel;
using System.Text.RegularExpressions;
using RSG.Model.Common;

namespace ContentBrowser.View
{
    /// <summary>
    /// Interaction logic for ContentBrowserView.xaml
    /// </summary>
    public partial class ContentBrowserView : ToolWindowBase<ContentBrowserDataContext>
    {
        #region Dependency properties

        /// <summary>
        /// The toggle preview property is bound to the ShowGridPreview property of the view model. When this property changes, it triggers the events that update the settings file to store the
        /// visibility of the preview pane. This is then read when Workbench starts up again and is set accordingly. See the OnContentLoaded() override.
        /// </summary>
        public static readonly DependencyProperty TogglePreviewPaneProperty = DependencyProperty.Register("TogglePreviewPane", typeof(bool), typeof(ContentBrowserView), new PropertyMetadata(TogglePreview_Changed));

        #endregion

        #region Constants
        public static readonly Guid GUID = new Guid("A8C583E1-E466-4B2E-BF3B-8F369D83A927");
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Determines whether the tool window should be floating by default
        /// </summary>
        public override Boolean FloatByDefault
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gives the default size for a floating window. This 
        /// size will be used for the first time the window is set to float
        /// </summary>
        public override Size DefaultFloatSize
        {
            get
            {
                return new Size(1000, 600);
            }
        }
        #endregion // Properties

        #region Constructor
        /// <summary>
        /// Default Constructor
        /// </summary>
        public ContentBrowserView()
            : base("ContentBrowser", new ContentBrowserDataContext())
        {
            InitializeComponent();

            this.ID = GUID;
            this.SaveModel = this.ViewModel;

            AddBindings();
        }


        /// <summary>
        /// Creates a ContentBrowserView with the given IBrowsers as it
        /// panel source
        /// </summary>
        public ContentBrowserView(ContentBrowserDataContext viewModel)
            : base("ContentBrowser", viewModel)
        {
            InitializeComponent();

            this.ID = GUID;
            this.SaveModel = this.ViewModel;

            AddBindings();
        }

        #endregion // Constructor

        /// <summary>
        /// Sets up additional property bindings.
        /// </summary>
        private void AddBindings()
        {
            Binding binding = new Binding();
            binding.Source = ViewModel;
            binding.Path = new PropertyPath("ShowGridPreview");
            this.SetBinding(TogglePreviewPaneProperty, binding);
        }

        /// <summary>
        /// When the content is loaded, read the current state of the preview pane toggle value.
        /// </summary>
        protected override void OnContentLoaded()
        {
            base.OnContentLoaded();

            bool isVisible = (bool)GetValue(TogglePreviewPaneProperty);
            if (!isVisible)
            {
                ViewModel.TogglePreviewCommand.Execute(false);
                TogglePreviewPane(GridPreviewSplitter.Visibility == Visibility.Visible);
            }
        }

        private void CollectionViewSource_Filter(Object sender, FilterEventArgs e)
        {
            e.Accepted = true;
        }

        private void GridView_SelectionChanged(Object sender, SelectionChangedEventArgs e)
        {
            List<RSG.Model.Common.AssetBase> selectedItems = new List<RSG.Model.Common.AssetBase>();
            foreach (var selected in this.GridView.SelectedItems)
            {
                IGridViewModel viewModel = selected as IGridViewModel;
                if (viewModel == null)
                {
                    continue;
                }

                AssetBase assetBase = viewModel.Model as AssetBase;
                if (assetBase == null)
                {
                    continue;
                }

                selectedItems.Add(assetBase);
            }

            for (int i = 0; i < this.GridView.Items.Count; i++)
            {
                ListBoxItem item = this.GridView.ItemContainerGenerator.ContainerFromIndex(i) as ListBoxItem;
                if (item == null)
                {
                    continue;
                }
                
                List<RSG.Model.Common.IAsset> assets = new List<RSG.Model.Common.IAsset>();
                foreach (object selected in this.GridView.SelectedItems)
                {
                    IGridViewModel viewModel = selected as IGridViewModel;
                    if (viewModel == null || viewModel.Model == null)
                    {
                        continue;
                    }

                    assets.Add(viewModel.Model);
                }

                item.Tag = assets;
            }

            this.ViewModel.SelectedGridItems = selectedItems;
            if (selectedItems.Count == 1)
            {
                this.ViewModel.SelectedDetailedItem = selectedItems[0];
            }
            else
            {
                this.ViewModel.SelectedDetailedItem = null;
            }
        }

        private void DetailView_SelectionChanged(Object sender, SelectionChangedEventArgs e)
        {
            List<RSG.Model.Common.AssetBase> selectedItems = new List<RSG.Model.Common.AssetBase>();
            foreach (var selected in this.DetailView.SelectedItems)
            {
                if (selected is ViewModel.IGridViewModel)
                {
                    if ((selected as IGridViewModel).Model is RSG.Model.Common.AssetBase)
                        selectedItems.Add((selected as IGridViewModel).Model as RSG.Model.Common.AssetBase);
                }
            }
            this.ViewModel.SelectedGridItems = selectedItems;
            if (selectedItems.Count == 1)
            {
                this.ViewModel.SelectedDetailedItem = selectedItems[0];
            }
            else
            {
                this.ViewModel.SelectedDetailedItem = null;
            }

            for (int i = 0; i < this.DetailView.Items.Count; i++)
            {
                ListBoxItem item = this.DetailView.ItemContainerGenerator.ContainerFromIndex(i) as ListBoxItem;
                if (item != null)
                {
                    List<RSG.Model.Common.IAsset> assets = new List<RSG.Model.Common.IAsset>();
                    foreach (Object selected in this.DetailView.SelectedItems)
                    {
                        if (selected is ContentBrowser.ViewModel.IGridViewModel)
                        {
                            assets.Add((selected as ContentBrowser.ViewModel.IGridViewModel).Model);
                        }
                    }
                    item.Tag = assets;
                }
            }
        }

        #region Overridden Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public override List<ISearchResult> FindAll(ISearchQuery query)
        {
            if (query.Expression == null)
                return new List<ISearchResult>();

            var results = this.ViewModel.BrowserCollection.SelectedBrowser.FindResults(query);
            foreach (var result in results)
            {
                result.Content = this;
            }

            return results;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="result"></param>
        public override void SelectSearchResult(ISearchResult result)
        {
            if (!(result is SearchResult))
                return;

            var searchResult = result as SearchResult;
            foreach (var browser in this.ViewModel.BrowserCollection.BrowserItems)
            {
                if (ReferenceEquals(browser.Model, searchResult.Browser))
                {
                    browser.IsSelected = true;                    
                    browser.SelectAsset(searchResult.Asset);
                    break;
                }
            }
        }
        #endregion

        private void GridView_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.GridView.UnselectAll();
        }

        private void DetailView_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DetailView.UnselectAll();
        }

        GridLength gridLength = new GridLength(14, GridUnitType.Star);
        GridLength previewLength = new GridLength(10, GridUnitType.Star);

        private void GridSplitter_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var columns = this.MainGrid.ColumnDefinitions;
            if (GridSourceSplitter.Visibility == Visibility.Visible)
            {
                columns[1].Width = new GridLength(4);
                columns[2].Width = gridLength;

                if (GridPreviewSplitter.Visibility == Visibility.Visible)
                {
                    columns[3].Width = new GridLength(4);
                    columns[4].Width = previewLength;
                }
            }
            else
            {
                gridLength = columns[2].Width;
                if (GridPreviewSplitter.Visibility == Visibility.Visible)
                {
                    previewLength = columns[4].Width;
                }

                columns[1].Width = new GridLength(0);
                columns[2].Width = new GridLength(0);
                columns[3].Width = new GridLength(0);
                columns[4].Width = new GridLength(0);
            }
        }

        /// <summary>
        /// Handles the TogglePreview property changed event.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Property changed event arguments.</param>
        private static void TogglePreview_Changed(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var view = sender as ContentBrowserView;
            view.TogglePreviewPane(view.GridPreviewSplitter.Visibility == Visibility.Visible);
        }

        /// <summary>
        /// Toggles the preview pane on or off. 
        /// </summary>
        /// <param name="isVisible">Set to true if the preview pane is to be visible.</param>
        private void TogglePreviewPane(bool isVisible)
        {
            var columns = MainGrid.ColumnDefinitions;
            if (isVisible)
            {
                columns[3].Width = new GridLength(4);
                columns[4].Width = previewLength;
            }
            else
            {
                previewLength = columns[4].Width;

                columns[3].Width = new GridLength(0);
                columns[4].Width = new GridLength(0);
            }
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch ((sender as ComboBox).SelectedIndex)
            {
                case 0:
                    this.ViewModel.ShowingDetailsView = false;
                    this.ViewModel.RenderSize = 128;
                    break;
                case 1:
                    this.ViewModel.ShowingDetailsView = false;
                    this.ViewModel.RenderSize = 96;
                    break;
                case 2:
                    this.ViewModel.ShowingDetailsView = false;
                    this.ViewModel.RenderSize = 64;
                    break;
                case 3:
                    this.ViewModel.ShowingDetailsView = true;
                    break;

            }
        }
    } // ContentBrowserView

    public class DetailItemConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is IAsset)
            {
                return DetailViewModelFactory.GetViewModel(value as IAsset);
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

} // ContentBrowser.View
