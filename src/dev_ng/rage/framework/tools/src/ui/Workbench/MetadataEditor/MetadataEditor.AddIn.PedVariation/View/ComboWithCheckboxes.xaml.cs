﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MetadataEditor.AddIn.PedVariation.View
{
    public interface IComboWithCheckboxesItem
    {
        Boolean IsChecked { get; }

        String DisplayName { get; }
    }

    /// <summary> 
    ///Interaction logic for ComboWithCheckboxes.xaml 
    /// </summary> 
    public partial class ComboWithCheckboxes : UserControl
    { 
        #region Dependency Properties 

        /// <summary> 
        ///Gets or sets a collection used to generate the content of the ComboBox 
        /// </summary> 
        public Object ItemsSource 
        { 
            get{ return(Object)GetValue(ItemsSourceProperty); } 
            set 
            { 
                SetValue(ItemsSourceProperty, value); 

                SetText(); 
            } 
        }

        public static readonly DependencyProperty ItemsSourceProperty = 
            DependencyProperty.Register("ItemsSource", typeof(object), typeof(ComboWithCheckboxes), new UIPropertyMetadata(OnSourceChanged));
 
        private static void OnSourceChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue is System.Collections.IList)
            {
                (sender as ComboWithCheckboxes).SetText();
            }
        }

        /// <summary> 
        ///Gets or sets the text displayed in the ComboBox 
        /// </summary> 
        public String Text 
        { 
            get{ return(string)GetValue(TextProperty); } 
            set{ SetValue(TextProperty, value); } 
        } 

        public static readonly DependencyProperty TextProperty = 
            DependencyProperty.Register("Text", typeof(string), typeof(ComboWithCheckboxes), new UIPropertyMetadata(String.Empty));

        /// <summary> 
        ///Gets or sets the text displayed in the ComboBox if there are no selected items 
        /// </summary> 
        public String DefaultText 
        { 
            get{ return(string)GetValue(DefaultTextProperty); } 
            set{ SetValue(DefaultTextProperty, value); } 
        } 

        // Using a DependencyProperty as the backing store for DefaultText.  This enables animation, styling, binding, etc... 
        public static readonly DependencyProperty DefaultTextProperty = 
            DependencyProperty.Register("DefaultText", typeof(string), typeof(ComboWithCheckboxes), new UIPropertyMetadata(string.Empty,
                OnDefaultTextChanged)); 
        #endregion 

        public ComboWithCheckboxes() 
        { 
            InitializeComponent();
            if (String.IsNullOrEmpty(this.Text))
            {
                this.Text = this.DefaultText;
            }
        } 

        /// <summary> 
        ///Whenever a CheckBox is checked, change the text displayed 
        /// </summary> 
        /// <param name="sender"></param> 
        /// <param name="e"></param> 
        private void CheckBox_Click(Object sender, RoutedEventArgs e) 
        { 
            SetText(); 
        } 

        /// <summary> 
        ///Set the text property of this control (bound to the ContentPresenter of the ComboBox) 
        /// </summary> 
        private void SetText()
        {
            String text = String.Empty;

            int includeCount = 0;
            foreach (Object item in this.ItemsSource as System.Collections.IList)
            {
                if (item is IComboWithCheckboxesItem)
                {
                    if ((item as IComboWithCheckboxesItem).IsChecked == true)
                    {
                        if (includeCount != 0)
                        {
                            text += " | " + (item as IComboWithCheckboxesItem).DisplayName;
                        }
                        else
                        {
                            text += (item as IComboWithCheckboxesItem).DisplayName;
                        }
                        includeCount++;
                    }
                }
            }
            // set DefaultText if nothing else selected 
            if (String.IsNullOrEmpty(text))
            {
                this.Text = this.DefaultText;
            }
            else
            {
                this.Text = text;
            }
        }

        private static void OnDefaultTextChanged(Object s, DependencyPropertyChangedEventArgs e)
        {
            ComboWithCheckboxes t = s as ComboWithCheckboxes;
            if (t == null)
                return;

            if (String.IsNullOrEmpty(t.Text) || String.Compare(t.Text, t.DefaultText) == 0)
            {
                t.Text = t.DefaultText;
            }
        }
    } 
}
