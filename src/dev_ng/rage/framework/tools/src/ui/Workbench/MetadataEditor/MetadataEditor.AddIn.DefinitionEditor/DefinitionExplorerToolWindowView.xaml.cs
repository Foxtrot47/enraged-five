﻿using System;
using System.Diagnostics;
using System.Windows;
using Workbench.AddIn;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.Services;
using RSG.Metadata.Model;
using RSG.Metadata.Parser;
using MetadataEditor.AddIn.DefinitionEditor.ViewModel;
using System.Collections.Generic;
using RSG.Base.Editor;
using System.Windows.Controls;

namespace MetadataEditor.AddIn.DefinitionEditor
{
    /// <summary>
    /// 
    /// </summary>
    public partial class DefinitionExplorerToolWindowView : ToolWindowBase<DefinitionExplorerToolWindowViewModel>
    {
        #region Properties
        ILayoutManager LayoutManager
        {
            get;
            set;
        }

        IPropertyInspector PropertyInspector
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public DefinitionExplorerToolWindowView()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public DefinitionExplorerToolWindowView(IConfigurationService config, ILayoutManager layoutManager, IStructureDictionary structures, IPropertyInspector propertyInspector)
            : base(DefinitionExplorerToolWindowViewModel.TOOLWINDOW_NAME,
            new DefinitionExplorerToolWindowViewModel(config, structures))
        {
            InitializeComponent();
            this.LayoutManager = layoutManager;
            this.PropertyInspector = propertyInspector;
        }
        #endregion // Constructor(s)

        #region Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TreeView_MouseDoubleClick(Object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (LayoutManager != null)
            {
                if ((sender as System.Windows.Controls.TreeView).SelectedItem is StructureViewModel)
                {
                    Structure model = ((sender as System.Windows.Controls.TreeView).SelectedItem as StructureViewModel).Model;
                    Debug.Assert(null != model, "No selected definition model.");
                    if (null == model)
                        return;

                    DefinitionEditorView newDocument = new DefinitionEditorView(model, this.PropertyInspector);
                    this.LayoutManager.ShowDocument(newDocument);
                }
            }
        }
        #endregion // Event Handlers

        #region Overridden Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public override List<ISearchResult> FindAll(ISearchQuery query)
        {
            List<ISearchResult> results = new List<ISearchResult>();

            foreach (var folder in this.ViewModel.Roots)
            {
                if (folder is ViewModel.HierarchicalFolder)
                {
                    foreach (StructureViewModel s in GetAllStructures(folder as ViewModel.HierarchicalFolder))
                    {
                        if (query.Expression.IsMatch(s.Model.ShortDataType))
                        {
                            results.Add(new SearchResult(s, s.Model.ShortDataType, this));
                        }
                    }
                }
            }
            return results;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="result"></param>
        public override void SelectSearchResult(ISearchResult result)
        {
            if (result.Data is IViewModel)
            {
                (result.Data as IViewModel).IsSelected = true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private IEnumerable<StructureViewModel> GetAllStructures(HierarchicalFolder root)
        {
            foreach (var child in root.Children)
            {
                if (child is StructureViewModel)
                {
                    yield return child as StructureViewModel;
                    foreach (var grandchild in GetAllStructures(child as StructureViewModel))
                        yield return grandchild;
                }

                if (child is HierarchicalFolder)
                {
                    foreach (var grandchild in GetAllStructures(child as HierarchicalFolder))
                        yield return grandchild;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="root"></param>
        /// <returns></returns>
        private IEnumerable<StructureViewModel> GetAllStructures(StructureViewModel root)
        {
            foreach (var child in root.Children)
            {
                if (child is StructureViewModel)
                {
                    yield return child as StructureViewModel;
                    foreach (var grandchild in GetAllStructures(child as StructureViewModel))
                        yield return grandchild;
                }

                if (child is HierarchicalFolder)
                {
                    foreach (var grandchild in GetAllStructures(child as HierarchicalFolder))
                        yield return grandchild;
                }
            }
        }
        #endregion

        private void TreeViewItem_Selected(object sender, RoutedEventArgs e)
        {
            (sender as TreeViewItem).BringIntoView();
        }
    }
} // MetadataEditor.AddIn.DefinitionEditorViewModel namespace
