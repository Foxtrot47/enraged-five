﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using RSG.Base.ConfigParser;
using RSG.Model.Common;
using RSG.Model.Vehicle;
using RSG.Platform;
using Workbench.AddIn.Services.Model;

namespace LevelBrowser.AddIn.MapBrowser
{

    /// <summary>
    /// 
    /// </summary>
    [Workbench.AddIn.ExportExtension(ExtensionPoints.LevelBrowserComponent, typeof(ILevelBrowserComponent))]
    public class VehicleBrowserProxy : ILevelBrowserComponent, IPartImportsSatisfiedNotification
    {
        #region MEF Imports
        /// <summary>
        /// The data source browser
        /// </summary>
        [Workbench.AddIn.ImportExtension(Workbench.AddIn.CompositionPoints.DataSourceBrowser, typeof(IDataSourceBrowser))]
        private Lazy<IDataSourceBrowser> DataSourceBrowser { get; set; }

        /// <summary>
        /// The build browser
        /// </summary>
        [Workbench.AddIn.ImportExtension(Workbench.AddIn.CompositionPoints.BuildBrowser, typeof(IBuildBrowser))]
        private Lazy<IBuildBrowser> BuildBrowser { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Workbench.AddIn.ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(Workbench.AddIn.Services.IConfigurationService))]
        private Workbench.AddIn.Services.IConfigurationService ConfigService { get; set; }
        #endregion

        #region Member Data
        /// <summary>
        /// Reference to the list of levels.
        /// </summary>
        private ILevelCollection Levels { get; set; }

        /// <summary>
        /// Factory for creating the appropriate vehicle collection (based off of the data source).
        /// </summary>
        private IVehicleFactory VehicleFactory { get; set; }

        /// <summary>
        /// Background worker for loading the map data.
        /// </summary>
        private BackgroundWorker AsyncWorker { get; set; }
        #endregion // Member Data

        #region Constructors
        /// <summary>
        /// Default constructors
        /// </summary>
        public VehicleBrowserProxy()
        {
            this.AsyncWorker = new BackgroundWorker();
            this.AsyncWorker.WorkerReportsProgress = false;
            this.AsyncWorker.WorkerSupportsCancellation = true;
            this.AsyncWorker.DoWork += this.RefreshDoWork;
            this.AsyncWorker.RunWorkerCompleted += this.RefreshCompleted;
        }
        #endregion // Constructors

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="levels"></param>
        /// <param name="gv"></param>
        public void OnRefresh(ILevelCollection levels)
        {
            this.Levels = levels;

            if (this.AsyncWorker.IsBusy)
                this.AsyncWorker.CancelAsync();
            else
                this.AsyncWorker.RunWorkerAsync();
        }

        /// <summary>
        /// Returns all of the paths that need checking and syncing before the user can start up the
        /// level browser.
        /// </summary>
        public String[] GetFilesForSync(ConfigGameView gv)
        {
            List<String> files = new List<String>();
            // Grab the vehicles meta file
            files.Add(System.IO.Path.Combine(gv.CommonDir, "data/levels/gta5/vehicles.meta"));
            /// Grab the vehicle xml files
            files.Add(System.IO.Path.Combine(gv.AssetsDir, "vehicles....xml"));
            /// Grab the prop renders
            files.Add(System.IO.Path.Combine(gv.AssetsDir, "vehicles", "renders....jpg"));

            // Grab vehicles rpf files
            foreach (ContentNode node in gv.Content.Root.FindAll("vehiclesdirectory"))
            {
                string independentPath = GeneratePathForNode(node);

                foreach (ConfigTargetInfo targetInfo in gv.Targets(gv.Branch).Where(item => item.Enabled))
                {
                    RSG.Platform.Platform platform = PlatformUtils.PlatformFromString(targetInfo.Name);
                    var target = ConfigService.Config.Project.DefaultBranch.Targets[platform];

                    string platformFilePath = RSG.Pipeline.Services.Platform.PlatformPathConversion.ConvertAndRemapFilenameToPlatform(target, independentPath);
                    files.Add(platformFilePath);
                }
            }

            return files.ToArray();
        }

        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Work handler for the loading thread
        /// </summary>
        private void RefreshDoWork(Object sender, DoWorkEventArgs e)
        {
            // Add vehicles to all the levels
            foreach (ILevel level in Levels)
            {
                if (level.Name != "None")
                {
                    IVehicleCollection vehicles = null;

                    // Only create a collection if a data source has been selected
                    String buildIdentifier = (BuildBrowser.Value.SelectedBuild != null ? BuildBrowser.Value.SelectedBuild.Identifier : null);
                    vehicles = VehicleFactory.CreateCollection(DataSourceBrowser.Value.SelectedSource, buildIdentifier);

                    level.Vehicles = vehicles;

                    // TODO: Hacky optimisation where we rely on the gta5 level being the first one after "None" this really needs fixing!
                    break;
                }
            }

            BackgroundWorker worker = sender as BackgroundWorker;
            if (worker.CancellationPending == true)
            {
                e.Cancel = true;
            }
        }

        /// <summary>
        /// Completed handler for the loading thread
        /// </summary>
        private void RefreshCompleted(Object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                OnRefresh(this.Levels);
            }
        }

        /// <summary>
        /// Generates the path to a particular node by working its way up the tree
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        private string GeneratePathForNode(ContentNode node)
        {
            string result = node.Name + ".zip";

            while (node != null)
            {
                if (node is ContentNodeGroup)
                {
                    ContentNodeGroup groupNode = node as ContentNodeGroup;

                    if (!String.IsNullOrEmpty(groupNode.RelativePath))
                    {
                        result = Path.Combine(groupNode.RelativePath, result);
                    }
                }

                node = node.Parent;
            }

            return Path.GetFullPath(result);
        }
        #endregion // Private Methods

        #region IPartImportsSatisfiedNotification Interface
        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {
            VehicleFactory = new VehicleFactory(ConfigService.Config);
        }
        #endregion // IPartImportsSatisfiedNotification Interface
    } // VehicleBrowserProxy

} // LevelBrowser.AddIn.MapBrowser
