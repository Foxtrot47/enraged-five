﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using RSG.Model.Common;
using ContentBrowser.ViewModel.MapViewModels3;

namespace ContentBrowser.ViewModel
{

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public static class DetailViewModelFactory
    {
        public static IViewModel GetViewModel(IAsset asset)
        {
            if (asset is ITexture)
            {
                return new TextureDetailsViewModel(asset as ITexture);
            }
            return null;
        }
    }
}
