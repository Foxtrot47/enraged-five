﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Windows;
using System.Xml;
using MetadataEditor.AddIn.MetaEditor.BasicViewModel;
using MetadataEditor.AddIn.MetaEditor.GridViewModel;
using RSG.Base.Collections;
using RSG.Base.Configuration;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using RSG.Base.Logging;
using RSG.Base.Windows.DragDrop;
using RSG.Metadata.Data;
using RSG.Metadata.Model;
using RSG.Metadata.Parser;
using RSG.Metadata.Util;

namespace MetadataEditor.AddIn.MetaEditor.ViewModel
{
    using GridViewModelCollection = ObservableCollection<GridViewModel.GridTunableViewModel>;
    using TreeViewModelCollection = ObservableDictionary<String, ITunableViewModel>;
    using System.Threading;
    using System.Collections.Concurrent;
    using System.Net;

    /// <summary>
    /// A rest request to send to the game
    /// </summary>
    class RestRequest
    {
        public string method;
        public string url;
        public byte[] data;
        public ITunable tunable;
    }

    /// <summary>
    /// 
    /// </summary>
    public class ItemSelectedEventArgs : System.EventArgs
    {
        #region Properties
        public object ItemSelected
        {
            get;
            private set;
        }
        #endregion

        #region Constructor
        public ItemSelectedEventArgs(object itemSelected)
        {
            this.ItemSelected = itemSelected;
        }
        #endregion
    }

    /// <summary>
    /// The view model that wraps around a 
    /// <see cref="IMetaFile"/> object.
    /// </summary>
    public class MetaFileViewModel :
        ViewModelBase, IDropTarget, IWeakEventListener, IMetadataViewModel
    {
        #region Events
        public event EventHandler<ItemSelectedEventArgs> ItemSelected;

        public event EventHandler UnSelectAllRequested;
        #endregion // Events

        #region Properties and Associated Member Data
        /// <summary>
        /// A reference to the wrapped 
        /// <see cref="IMetaFile"/> object.
        /// </summary>
        public IMetaFile Model
        {
            get { return m_model; }
            protected set
            {
                SetPropertyValue(value, () => this.Model,
                    new PropertySetDelegate(delegate(Object newValue) 
                        { 
                            m_model = (IMetaFile)newValue; 
                        }
                    ));
            }
        }

        /// <summary>
        /// Associated project branch.
        /// </summary>
        public IBranch Branch
        {
            get { return m_Branch; }
            protected set
            {
                SetPropertyValue(value, () => this.Branch,
                    new PropertySetDelegate(delegate(Object newValue)
                    {
                        m_Branch = (IBranch)newValue;
                    }
                    ));
            }
        }
        private IBranch m_Branch;

        /// <summary>
        /// 
        /// </summary>
        public TreeViewModelCollection Members
        {
            get { return m_treeViewModel; }
            set
            {
                SetPropertyValue(value, m_treeViewModel, () => this.Members,
                    new PropertySetDelegate(delegate(Object newValue) 
                        {
                            m_treeViewModel = (TreeViewModelCollection)newValue;
                        }
                    ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<ITunableViewModel> AllTunableViewModels
        {
            get { return m_allTunableViewModels; }
            set
            {
                SetPropertyValue(value, m_allTunableViewModels, () => this.AllTunableViewModels,
                    new PropertySetDelegate(delegate(Object newValue)
                    {
                        m_allTunableViewModels = (ObservableCollection<ITunableViewModel>)newValue;
                    }
                    ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public GridViewModelCollection BreadCrumbs
        {
            get { return m_breadcrumbs; }
            set
            {
                SetPropertyValue(value, m_breadcrumbs, () => this.BreadCrumbs,
                    new PropertySetDelegate(delegate(Object newValue) 
                        {
                            m_breadcrumbs = (GridViewModelCollection)newValue;
                        }
                    ));
            }
        }

        /// <summary>
        /// The currently selected content in the grid views tab control
        /// </summary>
        public GridViewModel.GridTunableViewModel BreadCrumbSelection
        {
            get
            {
                return this.breadCrumbSelection;
            }
            set
            {
                if (object.ReferenceEquals(value, this.breadCrumbSelection))
                {
                    return;
                }

                int index = this.BreadCrumbs.IndexOf(value);
                if (index != -1)
                {
                    while (this.BreadCrumbs.Count > index + 1)
                    {
                        this.BreadCrumbs.RemoveAt(this.BreadCrumbs.Count - 1);
                    }
                }

                this.breadCrumbSelection = value;
                OnPropertyChanged("BreadCrumbSelection");
            }
        }
        GridViewModel.GridTunableViewModel breadCrumbSelection;

        /// <summary>
        /// 
        /// </summary>
        public List<ITunableViewModel> SelectedItems
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsLiveEditing
        {
            get;
            protected set;
        }
        #endregion // Properties and Associated Member Data

        #region Member Data
        private IMetaFile m_model;
        private TreeViewModelCollection m_treeViewModel;
        private GridViewModelCollection m_breadcrumbs;
        private ObservableCollection<ITunableViewModel> m_allTunableViewModels;
        private IList<ITunableViewModel> m_rootViewModels;

        // Live-link stuff
        private Uri m_liveLinkUrl;
        private Thread m_requestThread;
        private BlockingCollection<RestRequest> m_requestQueue;
        private GridViewModel.GridTunableViewModel previousSelection;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        public MetaFileViewModel(IBranch branch)
            : this(branch, new MetaFile())
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public MetaFileViewModel(IBranch branch, IMetaFile model)
        {
            this.Branch = branch;
            this.Model = model;
            this.m_treeViewModel = new TreeViewModelCollection();
            this.m_breadcrumbs = new GridViewModelCollection();
            this.m_allTunableViewModels = new ObservableCollection<ITunableViewModel>();
            this.m_rootViewModels = new List<ITunableViewModel>();
            this.IsLiveEditing = false;
            if (model is MetaFile)
            {
                foreach (KeyValuePair<String, ITunable> kvp in (model as MetaFile).Members)
                {
                    ITunableViewModel vm = MetadataEditor.AddIn.MetaEditor.BasicViewModel.TunableViewModelFactory.Create(null, kvp.Value, this);
                    this.m_treeViewModel.Add(kvp.Key, vm);
                    this.m_allTunableViewModels.Add(vm);
                    this.m_rootViewModels.Add(vm);
                    this.m_breadcrumbs.Add(GridViewModel.TunableViewModelFactory.Create(null, kvp.Value, this));
                    this.breadCrumbSelection = this.m_breadcrumbs[0];
                }
            }
            else if (model is MultiMetaFiles)
            {
                foreach (KeyValuePair<String, ITunable> kvp in (model as MultiMetaFiles).Members)
                {
                    ITunableViewModel vm = MetadataEditor.AddIn.MetaEditor.BasicViewModel.TunableViewModelFactory.Create(null, kvp.Value, this);
                    this.m_treeViewModel.Add(kvp.Key, vm);
                    this.m_allTunableViewModels.Add(vm);
                    this.m_rootViewModels.Add(vm);
                    this.m_breadcrumbs.Add(GridViewModel.TunableViewModelFactory.Create(null, kvp.Value, this));
                    this.breadCrumbSelection = this.m_breadcrumbs[0];
                }
            }

            this.SelectedItems = new List<ITunableViewModel>();
        }
        #endregion // Constructor(s)

        #region Static Controller Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="stream"></param>
        /// <param name="defDict"></param>
        /// <returns></returns>
        public static MetaFileViewModel Create(IBranch branch, Stream stream, StructureDictionary defDict)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                try
                {
                    doc.Load( stream );
                }
                catch
                {
                    Log.Log__ErrorCtx( "Stream",
                        "The stream doesn't appear to be a valid xml file. Please make sure that this file is a well formed xml file." );
                    return null;
                }

                // Root node of XML document determines the definition name.
                Structure defModel = null;
                String structDefName = Namespace.ConvertDataTypeToFriendlyName( doc.DocumentElement.Name );

                if ( defDict.TryGetValue( structDefName, out defModel ) )
                {
                    MetaFile metaFile = new MetaFile(stream, defModel, true);
                    MetaFileViewModel model = new MetaFileViewModel(branch, metaFile);
                    return (model);
                }
                else
                {
                    Log.Log__ErrorCtx( "Stream",
                        "Cannot open the stream as the root type '{0}' cannot be found as a loaded structure.",
                        structDefName );
                    return null;
                }
            }
            catch ( Exception ex )
            {
                Log.Log__ExceptionCtx( ex, RSG.Metadata.Consts.LOG_CONTEXT,
                    "Unhandled exception constructing MetaFileViewModel from stream.");
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static MetaFileViewModel Create(IBranch branch, String filename, StructureDictionary defDict)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                try
                {
                    doc.Load(filename);
                }
                catch
                {
                    Log.Log__ErrorCtx(filename, 
                        "The file '{0}' doesn't appear to be a valid xml file. Please make sure that this file is a well formed xml file.",
                        filename);
                    return null;
                }

                // Root node of XML document determines the definition name.
                Structure defModel = null;
                String structDefName = Namespace.ConvertDataTypeToFriendlyName(doc.DocumentElement.Name);

                if (defDict.TryGetValue(structDefName, out defModel))
                {
                    MetaFile metaFile = new MetaFile(filename, defModel);
                    MetaFileViewModel model = new MetaFileViewModel(branch, metaFile);
                    return (model);
                }
                else
                {
                    Log.Log__ErrorCtx(filename,
                        "Cannot open the file '{0}' as the root type '{1}' cannot be found as a loaded structure.",
                        filename,
                        structDefName);
                    return null;
                }
            }
            catch (Exception ex)
            {
                Log.Log__ExceptionCtx(ex, RSG.Metadata.Consts.LOG_CONTEXT,
                    "Unhandled exception constructing MetaFileViewModel from {0}.",
                    filename);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="definition"></param>
        /// <returns></returns>
        public static MetaFileViewModel Create(IBranch branch, Structure definition)
        {
            try
            {
                MetaFile metaFile = new MetaFile(definition);
                MetaFileViewModel model = new MetaFileViewModel(branch, metaFile);
                return (model);
            }
            catch (Exception ex)
            {
                Log.Log__ExceptionCtx(ex, RSG.Metadata.Consts.LOG_CONTEXT,
                    "Unhandled exception constructing new MetaFileViewModel {0}.",
                    definition.DataType);
                return (null);
            }
        }

        #endregion // Static Controller Methods

        #region Controller Methods
        /// <summary>
        /// Gets the model behind the view model as a MetaFile.
        /// </summary>
        /// <returns></returns>
        public MetaFile GetMetaFile()
        {
            return this.Model as MetaFile;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        public void OnItemSelected(object item)
        {
            if (this.ItemSelected != null)
                this.ItemSelected(this, new ItemSelectedEventArgs(item));
        }

        /// <summary>
        /// Enables the live-link without performing any additional fluff
        /// </summary>
        /// <param name="liveLinkUrl"></param>
        public void EnableLiveLink(Uri liveLinkUrl)
        {
            // Set up the tunable listeners for the currently visible tunable vm's
            AddTunableListeners(this.m_rootViewModels);

            // Setup the request queue and background processing thread
            m_liveLinkUrl = liveLinkUrl;
            m_requestQueue = new BlockingCollection<RestRequest>();
            m_requestThread = new Thread(new ThreadStart(WebRequestThread));
            m_requestThread.Name = "Live Editing Request Thread";
            m_requestThread.IsBackground = true;
            m_requestThread.Start();

            IsLiveEditing = true;
        }

        /// <summary>
        /// Enable the live-link replacing the local file with the passed in file
        /// </summary>
        /// <param name="newFile"></param>
        public void EnableLiveLink(Uri liveLinkUrl, IMetaFile newFile)
        {
            // Check if we need to swap out the file that is currently on display to the user
            MetaFile newModel = newFile as MetaFile;
            MetaFile oldModel = this.Model as MetaFile;
            if (newModel != null && oldModel != null)
            {
                oldModel.Members = null;
                oldModel.Members = newModel.Members;

                this.AllTunableViewModels.Clear();
                this.m_treeViewModel.Clear();
                this.m_breadcrumbs.Clear();
                this.m_rootViewModels.Clear();

                foreach (KeyValuePair<String, ITunable> kvp in (Model as MetaFile).Members)
                {
                    ITunableViewModel vm = MetadataEditor.AddIn.MetaEditor.BasicViewModel.TunableViewModelFactory.Create(null, kvp.Value, this);
                    this.m_treeViewModel.Add(kvp.Key, vm);
                    this.AllTunableViewModels.Add(vm);
                    this.m_rootViewModels.Add(vm);
                }
            }

            EnableLiveLink(liveLinkUrl);
        }

        /// <summary>
        /// Enable the live-link and push all the local data to the game (if so requested
        /// </summary>
        /// <param name="liveLinkUrl"></param>
        /// <param name="sendDataToGame"></param>
        public void EnableLiveLink(Uri liveLinkUrl, bool sendDataToGame)
        {
            EnableLiveLink(liveLinkUrl);

            if (sendDataToGame)
            {
                foreach (KeyValuePair<String, ITunable> kvp in (Model as MetaFile).Members)
                {
                    SendRootStructure(kvp.Value as StructureTunable, liveLinkUrl.ToString());
                    break;
                }

            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void DisableLiveLink()
        {
            RemoveTunableListeners(this.AllTunableViewModels);
            m_requestQueue.CompleteAdding();
            IsLiveEditing = false;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Save(String filename)
        {
            try
            {
                this.Model.Serialise(filename);
            }
            catch (Exception ex)
            {
                throw new ModelException("Unhandled exception saving MetaFileViewModel.", ex);
            }
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tunables"></param>
        private void AddTunableListeners(IEnumerable tunables)
        {
            foreach (ITunableViewModel vm in tunables)
            {
                AddTunableListeners(vm.Model);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tunables"></param>
        private void RemoveTunableListeners(IEnumerable tunables)
        {
            foreach (ITunableViewModel vm in tunables)
            {
                RemoveTunableListeners(vm.Model);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tunable"></param>
        private void AddTunableListeners(ITunable tunable)
        {
            if (tunable == null)
            {
                Log.Log__Warning("Tried to add listener to tunable that was null");
                return;
            }

            if (tunable is StructureTunable)
            {
                StructureTunable structure = tunable as StructureTunable;
                foreach (KeyValuePair<String, ITunable> subTunable in structure)
                {
                    AddTunableListeners(subTunable.Value);
                }
            }
            else if (tunable is ArrayTunable)
            {
                ArrayTunable array = tunable as ArrayTunable;
                CollectionChangedEventManager.AddListener(array, this);
                PropertyChangedEventManager.AddListener(array, this, "Items");
                foreach (ITunable subTunable in array)
                {
                    AddTunableListeners(subTunable);
                }
            }
            else if (tunable is MapTunable)
            {
                MapTunable map = tunable as MapTunable;
                CollectionChangedEventManager.AddListener(map, this);
                PropertyChangedEventManager.AddListener(map, this, "Items");
                foreach (ITunable subTunable in map.Items)
                {
                    AddTunableListeners(subTunable);
                }
            }
            else if (tunable is MapItemTunable)
            {
                MapItemTunable mapItem = tunable as MapItemTunable;
                ITunable subTunable = mapItem.Value as ITunable;
                if (subTunable != null)
                    AddTunableListeners(subTunable);
            }
            else if (tunable is PointerTunable)
            {
                PointerTunable pointer = tunable as PointerTunable;
                ITunable subTunable = pointer.Value as ITunable;
                if (subTunable != null)
                    AddTunableListeners(subTunable);
            }
            else if (tunable is TunableBase)
            {
                TunableBase tunableBase = tunable as TunableBase;
                PropertyChangedEventManager.AddListener(tunableBase, this, "Value");
            }
            else
            {
                Log.Log__Warning("Tried to add listener to unknown tunable type: {0}", Tunable.GetFullPath(tunable));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tunable"></param>
        private void RemoveTunableListeners(ITunable tunable)
        {
            if (tunable == null)
            {
                Log.Log__Warning("Tried to remove listener on tunable that was null");
                return;
            }

            if (tunable is StructureTunable)
            {
                StructureTunable structure = tunable as StructureTunable;
                foreach (KeyValuePair<String, ITunable> subTunable in structure)
                {
                    RemoveTunableListeners(subTunable.Value);
                }
            }
            else if (tunable is ArrayTunable)
            {
                ArrayTunable array = tunable as ArrayTunable;
                CollectionChangedEventManager.RemoveListener(array, this);
                PropertyChangedEventManager.RemoveListener(array, this, "Items");
                foreach (ITunable subTunable in array)
                {
                    RemoveTunableListeners(subTunable);
                }
            }
            else if (tunable is PointerTunable)
            {
                PointerTunable pointer = tunable as PointerTunable;
                ITunable subTunable = pointer.Value as ITunable;
                if (subTunable != null)
                    RemoveTunableListeners(subTunable);
            }
            else if (tunable is TunableBase)
            {
                TunableBase tunableBase = tunable as TunableBase;
                PropertyChangedEventManager.RemoveListener(tunableBase, this, "Value");
            }
            else
            {
                Log.Log__Warning("Tried to remove listener on unknown tunable type: {0}", Tunable.GetFullPath(tunable));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="root"></param>
        /// <param name="tunables"></param>
        public void GetAllTunables(ITunableViewModel root, ref IList<ITunableViewModel> tunables)
        {
            if (!tunables.Contains(root))
                tunables.Add(root);

            if (root is HierarchicalTunableViewModel)
            {
                foreach (TunableViewModel child in (root as HierarchicalTunableViewModel).Members)
                {
                    GetAllTunables(child, ref tunables);
                }
            }
            else if (root is MetadataEditor.AddIn.MetaEditor.GridViewModel.GridTunableViewModel)
            {
                foreach (TunableViewModel child in (root as MetadataEditor.AddIn.MetaEditor.GridViewModel.GridTunableViewModel).Children)
                {
                    GetAllTunables(child, ref tunables);
                }
            }
        }

        public void UnSelectAll()
        {
            UnSelectAll(false);
        }

        /// <summary>
        /// 
        /// </summary>
        public void UnSelectAll(bool useBreadcrumbs)
        {
            foreach (ITunableViewModel vm in AllTunableViewModels)
            {
                vm.IsSelected = false;
            }

            if (this.UnSelectAllRequested != null)
                this.UnSelectAllRequested(this, EventArgs.Empty);
        }
        #endregion // Private Methods

        #region IWeakEventListener Implementation
        /// <summary>
        /// 
        /// </summary>
        /// <param name="managerType"></param>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
        {
            ITunable tunableSender = (ITunable)sender;
            string url = GetTunableURL(tunableSender);
            if (String.IsNullOrEmpty(url))
            {
                return true;
            }

            if (sender is TunableBase && e is PropertyChangedEventArgs)
            {
                TunableBase tunable = sender as TunableBase;

                PropertyChangedEventArgs pcArgs = e as PropertyChangedEventArgs;
                if (pcArgs != null && pcArgs.PropertyName == "Value")
                {
                    HandleTunableBaseModified(tunable, url);
                }
            }
            else if ((sender is ArrayTunable || sender is MapTunable) && e is NotifyCollectionChangedEventArgs)
            {
                NotifyCollectionChangedEventArgs pcArgs = e as NotifyCollectionChangedEventArgs;
                if (pcArgs != null)
                {
                    if (pcArgs.Action == NotifyCollectionChangedAction.Add)
                    {
                        HandleCollectionAdd(pcArgs.NewItems, pcArgs.NewStartingIndex, url);
                    }
                    else if (pcArgs.Action == NotifyCollectionChangedAction.Remove)
                    {
                        HandleCollectionRemove(pcArgs.OldItems, pcArgs.OldStartingIndex, url);
                    }
                }
            }
            
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tunable"></param>
        /// <param name="newItems"></param>
        /// <param name="startIndex"></param>
        /// <param name="url"></param>
        private void HandleCollectionAdd(IList newItems, int startIndex, string url)
        {
            byte[] data = new byte[] { };
            foreach (ITunable newTunable in newItems)
            {
                string requestUrl = url + "?insert=" + startIndex.ToString();
                ++startIndex;

                if (newTunable is StructureTunable || newTunable is PointerTunable)
                {
                    ITunableSerialisable structure = (ITunableSerialisable)newTunable;
                    XmlDocument root = new XmlDocument();
                    XmlElement element = structure.Serialise(root, true);

                    MemoryStream stream = new MemoryStream();

                    XmlWriter writer = new XmlTextWriter(stream, Encoding.UTF8);
                    element.WriteTo(writer);
                    writer.Flush();

                    //string value = newTunable.ToXmlString();
                    //value = value.Replace( "\"", "" );
                    //data =  System.Text.Encoding.UTF8.GetBytes( value );
                    data = new byte[(int)stream.Length];
                    stream.Position = 0;
                    stream.Read(data, 0, (int)stream.Length);
                }

                AddWebRequest("POST", requestUrl, data, newTunable);

                AddTunableListeners(newTunable);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tunable"></param>
        /// <param name="oldItems"></param>
        /// <param name="startIndex"></param>
        /// <param name="url"></param>
        private void HandleCollectionRemove(IList oldItems, int startIndex, string url)
        {
            byte[] dummyData = new byte[] { };
            string requesturl = url + "?delete=" + startIndex.ToString();

            foreach (ITunable newTunable in oldItems)
            {
                AddWebRequest("POST", requesturl, dummyData, newTunable);

                RemoveTunableListeners(newTunable);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tunable"></param>
        /// <param name="url"></param>
        private void HandleTunableBaseModified(TunableBase tunable, string url)
        {
            // Create data for URL request
            String value = tunable.ToXmlString();
            if (!String.IsNullOrEmpty(value))
            {
                value = value.Replace("\"", "");
                byte[] data = System.Text.Encoding.UTF8.GetBytes(value);

                AddWebRequest("PUT", url, data, tunable);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tunable"></param>
        /// <param name="url"></param>
        private void SendRootStructure(StructureTunable tunable, string url)
        {
            string value = tunable.ToXmlString();
            byte[] data = System.Text.Encoding.UTF8.GetBytes(value);

            AddWebRequest("PUT", url, data, tunable);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="method"></param>
        /// <param name="url"></param>
        /// <param name="data"></param>
        /// <param name="tunable"></param>
        private void AddWebRequest(string method, string url, byte[] data, ITunable tunable)
        {
            RestRequest request = new RestRequest();
            request.method = method;
            request.url = url;
            request.data = data;
            request.tunable = tunable;
            m_requestQueue.Add(request);
        }

        /// <summary>
        /// Creates the url for a particular tunable
        /// </summary>
        /// <param name="tunable"></param>
        /// <returns></returns>
        private String GetTunableURL(ITunable tunable)
        {
            // Build url string.
            string tunablePath = Tunable.GetFullPath(tunable);
            return m_liveLinkUrl.ToString() + "/" + tunablePath;
        }
        #endregion // IWeakEventListener Implementation

        #region Background Live-Link Thread
        /// <summary>
        /// Thread that processes any requests that are in the queue
        /// </summary>
        private void WebRequestThread()
        {
            foreach (RestRequest request in m_requestQueue.GetConsumingEnumerable())
            {
                ProcessRequest(request);
            }
        }

        /// <summary>
        /// Process a single rest request
        /// </summary>
        /// <param name="request"></param>
        private void ProcessRequest(RestRequest request)
        {
            using (WebClient client = new WebClient())
            {
                try
                {
                    client.UploadDataCompleted += (data_sender, data_ev) =>
                    {
                        /*
                        Log.Log__Debug(
                            "Completed sending to {0}", 
                            Tunable.GetFullPath(tunable));
                        */
                        if (data_ev.Error != null)
                        {
                            WebResponse webResponse = ((System.Net.WebException)(data_ev.Error)).Response;
                            string response = null;

                            if (webResponse != null)
                            {
                                Stream responseStream = webResponse.GetResponseStream();
                                byte[] responseBuffer = new byte[responseStream.Length];
                                responseStream.Read(responseBuffer, 0, (int)responseStream.Length);
                                response = Encoding.UTF8.GetString(responseBuffer);
                                responseStream.Close();
                            }
                            else
                            {
                                response = "No response";
                            }

                            //Log.Log__Exception(
                            //    data_ev.Error,
                            //    ">>>> Failed sending updated data for tunable '{0}' to url {1}. Error was: {2} \n <<<<",
                            //    RSG.Metadata.Util.Tunable.GetFullPath(request.tunable),
                            //    request.url,
                            //    response);

                        }
                    };
                    client.UploadDataAsync(new Uri(request.url), request.method, request.data);
                }
                catch (WebException exception)
                {
                    Log.Log__Exception(
                        exception,
                        "Failed sending updated data for tunable '{0}' to url {1}",
                        request.tunable.Name,
                        request.url);
                }
            }
        }
        #endregion // Background Live-Link Thread

        #region Private helper methods

        /// <summary>
        /// Enumerate through the top-level members.
        /// </summary>
        /// <param name="useBreadcrumbs">True if the breadcrumbs property is to be used.</param>
        /// <returns>The enumeration of view models.</returns>
        private IEnumerable<ITunableViewModel> GetMembers(bool useBreadcrumbs)
        {
            if (useBreadcrumbs)
            {
                foreach (var item in BreadCrumbs)
                {
                    yield return item;
                }
            }
            else
            {
                foreach (var item in Members.Values)
                {
                    yield return item;
                }
            }
        }

        #endregion

        #region Drag and Drop
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dropInfo"></param>
        public void DragOver(DropInfo dropInfo)
        {
            dropInfo.Effects = System.Windows.DragDropEffects.Move;
            if (dropInfo.TargetItem == dropInfo.Data)
                dropInfo.Effects = System.Windows.DragDropEffects.None;
            if (!(dropInfo.TargetItem is ITunableViewModel) || !(dropInfo.Data is ITunableViewModel))
                dropInfo.Effects = System.Windows.DragDropEffects.None;

            if (dropInfo.Effects == System.Windows.DragDropEffects.None)
                return;

            ITunableViewModel data = dropInfo.Data as ITunableViewModel;
            ITunableViewModel target = dropInfo.TargetItem as ITunableViewModel;

            if (data.Parent != target.Parent)
                dropInfo.Effects = System.Windows.DragDropEffects.None;
            if (!(data.Parent is BasicViewModel.ArrayTunableViewModel) &&
                !(data.Parent is BasicViewModel.MapTunableViewModel))
                dropInfo.Effects = System.Windows.DragDropEffects.None;

            if (!data.Parent.Model.Definition.CanLiveEdit && this.IsLiveEditing)
            {
                dropInfo.Effects = System.Windows.DragDropEffects.None;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dropInfo"></param>
        public void Drop(DropInfo dropInfo)
        {
            ITunableViewModel data = dropInfo.Data as ITunableViewModel;
            ITunableViewModel target = dropInfo.TargetItem as ITunableViewModel;

            if (data.Parent is BasicViewModel.ArrayTunableViewModel)
            {
                if (!data.Parent.Model.Definition.CanLiveEdit && this.IsLiveEditing)
                {
                    return;
                }

                BasicViewModel.ArrayTunableViewModel array = data.Parent as BasicViewModel.ArrayTunableViewModel;

                int oldIndex = array.Members.IndexOf(data);
                if (target == array)
                    (array.Model as ArrayTunable).Move(oldIndex, 0);
                else
                    (array.Model as ArrayTunable).Move(oldIndex, array.Members.IndexOf(target));
            }
            else if (data.Parent is BasicViewModel.MapTunableViewModel)
            {
                if (!data.Parent.Model.Definition.CanLiveEdit && this.IsLiveEditing)
                {
                    return;
                }

                BasicViewModel.MapTunableViewModel map = data.Parent as BasicViewModel.MapTunableViewModel;
                int oldIndex = map.Members.IndexOf(data);
                if (target == map)
                    (map.Model as MapTunable).Move(oldIndex, 0);
                else
                    (map.Model as MapTunable).Move(oldIndex, map.Members.IndexOf(target));
            }
        }
        #endregion // Drag and Drop
    } // MetaFileViewModel
} // MetadataEditor.AddIn.MetaEditor.ViewModel
