﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MapViewport.AddIn
{
    /// <summary>
    /// 
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class OverlayParameterAttribute : Attribute
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public object DefaultValue
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public string FriendlyName
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="defaultValue"></param>
        public OverlayParameterAttribute(object defaultValue)
            : base()
        {
            DefaultValue = defaultValue;
        }
        #endregion // Constructor(s)
    } // OverlayParameterAttribute
}
