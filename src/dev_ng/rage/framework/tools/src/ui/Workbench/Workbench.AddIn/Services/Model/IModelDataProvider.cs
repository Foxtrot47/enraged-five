﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Model.Common.Map;

namespace Workbench.AddIn.Services.Model
{
    /// <summary>
    /// Exposes methods to get access to the model data.
    /// </summary>
    public interface IModelDataProvider
    {
        #region Methods
        /// <summary>
        /// Retrieves a new level collection for the requested datasource/build.
        /// </summary>
        /// <returns></returns>
        ILevelCollection RetrieveLevelCollection(DataSource source, String buildIdentifier = null);

        /// <summary>
        /// Requests that certain stats are loaded for a particular level.
        /// </summary>
        /// <param name="level"></param>
        /// <param name="statsToLoad"></param>
        void LoadStats(ILevel level, IEnumerable<StreamableStat> statsToLoad, bool foreground = false);
        #endregion // Methods
    } // IModelDataProvider
}
