﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn;
using Workbench.AddIn.Commands;

namespace LevelBrowser.AddIn.MapBrowser.Commands
{
    
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Extensions.MapSectionCommands, typeof(IWorkbenchCommand))]
    class PS3ObjectPositionCheckerCommand : ObjectPositionCheckerBase
    {
        #region Constants
        public static readonly String GUID = "142734CF-8F38-4119-B242-EA1530EE080C";
        public static readonly String PS3_BAT = "game_psn_bankrelease_snc.bat";
        #endregion // Constants
        
        #region Constructor
        /// <summary>
        /// Default constructor.
        /// </summary>
        public PS3ObjectPositionCheckerCommand()
            : base(new Guid(GUID), "_Object Position Checker [PS3]", PS3_BAT)
        {
        }
        #endregion // Constructor
    }

} // LevelBrowser.AddIn.MapBrowser.Commands namespace
