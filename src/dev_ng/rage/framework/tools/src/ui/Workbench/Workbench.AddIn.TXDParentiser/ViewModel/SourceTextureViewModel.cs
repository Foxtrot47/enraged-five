﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using RSG.Base.Collections;
using RSG.Model.GlobalTXD;
using RSG.Base.Windows.DragDrop;

namespace Workbench.AddIn.TXDParentiser.ViewModel
{
    public class SourceTextureViewModel : RSG.Base.Editor.HierarchicalViewModelBase, ITextureChildViewModel, IDragSource
    {
        #region Properties
        private float m_selectedSharedPercentage;
        public float SelectedSharedPercentage
        {
            get { return m_selectedSharedPercentage; }
            set
            {
                this.SetPropertyValue(
                    ref this.m_selectedSharedPercentage, value, "SelectedSharedPercentage");
            }
        }

        private int m_selectedSharedCount;
        public int SelectedSharedCount
        {
            get { return m_selectedSharedCount; }
            set
            {
                this.SetPropertyValue(
                    ref this.m_selectedSharedCount, value, "SelectedSharedCount");
            }
        }

        private int m_selectedSaveIfPromoted;
        public int SelectedSaveIfPromoted
        {
            get { return m_selectedSaveIfPromoted; }
            set
            {
                this.SetPropertyValue(
                    ref this.m_selectedSaveIfPromoted, value, "SelectedSaveIfPromoted");
            }
        }

        public bool IsDropTarget
        {
            get { return false; }
        }

        /// <summary>
        /// The model reference that this view model represents.
        /// </summary>
        public SourceTexture Model
        {
            get { return m_model; }
            set { m_model = value; }
        }
        private SourceTexture m_model;

        /// <summary>
        /// The parent global dictionary if it has one
        /// </summary>
        public new ITextureContainerViewModel Parent
        {
            get { return m_parent; }
            set
            {
                this.SetPropertyValue(ref this.m_parent, value, "Parent");
            }
        }
        private ITextureContainerViewModel m_parent;

        /// <summary>
        /// The global root object that this dictionary belongs to
        /// </summary>
        public GlobalRootViewModel Root
        {
            get { return m_root; }
            set
            {
                this.SetPropertyValue(ref this.m_root, value, "Root");
            }
        }
        private GlobalRootViewModel m_root;

        /// <summary>
        /// The user data that can be attached to this view model
        /// </summary>
        public RSG.Base.Collections.UserData ViewModelUserData
        {
            get { return m_viewModelUserData; }
            set { m_viewModelUserData = value; }
        }
        private RSG.Base.Collections.UserData m_viewModelUserData;

        /// <summary>
        /// Respresents where the UIElement bound to this object currently has focus
        /// </summary>
        public bool IsFocused
        {
            get { return m_isFocused; }
            set
            {
                this.SetPropertyValue(ref this.m_isFocused, value, "IsFocused");
            }
        }
        private bool m_isFocused = false;
        
        /// <summary>
        /// A small get property that returns the smallest possible
        /// thumbnail for this texture
        /// </summary>
        public BitmapImage ThumbnailSmall
        {
            get
            {
                if (System.IO.File.Exists(this.Model.Filename))
                {
                    Random random = new Random();
                    System.Threading.Thread.Sleep(random.Next(0, 20));
                    Uri uriSource = new Uri(this.Model.Filename, UriKind.RelativeOrAbsolute);

                    BitmapImage small = new BitmapImage();
                    small.BeginInit();
                    small.DecodePixelWidth = 1;
                    small.CreateOptions = BitmapCreateOptions.DelayCreation;
                    small.CacheOption = BitmapCacheOption.None;
                    small.UriSource = uriSource;
                    small.EndInit();

                    small.Freeze();

                    return small;
                }
                return null;
            }
        }

        /// <summary>
        /// The thumbnail that for this texture, and small 16x16
        /// scaled image of the texture.
        /// This has to be bound to using IsAsync or it'll be too long a delay to show
        /// a full texture dictionary
        /// </summary>
        public BitmapImage Thumbnail
        {
            get
            {
                if (System.IO.File.Exists(this.Model.Filename))
                {
                    Uri uriSource = new Uri(this.Model.Filename, UriKind.RelativeOrAbsolute);

                    BitmapImage small = new BitmapImage();
                    small.BeginInit();
                    small.DecodePixelWidth = 256;
                    small.CreateOptions = BitmapCreateOptions.DelayCreation;
                    small.CacheOption = BitmapCacheOption.None;
                    small.UriSource = uriSource;
                    small.EndInit();

                    small.Freeze();

                    return small;
                }
                return null;
            }
            set
            {
                m_thumbnail = value;
            }
        }
        private BitmapImage m_thumbnail = null;

        /// <summary>
        /// A small get property that returns the smallest possible
        /// thumbnail for this texture
        /// </summary>
        public BitmapImage ThumbnailFast
        {
            get
            {
                return null;
            }
        }

        public string StreamName { get { return this.Model.StreamName; } }
        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        public SourceTextureViewModel(SourceTexture model, SourceTextureDictionaryViewModel parent)
        {
            this.Model = model;
            this.Parent = parent;
            this.Root = parent.Root;
            this.ViewModelUserData = new RSG.Base.Collections.UserData();
        }

        #endregion // Constructors

        public void OnThumbnailCreated()
        {
            OnPropertyChanged("Thumbnail");
        }

        #region IDragSource

        public Object StartDrag(DragInfo dragInfo)
        {
            dragInfo.Effects = DragDropEffects.Move;
            if (!this.Model.Promoted)
                return this.Model;
            else
                return null;
        }

        #endregion // IDragSource

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        public void ExpandedTo()
        {
            if (this.Parent != null)
                this.Parent.Expand();
            if (this.Parent is IDictionaryChildViewModel)
                (this.Parent as IDictionaryChildViewModel).ExpandedTo();
        }
        #endregion
    }
} // Workbench.AddIn.TXDParentiser.ViewModel
