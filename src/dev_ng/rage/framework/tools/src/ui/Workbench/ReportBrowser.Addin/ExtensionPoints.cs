﻿using System;

namespace Report.AddIn
{
    public class ExtensionPoints
    {
        /// <summary>
        /// Builder's report category extension point.
        /// </summary>
        public const String BuilderReport = "3EB22486-8037-476A-AB1B-0A42CD9018D7";

        /// <summary>
        /// Report category extension point.
        /// </summary>
        public const String ReportCategory = "5D831C60-E395-4A93-9A37-992C88D4CA71";
 
        /// <summary>
        /// Map Stats Report category extension point.
        /// </summary>
        public const String MapStatsReport = "52F2C565-B726-4D0D-BEFE-4E1E2CE7AE0F";
    }
}