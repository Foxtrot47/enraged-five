﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Workbench.AddIn.TXDParentiser;
using RSG.Model.GlobalTXD.ViewModel;

namespace Workbench.AddIn.TXDParentiser.UI.Views
{
    ///// <summary>
    ///// Interaction logic for TextureStatistics.xaml
    ///// </summary>
    //public partial class TextureStatistics : UserControl, INotifyPropertyChanged
    //{
    //    #region Property change events

    //    /// <summary>
    //    /// Property changed event fired when the any of the properties change
    //    /// </summary>
    //    public event PropertyChangedEventHandler PropertyChanged;

    //    /// <summary>
    //    /// Create the OnPropertyChanged method to raise the event
    //    /// </summary>
    //    /// <param name="name"></param>
    //    protected void OnPropertyChanged(string name)
    //    {
    //        PropertyChangedEventHandler handler = PropertyChanged;
    //        if (handler != null)
    //        {
    //            handler(this, new PropertyChangedEventArgs(name));
    //        }
    //    }

    //    #endregion // Property change events

    //    #region Properties

    //    /// <summary>
    //    /// The name of the texture that is being displayed
    //    /// </summary>
    //    public String TextureName
    //    {
    //        get { return m_textureName; }
    //        set { m_textureName = value; OnPropertyChanged("TextureName"); }
    //    }
    //    private String m_textureName;

    //    /// <summary>
    //    /// The stream name of the texture that is being displayed
    //    /// </summary>
    //    public String StreamName
    //    {
    //        get { return m_streamName; }
    //        set { m_streamName = value; OnPropertyChanged("StreamName"); }
    //    }
    //    private String m_streamName;

    //    /// <summary>
    //    /// The size of the texture that is being displayed
    //    /// </summary>
    //    public float TextureSize
    //    {
    //        get { return m_textureSize; }
    //        set { m_textureSize = value; OnPropertyChanged("TextureSize"); }
    //    }
    //    private float m_textureSize;

    //    /// <summary>
    //    /// The filename for the texture
    //    /// </summary>
    //    public String Filename
    //    {
    //        get { return m_filename; }
    //        set { m_filename = value; OnPropertyChanged("Filename"); }
    //    }
    //    private String m_filename;

    //    /// <summary>
    //    /// The filename for the alpha texture if one is there or a copy of the filename
    //    /// </summary>
    //    public String AlphaFilename
    //    {
    //        get { return m_alphaFilename; }
    //        set { m_alphaFilename = value; OnPropertyChanged("AlphaFilename"); }
    //    }
    //    private String m_alphaFilename;

    //    /// <summary>
    //    /// The actual texture that is being displayed
    //    /// </summary>
    //    public TextureViewModel Source
    //    {
    //        get { return m_Source; }
    //        set { m_Source = value; OnPropertyChanged("Source"); }
    //    }
    //    private TextureViewModel m_Source;

    //    #endregion // Properties

    //    #region Constructor(s)

    //    /// <summary>
    //    /// Default constructor
    //    /// </summary>
    //    public TextureStatistics()
    //    {
    //        InitializeComponent();
    //    }

    //    #endregion // Constructor(s)

    //    #region Event Handlers

    //    /// <summary>
    //    /// Using this event set the properties of this object to the propertes of the
    //    /// texture (if it is a texture)
    //    /// </summary>
    //    /// <param name="sender">The object that sent the event</param>
    //    /// <param name="e">Additional parameters that the event has</param>
    //    private void OnDataContextChanged(Object sender, DependencyPropertyChangedEventArgs e)
    //    {
    //        if (!(e.NewValue is TextureViewModel))
    //        {
    //            this.Source = null;
    //            return;
    //        }

    //        this.Source = e.NewValue as TextureViewModel;

    //        this.TextureName = System.IO.Path.GetFileNameWithoutExtension(this.Source.Filename);
    //        this.Filename = this.Source.Filename;
    //        if (this.Source.Model.HasAlphaTexture == true)
    //        {
    //            this.AlphaFilename = this.Source.Model.AlphaFilepath;
    //        }
    //        else
    //        {
    //            this.AlphaFilename = this.Source.Filename;
    //        }

    //        if (String.IsNullOrEmpty(this.Source.Model.StreamName))
    //        {
    //            this.StreamName = "Not found in stream";
    //        }
    //        else
    //        {
    //            this.StreamName = this.Source.Model.StreamName;
    //            this.TextureSize = this.Source.TextureSize;
    //        }
    //    }

    //    #endregion // Event Handlers;

    //}
}
