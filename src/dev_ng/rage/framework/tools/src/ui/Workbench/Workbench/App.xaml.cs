﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows;
using System.Xml.Linq;
using RSG.Base;
using RSG.Base.Configuration;
using RSG.Base.Configuration.Bugstar;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.Net;
using RSG.Base.Windows;
using RSG.Interop.Microsoft;
using RSG.Interop.Bugstar.Organisation;
using RSG.Interop.Bugstar;
using RSG.UniversalLog;
using Workbench.UI;
using Workbench.AddIn;
using Workbench.AddIn.Services;
using Workbench.AddIn.Services.File;

namespace Workbench
{
    /// <summary>
    /// 
    /// </summary>
    public partial class App :
        Application,
        IPartImportsSatisfiedNotification
    {
        #region Constants
        /// <summary>
        /// 
        /// </summary>
        private const String AUTHOR = "David Muir";

        /// <summary>
        /// 
        /// </summary>
        private const String AUTHOR_EMAIL = "david.muir@rockstarnorth.com";

        /// <summary>
        /// Instance mutex string; "Local" allows application to start in different
        /// UserSessions on same PC (e.g. Quick User Change).  Using "Global"
        /// will prevent that.
        /// </summary>
        private const String INSTANCE_MUTEX = "Local\\RockstarGames.Workbench.Instance";
        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.CoreServicesProvider, 
            typeof(ICoreServiceProvider))]
        private ICoreServiceProvider ViewModel { get; set; }

        /// <summary>
        /// Application startup commands.
        /// </summary>
        [ImportManyExtension(Workbench.AddIn.ExtensionPoints.StartupCommand,
            typeof(IApplicationCommand), AllowRecomposition = true)]
        private IEnumerable<IApplicationCommand> StartupCommands { get; set; }

        /// <summary>
        /// Application shutdown commands.
        /// </summary>        
        [ImportManyExtension(Workbench.AddIn.ExtensionPoints.ShutdownCommand,
            typeof(IApplicationCommand), AllowRecomposition = true)]
        private IEnumerable<IApplicationCommand> ShutdownCommands { get; set; }

        /// <summary>
        /// Non-specific parts of the composition; typically used for exports
        /// so they can import specific items.
        /// </summary>
        [ImportManyExtension(Workbench.AddIn.ExtensionPoints.Void,
            typeof(Object), AllowRecomposition=true)]
        private IEnumerable<Object> VoidObjects { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ExtensionService,
            typeof(IExtensionService))]
        private IExtensionService ExtensionService { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.WorkbenchFileManager, typeof(Workbench.AddIn.Services.File.IFileManager))]
        private Workbench.AddIn.Services.File.IFileManager FileManager { get; set; }
        
        /// <summary>
        /// MEF Import for Workbench's task progress service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.TaskProgressService, typeof(Workbench.AddIn.Services.ITaskProgressService))]
        private ITaskProgressService TaskProgressService { get; set; }

        /// <summary>
        /// MEF Import for the Application Arguments Service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ApplicationArgumentsService,
            typeof(IApplicationArgumentsService))]
        private IApplicationArgumentsService ApplicationArgService { get; set; }

        /// <summary>
        /// Non-specific parts of the composition; typically used for exports
        /// so they can import specific items.
        /// </summary>
        [ImportManyExtension(Workbench.AddIn.ExtensionPoints.OpenService,
            typeof(IOpenService))]
        private IEnumerable<IOpenService> OpenServices { get; set; }
        
        /// <summary>
        /// Non-specific parts of the composition; typically used for exports
        /// so they can import specific items.
        /// </summary>
        [ImportManyExtension(Workbench.AddIn.ExtensionPoints.ToolbarResource, typeof(ResourceDictionary))]
        private IEnumerable<ResourceDictionary> ToolbarResources { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.BugstarService,
        typeof(Workbench.AddIn.Services.IBugstarService))]
        private IBugstarService BugstarService { get; set; }
        #endregion // MEF Imports

        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private CompositionContainer m_CompContainer = null;

        /// <summary>
        /// 
        /// </summary>
        private bool m_bStartupCommandsExecuted = false;

        /// <summary>
        /// 
        /// </summary>
        internal IUniversalLogTarget m_uLogFile = null;

        internal string layoutArgument = null;

        /// <summary>
        /// 
        /// </summary>
        private Window m_splashWindow;
        #endregion // Member Data
        
        #region Constructor(s) / Destructor
        /// <summary>
        /// Default constructor.
        /// </summary>
        public App()
            : base()
        {
            Thread thread = Thread.CurrentThread;
            thread.Name = "Main";

            UpgradeSettings();

            // Setup the log for the workbench
            LogFactory.Initialize();
            Log.StaticLog.Name = Assembly.GetEntryAssembly().GetName().Name;
            m_uLogFile = LogFactory.CreateUniversalLogFile(Log.StaticLog);
        }
        #endregion // Constructor(s) / Destructor

        #region IPartImportsSatisfiedNotification Methods
        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {
            // Add any resource dictionaries that the toolbars need
            foreach (ResourceDictionary dict in ToolbarResources)
            {
                this.Resources.MergedDictionaries.Add(dict);
            }

            this.MainWindow.DataContext = this.ViewModel;
            (this.MainWindow as WorkbenchView).ApplicationArgumentsService = ApplicationArgService;
            this.MainWindow.Loaded += (this.MainWindow as WorkbenchView).Window_Loaded;
#warning DHM FIX ME: uuuurgggggh!  To get a service?  "ViewModel"?
            this.ViewModel.LayoutManager.Loaded += new EventHandler(LayoutManager_Loaded);

            this.TaskProgressService.ProgressChanged += new TaskProgressEventHandler(TaskProgressService_ProgressChanged);

            // Invoke startup commands.
            if (!m_bStartupCommandsExecuted)
            {
                ICollection<IApplicationCommand> commands =
                    ExtensionService.Sort(this.StartupCommands);
                RunApplicationCommands(commands);

                m_bStartupCommandsExecuted = false;
            }
        }
        #endregion // IPartImportsSatisfiedNotification Methods

        #region Private Methods
        /// <summary>
        /// Execute a collection of IApplicationCommand objects.
        /// </summary>
        /// <param name="commands">Collection of IApplicationCommand objects.</param>
        private void RunApplicationCommands(ICollection<IApplicationCommand> commands)
        {
            foreach (IApplicationCommand command in commands)
            {
                try
                {
                    Log.Log__Debug("Running command ID: {0}.",
                        command.ID);
                    command.Execute();
                }
                catch (Exception ex)
                {
                    Log.Log__Exception(ex, "Unhandled exception during command ID: {0}.",
                        command.ID);
                }
            }
        }

        /// <summary>
        /// Upgrade our settings from the previous version.
        /// </summary>
        private void UpgradeSettings()
        {
            try
            {
                try
                {
                    if (Workbench.Properties.Settings.Default.NeedsUpdating == true)
                    {
                        Workbench.Properties.Settings.Default.Upgrade();
                        Workbench.Properties.Settings.Default.NeedsUpdating = false;
                        Workbench.Properties.Settings.Default.Save();
                    }
                }
                catch (System.Configuration.ConfigurationException ex)
                {
                    Log.Log__Exception(ex, "Configuration data exception during settings upgrade; resetting to default.");
                    Workbench.Properties.Settings.Default.Reset();
                }
            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex, "Uncategorised exception during settings upgrade or reset; not resetting to default.");
            }
        }

        /// <summary>
        /// MEF compose.
        /// </summary>
        /// <returns></returns>
        private bool Compose()
        {
            try
            {
                AggregateCatalog catalog = new AggregateCatalog();
                catalog.Catalogs.Add(new AssemblyCatalog(
                    Assembly.GetExecutingAssembly()));
                catalog.Catalogs.Add(new DirectoryCatalog(
                    Workbench.Properties.Settings.Default.PluginPath));

                m_CompContainer = new CompositionContainer(catalog);

                try
                {
                    m_CompContainer.ComposeParts(this);
                }
                catch (CompositionException ex)
                {
                    ExceptionStackTraceDlg dlg = new ExceptionStackTraceDlg(ex,
                        Workbench.Resources.Strings.Author,
                        Workbench.Resources.Strings.AuthorEmail);
                    dlg.ShowDialog();

                    return (false);
                }
            }
            catch (Exception ex)
            {
                ExceptionStackTraceDlg dlg = new ExceptionStackTraceDlg(ex,
                    Workbench.Resources.Strings.Author,
                    Workbench.Resources.Strings.AuthorEmail);
                dlg.ShowDialog();

                return (false);
            }

            return (true);
        }
        #endregion // Private Methods

        #region Application Overridden Methods
        /// <summary>
        /// Application OnStartup Handler
        /// </summary>
        /// <param name="e"></param>
        protected override void OnStartup(StartupEventArgs e)
        {
            Log.Log__Profile("Workbench OnStartup PreShow");

            // Single Instance check.
            if (Workbench.Properties.Settings.Default.SingleInstance)
            {
#if DEBUG
                String name = String.Format("{0}_DEBUG", Assembly.GetExecutingAssembly().GetName().Name);
#else
                String name = Assembly.GetExecutingAssembly().GetName().Name;
#endif
                if (!ApplicationInstanceManager.CreateSingleInstance(name, InstanceStartupCallback))
                {
                    // Cannot use the MessageService because it won't have
                    // been composed by this point in the secondary instance.
#if DEBUG
                    MessageBox.Show("Workbench instance already running; arguments will be passed to existing instance for processing.");
#endif // DEBUG
                    Environment.Exit(0); // Exit if instance already running.
                }
            }

            m_splashWindow = new SplashWindow();
            m_splashWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            m_splashWindow.Show();

            base.MainWindow = new WorkbenchView();
            this.MainWindow.Closing += MainWindow_Closing;
            this.MainWindow.IsVisibleChanged += new DependencyPropertyChangedEventHandler(MainWindow_IsVisibleChanged);

            // Make sure the extensions folder exists, it might not exist if
            // we haven't built any yet.
            if (!System.IO.Directory.Exists(Workbench.Properties.Settings.Default.PluginPath))
            {
                System.IO.Directory.CreateDirectory(Workbench.Properties.Settings.Default.PluginPath);
            }


            // Otherwise continue startup.
            base.OnStartup(e);
            
            this.DispatcherUnhandledException += Application_DispatcherUnhandledException;
            AppDomain.CurrentDomain.UnhandledException += Application_AppDomainUnhandledException;

            StringBuilder args = new StringBuilder();
            foreach (String a in e.Args)
            {
                if (a.StartsWith("--layout"))
                {
                    this.layoutArgument = a.Replace("--layout", "");
                    this.layoutArgument = this.layoutArgument.Replace("=", "");
                    continue;
                }

                args.AppendFormat("{0} ", a);
            }

            Services.ApplicationArgumentsService.SetArguments(args.ToString());

            Log.Log__Profile("Composition");
            bool composeSuccessful = Compose();
            Log.Log__ProfileEnd();
            Log.Log__ProfileEnd();
            if (composeSuccessful)
            {
                CheckBrowserVersion();
                MainWindow.Show();
            }
            else
            {
                Shutdown();
            }
        }

        /// <summary>
        /// Hide the splash screen once the main form has been shown.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void MainWindow_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            this.MainWindow.IsVisibleChanged -= MainWindow_IsVisibleChanged;
            m_splashWindow.Close();
        }


        /// <summary>
        /// Function invoked when another instance tries to startup; and when
        /// user has enable "Single Instance" option.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void InstanceStartupCallback(Object sender, InstanceCallbackEventArgs e)
        {
            if (e.IsFirstInstance)
                return;

            Log.Log__Debug("Another Workbench instance tried to startup; only allowing once instance.");
            Log.Log__Debug("Arguments: {0}.", e.CommandLineArgs);

            // Skip the main exe name.
            String[] args = new String[e.CommandLineArgs.Length - 1];
            Array.Copy(e.CommandLineArgs, 1, args, 0, e.CommandLineArgs.Length - 1);
            if (args.Length == 0)
            {
                return;
            }

            if (args[0].StartsWith("--layout"))
            {
                string layoutname = args[0].Replace("--layout", "");
                layoutname = layoutname.Replace("=", "");

                WorkbenchViewModel vm = this.ViewModel as WorkbenchViewModel;
                if (vm == null)
                {
                    return;
                }

                vm.ExternalLayoutCommandRecieved(layoutname);
            }
            else
            {
                this.ApplicationArgService.InjectArguments(args);
            }
        }

        void MainWindow_Closing(Object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (this.FileManager != null)
            {
                if (!FileManager.OnExiting())
                    e.Cancel = true;
            }
        }

        /// <summary>
        /// Application OnExit Handler
        /// </summary>
        /// <param name="e"></param>
        protected override void OnExit(ExitEventArgs e)
        {            
            if (this.ViewModel.LayoutManager != null)
            {
                this.ViewModel.LayoutManager.OnExit();

                Workbench.Properties.Settings.Default.LastPresentLayout = this.ViewModel.LayoutManager.GetLayoutSerialization();

                Workbench.Properties.Settings.Default.OpenedFiles = new System.Collections.Specialized.StringCollection();
                foreach (var document in this.ViewModel.LayoutManager.Documents)
                {
                    if (document.OpenService != null && !string.IsNullOrEmpty(document.Path))
                    {
                        Workbench.Properties.Settings.Default.OpenedFiles.Add(string.Format("{0}[{1}]", document.Path, document.OpenService.ID.ToString()));
                    }
                }
            }

            Workbench.Properties.Settings.Default.Save();

            // Run all of our shutdown commands.
            if (this.ShutdownCommands != null)
            {
                ICollection<IApplicationCommand> commands = ExtensionService.Sort(this.ShutdownCommands);
                RunApplicationCommands(commands);
            }

            if (this.TaskProgressService != null)
            {
                TaskProgressService.Stop();
                this.TaskProgressService.ProgressChanged -= TaskProgressService_ProgressChanged;
            }

            Thread.Sleep(250);

            if (null != this.m_CompContainer)
            {
                this.m_CompContainer.Dispose();
            }
            base.OnExit(e);
        }
        #endregion // Application Overridden Methods

        #region Application Events
        /// <summary>
        /// Dispatcher unhandled exception event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Application_DispatcherUnhandledException(Object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            RSG.Base.Windows.ExceptionStackTraceDlg dlg =
                new RSG.Base.Windows.ExceptionStackTraceDlg(e.Exception, null, AUTHOR, AUTHOR_EMAIL);

            IBugstarService service = this.BugstarService;
            if (service != null)
            {
                BugstarConnection connection = service.Connection;
                if (!connection.IsReadOnly)
                {
                    dlg.LogBug += LogBug;
                }
            }
            
            dlg.ShowDialog();
            e.Handled = true;
        }

        void LogBug(object sender, LogBugEventArgs e)
        {
            try
            {
                IBugstarService service = this.BugstarService;
                BugstarConnection connection = service.Connection;
                uint projectId = ConfigFactory.CreateBugstarConfig().ProjectId;
                Project project = Project.GetProjectById(connection, projectId);
                BugBuilder builder = new BugBuilder(project);

                builder.Summary = e.Summary;
                builder.Description = e.Description;
                builder.Category = BugCategory.A;
                builder.Priority = BugPriority.P3;
                builder.State = BugState.Dev;
                IEnumerable<Component> components = project.Components;
                Component bugComponent = components.Where(component => 0 == String.Compare(component.FullName, "Tools", true)).FirstOrDefault();
                if (bugComponent != null)
                {
                    builder.ComponentId = bugComponent.Id;
                }

                Team team = Team.GetTeamById(project, 9420);
                if (team != null)
                {
                    builder.DeveloperId = team.Id;
                }

                IEnumerable<User> users = project.GetUsersList();
                User tester = users.Where(user => 0 == String.Compare(user.UserName, Environment.GetEnvironmentVariable("USERNAME"), true)).FirstOrDefault();
                User currentUser = project.CurrentUser;
                if (tester != null)
                {
                    builder.TesterId = tester.Id;
                }

                builder.Tags.Add("Auto Tool Bug");
                Bug bug = builder.ToBug(connection);
            }
            catch (Exception ex)
            {
                string msg = string.Format("Unable to add bug due to a {0} exception.", ex.GetType().Name);
                MessageBox.Show(msg, "Error", MessageBoxButton.OK, MessageBoxImage.Error); 
            }
        }

        /// <summary>
        /// AppDomain unhandled exception event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Application_AppDomainUnhandledException(Object sender, UnhandledExceptionEventArgs e)
        {
            Exception ex = (e.ExceptionObject as Exception);
            RSG.Base.Windows.ExceptionStackTraceDlg dlg =
                new RSG.Base.Windows.ExceptionStackTraceDlg(ex, null, AUTHOR, AUTHOR_EMAIL);

            IBugstarService service = this.BugstarService;
            if (service != null)
            {
                BugstarConnection connection = service.Connection;
                if (!connection.IsReadOnly)
                {
                    dlg.LogBug += LogBug;
                }
            }

            dlg.ShowDialog();
        }
        #endregion // Application Events

        #region Private helper methods

        /// <summary>
        /// Check the browser version.
        /// </summary>
        private void CheckBrowserVersion()
        {
            String webControlVersion = Workbench.Properties.Settings.Default.IEWebBrowser;
            int minIeVersion = Workbench.Properties.Settings.Default.IEMinimumVersion;

            if (!InternetExplorer.IsMinimumVersionInstalled(minIeVersion))
            {
                String message = String.Format("Microsoft Internet Explorer version {0} or greater is required.  Installed version: {1}.",
                    minIeVersion, InternetExplorer.VersionInstalled());
                this.ViewModel.UserInterfaceService.Show(message, MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                InternetExplorerVersion version = InternetExplorerVersion.Default;
                if (Enum.TryParse<InternetExplorerVersion>(webControlVersion, out version))
                {
                    InternetExplorer.SetEmbeddedVersionFor(version, "workbench", true);
                }
            }            
        }

        #endregion

        #region Layout Events
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void LayoutManager_Loaded(object sender, EventArgs e)
        {
            if (Workbench.Properties.Settings.Default.OpenedFiles == null)
                return;
            if (Workbench.Properties.Settings.Default.OpenedFiles.Count == 0)
                return;

            Dictionary<string, IOpenService> openServices = new Dictionary<string, IOpenService>();
            foreach (var openService in OpenServices)
            {
                openServices.Add(openService.ID.ToString(), openService);
            }
            foreach (var file in Workbench.Properties.Settings.Default.OpenedFiles)
            {
                string[] parts = file.Split(new char[] { '[', ']' }, StringSplitOptions.RemoveEmptyEntries);
                if (parts.Length != 2)
                    continue;
                if (!System.IO.File.Exists(parts[0]))
                    continue;
                string openID = parts[1];
                if (!openServices.ContainsKey(openID))
                    continue;

                Workbench.AddIn.UI.Layout.IDocumentBase doc = null;
                RSG.Base.Editor.IModel model = null;
                openServices[openID].Open(out doc, out model, parts[0], -1);
                if (doc != null)
                {
                    doc.OpenService = openServices[openID];
                    doc.Path = parts[0];
                    this.ViewModel.LayoutManager.ShowDocument(doc);
                }
            }
        }

        /// <summary>
        /// Task progress service progress changed event handler.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        void TaskProgressService_ProgressChanged(object sender, TaskProgressServiceArgs e)
        {
            ViewModel.TaskPercentageComplete = e.PercentageComplete;
            ViewModel.TaskMessage = e.Message;
        }
        #endregion
    }
} // Workbench namespace
