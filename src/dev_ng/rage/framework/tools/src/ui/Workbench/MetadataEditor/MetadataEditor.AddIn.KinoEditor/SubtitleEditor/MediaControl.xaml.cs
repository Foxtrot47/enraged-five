﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;
using System.Windows.Threading;
using System.Globalization;
using System.ComponentModel;
using System.Windows.Interop;

namespace MetadataEditor.AddIn.KinoEditor.Subtitle_Editor
{
    /// <summary>
    /// Interaction logic for MediaControl.xaml
    /// </summary>
    public partial class MediaControl : UserControl, INotifyPropertyChanged
    {
        /// <summary>
        /// Property changed event handler.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Total number of frames in the clip.
        /// </summary>
        public double TotalFrames
        {
            get
            {
                return m_totalFrames;
            }
            set
            {
                bool isDifferent = m_totalFrames != value;
                m_totalFrames = value;

                if (isDifferent)
                {
                    FirePropertyChange("TotalFrames");
                }
            }
        }

        private double m_totalFrames;
        
        /// <summary>
        /// The current frame.
        /// </summary>
        public double CurrentFrame
        {
            get { return m_currentFrame; }
            set
            {
                bool isDifferent = m_currentFrame != value;
                m_currentFrame = value;

                if (isDifferent)
                {
                    FirePropertyChange("CurrentFrame");
                }
            }
        }

        private double m_currentFrame;

        /// <summary>
        /// The number of frames per second for the video playback.
        /// </summary>
        private static readonly double c_framesPerSec = 30;

        bool isDragging = false;
        DispatcherTimer timer;  // handles position of slider and player
        TimeSpan thirtyFrames;
        private double Maximum = 0;

        public MediaControl()
        {
            InitializeComponent();
            thirtyFrames = new TimeSpan(0, 0, 0, 0, (int)(1000 / c_framesPerSec));
            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(0.1);
            timer.Tick += new EventHandler(timer_Tick);
            m_totalFrames = -1;
            m_currentFrame = -1;
        }

        /// <summary>
        /// Fire the property change event for the given property name.
        /// </summary>
        /// <param name="propertyName">Property name.</param>
        private void FirePropertyChange(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void UserControl_DataContextChanged(object senderv, DependencyPropertyChangedEventArgs ev)
        {
            ((SubtitleEditorViewModel)this.DataContext).LoadRequested += (sender, e) =>
            {
                mediaPlayerMain.Play();
                mediaPlayerMain.Pause();
                mediaPlayerMain.Position = TimeSpan.Zero;
                PrintDetails(0);
            };
        }

        void timer_Tick(object sender, EventArgs e)
        {
            if (!isDragging)
            {
                slider1.Value = mediaPlayerMain.Position.TotalSeconds;
            }
            else
            {
                mediaPlayerMain.Position = TimeSpan.FromSeconds(slider1.Value);
            }

            double currentSeconds = slider1.Value;
            if (mediaPlayerMain.NaturalDuration.HasTimeSpan)
            {
                TotalFrames = (int)(mediaPlayerMain.NaturalDuration.TimeSpan.TotalSeconds * c_framesPerSec);
                CurrentFrame = (int)(Math.Ceiling(currentSeconds * c_framesPerSec));
            }
        }

        private void PrintDetails(double currentSeconds)
        {
            if (mediaPlayerMain.NaturalDuration.HasTimeSpan)
            {
                double frames = (int)(mediaPlayerMain.NaturalDuration.TimeSpan.TotalSeconds * c_framesPerSec);
                double currentFrame = (int)(Math.Ceiling(currentSeconds * c_framesPerSec));

                string details = mediaPlayerMain.Source.LocalPath;
                details += "\n\n";
                details += String.Format("{0} / {1} frames", currentFrame, frames);

                tbPosition.Text = details;
            }
        }

        private void sliderPosition_ValueChanged(
            object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (isDragging)
            {
                this.mediaPlayerMain.Position = TimeSpan.FromSeconds(slider1.Value);
            }

            PrintDetails(e.NewValue);
        }

        private void sliderPosition_DragStarted(
            object sender, DragStartedEventArgs e)
        {
            mediaPlayerMain.Pause();
            isDragging = true;
        }

        private void sliderPosition_DragCompleted(
            object sender, DragCompletedEventArgs e)
        {
            isDragging = false;
            this.mediaPlayerMain.Position = TimeSpan.FromSeconds(slider1.Value);
            this.mediaPlayerMain.Play();
            this.mediaPlayerMain.Pause();
        }

        private void mediaElement_MediaOpened(object sender, RoutedEventArgs e)
        {
            if (mediaPlayerMain.NaturalDuration.HasTimeSpan)
            {
                TimeSpan ts = mediaPlayerMain.NaturalDuration.TimeSpan;

                slider1.Maximum = ts.TotalSeconds;
                slider1.SmallChange = 1;
                slider1.LargeChange = Math.Min(10, ts.Seconds / 10);
                InvalidateVisual();
            }
            timer.Start();
        }
        private void mediaElement_MediaEnded(object sender, RoutedEventArgs e)
        {
            
        }

        private void btnPlay_Click(object sender, RoutedEventArgs e)
        {
            mediaPlayerMain.Play();
        }

        private void btnStepForward_Click(object sender, RoutedEventArgs e)
        {
            TimeSpan newPosition = mediaPlayerMain.Position + thirtyFrames;
            TimeSpan ts = mediaPlayerMain.NaturalDuration.TimeSpan;

            if (newPosition > ts)
            {
                newPosition = ts;
            }

            mediaPlayerMain.Position = newPosition;
            mediaPlayerMain.Play();
            mediaPlayerMain.Pause();
        }

        private void btnBackOneFrame_Click(object sender, RoutedEventArgs e)
        {
            TimeSpan newPosition = mediaPlayerMain.Position - thirtyFrames;
            if (newPosition < TimeSpan.Zero)
            {
                newPosition = TimeSpan.Zero;
            }

            mediaPlayerMain.Position = newPosition;
            mediaPlayerMain.Play();
            mediaPlayerMain.Pause();
        }

        private void btnPause_Click(object sender, RoutedEventArgs e)
        {
            mediaPlayerMain.Pause();
        }

        private void mediaPlayerMain_SourceUpdated(object sender, DataTransferEventArgs e)
        {
            mediaPlayerMain.Stop();
            PrintDetails(0);
        }

        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {
            HwndSource hwndSource = PresentationSource.FromVisual(this) as HwndSource;
            HwndTarget hwndTarget = hwndSource.CompositionTarget;
            hwndTarget.RenderMode = RenderMode.SoftwareOnly;
        }
    }
}

