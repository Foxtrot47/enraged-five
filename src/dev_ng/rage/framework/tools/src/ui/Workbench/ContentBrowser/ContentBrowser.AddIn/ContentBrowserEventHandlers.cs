﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Base.Windows.Helpers;
using RSG.Statistics.Common.Dto.GameAssets;

namespace ContentBrowser.AddIn
{
    

    /// <summary>
    /// Grid changed
    /// </summary>
    public class GridSelectionChangedEventArgs : ItemChangedEventArgs<IEnumerable<IAsset>>
    {
        public GridSelectionChangedEventArgs(IEnumerable<IAsset> oldItems, IEnumerable<IAsset> newItems)
            : base(oldItems, newItems)
        {
        }
    }

    public delegate void GridSelectionChangedEventHandler(Object sender, GridSelectionChangedEventArgs args);

    public delegate void SelectedPanelItemChangedHandler(IAsset oldValue, IAsset newValue);
}
