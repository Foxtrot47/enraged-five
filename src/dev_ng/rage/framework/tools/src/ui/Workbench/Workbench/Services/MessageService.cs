﻿using System;
using System.ComponentModel.Composition;
using System.Windows;
using Workbench.AddIn;

namespace Workbench.Services
{
    
    /// <summary>
    /// 
    /// </summary>
    [Obsolete("Use ICoreServiceProvider.UserInterfaceService instead.")]
    [ExportExtension(Workbench.AddIn.CompositionPoints.MessageService,
        typeof(Workbench.AddIn.Services.IMessageService))]
    class MessageService : Workbench.AddIn.Services.IMessageService
    {
        private static readonly string c_caption;

        /// <summary>
        /// Default constructor.
        /// </summary>
        public MessageService()
        {
        }

        static MessageService()
        {
            c_caption = Resources.Strings.MainWindow_Title;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public MessageBoxResult Show(String messageBoxText)
        {
            if (Application.Current != null)
            {
                return (MessageBoxResult)Application.Current.Dispatcher.Invoke(
                    new Func<MessageBoxResult>
                    (
                        delegate()
                        {
                            if (Application.Current.MainWindow != null)
                            {
                                return (MessageBox.Show(Application.Current.MainWindow, messageBoxText, c_caption));
                            }
                            else
                            {
                                return (MessageBox.Show(messageBoxText, c_caption));
                            }
                        }
                    ),
                    System.Windows.Threading.DispatcherPriority.Send);
            }
            else
            {
                return (MessageBox.Show(messageBoxText, c_caption));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="messageBoxText"></param>
        /// <param name="buttons"></param>
        /// <returns></returns>
        public MessageBoxResult Show(String messageBoxText, MessageBoxButton buttons)
        {
            if (System.Windows.Application.Current != null)
            {
                return (MessageBoxResult)Application.Current.Dispatcher.Invoke(
                    new Func<MessageBoxResult>
                    (
                        delegate()
                        {

                            if (Application.Current.MainWindow != null)
                            {
                                return (MessageBox.Show(Application.Current.MainWindow, messageBoxText, c_caption, buttons));
                            }
                            else
                            {
                                return (MessageBox.Show(messageBoxText, c_caption, buttons));
                            }
                        }
                    ),
                    System.Windows.Threading.DispatcherPriority.Send);
            }
            else
            {
                return (MessageBox.Show(messageBoxText, c_caption, buttons));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="messageBoxText"></param>
        /// <param name="buttons"></param>
        /// <param name="image"></param>
        /// <returns></returns>
        public MessageBoxResult Show(String messageBoxText, MessageBoxButton buttons, MessageBoxImage image)
        {
            if (System.Windows.Application.Current != null)
            {
                return (MessageBoxResult)Application.Current.Dispatcher.Invoke(
                    new Func<MessageBoxResult>
                    (
                        delegate()
                        {

                            if (Application.Current.MainWindow != null)
                            {
                                return (MessageBox.Show(Application.Current.MainWindow, messageBoxText, c_caption, buttons, image));
                            }
                            else
                            {
                                return (MessageBox.Show(messageBoxText, c_caption, buttons, image));
                            }
                        }
                    ),
                    System.Windows.Threading.DispatcherPriority.Send);
            }
            else
            {
                return (MessageBox.Show(messageBoxText, c_caption, buttons, image));
            }
        }
    }

} // Workbench.Services namespace
