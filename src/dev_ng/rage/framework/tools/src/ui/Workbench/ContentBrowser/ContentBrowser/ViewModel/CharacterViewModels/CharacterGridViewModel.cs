﻿using System;
using System.IO;
using RSG.Model.Character;
using ContentBrowser.ViewModel.GridViewModels;
using RSG.Model.Common;

namespace ContentBrowser.ViewModel.CharacterViewModels
{
    /// <summary>
    /// 
    /// </summary>
    internal class CharacterGridViewModel : GridViewModelBase
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// 
        /// </summary>
        public String RenderFilename
        {
            get
            {
                if (!String.IsNullOrEmpty(ArtDir))
                {
                    String filename = Path.Combine(ArtDir, "peds", "Renders", String.Format("{0}.jpg", this.Name));
                    if (File.Exists(filename))
                    {
                        return filename;
                    }
                }
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private String ArtDir
        {
            get;
            set;
        }
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public CharacterGridViewModel(ICharacter c, string artDir)
            : base(c)
        {
            ArtDir = artDir;
        }
        #endregion // Constructor(s)
    }

} // ContentBrowser.ViewModel.CharacterViewModels namespace
