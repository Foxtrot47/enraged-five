﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Collections;
using RSG.Model.GlobalTXD;

namespace Workbench.AddIn.TXDParentiser.ViewModel
{
    public interface IDictionaryContainerViewModel
    {
        /// <summary>
        /// Represents the level that this texture dictionary
        /// </summary>
        int Level { get; }

        /// <summary>
        /// Represents the level that this texture dictionary
        /// </summary>
        string Name { get; }

        /// <summary>
        /// If this is true it means that source texture dictionaries can be added to
        /// this container as children
        /// </summary>
        Boolean CanAcceptSourceDictionaries { get; }

        /// <summary>
        /// If this is true it means that global texture dictionaries can be added to
        /// this container as children
        /// </summary>
        Boolean CanAcceptGlobalDictionaries { get; }

        ObservableCollection<GlobalTextureDictionaryViewModel> GlobalTextureDictionaries { get; }

        /// <summary>
        /// A dictionary of uniquely named source texture dictionaries that are children of this global root
        /// The dictionaries are indexed by names and these names need to be unique.
        /// </summary>
        ObservableCollection<SourceTextureDictionaryViewModel> SourceTextureDictionaries { get; }

        IDictionaryContainerViewModel Parent { get; set; }

        /// <summary>
        /// Returns true iff this dictionary has a texture dictionary in it with
        /// the given name. If recursive is true it will recursive through all the texture
        /// dictionaries as well
        /// </summary>
        Boolean ContainsTextureDictionary(String name, Boolean recursive);

        /// <summary>
        /// Adds the given dictionary into the children texture dictionaries iff the name
        /// is unique.
        /// </summary>
        Boolean AddExistingGlobalDictionary(GlobalTextureDictionaryViewModel dictionary);

        /// <summary>
        /// Creates a new dictionary with the given name and adds it to this dictionary 
        /// iff the name is unique
        /// </summary>
        Boolean AddNewGlobalDictionary(String name);

        /// <summary>
        /// Creates a new source dictionary using the given texture dictionary as a 
        /// source guide.
        /// </summary>
        Boolean AddNewSourceDictionary(TextureDictionaryViewModel dictionary);

        /// <summary>
        /// Removes the texture dictionary with the given name from the list iff
        /// it exists.
        /// </summary>
        Boolean RemoveGlobalTextureDictionary(String name);

        /// <summary>
        /// Removes the given texture dictionary from the list iff
        /// it exists.
        /// </summary>
        Boolean RemoveGlobalTextureDictionary(GlobalTextureDictionaryViewModel dictionary);

        /// <summary>
        /// Removes the given texture dictionary from the list iff
        /// it exists.
        /// </summary>
        Boolean RemoveSourceTextureDictionary(SourceTextureDictionaryViewModel dictionary);

        void RaiseGlobalDictionariesChangedEvent();

        void RaiseSourceDictionariesChangedEvent();

        void Expand();
    }
} // Workbench.AddIn.TXDParentiser.ViewModel
