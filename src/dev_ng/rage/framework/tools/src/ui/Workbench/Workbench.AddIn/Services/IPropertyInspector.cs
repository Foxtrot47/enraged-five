﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Workbench.AddIn.Services
{
    /// <summary>
    /// The main interface for a property grid inspector
    /// </summary>
    public interface IPropertyInspector
    {
        #region Methods
        
        /// <summary>
        /// Pushes a object onto the property grid
        /// </summary>
        void Push(Object item);

        /// <summary>
        /// Pushes the given collection onto the property grid
        /// </summary>
        void PushRange(IEnumerable<Object> collection);

        /// <summary>
        /// Pops an object off the property grid
        /// </summary>
        void Pop(Object item);

        /// <summary>
        /// Pops the given collection off the property grid
        /// </summary>
        void PopRange(IEnumerable<Object> collection);

        /// <summary>
        /// Clears the property grid
        /// </summary>
        void Clear();

        #endregion // Methods
    } // IPropertyInspector
} // Workbench.AddIn.Services
