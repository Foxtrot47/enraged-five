﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using Workbench.AddIn.Services;

namespace Workbench.Model
{

    /// <summary>
    /// 
    /// </summary>
    internal class ExternalTool : 
        ModelBase,
        IExternalTool
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Command title to display on user-interface.
        /// </summary>
        public String Title
        {
            get { return m_sTitle; }
            set
            {
                SetPropertyValue(value, () => this.Title,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_sTitle = (String)newValue;
                        }
                ));
            }
        }
        private String m_sTitle;
        
        /// <summary>
        /// Command string to execute (pre-environment substitution).
        /// </summary>
        public String Command
        {
            get { return m_sCommand; }
            set
            {
                SetPropertyValue(value, () => this.Command,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_sCommand = (String)newValue;
                        }
                ));
            }
        }
        private String m_sCommand;
        
        /// <summary>
        /// Command arguments string (pre-environment substitution).
        /// </summary>
        public String Arguments
        {
            get { return m_sArguments; }
            set
            {
                SetPropertyValue(value, () => this.Arguments,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_sArguments = (String)newValue;
                        }
                ));
            }
        }
        private String m_sArguments;

        /// <summary>
        /// Command arguments string (pre-environment substitution).
        /// </summary>
        public String InitialDirectory
        {
            get { return m_sInitialDirectory; }
            set
            {
                SetPropertyValue(value, () => this.InitialDirectory,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_sInitialDirectory = (String)newValue;
                        }
                ));
            }
        }
        private String m_sInitialDirectory;

        /// <summary>
        /// Pipe standard output and standard error to our logging window.
        /// </summary>
        public bool UseOutputWindow
        {
            get { return m_bUseOutputWindow; }
            set
            {
                SetPropertyValue(value, () => this.UseOutputWindow,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_bUseOutputWindow = (bool)newValue;
                        }
                ));
            }
        }
        private bool m_bUseOutputWindow;

        /// <summary>
        /// Pipe standard output and standard error to our logging window.
        /// </summary>
        public bool PromptForArguments
        {
            get { return m_bPromptForArguments; }
            set
            {
                SetPropertyValue(value, () => this.PromptForArguments,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_bPromptForArguments = (bool)newValue;
                        }
                ));
            }
        }
        private bool m_bPromptForArguments;
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor, specifying all member data.
        /// </summary>
        /// <param name="title"></param>
        /// <param name="command"></param>
        /// <param name="args"></param>
        /// <param name="initDir"></param>
        /// <param name="useOutput"></param>
        /// <param name="promptArgs"></param>
        public ExternalTool(String title, String command, String args, String initDir, bool useOutput, bool promptArgs)
        {
            this.Title = (title.Clone() as String);
            this.Command = (command.Clone() as String);
            this.Arguments = (args.Clone() as String);
            this.InitialDirectory = (initDir.Clone() as String);
            this.UseOutputWindow = useOutput;
            this.PromptForArguments = promptArgs;
        }
        
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ExternalTool(String input)
        {
            String[] parts = input.Split(new char[] { '|' });
            Debug.Assert(6 == parts.Length, "Invalid tool configuration.");
            
            this.Title = (parts[0].Clone() as String);
            this.Command = (parts[1].Clone() as String);
            this.Arguments = (parts[2].Clone() as String);
            this.InitialDirectory = (parts[3].Clone() as String);
            this.UseOutputWindow = bool.Parse(parts[4]);
            this.PromptForArguments = bool.Parse(parts[5]);
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Serialise this object to an XmlElement.
        /// </summary>
        /// <param name="doc"></param>
        /// <returns></returns>
        public String Serialise()
        {
            String tool = String.Format("{0}|{1}|{2}|{3}|{4}|{5}",
                this.Title, this.Command, this.Arguments, this.InitialDirectory,
                this.UseOutputWindow, this.PromptForArguments);
            return (tool);
        }

        /// <summary>
        /// Execute this external tool.
        /// </summary>
        /// <returns></returns>
        public bool Execute(Dictionary<String, String> environment)
        {
            return (Execute(environment, null));
        }

        /// <summary>
        /// Execute this external tool, using handler as stdout/stderr handler.
        /// </summary>
        /// <param name="envirnoment"></param>
        /// <param name="handler"></param>
        /// <returns></returns>
        public bool Execute(Dictionary<String, String> envirnoment, DataReceivedEventHandler handler)
        {
            Process p = new Process();
            p.StartInfo.FileName = Environment.ExpandEnvironmentVariables(this.Command);
            p.StartInfo.Arguments = Environment.ExpandEnvironmentVariables(this.Arguments);
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.CreateNoWindow = true;
            p.StartInfo.WorkingDirectory = Environment.ExpandEnvironmentVariables(this.InitialDirectory);
            p.StartInfo.RedirectStandardOutput = (null != handler);
            p.StartInfo.RedirectStandardInput = false;
            p.StartInfo.RedirectStandardError = (null != handler);

            p.OutputDataReceived += handler;
            p.ErrorDataReceived += handler;

            bool started = p.Start();
            if (null != handler)
            {
                p.BeginOutputReadLine();
                p.BeginErrorReadLine();
            }
            return (started);
        }
        #endregion // Controller Methods
    }

} // Workbench.Model namespace
