﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows;

namespace Workbench.AddIn.UI.Menu
{
    /// <summary>
    /// Creates a content control that works like a user control
    /// </summary>
    public class ContextMenuWrapper : ContentControl
    {
        #region Constructor(s)
        /// <summary>
        /// Static constructor.
        /// </summary>
        static ContextMenuWrapper()
        {
            //Make it apply a default style to itself
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ContextMenuWrapper),
                   new FrameworkPropertyMetadata(typeof(ContextMenuWrapper)));
        }
        #endregion // Constructor(s)
    }

} // Workbench.AddIn.UI.Menu namespace