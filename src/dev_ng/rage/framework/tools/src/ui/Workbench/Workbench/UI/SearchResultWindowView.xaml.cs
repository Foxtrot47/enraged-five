﻿using System;
using System.Windows;
using System.Windows.Data;
using System.Windows.Controls;
using Workbench.AddIn;
using Workbench.AddIn.UI.Layout;
using System.Linq;
using RSG.Base.Windows.Controls;
using Workbench.AddIn.Services;

namespace Workbench.UI
{
    /// <summary>
    /// Interaction logic for LogToolWindowView.xaml
    /// </summary>
    public partial class SearchResultWindowView : ToolWindowBase<SearchResultWindowViewModel>
    {
        #region Constants
        public static readonly Guid GUID = new Guid("738E0EF4-1ED7-4FCB-8400-F07D089E1CCF");
        #endregion // Constants

        #region Constructor

        public SearchResultWindowView()
            : base(Workbench.Resources.Strings.ApplicationLog_Name,
                   new SearchResultWindowViewModel())
        {
            this.Name = "Search_Results";
            InitializeComponent();

            this.ID = GUID;
            this.SaveModel = this.ViewModel;
        }

        public SearchResultWindowView(SearchResultWindowViewModel vm)
            : base(Workbench.Resources.Strings.ApplicationLog_Name, vm)
        {
            this.Name = "Search_Results";
            InitializeComponent();

            this.ID = GUID;
            this.SaveModel = this.ViewModel;
        }

        #endregion // Constructor

        private void ItemDoubleClicked(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            TextBox textbox = sender as TextBox;
            if (textbox == null)
            {
                return;
            }

            ISearchResult selectedResult = null;
            int findIndex = 0;
            foreach (ISearchResult result in this.ViewModel.SearchOutput)
            {
                findIndex += result.DisplayText.Length;
                if (findIndex >= textbox.CaretIndex)
                {
                    selectedResult = result;
                    break;
                }

                findIndex += 2;
            }

            if (selectedResult == null || selectedResult.Content == null)
            {
                return;
            }
          
            selectedResult.Content.SelectSearchResult(selectedResult);
        }
    }

} // Workbench.UI namespace
