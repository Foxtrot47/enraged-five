﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Workbench.AddIn.TXDParentiser;
using RSG.Model.GlobalTXD.ViewModel;

namespace Workbench.AddIn.TXDParentiser.UI.Views
{
    ///// <summary>
    ///// Interaction logic for UserDictionaryStatistics.xaml
    ///// </summary>
    //public partial class UserDictionaryStatistics : UserControl, INotifyPropertyChanged
    //{
    //    #region Property change events

    //    /// <summary>
    //    /// Property changed event fired when the any of the properties change
    //    /// </summary>
    //    public event PropertyChangedEventHandler PropertyChanged;

    //    /// <summary>
    //    /// Create the OnPropertyChanged method to raise the event
    //    /// </summary>
    //    /// <param name="name"></param>
    //    protected void OnPropertyChanged(string name)
    //    {
    //        PropertyChangedEventHandler handler = PropertyChanged;
    //        if (handler != null)
    //        {
    //            handler(this, new PropertyChangedEventArgs(name));
    //        }
    //    }

    //    #endregion // Property change events

    //    public ObservableCollection<TexturePreviewPanel> TexturePreviews
    //    {
    //        get { return m_texturePreviews; }
    //        set { m_texturePreviews = value; }
    //    }
    //    private ObservableCollection<TexturePreviewPanel> m_texturePreviews;

    //    public UserTextureDictionaryViewModel Source
    //    {
    //        get { return m_Source; }
    //        set { m_Source = value; OnPropertyChanged("Source"); }
    //    }
    //    private UserTextureDictionaryViewModel m_Source;

    //    public UserDictionaryStatistics()
    //    {
    //        InitializeComponent();
    //        this.TexturePreviews = new ObservableCollection<TexturePreviewPanel>();
    //    }

    //    private void OnDataContextChanged(Object sender, DependencyPropertyChangedEventArgs e)
    //    {
    //        if (this.TexturePreviews == null)
    //            this.TexturePreviews = new ObservableCollection<TexturePreviewPanel>();

    //        this.m_texturePreviews.Clear();

    //        if (!(e.NewValue is UserTextureDictionaryViewModel))
    //        {
    //            this.Source = null;
    //            return;
    //        }

    //        this.Source = e.NewValue as UserTextureDictionaryViewModel;

    //        foreach (IMapParent child in this.Source.Children)
    //        {
    //            if (child is TextureViewModel)
    //            {
    //                TexturePreviewPanel newTexturePanel = new TexturePreviewPanel();
    //                newTexturePanel.DataContext = child;
    //                m_texturePreviews.Add(newTexturePanel);
    //            }
    //        }
    //    }

    //    private void OnUnloaded(object sender, RoutedEventArgs e)
    //    {
    //        this.TexturePreviews.Clear();
    //        this.TexturePreviews = null;
    //    }
    //}
}
