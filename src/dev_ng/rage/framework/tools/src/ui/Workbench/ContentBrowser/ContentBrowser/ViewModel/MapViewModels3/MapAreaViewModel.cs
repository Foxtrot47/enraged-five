﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common.Map;

namespace ContentBrowser.ViewModel.MapViewModels3
{
    /// <summary>
    /// MapArea view model that simply monitors a map area's map node children
    /// </summary>
    public class MapAreaViewModel : ObservableContainerViewModelBase<IMapNode>
    {
        #region Properties
        /// <summary>
        /// The number of viewmodels to create before displaying them on the
        /// screen. Less than or equal to 0 implies no batching
        /// </summary>
        protected override uint AssetCreationBatchSize
        {
            get
            {
                return 5;
            }
        }

        /// <summary>
        /// The number of viewmodels to create before displaying them on the
        /// screen. Less than or equal to 0 implies no batching
        /// </summary>
        protected override uint GridCreationBatchSize
        {
            get
            {
                return 5;
            }
        }
        #endregion // Properties

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public MapAreaViewModel(IMapArea area)
            : base(area, area.ChildNodes)
        {
        }
        #endregion // Constructor(s)
    } // MapAreaViewModel
}
