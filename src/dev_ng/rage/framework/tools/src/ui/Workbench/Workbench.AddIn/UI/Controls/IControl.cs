﻿using System;
using Workbench.AddIn.Services;

namespace Workbench.AddIn.UI.Controls
{

    /// <summary>
    /// 
    /// </summary>
    public interface IControl : IExtension
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        String ToolTip { get; }
        
        /// <summary>
        /// 
        /// </summary>
        bool Visible { get; }
        #endregion // Properties
    }

} // Workbench.AddIn.UI.Controls
