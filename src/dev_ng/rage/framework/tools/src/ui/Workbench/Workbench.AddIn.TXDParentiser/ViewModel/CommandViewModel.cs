﻿using System;
using System.Windows.Media.Imaging;

namespace Workbench.AddIn.TXDParentiser.ViewModel
{
    /// <summary>
    /// Represents an actionable item displayed by a View.
    /// </summary>
    public class CommandViewModel : RSG.Base.Editor.ViewModelBase
    {
        #region Fields

        private BitmapImage m_icon;
        private Object m_toolTip;
        private String m_displayString;

        #endregion // Fields

        #region Properties

        /// <summary>
        /// The main command that the view binds to
        /// </summary>
        public System.Windows.Input.ICommand Command 
        { 
            get; 
            private set;
        }

        /// <summary>
        /// The content that can be displayed in the control that is
        /// bound to this command
        /// </summary>
        public BitmapImage Icon
        {
            get { return m_icon; }
            private set
            {
                this.SetPropertyValue(ref this.m_icon, value, "Icon");
            }
        }

        /// <summary>
        /// The tooltip for the command, this can be changed
        /// at anytime using the set method
        /// </summary>
        public Object ToolTip
        {
            get { return m_toolTip; }
            set
            {
                this.SetPropertyValue(ref this.m_toolTip, value, "ToolTip");
            }
        }

        /// <summary>
        /// The tooltip for the command, this can be changed
        /// at anytime using the set method
        /// </summary>
        public String DisplayString
        {
            get { return m_displayString; }
            set
            {
                this.SetPropertyValue(ref this.m_displayString, value, "DisplayString");
            }
        }

        #endregion // Properties

        #region Constructors

        public CommandViewModel(BitmapImage icon, String toolTip, System.Windows.Input.ICommand command)
        {
            if (command == null)
                throw new ArgumentNullException("command");

            this.Icon = icon;
            this.Command = command;
            this.ToolTip = toolTip;
        }

        public CommandViewModel(String displayString, String toolTip, System.Windows.Input.ICommand command)
        {
            if (command == null)
                throw new ArgumentNullException("command");

            this.DisplayString = displayString;
            this.Command = command;
            this.ToolTip = toolTip;
        }

        public CommandViewModel(String displayString, BitmapImage icon, String toolTip, System.Windows.Input.ICommand command)
        {
            if (command == null)
                throw new ArgumentNullException("command");

            this.Icon = icon;
            this.DisplayString = displayString;
            this.Command = command;
            this.ToolTip = toolTip;
        }

        #endregion // Constructors
    } // CommandViewModel
} // Workbench.AddIn.TXDParentiser.ViewModel
