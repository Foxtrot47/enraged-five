﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Workbench.AddIn;
using Workbench.AddIn.Services;

namespace LevelBrowser.AddIn.MapBrowser
{
    /// <summary>
    /// Settings for Metadata Definitions
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.Settings, typeof(ISettings))]
    public class MapBrowserSettings : Workbench.AddIn.Services.SettingsBase, ISettings
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        [Description("Additional game commandline arguments that are used when kicking off the object position checker.")]
        public String ObjectPositionCheckerArgs
        {
            get { return Properties.Settings.Default.ObjectPositionCheckerArgs; }
            set { Properties.Settings.Default.ObjectPositionCheckerArgs = value; }
        }

        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public MapBrowserSettings()
            : base("Map Browser")
        {
        }
        #endregion // Constructor(s)

        #region Methods
        /// <summary>
        /// Checks its current state for any invalid data.
        /// </summary>
        /// <param name="errors">The service will fill this up with one string per 
        /// error it finds in its data.</param>
        /// <returns>true if everything is fine, false if an error was found</returns>
        public Boolean Validate(List<String> errors)
        {
            return true;
        }

        /// <summary>
        /// Loads settings
        /// </summary>
        public void Load()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public void Apply()
        {
            Properties.Settings.Default.Save();
        }
        #endregion // Methods
    } 
}
