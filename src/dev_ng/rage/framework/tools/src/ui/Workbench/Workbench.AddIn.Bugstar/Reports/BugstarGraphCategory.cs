﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Report;
using Report.AddIn;
using System.ComponentModel.Composition;
using RSG.Interop.Bugstar.Organisation;
using RSG.Base.Logging;
using Workbench.AddIn.Services;
using RSG.Base.Collections;
using RSG.Model.Common;

namespace Workbench.AddIn.Bugstar.Reports
{
    [ExportExtension(ExtensionPoints.Report, typeof(IReport))]
    class BugstarGraphCategory :
        ReportCategory,
        IPartImportsSatisfiedNotification
    {
        #region Constants
        private const String NAME = "Graphs";
        private const String DESC = "List of all graphs that originate from Bugstar.";
        #endregion // Constants
        
        #region Properties and Associated Member Data
        /// <summary>
        /// Category report objects.
        /// This is abstract since we wish your class to implement it with the appropriate attribute ( as defined by your class ) for MEF imports.
        /// </summary>
        public override IEnumerable<IReportItem> Reports
        {
            
            get { return m_reports; }
            protected set
            {
                SetPropertyValue(value, () => Reports,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_reports = (IEnumerable<IReportItem>)newValue;
                        }
                ));
            }
        }
        private IEnumerable<IReportItem> m_reports;

        /// <summary>
        /// MEF import for login service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LoginService, typeof(Workbench.AddIn.Services.ILoginService))]
        public Workbench.AddIn.Services.ILoginService LoginService { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(CompositionPoints.ConfigurationService, typeof(Workbench.AddIn.Services.IConfigurationService))]
        Lazy<Workbench.AddIn.Services.IConfigurationService> Config { get; set; }

        /// <summary>
        /// Bugstar Service MEF import.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.BugstarService, typeof(IBugstarService))]
        protected IBugstarService BugstarService { get; set; }
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public BugstarGraphCategory()
            : base(NAME, DESC)
        {
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// Method that gets called to intialise the category's reports.
        /// </summary>
        public override void InitialiseReports()
        {
            base.InitialiseReports();

            List<IReportItem> reports = new List<IReportItem>();

            try
            {
                foreach (RSG.Interop.Bugstar.Search.Graph graph in BugstarService.GetUserGraphs())
                {
                    reports.Add(new BugstarGraph(graph));
                }
            }
            catch (System.Exception ex)
            {
                Log.Log__Exception(ex, "Unhandled exception while populating the list of bugstar graphs.");
            }

            Reports = reports;
            AssetChildren.AddRange(reports);
        }

        /// <summary>
        /// 
        /// </summary>
        public override void OnImportsSatisfied()
        {
        }
        #endregion // IPartImportsSatisfiedNotification Interface
    } // BugstarGraphCategory
}
