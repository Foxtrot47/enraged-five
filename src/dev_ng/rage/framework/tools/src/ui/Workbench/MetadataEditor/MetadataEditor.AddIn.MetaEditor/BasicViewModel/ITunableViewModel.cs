﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using RSG.Metadata.Data;
using MetadataEditor.AddIn.MetaEditor.ViewModel;

namespace MetadataEditor.AddIn.MetaEditor.BasicViewModel
{
    /// <summary>
    /// Represents a view model of the ITunable interface
    /// </summary>
    public interface ITunableViewModel
    {
        #region Properties

        /// <summary>
        /// Reference to the Tunable model this represents.
        /// </summary>
        ITunable Model { get; }

        Boolean? InheritParent { get; set; }

        String Name { get; set; }

        String FriendlyTypeName { get; set; }

        Boolean IsSelected { get; set; }

        Boolean IsExpanded { get; set; }

        MetaFileViewModel RootViewModel { get; set; }

        ITunableViewModel Parent { get; }

        #endregion // Properties

        #region Members
        ITunableViewModel FindNext(Regex expression);

        ITunableViewModel FindNext(Regex expression, bool checkParent);

        ITunableViewModel FindPrevious(Regex expression);

        List<ITunableViewModel> FindAll(Regex expression);

        Boolean IsMatch(Regex expression);
        #endregion // Members
    }
}
