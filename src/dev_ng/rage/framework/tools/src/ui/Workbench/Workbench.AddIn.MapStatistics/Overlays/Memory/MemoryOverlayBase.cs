﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MapViewport.AddIn;
using RSG.Model.Common.Map;
using RSG.Model.Common.Util;
using RSG.Base.Tasks;
using RSG.Model.Common;
using System.Threading;
using RSG.Base.Windows.Dialogs;
using RSG.Base.Extensions;
using ContentBrowser.AddIn;
using RSG.Base.Attributes;
using RSG.Base.Collections;
using RSG.Platform;
using System.Windows.Media;
using RSG.Base.Drawing;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using Workbench.AddIn.Services.Model;

namespace Workbench.AddIn.MapStatistics.Overlays.Memory
{
    /// <summary>
    /// Type of memory stat to view.
    /// </summary>
    public enum MemoryStatType
    {
        [FriendlyName("Physical Memory")]
        PhysicalSize,

        [FriendlyName("Virtual Memory")]
        VirtualSize
    } // MemoryStat

    /// <summary>
    /// Base class for overlays that visualise memory values per platform/filetype.
    /// </summary>
    public abstract class MemoryOverlayBase : LevelDependentViewportOverlay
    {
        #region MEF Imports
        /// <summary>
        /// Content browser.
        /// </summary>
        [ImportExtension(ContentBrowser.AddIn.CompositionPoints.ContentBrowser, typeof(IContentBrowser))]
        protected IContentBrowser ContentBrowserProxy { get; set; }

        /// <summary>
        /// Platform browser.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.PlatformBrowser, typeof(IPlatformBrowser))]
        protected IPlatformBrowser PlatformBrowser { get; set; }
        #endregion // MEF Imports

        #region Properties
        /// <summary>
        /// Whether we should display total values instead of one of the file type values.
        /// </summary>
        public bool ShowTotals
        {
            get { return m_showTotals; }
            set
            {
                SetPropertyValue(value, () => ShowTotals,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_showTotals = (bool)newValue;
                            OnDisplayOptionChanged();
                        }
                ));
            }
        }
        private bool m_showTotals;

        /// <summary>
        /// List of valid file types to show memory information for.
        /// </summary>
        public ObservableCollection<FileType> ValidFileTypes
        {
            get { return m_validFileTypes; }
            set
            {
                SetPropertyValue(value, () => ValidFileTypes,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_validFileTypes = (ObservableCollection<FileType>)newValue;
                        }
                ));
            }
        }
        private ObservableCollection<FileType> m_validFileTypes;

        /// <summary>
        /// The currently selected file type to show.
        /// </summary>
        public FileType SelectedFileType
        {
            get { return m_selectedFileType; }
            set
            {
                SetPropertyValue(value, () => SelectedFileType,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_selectedFileType = (FileType)newValue;
                            OnDisplayOptionChanged();
                        }
                ));
            }
        }
        private FileType m_selectedFileType;

        /// <summary>
        /// The memory stat to view (virtual/physical).
        /// </summary>
        public MemoryStatType SelectedMemoryStat
        {
            get { return m_selectedMemoryStat; }
            set
            {
                SetPropertyValue(value, () => SelectedMemoryStat,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_selectedMemoryStat = (MemoryStatType)newValue;
                            OnDisplayOptionChanged();
                        }
                ));
            }
        }
        private MemoryStatType m_selectedMemoryStat;

        /// <summary>
        /// Whether to only show the worst n cases (where n is based on the <see cref="WorstCaseCount" /> property).
        /// </summary>
        public bool OnlyWorstCases
        {
            get { return m_onlyWorstCases; }
            set
            {
                SetPropertyValue(value, () => OnlyWorstCases,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_onlyWorstCases = (Boolean)newValue;
                            OnFilterChanged();
                        }
                ));
            }
        }
        private bool m_onlyWorstCases;

        /// <summary>
        /// Number of worst case values to show.
        /// </summary>
        public int WorstCaseCount
        {
            get { return m_worstCaseCount; }
            set
            {
                SetPropertyValue(value, () => WorstCaseCount,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_worstCaseCount = (int)newValue;

                            if (OnlyWorstCases)
                            {
                                OnFilterChanged();
                            }
                        }
                ));
            }
        }
        private int m_worstCaseCount;

        /// <summary>
        /// Maximum geometry count used by the worst case value spinner.
        /// </summary>
        public int MaximumGeometryCount
        {
            get { return m_maximumGeometryCount; }
            set
            {
                SetPropertyValue(value, () => MaximumGeometryCount,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_maximumGeometryCount = (int)newValue;
                        }
                ));
            }
        }
        private int m_maximumGeometryCount;

        /// <summary>
        /// Show only values that are greater than the <see cref="GreaterThanLimit" /> property.
        /// </summary>
        public bool OnlyGreaterThan
        {
            get { return m_onlyGreaterThan; }
            set
            {
                SetPropertyValue(value, () => OnlyGreaterThan,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_onlyGreaterThan = (Boolean)newValue;
                            OnFilterChanged();
                        }
                ));
            }
        }
        private bool m_onlyGreaterThan;

        /// <summary>
        /// Limit used when the <see cref="OnlyGreaterThan"/> flag is set.
        /// </summary>
        public float GreaterThanLimit
        {
            get { return m_greaterThanLimit; }
            set
            {
                SetPropertyValue(value, () => GreaterThanLimit,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_greaterThanLimit = (float)newValue;

                            if (OnlyGreaterThan)
                            {
                                OnFilterChanged();
                            }
                        }
                ));
            }
        }
        private float m_greaterThanLimit;

        /// <summary>
        /// Show only values that are greater than the average.
        /// </summary>
        public bool OnlyGreaterThanAverage
        {
            get { return m_onlyGreaterThanAverage; }
            set
            {
                SetPropertyValue(value, () => OnlyGreaterThanAverage,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_onlyGreaterThanAverage = (Boolean)newValue;
                            OnFilterChanged();
                        }
                ));
            }
        }
        private bool m_onlyGreaterThanAverage;

        /// <summary>
        /// Average value.
        /// </summary>
        public double Average
        {
            get { return m_average; }
            set
            {
                SetPropertyValue(value, () => Average,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_average = (double)newValue;
                        }
                ));
            }
        }
        private double m_average;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="desc"></param>
        protected MemoryOverlayBase(String name, String desc)
            : base(name, desc)
        {
            m_validFileTypes = new ObservableCollection<FileType>();
            m_validFileTypes.BeginUpdate();
            m_validFileTypes.Add(FileType.Drawable);
            m_validFileTypes.Add(FileType.DrawableDictionary);
            m_validFileTypes.Add(FileType.Fragment);
            m_validFileTypes.Add(FileType.TextureDictionary);
            m_validFileTypes.Add(FileType.BoundsFile);
            m_validFileTypes.Add(FileType.BoundsDictionary);
            m_validFileTypes.Add(FileType.ClipDictionary);
            m_validFileTypes.EndUpdate();

            m_selectedFileType = FileType.Drawable;
            m_selectedMemoryStat = MemoryStatType.PhysicalSize;
        }
        #endregion // Constructor(s)

        #region ViewportOverlay Overrides
        /// <summary>
        /// Gets called when the user wants to view this overlay
        /// </summary>
        public override void Activated()
        {
            base.Activated();
            ContentBrowserProxy.GridSelectionChanged += GridSelectionChanged;
            PlatformBrowser.PlatformChanged += OnPlatformChanged;
            SelectionChanged(ContentBrowserProxy.SelectedGridItems);
        }

        /// <summary>
        /// Gets called when the user turns off this overlay
        /// </summary>
        public override void Deactivated()
        {
            ContentBrowserProxy.GridSelectionChanged -= GridSelectionChanged;
            PlatformBrowser.PlatformChanged -= OnPlatformChanged;
            base.Deactivated();
        }

        /// <summary>
        /// Gets the control that should be shown in the overlay details panel
        /// </summary>
        public override System.Windows.Controls.Control GetOverlayControl()
        {
            return new MemoryOverlayView();
        }
        #endregion // ViewportOverlay Overrides

        #region Event Handlers
        /// <summary>
        /// Called whenever the user selects a different platform via the platform browser.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnPlatformChanged(object sender, PlatformChangedEventArgs e)
        {
            RefreshGeometry(GetSectionSelection(ContentBrowserProxy.SelectedGridItems));
        }

        /// <summary>
        /// Called when the user changes either the file type or memory stat type to display.
        /// </summary>
        protected void OnDisplayOptionChanged()
        {
            RefreshGeometry(GetSectionSelection(ContentBrowserProxy.SelectedGridItems));
        }

        /// <summary>
        /// Gets called when any stat render option changes and we need to redraw the geometry.
        /// </summary>
        protected void OnFilterChanged()
        {
            RefreshGeometry(GetSectionSelection(ContentBrowserProxy.SelectedGridItems));
        }

        /// <summary>
        /// Get called when the items selected in the content browser change.
        /// </summary>
        void GridSelectionChanged(Object sender, ContentBrowser.AddIn.GridSelectionChangedEventArgs args)
        {
            SelectionChanged(args.NewItem);
        }
        #endregion // Event Handlers

        #region Virtual/Abstract Methods
        /// <summary>
        /// Retrieves the value to display for a particular section.
        /// </summary>
        /// <param name="section"></param>
        /// <param name="platform"></param>
        /// <param name="stat"></param>
        /// <param name="fileType"></param>
        /// <returns></returns>
        protected abstract double GetValueForSection(IMapSection section, RSG.Platform.Platform platform, MemoryStatType stat, RSG.Platform.FileType fileType);

        /// <summary>
        /// Gets the total value for a particular section.
        /// </summary>
        /// <param name="section"></param>
        /// <param name="platform"></param>
        /// <param name="stat"></param>
        /// <returns></returns>
        protected abstract double GetTotalValueForSection(IMapSection section, RSG.Platform.Platform platform, MemoryStatType stat);

        /// <summary>
        /// Retrieves the text to add as a part of the viewport geometry.
        /// </summary>
        /// <param name="section"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        protected virtual string GetUserTextForSection(IMapSection section, double value)
        {
            return value.ToString();
        }
        #endregion // Virtual/Abstract Methods

        #region Private Methods
        /// <summary>
        /// Gets called when the items selected in the content browser change and when
        /// the overlay is first shown.
        /// </summary>
        private void SelectionChanged(IEnumerable<IAsset> selectedAssets)
        {
            if (ContentBrowserProxy.SelectedGridItems != null)
            {
                ISet<IMapSection> sections = GetSectionSelection(ContentBrowserProxy.SelectedGridItems);

                CancellationTokenSource cts = new CancellationTokenSource();
                SectionTaskContext context = new SectionTaskContext(cts.Token, sections);

                ActionTask task = new ActionTask("Load Data", (ctx, progress) => EnsureDataLoaded((SectionTaskContext)ctx, progress));

                TaskExecutionDialog dialog = new TaskExecutionDialog();
                TaskExecutionViewModel vm = new TaskExecutionViewModel("Generating Overlay", task, cts, context);
                vm.AutoCloseOnCompletion = true;
                dialog.DataContext = vm;
                Nullable<bool> selectionResult = dialog.ShowDialog();
                if (false == selectionResult)
                {
                    return;
                }

                // Force an update of the geometry that is shown on the overlay.
                RefreshGeometry(sections);
            }
        }

        /// <summary>
        /// Returns the collection of sections to use for this overlay
        /// </summary>
        /// <param name="selectedAssets"></param>
        /// <returns></returns>
        private ISet<IMapSection> GetSectionSelection(IEnumerable<IAsset> selectedAssets)
        {
            // Gather the sections that the user has selected by drilling down the hierarchy
            ISet<IMapSection> unfilteredSections = new HashSet<IMapSection>();

            foreach (IAsset asset in selectedAssets)
            {
                if (asset is ILevel)
                {
                    ILevel level = (ILevel)asset;
                    unfilteredSections.AddRange(level.MapHierarchy.AllSections);
                }
                else if (asset is IMapArea)
                {
                    unfilteredSections.AddRange((asset as IMapArea).AllDescendentSections);
                }
                else if (asset is IMapSection)
                {
                    unfilteredSections.Add(asset as IMapSection);
                }
            }

            // Filter out sections that are of no use to us (ones that don't have vector maps).
            ISet<IMapSection> filteredSections = new HashSet<IMapSection>();
            filteredSections.AddRange(unfilteredSections.Where(item => item.VectorMapPoints != null && item.VectorMapPoints.Count > 0));
            return filteredSections;
        }

        /// <summary>
        /// Makes sure that the data required by the overlay is loaded.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="progress"></param>
        private void EnsureDataLoaded(SectionTaskContext context, IProgress<TaskProgress> progress)
        {
            float increment = 1.0f / context.Sections.Count();

            // Load all the map section data
            foreach (IMapSection section in context.Sections)
            {
                context.Token.ThrowIfCancellationRequested();

                // Update the progress information
                string message = String.Format("Loading {0}", section.Name);
                progress.Report(new TaskProgress(increment, true, message));

                section.LoadStats(new StreamableStat[] { StreamableMapSectionStat.PlatformStats });
            }
        }

        /// <summary>
        /// Refreshes the geometry that is shown.
        /// </summary>
        /// <param name="sections"></param>
        private void RefreshGeometry(ISet<IMapSection> sections)
        {
            Geometry.BeginUpdate();
            Geometry.Clear();

            // No need to do anything if no sections were selected.
            if (sections.Count > 0)
            {
                // Gather the min/max stats values
                IDictionary<IMapSection, double> sectionValues = new Dictionary<IMapSection, double>();
                foreach (IMapSection section in sections)
                {
                    double value = 0.0;
                    if (ShowTotals)
                    {
                        value = GetTotalValueForSection(section, PlatformBrowser.SelectedPlatform, SelectedMemoryStat);
                    }
                    else
                    {
                        value = GetValueForSection(section, PlatformBrowser.SelectedPlatform, SelectedMemoryStat, SelectedFileType);
                    }
                    sectionValues.Add(section, value);
                }

                // Update the stats by which we can filter
                double minValue = sectionValues.Values.Min();
                double maxValue = sectionValues.Values.Max();

                double total = sectionValues.Values.Sum();
                Average = total / sections.Count;
                MaximumGeometryCount = sections.Count;

                // Get a sorted list of key value pairs (sorted by descending value).
                List<KeyValuePair<IMapSection, double>> sortedValues = sectionValues.ToList();
                sortedValues.Sort((firstPair, nextPair) => nextPair.Value.CompareTo(firstPair.Value));

                // Render each section with a colour based on how expensive it is in relation to the other sections.
                double statRange = maxValue - minValue;
                int sectionsRendered = 0;

                foreach (KeyValuePair<IMapSection, double> pair in sortedValues)
                {
                    IMapSection section = pair.Key;
                    double value = pair.Value;

                    // Should we render this section?
                    bool renderSection = true;
                    if ((OnlyGreaterThanAverage && value <= Average) ||
                        (OnlyGreaterThan && value <= GreaterThanLimit))
                    {
                        renderSection = false;
                    }

                    if (renderSection)
                    {
                        // Location within the stat range.
                        double percentage = 1.0;
                        if (value != 0.0)
                        {
                            percentage = (1.0 - (value - minValue) / statRange);
                        }

                        // Uses green -> red range (dividing by 3 below is same as *0.33333 which equates to red in hues between 0 and 1)
                        byte h = (byte)((percentage / 3.0) * 255);
                        byte s = (byte)(0.9 * 255);
                        byte v = (byte)(0.9 * 255);
                        System.Drawing.Color drawingColor = ColourSpaceConv.HSVtoColor(new HSV(h, s, v));
                        Color fillColour = Color.FromRgb(drawingColor.R, drawingColor.G, drawingColor.B);

                        // Create the viewport geometry.
                        Viewport2DShape newGeometry = new Viewport2DShape(etCoordSpace.World, String.Empty, section.VectorMapPoints.ToArray(), Colors.Black, 1, fillColour);
                        newGeometry.PickData = section;
                        newGeometry.UserText = GetUserTextForSection(section, value);
                        Geometry.Add(newGeometry);

                        // Keep track of how many we've rendered.
                        sectionsRendered++;
                    }

                    // Check if we've reached the limit of how many we should be rendering.
                    if (OnlyWorstCases && sectionsRendered >= WorstCaseCount)
                    {
                        break;
                    }
                }
            }
            else
            {
                Average = 0.0;
                MaximumGeometryCount = 1;
            }

            Geometry.EndUpdate();
        }
        #endregion // Private Methods
    } // MemoryValuesOverlay
}
