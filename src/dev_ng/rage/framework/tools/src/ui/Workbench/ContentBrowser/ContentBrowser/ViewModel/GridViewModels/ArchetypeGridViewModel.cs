﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common.Map;
using RSG.Model.Map3;
using System.IO;

namespace ContentBrowser.ViewModel.GridViewModels
{
    /// <summary>
    /// 
    /// </summary>
    public class ArchetypeGridViewModel : GridViewModelBase
    {
        #region Properties
        /// <summary>
        /// A IAsset reference to the model the view model
        /// is representing
        /// </summary>
        public new IMapArchetype Model
        {
            get { return m_archetype; }
            set
            {
                SetPropertyValue(value, () => Model,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_archetype = (IMapArchetype)newValue;
                        }
                ));
            }
        }
        private IMapArchetype m_archetype;

        /// <summary>
        /// The main name property which by default
        /// will be shown as the header for a browser item
        /// </summary>
        public override String Name
        {
            get { return this.Model.Name; }
        }

        /// <summary>
        /// The main name filename which by default
        /// will be shown as icon for the asset
        /// </summary>
        public String Filename
        {
            get 
            {
                if (this.m_filename == null)
                {
                    if (!String.IsNullOrEmpty(AssetsDir))
                    {
                        String dir = System.IO.Path.Combine(AssetsDir, "maps", "propRenders");
                        if (System.IO.Directory.Exists(dir))
                        {
                            String name = this.Name;
                            if (this.Name.EndsWith("_frag_"))
                            {
                                name = this.Name.Replace("_frag_", "");
                            }
                            else if (this.Name.EndsWith("_anim"))
                            {
                                name = this.Name.Replace("_anim", "");
                            }
                            else if (this.Name.EndsWith("_milo_"))
                            {
                                name = this.Name.Replace("_milo_", "");
                            }

                            string filename = Path.Combine(dir, String.Format("{0}.jpg", name));
                            if (File.Exists(filename))
                            {
                                this.m_filename = filename;
                            }
                        }
                        else
                        {
                            this.m_filename = null;
                        }
                    }
                    else
                    {
                        this.m_filename = null;
                    }
                }
                return this.m_filename;
            }
        }
        private String m_filename;
        
        /// <summary>
        /// The main name filename which by default
        /// will be shown as icon for the asset
        /// </summary>
        public String JaxxFilename
        {
            get 
            {
                if (this.m_jaxxFilename == null)
                {
                    if (!String.IsNullOrEmpty(AssetsDir))
                    {
                        String filename = System.IO.Path.Combine(AssetsDir, "maps", "propRenders");
                        if (System.IO.Directory.Exists(filename))
                        {
                            String name = this.Name;
                            if (this.Name.EndsWith("_frag_"))
                                name = this.Name.Replace("_frag_", "");
                            else if (this.Name.EndsWith("_anim"))
                                name = this.Name.Replace("_anim", "");
                            else if (this.Name.EndsWith("_milo_"))
                                name = this.Name.Replace("_milo_", "");

                            String[] renderFiles = System.IO.Directory.GetFiles(filename, String.Format("{0}_jaxx.jpg", name), System.IO.SearchOption.AllDirectories);
                            if (renderFiles.Length > 0)
                            {
                                this.m_jaxxFilename = renderFiles[0];
                            }
                        }
                        else
                        {
                            this.m_jaxxFilename = String.Empty;
                        }
                    }
                    else
                    {
                        this.m_jaxxFilename = String.Empty;
                    }
                }
                return this.m_jaxxFilename;
            }
        }
        private String m_jaxxFilename;

        /// <summary>
        /// 
        /// </summary>
        private String AssetsDir
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public ArchetypeGridViewModel(IMapArchetype archetype, string assetsDir)
            : base(archetype)
        {
            this.m_archetype = archetype;
            this.m_filename = null;
            AssetsDir = assetsDir;
        }
        #endregion // Constructor
    } // ArchetypeGridViewModel
}
