﻿using System;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;

namespace Workbench.AddIn.Automation.UI
{

    /// <summary>
    /// Base automation tab view model; for tabs in the Automation Tool Window.
    /// </summary>
    internal abstract class AutomationTabViewModelBase : ViewModelBase
    {
        #region Properties
        /// <summary>
        /// Tab header.
        /// </summary>
        public String Header
        {
            get { return m_sHeader; }
            private set
            {
                SetPropertyValue(value, () => this.Header,
                    new PropertySetDelegate(delegate(Object newValue) { m_sHeader = (String)newValue; }));
            }
        }
        private String m_sHeader;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="header"></param>
        public AutomationTabViewModelBase(String header)
        {
            this.Header = header;
        }
        #endregion // Constructor(s)

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterString"></param>
        public virtual void FilterUsername(string filterString)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterString"></param>
        public virtual void FilterUsername(bool justCompleted)
        {
        }
        #endregion
    }

} // Workbench.AddIn.Automation.UI namespace
