﻿namespace LevelBrowser.AddIn.MapBrowser.BatchExporter
{
    /// <summary>
    /// Extension points for MEF.
    /// </summary>
    public static class ExtensionPoints
    {
        /// <summary>
        /// Save command. 
        /// </summary>
        public const string SaveCommand = "00457E3C-04BA-4894-B21D-762876AA8D4F";

        /// <summary>
        /// Load command.
        /// </summary>
        public const string LoadCommand = "02BD5D77-95AD-4ea6-8517-40ABA36AF539";

        /// <summary>
        /// Clear command.
        /// </summary>
        public const string ClearCommand = "952B9C3B-7626-4b36-8B15-CD00232A4350";

        /// <summary>
        /// Export command.
        /// </summary>
        public const string ExportCommand = "39F71342-4AE5-4061-BD04-2E16E3BDF3E3";

        /// <summary>
        /// Remove selection command.
        /// </summary>
        public const string RemoveSelectionCommand = "ABE6550D-FA8F-4fd1-B709-A90B2ED238D2";

        /// <summary>
        /// Export settings model.
        /// </summary>
        public const string ExportSettingsModel = "6D367CFF-AE76-4f11-949D-DE90DAEDEE03";

        /// <summary>
        /// Export view model.
        /// </summary>
        public const string BatchExportViewModel = "08517BD3-9F85-4782-ACF4-CB823782B1C3";
    }
}
