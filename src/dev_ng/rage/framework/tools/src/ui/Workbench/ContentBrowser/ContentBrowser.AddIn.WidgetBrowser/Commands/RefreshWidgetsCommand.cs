﻿using System;
using System.Diagnostics;
using System.Windows.Input;
using RSG.Base.Editor;
using System.Windows.Media.Imaging;
using RSG.Base.Logging;
using System.Collections.ObjectModel;
using ragCore;
using System.Threading;
using System.ComponentModel;
using RSG.Base.Network;
using ragWidgets;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using RSG.Model.Common;
using System.Windows.Threading;
using Workbench.AddIn.Services;
using Workbench.AddIn;

namespace ContentBrowser.AddIn.WidgetBrowser.Commands
{
    /// <summary>
    ///  A command whose sole purpose is to relay its functionality 
    ///  to other objects by invoking delegates
    /// </summary>
    public class RefreshWidgetsCommand
    {
        public class Result
        {
        }

        public class CommandContext
        {
            public delegate void PipeReadFunction();
            public delegate void CompleteCallback( Result result );
            public delegate void OnStartedCallback();

            public cRemoteConsole Console { get; set; }
            public ProxyConnection ProxyConnection { get; set; }
            public BankPipe PipeBank { get; set; }
            public INamedPipe PipeOutput { get; set; }
            public INamedPipe PipeEvents { get; set; }
            public PipeReadFunction ReadBankPipe { get; set; }
            public PipeReadFunction ReadOutputPipe { get; set; }
            public ObservableCollection<IAsset> AssetHierarchy { get; set; }

            public bool ProxyInitialised { get; set; }
            public CompleteCallback OnSuccess;
            public CompleteCallback OnFail;
            public OnStartedCallback OnStarted;

            public ObservableCollection<WidgetBank> Banks { get; set; }
        }

        #region Members

        public bool m_Working { get; set; }

        private CommandContext m_Context;
        private Result m_Result;

        private PipeID m_RagPipeName;
        private INamedPipe m_RagPipe = PipeID.CreateNamedPipe();
        private Thread m_RagePipeThread;
        private bool m_StoppingPipe;

        #endregion // Members


        #region Properties

        public ContentBrowserCommand Command { get; set; }
        public CommandContext Context { get { return m_Context; } }
        #endregion // Properties


        #region Constructors

        public RefreshWidgetsCommand( CommandContext context )
        {
            m_Context = context;
            Command = CreateRefreshWidgetsCommand();
        }

        #endregion // Constructors

        #region Command functions

        private ContentBrowser.AddIn.ContentBrowserCommand CreateRefreshWidgetsCommand()
        {
            Action<Object> execute = param => ExecuteRefreshWidgets();
            Predicate<Object> canExecute = param => CanExecuteRefreshWidgets();
            String toolTip = "Refresh Widget Tree";
            BitmapImage image = null;
            try
            {
                Uri uriSource = new Uri( "pack://application:,,,/LevelBrowser;component/Resources/Images/refresh.png" );
                image = new BitmapImage( uriSource );
            }
            catch
            {
            }

            return new ContentBrowser.AddIn.ContentBrowserCommand( execute, canExecute, toolTip, image );
        }

        private Boolean CanExecuteRefreshWidgets()
        {
            return !m_Working;
        }

        /// <summary>
        /// 
        /// </summary>
        private void ExecuteRefreshWidgets()
        {
            Log.Log__Message( "Refreshing widget browser..." );

            m_Result = new Result();
            m_Working = true;
            m_Context.Banks.Clear();
            m_Context.OnStarted();


            CloseConnections();

            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += new DoWorkEventHandler( SetupConnectionToProxy );
            worker.RunWorkerCompleted += delegate( object s, RunWorkerCompletedEventArgs args )
            {
                m_Working = false;
                Log.Log__Message( "Finished refreshing widget browser." );
            };

            worker.RunWorkerAsync();
        }
        #endregion //  Command functions







        private void CloseConnections()
        {
            Log.Log__Message( "Widget editor closing connections" );
            m_StoppingPipe = true;


            if ( m_RagPipe != null )
            {
                m_RagPipe.CancelCreate();
            }

            if ( m_Context.ProxyConnection != null )
            {
                m_Context.ProxyConnection.DisconnectFromProxy();

                // Allow proxy to shut it down peacefully before closing pipes
                if ( m_Context.Console.IsConnected() )
                {
                    string command = "getProxyStatus " + m_Context.ProxyConnection.ProxyUID;
                    for ( int i = 0; i < 20; i++ )
                    {
                        Thread.Sleep( 200 );
                        string proxyStatus = m_Context.Console.SendCommand( command );
                        if ( proxyStatus.Contains( "Error" ) )
                        {
                            break;
                        }
                    }
                }
                else
                {
                    Thread.Sleep( 1000 );
                }
            }

            m_Context.PipeBank.Close();
            m_Context.PipeOutput.Close();
            m_Context.PipeEvents.Close();

            m_StoppingPipe = false;
        }





        void SetupConnectionToProxy( object sender, DoWorkEventArgs e )
        {
            if ( !m_Context.Console.IsConnected() )
            {
                Log.Log__Error( "Console isn't connected." );
                return;
            }

            if ( m_Context.Console.SendCommand( "gameconnected" ) != "true" )
            {
                Log.Log__Error( "Proxy isn't connected to the game yet" );
                return;
            }

            ProxyLock proxyLock = null;
            try
            {
                proxyLock = m_Context.ProxyConnection.ConnectToProxy( 60000 );
                Log.Log__Message( "Connected to proxy" );
            }
            catch ( Exception ex )
            {
                Log.Log__Exception( ex, "Exception received when trying to connect to proxy." );
                m_Context.ProxyConnection.DisconnectFromProxy();
                return;
            }

            m_RagPipeName = new PipeID( "pipe_rag", IPAddress.Loopback, proxyLock.Port );
            proxyLock.Release();

            bool success = true;

            while ( !m_StoppingPipe )
            {
                bool result = m_RagPipe.Create( m_RagPipeName, true );
                if ( !result )
                {
                    Log.Log__Error( "Could not connect to pipe '{0}'!", m_RagPipeName.ToString() );
                    success = false;
                    break;
                }
                else if ( !m_StoppingPipe )
                {
                    Log.Log__Message( "Connected to proxy (again)" );
                    ProxyHandshake.HandshakeResult handshake = PerformProxyHandshake( m_RagPipe );
                    if ( handshake != null )
                    {
                        Log.Log__Message( "Proxy handshake completed successfully" );

                        // Handshake is done, but wait until Proxy is completely initialised
                        // before creating assets out of banks/widgets.
                        string command = "getProxyStatus " + m_Context.ProxyConnection.ProxyUID;
                        while ( !m_StoppingPipe )
                        {
                            string proxyStatus = m_Context.Console.SendCommand( command );
                            if ( proxyStatus == "ReadyIdle" || proxyStatus == "ReadyWorking" )
                            {
                                Log.Log__Message( "Proxy initialised, setting up widget tree..." );
                                //Status = "";
                                /*
                                lock ( m_Context.Banks )
                                {
                                    foreach ( var bank in m_Context.Banks )
                                    {
                                        Dispatcher.CurrentDispatcher.Invoke( (Action)delegate()
                                        {
                                            var bankAsset = new WidgetAsset( bank );
                                            m_Context.AssetHierarchy.Add( bankAsset );
                                            SetupAssetHierarchy( bankAsset );
                                        } );
                                    }

                                    m_Context.ProxyInitialised = true;
                                }
                                */
                                break;
                            }
                            else if ( proxyStatus.Contains( "Error" ) )
                            {
                                Log.Log__Error( "Proxy status returned error: {0}", proxyStatus );
                                success = false;
                                break;
                            }
                            else if ( proxyStatus != "Initialising" )
                            {
                                Log.Log__Error( "Proxy status returned unexpected value: {0}", proxyStatus );
                                success = false;
                                break;
                            }


                            Thread.Sleep( 100 );
                        }

                        break;
                    }

                    break;
                }
            }

            m_RagPipe.Close();

            if ( !success || m_StoppingPipe )
            {
                Log.Log__Message( "An error occured while connecting to the Proxy, resetting." );
                m_Context.OnFail( m_Result );
            }
            else
            {
                m_Context.OnSuccess( m_Result );
            }
        }





        public ProxyHandshake.HandshakeResult PerformProxyHandshake( INamedPipe pipe )
        {
            IPAddress address = IPAddress.Parse( pipe.Socket.LocalEndPoint.ToString().Split( ':' )[0] );

            bool timedOut;
            ProxyHandshake.HandshakeResult result = new ProxyHandshake.HandshakeResult();


            int basePort = 60000;
            result.ReservedSockets = new List<TcpListener>();
            result.PortNumber = RagUtils.FindAvailableSockets( basePort, 10, result.ReservedSockets );

            result.PipeNameBank = RagUtils.GetRagPipeSocket( result.PortNumber, (int)PipeID.EnumSocketOffset.SOCKET_OFFSET_BANK, result.ReservedSockets );
            result.PipeNameOutput = RagUtils.GetRagPipeSocket( result.PortNumber, (int)PipeID.EnumSocketOffset.SOCKET_OFFSET_OUTPUT, result.ReservedSockets );
            result.PipeNameEvents = RagUtils.GetRagPipeSocket( result.PortNumber, (int)PipeID.EnumSocketOffset.SOCKET_OFFSET_EVENTS, result.ReservedSockets );

            
            Log.Log__Message( "Connecting bank and output pipes..." );
            Log.Log__Message( "Bank: {0}", result.PipeNameBank.ToString() );
            Log.Log__Message( "Output: {0}", result.PipeNameOutput.ToString() );

            RagUtils.ConnectToPipe( result.PipeNameBank, m_Context.PipeBank );
            RagUtils.ConnectToPipe( result.PipeNameOutput, m_Context.PipeOutput );
            RagUtils.ConnectToPipe( result.PipeNameEvents, m_Context.PipeEvents );
            /*
            RagUtils.ConnectToPipe( result.PipeNameBank, m_Context.PipeBank, delegate( PipeID pipeId )
            {
                lock ( result )
                {
                    Log.Log__Message( result.PipeNameBank.Name + "Connected!" );
                    ++numConnected;
                }
            },
                 delegate( PipeID pipeId )
                 {
                     lock ( result )
                     {
                         numConnected = -100;
                     }
                 } );


            RagUtils.ConnectToPipe( result.PipeNameOutput, m_Context.PipeOutput, delegate( PipeID pipeId )
            {
                lock ( result )
                {
                    Log.Log__Message( result.PipeNameOutput.Name + "Connected!" );
                    ++numConnected;
                }
            },
                 delegate( PipeID pipeId )
                 {
                     lock ( result )
                     {
                         numConnected = -100;
                     }
                 } );
            */
            Log.Log__Message( "Waiting for pipes to start..." );
            for ( int i = 0; i < 1000; ++i )
            {
                Thread.Sleep( 10 );
                if ( ((NamedPipeSocket)m_Context.PipeBank).IsListeningForConnections() &&
                     ((NamedPipeSocket)m_Context.PipeOutput).IsListeningForConnections() )
                {
                    Log.Log__Message( "Pipes listening" );
                    break;
                }
            }


            Log.Log__Message( "Performing handshake..." );
            for ( int i = 0; i < 1; i++ )
            {
                if ( !ProxyHandshake.DoHandshake( pipe, out timedOut, ref result ) )
                {
                    Log.Log__Error( "Handshake with Proxy failed" );
                    if ( timedOut )
                    {
                        Log.Log__Error( "Rag timed out waiting for the application startup data.", "Connection Timed Out" );
                    }
                    return null;
                }

            }


            // send the base socket and version numbers
            Log.Log__Message( "Sending port number..." );
            RemotePacket p = new RemotePacket( pipe );
            p.BeginWrite();
            p.Write_s32( result.PortNumber );
            p.Write_float( ProxyHandshake.RAG_VERSION );
            p.Send();


            // Wait for Rag Proxy to connect to the pipes
            while ( true )
            {
                if (m_Context.PipeBank.IsConnected() &&
                    m_Context.PipeOutput.IsConnected() )
                {
                    Log.Log__Message( "Pipes connected" );
                    break;
                }

                if ( !((NamedPipeSocket)m_Context.PipeBank).IsListeningForConnections() ||
                     !((NamedPipeSocket)m_Context.PipeOutput).IsListeningForConnections() )
                {
                    Log.Log__Message( "Pipes stopped" );
                    m_Context.PipeBank.CancelCreate();
                    m_Context.PipeOutput.CancelCreate();
                    return null;
                }
            }


            // Send input window handle
            BankRemotePacket packet = new BankRemotePacket( m_Context.PipeBank );
            packet.Begin( BankRemotePacket.EnumPacketType.USER_3, Widget.ComputeGUID( 'b', 'm', 'g', 'r' ) );
            packet.Write_u32( 12345 ); // todo send an actual window's handle
            packet.Send();

            Log.Log__Message( "Handshake on pipe {0} done.", pipe.Name );
            Log.Log__Message( "Establishing connection with client: {0} {1}", result.ExeName, result.Args );

            return result;
        }





        private void SetupAssetHierarchy( WidgetAsset widgetAsset )
        {
            if ( widgetAsset.Model is WidgetGroupBase )
            {
                var groupWidget = widgetAsset.Model as WidgetGroupBase;
                foreach ( var item in groupWidget.WidgetList )
                {
                    WidgetAsset childAsset = new WidgetAsset( item );
                    widgetAsset.AssetChildren.Add( childAsset );
                    SetupAssetHierarchy( childAsset );
                }
            }
        }





    } // BrowserCommand
} // ContentBrowser.Model
