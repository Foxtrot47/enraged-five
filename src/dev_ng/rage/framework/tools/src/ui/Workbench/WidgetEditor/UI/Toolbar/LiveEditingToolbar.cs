﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn;
using Workbench.AddIn.Commands;
using System.ComponentModel.Composition;
using Workbench.AddIn.Services;
using WidgetEditor.UI.Toolbar;

namespace WdigetEditor.UI.Toolbar
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.ToolbarTray, typeof(IToolbar))]
    public class LiveEditingToolbar : ToolBarBase, IPartImportsSatisfiedNotification
    {
        #region MEF Imports
        /// <summary>
        /// Toolbar subitems.
        /// </summary>
        [ImportManyExtension(WidgetEditor.AddIn.ExtensionPoints.LiveEditingToolbar, typeof(IWorkbenchCommand))]
        private IEnumerable<IWorkbenchCommand> ImportedItems { get; set; }
        #endregion // MEF Imports
        
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public LiveEditingToolbar()
        {
        }
        #endregion // Constructor(s)

        #region IPartImportsSatisfiedNotification Methods
        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {
            List<IWorkbenchCommand> allItems = new List<IWorkbenchCommand>();
            allItems.AddRange(this.SortToolBarItems(ImportedItems));
            this.Items = allItems;
        }
        #endregion // IPartImportsSatisfiedNotification Methods
    } // MetadataEditorToolbar


    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(WidgetEditor.AddIn.ExtensionPoints.LiveEditingToolbar, typeof(IWorkbenchCommand))]
    class LiveEditingToolbarLabel : WorkbenchCommandLabel
    {
        #region Constants
        /// <summary>
        /// Unique GUID for this label
        /// </summary>
        public static readonly Guid GUID = new Guid("AF109C09-548C-4E62-9995-811642BBEB20");
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public LiveEditingToolbarLabel()
            : base()
        {
            this.ID = GUID;
            this.Header = "Live Editing:";
        }
        #endregion // Constructor(s)

        #region Public Members
        /// <summary>
        /// 
        /// </summary>
        /// <param name="relative"></param>
        /// <param name="direction"></param>
        public void SetPlacement(Guid relative, Direction direction)
        {
            this.RelativeID = relative;
            this.Direction = direction;
        }
        #endregion // Public Members
    } // ContentBrowserToolbarDataSourceLabel


    /// <summary>
    /// Separator for after the game connections drop-down
    /// </summary>
    [ExportExtension(WidgetEditor.AddIn.ExtensionPoints.LiveEditingToolbar, typeof(IWorkbenchCommand))]
    class LiveEditingToolbarSeperator : WorkbenchCommandSeparator
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public LiveEditingToolbarSeperator()
        {
            this.ID = new Guid(WidgetEditor.AddIn.ExtensionPoints.LiveEditingToolbarSeperator);
            this.IsSeparator = true;
            this.RelativeToolBarID = RefreshConnectionsToolbarButton.GUID;
            this.Direction = Direction.After;
        }
        #endregion // Constructor(s)
    } // StandardToolbarSep2
}
