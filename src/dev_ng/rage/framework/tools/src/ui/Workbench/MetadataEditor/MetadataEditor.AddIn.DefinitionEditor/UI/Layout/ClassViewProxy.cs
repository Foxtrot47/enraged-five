﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn;
using Workbench.AddIn.Services;
using Workbench.AddIn.UI.Layout;

namespace MetadataEditor.AddIn.DefinitionEditor.UI.Layout
{
    [ExportExtension(Workbench.AddIn.ExtensionPoints.ToolWindowProxy, typeof(IToolWindowProxy))]
    public class ClassViewProxy : IToolWindowProxy
    {
        #region MEF Imports

        [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager,
            typeof(ILayoutManager))]
        private Lazy<ILayoutManager> LayoutManager { get; set; }

        [ImportExtension(MetadataEditor.AddIn.CompositionPoints.StructureDictionary,
            typeof(IStructureDictionary))]
        private Lazy<IStructureDictionary> StructureDictionary { get; set; }

        [ImportExtension(Workbench.AddIn.CompositionPoints.PropertyInspectorService,
            typeof(IPropertyInspector))]
        private Lazy<IPropertyInspector> PropertyInspector { get; set; }

        #endregion // MEF Imports

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public ClassViewProxy()
        {
            this.Name = ClassViewToolWindowViewModel.TOOLWINDOW_NAME;
            this.Header = ClassViewToolWindowViewModel.TOOLWINDOW_TITLE;
        }

        #endregion // Constructor

        #region Public Functions

        /// <summary>
        /// A abstract function that is overriden by the plugin to create the
        /// actual instance of the tool window and pass it back as a out paramter
        /// </summary>
        protected override Boolean CreateToolWindow(out IToolWindowBase toolWindow)
        {
            toolWindow = new ClassViewToolWindowView(LayoutManager.Value, this.StructureDictionary.Value, this.PropertyInspector.Value);

            return true;
        }

        #endregion // Public Functions
    }
}
