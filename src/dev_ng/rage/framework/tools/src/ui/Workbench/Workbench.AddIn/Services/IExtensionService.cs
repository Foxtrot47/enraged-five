﻿using System;
using System.Collections.Generic;

namespace Workbench.AddIn.Services
{

    /// <summary>
    /// Extension service.
    /// </summary>
    public interface IExtensionService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="extensions"></param>
        /// <returns></returns>
        IList<T> Sort<T>(IEnumerable<T> extensions) 
            where T : IExtension;
 
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="extensionCollection1"></param>
        /// <param name="joinItem"></param>
        /// <param name="extensionCollection2"></param>
        /// <returns></returns>
        IEnumerable<T> SortAndJoin<T>(IEnumerable<T> extensionCollection1, T joinItem, IEnumerable<T> extensionCollection2) 
            where T : IExtension;    
    }

} // Workbench.AddIn.Services
