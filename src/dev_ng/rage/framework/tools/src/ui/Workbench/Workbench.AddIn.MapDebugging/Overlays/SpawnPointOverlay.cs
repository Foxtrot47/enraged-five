﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Media;
using RSG.Base.Collections;
using Workbench.AddIn.Services;
using RSG.Base.ConfigParser;
using Workbench.AddIn.MapDebugging.OverlayViews;
using System.ComponentModel.Composition;
using RSG.Base.Editor;
using RSG.Base.Math;
using RSG.Base.Windows.Controls.WpfViewport2D.Controls;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using RSG.Model.Common;
using RSG.Model.Common.Map;
using RSG.Base.Extensions;
using RSG.Base.Tasks;
using RSG.Base.Windows.Dialogs;
using RSG.Model.Common.Extensions;
using RSG.Model.Common.Util;
using WidgetEditor.AddIn;
using MapViewport.AddIn;
using Workbench.AddIn.Services.Model;
using Workbench.AddIn.MapDebugging.ToolTips;
using System.Windows.Input;


namespace Workbench.AddIn.MapDebugging.Overlays
{
    /// <summary>
    /// Intermediate class for tracking an item's color.
    /// </summary>
    public class SpawnPointFilterItem : ViewModelBase
    {
        #region Fields
        private string m_name;
        private Color m_colour;
        #endregion

        #region Constructors
        public SpawnPointFilterItem(string name, Color color)
        {
            this.Name = name;
            this.Colour = color;
        }
        #endregion

        #region Properties

        /// <summary>
        /// Name of the spawn point filter.
        /// </summary>
        public string Name
        {
            get { return m_name; }
            set 
            {
                if (m_name == value)
                    return;

                this.m_name = value;
                OnPropertyChanged("Name"); 
            }
        }

        /// <summary>
        /// The colour of the section when it is rendered onto the map.
        /// </summary>
        public Color Colour
        {
            get { return m_colour; }
            set 
            {
                if (m_colour == value)
                    return;

                this.m_colour = value;
                OnPropertyChanged("Colour");  
            }
        }
        #endregion 

        #region Methods
        public override string ToString()
        {
            return this.Name;
        }
        #endregion
    }

    /// <summary>
    /// Intermediate class for handling a group's name and color in the UI.
    /// </summary>
    public class SpawnPointGroupItem : ViewModelBase
    {
        #region Fields
        private string m_displayName;
        private string m_name;
        private Color m_colour;
        #endregion

        #region Constructors
        public SpawnPointGroupItem(string displayName, string name, Color color)
        {
            this.DisplayName = displayName;
            this.Name = name;
            this.Colour = color;
        }
        #endregion

        #region Properties

        /// <summary>
        /// Name of the spawn point filter.
        /// </summary>
        public string DisplayName
        {
            get { return m_displayName; }
            set
            {
                if (m_displayName == value)
                    return;

                this.m_displayName = value;
                OnPropertyChanged("DisplayName");
            }
        }

        /// <summary>
        /// Name of the spawn point filter.
        /// </summary>
        public string Name
        {
            get { return m_name; }
            set
            {
                if (m_name == value)
                    return;

                this.m_name = value;
                OnPropertyChanged("Name");
            }
        }
        
        /// <summary>
        /// The colour of the section when it is rendered onto the map.
        /// </summary>
        public Color Colour
        {
            get { return m_colour; }
            set 
            {
                if (m_colour == value)
                    return;

                this.m_colour = value;
                OnPropertyChanged("Colour");  
            }
        }


        #endregion

        #region Methods
        public override string ToString()
        {
            return this.DisplayName;
        }
        #endregion
    }

    /// <summary>
    /// Comparer class to sort spawn points by position.
    /// </summary>
    public class SpawnPointInstanceComparer : IComparer<SpawnPointInstance>
    {
        public int Compare(SpawnPointInstance left, SpawnPointInstance right)
        {
            if (left.Position.X == right.Position.X)
            {
                if (left.Position.Y == right.Position.Y) return 0;

                return (left.Position.Y - right.Position.Y) > 0 ? 1 : -1;
            }
            else
            {
                return (left.Position.X - right.Position.X) > 0 ? 1 : -1;
            }
        }

    }

    /// <summary>
    /// A helper class for matching up a spawn point with the instance.
    /// </summary>
    public struct SpawnPointInstance
    {
        public SpawnPointInstance(ISpawnPoint spawnPoint, Vector3f position, IEntity entity)
        {
            SpawnPoint = spawnPoint;
            Position = position;
            Entity = entity;
        }

        public ISpawnPoint SpawnPoint;
        public Vector3f Position;
        public IEntity Entity;
    }

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension("Workbench.AddIn.MapDebugging.OverlayGroups.MapDebug", typeof(Viewport.AddIn.ViewportOverlay))]
    public class SpawnPointOverlay : LevelDependentViewportOverlay
    {
        #region Private member fields

        private SortedList<string, int> m_modelTypes;

        #endregion

        #region Constants
        /// <summary>
        /// Overlay name/description.
        /// </summary>
        private const string c_name = "Spawn Points";
        private const string c_description = "Displays the spawn points on the map based on the current filtering settings.";

        /// <summary>
        /// Location relative to the level's path that the scenario meta files live in.
        /// </summary>
        private const string c_scenarioMetafileDirectory = @"scenario";

        /// <summary>
        /// Scale of the overlay image in relation to the level's background image.
        /// </summary>
        private const float c_overlayImageScale = 0.25f;

        /// <summary>
        /// Default colors for spawn points/chained graph nodes.
        /// </summary>
        /// 
        private System.Windows.Media.Color c_defaultTypeColour = System.Windows.Media.Color.FromArgb(255, 0, 127, 0);
        private System.Drawing.Color c_defaultExportedSpawnPointColor = System.Drawing.Color.Red;
        private System.Drawing.Color c_defaultMetafileSpawnPointColor = System.Drawing.Color.Green;
        private System.Drawing.Color c_defaultMetafileChainingGraphColor = System.Drawing.Color.Blue;
        private System.Drawing.Color c_defaultSelectedSpawnPointColor = System.Drawing.Color.Yellow;

        /// <summary>
        /// RAG bank paths for live editing.
        /// </summary>
        private const string RAG_SCENARIO_BANK = "Scenarios";        
        private const string RAG_CREATE_SCENARIO_WIDGETS = "Scenarios/Create Scenario widgets";
        private const string RAG_SCENARIO_DEBUG_GROUP = "Scenarios/Debug";
        private const string RAG_SCENARIO_EDITING_GROUP = "Scenarios/Scenario Point Editor";
        private const string RAG_RENDER_SCENARIO_POINTS = "Scenarios/Debug/Render Scenario Points";
        private const string RAG_ENABLE_SCENARIO_POINT_EDITING = "Scenarios/Scenario Point Editor/Editing";
        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// The perforce sync service
        /// </summary>
        [Workbench.AddIn.ImportExtension(Workbench.AddIn.CompositionPoints.PerforceSyncService, typeof(Workbench.AddIn.Services.IPerforceSyncService))]
        protected IPerforceSyncService PerforceSyncService { get; set; }

        /// <summary>
        /// A reference to view model for the map viewport
        /// </summary>
        [ImportExtension(Viewport.AddIn.CompositionPoints.MapViewport, typeof(Viewport.AddIn.IMapViewport))]
        protected Viewport.AddIn.IMapViewport MapViewportProxy { get; set; }

        /// <summary>
        /// MEF import for proxy UI
        /// </summary>
        [ImportExtension(WidgetEditor.AddIn.CompositionPoints.ProxyService, typeof(IProxyService))]
        public Lazy<IProxyService> ProxyService { get; set; }
        #endregion // MEF Imports
        
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public bool ShowAll
        {
            get { return m_showAll; }
            set
            {
                SetPropertyValue(value, () => ShowAll,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_showAll = (bool)newValue;
                            FlagOverlayChange();
                        }
                ));
            }
        }
        private bool m_showAll;

        /// <summary>
        /// 
        /// </summary>
        public bool ShowExported
        {
            get { return m_showExported; }
            set
            {
                SetPropertyValue(value, () => ShowExported,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_showExported = (bool)newValue;
                            FlagOverlayChange();
                        }
                ));
            }
        }
        private bool m_showExported;

        /// <summary>
        /// 
        /// </summary>
        public bool ShowMetafile
        {
            get { return m_showMetafile; }
            set
            {
                SetPropertyValue(value, () => ShowMetafile,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_showMetafile = (bool)newValue;
                            FlagOverlayChange();
                        }
                ));
            }
        }
        private bool m_showMetafile;

        /// <summary>
        /// Toggle for displaying chained paths.
        /// </summary>
        public bool ShowChainedPaths
        {
            get { return m_showChainedPaths; }
            set
            {
                SetPropertyValue(value, () => ShowChainedPaths,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_showChainedPaths = (bool)newValue;
                            FlagOverlayChange();
                        }
                ));
            }
        }
        private bool m_showChainedPaths;

        /// <summary>
        /// Model types.
        /// </summary>
        public ObservableCollection<SpawnPointFilterItem> SpawnPointModelTypes
        {
            get;
            set;
        }

        public ObservableCollection<SpawnPointFilterItem> FilterModelTypes
        {
            get;
            set;
        }

        //private ObservableCollection<string> m_pedTypes;

        /// <summary>
        /// Toggle for displaying a spawn points.
        /// </summary>
        public bool ShowSpawnPoints
        {
            get { return m_showSpawnPoints; }
            set
            {
                SetPropertyValue(value, () => ShowSpawnPoints,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_showSpawnPoints = (bool)newValue;
                            FlagOverlayChange();
                        }
                ));
            }
        }
        private bool m_showSpawnPoints;

        /// <summary>
        /// 
        /// </summary>
        public bool ShowSpecific
        {
            get { return m_showSpecific; }
            set
            {
                SetPropertyValue(value, () => ShowSpecific,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_showSpecific = (bool)newValue;
                            FlagOverlayChange();
                        }
                ));
            }
        }
        private bool m_showSpecific;

        /// <summary>
        /// 
        /// </summary>
        public string SpecificMetafile
        {
            get { return m_specificMetafile; }
            set
            {
                SetPropertyValue(value, () => SpecificMetafile,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_specificMetafile = (string)newValue;
                            FlagOverlayChange();
                        }
                ));
            }
        }
        private string m_specificMetafile;

        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<string> Metafiles
        {
            get;
            set;
        }

        /// <summary>
        /// Enables/disables the update overlay button.
        /// </summary>
        public bool EnableUpdateOverlay
        {
            get { return m_enableUpdateOverlay; }
            set
            {
                SetPropertyValue(value, () => EnableUpdateOverlay,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_enableUpdateOverlay = (bool)newValue;
                        }
                ));
            }
        }
        private bool m_enableUpdateOverlay;

        /// <summary>
        /// Method to update the render view based on a certain user interface event (e.g. color change).
        /// </summary>
        public bool ForceUpdateFilter
        {
            set
            {
                OnFilterChanged();
            }
        }

        /// <summary>
        /// Toggle for displaying data by type.
        /// </summary>
        public bool FilterByType
        {
            get { return m_filterByType; }
            set
            {
                SetPropertyValue(value, () => FilterByType,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_filterByType = (bool)newValue;
                            FlagOverlayChange();
                        }
                ));
            }
        }
        private bool m_filterByType;

        /// <summary>
        /// Filter by model type.
        /// </summary>
        public bool FilterByModelType
        {
            get { return m_filterByModelType; }
            set
            {
                SetPropertyValue(value, () => FilterByModelType,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_filterByModelType = (bool)newValue;
                            FlagOverlayChange();
                        }
                ));
            }
        }

        private bool m_filterByModelType;

        /// <summary>
        /// No filtering by any type.
        /// </summary>
        public bool NoFilterBy
        {
            get { return m_noFilterBy; }
            set
            {
                SetPropertyValue(value, () => NoFilterBy,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_noFilterBy = (bool)newValue;
                            FlagOverlayChange();
                        }
                ));
            }
        }

        private bool m_noFilterBy;

        /// <summary>
        /// A list of all possible types.
        /// </summary>
        public ObservableCollection<SpawnPointFilterItem> SpawnPointTypes
        {
            get;
            set;
        }

        /// <summary>
        /// A list of all types enabled for display in the user interface.
        /// </summary>
        public ObservableCollection<SpawnPointFilterItem> FilterTypes
        {
            get { return m_filterTypes; }
            set
            {
                SetPropertyValue(value, () => FilterTypes,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_filterTypes = (ObservableCollection<SpawnPointFilterItem>)newValue;
                            FlagOverlayChange();
                        }
                ));
            }
        }
        private ObservableCollection<SpawnPointFilterItem> m_filterTypes;
        private Dictionary<string, System.Drawing.Color> m_filterTypeColorDictionary; 

        /// <summary>
        /// Toggle for displaying by group.
        /// </summary>
        public bool FilterByGroup
        {
            get { return m_filterByGroup; }
            set
            {
                SetPropertyValue(value, () => FilterByGroup,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_filterByGroup = (bool)newValue;
                            FlagOverlayChange();
                        }
                ));
            }
        }
        private bool m_filterByGroup;

        /// <summary>
        /// A list of all possible groups.
        /// </summary>
        public ObservableCollection<SpawnPointGroupItem> SpawnPointGroups
        {
            get;
            set;
        }

        /// <summary>
        /// A list of all groups enabled for display in the user interface.
        /// </summary>
        public ObservableCollection<SpawnPointGroupItem> GroupTypes
        {
            get { return m_groupTypes; }
            set
            {
                SetPropertyValue(value, () => GroupTypes,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_groupTypes = (ObservableCollection<SpawnPointGroupItem>)newValue;
                            FlagOverlayChange();
                        }
                ));
            }
        }
        private ObservableCollection<SpawnPointGroupItem> m_groupTypes;
        private Dictionary<string, System.Drawing.Color> m_filterGroupColorDictionary;
        private Dictionary<string, System.Drawing.Color> m_filterModelTypeColorDictionary;

        /// <summary>
        /// String for filter by model set, if enabled.
        /// </summary>
        public string ModelSetFilter
        {
            get { return m_modelSetFilter; }
            set
            {
                SetPropertyValue(value, () => ModelSetFilter,
                new RSG.Base.Editor.Command.PropertySetDelegate(
                    delegate(Object newValue)
                    {
                        m_modelSetFilter = (string)newValue;
                        FlagOverlayChange();
                    }
                ));
            }
        }
        private string m_modelSetFilter;

        /// <summary>
        /// Toggle for filtering by start time.
        /// </summary>
        public bool FilterByStartTime
        {
            get { return m_filterByStartTime; }
            set
            {
                SetPropertyValue(value, () => FilterByStartTime,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_filterByStartTime = (bool)newValue;
                            FlagOverlayChange();
                        }
                ));
            }
        }
        private bool m_filterByStartTime;

        /// <summary>
        /// Value for filtering by start time.
        /// </summary>
        public int StartTimeFilter
        {
            get { return m_startTimeFilter; }
            set
            {
                SetPropertyValue(value, () => StartTimeFilter,
                new RSG.Base.Editor.Command.PropertySetDelegate(
                    delegate(Object newValue)
                    {
                        m_startTimeFilter = (int)newValue;
                		FlagOverlayChange();
                    }
                ));
            }
        }
        private int m_startTimeFilter;

        /// <summary>
        /// Toggle for filtering by end time.
        /// </summary>
        public bool FilterByEndTime
        {
            get { return m_filterByEndTime; }
            set
            {
                SetPropertyValue(value, () => FilterByEndTime,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_filterByEndTime = (bool)newValue;
                            FlagOverlayChange();
                        }
                ));
            }
        }
        private bool m_filterByEndTime;

        /// <summary>
        /// 
        /// </summary>
        public bool Include24HourScenerios
        {
            get { return m_include24HourScenerios; }
            set
            {
                SetPropertyValue(value, () => Include24HourScenerios,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_include24HourScenerios = (bool)newValue;
                            FlagOverlayChange();
                        }
                ));
            }
        }
        private bool m_include24HourScenerios;

        /// <summary>
        /// 
        /// </summary>
        public bool ShowExactTimeMatch
        {
            get { return m_showExactTimeMatch; }
            set
            {
                SetPropertyValue(value, () => ShowExactTimeMatch,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_showExactTimeMatch = (bool)newValue;
                            FlagOverlayChange();
                        }
                ));
            }
        }
        private bool m_showExactTimeMatch;

        /// <summary>
        /// Value for filtering by end time.
        /// </summary>
        public int EndTimeFilter
        {
            get { return m_endTimeFilter; }
            set
            {
                SetPropertyValue(value, () => EndTimeFilter,
                new RSG.Base.Editor.Command.PropertySetDelegate(
                    delegate(Object newValue)
                    {
                        m_endTimeFilter = (int)newValue;
                    	FlagOverlayChange();
                    }
                ));
            }
        }
        private int m_endTimeFilter;

        /// <summary>
        /// Total number of spawn points rendered on the map.
        /// </summary>
        public uint SpawnPointCount
        {
            get { return m_spawnPointCount; }
            set
            {
                SetPropertyValue(value, () => SpawnPointCount,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_spawnPointCount = (uint)newValue;
                        }
                ));
            }
        }
        private uint m_spawnPointCount;

        /// <summary>
        /// Total number of chained paths rendered on the map.
        /// </summary>
        public uint ChainedPathsCount
        {
            get { return m_chainedPathsCount; }
            set
            {
                SetPropertyValue(value, () => ChainedPathsCount,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_chainedPathsCount = (uint)newValue;
                        }
                ));
            }
        }
        private uint m_chainedPathsCount;

        /// <summary>
        /// 
        /// </summary>
        public int PathLineWidth
        {
            get { return m_pathLineWidth; }
            set
            {
                SetPropertyValue(value, () => PathLineWidth,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_pathLineWidth = (int)newValue;
                        }
                ));
            }
        }
        private int m_pathLineWidth;

        /// <summary>
        /// 
        /// </summary>
        public float SpawnPointSize
        {
            get { return m_spawnPointSize; }
            set
            {
                SetPropertyValue(value, () => SpawnPointSize,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_spawnPointSize = (float)newValue;
                            FlagOverlayChange();
                        }
                ));
            }
        }
        private float m_spawnPointSize;

        /// <summary>
        /// A list of all spawn points currently rendered.
        /// </summary>
        private List<SpawnPointInstance> SpawnPointInstances
        {
            get;
            set;
        }

        /// <summary>
        /// List of all chaining groups currently rendered
        /// </summary>
        private List<IChainingGraph> RenderedChainingGraphs
        {
            get;
            set;
        }

        /// <summary>
        /// A list of all spawn points currently selected.
        /// </summary>
        private List<SpawnPointInstance> SelectedSpawnPoints
        {
            get;
            set;
        }

        /// <summary>
        /// A list of all chaining graphs currently selected.
        /// </summary>
        private List<IChainingGraph> SelectedChainingGraphs
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        private Viewport2DImageOverlay OverlayImage
        {
            get;
            set;
        }

        /// <summary>
        /// Image specifically dedicated to drawing any selected objects; instead of re-drawing the entire overlay.
        /// </summary>
        private Viewport2DImageOverlay OverlaySelectionImage
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        private float ProgressIncrement
        {
            get;
            set;
        }

        private SpawnPointOverlayToolTipView CurrentToolTip
        {
            get;
            set;
        }
        #endregion
        
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public SpawnPointOverlay()
            : base(c_name, c_description)
        {
            Metafiles = new ObservableCollection<string>();
            SpawnPointTypes = new ObservableCollection<SpawnPointFilterItem>();
            FilterTypes = new ObservableCollection<SpawnPointFilterItem>();
            FilterModelTypes = new ObservableCollection<SpawnPointFilterItem>();
            SpawnPointGroups = new ObservableCollection<SpawnPointGroupItem>();
            GroupTypes = new ObservableCollection<SpawnPointGroupItem>();
            
            SpawnPointCount = 0;
            ShowAll = true;
            ShowChainedPaths = false;
            ShowSpawnPoints = true;
            PathLineWidth = 1;
            SpawnPointSize = 5.0f;
            EnableUpdateOverlay = false;

            SpawnPointInstances = new List<SpawnPointInstance>();
            RenderedChainingGraphs = new List<IChainingGraph>();
            SelectedSpawnPoints = new List<SpawnPointInstance>();
            SelectedChainingGraphs = new List<IChainingGraph>();
            m_filterTypeColorDictionary = new Dictionary<string, System.Drawing.Color>();
            m_filterGroupColorDictionary = new Dictionary<string, System.Drawing.Color>();
        }
        #endregion // Constructors

        #region Overrides
        /// <summary>
        /// Gets called when the user wants to view this overlay
        /// </summary>
        public override void Activated()
        {
            base.Activated();
            MapViewportProxy.ViewportClicked += ViewportClicked;
            ProxyService.Value.Console.AddUpdates(RAG_SCENARIO_BANK);

            //NOTE: Initialize widgets prior to requiring an update.  They will not exist prior to this call.
            InitializeWidgets();

            ProxyService.Value.Console.AddUpdates(RAG_SCENARIO_DEBUG_GROUP);
            ProxyService.Value.Console.AddUpdates(RAG_SCENARIO_EDITING_GROUP);

            if (LevelBrowserProxy.Value.SelectedLevel != null)
            {
                ContentNodeLevel levelNode = LevelBrowserProxy.Value.SelectedLevel.GetConfigLevelNode(Config.Value.GameConfig);

                if (levelNode != null)
                {
                    // Make sure that the scenario point meta files are synced
                    List<string> perforceFiles = new List<string>();
                    perforceFiles.Add(Path.Combine(Path.GetFullPath(levelNode.AbsolutePath), c_scenarioMetafileDirectory, "..."));

                    int syncedFiles = 0;
                    PerforceSyncService.Show("Some of the scenario point metadata files are currently out of date.\n" +
                                             "Would you like to grab latest on them now?",
                                             perforceFiles.ToArray(), ref syncedFiles, false, true);

                    // Make sure that the required data is loaded
                    CancellationTokenSource cts = new CancellationTokenSource();
                    LevelTaskContext context = new LevelTaskContext(cts.Token, LevelBrowserProxy.Value.SelectedLevel);

                    ITask loadDataTask = new ActionTask("Load Data", (ctx, progress) => EnsureDataLoaded((LevelTaskContext)ctx, progress));

                    TaskExecutionDialog dialog = new TaskExecutionDialog();
                    TaskExecutionViewModel vm = new TaskExecutionViewModel("Generating Overlay", loadDataTask, cts, context);
                    vm.AutoCloseOnCompletion = true;
                    dialog.DataContext = vm;
                    Nullable<bool> selectionResult = dialog.ShowDialog();
                    if (false == selectionResult)
                    {
                        return;
                    }

                    // Get the list of metafiles in the metafile directory
                    Metafiles.BeginUpdate();
                    Metafiles.Clear();
                    
                    string metafileDirectory = Path.Combine(levelNode.AbsolutePath, c_scenarioMetafileDirectory);
                    foreach (string metafile in Directory.GetFiles(metafileDirectory))
                    {
                        Metafiles.Add(Path.GetFileName(metafile));
                    }

                    Metafiles.EndUpdate();
                    SpecificMetafile = Metafiles.FirstOrDefault();

                    // Get the list of spawn types that may exist
                    ISet<string> types = GetTypes(LevelBrowserProxy.Value.SelectedLevel);
                    List<string> sortedTypes = new List<string>(types);
                    sortedTypes.Sort();

                    SpawnPointTypes.BeginUpdate();
                    foreach (string spawnType in sortedTypes)
                    {
                        SpawnPointTypes.Add(new SpawnPointFilterItem(spawnType, c_defaultTypeColour));
                    }
                    SpawnPointTypes.EndUpdate();
                    
                    FilterTypes.Clear();
                    FilterTypes.Add(SpawnPointTypes.FirstOrDefault());

                    ISet<string> spawnGroups = GetSpawnGroups(LevelBrowserProxy.Value.SelectedLevel);
                    List<string> sortedSpawnGroups = new List<string>(spawnGroups);
                    sortedSpawnGroups.Sort();
                    SpawnPointGroups.BeginUpdate();
                    foreach (string spawnGroup in sortedSpawnGroups)
                    {
                        string spawnGroupDisplayName = spawnGroup;
                        if (String.IsNullOrWhiteSpace(spawnGroup) == true)
                            spawnGroupDisplayName = "No Group";

                        SpawnPointGroups.Add(new SpawnPointGroupItem(spawnGroupDisplayName, spawnGroup, c_defaultTypeColour));
                    }
                    SpawnPointGroups.EndUpdate();
                    
                    GroupTypes.Clear();
                    GroupTypes.Add(SpawnPointGroups.FirstOrDefault());

                    // Force an update of the overlay
                    UpdateOverlayImage(LevelBrowserProxy.Value.SelectedLevel);

                    FillPedTypes(LevelBrowserProxy.Value.SelectedLevel.SpawnPoints);

                    OnFilterChanged();
                }
            }
        }

        /// <summary>
        /// Fill in the model types observable collection.
        /// </summary>
        /// <param name="spawnPoints">Spawn points.</param>
        private void FillPedTypes(IList<ISpawnPoint> spawnPoints)
        {
            SpawnPointModelTypes = new ObservableCollection<SpawnPointFilterItem>();

            m_modelTypes = new SortedList<string, int>();
            foreach (var spawnPoint in spawnPoints)
            {
                if (!m_modelTypes.ContainsKey(spawnPoint.ModelSet))
                {
                    m_modelTypes.Add(spawnPoint.ModelSet, 1);
                }
                else
                {
                    int currentCount = m_modelTypes[spawnPoint.ModelSet];
                    currentCount++;
                    m_modelTypes[spawnPoint.ModelSet] = currentCount;
                }
            }

            foreach (var spawnPoint in m_modelTypes.Keys)
            {
                SpawnPointModelTypes.Add(new SpawnPointFilterItem(spawnPoint, this.c_defaultTypeColour));
            }
        }

        /// <summary>
        /// Gets called when the user turns off this overlay
        /// </summary>
        public override void Deactivated()
        {
            this.Geometry.Clear();
            MapViewportProxy.ViewportClicked -= ViewportClicked;
            ProxyService.Value.Console.RemoveUpdates(RAG_SCENARIO_EDITING_GROUP);
            ProxyService.Value.Console.RemoveUpdates(RAG_SCENARIO_DEBUG_GROUP);
            ProxyService.Value.Console.RemoveUpdates(RAG_SCENARIO_BANK);
            OverlayImage = null;
            OverlaySelectionImage = null;
            base.Deactivated();
        }

        /// <summary>
        /// Gets the control that should be shown in the overlay details panel
        /// </summary>
        public override System.Windows.Controls.Control GetOverlayControl()
        {
            return new SpawnPointOverlayView();
        }

        /// <summary>
        /// Gets the control that shows information at the bottom right of the viewport.
        /// </summary>
        public override System.Windows.Controls.Control GetOverlayToolTipControl()
        {
            if (this.CurrentToolTip != null)
            {
                this.CurrentToolTip.LostFocus -= CurrentToolTip_LostFocus;
                foreach (System.Windows.Window window in System.Windows.Application.Current.Windows)
                {
                    window.Deactivated -= window_Deactivated;
                }
            }

            this.CurrentToolTip = new SpawnPointOverlayToolTipView(ProxyService, SelectedSpawnPoints, SelectedChainingGraphs);
            this.CurrentToolTip.LostFocus += CurrentToolTip_LostFocus;
            foreach (System.Windows.Window window in System.Windows.Application.Current.Windows)
            {
                window.Deactivated += window_Deactivated;
            }

            return this.CurrentToolTip;
        }

        void window_Deactivated(object sender, EventArgs e)
        {
            MapViewportProxy.HideToolTip();
        }

        void CurrentToolTip_LostFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            MapViewportProxy.HideToolTip();
        }
        #endregion // Overrides

        #region Event Callbacks
        /// <summary>
        /// Gets called when the user clicked somewhere on the viewport other than on geometry
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewportClicked(object sender, ViewportClickEventArgs e)
        {
            SelectedSpawnPoints.Clear();
            SelectedChainingGraphs.Clear();

            float threshold = m_spawnPointSize;
            float thresholdSq = m_spawnPointSize * m_spawnPointSize;

            //Find all spawn points selected.
            Vector3f worldPosition = new Vector3f((float)e.WorldPosition.X, (float)e.WorldPosition.Y, 0.0f);
            Vector2f lastSelectedVector = new Vector2f((float)e.WorldPosition.X, (float)e.WorldPosition.Y);

            foreach (SpawnPointInstance spawnPointInstance in SpawnPointInstances)
            {
                float xDelta = spawnPointInstance.Position.X - worldPosition.X;
                if (System.Math.Abs(xDelta) > threshold) continue;

                float yDelta = spawnPointInstance.Position.Y - worldPosition.Y;
                if (System.Math.Abs(yDelta) > threshold) continue;

                float distanceSq = (float)((xDelta * xDelta) + (yDelta * yDelta));
                if (distanceSq < thresholdSq)
                {
                    SelectedSpawnPoints.Add(spawnPointInstance);
                }
            }

            //Find all chaining graphs selected.
            foreach (IChainingGraph chainingGraph in RenderedChainingGraphs)
            {
                bool skip = false;
                System.Drawing.Color chainingGraphColor = c_defaultMetafileChainingGraphColor;
                foreach (LinkedList<IChainingGraphNode> chainedPathList in chainingGraph.ChainedPaths)
                {
                    LinkedListNode<IChainingGraphNode> currentNode = chainedPathList.First;

                    do
                    {
                        float xDelta = currentNode.Value.Position.X - worldPosition.X;
                        if (System.Math.Abs(xDelta) > threshold)
                        {
                            currentNode = currentNode.Next;
                            continue;
                        }

                        float yDelta = currentNode.Value.Position.Y - worldPosition.Y;
                        if (System.Math.Abs(yDelta) > threshold)
                        {
                            currentNode = currentNode.Next;
                            continue;
                        }

                        float distanceSq = (float)((xDelta * xDelta) + (yDelta * yDelta));
                        if (distanceSq < thresholdSq)
                        {
                            SelectedChainingGraphs.Add(chainingGraph);
                            skip = true;
                            break;
                        }

                        currentNode = currentNode.Next;
                    } while (currentNode != null);

                    if ( skip == true ) break;
                }
            }

            if (SelectedSpawnPoints.Count == 0 && SelectedChainingGraphs.Count == 0)
                MapViewportProxy.HideToolTip();

            // Update the overlay image's selection.
            if (LevelBrowserProxy.Value.SelectedLevel != null)
            {
                Vector2i size = new Vector2i(966, 627);
                Vector2f pos = new Vector2f(-483.0f, 627.0f);

                if (LevelBrowserProxy.Value.SelectedLevel.ImageBounds != null)
                {
                    size = new RSG.Base.Math.Vector2i((int)(LevelBrowserProxy.Value.SelectedLevel.ImageBounds.Max.X - LevelBrowserProxy.Value.SelectedLevel.ImageBounds.Min.X), (int)(LevelBrowserProxy.Value.SelectedLevel.ImageBounds.Max.Y - LevelBrowserProxy.Value.SelectedLevel.ImageBounds.Min.Y));
                    pos = new RSG.Base.Math.Vector2f(LevelBrowserProxy.Value.SelectedLevel.ImageBounds.Min.X, LevelBrowserProxy.Value.SelectedLevel.ImageBounds.Max.Y);
                }

                OverlaySelectionImage = new Viewport2DImageOverlay(etCoordSpace.World, "", new RSG.Base.Math.Vector2i((int)(size.X * c_overlayImageScale), (int)(size.Y * c_overlayImageScale)), pos, new RSG.Base.Math.Vector2f(size.X, size.Y));

                this.Geometry.Clear();
                this.Geometry.BeginUpdate();
                OverlaySelectionImage.BeginUpdate();

                foreach (SpawnPointInstance spawnPoint in SelectedSpawnPoints)
                {
                    OverlaySelectionImage.RenderCircle(spawnPoint.Position, m_spawnPointSize, c_defaultSelectedSpawnPointColor);
                }

                foreach (IChainingGraph chainingGraph in SelectedChainingGraphs)
                {
                    RenderChainingGraph(chainingGraph, OverlaySelectionImage, c_defaultSelectedSpawnPointColor);
                }

                OverlaySelectionImage.EndUpdate();
                this.Geometry.Add(OverlayImage);
                this.Geometry.Add(OverlaySelectionImage);
                this.Geometry.EndUpdate();
            }

            if (SelectedSpawnPoints.Count > 0 || SelectedChainingGraphs.Count > 0)
                MapViewportProxy.ShowToolTip(e.ViewPosition);
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Export the model type and their count to a file.
        /// </summary>
        internal void ExportModelTypeTotals()
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.Filter = "CSV (Comma delimited)|*.csv";
            Nullable<bool> result = dlg.ShowDialog();
            if (false == result)
            {
                return;
            }

            using (StreamWriter sw = new StreamWriter(dlg.FileName))
            {
                string header = "Model Type,Count";
                sw.WriteLine(header);

                foreach (var kvp in m_modelTypes)
                {
                    sw.WriteLine("{0},{1}", kvp.Key, kvp.Value);
                }
            }
        }

        internal void ExportReport()
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.Filter = "CSV (Comma delimited)|*.csv";
            Nullable<bool> result = dlg.ShowDialog();
            if (false == result)
            {
                return;
            }

            Dictionary<string, List<SpawnPointInstance>> spawnPoints = new Dictionary<string, List<SpawnPointInstance>>();
            foreach (SpawnPointInstance instance in this.SpawnPointInstances)
            {
                if (!spawnPoints.ContainsKey(instance.SpawnPoint.SpawnType))
                {
                    spawnPoints.Add(instance.SpawnPoint.SpawnType, new List<SpawnPointInstance>());
                }

                spawnPoints[instance.SpawnPoint.SpawnType].Add(instance);
            }

            using (StreamWriter sw = new StreamWriter(dlg.FileName))
            {
                string header = "Spawn Type,Total Instances,Region,Start Time,End Time,Model Set,Spawn Group,Attached To Object,Object Name,X,Y,Z";
                sw.WriteLine(header);
                foreach (KeyValuePair<string, List<SpawnPointInstance>> sp in spawnPoints)
                {
                    string count = sp.Value.Count.ToString();
                    foreach (SpawnPointInstance instance in sp.Value)
                    {
                        string region = Path.GetFileNameWithoutExtension(instance.SpawnPoint.SourceFile);
                        string set = instance.SpawnPoint.ModelSet;
                        string group = instance.SpawnPoint.SpawnGroup;
                        string attached = instance.Entity == null ? "false" : "true";
                        string objectName = "n/a";
                        if (instance.Entity != null && instance.Entity.ReferencedArchetype != null)
                        {
                            objectName = instance.Entity.ReferencedArchetype.Name;
                        }

                        string start = "n/a";
                        string x = instance.Position.X.ToString();
                        string y = instance.Position.Y.ToString();
                        string z = instance.Position.Z.ToString();

                        if (instance.SpawnPoint.StartTime.HasValue)
                        {
                            start = instance.SpawnPoint.StartTime.Value.ToString();
                        }

                        string end = "n/a";
                        if (instance.SpawnPoint.EndTime.HasValue)
                        {
                            end = instance.SpawnPoint.EndTime.Value.ToString();
                        }

                        string point = "{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11}";
                        point = string.Format(point, sp.Key, count, region, start, end, set, group, attached, objectName, x, y, z);
                        sw.WriteLine(point);
                    }
                }
            }
        }

         /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="progress"></param>
        private void EnsureDataLoaded(LevelTaskContext context, IProgress<TaskProgress> progress)
        {
            ContentNodeLevel levelNode = LevelBrowserProxy.Value.SelectedLevel.GetConfigLevelNode(Config.Value.GameConfig);
            
            // Compute the total number of steps to process
            int steps = context.MapHierarchy.AllSections.Count() + 1;
            float increment = 1.0f / steps;

            // Load the level spawn points
            progress.Report(new TaskProgress(increment, true, "Loading level spawn points"));
            context.Level.LoadStats(new StreamableStat[] { StreamableLevelStat.SpawnPointClusters, StreamableLevelStat.SpawnPoints, StreamableLevelStat.ChainingGraphs });
            //context.Level.RequestStatistics(new StreamableLevelStat[] { StreamableLevelStat.SpawnPoints }, false);

            // Load all the map section data
            foreach (IMapSection section in context.MapHierarchy.AllSections)
            {
                context.Token.ThrowIfCancellationRequested();

                // Update the progress information
                string message = String.Format("Loading {0}", section.Name);
                progress.Report(new TaskProgress(increment, true, message));

                // Only request that the archetypes for this section are loaded
                section.LoadStats(new StreamableStat[] { StreamableMapSectionStat.Archetypes, StreamableMapSectionStat.Entities });
            }
        }

        /// <summary>
        /// Acquires all types used by spawn points and chaining graphs.
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        private ISet<string> GetTypes(ILevel level)
        {
            ISet<string> types = new HashSet<string>();
            types.AddRange(level.SpawnPoints.Select(item => item.SpawnType));

            IMapHierarchy hierarchy = LevelBrowserProxy.Value.SelectedLevel.MapHierarchy;

            foreach (IMapSection section in hierarchy.AllSections)
            {
                if (section == null)
                {
                    continue;
                }

                if (section.Archetypes != null)
                {
                    foreach (IMapArchetype archetype in section.Archetypes)
                    {
                        ISimpleMapArchetype simple = archetype as ISimpleMapArchetype;
                        if (simple == null || simple.SpawnPoints == null)
                        {
                            continue;
                        }

                        foreach (ISpawnPoint spawnPoint in simple.SpawnPoints)
                        {
                            if (!types.Contains(spawnPoint.SpawnType))
                            {
                                types.Add(spawnPoint.SpawnType);
                            }
                        }
                    }
                }

                if (section.ChildEntities != null)
                {
                    foreach (IEntity entity in section.ChildEntities)
                    {
                        if (entity == null || entity.SpawnPoints == null)
                        {
                            continue;
                        }

                        foreach (ISpawnPoint spawnPoint in entity.SpawnPoints)
                        {
                            if (!types.Contains(spawnPoint.SpawnType))
                            {
                                types.Add(spawnPoint.SpawnType);
                            }
                        }
                    }
                }
            }

            foreach (IChainingGraph graphs in level.ChainingGraphs)
            {
                foreach (IChainingGraphNode nodes in graphs.ChainedNodes)
                {
                    types.Add(nodes.ScenarioType);
                }
            }

            return types;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        private ISet<string> GetSpawnGroups(ILevel level)
        {
            ISet<string> spawnGroups = new HashSet<string>();
            spawnGroups.AddRange(level.SpawnPoints.Select(item => item.SpawnGroup));

            IMapHierarchy hierarchy = LevelBrowserProxy.Value.SelectedLevel.MapHierarchy;

            foreach (IMapSection section in hierarchy.AllSections)
            {
                if (section == null)
                {
                    continue;
                }

                if (section.Archetypes != null)
                {
                    foreach (IMapArchetype archetype in section.Archetypes)
                    {
                        ISimpleMapArchetype simple = archetype as ISimpleMapArchetype;
                        if (simple == null || simple.SpawnPoints == null)
                        {
                            continue;
                        }

                        foreach (ISpawnPoint spawnPoint in simple.SpawnPoints)
                        {
                            if (!spawnGroups.Contains(spawnPoint.SpawnGroup))
                            {
                                spawnGroups.Add(spawnPoint.SpawnGroup);
                            }
                        }
                    }
                }

                if (section.ChildEntities != null)
                {
                    foreach (IEntity entity in section.ChildEntities)
                    {
                        if (entity == null || entity.SpawnPoints == null)
                        {
                            continue;
                        }

                        foreach (ISpawnPoint spawnPoint in entity.SpawnPoints)
                        {
                            if (!spawnGroups.Contains(spawnPoint.SpawnGroup))
                            {
                                spawnGroups.Add(spawnPoint.SpawnGroup);
                            }
                        }
                    }
                }
            }

            return spawnGroups;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        private void UpdateOverlayImage(ILevel level)
        {
            Geometry.Clear();

            // Update the overlay image
            if (level != null)
            {
                Vector2i size = new Vector2i(966, 627);
                Vector2f pos = new Vector2f(-483.0f, 627.0f);

                if (level.ImageBounds != null)
                {
                    size = new RSG.Base.Math.Vector2i((int)(level.ImageBounds.Max.X - level.ImageBounds.Min.X), (int)(level.ImageBounds.Max.Y - level.ImageBounds.Min.Y));
                    pos = new RSG.Base.Math.Vector2f(level.ImageBounds.Min.X, level.ImageBounds.Max.Y);
                }

                OverlaySelectionImage = new Viewport2DImageOverlay(etCoordSpace.World, "", new RSG.Base.Math.Vector2i((int)(size.X * c_overlayImageScale), (int)(size.Y * c_overlayImageScale)), pos, new RSG.Base.Math.Vector2f(size.X, size.Y));
                Geometry.Add(OverlaySelectionImage);

                OverlayImage = new Viewport2DImageOverlay(etCoordSpace.World, "", new RSG.Base.Math.Vector2i((int)(size.X * c_overlayImageScale), (int)(size.Y * c_overlayImageScale)), pos, new RSG.Base.Math.Vector2f(size.X, size.Y));
                Geometry.Add(OverlayImage);
            }
            else
            {
                OverlayImage = null;
                OverlaySelectionImage = null;
            }
        }

        /// <summary>
        /// Marks the overlay as requiring an update.
        /// </summary>
        private void FlagOverlayChange()
        {
            EnableUpdateOverlay = true;
        }

        /// <summary>
        /// Updated the map viewport with a new display based on filter parameters.
        /// </summary>
        private void OnFilterChanged()
        {
            if (OverlayImage == null)
            {
                return;
            }

            //Hide any tooltip that's currently shown.
            MapViewportProxy.HideToolTip();

            this.Geometry.BeginUpdate();
            this.Geometry.Clear();

            //Regenerate the dictionaries used to quickly determine a type/groups color.
            m_filterTypeColorDictionary = new Dictionary<string, System.Drawing.Color>();
            foreach (SpawnPointFilterItem item in FilterTypes)
            {
                if (m_filterTypeColorDictionary.ContainsKey(item.Name) == false)
                {
                    m_filterTypeColorDictionary.Add(item.Name, System.Drawing.Color.FromArgb(item.Colour.A, item.Colour.R, item.Colour.G, item.Colour.B));
                }
            }

            m_filterGroupColorDictionary = new Dictionary<string, System.Drawing.Color>();
            foreach (SpawnPointGroupItem item in GroupTypes)
            {
                string name = item.Name;
                if (name == null)
                {
                    name = item.DisplayName;
                }

                if (m_filterGroupColorDictionary.ContainsKey(name) == false)
                {
                    m_filterGroupColorDictionary.Add(name, System.Drawing.Color.FromArgb(item.Colour.A, item.Colour.R, item.Colour.G, item.Colour.B));
                }
            }

            m_filterModelTypeColorDictionary = new Dictionary<string, System.Drawing.Color>();
            foreach (SpawnPointFilterItem item in FilterModelTypes)
            {
                if (m_filterModelTypeColorDictionary.ContainsKey(item.Name) == false)
                    m_filterModelTypeColorDictionary.Add(item.Name, System.Drawing.Color.FromArgb(item.Colour.A, item.Colour.R, item.Colour.G, item.Colour.B));
            }

            OverlayImage.BeginUpdate();
            SpawnPointCount = 0;
            ChainedPathsCount = 0;

            SelectedSpawnPoints.Clear();
            SelectedChainingGraphs.Clear();
            SpawnPointInstances.Clear();
            RenderedChainingGraphs.Clear();

            if ( ShowSpawnPoints )
            {
                if ( ShowAll || ShowExported)
                {
                    RenderExportedSpawnPoints();
                }

                if ( ShowAll || ShowMetafile)
                {
                    RenderMetafileSpawnPoints();
                }

                if (ShowSpecific)
                {
                    RenderMetafileSpawnPoints(SpecificMetafile);
                }

                SpawnPointInstances.Sort(new SpawnPointInstanceComparer());

                SpawnPointCount = (uint)SpawnPointInstances.Count;
            }

            if (ShowChainedPaths)
            {
                if (ShowAll || ShowMetafile)
                {
                    RenderMetafilePaths();
                }

                if (ShowSpecific)
                {
                    RenderMetafilePaths(SpecificMetafile);
                }

                ChainedPathsCount = (uint)RenderedChainingGraphs.Count;
            }
            
            OverlayImage.EndUpdate();

            this.Geometry.Add(OverlayImage);
            this.Geometry.EndUpdate();

            EnableUpdateOverlay = false;
        }


        /// <summary>
        /// Helper function for determining if a spawn type is enabled/visible in the overlay.
        /// </summary>
        /// <param name="spawnType"></param>
        /// <returns></returns>
        private bool IsSpawnPointEnabled(ISpawnPoint spawnPoint, ref System.Drawing.Color color)
        {
            if (!FilterByStartTime && !FilterByEndTime && !FilterByType && !FilterByGroup && !FilterByModelType)
                return true;  //No filters are on; it's enabled.

            if (FilterByStartTime)
            {
                if (this.Include24HourScenerios && spawnPoint.StartTime == null)
                {
                    if (this.ShowExactTimeMatch)
                    {
                        return false;
                    }
                }
                else
                {
                    if (spawnPoint.StartTime == null)
                    {
                        return false;
                    }

                    if (this.ShowExactTimeMatch)
                    {
                        if (spawnPoint.StartTime != StartTimeFilter)
                        {
                            return false;
                        }
                    }
                    else
                    {
                        if (spawnPoint.StartTime < StartTimeFilter)
                        {
                            return false;
                        }
                    }
                }
            }

            if (FilterByEndTime)
            {
                if (this.Include24HourScenerios && spawnPoint.EndTime == null)
                {
                    if (this.ShowExactTimeMatch)
                    {
                        return false;
                    }
                }
                else
                {
                    if (spawnPoint.EndTime == null)
                    {
                        return false;
                    }

                    if (this.ShowExactTimeMatch)
                    {
                        if (spawnPoint.EndTime != EndTimeFilter)
                        {
                            return false;
                        }
                    }
                    else
                    {
                        if (spawnPoint.EndTime < EndTimeFilter)
                        {
                            return false;
                        }
                    }
                }
            }

            if (FilterByModelType)
            {
                if (String.IsNullOrWhiteSpace(spawnPoint.ModelSet))
                {
                    return false;
                }
                else
                {
                    if (m_filterModelTypeColorDictionary.ContainsKey(spawnPoint.ModelSet))
                    {
                        color = m_filterModelTypeColorDictionary[spawnPoint.ModelSet];
                        return true;
                    }
                }

                return false;
            }

            if ( FilterByType )
            {
                if (m_filterTypeColorDictionary.ContainsKey(spawnPoint.SpawnType) == true)
                {
                    color = m_filterTypeColorDictionary[spawnPoint.SpawnType];
                }
                else
                {
                    return false;
                }
            }
            
            if ( FilterByGroup )
            {
                if (m_filterGroupColorDictionary.ContainsKey(spawnPoint.SpawnGroup) == true)
                {
                    color = m_filterGroupColorDictionary[spawnPoint.SpawnGroup];
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Helper function for determining if a chained graph should be rendered
        /// </summary>
        /// <param name="spawnType"></param>
        /// <returns></returns>
        private bool IsChainingGraphEnabled(IChainingGraph chainingGraph)
        {
            if (!FilterByStartTime && !FilterByEndTime && !FilterByType && !FilterByGroup && !FilterByModelType)
                return true;  //No filters are on; it's enabled.

            if (FilterByType)
            {
                foreach (SpawnPointFilterItem item in FilterTypes)
                {
                    foreach(IChainingGraphNode chainingGraphNode in chainingGraph.ChainedNodes)
                    {
                        if (String.Compare(item.Name, chainingGraphNode.ScenarioType, true) == 0)
                        {
                            return true;
                        }
                    }                    
                }
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        private void RenderExportedSpawnPoints()
        {
            IMapHierarchy hierarchy = LevelBrowserProxy.Value.SelectedLevel.MapHierarchy;
            System.Drawing.Color spawnTypeColor = c_defaultExportedSpawnPointColor;
            foreach (IMapSection section in hierarchy.AllSections)
            {
                if (section == null || section.ChildEntities == null)
                {
                    continue;
                }
               
                foreach (IEntity entity in section.ChildEntities)
                {
                    this.RenderExportedSpawnPoints(entity, null);
                }
            }
        }

        private void RenderExportedSpawnPoints(IEntity entity, Vector3f location)
        {
            if (entity == null)
            {
                return;
            }

            IArchetype archetype = entity.ReferencedArchetype;
            if (entity.IsInteriorReference)
            {
                IInteriorArchetype interior = archetype as IInteriorArchetype;
                if (interior != null && interior.Rooms != null)
                {
                    foreach (IRoom room in interior.Rooms)
                    {
                        if (room != null && room.ChildEntities != null)
                        {
                            if (location == null)
                            {
                                location = entity.Position;
                            }

                            foreach (IEntity roomEntity in room.ChildEntities)
                            {
                                this.RenderExportedSpawnPoints(roomEntity, location + roomEntity.Position);
                            }
                        }
                    }
                }
            }

            System.Drawing.Color spawnTypeColor = c_defaultExportedSpawnPointColor;
            if (entity.SpawnPoints != null)
            {
                foreach (ISpawnPoint sp in entity.SpawnPoints)
                {
                    spawnTypeColor = c_defaultExportedSpawnPointColor;
                    if (!IsSpawnPointEnabled(sp, ref spawnTypeColor))
                    {
                        continue;
                    }

                    if (location == null)
                    {
                        location = entity.Position;
                    }

                    location += sp.Position;
                    SpawnPointInstances.Add(new SpawnPointInstance(sp, location, entity));
                    OverlayImage.RenderCircle(location, m_spawnPointSize, spawnTypeColor);
                }
            }

            ISimpleMapArchetype simple = archetype as ISimpleMapArchetype;
            if (simple != null && simple.SpawnPoints != null)
            {
                foreach (ISpawnPoint sp in simple.SpawnPoints)
                {
                    spawnTypeColor = c_defaultExportedSpawnPointColor;
                    if (!IsSpawnPointEnabled(sp, ref spawnTypeColor))
                    {
                        continue;
                    }

                    if (location == null)
                    {
                        location = entity.Position;
                    }

                    location += sp.Position;
                    SpawnPointInstances.Add(new SpawnPointInstance(sp, location, entity));
                    OverlayImage.RenderCircle(location, m_spawnPointSize, spawnTypeColor);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="color"></param>
        private void RenderMetafileSpawnPoints()
        {
            foreach (ISpawnPoint sp in LevelBrowserProxy.Value.SelectedLevel.SpawnPoints)
            {
                System.Drawing.Color spawnTypeColor = c_defaultMetafileSpawnPointColor;
                if ( IsSpawnPointEnabled(sp, ref spawnTypeColor))
                {
                    SpawnPointInstances.Add(new SpawnPointInstance(sp, sp.Position, null));
                    OverlayImage.RenderCircle(sp.Position, m_spawnPointSize, spawnTypeColor);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private void RenderMetafilePaths()
        {
            foreach (IChainingGraph chainingGraph in LevelBrowserProxy.Value.SelectedLevel.ChainingGraphs)
            {
                if (IsChainingGraphEnabled(chainingGraph) == true)
                {
                    RenderChainingGraph(chainingGraph, OverlayImage, null);
                    RenderedChainingGraphs.Add(chainingGraph);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="color"></param>
        private void RenderMetafilePaths(string file)
        {
            foreach (IChainingGraph chainingGraph in LevelBrowserProxy.Value.SelectedLevel.ChainingGraphs)
            {
                if (Path.GetFileName(chainingGraph.SourceFile) == file && IsChainingGraphEnabled(chainingGraph) == true)
                {
                    RenderChainingGraph(chainingGraph, OverlayImage, null);
                    RenderedChainingGraphs.Add(chainingGraph);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="color"></param>
        private void RenderMetafileSpawnPoints(string file)
        {
            foreach (ISpawnPoint sp in LevelBrowserProxy.Value.SelectedLevel.SpawnPoints)
            {
                if (Path.GetFileName(sp.SourceFile) == file)
                {
                    System.Drawing.Color spawnTypeColor = c_defaultMetafileSpawnPointColor;
                    if ( IsSpawnPointEnabled(sp, ref spawnTypeColor))
                    {
                        SpawnPointInstances.Add(new SpawnPointInstance(sp, sp.Position, null));
                        OverlayImage.RenderCircle(sp.Position, m_spawnPointSize, spawnTypeColor);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sp"></param>
        private void RenderSpawnPoint(ISpawnPoint sp)
        {
            System.Drawing.Color spawnTypeColor = c_defaultMetafileSpawnPointColor;
            if (IsSpawnPointEnabled(sp, ref spawnTypeColor))
            {
                OverlayImage.RenderCircle(sp.Position, m_spawnPointSize, spawnTypeColor);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="chainingGraph"></param>
        private void RenderChainingGraph(IChainingGraph chainingGraph, Viewport2DImageOverlay imageOverlay, Nullable<System.Drawing.Color> overrideColor)
        {
            System.Drawing.Color chainingGraphColor = c_defaultMetafileChainingGraphColor;
            foreach (LinkedList<IChainingGraphNode> chainedPathList in chainingGraph.ChainedPaths)
            {
                LinkedListNode<IChainingGraphNode> currentNode = chainedPathList.First;

                do
                {
                    if ( overrideColor.HasValue )
                        chainingGraphColor = overrideColor.Value;
                    else if (m_filterTypeColorDictionary.ContainsKey(currentNode.Value.ScenarioType) == true)
                        chainingGraphColor = m_filterTypeColorDictionary[currentNode.Value.ScenarioType];

                    imageOverlay.RenderCircle(currentNode.Value.Position, m_spawnPointSize, chainingGraphColor);

                    if (currentNode.Value.HasOutgoingEdges == true && currentNode.Next != null)
                    {
                        Vector3f[] points = new Vector3f[2];
                        points[0] = currentNode.Value.Position;
                        points[1] = currentNode.Next.Value.Position;
                        imageOverlay.RenderLines(points, chainingGraphColor, PathLineWidth);
                    }

                    currentNode = currentNode.Next;
                } while (currentNode != null);
            }
        }

        /// <summary>
        /// Sets all widgets for this overlay.
        /// </summary>
        private void InitializeWidgets()
        {
            if (ProxyService.Value.IsProxyConnected)
            {
                if (ProxyService.Value.Console.WidgetExists(RAG_CREATE_SCENARIO_WIDGETS))
                    ProxyService.Value.Console.PressWidgetButton(RAG_CREATE_SCENARIO_WIDGETS);

                ProxyService.Value.Console.WriteBoolWidget(RAG_ENABLE_SCENARIO_POINT_EDITING, true);
                ProxyService.Value.Console.WriteBoolWidget(RAG_RENDER_SCENARIO_POINTS, true);
            }
        }

        #endregion // Private Methods
    } // SpawnPointOverlay
}
