﻿using System;
using System.Collections.Generic;
using RSG.Model.Common.Map;

namespace Workbench.AddIn.MapStatistics.Overlays
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(ExtensionPoints.CostStatisticsOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class TotalCostOverlay : StatisticOverlayBase
    {
        #region Constants
        private const string c_name = "Total Cost";
        private const string c_description = "The total exported data size in bytes of the map container.  This includes the cost of the entities and texture dictionaries. Use the options below to change what gets included in the maps output.";
        #endregion // Constants

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public TotalCostOverlay()
            : base(c_name, c_description)
        {
            m_dontShowUniqueOptions = true;
            m_dontShowInteriorOptions = true;
            m_includeDynamic = true;
            m_includeNonDynamic = true;
        }
        #endregion // Constructor

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        protected double GetStatisticValue(IMapSection section)
        {
            if (!this.IncludeDynamic && !this.IncludeNonDynamic)
                return 0.0;
            if (!this.IncludeDrawableEntities && !this.IncludeNonDrawableEntities)
                return 0.0;

            StatEntityType type = StatEntityType.Total;
            if (this.IncludeDynamic && !this.IncludeNonDynamic)
            {
                type = StatEntityType.Dynamic;
            }
            else if (!this.IncludeDynamic && this.IncludeNonDynamic)
            {
                type = StatEntityType.NonDynamic;
            }

            double stat = 0.0;
            if (this.IncludeDrawableEntities && !this.IncludeNonDrawableEntities)
            {
                stat = AddStatistic(section, StatGroup.DrawableEntities, type);
            }
            else if (!this.IncludeDrawableEntities && this.IncludeNonDrawableEntities)
            {
                stat = AddStatistic(section, StatGroup.NonDrawableEntities, type);
            }
            else
            {
                stat = AddStatistic(section, StatGroup.AllNonInteriorEntities, type);
            }
            return stat;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stat"></param>
        /// <param name="stats"></param>
        /// <param name="group"></param>
        /// <param name="type"></param>
        private uint AddStatistic(IMapSection section, StatGroup group, StatEntityType type)
        {
            uint stat = 0;

            if (this.IncludeHighDetailed)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Hd, type)];
                stat += stats.TXDCost + stats.DrawableCost;
            }
            if (this.IncludeLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Lod, type)];
                stat += stats.TXDCost + stats.DrawableCost;
            }
            if (this.IncludeSuperLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.SLod1, type)];
                stat += stats.TXDCost + stats.DrawableCost;
            }

            return stat;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="propgroup"></param>
        /// <returns></returns>
        protected override double GetStatisticValue(IMapSection section, IMapSection propgroup)
        {
            double stat = 0;
            if (section != null)
                stat += GetStatisticValue(section);
            if (propgroup != null)
                stat += GetStatisticValue(propgroup);

            return stat;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sectionStat"></param>
        /// <returns></returns>
        protected override String GeometryUserText(KeyValuePair<double, IMapSection> sectionStat)
        {
            return StatisticsUtil.SizeToString((ulong)sectionStat.Key);
        }
        #endregion // Overrides
    } // TotalCostOverlay

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(ExtensionPoints.CostStatisticsOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class InstanceCostOverlay : StatisticOverlayBase
    {
        #region Constants
        private const string c_name = "Entity Cost";
        private const string c_description = "";
        #endregion // Constants
        
        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public InstanceCostOverlay()
            : base(c_name, c_description)
        {
            m_dontShowUniqueOptions = true;
            m_dontShowInteriorOptions = true;
            m_includeDynamic = true;
            m_includeNonDynamic = true;
        }
        #endregion // Constructor

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        protected double GetStatisticValue(IMapSection section)
        {
            if (!this.IncludeDynamic && !this.IncludeNonDynamic)
                return 0.0;
            if (!this.IncludeDrawableEntities && !this.IncludeNonDrawableEntities)
                return 0.0;

            StatEntityType type = StatEntityType.Total;
            if (this.IncludeDynamic && !this.IncludeNonDynamic)
            {
                type = StatEntityType.Dynamic;
            }
            else if (!this.IncludeDynamic && this.IncludeNonDynamic)
            {
                type = StatEntityType.NonDynamic;
            }

            double stat = 0.0;
            if (this.IncludeDrawableEntities && !this.IncludeNonDrawableEntities)
            {
                stat = AddStatistic(section, StatGroup.DrawableEntities, type);
            }
            else if (!this.IncludeDrawableEntities && this.IncludeNonDrawableEntities)
            {
                stat = AddStatistic(section, StatGroup.NonDrawableEntities, type);
            }
            else
            {
                stat = AddStatistic(section, StatGroup.AllNonInteriorEntities, type);
            }
            return stat;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stat"></param>
        /// <param name="stats"></param>
        /// <param name="group"></param>
        /// <param name="type"></param>
        private uint AddStatistic(IMapSection section, StatGroup group, StatEntityType type)
        {
            uint stat = 0;

            if (this.IncludeHighDetailed)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Hd, type)];
                stat += stats.DrawableCost;
            }
            if (this.IncludeLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Lod, type)];
                stat += stats.DrawableCost;
            }
            if (this.IncludeSuperLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.SLod1, type)];
                stat += stats.DrawableCost;
            }

            return stat;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="propgroup"></param>
        /// <returns></returns>
        protected override double GetStatisticValue(IMapSection section, IMapSection propgroup)
        {
            double stat = 0;
            if (section != null)
                stat += GetStatisticValue(section);
            if (propgroup != null)
                stat += GetStatisticValue(propgroup);

            return stat;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sectionStat"></param>
        /// <returns></returns>
        protected override String GeometryUserText(KeyValuePair<double, IMapSection> sectionStat)
        {
            return StatisticsUtil.SizeToString((ulong)sectionStat.Key);
        }
        #endregion // Overrides
    } // GeometryCostOverlay

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(ExtensionPoints.CostStatisticsOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class TextureDictionaryCostOverlay : StatisticOverlayBase
    {
        #region Constants
        private const string c_name = "Texture Dictionary Cost";
        private const string c_description = "The total exported data size in bytes of the map section. This includes the cost of the geometry and texture dictionaries." +
                " Use the options below to change what gets included in the maps output and see the table below for a more detailed view of the selected maps statistics.";
        #endregion // Constants

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public TextureDictionaryCostOverlay()
            : base(c_name, c_description)
        {
            m_dontShowUniqueOptions = true;
            m_dontShowInteriorOptions = true;
            m_includeDynamic = true;
            m_includeNonDynamic = true;
        }
        #endregion // Constructor

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        protected double GetStatisticValue(IMapSection section)
        {
            if (!this.IncludeDynamic && !this.IncludeNonDynamic)
                return 0.0;
            if (!this.IncludeDrawableEntities && !this.IncludeNonDrawableEntities)
                return 0.0;

            StatEntityType type = StatEntityType.Total;
            if (this.IncludeDynamic && !this.IncludeNonDynamic)
            {
                type = StatEntityType.Dynamic;
            }
            else if (!this.IncludeDynamic && this.IncludeNonDynamic)
            {
                type = StatEntityType.NonDynamic;
            }

            double stat = 0.0;
            if (this.IncludeDrawableEntities && !this.IncludeNonDrawableEntities)
            {
                stat = AddStatistic(section, StatGroup.DrawableEntities, type);
            }
            else if (!this.IncludeDrawableEntities && this.IncludeNonDrawableEntities)
            {
                stat = AddStatistic(section, StatGroup.NonDrawableEntities, type);
            }
            else
            {
                stat = AddStatistic(section, StatGroup.AllNonInteriorEntities, type);
            }
            return stat;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stat"></param>
        /// <param name="stats"></param>
        /// <param name="group"></param>
        /// <param name="type"></param>
        private uint AddStatistic(IMapSection section, StatGroup group, StatEntityType type)
        {
            uint stat = 0;

            if (this.IncludeHighDetailed)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Hd, type)];
                stat += stats.TXDCost;
            }
            if (this.IncludeLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Lod, type)];
                stat += stats.TXDCost;
            }
            if (this.IncludeSuperLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.SLod1, type)];
                stat += stats.TXDCost;
            }

            return stat;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="propgroup"></param>
        /// <returns></returns>
        protected override double GetStatisticValue(IMapSection section, IMapSection propgroup)
        {
            double stat = 0;
            if (section != null)
                stat += GetStatisticValue(section);
            if (propgroup != null)
                stat += GetStatisticValue(propgroup);

            return stat;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sectionStat"></param>
        /// <returns></returns>
        protected override String GeometryUserText(KeyValuePair<double, IMapSection> sectionStat)
        {
            return StatisticsUtil.SizeToString((ulong)sectionStat.Key);
        }
        #endregion // Overrides
    } // TextureDictionaryCostOverlay
} // Workbench.AddIn.MapStatistics.Overlays