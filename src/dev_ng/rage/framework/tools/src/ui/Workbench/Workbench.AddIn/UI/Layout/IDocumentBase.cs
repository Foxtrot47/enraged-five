﻿using System;
using System.ComponentModel.Composition;
using System.Windows;
using System.Windows.Data;
using System.Windows.Controls;
using System.ComponentModel;
using System.Collections.Generic;
using System.Windows.Media;
using RSG.Base.Editor;
using AvalonDock;
using Workbench.AddIn.Services;
using Workbench.AddIn.Services.File;
using System.Text.RegularExpressions;

namespace Workbench.AddIn.UI.Layout
{
    /// <summary>
    /// You can use this class as the base class to any document
    /// view. This class inherits from document content so that the view
    /// is a ContentControl and can be used directly as the content. Use this
    /// class if your view needs a viewmodel property that is a viewmodelbase.
    /// Using this class makes sure that the viewmodel is bounded to the
    /// data content and it means you have a property (ViewModel) in you view that
    /// you can use without casting
    /// </summary>
    public class DocumentBase<T> : IDocumentBase where T : IModel
    {
        #region Properties

        /// <summary>
        /// The view model that is to act as the data content for the tool
        /// window
        /// </summary>
        public T ViewModel
        {
            get { return m_viewModel; }
            set { m_viewModel = value; RaisePropertyChanged("ViewModel"); }
        }
        private T m_viewModel;

        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Default Constructor
        /// </summary>
        public DocumentBase()
        {
        }

        /// <summary>
        /// Creates the tool window base object with the given
        /// name and view model and makes sure that the
        /// data content is bound to this view model
        /// </summary>
        public DocumentBase(String name, T viewModel)
        {
            this.Name = name;
            this.ViewModel = viewModel;
            this.SaveModel = viewModel;
            Binding binding = new Binding();
            binding.Source = this;
            binding.Path = new PropertyPath("ViewModel");
            binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;

            this.SetBinding(ToolWindowBase<T>.DataContextProperty, binding);

            Binding modifiedBinding = new Binding();
            modifiedBinding.Source = viewModel;
            modifiedBinding.Path = new PropertyPath("IsModified");
            modifiedBinding.Mode = BindingMode.TwoWay;
            modifiedBinding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;

            this.SetBinding(ToolWindowBase<T>.IsModifiedProperty, modifiedBinding);
        }

        #endregion // Constructors

        #region Event Handler Functions

        /// <summary>
        /// Gets called when the document is about to be opened
        /// </summary>
        public override void OnOpening(Object sender, EventArgs e) { }

        /// <summary>
        /// Gets called when the document has just opened
        /// </summary>
        public override void OnOpened(Object sender, EventArgs e) { }

        /// <summary>
        /// Gets called when the document is about to close
        /// </summary>
        public override void OnClosing(Object sender, CancelEventArgs e) { }

        /// <summary>
        /// Gets called when the document has just closed
        /// </summary>
        public override void OnClosed(Object sender, EventArgs e) { }

        /// <summary>
        /// Gets called when the document has just finished
        /// being created
        /// </summary>
        public override void OnCreated(Object sender, EventArgs e) { }

        /// <summary>
        /// Window receive focus handler.
        /// </summary>
        public override void OnFocus(Object sender, EventArgs e) { }

        /// <summary>
        /// Window lose focus handler.
        /// </summary>
        public override void OnLostFocus(Object sender, EventArgs e) { }

        #endregion // Event Handler Functions
    }


    public class IDocumentBase : DocumentContent, IContentBase
    {
        #region Events
        /// <summary>
        /// 
        /// </summary>
        public event ContentSelectionChangedEventHandler SelectionChanged;
        #endregion

        #region Properties
        /// <summary>
        /// Unique window name; used for layout serialisation (alpha-numeric only).
        /// </summary>
        public new String Name
        {
            get { return base.Name; }
            set { base.Name = value; }
        }

        /// <summary>
        /// Window title for UI display.
        /// </summary>
        public new String Title
        {
            get { return base.Title; }
            set { base.Title = value; }
        }

        /// <summary>
        /// Window icon for UI display.
        /// </summary>
        public new ImageSource Icon
        {
            get { return base.Icon; }
            set { base.Icon = value; }
        }

        /// <summary>
        /// Represents whether this document has been
        /// modified by the user.
        /// </summary>
        public new Boolean IsModified
        {
            get { return base.IsModified; }
            set { base.IsModified = value; }
        }

        /// <summary>
        /// Represents whether this document is
        /// currently in a readonly state. This basically
        /// means whether the file the document is
        /// representing has the readonly attribute set.
        /// </summary>
        public new Boolean IsReadOnly
        {
            get { return base.IsReadOnly; }
            set { base.IsReadOnly = value; }
        }

        /// <summary>
        /// This is the path to the file that the
        /// document is representing, this is so
        /// that the IsReadOnly property can be updated
        /// and set
        /// </summary>
        public new String Path
        {
            get { return base.Path; }
            set { base.Path = value; }
        }

        /// <summary>
        /// ExtensionService GUID.
        /// </summary>
        public Guid ID
        {
            get { return m_ID; }
            protected set
            {
                m_ID = value;
            }
        }
        private Guid m_ID = Guid.Empty;

        /// <summary>
        /// 
        /// </summary>
        public IModel SaveModel
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public Guid RelativeID
        {
            get { return m_eRelativeID; }
            protected set
            {
                m_eRelativeID = value;
            }
        }
        private Guid m_eRelativeID = Guid.Empty;

        /// <summary>
        /// 
        /// </summary>
        public Direction Direction
        {
            get { return m_eDirection; }
            protected set
            {
                m_eDirection = value;
            }
        }
        private Direction m_eDirection = Direction.After;

        /// <summary>
        /// The open service that was used to create this document.
        /// </summary>
        public IOpenService OpenService
        {
            get { return m_openService; }
            set { m_openService = value; }
        }
        private IOpenService m_openService;

        /// <summary>
        /// 
        /// </summary>
        private List<object> CurrentSelection
        {
            get;
            set;
        }

        /// <summary>
        /// Generic data associated with the document
        /// </summary>
        public object DocumentData
        {
            get;
            set;
        }
        #endregion // Properties

        #region Event Handler Functions

        /// <summary>
        /// Gets called when the document is about to be opened
        /// </summary>
        public virtual void OnOpening(Object sender, EventArgs e) { }

        /// <summary>
        /// Gets called when the document has just opened
        /// </summary>
        public virtual void OnOpened(Object sender, EventArgs e) { }

        /// <summary>
        /// Gets called when the document is about to close
        /// </summary>
        public virtual void OnClosing(Object sender, CancelEventArgs e) { }

        /// <summary>
        /// Gets called when the document has just closed
        /// </summary>
        public virtual void OnClosed(Object sender, EventArgs e) { }

        /// <summary>
        /// Gets called when the document has just finished
        /// being created
        /// </summary>
        public virtual void OnCreated(Object sender, EventArgs e) { }

        /// <summary>
        /// Window receive focus handler.
        /// </summary>
        public virtual void OnFocus(Object sender, EventArgs e) { }

        /// <summary>
        /// Window lose focus handler.
        /// </summary>
        public virtual void OnLostFocus(Object sender, EventArgs e) { }

        #endregion // Event Handler Functions

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="previousResult"></param>
        /// <returns></returns>
        public virtual object FindNext(ISearchQuery query, object previousResult)
        {
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="previousResult"></param>
        /// <returns></returns>
        public virtual object FindPrevious(ISearchQuery query, object previousResult)
        {
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public virtual List<ISearchResult> FindAll(ISearchQuery query)
        {
            return new List<ISearchResult>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="result"></param>
        public virtual void SelectSearchResult(ISearchResult result)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newSelection"></param>
        protected void OnSelectionChanged(List<object> selection)
        {
            var handler = this.SelectionChanged;
            if (handler == null)
                return;

            List<object> newItems = new List<object>();
            List<object> oldItems = new List<object>();

            if (this.CurrentSelection == null)
            {
                this.CurrentSelection = new List<object>(selection);
                newItems.AddRange(selection);
            }
            else
            {
                foreach (var obj in selection)
                {
                    if (!this.CurrentSelection.Contains(obj))
                    {
                        newItems.Add(obj);
                    }
                }
                foreach (var obj in this.CurrentSelection)
                {
                    if (!selection.Contains(obj))
                    {
                        oldItems.Add(obj);
                    }
                }
                this.CurrentSelection = selection;
            }

            handler(this, new ContentSelectionChangedEventArgs(newItems, oldItems, selection));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual List<object> GetCurrentSelection()
        {
            return new List<object>();
        }
        #endregion
    } // IDocumentBase
}
