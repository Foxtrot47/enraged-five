﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn.Services;

namespace Workbench.AddIn.Automation
{
    /// <summary>
    /// Automation monitor settings.
    /// </summary>
    [ExportExtension(Workbench.AddIn.Automation.AutomationCompositionPoints.AutomationMonitorSettings, typeof(IAutomationMonitorSettings))]
    public class AutomationMonitorSettings : SettingsBase, IAutomationMonitorSettings
    {
        /// <summary>
        /// The last used service. Includes the friendly name and the Uri.
        /// </summary>
        public string LastUsedService
        {
            get
            {
                return Workbench.AddIn.Automation.Properties.Settings.Default.LastUsedService;
            }
            set
            {
                Workbench.AddIn.Automation.Properties.Settings.Default.LastUsedService = value;
            }
        }

        #region ISettings methods

        /// <summary>
        /// Validate the settings. Always returns true.
        /// </summary>
        /// <param name="errors">Errors.</param>
        /// <returns>Always returns true.</returns>
        public bool Validate(List<string> errors)
        {
            return true;
        }

        /// <summary>
        /// Load the settings. Does nothing in this implementation.
        /// </summary>
        public void Load()
        {
            
        }

        /// <summary>
        /// Apply the settings.
        /// </summary>
        public void Apply()
        {
            Properties.Settings.Default.Save();
        }

        #endregion
    }
}
