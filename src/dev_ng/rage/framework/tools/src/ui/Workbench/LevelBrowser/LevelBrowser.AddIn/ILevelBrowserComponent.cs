﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Base.Editor;
using RSG.Base.ConfigParser;

namespace LevelBrowser.AddIn
{
    /// <summary>
    /// The main interface used to create new top level
    /// nodes for the level browser. These nodes get places
    /// just after the level node and are responisible for 
    /// creating the hierarchy beneath them.
    /// </summary>
    public interface ILevelBrowserComponent
    {
        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="levels"></param>
        void OnRefresh(ILevelCollection levels);
        
        /// <summary>
        /// Returns all of the paths that need checking and syncing before the user can start up the
        /// level browser.
        /// </summary>
        String[] GetFilesForSync(ConfigGameView gv);
        #endregion // Methods
    } // ILevelBrowserComponent
} // LevelBrowser.AddIn
