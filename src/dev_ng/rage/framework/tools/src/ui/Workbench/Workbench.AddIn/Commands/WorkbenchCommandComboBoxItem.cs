﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Workbench.AddIn.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public class WorkbenchCommandComboBoxItem : WorkbenchMenuItemBase
    {
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public WorkbenchCommandComboBoxItem()
        {
            this.IsLabel = true;
        }
        #endregion // Constructor(s)

        #region ICommand Implementation
        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
        }
        #endregion // ICommand Implementation
    } // WorkbenchCommandComboBoxItem
}
