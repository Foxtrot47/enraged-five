﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.LiveEditing;

namespace MetadataEditor.AddIn
{
    /// <summary>
    /// Service that provides the live link rest services
    /// </summary>
    public interface ILiveEditingService
    {
        #region Events
        /// <summary>
        /// Event that gets fired when the root service has changed
        /// </summary>
        event EventHandler RootServiceChanged;
        #endregion // Events

        #region Properties
        /// <summary>
        /// The root live editing service of the game that we are currently connected to
        /// </summary>
        IDirectoryService LiveEditRootService { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Force a refresh of the root service
        /// </summary>
        void Refresh();

        /// <summary>
        /// Returns the object service for a particular file (if it exists)
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        IObjectService GetLiveEditingServiceForFile(string filePath);
        #endregion // Methods
    } // ILiveEditingService
}
