﻿using System;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn.UI.Controls;
using RSG.Base.Windows.Controls;

namespace Workbench.AddIn.Commands
{
    /// <summary>
    /// The base class for all menu items in the workbench
    /// </summary>
    public abstract class WorkbenchMenuItemBase : WorkbenchCommandControlBase, IWorkbenchCommand
    {
        #region Members

        private String m_header;
        private IEnumerable<IWorkbenchCommand> m_items;
        private BitmapImage m_imageSource;
        private Boolean m_imageValid;
        private Boolean m_isCheckable;
        private Boolean m_isChecked;
        private Boolean m_isToggle;
        private Boolean m_isSeparator;
        private Boolean m_isLabel;
        private Boolean m_isImage;
        private Boolean m_isCombobox;
        private Boolean m_isSubmenuOpen;
        private Object m_context;
        private KeyGesture m_keyGesture;
        private Guid m_relativeToolBarID;
        private Boolean m_isDefault;

        #endregion // Members

        #region Properties

        /// <summary>
        /// The text that will be displayed in the menu item or
        /// next to a checkable toolbar item.
        /// </summary>
        public String Header
        {
            get { return m_header; }
            set
            {
                if (m_header == value)
                    return;

                SetPropertyValue(value, () => this.Header,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue) 
                        {
                            m_header = (String)newValue;
                        }
                    )
                );
            }
        }

        /// <summary>
        /// The child commands to this one. Any child commands will be
        /// set as a sub menu to a menu item.
        /// </summary>
        public IEnumerable<IWorkbenchCommand> Items
        {
            get { return m_items; }
            set
            {
                if (m_items == value)
                    return;

                SetPropertyValue(value, () => this.Items,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_items = (IEnumerable<IWorkbenchCommand>)newValue;
                        }
                    )
                );
            }
        }

        /// <summary>
        /// The image to use if any in the menu item and
        /// the toolbar button
        /// </summary>
        public Image Image
        {
            get 
            {
                GreyableImage image = new GreyableImage();
                if (this.ImageSource != null)
                    image.Source = this.ImageSource;
                return image;
            }
        }

        /// <summary>
        /// The source for the image
        /// </summary>
        public BitmapImage ImageSource
        {
            get { return m_imageSource; }
            set
            {
                if (m_imageSource == value)
                    return;

                SetPropertyValue(value, () => this.ImageSource,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_imageSource = (BitmapImage)newValue;
                        }
                    )
                );

                if (value != null)
                    this.ImageValid = true;
                else
                    this.ImageValid = false;

                OnPropertyChanged("Image");
            }
        }

        /// <summary>
        /// Represents whether the image property is valid to show
        /// </summary>
        public Boolean ImageValid
        {
            get { return m_imageValid; }
            set
            {
                if (m_imageValid == value)
                    return;

                SetPropertyValue(value, () => this.ImageValid,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_imageValid = (Boolean)newValue;
                        }
                    )
                );
            }
        }

        /// <summary>
        /// Set to true if this command can have a checkable state. For a menu item
        /// this adds a checkbox to it and for a toolbar the command gets drawn as a checkbox
        /// with the header to the right.
        /// </summary>
        public Boolean IsCheckable
        {
            get { return m_isCheckable; }
            set
            {
                if (m_isCheckable == value)
                    return;

                SetPropertyValue(value, () => this.IsCheckable,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_isCheckable = (Boolean)newValue;
                        }
                    )
                );
            }
        }

        /// <summary>
        /// A boolean property that represents whether this command is currently checked. The
        /// default value is false.
        /// </summary>
        public Boolean IsChecked
        {
            get { return m_isChecked; }
            set
            {
                if (m_isChecked == value)
                    return;

                SetPropertyValue(value, () => this.IsChecked,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_isChecked = (Boolean)newValue;
                        }
                    )
                );
            }
        }

        /// <summary>
        /// When set means that this is should be represented as a toggle button in
        /// a tool bar
        /// </summary>
        public Boolean IsToggle
        {
            get { return m_isToggle; }
            set
            {
                if (m_isToggle == value)
                    return;

                SetPropertyValue(value, () => this.IsToggle,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_isToggle = (Boolean)newValue;
                        }
                    )
                );
            }
        }

        /// <summary>
        /// Set to true if this command actual represents a separator in a command control (i.e menu, toolbar)
        /// </summary>
        public Boolean IsSeparator
        {
            get { return m_isSeparator; }
            set
            {
                if (m_isSeparator == value)
                    return;

                SetPropertyValue(value, () => this.IsSeparator,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_isSeparator = (Boolean)newValue;
                        }
                    )
                );
            }
        }

        /// <summary>
        /// Set to true if this command actual represents a label in a command control (i.e menu, toolbar)
        /// </summary>
        public Boolean IsLabel
        {
            get { return m_isLabel; }
            set
            {
                if (m_isLabel == value)
                    return;

                SetPropertyValue(value, () => this.IsLabel,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_isLabel = (Boolean)newValue;
                        }
                    )
                );
            }
        }

        /// <summary>
        /// Set to true if this command actual represents an image in a command control (i.e menu, toolbar)
        /// </summary>
        public Boolean IsImage
        {
            get { return m_isImage; }
            set
            {
                if (m_isImage == value)
                    return;

                SetPropertyValue(value, () => this.IsImage,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_isImage = (Boolean)newValue;
                        }
                    )
                );
            }
        }

        /// <summary>
        /// Set to true if this command actual represents a combobox in a command control (i.e menu, toolbar)
        /// </summary>
        public Boolean IsCombobox
        {
            get { return m_isCombobox; }
            set
            {
                if (m_isCombobox == value)
                    return;

                SetPropertyValue(value, () => this.IsCombobox,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_isCombobox = (Boolean)newValue;
                        }
                    )
                );
            }
        }

        /// <summary>
        /// Represents whether the submenu to this command is
        /// open (only used for menu items)
        /// </summary>
        public Boolean IsSubmenuOpen
        {
            get { return m_isSubmenuOpen; }
            set
            {
                if (m_isSubmenuOpen == value)
                    return;

                SetPropertyValue(value, () => this.IsSubmenuOpen,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_isSubmenuOpen = (Boolean)newValue;
                        }
                    )
                );

                OnIsSubmenuOpenChanged();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Object Context
        {
            get { return m_context; }
            set
            {
                if (m_context == value)
                    return;

                SetPropertyValue(value, () => this.Context,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_context = newValue;
                        }
                    )
                );
            }
        }

        /// <summary>
        /// The keybinding that is placed on this command.
        /// This binding is added to the applications bindings.
        /// (Only used if this is a main menu item)
        /// </summary>
        public KeyGesture KeyGesture
        {
            get { return m_keyGesture; }
            set { m_keyGesture = value; }
        }

        /// <summary>
        /// The Guid that is the relative to this
        /// command if it is in a toolbar
        /// </summary>
        public Guid RelativeToolBarID 
        {
            get { return m_relativeToolBarID; }
            set { m_relativeToolBarID = value; }
        }

        /// <summary>
        /// Used when this command is in a list of other commands
        /// </summary>
        public Boolean IsDefault
        {
            get { return m_isDefault; }
            set
            {
                if (m_isDefault == value)
                    return;

                SetPropertyValue(value, () => this.IsDefault,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_isDefault = (Boolean)newValue;
                        }
                    )
                );
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string CustomDataTemplateName
        {
            get;
            protected set;
        }

        /// <summary>
        /// Custom data template for combo box items that are selected.
        /// </summary>
        public string CustomSelectedDataTemplateName
        {
            get;
            protected set;
        }

        #endregion // Properties

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public WorkbenchMenuItemBase()
            : base(null, null)
        {
        }

        /// <summary>
        /// Creates a menu item with just a execute method
        /// </summary>
        public WorkbenchMenuItemBase(Action<Object> executeMethod)
            : base(null, executeMethod)
        {
        }

        /// <summary>
        /// Creates a command control with both a can execute and a execute method
        /// </summary>
        public WorkbenchMenuItemBase(Predicate<Object> canExecuteMethod, Action<Object> executeMethod)
            : base(canExecuteMethod, executeMethod)
        {
        }

        #endregion // Constructor

        #region Protected Methods

        /// <summary>
        /// This is a helper function so you can assign the Icon directly
        /// from a Bitmap, such as one from a resources file.
        /// </summary>
        /// <param name="value"></param>
        protected void SetImageFromBitmap(System.Drawing.Bitmap value)
        {
            if (value != null)
            {
                BitmapSource source = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                    value.GetHbitmap(),
                    IntPtr.Zero,
                    System.Windows.Int32Rect.Empty,
                    System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions());

                PngBitmapEncoder encoder = new PngBitmapEncoder();
                System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
                this.ImageSource = new BitmapImage();

                encoder.Frames.Add(BitmapFrame.Create(source));
                encoder.Save(memoryStream);

                this.ImageSource.BeginInit();
                this.ImageSource.StreamSource = new System.IO.MemoryStream(memoryStream.ToArray());
                this.ImageSource.EndInit();
                memoryStream.Close();
            }
        }

        /// <summary>
        /// Gets called whenever the sub menu open state changes
        /// </summary>
        protected virtual void OnIsSubmenuOpenChanged()
        {
        }

        #endregion // Protected Methods
    } // WorkbenchMenuItemBase
} // Workbench.AddIn.Commands
