﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;

namespace Workbench.Services.Perforce
{
    /// <summary>
    /// Defines a single perforce item that can be used
    /// to sync to.
    /// </summary>
    public class PerforceItem : ModelBase
    {
        #region Members

        private Boolean m_syncItem;
        private String m_depotFile;
        private int m_fileSize;

        #endregion // Members

        #region Properties

        /// <summary>
        /// Represents where this item should be synced or not
        /// </summary>
        public Boolean SyncItem
        {
            get { return m_syncItem; }
            set
            {
                SetPropertyValue(value, () => this.SyncItem,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_syncItem = (Boolean)newValue;
                        }
                    )
                );
            }
        }

        /// <summary>
        /// The filename to this perforce
        /// sync item.
        /// </summary>
        public String DepotFile
        {
            get { return m_depotFile; }
            private set
            {
                SetPropertyValue(value, () => this.DepotFile,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_depotFile = (String)newValue;
                        }
                    )
                );
            }
        }

        /// <summary>
        /// The size of the file as reported by
        /// the perforce record else as reported
        /// by the .Net framework
        /// </summary>
        public int FileSize
        {
            get { return m_fileSize; }
            private set
            {
                SetPropertyValue(value, () => this.FileSize,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_fileSize = (int)newValue;
                        }
                    )
                );
            }
        }

        #endregion // Properties

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public PerforceItem(P4API.P4Record record)
        {
            this.SyncItem = true;
            this.DepotFile = String.Empty;
            Debug.Assert(record.Fields.ContainsKey("depotFile"),
                         "Unable to create perforce item from record that doesn't contain a depotFile value!");

            if (record.Fields.ContainsKey("depotFile"))
            {
                this.DepotFile = record["depotFile"];

                if (record.Fields.ContainsKey("fileSize"))
                {
                    int size = 0;
                    String fileSize = record["fileSize"];
                    if (int.TryParse(fileSize, out size))
                    {
                        this.FileSize = size;
                    }
                    else
                    {
                        System.IO.FileInfo fileInfo = new System.IO.FileInfo(this.DepotFile);
                        this.FileSize = (int)fileInfo.Length;
                    }
                }
                else
                {
                    System.IO.FileInfo fileInfo = new System.IO.FileInfo(this.DepotFile);
                    if (fileInfo.Exists == true)
                        this.FileSize = (int)fileInfo.Length;
                    else
                        this.FileSize = 0;
                }
            }
        }

        #endregion // Constructor
    } // PerforceItem
} // Workbench.Services.Perforce
