﻿using System;
using System.Windows.Media;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Windows.Controls.WpfViewport2D;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using RSG.Base.Math;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using Workbench.AddIn.Bugstar.OverlayViews;
using RSG.Interop.Bugstar;
using RSG.Base.Logging;
using System.IO;
using System.Xml;
using System.Collections.Specialized;
using RSG.Base.Windows.Controls.WpfViewport2D.Controls;
using RSG.Interop.Bugstar.Organisation;
using Workbench.AddIn.Services;
using RSG.Model.Common;
using Workbench.AddIn.Services.Model;

namespace Workbench.AddIn.Bugstar.Overlays
{
    [ExportExtension(ExtensionPoints.Overlay, typeof(Viewport.AddIn.IViewportOverlay))]
    public class BugsByRadiusOverlay : Viewport.AddIn.ViewportOverlay
    {
        #region Constants
        private const string c_name = "Bugs by Radius";
        private const string c_description = "Click somewhere on the map viewport to see all bugs within a set radius.";
        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// A reference to view model for the map viewport
        /// </summary>
        [ImportExtension(Viewport.AddIn.CompositionPoints.MapViewport, typeof(Viewport.AddIn.IMapViewport))]
        public Viewport.AddIn.IMapViewport MapViewport { get; set; }

        /// <summary>
        /// Level Browser
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LevelBrowser, typeof(ILevelBrowser))]
        Lazy<ILevelBrowser> LevelBrowserProxy { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(CompositionPoints.ConfigurationService, typeof(Workbench.AddIn.Services.IConfigurationService))]
        Lazy<Workbench.AddIn.Services.IConfigurationService> Config { get; set; }

        /// <summary>
        /// MEF import for login service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LoginService, typeof(Workbench.AddIn.Services.ILoginService))]
        public Workbench.AddIn.Services.ILoginService LoginService { get; set; }

        /// <summary>
        /// Bugstar Service MEF import.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.BugstarService, typeof(IBugstarService))]
        protected IBugstarService BugstarService { get; set; }
        #endregion // MEF Imports

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public Bug SelectedBug
        {
            get { return m_selectedBug; }
            set
            {
                SetPropertyValue(value, () => SelectedBug,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_selectedBug = (Bug)newValue;
                        }
                ));
            }
        }
        private Bug m_selectedBug;

        /// <summary>
        /// 
        /// </summary>
        public float Radius
        {
            get { return m_radius; }
            set
            {
                SetPropertyValue(value, () => Radius,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_radius = (float)newValue;

                            this.Geometry.Clear();
                            SelectedBug = null;
                        }
                ));
            }
        }
        private float m_radius;
        #endregion

        #region Constructors
        /// <summary>
        /// Default Constructor
        /// </summary>
        public BugsByRadiusOverlay()
            : base(c_name, c_description)
        {
            this.Radius = 50;
            this.DataSourceModes[DataSource.Database] = true;
        }
        #endregion // Constructors

        #region Overrides
        /// <summary>
        /// Gets called when the user wants to view this overlay
        /// </summary>
        public override void Activated()
        {
            base.Activated();
            MapViewport.SelectedOverlayGeometry.CollectionChanged += SelectedOverlayGeometryChanged;
            MapViewport.ViewportClicked += ViewportClicked;
        }

        /// <summary>
        /// Gets called when the user turns off this overlay
        /// </summary>
        public override void Deactivated()
        {
            base.Deactivated();
            MapViewport.SelectedOverlayGeometry.CollectionChanged -= SelectedOverlayGeometryChanged;
            MapViewport.ViewportClicked -= ViewportClicked;
        }

        /// <summary>
        /// Gets the control that should be shown in the overlay details panel
        /// </summary>
        public override System.Windows.Controls.Control GetOverlayControl()
        {
            return new BugRadiusDetailsView();
        }
        #endregion // Overrides

        #region Event Callbacks
        /// <summary>
        /// Gets called when the user clicked somewhere on the viewport other than on geometry
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewportClicked(object sender, ViewportClickEventArgs e)
        {
            // Clear the geometry
            this.Geometry.BeginUpdate();
            this.Geometry.Clear();

            // Query bugstar for bugs
            try
            {
                // Create the bounds of the bug search (its a square otherwise we could have used the circle)
                Vector2f center = new Vector2f((float)e.WorldPosition.X, (float)e.WorldPosition.Y);

                Vector2f[] searchBounds = new Vector2f[4];
                searchBounds[0] = new Vector2f(center.X - Radius, center.Y - Radius);
                searchBounds[1] = new Vector2f(center.X + Radius, center.Y - Radius);
                searchBounds[2] = new Vector2f(center.X + Radius, center.Y + Radius);
                searchBounds[3] = new Vector2f(center.X - Radius, center.Y + Radius);

                this.Geometry.Add(new Viewport2DShape(etCoordSpace.World, "Search outline", searchBounds, Colors.Black, 2));

                // Search for the bugs in the designated area and then add them as circles
                List<Bug> bugs = BugstarService.Project.GetOpenBugsInArea(center, Radius, "Id,Summary,Description,X,Y,Z,Developer,Tester,State,Component,DueDate");

                foreach (Bug bug in bugs)
                {
                    Viewport2DCircle newGeometry = new Viewport2DCircle(etCoordSpace.World, "Filled shape for bug", new Vector2f(bug.Location.X, bug.Location.Y), 10.0f, Colors.Green);
                    newGeometry.PickData = bug;
                    this.Geometry.Add(newGeometry);
                }
            }
            catch (System.Exception ex)
            {
                Log.Log__Exception(ex, "Unexpected exception while attempting to retrieve bug information from bugstar.");
            }

            this.Geometry.EndUpdate();
        }

        /// <summary>
        /// Gets called when map geometry selection changes
        /// </summary>
        private void SelectedOverlayGeometryChanged(Object sender, NotifyCollectionChangedEventArgs e)
        {
            if (this.IsCurrentlyActive == true)
            {
                if (sender is RSG.Base.Collections.ObservableCollection<object>)
                {
                    RSG.Base.Collections.ObservableCollection<object> collection = sender as RSG.Base.Collections.ObservableCollection<object>;

                    if (collection.Count > 0)
                    {
                        SelectedBug = collection[0] as Bug;
                    }
                }
            }
        }
        #endregion // Event Callbacks
    } // BugstarBugOverlay
} // Workbench.AddIn.Bugstar.Overlays
