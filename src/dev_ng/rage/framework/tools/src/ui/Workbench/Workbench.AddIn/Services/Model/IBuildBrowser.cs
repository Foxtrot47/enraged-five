﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Windows.Helpers;
using RSG.Model.Common;
using RSG.Statistics.Common.Dto.GameAssets;

namespace Workbench.AddIn.Services.Model
{
    /// <summary>
    /// 
    /// </summary>
    public interface IBuildBrowser
    {
        #region Events
        /// <summary>
        /// Gets called when the current build changes
        /// </summary>
        event BuildChangedEventHandler BuildChanged;
        #endregion // Events

        #region Properties
        /// <summary>
        /// The currently selected item in the build collection
        /// </summary>
        BuildDto SelectedBuild { get; }

        /// <summary>
        /// List of builds.
        /// TODO: Move this to a build collection that is exposed from somewhere else.
        /// </summary>
        IList<BuildDto> AllBuilds { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Updates the builds.
        /// </summary>
        void UpdateBuilds(DataSource source);
        #endregion // Methods
    } // IBuildBrowser

    /// <summary>
    /// Build changed
    /// </summary>
    public class BuildChangedEventArgs : ItemChangedEventArgs<BuildDto>
    {
        public BuildChangedEventArgs(BuildDto oldBuild, BuildDto newBuild)
            : base(oldBuild, newBuild)
        {
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="args"></param>
    public delegate void BuildChangedEventHandler(Object sender, BuildChangedEventArgs args);

}
