﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using Ionic.Zip;
using RSG.Base.Collections;
using RSG.Base.ConfigParser;
using RSG.Base.Math;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using RSG.Base.Windows.Helpers;
using RSG.Bounds;
using RSG.Model.Common;
using RSG.Model.Common.Extensions;
using RSG.Model.Common.Map;
using MapViewport.AddIn;
using System.Windows.Forms;
using Workbench.AddIn.Services;
using Workbench.AddIn.Services.Model;

namespace Workbench.AddIn.MapStatistics.Overlays.Bounds
{
    /// <summary>
    /// Common base class for bound overlays
    /// </summary>
    public abstract class BoundsOverlayBase : LevelDependentViewportOverlay
    {
        #region MEF Imports
        /// <summary>
        /// The perforce sync service
        /// </summary>
        [Workbench.AddIn.ImportExtension(Workbench.AddIn.CompositionPoints.PerforceSyncService, typeof(Workbench.AddIn.Services.IPerforceSyncService))]
        private Lazy<Workbench.AddIn.Services.IPerforceSyncService> PerforceSyncService { get; set; }

        /// <summary>        
        /// MEF import for message box service.        
        /// </summary>        
        [ImportExtension(Workbench.AddIn.CompositionPoints.MessageService, typeof(Workbench.AddIn.Services.IMessageService))]        
        private IMessageService MessageService { get; set; }
        #endregion // MEF Imports

        #region Properties
        /// <summary>
        /// List of map sections to show information for
        /// </summary>
        public ObservableCollection<IMapSection> MapSections
        {
            get { return m_mapSections; }
            set
            {
                SetPropertyValue(value, () => MapSections,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_mapSections = (ObservableCollection<IMapSection>)newValue;
                        }
                ));
            }
        }
        private ObservableCollection<IMapSection> m_mapSections;

        /// <summary>
        /// Currently selected map section
        /// </summary>
        public IMapSection SelectedMapSection
        {
            get { return m_selectedMapSection; }
            set
            {
                SetPropertyValue(value, () => SelectedMapSection,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_selectedMapSection = (IMapSection)newValue;
                            OnSelectedMapSectionChanged();
                        }
                ));
            }
        }
        private IMapSection m_selectedMapSection;

        /// <summary>
        /// Relay command for refreshing the overlay
        /// </summary>
        public RelayCommand RefreshOverlayCommand
        {
            get
            {
                if (m_refreshOverlayCommand == null)
                {
                    m_refreshOverlayCommand = new RelayCommand(param => RefreshOverlay());
                }

                return m_refreshOverlayCommand;
            }
        }
        private RelayCommand m_refreshOverlayCommand;


        /// <summary>
        /// Overlay image that is used for rendering to texture
        /// </summary>
        protected Viewport2DImageOverlay OverlayImage
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="desc"></param>
        protected BoundsOverlayBase(string name, string desc)
            : base(name, desc)
        {
            m_mapSections = new ObservableCollection<IMapSection>();
        }
        #endregion // Constructor(s)

        #region Abstract Functions
        /// <summary>
        /// 
        /// </summary>
        protected abstract void OnSelectedMapSectionChanged();

        /// <summary>
        /// 
        /// </summary>
        protected abstract void RefreshOverlay();
        #endregion


        #region Overrides
        /// <summary>
        /// Gets called when the user wants to view this overlay
        /// </summary>
        public override void Activated()
        {
            base.Activated();

            // Make sure a level is selected
            if (LevelBrowserProxy.Value.SelectedLevel != null)
            {
                // Setup the overlay image
                Vector2i size = new Vector2i(966, 627);
                Vector2f pos = new Vector2f(-483.0f, 627.0f);

                ILevel level = LevelBrowserProxy.Value.SelectedLevel;
                if (level.ImageBounds != null)
                {
                    size = new Vector2i((int)(level.ImageBounds.Max.X - level.ImageBounds.Min.X), (int)(level.ImageBounds.Max.Y - level.ImageBounds.Min.Y));
                    pos = new Vector2f(level.ImageBounds.Min.X, level.ImageBounds.Max.Y);
                }

                float imageScaleFactor = 0.1f;
                OverlayImage = new Viewport2DImageOverlay(etCoordSpace.World, "", new Vector2i((int)(size.X * imageScaleFactor), (int)(size.Y * imageScaleFactor)), pos, new Vector2f(size.X, size.Y));
                Geometry.Add(OverlayImage);

                // Populate the map section's list
                IMapHierarchy hierarchy = level.GetMapHierarchy();

                List<IMapSection> sections = new List<IMapSection>();
                sections.AddRange(hierarchy.AllSections);
                sections.Sort();

                MapSections.AddRange(sections);
            }
        }

        /// <summary>
        /// Gets called when the user turns off this overlay
        /// </summary>
        public override void Deactivated()
        {
            Geometry.Clear();
            MapSections.Clear();
            SelectedMapSection = null;

            base.Deactivated();
        }
        #endregion // Overrides

        #region Helper Functions
        /// <summary>
        /// Loads all bounds data files for a particular section
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        protected List<BNDFile> LoadBoundsDataForSection(IMapSection section)
        {
            List<ContentNodeMapProcessedZip> processedZipNodes = new List<ContentNodeMapProcessedZip>();

            // Get the node that we actually want to process
            IList<Tuple<String, String>> processedZipFiles = new List<Tuple<String, String>>();

            ContentNodeMap mapNode = section.GetConfigMapNode(Config.Value.GameConfig);
            foreach (ContentNode exportNode in mapNode.Outputs)
            {
                // Note that if there aren't any processed nodes we ignore it
                foreach (ContentNode processedNode in exportNode.Outputs)
                {
                    // Load in the processed zip file and extract the bounds data
                    ContentNodeMapProcessedZip processedZipNode = processedNode as ContentNodeMapProcessedZip;

                    if (processedZipNode != null)
                    {
                        String filename = processedZipNode.Filename;
                        filename = Path.Combine(Path.GetDirectoryName(filename), Path.GetFileNameWithoutExtension(filename) + "_collision" + Path.GetExtension(filename));
                        processedZipFiles.Add(Tuple.Create(processedZipNode.Name, filename));
                    }
                }
            }

            // We need to ensure that all the processed zip files are up to date in p4 for this section
            int syncedFiles = 0;
            PerforceSyncService.Value.Show("You current have some processed zip files that are out of date.\n" +
                                           "Would you like to grab latest now?\n\n" +
                                           "You can select which files to grab using the table below.\n",
                                           processedZipFiles.Select(item => item.Item2).ToArray(), ref syncedFiles, false, true);

            // Now we have latest, load up the bnd files.
            List<BNDFile> boundFiles = new List<BNDFile>();
            IList<String> processedNotFound = new List<String>();
            foreach (Tuple<String, String> processedZipFile in processedZipFiles)
            {
                if (File.Exists(processedZipFile.Item2))
                {
                    boundFiles.AddRange(LoadBoundsDataFromProcessedZipFile(processedZipFile.Item1, processedZipFile.Item2));
                }
                else
                {
                    processedNotFound.Add(processedZipFile.Item2);
                }
            }

            if (processedNotFound.Any())
            {
                MessageService.Show(String.Format("There are {0} processed zip file(s) that could not be found. Please ensure that you are up-to-date with the processed folder in GAME\\assets. Not having these file(s) will result in incorrect data being shown on the overlay's panel.", processedNotFound.Count));
            }

            return boundFiles;
        }

        /// <summary>
        /// Renders a bound object's bounds to the overlay texture
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="colour"></param>
        protected void RenderBounds(BoundObject obj, Color colour)
        {
            if (obj is Composite)
            {
                Composite composite = (Composite)obj;

                foreach (CompositeChild child in composite.Children)
                {
                    RenderBounds(child.BoundObject, colour);
                }
            }
            else
            {
                // Convert the bound object to a BVH and then process
                BVH bvh = BoundObjectConverter.ToBVH(obj);
                RenderBVH(bvh, colour);
            }
        }
        #endregion // Helper Functions

        #region Private Methods

        /// <summary>
        /// Loads all bounds data files found in a particular processed zip file
        /// </summary>
        /// <param name="processedNode"></param>
        /// <returns></returns>
        private List<BNDFile> LoadBoundsDataFromProcessedZipFile(string sectionName, string zipFileName)
        {
            List<BNDFile> boundFiles = new List<BNDFile>();

            using (ZipFile zipFile = ZipFile.Read(zipFileName))
            {
                IEnumerable<ZipEntry> boundEntries = zipFile.Where(item => (item.FileName.EndsWith(".ibd.zip") || item.FileName.EndsWith(".ibr.zip") || item.FileName.EndsWith(".ibn.zip") || item.FileName.EndsWith("collision.zip")));
                foreach (ZipEntry entry in boundEntries)
                {
                    using (MemoryStream zipStream = new MemoryStream())
                    {
                        entry.Extract(zipStream);
                        zipStream.Position = 0;

                        string boundFileName = Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(entry.FileName)) + ".bnd";

                        BNDFile boundFile = LoadBoundsDataFromBoundsZipFile(boundFileName, zipStream);
                        if (boundFile != null)
                        {
                            boundFiles.Add(boundFile);
                        }
                    }
                }
            }

            return boundFiles;
        }

        /// <summary>
        /// Loads the single bounds data file found in a particular .ibn.zip file
        /// </summary>
        /// <param name="boundFileName"></param>
        /// <param name="zipStream"></param>
        /// <returns></returns>
        private BNDFile LoadBoundsDataFromBoundsZipFile(string boundFileName, Stream zipStream)
        {
            using (ZipFile zipFile = ZipFile.Read(zipStream))
            {
                if (zipFile.ContainsEntry(boundFileName))
                {
                    using (MemoryStream fileStream = new MemoryStream())
                    {
                        zipFile[boundFileName].Extract(fileStream);
                        fileStream.Position = 0;
                        return BNDFile.Load(boundFileName, fileStream);
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Renders a BVH's bounds to the overlay texture
        /// </summary>
        /// <param name="bvh"></param>
        /// <param name="colour"></param>
        private void RenderBVH(BVH bvh, Color colour)
        {
            foreach (BVHPrimitive prim in bvh.Primitives.Where(item => item is BVHTriangle))
            {
                BVHTriangle tri = (BVHTriangle)prim;

                Vector2f[] outline = new Vector2f[3];
                outline[0] = new Vector2f(bvh.Verts[tri.Index0].Position.X, bvh.Verts[tri.Index0].Position.Y);
                outline[1] = new Vector2f(bvh.Verts[tri.Index1].Position.X, bvh.Verts[tri.Index1].Position.Y);
                outline[2] = new Vector2f(bvh.Verts[tri.Index2].Position.X, bvh.Verts[tri.Index2].Position.Y);

                OverlayImage.RenderPolygon(outline, colour);
            }
        }
        #endregion // Private Functions
    }
}
