﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;

namespace ContentBrowser.ViewModel.VehicleViewModels
{
    /// <summary>
    /// 
    /// </summary>
    public class VehicleCollectionViewModel : ObservableContainerViewModelBase<IVehicle>
    {
        #region Properties
        /// <summary>
        /// The number of viewmodels to create before displaying them on the
        /// screen. Less than or equal to 0 implies no batching
        /// </summary>
        protected override uint AssetCreationBatchSize
        {
            get
            {
                return 10;
            }
        }

        /// <summary>
        /// The number of viewmodels to create before displaying them on the
        /// screen. Less than or equal to 0 implies no batching
        /// </summary>
        protected override uint GridCreationBatchSize
        {
            get
            {
                return 10;
            }
        }
        #endregion // Properties

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public VehicleCollectionViewModel(IVehicleCollection vehicles)
            : base(vehicles, vehicles.Vehicles)
        {
        }
        #endregion // Constructor(s)
    } // VehicleCollectionViewModel
}
