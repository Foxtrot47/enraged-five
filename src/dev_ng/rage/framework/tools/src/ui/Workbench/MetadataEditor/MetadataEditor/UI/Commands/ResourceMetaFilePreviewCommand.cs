﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using RSG.Base.ConfigParser;
using RSG.Base.Logging;
using RSG.Platform;
using Workbench.AddIn;
using Workbench.AddIn.Commands;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.Services;
using MetadataEditor.AddIn;
using MetadataEditor.RestData;
using Ionic.Zip;
using WidgetEditor.AddIn;
using System.Windows;
using RSG.Base.Configuration;

namespace MetadataEditor.UI.Commands
{

    /// <summary>
    /// Resource metadata file (preview) command.
    /// </summary>
    [ExportExtension(MetadataEditor.AddIn.ExtensionPoints.MenuMetadata, 
        typeof(IWorkbenchCommand))]
    [ExportExtension(MetadataEditor.AddIn.ExtensionPoints.MetadataToolbar,
        typeof(IWorkbenchCommand))]
    class ResourceMetaFilePreviewCommand :
        WorkbenchMenuItemBase
    {
        #region MEF Imports
        /// <summary>
        /// Layout Manager Service
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager, typeof(ILayoutManager))]
        private ILayoutManager LayoutManager { get; set; }

        /// <summary>
        /// Configuration service
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(Workbench.AddIn.Services.IConfigurationService))]
        private IConfigurationService ConfigService { get; set; }

        /// <summary>
        /// MEF import for message box service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.MessageService, typeof(Workbench.AddIn.Services.IMessageService))]
        private IMessageService MessageService { get; set; }

        /// <summary>
        /// MEF import for proxy UI
        /// </summary>
        [ImportExtension(WidgetEditor.AddIn.CompositionPoints.ProxyService, typeof(IProxyService))]
        public IProxyService ProxyService { get; set; }
        #endregion // MEF Imports

        #region Constants
        /// <summary>
        /// Unique GUID for the menu item
        /// </summary>
        public static readonly Guid GUID = new Guid("6266F112-B947-4D55-94E4-E35759D8565D");

        /// <summary>
        /// Path to the rag widget for enabling the preview folder.
        /// </summary>
        private const String RAG_PREVIEW_ENABLE = "Preview Folder/Enable";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ResourceMetaFilePreviewCommand()
        {
            this.Header = "Resource Metadata File (_Preview)";
            this.ToolTip = "Preview metadata; particularly for TCS files where their linked texture dictionaries are converted.";
            this.ID = GUID;
            this.KeyGesture = new System.Windows.Input.KeyGesture(
                System.Windows.Input.Key.F7, System.Windows.Input.ModifierKeys.Shift);
            this.SetImageFromBitmap(Resources.Images.ConvertPreview);
        }
        #endregion // Constructor(s)

        #region ICommand Implementation
        /// <summary>
        /// Determine whether this command is enabled.
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        /// 
        public override bool CanExecute(Object parameter)
        {
            IDocumentBase activeDocument = LayoutManager.ActiveDocument();
            if ((null != activeDocument) && (activeDocument is IMetadataView))
            {
                return (true);
            }

            return (false);
        }

        /// <summary>
        /// Execute this command.
        /// </summary>
        /// <param name="parameter"></param>
        public override void Execute(Object parameter)
        {
            // Check whether the user has the preview flag set in game.
            CheckPreviewEnabledFlag();

            // Determine if we are previewing regular metadata or a TCS file.
            IDocumentBase activeDocument = LayoutManager.ActiveDocument();
            int exitCode = 0;
            if (activeDocument.DocumentData is MetadataInfo)
            {
                exitCode = ExecutePreviewTextureDictionary();

                if (0 == exitCode)
                {
                    // Preview conversion succeeded
                    MessageService.Show("The preview file was successfully generated.");
                }
                else
                {
                    // Preview conversion failed
                    MessageService.Show("An error occurred while generating the preview file.");
                }
            }
            else
            {
                FileType fileType = FileTypeUtils.ConvertExtensionToFileType(Path.GetExtension(activeDocument.Path));

                if (fileType == FileType.TexturePipelineTemplate)
                {
                    MessageService.Show("It is currently not possible to preview texture pipeline template files (TCP) files.\n\n" +
                        "The only way to preview changes to TCP files is by previewing a TCS file that makes use of it and was opened through " +
                        "the game's 'Texture Viewer' -> 'Edit Metadata File' interface in RAG.");
                }
                else if (fileType == FileType.TexturePipelineSpecification)
                {
                    MessageService.Show("It is currently only possible to preview texture pipeline specification (TCS) files that have been " +
                        "sent from the game via the 'Texture Viewer' -> 'Edit Metadata File' interface in RAG.");
                }
                else
                {
                    exitCode = ExecutePreviewFile(activeDocument.Path);

                    if (0 == exitCode)
                    {
                        // Preview conversion succeeded
                        MessageService.Show("The preview file was successfully generated.");
                    }
                    else
                    {
                        // Preview conversion failed
                        MessageService.Show("An error occurred while generating the preview file.");
                    }
                }
            }
        }
        #endregion // ICommand Implementation

        #region Private Methods
        /// <summary>
        /// Checks whether the user has the preview flag enabled in game.
        /// </summary>
        private void CheckPreviewEnabledFlag()
        {
            if (ProxyService.IsGameConnected && !ProxyService.Console.ReadBoolWidget(RAG_PREVIEW_ENABLE))
            {
                MessageBoxResult result = MessageService.Show(
                    "The preview flag is not currently enabled in the game.  Do you want to enable it?",
                    MessageBoxButton.YesNo, MessageBoxImage.Warning);

                if (result == MessageBoxResult.Yes)
                {
                    ProxyService.Console.WriteBoolWidget(RAG_PREVIEW_ENABLE, true);
                }
            }
        }

        /// <summary>
        /// Preview texture dictionary; from TCS metadata editing.
        /// </summary>
        /// <returns></returns>
        private int ExecutePreviewTextureDictionary()
        {
            // Get the metadata texture data info from the active document
            MetadataInfo info = null;

            IDocumentBase activeDocument = LayoutManager.ActiveDocument();
            if (activeDocument != null && activeDocument.DocumentData != null)
            {
                if (activeDocument.DocumentData is MetadataInfo)
                {
                    info = activeDocument.DocumentData as MetadataInfo;
                }
            }

            if (info == null)
            {
                throw new ArgumentNullException("You shouldn't have been able to execute this action!");
            }

            // We have two different cases to cater for now
            // 1) Special case where the texture name has 'platform:' in it
            // 2) Default case where we get a texture name, RPF file, asset type and asset name
            String extractedFileName = null;

            if ((0 == info.RPFFilename.Length) && info.AssetName.StartsWith("platform:"))
            {
                extractedFileName = ExtractZipFileForPlatformTexture(info);
            }
            else
            {
                extractedFileName = ExtractZipFileForAsset(info);
            }

            // Check that a file was successfully copied into the cache dir
            if (extractedFileName != null)
            {
                return (ExecutePreviewFile(extractedFileName));
            }
            else
            {
                MessageService.Show("Unable to determine which file to process for previewing the tcs change.");
            }
            return (0);
        }

        /// <summary>
        /// Preview any file.
        /// </summary>
        /// <returns></returns>
        private int ExecutePreviewFile(String fullPath)
        {
            IConfig config = ConfigService.Config;
            string filename = Path.GetFileName(fullPath);
            string previewDirectory = config.Project.DefaultBranch.Preview;
            string libDirectory = Path.Combine(config.ToolsRoot, "ironlib", "lib");

            string exe = Path.Combine(libDirectory, "RSG.Pipeline.Convert.exe");
            string output = Path.Combine(previewDirectory, filename);
            string input = fullPath;

            StringBuilder sb = new StringBuilder();
            sb.Append(" --no-content");
            sb.Append(" --rebuild");
            sb.Append(" --preview");
            sb.Append(" --proc RSG.Pipeline.Processor.Platform.Rage");
            sb.AppendFormat(" --output {0}", output);
            sb.AppendFormat(" {0}", input);

            Process previewProcess = new Process();
            previewProcess.StartInfo.UseShellExecute = false;
            previewProcess.StartInfo.FileName = exe;
            previewProcess.StartInfo.Arguments = sb.ToString();

            Log.Log__Message("Preview convert: {0} {1}",
                previewProcess.StartInfo.FileName,
                previewProcess.StartInfo.Arguments);
            
            previewProcess.Start();
            previewProcess.WaitForExit();

            return (previewProcess.ExitCode);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        private string ExtractZipFileForPlatformTexture(MetadataInfo info)
        {
            // Determine the path of the source/destination
            string zipFilePath = info.AssetName.Replace("platform:", ConfigService.GameConfig.ExportDir) + "." + info.AssetType.GetExportExtension() + ".zip";
            string zipFileName = Path.GetFileName(zipFilePath);
            string cacheDir = Path.Combine(ConfigService.GameConfig.ToolsCacheDir, 
                Properties.Settings.Default.MetadataAssetsCacheDirOffset);
            string cacheFilePath = Path.Combine(cacheDir, zipFileName);


            // Ensure that the destination folder exists
            if (!Directory.Exists(cacheDir))
            {
                Directory.CreateDirectory(cacheDir);
            }

            // Copy the file across
            File.Copy(zipFilePath, cacheFilePath);
            return cacheFilePath;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        private string ExtractZipFileForAsset(MetadataInfo info)
        {
            // I assume here that the content node name is the same as the RPF file name minus its extension
            string contentName = Path.GetFileNameWithoutExtension(info.RPFFilename).ToLower();

            // Get all processed map zip nodes for this item
            List<ContentNode> processedMapZipNodes = ConfigService.GameConfig.Content.Root.FindAll(
                contentName, "processedmapzip");
            List<ContentNode> zipNodes = new List<ContentNode>();

            // If we didn't find any then checked for "normal" non-processed map zip nodes
            if (processedMapZipNodes.Count == 0)
            {
                zipNodes = ConfigService.GameConfig.Content.Root.FindAll(contentName, "mapzip");

                // If we still didn't find any, we need to look for a zip node of the name
                // This is a special case for shared textures such as gtxd
                if (zipNodes.Count == 0)
                {
                    zipNodes = ConfigService.GameConfig.Content.Root.FindAll(contentName, "zip");
                }
            }
            else
            {
                // We should only have one (otherwise there is an error in the content tree)
                ContentNode processedNode = processedMapZipNodes.First();
                zipNodes = processedNode.Inputs.ToList();
            }

            // Find the zip file for the asset that contains the texture in the map zip files
            string cacheDir = Path.Combine(ConfigService.GameConfig.ToolsCacheDir, Properties.Settings.Default.MetadataAssetsCacheDirOffset);
            string extractedFileName = null;

            foreach (ContentNode mapZipNode in zipNodes)
            {
                if (mapZipNode is ContentNodeFile)
                {
                    string zipFileName = Path.Combine((mapZipNode as ContentNodeFile).Path, mapZipNode.Name + ".zip");
                    using (ZipFile sectionZip = ZipFile.Read(zipFileName))
                    {
                        string assetZipFilename = info.AssetName + "." + info.AssetType.GetExportExtension() + ".zip";
                        if (sectionZip.ContainsEntry(assetZipFilename))
                        {
                            sectionZip.ExtractSelectedEntries(assetZipFilename, null, cacheDir, ExtractExistingFileAction.OverwriteSilently);
                            extractedFileName = Path.Combine(cacheDir, assetZipFilename);
                            break;
                        }
                    }
                }
            }

            return extractedFileName;
        }
        #endregion // Private Methods
    }

} // MetadataEditor.UI.Commands namespace
