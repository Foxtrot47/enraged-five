﻿using MetadataEditor.AddIn.MetaEditor.ViewModel;
using RSG.Metadata.Data;

namespace MetadataEditor.AddIn.MetaEditor.GridViewModel
{
    /// <summary>
    /// The view model that wraps around a  <see cref="BoolTunable"/> object. Used for the
    /// grid view.
    /// </summary>
    public class BoolTunableViewModel : GridTunableViewModel
    {
        #region Constructor
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="MetadataEditor.AddIn.MetaEditor.GridViewModel.IntegerTunableViewModel"/>
        /// class.
        /// </summary>
        public BoolTunableViewModel(GridTunableViewModel parent, BoolTunable m, MetaFileViewModel root)
            : base(parent, m, root)
        {
        }
        #endregion // Constructor
    } // BoolTunableViewModel
} // MetadataEditor.AddIn.MetaEditor.GridViewModel
