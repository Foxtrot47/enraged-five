﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Workbench.AddIn;
using Workbench.AddIn.UI;
using RSG.Base.Logging;

namespace Workbench.UI
{

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.Void)]
    public partial class AboutBox : Window, IPartImportsSatisfiedNotification
    {
        #region Constants
        /// <summary>
        /// 
        /// </summary>
        public readonly String AUTHOR = "David Muir <david.muir@rockstarnorth.com>";
        #endregion // Constants

        #region Static Properties
        /// <summary>
        /// 
        /// </summary>
        public static AboutBox AboutBoxDialog = null;
        #endregion // Static Properties

        #region MEF Imports (optional)

        /// <summary>
        /// MEF import for plugin aboutbox (optional).
        /// </summary>
        [ImportManyExtension(Workbench.AddIn.ExtensionPoints.PluginInformation, typeof(IPluginDetails))]
        public IEnumerable<IPluginDetails> PluginDetails { get; set; }

        #endregion // MEF Imports (optional)

        #region Properties and Associated Member Data
        /// <summary>
        /// 
        /// </summary>
        public String Author
        {
            get { return AUTHOR; }
        }

        /// <summary>
        /// 
        /// </summary>
        public RSG.Base.Reflection.AssemblyInfo Info
        {
            get;
            private set;
        }
        #endregion // Properties and Associated Member Data

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public AboutBox()
        {
            if (null == AboutBoxDialog)
                AboutBoxDialog = this;
            this.Info = new RSG.Base.Reflection.AssemblyInfo(
                Assembly.GetExecutingAssembly());
            InitializeComponent();
        }

        #endregion // Constructor(s)

        #region IPartImportsSatisfiedNotification Interface

        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {
            // Ensure ItemsSource is refreshed after we have imported our
            // plugin details.
            this.lstPlugins.ItemsSource = this.PluginDetails;
        }

        #endregion // IPartImportsSatisfiedNotification Interface

        #region Event Handlers

        /// <summary>
        /// 
        /// </summary>
        private void Button_Click(Object sender, RoutedEventArgs e)
        {
            Hide();
        }

        /// <summary>
        /// 
        /// </summary>
        private void Window_Closing(Object sender, CancelEventArgs e)
        {
            e.Cancel = true;
            Hide();
        }

        #endregion // Event Handlers
    }

} // Workbench.UI namespace
