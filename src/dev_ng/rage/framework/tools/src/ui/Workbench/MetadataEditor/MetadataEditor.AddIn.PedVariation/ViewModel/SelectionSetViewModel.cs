﻿namespace MetadataEditor.AddIn.PedVariation.ViewModel
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using RSG.Base.Editor;
    using RSG.Metadata.Characters;

    public class SelectionSetViewModel : ViewModelBase
    {
        #region Fields
        /// <summary>
        /// The private reference to the model data provider for this view model.
        /// </summary>
        private SelectionSet _selectionSet;

        /// <summary>
        /// The private field used for the <see cref="ComponentSets"/> property.
        /// </summary>
        private ObservableCollection<ComponentSetViewModel> _componentSets;

        /// <summary>
        /// The private field used for the <see cref="PropSets"/> property.
        /// </summary>
        private ObservableCollection<PropSetViewModel> _propSets;

        /// <summary>
        /// The private field used for the <see cref="IsExpanded"/> property.
        /// </summary>
        private bool _isExpanded;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SelectionSetViewModel"/> class.
        /// </summary>
        /// <param name="selectionSet">
        /// The model that provides the data for this view model.
        /// </param>
        public SelectionSetViewModel(SelectionSet selectionSet, SelectionSetCollectionViewModel parent)
        {
            this._selectionSet = selectionSet;
            this._componentSets = new ObservableCollection<ComponentSetViewModel>();

            foreach (KeyValuePair<int, SortedList<int, string>> component in parent.Components)
            {
                this._componentSets.Add(new ComponentSetViewModel(selectionSet, component.Key, parent));
            }

            this._propSets = new ObservableCollection<PropSetViewModel>();
            for (int i = 0; i < parent.AvailableAnchorPointCount; i++)
            {
                this._propSets.Add(new PropSetViewModel(selectionSet, i, parent));
            }
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the name for this selection set.
        /// </summary>
        public string Name
        {
            get { return this._selectionSet.Name; }
            set { this._selectionSet.Name = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this selection set is currently expanded
        /// in the view.
        /// </summary>
        public bool IsExpanded
        {
            get { return this._isExpanded; }
            set { this.SetPropertyValue(ref this._isExpanded, value, "IsExpanded"); }
        }

        /// <summary>
        /// Gets the collection of component set view models to show to the user.
        /// </summary>
        public ObservableCollection<ComponentSetViewModel> ComponentSets
        {
            get { return this._componentSets; }
        }

        /// <summary>
        /// Gets the collection of prop set view models to show to the user.
        /// </summary>
        public ObservableCollection<PropSetViewModel> PropSets
        {
            get { return this._propSets; }
        }
        #endregion Properties
    }
}
