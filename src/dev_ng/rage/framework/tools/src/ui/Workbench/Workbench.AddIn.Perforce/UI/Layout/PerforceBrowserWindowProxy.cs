﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using Workbench.AddIn.Services;
using Workbench.AddIn.UI.Layout;

namespace Workbench.AddIn.Perforce.UI.Layout
{

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.ToolWindowProxy, typeof(IToolWindowProxy))]
    class PerforceBrowserWindowProxy : IToolWindowProxy
    {
        #region MEF Imports
        /// <summary>
        /// Configuration service reference.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(IConfigurationService))]
        private Lazy<IConfigurationService> ConfigService { get; set; }

        /// <summary>
        /// Layout manager reference.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager, typeof(ILayoutManager))]
        private Lazy<ILayoutManager> LayoutManager { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(CompositionPoints.PerforceBrowser, typeof(IPerforceBrowser))]
        private Lazy<IPerforceBrowser> ViewModel { get; set; }
        #endregion // MEF Imports

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public PerforceBrowserWindowProxy()
        {
            this.Name = Resources.Strings.PerforceBrowser_Name;
            this.Header = Resources.Strings.PerforceBrowser_Title;
        }

        #endregion // Constructor(s)

        #region Public Functions
        /// <summary>
        /// A abstract function that is overriden by the plugin to create the
        /// actual instance of the tool window and pass it back as a out 
        /// parameter.
        /// </summary>
        protected override Boolean CreateToolWindow(out IToolWindowBase toolWindow)
        {
            PerforceBrowserViewModel viewModel = (ViewModel.Value as PerforceBrowserViewModel);
            toolWindow = new PerforceBrowserView(LayoutManager.Value, viewModel);
            return true;
        }
        #endregion // Public Functions
    }

} // Workbench.AddIn.Perforce.UI.Layout namespace
