﻿using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using RSG.Base.Editor;
using Workbench.AddIn.Services;
using Workbench.AddIn.UI.Layout;
using System.Collections.Generic;
using RSG.UniversalLog;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Windows.Documents;

namespace Workbench.AddIn.UniversalLog
{
    /// <summary>
    /// Interaction logic for UniversalLogView.xaml
    /// </summary>
    public partial class UniversalLogView : DocumentBase<UniversalLogViewModel>
    {
        #region Constants
        public static readonly Guid GUID = new Guid("00358026-BF1B-4D17-962D-66F4154A3658");
        #endregion // Constants

        #region Constructor
        public UniversalLogView(string filename, IConfigurationService config, IWebBrowserService webBrowser)
            : base("UniversalLogView", new UniversalLogViewModel(filename, config, webBrowser))
        {
            InitializeComponent();

            this.Title = System.IO.Path.GetFileNameWithoutExtension(filename);
            this.ID = GUID;
            this.IsModified = false;
            this.Path = System.IO.Path.GetFullPath(filename);
        } 
        #endregion

        #region Event Handlers
        private void TextBox_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            (sender as TextBox).ScrollToEnd();
        }
        #endregion
    } // UniversalLogView
} // Workbench.AddIn.UniversalLog
