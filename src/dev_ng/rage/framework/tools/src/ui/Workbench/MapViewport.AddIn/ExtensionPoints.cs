﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Viewport.AddIn
{
    public class ExtensionPoints
    {    
        /// <summary>
        /// Point to import/export viewport overlay groups with
        /// </summary>
        public const String OverlayGroup = "29DF28EA-BE88-4916-BC48-FA58FA7C0982";
        
        /// <summary>
        /// The point to import/export overlays into the miscellaneous group with
        /// </summary>
        public const String MiscellaneousOverlayGroup = "105F3327-447F-409B-BB04-C38395089ABC";
    } // ExtensionPoints
} // MapViewport.AddIn
