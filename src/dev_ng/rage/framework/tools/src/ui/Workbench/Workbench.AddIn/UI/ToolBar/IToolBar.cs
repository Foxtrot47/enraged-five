﻿using System;
using System.Collections.Generic;
using Workbench.AddIn.UI.Controls;

namespace Workbench.AddIn.UI.ToolBar
{

    /// <summary>
    /// ToolBar interface.
    /// </summary>
    public interface IToolBar : IControl
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        String Header { get; }
        
        /// <summary>
        /// 
        /// </summary>
        String Name { get; }

        /// <summary>
        /// 
        /// </summary>
        IEnumerable<IToolBarItem> Items { get; }
        #endregion // Properties
    }

} // Workbench.AddIn.UI.ToolBar namespace
