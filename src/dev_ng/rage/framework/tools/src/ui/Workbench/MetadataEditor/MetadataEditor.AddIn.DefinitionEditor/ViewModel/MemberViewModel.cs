﻿using System;

namespace MetadataEditor.AddIn.DefinitionEditor.ViewModel
{

    /// <summary>
    /// ViewModel for the IMember series of classes.
    /// </summary>
    public class MemberViewModel : 
        RSG.Base.Editor.ViewModelBase,
        IMemberViewModel
    {
        #region Properties
        /// <summary>
        /// Reference to the Member model this represents.
        /// </summary>
        public RSG.Metadata.Parser.IMember Model { get; protected set; }

        /// <summary>
        /// Reference to the parent Member ViewModel (for hierarchy).
        /// </summary>
        public IMemberViewModel Parent { get; protected set;  }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="parent"></param>
        public MemberViewModel(IMemberViewModel parent, RSG.Metadata.Parser.IMember m)
        {
            this.Parent = parent;
            this.Model = m;
        }
        #endregion // Constructor(s)
    }

} // RSG.Metadata.ViewModel namespace
