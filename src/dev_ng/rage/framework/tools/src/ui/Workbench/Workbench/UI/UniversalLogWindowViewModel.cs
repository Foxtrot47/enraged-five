﻿using RSG.Base.Editor;
using System.Collections.Generic;
using Workbench.AddIn.Services;
using RSG.UniversalLog;
using System;
using System.Linq;
using System.Data;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Drawing;
using System.Windows.Controls;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Xml;
using RSG.Base.Logging.Universal;
using RSG.UniversalLog.ViewModel;
using System.Net.Mail;

namespace Workbench.UI
{
    /// <summary>
    /// 
    /// </summary>
    public class UniversalLogWindowViewModel : ViewModelBase, IFilterDataModel
    {
        #region Fields
        private RelayCommand sendEmailCommand;
        private RelayCommand clearLogCommand;
        private KeyValuePair<string, IUniversalLogListener> m_selectedListener;
        private IConfigurationService m_config;
        private IWebBrowserService m_webBrowser;
        private ObservableCollection<UniversalLogComponentViewModel> m_messageCollection;
        private ObservableCollection<UniversalLogComponentViewModel> m_allMessageCollection;
        private bool showErrors;
        private bool showWarnings;
        private bool showMessages;
        private bool showDebugMessages;
        #endregion

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public RelayCommand SendEmailCommand
        {
            get
            {
                if (this.sendEmailCommand == null)
                {
                    this.sendEmailCommand = new RelayCommand(this.SendEmail);
                }

                return this.sendEmailCommand;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand ClearLogCommand
        {
            get
            {
                if (this.clearLogCommand == null)
                {
                    this.clearLogCommand = new RelayCommand(this.Clear);
                }

                return this.clearLogCommand;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, IUniversalLogListener> Listeners
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public KeyValuePair<string, IUniversalLogListener> SelectedListener
        {
            get { return m_selectedListener; }
            set 
            {
                if (Object.ReferenceEquals(m_selectedListener, value))
                    return;

                m_selectedListener = value; 
                OnSelectedListenerChanged();
            }
        }

        public bool ShowErrors
        {
            get
            {
                return this.showErrors;
            }

            set
            {
                if (this.showErrors == value)
                {
                    return;
                }

                this.showErrors = value;
                this.OnPropertyChanged("ShowErrors");
                this.OnFilterOptionChanged();
            }
        }

        public bool ShowWarnings
        {
            get
            {
                return this.showWarnings;
            }

            set
            {
                if (this.showWarnings == value)
                {
                    return;
                }

                this.showWarnings = value;
                this.OnPropertyChanged("ShowWarnings");
                this.OnFilterOptionChanged();
            }
        }

        public bool ShowMessages
        {
            get
            {
                return this.showMessages;
            }

            set
            {
                if (this.showMessages == value)
                {
                    return;
                }

                this.showMessages = value;
                this.OnPropertyChanged("ShowMessages");
                this.OnFilterOptionChanged();
            }
        }

        public bool ShowDebugMessages
        {
            get
            {
                return this.showDebugMessages;
            }

            set
            {
                if (this.showDebugMessages == value)
                {
                    return;
                }

                this.showDebugMessages = value;
                this.OnPropertyChanged("ShowDebugMessages");
                this.OnFilterOptionChanged();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Expression
        {
            get { return m_expression; }
            set
            {
                if (m_expression == value)
                    return;

                m_expression = value;
                if (!string.IsNullOrWhiteSpace(this.m_expression))
                {
                    if (CaseSensitive)
                        m_regularExpression = new Regex(m_expression);
                    else
                        m_regularExpression = new Regex(m_expression, RegexOptions.IgnoreCase);
                }
                OnPropertyChanged("Expression");
                UpdateText();
            }
        }
        private string m_expression;
        private Regex m_regularExpression;
        
        /// <summary>
        /// 
        /// </summary>
        public bool CaseSensitive
        {
            get { return m_caseSensitive; }
            set
            {
                if (m_caseSensitive == value)
                    return;

                m_caseSensitive = value;
                OnPropertyChanged("CaseSensitive");
                if (!string.IsNullOrWhiteSpace(this.Expression))
                {
                    if (CaseSensitive)
                        m_regularExpression = new Regex(m_expression);
                    else
                        m_regularExpression = new Regex(m_expression, RegexOptions.IgnoreCase);

                    UpdateText();
                }
            }
        }
        private bool m_caseSensitive;       

        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<UniversalLogComponentViewModel> MessageCollection
        {
            get { return m_messageCollection; }
            set { m_messageCollection = value; }
        }
        #endregion
        
        #region Constructor
        public UniversalLogWindowViewModel(IEnumerable<ILoggingService> loggers, IConfigurationService config, IWebBrowserService webBrowser)
        {
            this.m_config = config;
            this.m_webBrowser = webBrowser;
            this.MessageCollection = new ObservableCollection<UniversalLogComponentViewModel>();
            this.m_allMessageCollection = new ObservableCollection<UniversalLogComponentViewModel>();
            this.ShowWarnings = true;
            this.ShowErrors = true;
            this.ShowMessages = true;
            this.ShowDebugMessages = true;

            this.Listeners = new Dictionary<string, IUniversalLogListener>();
            IUniversalLogListener defaultLogger = null;
            foreach (ILoggingService logger in loggers)
            {
                Guid guid = Guid.NewGuid();
                string index = string.Empty;
                if (!this.Listeners.ContainsKey(logger.UIName))
                {
                    this.Listeners.Add(logger.UIName, null);
                    index = logger.UIName;
                }
                else
                {
                    this.Listeners.Add(guid.ToString(), null);
                    index = guid.ToString();
                }

                if (logger is IInternalLoggingService)
                {
                    this.Listeners[index] = new UniversalLogListener((logger as IInternalLoggingService).LogObject);
                    this.Listeners[index].UniversalBufferChanged += OnUniversalBufferChanged;
                }
                else if (logger is IExternalLoggingService)
                {
                    //this.Listeners[index] = new UniversalLogListener((logger as IExternalLoggingService).LogDirectory, (logger as IExternalLoggingService).LogFilenamePattern);
                    //this.Listeners[index].ListenerUpdated += OnListenerUpdated;
                }

                if (logger.IsDefault)
                {
                    defaultLogger = this.Listeners[index];
                }
            }
            if (defaultLogger != null)
            {
                foreach (var listener in this.Listeners)
                {
                    if (listener.Value == defaultLogger)
                    {
                        this.SelectedListener = listener;
                        break;
                    }
                }
            }
            else
            {
                this.SelectedListener = this.Listeners.FirstOrDefault();
            }
        }
        #endregion // Constructor

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        public void Clear(object parameter)
        {
            this.m_allMessageCollection.Clear();
            this.MessageCollection.Clear();
            if (this.SelectedListener.Value != null)
            {
                this.SelectedListener.Value.Clear();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void SendEmail(object parameter)
        {
            string[] receivers = this.m_config.GameConfig.ConfigCoreView.GetLogMailAddresses();
            if (this.SelectedListener.Value != null)
            {
                try
                {
                    List<Attachment> att = new List<Attachment>();

                    string messagebody = "See attachment\n\n\n";
                    string subject = "Universal Log Report from " + Environment.UserName;

                    using (MemoryStream stream = new MemoryStream())
                    {
                        XmlWriterSettings settings = new XmlWriterSettings();
                        settings.Indent = true;
                        using (XmlWriter writer = XmlWriter.Create(stream, settings))
                        {
                            writer.WriteStartDocument(true);
                            writer.WriteStartElement("ulog");
                            writer.WriteAttributeString("version", "1.0");
                            writer.WriteAttributeString("name", Environment.UserName);
                            writer.WriteAttributeString("machine", Environment.MachineName);

                            this.SelectedListener.Value.Serialise(writer);

                            writer.WriteEndElement();
                            writer.WriteEndDocument();
                        }

                        stream.Position = 0;
                        att.Add(new Attachment(stream, "WorkbenchULogReport.ulog"));
                        RSG.UniversalLog.Util.Email.SendEmail(this.m_config.Config, receivers, subject, messagebody, att.ToArray());
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(string.Format("Unable to send email due to the following error: {0}", ex.Message), "Error", MessageBoxButton.OK);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnUniversalBufferChanged(object sender, UniversalBufferChangedEventArgs e)
        {
            if (!ReferenceEquals(sender, this.SelectedListener.Value))
            {
                return;
            }

            if (Application.Current != null)
            {
                Application.Current.Dispatcher.BeginInvoke(
                    new Action(
                        delegate
                        {
                            if (e.Added)
                            {
                                foreach (var data in e.Data)
                                {
                                    var newVm = UniversalLogComponentViewModel.Create(data);
                                    this.m_allMessageCollection.Add(newVm);
                                    if (this.IsValid(newVm))
                                    {
                                        this.MessageCollection.Add(newVm);
                                    }
                                }
                            }
                        }
                    ),
                    System.Windows.Threading.DispatcherPriority.ApplicationIdle,
                    null);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void OnSelectedListenerChanged()
        {
            if (this.SelectedListener.Value == null)
            {
                this.MessageCollection.Clear();
                return;
            }

            this.m_allMessageCollection.Clear();
            foreach (var message in this.SelectedListener.Value.Buffer)
            {
                this.m_allMessageCollection.Add(UniversalLogComponentViewModel.Create(message));
            }

            this.FilterMessageCollection(true);
        }

        /// <summary>
        /// 
        /// </summary>
        private void OnFilterOptionChanged()
        {
            this.FilterMessageCollection(false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="clear"></param>
        private void FilterMessageCollection(bool clear)
        {
            if (clear)
            {
                this.MessageCollection.Clear();
            }

            int validCount = 0;
            foreach (var message in this.m_allMessageCollection)
            {
                bool valid = this.IsValid(message);
                if (valid && !this.MessageCollection.Contains(message))
                {
                    this.MessageCollection.Insert(validCount, message);
                }
                else if (!valid && this.MessageCollection.Contains(message))
                {
                    this.MessageCollection.Remove(message);
                }

                if (valid)
                {
                    validCount++;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        private bool IsValid(UniversalLogComponentViewModel viewModel)
        {
            if (viewModel.ComponentType == UniversalLogComponentTypes.Profiling)
            {
                return false;
            }

            if (viewModel.ComponentType == UniversalLogComponentTypes.Error && this.ShowErrors == false)
            {
                return false;
            }

            if (viewModel.ComponentType == UniversalLogComponentTypes.Warning && this.ShowWarnings == false)
            {
                return false;
            }

            if (viewModel.ComponentType == UniversalLogComponentTypes.DebugMessage && this.ShowDebugMessages == false)
            {
                return false;
            }

            if (viewModel.ComponentType == UniversalLogComponentTypes.Message && this.ShowMessages == false)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="messages"></param>
        /// <returns></returns>
        private string GetRichTextForMessages(IEnumerable<DataRow> messages)
        {
            string text = string.Empty;

            string startTag = "{\\rtf1\\ansi\\ansicpg1252\\uc1\\htmautsp\\deff2";
            string fontTable = "{\\fonttbl{\\f0\\fcharset0 Times New Roman;}{\\f2\\fcharset0 Segoe UI;}}";
            string colourTable = "{\\colortbl\\red0\\green0\\blue0;\\red255\\green255\\blue255;\\red31\\green73\\blue125;\\red255\\green192\\blue0;\\red255\\green0\\blue0;}";

            text += startTag;
            text += fontTable;
            text += colourTable;
            text += "\\loch\\hich\\dbch\\pard\\plain\\ltrpar\\itap0{\\lang1033\\fs22\\f2\\cf0 \\cf0\\ql";

            foreach (DataRow msg in messages)
            {
                text += GetRichTextForMessage(msg);
            }

            text += "}}";
            return text;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="messages"></param>
        /// <returns></returns>
        private string GetPlainTextForMessages(IEnumerable<DataRow> messages)
        {
            string text = string.Empty;

            string msgStart = string.Empty;
            foreach (DataRow msg in messages)
            {
                text += string.Format("{0}{1}", msgStart, GetPlainTextForMessage(msg));
                msgStart = Environment.NewLine;
            }

            return text;
        }
    
        /// <summary>
        /// 
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        private string GetRichTextForMessage(DataRow row)
        {
            string richText = string.Empty;
            string msgType = row["Type"].ToString();
            string timestamp = row["Timestamp"].ToString();
            string msg = row["Message"].ToString();
            string colour = string.Empty;

            if (msgType == "debug")
            {
                colour += "cf2";
            }
            else if (msgType == "message")
            {
                colour += "cf0";
            }
            else if (msgType == "warning")
            {
                colour += "cf3";
            }
            else if (msgType == "error")
            {
                colour += "cf4";
            }

            richText += "{\\lang1033\\fs18\\f2\\cf0 \\cf0\\ql{\\f2 {\\lang2057\\";
            richText += colour;
            richText += "\\ltrch ";
            richText += msgType;
            richText += "}{\\lang2057\\ltrch  ";
            richText += timestamp;
            richText += " \\endash  ";
            richText += msg;
            richText +=  "}\\li0\\ri0\\sa0\\sb0\\fi0\\ql\\par}";

            return richText;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        private string GetPlainTextForMessage(DataRow row)
        {
            string text = string.Empty;
            string msgType = row["Type"].ToString();
            string timestamp = row["Timestamp"].ToString();
            string msg = row["Message"].ToString();

            text += string.Format("{0} {1} - {2}", msgType, timestamp, msg).Trim();

            return text;
        }

        /// <summary>
        /// 
        /// </summary>
        private void UpdateText()
        {
            //this.Text = string.Empty;
            //if (this.SelectedFiles != null && this.SelectedListener.Value != null)
            //{
            //    DataTable table = this.SelectedListener.Value.Logs;
            //    if (this.OrderByFile)
            //    {
            //        var messages = (from r in table.AsEnumerable()
            //                        where SelectedFiles.Contains(r["Filename"] as string)
            //                        where SelectedContexts.Contains(r["Context"] as string)
            //                        where SelectedMessageTypes.Contains(r["Type"] as string)
            //                        orderby r["Filename"]
            //                        select r).Where(ExpressionMatch);

            ////        this.Text = GetPlainTextForMessages(messages);
            //        UpdateMessageCollection(messages);
            //    }
            //    else
            //    {
            //        var messages = (from r in table.AsEnumerable()
            //                        where SelectedFiles.Contains(r["Filename"] as string)
            //                        where SelectedContexts.Contains(r["Context"] as string)
            //                        where SelectedMessageTypes.Contains(r["Type"] as string)
            //                        orderby r["Timestamp"]
            //                        select r).Where(ExpressionMatch);

            //     //   this.Text = GetPlainTextForMessages(messages);
            //        UpdateMessageCollection(messages);
            //    }
            //}
        }

        private bool ExpressionMatch(DataRow row)
        {
            if (string.IsNullOrWhiteSpace(this.Expression) || m_regularExpression == null)
                return true;

            if (this.CaseSensitive)
            {
                if (this.m_regularExpression.IsMatch(row["Message"] as string))
                    return true;
            }
            else
            {
                if (this.m_regularExpression.IsMatch(row["Message"] as string))
                    return true;
            }

            return false;
        }
        #endregion
    } // UniversalLogWindowViewModel

    /// <summary>
    /// 
    /// </summary>
    public class WatermarkTextBox : TextBox
    {
        #region Properties
        #region Watermark
        public static readonly DependencyProperty WatermarkProperty = DependencyProperty.Register("Watermark", typeof(object), typeof(WatermarkTextBox), new UIPropertyMetadata(null));
        public object Watermark
        {
            get { return (object)GetValue(WatermarkProperty); }
            set { SetValue(WatermarkProperty, value); }
        }

        #endregion //Watermark
        #region WatermarkTemplate

        public static readonly DependencyProperty WatermarkTemplateProperty = DependencyProperty.Register("WatermarkTemplate", typeof(DataTemplate), typeof(WatermarkTextBox), new UIPropertyMetadata(null));
        public DataTemplate WatermarkTemplate
        {
            get { return (DataTemplate)GetValue(WatermarkTemplateProperty); }
            set { SetValue(WatermarkTemplateProperty, value); }
        }

        #endregion //WatermarkTemplate
        #endregion //Properties

        #region Constructors
        static WatermarkTextBox()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(WatermarkTextBox), new FrameworkPropertyMetadata(typeof(WatermarkTextBox)));
        }
        #endregion //Constructors

        #region
        protected override void OnKeyDown(System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                var expression = GetBindingExpression(WatermarkTextBox.TextProperty);
                expression.UpdateSource();
            }
            base.OnKeyDown(e);
        }
        #endregion
    }

} // Workbench.UI