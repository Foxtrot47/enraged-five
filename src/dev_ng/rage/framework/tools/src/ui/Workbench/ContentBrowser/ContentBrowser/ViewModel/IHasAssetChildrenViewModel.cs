﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Base.Collections;

namespace ContentBrowser.ViewModel
{
    /// <summary>
    /// Interface for any asset view model that also contains child assets
    /// </summary>
    public interface IHasAssetChildrenViewModel : IAssetViewModel
    {
        #region Properties

        /// <summary>
        /// The collection of assets belonging to this container
        /// </summary>
        ObservableCollection<IAssetViewModel> AssetChildren { get; }

        /// <summary>
        /// Collection of the file assets this container has in it.
        /// </summary>
        ObservableCollection<IGridViewModel> GridAssets { get; }
        
        #endregion // Properties

        #region Methods
        
        /// <summary>
        /// Returns true if this container contains a asset child
        /// with the given name
        /// </summary>
        Boolean ContainsAssetChild(String name);

        /// <summary>
        /// Returns true if this container contains a asset child
        /// with the same model as the given
        /// </summary>
        Boolean ContainsAssetChild(IAsset asset);

        /// <summary>
        /// Returns the asset in this containers asset children
        /// with the given name or null if not present
        /// </summary>
        IAssetViewModel GetAssetChild(String name);

        /// <summary>
        /// Returns the asset in this containers asset children
        /// with the given asset model or null if not present
        /// </summary>
        IAssetViewModel GetAssetChild(IAsset asset);

        /// <summary>
        /// Returns true if this container contains a grid asset
        /// with the given name
        /// </summary>
        Boolean ContainsGridAsset(String name);

        /// <summary>
        /// Returns true if this container contains a grid asset
        /// with the model
        /// </summary>
        Boolean ContainsGridAsset(IAsset asset);

        /// <summary>
        /// Returns the grid asset in this containers grid assets
        /// with the given name or null if not present
        /// </summary>
        IGridViewModel GetGridAsset(String name);

        /// <summary>
        /// Returns the grid asset in this containers grid assets
        /// with the given model or null if not present
        /// </summary>
        IGridViewModel GetGridAsset(IAsset asset);

        #endregion // Methods
    } // IHasAssetChildrenViewModel
} // ContentBrowser.ViewModel
