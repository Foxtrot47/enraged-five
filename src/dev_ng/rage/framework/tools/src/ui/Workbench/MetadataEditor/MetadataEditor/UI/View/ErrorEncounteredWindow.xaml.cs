﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MetadataEditor.UI.View
{
    /// <summary>
    /// Interaction logic for ErrorEncounteredWindow.xaml
    /// </summary>
    public partial class ErrorEncounteredWindow : Window
    {
        public event EventHandler BugRequested;

        public ErrorEncounteredWindow()
        {
            InitializeComponent();
        }

        private void AddBugClicked(object sender, EventArgs e)
        {
            if (BugRequested == null)
            {
                return;
            }

            BugRequested(this, EventArgs.Empty);
        }

        private void OkClicked(object sender, EventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
