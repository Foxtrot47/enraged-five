﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Collections.ObjectModel;
using RSG.Model.Common;
using RSG.Model.Common.Map;
using RSG.Base.Windows.Helpers;

namespace ContentBrowser.AddIn.ReportBrowser.SectionSelection
{
    /// <summary>
    /// 
    /// </summary>
    public class SectionSelectionViewModel
    {
        #region Fields
        /// <summary>
        /// 
        /// </summary>
        private object m_sync = new object();
        private RelayCommand m_uncheckAll;
        private RelayCommand m_checkAll;
        #endregion

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public RelayCommand UnCheckAllCommand
        {
            get
            {
                lock (m_sync)
                {
                    if (m_uncheckAll == null)
                    {
                        m_uncheckAll = new RelayCommand(param => UnCheckAll(param), null);
                    }
                }
                return m_uncheckAll;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand CheckAllCommand
        {
            get
            {
                lock (m_sync)
                {
                    if (m_checkAll == null)
                    {
                        m_checkAll = new RelayCommand(param => CheckAll(param), null);
                    }
                }
                return m_checkAll;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<SectionViewModel> Sections
        {
            get;
            set;
        }
        #endregion

        #region Commands
        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        private void UnCheckAll(object param)
        {
            this.ModifiyCheck(this.Sections[0], false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        private void CheckAll(object param)
        {
            this.ModifiyCheck(this.Sections[0], true);
        }
        #endregion // Commands

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        public SectionSelectionViewModel(ILevel level)
        {
            this.Sections = new ObservableCollection<SectionViewModel>();

            IMapHierarchy hierarchy = level.MapHierarchy;
            if (hierarchy != null)
            {
                foreach (IMapNode child in hierarchy.ChildNodes)
                {
                    Sections.Add(new SectionViewModel(child));
                }
            }
        }
        #endregion // Constructor(s)

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sections"></param>
        public void GetSelectedSections(ref IList<IMapSection> sections)
        {
            GetSelectedSections(this.Sections[0], ref sections);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="root"></param>
        /// <param name="sections"></param>
        private void GetSelectedSections(SectionViewModel root, ref IList<IMapSection> sections)
        {
            if (root.IsChecked && root.Model is IMapSection)
            {
                sections.Add(root.Model as IMapSection);
            }

            foreach (SectionViewModel child in root.Children)
            {
                GetSelectedSections(child, ref sections);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="root"></param>
        /// <param name="state"></param>
        private void ModifiyCheck(SectionViewModel root, bool state)
        {
            root.IsChecked = state;
            foreach (SectionViewModel child in root.Children)
            {
                ModifiyCheck(child, state);
            }
        }
        #endregion // MEthods
    } // SectionSelectionViewModel
}
