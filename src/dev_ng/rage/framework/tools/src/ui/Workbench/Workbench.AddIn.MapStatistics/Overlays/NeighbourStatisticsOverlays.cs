﻿using System;
using System.Collections.Generic;
using System.Linq;
using RSG.Base.Collections;
using RSG.Model.Common;
using RSG.Model.Common.Map;

namespace Workbench.AddIn.MapStatistics.Overlays
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(ExtensionPoints.NeighbourStatisticsOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class UniquePropsOverlay : StatisticOverlayBase
    {
        #region Constants
        /// <summary>
        /// 
        /// </summary>
        private const string c_name = "Unique Props";
        private const string c_description = "";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ObservableDictionary<IMapArchetype, int> UniqueArchetypes
        {
            get { return m_uniqueArchetypes; }
            set
            {
                SetPropertyValue(value, () => UniqueArchetypes,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_uniqueArchetypes = (ObservableDictionary<IMapArchetype, int>)newValue;
                        }
                ));
            }
        }
        private ObservableDictionary<IMapArchetype, int> m_uniqueArchetypes;

        /// <summary>
        /// 
        /// </summary>
        public ObservableDictionary<IMapArchetype, int> NonUniqueArchetypes
        {
            get { return m_nonUniqueArchetypes; }
            set
            {
                SetPropertyValue(value, () => NonUniqueArchetypes,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_nonUniqueArchetypes = (ObservableDictionary<IMapArchetype, int>)newValue;
                        }
                ));
            }
        }
        private ObservableDictionary<IMapArchetype, int> m_nonUniqueArchetypes;
        #endregion // Properties

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public UniquePropsOverlay()
            : base(c_name, c_description)
        {
            m_dontShowInteriorOptions = true;
            m_dontShowObjectOptions = true;
            m_dontShowUniqueOptions = true;
            m_includeDynamic = true;
            m_includeNonDynamic = true;

            m_uniqueArchetypes = new ObservableDictionary<IMapArchetype, int>();
            m_nonUniqueArchetypes = new ObservableDictionary<IMapArchetype, int>();
        }
        #endregion // Constructor

        #region Overrides
        /// <summary>
        /// Gets the control that should be shown in the overlay details panel
        /// </summary>
        public override System.Windows.Controls.Control GetOverlayControl()
        {
            return new NeighbourUniquePropOverlay();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="props"></param>
        protected void GetStatisticValue(IMapSection section, ref IDictionary<IMapArchetype, int> props)
        {
            // Go over all entities that reference a valid archetype
            foreach (IEntity entity in section.ChildEntities.Where(i => i.ReferencedArchetype != null))
            {
                if (!entity.IsReference || entity.IsInteriorReference)
                {
                    continue;
                }

                ISimpleMapArchetype simple = entity.ReferencedArchetype as ISimpleMapArchetype;
                IMapArchetype archetype = entity.ReferencedArchetype as IMapArchetype;
                if (simple == null || archetype == null)
                {
                    continue;
                }

                bool isDynamic = simple.IsDynamic;
                if (isDynamic && !this.IncludeDynamic)
                {
                    continue;
                }
                
                if (!isDynamic && !this.IncludeNonDynamic)
                {
                    continue;
                }


                if (!props.ContainsKey(archetype))
                {
                    props[archetype] = 0;
                }

                props[archetype]++;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="propgroup"></param>
        /// <returns></returns>
        protected override double GetStatisticValue(IMapSection section, IMapSection propgroup)
        {
            IDictionary<IMapArchetype, int> props = new Dictionary<IMapArchetype, int>();
            if (section != null)
                GetStatisticValue(section, ref props);
            if (propgroup != null)
                GetStatisticValue(propgroup, ref props);

            IList<IMapSection> neighbours = section.Neighbours[(int)NeighbourMode.Distance];
            if (neighbours.Count == 0 && propgroup != null)
                neighbours = propgroup.Neighbours[(int)NeighbourMode.Distance];

            if (neighbours.Count != 0)
            {
                foreach (IMapSection neighbour in neighbours)
                {
                    if (neighbour != section && neighbour != propgroup)
                    {
                        foreach (IEntity entity in neighbour.ChildEntities.Where(item => item.ReferencedArchetype != null))
                        {
                            IMapArchetype archetype = (IMapArchetype)entity.ReferencedArchetype;

                            if (props.ContainsKey(archetype))
                            {
                                props.Remove(archetype);
                            }
                        }
                    }
                }
                return props.Count;
            }

            return 0.0;
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void OnSelectedSectionChanged()
        {
            this.UniqueArchetypes.Clear();
            this.NonUniqueArchetypes.Clear();

            if (this.SelectedSection != null)
            {
                IDictionary<IMapArchetype, int> uniqueProps = new Dictionary<IMapArchetype, int>();

                if (this.SelectedSection != null)
                {
                    GetStatisticValue(this.SelectedSection, ref uniqueProps);
                }
                if (this.SelectedSection.PropGroup != null && this.IncludePropGroup == true)
                {
                    GetStatisticValue(this.SelectedSection.PropGroup, ref uniqueProps);
                }

                foreach (IMapArchetype unique in uniqueProps.Keys)
                {
                    this.NonUniqueArchetypes.Add(unique, 1);
                }

                IList<IMapSection> neighbours = this.SelectedSection.Neighbours[(int)NeighbourMode.Distance];
                if (neighbours.Count == 0 && this.SelectedSection.PropGroup != null)
                {
                    neighbours = this.SelectedSection.PropGroup.Neighbours[(int)NeighbourMode.Distance];
                }

                foreach (IMapSection neighbour in neighbours)
                {
                    ISet<IMapArchetype> registeredArchetypesInNeighbour = new HashSet<IMapArchetype>();

                    if (neighbour != this.SelectedSection && neighbour != this.SelectedSection.PropGroup)
                    {
                        foreach (IEntity entity in neighbour.ChildEntities.Where(item => item.ReferencedArchetype != null))
                        {
                            IMapArchetype archetype = (IMapArchetype)entity.ReferencedArchetype;
                            if (uniqueProps.ContainsKey(archetype))
                            {
                                uniqueProps.Remove(archetype);
                            }

                            if (NonUniqueArchetypes.ContainsKey(archetype))
                            {
                                if (!registeredArchetypesInNeighbour.Contains(archetype))
                                {
                                    registeredArchetypesInNeighbour.Add(archetype);
                                    this.NonUniqueArchetypes[archetype]++;
                                }
                            }
                        }
                    }
                }
                
                foreach (KeyValuePair<IMapArchetype, int> unique in uniqueProps)
                {
                    this.UniqueArchetypes.Add(unique.Key, unique.Value);
                }

                List<IMapArchetype> remove = (NonUniqueArchetypes.Where(item => item.Value == 1).Select(item => item.Key)).ToList();

                foreach (IMapArchetype removeDefinition in remove)
                {
                    this.NonUniqueArchetypes.Remove(removeDefinition);
                }
            }
        }
        #endregion // Overrides
    } // GeometryCountOverlay
} // Workbench.AddIn.MapStatistics.Overlays
 
