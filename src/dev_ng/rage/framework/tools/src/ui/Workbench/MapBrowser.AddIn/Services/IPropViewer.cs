﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Map.AddIn.Services
{
    public interface IPropViewer
    {
        /// <summary>
        /// Creates a new document using a map secion to display the ide props
        /// on.
        /// </summary>
        void CreateViewerDocument(RSG.Model.Map.MapSection section);
    }
}
