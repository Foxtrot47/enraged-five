﻿using System;
using System.ComponentModel.Composition;
using System.Windows.Media.Imaging;
using System.Net;
using System.IO;
using System.Text;
using System.Windows.Input;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Metadata.Model;
using RSG.Model.LiveEditing;
using MetadataEditor.AddIn.MetaEditor.ViewModel;
using MetadataEditor.AddIn.MetaEditor.View;
using WidgetEditor.AddIn;
using Workbench.AddIn;
using Workbench.AddIn.Commands;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.Services;
using Workbench.AddIn.Services.File;

namespace MetadataEditor.AddIn.MetaEditor.UI.Toolbar
{
    /// <summary>
    /// Live editing connection command.
    /// </summary>
    [ExportExtension(WidgetEditor.AddIn.ExtensionPoints.LiveEditingToolbar, typeof(IWorkbenchCommand))]
    public class LiveEditingConnect : WorkbenchMenuItemBase, IPartImportsSatisfiedNotification
    {
        #region Constants
        /// <summary>
        /// Unique GUID for this button
        /// </summary>
        public static readonly Guid GUID = new Guid("0AA1EDCD-52FB-4E46-B85C-A043B1545EA5");
        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// Proxy service for getting the currently connected game
        /// </summary>
        [ImportExtension(WidgetEditor.AddIn.CompositionPoints.ProxyService, typeof(IProxyService))]
        private IProxyService ProxyService { get; set; }

        /// <summary>
        /// Metadata open service
        /// </summary>
        [ImportExtension(MetadataEditor.AddIn.CompositionPoints.LiveEditingService, typeof(ILiveEditingService))]
        private ILiveEditingService LiveEditingService { get; set; }

        /// <summary>
        /// Metadata open service
        /// </summary>
        [ImportExtension(MetadataEditor.AddIn.CompositionPoints.MetadataOpenService, typeof(IOpenService))]
        private IOpenService MetadataOpenService { get; set; }

        /// <summary>
        /// MEF import for workbench Layout Manager component.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager, typeof(ILayoutManager))]
        public ILayoutManager LayoutManager { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(MetadataEditor.AddIn.CompositionPoints.StructureDictionary, typeof(IStructureDictionary))]
        private Lazy<IStructureDictionary> Structures { get; set; }

        /// <summary>
        /// MEF import for message box service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.MessageService, typeof(Workbench.AddIn.Services.IMessageService))]
        private Lazy<IMessageService> MessageService { get; set; }

        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(Workbench.AddIn.Services.IConfigurationService))]
        private Lazy<IConfigurationService> ConfigService { get; set; }

        private bool currentlyOpeningDocument = false;
        #endregion // MEF Imports

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        private bool CanLiveEditCurrentDocument
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public LiveEditingConnect()
            : base()
        {
            this.ID = GUID;
            this.IsToggle = true;
            this.ToolTip = "Indicates live-link status with the game.";
            this.RelativeToolBarID = new Guid(WidgetEditor.AddIn.ExtensionPoints.LiveEditingToolbarSeperator);
            this.Direction = Workbench.AddIn.Services.Direction.After;
            this.ImageSource = new BitmapImage(new Uri("pack://application:,,,/MetadataEditor.AddIn.MetaEditor;component/Resources/link.png"));
            this.CanLiveEditCurrentDocument = false;
        }
        #endregion // Constructor(s)

        #region ICommand Implementation
        /// <summary>
        /// Gets called when deciding whether this menu item's
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            bool isConnected = this.ProxyService.IsGameConnected;
            bool documentValid = false;
            IMetadataView view = null;
            foreach (var document in LayoutManager.Documents)
            {
                if (document.IsActiveDocument)
                {
                    view = document as IMetadataView;
                    break;
                }
            }

            if (view != null)
            {
                documentValid = LiveEditingService.GetLiveEditingServiceForFile(view.Path) != null;
            }
           
            if (!isConnected)
            {
                this.ImageSource = new BitmapImage(new Uri("pack://application:,,,/MetadataEditor.AddIn.MetaEditor;component/Resources/unlink.png"));
                this.ToolTip = "Unable to live edit due to no game connection detected/selected.";
            }
            else if (!documentValid)
            {
                this.ImageSource = new BitmapImage(new Uri("pack://application:,,,/MetadataEditor.AddIn.MetaEditor;component/Resources/unlink.png"));
                this.ToolTip = "Current document is not valid to be live edited.";
            }

            if (isConnected && documentValid)
            {
                this.ImageSource = new BitmapImage(new Uri("pack://application:,,,/MetadataEditor.AddIn.MetaEditor;component/Resources/link.png"));
                this.ToolTip = "Indicates live-link status with the game.";
                return true;
            }

            return false;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
            IMetadataView view = null;
            IDocumentBase metadataDocument = null;
            foreach (IDocumentBase document in LayoutManager.Documents)
            {
                if (document.IsActiveDocument)
                {
                    if (document.IsActiveDocument)
                    {
                        view = document as IMetadataView;
                        metadataDocument = document;
                        break;
                    }
                }
            }

            if (view != null)
            {
                // If the user asked to setup the live-link we need to do some ground work before allowing them to use it
                if (IsChecked)
                {
                    bool liveLinkEnabled = false;

                    // We first need to check whether the game's data matches the currently edited data
                    IObjectService objService = LiveEditingService.GetLiveEditingServiceForFile(view.Path);
                    MetaFile gameMetaFile = GetMetaFileFromGame(objService);
                    MetaFile localMetaFile = view.GetMetadataViewModel().GetMetaFile();

                    if (gameMetaFile != null)
                    {
                        // If the files are identical we can simply enable the live-link in the view model
                        if (gameMetaFile.Members.HasSameValue(localMetaFile.Members))
                        {
                            view.GetMetadataViewModel().EnableLiveLink(objService.Url);
                            liveLinkEnabled = true;
                        }
                        else
                        {
                            // If they aren't identical prompt the user asking him what he'd like to do
                            // He can either replace the local data with that from the game or send the local
                            // data to the game.
                            ChooseMetadataSourceDialog dlg = new ChooseMetadataSourceDialog();
                            bool? res = dlg.ShowDialog();
                            if (res != null && res != false)
                            {
                                if (dlg.UseGameData)
                                {
                                    bool closed = true;
                                    this.currentlyOpeningDocument = true;
                                    this.LayoutManager.CloseDocument(metadataDocument);
                                    foreach (IDocumentBase document in LayoutManager.Documents)
                                    {
                                        if (object.ReferenceEquals(document, metadataDocument))
                                        {
                                            closed = false;
                                            break;
                                        }
                                    }

                                    if (closed)
                                    {
                                        Workbench.AddIn.UI.Layout.IDocumentBase doc = this.MetadataOpenService.Open(gameMetaFile);
                                        if (doc != null)
                                        {
                                            doc.OpenService = this.MetadataOpenService;
                                            gameMetaFile.Filename = metadataDocument.Path;
                                            doc.Path = metadataDocument.Path;
                                            doc.Title = Path.GetFileName(metadataDocument.Path);
                                            this.LayoutManager.ShowDocument(doc);
                                        }

                                        (doc as IMetadataView).GetMetadataViewModel().EnableLiveLink(objService.Url);
                                        liveLinkEnabled = true;
                                    }

                                    this.currentlyOpeningDocument = false;
                                }
                                else
                                {
                                    view.GetMetadataViewModel().EnableLiveLink(objService.Url, true);
                                    liveLinkEnabled = true;
                                }
                            }
                        }
                    }

                    // If for whatever reason the live-link wasn't enabled, uncheck the toggle button
                    if (!liveLinkEnabled)
                    {
                        IsChecked = false;
                    }
                    else
                    {
                        IsChecked = true;
                    }
                }
                else
                {
                    view.GetMetadataViewModel().DisableLiveLink();
                }
            }
        }
        #endregion // ICommand Implementation

        #region IPartImportsSatisfiedNotification Implementation
        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {
            LayoutManager.ActiveDocumentChanged += LayoutManager_ActiveDocumentChanged;
            ProxyService.GameConnectionChanged += ProxyService_GameConnectionChanged;
        }
        #endregion // IPartImportsSatisfiedNotification Implementation

        #region Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProxyService_GameConnectionChanged(object sender, GameConnectionChangedEventArgs e)
        {
            // If the game connection changes, disable all the live-linked documents
            bool liveLinkLost = false;
            IMetadataView active = null;
            foreach (IDocumentBase document in LayoutManager.Documents)
            {
                IMetadataView view = document as IMetadataView;
                if (view != null)
                {
                    if (document.IsActiveDocument)
                    {
                        active = view;
                    }
                    if (view.GetMetadataViewModel().IsLiveEditing)
                    {
                        view.GetMetadataViewModel().DisableLiveLink();
                        liveLinkLost = true;
                    }
                }
            }

            // Update the state of the toggle button based on whether the current document is a metadata file and whether its live-editing
            if (active != null && IsChecked != active.GetMetadataViewModel().IsLiveEditing)
            {
                IsChecked = active.GetMetadataViewModel().IsLiveEditing;
            }

            // Inform the user that the live-link was severed
            if (liveLinkLost)
            {
                MessageService.Value.Show("Changing the selected game connection has severed the connection for a couple of live-linked metadata documents to ensure that the local file and game files aren't out of sync.");
            }

            // Refresh the button's state
            CommandManager.InvalidateRequerySuggested();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LayoutManager_ActiveDocumentChanged(object sender, EventArgs e)
        {
            if (this.currentlyOpeningDocument == true)
            {
                return;
            }

            // Determine whether the document that is now selected is a metadata document and whether it's live-linked
            IMetadataView view = null;
            foreach (var document in LayoutManager.Documents)
            {
                if (document.IsActiveDocument)
                {
                    view = document as IMetadataView;
                    break;
                }
            }

            if (view != null)
            {
                IsChecked = view.GetMetadataViewModel().IsLiveEditing;
                CanLiveEditCurrentDocument = (LiveEditingService.GetLiveEditingServiceForFile(view.Path) != null);
            }
            else
            {
                IsChecked = false;
                CanLiveEditCurrentDocument = false;
            }

            // Do this after we've updated the can live edit flag
            CommandManager.InvalidateRequerySuggested();
        }
        #endregion // Event Handlers

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        private MetaFile GetMetaFileFromGame(IObjectService service)
        {
            MetaFileViewModel vm = null;
            string metadata = null;
            using (WebClient client = new WebClient())
            {
                try
                {
                    metadata = client.DownloadString(service.Url);
                }
                catch (Exception ex)
                {
                    string msg = string.Format("Could not connect to service '{0}' at URL: {1}", service.Name, service.Url);
                    Log.Log__Exception(ex, msg);
                    MessageService.Value.Show(msg);
                    return null;
                }
            }

            if (metadata == null)
            {
                string msg = string.Format("The game returned a empty string for the service '{0}' at URL: {1}", service.Name, service.Url);
                Log.Log__Error(msg);
                MessageService.Value.Show(msg);
                return null;
            }

            try
            {
                byte[] byteArray = Encoding.UTF8.GetBytes(metadata);
                MemoryStream stream = new MemoryStream(byteArray);
                IBranch branch = this.ConfigService.Value.Config.Project.DefaultBranch;
                stream.Position = 0;
                vm = MetaFileViewModel.Create(branch, stream, this.Structures.Value.Structures);
            }
            catch (Exception ex)
            {
                string msg = string.Format("Could not create a valid metadata file from the data the game sent from the service '{0}' at URL: {1}", service.Name, service.Url);
                Log.Log__Exception(ex, msg);
                MessageService.Value.Show(msg);
                return null;
            }

            return (vm != null ? (MetaFile)vm.Model : null);
        }
        #endregion // Private Methods
    } // LiveEditingConnectButton
}
