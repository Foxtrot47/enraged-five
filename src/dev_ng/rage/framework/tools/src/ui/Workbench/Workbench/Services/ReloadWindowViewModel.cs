﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;

namespace Workbench.Services
{
    public class ReloadWindowViewModel : ViewModelBase
    {
        #region Fields
        private string m_filename;
        #endregion

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public string Filename
        {
            get { return m_filename; }
            set
            {
                SetPropertyValue(value, m_filename, () => this.Filename,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_filename = (string)newValue;
                        }
                ));
            }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        public ReloadWindowViewModel(string filename)
        {
            this.Filename = filename;
        }
        #endregion
    } // ReloadWindowViewModel
}
