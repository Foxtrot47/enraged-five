﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Configuration;
using RSG.Metadata.Model;
using RSG.Metadata.Parser;
using MetadataEditor.AddIn;
using MetadataEditor.AddIn.View;
using Workbench.AddIn;
using Workbench.AddIn.UI.Layout;
using MetadataEditor.AddIn.MetaEditor.ViewModel;
using Workbench.AddIn.Services;

namespace MetadataEditor.AddIn.MetaEditor
{
    [ExportExtension(MetadataEditor.AddIn.ExtensionPoints.DefaultMetadataDocument, typeof(IMetadataDocumentProxy))]
    public class MetadataEditorBasicProxy : IMetadataDocumentProxy
    {
        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(Workbench.AddIn.Services.IConfigurationService))]
        private Lazy<IConfigurationService> ConfigurationService { get; set; }

        /// <summary>
        /// Array of String Metadata types this Metadata Document can view
        /// (e.g. "*", "::rage::cutfCutSceneFile2").
        /// </summary>
        public String[] SupportedMetadataTypes
        {
            get { return new String[] { "*" }; }
        }

        /// <summary>
        /// 
        /// </summary>
        public MetadataDocumentType DocumentType
        {
            get { return (MetadataDocumentType.Default); }
        }

        /// <summary>
        /// Array of support document GUIDs.
        /// </summary>
        public Guid[] SupportedContent
        {
            get { return m_supportedContent; }
        }
        private Guid[] m_supportedContent = { new Guid(CompositionPoints.MetaEditorBasic) };

        /// <summary>
        /// 
        /// </summary>
        /// <param name="metaFile"></param>
        /// <returns></returns>
        public bool CreateFile(out IMetaFile metaFile, string filename, StructureDictionary structureDictionary, Structure rootDefinition)
        {
            metaFile = new MetaFile(filename, structureDictionary, true);
            return true;
        }

        /// <summary>
        /// Used to create a new instance of this document type from the given model
        /// </summary>
        public Boolean OpenDocument(out IDocumentBase document, IMetaFile file)
        {
            document = null;
            if (file != null)
            {
                IBranch branch = this.ConfigurationService.Value.Config.Project.DefaultBranch;
                MetaFileViewModel viewModel = new MetaFileViewModel(branch, file);
                document = new MetaEditorBasicView(viewModel);

                return true;
            }
            return false;
        }

        /// <summary>
        /// Used to create a new instance of this document type
        /// </summary>
        public Boolean CreateNewDocument(out IDocumentBase document, out RSG.Base.Editor.IModel model, RSG.Metadata.Parser.Structure definition, StructureDictionary dictionary)
        {
            IBranch branch = this.ConfigurationService.Value.Config.Project.DefaultBranch;
            model = MetaFileViewModel.Create(branch, definition);
            document = new MetaEditorBasicView(model as MetaFileViewModel);
            return true;
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public MetadataEditorBasicProxy()
        {
        }
    }

    [ExportExtension(Workbench.AddIn.ExtensionPoints.SearchableTags, typeof(Workbench.AddIn.UI.Layout.ISearchableTags))]
    public class SearchableTags : Workbench.AddIn.UI.Layout.ISearchableTags
    {
        #region ISearchableTags Members

        public List<string> Tags
        {
            get 
            {
                return new List<string>()
                {
                    "Tunables"
                };
            }
        }

        #endregion
    }
}
