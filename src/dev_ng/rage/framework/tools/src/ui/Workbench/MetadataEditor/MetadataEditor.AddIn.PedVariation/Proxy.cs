﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MetadataEditor.AddIn;
using Workbench.AddIn;
using Workbench.AddIn.UI.Layout;
using RSG.Metadata.Model;
using MetadataEditor.AddIn.View;
using MetadataEditor.AddIn.PedVariation.ViewModel;
using MetadataEditor.AddIn.PedVariation.View;
using RSG.Metadata.Parser;
using RSG.Metadata.Characters;
using Workbench.AddIn.Services;
using System.IO;

namespace MetadataEditor.AddIn.PedVariation
{
    [ExportExtension(MetadataEditor.AddIn.ExtensionPoints.MetadataDocument, typeof(IMetadataDocumentProxy))]
    public class Proxy : IMetadataDocumentProxy
    {
        #region MEF Imports
        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(IConfigurationService))]
        public IConfigurationService ConfigService { get; set; }

        [ImportExtension(MetadataEditor.AddIn.CompositionPoints.StructureDictionary, typeof(IStructureDictionary))]
        public IStructureDictionary StructureDictionaryService { get; set; }

        /// <summary>
        /// The perforce sync service
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.PerforceSyncService, typeof(IPerforceSyncService))]
        private IPerforceSyncService PerforceSyncService { get; set; }
        #endregion


        /// <summary>
        /// Array of String Metadata types this Metadata Document can view
        /// (e.g. "*", "::rage::cutfCutSceneFile2").
        /// </summary>
        public String[] SupportedMetadataTypes
        {
            get { return new String[] { "CPedVariationInfo" }; }
        }

        /// <summary>
        /// 
        /// </summary>
        public MetadataDocumentType DocumentType
        {
            get { return (MetadataDocumentType.TypeSpecific); }
        }

        /// <summary>
        /// Array of support document GUIDs.
        /// </summary>
        public Guid[] SupportedContent
        {
            get { return m_supportedContent; }
        }
        private Guid[] m_supportedContent = { MetaEditorView.CONSTANT_ID };

        /// <summary>
        /// 
        /// </summary>
        /// <param name="metaFile"></param>
        /// <returns></returns>
        public bool CreateFile(out IMetaFile metaFile, string filename, StructureDictionary structureDictionary, Structure rootDefinition)
        {
            string audioValuesFilename = Path.Combine(ConfigService.GameConfig.MetadataDir, "workbench", "PedVariation", "ComponentAudioIds.meta");
            int syncedFiles = 0;
            PerforceSyncResult result =
                PerforceSyncService.Show("You need to have latest on the audio values before opening a ped metadata file.",
                                         new string[] { audioValuesFilename }, ref syncedFiles, true, false);

            if (result == PerforceSyncResult.SyncedAll || result == PerforceSyncResult.Ok)
            {
                RSG.Metadata.Model.MetaFile audioValues = new MetaFile(audioValuesFilename, structureDictionary);
                metaFile = new PedVariationMetaFile(filename, rootDefinition, structureDictionary, audioValues);
                return true;
            }

            metaFile = null;
            return false;
        }

        /// <summary>
        /// Used to create a new instance of this document type from the given model
        /// </summary>
        public Boolean OpenDocument(out IDocumentBase document, IMetaFile file)
        {
            document = null;
            if (file != null)
            {
                MetaFileViewModel viewModel = new MetaFileViewModel(file as PedVariationMetaFile, StructureDictionaryService.Structures);
                document = new MetaEditorView(viewModel);

                return true;
            }
            return false;
        }

        /// <summary>
        /// Used to create a new instance of this document type
        /// </summary>
        public Boolean CreateNewDocument(out IDocumentBase document, out RSG.Base.Editor.IModel model, RSG.Metadata.Parser.Structure definition, StructureDictionary structureDictionary)
        {
            model = MetaFileViewModel.Create(definition, structureDictionary);
            document = new MetaEditorView(model as MetaFileViewModel);
            return true;
        }

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public Proxy()
        {
        }
        #endregion
    }
}
