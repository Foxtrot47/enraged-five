﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Workbench.AddIn.SceneOverrideEditor
{
    public class LODDistanceEdit
    {
        public LODDistanceEdit(uint hash, uint guid, float posX, float posY, float posZ, String imapName, String modelName, float distance, float childDistance)
        {
            Guid = guid;
            Hash = hash;
            PosX = posX;
            PosY = posY;
            PosZ = posZ;
            IMAPName = imapName;
            ModelName = modelName;
            Distance = distance;
            ChildDistance = childDistance;
        }

        public uint Guid { get; private set; }
        public uint Hash { get; private set; }
        public float PosX { get; private set; }
        public float PosY { get; private set; }
        public float PosZ { get; private set; }
        public String IMAPName { get; private set; }
        public String ModelName { get; private set; }
        public float Distance { get; set; }
        public float ChildDistance { get; set; }

        public bool HasDistance { get { return Distance != invalidDistance_; } }
        public bool HasChildDistance { get { return ChildDistance != invalidDistance_; } }

        public String PositionText
        {
            get
            {
                return String.Format("[{0}, {1}, {2}]", PosX.ToString("0.00"), PosY.ToString("0.00"), PosZ.ToString("0.00"));
            }
        }

        public String LODDistanceText
        {
            get
            {
                return Distance.ToString("0.00");
            }
        }

        public String ChildLODDistanceText
        {
            get
            {
                return ChildDistance.ToString("0.00");
            }
        }

        private static float invalidDistance_ = -1.0f;
    }
}
