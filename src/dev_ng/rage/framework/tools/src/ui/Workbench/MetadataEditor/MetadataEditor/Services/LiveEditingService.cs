﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MetadataEditor.AddIn;
using Workbench.AddIn;
using WidgetEditor.AddIn;
using System.ComponentModel.Composition;
using RSG.Model.LiveEditing;
using Workbench.AddIn.Services;
using System.ComponentModel;
using RSG.Model.Common;

namespace MetadataEditor.Services
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(MetadataEditor.AddIn.CompositionPoints.LiveEditingService, typeof(ILiveEditingService))]
    public class LiveEditingService : ILiveEditingService, IPartImportsSatisfiedNotification
    {
        #region Constants
        /// <summary>
        /// The port that the game is hosting it's REST services on.
        /// </summary>
        private const uint _gameRestServicePort = 7890;
        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// MEF import for workbench configuration service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(IConfigurationService))]
        private IConfigurationService ConfigurationService { get; set; }

        /// <summary>
        /// Proxy service for getting the currently connected game
        /// </summary>
        [ImportExtension(WidgetEditor.AddIn.CompositionPoints.ProxyService, typeof(IProxyService))]
        private IProxyService ProxyService { get; set; }
        #endregion // MEF Imports

        #region Events
        /// <summary>
        /// Event that gets fired when the root service has changed
        /// </summary>
        public event EventHandler RootServiceChanged;
        #endregion // Events

        #region Properties
        /// <summary>
        /// The root live editing service of the game that we are currently connected to
        /// </summary>
        public IDirectoryService LiveEditRootService
        {
            get;
            protected set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default Constructor
        /// </summary>
        public LiveEditingService()
        {
            LiveEditRootService = null;
        }
        #endregion // Constructor(s)

        #region IPartImportsSatisfiedNotification Interface
        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {
            ProxyService.GameConnectionChanged += new GameConnectionChangedEventHandler(ProxyService_GameConnectionChanged);

            // Force an initial refresh of the services
            Refresh();
        }
        #endregion // IPartImportsSatisfiedNotification Interface

        #region Event handling
        /// <summary>
        /// Callback for when the currently selected game connection changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ProxyService_GameConnectionChanged(object sender, GameConnectionChangedEventArgs e)
        {
            Refresh();
        }
        #endregion // Event Handling

        #region ILiveEditingService Implementation
        /// <summary>
        /// Force a refresh of the root service
        /// </summary>
        public void Refresh()
        {
            if (ProxyService.IsGameConnected)
            {
                // Create the url to use
                Uri url = new Uri(String.Format("http://{0}:{1}", ProxyService.SelectedGameIP, _gameRestServicePort));
                
                BackgroundWorker worker = new BackgroundWorker();
                worker.DoWork += new DoWorkEventHandler(Refresh_DoWork);
                worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(Refresh_RunWorkerCompleted);
                worker.RunWorkerAsync(url);
            }
            else
            {
                LiveEditRootService = null;

                if (RootServiceChanged != null)
                {
                    RootServiceChanged(this, EventArgs.Empty);
                }
            }
        }

        /// <summary>
        /// Queries whether a particular file can be live edited
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public IObjectService GetLiveEditingServiceForFile(string filePath)
        {
            if (LiveEditRootService == null)
            {
                return null;
            }
            else
            {
                // Work through the hierarchy checking whether an object service with the passed in file path exists
                return GetLiveEditingService(LiveEditRootService, filePath);
            }
        }
        #endregion // ILiveEditingService Implementation

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Refresh_DoWork(Object sender, DoWorkEventArgs e)
        {
            e.Result = RootService.AcquireRootService((Uri)e.Argument, ConfigurationService.GameConfig);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Refresh_RunWorkerCompleted(Object sender, RunWorkerCompletedEventArgs e)
        {
            LiveEditRootService = e.Result as RootService;

            if (RootServiceChanged != null)
            {
                RootServiceChanged(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="directoryService"></param>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private IObjectService GetLiveEditingService(IDirectoryService directoryService, string filePath)
        {
            // Iterate over all of the directory's children
            foreach (IAsset child in directoryService.AssetChildren)
            {
                if (child is IObjectService)
                {
                    IObjectService objService = (IObjectService)child;
                    if (objService.AssociatedFilePath.Equals(filePath, StringComparison.CurrentCultureIgnoreCase))
                    {
                        return objService;
                    }
                }
                else if (child is IDirectoryService)
                {
                    IObjectService objService = GetLiveEditingService((IDirectoryService)child, filePath);
                    if (objService != null)
                    {
                        return objService;
                    }
                }
            }

            return null;
        }
        #endregion // Private Methods
    } // LiveEditingService
}
