﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;

namespace Workbench.AddIn.Services
{

    /// <summary>
    /// External tool service.
    /// </summary>
    public interface IExternalToolService
    {
        List<IExternalTool> Tools { get; set; }

        void Refresh();
    }

    /// <summary>
    /// External tool interface.
    /// </summary>
    public interface IExternalTool
    {
        #region Properties
        /// <summary>
        /// Command title to display on user-interface.
        /// </summary>
        String Title { get; }
        
        /// <summary>
        /// Command string to execute (pre-environment substitution).
        /// </summary>
        String Command { get; }
        
        /// <summary>
        /// Command arguments string (pre-environment substitution).
        /// </summary>
        String Arguments { get; }
        
        /// <summary>
        /// Command arguments string (pre-environment substitution).
        /// </summary>
        String InitialDirectory { get; }

        /// <summary>
        /// Pipe standard output and standard error to our logging window.
        /// </summary>
        bool UseOutputWindow { get; }

        /// <summary>
        /// Pipe standard output and standard error to our logging window.
        /// </summary>
        bool PromptForArguments { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Execute this external tool.
        /// </summary>
        /// <param name="environment"></param>
        /// <returns></returns>
        bool Execute(Dictionary<String, String> environment);

        /// <summary>
        /// Execute this external tool, using handler as stdout/stderr handler.
        /// </summary>
        /// <param name="envirnoment"></param>
        /// <param name="handler"></param>
        /// <returns></returns>
        bool Execute(Dictionary<String, String> envirnoment, DataReceivedEventHandler handler);
        #endregion // Methods
    }

} // Workbench.AddIn.Services namespace
