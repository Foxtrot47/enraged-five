﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common.Map;
using RSG.Model.Common;
using RSG.Model.Asset;
using RSG.Base.Collections;

namespace ContentBrowser.ViewModel.MapViewModels3
{
    /// <summary>
    /// 
    /// </summary>
    public class TextureDictionaryGroupViewModel : ObservableContainerViewModelBase<IAsset>
    {
        #region Properties
        /// <summary>
        /// The main name property which by default will be shown as the header for a browser item
        /// </summary>
        public override String Name
        {
            get
            {
                return "Texture Dictionaries";
            }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public TextureDictionaryGroupViewModel(IMapSection section)
            : base(section, null)
        {
            AggregatedObservableCollection<IAsset> agg = new AggregatedObservableCollection<IAsset>();
            agg.ChildCollections.Add(section.Archetypes);
            agg.ChildCollections.Add(section.ChildEntities);
            MonitoredCollection = agg;
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        protected override IEnumerable<IAsset> PreprocessAsset(IAsset asset)
        {
            // Get the map archetype to process
            IMapArchetype archetype = null;
            if (asset is IMapArchetype)
            {
                archetype = (IMapArchetype)asset;
            }
            else if (asset is IEntity && (asset as IEntity).ReferencedArchetype is IMapArchetype)
            {
                archetype = (IMapArchetype)(asset as IEntity).ReferencedArchetype;
            }

            // Process the archetypes txds
            if (archetype != null && !(archetype is IInteriorArchetype || archetype is IStatedAnimArchetype))
            {
                foreach (KeyValuePair<string, uint> pair in archetype.TxdExportSizes)
                {
                    IAssetViewModel txdVM = this.GetAssetChild(pair.Key);

                    if (txdVM != null && txdVM.Model is TextureDictionary)
                    {
                        TextureDictionary txd = (TextureDictionary)txdVM.Model;
                        txd.AddAssetChildren(archetype.Textures);
                    }
                    else
                    {
                        yield return new TextureDictionary(pair.Key, "", archetype.Textures, this.Model.Name);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected override IEnumerable<IAsset> PreprocessGridAsset(IAsset asset)
        {
            // Get the map archetype to process
            IMapArchetype archetype = null;
            if (asset is IMapArchetype)
            {
                archetype = (IMapArchetype)asset;
            }
            else if (asset is IEntity && (asset as IEntity).ReferencedArchetype is IMapArchetype)
            {
                archetype = (IMapArchetype)(asset as IEntity).ReferencedArchetype;
            }

            // Process the archetypes txds
            if (archetype != null && !(archetype is IInteriorArchetype || archetype is IStatedAnimArchetype))
            {
                foreach (KeyValuePair<string, uint> pair in archetype.TxdExportSizes)
                {
                    IGridViewModel txdVM = this.GetGridAsset(pair.Key);

                    if (txdVM != null && txdVM.Model is TextureDictionary)
                    {
                        TextureDictionary txd = (TextureDictionary)txdVM.Model;
                        txd.AddAssetChildren(archetype.Textures);
                    }
                    else
                    {
                        yield return new TextureDictionary(pair.Key, "", archetype.Textures, this.Model.Name);
                    }
                }
            }
        }
        #endregion // Overrides
    } // TextureDictionaryGroupViewModel
}
