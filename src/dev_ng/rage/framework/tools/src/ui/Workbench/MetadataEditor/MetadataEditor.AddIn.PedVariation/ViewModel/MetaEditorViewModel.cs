﻿using System;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Diagnostics;
using Workbench.AddIn;
using Workbench.AddIn.UI.Layout;
using MetadataEditor.AddIn.View;
using RSG.Base.Editor;

namespace MetadataEditor.AddIn.PedVariation.ViewModel
{
    /// <summary>
    /// Simple ViewModel for the core Metadata Editor(s).
    /// </summary>
    class MetaEditorViewModel : ViewModelBase
    {
        #region Constants
        public const String DOCUMENT_NAME = "MetaEditorViewModel";
        public const String DOCUMENT_TITLE = "Metadata";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public MetaEditorViewModel(MetaFileViewModel viewModel)
        {
        }
        #endregion // Constructor(s)
    }
} // MetadataEditor.AddIn.MetaEditor namespace
