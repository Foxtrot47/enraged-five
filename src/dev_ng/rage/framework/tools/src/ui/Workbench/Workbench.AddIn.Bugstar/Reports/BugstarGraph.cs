﻿using System;
using ReportBrowser.Addin;
using RSG.Model.Report;
using RSG.Base.Tasks;
using System.Windows;

namespace Workbench.AddIn.Bugstar.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public class BugstarGraph :
        DocumentReport,
        IDynamicReport
    {
        #region Constants
        private static readonly String DESC = "Bugstar Graph";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Task to run for generating this report.
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (m_task == null)
                {
                    m_task = new ActionTask("Generating report", (context, progress) => GenerateReport((DynamicReportContext)context, progress));
                }
                return m_task;
            }
        }
        private ITask m_task;
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private RSG.Interop.Bugstar.Search.Graph m_graph;
        #endregion

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="graph"></param>
        public BugstarGraph(RSG.Interop.Bugstar.Search.Graph graph)
            : base(graph.Name, DESC)
        {
            m_graph = graph;
        }
        #endregion // Constructor(s)
    
        #region IDynamicReport Interface
        /// <summary>
        /// Generates the report dynamically
        /// </summary>
        private void GenerateReport(DynamicReportContext context, IProgress<TaskProgress> progress)
        {
            // Invoke creation of the view onto the UI thread.
            Application.Current.Dispatcher.Invoke(
                    new Action
                    (
                        delegate()
                        {
                            Document = new BugstarGraphView(m_graph);
                        }
                    ),
                    System.Windows.Threading.DispatcherPriority.ContextIdle
                );
        }
        #endregion
    }
}
