﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using RSG.Base.Logging;
using RSG.Base.Windows.Controls;
using Workbench.AddIn;
using Workbench.AddIn.Services.File;
using Workbench.AddIn.UI.Layout;
using RSG.Base.Editor;
using RSG.Base.Collections;
using RSG.Model.LiveEditing;
using System.Windows;
using System.Net;
using System.Xml.Linq;
using System.Xml;
using System.Text;
using System.Diagnostics;
using WidgetEditor.UI;
using System.IO;
using RSG.Base.Editor.Command;
using ragCore;
using System.Threading;
using ragWidgets;
using System.Net.Sockets;
using RSG.Base.Network;

namespace WidgetEditor.ViewModel
{

    /// <summary>
    /// Application log view model
    /// </summary>
    public class WidgetEditorWindowViewModel : ViewModelBase
    {
        #region Imports
        #endregion

        #region Members

        private ILayoutManager m_LayoutManager;
        private Dictionary<IContentBase, RESTService> m_DocumentToServiceMap = new Dictionary<IContentBase, RESTService>();
        
        // rag
        private Int32 m_PortNumber = 2000;
        private ProxyConnection m_ProxyConnection;
        private PipeID m_RagPipeName;
        private INamedPipe m_RagPipe = PipeID.CreateNamedPipe();
        private Thread m_RagePipeThread;
        private Thread m_PipeUpdateThread;
        private bool m_Quiting;
        private bool m_StoppingPipe;
        private bool m_StartThread = true;
        private BankPipe m_PipeBank = new BankPipe();
        private INamedPipe m_PipeOutput = PipeID.CreateNamedPipe();
        private cRemoteConsole m_Console = new cRemoteConsole();   


        #endregion

        #region Properties


        public ObservableCollection<Widget> Widgets { get; set; }
        public ObservableCollection<WidgetViewModel> Banks { get; set; }

        private string m_Status;
        public string Status
        {
            get { return m_Status; }
            set
            {
                SetPropertyValue( value, () => this.Status,
                                new PropertySetDelegate( delegate( Object newValue ) { m_Status = (string)newValue; } ) );
            }
        }

        private bool m_Working = false;
        public bool Working
        {
            get { return m_Working; }
            set
            {
                SetPropertyValue( value, () => this.Working,
                    new PropertySetDelegate(
                        delegate( Object newValue )
                        {
                            m_Working = (Boolean)newValue;
                        }
                    )
                );
            }
        }

        #endregion // Properties

        #region Constructor


        public WidgetEditorWindowViewModel(
            ILayoutManager LayoutManager )
        {
            this.m_LayoutManager = LayoutManager;

            Widgets = new ObservableCollection<Widget>();
            Banks = new ObservableCollection<WidgetViewModel>();
            m_ProxyConnection = new ProxyConnection( "Rag Proxy" );
            m_ProxyConnection.ProxyDisconnected += new EventHandler( m_ProxyConnection_ProxyDisconnected );

            Application.Current.Exit += new ExitEventHandler( Current_Exit );
            //StartRagePipeThread();
        }

        #endregion // Constructor

        void Current_Exit( object sender, ExitEventArgs e )
        {
            m_RagPipe.CancelCreate();
            m_ProxyConnection.DisconnectFromProxy();
            m_PipeBank.Close();
            m_PipeOutput.Close();
        }

        private void StartRagePipeThread()
        {
            if ( m_StartThread && !m_Quiting )
            {
                m_StoppingPipe = false;
                m_RagePipeThread = new Thread( new ThreadStart( RagePipeProc ) );
                m_RagePipeThread.Name = "Rag Connection Thread";
                m_RagePipeThread.Start();
            }

            m_StartThread = false;
        }

        void m_ProxyConnection_ProxyDisconnected( object sender, EventArgs e )
        {
            Log.Log__Message( "Widget editor disconnected from proxy" );
        }










        private void RagePipeProc()
        {
            string threadName = m_RagePipeThread.Name;

            Console.WriteLine( "Started " + threadName );
            Status = "Connecting to proxy...";

            ProxyLock proxyLock = null;
            if ( m_RagPipeName == null )
            {
                if ( m_ProxyConnection != null )
                {
                    try
                    {
                        proxyLock = m_ProxyConnection.ConnectToProxy( 60000 );
                    }
                    catch ( TimeoutException )
                    {
                        if ( proxyLock != null )
                        {
                            proxyLock.Release();
                        }

                        Console.WriteLine( "RagePipeProc timed out while waiting for proxy connection." );
                        object[] args = new object[2];
                        args[0] = this;
                        args[1] = new EventArgs();
                        // TODO RAG Invoke( new EventHandler( exitToolStripMenuItem_Click ), args );
                        return;
                    }

                    m_PortNumber = m_ProxyConnection.Port;
                }

                m_RagPipeName = new PipeID( "pipe_rag", IPAddress.Loopback, m_PortNumber );
            }

            Console.WriteLine( "Waiting for client to connect to pipe '{0}' on port {1}...", m_RagPipeName.ToString(), m_RagPipeName.Port );

            if ( proxyLock != null )
            {
                proxyLock.Release();
            }
       
            bool mainWindowStarted = false;
            while ( !m_StoppingPipe )
            {
                try
                {
                    // wait until we get a connection:      
                    bool result = m_RagPipe.Create( m_RagPipeName, true );
                    if ( !result )
                    {
                        Console.WriteLine( "Could not connect to pipe '{0}'!", m_RagPipeName.ToString() );

                        if ( !m_StoppingPipe )
                        {
                            object[] args = new object[2];
                            args[0] = this;
                            args[1] = new EventArgs();
                            // TODO RAG Invoke( new EventHandler( exitToolStripMenuItem_Click ), args );
                        }

                        break;
                    }
                    else if ( !m_StoppingPipe )
                    {
                        Console.WriteLine( "Connected to pipe '{0}'.", m_RagPipeName.ToString() );
                        WidgetAngle.AddHandler();
                        WidgetBank.AddHandlers();
                        WidgetButton.AddHandlers();
                        WidgetColor.AddHandlers();
                        WidgetCombo.AddHandlers();
                        WidgetData.AddHandlers();
                        WidgetGroup.AddHandlers();
                        WidgetImageViewer.AddHandlers();
                        WidgetList.AddHandlers();
                        WidgetMatrix.AddHandlers();
                        WidgetSeparator.AddHandlers();
                        WidgetSliderFloat.AddHandlers();
                        WidgetSliderInt.AddHandlers();
                        WidgetText.AddHandlers();
                        WidgetTitle.AddHandlers();
                        WidgetToggle.AddHandlers();
                        WidgetTreeList.AddHandlers();
                        WidgetVector.AddHandlers();


                        Widget.WidgetAdded += new WidgetEventHandler( Widget_WidgetAdded );
                        Widget.WidgetRemoved += new WidgetEventHandler( Widget_WidgetRemoved );
                        WidgetBank.AddBankEvent += new WidgetBank.BankDelegate( WidgetBank_AddBankEvent );
                        BankRemotePacket.DestroyHandler = new BankRemotePacket.DestroyDelegate( Widget.DestroyHandler );

                        Status = "Performind Proxy handshake...";
                        ProxyHandshake.HandshakeResult handshake = ReadMainAppInfo( m_RagPipe );
                        if ( handshake != null )
                        {
                            Status = "Acquiring widget hierarchy...";
                            m_PipeUpdateThread = new Thread( ThreadUpdate );
                            m_PipeUpdateThread.Name = "Pipe thread";
                            m_PipeUpdateThread.Start();


                            string command = "getProxyStatus " + m_ProxyConnection.ProxyUID;
                            m_Console.Connect();
                            while ( true )
                            {
                                string proxyStatus = m_Console.SendCommand( command );
                                if ( proxyStatus == "ReadyIdle" || proxyStatus == "ReadyWorking" )
                                {
                                    Status = "";
                                    break;
                                }


                                Thread.Sleep( 100 );
                            }

                            break;                            
                        }

                        m_RagPipe.Close();
                    }
                }
                catch ( ThreadAbortException )
                {
                    break;
                }
            }

            Console.WriteLine( "Ended " + threadName );

            if ( mainWindowStarted )
            {
                Console.WriteLine( "Connection complete." );
            }
        }

        private void StartProcessingPipes( ProxyHandshake.HandshakeResult handshake )
        {


        }


        private bool ConnectToPipe( PipeID pipeId, INamedPipe pipe )
        {
            if ( pipe.IsConnected() )
            {
                return true;
            }

            Console.WriteLine( "Waiting for client '{0}' to connect to pipe '{1}' on port {2}...", "lols", pipeId.ToString(), pipeId.Port );

            bool result = pipe.Create( pipeId, true );
            if ( result )
            {
                Console.WriteLine( "Connected to pipe '{0}'.", pipeId.ToString() );
            }
            else
            {
                Console.WriteLine( "Could not connect to pipe '{0}'!", pipeId.ToString() );
            }

            return result;
        }


        private void ThreadUpdate()
        {
            while ( Application.Current != null )
            {

                BankRemotePacket.Update( m_PipeBank );
                BankRemotePacket.Process( m_PipeBank );
                /*
                if ( m_PipeBank.HasData() )
                {
                    Byte[] data = new Byte[5];
                    m_PipeBank.ReadData( data, 4 , false );
                    string instr = Encoding.ASCII.GetString( data );

                    SetText( instr );
                }
                 * */
            }
        }

        public ProxyHandshake.HandshakeResult ReadMainAppInfo( INamedPipe pipe )
        {
            IPAddress address = IPAddress.Parse( pipe.Socket.LocalEndPoint.ToString().Split( ':' )[0] );

            bool timedOut;
            ProxyHandshake.HandshakeResult result = new ProxyHandshake.HandshakeResult();


            int basePort = 60000;
            result.ReservedSockets = new List<TcpListener>();
            result.PortNumber = RagUtils.FindAvailableSockets( basePort, 10, result.ReservedSockets );

            result.PipeNameBank = RagUtils.GetRagPipeSocket( basePort, (int)PipeID.EnumSocketOffset.SOCKET_OFFSET_BANK, result.ReservedSockets );
            result.PipeNameOutput = RagUtils.GetRagPipeSocket( basePort, (int)PipeID.EnumSocketOffset.SOCKET_OFFSET_OUTPUT, result.ReservedSockets );
            result.PipeNameEvents = RagUtils.GetRagPipeSocket( basePort, (int)PipeID.EnumSocketOffset.SOCKET_OFFSET_EVENTS, result.ReservedSockets );

            int numConnected = 0;

            RagUtils.ConnectToPipe( result.PipeNameBank, m_PipeBank, delegate( PipeID pipeId )
                {
                    lock ( result )
                    {
                        ++numConnected;                        
                    }
                },
                 delegate( PipeID pipeId )
                {
                    lock ( result )
                    {
                        numConnected = -100;                        
                    }
                } );


            RagUtils.ConnectToPipe( result.PipeNameOutput, m_PipeOutput, delegate( PipeID pipeId )
            {
                lock ( result )
                {
                    ++numConnected;
                }
            },
                 delegate( PipeID pipeId )
                 {
                     lock ( result )
                     {
                         numConnected = -100;
                     }
                 } );


            for ( int i = 0; i < 2; i++ )
            {
                if ( !ProxyHandshake.DoHandshake( pipe, out timedOut, ref result ) )
                {
                    Log.Log__Error( "Handshake with Proxy failed" );
                    if ( timedOut )
                    {
                        Log.Log__Error( "Rag timed out waiting for the application startup data.", "Connection Timed Out" );
                    }
                    return null;
                }

            }

            // send the base socket and version numbers
            RemotePacket p = new RemotePacket( pipe );
            p.BeginWrite();
            p.Write_s32( result.PortNumber );
            p.Write_float( ProxyHandshake.RAG_VERSION );
            p.Send();


            while ( numConnected != 2 || numConnected < 0 )
            {
                Thread.Sleep( 10 );
            }

            if ( numConnected < 0 )
            {
                return null;
            }

            // Send input window handle
            BankRemotePacket packet = new BankRemotePacket( m_PipeBank );
            packet.Begin( BankRemotePacket.EnumPacketType.USER_3, Widget.ComputeGUID( 'b', 'm', 'g', 'r' ) );
            packet.Write_u32( 12345 );
            packet.Send();

            Console.WriteLine( "Handshake on pipe {0} done.", pipe.Name );
            Console.WriteLine( "Establishing connection with client: {0} {1}", result.ExeName, result.Args );
            
            return result;
        }


        void WidgetBank_AddBankEvent( BankPipe pipe, WidgetBank bank )
        {
            Dispatcher.Invoke( (Action)delegate()
            {
                Banks.Add( new WidgetViewModel( bank ) );
            } );
        }



        void Widget_WidgetRemoved( object sender, WidgetEventArgs e )
        {
            Dispatcher.Invoke( (Action)delegate()
            {
                Widgets.Remove( e.Widget );
            } );
        }

        void Widget_WidgetAdded( object sender, WidgetEventArgs e )
        {
            Dispatcher.Invoke( (Action ) delegate() 
            {
                Widgets.Add( e.Widget  );
            } );
            
        }
























        #region Commands

        private RelayCommand m_OpenServiceCommand;

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand OpenServiceCommand
        {
            get
            {
                if ( m_OpenServiceCommand == null )
                {
                    m_OpenServiceCommand = new RelayCommand( param => this.OpenService( param ), param => this.CanOpenService( param ) );
                }
                return m_OpenServiceCommand;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private Boolean CanOpenService( Object param )
        {
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        private void OpenService( Object param )
        {
        }



        #endregion



    }


} // Workbench.UI namespace
