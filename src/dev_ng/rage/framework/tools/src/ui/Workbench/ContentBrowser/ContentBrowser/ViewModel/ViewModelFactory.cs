﻿using System;
using System.Collections.Generic;
using ContentBrowser.AddIn;
using ContentBrowser.ViewModel.CharacterViewModels;
using ContentBrowser.ViewModel.GridViewModels;
using ContentBrowser.ViewModel.LevelViewModels;
using ContentBrowser.ViewModel.PerforceViewModels;
using ContentBrowser.ViewModel.ReportViewModels;
using ContentBrowser.ViewModel.VehicleViewModels;
using ContentBrowser.ViewModel.WeaponViewModels;
using RSG.Model.Character;
using RSG.Model.Common;
using RSG.Model.Perforce;
using RSG.Model.Report;
using RSG.Model.Vehicle;
using RSG.Model.Weapon;
using Workbench.AddIn;
using Workbench.AddIn.Services;
using RSG.Model.Asset.Level;
using RSG.Model.LiveEditing;
using ContentBrowser.ViewModel.LiveEditingViewModels;
using Workbench.AddIn.Commands;
using Workbench.AddIn.Services.File;
using System.Collections;
using System.ComponentModel.Composition;
using RSG.Model.Common.Map;
using ContentBrowser.ViewModel.MapViewModels3;
using RSG.Model.Asset;

namespace ContentBrowser.ViewModel
{
    /// <summary>
    /// Using this factory we can create the view models
    /// neccessary to view the browser items in the source
    /// panel. 
    /// </summary>
    [Workbench.AddIn.ExportExtension(Workbench.AddIn.ExtensionPoints.Void, typeof(Object))]
    public class ViewModelFactory : System.ComponentModel.Composition.IPartImportsSatisfiedNotification
    {
        #region Static Properties
        private static ContentBrowser.Commands.CommandContainer StaticCommandContainer { get; set; }
        private static IConfigurationService StaticConfig { get; set; }
        private static ITaskProgressService StaticTaskProgressService { get; set; }
        public static LevelViewModel LastLevelCreated { get; private set; }
        #endregion // Static Properties

        #region MEF Imports
        /// <summary>
        /// 
        /// </summary>
        [Workbench.AddIn.ImportExtension("ContentBrowser.Commands.CommandContainer", typeof(ContentBrowser.Commands.CommandContainer))]
        public ContentBrowser.Commands.CommandContainer CommandContainer { get; set; }

        /// <summary>
        /// Configuration service
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(IConfigurationService))]
        private IConfigurationService Config { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.TaskProgressService, typeof(ITaskProgressService))]
        private ITaskProgressService TaskProgressService { get; set; }
        #endregion // MEF Imports

        #region Static Functions

        /// <summary>
        /// Creates a view model from a model to be used in the browser tree 
        /// view.
        /// </summary>
        public static IAssetViewModel CreateViewModel(IAsset model)
        {
            IAssetViewModel viewModel = null;
            if (model is ILevel)
            {
                LastLevelCreated = new LevelViewModel(model as ILevel);
                viewModel = LastLevelCreated;
            }
            else if (model is ITexture)
            {
                viewModel = new TextureViewModel(model as ITexture);
            }
            else if (model is ICharacter)
            {
                viewModel = new CharacterViewModel(model as ICharacter, StaticTaskProgressService);
            }
            else if (model is ICharacterCollection)
            {
                viewModel = new CharacterCollectionViewModel(model as ICharacterCollection);
            }
            else if (model is ICharacterCategoryCollection)
            {
                viewModel = new CharacterCategoryCollectionViewModel(model as ICharacterCategoryCollection);
            }
            else if (model is IVehicle)
            {
                viewModel = new VehicleViewModel(model as IVehicle, StaticTaskProgressService);
            }
            else if (model is IVehicleCollection)
            {
                viewModel = new VehicleCollectionViewModel(model as IVehicleCollection);
            }
            else if (model is IWeapon)
            {
                viewModel = new WeaponViewModel(model as IWeapon, StaticTaskProgressService);
            }
            else if (model is IWeaponCollection)
            {
                viewModel = new WeaponCollectionViewModel(model as IWeaponCollection);
            }
            else if (model is IReportCategory)
            {
                viewModel = new ReportCategoryViewModel(model as IReportCategory, StaticTaskProgressService);
            }
            else if (model is IReport)
            {
                viewModel = (new ReportViewModel(model as IReport));
            }
            else if (model is IObjectService)
            {
                viewModel = new ObjectServiceViewModel((IObjectService)model);
            }
            else if (model is Workspace)
            {
                viewModel = new WorkspaceViewModel(model as Workspace);
            }
            else if (model is Depot)
            {
                viewModel = (new DepotViewModel(model as Depot));
            }
            else if (model is IMapHierarchy)
            {
                viewModel = new MapHierarchyViewModel(model as IMapHierarchy);
            }
            else if (model is IMapArea)
            {
                viewModel = new MapAreaViewModel(model as IMapArea);
            }
            else if (model is IMapSection)
            {
                viewModel = new MapSectionViewModel(model as IMapSection, StaticTaskProgressService);
            }
            else if (model is IInteriorArchetype)
            {
                viewModel = new InteriorArchetypeViewModel(model as IInteriorArchetype);
            }
            else if (model is IFragmentArchetype)
            {
                viewModel = new FragmentArchetypeViewModel(model as IFragmentArchetype);
            }
            else if (model is IDrawableArchetype)
            {
                viewModel = new DrawableArchetypeViewModel(model as IDrawableArchetype);
            }
            else if (model is IStatedAnimArchetype)
            {
                viewModel = new StatedAnimArchetypeViewModel(model as IStatedAnimArchetype);
            }
            else if (model is IRoom)
            {
                viewModel = new RoomViewModel(model as IRoom);
            }
            else if (model is IEntity)
            {
                viewModel = new EntityViewModel(model as IEntity);
            }
            else if (model is TextureDictionary)
            {
                viewModel = new TextureDictionaryViewModel(model as TextureDictionary, LastLevelCreated);
            }

            if (viewModel == null)
                viewModel = ViewModelFactory.CreateDefaultViewModel(model);

            if (viewModel != null && StaticCommandContainer != null)
            {
                foreach (KeyValuePair<Type, List<Workbench.AddIn.Commands.IWorkbenchCommand>> assetTypeCommands in StaticCommandContainer.AssetCommands)
                {
                    if (assetTypeCommands.Key.IsClass)
                    {
                        if (model.GetType() == assetTypeCommands.Key || model.GetType().IsSubclassOf(assetTypeCommands.Key))
                        {
                            viewModel.Commands = new System.Collections.ObjectModel.ObservableCollection<Workbench.AddIn.Commands.IWorkbenchCommand>(assetTypeCommands.Value);
                        }
                    }
                    else if (assetTypeCommands.Key.IsInterface)
                    {
                        if (model.GetType().GetInterface(assetTypeCommands.Key.Name, true) != null)
                        {
                            viewModel.Commands = new System.Collections.ObjectModel.ObservableCollection<Workbench.AddIn.Commands.IWorkbenchCommand>(assetTypeCommands.Value);
                        }
                    }
                }
                foreach (KeyValuePair<Type, Workbench.AddIn.Commands.IWorkbenchCommand> assetTypeCommands in StaticCommandContainer.DefaultAssetCommands)
                {
                    if (assetTypeCommands.Key.IsClass)
                    {
                        if (model.GetType() == assetTypeCommands.Key || model.GetType().IsSubclassOf(assetTypeCommands.Key))
                        {
                            viewModel.DefaultCommand = assetTypeCommands.Value;
                        }
                    }
                    else if (assetTypeCommands.Key.IsInterface)
                    {
                        if (model.GetType().GetInterface(assetTypeCommands.Key.Name, true) != null)
                        {
                            viewModel.DefaultCommand = assetTypeCommands.Value;
                        }
                    }
                }
            }
            return viewModel;
        }

        /// <summary>
        /// Creates a asset view model for a model using only the default view
        /// model classes
        /// </summary>
        private static IAssetViewModel CreateDefaultViewModel(IAsset model)
        {
            if (model is IFileAsset && model is IHasAssetChildren)
            {
                return new FileAssetContainerViewModelBase(model as IHasAssetChildren);
            }
            else if (model is IHasAssetChildren)
            {
                return new AssetContainerViewModelBase(model as IHasAssetChildren);
            }
            else if (model is IFileAsset)
            {
                return new FileAssetViewModelBase(model as IFileAsset);
            }
            else if (model is IAsset)
            {
                return new AssetViewModelBase(model as IAsset);
            }
            else
            {
                return new AssetDummyViewModel(model.Name);
            }
        }

        /// <summary>
        /// Creates a view model from a model to be used in the browser
        /// grid view.
        /// </summary>
        public static IGridViewModel CreateGridViewModel(IAsset model)
        {
            IGridViewModel viewModel = null;

            if (model is ITexture)
            {
                viewModel = new TextureGridViewModel(model as ITexture);
            }
            else if (model is IMapArchetype)
            {
                viewModel = new ArchetypeGridViewModel(model as IMapArchetype, StaticConfig.GameConfig.ArtDir);
            }
            else if (model is ICharacter)
            {
                viewModel = (new CharacterGridViewModel(model as ICharacter, StaticConfig.GameConfig.ArtDir));
            }
            else if (model is IVehicle)
            {
                viewModel = new VehicleGridViewModel(model as IVehicle, StaticConfig.GameConfig.AssetsDir);
            }
            else if (model is IWeapon)
            {
                viewModel = new WeaponGridViewModel(model as IWeapon, StaticConfig.GameConfig.AssetsDir);
            }
            else if (model is IReport)
            {
                viewModel = new ReportGridViewModel(model as IReport);
            }
            else if (model is IObjectService)
            {
                viewModel = new ObjectServiceGridViewModel((IObjectService)model);
            }

            if (viewModel == null)
                viewModel = ViewModelFactory.CreateDefaultGridViewModel(model);

            if (viewModel != null && StaticCommandContainer != null)
            {
                foreach (KeyValuePair<Type, List<Workbench.AddIn.Commands.IWorkbenchCommand>> assetTypeCommands in StaticCommandContainer.AssetCommands)
                {
                    if (assetTypeCommands.Key.IsClass)
                    {
                        if (model.GetType() == assetTypeCommands.Key || model.GetType().IsSubclassOf(assetTypeCommands.Key))
                        {
                            viewModel.Commands = new System.Collections.ObjectModel.ObservableCollection<Workbench.AddIn.Commands.IWorkbenchCommand>(assetTypeCommands.Value);
                        }
                    }
                    else if (assetTypeCommands.Key.IsInterface)
                    {
                        if (model.GetType().GetInterface(assetTypeCommands.Key.Name, true) != null)
                        {
                            viewModel.Commands = new System.Collections.ObjectModel.ObservableCollection<Workbench.AddIn.Commands.IWorkbenchCommand>(assetTypeCommands.Value);
                        }
                    }
                }

                if (viewModel is IHasAssetChildrenViewModel)
                {
                    viewModel.DefaultCommand = new OpenContainerCommand(viewModel);
                }
                else
                {
                    foreach (KeyValuePair<Type, Workbench.AddIn.Commands.IWorkbenchCommand> assetTypeCommands in StaticCommandContainer.DefaultAssetCommands)
                    {
                        if (assetTypeCommands.Key.IsClass)
                        {
                            if (model.GetType() == assetTypeCommands.Key || model.GetType().IsSubclassOf(assetTypeCommands.Key))
                            {
                                viewModel.DefaultCommand = assetTypeCommands.Value;
                            }
                        }
                        else if (assetTypeCommands.Key.IsInterface)
                        {
                            if (model.GetType().GetInterface(assetTypeCommands.Key.Name, true) != null)
                            {
                                viewModel.DefaultCommand = assetTypeCommands.Value;
                            }
                        }
                    }
                }
            }
            return viewModel;
        }

        /// <summary>
        /// Creates a grid asset view model for a model using only the default view
        /// model classes
        /// </summary>
        private static IGridViewModel CreateDefaultGridViewModel(IAsset model)
        {
            if (model is IHasAssetChildren)
            {
                return new AssetContainerGridViewModel(model as IHasAssetChildren);
            }
            else
            {
                return new DummyAssetContainerGridViewModel(model.Name, model);
            }
        }

        public static IBrowserViewModel CreateBrowserViewModel(BrowserBase browser)
        {
            return new GenericBrowserViewModel(browser);
        }

        #endregion // Static Functions


        #region Default Open Command
        /// <summary>
        /// 
        /// </summary>
        [ExportExtension(Workbench.AddIn.ExtensionPoints.Void)]
        public class OpenContainerCommand : WorkbenchMenuItemBase, IPartImportsSatisfiedNotification
        {
            private static ContentBrowserDataContext SavedContentBrowser { get; set; }
            private IAsset asset;
            private bool IsMapObjects;
            private bool IsTextureDictionaries;
            private bool IsDummyObject;

            #region MEF
            [Workbench.AddIn.ImportExtension(ContentBrowser.AddIn.CompositionPoints.ContentBrowser, typeof(IContentBrowser))]
            IContentBrowser ContentBrowserImport { get; set; }
            #endregion

            #region Constructor
            /// <summary>
            /// Default constructor.
            /// </summary>
            public OpenContainerCommand(IGridViewModel viewmodel)
            {
                this.IsDefault = true;
                asset = viewmodel.Model;
            }

            /// <summary>
            /// Default constructor.
            /// </summary>
            public OpenContainerCommand(IGridViewModel viewmodel, bool isMapObjects, bool isTextureDictionaries)
            {
                this.IsDefault = true;
                asset = viewmodel.Model;
                IsMapObjects = isMapObjects;
                IsTextureDictionaries = isTextureDictionaries;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="viewModel"></param>
            /// <param name="isDummyObject"></param>
            public OpenContainerCommand(IGridViewModel viewModel, bool isDummyObject)
            {
                this.IsDefault = true;
                asset = viewModel.Model;
                IsDummyObject = isDummyObject;
            }

            /// <summary>
            /// Default constructor.
            /// </summary>
            public OpenContainerCommand()
            {
                this.IsDefault = true;
            }
            #endregion // Constructor

            #region ICommand Implementation
            /// <summary>
            /// Gets called when deciding whether this menu items
            /// command can be executed or not
            /// </summary>
            public override Boolean CanExecute(Object parameter)
            {
                return true;
            }

            /// <summary>
            /// Gets called when executing the command for this
            /// menu item
            /// </summary>
            public override void Execute(Object parameter)
            {
                IBrowserViewModel browser = SavedContentBrowser.BrowserCollection.SelectedBrowser;
                if (browser is GenericBrowserViewModel)
                {
                    if (IsDummyObject)
                    {
                        (browser as GenericBrowserViewModel).SelectDummyAsset(asset);
                    }
                    else if (IsMapObjects)
                    {
                        (browser as GenericBrowserViewModel).SelectMapObjectAsset(asset);
                    }
                    else if (IsTextureDictionaries)
                    {
                        (browser as GenericBrowserViewModel).SelectTextureDictionariesAsset(asset);
                    }
                    else
                    {
                        (browser as GenericBrowserViewModel).SelectAsset(asset);
                    }
                }
                else
                {
                    browser.SelectAsset(asset);
                }
            }
            #endregion // ICommand Implementation

            #region IPartImportsSatisfiedNotification Interface
            /// <summary>
            /// 
            /// </summary>
            public void OnImportsSatisfied()
            {
                SavedContentBrowser = this.ContentBrowserImport as ContentBrowserDataContext;
            }
            #endregion // IPartImportsSatisfiedNotification Interface
        } // OpenMetadataFileCommand
        #endregion

        #region IPartImportsSatisfiedNotification Interface

        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {
            StaticCommandContainer = this.CommandContainer;
            StaticConfig = this.Config;
            StaticTaskProgressService = this.TaskProgressService;
        }

        #endregion // IPartImportsSatisfiedNotification Interface
    } // ViewModelFactory
} // ContentBrowser.ViewModel
