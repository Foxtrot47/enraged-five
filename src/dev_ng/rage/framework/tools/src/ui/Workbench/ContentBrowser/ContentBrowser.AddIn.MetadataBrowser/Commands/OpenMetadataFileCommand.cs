﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn;
using Workbench.AddIn.Commands;
using Workbench.AddIn.Services.File;
using RSG.Model.LiveEditing;
using System.Collections;
using Workbench.AddIn.Services;

namespace ContentBrowser.AddIn.MetadataBrowser.Commands
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(ContentBrowser.AddIn.ExtensionPoints.MetadataAssetCommands, typeof(IWorkbenchCommand))]
    public class OpenMetadataFileCommand : WorkbenchMenuItemBase
    {
        #region MEF Imports
        /// <summary>
        /// Workbench open service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.WorkbenchOpenService, typeof(IWorkbenchOpenService))]
        private Lazy<IWorkbenchOpenService> WorkbenchOpenService { get; set; }

        /// <summary>
        /// Metadata open service
        /// </summary>
        [ImportExtension(MetadataEditor.AddIn.CompositionPoints.MetadataOpenService, typeof(IOpenService))]
        private Lazy<IOpenService> MetadataOpenService { get; set; }

        /// <summary>
        /// MEF import for message box service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.MessageService, typeof(Workbench.AddIn.Services.IMessageService))]
        private Lazy<IMessageService> MessageService { get; set; }
        #endregion // MEF Imports

        #region Constructor
        /// <summary>
        /// Default constructor.
        /// </summary>
        public OpenMetadataFileCommand()
        {
            this.Header = "_Open";
            this.IsDefault = true;
        }
        #endregion // Constructor

        #region ICommand Implementation
        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
            if (parameter is IEnumerable)
            {
                foreach (Object service in (parameter as IEnumerable))
                {
                    this.OpenMetadataFile(service as IObjectService);
                }
            }
            else
            {
                this.OpenMetadataFile(parameter as IObjectService);
            }
        }
        #endregion // ICommand Implementation

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="objService"></param>
        private void OpenMetadataFile(IObjectService objService)
        {
            if (!string.IsNullOrEmpty(objService.AssociatedFilePath))
            {
                string extension = System.IO.Path.GetExtension(objService.AssociatedFilePath);
                if (!MetadataOpenService.Value.Filter.Contains(extension))
                {
                    MessageService.Value.Show(string.Format("The file associated with the {0} service has an extension '{1}' that isn't recognised by the metadata editor so can't be opened.", objService.Name, extension));
                    return;
                }

                WorkbenchOpenService.Value.OpenFileWithService(MetadataOpenService.Value, objService.AssociatedFilePath);
            }
            else
            {
                // Let the user know that there isn't a corresponding metadata file for this service
                MessageService.Value.Show(string.Format("There is no metadata file associated with the {0} service so cannot be opened and edited inside the workbench.", objService.Name));
            }
        }
        #endregion // Private Methods
    } // OpenMetadataFileCommand
}
