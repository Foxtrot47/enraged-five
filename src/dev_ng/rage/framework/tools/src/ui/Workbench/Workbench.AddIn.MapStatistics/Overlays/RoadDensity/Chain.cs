﻿using System.Collections.Generic;
using RSG.Base.Math;
using RSG.Model.Map3;
using System;

namespace Workbench.AddIn.MapStatistics.Overlays.RoadDensity
{
    #region Helper classes

    /// <summary>
    /// Points mapped to a specific density.
    /// </summary>
    public class DensityPoints
    {
        /// <summary>
        /// Density.
        /// </summary>
        public int Density
        {
            get;
            set;
        }

        /// <summary>
        /// Point array.
        /// </summary>
        public Vector2f[] Points
        {
            get;
            set;
        }
        
        public string StreetNames
        {
            get;
            set;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="density">Density.</param>
        public DensityPoints(int density)
        {
            Density = density;
        }
    }

    /// <summary>
    /// Density point collection.
    /// </summary>
    public class DensityPointCollection : List<DensityPoints>
    {

    }

    #endregion

    /// <summary>
    /// Chain of points that make up a path in the game.
    /// </summary>
    class Chain
    {
        #region Public constants

        public static readonly int c_pedAssistedMovementDensity = 99;
        public static readonly int c_pedCrossingDensity = 100;

        #endregion

        #region Private member fields

        private Vector2f m_offset;

        #endregion

        #region Public properties

        /// <summary>
        /// Points in the chain.
        /// </summary>
        public List<ChainPoint> Points
        {
            get;
            private set;
        }

        /// <summary>
        /// Traffic density.
        /// </summary>
        public int Density
        {
            get;
            private set;
        }

        public string StreetNames
        {
            get;
            set;
        }
        
        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="offset">Offset</param>
        public Chain(Vector2f offset)
        {
            Points = new List<ChainPoint>();
            StreetNames = String.Empty;
            m_offset = offset;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Create an array of Vector2f from the points in the chain.
        /// </summary>
        /// <returns>Array of points.</returns>
        public Vector2f[] ToPointArray()
        {
            Vector2f[] points = new Vector2f[Points.Count];
            int i = 0;
            Points.ForEach(pt => points[i++] = pt.Position);
            return points;
        }

        private List<Vector2f> ToPointList()
        {
            List<Vector2f> points = new List<Vector2f>();
            Points.ForEach(pt => points.Add(pt.Position));
            return points;
        }

        public DensityPointCollection ToPointArrays()
        {
            DensityPointCollection pointList = new DensityPointCollection();
            FillList(pointList);

            return pointList;
        }

        private void FillList(DensityPointCollection bucket)
        {
            int lastDensity = Points[0].Density;
            DensityPoints points = new DensityPoints(lastDensity);
            List<Vector2f> buffer = new List<Vector2f>();

            bucket.Add(points);

            for (int i = 0; i < Points.Count; i++)
            {
                ChainPoint pt = Points[i];
                buffer.Add(pt.Position);

                if (pt.Density != lastDensity)
                {
                    points.Points = buffer.ToArray();       // assign the last density points.
                    buffer = new List<Vector2f>();          // create a new buffer to insert the position data
                    points = new DensityPoints(pt.Density); // create a new density points.
                    bucket.Add(points);                     // add those new points to the collection
                    buffer.Add(pt.Position);                // add the same position again to the new density 
                    lastDensity = pt.Density;               // so we always have a starting point
                }
            }

            if (buffer.Count == 1)
            {
                buffer.Add(buffer[0]);
            }

            points.Points = buffer.ToArray();
        }

        /// <summary>
        /// Add a point to the chain.
        /// </summary>
        /// <param name="x">X co-ordinate.</param>
        /// <param name="y">Y co-ordinate.</param>
        /// <param name="density">Traffic density.</param>
        /// <param name="specialUse">Special use flag.</param>
        public void Add(float x, float y, int density, VehicleNodeSpecialUse specialUse)
        {
            ChainPoint cp = new ChainPoint();
            cp.Position = new Vector2f(x + m_offset.X, y + m_offset.Y);
            cp.Density = density;
            cp.SpecialUse = specialUse;

            Points.Add(cp);
        }

        #endregion

        #region Internal methods

        /// <summary>
        /// Calculate the density.
        /// </summary>
        internal void CalcDensity()
        {
            if (Points.Count == 0) return;

            int density = 0;
            foreach (var pt in Points)
            {
                density += pt.Density;
            }

            float dens = (float)density;
            dens /= (float)Points.Count;
            double ceil = Math.Ceiling(dens);

            Density = density / Points.Count;
            Density = (int)ceil;
        }

        #endregion
    }
}
