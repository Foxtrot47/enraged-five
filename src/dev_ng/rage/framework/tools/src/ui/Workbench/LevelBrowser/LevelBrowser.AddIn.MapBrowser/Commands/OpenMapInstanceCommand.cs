﻿using System;
using System.ComponentModel.Composition;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using RSG.Model.Common.Map;
using Workbench.AddIn;
using Workbench.AddIn.Services;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.Commands;

namespace LevelBrowser.AddIn.MapBrowser.Commands
{

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Extensions.MapDefinitionCommands, typeof(IWorkbenchCommand))]
    class OpenMapInstanceCommand : WorkbenchMenuItemBase
    {
        #region MEF Imports

        [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager, typeof(ILayoutManager))]
        private ILayoutManager LayoutManager { get; set; }

        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(IConfigurationService))]
        private IConfigurationService ConfigService { get; set; }


        #endregion // MEF Imports

        #region Constructor

        /// <summary>
        /// Default constructor.
        /// </summary>
        public OpenMapInstanceCommand()
        {
            this.Header = "_Open";
            this.IsDefault = true;
        }

        #endregion // Constructor

        #region ICommand Implementation
        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
            if (parameter is IList)
            {
                foreach (Object instance in (parameter as IList))
                {
                    Debug.Assert(instance is IEntity);
                    this.OpenDefinition((IEntity)instance);
                }
            }
            else
            {
                Debug.Assert(parameter is IEntity);
                this.OpenDefinition((IEntity)parameter);
            }
        }
        #endregion // ICommand Implementation

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        private void OpenDefinition(IEntity entity)
        {
            Debug.Assert(null != entity, "Entity is null.");
            if (null == entity)
                return;

            Debug.Assert(entity.Parent is IMapSection);
            if (!(entity.Parent is IMapSection))
                return;

            IMapSection section = (IMapSection)entity.Parent;
            RSG.Base.ConfigParser.ConfigGameView gv = ConfigService.GameConfig;
            if (System.IO.File.Exists(section.ExportZipFilepath))
            {
                ProcessStartInfo runRuby = new System.Diagnostics.ProcessStartInfo();
                runRuby.FileName = System.IO.Path.Combine(gv.ToolsLibDir, "util", "data_extract_singlefile_from_zip.rb");
                runRuby.Arguments = @"--output=" +
                    System.IO.Path.Combine(gv.ProjectRootDir, "cache", "raw", "maps", section.Name) + 
                    " --filter=" + 
                    entity.Name + "[.].* " + "--overwrite " +
                    section.ExportZipFilepath;
                System.Diagnostics.Process p = System.Diagnostics.Process.Start(runRuby);
                p.WaitForExit();

                String[] files = System.IO.Directory.GetFiles(System.IO.Path.Combine(gv.ProjectRootDir, "cache", "raw", "maps", section.Name), entity.Name + ".*");
                if (files.Length > 0)
                {
                    System.Diagnostics.Process.Start(files[0]);
                }
            }
        }
        #endregion // Private Methods

    } // OpenMapSectionCommand

} // LevelBrowser.AddIn.MapBrowser.Commands namespace
