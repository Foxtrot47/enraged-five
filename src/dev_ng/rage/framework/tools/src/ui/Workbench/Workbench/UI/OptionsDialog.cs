﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using Workbench.AddIn;
using Workbench.AddIn.Services;

namespace Workbench.UI
{

    /// <summary>
    /// 
    /// </summary>
    class OptionsDialog
    {
        #region MEF Imports
        /// <summary>
        /// 
        /// </summary>
        [ImportManyExtension(Workbench.AddIn.ExtensionPoints.Settings,
            typeof(ISettings))]
        public IEnumerable<ISettings> Settings;
        #endregion // MEF Imports
    }

} // Workbench.UI namespace
