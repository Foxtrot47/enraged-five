﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Windows;
using RSG.Base.Editor;
using RSG.Base.Logging;
using Workbench.AddIn;
using Workbench.AddIn.Commands;
using Workbench.AddIn.Services;
using Workbench.AddIn.Services.File;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.UI.Menu;

namespace Workbench.UI.Menu
{
    /// <summary>
    /// File, Save As menu item.
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.MenuFile, typeof(IWorkbenchCommand))]
    [ExportExtension(Workbench.AddIn.CompositionPoints.WorkbenchSaveService, typeof(IWorkbenchSaveService))]
    class SaveAsFileMenuItem : WorkbenchMenuItemBase, IWorkbenchSaveService
    {
        #region Events

        public event DocumentSavedHandler DocumentSaved;

        #endregion // Events

        #region MEF Imports

        /// <summary>
        /// MEF import for all ISaveServices.
        /// </summary>
        [ImportManyExtension(Workbench.AddIn.ExtensionPoints.SaveService, typeof(ISaveService))]
        private IEnumerable<ISaveService> Services { get; set; }

        /// <summary>
        /// MEF import for the Workbench's LayoutManager.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager, typeof(ILayoutManager))]
        private ILayoutManager LayoutManager { get; set; }

        [ImportExtension(Workbench.AddIn.CompositionPoints.MessageService,
            typeof(Workbench.AddIn.Services.IMessageService))]
        protected IMessageService MessageService { get; set; }

        #endregion // MEF Imports

        #region Properties

        private ISaveService CurrentService
        {
            get;
            set;
        }

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Default constructor.
        /// </summary>
        public SaveAsFileMenuItem()
        {
            this.Header = "Save _As";
            this.ID = new Guid(Workbench.AddIn.CompositionPoints.MenuFileSaveAs);
            this.RelativeID = new Guid(Workbench.AddIn.CompositionPoints.MenuFileSave);
            Direction = Direction.After;
            this.SetImageFromBitmap(Workbench.Resources.Images.SaveAs);
        }

        #endregion // Constructor(s)

        #region ICommandControl Interface

        /// <summary>
        /// 
        /// </summary>
        public override bool CanExecute(Object parameter)
        {
            IContentBase content = this.LayoutManager.GetActiveContent();

            if (content == null)
            {
                this.CurrentService = null;
                return false;
            }
            else
            {
                if (content.ID == Guid.Empty || content.ID == null)
                {
                    this.CurrentService = null;
                    return false;
                }
                else
                {
                    // Check that there is a save service available for this
                    // content
                    foreach (ISaveService service in this.Services)
                    {
                        List<Guid> supportedContent = new List<Guid>(service.SupportedContent);
                        if (supportedContent.Contains(content.ID))
                        {
                            if (service.CanExecuteSaveAs() == true)
                            {
                                this.CurrentService = service;
                                return true;
                            }
                        }
                    }
                }
            }

            this.CurrentService = null;
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        public override void Execute(Object parameter)
        {
            Log.Log__Debug("SaveAsFileMenuItem::Execute()");

            // For the currently selected content; determine which 
            // ISaveAsService we should use.
            IContentBase content = this.LayoutManager.GetActiveContent();
            ISaveService service = this.CurrentService;

            Debug.Assert(null != service, "No available ISaveService for current document.");
            if (null == service)
            {
                Log.Log__Error("No available ISaveService for current content.");
                return;
            }

            int filterIndex = -1;
            String path = String.Empty;

            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.Filter = service.Filter;
            dlg.FilterIndex = service.DefaultFilterIndex;

            Nullable<bool> result = dlg.ShowDialog();
            if (false == result)
                return;

            filterIndex = dlg.FilterIndex;
            path = dlg.FileName;

            // Check to see if path is readonly; if it is show a message box
            // that displays a warning to the user and make sure that the save process finishes
            String savePath = path;
            System.IO.FileInfo fi = new System.IO.FileInfo(path);
            if (System.IO.File.Exists(path))
            {
                if (fi.Attributes.HasFlag(System.IO.FileAttributes.ReadOnly))
                {
                    Boolean save = HandleReadOnlyFile(ref savePath, ref filterIndex, fi, service);
                    if (save == false)
                    {
                        return;
                    }
                }
            }

            IModel saveModel = content.SaveModel;
            Boolean setPathToSavePath = true;
            bool saveResult = service.Save(saveModel, savePath, -1, ref setPathToSavePath);
            if (saveResult)
            {
                content.IsModified = false;
                if (setPathToSavePath == true)
                {
                    content.Path = savePath;
                    content.Title = System.IO.Path.GetFileName(savePath);
                }
                Log.Log__Debug("SaveAsService {0} successful.", service.ID);
                if (this.DocumentSaved != null && content is IDocumentBase)
                    this.DocumentSaved(content as IDocumentBase);
            }
            else
            {
                Log.Log__Error("SaveAsService {0} failed.", service.ID);
            }
        }

        private Boolean HandleReadOnlyFile(ref String filename, ref int filterIndex, System.IO.FileInfo fi, ISaveService service)
        {
            Boolean save = false;

            Services.SaveReadOnlyWindow readonlyWindow = new Services.SaveReadOnlyWindow(filename);
            readonlyWindow.Owner = App.Current.MainWindow;
            readonlyWindow.ShowDialog();
            switch (readonlyWindow.Result)
            {
                case Workbench.Services.ReadOnlyResult.Cancel:
                    {
                    }
                    break;
                case Workbench.Services.ReadOnlyResult.Overwrite:
                    {
                        fi.IsReadOnly = false;
                        save = true;
                    }
                    break;
                case Workbench.Services.ReadOnlyResult.SaveAs:
                    {
                        Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
                        dlg.Filter = service.Filter;
                        dlg.FilterIndex = service.DefaultFilterIndex;

                        Nullable<bool> result = dlg.ShowDialog();
                        if (false == result)
                        {
                            save = false;
                        }
                        else
                        {
                            save = true;
                            filename = dlg.FileName;
                            filterIndex = dlg.FilterIndex;
                        }
                    }
                    break;
                default:
                    break;
            }
            return save;
        }

        #endregion // ICommandControl Interface
    } // SaveAsFileMenuItem
} // Workbench.UI.Menu namespace
