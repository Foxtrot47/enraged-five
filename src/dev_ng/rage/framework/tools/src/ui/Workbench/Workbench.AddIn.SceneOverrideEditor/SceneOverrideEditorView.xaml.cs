﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using PerforceBrowser.AddIn;
using WidgetEditor.AddIn;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.Services;

namespace Workbench.AddIn.SceneOverrideEditor
{
    /// <summary>
    /// Interaction logic for SceneOverrideEditorView.xaml
    /// </summary>
    public partial class SceneOverrideEditorView : ToolWindowBase<SceneOverrideEditorViewModel>
    {
        public SceneOverrideEditorView(
            IConfigurationService configurationService, 
            IMessageService messageService, 
            IPerforceService perforceService, 
            IProgressService progressService, 
            IProxyService proxyService)
            : base(Workbench.AddIn.SceneOverrideEditor.Resources.Strings.SceneOverrideEditor_Name,
                   new SceneOverrideEditorViewModel(configurationService, messageService, perforceService, progressService, proxyService))
        {
            InitializeComponent();
        }
    }
}
