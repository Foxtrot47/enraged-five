﻿using System;
using RSG.Base.Editor;

namespace Workbench.AddIn.Services
{

    /// <summary>
    /// 
    /// </summary>
    public interface ISelectionService
    {
        #region Events
        /// <summary>
        /// Event raised when the selected Model changes.
        /// </summary>
        event EventHandler<ModelEventArgs> SelectedModelChanged;

        /// <summary>
        /// Event raised when the selected ViewModel changes.
        /// </summary>
        event EventHandler<ViewModelEventArgs> SelectedViewModelChanged;
        #endregion // Events

        #region Properties
        /// <summary>
        /// Currently selected Model.
        /// </summary>
        IModel SelectedModel { get; }

        /// <summary>
        /// Currently selected ViewModel.
        /// </summary>
        IViewModel SelectedViewModel { get; }
        #endregion // Properties
    }

} // Workbench.AddIn.Services namespace
