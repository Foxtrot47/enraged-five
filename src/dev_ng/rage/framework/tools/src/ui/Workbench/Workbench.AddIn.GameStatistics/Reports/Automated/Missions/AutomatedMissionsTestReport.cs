﻿using RSG.Model.Report.Reports.GameStatsReports.Tests;
using RSG.SourceControl.Perforce;

namespace Workbench.AddIn.GameStatistics.Reports.Automated.Missions
{
    /// <summary>
    /// 
    /// </summary>
    public class AutomatedMissionsTestReport : AutomatedTestReport
    {
        #region Constants
        /// <summary>
        /// Report description.
        /// </summary>
        private const string c_description = "Generates a report showing information for a single automated mission's test results .";

        /// <summary>
        /// Directory where cached version of the p4 files are stored.
        /// </summary>
        private const string c_cacheDir = @"cache:\Reports\Automated\Missions";
        #endregion // Constants

        #region Private Member Data
        /// <summary>
        /// Place to save cached p4 files to.
        /// </summary>
        protected override string CacheDir
        {
            get
            {
                return c_cacheDir;
            }
        }

        private P4 perforceConnection;
        #endregion // Private Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="graph"></param>
        public AutomatedMissionsTestReport(string missionId, string depotFile, P4 perforceConnection)
            : base(missionId, c_description, depotFile, perforceConnection)
        {
            this.perforceConnection = perforceConnection;
            SmokeTests.Add(new FpsSmokeTest());
            SmokeTests.Add(new CpuSmokeTest());
            SmokeTests.Add(new ScriptMemSmokeTest());
            SmokeTests.Add(new ThreadSmokeTest());
        }
        #endregion // Constructor(s)

        #region Methods
        protected override P4 GetPerforceConnection()
        {
            return this.perforceConnection;
        }
        #endregion
    } // AutomatedMissionsTestReport
}
