﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using RSG.Base.Windows.Controls.WpfViewport2D;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using RSG.Base.Math;
using RSG.Model.Common;
using RSG.Base.Attributes;
using System.ComponentModel;
using RSG.Model.Common.Map;
using RSG.Model.Common.Extensions;
using RSG.Base.Extensions;
using System.Threading;
using RSG.Base.Tasks;
using RSG.Base.Windows.Dialogs;
using RSG.Model.Common.Util;
using MapViewport.AddIn;
using RSG.Base.Windows.Helpers;
using System.IO;
using RSG.Base.Drawing;

namespace Workbench.AddIn.MapDebugging.Overlays
{
    /// <summary>
    /// 
    /// </summary>
    public enum EntityPriorities
    {
        [FriendlyName("0")]
        [Description("Required")]
        Level0,

        [FriendlyName("1")]
        [Description("High")]
        Level1,

        [FriendlyName("2")]
        [Description("Medium")]
        Level2,

        [FriendlyName("3")]
        [Description("Low")]
        Level3,

        [FriendlyName("1-3")]
        [Description("High to Low")]
        Level13,
    }

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension("Workbench.AddIn.MapDebugging.OverlayGroups.MapDebug", typeof(Viewport.AddIn.ViewportOverlay))]
    public class MapInstancePriorityLevel : LevelDependentViewportOverlay
    {
        #region Constants
        /// <summary>
        /// Overlay name/description
        /// </summary>
        private const String c_name = "Entity Priority";
        private const String c_description = "Displays map entities that have a specific priority level.";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public EntityPriorities SelectedPriorityLevel
        {
            get { return m_selectedPriorityLevel; }
            set
            {
                SetPropertyValue(value, () => SelectedPriorityLevel,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_selectedPriorityLevel = (EntityPriorities)newValue;
                            OnRenderOptionChanged();
                        }
                ));
            }
        }
        private EntityPriorities m_selectedPriorityLevel;

        /// <summary>
        /// 
        /// </summary>
        public bool ShowAll
        {
            get { return m_showAll; }
            set
            {
                SetPropertyValue(value, () => ShowAll,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_showAll = (bool)newValue;
                            OnRenderOptionChanged();
                        }
                ));
            }
        }
        private bool m_showAll;

        /// <summary>
        /// 
        /// </summary>
        public bool ShowLowPriority
        {
            get { return m_showLowPriority; }
            set
            {
                SetPropertyValue(value, () => ShowLowPriority,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_showLowPriority = (bool)newValue;
                            OnRenderOptionChanged();
                        }
                ));
            }
        }
        private bool m_showLowPriority;

        /// <summary>
        /// 
        /// </summary>
        public bool ShowNonLowPriority
        {
            get { return m_showNonLowPriority; }
            set
            {
                SetPropertyValue(value, () => ShowNonLowPriority,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_showNonLowPriority = (bool)newValue;
                            OnRenderOptionChanged();
                        }
                ));
            }
        }
        private bool m_showNonLowPriority;

        /// <summary>
        /// 
        /// </summary>
        public bool ShowOneToThreePriority
        {
            get { return m_showOneToThreePriority; }
            set
            {
                SetPropertyValue(value, () => ShowOneToThreePriority,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_showOneToThreePriority = (bool)newValue;
                            OnRenderOptionChanged();
                        }
                ));
            }
        }
        private bool m_showOneToThreePriority;

        /// <summary>
        /// 
        /// </summary>
        public bool IncludeDynamic
        {
            get { return m_includeDynamic; }
            set
            {
                SetPropertyValue(value, () => IncludeDynamic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_includeDynamic = (bool)newValue;
                            OnRenderOptionChanged();
                        }
                ));
            }
        }
        private bool m_includeDynamic;

        /// <summary>
        /// 
        /// </summary>
        public bool IncludeStatic
        {
            get { return m_includeStatic; }
            set
            {
                SetPropertyValue(value, () => IncludeStatic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_includeStatic = (bool)newValue;
                            OnRenderOptionChanged();
                        }
                ));
            }
        }
        private bool m_includeStatic;

        /// <summary>
        /// 
        /// </summary>
        public bool ShowAsDensity
        {
            get { return m_ShowAsDensity; }
            set
            {
                SetPropertyValue(value, () => ShowAsDensity,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_ShowAsDensity = (bool)newValue;
                            OnRenderOptionChanged();
                        }
                ));
            }
        }
        private bool m_ShowAsDensity;

        /// <summary>
        /// Command that gets fired off when the "Export to .csv File" button is pressed.
        /// </summary>
        public RelayCommand ExportNonLodedToCSV
        {
            get
            {
                if (m_ExportNonLodedToCSV == null)
                {
                    m_ExportNonLodedToCSV = new RelayCommand(param => ExportNonLodedToCsvFile(param));
                }

                return m_ExportNonLodedToCSV;
            }
        }
        private RelayCommand m_ExportNonLodedToCSV;

        /// <summary>
        /// Command that gets fired off when the "Export to .csv File" button is pressed.
        /// </summary>
        public RelayCommand ExportLodedToCSV
        {
            get
            {
                if (m_ExportLodedToCSV == null)
                {
                    m_ExportLodedToCSV = new RelayCommand(param => ExportLodedToCsvFile(param));
                }

                return m_ExportLodedToCSV;
            }
        }
        private RelayCommand m_ExportLodedToCSV;
        #endregion // Properties

        #region MEF Imports
        /// <summary>
        /// Content Browser
        /// </summary>
        [ImportExtension(ContentBrowser.AddIn.CompositionPoints.ContentBrowser, typeof(ContentBrowser.AddIn.IContentBrowser))]
        protected ContentBrowser.AddIn.IContentBrowser ContentBrowserProxy { get; set; }
        
        /// <summary>
        /// A reference to view model for the map viewport
        /// </summary>
        [ImportExtension(Viewport.AddIn.CompositionPoints.MapViewport, typeof(Viewport.AddIn.IMapViewport))]
        public Viewport.AddIn.IMapViewport MapViewportProxy { get; set; }
        #endregion // MEF Imports

        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public MapInstancePriorityLevel()
            : base(c_name, c_description)
        {
            m_selectedPriorityLevel = EntityPriorities.Level0;
            m_showAll = true;
            this.m_includeDynamic = true;
            this.m_includeStatic = true;
        }
        #endregion // Constructors

        #region Overrides

        /// <summary>
        /// Gets called when the user wants to view this overlay
        /// </summary>
        public override void Activated()
        {
            base.Activated();

            this.ContentBrowserProxy.GridSelectionChanged += GridSelectionChanged;
            this.SelectionChanged(this.ContentBrowserProxy.SelectedGridItems);
        }

        /// <summary>
        /// Gets called when the user turns off this overlay
        /// </summary>
        public override void Deactivated()
        {
            this.ContentBrowserProxy.GridSelectionChanged -= GridSelectionChanged;
            base.Deactivated();
        }

        /// <summary>
        /// Gets the control that should be shown in the overlay details panel
        /// </summary>
        public override System.Windows.Controls.Control GetOverlayControl()
        {
            return new OverlayViews.PriorityLevelOverlayView();
        }
        #endregion // Overrides

        #region Event Handling
        /// <summary>
        /// Gets called when any stat render option changes and we need to redraw the geometry
        /// </summary>
        void OnRenderOptionChanged()
        {
            RefreshGeometry(GetValidSectionSelection(this.ContentBrowserProxy.SelectedGridItems));
        }

        /// <summary>
        /// Get called when the items selected in the content browser change
        /// </summary>
        void GridSelectionChanged(Object sender, ContentBrowser.AddIn.GridSelectionChangedEventArgs args)
        {
            SelectionChanged(args.NewItem);
        }

        private ISet<IMapSection> validSections;

        /// <summary>
        /// Get called when the items selected in the content browser change and when
        /// the overlay is first shown
        /// </summary>
        private void SelectionChanged(IEnumerable<IAsset> selectedAssets)
        {
            MapViewportProxy.SelectedOverlayGeometry.Clear();

            // Retrieve the list of sections based on what the user has selected in the content browser
            this.validSections = GetValidSectionSelection(selectedAssets);

            // Ensure the required data is loaded
            CancellationTokenSource cts = new CancellationTokenSource();
            SectionTaskContext context = new SectionTaskContext(cts.Token, validSections);

            ITask loadDataTask = new ActionTask("Load Data", (ctx, progress) => EnsureDataLoaded((SectionTaskContext)ctx, progress));

            TaskExecutionDialog dialog = new TaskExecutionDialog();
            TaskExecutionViewModel vm = new TaskExecutionViewModel("Generating Overlay", loadDataTask, cts, context);
            vm.AutoCloseOnCompletion = true;
            dialog.DataContext = vm;
            Nullable<bool> selectionResult = dialog.ShowDialog();
            if (false == selectionResult)
            {
                return;
            }

            // Update the geometry that is rendered on screen
            RefreshGeometry(validSections);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="progress"></param>
        private void EnsureDataLoaded(SectionTaskContext context, IProgress<TaskProgress> progress)
        {
            float increment = 1.0f / context.Sections.Count();

            // Load all the map section data
            foreach (IMapSection section in context.Sections)
            {
                context.Token.ThrowIfCancellationRequested();

                // Update the progress information
                string message = String.Format("Loading {0}", section.Name);
                progress.Report(new TaskProgress(increment, true, message));

                section.LoadStats(new StreamableStat[] { StreamableMapSectionStat.Entities });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="selectedAssets"></param>
        /// <returns></returns>
        private ISet<IMapSection> GetValidSectionSelection(IEnumerable<IAsset> selectedAssets)
        {
            ISet<IMapSection> selectedSections = new HashSet<IMapSection>();

            foreach (IAsset asset in selectedAssets)
            {
                if (asset is ILevel)
                {
                    selectedSections.AddRange((asset as ILevel).GetMapHierarchy().AllSections);
                }
                else if (asset is IMapArea)
                {
                    selectedSections.AddRange((asset as IMapArea).AllDescendentSections);
                }
                else if (asset is IMapSection)
                {
                    selectedSections.Add(asset as IMapSection);
                }
            }

            return selectedSections;
        }

        /// <summary>
        /// Creates the viewport geometry for the given map sections
        /// </summary>
        /// <param name="validSections"></param>
        private void RefreshGeometry(IEnumerable<IMapSection> validSections)
        {
            this.Geometry.BeginUpdate();
            this.Geometry.Clear();

            if (this.ShowAsDensity == false)
            {
                Brush brush = Brushes.Black;
                Color colour = Colors.Green;
                foreach (IMapSection section in validSections)
                {
                    foreach (IEntity entity in section.ChildEntities)
                    {
                        if (ShouldRenderEntity(entity))
                        {
                            Viewport2DCircle newInst = new Viewport2DCircle(etCoordSpace.World,
                                                       String.Empty, new Vector2f(entity.Position.X, entity.Position.Y),
                                                       10.0f, Colors.Green, 2);
                            this.Geometry.Add(newInst);
                        }
                    }
                }
            }
            else
            {

                HashSet<IMapSection> storedSections = new HashSet<IMapSection>();
                IDictionary<int, IList<IMapSection>> sections = new SortedDictionary<int, IList<IMapSection>>();
                int maximumValue = int.MinValue;
                int minimumValue = int.MaxValue;

                foreach (IMapSection section in validSections)
                {
                    if (storedSections.Contains(section))
                    {
                        continue;
                    }

                    storedSections.Add(section);
                    int count = section.ChildEntities.Where(e => this.ShouldRenderEntity(e)).Count();

                    if (section.IsPropGroup)
                    {
                        // If main container is section use that and add counts together
                        // else just use main container with this count.
                        IMapSection mainContainer = validSections.Where(s => object.ReferenceEquals(s.PropGroup, section)).FirstOrDefault();
                        if (mainContainer == null)
                        {
                            mainContainer = section.MapHierarchy.AllSections.Where(s => object.ReferenceEquals(s.PropGroup, section)).FirstOrDefault();
                            if (mainContainer != null)
                            {
                                if (!sections.ContainsKey(count))
                                {
                                    sections.Add(count, new List<IMapSection>());
                                }

                                sections[count].Add(mainContainer);
                            }
                        }
                        else
                        {
                            count += mainContainer.ChildEntities.Where(e => this.ShouldRenderEntity(e)).Count();
                            if (!sections.ContainsKey(count))
                            {
                                sections.Add(count, new List<IMapSection>());
                            }

                            storedSections.Add(mainContainer);
                            sections[count].Add(mainContainer);
                        }
                    }
                    else
                    {
                        IMapSection propGroup = validSections.Where(s => object.ReferenceEquals(s, section.PropGroup)).FirstOrDefault();
                        if (propGroup != null)
                        {
                            count += propGroup.ChildEntities.Where(e => this.ShouldRenderEntity(e)).Count();
                            storedSections.Add(propGroup);
                        }

                        if (!sections.ContainsKey(count))
                        {
                            sections.Add(count, new List<IMapSection>());
                        }

                        sections[count].Add(section);
                    }

                    if (count > maximumValue)
                    {
                        maximumValue = count;
                    }
                    if (count < minimumValue)
                    {
                        minimumValue = count;
                    }
                }


                int totalStat = sections.Sum(item => item.Key * item.Value.Count);
                int totalCount = sections.Sum(item => item.Value.Count);
                if (sections.Count > 0)
                {
                    Color colour = Colors.White;
                    double statRange = maximumValue - minimumValue;
                    int sectionRenderedCount = 0;

                    foreach (KeyValuePair<int, IList<IMapSection>> sectionValue in sections.Reverse())
                    {
                        if (sectionValue.Key == maximumValue && sectionValue.Key == minimumValue)
                        {
                            colour = Colors.White;
                        }
                        else if (sectionValue.Key == 0)
                        {
                            colour = Colors.White;
                        }
                        else
                        {
                            double percentage = (1.0 - (sectionValue.Key - minimumValue) / statRange);

                            // Uses green -> red range (dividing by 3 below is same as *0.33333 which equates to red in hues between 0 and 1)
                            byte h = (byte)((percentage / 3.0) * 255);
                            byte s = (byte)(0.9 * 255);
                            byte v = (byte)(0.9 * 255);
                            System.Drawing.Color drawingColor = ColourSpaceConv.HSVtoColor(new HSV(h, s, v));
                            colour = Color.FromRgb(drawingColor.R, drawingColor.G, drawingColor.B);
                        }

                        foreach (IMapSection section in sectionValue.Value)
                        {
                            if (section.VectorMapPoints != null)
                            {
                                IList<Vector2f> points = section.VectorMapPoints;
                                Viewport2DShape newGeometry = new Viewport2DShape(etCoordSpace.World, String.Empty, points.ToArray(), colour, 1, colour);
                                newGeometry.PickData = section;
                                this.Geometry.Add(newGeometry);

                                sectionRenderedCount++;
                            }

                            //if (this.OnlyWorstCases == true && sectionRenderedCount == this.WorstCaseCount)
                            //{
                            //    break;
                            //}
                        }

                        //if (this.OnlyWorstCases == true && sectionRenderedCount == this.WorstCaseCount)
                        //{
                        //    break;
                        //}
                    }
                }

                //this.MaximumGeometryCount = System.Math.Max(1, sections.SelectMany(item => item.Value).Select(item => item.VectorMapPoints != null).Count());
            }

            this.Geometry.EndUpdate();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        private bool ShouldRenderEntity(IEntity entity)
        {
            bool isDynamic = false;
            ISimpleMapArchetype archetype = entity.ReferencedArchetype as ISimpleMapArchetype;
            if (archetype != null)
            {
                isDynamic = archetype.IsDynamic;
            }

            if (isDynamic && this.IncludeDynamic == false)
            {
                return false;
            }

            if (!isDynamic && this.IncludeStatic == false)
            {
                return false;
            }

            switch (this.SelectedPriorityLevel)
            {
                case EntityPriorities.Level0:
                    if (entity.PriorityLevel != 0)
                    {
                        return false;
                    }
                    break;
                case EntityPriorities.Level1:
                    if (entity.PriorityLevel != 1)
                    {
                        return false;
                    }
                    break;
                case EntityPriorities.Level2:
                    if (entity.PriorityLevel != 2)
                    {
                        return false;
                    }
                    break;
                case EntityPriorities.Level3:
                    if (entity.PriorityLevel != 3)
                    {
                        return false;
                    }
                    break;
                case EntityPriorities.Level13:
                    if (entity.PriorityLevel == 0)
                    {
                        return false;
                    }
                    break;
                default:
                    break;
            }

            return ShowAll || (entity.IsLowPriority == true && ShowLowPriority) || (entity.IsLowPriority == false && ShowNonLowPriority);
        }

        private void ExportNonLodedToCsvFile(object parameter)
        {
            this.ExportToCsvFile(e => e.IsReference && e.LodParent == null && e.IsInteriorReference == false && e.ReferencedArchetype is ISimpleMapArchetype);
        }

        private void ExportLodedToCsvFile(object parameter)
        {
            this.ExportToCsvFile(e => e.IsReference && e.LodParent != null && e.IsInteriorReference == false && e.ReferencedArchetype is ISimpleMapArchetype);
        }

        private void ExportToCsvFile(Func<IEntity, bool> predicate)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.Filter = "CSV (Comma delimited)|*.csv";
            Nullable<bool> result = dlg.ShowDialog();
            if (false == result)
            {
                return;
            }

            CancellationTokenSource cts = new CancellationTokenSource();
            LevelTaskContext context = new LevelTaskContext(cts.Token, LevelBrowserProxy.Value.SelectedLevel);
            IMapHierarchy hierarchy = context.MapHierarchy.AllSections.First().MapHierarchy;

            ITask dataLoadTask = hierarchy.CreateStatLoadTask(context.MapHierarchy.AllSections, new StreamableStat[] { StreamableMapSectionStat.Entities });

            if (dataLoadTask != null)
            {
                TaskExecutionDialog dialog = new TaskExecutionDialog();
                TaskExecutionViewModel vm = new TaskExecutionViewModel("Generating Data For CSV Report...", dataLoadTask, cts, context);
                vm.AutoCloseOnCompletion = true;
                dialog.DataContext = vm;
                Nullable<bool> selectionResult = dialog.ShowDialog();
                if (false == selectionResult)
                {
                    return;
                }
            }

            using (StreamWriter sw = new StreamWriter(dlg.FileName))
            {
                string header = "Section, Parent Area ,Grandparent Area,Total Prop Count,Static Prop %,Static Prop Count,Dynamic Prop %,Dynamic Prop Count,";
                header += "Total P0 %,Total P0 Count,Total P1 %,Total P1 Count,Total P2 %,Total P2 Count,Total P3 %,Total P3 Count,";
                header += "Static P0 %,Static P0 Count,Static P1 %,Static P1 Count,Static P2 %,Static P2 Count,Static P3 %,Static P3 Count,";
                header += "Dyn P0 %,Dyn P0 Count,Dyn P1 %,Dyn P1 Count,Dyn P2 %,Dyn P2 Count,Dyn P3 %,Dyn P3 Count,";
                sw.WriteLine(header);

                foreach (var section in context.MapHierarchy.AllSections)
                {
                    if (section == null)
                    {
                        continue;
                    }

                    string sectionName = section.Name;
                    string parentName = "n/a";
                    string grandParentName = "n/a";
                    IMapArea parent = null;
                    IMapArea grandParent = null;
                    parent = section.ParentArea;
                    if (parent != null)
                    {
                        parentName = parent.Name;
                        grandParent = parent.ParentArea;
                        if (grandParent != null)
                        {
                            grandParentName = grandParent.Name;
                        }
                    }

                    List<IEntity> references = (section.ChildEntities.Where(predicate)).ToList();
                    List<IEntity> staticReferences = (references.Where(e => (e.ReferencedArchetype as ISimpleMapArchetype).IsDynamic == false)).ToList();
                    List<IEntity> dynamicReferences = (references.Where(e => (e.ReferencedArchetype as ISimpleMapArchetype).IsDynamic == true)).ToList();

                    int totalPropCount = references.Count;
                    float inversePropCount = totalPropCount > 0 ? 1.0f / totalPropCount : 0.0f;
                    int totalStaticCount = staticReferences.Count;
                    float staticPropPercent = totalStaticCount * inversePropCount;
                    int totalDynamicCount = totalPropCount - totalStaticCount;
                    float dynamicPropPercent = totalDynamicCount * inversePropCount;

                    string baseValues = string.Format("{0},{1},{2},{3},{4},{5},{6},{7}",
                        sectionName,
                        parentName,
                        grandParentName,
                        totalPropCount,
                        staticPropPercent.ToString("P2"),
                        totalStaticCount,
                        dynamicPropPercent.ToString("P2"),
                        totalDynamicCount);

                    int totalP0Count = (references.Where(e => e.PriorityLevel == 0)).Count();
                    float totalP0Percent = totalP0Count * inversePropCount;
                    int totalP1Count = (references.Where(e => e.PriorityLevel == 1)).Count();
                    float totalP1Percent = totalP1Count * inversePropCount;
                    int totalP2Count = (references.Where(e => e.PriorityLevel == 2)).Count();
                    float totalP2Percent = totalP2Count * inversePropCount;
                    int totalP3Count = (references.Where(e => e.PriorityLevel == 3)).Count();
                    float totalP3Percent = totalP3Count * inversePropCount;

                    string totals = string.Format("{0},{1},{2},{3},{4},{5},{6},{7}",
                        totalP0Percent.ToString("P2"),
                        totalP0Count,
                        totalP1Percent.ToString("P2"),
                        totalP1Count,
                        totalP2Percent.ToString("P2"),
                        totalP2Count,
                        totalP3Percent.ToString("P2"),
                        totalP3Count);

                    int staticP0Count = (staticReferences.Where(e => e.PriorityLevel == 0)).Count();
                    float staticP0Percent = staticP0Count * inversePropCount; ;
                    int staticP1Count = (staticReferences.Where(e => e.PriorityLevel == 1)).Count();
                    float staticP1Percent = staticP1Count * inversePropCount; ;
                    int staticP2Count = (staticReferences.Where(e => e.PriorityLevel == 2)).Count();
                    float staticP2Percent = staticP2Count * inversePropCount; ;
                    int staticP3Count = (staticReferences.Where(e => e.PriorityLevel == 3)).Count();
                    float staticP3Percent = staticP3Count * inversePropCount; ;

                    string staticTotals = string.Format("{0},{1},{2},{3},{4},{5},{6},{7}",
                        staticP0Percent.ToString("P2"),
                        staticP0Count,
                        staticP1Percent.ToString("P2"),
                        staticP1Count,
                        staticP2Percent.ToString("P2"),
                        staticP2Count,
                        staticP3Percent.ToString("P2"),
                        staticP3Count);

                    int dynamicP0Count = (dynamicReferences.Where(e => e.PriorityLevel == 0)).Count();
                    float dynamicP0Percent = dynamicP0Count * inversePropCount; ;
                    int dynamicP1Count = (dynamicReferences.Where(e => e.PriorityLevel == 1)).Count();
                    float dynamicP1Percent = dynamicP1Count * inversePropCount; ;
                    int dynamicP2Count = (dynamicReferences.Where(e => e.PriorityLevel == 2)).Count();
                    float dynamicP2Percent = dynamicP2Count * inversePropCount; ;
                    int dynamicP3Count = (dynamicReferences.Where(e => e.PriorityLevel == 3)).Count();
                    float dynamicP3Percent = dynamicP3Count * inversePropCount; ;

                    string dynamicTotals = string.Format("{0},{1},{2},{3},{4},{5},{6},{7}",
                        dynamicP0Percent.ToString("P2"),
                        dynamicP0Count,
                        dynamicP1Percent.ToString("P2"),
                        dynamicP1Count,
                        dynamicP2Percent.ToString("P2"),
                        dynamicP2Count,
                        dynamicP3Percent.ToString("P2"),
                        dynamicP3Count);

                    string record = string.Format("{0},{1},{2},{3}",
                        baseValues, totals, staticTotals, dynamicTotals);

                    sw.WriteLine(record);
                }
            }
        }
        #endregion // Event Handling
    } // MapInstancePriorityLevel
} // Workbench.AddIn.MapDebugging.Overlays
