﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn.Services;

namespace Viewport.AddIn
{
    /// <summary>
    /// 
    /// </summary>
    public interface IMapViewportSettings : ISettings
    {
        bool DisplayGrid { get; set; }
        uint MajorLineSpacing { get; set; }
        uint MinorLineSpacing { get; set; }
        string SelectedOverlayGroup { get; set; }
        string SelectedOverlay { get; set; }
    } // IMapViewportSettings
}
