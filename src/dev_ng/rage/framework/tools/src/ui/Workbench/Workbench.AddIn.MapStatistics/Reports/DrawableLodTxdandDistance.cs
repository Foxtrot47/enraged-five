﻿namespace Workbench.AddIn.MapStatistics.Reports
{
    using RSG.Model.Report2.Reports.Map;

    /// <summary>
    /// Workbench export wrapper around the DrawableLodTxdsAndDistances report.
    /// </summary>
    [ExportExtension(Report.AddIn.ExtensionPoints.MapStatsReport, typeof(RSG.Model.Report.IReport))]
    public class DrawableLodTxdandDistance : DrawableLodTxdsAndDistances
    {
    } // Workbench.AddIn.MapStatistics.Reports.DrawableLodTxdandDistance {Class}
} // Workbench.AddIn.MapStatistics.Reports {Namespace}
