﻿using System;
using System.Windows.Input;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Windows;
using RSG.Base.Logging;
using Workbench.AddIn;
using Workbench.AddIn.Commands;
using Workbench.AddIn.Services;
using Workbench.AddIn.Services.File;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.UI.Menu;

namespace Workbench.UI.Menu
{
    /// <summary>
    /// Top-level File menu.
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.MenuMain, typeof(IWorkbenchCommand))]
    public class FileMenu : WorkbenchMenuItemBase,
        IPartImportsSatisfiedNotification
    {
        #region MEF Imports

        /// <summary>
        /// Extensions service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ExtensionService)]
        private IExtensionService ExtensionService { get; set; }

        /// <summary>
        /// File menu subitems.
        /// </summary>
        [ImportManyExtension(Workbench.AddIn.ExtensionPoints.MenuFile, typeof(IWorkbenchCommand))]
        private IEnumerable<IWorkbenchCommand> items { get; set; }

        #endregion // MEF Imports

        #region Constructor(s)

        /// <summary>
        /// Default constructor.
        /// </summary>
        public FileMenu()
        {
            this.Header = "_File";
            this.ID = new Guid(Workbench.AddIn.ExtensionPoints.MenuFile);
            this.RelativeID = new Guid(Workbench.AddIn.ExtensionPoints.MenuHelp);
            this.Direction = AddIn.Services.Direction.Before;
        }

        #endregion // Constructor(s)

        #region IPartImportsSatisfiedNotification Methods

        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {
            this.Items = ExtensionService.Sort(items);
        }

        #endregion // IPartImportsSatisfiedNotification Methods

        #region ICommand Implementation

        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
        }

        #endregion // ICommand Implementation
    } // FileMenu

    /// <summary>
    /// File, Separator.
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.MenuFile, typeof(IWorkbenchCommand))]
    class FileMenuSep1 : WorkbenchMenuItemBase
    {
        #region Constants

        public static readonly Guid GUID = new Guid("411D5A04-57EA-41CB-887A-82B2E2E227F1");

        #endregion // Constants

        #region Constructor(s)

        /// <summary>
        /// Default constructor.
        /// </summary>
        public FileMenuSep1()
        {
            this.IsSeparator = true;
            this.ID = GUID;
            this.RelativeID = new Guid(Workbench.AddIn.CompositionPoints.MenuFileOpen);
            this.Direction = Direction.After;
        }

        #endregion // Constructor(s)

        #region ICommand Implementation

        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
        }

        #endregion // ICommand Implementation
    } // FileMenuSep1

    /// <summary>
    /// File menu, Close menu item.
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.MenuFile, typeof(IWorkbenchCommand))]
    class CloseMenuItem : WorkbenchMenuItemBase
    {
        #region MEF Imports

        /// <summary>
        /// Workbench Layout Manager.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager, typeof(ILayoutManager))]
        private ILayoutManager LayoutManager { get; set; }

        #endregion // MEF Imports

        #region Constructor(s)

        /// <summary>
        /// Default constructor.
        /// </summary>
        public CloseMenuItem()
        {
            this.Header = "_Close";
            this.ID = new Guid(Workbench.AddIn.CompositionPoints.MenuFileClose);
            this.RelativeID = FileMenuSep1.GUID;
            this.Direction = Direction.After;
            this.KeyGesture = new KeyGesture(Key.W, ModifierKeys.Control);
        }

        #endregion // Constructor(s)

        #region ICommand Implementation

        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return this.LayoutManager.ActiveDocument() != null;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
            Log.Log__Debug("CloseMenuItem::Execute()");
            this.LayoutManager.CloseDocument(this.LayoutManager.ActiveDocument());
        }

        #endregion // ICommand Implementation
    } // CloseMenuItem

    /// <summary>
    /// File menu, Close All menu item.
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.MenuFile, typeof(IWorkbenchCommand))]
    class CloseAllMenuItem : WorkbenchMenuItemBase
    {
        #region MEF Imports
        /// <summary>
        /// Workbench Layout Manager.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager,
            typeof(ILayoutManager))]
        private ILayoutManager LayoutManager { get; set; }
        #endregion // MEF Imports

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public CloseAllMenuItem()
        {
            this.Header = "Close All";
            ID = new Guid(Workbench.AddIn.CompositionPoints.MenuFileCloseAll);
            RelativeID = new Guid(Workbench.AddIn.CompositionPoints.MenuFileClose);
            Direction = Direction.After;
        }
        #endregion // Constructor(s)

        #region ICommand Implementation

        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return this.LayoutManager.Documents.Count > 0;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
            Log.Log__Debug("CloseAllMenuItem::Execute()");
            this.LayoutManager.CloseAllDocuments();
        }

        #endregion // ICommand Implementation
    } // CloseAllMenuItem

    /// <summary>
    /// File menu, second separator.
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.MenuFile, typeof(IWorkbenchCommand))]
    class FileMenuSep2 : WorkbenchCommandSeparator
    {
        #region Constants
        public static readonly Guid GUID = new Guid("565ADA93-2ACF-4969-8F6D-2E82EF0B0C66");
        #endregion // Constants

        #region Constructor(s)

        /// <summary>
        /// Default constructor.
        /// </summary>
        public FileMenuSep2()
        {
            this.IsSeparator = true;
            this.ID = GUID;
            this.RelativeID = new Guid(Workbench.AddIn.CompositionPoints.MenuFileCloseAll);
            this.Direction = Direction.After;
        }

        #endregion // Constructor(s)
    } // FileMenuSep2

    /// <summary>
    /// File, Save All Menu Item.
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.MenuFile, typeof(IWorkbenchCommand))]
    [ExportExtension(Workbench.AddIn.ExtensionPoints.ToolbarStandard, typeof(IWorkbenchCommand))]
    class SaveAllFileMenuItem : WorkbenchMenuItemBase
    {
        #region Events

        public event DocumentSavedHandler DocumentSaved;

        #endregion // Events

        #region MEF Imports

        /// <summary>
        /// MEF import for all ISaveServices.
        /// </summary>
        [ImportManyExtension(Workbench.AddIn.ExtensionPoints.SaveService, typeof(ISaveService))]
        private IEnumerable<ISaveService> Services { get; set; }

        /// <summary>
        /// MEF import for the ISaveAsService objects.
        /// </summary>
        [ImportManyExtension(Workbench.AddIn.ExtensionPoints.SaveService, typeof(ISaveService))]
        private IEnumerable<ISaveService> SaveServices { get; set; }

        /// <summary>
        /// Layout manager reference.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager, typeof(ILayoutManager))]
        private ILayoutManager LayoutManager { get; set; }

        [ImportExtension(Workbench.AddIn.CompositionPoints.MessageService,
            typeof(Workbench.AddIn.Services.IMessageService))]
        protected IMessageService MessageService { get; set; }

        #endregion // MEF Imports

        #region Constructor(s)

        /// <summary>
        /// Default constructor.
        /// </summary>
        public SaveAllFileMenuItem()
        {
            this.Header = "Save All";
            this.ID = new Guid(Workbench.AddIn.CompositionPoints.MenuFileSaveAll);
            this.RelativeID = new Guid(Workbench.AddIn.CompositionPoints.MenuFileSaveAs);
            this.RelativeToolBarID = new Guid(Workbench.AddIn.CompositionPoints.MenuFileSave);
            this.Direction = Direction.After;
            this.SetImageFromBitmap(Workbench.Resources.Images.SaveAll);
            this.KeyGesture = new KeyGesture(Key.S, ModifierKeys.Control | ModifierKeys.Shift);
            this.ToolTip = "Save All";
        }

        #endregion // Constructor(s)

        #region ICommand Implementation

        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
            Log.Log__Debug("SaveAllFileMenuItem::Execute()");
            List<ISaveService> services = new List<ISaveService>();
            foreach (IDocumentBase document in this.LayoutManager.Documents)
            {
                if (document.ID != Guid.Empty && document.ID != null)
                {
                    // Check that there is a save service available for this
                    // content
                    foreach (ISaveService service in this.Services)
                    {
                        List<Guid> supportedContent = new List<Guid>(service.SupportedContent);
                        if (supportedContent.Contains(document.ID))
                        {
                            if (service.CanExecuteSave() == true)
                            {
                                int filterIndex = -1;
                                String path = document.Path;
                                System.IO.FileInfo fi = null;
                                if (String.IsNullOrEmpty(document.Path))
                                {
                                    Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
                                    dlg.Filter = service.Filter;
                                    dlg.FilterIndex = service.DefaultFilterIndex;

                                    Nullable<bool> result = dlg.ShowDialog();
                                    if (false == result)
                                        return;

                                    filterIndex = dlg.FilterIndex;
                                    path = dlg.FileName;
                                }

                                if (path.Contains("\n"))
                                {
                                    String[] files = path.Split(new char[] { '\n' });
                                    RSG.Base.Editor.IModel saveModel = document.SaveModel;
                                    foreach (String file in files)
                                    {
                                        String savePath = file;
                                        fi = new System.IO.FileInfo(file);
                                        if (fi.Exists == true && fi.Attributes.HasFlag(System.IO.FileAttributes.ReadOnly))
                                        {
                                            Boolean save = HandleReadOnlyFile(ref savePath, ref filterIndex, fi, service);
                                            if (save == false)
                                            {
                                                continue;
                                            }
                                        }

                                        Boolean setPathToSavePath = true;
                                        bool saveResult = service.Save(saveModel, savePath, -1, ref setPathToSavePath);
                                        if (saveResult)
                                        {
                                            document.IsModified = false;
                                            Log.Log__Debug("SaveService {0} successful on file {1}.", service.ID, file);
                                        }
                                        else
                                        {
                                            Log.Log__Error("SaveService {0} failed on file {1}.", service.ID, file);
                                        }
                                    }
                                    if (this.DocumentSaved != null)
                                        this.DocumentSaved(document);
                                }
                                else
                                {
                                    // Check to see if path is readonly; if it is show a message box
                                    // that displays a warning to the user and make sure that the save process finishes
                                    String savePath = path;
                                    fi = new System.IO.FileInfo(path);
                                    if (fi.Exists == true && fi.Attributes.HasFlag(System.IO.FileAttributes.ReadOnly))
                                    {
                                        Boolean save = HandleReadOnlyFile(ref savePath, ref filterIndex, fi, service);
                                        if (save == false)
                                        {
                                            return;
                                        }
                                    }

                                    RSG.Base.Editor.IModel saveModel = document.SaveModel;
                                    Boolean setPathToSavePath = true;
                                    bool saveResult = service.Save(saveModel, savePath, -1, ref setPathToSavePath);
                                    if (saveResult)
                                    {
                                        if (this.DocumentSaved != null)
                                            this.DocumentSaved(document);

                                        document.IsModified = false;
                                        if (setPathToSavePath == true)
                                        {
                                            document.Path = savePath;
                                            document.Title = System.IO.Path.GetFileName(savePath);
                                        }
                                        Log.Log__Debug("SaveService {0} successful.", service.ID);
                                    }
                                    else
                                    {
                                        Log.Log__Error("SaveService {0} failed.", service.ID);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private Boolean HandleReadOnlyFile(ref String filename, ref int filterIndex, System.IO.FileInfo fi, ISaveService service)
        {
            Boolean save = false;

            Services.SaveReadOnlyWindow readonlyWindow = new Services.SaveReadOnlyWindow(filename);
            readonlyWindow.Owner = App.Current.MainWindow;
            readonlyWindow.ShowDialog();
            switch (readonlyWindow.Result)
            {
                case Workbench.Services.ReadOnlyResult.Cancel:
                    {
                    }
                    break;
                case Workbench.Services.ReadOnlyResult.Overwrite:
                    {
                        fi.IsReadOnly = false;
                        save = true;
                    }
                    break;
                case Workbench.Services.ReadOnlyResult.SaveAs:
                    {
                        Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
                        dlg.Filter = service.Filter;
                        dlg.FilterIndex = service.DefaultFilterIndex;

                        Nullable<bool> result = dlg.ShowDialog();
                        if (false == result)
                        {
                            save = false;
                        }
                        else
                        {
                            save = true;
                            filename = dlg.FileName;
                            filterIndex = dlg.FilterIndex;
                        }
                    }
                    break;
                default:
                    break;
            }
            return save;
        }

        #endregion // ICommand Implementation
    } // SaveAllFileMenuItem

    /// <summary>
    /// File menu, separator 3.
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.MenuFile, typeof(IWorkbenchCommand))]
    class FileMenuSep3 : WorkbenchCommandSeparator
    {
        #region Constants

        public static readonly Guid GUID = new Guid("3AF5F3F3-2085-4D86-979D-611833A22811");

        #endregion // Constants

        #region Constructor(s)

        /// <summary>
        /// Default constructor.
        /// </summary>
        public FileMenuSep3()
        {
            this.IsSeparator = true;
            this.ID = GUID;
            this.RelativeID = new Guid(Workbench.AddIn.CompositionPoints.MenuFileSaveAll);
            this.Direction = Direction.After;
        }

        #endregion // Constructor(s)
    } // FileMenuSep3

    /// <summary>
    /// File menu, separator 4.
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.MenuFile, typeof(IWorkbenchCommand))]
    class FileMenuSep4 : WorkbenchCommandSeparator
    {
        #region Constants

        public static readonly Guid GUID = new Guid("4944B8FE-3D3A-4880-92D8-82743A02B6EA");

        #endregion // Constants

        #region Constructor(s)

        /// <summary>
        /// Default constructor.
        /// </summary>
        public FileMenuSep4()
        {
            this.IsSeparator = true;
            this.ID = GUID;
            this.RelativeID = new Guid(Workbench.AddIn.CompositionPoints.MenuFileRecentFiles);
            this.Direction = Direction.After;
        }

        #endregion // Constructor(s)
    } // FileMenuSep4

    /// <summary>
    /// File -> Exit Menu Item.
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.MenuFile, typeof(IWorkbenchCommand))]
    class FileExitMenuItem : WorkbenchMenuItemBase
    {
        #region Constructor(s)

        /// <summary>
        /// Default constructor.
        /// </summary>
        public FileExitMenuItem()
        {
            this.Header = "E_xit";
            ID = new Guid(Workbench.AddIn.CompositionPoints.MenuFileExit);
            //RelativeID = FileMenuSep4.GUID;
            Direction = Direction.After;
            SetImageFromBitmap(Resources.Images.Exit);
            this.KeyGesture = new KeyGesture(Key.F4, ModifierKeys.Alt);
        }

        #endregion // Constructor(s)

        #region ICommand Implementation

        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
            Log.Log__Debug("FileExitMenuItem::Execute()");
            Application.Current.MainWindow.Close();
        }

        #endregion // ICommand Implementation
    } // FileExitMenuItem
} // Workbench.UI.Menu namespace
