﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Workbench.AddIn.UI.Layout;
using RSG.Interop.Bugstar.Search;

namespace Workbench.AddIn.Bugstar.Reports
{
    /// <summary>
    /// Interaction logic for BugstarGraphView.xaml
    /// </summary>
    public partial class BugstarGraphView : DocumentBase<BugstarGraphViewModel>
    {
        #region Constants
        private const String NAME = "BugstarGraphView";
        private const String TITLE = "Report: {0}";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public BugstarGraphView(Graph graph)
            : base(NAME, new BugstarGraphViewModel(graph))
        {
            InitializeComponent();
            this.Title = String.Format(TITLE, graph.Name);
        }
        #endregion // Constructor(s)
    } // BugstarGraphView
} // Workbench.AddIn.Bugstar.Reports
