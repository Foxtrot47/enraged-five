﻿using System;
using System.Diagnostics;
using RSG.Base.Editor;
using RSG.Base.Logging;
using Workbench.AddIn;
using Workbench.AddIn.Services.File;
using RSG.Model.GlobalTXD;
using System.Collections.Generic;
using Workbench.AddIn.UI.Layout;

namespace Workbench.AddIn.TXDParentiser.Services.File
{
    /// <summary>
    /// Service to save a parent txd to a new disk file.
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.SaveAsService, typeof(ISaveAsService))]
    class SaveAsService : ISaveAsService
    {
        #region Constants
        private readonly Guid GUID = new Guid("EA12F914-5F6A-4655-97B4-E95D6E33164F");
        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// MEF import for Documents (IDocument).
        /// </summary>
        [ImportManyExtension(Workbench.AddIn.TXDParentiser.CompositionPoints.TXDParentiserDocument, typeof(TXDParentiserViewModel))]
        private IEnumerable<TXDParentiserViewModel> Documents { get; set; }
        #endregion // MEF Imports

        #region Properties

        /// <summary>
        /// Service GUID.
        /// </summary>
        public Guid ID
        {
            get { return (GUID); }
        }

        /// <summary>
        /// Save dialog filter string.
        /// </summary>
        public String Filter
        {
            get { return "TXD documents (.xml)|*.xml"; }
        }

        /// <summary>
        /// Save dialog default filter index.
        /// </summary>
        public int DefaultFilterIndex
        {
            get { return 0; }
        }

        /// <summary>
        /// Array of supported document GUIDs.
        /// </summary>
        public Guid[] SupportedDocuments
        {
            get
            {
                List<Guid> docs = new List<Guid>();
                foreach (TXDParentiserViewModel d in this.Documents)
                {
                    docs.Add((d as IDocument).ID);
                }
                return (docs.ToArray());
            }
        }

        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public SaveAsService()
        {
        }
        #endregion // Constructors(s)

        #region Methods

        /// <summary>
        /// Save the specified model data to disk file.
        /// </summary>
        public bool SaveAs(IModel model, String filename, int filterIndex)
        {
            Debug.Assert(model is GlobalRoot, "Invalid model passed to TXD Parentiser SaveModel.");
            if (!(model is GlobalRoot))
                return (false);

            bool result = true;
            try
            {
                GlobalRoot root = (model as GlobalRoot);
                root.Serialise(filename);
            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex, "Exception during TXD Parentiser SaveService");
                result = false;
            }

            return (result);
        }

        #endregion // Methods

    }
}
