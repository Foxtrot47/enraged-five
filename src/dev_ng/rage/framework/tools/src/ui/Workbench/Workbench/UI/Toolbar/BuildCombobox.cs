﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using ContentBrowser.AddIn;
using RSG.Base.Logging;
using RSG.Base.Tasks;
using RSG.Model.Common;
using RSG.Statistics.Client.ServiceClients;
using RSG.Statistics.Common.Dto.GameAssets;
using Workbench.AddIn;
using Workbench.AddIn.Commands;
using Workbench.AddIn.Services;
using Workbench.AddIn.Services.Model;

namespace Workbench.UI.Toolbar
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.DataModeToolbar, typeof(IWorkbenchCommand))]
    [ExportExtension(Workbench.AddIn.CompositionPoints.BuildBrowser, typeof(IBuildBrowser))]
    class BuildCombobox : WorkbenchCommandCombobox, IPartImportsSatisfiedNotification, IBuildBrowser
    {
        #region Constants
        /// <summary>
        /// Unique GUID for the build combo box
        /// </summary>
        public static readonly Guid GUID = new Guid("5CFC8940-F138-4023-A045-76EFC1CCACC0");
        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.DataSourceBrowser, typeof(IDataSourceBrowser))]
        IDataSourceBrowser DataSourceBrowser { get; set; }

        /// <summary>
        /// MEF import for content browser settings
        /// </summary>
        [ImportExtension(ContentBrowser.AddIn.CompositionPoints.ContentBrowserSettings, typeof(ContentBrowser.AddIn.IContentBrowserSettings))]
        ContentBrowser.AddIn.IContentBrowserSettings Settings { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.TaskProgressService, typeof(ITaskProgressService))]
        private ITaskProgressService TaskProgressService { get; set; }
        #endregion // MEF Imports

        #region Events
        /// <summary>
        /// Gets called when the current build changes
        /// </summary>
        public event BuildChangedEventHandler BuildChanged;
        #endregion

        #region Properties
        /// <summary>
        /// The currently selected item in the build collection
        /// </summary>
        public BuildDto SelectedBuild
        {
            get
            {
                BuildComboboxItem label = SelectedItem as BuildComboboxItem;
                return (label != null ? label.Build : null);
            }
        }

        /// <summary>
        /// List of builds.
        /// TODO: Move this to a build collection that is exposed from somewhere else.
        /// </summary>
        public IList<BuildDto> AllBuilds { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public BuildCombobox()
            : base()
        {
            this.ID = GUID;
            this.RelativeToolBarID = BuildLabel.GUID;
            this.Direction = Direction.After;
        }
        #endregion // Constructor(s)

        #region IBuildBrowser Implementation
        /// <summary>
        /// 
        /// </summary>
        public void UpdateBuilds(DataSource source)
        {
            // Populate the list of builds
            List<IWorkbenchCommand> allItems = new List<IWorkbenchCommand>();
            List<BuildDto> filteredBuilds = new List<BuildDto>();

            try
            {
                // Get the list of builds
                using (BuildClient client = new BuildClient())
                {
                    BuildDtos dtos = client.GetAll();
                    List<BuildDto>builds = dtos.Items;
                    builds.Sort();
                    builds.Reverse();

                    foreach (BuildDto build in builds)
                    {
                        // Only add the build if it has some relevent stats associated with it
                        if (build.HasAssetStats ||
                            build.HasAutomatedEverythingStats || build.HasAutomatedMapOnlyStats || build.HasAutomatedNighttimeMapOnlyStats ||
                            build.HasMagDemoStats || build.HasMemShortfallStats || build.HasShapeTestStats)
                        {
                            allItems.Add(new BuildComboboxItem(build));
                            filteredBuilds.Add(build);
                        }
                    }
                }
            }
            catch (System.Exception)
            {
                Log.Log__Warning("Unable to populate the list of builds.");
            }

            this.IsEnabled = (source != DataSource.SceneXml);
            this.AllBuilds = filteredBuilds;
            this.Items = allItems;

            BuildDto previousBuild = null;
            if (!String.IsNullOrEmpty(Settings.Build))
            {
                previousBuild = AllBuilds.FirstOrDefault(item => item.Identifier == Settings.Build);

                if (previousBuild == null)
                {
                    Log.Log__Warning("Previous build with identifier '{0}' is no longer available.", Settings.Build);
                }
            }
            if (previousBuild == null)
            {
                previousBuild = AllBuilds.FirstOrDefault();
            }
            this.SelectedItem = Items.Cast<BuildComboboxItem>().FirstOrDefault(item => item.Build == previousBuild);
        }
        #endregion // IDataSourceBrowser Implementation

        #region WorkbenchCommandCombobox Overrides
        /// <summary>
        /// Callback for whenever the selected item changes
        /// </summary>
        /// <param name="oldValue"></param>
        /// <param name="newValue"></param>
        protected override void PostSelectedItemChanged(IWorkbenchCommand oldValue, IWorkbenchCommand newValue)
        {
            BuildComboboxItem oldLabel = oldValue as BuildComboboxItem;
            BuildComboboxItem newLabel = newValue as BuildComboboxItem;

            BuildDto oldBuild = (oldLabel != null ? oldLabel.Build : null);
            BuildDto newBuild = (newLabel != null ? newLabel.Build : null);

            if (newBuild != null)
            {
                Settings.Build = newBuild.Identifier.ToString();
                Settings.Apply();
            }

            if (BuildChanged != null)
            {
                BuildChanged(this, new BuildChangedEventArgs(oldBuild, newBuild));
            }
        }
        #endregion

        #region Private Functions
        /// <summary>
        /// Callback for when the datasource has changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnDataSourceChanged(Object sender, DataSourceChangedEventArgs e)
        {
            IsEnabled = (e.NewItem != DataSource.SceneXml);
        }
        #endregion // Private Functions

        #region IPartImportsSatisfiedNotification Methods
        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {
            DataSourceBrowser.DataSourceChanged += OnDataSourceChanged;
        }
        #endregion // IPartImportsSatisfiedNotification Methods
    } // ContentBrowserToolbarTestCombobox
} // ContentBrowser.UI.Toolbar
