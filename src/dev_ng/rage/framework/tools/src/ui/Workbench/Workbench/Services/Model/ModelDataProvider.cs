﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Linq;
using System.Text;
using RSG.Base.ConfigParser;
using RSG.Base.Configuration;
using RSG.Base.Tasks;
using RSG.Model.Asset.Level;
using RSG.Model.Common;
using RSG.Model.Common.Map;
using RSG.Model.Common.Util;
using RSG.Model.Weapon;
using RSG.Platform;
using Workbench.AddIn;
using Workbench.AddIn.Services;
using Workbench.AddIn.Services.Model;

namespace Workbench.Services.Model
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Workbench.AddIn.CompositionPoints.ModelDataProvider, typeof(IModelDataProvider))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class ModelDataProvider : IModelDataProvider, IPartImportsSatisfiedNotification, IDisposable
    {
        #region Member Data
        /// <summary>
        /// For now we only store a single collection.
        /// </summary>
        private ILevelCollection CurrentCollection { get; set; }

        /// <summary>
        /// Factory for level creation.
        /// </summary>
        private ILevelFactory LevelFactory { get; set; }

        /// <summary>
        /// Factory for creating the appropriate weapon collection (based off of the data source).
        /// </summary>
        private IWeaponFactory WeaponFactory { get; set; }
        #endregion // Member Data

        #region MEF Imports
        /// <summary>
        /// Config service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(IConfigurationService))]
        private IConfigurationService ConfigService { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.TaskProgressService, typeof(ITaskProgressService))]
        private ITaskProgressService TaskProgressService { get; set; }

        /// <summary>
        /// The perforce sync service
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.PerforceSyncService, typeof(IPerforceSyncService))]
        private Lazy<IPerforceSyncService> PerforceSyncService { get; set; }
        #endregion // MEF Imports

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ModelDataProvider()
        {
        }
        #endregion // Constructor(s)

        #region IModelDataProvider Implementation
        /// <summary>
        /// Retrieves a new level collection for the requested datasource/build.
        /// </summary>
        /// <returns></returns>
        public ILevelCollection RetrieveLevelCollection(DataSource source, String buildIdentifier = null)
        {
            if (CurrentCollection != null)
            {
                CurrentCollection.Dispose();
            }

            CurrentCollection = LevelFactory.CreateCollection(source, buildIdentifier);
            return CurrentCollection;
        }

        /// <summary>
        /// Requests that certain stats are loaded for a particular level.
        /// </summary>
        /// <param name="level"></param>
        /// <param name="statsToLoad"></param>
        public void LoadStats(ILevel level, IEnumerable<StreamableStat> statsToLoad, bool foreground = false)
        {
            throw new NotImplementedException();
            // The following is part of "the plan".
            /*
            TaskPriority priority = (foreground ? TaskPriority.Foreground : TaskPriority.Background);

            if (level is LocalLevel)
            {
                //ShowPerforceSyncDialog(level);
            }

            if (statsToLoad.Contains(StreamableLevelStat.Weapons))
            {
                ActionTask task = new ActionTask("Loading weapon collection", (ctx, progress) => LoadWeaponCollection(ctx, progress));
                TaskProgressService.Add(task, new LevelTaskContext(level), priority);
            }
            */
        }
        #endregion // IModelDataProvider Implementation

        #region Private Methods
        /// <summary>
        /// The following is part of "the plan".
        /// </summary>
        /// <param name="context"></param>
        /// <param name="progress"></param>
        private void LoadWeaponCollection(ITaskContext context, IProgress<TaskProgress> progress)
        {
            Debug.Assert(context is LevelTaskContext, "context isn't a LevelTaskContext");
            LevelTaskContext levelContext = (LevelTaskContext)context;

            // What kind of level is it?
            if (levelContext.Level is LocalLevel)
            {
                LocalLevel localLevel = (LocalLevel)levelContext.Level;
                localLevel.Weapons = WeaponFactory.CreateCollection(DataSource.SceneXml);
            }
            else if (levelContext.Level is DbLevel)
            {
                DbLevel dbLevel = (DbLevel)levelContext.Level;
                dbLevel.Weapons = WeaponFactory.CreateCollection(DataSource.Database, (dbLevel.ParentCollection as SingleBuildLevelCollection).BuildIdentifier);
            }
            else
            {
                String message = String.Format("Unknown level type '{0}' encounterd.", levelContext.Level.GetType().FullName);
                Debug.Assert(false, message);
                throw new ArgumentException(message);
            }
        }
        #endregion // Private Methods

        #region IPartImportsSatisfiedNotification Interface
        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {
            LevelFactory = new LevelFactory(ConfigService.Config, ConfigService.BugstarConfig);
            WeaponFactory = new WeaponFactory(ConfigService.Config);
        }
        #endregion // IPartImportsSatisfiedNotification Interface

        #region IDisposable Implementation
        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            if (CurrentCollection != null)
            {
                CurrentCollection.Dispose();
            }
        }
        #endregion // IDisposable Implementation
    } // ModelDataProvider
}
