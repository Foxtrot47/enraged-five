﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Workbench.AddIn.TXDParentiser
{
    /// <summary>
    /// Extension Points
    /// </summary>
    public static class ExtensionPoints
    {
        /// <summary>
        /// Point to import/export viewport overlays for the collision group overlay group
        /// </summary>
        public const String TxdParentiserOverlayGroup = "C47A0F39-F03B-4218-8EC1-D3E4BFD8DA00";
    } // ExtensionPoints
}
