﻿using System;
using System.ComponentModel.Composition;
using System.Text;
using System.Windows;
using RSG.Base.Windows.Dialogs;
using Workbench.AddIn;

namespace Workbench.Services
{
    
    /// <summary>
    /// User-Interface Service implementation.
    /// </summary>
    [ExportExtension(Workbench.AddIn.CompositionPoints.UserInterfaceService,
        typeof(Workbench.AddIn.Services.IUserInterfaceService))]
    class UserInterfaceService : Workbench.AddIn.Services.IUserInterfaceService
    {
        #region Properties
        /// <summary>
        /// Service identifier.
        /// </summary>
        public Guid ID
        {
            get { return (new Guid("43469B68-436B-493A-9358-682E6B9CE8C6")); }
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// User-interface title string.
        /// </summary>
        private readonly String Title;
        
        /// <summary>
        /// User login base64 encoded credentials.
        /// </summary>
        private String m_loginCredentials;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public UserInterfaceService()
        {
            this.Title = Resources.Strings.MainWindow_Title;
        }
        #endregion // Constructor(s)

        #region MessageBox Methods
        /// <summary>
        /// Show message box with simple message (OK button, no icon).
        /// </summary>
        /// <returns></returns>
        public MessageBoxResult Show(String messageBoxText)
        {
            if (Application.Current != null)
            {
                return (MessageBoxResult)Application.Current.Dispatcher.Invoke(
                    new Func<MessageBoxResult>
                    (
                        delegate()
                        {
                            if (Application.Current.MainWindow != null)
                            {
                                return (MessageBox.Show(Application.Current.MainWindow, messageBoxText, this.Title));
                            }
                            else
                            {
                                return (MessageBox.Show(messageBoxText, this.Title));
                            }
                        }
                    ),
                    System.Windows.Threading.DispatcherPriority.Send);
            }
            else
            {
                return (MessageBox.Show(messageBoxText, this.Title));
            }
        }

        /// <summary>
        /// Show message box with message and configurable buttons.
        /// </summary>
        /// <param name="messageBoxText"></param>
        /// <param name="buttons"></param>
        /// <returns></returns>
        public MessageBoxResult Show(String messageBoxText, MessageBoxButton buttons)
        {
            if (System.Windows.Application.Current != null)
            {
                return (MessageBoxResult)Application.Current.Dispatcher.Invoke(
                    new Func<MessageBoxResult>
                    (
                        delegate()
                        {

                            if (Application.Current.MainWindow != null)
                            {
                                return (MessageBox.Show(Application.Current.MainWindow, messageBoxText, this.Title, buttons));
                            }
                            else
                            {
                                return (MessageBox.Show(messageBoxText, this.Title, buttons));
                            }
                        }
                    ),
                    System.Windows.Threading.DispatcherPriority.Send);
            }
            else
            {
                return (MessageBox.Show(messageBoxText, this.Title, buttons));
            }
        }

        /// <summary>
        /// Show message box with message, configurable buttons and icon.
        /// </summary>
        /// <param name="messageBoxText"></param>
        /// <param name="buttons"></param>
        /// <param name="image"></param>
        /// <returns></returns>
        public MessageBoxResult Show(String messageBoxText, MessageBoxButton buttons, MessageBoxImage image)
        {
            if (System.Windows.Application.Current != null)
            {
                return (MessageBoxResult)Application.Current.Dispatcher.Invoke(
                    new Func<MessageBoxResult>
                    (
                        delegate()
                        {

                            if (Application.Current.MainWindow != null)
                            {
                                return (MessageBox.Show(Application.Current.MainWindow, messageBoxText, this.Title, buttons, image));
                            }
                            else
                            {
                                return (MessageBox.Show(messageBoxText, this.Title, buttons, image));
                            }
                        }
                    ),
                    System.Windows.Threading.DispatcherPriority.Send);
            }
            else
            {
                return (MessageBox.Show(messageBoxText, this.Title, buttons, image));
            }
        }
        #endregion // MessageBox Methods
        
        #region Login Methods
        /// <summary>
        /// Prompts the user for his login details
        /// </summary>
        /// <param name="loginPredicate"></param>
        /// <returns>true if everything is fine, false if an error was found</returns>
        public bool Login(Func<LoginDetails, LoginResults> loginPredicate)
        {
            return Login(loginPredicate, "Login Dialog", "Please enter your login details below:");
        }

        /// <summary>
        /// Prompts the user for his login details with the supplied dialog title/message
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public bool Login(Func<LoginDetails, LoginResults> loginPredicate, string title, string message)
        {
            // Check whether the user saved their details from their previous session
            if (!String.IsNullOrEmpty(m_loginCredentials))
            {
                string usernamePassword = new ASCIIEncoding().GetString(Convert.FromBase64String(m_loginCredentials));
                int colonIndex = usernamePassword.IndexOf(':');

                if (colonIndex != -1)
                {
                    String username = usernamePassword.Substring(0, colonIndex);
                    String password = usernamePassword.Substring(colonIndex + 1);

                    // Check whether they are still valid
                    LoginResults results = loginPredicate.Invoke(new LoginDetails { Username = username, Password = password });
                    if (results.Success)
                    {
                        return true;
                    }
                    else
                    {
                        m_loginCredentials = null;
                    }
                }
            }

            // Show the login dialog
            // Temporarily disable the remember me functionality and always remember the user (url:bugstar:617700).
            LoginViewModel vm = new LoginViewModel(loginPredicate, title, message, new Uri("pack://application:,,,/Workbench;component/Data/p4bugstar.png"));
            vm.Username = Environment.GetEnvironmentVariable("USERNAME");
            vm.IsUsernameReadOnly = true;

            LoginWindow loginWindow = new LoginWindow(vm);
            bool? result = loginWindow.ShowDialog();

            if (result == true)
            {
                string usernamePassword = String.Format("{0}:{1}", vm.Username, vm.Password);
                m_loginCredentials = Convert.ToBase64String(new ASCIIEncoding().GetBytes(usernamePassword));
                return true;
            }

            m_loginCredentials = null;
            return false;
        }
        #endregion // Login Methods
    }

} // Workbench.Services namespace
