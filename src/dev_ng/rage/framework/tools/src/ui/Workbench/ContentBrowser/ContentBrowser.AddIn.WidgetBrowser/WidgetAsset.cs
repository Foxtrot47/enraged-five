﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using RSG.Base.Collections;
using ragCore;
using ragWidgets;
using System.Diagnostics;
using RSG.Base.Editor.Command;
using RSG.Model.Common;
using System.Windows.Threading;

namespace ContentBrowser.AddIn.WidgetBrowser
{
    public enum WidgetType
    {
        Bank = 0,
        Group,
        Widget
    }

    public class WidgetAsset :  AssetContainerBase
    {
        #region Properties
        public Widget Model { get; set; }

        private WidgetType m_Type;
        public WidgetType Type
        {
            get { return m_Type; }
            set
            {
                SetPropertyValue( value, () => this.Type,
                                new PropertySetDelegate( delegate( Object newValue ) { m_Type = (WidgetType)newValue; } ) );
            }
        }

        #endregion

        #region Variables
        #endregion



        public WidgetAsset( Widget model )
        {
            Model = model;

            Model.WidgetAddedInstance += new WidgetEventHandler( model_WidgetAdded );
            model.ValueChanged += new EventHandler( model_ValueChanged );

            if ( Model is WidgetBank )
            {
                Type = WidgetType.Bank;
            }
            else if ( Model is WidgetGroup )
            {
                Type = WidgetType.Group;
            }
            else
            {
                Type = WidgetType.Widget;
            }

            Name = model.Title;
        }

        void model_ValueChanged( object sender, EventArgs e )
        {
            /*
            bool isModified = Model.IsModified();
            if ( IsModified != isModified )
            {
                IsModified = isModified;
                WidgetAsset parent = (Parent as WidgetAsset);
                if ( isModified && !parent.IsModified )
                {
                    parent.IsModified = true;
                }
                else if ( !isModified && parent.IsModified )
                {
                    // We went from modified to unmodified, and our parent
                    // thinks it's modified, so it needs to recheck all
                    // its children.
                    parent.UpdateIsModified();                                
                }                
            }
             * */
        }

        void model_WidgetAdded( object sender, WidgetEventArgs e )
        {
            Debug.Assert( Type != WidgetType.Widget );
 
            Dispatcher.CurrentDispatcher.Invoke( (Action)delegate()
            {
                WidgetAsset child = new WidgetAsset( e.Widget );
                //child.Parent = this;
                AssetChildren.Add( child );
            } );
        }

        public void UpdateIsModified()
        {
            /*
            foreach ( WidgetViewModel child in AssetChildren )
            {
                if ( child.IsModified )
                {
                    IsModified = true;
                    (Parent as WidgetViewModel).UpdateIsModified();
                    return;
                }
            }

            IsModified = Model.IsModified();
            (Parent as WidgetViewModel).UpdateIsModified();
        */
        }
    }
} // ns ContentBrowser.AddIn.WidgetBrowser
