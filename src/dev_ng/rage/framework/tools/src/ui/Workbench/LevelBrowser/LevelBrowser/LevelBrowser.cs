﻿using System;
using System.Windows;
using System.Xml;
using System.Xml.XPath;
using System.Windows.Controls;
using System.ComponentModel.Composition;
using System.Windows.Media.Imaging;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Workbench.AddIn;
using RSG.Base.Editor;
using RSG.Model.Common;
using System.Text.RegularExpressions;
using Workbench.AddIn.Services;
using ContentBrowser.AddIn;
using Workbench.AddIn.UI.Layout;
using RSG.Model.Asset;
using RSG.Model.Asset.Level;
using RSG.Model.Common.Map;
using RSG.Model.Common.Extensions;
using System.Threading;
using RSG.Model.Common.Util;
using RSG.Base.Tasks;
using RSG.Base.Windows.Dialogs;
using Workbench.AddIn.Services.Model;

namespace LevelBrowser
{
    /// <summary>
    /// 
    /// </summary>
    [Workbench.AddIn.ExportExtension(ContentBrowser.AddIn.ExtensionPoints.Browser, typeof(ContentBrowser.AddIn.BrowserBase))]
    class LevelBrowserProxy : ContentBrowser.AddIn.BrowserBase, IPartImportsSatisfiedNotification
    {
        #region MEF Imports
        /// <summary>
        /// The main content browser export object; will also be used as the view model
        /// for the content browsers main window
        /// </summary>
        [Workbench.AddIn.ImportExtension(ContentBrowser.AddIn.CompositionPoints.ContentBrowser, typeof(ContentBrowser.AddIn.IContentBrowser))]
        private Lazy<ContentBrowser.AddIn.IContentBrowser> ContentBrowserImport { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Workbench.AddIn.ImportManyExtension(LevelBrowser.AddIn.ExtensionPoints.LevelBrowserComponent, typeof(LevelBrowser.AddIn.ILevelBrowserComponent))]
        private IEnumerable<LevelBrowser.AddIn.ILevelBrowserComponent> BrowserComponentImports { get; set; }

        /// <summary>
        /// The main content browser export object; will also be used as the view model
        /// for the content browsers main window
        /// </summary>
        [Workbench.AddIn.ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(Workbench.AddIn.Services.IConfigurationService))]
        private Lazy<Workbench.AddIn.Services.IConfigurationService> ConfigurationService { get; set; }

        /// <summary>
        /// The perforce sync service
        /// </summary>
        [Workbench.AddIn.ImportExtension(Workbench.AddIn.CompositionPoints.PerforceSyncService, typeof(Workbench.AddIn.Services.IPerforceSyncService))]
        private Lazy<Workbench.AddIn.Services.IPerforceSyncService> PerforceSyncService { get; set; }

        /// <summary>
        /// The data source browser
        /// </summary>
        [Workbench.AddIn.ImportExtension(Workbench.AddIn.CompositionPoints.DataSourceBrowser, typeof(IDataSourceBrowser))]
        private Lazy<IDataSourceBrowser> DataSourceBrowser { get; set; }

        /// <summary>
        /// The build browser
        /// </summary>
        [Workbench.AddIn.ImportExtension(Workbench.AddIn.CompositionPoints.BuildBrowser, typeof(IBuildBrowser))]
        private Lazy<IBuildBrowser> BuildBrowser { get; set; }

        /// <summary>
        /// The level browser
        /// </summary>
        [Workbench.AddIn.ImportExtension(Workbench.AddIn.CompositionPoints.LevelBrowser, typeof(ILevelBrowser))]
        private Lazy<ILevelBrowser> LevelBrowserImport { get; set; }
        #endregion // MEF Imports

        #region Properties and Associated Member Data
        /// <summary>
        /// Represents whether the level browser is currently refreshing
        /// </summary>
        public Boolean CurrentlyRefreshing
        {
            get { return m_currentlyRefreshing; }
            set
            {
                if (m_currentlyRefreshing == value)
                    return;

                SetPropertyValue(value, () => this.CurrentlyRefreshing,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_currentlyRefreshing = (Boolean)newValue;
                        }
                ));
            }
        }
        private Boolean m_currentlyRefreshing;

        /// <summary>
        /// Determines if this is the default browser for the
        /// content browser.
        /// </summary>
        public override Boolean DefaultBrowser
        {
            get { return true; }
        }

        /// <summary>
        /// The icon that will be displayed to represent this browser
        /// </summary>
        public override BitmapImage BrowserIcon
        {
            get
            {
                Uri uriSource = new Uri("pack://application:,,,/LevelBrowser;component/Resources/Images/BrowserIcon.png");
                return new BitmapImage(uriSource);
            }
        }

        /// <summary>
        /// Returns the control to use for the
        /// browser header in the view or null to
        /// use the default one.
        /// </summary>
        public override Control HeaderControl
        {
            get { return new LevelBrowserHeader(); }
        }
        #endregion // Properties

        #region Constructor

        /// <summary>
        /// Default Constructor
        /// </summary>
        public LevelBrowserProxy()
            : base("Level Browser")
        {
            this.ID = ContentBrowser.AddIn.CompositionPoints.LevelBrowserGuid;
        }

        #endregion // Constructor

        #region Override Methods
        /// <summary>
        /// Override to create the commands that will be attached to
        /// this browser
        /// </summary>
        protected override ObservableCollection<ContentBrowser.AddIn.ContentBrowserCommand> CreateCommands()
        {
            ObservableCollection<ContentBrowser.AddIn.ContentBrowserCommand> commands = base.CreateCommands();
            commands.Add(this.CreateRefreshCommand());

            return commands;
        }

        /// <summary>
        /// Gets called once the browser collection system has fully loaded
        /// </summary>
        public override void Initialise()
        {
            this.ExecuteRefreshCommand();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public override List<ISearchResult> FindResults(ISearchQuery query, IAsset asset)
        {
            if (LevelBrowserImport.Value.SelectedLevel == null)
                return base.FindResults(query, asset);

            bool dataMissing = false;

            IMapHierarchy hierarchy = LevelBrowserImport.Value.SelectedLevel.GetMapHierarchy();
            foreach (IMapSection section in hierarchy.AllSections)
            {
                if (!section.IsStatLoaded(StreamableMapSectionStat.Entities) || !section.IsStatLoaded(StreamableMapSectionStat.Archetypes))
                {
                    dataMissing = true;
                    break;
                }
            }

            if (dataMissing)
            {
                string message = "Would you like to load the map container data so you can search for map object, texture dictionaries etc?";
                MessageBoxResult result = MessageBox.Show(message, "Loading", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    CancellationTokenSource cts = new CancellationTokenSource();
                    LevelTaskContext context = new LevelTaskContext(cts.Token, LevelBrowserImport.Value.SelectedLevel);
                    ITask loadDataTask = new ActionTask("Load Data", (ctx, progress) => LoadMapData((LevelTaskContext)ctx, progress));

                    TaskExecutionDialog dialog = new TaskExecutionDialog();
                    TaskExecutionViewModel vm = new TaskExecutionViewModel("Loading Map Data", loadDataTask, cts, context);
                    vm.AutoCloseOnCompletion = true;
                    dialog.DataContext = vm;
                    Nullable<bool> selectionResult = dialog.ShowDialog();
                    if (false == selectionResult)
                    {
                        return base.FindResults(query, asset);
                    }
                }
            }

            List<ISearchResult> results = new List<ISearchResult>();
            if (query.Expression == null)
                return results;

            List<IAsset> searchResults = new List<IAsset>();
            if (asset != null)
                searchResults = asset.Find(query.Expression);
            else
                searchResults = LevelBrowserImport.Value.SelectedLevel.Find(query.Expression);

            ValidateSearchResults(ref searchResults);
            foreach (IAsset searchResult in searchResults)
            {
                if (query.UseTag)
                {
                    switch (query.Tag)
                    {
                        case "Texture Dictionaries":
                            if (!(searchResult is TextureDictionary))
                                continue;
                            break;
                        case "Drawables":
                            if (!(searchResult is IArchetype))
                                continue;
                            break;
                        case "Drawable Entities":
                            if (!(searchResult is IEntity))
                                continue;
                            break;
                        case "Textures":
                            if (!(searchResult is ITexture))
                                continue;
                            break;
                        default:
                            continue;
                    }
                }

                var type = searchResult.GetType();
                var typeName = type.Name;
                string searchString = string.Format("{0} - {1}", typeName, searchResult.Name);
                if (searchResult is ITexture)
                {
                    string textureChain = string.Empty;
                    ISimpleMapArchetype archetype = ((searchResult as ITexture).Parent as ISimpleMapArchetype);
                    IMapNode mapNode = archetype.ParentSection;

                    while (mapNode != null)
                    {
                        if (String.IsNullOrWhiteSpace(textureChain))
                            textureChain = mapNode.Name;
                        else
                            textureChain = mapNode.Name + "/" + textureChain;
                        
                        mapNode = mapNode.ParentArea;
                    }
                    textureChain = textureChain + "/" + archetype.Name + searchResult.Name + ".dds";
                }
                else if (searchResult is TextureDictionary)
                {
                    searchString = string.Format("{0} - {1}", (searchResult as TextureDictionary).SourceSectionName, searchResult.Name);
                }
                else if (searchResult is IMapArchetype)
                {
                    searchString = string.Format("{0} - {1}", (searchResult as IMapArchetype).ParentSection.Name, searchResult.Name);
                }
                else if (searchResult is IEntity)
                {
                    searchString = string.Format("{0} - {1}", (searchResult as IEntity).Parent.Name, searchResult.Name);
                }
                results.Add(new SearchResult(searchString, this, searchResult));
            }
            return results;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="progress"></param>
        private void LoadMapData(LevelTaskContext context, IProgress<TaskProgress> progress)
        {
            float increment = 1.0f / context.MapHierarchy.AllSections.Count();

            // Load all the map section data
            foreach (IMapSection section in context.MapHierarchy.AllSections)
            {
                context.Token.ThrowIfCancellationRequested();

                // Update the progress information
                string message = String.Format("Loading {0}", section.Name);
                progress.Report(new TaskProgress(increment, true, message));

                section.RequestAllStatistics(false);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="root"></param>
        /// <param name="recursive"></param>
        /// <returns></returns>
        private IEnumerable<IAsset> GetAllAssets(IAsset root, bool returnSelf, bool recursive)
        {
            if (returnSelf)
                yield return root;

            if (root is IHasAssetChildren && recursive == true)
            {
                foreach (IAsset child in (root as IHasAssetChildren).AssetChildren)
                {
                    foreach (IAsset asset in GetAllAssets(child, true, true))
                        yield return asset;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="results"></param>
        private void ValidateSearchResults(ref List<IAsset> results)
        {
            var invalid = new List<IAsset>();
            foreach (var asset in results)
            {
                if (asset is IMapArchetype)
                {
                    if ((asset as IMapArchetype).ParentSection.ExportEntities == true)
                    {
                        invalid.Add(asset);
                    }
                }
                else if (asset is IEntity)
                {
                    if ((asset as IEntity).Parent is IMapSection)
                    {
                        if (!((asset as IEntity).Parent as IMapSection).ExportEntities == true)
                        {
                            invalid.Add(asset);
                        }
                    }
                }
            }
            foreach (var asset in invalid)
            {
                results.Remove(asset);
            }
        }
        #endregion // Override Methods

        #region Command Creation
        /// <summary>
        /// Creates the refresh command, used to refresh the level browser data
        /// </summary>
        private ContentBrowser.AddIn.ContentBrowserCommand CreateRefreshCommand()
        {
            Action<Object> execute = param => ExecuteRefreshCommand();
            String toolTip = "Refresh Map Data";
            BitmapImage image = null;
            try
            {
                Uri uriSource = new Uri("pack://application:,,,/LevelBrowser;component/Resources/Images/refresh.png");
                image = new BitmapImage(uriSource);
            }
            catch
            {
            }

            return new ContentBrowser.AddIn.ContentBrowserCommand(execute, toolTip, image);
        }
        #endregion // Command Creation

        #region Command Callbacks
        /// <summary>
        /// 
        /// </summary>
        private void ExecuteRefreshCommand()
        {
            this.CurrentlyRefreshing = true;
            this.ClearAssetHierarchy();

            // Only attempt to sync data if we are showing non-database data
            if (DataSourceBrowser.Value.SelectedSource == DataSource.SceneXml)
            {
                // Ask the user if they want to grab any files that are currently out of data from any of the level browser components
                List<String> perforceFiles = new List<String>();
                foreach (LevelBrowser.AddIn.ILevelBrowserComponent component in this.BrowserComponentImports)
                {
                    perforceFiles.AddRange(component.GetFilesForSync(this.ConfigurationService.Value.GameConfig));
                }
                if (perforceFiles.Count > 0)
                {
                    int syncedFiles = 0;
                    this.PerforceSyncService.Value.Show("You current have a number of level data files that are out of date that the level browser uses to display data.\n" +
                                                        "Would you like to grab latest now?\n\n" +
                                                        "You can select which files to grab using the table below.\n"
                                                        , perforceFiles.ToArray(), ref syncedFiles, false, true);
                }
            }

            // Clear out the data in all levels
            foreach (ILevel level in LevelBrowserImport.Value.LevelCollection)
            {
                level.Initialised = false;
                level.MapHierarchy = null;
                level.Characters = null;
                level.Vehicles = null;
                level.Weapons = null;
            }

            // Refresh each sub component of the level browser
            foreach (LevelBrowser.AddIn.ILevelBrowserComponent component in this.BrowserComponentImports)
            {
                component.OnRefresh(LevelBrowserImport.Value.LevelCollection);
            }

            this.UpdateAssetHierarchyForLevel(LevelBrowserImport.Value.SelectedLevel);

            this.CurrentlyRefreshing = false;
        }
        #endregion // Command Callbacks

        #region Private Functions
        /// <summary>
        /// Creates the asset hierarchy for this browser using the given level
        /// </summary>
        /// <param name="level"></param>
        private void UpdateAssetHierarchyForLevel(ILevel level)
        {
            this.ClearAssetHierarchy();

            if (level != null)
            {
                this.AssetHierarchy.Add(level);
            }
        }

        /// <summary>
        /// Need to make sure the asset hierarchy is complete cleaned up before
        /// pointing it to a new location. We also need to call the Garbage Collector
        /// so that the previous objects are destroyed straight away to make sure no events
        /// are fired.
        /// </summary>
        private void ClearAssetHierarchy()
        {
            this.AssetHierarchy.Clear();
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
        }

        /// <summary>
        /// Callback for when the datasource has changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnDataSourceChanged(Object sender, DataSourceChangedEventArgs e)
        {
            ExecuteRefreshCommand();
        }

        /// <summary>
        /// Callback for when the build has changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnBuildChanged(Object sender, BuildChangedEventArgs e)
        {
            ExecuteRefreshCommand();
        }

        /// <summary>
        /// Callback for when the selected level has changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnLevelChanged(Object sender, LevelChangedEventArgs e)
        {
            UpdateAssetHierarchyForLevel(e.NewItem);
        }
        #endregion // Private Functions

        #region IPartImportsSatisfiedNotification Implementation
        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {
            // Set up the callbacks that monitor the datasource/build changing
            DataSourceBrowser.Value.DataSourceChanged += OnDataSourceChanged;
            BuildBrowser.Value.BuildChanged += OnBuildChanged;
            LevelBrowserImport.Value.LevelChanged += OnLevelChanged;
        }
        #endregion // IPartImportsSatisfiedNotification Implementation
    } // LevelBrowserProxy


    [Workbench.AddIn.ExportExtension(Workbench.AddIn.ExtensionPoints.SearchableTags, typeof(Workbench.AddIn.UI.Layout.ISearchableTags))]
    public class SearchableTags : Workbench.AddIn.UI.Layout.ISearchableTags
    {
        #region ISearchableTags Members

        public List<string> Tags
        {
            get
            {
                return new List<string>()
                {
                    "Texture Dictionaries",
                    "Drawables",
                    "Drawable Entities",
                    "Textures"
                };
            }
        }

        #endregion
    }
} // ContentBrowser.AddIn.LevelBrowser
