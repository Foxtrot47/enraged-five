﻿using System;
using System.ComponentModel.Composition;
using System.Collections.Generic;
using RSG.Base.Windows.Controls.WpfViewport2D;

namespace Workbench.AddIn.MapDebugging.Overlays
{

    /// <summary>
    /// Map Debugging Overlay Group
    /// </summary>
    [ExportExtension(Viewport.AddIn.ExtensionPoints.OverlayGroup, 
        typeof(Viewport.AddIn.IViewportOverlayGroup))]
    public class MapDebugGroup : Viewport.AddIn.ViewportOverlayGroup, IPartImportsSatisfiedNotification
    {
        #region MEF Imports
        /// <summary>
        /// Overlays that form this Group.
        /// </summary>
        [ImportManyExtension("Workbench.AddIn.MapDebugging.OverlayGroups.MapDebug",
            typeof(Viewport.AddIn.ViewportOverlay))]
        IEnumerable<Viewport.AddIn.ViewportOverlay> ImportedOverlays
        {
            get;
            set;
        }
        #endregion // MEF Imports

        #region Constructor
        /// <summary>
        /// Default constructor.
        /// </summary>
        public MapDebugGroup()
            : base("Map Debug")
        {
        }
        #endregion // Constructor

        #region IPartImportsSatisfiedNotification Interface
        /// <summary>
        /// MEF callback on imports.
        /// </summary>
        public void OnImportsSatisfied()
        {
            foreach (Viewport.AddIn.ViewportOverlay overlay in this.ImportedOverlays)
            {
                this.Overlays.Add(overlay);
            }
        }
        #endregion // IPartImportsSatisfiedNotification Interface
    }

}
