﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MetadataEditor.AddIn.KinoEditor
{
    /// <summary>
    /// Interaction logic for DragCanvasViewer.xaml
    /// </summary>
    public partial class DragCanvasViewer : UserControl
    {
        #region Data

        NodeFactory factory;

        #endregion

        #region Constructor

        public DragCanvasViewer()
        {
            InitializeComponent();

            factory = new NodeFactory(dragCanvas);
            //factory.CreateChild(new UserControl1(), new Point(10, 20));
            factory.CreateChild(new UserControl2(), new Point(2400, 2400));

            scrollViewer.ScrollChanged += new ScrollChangedEventHandler(ScrollViewer_ScrollChanged);

            scrollViewer.ScrollToVerticalOffset(4096/2);
            scrollViewer.ScrollToHorizontalOffset(4096 / 2);
            
        }

        #endregion

        #region Overrides

        protected override void OnPreviewMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            if (!this.dragCanvas.IsMouseOverChildren())
            {
                // Mouse is not over any children, which means the click came from an empty piece of canvas
                Cursor = Cursors.ScrollAll;

                this.dragCanvas.InternalPreviewMouseLeftButtonDown(e.GetPosition(this));
            }

            base.OnPreviewMouseLeftButtonDown(e);
        }

        protected override void OnPreviewMouseWheel(MouseWheelEventArgs e)
        {
            base.OnPreviewMouseWheel(e);

            Console.WriteLine(this.dragCanvas.Scale.ToString());

            this.dragCanvas.InternalOnPreviewMouseWheel(e.Delta);
 
            Size diagramSize = new Size(this.dragCanvas.ActualWidth * this.dragCanvas.Scale, this.dragCanvas.ActualHeight * this.dragCanvas.Scale);

            this.grid.Width = diagramSize.Width;
            this.grid.Height = diagramSize.Height;

            e.Handled = true;
        }

        void ScrollViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            double horizontalOffset = 0;
            double verticalOffset = 0;

            if (e.ExtentWidthChange != 0 && e.ExtentWidthChange != e.ExtentWidth)
            {
                // Keep centered horizontally.
                double percent = e.ExtentWidthChange / (e.ExtentWidth - e.ExtentWidthChange);
                double middle = e.HorizontalOffset + (e.ViewportWidth / 2);
                horizontalOffset = middle * percent;
                //Console.WriteLine("Horizontal Offset (Zoom Change): {0}", horizontalOffset);
                scrollViewer.ScrollToHorizontalOffset(e.HorizontalOffset + horizontalOffset);
            }

            if (e.ExtentHeightChange != 0 && e.ExtentHeightChange != e.ExtentHeight)
            {
                // Keep centered vertically.
                double percent = e.ExtentHeightChange / (e.ExtentHeight - e.ExtentHeightChange);
                double middle = e.VerticalOffset + (e.ViewportHeight / 2);
                verticalOffset = middle * percent;
                //Console.WriteLine("Vertical Offset (Zoom Change): {0}", verticalOffset);
                scrollViewer.ScrollToVerticalOffset(e.VerticalOffset + verticalOffset);
            }
        }

        protected override void OnPreviewMouseMove(MouseEventArgs e)
        {
            if (!this.dragCanvas.IsMouseOverChildren())
            {
                for (int i = 0; i < this.dragCanvas.Children.Count; ++i)
                {
                    this.dragCanvas.InternalPreviewMouseMove(this.dragCanvas.Children[i], e.GetPosition(this));
                }
            }

            base.OnPreviewMouseMove(e);
        }

        protected override void OnPreviewMouseUp(MouseButtonEventArgs e)
        {
            this.dragCanvas.InternalOnPreviewMouseUp();
            Cursor = Cursors.Arrow;
            base.OnPreviewMouseUp(e);
        }

        #endregion
    }
}
