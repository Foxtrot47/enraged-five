﻿using System;
using System.Collections.Generic;
using RSG.Base.Logging;
using Workbench.AddIn;
using Workbench.AddIn.Services;

namespace Workbench.Services
{
    
    /// <summary>
    /// Generic host extensions service.
    /// </summary>
    [ExportExtension(Workbench.AddIn.CompositionPoints.ExtensionService,
        typeof(IExtensionService))]
    class ExtensionService : IExtensionService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="extensionsCollection"></param>
        /// <returns></returns>
        public IList<T> Sort<T>(IEnumerable<T> extensionsCollection)
            where T : IExtension
        {
            List<T> extensions = new List<T>(extensionsCollection);
            List<T> sortedExtensions = new List<T>();
            List<T> unsortedExtensions = new List<T>();
            foreach (T newExtension in extensions)
            {
                if ((null == newExtension.RelativeID) || 
                    (Guid.Empty == newExtension.RelativeID))
                {
                    sortedExtensions.Add(newExtension);
                }
                else if (FindByGuid(newExtension.RelativeID, extensions) == -1)
                {
                    // found a configuration error
                    Log.Log__Warning("Configuration error with extension ID {0}, RelativeID of {1} doesn't exist.",
                        newExtension.ID, newExtension.RelativeID);
                    sortedExtensions.Add(newExtension);
                }
                else
                {
                    unsortedExtensions.Add(newExtension);
                }
            }
            while (unsortedExtensions.Count > 0)
            {
                List<T> stillUnsortedExtensions = new List<T>();
                int startingCount = unsortedExtensions.Count;
                foreach (T newExtension in unsortedExtensions)
                {
                    int index = FindByGuid(newExtension.RelativeID, sortedExtensions);
                    if (index > -1)
                    {
                        if (Direction.Before == newExtension.Direction)
                        {
                            sortedExtensions.Insert(index, newExtension);
                        }
                        else
                        {
                            if (index == sortedExtensions.Count - 1)
                            {
                                //it's to be inserted after the last item in the list
                                sortedExtensions.Add(newExtension);
                            }
                            else
                            {
                                sortedExtensions.Insert(index + 1, newExtension);
                            }
                        }
                    }
                    else
                    {
                        stillUnsortedExtensions.Add(newExtension);
                    }
                }
                if (startingCount == stillUnsortedExtensions.Count)
                {
                    // We didn't make any progress
                    Log.Log__Error("Configuration error with one of these extensions:");
                    foreach (IExtension ext in stillUnsortedExtensions)
                    {
                        Log.Log__Error("\tID = {0}, RelativeID = {1}", ext.ID, ext.RelativeID);
                    }
                    // Pick one and add it at the end.
                    sortedExtensions.Add(stillUnsortedExtensions[0]);
                    stillUnsortedExtensions.RemoveAt(0);
                }
                unsortedExtensions = stillUnsortedExtensions;
            }
            return sortedExtensions;
        }

        /// <summary>
        /// Joins two extension collections and puts another item between them.
        /// Very handy for joining two imported collections of IMenuItems with a separator
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="e1"></param>
        /// <param name="joinItem"></param>
        /// <param name="e2"></param>
        /// <returns></returns>
        public IEnumerable<T> SortAndJoin<T>(IEnumerable<T> e1, T joinItem, IEnumerable<T> e2) 
            where T : IExtension
        {
            IEnumerable<T> sorted1 = Sort(e1);
            IEnumerable<T> sorted2 = Sort(e2);

            foreach (T t in sorted1)
            {
                yield return t;
            }

            yield return joinItem;

            foreach (T t in sorted2)
            {
                yield return t;
            }
        }

        /// <summary>
        /// Find the index of a particular extension by Guid, -1 returned if 
        /// not found.
        /// </summary>
        /// <typeparam name="T">IExtension subclass</typeparam>
        /// <param name="id">Guid of extension to find.</param>
        /// <param name="extensions">Collection of IExtension objects.</param>
        /// <returns>Index of extension, -1 returned if not found.</returns>
        private int FindByGuid<T>(Guid id, IList<T> extensions)
            where T : IExtension
        {
            for (int i = 0; i < extensions.Count; ++i)
            {
                if (extensions[i].ID == id)
                    return (i);
            }
            return (-1);
        }
    }

} // Workbench.Services namespace
