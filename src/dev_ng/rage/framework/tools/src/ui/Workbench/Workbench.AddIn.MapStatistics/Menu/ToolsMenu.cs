﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using RSG.Base.Logging;
using Workbench.AddIn;
using Workbench.AddIn.Services;
using Workbench.AddIn;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.UI.Menu;

namespace Workbench.AddIn.MapStatistics.Menu
{

    /// <summary>
    /// 
    /// </summary>
    //[ExportExtension(Workbench.AddIn.ExtensionPoints.MenuTools,
    //    typeof(IMenuItem))]
    //class MapStatisticsToolsMenu : MenuItemBase
    //{
    //    #region MEF Imports

    //    [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager, typeof(ILayoutManager))]
    //    private ILayoutManager LayoutManager { get; set; }

    //    [ImportExtension(Workbench.AddIn.MapStatistics.CompositionPoints.MapStatisticsDocument, typeof(MapStatisticsViewModel))]
    //    private MapStatisticsViewModel MapStatisticsViewer { get; set; }

    //    #endregion // MEF Imports

    //    #region Constructor(s)

    //    /// <summary>
    //    /// Default constructor.
    //    /// </summary>
    //    public MapStatisticsToolsMenu()
    //        : base("_Map Statistics")
    //    {
    //    }

    //    #endregion // Constructor(s)

    //    #region ICommandControl Interface

    //    /// <summary>
    //    /// This is called whenever the user selects the TXD Parentiser menu option.
    //    /// Just creates the main document and uses the layout manager to display it.
    //    /// </summary>
    //    /// <param name="parameter"></param>
    //    public override void Execute(Object parameter)
    //    {
    //        Log.Log__Debug("MapStatistics::Execute()");

    //        IDocument TXDParentiserDoc = MapStatisticsViewer.CreateDocument("Map Statistics");
    //        LayoutManager.ShowDocument(TXDParentiserDoc);
    //    }
    //    #endregion // ICommandControl Interface

    //} // MapStatisticsToolsMenu

} // Workbench.AddIn.TXDParentiser.UI.Menu
