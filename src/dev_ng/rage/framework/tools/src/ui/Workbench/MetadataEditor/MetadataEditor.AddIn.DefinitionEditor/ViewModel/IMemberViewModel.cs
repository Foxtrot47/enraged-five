﻿using System;

namespace MetadataEditor.AddIn.DefinitionEditor.ViewModel
{

    /// <summary>
    /// ViewModel for the IMember series of classes.
    /// </summary>
    public interface IMemberViewModel
    {
        #region Properties
        /// <summary>
        /// Reference to the Member model this represents.
        /// </summary>
        RSG.Metadata.Parser.IMember Model { get; }

        /// <summary>
        /// Reference to the parent Member ViewModel (for hierarchy).
        /// </summary>
        IMemberViewModel Parent { get; }
        #endregion // Properties
    }

} // RSG.Metadata.ViewModel namespace
