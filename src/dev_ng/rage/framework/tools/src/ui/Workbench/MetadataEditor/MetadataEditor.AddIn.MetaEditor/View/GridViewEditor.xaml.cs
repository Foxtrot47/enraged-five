﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MetadataEditor.AddIn.MetaEditor.GridViewModel;
using System.Collections.Specialized;
using System.Collections;
using MetadataEditor.AddIn.MetaEditor.ViewModel;
using System.Reflection;

namespace MetadataEditor.AddIn.MetaEditor.View
{
    /// <summary>
    /// Interaction logic for GridViewEditor.xaml
    /// </summary>
    public partial class GridViewEditor : UserControl
    {
        public GridViewEditor()
        {
            InitializeComponent();
        }
    }

    public static class DataGridColumns
    {
        private static Dictionary<WeakReference, List<WeakReference>> mapping = new Dictionary<WeakReference, List<WeakReference>>();

        public static readonly DependencyProperty ColumnSourceProperty = DependencyProperty.RegisterAttached(
            "ColumnSource",
            typeof(ICollection<DataGridColumn>),
            typeof(DataGridColumns),
            new FrameworkPropertyMetadata(null,
                OnColumnSourceChanged));

        public static void SetColumnSource(UIElement element, ICollection<DataGridColumn> value)
        {
            element.SetValue(ColumnSourceProperty, value);
        }
        public static ICollection<DataGridColumn> GetColumnSource(UIElement element)
        {
            return (ICollection<DataGridColumn>)element.GetValue(ColumnSourceProperty);
        }

        private static void OnColumnSourceChanged(DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            if (e.OldValue is INotifyCollectionChanged)
            {
                (e.OldValue as INotifyCollectionChanged).CollectionChanged -= SourceCollectionChanged;
                WeakReference remove = null;
                foreach (var kvp in mapping)
                {
                    if (kvp.Key.Target == e.OldValue)
                    {
                        WeakReference itemRemove = null;
                        foreach (WeakReference item in kvp.Value)
                        {
                            if (item.Target == s)
                            {
                                itemRemove = item;
                                break;
                            }
                        }

                        if (itemRemove != null)
                        {
                            kvp.Value.Remove(itemRemove);
                            if (kvp.Value.Count == 0)
                            {
                                remove = kvp.Key;
                            }
                        }

                        break;
                    }
                }

                if (remove != null)
                {
                    mapping.Remove(remove);
                }
            }

            if (e.NewValue is INotifyCollectionChanged)
            {
                (e.NewValue as INotifyCollectionChanged).CollectionChanged += SourceCollectionChanged;

                WeakReference index = null;
                foreach (var kvp in mapping)
                {
                    if (kvp.Key.Target == e.NewValue)
                    {
                        index = kvp.Key;
                        break;
                    }
                }

                if (index == null)
                {
                    mapping.Add(new WeakReference(e.NewValue), new List<WeakReference>() { new WeakReference(s) });
                }
                else
                {
                    mapping[index].Add(new WeakReference(s));
                }
            }


            ResetColumns(s as DataGrid, e.NewValue as ICollection<DataGridColumn>);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void SourceCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            List<DataGrid> dataGrids = new List<DataGrid>();
            WeakReference index = null;
            foreach (var kvp in mapping)
            {
                if (kvp.Key.Target == sender)
                {
                    index = kvp.Key;
                    break;
                }
            }

            if (index == null)
            {
                return;
            }

            foreach (WeakReference item in mapping[index])
            {
                if (item.IsAlive && item.Target is DataGrid)
                {
                    dataGrids.Add(item.Target as DataGrid);
                }
            }

            if (dataGrids.Count == 0)
            {
                return;
            }

            foreach (DataGrid grid in dataGrids)
            {
                switch (e.Action)
                {
                    case NotifyCollectionChangedAction.Add:
                        AddColumns(grid, e.NewItems.Cast<DataGridColumn>());
                        break;
                    case NotifyCollectionChangedAction.Remove:
                        AddColumns(grid, e.OldItems.Cast<DataGridColumn>());
                        break;
                    case NotifyCollectionChangedAction.Reset:
                        ResetColumns(grid, sender as ICollection<DataGridColumn>);
                        break;
                    default:
                        break;
                }
            }
        }

        private static void ResetColumns(DataGrid grid, ICollection<DataGridColumn> columns)
        {
            if (grid == null || columns == null)
            {
                return;
            }

            try
            {
                Dictionary<string, DataGridColumn> headers = new Dictionary<string, DataGridColumn>();
                foreach (DataGridColumn column in grid.Columns)
                {
                    string header = null;
                    if (column.Header is TextBlock)
                    {
                        header = (column.Header as TextBlock).Text;
                    }
                    else
                    {
                        header = column.Header.ToString();
                    }

                    headers.Add(header, column);
                }

                foreach (DataGridColumn column in columns)
                {
                    string header = null;
                    if (column.Header is TextBlock)
                    {
                        header = (column.Header as TextBlock).Text;
                    }
                    else
                    {
                        header = column.Header.ToString();
                    }

                    column.SetValue(FrameworkElement.TagProperty, grid);
                    if (grid.Columns.Contains(column))
                    {
                        continue;
                    }

                    if (headers.ContainsKey(header))
                    {
                        grid.Columns.Remove(headers[header]);
                    }

                    grid.Columns.Add(column);
                }
            }
            catch
            {

            }
        }

        private static void AddColumns(DataGrid grid, IEnumerable<DataGridColumn> newColumns)
        {
            if (grid == null || newColumns == null)
            {
                return;
            }
        }

        private static void RemoveColumns(DataGrid grid, IEnumerable<DataGridColumn> oldColumns)
        {
            if (grid == null || oldColumns == null)
            {
                return;
            }
        }
    }

    public class SingleEnumerableConverter : IValueConverter
    {

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return Enumerable.Repeat(value, 1);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public static class SmoothScrollingBehavior
    {
        public static bool GetIsEnabled(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsEnabledProperty);
        }

        public static void SetIsEnabled(DependencyObject obj, bool value)
        {
            obj.SetValue(IsEnabledProperty, value);
        }

        public static readonly DependencyProperty IsEnabledProperty =
            DependencyProperty.RegisterAttached(
            "IsEnabled",
            typeof(bool),
            typeof(SmoothScrollingBehavior),
            new UIPropertyMetadata(false, HandleIsEnabledChanged));

        private static void HandleIsEnabledChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var vsp = d as VirtualizingStackPanel;
            if (vsp == null)
            {
                return;
            }

            var property = typeof(VirtualizingStackPanel).GetProperty("IsPixelBased",
                                                                         BindingFlags.NonPublic | BindingFlags.Instance);

            if (property == null)
            {
                return;
            }

            if ((bool)e.NewValue == true)
            {
                property.SetValue(vsp, true, new object[0]);
            }
            else
            {
                property.SetValue(vsp, false, new object[0]);
            }
        }
    }
}
