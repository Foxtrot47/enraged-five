﻿using System;
using System.Collections.Generic;
using System.Linq;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using RSG.Interop.Bugstar.Organisation;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Triggers;

namespace Workbench.AddIn.Automation.UI
{

    /// <summary>
    /// Automation Job View Model; wrapper around IJob with some additional
    /// information for display purposes.
    /// </summary>
    internal class AutomationJobViewModel : ViewModelBase
    {
        #region Properties
        /// <summary>
        /// Automation job.
        /// </summary>
        public IJob Job
        {
            get { return m_Job; }
            private set
            {                
                SetPropertyValue(value, () => this.Job,
                    new PropertySetDelegate(delegate(Object newValue) { m_Job = (IJob)newValue; }));
            }
        }
        private IJob m_Job;

        /// <summary>
        /// Bugstar User object for job owner.
        /// </summary>
        public User User
        {
            get { return m_User; }
            private set
            {                
                SetPropertyValue(value, () => this.User,
                    new PropertySetDelegate(delegate(Object newValue) { m_User = (User)newValue; }));

                OnPropertyChanged("IsCurrentUser"); // force update on the IsCurrentUser property.
            }
        }
        private User m_User;

        /// <summary>
        /// Is current user.
        /// </summary>
        public bool IsCurrentUser
        {
            get
            {
                return m_User == null ? false : Environment.UserName.ToLower() == m_User.UserName.ToLower();
            }
            set
            {
                SetPropertyValue(value, () => this.IsCurrentUser,
                    new PropertySetDelegate(delegate(Object newValue) { /*dummy update*/ }));
            }
        }

        /// <summary>
        /// Selected state of this task view model.
        /// </summary>
        public bool IsSelected
        {
            get { return m_bIsSelected; }
            set
            {
                this.m_bIsSelected = value;
                this.NotifyPropertyChanged("IsSelected");
            }
        }

        private bool m_bIsSelected;

        /// <summary>
        /// True if the file set has been downloaded in this session.
        /// </summary>
        public bool IsDownloaded
        {
            get { return m_isDownloaded; }
            set
            {
                m_isDownloaded = value;
                this.NotifyPropertyChanged("IsDownloaded");
            }
        }

        private bool m_isDownloaded;

        /// <summary>
        /// True if the file set download is in process.
        /// </summary>
        public bool IsDownloading
        {
            get { return m_isDownloading; }
            set
            {
                m_isDownloading = value;
                this.NotifyPropertyChanged("IsDownloading");
            }
        }

        private bool m_isDownloading;

        /// <summary>
        /// Selected state of this task view model.
        /// </summary>
        public bool IsVisible
        {
            get { return this.isVisible; }
            set
            {
                SetPropertyValue(value, () => this.IsVisible,
                    new PropertySetDelegate(delegate(Object newValue)
                        {
                            this.isVisible = (bool)newValue;
                        }));
            }
        }
        private bool isVisible;

        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor; from automation IJob object.
        /// </summary>
        /// <param name="project">Bugstar Project</param>
        /// <param name="job">Automation Job</param>
        public AutomationJobViewModel(User[] users, IJob job)
        {
            this.Job = job;
            this.isVisible = true;
            String username = String.Empty;
            if (job.Trigger is ChangelistTrigger)
                username = ((ChangelistTrigger)job.Trigger).Username;
            else if (job.Trigger is UserRequestTrigger)
                username = ((UserRequestTrigger)job.Trigger).Username;

            if (!String.IsNullOrEmpty(username))
                this.User = users.Where(user => 0 == String.Compare(username, user.UserName)).FirstOrDefault();            
        }
        #endregion // Constructor(s)
    }

} // Workbench.AddIn.Automation.UI namespace
