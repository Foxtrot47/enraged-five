﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.ReportBrowser;
using RSG.Base.Logging;
using System.Web;

namespace Workbench.AddIn.ReportBrowser
{
    /// <summary>
    /// Interaction logic for ReportView.xaml
    /// </summary>
    public partial class ReportUriView : DocumentBase<ReportUriViewModel>
    {
        #region Constants

        public static readonly Guid GUID = new Guid("97EA0D12-51E6-45EB-88D2-E2C4340EF8A0");

        #endregion // Constants

        #region Properties
        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Default constructor.
        /// </summary>
        public ReportUriView(ReportUriViewModel vm)
            : base("ReportUriViewModel", vm)
        {
            InitializeComponent();
            this.SaveModel = null; // Prevent undo redo happening.
            this.ID = GUID;
        }

        #endregion // Constructor(s) 

        #region webbrowser code behind - lazy way of getting this done - could be written in the viewmodel though as would be much better - just time constrained.
        /// <summary>
        /// 
        /// </summary>
        private void webBrowser_Navigating(Object sender, NavigatingCancelEventArgs e)
        {
            WebBrowser webBrowser = (sender as WebBrowser);
            if (null == webBrowser)
                return;

            if (e.Uri != null)
            {
                if (e.Uri.AbsolutePath != "blank")
                {
                    this.ViewModel.Address = e.Uri.OriginalString;
                    this.addressTextBlock1.Text = this.ViewModel.Address;
                }
                else
                {
                    // Anchor clicked on when the webbrowser is using a document stream
                    e.Cancel = true;
                }
            }

            if (!e.Cancel)
            {
                this.ViewModel.IsNavigating = true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void webBrowser_LoadCompleted(object sender, NavigationEventArgs e)
        {
            WebBrowser webBrowser = (sender as WebBrowser);

            if (null == webBrowser)
                return;

            // Check if the web browser is using a stream
            if (e.Uri == null)
            {
                mshtml.HTMLDocument doc = (mshtml.HTMLDocument)webBrowser.Document;

                // Add event handlers to each of the links that scrolls what they are linking to into view
                foreach (mshtml.IHTMLElement link in doc.links)
                {
                    mshtml.HTMLAnchorElement anchor = link as mshtml.HTMLAnchorElement;
                    if (anchor != null)
                    {
                        mshtml.HTMLAnchorEvents_Event handler = anchor as mshtml.HTMLAnchorEvents_Event;
                        if (handler != null)
                        {
                            handler.onclick += new mshtml.HTMLAnchorEvents_onclickEventHandler(delegate()
                            {
                                string anchorName = anchor.hash.TrimStart(new char[] { '#' }).Replace("%20", " ");
                                mshtml.IHTMLElement element = doc.anchors.item(anchorName);

                                if (element != null)
                                {
                                    element.scrollIntoView();
                                }
                                return true;
                            });
                        }
                    }
                }
            }

            this.ViewModel.IsNavigating = false;
        }

        private void goNavigateButton_Click(object sender, RoutedEventArgs e)
        {
            // Get URI to navigate to
            Uri uri = new Uri(this.addressTextBlock1.Text, UriKind.RelativeOrAbsolute);

            // Only absolute URIs can be navigated to
            if (!uri.IsAbsoluteUri)
            {
                MessageBox.Show("The Address URI must be absolute eg 'http://www.microsoft.com'");
                return;
            }

            // Navigate to the desired URL by calling the .Navigate method
            this.ViewModel.Address = uri.OriginalString;
            try
            {
                this.webBrowser1.Navigate(uri);
            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex,"WebBrowser navigation failed");
            }            
        }

        private void backNavigateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.webBrowser1.CanGoBack)
                {
                    this.webBrowser1.GoBack();
                }
            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex, "WebBrowser navigation failed going back");
            }    
        }

        private void forwardNavigateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.webBrowser1.CanGoForward)
                {
                    this.webBrowser1.GoForward();
                }
            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex,"WebBrowser navigation failed going forward");
            }    
        }

        private void homeNavigateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // DW - probably not the best way!!!
                while (this.webBrowser1.CanGoBack)
                {
                    this.webBrowser1.GoBack();
                }
            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex,"WebBrowser navigation failed going home");
            }    
        }
        #endregion // webbrowser code behind
    }
}
