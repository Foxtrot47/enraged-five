﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows;
using RSG.Base.Logging;
using Workbench.AddIn;
using Workbench.AddIn.Services;
using Workbench.AddIn;
using Workbench.AddIn.UI.Layout;
using MetadataEditor.AddIn;

namespace MetadataEditor
{

    ///// <summary>
    ///// Metadata Editor Startup Command.
    ///// </summary>
    //[ExportExtension(Workbench.AddIn.ExtensionPoints.StartupCommand,
    //    typeof(IApplicationCommand))]
    //class Startup : ExtensionBase, IApplicationCommand
    //{
    //    #region MEF Imports
    //    [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager,
    //        typeof(ILayoutManager))]
    //    private Lazy<ILayoutManager> LayoutManager { get; set; }
    
    //    [ImportManyExtension(Workbench.AddIn.ExtensionPoints.ToolWindow,
    //        typeof(IToolWindow))]
    //    private IEnumerable<IToolWindow> ToolWindows { get; set; }
    //    #endregion // MEF Imports

    //    #region IApplicationCommand Interface
    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    /// <param name="args"></param>
    //    public void Execute(params Object[] args)
    //    {
    //        Log.Log__Debug("MetadataEditor Startup Command.");
    //        // Show all tool windows
    //        foreach (IToolWindow window in ToolWindows)
    //            LayoutManager.Value.ShowToolWindow(window);
    //    }
    //    #endregion // IApplicationCommand Interface
    //}

} // MetadataEditor namespace
