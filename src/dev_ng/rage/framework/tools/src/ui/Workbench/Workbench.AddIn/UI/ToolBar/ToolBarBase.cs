﻿using System;
using System.Collections.Generic;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using Workbench.AddIn.UI.Controls;

namespace Workbench.AddIn.UI.ToolBar
{

    /// <summary>
    /// 
    /// </summary>
    public class ToolBarBase : ControlBase, IToolBar
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public String Header
        {
            get { return m_sHeader; }
            set
            {
                SetPropertyValue(value, () => this.Header,
                    new PropertySetDelegate(delegate(Object newValue) { m_sHeader = (String)newValue; }));
            }
        }
        private String m_sHeader = String.Empty;

        /// <summary>
        /// 
        /// </summary>
        public String Name
        {
            get { return m_sName; }
            set
            {
                SetPropertyValue(value, () => this.Name,
                    new PropertySetDelegate(delegate(Object newValue) { m_sName = (String)newValue; }));
            }
        }
        private String m_sName = String.Empty;

        /// <summary>
        /// Collection of IToolBarItems in the toolbar.
        /// </summary>
        public IEnumerable<IToolBarItem> Items
        {
            get { return m_Items; }
            set
            {
                SetPropertyValue(value, () => this.Items,
                    new PropertySetDelegate(delegate(Object newValue) { m_Items = (IEnumerable<IToolBarItem>)newValue; }));
            }
        }
        private IEnumerable<IToolBarItem> m_Items = null;
        #endregion // Properties
    }

} // Workbench.AddIn.UI.ToolBar namespace
