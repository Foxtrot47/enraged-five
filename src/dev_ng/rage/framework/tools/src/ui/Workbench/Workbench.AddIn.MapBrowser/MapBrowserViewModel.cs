﻿using System;
using System.Collections.Specialized;
using System.ComponentModel.Composition;
using System.Threading;
using System.Windows.Threading;
using System.Windows;
using System.Windows.Data;
using System.Windows.Controls;
using System.Xml;
using System.Xml.XPath;
using System.ComponentModel;
using RSG.Base.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn;
using Workbench.AddIn.Services;
using Workbench.AddIn.UI.Layout;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using RSG.Model.Map;
using RSG.Model.Map.Statistics;
using RSG.Model.Map.Filters;
using RSG.Model.Map.ViewModel;
using RSG.Base.Windows.Controls.WpfViewport2D;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using Map.AddIn.Services;
using RSG.Base.Math;

namespace Workbench.AddIn.MapBrowser
{
    [ExportExtension(Map.AddIn.Services.CompositionPoints.MapBrowser, typeof(IMapBrowser))]
    internal class MapBrowserViewModel : ViewModelBase, IMapBrowser
    {
        #region Events

        /// <summary>
        /// Gets fired whenever the selected level changes to something different
        /// </summary>
        public event EventHandler SelectedLevelChanged;

        /// <summary>
        /// Gets fired whenever a item is double clicked in the map browser
        /// tree view
        /// </summary>
        public event RoutedEventHandler ComponentDoubleClicked;

        /// <summary>
        /// Fires the ComponentDoubleClicked event
        /// </summary>
        internal void FireComponentDoubleClickedEvent(Object item, RoutedEventArgs e)
        {
            if (this.ComponentDoubleClicked != null)
                this.ComponentDoubleClicked(item, e);
        }

        /// <summary>
        /// Gets fired whenever a item gets expanded
        /// </summary>
        public event RoutedEventHandler ComponentExpanded;

        /// <summary>
        /// Fires the ComponentExapnded event
        /// </summary>
        internal void FireComponentExpandedEvent(Object item, RoutedEventArgs e)
        {
            if (this.ComponentExpanded != null)
                this.ComponentExpanded(item, e);
        }

        #endregion // Events

        #region Properties

        /// <summary>
        /// The configuration service that contains
        /// the game config
        /// </summary>
        public IConfigurationService ConfigurationService
        {
            get;
            set;
        }

        /// <summary>
        /// The main layout manager for the application, used to show/hide
        /// the tool windows.
        /// </summary>
        public ILayoutManager LayoutManager
        {
            get { return m_layoutManager; }
            set
            {
                if (value != null)
                {
                    value.ContentOpened += this.OnContentOpened;
                    value.ContentClosed += this.OnContentClosed;
                }
                else if (this.LayoutManager != null)
                {
                    value.ContentOpened -= this.OnContentOpened;
                    value.ContentClosed -= this.OnContentClosed;
                }
                m_layoutManager = value;
            }
        }
        private ILayoutManager m_layoutManager;

        /// <summary>
        /// The list of all opened map user proxies that need to be operated
        /// with when the map data gets reloaded etc.
        /// </summary>
        public Dictionary<IMapBrowserUserProxy, int> OpenedMapUsers
        {
            get;
            set;
        }

        /// <summary>
        /// A list of all the proxies that represent plugins that
        /// use the map browser for user data.
        /// </summary>
        public List<IMapBrowserUserProxy> MapUserProxies
        {
            get;
            set;
        }

        /// <summary>
        /// If true it represents that the map data is currently
        /// in the refreshing state and that it's contents 
        /// aren't reliable
        /// </summary>
        public Boolean RefreshingMapData
        {
            get { return m_refreshingMapData; }
            set 
            { 
                SetPropertyValue(value, () => this.RefreshingMapData,
                    new PropertySetDelegate(delegate(Object newValue) { m_refreshingMapData = (Boolean)newValue; }));
            }
        }
        private Boolean m_refreshingMapData;

        /// <summary>
        /// If true it represents that the map data has been valid and is currently in
        /// a stable, reliable state.
        /// </summary>
        public Boolean ValidMapData
        {
            get { return m_validMapData; }
            set
            {
                SetPropertyValue(value, () => this.ValidMapData,
                    new PropertySetDelegate(delegate(Object newValue) { m_validMapData = (Boolean)newValue; }));
            }
        }
        private Boolean m_validMapData;

        /// <summary>
        /// A instance of the level dictionary class used in the creation of the map data.
        /// This is set back to null after it is used.
        /// </summary>
        public LevelDictionary LevelDictionary
        {
            get { return m_levelDictionary; }
            set
            {
                SetPropertyValue(value, () => this.LevelDictionary,
                    new PropertySetDelegate(delegate(Object newValue) { m_levelDictionary = (LevelDictionary)newValue; }));
            }
        }
        private LevelDictionary m_levelDictionary = null;

        /// <summary>
        /// The map data got from loading the content tree
        /// </summary>
        public LevelDictionaryViewModel ContentLevel
        {
            get { return m_contentLevel; }
            set
            {
                SetPropertyValue(value, () => this.ContentLevel,
                    new PropertySetDelegate(delegate(Object newValue) { m_contentLevel = (LevelDictionaryViewModel)newValue; }));
            }
        }
        private LevelDictionaryViewModel m_contentLevel = null;

        /// <summary>
        /// The current level that is displayed in the map browser UI
        /// </summary>
        public LevelViewModel SelectedLevel
        {
            get { return m_selectedLevel; }
            set
            {
                String previousName = m_selectedLevel == null ? String.Empty : m_selectedLevel.Name;
                String currentName = value == null ? String.Empty : value.Name;
                LevelViewModel preivousLevel = m_selectedLevel;

                SetPropertyValue(value, () => this.SelectedLevel,
                    new PropertySetDelegate(delegate(Object newValue) { m_selectedLevel = (LevelViewModel)newValue; }));

                if (previousName != currentName)
                {
                    this.MapSections = new List<MapSectionViewModel>();
                    this.PropGroupSections = new List<PropGroupSectionViewModel>();
                    this.InteriorSections = new List<InteriorSectionViewModel>();
                    this.PropSections = new List<PropSectionViewModel>();
                    this.LevelGeometry = new Dictionary<MapSectionViewModel, List<Vector2f>>();
                    if (value != null)
                    {
                        foreach (IMapSectionViewModel section in ViewModelUtilities.GetAllMapSections(value, true))
                        {
                            if (section.Model.SectionType == SectionTypes.MAP)
                            {
                                this.MapSections.Add(section as MapSectionViewModel);
                            }
                            else if (section.Model.SectionType == SectionTypes.PROPGROUP)
                            {
                                this.PropGroupSections.Add(section as PropGroupSectionViewModel);
                            }
                            else if (section.Model.SectionType == SectionTypes.INTERIORS)
                            {
                                this.InteriorSections.Add(section as InteriorSectionViewModel);
                            }
                            else if (section.Model.SectionType == SectionTypes.PROPS)
                            {
                                this.PropSections.Add(section as PropSectionViewModel);
                            }
                        }

                        String filename = System.IO.Path.Combine(this.ConfigurationService.GameConfig.ToolsConfigDir, "content", "levels", value.Name, "vector_map.xml");
                        if (System.IO.File.Exists(filename))
                        {
                            this.ParseVectorMapFile(value, filename);
                        }
                    }

                    if (this.SelectedLevelChanged != null)
                        this.SelectedLevelChanged(value, EventArgs.Empty);
                }
                if (preivousLevel != null && value != null)
                {
                    OnLevelChanged(preivousLevel, value);
                }
            }
        }
        private LevelViewModel m_selectedLevel;

        /// <summary>
        /// A list of all the threads that are currently active and in the
        /// process of loading the user data. This is that we can cancel them
        /// if we need to.
        /// </summary>
        private Dictionary<Guid, BackgroundWorker> UserDataLoadingThreads
        {
            get;
            set;
        }

        /// <summary>
        /// A list containing all the map sections that are in the currently
        /// selected level
        /// </summary>
        public List<MapSectionViewModel> MapSections
        {
            get;
            private set;
        }

        /// <summary>
        /// A list containing all the prop group sections that are in the currently
        /// selected level
        /// </summary>
        public List<PropGroupSectionViewModel> PropGroupSections
        { 
            get;
            set;
        }

        /// <summary>
        /// A list containing all the interior sections that are in the currently
        /// selected level
        /// </summary>
        public List<InteriorSectionViewModel> InteriorSections
        {
            get;
            set;
        }

        /// <summary>
        /// A list containing all the prop sections that are in the currently
        /// selected level
        /// </summary>
        public List<PropSectionViewModel> PropSections
        {
            get;
            set;
        }

        /// <summary>
        /// The dictionary of all the references in all the currently
        /// loaded levels, these references also include the appearence count and the
        /// location lists (when the statistics have been loaded)
        /// </summary>
        public Dictionary<Level, LevelReferences> AllReferences
        {
            get;
            set;
        }

        /// <summary>
        /// A list that contains references to all the definitions
        /// in the loaded map data
        /// </summary>
        public DefinitionList GenericDefinitions
        {
            get;
            set;
        }

        /// <summary>
        /// A collection of objects currently selected in the map browser treeview
        /// </summary>
        public ObservableCollection<Object> SelectedItems
        {
            get { return m_selectedItems; }
            set
            {
                SetPropertyValue(value, () => this.SelectedItems,
                    new PropertySetDelegate(delegate(Object newValue) { m_selectedItems = (ObservableCollection<Object>)newValue; }));
            }
        }
        private ObservableCollection<Object> m_selectedItems;

        /// <summary>
        /// A collection of objects currently selected in the map browser treeview
        /// </summary>
        public ObservableCollection<IMapSectionViewModel> SelectedSections
        {
            get { return m_selectedSections; }
            set
            {
                SetPropertyValue(value, () => this.SelectedSections,
                    new PropertySetDelegate(delegate(Object newValue) { m_selectedSections = (ObservableCollection<IMapSectionViewModel>)newValue; }));
            }
        }
        private ObservableCollection<IMapSectionViewModel> m_selectedSections;

        /// <summary>
        /// Contains the points for all the selected level map section geometry
        /// </summary>
        public Dictionary<MapSectionViewModel, List<Vector2f>> LevelGeometry
        {
            get;
            private set;
        }

        /// <summary>
        /// A reference to the main tree view so that we can handle selection changes
        /// </summary>
        public RSG.Base.Windows.Controls.MultiSelect.TreeView TreeView
        {
            get;
            set;
        }

        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        public MapBrowserViewModel()
        {
            this.SelectedItems = new ObservableCollection<Object>();
            this.SelectedItems.CollectionChanged += new NotifyCollectionChangedEventHandler(OnSelectedItemsChanged);

            this.OpenedMapUsers = new Dictionary<IMapBrowserUserProxy, int>();
            this.UserDataLoadingThreads = new Dictionary<Guid, BackgroundWorker>();
        }

        void OnSelectedItemsChanged(Object sender, NotifyCollectionChangedEventArgs e)
        {
            this.SelectedSections = new ObservableCollection<IMapSectionViewModel>();
            foreach (Object section in this.SelectedItems)
            {
                if (section is IMapSectionViewModel)
                {
                    this.SelectedSections.Add(section as IMapSectionViewModel);
                }
            }
        }

        #endregion // Constructors

        #region Public Functions

        /// <summary>
        /// Loads a single user proxy up when the first instance of the user is opened
        /// </summary>
        public void LoadSingleUserProxy(IMapBrowserUserProxy proxy, BackgroundWorker thread, System.Threading.AutoResetEvent handler)
        {
            if (this.SelectedLevel != null && this.OpenedMapUsers.Count > 0)
            {
                List<IMapViewModelContainer> areas = this.SelectedLevel.NonGenericDefintionContainerChildren.ToList();
                List<IMapViewModelComponent> sections = new List<IMapViewModelComponent>();
                foreach (IMapViewModelContainer area in areas)
                {
                    foreach (IMapViewModelComponent section in area.ComponentChildren)
                    {
                        sections.Add(section);
                    }
                }

                foreach (IMapViewModelComponent section in sections)
                {
                    if (section is MapSectionViewModel)
                    {
                        MapSectionViewModel mapSection = section as MapSectionViewModel;
                        if (proxy.IsSectionValid(mapSection))
                        {
                            if (mapSection.Model.InitialiseSection(true, false))
                            {
                                proxy.LoadUserData(this.Dispatcher, mapSection);
                            }
                        }
                    }
                    // Determine if we have a cancellation
                    if (thread != null && thread.CancellationPending == true)
                    {
                        Thread.Sleep(1000);
                        handler.Set();
                        return;
                    }
                }
            }
            handler.Set();
            this.UserDataLoadingThreads.Remove(proxy.UserViewID);
        }

        /// <summary>
        /// Unloads the user data for a single proxy, this happens when the last instance
        /// of the user is closed.
        /// </summary>
        public void UnloadSingleUserProxy(IMapBrowserUserProxy proxy)
        {
            BackgroundWorker userDataThread = new BackgroundWorker();
            userDataThread.DoWork += new DoWorkEventHandler
            (
                delegate(Object obj, DoWorkEventArgs ue)
                {
                    if (this.SelectedLevel != null)
                    {
                        foreach (IMapViewModelContainer area in this.SelectedLevel.NonGenericDefintionContainerChildren)
                        {
                            foreach (IMapViewModelComponent section in area.ComponentChildren)
                            {
                                if (section is MapSectionViewModel)
                                {
                                    MapSectionViewModel mapSection = section as MapSectionViewModel;
                                    if (proxy.IsSectionValid(mapSection))
                                    {
                                        proxy.UnloadUserData(this.Dispatcher, mapSection);
                                    }
                                }
                            }
                        }
                    }
                }
            );
            userDataThread.RunWorkerAsync();
        }

        /// <summary>
        /// Unloads all the proxies for a certain level. This needs to happen before the
        /// user switches levels.
        /// </summary>
        public void UnloadAllUserProxies(LevelViewModel level)
        {
            if (this.SelectedLevel != null && this.OpenedMapUsers.Count > 0)
            {
                foreach (IMapViewModelContainer area in level.NonGenericDefintionContainerChildren)
                {
                    foreach (IMapViewModelComponent section in area.ComponentChildren)
                    {
                        if (section is MapSectionViewModel)
                        {
                            MapSectionViewModel mapSection = section as MapSectionViewModel;
                            foreach (IMapBrowserUserProxy proxy in this.OpenedMapUsers.Keys)
                            {
                                if (proxy.IsSectionValid(mapSection))
                                {
                                    proxy.UnloadUserData(this.Dispatcher, mapSection);
                                }
                            }
                        }
                    }
                }
            }
        }

        #endregion // Public Functions

        #region Private Functions

        /// <summary>
        /// Parses the vector map xml file to get the vector information for each container and create
        /// the geometry
        /// </summary>
        private void ParseVectorMapFile(LevelViewModel level, String filename)
        {
            this.LevelGeometry = new Dictionary<MapSectionViewModel, List<Vector2f>>();

            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(filename);

                XPathNavigator navigator = xmlDoc.CreateNavigator();
                XPathNodeIterator splineIter = navigator.Select(XPathExpression.Compile("/vector_map/section"));
                while (splineIter.MoveNext())
                {
                    // Parse section name
                    String mapSectionName = String.Empty;
                    XPathNodeIterator navNameIter = splineIter.Current.Select(XPathExpression.Compile("@name"));
                    if (navNameIter.MoveNext())
                    {
                        mapSectionName = (navNameIter.Current.Value as String);
                    }

                    //System.Diagnostics.Debug.Assert(!String.IsNullOrEmpty(mapSectionName), "Map Section name empty; invalid vector map XML.");

                    MapSectionViewModel section = level.GetMapSectionByName(mapSectionName, true);
                    if (section == null)
                    {
                        RSG.Base.Logging.Log.Log__Error("Map Section \"{0}\" not found in current level.", mapSectionName);
                        continue;
                    }

                    // Parse points
                    List<Vector2f> points = new List<Vector2f>();
                    GetMapSectionPoints(splineIter.Current, ref points);
                    if (!this.LevelGeometry.ContainsKey(section))
                    {
                        this.LevelGeometry.Add(section, points);
                    }
                }
            }
            catch (Exception ex)
            {
                RSG.Base.Logging.Log.Log__Exception(ex, "Unhandled exception in vector map parse");
            }
        }

        private void GetMapSectionPoints(XPathNavigator navigator, ref List<Vector2f> points)
        {
            XPathNodeIterator pointIter = navigator.Select(XPathExpression.Compile("spline/point"));
            while (pointIter.MoveNext())
            {
                XPathNavigator nav = pointIter.Current;
                String xCoord = nav.GetAttribute("x", "");
                String yCoord = nav.GetAttribute("y", "");
                RSG.Base.Math.Vector2f v = new RSG.Base.Math.Vector2f(float.Parse(xCoord), float.Parse(yCoord));
                points.Add(v);
            }
        }

        private void OnContentOpened(IContentBase openedContent)
        {
            foreach (IMapBrowserUserProxy proxy in this.MapUserProxies)
            {
                if (proxy.UserViewID == openedContent.ID)
                {
                    if (this.OpenedMapUsers.ContainsKey(proxy))
                    {
                        this.OpenedMapUsers[proxy]++;
                    }
                    else
                    {
                        this.OpenedMapUsers.Add(proxy, 1);
                        if (this.SelectedLevel != null && this.ValidMapData == true && !this.UserDataLoadingThreads.ContainsKey(proxy.UserViewID))
                        {
                            BackgroundWorker thread = new BackgroundWorker();
                            thread.WorkerReportsProgress = false;
                            thread.WorkerSupportsCancellation = true;
                            System.Threading.AutoResetEvent handler = new System.Threading.AutoResetEvent(false);
                            thread.DoWork += new DoWorkEventHandler
                            (
                                delegate(Object o, DoWorkEventArgs e)
                                {
                                    this.LoadSingleUserProxy(proxy, thread, handler);
                                }
                            );

                            this.UserDataLoadingThreads.Add(proxy.UserViewID, thread);
                            thread.RunWorkerAsync();
                        }
                    }
                }
            }
        }

        private void OnContentClosed(IContentBase closedContent)
        {
            foreach (IMapBrowserUserProxy proxy in this.MapUserProxies)
            {
                if (proxy.UserViewID == closedContent.ID)
                {
                    if (this.OpenedMapUsers.ContainsKey(proxy))
                    {
                        this.OpenedMapUsers[proxy]--;
                        if (this.OpenedMapUsers[proxy] == 0)
                        {
                            this.OpenedMapUsers.Remove(proxy);
                            if (this.UserDataLoadingThreads.ContainsKey(proxy.UserViewID))
                            {
                                this.UserDataLoadingThreads[proxy.UserViewID].CancelAsync();
                                this.UserDataLoadingThreads[proxy.UserViewID].RunWorkerCompleted += new RunWorkerCompletedEventHandler
                                (
                                    delegate(Object o, RunWorkerCompletedEventArgs e)
                                    {
                                        UnloadSingleUserProxy(proxy);
                                        this.UserDataLoadingThreads.Remove(proxy.UserViewID);
                                    }
                                );
                            }
                            else
                            {
                                UnloadSingleUserProxy(proxy);
                            }
                        }
                    }
                }
            }
        }

        private void OnLevelChanged(LevelViewModel oldLevel, LevelViewModel newLevel)
        {
            if (oldLevel == newLevel)
                return;

            // Cancel all current load threads
            foreach (KeyValuePair<Guid, BackgroundWorker> thread in this.UserDataLoadingThreads)
            {
                thread.Value.CancelAsync();
            }

            this.UserDataLoadingThreads.Clear();
            // Unload all user data for the old level
            UnloadAllUserProxies(oldLevel);

            // Load each proxy on it's own background thread
            foreach (IMapBrowserUserProxy proxy in this.OpenedMapUsers.Keys)
            {
                BackgroundWorker thread = new BackgroundWorker();
                thread.WorkerReportsProgress = false;
                thread.WorkerSupportsCancellation = true;
                System.Threading.AutoResetEvent handler = new System.Threading.AutoResetEvent(false);
                thread.DoWork += new DoWorkEventHandler
                (
                    delegate(Object o, DoWorkEventArgs e)
                    {
                        this.LoadSingleUserProxy(proxy, thread, handler);
                    }
                );
                this.UserDataLoadingThreads.Add(proxy.UserViewID, thread);
                thread.RunWorkerAsync();
            }

        }

        #endregion // Private Functions

        #region Methods

        /// <summary>
        /// Clears the selection in the treeview
        /// </summary>
        public void ClearSelection()
        {
            this.TreeView.ClearSelection();
        }

        #endregion // Methods
    } // MapBrowserViewModel
} // Workbench.AddIn.MapBrowser
