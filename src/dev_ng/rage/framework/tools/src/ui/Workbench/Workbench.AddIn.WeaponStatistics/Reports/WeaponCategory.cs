﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using Report.AddIn;
using RSG.Model.Report;

namespace Workbench.AddIn.WeaponStatistics.Reports
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Report.AddIn.ExtensionPoints.ReportCategory, typeof(IReportCategory))]
    class WeaponCategory :
        ReportCategory,
        IPartImportsSatisfiedNotification
    {
        #region Constants
        private const String NAME = "Weapons";
        private const String DESC = "Weapon Reports.";
        #endregion // Constants

        #region Properties and Associated Member Data
        /// <summary>
        /// Category report objects.
        /// This is abstract since we wish your class to implement it with the appropriate attribute ( as defined by your class ) for MEF imports.
        /// </summary>
        [ImportManyExtension(ExtensionPoints.Report, typeof(IReport))]
        public override IEnumerable<IReportItem> Reports { get; protected set; }
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public WeaponCategory()
            : base(NAME, DESC)
        {
        }
        #endregion // Constructor(s)
    } // WeaponCategory
} // Workbench.AddIn.WeaponStatistics.Reports
