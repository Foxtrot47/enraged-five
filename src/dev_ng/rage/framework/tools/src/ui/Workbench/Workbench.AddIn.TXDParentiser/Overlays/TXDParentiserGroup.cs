﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Viewport.AddIn;
using System.ComponentModel.Composition;

namespace Workbench.AddIn.TXDParentiser.Overlays
{
    /// <summary>
    /// Map Debugging Overlay Group
    /// </summary>
    [ExportExtension(Viewport.AddIn.ExtensionPoints.OverlayGroup, typeof(IViewportOverlayGroup))]
    public class TXDParentiserGroup : ViewportOverlayGroup, IPartImportsSatisfiedNotification
    {
        #region MEF Imports
        /// <summary>
        /// Overlays that form this Group.
        /// </summary>
        [ImportManyExtension(ExtensionPoints.TxdParentiserOverlayGroup, typeof(IViewportOverlay))]
        private IEnumerable<IViewportOverlay> ImportedOverlays
        {
            get;
            set;
        }
        #endregion // MEF Imports

        #region Constructor
        /// <summary>
        /// Default constructor.
        /// </summary>
        public TXDParentiserGroup()
            : base("TXD Parentiser")
        {
        }
        #endregion // Constructor

        #region IPartImportsSatisfiedNotification Interface
        /// <summary>
        /// MEF callback on imports.
        /// </summary>
        public void OnImportsSatisfied()
        {
            this.Overlays.AddRange(ImportedOverlays);
        }
        #endregion // IPartImportsSatisfiedNotification Interface
    } // TXDParentiser
}
