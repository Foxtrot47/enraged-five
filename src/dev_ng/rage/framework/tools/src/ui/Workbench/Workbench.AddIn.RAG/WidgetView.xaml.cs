﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using RSG.Base.Logging;
using Editor.AddIn;
using Editor.Workbench.AddIn;
using Editor.Workbench.AddIn.Services;

namespace Editor.Workbench.AddIn.RAG
{

    /// <summary>
    /// Interaction logic for WidgetView.xaml
    /// </summary>
    [ExportExtension(Editor.AddIn.ExtensionPoints.Views,
        typeof(ResourceDictionary))]
    partial class WidgetView : ResourceDictionary
    {
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public WidgetView()
        {
            InitializeComponent();
        }
        #endregion // Constructor(s)

        #region Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnConnect_Click(Object sender, RoutedEventArgs e)
        {
            WidgetViewModel vm = GetViewModel(sender as FrameworkElement);
            if (vm.Connect())
            {
                Log.Log__Message("Successfully connected to {0}.", vm.Hostname);
            }
            else
            {
                Log.Log__Error("Failed to connect to {0}.", vm.Hostname);
            }
        }
        #endregion // Event Handlers

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        private WidgetViewModel GetViewModel(FrameworkElement element)
        {
            WidgetViewModel vm = (element.DataContext as WidgetViewModel);
            return (vm);
        }
        #endregion // Private Methods
    }

} // Editor.Workbench.AddIn.RAG namespace
