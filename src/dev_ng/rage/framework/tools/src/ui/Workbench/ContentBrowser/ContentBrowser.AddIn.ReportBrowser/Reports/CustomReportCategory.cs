﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Report;
using RSG.Base.Configuration.Reports;

namespace ContentBrowser.AddIn.ReportBrowser.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public class CustomReportCategory : Report.AddIn.ReportCategory
    {
        #region Constants
        private const string DESC = "Custom report as obtained from the reports.xml config file.";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// List of reports that make up this category
        /// </summary>
        public override IEnumerable<IReportItem> Reports
        {
            get;
            protected set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public CustomReportCategory(ICustomReportGroup group)
            : base(group.Name, DESC)
        {
            IList<IReportItem> reports = new List<IReportItem>();

            foreach (ICustomReportGroup subGroup in group.SubGroups)
            {
                reports.Add(new CustomReportCategory(subGroup));
            }

            foreach (ICustomReport report in group.Reports)
            {
                reports.Add(new RSG.Model.Report.Reports.CustomReport(report));
            }
            
            Reports = reports;
            AssetChildren.AddRange(reports);
        }
        #endregion // Constructor(s)
    } // CustomReportCategory
}
