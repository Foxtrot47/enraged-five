﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Model.Common.Map;
using System.Collections.Specialized;
using System.Windows;
using System.ComponentModel;

namespace ContentBrowser.ViewModel.MapViewModels3
{
    /// <summary>
    /// 
    /// </summary>
    public class FragmentArchetypeViewModel : MapArchetypeViewModel
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        private IFragmentArchetype FragmentArchetype
        {
            get
            {
                return (IFragmentArchetype)Model;
            }
        }
        #endregion // Properties

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public FragmentArchetypeViewModel(IFragmentArchetype fragment)
            : base(fragment)
        {
        }
        #endregion // Constructor(s)
    } // FragmentArchetypeViewModel
}
