﻿using System.Collections.Generic;
using MetadataEditor.AddIn.MetaEditor.ViewModel;
using RSG.Metadata.Data;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows;
using RSG.Base.Collections;
using RSG.Base.Editor.Command;
using System.Collections.ObjectModel;
using System;
using MetadataEditor.AddIn.MetaEditor.BasicViewModel;
using System.Text.RegularExpressions;

namespace MetadataEditor.AddIn.MetaEditor.GridViewModel
{
    /// <summary>
    /// 
    /// </summary>
    public class StructureTunableViewModel : GridTunableViewModel
    {
        #region Properties

        /// <summary>
        /// Exposes the key property to let users edit the value.
        /// </summary>
        public string TunableName
        {
            get
            {
                var parentModel = Model.ParentModel;

                if (Parent == null || !(Parent.Model is MapTunable))
                {
                    return String.Empty;
                }

                var mapTunable = Parent.Model as MapTunable;
                string keyValue = String.Empty;

                foreach (var item in mapTunable.Items)
                {
                    if (item.Value == parentModel)
                    {
                        keyValue = item.Key;
                        break;
                    }
                }

                if (String.IsNullOrEmpty(keyValue) && parentModel is MapItemTunable)
                {
                    keyValue = ((MapItemTunable)parentModel).Key;
                }

                return keyValue;
            }
            set
            {
                var parentModel = Model.ParentModel;

                if (Parent == null || !(Parent.Model is MapTunable))
                {
                    return;
                }

                var mapTunable = Parent.Model as MapTunable;
                string keyValue = String.Empty;

                foreach (var item in mapTunable.Items)
                {
                    if (item.Value == parentModel)
                    {
                        item.Key = value;
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Columns to display in the table.
        /// </summary>
        public Collection<DataGridColumn> Columns
        {
            get
            {
                Collection<DataGridColumn> columns = new Collection<DataGridColumn>();

                int index = 0;
                foreach (GridTunableViewModel child in this.Children)
                {
                    DataGridTemplateColumn childColumn = new DataGridTemplateColumn();
                    childColumn.IsReadOnly = true;
                    childColumn.Header = child.Name;
                    childColumn.MinWidth = 70.0;

                    DataTemplate cbdt = new DataTemplate();
                    FrameworkElementFactory ccFactory = new FrameworkElementFactory(typeof(ContentControl));

                    Binding binding = new Binding(string.Format("Children[{0}]", index));
                    ccFactory.SetBinding(ContentControl.ContentProperty, binding);
                    cbdt.VisualTree = ccFactory;
                    childColumn.CellTemplate = cbdt;

                    if (this.Parent == null)
                    {
                        bool isStructure = false;
                        double width = 0;
                        if (child is StructureTunableViewModel)
                        {
                            width = (child as StructureTunableViewModel).Children.Count * 70 + 30;
                            isStructure = true;
                        }
                        else if (child is PointerTunableViewModel)
                        {
                            if ((child as PointerTunableViewModel).IsStructure)
                            {
                                int count = ((child as PointerTunableViewModel).Value as StructureTunableViewModel).Children.Count;
                                width = count * 70 + 30;
                                isStructure = true;
                            }
                        }


                        if (isStructure)
                        {
                            childColumn.Width = new DataGridLength(width, DataGridLengthUnitType.Pixel);
                        }
                        else
                        {
                            childColumn.Width = DataGridLength.Auto;
                        }
                    }
                    else
                    {
                        childColumn.Width = new DataGridLength(1, DataGridLengthUnitType.Star);
                    }

                    columns.Add(childColumn);
                    index++;
                }


                //DataGridTextColumn nameColumn = new DataGridTextColumn();
                //nameColumn.Header = "Name";
                //nameColumn.IsReadOnly = true;
                //Binding nameBinding = new Binding("Name");
                //nameColumn.Binding = nameBinding;
                //columns.Add(nameColumn);

                //DataGridTextColumn typeColumn = new DataGridTextColumn();
                //typeColumn.Header = "Type";
                //typeColumn.IsReadOnly = true;
                //Binding typeBinding = new Binding("FriendlyTypeName");
                //typeBinding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
                //typeColumn.Binding = typeBinding;
                //columns.Add(typeColumn);

                //DataGridTemplateColumn valueColumn = new DataGridTemplateColumn();
                //valueColumn.Header = "Value";
                //DataTemplate dt = new DataTemplate();
                //FrameworkElementFactory ccFactory = new FrameworkElementFactory(typeof(ContentControl));
                //ccFactory.SetBinding(ContentControl.ContentProperty, new Binding());
                //dt.VisualTree = ccFactory;
                //valueColumn.CellTemplate = dt;
                //columns.Add(valueColumn);

                //DataGridTemplateColumn inheritColumn = new DataGridTemplateColumn();
                //inheritColumn.Header = "Inherit";

                //DataTemplate cbdt = new DataTemplate();
                //FrameworkElementFactory cbFactory = new FrameworkElementFactory(typeof(CheckBox));
                //cbFactory.SetBinding(CheckBox.IsEnabledProperty, new Binding("Model.CanInheritParent"));
                //cbFactory.SetValue(CheckBox.WidthProperty, (double)22);
                //cbFactory.SetValue(CheckBox.HeightProperty, (double)22);
                //cbFactory.SetValue(CheckBox.VerticalAlignmentProperty, VerticalAlignment.Top);
                //Style checkboxStyle = new Style(typeof(CheckBox));
                //checkboxStyle.Setters.Add(new Setter(CheckBox.IsCheckedProperty, false));
                //Trigger trigger = new Trigger()
                //{
                //    Property = CheckBox.IsEnabledProperty,
                //    Value = true,
                //};
                //trigger.Setters.Add(new Setter(CheckBox.IsCheckedProperty, new Binding("InheritParent") { UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged }));
                //checkboxStyle.Triggers.Add(trigger);
                //cbFactory.SetValue(CheckBox.StyleProperty, checkboxStyle);
                //cbdt.VisualTree = cbFactory;
                //inheritColumn.CellTemplate = cbdt;

                //columns.Add(inheritColumn);

                //if (this.Parent == null)
                //{
                //    nameColumn.Width = new DataGridLength(186, DataGridLengthUnitType.Pixel);
                //    typeColumn.Width = new DataGridLength(228, DataGridLengthUnitType.Pixel);
                //    valueColumn.Width = new DataGridLength(1061, DataGridLengthUnitType.Pixel);
                //    inheritColumn.Width = new DataGridLength(100, DataGridLengthUnitType.Pixel);
                //}
                //else
                //{
                //    nameColumn.Width = new DataGridLength(15, DataGridLengthUnitType.Star);
                //    typeColumn.Width = new DataGridLength(12, DataGridLengthUnitType.Star);
                //    valueColumn.Width = new DataGridLength(69, DataGridLengthUnitType.Star);
                //    inheritColumn.Width = new DataGridLength(6, DataGridLengthUnitType.Star);
                //}

                return columns;
            }
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        internal override ITunableViewModel FindNextInChildren(Regex expression)
        {
            foreach (var child in this.Children)
            {
                ITunableViewModel found = child.FindNextInChildren(expression);
                if (found != null)
                {
                    return found;
                }

                if (child.IsMatch(expression))
                {
                    return child;
                }
            }

            return null;
        }

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="MetadataEditor.AddIn.MetaEditor.GridViewModel.StructureTunableViewModel"/>
        /// class.
        /// </summary>
        public StructureTunableViewModel(GridTunableViewModel parent, StructureTunable m, MetaFileViewModel root)
            : base(parent, m, root)
        {
            foreach (KeyValuePair<string, ITunable> t in m)
            {
                if (t.Value.Definition.HideWidgets == true)
                {
                    continue;
                }

                GridTunableViewModel vm = TunableViewModelFactory.Create(this, t.Value, root);
                this.Children.Add(vm);
            }

            if (this.Parent == null)
                this.IsExpanded = true;
        }
        #endregion
    } // MetadataEditor.AddIn.MetaEditor.GridViewModel.StructureTunableViewModel
} // MetadataEditor.AddIn.MetaEditor.GridViewModel
