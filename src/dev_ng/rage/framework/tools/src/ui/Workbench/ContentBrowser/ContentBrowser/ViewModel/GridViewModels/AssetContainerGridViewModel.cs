﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;

namespace ContentBrowser.ViewModel.GridViewModels
{
    /// <summary>
    /// 
    /// </summary>
    public class AssetContainerGridViewModel : GridViewModelBase
    {
        #region Members

        private IHasAssetChildren m_model = null;

        #endregion // Members

        #region Properties

        /// <summary>
        /// A reference to the types model that this
        /// view model is wrapping
        /// </summary>
        public new IHasAssetChildren Model
        {
            get { return m_model; }
            set
            {
                if (m_model == value)
                    return;

                SetPropertyValue(value, () => Model,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_model = (IHasAssetChildren)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The main name property which by default
        /// will be shown as the header for a browser item
        /// </summary>
        public override String Name
        {
            get { return this.Model.Name; }
        }

        #endregion // Properties
        
        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public AssetContainerGridViewModel(IHasAssetChildren model)
            : base(model)
        {
            this.Model = model;
        }

        #endregion // Constructor

        #region Object Overrides

        /// <summary>
        /// Make sure the name is returned on ToString
        /// </summary>
        public override String ToString()
        {
            return this.Name;
        }

        /// <summary>
        /// Gets the hash code for this level
        /// using the name property
        /// </summary>
        public override int GetHashCode()
        {
            return this.Name.GetHashCode();
        }

        #endregion // Object Overrides
    } // AssetContainerGridViewModel

    /// <summary>
    /// 
    /// </summary>
    public class DummyAssetContainerGridViewModel : GridViewModelBase
    {
        #region Members

        private String m_name;

        #endregion // Members

        #region Properties

        /// <summary>
        /// The main name property which by default
        /// will be shown as the header for a browser item
        /// </summary>
        public override String Name
        {
            get { return this.m_name; }
        }

        #endregion // Properties

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public DummyAssetContainerGridViewModel(String dummyName, IAsset model)
            : base(model)
        {
            this.m_name = dummyName;
        }

        #endregion // Constructor

        #region Object Overrides

        /// <summary>
        /// Make sure the name is returned on ToString
        /// </summary>
        public override String ToString()
        {
            return this.Name;
        }

        /// <summary>
        /// Gets the hash code for this level
        /// using the name property
        /// </summary>
        public override int GetHashCode()
        {
            return this.Name.GetHashCode();
        }

        #endregion // Object Overrides
    } // DummyAssetContainerGridViewModel
} // ContentBrowser.ViewModel.GridViewModels
