﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MetadataEditor.AddIn;
using Workbench.AddIn.Services;
using Workbench.AddIn;

namespace MetadataEditor
{
    /// <summary>
    /// Settings for Metadata Definitions
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.Settings, typeof(ISettings))]
    public class MetadataPreferences : SettingsBase, ISettings
    {
        #region MEF Imports

        [ImportExtension(MetadataEditor.AddIn.CompositionPoints.StructureDictionary, typeof(IStructureDictionary)),
         System.ComponentModel.Browsable(false)]
        public LoadedStructures LoadedStructures { get; set; }

        #endregion // MEF IMports

        #region Properties

        /// <summary>
        /// Metadata definition location root location
        /// </summary>
        [System.ComponentModel.DisplayName("Location"),
         System.ComponentModel.Description("The root directory the metadata definitions are loaded from.")]
        public String DefinitionLocation { get; set; }

        #endregion // Properties

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        public MetadataPreferences()
        {
            this.Title = "Metadata Definitions";
        }

        #endregion // Constructor

        #region Methods

        /// <summary>
        /// Checks its current state for any invalid data.
        /// </summary>
        /// <param name="errors">The service will fill this up with one string per 
        /// error it finds in its data.</param>
        /// <returns>true if everything is fine, false if an error was found</returns>
        public Boolean Validate(List<String> errors)
        {
            return true;
        }

        /// <summary>
        /// Loads settings
        /// </summary>
        public void Load()
        {
            this.DefinitionLocation = LoadedStructures.DefinitionRootLocation;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Apply()
        {
            LoadedStructures.DefinitionRootLocation = this.DefinitionLocation;
        }

        #endregion // Methods
    } // MetadataPreferences
} // MetadataEditor
