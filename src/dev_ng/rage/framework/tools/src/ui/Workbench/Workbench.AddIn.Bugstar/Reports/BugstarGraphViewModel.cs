﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using RSG.Base.Collections;
using RSG.Base.Editor.Command;
using RSG.Interop.Bugstar.Search;

namespace Workbench.AddIn.Bugstar.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public class BugstarGraphViewModel :
        ViewModelBase
    {
        #region Properties
        /// <summary>
        /// Graph model object
        /// </summary>
        public Graph Graph
        {
            get { return m_graph; }
            set
            {
                SetPropertyValue(value, () => this.Graph,
                       new PropertySetDelegate(
                           delegate(Object newValue)
                           {
                               m_graph = (Graph)newValue;
                           }
                       )
                   );
            }
        }
        private Graph m_graph;

        /// <summary>
        /// Data points associated with the graph
        /// Don't directly access Graph.DataPoints as it queries bugstar each time it is accessed
        /// which for the graphs is a LOT!
        /// </summary>
        public ObservableCollection<KeyValuePair<string, int>> GraphData
        {
            get { return m_graphData; }
            set
            {
                SetPropertyValue(value, () => this.PetData,
                       new PropertySetDelegate(
                           delegate(Object newValue)
                           {
                               m_graphData = (ObservableCollection<KeyValuePair<string, int>>)newValue;
                           }
                       )
                   );
            }
        }
        private ObservableCollection<KeyValuePair<string, int>> m_graphData;

        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<KeyValuePair<string, int>> PetData
        {
            get { return m_petData; }
            set
            {
                SetPropertyValue(value, () => this.PetData,
                       new PropertySetDelegate(
                           delegate(Object newValue)
                           {
                               m_petData = (ObservableCollection<KeyValuePair<string, int>>)newValue;
                           }
                       )
                   );
            }
        }
        private ObservableCollection<KeyValuePair<string, int>> m_petData;

        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<KeyValuePair<string, int>> PetData2
        {
            get { return m_petData2; }
            set
            {
                SetPropertyValue(value, () => this.PetData2,
                       new PropertySetDelegate(
                           delegate(Object newValue)
                           {
                               m_petData2 = (ObservableCollection<KeyValuePair<string, int>>)newValue;
                           }
                       )
                   );
            }
        }
        private ObservableCollection<KeyValuePair<string, int>> m_petData2;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default Constructor
        /// </summary>
        public BugstarGraphViewModel(Graph graph)
            : base()
        {
            Graph = graph;
            GraphData = new ObservableCollection<KeyValuePair<string,int>>(graph.DataPoints);

            m_petData = new ObservableCollection<KeyValuePair<string, int>>();
            m_petData.Add(new KeyValuePair<string, int>("Dog", 30));
            m_petData.Add(new KeyValuePair<string, int>("Cat", 25));
            m_petData.Add(new KeyValuePair<string, int>("Rat", 5));
            m_petData.Add(new KeyValuePair<string, int>("Hampster", 8));
            m_petData.Add(new KeyValuePair<string, int>("Rabbit", 12));


            m_petData2 = new ObservableCollection<KeyValuePair<string, int>>();
            m_petData2.Add(new KeyValuePair<string, int>("Dog", 10));
            m_petData2.Add(new KeyValuePair<string, int>("Cat", 5));
            m_petData2.Add(new KeyValuePair<string, int>("Rat", 12));
            m_petData2.Add(new KeyValuePair<string, int>("Hampster", 16));
            m_petData2.Add(new KeyValuePair<string, int>("Rabbit", 7));
            m_petData2.Add(new KeyValuePair<string, int>("Dolphin", 1));
        }
        #endregion // Constructor(s)
    } // BugstarGraphViewModel
} // Workbench.AddIn.Bugstar.Reports
