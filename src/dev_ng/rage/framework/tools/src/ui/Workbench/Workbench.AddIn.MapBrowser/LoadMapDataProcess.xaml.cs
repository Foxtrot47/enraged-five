﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Interop;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RSG.Model.Map;
using RSG.Model.Map.ViewModel;
using RSG.Model.Map.Filters;
using RSG.Model.Map.Statistics;

namespace Workbench.AddIn.MapBrowser
{
    /// <summary>
    /// Interaction logic for BackgroundProcess.xaml
    /// </summary>
    public partial class LoadMapDataProcess : Window,  INotifyPropertyChanged
    {
        #region Events

        /// <summary>
        /// Property changed event fired when the name changes so that the 
        /// binding can be dynamic.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Create the OnPropertyChanged method to raise the event
        /// </summary>
        /// <param name="name"></param>
        protected void OnPropertyChanged(String name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        #endregion

        #region Dll Imports

        private const int GWL_STYLE = -16; private const int WS_SYSMENU = 0x80000;

        [DllImport("user32.dll", SetLastError = true)]
        private static extern int GetWindowLong(IntPtr hWnd, int nIndex);

        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        #endregion // Dll Imports

        #region Properties

        public int CurrentSection
        {
            get { return m_currentSection; }
            set
            {
                m_currentSection = value;
                OnPropertyChanged("CurrentSection");
            }
        }
        private int m_currentSection = 0;

        public int TotalSections
        {
            get { return m_totalSections; }
            set
            {
                m_totalSections = value;
                OnPropertyChanged("TotalSections");
            }
        }
        private int m_totalSections = 0;

        public String LoadingSection
        {
            get { return m_sectionName; }
            set
            {
                m_sectionName = value;
                OnPropertyChanged("LoadingSection");
            }
        }
        private String m_sectionName = String.Empty;

        internal MapBrowserViewModel ViewModel
        {
            get;
            set;
        }

        public Boolean CurrentlyLoading
        {
            get;
            set;
        }

        public Boolean LoadingViewModel
        {
            get { return m_loadingViewModel; }
            set
            {
                m_loadingViewModel = value;
                OnPropertyChanged("LoadingViewModel");
            }
        }
        private Boolean m_loadingViewModel = false;

        #endregion // Properties

        #region Constructors

        public LoadMapDataProcess()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        internal LoadMapDataProcess(MapBrowserViewModel viewModel)
        {
            InitializeComponent();
            this.DataContext = this;
            this.ViewModel = viewModel;
        }

        #endregion // Constructors

        #region Loading Mathods

        /// <summary>
        /// Makes sure that the close button is removed so the user cannot cancel the loading thread
        /// </summary>
        private void OnLoaded(Object sender, RoutedEventArgs e)
        {
            var hwnd = new WindowInteropHelper(this).Handle;
            SetWindowLong(hwnd, GWL_STYLE, GetWindowLong(hwnd, GWL_STYLE) & ~WS_SYSMENU);

            CurrentlyLoading = true;
            BackgroundWorker thread = this.CreateThread();
            if (thread != null)
            {
                thread.RunWorkerAsync();
            }
            else
            {
                MessageBox.Show("Unknown thread error in creating map data", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                DialogResult = false;
            }
        }

        /// <summary>
        /// Creates the thread that is used to load up the 
        /// generic map sections
        /// </summary>
        private BackgroundWorker CreateThread()
        {
            if (ViewModel != null)
            {
                BackgroundWorker backgroundWorker = new BackgroundWorker();
                backgroundWorker.WorkerReportsProgress = true;

                // what to do in the background thread
                backgroundWorker.DoWork += new DoWorkEventHandler
                (
                    delegate(Object o, DoWorkEventArgs args)
                    {
                        this.LoadGeneric(backgroundWorker);
                    }
                );

                backgroundWorker.ProgressChanged += new ProgressChangedEventHandler
                (
                    delegate(Object o, ProgressChangedEventArgs args)
                    {
                        if (this.LoadProgress != null)
                        {
                            this.LoadProgress.Value = args.ProgressPercentage;
                        }
                    }
                );

                backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler
                (
                    delegate(Object o, RunWorkerCompletedEventArgs args)
                    {
                        this.LoadProgress.Value = 0;
                        this.ViewModel.RefreshingMapData = false;
                        this.ViewModel.ValidMapData = true;
                        Mouse.OverrideCursor = null;

                        SceneManager.Reset();
                        GC.Collect();
                        GC.WaitForPendingFinalizers();
                        CurrentlyLoading = false;
                        DialogResult = true;
                    }
                );

                return backgroundWorker;
            }
            return null;
        }

        /// <summary>
        /// Loads the generic map sections up and makes sure that the props are done
        /// first, before the interiors
        /// </summary>
        private void LoadGeneric(BackgroundWorker thread)
        {
            SceneManager.Reset();
            this.ViewModel.AllReferences = new Dictionary<Level, LevelReferences>();
            this.ViewModel.GenericDefinitions = new DefinitionList();
            this.ViewModel.ValidMapData = false;
            try
            {
                this.ViewModel.LevelDictionary = new LevelDictionary();

                List<MapSection> sectionsToload = new List<MapSection>();
                int totalSectionsToLoad = this.ViewModel.LevelDictionary.DoPreLoadStep(LevelFilters.All, AreaFilters.All, MapFilters.All, this.ViewModel.ConfigurationService.GameConfig, ref sectionsToload);
                System.Diagnostics.Debug.Print(totalSectionsToLoad.ToString());

                int i = 0;
                List<MapSection> interiorSections = new List<MapSection>();
                int totalGenericCount = sectionsToload.Where(s => s.ExportInstances == false && s.ExportDefinitions == true).Count();

                this.TotalSections = totalGenericCount;

                foreach (MapSection mapSection in sectionsToload)
                {
                    // Determine generic sections and create their scenes and then their statistics
                    if (mapSection.ExportInstances == false && mapSection.ExportDefinitions == true)
                    {
                        if (mapSection.CreateExportPaths())
                        {
                            RSG.SceneXml.Scene scene = SceneManager.GetScene(mapSection.SceneXmlFilename);
                            SectionTypes type = mapSection.DetermineSectionType(scene);
                            if (type == SectionTypes.PROPS)
                            {
                                this.CurrentSection = i + 1;
                                this.LoadingSection = mapSection.Name;

                                mapSection.CreateDataFromSceneXml(this.ViewModel.GenericDefinitions);

                                int percentageComplete = (int)(System.Math.Ceiling((100.0 / totalGenericCount) * ++i));
                                thread.ReportProgress(percentageComplete);
                            }
                            else if (type == SectionTypes.INTERIORS)
                            {
                                interiorSections.Add(mapSection);
                            }
                        }
                        else
                        {
                            this.CurrentSection = i + 1;
                            this.LoadingSection = mapSection.Name;

                            int percentageComplete = (int)(System.Math.Ceiling((100.0 / totalGenericCount) * ++i));
                            thread.ReportProgress(percentageComplete);

                            mapSection.ValidMapData = false;
                            mapSection.Container.MapComponents.Remove(mapSection.Name.ToLower());
                        }
                    }
                    else
                    {
                        if (mapSection.ExportInstances == true)
                        {
                            if (mapSection.ExportDefinitions == true)
                            {
                                mapSection.SectionType = SectionTypes.MAP;

                            }
                            else
                            {
                                mapSection.SectionType = SectionTypes.PROPGROUP;
                            }
                        }
                        else
                        {
                            mapSection.SectionType = SectionTypes.UNKNOWN;
                        }
                    }
                }

                foreach (MapSection interior in interiorSections)
                {
                    this.CurrentSection = i + 1;
                    this.LoadingSection = interior.Name;

                    interior.CreateDataFromSceneXml(this.ViewModel.GenericDefinitions);

                    int percentageComplete = (int)(System.Math.Ceiling((100.0 / totalGenericCount) * ++i));
                    thread.ReportProgress(percentageComplete);
                }


                this.Dispatcher.Invoke(
                    new Action(
                        delegate()
                        {
                            this.SectionCount.Text = "Loading view model";
                            this.LoadingViewModel = true;
                            Mouse.OverrideCursor = Cursors.Wait;
                        }
                    ), System.Windows.Threading.DispatcherPriority.Normal);

                System.Threading.Thread.Sleep(500);

                this.Dispatcher.Invoke(
                    new Action(
                        delegate()
                        {
                            this.ViewModel.ContentLevel = new LevelDictionaryViewModel(null, this.ViewModel.LevelDictionary);
                            this.ViewModel.SelectedLevel = this.ViewModel.ContentLevel.Levels.FirstOrDefault();
                            this.ViewModel.SelectedLevel.IsSelected = true;
                            this.ViewModel.LevelDictionary = null;
                        }
                    ), System.Windows.Threading.DispatcherPriority.Normal);
            }
            catch
            {
                this.ViewModel.ContentLevel = null;
                RSG.Base.Logging.Log.Log__Error("Error in the map loading background thread");
                this.Dispatcher.Invoke(
                    new Action(
                        delegate()
                        {
                            Mouse.OverrideCursor = null;
                        }
                    ), System.Windows.Threading.DispatcherPriority.Normal);
            }
            finally
            {
            }
        }

        #endregion // Loading Mathods
    } // LoadMapDataProcess
} // Workbench.AddIn.MapBrowser
