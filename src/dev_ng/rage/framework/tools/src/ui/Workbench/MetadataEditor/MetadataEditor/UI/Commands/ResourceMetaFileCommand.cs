﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using RSG.Base.ConfigParser;
using RSG.Base.Logging;
using RSG.Platform;
using Workbench.AddIn;
using Workbench.AddIn.Commands;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.Services;
using MetadataEditor.AddIn;

namespace MetadataEditor.UI.Commands
{

    /// <summary>
    /// Resource metadata file command.
    /// </summary>
    [ExportExtension(MetadataEditor.AddIn.ExtensionPoints.MenuMetadata, 
        typeof(IWorkbenchCommand))]
    [ExportExtension(MetadataEditor.AddIn.ExtensionPoints.MetadataToolbar,
        typeof(IWorkbenchCommand))]
    class ResourceMetaFileCommand : WorkbenchMenuItemBase
    {
        #region MEF Imports
        /// <summary>
        /// Layout Manager Service
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager, typeof(ILayoutManager))]
        private ILayoutManager LayoutManager { get; set; }

        /// <summary>
        /// Configuration service
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(IConfigurationService))]
        private IConfigurationService ConfigService { get; set; }

        /// <summary>
        /// MEF import for message box service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.MessageService, typeof(IMessageService))]
        private IMessageService MessageService { get; set; }
        #endregion // MEF Imports

        #region Constants
        /// <summary>
        /// Unique GUID for the menu item
        /// </summary>
        public static readonly Guid GUID = new Guid("EFE1A272-B00D-4805-9B39-9078BA0F86BD");
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ResourceMetaFileCommand()
        {
            this.Header = "_Resource Meta File";
            this.ID = GUID;
            this.KeyGesture = new System.Windows.Input.KeyGesture(System.Windows.Input.Key.F7);
            this.SetImageFromBitmap(Resources.Images.Convert);
        }
        #endregion // Constructor(s)

        #region ICommand Implementation
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public override bool CanExecute(object parameter)
        {
            IDocumentBase activeDocument = LayoutManager.ActiveDocument();
            IMetadataView view = activeDocument as IMetadataView;
            if (view == null || view.Path == null)
            {
                return false;
            }

            return view.Path.StartsWith(ConfigService.GameConfig.ExportDir, StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        public override void Execute(object parameter)
        {
            IDocumentBase activeDocument = LayoutManager.ActiveDocument();
            String outputFilename = Path.Combine(ConfigService.Config.Project.DefaultBranch.Preview,
                Path.GetFileName(activeDocument.Path));
            StringBuilder sb = new StringBuilder();
            sb.Append(" --no-content");
            sb.Append(" --rebuild");
            sb.AppendFormat(" {0}", Path.GetFullPath(activeDocument.Path));

            Process convertProcess = new Process();
            convertProcess.StartInfo.UseShellExecute = false;
            convertProcess.StartInfo.FileName = Path.Combine(ConfigService.GameConfig.ToolsRootDir,
                "ironlib", "lib", "RSG.Pipeline.Convert.exe");
            convertProcess.StartInfo.Arguments = sb.ToString();

            Log.Log__Message("Convert: {0} {1}",
                convertProcess.StartInfo.FileName,
                convertProcess.StartInfo.Arguments);

            convertProcess.Start();
            convertProcess.WaitForExit();

            if (convertProcess.ExitCode == 0)
            {
                // Preview conversion succeeded
                MessageService.Show("Successfully converted the metadata file.");
            }
            else
            {
                // Preview conversion failed
                MessageService.Show("An error occurred while converting the file.");
            }
        }
        #endregion // ICommand Implementation
    } // MetadataResourceMetaFileMenuItem

} // MetadataEditor.UI.Commands namespace
