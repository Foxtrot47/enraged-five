﻿using System;
using RSG.Model.Report;
using ContentBrowser.ViewModel.GridViewModels;

namespace ContentBrowser.ViewModel.ReportViewModels
{

    /// <summary>
    /// 
    /// </summary>
    internal class ReportGridViewModel : GridViewModelBase
    {
        #region Constructor(s)

        /// <summary>
        /// Default constructor
        /// </summary>
        public ReportGridViewModel(IReport report)
            : base(report)
        {
        }
        #endregion // Constructor(s)
    }

} // ContentBrowser.ViewModel.ReportViewModels
