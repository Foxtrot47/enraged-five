﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using Workbench.AddIn;
using Workbench.AddIn.Commands;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.UI.Menu;
using Workbench.AddIn.Services;
using Workbench.AddIn.Services.File;
using RSG.Base.Logging;
using Workbench.Services;
using System.Windows;
using RSG.Base.Editor;

namespace Workbench.UI.Menu
{
    /// <summary>
    /// File -> Recent Files List menu item.
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.MenuFile, typeof(IWorkbenchCommand))]
    class FileMenuRecentFiles : WorkbenchMenuItemBase, IPartImportsSatisfiedNotification
    {
        #region MEF Imports
        /// <summary>
        /// Extensions service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ExtensionService)]
        private IExtensionService ExtensionService { get; set; }

        [ImportExtension(Workbench.AddIn.CompositionPoints.MessageService, 
            typeof(Workbench.AddIn.Services.IMessageService))]
        protected IMessageService MessageService { get; set; }

        /// <summary>
        /// MEF import for the IOpenService objects.
        /// </summary>
        [ImportManyExtension(Workbench.AddIn.ExtensionPoints.OpenService,
            typeof(IOpenService))]
        private IEnumerable<IOpenService> OpenServices { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager,
            typeof(ILayoutManager))]
        private ILayoutManager LayoutManager { get; set; }

        /// <summary>
        /// Recently Used File menu subitems.
        /// </summary>
        [ImportManyExtension(Workbench.AddIn.ExtensionPoints.MenuFileRecentFiles,
            typeof(IWorkbenchCommand))]
        private IEnumerable<IWorkbenchCommand> items { get; set; }

        /// <summary>
        /// Extensions service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.WorkbenchOpenService)]
        private IWorkbenchOpenService WorkbenchOpenService { get; set; }

        #endregion // MEF Imports

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public FileMenuRecentFiles()
        {
            this.Header = "Recent _Files";
            this.ID = new Guid(Workbench.AddIn.CompositionPoints.MenuFileRecentFiles);
            this.RelativeID = FileMenuSep3.GUID;
            this.Direction = Direction.After;
        }
        #endregion // Constructor(s)

        #region IPartImportsSatisfiedNotification Methods
        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {
            this.Items = ExtensionService.Sort(items);
        }
        #endregion // IPartImportsSatisfiedNotification Methods

        #region ICommand Implementation
        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return (true);
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
            Log.Log__Debug("FileMenuRecentFiles::Execute()");
        }

        #endregion // ICommand Implementation

        #region Submenu Methods

        /// <summary>
        /// Gets called whenever the sub menu open state changes
        /// </summary>
        protected override void OnIsSubmenuOpenChanged()
        {
            if (!IsSubmenuOpen)
                return;

            List<IWorkbenchCommand> newItems = new List<IWorkbenchCommand>();
            RecentFileService.RecentlyUsedFile[] files =
                RecentFileService.GetFiles();
            int index = 1;
            Guid previousId = new Guid();
            IOpenService openService = null;
            foreach (RecentFileService.RecentlyUsedFile file in files)
            {
                foreach (IOpenService os in this.OpenServices)
                {
                    if (os.ID == file.OpenService)
                    {
                        openService = os;
                        break;
                    }
                }

                MRUEntryMenuitem mi = new MRUEntryMenuitem(index, file.Filename,
                    openService, previousId, this.MessageService, this.LayoutManager, this.WorkbenchOpenService);
                newItems.Add(mi);

                ++index;
            }
            this.Items = ExtensionService.SortAndJoin(newItems, new WorkbenchCommandSeparator(), items);
        }

        #endregion // Submenu Methods
    } // FileMenuRecentFiles

    #region MRUEntryMenuitem Class

    /// <summary>
    /// MRUEntryMenuitem represents a most-recently-used file menu item.
    /// </summary>
    class MRUEntryMenuitem : WorkbenchMenuItemBase
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Recently used file filename.
        /// </summary>
        private String Filename { get; set; }

        /// <summary>
        /// Workbench open service.
        /// </summary>
        private IOpenService OpenService { get; set; }

        /// <summary>
        /// Workbench messagebox service.
        /// </summary>
        private IMessageService MessageService { get; set; }

        /// <summary>
        /// Workbench layout manager service.
        /// </summary>
        private ILayoutManager LayoutManager { get; set; }

        /// <summary>
        /// 
        /// </summary>
        private IWorkbenchOpenService WorkbenchOpenService { get; set; }
        #endregion // Properties and Associated Member Data

        #region Constructor(s)

        /// <summary>
        /// Constructor.
        /// </summary>
        public MRUEntryMenuitem(int index, String filename, IOpenService os, Guid previousId, IMessageService ms, ILayoutManager layoutManager, IWorkbenchOpenService workbenchOpenService)
        {
            this.WorkbenchOpenService = workbenchOpenService;
            this.Header = String.Empty;
            this.ID = Guid.NewGuid();
            this.RelativeID = previousId;
            this.Direction = Direction.After;
            this.Header = String.Format("_{0} {1}", index, filename);
            this.Filename = filename;
            this.OpenService = os;
            this.MessageService = ms;
            this.LayoutManager = layoutManager;
        }

        /// <summary>
        /// 
        /// </summary>
        public MRUEntryMenuitem(String header)
        {
            this.Header = header;
        }

        #endregion // Constructor(s)

        #region CommandControl Methods

        /// <summary>
        /// 
        /// </summary>
        public override bool CanExecute(Object parameter)
        {
            return (true);
        }

        /// <summary>
        /// 
        /// </summary>
        public override void Execute(Object parameter)
        {
            Log.Log__Debug("MRUEntryMenuitem::Execute()");

            if (null == this.OpenService)
            {
                // We support having recently used files whose Open Service has
                // disappeared since it was opened.
                String msg = String.Format("File {0} cannot be opened because the plugin used to open it is no longer available.",
                    this.Filename);
                MessageService.Show(msg);
            }
            else
            {
                this.WorkbenchOpenService.OpenFileWithService(this.OpenService, this.Filename);
            }
        }

        #endregion // CommandControl Methods
    }

    #endregion // MRUEntryMenuitem Class

    #region Generic Recent Files Menu Classes

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.MenuFileRecentFiles, typeof(IWorkbenchCommand))]
    class ClearRecentFilesMenuItem : WorkbenchMenuItemBase
    {
        #region Constants

        public static readonly Guid GUID = new Guid("353F81F8-3327-4419-A739-2F3B7D2DD2D4");

        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// MEF import for message box service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.MessageService, typeof(Workbench.AddIn.Services.IMessageService))]
        public IMessageService MessageService { get; set; }
        #endregion // MEF Imports

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ClearRecentFilesMenuItem()
        {
            this.Header = "Clear Recent File List";
            this.ID = GUID;
            SetImageFromBitmap(Resources.Images.Delete);
        }
        #endregion // Constructor(s)

        #region IMenuItem Interface
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public override bool CanExecute(Object parameter)
        {
            return (true);
        }

        /// <summary>
        /// 
        /// </summary>
        public override void Execute(Object parameter)
        {
            if (MessageBoxResult.Yes == MessageService.Show("Are you sure you want to clear the recently used file list?", MessageBoxButton.YesNo))
            {
                RecentFileService.Clear();
                Log.Log__Message("MRU list cleared.");
            }
        }
        #endregion // IMenuItem Interface
    } // ClearRecentFilesMenuItem

    #endregion // Generic Recent Files Menu Classes
} // Workbench.UI.Menu namespace
