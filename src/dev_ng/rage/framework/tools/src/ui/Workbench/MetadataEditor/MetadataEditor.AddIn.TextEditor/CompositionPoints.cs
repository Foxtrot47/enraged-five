﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetadataEditor.AddIn.TextEditor
{

    /// <summary>
    /// 
    /// </summary>
    internal static class CompositionPoints
    {
        /// <summary>
        /// 
        /// </summary>
        public const String TextEditor =
            "4C04C1DA-57F8-4902-9BDA-61237FD346A0";

        /// <summary>
        /// 
        /// </summary>
        public const String XmlTreeViewer =
            "5E9B181A-94B2-4F9F-A529-8B5714CE9066";
    }

} // MetadataEditor.AddIn.TextEditor namespace
