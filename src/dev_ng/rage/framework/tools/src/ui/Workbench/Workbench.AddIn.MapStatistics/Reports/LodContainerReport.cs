﻿using RSG.Model.Report;
using RSG.Model.Report.Reports;

namespace Workbench.AddIn.MapStatistics.Reports
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Report.AddIn.ExtensionPoints.MapStatsReport, typeof(IReport))]
    public class LodContainerReport : RSG.Model.Report.Reports.Map.LodContainerReport
    {
    } // LodContainerValidationReport
} // Workbench.AddIn.MapStatistics.Reports
