﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Report.Reports.GameStatsReports.Tests;
using RSG.Model.Statistics.Captures;
using RSG.Base.Editor.Command;

namespace Workbench.AddIn.GameStatistics.Reports.Automated.ViewModels
{
    /// <summary>
    /// 
    /// </summary>
    public class MemorySmokeTestViewModel : SmokeTestViewModelBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public IList<MemoryResult> LatestResults
        {
            get { return m_latestResults; }
            set
            {
                SetPropertyValue(value, () => this.LatestResults,
                          new PropertySetDelegate(delegate(object newValue) { m_latestResults = (IList<MemoryResult>)newValue; }));
            }
        }
        private IList<MemoryResult> m_latestResults;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public MemorySmokeTestViewModel(MemorySmokeTest smokeTest, string testName)
            : base(smokeTest, testName)
        {
            if (smokeTest.LatestStats.ContainsKey(testName))
            {
                LatestResults = smokeTest.LatestStats[testName];
            }
        }
        #endregion // Constructor(s)
    } // MemorySmokeTestViewModel
}
