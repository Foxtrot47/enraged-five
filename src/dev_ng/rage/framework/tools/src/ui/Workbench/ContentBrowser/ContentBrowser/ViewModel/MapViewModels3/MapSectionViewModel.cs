﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using RSG.Model.Common.Map;
using RSG.Base.Collections;
using RSG.Model.Common;
using ContentBrowser.ViewModel.GridViewModels;
using System.Windows;
using System.ComponentModel;
using ContentBrowser.Util;
using RSG.Model.Asset;
using Workbench.AddIn.Services;
using RSG.Base.Tasks;

namespace ContentBrowser.ViewModel.MapViewModels3
{
    /// <summary>
    /// 
    /// </summary>
    public class MapSectionViewModel : ContainerViewModelBase, IWeakEventListener
    {
        #region Properties
        /// <summary>
        /// Section this view model is for.
        /// </summary>
        private IMapSection Section { get; set; }

        /// <summary>
        /// Thread synchronisation object.
        /// </summary>
        private object SyncObject { get; set; }

        /// <summary>
        /// Flag indicating whether the section has been loaded.
        /// </summary>
        private bool Created { get; set; }

        /// <summary>
        /// Task progress service for loading the sections stats.
        /// </summary>
        private ITaskProgressService TaskProgressService { get; set; }
        #endregion // Properties

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public MapSectionViewModel(IMapSection section, ITaskProgressService taskProgressService)
            : base(section)
        {
            Section = section;
            SyncObject = new object();
            Created = false;
            TaskProgressService = taskProgressService;

            PropertyChangedEventManager.AddListener(Section, this, "Archetypes");
            PropertyChangedEventManager.AddListener(Section, this, "ChildEntities");

            if (Section.Archetypes != null)
            {
                CollectionChangedEventManager.AddListener(Section.Archetypes, this);
            }

            if (Section.ChildEntities != null)
            {
                CollectionChangedEventManager.AddListener(Section.ChildEntities, this);
            }
        }
        #endregion // Constructor

        #region Overrides
        /// <summary>
        /// This gets called whenever the selection state for this view model
        /// has changed
        /// </summary>
        protected override void OnSelectionChanged(Boolean oldValue, Boolean newValue)
        {
            RequestSectionStats();
            RefreshGridViewModelsThreaded();
        }

        /// <summary>
        /// This gets called whenever the expansion state for this view model
        /// has changed
        /// </summary>
        protected override void OnExpansionChanged(Boolean oldValue, Boolean newValue)
        {
            RequestSectionStats();
            RefreshTreeViewModelsThreaded();
        }
        #endregion // Overrides

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void RequestSectionStats()
        {
            lock (SyncObject)
            {
                if (!Created)
                {
                    Created = true;
                    ITask actionTask = new ActionTask("Map Section Load", (context, progress) => Section.LoadAllStats());
                    TaskProgressService.Add(actionTask, new TaskContext(), TaskPriority.Background);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void RefreshGridViewModels()
        {
            GridAssets.BeginUpdate();
            GridAssets.Clear();
            if (IsSelected)
            {
                if (Section.Archetypes != null && Section.Archetypes.Count > 0)
                {
                    DummyAssetContainerGridViewModel vm = new DummyAssetContainerGridViewModel("Archetypes", this.Model);
                    vm.DefaultCommand = new ViewModelFactory.OpenContainerCommand(vm, true);
                    GridAssets.Add(vm);
                }

                if (Section.ChildEntities != null && Section.ChildEntities.Count > 0)
                {
                    GridAssets.Add(new DummyAssetContainerGridViewModel("Entities", null));
                }

                if ((Section.ChildEntities != null && Section.ChildEntities.Count > 0) ||
                    (Section.Archetypes != null && Section.Archetypes.Count > 0))
                {
                    GridAssets.Add(new DummyAssetContainerGridViewModel("Texture Dictionaries", null));
                }
            }
            GridAssets.EndUpdate();
        }

        /// <summary>
        /// 
        /// </summary>
        private void RefreshGridViewModelsThreaded()
        {
            if (Application.Current != null)
            {
                Application.Current.Dispatcher.Invoke(
                    new Action
                    (
                        delegate()
                        {
                            RefreshGridViewModels();
                        }
                    ),
                    System.Windows.Threading.DispatcherPriority.ContextIdle
                );
            }
            else
            {
                RefreshGridViewModels();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void RefreshTreeViewModels()
        {
            if (this.AssetChildren == null)
            {
                this.AssetChildren = new ObservableCollection<IAssetViewModel>();
            }

            this.AssetChildren.BeginUpdate();
            this.AssetChildren.Clear();

            if (!this.IsExpanded || this.Section == null)
            {
                AssetChildren.Add(new AssetDummyViewModel());
                AssetChildren.EndUpdate();
                return;
            }

            if (this.Section.Archetypes == null && this.Section.ChildEntities == null)
            {
                this.AssetChildren.Add(new AssetDummyViewModel("loading..."));
                AssetChildren.EndUpdate();
                return;
            }

            bool createTextures = false;
            bool createArchetypes = false;
            bool createEntities = false;
            if (this.Section.Archetypes != null && this.Section.Archetypes.Count > 0)
            {
                createArchetypes = true;
                createTextures = true;
            }

            if (this.Section.ChildEntities != null && this.Section.ChildEntities.Count > 0)
            {
                createEntities = true;
                createTextures = true;
            }

            if (createArchetypes)
            {
                try
                {
                    this.AssetChildren.Add(
                        new MapSectionArchetypeGroupViewModel(this.Section));
                }
                catch
                {
                }
            }

            if (createEntities)
            {
                try
                {
                    this.AssetChildren.Add(
                        new MapSectionEntityGroupViewModel(this.Section));
                }
                catch
                {
                }
            }

            if (createTextures)
            {
                try
                {
                    this.AssetChildren.Add(
                        new TextureDictionaryGroupViewModel(this.Section));
                }
                catch
                {
                }
            }
            
            this.AssetChildren.EndUpdate();
        }

        /// <summary>
        /// 
        /// </summary>
        private void RefreshTreeViewModelsThreaded()
        {
            if (Application.Current != null)
            {
                Application.Current.Dispatcher.Invoke(
                    new Action
                    (
                        delegate()
                        {
                            RefreshTreeViewModels();
                        }
                    ),
                    System.Windows.Threading.DispatcherPriority.ContextIdle
                );
            }
            else
            {
                RefreshTreeViewModels();
            }
        }
        #endregion // Private Methods

        #region IWeakEventListener Implementation
        /// <summary>
        /// 
        /// </summary>
        public bool ReceiveWeakEvent(Type managerType, Object sender, EventArgs e)
        {
            if (managerType == typeof(PropertyChangedEventManager))
            {
                PropertyChangedEventArgs eventArgs = e as PropertyChangedEventArgs;
                if (eventArgs.PropertyName == "Archetypes")
                {
                    if (Section.Archetypes != null)
                    {
                        CollectionChangedEventManager.AddListener(Section.Archetypes, this);
                    }

                    RefreshTreeViewModelsThreaded();
                    RefreshGridViewModelsThreaded();
                }
                else if (eventArgs.PropertyName == "ChildEntities")
                {
                    if (Section.ChildEntities != null)
                    {
                        CollectionChangedEventManager.AddListener(Section.ChildEntities, this);
                    }

                    RefreshTreeViewModelsThreaded();
                    RefreshGridViewModelsThreaded();
                }
            }
            else if (managerType == typeof(CollectionChangedEventManager))
            {
                if (sender == Section.Archetypes && (Section.Archetypes.Count == 0 || archetypeOldCount == 0))
                {
                    archetypeOldCount = Section.Archetypes.Count;
                    RefreshTreeViewModelsThreaded();
                    RefreshGridViewModelsThreaded();
                }
                else if (sender == Section.ChildEntities && (Section.ChildEntities.Count == 0 || entityOldCount == 0))
                {
                    entityOldCount = Section.ChildEntities.Count;
                    RefreshTreeViewModelsThreaded();
                    RefreshGridViewModelsThreaded();
                }
            }

            return true;
        }

        private int archetypeOldCount = 0;
        private int entityOldCount = 0;
        #endregion // IWeakEventListener
    } // MapSectionViewModel
}
