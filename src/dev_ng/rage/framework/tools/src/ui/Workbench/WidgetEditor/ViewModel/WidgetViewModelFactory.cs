﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using RSG.Base.Collections;
using ragCore;
using ragWidgets;
using System.Diagnostics;
using RSG.Base.Editor.Command;

namespace WidgetEditor.ViewModel
{

    public class WidgetViewModelFactory
    {

        public static WidgetViewModel CreateViewModel( Widget w )
        {
            if (w is WidgetText)
            {

            }
            else if ( w is WidgetToggle )
            {
                return new WidgetToggleViewModel( (WidgetToggle)w );
            }
            else if ( w is WidgetGroupBase )
            {
                return new WidgetGroupViewModel( (WidgetGroupBase)w );
            }
            else if ( w is WidgetText )
            {
                return new WidgetTextViewModel( (WidgetText)w );
            }
            else if ( w is WidgetAngle )
            {
                return new WidgetAngleViewModel( (WidgetAngle)w );
            }

            return new WidgetViewModel(w);
        }
    }
}
