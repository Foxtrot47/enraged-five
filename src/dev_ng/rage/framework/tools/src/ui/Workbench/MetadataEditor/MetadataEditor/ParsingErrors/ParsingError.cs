﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RSG.Base.Editor;

namespace MetadataEditor.ParsingErrors
{
    public class ParsingError : ModelBase
    {
        #region Members

        private String m_name;

        #endregion // Members

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public String Name
        {
            get { return m_name; }
            set
            {
                SetPropertyValue(value, () => this.Name,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_name = (String)newValue;
                        }
                ));
            }
        }

        #endregion // Properties
        
        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        public ParsingError(String name)
        {
            this.Name = name;
        }

        #endregion // Constructors
    } // ParsingError

    /// <summary>
    /// Represents the error where the same enum definition
    /// (i.e the same data type) was located in more than one file
    /// </summary>
    public class DuplicateEnumError : ParsingError
    {
        #region Members
        
        private ObservableCollection<String> m_fileLocations;

        #endregion // Members

        #region Properties
        
        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<String> FileLocations
        {
            get { return m_fileLocations; }
            set
            {
                SetPropertyValue(value, () => this.FileLocations,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_fileLocations = (ObservableCollection<String>)newValue;
                        }
                ));
            }
        }

        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        public DuplicateEnumError(String name, IEnumerable<String> fileLocations)
            : base(name)
        {
            this.FileLocations = new ObservableCollection<String>();
            foreach (String location in fileLocations)
            {
                this.FileLocations.Add(location);
            }
        }

        #endregion // Constructors
    } // DuplicateEnumError

    /// <summary>
    /// Represents the error where the same enum definition
    /// (i.e the same data type) was located in more than one file
    /// </summary>
    public class DuplicateStructError : ParsingError
    {
        #region Members

        private ObservableCollection<String> m_fileLocations;

        #endregion // Members

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<String> FileLocations
        {
            get { return m_fileLocations; }
            set
            {
                SetPropertyValue(value, () => this.FileLocations,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_fileLocations = (ObservableCollection<String>)newValue;
                        }
                ));
            }
        }

        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        public DuplicateStructError(String name, IEnumerable<String> fileLocations)
            : base(name)
        {
            this.FileLocations = new ObservableCollection<String>();
            foreach (String location in fileLocations)
            {
                this.FileLocations.Add(location);
            }
        }

        #endregion // Constructors
    } // DuplicateEnumError

    /// <summary>
    /// Represents the error where the same enum definition
    /// (i.e the same data type) was located in more than one file
    /// </summary>
    public class FileErrors : ParsingError
    {
        #region Members

        private ObservableCollection<String> m_fileErrors;

        #endregion // Members

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<String> Errors
        {
            get { return m_fileErrors; }
            set
            {
                SetPropertyValue(value, () => this.Errors,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_fileErrors = (ObservableCollection<String>)newValue;
                        }
                ));
            }
        }

        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        public FileErrors(String name)
            : base(name)
        {
            this.Errors = new ObservableCollection<String>();
        }

        #endregion // Constructors
    } // DuplicateEnumError
} // MetadataEditor.ParsingErrors
