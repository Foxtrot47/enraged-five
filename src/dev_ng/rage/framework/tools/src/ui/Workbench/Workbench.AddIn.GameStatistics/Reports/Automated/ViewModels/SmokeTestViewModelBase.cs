﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using RSG.Model.Report.Reports.GameStatsReports.Tests;

namespace Workbench.AddIn.GameStatistics.Reports.Automated.ViewModels
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class SmokeTestViewModelBase : ViewModelBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public string SmokeTestName
        {
            get { return m_smokeTestName; }
            set
            {
                SetPropertyValue(value, () => this.SmokeTestName,
                       new PropertySetDelegate(delegate(Object newValue) { m_smokeTestName = (string)newValue; }));
            }
        }
        private string m_smokeTestName;

        /// <summary>
        /// 
        /// </summary>
        public string TestResultName
        {
            get { return m_testResultName; }
            set
            {
                SetPropertyValue(value, () => this.TestResultName,
                       new PropertySetDelegate(delegate(Object newValue) { m_testResultName = (string)newValue; }));
            }
        }
        private string m_testResultName;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        protected SmokeTestViewModelBase(string smokeTestName)
            : base()
        {
            SmokeTestName = smokeTestName;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="smokeTest"></param>
        protected SmokeTestViewModelBase(ISmokeTest smokeTest, string testResultName)
            : this(smokeTest.SmokeTestName)
        {
            m_testResultName = testResultName;
        }
        #endregion // Constructor(s)
    } // SmokeTestViewModelBase
}
