﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Data;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using RSG.Base.ConfigParser;
using RSG.Base.Configuration;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using RSG.Base.Logging;
using Tasks = RSG.Base.Tasks;
using RSG.Base.Windows.Dialogs;
using RSG.Interop.Bugstar.Organisation;
using RSG.Pipeline.Automation.Common.Client;
using RSG.Pipeline.Automation.Common.Export;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Tasks;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Automation.Common.Utils;
using RSG.Pipeline.Automation.Consumers;
using Workbench.AddIn.Services;
using Workbench.AddIn.Services.File;
using PerforceBrowser.AddIn;
using RSG.SourceControl.Perforce;
using System.Collections.Specialized;
using RSG.Pipeline.Automation.Common;
using Ionic.Zip;
using P4API;
using RSG.Platform;
using WidgetEditor.AddIn;
using RSG.Pipeline.Automation.Common.Services.Messages;

namespace Workbench.AddIn.Automation.UI
{

    /// <summary>
    /// Automation Monitor top-level view model.
    /// </summary>
    internal sealed class AutomationMonitorViewModel : ViewModelBase
    {
        #region Properties
        /// <summary>
        /// Available automation services (from automation configuration data).
        /// </summary>
        public ObservableCollection<AutomationServiceViewModel> AutomationServices
        {
            get;
            private set;
        }

        /// <summary>
        /// Attached automation service.
        /// </summary>
        public AutomationServiceViewModel Service
        {
            get { return m_Service; }
            set
            {
                SetPropertyValue(value, () => this.Service,
                    new PropertySetDelegate(delegate(Object newValue) { m_Service = (AutomationServiceViewModel)newValue; }));

                if (m_Service!=null)
                {
                    Settings.LastUsedService = m_Service.FriendlyName + ";" + m_Service.AutomationServiceConnection.AbsoluteUri;
                    Settings.Apply();
                }
            }
        }
        private AutomationServiceViewModel m_Service;
        
        /// <summary>
        /// The visual indicator of when the monitor was last refreshed. 
        /// </summary>
        public string LastUpdated
        {
            get { return m_lastUpdated; }
            set
            {
                SetPropertyValue(value, () => this.LastUpdated,
                    new PropertySetDelegate(delegate(Object newValue) { m_lastUpdated = newValue.ToString(); }));
            }
        }
        private string m_lastUpdated;

        /// <summary>
        /// Task view models (per automation task available on the connected service).
        /// </summary>
        public ObservableCollection<AutomationTabViewModelBase> Tabs
        {
            get { return m_Tabs; }
            private set
            {
                SetPropertyValue(value, () => this.Tabs,
                    new PropertySetDelegate(delegate(Object newValue) { m_Tabs = (ObservableCollection<AutomationTabViewModelBase>)newValue; }));
            }
        }
        private ObservableCollection<AutomationTabViewModelBase> m_Tabs;

        /// <summary>
        /// List of studios.
        /// </summary>
        public ObservableCollection<IStudio> Studios
        {
            get { return m_studios; }
            set
            {
                SetPropertyValue(value, () => this.Studios,
                    new PropertySetDelegate(delegate(Object newValue) { m_studios = (ObservableCollection<IStudio>)newValue; }));
            }
        }
        private ObservableCollection<IStudio> m_studios;

        /// <summary>
        /// List of studios.
        /// </summary>
        public bool OnlyMyCompleted
        {
            get { return m_onlyMyCompleted; }
            set
            {
                SetPropertyValue(ref this.m_onlyMyCompleted, value, "OnlyMyCompleted");
            }
        }
        private bool m_onlyMyCompleted;


        /// <summary>
        /// The currently selected studio.
        /// </summary>
        public IStudio SelectedStudio
        {
            get { return m_selectedStudio; }
            set
            {
                SetPropertyValue(value, () => this.SelectedStudio,
                                    new PropertySetDelegate(delegate(Object newValue) 
                                        { 
                                            m_selectedStudio = (IStudio)newValue;
                                            RefreshServices();
                                            Refresh();
                                        }));
            }
        }

        private IStudio m_selectedStudio;


        /// <summary>
        /// Selected automation tab view model (may be null).
        /// </summary>
        public AutomationTabViewModelBase SelectedTab
        {
            get
            {
                if (this.Tabs == null)
                {
                    return null;
                }

                return (this.Tabs.Where(t => t.IsSelected).FirstOrDefault());
            }
            set
            {
                AutomationTaskViewModel oldSelection = m_selectedTab as AutomationTaskViewModel;
                if (oldSelection != null)
                {
                    oldSelection.Jobs.CollectionChanged -= SelectedTabJobsCollectionChanged;
                    foreach (var job in oldSelection.Jobs)
                    {
                        job.PropertyChanged -= SelectedTabJobPropertyChanged;
                    }
                }

                m_selectedTab = value;
                this.OnPropertyChanged("SelectedJobsCompleted");
                this.OnPropertyChanged("SelectedJobsCompletedOrErrored");
                AutomationTaskViewModel newSelection = value as AutomationTaskViewModel;
                if (newSelection == null)
                {
                    return;
                }

                newSelection.Jobs.CollectionChanged += SelectedTabJobsCollectionChanged;
                foreach (var job in newSelection.Jobs)
                {
                    job.PropertyChanged += SelectedTabJobPropertyChanged;
                }
            }
        }
        private AutomationTabViewModelBase m_selectedTab;
        
        /// <summary>
        /// Gets called when the collection of jobs on the tab that is currently selected
        /// have changed.
        /// </summary>
        /// <param name="sender">
        /// The collection this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Collections.Specialized.NotifyCollectionChangedEventArgs data for
        /// this event.
        /// </param>
        private void SelectedTabJobsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            this.OnPropertyChanged("SelectedJobsCompleted");
            this.OnPropertyChanged("SelectedJobsCompletedOrErrored");
            if (e.OldItems != null)
            {
                foreach (var oldItem in e.OldItems)
                {
                    AutomationJobViewModel job = oldItem as AutomationJobViewModel;
                    if (job == null)
                    {
                        continue;
                    }

                    job.PropertyChanged -= SelectedTabJobPropertyChanged;
                }
            }

            if (e.NewItems != null)
            {
                foreach (var newItem in e.NewItems)
                {
                    AutomationJobViewModel job = newItem as AutomationJobViewModel;
                    if (job == null)
                    {
                        continue;
                    }

                    job.PropertyChanged += SelectedTabJobPropertyChanged;
                }
            }
        }

        /// <summary>
        /// Gets called if a property on a job that is currently located inside the
        /// selected tab changes.
        /// </summary>
        /// <param name="sender">
        /// The job object this event handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.ComponentModel.PropertyChangedEventArgs data for this event.
        /// </param>
        private void SelectedTabJobPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            this.OnPropertyChanged("SelectedJobsCompleted");
            this.OnPropertyChanged("SelectedJobsCompletedOrErrored");
        }

        /// <summary>
        /// Get a value indicating whether all jobs in the currently selected tab
        /// are complete.
        /// </summary>
        public bool SelectedJobsCompleted
        {
            get
            {
                AutomationTaskViewModel taskVM = this.SelectedTab as AutomationTaskViewModel;
                if (taskVM == null)
                {
                    return false;
                }

                bool result = false;
                foreach (var job in taskVM.SelectedJobs)
                {
                    if (job.Job.State == JobState.Completed)
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                        break;
                    }
                }

                return result;
            }
        }

        /// <summary>
        /// Gets a value indicating whether there are any selected jos that are completed
        /// or have finished downloading with errors.
        /// </summary>
        public bool SelectedJobsCompletedOrErrored
        {
            get
            {
                AutomationTaskViewModel taskVM = this.SelectedTab as AutomationTaskViewModel;
                if (taskVM == null)
                {
                    return false;
                }

                IEnumerable<AutomationJobViewModel> selectedJobs =
                    taskVM.SelectedJobs.Where(j =>
                        (JobState.Completed == j.Job.State) || (JobState.Errors == j.Job.State));

                if (selectedJobs.Count() <= 0)
                {
                    return false;
                }

                return true;
            }
        }

        /// <summary>
        /// Automation Task View Models View.
        /// </summary>
        public ICollectionView TabsView
        {
            get { return m_TabsView; }
            private set
            {
                SetPropertyValue(value, () => this.TabsView,
                    new PropertySetDelegate(delegate(Object newValue) { m_TabsView = (ICollectionView)newValue; }));
            }
        }
        private ICollectionView m_TabsView;

        public string FilterUsername
        {
            get { return this.filterUsername; }
            set
            {
                if (this.SetPropertyValue(ref this.filterUsername, value, "FilterUsername"))
                {
                    if (this.Tabs != null)
                    {
                        foreach (AutomationTabViewModelBase tab in this.Tabs)
                        {
                            tab.FilterUsername(this.filterUsername);
                        }
                    }
                }
            }
        }
        private string filterUsername;

        /// <summary>
        /// 
        /// </summary>
        public System.Windows.Media.Color OwnerColor { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public System.Windows.Media.Color OwnerCompletedColor { get; private set; }
        #endregion // Properties

        #region Member Data

        /// <summary>
        /// Workbench configuration service.
        /// </summary>
        private IConfigurationService ConfigurationService;

        /// <summary>
        /// Workbench Open Service (for opening Universal Log files).
        /// </summary>
        private IWorkbenchOpenService WorkbenchOpenService;

        /// <summary>
        /// Workbench Universal Log Open Service.
        /// </summary>
        private IOpenService ULogOpenService;

        /// <summary>
        /// Automation monitor settings.
        /// </summary>
        private IAutomationMonitorSettings Settings;

        /// <summary>
        /// Proxy service for checking for game connections
        /// </summary>
        private IProxyService ProxyService;

        /// <summary>
        /// Bugstar service for getting users/project.
        /// </summary>
        private IBugstarService BugstarService;

        /// <summary>
        /// Progress service.
        /// </summary>
        private ITaskProgressService TaskProgressService;

        #endregion // Member Data

        #region Constants

        // Pending changelist for zip, xml and other export network files.
        private static readonly String NetworkExportChangelistDescription = "Network export changes";

        #endregion

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public AutomationMonitorViewModel(ICoreServiceProvider coreServices,
            IOpenService ulogOpenService, IAutomationMonitorSettings settings)
        {
            this.ConfigurationService = coreServices.ConfigurationService;
            this.WorkbenchOpenService = coreServices.OpenService;
            this.ULogOpenService = ulogOpenService;
            this.Settings = settings;
            this.ProxyService = coreServices.ProxyService;
            this.BugstarService = coreServices.BugstarService;
            this.TaskProgressService = coreServices.TaskProgressService;

            m_studios = new ObservableCollection<IStudio>();
            foreach (var studio in ConfigurationService.Config.Studios)
            {
                Studios.Add(studio);
            }

            m_selectedStudio = ConfigurationService.Config.Studios.ThisStudio;

            AutomationServices = new ObservableCollection<AutomationServiceViewModel>();

            RefreshServices();

            this.OwnerColor = System.Windows.Media.Color.FromArgb(100, 0, 128, 0);
            this.OwnerCompletedColor = System.Windows.Media.Color.FromArgb(100, 0, 200, 0);

            LastUpdated = "Ready.";
        }

        /// <summary>
        /// Refresh the services list.
        /// </summary>
        private void RefreshServices()
        {
            AutomationServices.Clear();

            foreach (var parameters in ConfigurationService.AutomationCoreServices.GetAllParameters(SelectedStudio))
            {
                AutomationServices.Add(new AutomationServiceViewModel(parameters));
            }

            bool setFirstOrDefault = true;

            if (!String.IsNullOrEmpty(Settings.LastUsedService))
            {
                string[] split = Settings.LastUsedService.Split(";".ToCharArray());
                string friendlyName = split[0];
                string uri = split[1];

                var found = AutomationServices.FirstOrDefault(
                    (AutomationServiceViewModel vm) =>
                    {
                        return vm.FriendlyName == friendlyName && vm.AutomationServiceConnection.AbsoluteUri == uri;
                    }
                );
                if (found != null)
                {
                    this.Service = found;
                    setFirstOrDefault = false;
                }
            }

            if (setFirstOrDefault)
            {
                this.Service = AutomationServices.FirstOrDefault();
            }
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Refresh task status from automation service.
        /// </summary>
        public void Refresh()
        {
            LastUpdated = "Refreshing...";
            // Kick this off as a background task.
            Tasks.ITask task = new Tasks.ActionTask("Refresh Automation Monitor View Model", (context, progress) => this.RefreshAction(context));
            task.OnTaskCompleted += OnRefreshTaskCompleted;
            TaskProgressService.Add(task, new Tasks.TaskContext(), Workbench.AddIn.Services.TaskPriority.Background);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void RefreshAction(Tasks.ITaskContext context)
        {
            List<AutomationTabViewModelBase> tabs = new List<AutomationTabViewModelBase>();


            if (this.Service != null)
            {
                AutomationAdminConsumer consumer = new AutomationAdminConsumer(
                    this.Service.AutomationServiceConnection);
                IEnumerable<TaskStatus> statuses = consumer.Monitor();
                IEnumerable<WorkerStatus> clientStatus = consumer.Clients();

                IEnumerable<AutomationTaskViewModel> statusVMs = statuses.Select(s =>
                    new AutomationTaskViewModel(this.BugstarService.ProjectUsers, s));
                AutomationClientsViewModel clientVM = new AutomationClientsViewModel(clientStatus);

               

                tabs.AddRange(statusVMs);
                tabs.Add(clientVM);
            }

            context.ReturnValue = tabs;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnRefreshTaskCompleted(object sender, Tasks.TaskCompletedEventArgs e)
        {
            List<AutomationTabViewModelBase> tabs = (List<AutomationTabViewModelBase>)e.ReturnValue;

 //           this.FilterUsername = string.Empty;

            IEnumerable<Guid> downloadedjobIds = Enumerable.Empty<Guid>();
            IEnumerable<Guid> selectedjobIds = Enumerable.Empty<Guid>();

            if (this.Tabs != null)
            {
                downloadedjobIds = from t in this.Tabs.OfType<AutomationTaskViewModel>()
                                   from j in t.Jobs
                                   where j.IsDownloaded == true
                                   select j.Job.ID;

                selectedjobIds = from t in this.Tabs.OfType<AutomationTaskViewModel>()
                                   from j in t.Jobs
                                   where j.IsSelected == true
                                   select j.Job.ID;
            }

            HashSet<Guid> downloadedHashSet = new HashSet<Guid>(downloadedjobIds);
            HashSet<Guid> selectedHashSet = new HashSet<Guid>(selectedjobIds);

            this.Tabs = new ObservableCollection<AutomationTabViewModelBase>(tabs);

            if (this.Tabs != null)
            {
                foreach (AutomationTaskViewModel taskVm in this.Tabs.OfType<AutomationTaskViewModel>())
                {
                    foreach (AutomationJobViewModel job in taskVm.Jobs)
                    {
                        if (downloadedHashSet.Contains(job.Job.ID))
                        {
                            job.IsDownloaded = true;
                        }

                        if (selectedHashSet.Contains(job.Job.ID))
                        {
                            job.IsSelected = true;
                        }
                    }
                }
            }

            if (this.Tabs != null)
            {
                foreach (AutomationTabViewModelBase tab in this.Tabs)
                {
                    if (this.OnlyMyCompleted)
                    {
                        tab.FilterUsername(true);
                    }
                    else if (!string.IsNullOrWhiteSpace(this.FilterUsername))
                    {
                        tab.FilterUsername(this.FilterUsername);
                    }
                }
            }

            this.TabsView = new CollectionView(this.Tabs);
            this.SelectedTab = this.Tabs.FirstOrDefault();

            LastUpdated = String.Format("Last Updated : {0}", DateTime.Now.ToLocalTime().ToString());
        }

        /// <summary>
        /// Attempts to Download the file from the server, then copies from the workbench cache into
        /// the correct location and removes the version stored in the cache
        /// </summary>
        /// <param name="job"></param>
        /// <param name="relFilename"></param>
        /// <param name="downFilename"></param>
        /// <param name="destFilename"></param>
        /// <returns></returns>
        public bool DownloadCopyAndDeleteFile(FileTransferServiceConsumer ftConsumer, MapExportJob job, String relFilename, String downFilename, String destFilename)
        {
            Byte[] md5;
            bool fileResult = ftConsumer.DownloadFile(job, relFilename, Path.GetFileName(downFilename), out md5);
            if (!fileResult)
            {
                Log.Log__Error("Error downloading export file: {0} for job {1}.  MD5: {2}.", 
                    relFilename, job.ID, RSG.Base.IO.FileMD5.Format(md5));
            }
            else
            {
                try
                {
                    File.Copy(downFilename, destFilename, true);
                    File.Delete(downFilename);
                }
                catch (Exception ex)
                {
                    Log.Log__Error("Exception in job {0} while copying or deleting file: {1} to {2}: {3}", job.ID, downFilename, destFilename, ex.Message);
                    fileResult &= false;
                }
            }
            return fileResult;
        }
        
        /// <summary>
        /// Download exported zip and xml, and built rpfs into build directory.
        /// </summary>
        /// <param name="jobs"></param>
        public void DownloadExportAndBuildData(IEnumerable<AutomationJobViewModel> jobs)
        {
            string ids = GetJobIds(jobs);
            ResetDownloadFlags(jobs);

            ProxyService.RefreshGameConnections();
            if (ProxyService.IsGameConnected)
            {
                Log.Log__Error("Download of Job {0} export and build files abandoned - the game is running!", ids);
                MessageBox.Show(String.Format("Download of Job {0} export and build files abandoned - the game is running!",
                    jobs.First().Job.ID));
                return;
            }

            CancellationTokenSource cts = new CancellationTokenSource();
            Tasks.ITaskContext context = new Tasks.TaskContext(cts.Token, jobs);
            Tasks.ITask downloadDataTask = new Tasks.ActionTask("Downloading Export and Build Files",
                (ctx, progress) => DownloadExportAndBuildDataAction(ctx, progress));
            downloadDataTask.ReportsProgress = true;    

            TaskExecutionDialog dialog = new TaskExecutionDialog();
            TaskExecutionViewModel vm = new TaskExecutionViewModel("Downloading Export and Build Files",
                downloadDataTask, cts, context);
            vm.AutoCloseOnCompletion = true;
            dialog.DataContext = vm;
            dialog.ShowDialog();

            bool taskReturnCode = false;
            if (context.ReturnValue != null)
            {
                taskReturnCode = (bool)context.ReturnValue;
            }

            if (taskReturnCode)
            {
                if (jobs.Count() == 1)
                {
                    MessageBox.Show(String.Format("Successfully downloaded Job {0}.", ids), "Download Task");
                }
                else
                {
                    MessageBox.Show(String.Format("Successfully downloaded {0} jobs.  No errors have occurred.", jobs.Count()), "Download Tasks");
                }
                
                SetDownloadedState(jobs);
            }
            else
            {
                ResetDownloadFlags(jobs);

                if (jobs.Count() == 1)
                {
                    string error = String.Format("Error downloading Job {0} Export and Build Files.  Refer to the Universal Log Output window for more information.", ids);
                    MessageBox.Show(error, "Download Tasks");
                    Log.Log__Error(error);
                }
                else
                {
                    string error = String.Format("Errors have occurred downloading {0} jobs.  Refer to the Universal Log Output window for more information.", jobs.Count());
                    MessageBox.Show(error, "Download Tasks");
                    Log.Log__Error(error);
                }
            }
        }

        /// <summary>
        /// Reset the downloaded flags.
        /// </summary>
        /// <param name="jobs">List of automation job view models.</param>
        private void ResetDownloadFlags(IEnumerable<AutomationJobViewModel> jobs)
        {
            foreach (var job in jobs)
            {
                job.IsDownloading = false;
                job.IsDownloaded = false;
            }
        }

        /// <summary>
        /// Set the downloaded state for a collection of jobs.
        /// </summary>
        /// <param name="jobs">List of automation job view models.</param>
        private void SetDownloadedState(IEnumerable<AutomationJobViewModel> jobs)
        {
            foreach (var job in jobs)
            {
                job.IsDownloaded = job.IsDownloading ? true : false;
                job.IsDownloading = false;
            }
        }

        /// <summary>
        /// Get a text comma separated value list of all the job IDs in the enumeration.
        /// </summary>
        /// <param name="jobs">Enumeration of job view models.</param>
        /// <returns>Text comma separated value list of all the job IDs in the enumeration.</returns>
        private string GetJobIds(IEnumerable<AutomationJobViewModel> jobs)
        {
            string ids = jobs.First().Job.ID.ToString();
            bool first = true;
            foreach (var job in jobs)
            {
                if (first)
                {
                    first = false; continue;
                }

                ids += ", " + job.Job.ID.ToString();
            }

            return ids;
        }
        
        /// <summary>
        /// Download universal log into Workbench cache.
        /// </summary>
        /// <param name="jobs"></param>
        public void DownloadUniversalLog(IEnumerable<AutomationJobViewModel> jobs)
        {
            try
            {
                if (jobs == null || jobs.Count() == 0)
                {
                    return;
                }

                CancellationTokenSource cts = new CancellationTokenSource();
                Tasks.ITaskContext context = new Tasks.TaskContext(cts.Token, jobs);
                Tasks.ITask downloadDataTask = new Tasks.ActionTask("Downloading Universal Log",
                    (ctx, progress) => DownloadUniversalLogAction(ctx, progress));
                downloadDataTask.ReportsProgress = true;

                TaskExecutionDialog dialog = new TaskExecutionDialog();
                TaskExecutionViewModel vm = new TaskExecutionViewModel("Download Universal Log",
                    downloadDataTask, cts, context);
                vm.AutoCloseOnCompletion = true;
                dialog.DataContext = vm;
                bool? result = dialog.ShowDialog();
                if (context.ReturnValue == null || !(context.ReturnValue is KeyValuePair<bool, String>))
                {
                    Log.Log__Error("Error downloading Job {0} Universal Log.", jobs.First().Job.ID);
                    MessageBox.Show(String.Format("Error downloading Job {0} Universal Log.",
                        jobs.First().Job.ID));
                }

                KeyValuePair<bool, String> taskReturnCode = (KeyValuePair<bool, String>)context.ReturnValue;

                if (!taskReturnCode.Key)
                {
                    Log.Log__Error("Error downloading Universal Log.");
                    MessageBox.Show("Error downloading Universal Log.");
                }
            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex, "Error downloading Universal Log.");
                MessageBox.Show("Error downloading Universal Log.");
            }
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Download exported zip and xml, and built rpfs into build directory 
        /// (background thread worker).
        /// </summary>
        /// <param name="context"></param>
        /// <param name="progress"></param>
        private void DownloadExportAndBuildDataAction(Tasks.ITaskContext context, Tasks.IProgress<Tasks.TaskProgress> progress)
        {
            Debug.Assert(null != context.Argument,
                "No argument for asynchronous download task.  Internal error.");
            if (null == context.Argument)
            {
                Log.Log__Error("No argument for asynchronous download task.  Internal error.");
                return;
            }

            context.ReturnValue = true;

            List<AutomationJobViewModel> jobViewModels;
            lock (context.Argument)
            {
                IEnumerable<AutomationJobViewModel> jobViewModelEnumerables = (context.Argument as IEnumerable<AutomationJobViewModel>);
                jobViewModels = new List<AutomationJobViewModel>(jobViewModelEnumerables);
            }
            
            double count = 0;

            foreach (var jobVm in jobViewModels)
            {
                jobVm.IsDownloaded = false;
                jobVm.IsDownloading = true;
                count++;
            }

            double increment = count > 0 ? 1 / count : 0;
            double progressTotal = 0;

            // Do not prompt if this is a map job - we only care about network export jobbies
            bool CopyZipAndBuild = false;
            foreach (var jobVm in jobViewModels)
            {
                if (jobVm.Job.Role == CapabilityType.CutsceneExportNetwork)
                {
                    if (MessageBox.Show("Download Zip and Build Cutscenes?", "Download Cutscenes", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                        CopyZipAndBuild = true;
                    break;
                }
            }

            List<string> cutsceneMissionPaths = new List<string>();
            
            foreach (var jobVm in jobViewModels)
            {
                IJob job = jobVm.Job;
                try
                {
                    progress.Report(new Tasks.TaskProgress(progressTotal, String.Format("Downloading Job '{0}'", job.ID)));
                    progressTotal += increment;

                    switch (job.Role)
                    {
                        case CapabilityType.CutscenePackage:
                            {
                                DownloadCutscenePackage(jobVm, job, context);
                            }
                            break;
                        case CapabilityType.CutsceneExportNetwork:
                            {
                                if (DownloadCutsceneExportNetwork(jobVm, job, CopyZipAndBuild, cutsceneMissionPaths) == false)
                                    context.ReturnValue = false;
                            }
                            break;
                        case CapabilityType.MapExportNetwork:
                            {
                                DownloadMapExportNetwork(jobVm, job, context);
                            }
                            break;
                        default:
                            {

                            }
                            break;
                    }
                }
                catch (Exception e)
                {
                    Log.Log__Exception(e, "An error has occurred downloading from job " + job.ID + "." );
                    context.ReturnValue = false;
                }
            }

            if (CopyZipAndBuild && cutsceneMissionPaths.Count > 0)
            {
                // Kick off an ap3 build - pass all mission folders to build
                string folders = String.Join(" ", cutsceneMissionPaths);

                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.Arguments = folders;
                startInfo.FileName = Path.Combine(this.ConfigurationService.Config.ToolsRoot, "ironlib", "lib", "RSG.Pipeline.Convert.exe");
                startInfo.UseShellExecute = false;

                Process proc = Process.Start(startInfo);
                proc.WaitForExit();
            }

            progress.Report(new Tasks.TaskProgress(1.0f, "Complete."));
        }

        private void MakeDirectoryAndContentsWritable(string directory)
        {
            DirectoryInfo dirInfo = new DirectoryInfo(directory);
            foreach (FileSystemInfo fileSystemInfo in dirInfo.GetFileSystemInfos())
            {
                if (fileSystemInfo.Attributes.HasFlag(FileAttributes.ReadOnly))
                    fileSystemInfo.Attributes = fileSystemInfo.Attributes & (~FileAttributes.ReadOnly);
            }

            foreach (DirectoryInfo subDirectoryInfo in dirInfo.GetDirectories())
                MakeDirectoryAndContentsWritable(subDirectoryInfo.FullName);
        }

        /// <summary>
        /// Touches all files in the directory.
        /// </summary>
        /// <param name="directory"></param>
        private void TouchFiles(string directory)
        {
            string[] files = Directory.GetFiles(directory, "*.*", SearchOption.AllDirectories);

            foreach (string file in files)
            {
                File.SetLastWriteTimeUtc(file, DateTime.UtcNow);
            }
        }

        /// <summary>
        /// Reconciles the given directory.  
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="directory"></param>
        private void ReconcileDirectories(RSG.SourceControl.Perforce.P4 p4, string directory)
        {
            List<string> edited_files = new List<string>();

            string p4Directory = directory + "\\...";
            P4RecordSet reconcileRecord = p4.Run("reconcile", p4Directory);
            foreach (string warning in reconcileRecord.Warnings)
            {
                if (warning.Contains("no file(s) to reconcile.") == true)
                {
                    Log.Log__Message("No files have changed since downloading {0}.  No files related to this scene will be checked out.", Path.GetFileName(directory));
                }
            }

            p4.Run("reopen", "-t binary+l", p4Directory + ".anim", p4Directory + ".clip");
            p4.Run("reopen", "-t text", p4Directory + ".cutxml", p4Directory + ".txt", p4Directory + ".cutlist", p4Directory + ".cutpart", p4Directory + ".xml");
        }

        /// <summary>
        /// Finds all pending changelists for the user with the given description and deletes them if empty
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="changelistDescription"></param>
        private void DeleteEmptyPendingChangelists(RSG.SourceControl.Perforce.P4 p4, string changelistDescription)
        {
            if (changelistDescription.Equals(String.Empty))
                return;

            String[] findPendingChangelistsArgs = new String[] { "-l", "-s", "pending", "-u", p4.User };
            P4RecordSet pendingChangelistsRecord = p4.Run("changes", findPendingChangelistsArgs);

            List<String> networkExportChangelists = new List<String>();

            foreach (P4Record record in pendingChangelistsRecord.Records)
            {
                // Find any changelists that contain the changelistDescription and add to a list to
                // later check for 0 files in them.
                if (record.Fields.ContainsKey("desc") && record.Fields.ContainsKey("change"))
                {
                    string description = record.Fields["desc"].ToLower();
                    if (description.Contains(changelistDescription.ToLower()))
                    {
                        networkExportChangelists.Add(record.Fields["change"]);
                    }
                }
            }

            // Now check the matching changelists to see if there are 0 files in them, and delete if so.
            if (networkExportChangelists.Any())
            {
                P4RecordSet pendingNetworkExportChangelistRecords = p4.Run("describe", networkExportChangelists.ToArray());

                foreach (P4Record record in pendingNetworkExportChangelistRecords.Records)
                {
                    if (record.Fields.ContainsKey("change"))
                    {
                        // If no depotFiles in the record, the changelist is empty so delete
                        if (!record.ArrayFields.ContainsKey("depotFile"))
                            p4.Run("change", new String[] { "-d", record.Fields["change"] });
                    }
                }
            }
        }

        /// <summary>
        ///   Get the cutscene directory from the job. This uses the scenes data.cutpart within the userdata to work out what the scene path is
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        ///
        private string GetCutsceneDirectory(IJob job)
        {
            string strFiles = (string)job.UserData;
            string[] files = strFiles.Split(',');

            for (int i = 0; i < files.Length; ++i)
            {
                if (files[i].Contains("data.cutpart"))
                {
                    return Path.GetDirectoryName(files[i]);
                }
            }

            return String.Empty;
        }

        /// <summary>
        ///   Get the cutscene files from the job. This uses the scenes userdata to work out.
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        ///
        private string[] GetFilesFromJob(IJob job)
        {
            string userData = (string)job.UserData;

            string filesMarker = ":files:";
            int tokenIndex = userData.IndexOf(filesMarker) + filesMarker.Length;
            string data = userData.Substring(tokenIndex, userData.Length - tokenIndex);
            return data.Split(',');
        }

        /// <summary>
        ///   Get the cutscene fbx file from the job files.
        /// </summary>
        /// <param name="files"></param>
        /// <returns></returns>
        ///
        private string GetFBXFileFromFiles(string[] files)
        {
            foreach (string file in files)
            {
                if(Path.GetExtension(file).ToLower() == ".fbx")
                    return file;
            }

            return String.Empty;
        }

        /// <summary>
        /// Download universal log into Workbench cache (background thread 
        /// worker).
        /// </summary>
        /// <param name="context"></param>
        /// <param name="progress"></param>
        private void DownloadUniversalLogAction(Tasks.ITaskContext context, Tasks.IProgress<Tasks.TaskProgress> progress)
        {
            try
            {
                Debug.Assert(null != context.Argument,
                    "No argument for asynchronous log download task.  Internal error.");
                if (null == context.Argument)
                {
                    Log.Log__Error("No argument for asynchronous log download task.  Internal error.");
                    return;
                }
                Debug.Assert(context.Argument is IEnumerable<AutomationJobViewModel>,
                    "Invalid argument for asynchronous log download task.  Internal error.");
                if (!(context.Argument is IEnumerable<AutomationJobViewModel>))
                {
                    Log.Log__Error("Invalid argument for asynchronous log download task.  Internal error.");
                    return;
                }

                IEnumerable<AutomationJobViewModel> jobViewModels =
                    (context.Argument as IEnumerable<AutomationJobViewModel>);
                foreach (var jobVm in jobViewModels)
                {
                    IJob job = jobVm.Job;

                    bool result = true;
                    FileTransferServiceConsumer ftConsumer = new FileTransferServiceConsumer(
                        this.ConfigurationService.Config,
                        this.Service.FileTransferServiceConnection);

                    String downloadDirectory = ftConsumer.GetClientFileDirectory(job);

                    if (!Directory.Exists(downloadDirectory))
                        Directory.CreateDirectory(downloadDirectory);

                    String filename = String.Format("job_{0}.ulog", job.ID);
                    String pathname = Path.Combine(downloadDirectory, filename);
                    Byte[] md5;
                    bool fileResult = ftConsumer.DownloadFile(job, "job.ulog", filename, out md5);
                    result &= fileResult;
                    context.ReturnValue = new KeyValuePair<bool, String>(result, pathname);
                    progress.Report(new Tasks.TaskProgress(1.0f, "Complete."));
                    Thread.Sleep(100);

                    if (result)
                    {
                        this.WorkbenchOpenService.OpenFileWithService(this.ULogOpenService,
                            pathname);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex, "Error downloading Universal Log.");
                MessageBox.Show("Error downloading Universal Log.");
            }
        }

        /// <summary>
        /// Handles the download operation for a Cutscene Package task.
        /// </summary>
        /// <param name="jobVm"></param>
        /// <param name="job"></param>
        /// <param name="context"></param>
        private void DownloadCutscenePackage(AutomationJobViewModel jobVm, IJob job, Tasks.ITaskContext context)
        {
            FileTransferServiceConsumer ftConsumer = new FileTransferServiceConsumer(
                this.ConfigurationService.Config,
                this.Service.FileTransferServiceConnection);

            RSG.Base.Logging.Universal.IUniversalLog log = LogFactory.CreateUniversalLog("Download Cutscene Package");

            String jobDirectory = ftConsumer.GetClientFileDirectory(job);
            if (!Directory.Exists(jobDirectory))
                Directory.CreateDirectory(jobDirectory);

            bool result = true;
            const string logContext = "Cutscene Package Download";

            const string cutsceneExportPath = "$(export)\\anim\\cutscene";
            const string cutsceneTargetPath = "$(target)\\anim\\cutscene";

            /*MessageBoxResult zipFileResult = MessageBox.Show("Would you like to download the cutscene export .zip files?", "Cutscene Package Download", MessageBoxButton.YesNoCancel);
            if (zipFileResult == MessageBoxResult.Cancel)
                return;
            
            bool downloadZipFiles = (zipFileResult == MessageBoxResult.Yes) ? true : false;

            if (downloadZipFiles == true)
            {
                string cutsceneDirectory = config.Project.DefaultBranch.Environment.Subst(cutsceneExportPath);
                cutsceneDirectory = cutsceneDirectory.Replace("/", "\\");
                
                RSG.SourceControl.Perforce.P4 p4 = this.PerforceService.PerforceConnection;
                P4RecordSet syncRecords = p4.Run("sync", Path.Combine(cutsceneDirectory, "..."));
                
                foreach(string error in syncRecords.Errors)
                {
                    //Treat these as a warning since we're not considering them fatal.
                    Log.Log__WarningCtx(logContext, error);
                }

                const String exportZipName = "export.zip";
                String downloadFilename = Path.Combine(jobDirectory, exportZipName);

                bool fileResult = ftConsumer.DownloadFile(job, exportZipName, Path.GetFileName(downloadFilename));
                if (!fileResult)
                {
                    jobVm.IsDownloading = false;
                    result = false;
                    Log.Log__ErrorCtx(logContext, "Error downloading file: {0} for job {1}.", exportZipName, job.ID);
                }
                else
                {
                    using (ZipFile zipFile = ZipFile.Read(downloadFilename))
                    {
                        Directory.CreateDirectory(cutsceneDirectory);
                        zipFile.ExtractAll(cutsceneDirectory, ExtractExistingFileAction.OverwriteSilently);
                    }
                }
            }*/

            IConfig config = this.ConfigurationService.Config;
            Dictionary<String, IProject> projectDict = new Dictionary<string, IProject>();

            List<RemoteFileInfo> jobFiles = ftConsumer.FileAvailability2(job).ToList();

            foreach (RemoteFileInfo info in jobFiles)
            {
                String[] splitPath = info.RelativePath.Split(Path.DirectorySeparatorChar);
                if(splitPath.Length > 1)
                {
                    String projectName = splitPath[0];
                    if (!projectDict.ContainsKey(projectName) && config.AllProjects().ContainsKey(projectName))
                    {
                        projectDict.Add(projectName, config.AllProjects()[projectName]);
                    }
                }
            }
            
            // download appropriate data for projects on system
            foreach (KeyValuePair<String, IProject> kvp in projectDict)
            {
                IBranch branch = kvp.Value.Branches[job.BranchName];

                String projectJobDirectory = Path.Combine(jobDirectory, branch.Project.Name);
                if (!Directory.Exists(projectJobDirectory))
                    Directory.CreateDirectory(projectJobDirectory);

                //Download all enabled resourced data.
                foreach (ITarget target in branch.Targets.Values)
                {
                    if (target.Enabled == false) continue;

					// Now converted to a relative path
                    String resourceZipname = Path.Combine(branch.Project.Name, target.Platform.ToString() + "resourced.zip");
                    String downloadFilename = Path.Combine(jobDirectory, resourceZipname);

                    Byte[] md5;
                    bool fileResult = ftConsumer.DownloadFile(job, resourceZipname, resourceZipname, out md5);
                    if (!fileResult)
                    {
                        jobVm.IsDownloading = false;
                        result = false;
                        Log.Log__ErrorCtx(logContext, "Error downloading file: {0} for job {1}.  MD5: {2}.",
                            resourceZipname, job.ID, RSG.Base.IO.FileMD5.Format(md5));
                    }
                    else
                    {
                        string cutsceneDirectory = cutsceneTargetPath.Replace("$(target)", target.ResourcePath);
                        using (ZipFile zipFile = ZipFile.Read(downloadFilename))
                        {
                            Directory.CreateDirectory(cutsceneDirectory);
                            zipFile.ExtractAll(cutsceneDirectory, ExtractExistingFileAction.OverwriteSilently);
                        }
                    }
                }
            }

            Thread.Sleep(100);
            context.ReturnValue = result;         
        }

        private IBranch GetProjectOrDlcConfig(IConfig config, string cutsceneDir)
        {
            cutsceneDir = cutsceneDir.ToLower().Replace("\\", "/");
            foreach (KeyValuePair<String, IProject> pair in config.DLCProjects)
            {
                if (cutsceneDir.StartsWith(pair.Value.Root.Replace("\\", "/").ToLower()))
                {
                    IBranch branch = pair.Value.Branches[pair.Value.DefaultBranchName];
                    // Get the branch for the dlc, we look for the core branch in the dlc. If this doesnt exist we drop back to its default branch name
                    if (pair.Value.Branches.ContainsKey(config.CoreProject.DefaultBranchName))
                    {
                        branch = pair.Value.Branches[config.CoreProject.DefaultBranchName];
                    }

                    return branch;
                }
            }

            return config.Project.DefaultBranch;
        }

        /// <summary>
        /// Handles the download operation fo rthe Cutscene Export Network task.
        /// </summary>
        /// <param name="jobVm"></param>
        /// <param name="job"></param>
        /// <param name="context"></param>
        private bool DownloadCutsceneExportNetwork(AutomationJobViewModel jobVm, IJob job, bool copyzip, List<string> cutsceneMissionPaths)
        {
            FileTransferServiceConsumer ftConsumer = new FileTransferServiceConsumer(
                this.ConfigurationService.Config,
                this.Service.FileTransferServiceConnection);
            
            String jobDirectory = ftConsumer.GetClientFileDirectory(job);
            if (!Directory.Exists(jobDirectory))
                Directory.CreateDirectory(jobDirectory);

            String relativeFilename = "export.zip";
            String downloadFilename = Path.Combine(jobDirectory, relativeFilename);

            string cutsceneDirectory = GetCutsceneDirectory(job);
            IBranch branch = GetProjectOrDlcConfig(this.ConfigurationService.Config, cutsceneDirectory);

            Byte[] md5;
            bool fileResult = ftConsumer.DownloadFile(job, relativeFilename, Path.GetFileName(downloadFilename), out md5);
            if (!fileResult)
            {
                jobVm.IsDownloading = false;
                Log.Log__Error("Error downloading file: {0} for job {1}.  MD5: {2}.", 
                    relativeFilename, job.ID, RSG.Base.IO.FileMD5.Format(md5));
                return false;
            }
            else
            {
                // 1) Force sync the directory
                // 2) Delete the directory
                // 3) Extract the files
                // 4) Reconcile the dir with p4
                using (RSG.SourceControl.Perforce.P4 p4 = 
                    this.ConfigurationService.Config.Project.SCMConnect())
                {
                    //Revert any scene data already checked out.
                    List<String> args = new List<String>();
                    args.Add(cutsceneDirectory + "\\...");
                    p4.Run("revert", args.ToArray());

                    //Cutscene files are marked as exclusive checkout.  Verify that it can be checked out.
                    args.Clear();
                    args.Add("-a");
                    args.Add(cutsceneDirectory + "\\...");
                    P4API.P4RecordSet p4recordSet = p4.Run("opened", args.ToArray());
                    if (p4recordSet.Records.Length > 0)
                    {
                        const string exclusiveCheckoutModifier = "l";
                        bool exclusiveCheckoutEnabled = false;
                        foreach (P4Record record in p4recordSet.Records)
                        {
                            if (record["type"].Contains("+"))
                            {
                                string[] filetype = record["type"].Split('+');
                                if (filetype[1].Contains(exclusiveCheckoutModifier))
                                {
                                    exclusiveCheckoutEnabled = true;
                                    break;
                                }
                            }
                        }

                        if (exclusiveCheckoutEnabled == true)
                        {
                            Log.Log__Error(String.Format("Export files for {0} are already checked out by user {1}.  Unable to download cutscene files.", Path.GetFileName(cutsceneDirectory), p4recordSet.Records[0]["user"]));
                            return false;
                        }
                    }

                    //Sync to the latest cutscene data.
                    args.Clear();
                    args.Add("-f");
                    args.Add(cutsceneDirectory + "\\...");
                    p4.Run("sync", args.ToArray());

                    MakeDirectoryAndContentsWritable(cutsceneDirectory);
                    Directory.Delete(cutsceneDirectory, true);

                    using (ZipFile zipFile = ZipFile.Read(downloadFilename))
                    {
                        Directory.CreateDirectory(cutsceneDirectory);
                        zipFile.ExtractAll(cutsceneDirectory);
                    }

                    TouchFiles(cutsceneDirectory);
                    ReconcileDirectories(p4, cutsceneDirectory);
                }
            }


            relativeFilename = "resourced.zip";
            downloadFilename = Path.Combine(jobDirectory, "resourced.zip");

            fileResult = fileResult && ftConsumer.DownloadFile(job, relativeFilename, Path.GetFileName(downloadFilename), out md5);
            if (!fileResult)
            {
                Log.Log__Error("Error downloading file: {0} for job {1}.  MD5: {2}.", 
                    relativeFilename, job.ID, RSG.Base.IO.FileMD5.Format(md5));
                return false;
            }
            else
            {
				// resouced.zip contains preview data

                // Extract into the preview directory
                string strPreviewDir = branch.Preview;
                if (!Directory.Exists(strPreviewDir))
                    Directory.CreateDirectory(strPreviewDir);

                using (ZipFile zipFile = ZipFile.Read(downloadFilename))
                {
                    zipFile.ExtractAll(strPreviewDir, ExtractExistingFileAction.OverwriteSilently);
                }
            }

            if (copyzip)
            {
                // Find the fbx file path in the job
                string[] files = GetFilesFromJob(job);
                string fbxFile = GetFBXFileFromFiles(files);

                // Work out the zip filename here for the job
                fbxFile = fbxFile.Replace("/", "\\");
                string[] outputPathtokens = fbxFile.Split(new string[] { "\\" }, StringSplitOptions.RemoveEmptyEntries);

                string strScene = Path.Combine(branch.Art, "animation", "cutscene", "!!scenes");
                strScene = strScene.Replace("/", "\\");
                string[] assetPathTokens = strScene.Split(new string[] { "\\" }, StringSplitOptions.RemoveEmptyEntries);

                string strMission = Path.Combine(branch.Export, "anim", "cutscene", outputPathtokens[assetPathTokens.Length]);
                string strZip = Path.Combine(strMission, Path.GetFileName(cutsceneDirectory) + ".icd.zip");

                if (Directory.Exists(strMission) == false)
                {
                    Directory.CreateDirectory(strMission);
                }

                if (File.Exists(strZip) == true)
                {
                    File.SetAttributes(strZip, FileAttributes.Normal);
                }

                relativeFilename = Path.GetFileName(strZip);
                downloadFilename = Path.Combine(jobDirectory, Path.GetFileName(strZip));

                fileResult = fileResult && ftConsumer.DownloadFile(job, relativeFilename, Path.GetFileName(downloadFilename), out md5);
                if (!fileResult)
                {
                    Log.Log__Error("Error downloading file: {0} for job {1}.  MD5: {2}.", 
                        relativeFilename, job.ID, RSG.Base.IO.FileMD5.Format(md5));
                    return false;
                }
                else
                {
                    if (File.Exists(strZip)) File.SetAttributes(strZip, FileAttributes.Normal);
                    File.Copy(downloadFilename, strZip, true);
                    cutsceneMissionPaths.Add(strMission);
                }
            }
            
            Thread.Sleep(100);
            return true;
        }

        /// <summary>
        /// Handles the map export network download function.
        /// </summary>
        /// <param name="jobVm"></param>
        /// <param name="job"></param>
        /// <param name="context"></param>
        private void DownloadMapExportNetwork(AutomationJobViewModel jobVm, IJob job, Tasks.ITaskContext context)
        {
            Debug.Assert(job is MapExportJob,
                                "Invalid argument for asynchronous download task.  Internal error.");
            if (!(job is MapExportJob))
            {
                jobVm.IsDownloading = false;
                Log.Log__Error("Invalid argument for asynchronous download task.  Internal error.");
                return;
            }

            bool result = true;
            MapExportJob mapJob = (MapExportJob)job;

            FileTransferServiceConsumer ftConsumer = new FileTransferServiceConsumer(
                this.ConfigurationService.Config,
                this.Service.FileTransferServiceConnection);

            try
            {
                using (RSG.SourceControl.Perforce.P4 p4 = this.ConfigurationService.Config.Project.SCMConnect())
                using (ConfigGameView gv = new ConfigGameView())
                {
                    String jobDirectory = ftConsumer.GetClientFileDirectory(mapJob);

                    if (!Directory.Exists(jobDirectory))
                        Directory.CreateDirectory(jobDirectory);

                    String mapCacheFolder = Path.Combine("$(cache)", "raw", "maps");
                    mapCacheFolder = this.ConfigurationService.Config.Project.Environment.Subst(mapCacheFolder);

                    if (!Directory.Exists(mapCacheFolder))
                        Directory.CreateDirectory(mapCacheFolder);

                    // Check for any previous network export changelists which might now be empty - if multiple job downloads
                    // have been done for example, leaving some empty changelists.
                    DeleteEmptyPendingChangelists(p4, NetworkExportChangelistDescription);
                    String firstMapSectionName = Path.GetFileNameWithoutExtension(mapJob.ExportProcesses.FirstOrDefault().DCCSourceFilename);
                    String jobCreatedTime = mapJob.CreatedAt.ToString();

                    String changelistDescriptionWithJob =
                        String.Format("{0} for '{1}', sent at '{2}'.  ( GUID = {3} )",
                            NetworkExportChangelistDescription, firstMapSectionName, jobCreatedTime, job.ID.ToString());

                    // Create a changelist for this job download
                    P4PendingChangelist changeList = p4.CreatePendingChangelist(changelistDescriptionWithJob);

                    // List for files to move to the pending changelist
                    List<String> moveToChangelistArgs = new List<String>();
                    moveToChangelistArgs.Add("-c");
                    moveToChangelistArgs.Add(changeList.Number.ToString());

                    foreach (ExportProcess exportProc in mapJob.ExportProcesses)
                    {
                        // Make sure the path isn't knackered
                        exportProc.ExportFiles = exportProc.ExportFiles.Select(x => Path.GetFullPath(x));

                        FileMapping[] fms = FileMapping.Create(p4, exportProc.ExportFiles.ToArray());
                        FileState[] fss = FileState.Create(p4, fms.Select(fm => fm.DepotFilename).ToArray());

                        Debug.Assert(fms.Length == fss.Length);
                        if (fms.Length != fss.Length)
                        {
                            jobVm.IsDownloading = false;
                            Log.Log__Error("Filemapping array length does not match filestate array length in DownloadExportAndBuildData!");
                            result = false;
                            continue; // Skip.
                        }

                        //Force sync the xml and zip file to avoid any resolve issues later
                        List<String> args = new List<String>();
                        args.Add("-f");
                        args.AddRange(exportProc.ExportFiles);
                        p4.Run("sync", args.ToArray());

                        // Download the .zip and .xml files
                        for (int n = 0; n < fss.Length; ++n)
                        {
                            // Check to make sure the export file is checked out or writeable                                
                            if (fss[n].OpenAction != FileAction.Edit)
                            {
                                if (fss[n].OtherLocked == true)
                                {
                                    // Need to warn that the file can't be checked out and whether the user wants to
                                    // toggle the writable flag.
                                    string msg = String.Format("Unable to check out {0} for download. Would you like to make the file writable?  If not, this file will be skipped", fms[n].DepotFilename);
                                    MessageBoxResult writableResult = MessageBox.Show(msg, "Perforce Error", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
                                    if (writableResult == MessageBoxResult.OK)
                                    {
                                        FileInfo fileInfo = new FileInfo(fms[n].LocalFilename);
                                        fileInfo.IsReadOnly = false;
                                    }
                                    else
                                    {
                                        continue;
                                    }
                                }
                                else
                                {
                                    try
                                    {
                                        p4.Run("edit", fms[n].DepotFilename);
                                        // We've marked it for edit so add to list to move to pending changelist
                                        moveToChangelistArgs.Add(fms[n].DepotFilename);
                                    }
                                    catch (Exception ex)
                                    {
                                        jobVm.IsDownloading = false;
                                        Log.Log__Error("Error trying to checkout export file {0} for job {1}: {2}", fms[n].DepotFilename, mapJob.ID, ex.Message);
                                        result = false;
                                    }
                                }
                            }
                            else
                            {
                                // We already have it checked out so add to list
                                moveToChangelistArgs.Add(fms[n].DepotFilename);
                            }

                            String relativeFilename = Path.GetFileName(fms[n].LocalFilename);
                            String downloadFilename = Path.Combine(jobDirectory, relativeFilename);

                            DownloadCopyAndDeleteFile(ftConsumer, mapJob, relativeFilename, downloadFilename, fms[n].LocalFilename);
                        }

                        // Find the content node
                        String dccSourceBasename = Path.GetFileName(exportProc.DCCSourceFilename);
                        String mapName = Path.GetFileNameWithoutExtension(dccSourceBasename);
                        ContentNode mapZip = gv.Content.Root.FindFirst(mapName, "mapzip");
                        if ((null == mapZip) || !(mapZip is ContentNodeMapZip))
                        {
                            Log.Log__Error("Failed to find build data for map: {0}.", mapName);
                            result = false;
                        }
                        else
                        {
                            // Get correct rpf output for map prop files
                            ContentNodeMapProcessedZip mapProcZipNode = (ContentNodeMapProcessedZip)mapZip.Outputs.First();
                            ContentNode mapProcAreaLodNode = ((ContentNodeGroup)mapProcZipNode.Parent).FindAll("mapcombinezip").FirstOrDefault();

                            // Asset combine area node
                            Debug.Assert(mapProcAreaLodNode is ContentNodeMapCombineZip);
                            ContentNodeMapCombineZip mapAreaCombineZipNode = (ContentNodeMapCombineZip)mapProcAreaLodNode;

                            IConfig config = RSG.Base.Configuration.ConfigFactory.CreateConfig();
                            IBranch branch = config.Project.DefaultBranch;
                            // Download the platform RPFs for enabled targets
                            foreach (ITarget target in branch.Targets.Values)
                            {
                                if (!target.Enabled)
                                    continue;

                                if (mapAreaCombineZipNode != null)
                                {
                                    // Combined RPF e.g downtown.rpf
                                    String mapAreaCombineRPFPathname = RSG.Pipeline.Services.Platform.PlatformPathConversion.ConvertAndRemapFilenameToPlatform(target, Path.GetFullPath(mapAreaCombineZipNode.Filename));
                                    // Use the prefixed rpf name with map area name to find it on server and download it 
                                    String mapAreaZipFilename = target.Platform.ToString() + Path.GetFileName(Path.GetDirectoryName(mapAreaCombineRPFPathname)) + ".zip";
                                    String downloadFilenameCombined = Path.Combine(jobDirectory, mapAreaZipFilename);
                                    String mapAreaZipDestinationpathname = Path.Combine(mapCacheFolder, mapAreaZipFilename);

                                    if (ftConsumer.FileAvailability(job).Contains(mapAreaZipFilename, StringComparer.OrdinalIgnoreCase))
                                    {
                                        result &= DownloadCopyAndDeleteFile(ftConsumer, mapJob, mapAreaZipFilename, downloadFilenameCombined, mapAreaZipDestinationpathname);
                                        if (File.Exists(mapAreaZipDestinationpathname))
                                        {
                                            RSG.Pipeline.Services.Zip.ExtractAll(mapAreaZipDestinationpathname, "x:/", true);
                                            try
                                            {
                                                File.Delete(mapAreaZipDestinationpathname);
                                            }
                                            catch (System.Exception ex)
                                            {
                                                Log.Log__Warning("Failed to delete map area zip file containing area RPFs - {0} : {1}", mapAreaZipDestinationpathname, ex.ToString());
                                            }
                                        }
                                        else
                                        {
                                            Log.Log__Error("Map area zip filename not found locally to extract after DownloadCopyAndDeleteFile: {0}", mapAreaZipDestinationpathname);
                                            result = false;
                                        }
                                    }
                                    else
                                    {
                                        Log.Log__Warning("Map area zip filename not available on server: {0}", mapAreaZipFilename);
                                    }
                                }
                                else
                                {
                                    // Single RPF per platform e.g. v_bins.rpf or v_int_11.rpf
                                    string platformFilename = RSG.Pipeline.Services.Platform.PlatformPathConversion.ConvertAndRemapFilenameToPlatform(target, Path.GetFullPath(mapProcZipNode.Filename));
                                    // Use the prefixed rpf name with target name to find it on server and download it with 
                                    String relativeFilename = target.Platform.ToString() + Path.GetFileName(platformFilename);
                                    String downloadFilename = Path.Combine(jobDirectory, relativeFilename);

                                    if (ftConsumer.FileAvailability(job).Contains(relativeFilename, StringComparer.OrdinalIgnoreCase))
                                    {
                                        result &= DownloadCopyAndDeleteFile(ftConsumer, mapJob, relativeFilename, downloadFilename, platformFilename);
                                    }
                                }
                            }
                        }
                    }

                    // Check for a textures and TCS zip file, if there is one then mark the files for edit/add and extract
                    String networkExportChangelistPathname = CommonFuncs._networkExportChangelistFilesZip;

                    // If the job has the tcs and textures zip, then download it
                    if (ftConsumer.FileAvailability(job).Contains(networkExportChangelistPathname, StringComparer.OrdinalIgnoreCase))
                    {
                        String texturesAndTCSZipDownloadPathname = Path.Combine(jobDirectory, networkExportChangelistPathname);
                        String texturesAndTCSZipDestinationPathname = Path.Combine(mapCacheFolder, networkExportChangelistPathname);

                        DownloadCopyAndDeleteFile(ftConsumer, mapJob, networkExportChangelistPathname, texturesAndTCSZipDownloadPathname, texturesAndTCSZipDestinationPathname);

                        if (File.Exists(texturesAndTCSZipDestinationPathname))
                        {
                            try
                            {
                                IEnumerable<String> fileListNoDrive = new List<String>();
                                RSG.Pipeline.Services.Zip.GetFileList(texturesAndTCSZipDestinationPathname, out fileListNoDrive);

                                if (fileListNoDrive.Count() > 0)
                                {
                                    // Drive info isn't stored in the zip file but everything else is.  
                                    List<String> fileList = fileListNoDrive.Select(f => "x:/" + f).ToList();

                                    // TCS file list
                                    String[] tcsFiles = fileList.Where(f => Path.GetExtension(f).Equals(".tcs")).ToArray();

                                    String[] binaryPlusMFiles = fileList.Where(
                                        (f => Path.GetExtension(f).Equals(".tif") || Path.GetExtension(f).Equals(".dds") ||
                                                Path.GetExtension(f).Equals(".clip") || Path.GetExtension(f).Equals(".anim"))).ToArray();

                                    FileMapping[] tcsFileMappings = new FileMapping[0];
                                    if (tcsFiles.Any())
                                        tcsFileMappings = FileMapping.Create(p4, tcsFiles);

                                    FileMapping[] binaryPlusMFileMappings = new FileMapping[0];
                                    if (binaryPlusMFiles.Any())
                                        binaryPlusMFileMappings = FileMapping.Create(p4, binaryPlusMFiles);

                                    //Force sync the texture/TCS files to avoid any resolve issues later
                                    List<String> forceSyncArgs = new List<String>();
                                    forceSyncArgs.Add("-f");
                                    forceSyncArgs.AddRange(tcsFileMappings.Select(f => f.DepotFilename));
                                    forceSyncArgs.AddRange(binaryPlusMFileMappings.Select(f => f.DepotFilename));
                                    // > 1 due to -f flag
                                    if (forceSyncArgs.Count > 1)
                                        p4.Run("sync", forceSyncArgs.ToArray());

                                    List<String> tcsFilesToEdit = new List<String>();
                                    List<String> tcsFilesToAdd = new List<String>();
                                    List<String> binaryPlusMFilesToEdit = new List<String>();
                                    List<String> binaryPlusMFilesToAdd = new List<String>();

                                    // See if files need adding or edited.
                                    foreach (FileMapping fm in tcsFileMappings)
                                    {
                                        if (p4.Exists(fm.DepotFilename))
                                            tcsFilesToEdit.Add(fm.DepotFilename);
                                        else
                                            tcsFilesToAdd.Add(fm.ClientFilename);
                                    }
                                    foreach (FileMapping fm in binaryPlusMFileMappings)
                                    {
                                        if (p4.Exists(fm.DepotFilename))
                                            binaryPlusMFilesToEdit.Add(fm.DepotFilename);
                                        else
                                            binaryPlusMFilesToAdd.Add(fm.ClientFilename);
                                    }

                                    // Horrid but extract direct to x: drive.
                                    RSG.Pipeline.Services.Zip.ExtractAll(texturesAndTCSZipDestinationPathname, "x:/", true);

                                    if (tcsFilesToEdit.Any())
                                    {
                                        p4.Run("edit", tcsFilesToEdit.ToArray());
                                        moveToChangelistArgs.AddRange(tcsFilesToEdit);
                                    }
                                    if (tcsFilesToAdd.Any())
                                    {
                                        p4.Run("add", tcsFilesToAdd.ToArray());
                                        moveToChangelistArgs.AddRange(tcsFilesToAdd);
                                    }
                                    if (binaryPlusMFilesToEdit.Any())
                                    {
                                        List<String> textureFilesToEditArgs = new List<String> { "-t", "binary+m" };
                                        textureFilesToEditArgs.AddRange(binaryPlusMFilesToEdit);
                                        p4.Run("edit", textureFilesToEditArgs.ToArray());
                                        moveToChangelistArgs.AddRange(binaryPlusMFilesToEdit);
                                    }
                                    if (binaryPlusMFilesToAdd.Any())
                                    {
                                        List<String> textureFilesToAddArgs = new List<String> { "-t", "binary+m" };
                                        textureFilesToAddArgs.AddRange(binaryPlusMFilesToAdd);
                                        p4.Run("add", textureFilesToAddArgs.ToArray());
                                        moveToChangelistArgs.AddRange(binaryPlusMFilesToAdd);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                jobVm.IsDownloading = false;
                                Log.Log__Error("Exception whilst extracting texture/TCS files or creating/reverting perforce changelist: {0}", ex.ToString());
                                result = false;
                            }
                        }
                    }
                    else
                    {
                        Log.Log__Message("{0} does not exist on the server for this job", networkExportChangelistPathname);
                    }

                    // -c and the changelist number are always in moveToChangelistArgs
                    if (moveToChangelistArgs.Count > 2)
                    {
                        p4.Run("reopen", moveToChangelistArgs.ToArray());
                    }

                    // Revert unchanged - somebody might be downloading files which have
                    // been checked in by someone else so are now not needing checked in
                    String[] revertUnchangedFilesArgs = new String[] { "-a", "-c", changeList.Number.ToString() };
                    p4.Run("revert", revertUnchangedFilesArgs);

                    // Try and delete the changelist - it might be empty if unchanged files
                    // were reverted.
                    changeList.Delete();
                }
            }
            catch (Exception ex)
            {
                jobVm.IsDownloading = false;
                Log.Log__Error("Perforce exception in DownloadExportAndBuildData: {0}", ex.Message);
                result = false;
            }


            Thread.Sleep(100);
            context.ReturnValue = result;
        }
        #endregion // Private Methods
    }

} // Workbench.AddIn.Automation.UI namespace
