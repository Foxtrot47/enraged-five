﻿using System;
using System.ComponentModel.Composition;
using Workbench.AddIn;
using Workbench.AddIn.UI.Layout;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using RSG.Interop.Autodesk3dsmax;

namespace Workbench.AddIn._3dsmax.Listener
{

    /// <summary>
    /// 
    /// </summary>
    public class MaxScriptListenerViewModel : 
        ViewModelBase
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Selected 3dsmax instance.
        /// </summary>
        public MaxInstance SelectedMaxInstance
        {
            get { return m_SelectedMaxInstance; }
            set
            {
                SetPropertyValue(value, () => this.SelectedMaxInstance,
                  new PropertySetDelegate(delegate(Object newValue) { m_SelectedMaxInstance = (MaxInstance)newValue; }));
            
            }
        }
        private MaxInstance m_SelectedMaxInstance = null;

        /// <summary>
        /// Array of all the 3dsmax instances.
        /// </summary>
        public MaxInstance[] MaxInstances
        {
            get { return m_MaxInstances; }
            private set
            {
                SetPropertyValue(value, () => this.MaxInstances,
                   new PropertySetDelegate(delegate(Object newValue) { m_MaxInstances = (MaxInstance[])newValue; }));

                bool exists = false;
                foreach (MaxInstance inst in this.MaxInstances)
                {
                    if (inst.Equals(this.SelectedMaxInstance))
                        exists = true;
                }
                if ((!exists) && (this.MaxInstances.Length > 0))
                    this.SelectedMaxInstance = this.MaxInstances[0];
            }
        }
        private MaxInstance[] m_MaxInstances;
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public MaxScriptListenerViewModel()
        {
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Refresh our 3dsmax instance list.
        /// </summary>
        public void RefreshMaxInstances()
        {
            this.MaxInstances = MaxInstance.GetRunningInstances();
        }
        #endregion // Controller Methods

        #region IPartImportsSatisfiedNotification Interface
        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {
            RefreshMaxInstances();
        }
        #endregion // IPartImportsSatisfiedNotification Interface
    }

} // Workbench.AddIn._3dsmax.Listener namespace
