﻿using System;
using System.Windows.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Map.ViewModel;

namespace Map.AddIn.Services
{
    public interface IMapBrowserUserProxy
    {
        #region Properties

        /// <summary>
        /// The ID that the document/tool view has on it, so that the
        /// map browser knows when to load/unload the user data
        /// </summary>
        Guid UserViewID { get; }

        #endregion // Properties

        #region Methods

        /// <summary>
        /// Determines whether the given section needs to have user data loaded for it
        /// </summary>
        Boolean IsSectionValid(MapSectionViewModel section);

        /// <summary>
        /// Gets called whenever the userdata for the specific
        /// ID needs to get loaded for a specific level.
        /// </summary>
        void LoadUserData(Dispatcher dispatcher, MapSectionViewModel section);

        /// <summary>
        /// Gets called whenever the userdata for the specific
        /// ID needs to get unloaded for a specific level.
        /// </summary>
        void UnloadUserData(Dispatcher dispatcher, MapSectionViewModel section);

        #endregion // Methods
    }
}
