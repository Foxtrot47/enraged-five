﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetadataEditor.AddIn.KinoEditor.Subtitle_Editor
{
    public class Helper
    {
        public static int SecondsToFrames(double seconds)
        {
            return (int)seconds * 30;
        }
    }
}
