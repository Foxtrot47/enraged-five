﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MetadataEditor.AddIn.PedVariation.ViewModel;

namespace MetadataEditor.AddIn.PedVariation.View
{
    /// <summary>
    /// Interaction logic for SelectionSetCollection.xaml
    /// </summary>
    public partial class SelectionSetCollection : UserControl
    {
        public SelectionSetCollection()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            SelectionSetCollectionViewModel vm = this.DataContext as SelectionSetCollectionViewModel;
            if (vm == null)
            {
                return;
            }

            vm.AddNewSelectionSet();
        }

        private void CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            SelectionSetCollectionViewModel vm = this.DataContext as SelectionSetCollectionViewModel;
            if (vm == null)
            {
                return;
            }

            vm.Remove(e.Parameter as SelectionSetViewModel);
        }
    }
}
