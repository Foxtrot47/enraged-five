﻿using System;
using System.Reflection;
using System.Windows;
using RSG.Base.Logging;
using Workbench.AddIn;
using Workbench.AddIn.Services;
using Workbench.AddIn.UI.Layout;

namespace Workbench
{

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(
        Workbench.AddIn.ExtensionPoints.StartupCommand,
        typeof(IApplicationCommand))]
    class WorkbenchStartup : 
        ExtensionBase, 
        IApplicationCommand
    {
        public static readonly Guid GUID =
            new Guid("F97C861A-346D-43F9-A684-5FA04C284874");
        
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        WorkbenchStartup()
            : base()
        {
            this.ID = GUID;
            this.RelativeID = Guid.Empty;
        }
        #endregion // Constructor(s)

        #region IMenuItem Interface
        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        public void Execute(params Object[] args)
        {
            Log.Log__Message("Workbench startup command test");
            Assembly assembly = Assembly.GetExecutingAssembly();
            Version version = assembly.GetName().Version;

            if (App.Current != null && App.Current.MainWindow != null)
            {
#if DEBUG
                App.Current.MainWindow.Title = String.Format("{0} - {1} DEBUG",
                    Resources.Strings.MainWindow_Title, version);
#else
                App.Current.MainWindow.Title = String.Format("{0} - {1}",
                    Resources.Strings.MainWindow_Title, version);
#endif // DEBUG
            }
        }
        #endregion // IMenuItem Interface
    }
    
} // Workbench namespace
