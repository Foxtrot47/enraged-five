﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Workbench.AddIn.TXDParentiser.ViewModel
{
    /// <summary>
    /// 
    /// </summary>
    public class RadioGroupViewModel : RSG.Base.Editor.ViewModelBase
    {        
        #region Fields

        private Object m_toolTip;
        private String m_groupName;
        private ObservableCollection<RadioOptionViewModel> m_options;

        #endregion // Fields

        #region Properties

        /// <summary>
        /// The tooltip for the command, this can be changed
        /// at anytime using the set method
        /// </summary>
        public ObservableCollection<RadioOptionViewModel> Options
        {
            get { return m_options; }
            set
            {
                this.SetPropertyValue(ref this.m_options, value, "Options");
            }
        }

        /// <summary>
        /// The tooltip for the command, this can be changed
        /// at anytime using the set method
        /// </summary>
        public Object ToolTip
        {
            get { return m_toolTip; }
            set
            {
                this.SetPropertyValue(ref this.m_toolTip, value, "ToolTip");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public String GroupName
        {
            get { return m_groupName; }
            set
            {
                this.SetPropertyValue(ref this.m_groupName, value, "GroupName");
            }
        }

        #endregion // Properties
        
        #region Constructors

        /// <summary>
        /// Default Constructor
        /// </summary>
        public RadioGroupViewModel(String groupName, String toolTip, IList<RadioOptionViewModel> options)
        {
            this.Options = new ObservableCollection<RadioOptionViewModel>();
            this.GroupName = groupName;
            this.ToolTip = toolTip;
            foreach (RadioOptionViewModel option in options)
            {
                this.Options.Add(option);
            }
        }

        #endregion // Constructors
    } // RadioGroupViewModel
} // Workbench.AddIn.TXDParentiser.ViewModel
