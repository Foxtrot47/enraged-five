﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;

namespace Workbench.AddIn.GameStatistics.Reports.Automated.ViewModels
{
    /// <summary>
    /// Single result from running a smoke test against the test data.
    /// </summary>
    public class SmokeTestResultViewModel : ViewModelBase, IComparable<SmokeTestResultViewModel>
    {
        #region Properties
        /// <summary>
        /// Type of message this was (message, warning or error).
        /// </summary>
        public string Type
        {
            get { return m_type; }
            set
            {
                SetPropertyValue(value, () => this.Type,
                          new PropertySetDelegate(delegate(object newValue) { m_type = (string)newValue; }));
            }
        }
        private string m_type;

        /// <summary>
        /// 
        /// </summary>
        public string Message
        {
            get { return m_message; }
            set
            {
                SetPropertyValue(value, () => this.Message,
                          new PropertySetDelegate(delegate(object newValue) { m_message = (string)newValue; }));
            }
        }
        private string m_message;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public SmokeTestResultViewModel(string type, string message)
            : base()
        {
            Type = type;
            Message = message;
        }
        #endregion // Constructor(s)

        #region IComparable<SmokeTestResultViewModel> Interface
        /// <summary>
        /// Compare this result to another.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(SmokeTestResultViewModel other)
        {
            if (other == null)
            {
                return 1;
            }

            if (Type == other.Type)
            {
                return Message.CompareTo(other.Message);
            }
            else
            {
                if (Type == "Error")
                {
                    return -1;
                }
                else if (other.Type == "Error")
                {
                    return 1;
                }
                else if (Type == "Warning")
                {
                    return -1;
                }
                else if (other.Type == "Warning")
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
        }
        #endregion // IComparable<SmokeTestResultViewModel> Interface
    } // SmokeTestResultViewModel
}
