﻿using System;
using System.Windows.Input;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using RSG.Base.Editor;
using RSG.Base.Logging;
using Workbench.AddIn;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.Services.File;
using MetadataEditor.AddIn;
using MetadataEditor.AddIn.View;
using RSG.Metadata.Parser;
using RSG.Metadata.Model;
using MetadataEditor.ParsingErrors;
using System.Xml;
using RSG.Metadata.Parser;
using System.Windows;
using Workbench.AddIn.Services;
using System.IO;
using RSG.Metadata;
using MetadataEditor.UI.View;

namespace MetadataEditor.Services.File
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.OpenService, typeof(IOpenService))]
    [ExportExtension(Workbench.AddIn.ExtensionPoints.ToolbarStandard, typeof(IOpenService))]
    [ExportExtension(MetadataEditor.AddIn.CompositionPoints.MetadataOpenService, typeof(IOpenService))]
    class OpenService : IOpenService
    {
        #region MEF Imports

        /// <summary>
        /// 
        /// </summary>
        [ImportManyExtension(MetadataEditor.AddIn.ExtensionPoints.MetadataDocument,
            typeof(IMetadataDocumentProxy))]
        private List<IMetadataDocumentProxy> MetadataDocuments { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(MetadataEditor.AddIn.ExtensionPoints.DefaultMetadataDocument,
            typeof(IMetadataDocumentProxy))]
        private Lazy<IMetadataDocumentProxy> DefaultMetadataDocument { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(MetadataEditor.AddIn.CompositionPoints.StructureDictionary,
            typeof(IStructureDictionary))]
        private Lazy<IStructureDictionary> Structures { get; set; }

        /// <summary>
        /// The perforce sync service
        /// </summary>
        [Workbench.AddIn.ImportExtension(Workbench.AddIn.CompositionPoints.PerforceSyncService,
            typeof(Workbench.AddIn.Services.IPerforceSyncService))]
        private IPerforceSyncService PerforceSyncService { get; set; }

        #endregion // MEF Imports

        #region Constants
        private readonly Guid GUID =
            new Guid("ABF09071-294F-4EA0-A68F-9A3F899BA854");
        private readonly String COMMAND_LINE_KEY = "meta";
        #endregion // Constants

        #region Members

        private ParsingErrorGroup m_fileParsingErrors;
        private String m_currentFilename;
        private List<string> structureValidationErrors;
        private List<string> metadataLoadErrors;
        #endregion // Members

        #region Properties
        /// <summary>
        /// Service GUID.
        /// </summary>
        public Guid ID
        {
            get { return (GUID); }
        }
        /// <summary>
        /// Open service header string for UI display.
        /// </summary>
        public String Header
        {
            get { return (Properties.Settings.Default.MetadataFileOpenHeader); }
        }

        /// <summary>
        /// Open service Bitmap for UI display.
        /// </summary>
        public Bitmap Image
        {
            get { return (Resources.Images.OPEN); }
        }

        /// <summary>
        /// Open dialog filter string.
        /// </summary>
        public String Filter
        {
            get { return (Properties.Settings.Default.MetadataFileFilter); }
        }

        /// <summary>
        /// Determines if the open dialog allows multiple items selected
        /// </summary>
        public Boolean AllowMultiSelect 
        {
            get { return true; }
        }

        /// <summary>
        /// Open dialog default filter index.
        /// </summary>
        public int DefaultFilterIndex
        {
            get { return (Properties.Settings.Default.MetadataFileFilterIndex); }
        }

        /// <summary>
        /// 
        /// </summary>
        public ParsingErrorGroup FileParsingErrors
        {
            get { return m_fileParsingErrors; }
            set { m_fileParsingErrors = value; }
        }

        /// <summary>
        /// The keybinding that is placed on this command.
        /// This binding is added to the applications bindings.
        /// (Only used if this is a main menu item)
        /// </summary>
        public KeyGesture KeyGesture
        {
            get { return new KeyGesture(Key.O, ModifierKeys.Control);  }
        }

        /// <summary>
        /// Command line key.
        /// </summary>
        public String CommandLineKey
        {
            get { return COMMAND_LINE_KEY; }
        }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="document"></param>
        /// <param name="model"></param>
        /// <param name="filename"></param>
        /// <param name="filterIndex"></param>
        /// <returns></returns>
        public bool Open(out IDocumentBase document, out IModel model, String filename, int filterIndex)
        {
            // Prompt the user if the file isn't latest
            int syncedFiles = 0;
            string p4syncMsg = String.Format("The '{0}' metadata file is currently out of date.\nWould you like to grab of it latest now?", Path.GetFileName(filename));
            PerforceSyncService.Show(p4syncMsg, new string[] { filename }, ref syncedFiles, false, true);

            document = null;
            model = null;
            this.FileParsingErrors = null;
            this.m_currentFilename = filename;
            StructureDictionary dictionary = this.Structures.Value.Structures;
            Structure structure = null;
            IMetadataDocumentProxy documentPoxy = null;
            if (!this.ValidateRootNode(filename, out structure, out documentPoxy, dictionary))
            {
                return false;
            }

            Metadata.Log.LogError += OnStructureValidationError;
            try
            {
                this.structureValidationErrors = new List<string>();
                structure.CanBeInstanced();
                if (this.structureValidationErrors.Count > 0)
                {
                    ErrorEncounteredWindow window = new ErrorEncounteredWindow();
                    window.Message.Text = "Unable to open the metadata file due to errors caught inside the psc structure the file is instancing. The programmer responsible for the psc(s) listed in the errors below will need to be informed.";
                    window.ErrorList.ItemsSource = this.structureValidationErrors;
                    window.Title = string.Format("Error Opening {0}", filename);
                    window.ShowDialog();
                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to open the metadata file due to a unhandled exception caught while validating the psc structure the file is instancing.", "Unhandled Exception", MessageBoxButton.OK, MessageBoxImage.Error);
                Metadata.Log.Exception(ex, "Metadata Exception");
                return false;
            }
            finally
            {
                Metadata.Log.LogError -= OnStructureValidationError;
            }

            Stopwatch sw = Stopwatch.StartNew();
            IMetaFile metaFile = null;
            Metadata.Log.LogError += OnMetadataLoadError;
            try
            {
                this.metadataLoadErrors = new List<string>();
                bool successful = documentPoxy.CreateFile(out metaFile, filename, dictionary, structure);
                if (metadataLoadErrors.Count > 0)
                {
                    ErrorEncounteredWindow window = new ErrorEncounteredWindow();
                    window.Message.Text = "Unable to open the metadata file due to errors caught while creating the metadata model.";
                    window.ErrorList.ItemsSource = this.metadataLoadErrors;
                    window.Title = string.Format("Error Opening {0}", filename);
                    window.ShowDialog();
                    return false;
                }
            }
            catch (XmlException ex)
            {
                ErrorEncounteredWindow window = new ErrorEncounteredWindow();
                window.Message.Text = "Unable to open the metadata file due to the xml data being corrupted.";
                string errorMessage = string.Format("Xml exception caught.\n{0}", ex.Message);
                window.ErrorList.ItemsSource = System.Linq.Enumerable.Repeat(errorMessage, 1);
                window.Title = string.Format("Error Opening {0}", filename);
                window.ShowDialog();
                return false;
            }
            catch (ParserException ex)
            {
                MessageBox.Show(ex.Message, "Specific Metadata Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Metadata.Log.Exception(ex, "Specific Metadata Exception");
                return false;
            }
            catch (Exception ex)
            {
                if (string.IsNullOrWhiteSpace(ex.Message))
                {
                    MessageBox.Show("Unable to open the metadata file due to a unhandled exception caught while creating the metadata model.", "Unhandled Exception", MessageBoxButton.OK, MessageBoxImage.Error); 
                }
                else
                {
                    MessageBox.Show(ex.Message, "Caught Exception", MessageBoxButton.OK, MessageBoxImage.Error);
                }

                Metadata.Log.Exception(ex, "Metadata Exception");
                return false;
            }
            finally
            {
                Metadata.Log.LogError -= OnMetadataLoadError;
            }

            bool documentOpened = false;
            if (metaFile != null)
            {
                documentOpened = documentPoxy.OpenDocument(out document, metaFile);
                if (documentOpened)
                {
                    model = metaFile as IModel;
                    if (document != null)
                    {
                        document.OpenService = this;
                    }
                }
            }

            sw.Stop();
            RSG.Base.Logging.Log.Log__Message("The metadata file '{0}' has loaded in '{1}' millisecond.", filename, sw.ElapsedMilliseconds);
            return documentOpened;
        }

        private void OnStructureValidationError(object sender, LogMessageEventArgs e)
        {
            this.structureValidationErrors.Add(e.Message.Replace("\\r\\n", "\r\n"));
        }

        private void OnMetadataLoadError(object sender, LogMessageEventArgs e)
        {
            this.metadataLoadErrors.Add(e.Message.Replace("\\r\\n", "\r\n"));
        }

        public bool Open(out Dictionary<IDocumentBase, List<String>> documents, out List<IModel> models, List<String> filenames, int filterIndex)
        {
            // Ensure that the IMetadataModelView is constructed.
            documents = new Dictionary<IDocumentBase, List<String>>();
            models = new List<IModel>();
            this.FileParsingErrors = null;


            StructureDictionary structureDictionary = this.Structures.Value.Structures;

            System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
            Boolean result = false;

#warning TODO: Don't use the static log here.  We should really write a custom LogTarget that collects errors/warnings for use later.
            RSG.Base.Logging.Log.StaticLog.LogError += new EventHandler<RSG.Base.Logging.LogMessageEventArgs>(LogError);
            List<MetaFile> files = new List<MetaFile>();
            bool validate = true;
            foreach (String filename in filenames)
            {
                string root = GetRootDataType(filename);
                IMetadataDocumentProxy validProxy = GetValidProxy(root);
                if (string.IsNullOrEmpty(root) || validProxy == null)
                {
                    MessageBox.Show("Unable to open meta file. Document is missing root node", "Invalid File Format", MessageBoxButton.OK, MessageBoxImage.Error);
                    return false;
                }
                if (validate)
                {
                    Structure rootDefinition = null;
                    if (!structureDictionary.TryGetValue(root, out rootDefinition))
                    {
                        String message = String.Format("Unable to open meta file. The structure dictionary does not contain the type '{0}'.", root);
                        Debug.Assert(structureDictionary.ContainsKey(root), message);
                        MessageBox.Show(message, "Missing <structdef>", MessageBoxButton.OK, MessageBoxImage.Error);
                        return false;
                    }
                    validate = false;
                }

                this.m_currentFilename = filename;
                files.Add(new MetaFile(filename, this.Structures.Value.Structures));
            }
            MultiMetaFiles file = new MultiMetaFiles(files);
            RSG.Base.Logging.Log.StaticLog.LogError -= new EventHandler<RSG.Base.Logging.LogMessageEventArgs>(LogError);
            if (FileParsingErrors != null)
            {
                //this.ShowDefinitionErrorView();
            }

            if (file != null)
            {
                String rootDataType = file.Definition.DataType;

                // Loop through our supported metadata types and see if we have
                // a more convenient one than the default editor.  Use the
                // first one that matches; otherwise default.
                foreach (IMetadataDocumentProxy proxy in this.MetadataDocuments)
                {
                    List<String> types = new List<String>(proxy.SupportedMetadataTypes);
                    if (types.Contains(rootDataType))
                    {
                        IDocumentBase doc = null;
                        Boolean openResult = proxy.OpenDocument(out doc, file);
                        if (openResult == true)
                        {
                            documents.Add(doc, filenames);
                            models.Add(file);
                            result = true;
                            break;
                        }
                    }
                }
                if (result == false)
                {
                    // If we haven't created our IMetadataDocument yet then lets
                    // fallback to our default view.
                    IDocumentBase doc = null;
                    Boolean defaultResult = this.DefaultMetadataDocument.Value.OpenDocument(out doc, file);
                    if (defaultResult == true)
                    {
                        documents.Add(doc, filenames);
                        models.Add(file);
                        result = true;
                    }
                }
            }

            sw.Stop();
            RSG.Base.Logging.Log.Log__Message("The metadata files have loaded and took {0} millisecond to do it.", sw.ElapsedMilliseconds);

            return result;
        }

        private void LogError(Object sender, RSG.Base.Logging.LogMessageEventArgs e)
        {
            String context = "";
            if (!String.IsNullOrEmpty(e.Context) && System.IO.File.Exists(e.Context))
            {
                context = e.Context;
            }
            else
            {
                context = this.m_currentFilename;
            }

            if (this.FileParsingErrors == null)
                this.FileParsingErrors = new ParsingErrorGroup("File Parsing Errors");

            FileErrors newError = this.FileParsingErrors.GetError(context) as FileErrors;
            if (newError == null)
            {
                newError = new FileErrors(context);
                newError.Errors.Add(e.Message);
                this.FileParsingErrors.Errors.Add(newError);
            }
            else
            {
                newError.Errors.Add(e.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private IMetadataDocumentProxy GetValidProxy(string rootDataType)
        {
            IMetadataDocumentProxy validProxy = this.DefaultMetadataDocument.Value;

            // Loop through our supported metadata types and see if we have
            // a more convenient one than the default editor.  Use the
            // first one that matches; otherwise default.
            foreach (IMetadataDocumentProxy proxy in this.MetadataDocuments)
            {
                List<String> types = new List<String>(proxy.SupportedMetadataTypes);
                if (types.Contains(rootDataType))
                {
                    validProxy = proxy;
                    break;
                }
            }
            return validProxy;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private string GetRootDataType(string filename)
        {
            string baseType = null;
            using (XmlReader reader = XmlReader.Create(filename))
            {
                reader.MoveToContent();
                baseType = Namespace.ConvertDataTypeToFriendlyName(reader.Name);
            }

            return baseType;
        }

        private bool ValidateRootNode(string filename, out Structure structure, out IMetadataDocumentProxy proxy, StructureDictionary dictionary)
        {
            structure = null;
            proxy = null;
            MessageBoxButton button = MessageBoxButton.OK;
            MessageBoxImage image = MessageBoxImage.Error;
            string caption = "Invalid Metadata";
            string root = null;
            try
            {
                root = GetRootDataType(filename);
            }
            catch (XmlException ex)
            {
                string msg = "Unable to open meta file. The document contains invalid xml data.\n";
                msg += string.Format("{0}\n", ex.Message);
                msg += string.Format("Location: Line Number '{0}', Line Position '{1}'", ex.LineNumber, ex.LinePosition);
                MessageBox.Show(msg, caption, button, image);
                return false;
            }

            if (string.IsNullOrWhiteSpace(root))
            {
                MessageBox.Show("Unable to open meta file. Document is missing root node.", caption, button, image);
                return false;
            }

            proxy = GetValidProxy(root);
            if (proxy == null)
            {
                string msg = string.Format("Unable to open meta file. The root type '{0}' isn't linked to a document type.", root);
                MessageBox.Show(msg, caption, button, image);
                return false;
            }

            if (!dictionary.TryGetValue(root, out structure))
            {
                string msg = String.Format("Unable to open meta file. The loaded structure dictionary does not contain the psc type '{0}'.\n", root);
                msg += "If you are seeing this message it means that the psc files located underneath the {metadata}/definitions folder doesn't contain a definition for the type and needs to be added.";
                MessageBox.Show(msg, caption, button, image);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Creates a new document based on the specified model
        /// </summary>
        /// <param name="model">
        /// The model that determines what document is created.
        /// </param>
        /// <returns>
        /// A new document that has its model/viewmodel setup to represent the specified model.
        /// </returns>
        public IDocumentBase Open(IModel model)
        {
            IMetaFile metadataModel = model as IMetaFile;
            if (metadataModel == null)
            {
                return null;
            }

            string root = metadataModel.Definition.DataType;
            IMetadataDocumentProxy documentPoxy = GetValidProxy(root);
            if (documentPoxy == null)
            {
                return null;
            }

            IDocumentBase document = null;
            documentPoxy.OpenDocument(out document, metadataModel);
            if (document != null)
            {
                document.OpenService = this;
            }

            return document;
        }
        #endregion // Methods
    }
} // MetadataEditor.Services.File namespace
