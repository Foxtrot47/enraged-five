﻿using System;
using System.ComponentModel.Composition;

namespace Workbench.AddIn
{

    #region Workbench ExtensionService Import/Export Attributes
    /// <summary>
    /// Export ID attribute helper, saves us using String constants all over
    /// the place for contracts.
    /// </summary>
    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
    public class ExportExtensionAttribute : ExportAttribute
    {
        public ExportExtensionAttribute(String guid)
            : base(guid)
        {
            ID = guid;
        }

        public ExportExtensionAttribute(String guid, Type type)
            : base(guid, type)
        {
            ID = guid;
        }

        /// <summary>
        /// 
        /// </summary>
        public String ID { get; set; }
    }

    /// <summary>
    /// Import ID attribute helper, saves us using String constants all over
    /// the place for contracts.
    /// </summary>
    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    public class ImportExtensionAttribute : ImportAttribute
    {
        public ImportExtensionAttribute(String guid)
            : base(guid)
        {
            ID = guid;
        }

        public ImportExtensionAttribute(String guid, Type type)
            : base(guid, type)
        {
            ID = guid;
        }

        public String ID { get; set; }
    }

    /// <summary>
    /// Import Many Workbench attribute helper, saves us using String constants
    /// all over the place for contracts.
    /// </summary>
    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    public class ImportManyExtensionAttribute : ImportManyAttribute
    {
        public ImportManyExtensionAttribute(String guid)
            : base(guid)
        {
            ID = guid;
        }

        public ImportManyExtensionAttribute(String guid, Type type)
            : base(guid, type)
        {
            ID = guid;
        }

        public String ID { get; set; }
    }
    #endregion // Workbench ExtensionService Import/Export Attributes

} // Workbench.AddIn namespace
