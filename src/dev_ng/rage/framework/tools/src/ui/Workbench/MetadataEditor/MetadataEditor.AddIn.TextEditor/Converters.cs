﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Windows.Data;
using System.Xml;
using ICSharpCode.AvalonEdit.Document;
using System.Text;

namespace MetadataEditor.AddIn.TextEditor
{

    /// <summary>
    /// 
    /// </summary>
    [ValueConversion(typeof(XmlDocument), typeof(TextDocument))]
    public class XmlDocumentToTextDocumentConverter : IValueConverter
    {
        public Object Convert(Object value, Type targetType, Object b, CultureInfo culture)
        {
            //return new TextDocument("CONVERT CULTURE");

            TextDocument doc = new TextDocument();
            doc.Text = "BJKHJKHJKHJKHJKHJKHJKHJK";
            return (doc);
        }

        public Object ConvertBack(Object value, Type targetType, Object b, CultureInfo culture)
        {
            return ("CONVERT BACK CULTURE");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [ValueConversion(typeof(XmlDocument), typeof(TextDocument))]
    public class XmlDocumentToStringConverter : IValueConverter
    {
        public Object Convert(Object value, Type targetType, Object b, CultureInfo culture)
        {
            Debug.Assert(value is XmlDocument,
                "Invalid XmlDocument input to converter.");

            // Walk through the XmlDocument, pretty formatting it into
            // our StringBuilder object.
            XmlDocument xmlDoc = (value as XmlDocument);
            StringBuilder sb = new StringBuilder();
            foreach (XmlNode node in xmlDoc)
            {
                RecurseNode(ref sb, node, 0);
            }

            return (sb.ToString());
        }

        public Object ConvertBack(Object value, Type targetType, Object b, CultureInfo culture)
        {
            return ("CONVERT BACK CULTURE");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <param name="indent"></param>
        private void RecurseNode(ref StringBuilder sb, XmlNode node, int indent)
        {
            for (int i = 0; i < indent; ++i)
                sb.Append('\t');
            sb.Append(node.OuterXml);
            //sb.Append("Node: ");
            //sb.Append(node.Name);
            sb.Append(Environment.NewLine);
            foreach (XmlNode child in node.ChildNodes)
            {
                RecurseNode(ref sb, child, indent + 1);
            }
        }
    }

} // MetadataEditor.AddIn.TextEditor namespace
