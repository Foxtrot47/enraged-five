﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn.Services;

namespace Workbench.AddIn.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public class ToolBarBase : ExtensionBase, IToolbar
    {
        #region Members

        private IEnumerable<IWorkbenchCommand> m_items;

        #endregion // Members

        #region Properties

        /// <summary>
        /// The child commands to this one. Any child commands will be
        /// set as a sub menu to a menu item.
        /// </summary>
        public IEnumerable<IWorkbenchCommand> Items
        {
            get { return m_items; }
            set
            {
                if (m_items == value)
                    return;

                SetPropertyValue(value, () => this.Items,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_items = (IEnumerable<IWorkbenchCommand>)newValue;
                        }
                    )
                );
            }
        }

        #endregion // Properties

        #region Protected Methods

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="commandCollection"></param>
        /// <returns></returns>
        protected IList<T> SortToolBarItems<T>(IEnumerable<T> commandCollection) where T : IWorkbenchCommand
        {
            List<T> commands = new List<T>(commandCollection);
            List<T> sortedCommands = new List<T>();
            List<T> unsortedCommands = new List<T>();
            foreach (T newCommand in commands)
            {
                if ((null == newCommand.RelativeToolBarID) ||
                    (Guid.Empty == newCommand.RelativeToolBarID))
                {
                    sortedCommands.Add(newCommand);
                }
                else if (FindByGuid(newCommand.RelativeToolBarID, commands) == -1)
                {
                    // found a configuration error
                    RSG.Base.Logging.Log.Log__Warning("Configuration error with extension ID {0}, RelativeID of {1} doesn't exist.",
                        newCommand.ID, newCommand.RelativeToolBarID);
                    sortedCommands.Add(newCommand);
                }
                else
                {
                    unsortedCommands.Add(newCommand);
                }
            }
            while (unsortedCommands.Count > 0)
            {
                List<T> stillUnsortedCommands = new List<T>();
                int startingCount = unsortedCommands.Count;
                foreach (T newCommand in unsortedCommands)
                {
                    int index = FindByGuid(newCommand.RelativeToolBarID, sortedCommands);
                    if (index > -1)
                    {
                        if (Direction.Before == newCommand.Direction)
                        {
                            sortedCommands.Insert(index, newCommand);
                        }
                        else
                        {
                            if (index == sortedCommands.Count - 1)
                            {
                                //it's to be inserted after the last item in the list
                                sortedCommands.Add(newCommand);
                            }
                            else
                            {
                                sortedCommands.Insert(index + 1, newCommand);
                            }
                        }
                    }
                    else
                    {
                        stillUnsortedCommands.Add(newCommand);
                    }
                }
                if (startingCount == stillUnsortedCommands.Count)
                {
                    // We didn't make any progress
                    RSG.Base.Logging.Log.Log__Error("Configuration error with one of these extensions:");
                    foreach (T ext in stillUnsortedCommands)
                    {
                        RSG.Base.Logging.Log.Log__Error("\tID = {0}, RelativeID = {1}", ext.ID, ext.RelativeToolBarID);
                    }
                    // Pick one and add it at the end.
                    sortedCommands.Add(stillUnsortedCommands[0]);
                    stillUnsortedCommands.RemoveAt(0);
                }
                unsortedCommands = stillUnsortedCommands;
            }
            return sortedCommands;
        }

        /// <summary>
        /// Find the index of a particular extension by Guid, -1 returned if 
        /// not found.
        /// </summary>
        /// <typeparam name="T">IExtension subclass</typeparam>
        /// <param name="id">Guid of extension to find.</param>
        /// <param name="extensions">Collection of IExtension objects.</param>
        /// <returns>Index of extension, -1 returned if not found.</returns>
        private int FindByGuid<T>(Guid id, IList<T> extensions)
            where T : IExtension
        {
            for (int i = 0; i < extensions.Count; ++i)
            {
                if (extensions[i].ID == id)
                    return (i);
            }
            return (-1);
        }

        #endregion // Protected Methods
    } // ToolBarBase
} // Workbench.AddIn.Commands
