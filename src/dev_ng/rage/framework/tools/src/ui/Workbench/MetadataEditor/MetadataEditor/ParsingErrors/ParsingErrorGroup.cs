﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RSG.Base.Editor;

namespace MetadataEditor.ParsingErrors
{
    /// <summary>
    /// Represents a specific group of parsing errors
    /// </summary>
    public class ParsingErrorGroup : ModelBase
    {
        #region Members

        private String m_name;
        private ObservableCollection<ParsingError> m_errors;

        #endregion // Members

        #region Properties

        /// <summary>
        /// The name of the group
        /// </summary>
        public String Name
        {
            get { return m_name; }
            set
            {
                SetPropertyValue(value, () => this.Name,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_name = (String)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The name of the group
        /// </summary>
        public ObservableCollection<ParsingError> Errors
        {
            get { return m_errors; }
            set
            {
                SetPropertyValue(value, () => this.Errors,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_errors = (ObservableCollection<ParsingError>)newValue;
                        }
                ));
            }
        }

        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        public ParsingErrorGroup(String name)
        {
            this.Name = name;
            this.Errors = new ObservableCollection<ParsingError>();
        }

        #endregion // Constructors

        #region Public Functions

        public Boolean ContainsError(String name)
        {
            foreach (ParsingError error in this.Errors)
            {
                if (error.Name == name)
                    return true;
            }

            return false;
        }

        public ParsingError GetError(String name)
        {
            foreach (ParsingError error in this.Errors)
            {
                if (error.Name == name)
                    return error;
            }

            return null;
        }

        #endregion // Public Functions
    } // ParsingErrorGroup
} // MetadataEditor.ParsingErrors
