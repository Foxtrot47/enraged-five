﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using RSG.Base.Logging;
using RSG.Base.Windows.Controls;
using Workbench.AddIn;
using Workbench.AddIn.Services.File;
using Workbench.AddIn.UI.Layout;
using RSG.Base.Editor;
using System.Collections.ObjectModel;
using Workbench.AddIn.Services;
using RSG.Base.Windows.DragDrop;
using System.Windows;
using RSG.Base.Windows.DragDrop.Utilities;
using System.Text;

namespace Workbench.UI
{
    /// <summary>
    /// Application log view model
    /// </summary>
    [ExportExtension("Workbench.UI.SearchResultWindowViewModel", typeof(SearchResultWindowViewModel))]
    public class SearchResultWindowViewModel : ViewModelBase
    {
        #region Classes
        /// <summary>
        /// 
        /// </summary>
        internal class SearchBookends : ISearchResult
        {
            #region Properties
            /// <summary>
            /// 
            /// </summary>
            public object Data
            {
                get { return null; }
            }

            /// <summary>
            /// Represents whether the user can jump to the search result
            /// to see it in the view.
            /// </summary>
            public bool CanJumpTo
            {
                get { return false; }
            }

            /// <summary>
            /// Represents what to show the user in the search results
            /// window for this result.
            /// </summary>
            public string DisplayText
            {
                get;
                set;
            }

            /// <summary>
            /// A reference to the content
            /// the search result was found in.
            /// </summary>
            public IContentBase Content
            {
                get;
                set;
            }
            #endregion

            #region Constructor
            /// <summary>
            /// 
            /// </summary>
            /// <param name="displayText"></param>
            public SearchBookends(string displayText)
            {
                this.DisplayText = displayText;
            }
            #endregion
        } // SearchBookends
        #endregion

        #region Fields
        public string completeString;
        #endregion

        #region Properties
        public ObservableCollection<ISearchResult> SearchOutput
        {
            get;
            set;
        }

        public string CompleteString
        {
            get { return this.completeString; }
            set { this.SetPropertyValue(ref this.completeString, value, "CompleteString"); }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        public SearchResultWindowViewModel()
        {
            this.SearchOutput = new ObservableCollection<ISearchResult>();
        }
        #endregion // Constructor

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="searchExpression"></param>
        public void StartNewSearch(string searchExpression)
        {
            this.SearchOutput.Clear();
            this.SearchOutput.Add(new SearchBookends(string.Format("Searching for: {0}", searchExpression)));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="searchExpression"></param>
        public void FinishSearch(List<ISearchResult> results)
        {
            SortedDictionary<string, List<ISearchResult>> sorted = new SortedDictionary<string, List<ISearchResult>>();
            foreach (var result in results)
            {
                if (!sorted.ContainsKey(result.DisplayText))
                    sorted.Add(result.DisplayText, new List<ISearchResult>());

                sorted[result.DisplayText].Add(result);
            }

            foreach (var resultArray in sorted.Values)
            {
                foreach (var result in resultArray)
                {
                    this.SearchOutput.Add(result);
                }
            }
            this.SearchOutput.Add(new SearchBookends(string.Format("Found {0} occurrence(s)", results.Count)));

            StringBuilder builder = new StringBuilder();
            foreach (ISearchResult result in this.SearchOutput)
            {
                builder.AppendLine(result.DisplayText);
            }

            this.CompleteString = builder.ToString();
        }
        #endregion
    } // SearchResultWindowViewModel

    /// <summary>
    /// 
    /// </summary>
    public class ResultDragHandler : IDragSource
    {
        public virtual Object StartDrag(DragInfo dragInfo)
        {
            int itemCount = dragInfo.SourceItems.Cast<Object>().Count();
            dragInfo.Effects = (dragInfo.Data != null) ? DragDropEffects.Copy | DragDropEffects.Move : DragDropEffects.None;

            if (itemCount == 1)
            {
                object data = dragInfo.SourceItems.Cast<Object>().First();
                if (data is ISearchResult)
                {
                    return (data as ISearchResult).Data;
                }

                return dragInfo.SourceItems.Cast<Object>().First();
            }
            else if (itemCount > 1)
            {
                List<object> data = new List<object>();
                foreach (object obj in dragInfo.SourceItems)
                {
                    if (data is ISearchResult)
                    {
                        data.Add((data as ISearchResult).Data);
                    }
                    else
                    {
                        data.Add(obj);
                    }
                }
                return data;
            }

            return null;
        }
    } // DefaultDragHandler

} // Workbench.UI namespace
