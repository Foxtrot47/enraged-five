﻿using System;
using System.ComponentModel.Composition;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using RSG.Base.Logging;
using RSG.Model.Common.Map;
using Workbench.AddIn;
using Workbench.AddIn.Services;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.Commands;

namespace LevelBrowser.AddIn.MapBrowser.Commands
{

    /// <summary>
    /// Command that opens the map section zip file.
    /// </summary>
    [ExportExtension(Extensions.MapSectionCommands, typeof(IWorkbenchCommand))]
    class OpenMapSectionCommand : WorkbenchMenuItemBase
    {
        #region Constants
        public static readonly String GUID = "F2176617-013D-4135-90C9-51D91AB83106";
        #endregion // Constants

        #region MEF Imports
        [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager, typeof(ILayoutManager))]
        private ILayoutManager LayoutManager { get; set; }

        [ImportExtension(Workbench.AddIn.CompositionPoints.MessageService, typeof(IMessageService))]
        private IMessageService MessageService { get; set; }
        #endregion // MEF Imports

        #region Constructor

        /// <summary>
        /// Default constructor.
        /// </summary>
        public OpenMapSectionCommand()
        {
            this.Header = "_Open";
            this.IsDefault = true;
            this.ID = new Guid(GUID);
        }

        #endregion // Constructor

        #region ICommand Implementation

        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
            if (parameter is IList)
            {
                foreach (Object section in (parameter as IList))
                {
                    Debug.Assert(section is IMapSection, "Invalid Map Section selection.");
                    this.OpenMapSection((IMapSection)section);
                }
            }
            else
            {
                Debug.Assert(parameter is IMapSection, "Invalid Map Section selection.");
                this.OpenMapSection((IMapSection)parameter);
            }
        }
        #endregion // ICommand Implementation

        #region Private Methods
        /// <summary>
        /// Open a map section zip file.
        /// </summary>
        /// <param name="section"></param>
        private void OpenMapSection(IMapSection section)
        {
            Debug.Assert(null != section, "Map section is null.  Zip cannot be opened.  Internal error.");
            if (null == section)
            {
                Log.Log__Error("Map section is null.  Zip cannot be opened.  Internal error.");
                return;
            }

            if (String.IsNullOrEmpty(section.ExportZipFilepath))
            {
                this.MessageService.Show(String.Format("Cannot open map section zip file as it's path is unknown.",
                    section.Name), MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else
            {
                if (System.IO.File.Exists(section.ExportZipFilepath))
                {
                    System.Diagnostics.Process.Start(section.ExportZipFilepath);
                }
                else
                {
                    String message = String.Format("Map section's export file does not exist on your computer.{0}{0}File: {1}.",
                        Environment.NewLine, section.ExportZipFilepath);
                    Log.Log__Error(message);
                    MessageService.Show(message, MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
        #endregion // Private Methods

    } // OpenMapSectionCommand

} // LevelBrowser.AddIn.MapBrowser.Commands
