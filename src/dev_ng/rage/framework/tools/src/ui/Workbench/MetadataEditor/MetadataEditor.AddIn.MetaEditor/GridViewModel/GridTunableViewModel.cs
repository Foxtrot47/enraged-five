﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using MetadataEditor.AddIn.MetaEditor.ViewModel;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using RSG.Metadata.Data;
using System.Windows;
using System.ComponentModel;
using MetadataEditor.AddIn.MetaEditor.BasicViewModel;
using System.Text.RegularExpressions;

namespace MetadataEditor.AddIn.MetaEditor.GridViewModel
{
    /// <summary>
    /// The view model that wraps around a 
    /// <see cref="ITunable"/> object. Used
    /// for the grid view.
    /// </summary>
    public class GridTunableViewModel : TunableViewModel, IWeakEventListener // ModelBase, IWeakEventListener
    {
        #region Fields

        /// <summary>
        /// The private field used for the <see cref="Cildren"/> property.
        /// </summary>
        private ObservableCollection<GridTunableViewModel> children;

        /// <summary>
        /// The private field used for the <see cref="IsExpanded"/> property.
        /// </summary>
        private bool isExpanded;

        /// <summary>
        /// The private field used for the <see cref="IsSelected"/> property.
        /// </summary>
        private bool isSelected;

        /// <summary>
        /// The private field used for the <see cref="FriendlyTypeName"/> property.
        /// </summary>
        private string friendlyTypeName;
        #endregion

        #region Properties

        /// <summary>
        /// Gets the command used to edit the array by drilling down into it.
        /// </summary>
        public RelayCommand EditCommand
        {
            get
            {
                if (editCommand == null)
                {
                    editCommand = new RelayCommand(OnEdit);
                }

                return editCommand;
            }
        }

        private RelayCommand editCommand;
        
        /// <summary>
        /// Gets or sets the friendly type name for this view model.
        /// </summary>
        public string FriendlyTypeName
        {
            get { return this.friendlyTypeName; }
            protected set
            {
                SetPropertyValue(value, this.friendlyTypeName, () => this.FriendlyTypeName,
                    new PropertySetDelegate(delegate(Object newValue)
                    {
                        this.friendlyTypeName = (string)newValue;
                    }
                    ));
            }
        }

        /// <summary>
        /// A observable collection of
        /// child objects for this view model.
        /// </summary>
        public ObservableCollection<GridTunableViewModel> Children
        {
            get { return children; }
            set
            {
                SetPropertyValue(value, children, () => this.Children,
                    new PropertySetDelegate(delegate(Object newValue)
                    {
                        children = (ObservableCollection<GridTunableViewModel>)newValue;
                    }
                    ));
            }
        }
        
        /// <summary>
        /// Gets the name of the wrapped <see cref="ITunable"/> object.
        /// </summary>
        public string Name
        {
            get
            {
                return this.Model.Name;
            }
        }

        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="MetadataEditor.AddIn.MetaEditor.GridViewModel.GridTunableViewModel"/>
        /// class.
        /// </summary>
        public GridTunableViewModel(GridTunableViewModel parent, ITunable m, MetaFileViewModel root)
            : base(parent, m, root)
        {
            this.friendlyTypeName = this.Model.Definition.FriendlyTypeName;
            this.children = new ObservableCollection<GridTunableViewModel>();

            if (RawModel is PointerTunable)
            {
                PropertyChangedEventManager.AddListener(RawModel.ParentModel as ITunable, this, "Value");
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public override List<ITunableViewModel> FindAll(Regex expression)
        {
            List<ITunableViewModel> found = new List<ITunableViewModel>();
            if (this is GridTunableViewModel)
            {
                foreach (var tunable in (this as GridTunableViewModel).Children)
                {
                    if (tunable.IsMatch(expression))
                    {
                        found.Add(tunable);
                    }

                    found.AddRange(tunable.FindAll(expression));
                }
            }

            return found;
        }

        public override ITunableViewModel FindPrevious(Regex expression)
        {
            return FindPrevious(expression, true, false);
        }

        public override ITunableViewModel FindPrevious(Regex expression, bool checkParents, bool checkChildren)
        {
            if (this.Parent is GridTunableViewModel && checkParents)
            {
                ITunableViewModel result = (this.Parent as GridTunableViewModel).FindPreviousInChildren(expression, this, true, true);
                if (result != null)
                    return result;
            }
            return null;
        }

        private ITunableViewModel FindPreviousInChildren(Regex expression, GridTunableViewModel start, bool checkParents, bool checkChildren)
        {
            bool canSearch = false;
            for (int i = this.Children.Count - 1; i >= 0; i--)
            {
                if (canSearch)
                {
                    if (checkChildren && this.Children[i] is GridTunableViewModel)
                    {
                        ITunableViewModel result = (this.Children[i] as GridTunableViewModel).FindPreviousInChildren(expression);
                        if (result != null)
                            return result;
                    }
                    if (this.Children[i].IsMatch(expression))
                        return this.Children[i];
                }
                else if (this.Children[i] == start)
                {
                    canSearch = true;
                }
            }

            if (this.Parent is GridTunableViewModel && checkParents)
            {
                ITunableViewModel result = (this.Parent as GridTunableViewModel).FindPreviousInChildren(expression, this, checkParents, checkChildren);
                if (result != null)
                    return result;
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="start"></param>
        /// <param name="checkParents"></param>
        /// <param name="checkChildren"></param>
        /// <returns></returns>
        internal ITunableViewModel FindPreviousInChildren(Regex expression)
        {
            for (int i = this.Children.Count - 1; i >= 0; i--)
            {
                if (this.Children[i] is GridTunableViewModel)
                {
                    ITunableViewModel result = (this.Children[i] as GridTunableViewModel).FindPreviousInChildren(expression);
                    if (result != null)
                        return result;
                }

                if (this.Children[i].IsMatch(expression))
                    return this.Children[i];
            }
            return null;
        }

        public override ITunableViewModel FindNext(Regex expression)
        {
            if (this is StructureTunableViewModel)
            {
                ITunableViewModel result = (this as StructureTunableViewModel).FindNextInChildren(expression);
                if (result != null)
                    return result;
            }
            else if (Parent!=null && Parent is StructureTunableViewModel)
            {
                if (Parent.Parent != null && Parent.Parent is ArrayTunableViewModel)
                {
                    var arrayVM = Parent.Parent as ArrayTunableViewModel;
                    int found = -2;
                    for (int i = 0; i < arrayVM.Children.Count; i++)
                    {
                        if (arrayVM.Children[i] == Parent)
                        {
                            found = i;
                            break;
                        }
                    }

                    found++;
                    if (found < 0)
                    {
                        return null;
                    }

                    for (int j = found; j < arrayVM.Children.Count; j++)
                    {
                        ITunableViewModel result = arrayVM.Children[found].FindNextInChildren(expression);
                        if (result != null)
                        {
                            return result;
                        }
                    }
                }
            }

            return null;
        }

        internal override ITunableViewModel FindNextInChildren(System.Text.RegularExpressions.Regex expression)
        {
            foreach (var child in Children)
            {
                if (child.IsMatch(expression))
                {
                    return child;
                }
            }

            return null;
        }

        /// <summary>
        /// Handles all weak event listener captures.
        /// </summary>
        public virtual bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
        {
            if (e is EditorPropertyChangedEventArgs)
            {
                EditorPropertyChangedEventArgs args = e as EditorPropertyChangedEventArgs;
                if (args.Model != args.OriginalModel)
                    return true;
            }

            return true;
        }

        /// <summary>
        /// Creates the list of commands that can be executed by the user in the view.
        /// </summary>
        /// <returns></returns>
        private List<CommandViewModel> CreateCommands()
        {
            return new List<CommandViewModel>
            {
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        private void OnEdit(object parameter)
        {
            if (RootViewModel == null)
            {
                return;
            }

            if (RootViewModel.BreadCrumbs.Contains(this))
            {
                return;
            }

            RootViewModel.BreadCrumbs.Add(this);
            RootViewModel.BreadCrumbSelection = this;
        }

        #endregion
    } // MetadataEditor.AddIn.MetaEditor.GridViewModel.GridTunableViewModel
} // MetadataEditor.AddIn.MetaEditor.GridViewModel
