﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Workbench.AddIn.MapStatistics.Overlays.RoadDensity
{
    /// <summary>
    /// Interaction logic for RoadDensityControl.xaml
    /// </summary>
    public partial class RoadDensityControl : UserControl
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public RoadDensityControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Button click event handler to re-draw the paths.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (DataContext is RoadDensityOverlay)
            {
                var overlay = DataContext as RoadDensityOverlay;
                overlay.Refresh();
            }
        }
    }
}
