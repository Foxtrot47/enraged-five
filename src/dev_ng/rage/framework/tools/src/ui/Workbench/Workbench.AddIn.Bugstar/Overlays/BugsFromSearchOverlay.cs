﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Collections;
using RSG.Interop.Bugstar.Search;
using RSG.Base.Logging;
using Workbench.AddIn.Bugstar.OverlayViews;
using RSG.Interop.Bugstar.Game;
using RSG.Model.Common.Map;

namespace Workbench.AddIn.Bugstar.Overlays
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(ExtensionPoints.Overlay, typeof(Viewport.AddIn.IViewportOverlay))]
    public class BugsFromSearchOverlay : BugsHeatMapOverlayBase
    {
        private Dictionary<IMapSection, int> m_bugCount;

        #region Constants
        private const string c_name = "Bugs from Search";
        private const string c_description = "Shows all the bugs that are part of the selected search.";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<Search> Searches    
        {
            get { return m_searches; }
            set
            {
                SetPropertyValue(value, () => Searches,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_searches = (ObservableCollection<Search>)newValue;
                        }
                ));
            }
        }
        private ObservableCollection<Search> m_searches;

        /// <summary>
        /// 
        /// </summary>
        public Search SelectedSearch
        {
            get { return m_selectedSearch; }
            set
            {
                SetPropertyValue(value, () => SelectedSearch,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_selectedSearch = (Search)newValue;
                            OnSelectedSearchChanged();
                        }
                ));
            }
        }
        private Search m_selectedSearch;
        #endregion
        
        #region Constructors
        /// <summary>
        /// Default Constructor
        /// </summary>
        public BugsFromSearchOverlay()
            : base(c_name, c_description, true, false)
        {
            Searches = new ObservableCollection<Search>();
            m_bugCount = new Dictionary<IMapSection, int>();
        }
        #endregion // Constructors

        #region Overrides
        /// <summary>
        /// Gets called when the user wants to view this overlay
        /// </summary>
        public override void Activated()
        {
            List<Search> sortedSearches = new List<Search>();
            sortedSearches.AddRange(BugstarService.GetUserSearches());
            sortedSearches.Sort();

            Searches.BeginUpdate();
            Searches.Clear();
            Searches.AddRange(sortedSearches);
            Searches.EndUpdate();

            base.Activated();
        }

        /// <summary>
        /// Gets called when the user turns off this overlay
        /// </summary>
        public override void Deactivated()
        {
            base.Deactivated();
        }

        /// <summary>
        /// Gets the control that should be shown in the overlay details panel
        /// </summary>
        public override System.Windows.Controls.Control GetOverlayControl()
        {
            return new BugSearchDetailsView();
        }
        #endregion // Overrides

        #region Event Callbacks
        /// <summary>
        /// 
        /// </summary>
        protected void OnSelectedSearchChanged()
        {
            Refresh();
        }
        #endregion // Event Callbacks

        #region BugOverlayBase Overrides
        /// <summary>
        /// 
        /// </summary>
        protected override void RefreshBugList()
        {
            // Get the new list of bugs
            BugList.Clear();
            SelectedBug = null;

            if (SelectedSearch != null)
            {
                try
                {
                    BugList.AddRange(SelectedSearch.GetBugs("Id,Summary,Description,X,Y,Z,Developer,Tester,State,Component,DueDate,Priority,Category,Grid"));
                }
                catch (System.Exception ex)
                {
                    Log.Log__Exception(ex, "Unexpected exception while attempting to retrieve the bug list for search '{0}'.", SelectedSearch.Name);
                }
            }
        }

        /// <summary>
        /// Get the user text that will be rendered over the top of the section.
        /// </summary>
        /// <param name="section">Section.</param>
        /// <param name="sectionGeometry">Section geometry.</param>
        /// <returns></returns>
        protected override string GetUserText(RSG.Model.Common.Map.IMapSection section, RSG.Base.Windows.Controls.WpfViewport2D.Geometry.Viewport2DShape sectionGeometry)
        {
            return String.Format("{0} {1} bug(s)", section.Name, m_bugCount[section]);
        }

        /// <summary>
        /// Get the statistic for the section.
        /// </summary>
        /// <param name="section">Section.</param>
        /// <param name="statistic">Statistic.</param>
        /// <returns></returns>
        protected override bool GetStatistic(RSG.Model.Common.Map.IMapSection section, out double statistic)
        {
            statistic = 0;

            MapGrid grid = MapGrids.FirstOrDefault(item => item.Name.ToLower() == section.Name.ToLower());
            if (grid == null)
            {
                return false;
            }

            int bugCount = 0;

            foreach (var bug in BugList)
            {
                if (bug.Grid == grid.Id)
                {
                    bugCount++;
                }
            }

            m_bugCount[section] = bugCount;
            statistic = bugCount;
            return true;
        }
        #endregion // Private Methods

    } // BugsFromSearchOverlay
}
