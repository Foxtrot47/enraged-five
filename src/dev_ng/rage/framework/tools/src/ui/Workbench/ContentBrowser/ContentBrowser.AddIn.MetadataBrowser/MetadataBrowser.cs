﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Windows.Media.Imaging;
using MetadataEditor.AddIn;
using RSG.Metadata.Data;
using RSG.Model.LiveEditing;
using Workbench.AddIn;
using Workbench.AddIn.Services.File;

namespace ContentBrowser.AddIn.MetadataBrowser
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(ContentBrowser.AddIn.ExtensionPoints.Browser, typeof(ContentBrowser.AddIn.BrowserBase))]
    class MetadataBrowser : 
        ContentBrowser.AddIn.BrowserBase,
        IPartImportsSatisfiedNotification
    {
        #region MEF Imports
        /// <summary>
        /// Metadata open service
        /// </summary>
        [ImportExtension(MetadataEditor.AddIn.CompositionPoints.LiveEditingService, typeof(ILiveEditingService))]
        private ILiveEditingService LiveEditingService { get; set; }
        #endregion // MEF Imports

        #region Properties
        /// <summary>
        /// The icon that will be displayed to represent this browser
        /// </summary>
        public override BitmapImage BrowserIcon
        {
            get
            {
                Uri uriSource = new Uri("pack://application:,,,/ContentBrowser.AddIn.MetadataBrowser;component/Resources/Images/BrowserIcon.png");
                return new BitmapImage(uriSource);
            }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default Constructor
        /// </summary>
        public MetadataBrowser()
            : base("Metadata Browser")
        {
            this.AssetHierarchy.Clear();
            this.ID = ContentBrowser.AddIn.CompositionPoints.MetadataBrowserGuid;
            this.RelativeID = ContentBrowser.AddIn.CompositionPoints.LevelBrowserGuid;
            this.Direction = Workbench.AddIn.Services.Direction.After;
        }
        #endregion // Constructor(s)

        #region IPartImportsSatisfiedNotification Interface
        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {
            LiveEditingService.RootServiceChanged += LiveEditingService_RootServiceChanged;
        }
        #endregion // IPartImportsSatisfiedNotification Interface
        
        #region BrowserBase Override Methods
        /// <summary>
        /// Override to create the commands that will be attached to
        /// this browser
        /// </summary>
        protected override ObservableCollection<ContentBrowser.AddIn.ContentBrowserCommand> CreateCommands()
        {
            ObservableCollection<ContentBrowser.AddIn.ContentBrowserCommand> commands = base.CreateCommands();
            commands.Add(this.CreateRefreshCommand());
            return commands;
        }

        #endregion // BrowserBase Override Methods

        #region Command Creation

        /// <summary>
        /// Creates the refresh command, used to refresh the level browser data
        /// </summary>
        private ContentBrowser.AddIn.ContentBrowserCommand CreateRefreshCommand()
        {
            Action<Object> execute = param => ExecuteRefreshServices();
            Predicate<Object> canExecute = param => CanExecuteRefreshServices();
            String toolTip = "Refresh Live Services";
            BitmapImage image = new BitmapImage(new Uri("pack://application:,,,/LevelBrowser;component/Resources/Images/refresh.png"));

            return new ContentBrowser.AddIn.ContentBrowserCommand(execute, canExecute, toolTip, image);
        }

        #endregion // Command Creation

        #region Command Callbacks
        /// <summary>
        /// 
        /// </summary>
        private Boolean CanExecuteRefreshServices()
        {
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        private void ExecuteRefreshServices()
        {
            LiveEditingService.Refresh();
        }
        #endregion // Command Callbacks
        
        #region Event handling
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void LiveEditingService_RootServiceChanged(object sender, EventArgs e)
        {
            // Always clear out the asset hierarchy
            AssetHierarchy.Clear();

            if (LiveEditingService.LiveEditRootService != null)
            {
                AssetHierarchy.Add(LiveEditingService.LiveEditRootService);
            }
        }
        #endregion // Event Handling
    } // MetadataBrowser
} // ContentBrowser.AddIn.LevelBrowser
