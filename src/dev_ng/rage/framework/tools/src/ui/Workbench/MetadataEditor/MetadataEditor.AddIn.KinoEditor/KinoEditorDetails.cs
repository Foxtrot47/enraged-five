﻿using System;
using System.Reflection;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Workbench.AddIn;
using Workbench.AddIn.UI;

namespace MetadataEditor.AddIn.KinoEditor
{

    /// <summary>
    /// Metadata Editor Icon
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.PluginInformation,
        typeof(IPluginDetails))]
    class KinoEditorDetails : IPluginDetails
    {
        #region Constants
        /// <summary>
        /// 
        /// </summary>
        private static readonly String AUTHOR = "Mike Wilson <mike.wilson@rockstarnorth.com>";
        #endregion // Constants

        #region Properties and Associated Member Data
        /// <summary>
        /// 
        /// </summary>
        public BitmapSource Image
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public String Title
        {
            get { return this.AssInfo.Title; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String Version
        {
            get { return this.AssInfo.FileVersion; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String Author
        {
            get { return AUTHOR; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String Copyright
        {
            get { return this.AssInfo.Copyright; }
        }
        #endregion // Properties and Associated Member Data

        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private RSG.Base.Reflection.AssemblyInfo AssInfo;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public KinoEditorDetails()
        {
            this.AssInfo = new RSG.Base.Reflection.AssemblyInfo(
                Assembly.GetExecutingAssembly());

#warning FIX ME
            Uri uri = new Uri("Data/MetadataEditiorIcon.png", UriKind.Relative);
            this.Image = new BitmapImage(uri);
        }
        #endregion // Constructor(s)
    }

} // MetadataEditor.AddIn.KinoEditor namespace
