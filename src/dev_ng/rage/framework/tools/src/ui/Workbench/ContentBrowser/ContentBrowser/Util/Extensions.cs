﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Collections;
using System.Windows;

namespace ContentBrowser.Util
{
    /// <summary>
    /// Helper extension methods
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        public static void ClearThreaded<T>(this ObservableCollection<T> collection)
        {
            if (Application.Current != null)
            {
                Application.Current.Dispatcher.Invoke(
                    new Action
                    (
                        delegate()
                        {
                            lock (collection)
                            {
                                collection.Clear();
                            }
                        }
                    ),
                    System.Windows.Threading.DispatcherPriority.ContextIdle
                );
            }
            else
            {
                collection.Clear();
            }
        }

        /// <summary>
        /// Helper for invoking an add of an item to the passed in collection on the UI thread
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <param name="item"></param>
        public static void AddThreaded<T>(this ObservableCollection<T> collection, T item)
        {
            if (Application.Current != null)
            {
                Application.Current.Dispatcher.Invoke(
                    new Action
                    (
                        delegate()
                        {
                            lock (collection)
                            {
                                collection.Add(item);
                            }
                        }
                    ),
                    System.Windows.Threading.DispatcherPriority.ContextIdle
                );
            }
            else
            {
                collection.Add(item);
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <param name="items"></param>
        public static void AddRangeThreaded<T>(this ObservableCollection<T> collection, IEnumerable<T> items)
        {
            AddRangeThreaded(collection, items, false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <param name="items"></param>
        /// <param name="clear"></param>
        public static void AddRangeThreaded<T>(this ObservableCollection<T> collection, IEnumerable<T> items, bool clear)
        {
            if (Application.Current != null)
            {
                Application.Current.Dispatcher.Invoke(
                    new Action
                    (
                        delegate()
                        {
                            lock (collection)
                            {
                                collection.BeginUpdate();
                                if (clear)
                                {
                                    collection.Clear();
                                }
                                collection.AddRange(items);
                                collection.EndUpdate();
                            }
                        }
                    ),
                    System.Windows.Threading.DispatcherPriority.ContextIdle
                );
            }
            else
            {
                collection.BeginUpdate();
                if (clear)
                {
                    collection.Clear();
                }
                collection.AddRange(items);
                collection.EndUpdate();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <param name="item"></param>
        public static void RemoveThreaded<T>(this ObservableCollection<T> collection, T item)
        {
            if (Application.Current != null)
            {
                Application.Current.Dispatcher.Invoke(
                    new Action
                    (
                        delegate()
                        {
                            lock (collection)
                            {
                                collection.Remove(item);
                            }
                        }
                    ),
                    System.Windows.Threading.DispatcherPriority.ContextIdle
                );
            }
            else
            {
                collection.Remove(item);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <param name="items"></param>
        public static void RemoveRangeThreaded<T>(this ObservableCollection<T> collection, IEnumerable<T> items)
        {
            if (Application.Current != null)
            {
                Application.Current.Dispatcher.Invoke(
                    new Action
                    (
                        delegate()
                        {
                            lock (collection)
                            {
                                collection.BeginUpdate();
                                foreach (T item in items)
                                {
                                    collection.Remove(item);
                                }
                                collection.EndUpdate();
                            }
                        }
                    ),
                    System.Windows.Threading.DispatcherPriority.ContextIdle
                );
            }
            else
            {
                collection.BeginUpdate();
                foreach (T item in items)
                {
                    collection.Remove(item);
                }
                collection.EndUpdate();
            }
        }
    }
}
