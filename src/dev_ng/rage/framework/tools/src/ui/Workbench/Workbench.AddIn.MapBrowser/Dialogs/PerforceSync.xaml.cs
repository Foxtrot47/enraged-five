﻿using System;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RSG.Base.Editor.Command;
using RSG.Base.Logging;
using Workbench.AddIn.Services;

namespace Workbench.AddIn.MapBrowser.Dialogs
{
    /// <summary>
    /// Interaction logic for PerforceSync.xaml
    /// </summary>
    public partial class PerforceSync : Window
    {
        public class SyncEntry : RSG.Base.Editor.ViewModelBase
        {
            public Boolean Sync
            {
                get { return m_sync; }
                set
                {
                    SetPropertyValue(value, () => this.Sync,
                        new PropertySetDelegate(delegate(Object newValue) { m_sync = (Boolean)newValue; }));
                }
            }
            private Boolean m_sync;

            public String Name { get; set; }

            public String FolderLocation { get; set; }

            public String SyncPath { get; set; }

            public String Size { get; set; }

            public int SizeValue { get; set; }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="sync"></param>
            /// <param name="name"></param>
            /// <param name="folderLocation"></param>
            /// <param name="syncPath"></param>
            public SyncEntry(Boolean sync, String name, String folderLocation, String syncPath, String size)
            {
                Sync = sync;
                Name = name;
                FolderLocation = folderLocation;
                SyncPath = syncPath;
                this.Size = String.Empty;
                this.SizeValue = 0;

                int bytes = 0;
                if (int.TryParse(size, out bytes) == true)
                {
                    this.SizeValue = bytes;
                    if (bytes < 1024 * 1024)
                    {
                        int kbytes = bytes >> 10;
                        int kbyte = 1024;
                        int leftover = bytes - (kbytes * kbyte);

                        if (leftover > 0)
                        {
                            this.Size = String.Format("{0} KB", ((double)bytes / (double)kbyte).ToString("F1"));
                        }
                        else
                        {
                            this.Size = String.Format("{0} KB", bytes >> 10);
                        }
                    }
                    else
                    {
                        int mbytes = bytes >> 20;
                        int mbyte = 1024 * 1024;
                        int leftover = bytes - (mbytes * mbyte);

                        if (leftover > 0)
                        {
                            this.Size = String.Format("{0} MB", ((double)bytes / (double)mbyte).ToString("F1"));
                        }
                        else
                        {
                            this.Size = String.Format("{0} MB", bytes >> 20);
                        }
                    }
                }
            }
        }

        public String Message
        {
            get { return m_message; }
            set { m_message = value; }
        }
        private String m_message;

        public IConfigurationService Config
        {
            get;
            set;
        }

        public RSG.SourceControl.Perforce.P4 PerforceObject
        {
            get { return m_perforceObject; }
            set { m_perforceObject = value; }
        }
        private RSG.SourceControl.Perforce.P4 m_perforceObject = null;

        public Boolean GettingLatest
        {
            get { return (Boolean)GetValue(GettingLatestProperty); }
            set { SetValue(GettingLatestProperty, value); OnPropertyChanged(new DependencyPropertyChangedEventArgs(GettingLatestProperty, !(Boolean)value, value)); }
        }
        public static DependencyProperty GettingLatestProperty = DependencyProperty.Register("GettingLatest", typeof(Boolean), typeof(PerforceSync),
            new PropertyMetadata(false));

        public ObservableCollection<SyncEntry> SyncFiles { get; set; }

        public PerforceSync(P4API.P4RecordSet syncResult)
        {
            InitializeComponent();
            this.Message = "\nYou currently have " + syncResult.Records.Length.ToString() + " level data files out of date on your machine.\n" +
                "Would you like to grab latest now?\n\n" +
                "You can select which files to grab using the table below.\n";

            this.SyncFiles = new ObservableCollection<SyncEntry>();
            foreach (var record in syncResult.Records)
            {
                String depotFile = record["depotFile"];
                String size = record["fileSize"];
                this.SyncFiles.Add(new SyncEntry(true, System.IO.Path.GetFileName(depotFile), System.IO.Path.GetDirectoryName(depotFile), depotFile, size));
            }
        }

        private void GetLatest_Click(Object sender, RoutedEventArgs e)
        {
            this.GettingLatest = true;

            String currentDirectory = System.IO.Directory.GetCurrentDirectory();
            try
            {
                System.IO.Directory.SetCurrentDirectory(System.IO.Path.Combine(Config.GameConfig.ExportDir, "levels"));
                PerforceObject.Connect();
            }
            catch (P4API.Exceptions.P4APIExceptions ex)
            {
                System.IO.Directory.SetCurrentDirectory(currentDirectory);
                RSG.Base.Logging.Log.Log__Exception(ex, "Unhandled Perforce exception");
            }

            BackgroundWorker backgroundWorker = new BackgroundWorker();
            backgroundWorker.WorkerReportsProgress = true;

            backgroundWorker.DoWork += new DoWorkEventHandler
            (
                delegate(Object o, DoWorkEventArgs args)
                {
                    RSG.Base.Logging.Log.Enabled = false;
                    this.GrabLatest(currentDirectory, backgroundWorker);
                    RSG.Base.Logging.Log.Enabled = true;
                }
            );

            backgroundWorker.ProgressChanged += new ProgressChangedEventHandler
            (
                delegate(Object o, ProgressChangedEventArgs args)
                {
                    if (this.GrabProgress != null)
                    {
                        this.GrabProgress.Value = args.ProgressPercentage;
                    }
                }
            );

            backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler
            (
                delegate(Object o, RunWorkerCompletedEventArgs args)
                {
                    System.IO.Directory.SetCurrentDirectory(currentDirectory);
                    PerforceObject.Disconnect();
                    PerforceObject = null;

                    this.DialogResult = true;
                }
            );

            backgroundWorker.RunWorkerAsync();
        }

        private void GrabLatest(String currentDirectory, BackgroundWorker worker)
        {
            try
            {
                int totalSyncCount = 0;
                foreach (SyncEntry entry in this.SyncFiles)
                {
                    if (entry.Sync == true)
                    {
                        totalSyncCount++;
                    }
                }

                int i = 0;
                int percentageComplete = 0;
                foreach (SyncEntry entry in this.SyncFiles)
                {
                    if (!entry.Sync)
                        continue;

                    try
                    {
                        percentageComplete = (int)(System.Math.Ceiling((100.0 / totalSyncCount) * i));
                        worker.ReportProgress(percentageComplete);
                        PerforceObject.Run("sync", entry.SyncPath);
                        i++;
                    }
                    catch (P4API.Exceptions.P4APIExceptions)
                    {
                        Log.Log__Warning("Perforce error syncing: {0}.", entry.SyncPath);
                    }
                }
            }
            catch (P4API.Exceptions.P4APIExceptions ex)
            {
                System.IO.Directory.SetCurrentDirectory(currentDirectory);
                Log.Log__Exception(ex, "Unhandled Perforce exception");
            }
        }

        private void Continue_Click(Object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CheckBox_Click(Object sender, RoutedEventArgs e)
        {
            CheckBox checkBox = sender as CheckBox;
            if (checkBox.IsChecked == true)
            {
                foreach (SyncEntry entry in this.SyncFiles)
                {
                    entry.Sync = true;
                }
            }
            else
            {
                foreach (SyncEntry entry in this.SyncFiles)
                {
                    entry.Sync = false;
                }
            }
        }

        private void Cancel_Click(Object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private GridViewColumnHeader CurrentSortedColumn = null;
        private ListSortDirection SortDirection = ListSortDirection.Descending;
        private void SortClick(Object sender, RoutedEventArgs e)
        {
            GridViewColumnHeader column = sender as GridViewColumnHeader;
            String field = column.Tag as String;

            if (CurrentSortedColumn == null)
            {
                CurrentSortedColumn = column;
                SortDirection = ListSortDirection.Descending;
            }
            else
            {
                if (CurrentSortedColumn == column)
                {
                    if (SortDirection == ListSortDirection.Descending)
                    {
                        SortDirection = ListSortDirection.Ascending;
                    }
                    else
                    {
                        SortDirection = ListSortDirection.Descending;
                    }
                }
                else
                {
                    CurrentSortedColumn = column;
                    SortDirection = ListSortDirection.Descending;
                }
            }

            if (field == "Name")
            {
                SortNames();
            }
            else if (field == "Size")
            {
                SortSize();
            }
            else if (field == "Folder")
            {
                SortFolder();
            }
            else if (field == "Sync")
            {
                SortSync();
            }
        }

        private void SortNames()
        {
            SortedDictionary<String, Collection<SyncEntry>> sorted = new SortedDictionary<String, Collection<SyncEntry>>();

            foreach (SyncEntry entry in this.SyncFiles)
            {
                if (!sorted.ContainsKey(entry.Name))
                {
                    sorted.Add(entry.Name, new Collection<SyncEntry>());
                }
                sorted[entry.Name].Add(entry);
            }

            if (SortDirection == ListSortDirection.Descending)
            {
                SyncFiles.Clear();
                foreach (KeyValuePair<String, Collection<SyncEntry>> sortedNames in sorted)
                {
                    foreach (SyncEntry entry in sortedNames.Value)
                    {
                        SyncFiles.Add(entry);
                    }
                }
            }
            else
            {
                SyncFiles.Clear();
                foreach (KeyValuePair<String, Collection<SyncEntry>> sortedNames in sorted.Reverse())
                {
                    foreach (SyncEntry entry in sortedNames.Value.Reverse())
                    {
                        SyncFiles.Add(entry);
                    }
                }
            }
        }

        private void SortSize()
        {
            SortedDictionary<int, Collection<SyncEntry>> sorted = new SortedDictionary<int, Collection<SyncEntry>>();

            foreach (SyncEntry entry in this.SyncFiles)
            {
                if (!sorted.ContainsKey(entry.SizeValue))
                {
                    sorted.Add(entry.SizeValue, new Collection<SyncEntry>());
                }
                sorted[entry.SizeValue].Add(entry);
            }

            if (SortDirection == ListSortDirection.Descending)
            {
                SyncFiles.Clear();
                foreach (KeyValuePair<int, Collection<SyncEntry>> sortedSizes in sorted)
                {
                    foreach (SyncEntry entry in sortedSizes.Value)
                    {
                        SyncFiles.Add(entry);
                    }
                }
            }
            else
            {
                SyncFiles.Clear();
                foreach (KeyValuePair<int, Collection<SyncEntry>> sortedSizes in sorted.Reverse())
                {
                    foreach (SyncEntry entry in sortedSizes.Value.Reverse())
                    {
                        SyncFiles.Add(entry);
                    }
                }
            }
        }

        private void SortFolder()
        {
            SortedDictionary<String, Collection<SyncEntry>> sorted = new SortedDictionary<String, Collection<SyncEntry>>();

            foreach (SyncEntry entry in this.SyncFiles)
            {
                if (!sorted.ContainsKey(entry.FolderLocation))
                {
                    sorted.Add(entry.FolderLocation, new Collection<SyncEntry>());
                }
                sorted[entry.FolderLocation].Add(entry);
            }

            if (SortDirection == ListSortDirection.Descending)
            {
                SyncFiles.Clear();
                foreach (KeyValuePair<String, Collection<SyncEntry>> sortedFolders in sorted)
                {
                    foreach (SyncEntry entry in sortedFolders.Value)
                    {
                        SyncFiles.Add(entry);
                    }
                }
            }
            else
            {
                SyncFiles.Clear();
                foreach (KeyValuePair<String, Collection<SyncEntry>> sortedFolders in sorted.Reverse())
                {
                    foreach (SyncEntry entry in sortedFolders.Value.Reverse())
                    {
                        SyncFiles.Add(entry);
                    }
                }
            }
        }

        private void SortSync()
        {
            SortedDictionary<Boolean, Collection<SyncEntry>> sorted = new SortedDictionary<Boolean, Collection<SyncEntry>>();

            foreach (SyncEntry entry in this.SyncFiles)
            {
                if (!sorted.ContainsKey(entry.Sync))
                {
                    sorted.Add(entry.Sync, new Collection<SyncEntry>());
                }
                sorted[entry.Sync].Add(entry);
            }

            if (SortDirection == ListSortDirection.Descending)
            {
                SyncFiles.Clear();
                foreach (KeyValuePair<Boolean, Collection<SyncEntry>> sortedSyncs in sorted)
                {
                    foreach (SyncEntry entry in sortedSyncs.Value)
                    {
                        SyncFiles.Add(entry);
                    }
                }
            }
            else
            {
                SyncFiles.Clear();
                foreach (KeyValuePair<Boolean, Collection<SyncEntry>> sortedSyncs in sorted.Reverse())
                {
                    foreach (SyncEntry entry in sortedSyncs.Value.Reverse())
                    {
                        SyncFiles.Add(entry);
                    }
                }
            }
        }

    }


}
