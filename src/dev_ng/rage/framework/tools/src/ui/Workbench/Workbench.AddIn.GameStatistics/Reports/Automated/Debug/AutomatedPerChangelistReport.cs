﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Report;
using System.ComponentModel.Composition;
using RSG.Base.ConfigParser;
using RSG.Base.Tasks;
using RSG.SourceControl.Perforce;

namespace Workbench.AddIn.GameStatistics.Reports.Automated.Debug
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(ExtensionPoints.AutomatedDebugReport, typeof(IReport))]
    public class AutomatedPerChangelistReport : RSG.Model.Report.Reports.GameStatsReports.AutomatedPerChangelistReport, IPartImportsSatisfiedNotification
    {
        #region MEF Imports
        /// <summary>
        /// MEF import for login service.
        /// </summary>
        [ImportExtension(PerforceBrowser.AddIn.CompositionPoints.PerforceService, typeof(PerforceBrowser.AddIn.IPerforceService))]
        public PerforceBrowser.AddIn.IPerforceService PerforceService { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(Workbench.AddIn.Services.IConfigurationService))]
        protected Workbench.AddIn.Services.IConfigurationService Config { get; set; }
        #endregion // MEF Imports

        #region Properties
        /// <summary>
        /// Whether the email should be sent when in debug mode.
        /// </summary>
        public bool DebugSendEmail
        {
            get;
            set;
        }

        /// <summary>
        /// Email address to send the email to in debug mode.
        /// </summary>
        public string DebugEmailAddress
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public AutomatedPerChangelistReport()
            : base()
        {
            DebugSendEmail = true;
            DebugEmailAddress = "michael.taschler@rockstarnorth.com";
        }
        #endregion // Constructor(s)

        #region AutomatedPerChangelistReport Overrides
        /// <summary>
        /// Override the base class implementation to actually send the mail.
        /// </summary>
        /// <param name="gv"></param>
        protected override void GenerateReport(DynamicReportContext context, IProgress<TaskProgress> progress)
        {
            base.GenerateReport(context, progress);

#if DEBUG
            if (DebugSendEmail)
            {
                // Setup the email address of people to send this to.
                MailMessage.To.Clear();
                MailMessage.To.Add(DebugEmailAddress);

                // Send the message
                System.Net.Mail.SmtpClient smtpClient = new System.Net.Mail.SmtpClient(Config.GameConfig.LogMailServerHost, Config.GameConfig.LogMailServerPort);
                smtpClient.Send(MailMessage);
            }
#endif
        }
        #endregion // AutomatedPerChangelistReport Overrides

        #region IPartImportsSatisfiedNotification
        /// <summary>
        /// Called when a part's imports have been satisfied and it is safe to use.
        /// </summary>
        public void OnImportsSatisfied()
        {
            ReportsConfig = Config.ReportsConfig;
        }
        #endregion // IPartImportsSatisfiedNotification

        #region Methods
        protected override P4 GetPerforceConnection()
        {
            return PerforceService.PerforceConnection;
        }
        #endregion
    } // AutomatedPerChangelistReport
}
