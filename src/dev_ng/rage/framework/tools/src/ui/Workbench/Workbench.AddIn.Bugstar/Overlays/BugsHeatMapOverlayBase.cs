﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MapViewport.AddIn;
using RSG.Interop.Bugstar.Game;
using RSG.Model.Common.Map;
using Workbench.AddIn.Services;
using WidgetEditor.AddIn;
using RSG.Base.Collections;
using RSG.Interop.Bugstar;
using RSG.Interop.Bugstar.Common;
using RSG.Base.Windows.Helpers;
using RSG.Base.Math;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using System.Collections.Specialized;
using System.Threading;
using RSG.Base.Windows.Dialogs;
using RSG.Base.Tasks;
using RSG.Model.Common;
using System.Windows.Media;
using Workbench.AddIn.Services.Model;

namespace Workbench.AddIn.Bugstar.Overlays
{
    [ExportExtension(ExtensionPoints.Overlay, typeof(Viewport.AddIn.IViewportOverlay))]
    public abstract class BugsHeatMapOverlayBase : HeatMapLevelViewportOverlay
    {
        #region Constants
        private const String RAG_WARPCOORDINATES = "Debug/Warp Player x y z h vx vy vz";
        private const String RAG_WARPNOWBUTTON = "Debug/Warp now";

        /// <summary>
        /// Maximum number of dynamic objects to display in the viewport.  Any more than this and the bugs get rendered to texture instead.
        /// </summary>
        private const int c_maxDynamicViewportObjects = 1000;

        /// <summary>
        /// Size rtio of the main map that the render to texture image is.
        /// </summary>
        private const float c_renderToTextureImageRatio = 0.3333f;
        #endregion // Constants
        
        protected List<MapGrid> MapGrids { get; private set; }

        #region MEF Imports
        /// <summary>
        /// A reference to view model for the map viewport
        /// </summary>
        [ImportExtension(Viewport.AddIn.CompositionPoints.MapViewport, typeof(Viewport.AddIn.IMapViewport))]
        Lazy<Viewport.AddIn.IMapViewport> MapViewport { get; set; }

        /// <summary>
        /// Content Browser
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LevelBrowser, typeof(ILevelBrowser))]
        protected ILevelBrowser LevelBrowserProxy { get; set; }

        /// <summary>
        /// Configuration service
        /// </summary>
        [ImportExtension(CompositionPoints.ConfigurationService, typeof(IConfigurationService))]
        protected Lazy<IConfigurationService> Config { get; set; }

        /// <summary>
        /// MEF import for login service.
        /// </summary>
        [ImportExtension(CompositionPoints.LoginService, typeof(ILoginService))]
        Lazy<ILoginService> LoginService { get; set; }

        /// <summary>
        /// MEF import for proxy UI
        /// </summary>
        [ImportExtension(WidgetEditor.AddIn.CompositionPoints.ProxyService, typeof(IProxyService))]
        public Lazy<IProxyService> ProxyService { get; set; }

        /// <summary>
        /// MEF import for message box service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.MessageService, typeof(Workbench.AddIn.Services.IMessageService))]
        private Lazy<IMessageService> MessageService { get; set; }

        /// <summary>
        /// Bugstar Service MEF import.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.BugstarService, typeof(IBugstarService))]
        protected IBugstarService BugstarService { get; set; }
        #endregion // MEF Imports

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name">Overlay name.</param>
        /// <param name="description">Description.</param>
        /// <param name="useLogScale">True if a logarithmic scale is to be used.</param>
        /// <param name="delaySectionData">True if the intent is to delay-load the individual sections until they are selected.</param>
        public BugsHeatMapOverlayBase(string name, string description, bool useLogScale, bool delaySectionData)
            : base(name, description, false, useLogScale, !delaySectionData)
        {
            FilteredBugList = new ObservableCollection<Bug>();
            BugList = new List<Bug>();
            this.DataSourceModes[DataSource.Database] = true;
        }

        public override void Activated()
        {
            // Add the callback to respond to the selected bug changing
            MapViewport.Value.SelectedOverlayGeometry.CollectionChanged += SelectedOverlayGeometryChanged;

            uint bugstarLevelId = Config.Value.BugstarConfig.GetLevelId(LevelBrowserProxy.SelectedLevel.Name);

            Map currentMap = null;
            if (BugstarService.Project != null)
            {
                currentMap = Map.GetMapById(BugstarService.Project, bugstarLevelId);
            }

            if (currentMap != null)
            {
                MapGrids = new List<MapGrid>();
                MapGrids.AddRange(currentMap.Grids.Where(grid => grid.Active));
            }
            
            base.Activated();

            if (CategoryFilters == null)
            {
                CategoryFilters = GetCategoryList();
            }
        }
        

        #region Public Properties
        /// <summary>
        /// List of bugs that the overlay is currently showing
        /// </summary>
        public ObservableCollection<Bug> FilteredBugList
        {
            get { return m_filteredBugList; }
            set
            {
                SetPropertyValue(value, () => FilteredBugList,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_filteredBugList = (ObservableCollection<Bug>)newValue;
                        }
                ));
            }
        }
        private ObservableCollection<Bug> m_filteredBugList;

        /// <summary>
        /// The bug that is currently selected by the user
        /// </summary>
        public Bug SelectedBug
        {
            get { return m_selectedBug; }
            set
            {
                SetPropertyValue(value, () => SelectedBug,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_selectedBug = (Bug)newValue;
                        }
                ));
            }
        }
        private Bug m_selectedBug;

        /// <summary>
        /// Retrieves a list of bug categories
        /// </summary>
        public ObservableCollection<BugCategoryFilter> CategoryFilters
        {
            get { return m_categoryFilters; }
            set
            {
                SetPropertyValue(value, () => CategoryFilters,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_categoryFilters = (ObservableCollection<BugCategoryFilter>)newValue;
                        }
                ));
            }
        }
        private ObservableCollection<BugCategoryFilter> m_categoryFilters;
        #endregion // Public Properties

        #region Protected Properties
        /// <summary>
        /// 
        /// </summary>
        protected List<Bug> BugList
        {
            get;
            set;
        }
        #endregion // Private Properties

        #region Commands
        /// <summary>
        /// Centers the map around all the bugs in the current bug list
        /// </summary>
        public RelayCommand CenterMapOnBugsCommand
        {
            get
            {
                if (m_centerMapOnBugsCommand == null)
                {
                    m_centerMapOnBugsCommand = new RelayCommand(param => CenterMapOnBugList(), param => BugsExist());
                }

                return m_centerMapOnBugsCommand;
            }
        }
        private RelayCommand m_centerMapOnBugsCommand;

        /// <summary>
        /// Centers the map around all the bugs in the current bug list
        /// </summary>
        public RelayCommand CenterMapOnSelectedCommand
        {
            get
            {
                if (m_centerMapOnSelectedCommand == null)
                {
                    m_centerMapOnSelectedCommand = new RelayCommand(param => CenterMapOnBug(), param => IsBugSelected());
                }

                return m_centerMapOnSelectedCommand;
            }
        }
        private RelayCommand m_centerMapOnSelectedCommand;

        /// <summary>
        /// Command that gets fired off when the "Warp to in Game" button is pressed for the selected map instance
        /// </summary>
        public RelayCommand WarpToInGameCommand
        {
            get
            {
                if (m_warpToInGameCommand == null)
                {
                    m_warpToInGameCommand = new RelayCommand(param => WarpToInGame(), param => IsBugSelected());
                }

                return m_warpToInGameCommand;
            }
        }
        private RelayCommand m_warpToInGameCommand;

        /// <summary>
        /// Command for selecting the previous bug
        /// </summary>
        public RelayCommand PreviousBugCommand
        {
            get
            {
                if (m_previousBugCommand == null)
                {
                    m_previousBugCommand = new RelayCommand(param => SelectPreviousBug(), param => CanChangeBug());
                }

                return m_previousBugCommand;
            }
        }
        private RelayCommand m_previousBugCommand;

        /// <summary>
        /// Command for selecting the next bug
        /// </summary>
        public RelayCommand NextBugCommand
        {
            get
            {
                if (m_nextBugCommand == null)
                {
                    m_nextBugCommand = new RelayCommand(param => SelectNextBug(), param => CanChangeBug());
                }

                return m_nextBugCommand;
            }
        }
        private RelayCommand m_nextBugCommand;
        #endregion // Commands

        #region Command Callbacks
        protected override void OnDrawSectionsChanged()
        {
            this.Refresh();
        }

        /// <summary>
        /// 
        /// </summary>
        private void CenterMapOnBugList()
        {
            // Calculate the center/bounding box of all the bugs
            Vector2f center = new Vector2f(0.0f, 0.0f);
            BoundingBox2f bbox = new BoundingBox2f();

            foreach (Bug bug in FilteredBugList)
            {
                Vector2f bugLocation = new Vector2f(bug.Location.X, bug.Location.Y);
                center += bugLocation;
                bbox.Expand(bugLocation);
            }
            center /= FilteredBugList.Count;

            // Center the viewport first, then zoom in on the collection of bugs
            MapViewport.Value.CenterViewportOn(center);
            MapViewport.Value.ZoomViewportToFit(bbox);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool BugsExist()
        {
            return FilteredBugList.Count > 0;
        }

        /// <summary>
        /// 
        /// </summary>
        private void CenterMapOnBug()
        {
            Vector2f bugLocation = new Vector2f(SelectedBug.Location.X, SelectedBug.Location.Y);
            BoundingBox2f bbox = new BoundingBox2f();
            bbox.Expand(bugLocation);

            // Center the viewport then zoom into the bug
            MapViewport.Value.CenterViewportOn(bugLocation);
            MapViewport.Value.ZoomViewportToFit(bbox);
        }

        /// <summary>
        /// 
        /// </summary>
        private void WarpToInGame()
        {
            Vector3f position = new Vector3f(SelectedBug.Location);
            position.X -= 2.0f;
            position.Z += 1.0f;

            float heading = -(float)Math.PI / 2.0f;

            // Check whether the RAG proxy is connected
            if (!ProxyService.Value.IsGameConnected)
            {
                MessageService.Value.Show("RAG is not connected. Is the game running?");
            }
            else
            {
                ProxyService.Value.Console.WriteStringWidget(RAG_WARPCOORDINATES, String.Format("{0} {1} {2} {3}", position.X, position.Y, position.Z, heading));
                ProxyService.Value.Console.PressWidgetButton(RAG_WARPNOWBUTTON);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool IsBugSelected()
        {
            return SelectedBug != null;
        }

        /// <summary>
        /// 
        /// </summary>
        private void SelectPreviousBug()
        {
            Bug bugToSelect = null;

            if (SelectedBug == null)
            {
                bugToSelect = FilteredBugList.First();
            }
            else
            {
                int currentIdx = FilteredBugList.IndexOf(SelectedBug);
                int nextIdx = (currentIdx - 1); ;
                if (nextIdx < 0)
                {
                    nextIdx = FilteredBugList.Count - 1;
                }
                bugToSelect = FilteredBugList[nextIdx];
            }

            MapViewport.Value.ClearSelectedGeometry();
            if (bugToSelect != null)
            {
                Viewport2DGeometry geometryToSelect = Geometry.FirstOrDefault(item => item.PickData == bugToSelect);
                if (geometryToSelect != null)
                {
                    MapViewport.Value.SetGeometryAsSelected(geometryToSelect);
                    SelectedBug = bugToSelect;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void SelectNextBug()
        {
            Bug bugToSelect = null;

            if (SelectedBug == null)
            {
                bugToSelect = FilteredBugList.First();
            }
            else
            {
                int currentIdx = FilteredBugList.IndexOf(SelectedBug);
                int nextIdx = (currentIdx + 1) % FilteredBugList.Count;
                bugToSelect = FilteredBugList[nextIdx];
            }

            MapViewport.Value.ClearSelectedGeometry();
            if (bugToSelect != null)
            {
                Viewport2DGeometry geometryToSelect = Geometry.FirstOrDefault(item => item.PickData == bugToSelect);
                if (geometryToSelect != null)
                {
                    MapViewport.Value.SetGeometryAsSelected(geometryToSelect);
                    SelectedBug = bugToSelect;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool CanChangeBug()
        {
            return (FilteredBugList.Count > 1 || (SelectedBug == null && FilteredBugList.Count > 0));
        }
        #endregion // Command Callbacks

        #region Overrides
        /// <summary>
        /// Gets called when the user turns off this overlay
        /// </summary>
        public override void Deactivated()
        {
            base.Deactivated();
            MapViewport.Value.SelectedOverlayGeometry.CollectionChanged -= SelectedOverlayGeometryChanged;
            SelectedBug = null;
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RefreshSectionData()
        {
        }
        #endregion // Overrides

        #region Event Callbacks
        /// <summary>
        /// Gets called when map geometry selection changes
        /// </summary>
        private void SelectedOverlayGeometryChanged(Object sender, NotifyCollectionChangedEventArgs e)
        {
            if (sender is RSG.Base.Collections.ObservableCollection<object>)
            {
                RSG.Base.Collections.ObservableCollection<object> collection = sender as RSG.Base.Collections.ObservableCollection<object>;

                if (collection.Count == 1)
                {
                    SelectedBug = collection[0] as Bug;
                }
                else
                {
                    SelectedBug = null;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FilterChanged(object sender, EventArgs e)
        {
            RefreshFilteredBugList();
            RenderMap();
        }
        #endregion // Event Callbacks

        #region Abstract Methods
        /// <summary>
        /// Function used to update the current bug list
        /// </summary>
        //protected abstract void RefreshBugList();
        #endregion // Abstract Methods

        #region Protected Methods
        /// <summary>
        /// Updates the geometry that is displayed based on the current bug list
        /// </summary>
        protected void Refresh()
        {
            SelectedBug = null;

            CancellationTokenSource cts = new CancellationTokenSource();
            TaskContext context = new TaskContext(cts.Token);
            ITask requestDataTask = new ActionTask("Requesting Data from Bugstar", (ctx, progress) => RefreshBugList());
            requestDataTask.ReportsProgress = false;

            TaskExecutionDialog dialog = new TaskExecutionDialog();
            TaskExecutionViewModel vm = new TaskExecutionViewModel("Updating Overlay", requestDataTask, cts, context);
            vm.AutoCloseOnCompletion = true;
            dialog.DataContext = vm;
            if (dialog.ShowDialog() != false)
            {
                RefreshFilteredBugList();
                RefreshSectionData();
                RenderMap();
            }
        }

        /// <summary>
        /// Get the text to be displayed over the section.
        /// </summary>
        /// <param name="section">Section.</param>
        /// <param name="sectionGeometry">Geometry.</param>
        /// <returns>The text to be displayed over the section.</returns>
        protected override string GetUserText(RSG.Model.Common.Map.IMapSection section, RSG.Base.Windows.Controls.WpfViewport2D.Geometry.Viewport2DShape sectionGeometry)
        {
            return String.Format("{0} {1} bug(s)", section.Name, HeatMap.GetStatistic(section));
        }

        /// <summary>
        /// Updates the filtered bug list
        /// </summary>
        protected void RefreshFilteredBugList()
        {
            FilteredBugList.BeginUpdate();
            FilteredBugList.Clear();

            foreach (Bug bug in BugList)
            {
                bool include = true;

                BugCategoryFilter helper = CategoryFilters.FirstOrDefault(item => item.Category == bug.Category);
                if (helper == null || !helper.Enabled)
                {
                    include = false;
                }

                if (include)
                {
                    FilteredBugList.Add(bug);
                }
            }
            FilteredBugList.EndUpdate();

            // If the previously selected bug isn't in the filter anymore, deselect it
            if (!FilteredBugList.Contains(SelectedBug))
            {
                SelectedBug = null;
            }
        }

        /// <summary>
        /// Update the geometry based on the currently filtered bugs
        /// </summary>
        protected override void OnMapRendered()
        {
            if (FilteredBugList.Count > c_maxDynamicViewportObjects)
            {
#warning DHM FIX ME: I spy with my little eye, magic fucking numbers!
                RSG.Base.Math.Vector2i size = new RSG.Base.Math.Vector2i(966, 627);
                RSG.Base.Math.Vector2f pos = new RSG.Base.Math.Vector2f(-483.0f, 627.0f);
                ILevel level = LevelBrowserProxy.SelectedLevel;
                if (level.ImageBounds != null)
                {
                    size = new RSG.Base.Math.Vector2i((int)(level.ImageBounds.Max.X - level.ImageBounds.Min.X), (int)(level.ImageBounds.Max.Y - level.ImageBounds.Min.Y));
                    pos = new RSG.Base.Math.Vector2f(level.ImageBounds.Min.X, level.ImageBounds.Max.Y);
                }
                Viewport2DImageOverlay image = new Viewport2DImageOverlay(etCoordSpace.World, "", new RSG.Base.Math.Vector2i((int)(size.X * c_renderToTextureImageRatio), (int)(size.Y * c_renderToTextureImageRatio)), pos, new RSG.Base.Math.Vector2f(size.X, size.Y));
                image.BeginUpdate();

                foreach (Bug bug in FilteredBugList)
                {
                    image.RenderCircle(new Vector2f(bug.Location.X, bug.Location.Y), 10.0f, System.Drawing.Color.Green);
                }

                image.EndUpdate();

                Geometry.Add(image);
            }
            else
            {
                foreach (Bug bug in FilteredBugList)
                {
                    Viewport2DCircle newGeometry = new Viewport2DCircle(etCoordSpace.World, "Filled shape for bug", new Vector2f(bug.Location.X, bug.Location.Y), 10.0f, Colors.Green);
                    newGeometry.PickData = bug;
                    Geometry.Add(newGeometry);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void RefreshBugList()
        {
            // Get the new list of bugs
            BugList.Clear();
            GetSelectedMapData();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private ObservableCollection<BugCategoryFilter> GetCategoryList()
        {
            ObservableCollection<BugCategoryFilter> categories = new ObservableCollection<BugCategoryFilter>();

            foreach (BugCategory category in Enum.GetValues(typeof(BugCategory)))
            {
                BugCategoryFilter filter = new BugCategoryFilter(category);
                filter.FilterChanged += FilterChanged;
                categories.Add(filter);
            }

            return categories;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private ObservableCollection<BugPriority> GetPriorityList()
        {
            ObservableCollection<BugPriority> priorities = new ObservableCollection<BugPriority>();
            priorities.AddRange(Enum.GetValues(typeof(BugPriority)).Cast<BugPriority>());
            return priorities;
        }
        #endregion // Private Methods
    }
}
