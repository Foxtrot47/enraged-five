﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using RSG.Base.Logging;
using RSG.Base.Network;
using Editor.AddIn;
using Editor.Workbench.AddIn;
using Editor.Workbench.AddIn.UI.Layout;

namespace Editor.Workbench.AddIn.RAG
{

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Editor.Workbench.AddIn.ExtensionPoints.Document,
        typeof(IDocument))]
    [ExportExtension(Editor.Workbench.AddIn.RAG.CompositionPoints.RAG,
        typeof(WidgetViewModel))]
    [Document(Name = WidgetViewModel.DOCUMENT_NAME)]
    class WidgetViewModel : DocumentBase
    {
        #region Constants
        public const String DOCUMENT_NAME = "RAG";
        private const String TITLE = "RAG";
        private const String DEFAULT_HOSTNAME = "localhost";
        #endregion // Constants

        #region Properties and Associated Member Data
        /// <summary>
        /// 
        /// </summary>
        public String Hostname
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<cRemoteConsole.WidgetEntry> Widgets
        {
            get;
            private set;
        }
        #endregion // Properties and Associated Member Data

        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        public cRemoteConsole gameConnection = null;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public WidgetViewModel()
        {
            this.Name = DOCUMENT_NAME;
            this.Title = TITLE;
            this.Hostname = DEFAULT_HOSTNAME;
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        public bool Connect()
        {
            try
            {
                gameConnection = new cRemoteConsole();
                bool result = (gameConnection.Connect(this.Hostname));

                if (result)
                {
                    List<cRemoteConsole.WidgetEntry> widgets = gameConnection.ListWidgets("/");
                    this.Widgets = new ObservableCollection<cRemoteConsole.WidgetEntry>(widgets);
                }
                return (result);
            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex, "Unhandled exception connecting to {0}.", this.Hostname);
            }
            return (false);
        }
        #endregion // Controller Methods
    }

} // Editor.Workbench.AddIn.RAG namespace
