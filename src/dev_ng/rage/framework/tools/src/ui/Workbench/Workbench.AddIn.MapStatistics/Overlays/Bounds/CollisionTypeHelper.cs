﻿using RSG.Bounds;

namespace Workbench.AddIn.MapStatistics.Overlays.Bounds
{
    /// <summary>
    /// Helper class for a bounds collision type
    /// </summary>
    public class CollisionTypeHelper
    {
        public BNDFile.CollisionType Type { get; set; }
        public string Name { get; set; }
        public bool Enabled { get; set; }

        public CollisionTypeHelper(BNDFile.CollisionType type)
            : this(type, false)
        {
        }

        public CollisionTypeHelper(BNDFile.CollisionType type, bool enabled)
        {
            Type = type;
            Name = type.ToString();
            Enabled = enabled;
        }
    } // CollisionTypeHelper

}
