﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using SIO = System.IO;
using System.Windows.Media.Imaging;
using RSG.Metadata.Model;
using RSG.Metadata.Parser;

namespace MetadataEditor.AddIn.DefinitionEditor.ViewModel
{
    /// <summary>
    /// 
    /// </summary>
    public class NamespaceViewModel :
        RSG.Base.Editor.HierarchicalViewModelBase
    {
        #region Properties
        /// <summary>
        /// Modified property.
        /// </summary>
        public bool Modified { get; set; }

        /// <summary>
        /// Structure model data.
        /// </summary>
        public Namespace Model { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public new ObservableCollection<Object> Children { get; set; }

        #endregion // Properties

        #region Constructor(s)

        public NamespaceViewModel()
        {
            this.Model = new Namespace();
            this.Children = new ObservableCollection<Object>();
        }

        public NamespaceViewModel(Namespace m)
        {
            this.Model = m;
            this.Children = new ObservableCollection<Object>();
            SortedDictionary<String, List<NamespaceViewModel>> sortedNamespaces = new SortedDictionary<String, List<NamespaceViewModel>>();
            SortedDictionary<String, List<StructureViewModel>> sortedStructures = new SortedDictionary<String, List<StructureViewModel>>();
            if (m != null)
            {
                foreach (Object child in m.Children)
                {
                    if (child is Namespace)
                    {
                        NamespaceViewModel nvm = new NamespaceViewModel(child as Namespace);
                        if (!sortedNamespaces.ContainsKey(nvm.Model.Name))
                        {
                            sortedNamespaces.Add(nvm.Model.Name, new List<NamespaceViewModel>());
                        }
                        sortedNamespaces[nvm.Model.Name].Add(nvm);
                    }
                    else if (child is Structure)
                    {
                        StructureViewModel svm = new StructureViewModel(child as Structure, this);
                        if (svm.Model.ShortDataType != null)
                        {
                            if (!sortedStructures.ContainsKey(svm.Model.ShortDataType))
                            {
                                sortedStructures.Add(svm.Model.ShortDataType, new List<StructureViewModel>());
                            }
                            sortedStructures[svm.Model.ShortDataType].Add(svm);
                        }
                    }
                }
                foreach (List<NamespaceViewModel> nvmList in sortedNamespaces.Values)
                {
                    foreach (NamespaceViewModel nvm in nvmList)
                        this.Children.Add(nvm);
                }
                foreach (List<StructureViewModel> svmList in sortedStructures.Values)
                {
                    foreach (StructureViewModel svm in svmList)
                        this.Children.Add(svm);
                }
            }
        }

        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="datatypeOrNamespace"></param>
        /// <returns></returns>
        public NamespaceViewModel Find(String datatypeOrNamespace)
        {
            String ns = Namespace.GetNamespaceName(datatypeOrNamespace);
            if (datatypeOrNamespace.Equals(this.Model.FullName) || ns.Equals(this.Model.FullName))
                return (this);

            foreach (Object o in this.Children)
            {
                if (!(o is NamespaceViewModel))
                    continue;

                NamespaceViewModel cn = (o as NamespaceViewModel).Find(ns);
                if (null != cn)
                    return (cn);
            }
            return (null);
        }
        #endregion // Controller Methods

        #region Static Controller Methods
        /// <summary>
        /// Return new namespace chain for a String datatype identifier.
        /// </summary>
        /// <param name="datatype"></param>
        /// <returns></returns>
        public static NamespaceViewModel Create(String datatype, NamespaceViewModel parent)
        {
            Debug.Assert(!String.IsNullOrWhiteSpace(datatype));
            if (String.IsNullOrWhiteSpace(datatype))
                throw new ArgumentException();

            String friendlyDatatype = Namespace.ConvertDataTypeToFriendlyName(datatype);
            String[] parts = friendlyDatatype.Split(new String[] { Namespace.NAMESPACE_SEPARATOR_FRIENDLY },
                StringSplitOptions.RemoveEmptyEntries);
            Debug.Assert(parts.Length > 0);

            NamespaceViewModel root = new NamespaceViewModel(new Namespace(parts[0], parent.Model));
            NamespaceViewModel partParent = root;
            NamespaceViewModel child = root;
            for (int n = 1; n < parts.Length; n++)
            {
                child = new NamespaceViewModel(new Namespace(parts[n], partParent.Model));
                partParent.Children.Add(child);
                partParent = child;
            }
            return (child);
        }

        #endregion // Static Controller Methods
    } // NamespaceViewModel
} // RSG.Metadata.ViewModel namespace