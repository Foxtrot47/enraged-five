﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Report;
using RSG.Base.Tasks;
using Workbench.AddIn.Services;

namespace Workbench.AddIn.MapDebugging.Reports
{
    [ExportExtension(ExtensionPoints.Report, typeof(IReport))]
    public class ScriptModelMapInstanceReport : RSG.Model.Report.Reports.ScriptReports.ScriptModelMapInstanceReport
    {
        private int count = 0;

        #region MEF Imports
        /// <summary>
        /// The perforce sync service
        /// </summary>
        [Workbench.AddIn.ImportExtension(Workbench.AddIn.CompositionPoints.PerforceSyncService, typeof(Workbench.AddIn.Services.IPerforceSyncService))]
        private Lazy<Workbench.AddIn.Services.IPerforceSyncService> PerforceSyncService { get; set; }

        /// <summary>
        /// Progress service for displaying while gathering the data
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ProgressService, typeof(Workbench.AddIn.Services.IProgressService))]
        private IProgressService ProgressService { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(IConfigurationService))]
        private IConfigurationService Config { get; set; }
        #endregion // MEF Imports


        /// <summary>
        /// Generate report.
        /// </summary>
        /// <param name="context">Context.</param>
        /// <param name="progress">Progress.</param>
        protected override void GenerateReport(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            List<String> perforceFiles = new List<String>();
            perforceFiles.Add(Config.GameConfig.ScriptDir + @"\...");

            int syncedFiles = 0;
            PerforceSyncService.Value.Show("You current have a number of script files that are out of date.\n" +
                                           "Would you like to grab latest now?\n\n" +
                                           "You can select which files to grab using the table below.\n",
                                           perforceFiles.ToArray(), ref syncedFiles, false, true);

            base.GenerateReport(context, progress);
        }

    }
}
