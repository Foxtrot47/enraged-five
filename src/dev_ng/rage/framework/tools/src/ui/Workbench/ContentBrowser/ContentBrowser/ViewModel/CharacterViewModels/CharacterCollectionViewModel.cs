﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;

namespace ContentBrowser.ViewModel.CharacterViewModels
{
    /// <summary>
    /// 
    /// </summary>
    public class CharacterCollectionViewModel : ObservableContainerViewModelBase<ICharacterCategoryCollection>
    {
        #region Properties
        /// <summary>
        /// The number of viewmodels to create before displaying them on the
        /// screen. Less than or equal to 0 implies no batching
        /// </summary>
        protected override uint AssetCreationBatchSize
        {
            get
            {
                return 5;
            }
        }

        /// <summary>
        /// The number of viewmodels to create before displaying them on the
        /// screen. Less than or equal to 0 implies no batching
        /// </summary>
        protected override uint GridCreationBatchSize
        {
            get
            {
                return 5;
            }
        }
        #endregion // Properties

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public CharacterCollectionViewModel(ICharacterCollection characters)
            : base(characters, characters.CharacterCategories)
        {
        }
        #endregion // Constructor(s)
    } // CharacterCollectionViewModel
}
