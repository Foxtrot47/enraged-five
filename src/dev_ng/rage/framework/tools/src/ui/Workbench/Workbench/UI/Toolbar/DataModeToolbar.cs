﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using Workbench.AddIn;
using Workbench.AddIn.Commands;
using Workbench.AddIn.Services;
using System;

namespace Workbench.UI.Toolbar
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.ToolbarTray, typeof(IToolbar))] 
    public class DataModeToolbar : ToolBarBase, IPartImportsSatisfiedNotification
    {
        #region MEF Imports
        /// <summary>
        /// Window menu subitems.
        /// </summary>
        [ImportManyExtension(Workbench.AddIn.ExtensionPoints.DataModeToolbar, typeof(IWorkbenchCommand))]
        private IEnumerable<IWorkbenchCommand> ImportedItems { get; set; }
        #endregion // MEF Imports
        
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public DataModeToolbar()
        {
        }
        #endregion // Constructor(s)

        #region IPartImportsSatisfiedNotification Methods
        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {
            List<IWorkbenchCommand> allItems = new List<IWorkbenchCommand>();
            allItems.AddRange(this.SortToolBarItems(ImportedItems));
            this.Items = allItems;
        }

        #endregion // IPartImportsSatisfiedNotification Methods
    } // ContentBrowserToolbar


    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.DataModeToolbar, typeof(IWorkbenchCommand))]
    class DataSourceLabel : WorkbenchCommandLabel
    {
        #region Constants

        public static readonly Guid GUID = new Guid("9088A62A-AFD6-4F8E-9C02-AAA34B59105F");

        #endregion // Constants

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public DataSourceLabel()
            : base()
        {
            this.ID = GUID;
            this.Header = "Data Source:";
        }

        #endregion // Constructor(s)

        #region Public Members

        public void SetPlacement(Guid relative, Direction direction)
        {
            this.RelativeID = relative;
            this.Direction = direction;
        }

        #endregion // Public Members
    } // ContentBrowserToolbarDataSourceLabel

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.DataModeToolbar, typeof(IWorkbenchCommand))]
    class BuildLabel : WorkbenchCommandLabel
    {
        #region Constants

        public static readonly Guid GUID = new Guid("25C66F82-310D-4253-8472-D91D34BE51CB");

        #endregion // Constants

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public BuildLabel()
            : base()
        {
            this.ID = GUID;
            this.Header = "Build:";
            this.RelativeToolBarID = DataSourceCombobox.GUID;
            this.Direction = Direction.After;
        }

        #endregion // Constructor(s)

        #region Public Members

        public void SetPlacement(Guid relative, Direction direction)
        {
            this.RelativeID = relative;
            this.Direction = direction;
        }

        #endregion // Public Members
    } // ContentBrowserToolbarBuildLabel

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.DataModeToolbar, typeof(IWorkbenchCommand))]
    class LevelLabel : WorkbenchCommandLabel
    {
        #region Constants

        public static readonly Guid GUID = new Guid("0FA9E5E5-55ED-423D-A631-C8C489C53DA4");

        #endregion // Constants

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public LevelLabel()
            : base()
        {
            this.ID = GUID;
            this.Header = "Level:";
            this.RelativeToolBarID = BuildWarningImage.GUID;
            this.Direction = Direction.After;
        }

        #endregion // Constructor(s)

        #region Public Members

        public void SetPlacement(Guid relative, Direction direction)
        {
            this.RelativeID = relative;
            this.Direction = direction;
        }

        #endregion // Public Members
    } // ContentBrowserToolbarBuildLabel

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.DataModeToolbar, typeof(IWorkbenchCommand))]
    class PlatformLabel : WorkbenchCommandLabel
    {
        #region Constants

        public static readonly Guid GUID = new Guid("6F317FAB-ECE4-4F3E-A827-621831CEC90B");

        #endregion // Constants

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public PlatformLabel()
            : base()
        {
            this.ID = GUID;
            this.Header = "Platform:";
            this.RelativeToolBarID = LevelCombobox.GUID;
            this.Direction = Direction.After;
        }

        #endregion // Constructor(s)

        #region Public Members

        public void SetPlacement(Guid relative, Direction direction)
        {
            this.RelativeID = relative;
            this.Direction = direction;
        }

        #endregion // Public Members
    } // ContentBrowserToolbarPlatformLabel

} // ContentBrowser.UI.Toolbar
