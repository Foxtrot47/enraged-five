namespace Workbench.AddIn.TXDParentiser.Settings
{
    using System;
    using System.Collections.Generic;
    using Viewport.AddIn;
    using Workbench.AddIn.Services;
    using Workbench.AddIn.TXDParentiser.AddIn;

    [ExportExtension(Workbench.AddIn.ExtensionPoints.Settings, typeof(ISettings))]
    [ExportExtension(Viewport.AddIn.CompositionPoints.ParentizerSettings, typeof(IParentiserSettings))]
    public class ParentiserSettings : SettingsBase, IParentiserSettings
    {
        #region Backing data

        private string _gtxdZipDefaultName;
        private string _gtxdMetaDefaultName;
        private Dictionary<string, string> _gtxdZipName;
        private Dictionary<string, string> _gtxdMetaName;
        private Dictionary<string, string> _gtxdPrefix;

        #endregion



        #region Properties

        [System.ComponentModel.DisplayName("Gtxd Zip File Name")]
        [System.ComponentModel.Category("Parentiser")]
        [System.ComponentModel.Description("The Gtxd Zip File Name.")]
        public string GtxdZipName
        {
            get
            {
                return Properties.Settings1.Default.GtxdZipName;
            }
            set
            {
                Properties.Settings1.Default.GtxdZipName = value;
            }
        }

        [System.ComponentModel.DisplayName("Gtxd Meta File Name")]
        [System.ComponentModel.Category("Parentiser")]
        [System.ComponentModel.Description("The Gtxd Meta File Name.")]
        public string GtxdMetaName
        {
            get
            {
                return Properties.Settings1.Default.GtxdMetaName;
            }
            set
            {
                Properties.Settings1.Default.GtxdMetaName = value;
            }
        }

        [System.ComponentModel.DisplayName("Source Txd Prefix")]
        [System.ComponentModel.Category("Parentiser")]
        [System.ComponentModel.Description("The Source Txd Prefix.")]
        public string SourceTxdPrefix
        {
            get
            {
                return Properties.Settings1.Default.SourceTxdPrefix;
            }
            set
            {
                Properties.Settings1.Default.SourceTxdPrefix = value;
            }
        }
        #endregion // Properties

        #region Constructor

        public ParentiserSettings() :
            base("Parentiser Settings")
        {
        }
        #endregion

        #region Methods
        /// <summary>
        /// Loads settings
        /// </summary>
        public void Load()
        {
        }

        /// <summary>
        /// Applies settings
        /// </summary>
        public void Apply()
        {
            Properties.Settings1.Default.Save();
        }

        /// <summary>
        /// Checks its current state for any invalid data.
        /// </summary>
        /// <param name="errors">The service will fill this up with one string per 
        /// error it finds in its data.</param>
        /// <returns>true if everything is fine, false if an error was found</returns>
        public bool Validate(List<String> errors)
        {
            // No validation needed
            return true;
        }
        #endregion // Methods

    }
}