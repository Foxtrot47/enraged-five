﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using System.IO;

namespace ContentBrowser.ViewModel.GridViewModels
{
    /// <summary>
    /// 
    /// </summary>
    class TextureGridViewModel : GridViewModelBase
    {
        #region Members

        private ITexture m_model;

        #endregion // Members

        #region Properties

        /// <summary>
        /// A IAsset reference to the model the view model
        /// is representing
        /// </summary>
        public new ITexture Model
        {
            get { return m_model; }
            set
            {
                if (m_model == value)
                    return;

                SetPropertyValue(value, () => Model,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_model = (ITexture)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The main name property which by default
        /// will be shown as the header for a browser item
        /// </summary>
        public override String Name
        {
            get { return this.Model.Name; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Filename
        {
            get
            {
                return (File.Exists(Model.Filename) ? Model.Filename : null);
            }
        }

        #endregion // Properties

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public TextureGridViewModel(ITexture texture)
            :base(texture)
        {
            this.Model = texture;
        }

        /// <summary>
        /// Default destructor
        /// </summary>
        ~TextureGridViewModel()
        {

        }

        #endregion // Constructor
    } // TextureGridViewModel
} // ContentBrowser.ViewModel.GridViewModels
