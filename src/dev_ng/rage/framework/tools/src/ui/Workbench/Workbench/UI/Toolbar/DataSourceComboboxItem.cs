﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Extensions;
using RSG.Model.Common;
using Workbench.AddIn.Commands;

namespace Workbench.UI.Toolbar
{
    /// <summary>
    /// 
    /// </summary>
    public class DataSourceComboboxItem : WorkbenchCommandLabel
    {
        #region Constants
        /// <summary>
        /// 
        /// </summary>
        public static readonly Guid GUID = new Guid("0B1BDB02-54F7-4BA7-A499-C0E6B9CE80AE");
        #endregion // Constants

        #region Properties
        /// <summary>
        /// DataSource this label is for.
        /// </summary>
        public DataSource Source { get; protected set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="platform"></param>
        public DataSourceComboboxItem(DataSource source)
            : base()
        {
            this.ID = GUID;
            this.Header = source.GetDescriptionValue();
            Source = source;
        }
        #endregion // Constructor(s)
    } // DataSourceComboboxItem
} // Workbench.UI.Toolbar
