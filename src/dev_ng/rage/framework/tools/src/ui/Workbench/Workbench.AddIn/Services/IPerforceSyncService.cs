﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Workbench.AddIn.Services
{
    /// <summary>
    /// The different return results for the perforce sync window
    /// </summary>
    public enum PerforceSyncResult
    {
        /// <summary>
        /// Returns if the user has opted to continue with or
        /// without first getting latest.
        /// </summary>
        Ok,

        /// <summary>
        /// Returns if the user has opted to continue and 
        /// has synced with all the given files. This is also
        /// returned if none of the given files needed syncing
        /// </summary>
        SyncedAll,

        /// <summary>
        /// Returns if the user has opted to continue and 
        /// has synced with some of the given files but not all.
        /// </summary>
        SyncedSome,

        /// <summary>
        /// Returns if the user has opted to continue but
        /// hasn't synced with any file.
        /// </summary>
        SyncedNone,

        /// <summary>
        /// Returns if the user has opted to cancel the
        /// perforce sync by either pressing the cancel button
        /// or the close button on the popup window.
        /// </summary>
        Cancelled,

        /// <summary>
        /// Returns if there was an exception during the 
        /// perforce operation, if so use the syncedFilesCount
        /// parameter to determine how many files were synced to
        /// before the exception.
        /// </summary>
        Failed
    }

    public interface IPerforceSyncService
    {
        /// <summary>
        /// Shows the perforce sync dialog window that will allow the user to choose which
        /// files from the given set to sync to if any of them need to be synced to.
        /// </summary>
        /// <param name="message">The message you wish to display above the grid showing
        /// the user which files need syncing</param>
        /// <param name="filenames">An array of filenames that you would like perforce to
        /// check for you</param>
        /// <param name="syncedFilesCount">This parameter will be set to the number of files
        /// that were sycned to during the operation</param>
        /// <param name="forceSync">If this parameter is false it means that the user
        /// has the option to continue without syncing to any files else it means that the user has to
        /// sync to all the given files.</param>
        /// <returns></returns>
        PerforceSyncResult Show(String message, IEnumerable<String> filenames, ref int syncedFilesCount, Boolean forceSync, Boolean allowCancel);
    }
}
