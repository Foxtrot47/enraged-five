﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Metadata.Model;

namespace MetadataEditor.AddIn
{
    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public interface IMetadataViewModel
    {
        bool IsLiveEditing { get; }

        /// <summary>
        /// Enables the live-link without performing any additional fluff
        /// </summary>
        /// <param name="liveLinkUrl"></param>
        void EnableLiveLink(Uri liveLinkUrl);

        /// <summary>
        /// Enable the live-link replacing the local file with the passed in file
        /// </summary>
        /// <param name="newFile"></param>
        void EnableLiveLink(Uri liveLinkUrl, IMetaFile newFile);

        /// <summary>
        /// Enable the live-link and push all the local data to the game (if so requested
        /// </summary>
        /// <param name="liveLinkUrl"></param>
        /// <param name="sendDataToGame"></param>
        void EnableLiveLink(Uri liveLinkUrl, bool sendDataToGame);

        /// <summary>
        /// 
        /// </summary>
        void DisableLiveLink();

        /// <summary>
        /// Gets the model behind the view model as a MetaFile.
        /// </summary>
        /// <returns></returns>
        MetaFile GetMetaFile();
    }
}
