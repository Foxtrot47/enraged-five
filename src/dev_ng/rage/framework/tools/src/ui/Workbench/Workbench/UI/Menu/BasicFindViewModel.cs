﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using RSG.Base.Logging;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using Workbench.AddIn;
using Workbench.AddIn.Services;
using Workbench.AddIn.UI.Layout;

namespace Workbench.UI.Menu
{
    #region Enums
    public enum SearchArea
    {
        CurrentDocument,
        CurrentWindow,
        AllOpenDocuments,
        EntireWorkbench,
    }

    /// <summary>
    /// 
    /// </summary>
    public enum SearchParameter
    {
        None = 0,
        FindNext,
        FindPrevious,
        FindAll,
    }
    #endregion // Enums


    /// <summary>
    /// 
    /// </summary>
    public class BasicFindViewModel :
        ViewModelBase
    {
        #region Constants
        public const String BASICFIND_NAME = "Find_View";
        public const String BASICFIND_TITLE = "Quick Find";
        #endregion // Constants

        #region Fields
        private SearchArea m_searchArea;
        private ILayoutManager m_layoutManger;
        private IMessageService m_messageService;
        private Boolean m_activeDocument;
        private Boolean m_activeWindow;
        private IContentBase m_activeContent;
        private IContentBase m_findContent;
        private FindCommand m_findNextCommand;
        private FindCommand m_findPreviousCommand;
        private FindCommand m_findAllCommand;
        private String m_findText;
        private String m_tag;
        private Boolean m_matchCase;
        private Boolean m_useTag;
        private Boolean m_matchWholeWord;
        private Boolean m_useRegularExpressions;
        private IContentBase m_currentContent = null;
        private int m_contentIndex = 0;
        private object m_searchStartObject = null;
        private List<IContentBase> m_documents = new List<IContentBase>();
        private List<IContentBase> m_windows = new List<IContentBase>();
        private Object m_previousResult = null;
        private string m_currentWindow;
        private string m_currentDocument;
        private SearchParameter m_currentSearchParamter;
        private bool m_expressionError;
        #endregion // Fields

        #region Properties
        public SearchArea SearchArea
        {
            get { return m_searchArea; }
            set
            {
                SetPropertyValue(value, m_searchArea, () => this.SearchArea,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_searchArea = (SearchArea)newValue;
                            m_currentContent = null;
                            m_contentIndex = 0;
                            m_previousResult = null;
                        }
                    ));
            }
        }

        private ILayoutManager LayoutManager
        {
            get { return m_layoutManger; }
            set { m_layoutManger = value; }
        }

        private IMessageService MessageService
        {
            get { return m_messageService; }
            set { m_messageService = value; }
        }

        public Boolean ActiveDocument
        {
            get { return m_activeDocument; }
            set
            {
                SetPropertyValue(value, m_activeDocument, () => this.ActiveDocument,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_activeDocument = (Boolean)newValue;
                        }
                    ));
            }
        }

        public Boolean ActiveWindow
        {
            get { return m_activeWindow; }
            set
            {
                SetPropertyValue(value, m_activeWindow, () => this.ActiveWindow,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_activeWindow = (Boolean)newValue;
                        }
                    ));
            }
        }

        private IContentBase ActiveContent
        {
            get { return m_activeContent; }
            set { m_activeContent = value; }
        }

        internal IContentBase FindContent
        {
            get { return m_findContent; }
            set { m_findContent = value; this.OnActiveContentChanged(this.LayoutManager, EventArgs.Empty); }
        }

        public String FindText
        {
            get { return m_findText; }
            set
            {
                SetPropertyValue(value, m_findText, () => this.FindText,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_findText = (String)newValue;
                            m_previousResult = null;
                        }
                    ));
            }
        }

        public String Tag
        {
            get { return m_tag; }
            set
            {
                SetPropertyValue(value, m_tag, () => this.Tag,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_tag = (String)newValue;
                        }
                    ));
            }
        }

        public FindCommand FindNextCommand
        {
            get
            {
                if (m_findNextCommand == null)
                {
                    m_findNextCommand = new FindCommand(Find, CanExecuteFindCommand);
                }
                return m_findNextCommand;
            }
        }

        public FindCommand FindPreviousCommand
        {
            get
            {
                if (m_findPreviousCommand == null)
                {
                    m_findPreviousCommand = new FindCommand(Find, CanExecuteFindCommand);
                }
                return m_findPreviousCommand;
            }
        }

        public FindCommand FindAllCommand
        {
            get
            {
                if (m_findAllCommand == null)
                {
                    m_findAllCommand = new FindCommand(Find, CanExecuteFindCommand);
                }
                return m_findAllCommand;
            }
        }
  
        public Boolean MatchCase
        {
            get { return m_matchCase; }
            set
            {
                SetPropertyValue(value, m_matchCase, () => this.MatchCase,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_matchCase = (Boolean)newValue;
                        }
                    ));
            }
        }

        public Boolean UseTag
        {
            get { return m_useTag; }
            set
            {
                SetPropertyValue(value, m_useTag, () => this.UseTag,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_useTag = (Boolean)newValue;
                        }
                    ));
            }
        }

        public Boolean MatchWholeWord
        {
            get { return m_matchWholeWord; }
            set
            {
                SetPropertyValue(value, m_matchWholeWord, () => this.MatchWholeWord,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_matchWholeWord = (Boolean)newValue;
                        }
                    ));
            }
        }
              
        public Boolean UseRegularExpressions
        {
            get { return m_useRegularExpressions; }
            set
            {
                SetPropertyValue(value, m_useRegularExpressions, () => this.UseRegularExpressions,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_useRegularExpressions = (Boolean)newValue;
                        }
                    ));
            }
        }

        private SearchResultWindowViewModel SearchResultWindowViewModel
        {
            get;
            set;
        }

        private SearchResultWindowProxy SearchResultWindowProxy
        {
            get;
            set;
        }

        public string CurrentWindow
        {
            get { return m_currentWindow; }
            set
            {
                SetPropertyValue(value, m_currentWindow, () => this.CurrentWindow,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_currentWindow = (string)newValue;
                        }
                    ));
            }
        }

        public string CurrentDocument
        {
            get { return m_currentDocument; }
            set
            {
                SetPropertyValue(value, m_currentDocument, () => this.CurrentDocument,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_currentDocument = (string)newValue;
                        }
                    ));
            }
        }

        public bool ExpressionError
        {
            get { return m_expressionError; }
            set
            {
                SetPropertyValue(value, m_expressionError, () => this.ExpressionError,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_expressionError = (Boolean)newValue;
                        }
                    ));
            }
        }

        public string ExpressionErrorMessage
        {
            get { return m_expressionErrorMsg; }
            set
            {
                SetPropertyValue(value, m_expressionErrorMsg, () => this.ExpressionErrorMessage,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_expressionErrorMsg = (string)newValue;
                        }
                    ));
            }
        }
        private string m_expressionErrorMsg;

        /// <summary>
        /// 
        /// </summary>
        public List<string> Tags { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public BasicFindViewModel()
        {
            this.SearchArea = Menu.SearchArea.CurrentDocument;
            this.ActiveDocument = false;
            this.ActiveWindow = false;
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public BasicFindViewModel(ILayoutManager layoutManager, IMessageService messageService, SearchResultWindowViewModel searchResultWindowViewModel, SearchResultWindowProxy searchResultWindowProxy, List<string> tags)
            : this()
        {
            this.LayoutManager = layoutManager;
            this.Tags = tags;
            this.MessageService = messageService;
            this.SearchResultWindowViewModel = searchResultWindowViewModel;
            this.SearchResultWindowProxy = searchResultWindowProxy;
            this.ActiveContent = this.LayoutManager.GetActiveContent();
            this.LayoutManager.ActiveContentChanged += OnActiveContentChanged;
            this.OnActiveContentChanged(this.LayoutManager, EventArgs.Empty);
        }
        #endregion // Constructor(s)

        #region Methods
        /// <summary>
        /// Gets called whenever the layout managers
        /// active content has changed.
        /// </summary>
        /// <param name="sender">
        /// The layout manger this is attached to.
        /// </param>
        /// <param name="e">
        /// Event arguments.
        /// </param>
        void OnActiveContentChanged(Object sender, EventArgs e)
        {
            IContentBase active = this.LayoutManager.GetActiveContent();
            if (active == null || active == FindContent || active == this.SearchResultWindowProxy.GetToolWindow())
                return;

            this.ActiveContent = active;
            this.ActiveDocument = false;
            this.ActiveWindow = false;
            this.CurrentWindow = string.Empty;
            if (active is IDocumentBase)
            {
                this.ActiveDocument = true;
                this.CurrentDocument = active.Title;
            }
            else if (active is IToolWindowBase)
            {
                this.ActiveWindow = true;
                this.CurrentWindow = active.Title;
            }

            if (this.ActiveDocument == false && this.SearchArea == Menu.SearchArea.CurrentDocument)
            {
                if (this.ActiveWindow == true)
                    this.SearchArea = Menu.SearchArea.CurrentWindow;
                else
                    this.SearchArea = Menu.SearchArea.AllOpenDocuments;
            }
            if (this.ActiveWindow == false && this.SearchArea == Menu.SearchArea.CurrentWindow)
            {
                if (this.ActiveDocument == true)
                    this.SearchArea = Menu.SearchArea.CurrentDocument;
                else
                    this.SearchArea = Menu.SearchArea.AllOpenDocuments;
            }

            m_documents = new List<IContentBase>();
            foreach (IContentBase content in LayoutManager.Documents)
            {
                m_documents.Add(content);
            }
            m_windows = new List<IContentBase>();
            foreach (IToolWindowBase content in LayoutManager.ToolWindows)
            {
                if (this.LayoutManager.IsVisible(content))
                {
                    if (content == this.FindContent)
                        continue;

                    m_windows.Add(content);
                }
            }
        }
        #endregion

        #region Command Callbacks
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        internal Boolean CanExecuteFindCommand(Object parameter)
        {
            return this.FindText != null && this.FindText.Length > 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        internal void Find(object parameter)
        {
            if (!(parameter is SearchParameter))
                return;
            SearchParameter param = (SearchParameter)parameter;
            Regex expression = null;
            String pattern = String.Empty;
            if (this.UseRegularExpressions == true)
            {
                pattern = FindText;
            }
            else
            {
                this.ExpressionError = false;
                if (this.MatchWholeWord)
                {
                    pattern = WildcardToRegex(this.FindText, true);
                }
                else
                {
                    pattern = WildcardToRegex(this.FindText, false);
                }
            }

            try
            {
                if (this.MatchCase == true)
                {
                    expression = new Regex(pattern);
                }
                else
                {
                    expression = new Regex(pattern, RegexOptions.IgnoreCase);
                }
                if (expression == null)
                    return;
            }
            catch (Exception ex)
            {
                this.ExpressionError = true;
                ExpressionErrorMessage = ex.Message;
                return;
            }

            if (expression == null)
            {
                this.ExpressionError = false;
                return;
            }

            List<IContentBase> scope = new List<IContentBase>();
            switch (this.SearchArea)
            {
                case Menu.SearchArea.CurrentDocument:
                    {
                        if (this.ActiveContent == null)
                            return;

                        scope.Add(this.ActiveContent);
                    }
                    break;

                case Menu.SearchArea.CurrentWindow:
                    {
                        if (this.ActiveContent == null)
                            return;

                        scope.Add(this.ActiveContent);
                    }
                    break;

                case Menu.SearchArea.AllOpenDocuments:
                    {
                        if (m_documents.Count == 0)
                            return;

                        if (this.ActiveContent != null && m_documents.Contains(this.ActiveContent))
                        {
                            scope.Add(this.ActiveContent);
                        }
                        foreach (var content in m_documents)
                        {
                            if (!scope.Contains(content))
                                scope.Add(content);
                        }
                    }
                    break;

                case Menu.SearchArea.EntireWorkbench:
                    {
                        if (m_documents.Count == 0 && m_windows.Count == 0)
                            return;

                        if (this.ActiveContent != null)
                        {
                            scope.Add(this.ActiveContent);
                        }
                        foreach (var content in m_documents)
                        {
                            if (!scope.Contains(content))
                                scope.Add(content);
                        }
                        foreach (var content in m_windows)
                        {
                            if (!scope.Contains(content))
                                scope.Add(content);
                        }
                    }
                    break;
                default:
                    throw new NotImplementedException();
            }
            if (scope.Count <= 0)
                return;

            if (m_currentSearchParamter != param)
            {
                m_currentContent = null;
                m_contentIndex = 0;
                m_searchStartObject = null;
            }

            if (param == SearchParameter.FindNext || param == SearchParameter.FindPrevious)
            {
                Find(new SearchQuery(expression, UseTag, Tag), scope, param);
            }
            else if (param == SearchParameter.FindAll)
            {
                FindAll(new SearchQuery(expression, UseTag, Tag), scope);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="scope"></param>
        /// <param name="param"></param>
        internal void Find(ISearchQuery query, List<IContentBase> scope, SearchParameter param)
        {
            if (query == null || scope == null || scope.Count == 0)
                return;

            if (m_currentContent == null)
            {
                m_currentContent = scope[0];
                m_contentIndex = 0;
            }
            if (m_searchStartObject == null)
            {
                List<object> selection = m_currentContent.GetCurrentSelection();
                if (selection == null || selection.Count == 0)
                    m_searchStartObject = null;
                else
                    m_searchStartObject = selection[selection.Count - 1];
            }
            Boolean resultFound = false;
            while (resultFound == false && m_currentContent != null)
            {
                if (param == SearchParameter.FindNext)
                    m_searchStartObject = m_currentContent.FindNext(query, m_searchStartObject);
                else if (param == SearchParameter.FindPrevious)
                    m_searchStartObject = m_currentContent.FindPrevious(query, m_searchStartObject);
                else
                    m_searchStartObject = null;

                if (m_searchStartObject != null)
                {
                    resultFound = true;
                }
                else
                {
                    m_contentIndex++;
                    m_currentContent = scope.Count > m_contentIndex ? scope[m_contentIndex] : null;
                }
            }
            if (resultFound == false)
            {
                this.MessageService.Show("The text was not found in search scope.", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Information);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        internal void FindAll(ISearchQuery query, List<IContentBase> scope)
        {
            LayoutManager.ShowToolWindow(this.SearchResultWindowProxy.GetToolWindow());
            this.SearchResultWindowViewModel.StartNewSearch(FindText);

            List<ISearchResult> results = new List<ISearchResult>();
            foreach (IContentBase content in scope)
            {
                List<ISearchResult> contentResults = content.FindAll(query);
                if (contentResults != null)
                {
                    results.AddRange(contentResults);
                }
            }

            this.SearchResultWindowViewModel.FinishSearch(results);
            if (results.Count <= 0)
            {
                this.MessageService.Show("The text was not found in search scope.", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Information);
            }
        }

        private string WildcardToRegex(string pattern, bool matchWhole)
        {
            if (matchWhole == false)
                return Regex.Escape(pattern).Replace("\\*", ".*").Replace("\\?", ".");
            else
                return "^" + Regex.Escape(pattern) + "$";
        }
        #endregion
    } // BasicFindViewModel

    public class FindCommand : System.Windows.Input.ICommand
    {
        private Action<Object> m_execute;
        private Predicate<Object> m_canExecute;

        public FindCommand(Action<Object> execute, Predicate<Object> canExecute)
        {
            this.m_canExecute = canExecute;
            this.m_execute = execute;
        }

        #region ICommand Implementation

        /// <summary>
        /// 
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add { System.Windows.Input.CommandManager.RequerySuggested += value; }
            remove { System.Windows.Input.CommandManager.RequerySuggested -= value; }
        }

        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public Boolean CanExecute(Object parameter)
        {
            if (this.m_canExecute != null)
                return this.m_canExecute(parameter);

            return true;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public void Execute(Object parameter)
        {
            if (this.m_execute != null)
                this.m_execute(parameter);
        }

        #endregion // ICommand Implementation
    }

} // Workbench.UI.Menu
