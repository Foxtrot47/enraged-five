﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using RSG.Base.Math;
using RSG.Model.Common;
using System.Drawing;
using RSG.Model.Asset;
using RSG.Model.Common.Extensions;
using RSG.Model.Common.Map;
using RSG.Base.Tasks;
using System.Threading;
using RSG.Base.Windows.Dialogs;
using RSG.Base.Extensions;
using RSG.Model.Common.Util;
using MapViewport.AddIn;
using Workbench.AddIn.Services.Model;

namespace Workbench.AddIn.MapDebugging.Overlays
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension("Workbench.AddIn.MapDebugging.OverlayGroups.MapDebug", typeof(Viewport.AddIn.ViewportOverlay))]
    public class TextureLocationOverlay : LevelDependentViewportOverlay
    {
        #region Constants
        private const String c_name = "Texture Visiblity Bounds";
        private const String c_description = "Displays the visibility bounds of the selected texture on the map along with the map section outlines in which it can be found.";
        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// Content Browser
        /// </summary>
        [ImportExtension(ContentBrowser.AddIn.CompositionPoints.ContentBrowser, typeof(ContentBrowser.AddIn.IContentBrowser))]
        public Lazy<ContentBrowser.AddIn.IContentBrowser> ContentBrowserProxy { get; set; }
        #endregion // MEF Imports

        #region Properties
        /// <summary>
        /// Overlay image that is used for rendering to texture
        /// </summary>
        protected Viewport2DImageOverlay OverlayImage
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public TextureLocationOverlay()
            : base(c_name, c_description)
        {
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// Gets called when the user wants to view this overlay
        /// </summary>
        public override void Activated()
        {
            base.Activated();

            // Make sure that the sections have the required data loaded
            CancellationTokenSource cts = new CancellationTokenSource();
            LevelTaskContext context = new LevelTaskContext(cts.Token, LevelBrowserProxy.Value.SelectedLevel);

            ITask loadDataTask = new ActionTask("Load Data", (ctx, progress) => EnsureDataLoaded((LevelTaskContext)ctx, progress));

            TaskExecutionDialog dialog = new TaskExecutionDialog();
            TaskExecutionViewModel vm = new TaskExecutionViewModel("Generating Overlay", loadDataTask, cts, context);
            vm.AutoCloseOnCompletion = true;
            dialog.DataContext = vm;
            Nullable<bool> selectionResult = dialog.ShowDialog();
            if (false == selectionResult)
            {
                return;
            }

            // Make sure a level is selected
            if (LevelBrowserProxy.Value.SelectedLevel != null)
            {
                // Setup the overlay image
                Vector2i size = new Vector2i(966, 627);
                Vector2f pos = new Vector2f(-483.0f, 627.0f);

                ILevel level = LevelBrowserProxy.Value.SelectedLevel;
                if (level.ImageBounds != null)
                {
                    size = new Vector2i((int)(level.ImageBounds.Max.X - level.ImageBounds.Min.X), (int)(level.ImageBounds.Max.Y - level.ImageBounds.Min.Y));
                    pos = new Vector2f(level.ImageBounds.Min.X, level.ImageBounds.Max.Y);
                }

                float imageScaleFactor = 0.1f;
                OverlayImage = new Viewport2DImageOverlay(etCoordSpace.World, "", new Vector2i((int)(size.X * imageScaleFactor), (int)(size.Y * imageScaleFactor)), pos, new Vector2f(size.X, size.Y));
            }

            ContentBrowserProxy.Value.PanelItemChanged += PanelItemChanged;
            PanelItemChanged(null, this.ContentBrowserProxy.Value.SelectedItem);
        }

        /// <summary>
        /// Gets called when the user turns off this overlay
        /// </summary>
        public override void Deactivated()
        {
            ContentBrowserProxy.Value.PanelItemChanged -= PanelItemChanged;
            OverlayImage = null;
            base.Deactivated();
        }

        /// <summary>
        /// Gets the control that should be shown in the overlay details panel
        /// </summary>
        public override System.Windows.Controls.Control GetOverlayControl()
        {
            return null;
        }
        #endregion // Overrides

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="progress"></param>
        private void EnsureDataLoaded(LevelTaskContext context, IProgress<TaskProgress> progress)
        {
            IMapHierarchy hierarchy = context.MapHierarchy;

            if (hierarchy != null)
            {
                float increment = 1.0f / hierarchy.AllSections.Count();

                // Load all the map section data
                foreach (IMapSection section in hierarchy.AllSections)
                {
                    context.Token.ThrowIfCancellationRequested();

                    // Update the progress information
                    string message = String.Format("Loading {0}", section.Name);
                    progress.Report(new TaskProgress(increment, true, message));

                    // Only request that the entities for this section are loaded
                    section.LoadAllStats();
                }
            }
        }


        /// <summary>
        /// Get called when the item selected in the content browser panel change
        /// </summary>
        private void PanelItemChanged(RSG.Model.Common.IAsset oldValue, RSG.Model.Common.IAsset newValue)
        {
            OverlayImage.BeginUpdate();

            if (newValue is ITexture)
            {
                ITexture texture = (ITexture)newValue;
                RenderTextureGeometry(texture);
            }
            else if (newValue is TextureDictionary)
            {
                TextureDictionary txd = (TextureDictionary)newValue;
                foreach (Texture textureChild in txd.AssetChildren.Where(item => item is Texture))
                {
                    RenderTextureGeometry(textureChild);
                }
            }

            OverlayImage.UpdateImageOpacity(128);
            OverlayImage.EndUpdate();

            // Update the geometry being shown on the overlay
            Geometry.BeginUpdate();
            Geometry.Clear();
            Geometry.Add(OverlayImage);
            Geometry.EndUpdate();
        }

        /// <summary>
        /// Renders geometry for a single texture
        /// </summary>
        /// <param name="texture"></param>
        private void RenderTextureGeometry(ITexture texture)
        {
            IList<IEntity> validEntities = new List<IEntity>();

            // Iterate over all the archetypes we found and check the child textures to see if its one of the ones we are after
            foreach (IMapArchetype archetype in GetAllMapArchetypes())
            {
                foreach (ITexture archetypeTexture in archetype.Textures)
                {
                    if (archetypeTexture.Equals(texture))
                    {
                        validEntities.AddRange(archetype.Entities);
                    }
                }
            }

            // Iterate over the list of instances we found rendering them to the overlay
            foreach (IEntity entity in validEntities)
            {
                RenderMapInstanceBounds(entity.GetStreamingExtents(), Color.Green);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bounds"></param>
        private void RenderMapInstanceBounds(BoundingBox3f bounds, Color fillColour)
        {
            Vector2f[] points = new Vector2f[4];
            points[0] = new Vector2f(bounds.Min.X, bounds.Min.Y);
            points[1] = new Vector2f(bounds.Min.X, bounds.Max.Y);
            points[2] = new Vector2f(bounds.Max.X, bounds.Max.Y);
            points[3] = new Vector2f(bounds.Max.X, bounds.Min.Y);

            OverlayImage.RenderPolygon(points, fillColour);
        }

        /// <summary>
        /// Returns all the map definitions that exist in the map data
        /// </summary>
        /// <returns></returns>
        private IEnumerable<IMapArchetype> GetAllMapArchetypes()
        {
            foreach (IMapSection section in LevelBrowserProxy.Value.SelectedLevel.GetMapHierarchy().AllSections)
            {
                foreach (IMapArchetype archetype in section.Archetypes)
                {
                    yield return archetype;
                }
            }
        }
        #endregion // Private Methods
    } // TextureLocationOverlay
}
