﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Windows;
using RSG.Base.Editor;
using RSG.Base.Logging;
using Workbench.AddIn;
using Workbench.AddIn.Commands;
using Workbench.AddIn.Services;
using Workbench.AddIn.Services.File;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.UI.Menu;
using Workbench.Services;

namespace Workbench.UI.Menu
{
    /// <summary>
    /// File Open menu item.
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.MenuFile, typeof(IWorkbenchCommand))]
    [ExportExtension(Workbench.AddIn.CompositionPoints.WorkbenchOpenService, typeof(IWorkbenchOpenService))]
    class FileOpenMenuItem : WorkbenchMenuItemBase, IPartImportsSatisfiedNotification, IWorkbenchOpenService
    {
        #region MEF Imports
        /// <summary>
        /// Extensions service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ExtensionService)]
        private IExtensionService ExtensionService { get; set; }

        /// <summary>
        /// MEF import for workbench layout manager.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager,
            typeof(ILayoutManager))]
        private ILayoutManager LayoutManager { get; set; }

        /// <summary>
        /// MEF import for workbench file new services exposed by plugins.
        /// </summary>
        [ImportManyExtension(Workbench.AddIn.ExtensionPoints.OpenService,
            typeof(IOpenService), AllowRecomposition = true)]
        private IEnumerable<IOpenService> OpenServices { get; set; }

        /// <summary>
        /// Open menu subitems.
        /// </summary>
        [ImportManyExtension(Workbench.AddIn.ExtensionPoints.OpenMenu, typeof(IWorkbenchCommand))]
        private IEnumerable<IWorkbenchCommand> items { get; set; }

        /// <summary>
        /// MEF import for workbench arguments service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ApplicationArgumentsService,
            typeof(IApplicationArgumentsService))]
        private IApplicationArgumentsService ApplicationArgsService { get; set; }
        #endregion // MEF Imports

        #region Member Data
        private Dictionary<IDocumentBase, List<System.IO.FileSystemWatcher>> m_documentWatchers = new Dictionary<IDocumentBase, List<System.IO.FileSystemWatcher>>();
        private Dictionary<System.IO.FileSystemWatcher, IDocumentBase> m_watchers = new Dictionary<System.IO.FileSystemWatcher, IDocumentBase>();
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public FileOpenMenuItem()
        {
            this.Header = "_Open";
            this.ID = new Guid(Workbench.AddIn.CompositionPoints.MenuFileOpen);
            this.RelativeID = new Guid(Workbench.AddIn.CompositionPoints.MenuFileNew);
            this.Direction = Direction.After;
            this.SetImageFromBitmap(RSG.Base.Editor.Properties.Resources.OPEN);
        }
        #endregion // Constructor(s)

        #region IPartImportsSatisfiedNotification Methods
        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {
            Log.Log__Debug("FileOpenMenuItem::OnImportsSatisfied()");

            List<IWorkbenchCommand> openItems = new List<IWorkbenchCommand>();
            Guid previousId = Guid.NewGuid();
            foreach (IOpenService os in this.OpenServices)
            {
                OpenFileServiceMenuItem mi = new OpenFileServiceMenuItem(os, previousId, this.LayoutManager, this);
                if (os.KeyGesture != null)
                    mi.KeyGesture = os.KeyGesture;

                previousId = mi.ID;
                openItems.Add(mi);
            }
            if (0 == openItems.Count)
            {
                openItems.Add(new OpenFileServiceMenuItem());
            }

            openItems.AddRange(items);
            this.Items = ExtensionService.Sort(openItems);

            // Register a handler for command line argument startup and injection.
            this.ApplicationArgsService.ProcessStartupArguments += ProcessStartupArgumentsHandler;
            this.ApplicationArgsService.InjectedArgumentsChanged += InjectArgumentsHandler;
        }
        #endregion // IPartImportsSatisfiedNotification Methods

        #region ICommand Implementation

        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
        }

        #endregion // ICommand Implementation

        #region IWorkbenchOpenService
        /// <summary>
        /// 
        /// </summary>
        /// <param name="openService"></param>
        /// <param name="filename"></param>
        public void OpenFileWithService(IOpenService openService, String filename)
        {
            OpenFileWithService(openService, filename, null);
        }

        /// <summary>
        /// Overload of the OpenFileWithService that allows the user to attach some generic data
        /// to the document
        /// </summary>
        /// <param name="openService"></param>
        /// <param name="filename"></param>
        /// <param name="data"></param>
        public void OpenFileWithService(IOpenService openService, String filename, object data)
        {
            Application.Current.Dispatcher.Invoke((Action)delegate()
            {
                // Before calling the open service check to see if the filename is already opened and if
                // so just show it.
                foreach (IDocumentBase openedDocument in this.LayoutManager.Documents)
                {
                    if (String.Compare(openedDocument.Path, filename, true) == 0)
                    {
                        this.LayoutManager.ShowDocument(openedDocument);
                        return;
                    }
                }

                Log.Log__Debug("IOpenService: {0}.", filename);

                IDocumentBase document = null;
                IModel model = null;
                bool osres = openService.Open(out document, out model, filename, 0);
                Debug.Assert(osres && null != document && null != model, String.Format("OpenService {0} failed for file {1}.", openService.ID, filename));
                if (osres)
                {
                    document.DocumentData = data;
                    RegisterAndShowDocument(document, openService, filename);
                }
                else
                {
                    Log.Log__Error("OpenService {0} failed for file: {1}.", openService.ID, filename);
                }
            });
        }

        public void OpenFileWithService(IOpenService openService, List<String> filenames)
        {
            Application.Current.Dispatcher.Invoke((Action)delegate()
            {
                List<string> neededFilenames = new List<string>();
                foreach (String filename in filenames)
                {
                    // Before calling the open service check to see if the filename is already opened and if
                    // so just show it.
                    bool found = false;
                    foreach (IDocumentBase openedDocument in this.LayoutManager.Documents)
                    {
                        if (String.Compare(openedDocument.Path, filename, true) == 0)
                        {
                            this.LayoutManager.ShowDocument(openedDocument);
                            found = true;
                        }
                    }
                    if (!found)
                        neededFilenames.Add(filename);

                }
                if (neededFilenames.Count == 0)
                    return;

                if (neededFilenames.Count == 1)
                {
                    OpenFileWithService(openService, neededFilenames[0], null);
                    return;
                }

                String files = String.Empty;
                foreach (String filename in neededFilenames)
                {
                    files += filename + " ";
                }
                Log.Log__Debug("IOpenService: {0}.", files);

                Dictionary<IDocumentBase, List<String>> documents = null;
                List<IModel> model = null;
                bool osres = openService.Open(out documents, out model, neededFilenames, 0);
                Debug.Assert(osres && null != documents && null != model, String.Format("OpenService {0} failed for file {1}.", openService.ID, files));
                if (osres)
                {
                    foreach (KeyValuePair<IDocumentBase, List<String>> doc in documents)
                    {
                        files = String.Empty;
                        int index = 0;
                        Boolean docReadOnly = false;
                        foreach (String filename in doc.Value)
                        {
                            Boolean readOnly = false;
                            System.IO.FileAttributes attr = System.IO.File.GetAttributes(filename);
                            if ((attr & System.IO.FileAttributes.ReadOnly) == System.IO.FileAttributes.ReadOnly)
                            {
                                readOnly = true;
                                docReadOnly = true;
                            }
                            if (index == neededFilenames.Count - 1)
                                files += filename + (readOnly ? " [Read Only]" : "");
                            else
                                files += filename + (readOnly ? " [Read Only]" : "") + "\n";
                            index++;
                        }
                        doc.Key.IsReadOnly = docReadOnly;
                        RegisterAndShowDocument(doc.Key, openService, files);
                    }
                }
                else
                {
                    Log.Log__Error("OpenService {0} failed for file: {1}.", openService.ID, files);
                }
            });
        }
     
        private void RegisterAndShowDocument(IDocumentBase document, IOpenService openService, String filename)
        {
            document.Path = filename;
            document.OpenService = openService;
            if (!filename.Contains("\n"))
            {
                if (System.IO.File.Exists(filename))
                    RecentFileService.AddFile(filename, openService.ID);
            }
            Log.Log__Debug("OpenService {0} successful for file: {1}.", openService.ID, filename);
            this.LayoutManager.ShowDocument(document);
        }

        #endregion IWorkbenchOpenService

        #region Application Arguments Service Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProcessStartupArgumentsHandler(Object sender, ArgumentsEventArgs e)
        {
            Log.Log__Debug("Processing startup command line arguments.");
            ProcessArgumentsEvent(e);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void InjectArgumentsHandler(Object sender, ArgumentsEventArgs e)
        {
            Log.Log__Debug("Processing injected command line arguments.");
            ProcessArgumentsEvent(e);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void ProcessArgumentsEvent(ArgumentsEventArgs e)
        {
            // Find correct open service (all have to be the same one).
            IOpenService openService = null;
            foreach (IOpenService os in this.OpenServices)
            {
                if (0 == String.Compare(String.Empty, os.CommandLineKey, true))
                    continue;

                if (e.Arguments.ContainsKey(os.CommandLineKey))
                {
                    openService = os;
                    break;
                }
            }

            if (openService == null)
                return;

            // Find all our filenames.
            List<String> filenames = new List<String>();
            filenames.Add(e.Arguments[openService.CommandLineKey].ToString());
            filenames.AddRange(e.TrailingArguments);

            OpenFileWithService(openService, filenames);
        }
        #endregion // Application Arguments Service Event Handlers

    } // FileOpenMenuItem

    #region OpenFileServiceMenuItem Class

    /// <summary>
    /// 
    /// </summary>
    class OpenFileServiceMenuItem : WorkbenchMenuItemBase
    {
        #region Member Data
        /// <summary>
        /// Associated IOpenService.
        /// </summary>
        private IOpenService Service;

        /// <summary>
        /// Layout manager for showing the results.
        /// </summary>
        private ILayoutManager LayoutManager;

        /// <summary>
        /// 
        /// </summary>
        private IWorkbenchOpenService WorkbenchOpenService;

        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="os"></param>
        /// <param name="prevId"></param>
        /// <param name="lm"></param>
        public OpenFileServiceMenuItem(IOpenService os, Guid prevId, ILayoutManager lm, IWorkbenchOpenService workbenchOpenService)
        {
            this.Header = os.Header;
            this.ID = Guid.NewGuid();
            this.RelativeID = prevId;
            this.Direction = Direction.After;
            this.Service = os;
            this.LayoutManager = lm;
            this.WorkbenchOpenService = workbenchOpenService;
            this.SetImageFromBitmap(os.Image);
        }

        /// <summary>
        /// Default constructor (used to display no services available).
        /// </summary>
        public OpenFileServiceMenuItem()
        {
            this.Header = "[No Open Services Loaded]";
            this.ID = Guid.NewGuid();
            this.Service = null;
        }
        #endregion // Constructor(s)

        #region ICommand Implementation

        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return this.Service != null;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
            Log.Log__Debug("OpenFileServiceMenuItem::Execute() {0}", this.Header);

            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.Filter = this.Service.Filter;
            dlg.FilterIndex = this.Service.DefaultFilterIndex;
            if (this.Service.AllowMultiSelect == true)
                dlg.Multiselect = true;
            else
                dlg.Multiselect = false;

            Nullable<bool> result = dlg.ShowDialog();
            if (false == result)
                return;

            if (dlg.FileNames != null)
            {
                if (dlg.FileNames.Length == 1)
                {
                    this.WorkbenchOpenService.OpenFileWithService(this.Service, dlg.FileNames[0]);
                }
                else if (dlg.FileNames.Length > 1)
                {
                    this.WorkbenchOpenService.OpenFileWithService(this.Service, new List<String>(dlg.FileNames));
                }
            }
            else if (dlg.FileName != null)
            {
                this.WorkbenchOpenService.OpenFileWithService(this.Service, dlg.FileName);
            }
        }

        #endregion // ICommand Implementation
    } // OpenFileServiceMenuItem
    #endregion // OpenFileServiceMenuItem Class
} // Workbench.UI.Menu namespace
