using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Text;
using System.Threading;

using ragCore;

using ragWidgets;
using RSG.Base.Logging;

namespace ContentBrowser.AddIn.WidgetBrowser
{

	/// <summary>
	/// Summary description for BankManager.
	/// </summary>
	public class BankManager : IBankManager
	{
		public BankManager()
		{
		}

		public delegate void TimeStringDelegate(String str);
		public event ragCore.InitFinishedHandler InitFinished;
		public static event TimeStringDelegate TimeStringHandler;

		public static int GetWidgetTypeGUID() {return GetStaticGUID();}
		public static int GetStaticGUID()		{return Widget.ComputeGUID('b','m','g','r');}


        public static void AddHandlers(BankRemotePacketProcessor packetProcessor)
        {
            packetProcessor.AddType( GetStaticGUID(), new BankRemotePacketProcessor.HandlerDel( RemoteHandler ) );
        }

        private static void RemoteHandler( BankRemotePacket packet )
        {
            switch ( packet.BankCommand )
            {
                // open file dialog support #1:
                case BankRemotePacket.EnumPacketType.USER_0:
                    {
                        // Proxy handles this
                    }
                    break;

                // open file dialog support #2:
                case BankRemotePacket.EnumPacketType.USER_1:
                    break;

                // signal that the client has finished his initialization phase:
                case BankRemotePacket.EnumPacketType.USER_2:
                    {
                        //OnInitFinished();
                    }
                    break;

                // see if we got a confirmation that the application got the quit message:
                case BankRemotePacket.EnumPacketType.USER_4:
                    {
                        Log.Log__MessageCtx("RAG", "Got quit message from game" );
                    }
                    break;

                // see if its timer information:
                case BankRemotePacket.EnumPacketType.USER_5:
                    {
                        packet.Begin();

                        // This string contains current frame times and stuff for the game
                        String timeString = packet.Read_const_char();
                        if ( TimeStringHandler != null )
                            TimeStringHandler( timeString );

                        packet.End();
                    }
                    break;

                // see if it's a ping response
                case BankRemotePacket.EnumPacketType.USER_6:
                    {
                        // Proxy handles this
                    }
                    break;

                // CLIPBOARD_COPY:
                case BankRemotePacket.EnumPacketType.USER_7:
                    {
                        // Proxy handles this
                    }
                    break;

                // CLIPBOARD_CLEAR:
                case BankRemotePacket.EnumPacketType.USER_8:
                    {
                        // Proxy handles this
                    }
                    break;

                // CREATE_OUTPUT_WINDOW:
                case BankRemotePacket.EnumPacketType.USER_14:
                    {
                        packet.Begin();
                        string name = packet.Read_const_char();
                        string color = packet.Read_const_char();
                        packet.End();
                        // TODO
                        /*
                        RageApplication app = RageApplication.FindApplication( packet.Pipe );
                        if ( app != null )
                        {
                            app.AddOutputWindow( name, color );
                        }
                         * */
                    }
                    break;
            }
        }

        public void Reset() { OnPingResponse(); }

		protected void OnPingResponse() 
		{
			if (m_PingResponse != null) 
			{
				// m_PingResponse (the member variable) needs to be null before the
				// event is signaled, so that we can add a new ping request from that event.

				EventHandler pingEvent = m_PingResponse;
				m_PingResponse = null;
				pingEvent(this, System.EventArgs.Empty);
			}
		}

		private event EventHandler m_PingResponse;

        public void SendDelayedPing(EventHandler callback, BankPipe pipe)
		{
			// What we should really do here is see if there is already a ping response handler. If so we should
			// add a new handler that sends this as a second (or Nth) ping and then responds.
			// For now we just punt and have the new handler called for the old message.

			if (m_PingResponse == null) 
			{
				BankRemotePacket p = new BankRemotePacket(pipe);
				p.Begin(BankRemotePacket.EnumPacketType.USER_6,GetWidgetTypeGUID());
				p.Write_u32(0);
				p.Write_bool(true);
				p.Send();
			}
			m_PingResponse += callback;
		}

        public void SendPing(EventHandler callback, BankPipe pipe)
        {
            // What we should really do here is see if there is already a ping response handler. If so we should
            // add a new handler that sends this as a second (or Nth) ping and then responds.
            // For now we just punt and have the new handler called for the old message.

            if (m_PingResponse == null)
            {
                BankRemotePacket p = new BankRemotePacket(pipe);
                p.Begin(BankRemotePacket.EnumPacketType.USER_6, GetWidgetTypeGUID());
                p.Write_u32(0);
                p.Write_bool(false);
                p.Send();
            }
            m_PingResponse += callback;
        }



		public void AddBank(WidgetBank bank)
		{
			if (!m_Banks.Contains(bank))
				m_Banks.Add(bank);
		}

		public void RemoveBank(WidgetBank bank)
		{
			m_Banks.Remove(bank);
		}

        public List<Widget> AllBanks
		{
			get { return m_Banks; }
		}


		public Widget FindFirstWidgetFromPath(string path)
		{
			List<Widget> list = FindWidgetsFromPath(path);
			if (list == null || list.Count == 0) 
			{
				return null;
			}
			return list[0];
		}

		public List<Widget> FindWidgetsFromPath(string path)
		{
            if (path == "/")
            {
                return m_Banks;
            }
			return WidgetGroupBase.FindWidgetsFromPath(null, m_Banks, path);
		}

        public List<Widget> FindWidgetsFromPath( String[] path ) 
		{
            if (path.Length == 0)
            {
                return m_Banks;
            }
			return WidgetGroupBase.FindWidgetsFromPath(null, m_Banks, path, 0);
		}


        private List<Widget> m_Banks = new List<Widget>();
		private string m_ClipboardString="";
        private bool m_initFinished = false;

        public bool IsInitFinished
        {
            get
            {
                return m_initFinished;
            }
        }


        public Widget FindWidgetFromID( int id )
        {
            throw new NotImplementedException();
        }
    }
}
