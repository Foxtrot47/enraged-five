﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn.Services;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using System.Windows;
using Workbench.AddIn;

namespace Workbench.UI
{
    [ExportExtension(Workbench.AddIn.ExtensionPoints.LoggingOutput, typeof(ILoggingService))]
    public class WorkbenchUniversalLog : IInternalLoggingService
    {
        #region Properties
        public  string UIName 
        {
            get { return "Application Output"; }
        }

        public bool IsDefault
        {
            get { return true; }
        }

        public IUniversalLog LogObject
        {
            get { return (LogFactory.ApplicationLog); }
        }
        #endregion
    } // WorkbenchUniversalLog

    [ExportExtension(Workbench.AddIn.ExtensionPoints.LoggingOutput, typeof(ILoggingService))]
    public class StaticApplicationLog : IInternalLoggingService
    {
        #region Properties
        public string UIName
        {
            get { return "Static Output"; }
        }

        public bool IsDefault
        {
            get { return true; }
        }

        public IUniversalLog LogObject
        {
            get { return (Log.StaticLog); }
        }
        #endregion
    } // StaticApplicationLog
} // Workbench.UI
