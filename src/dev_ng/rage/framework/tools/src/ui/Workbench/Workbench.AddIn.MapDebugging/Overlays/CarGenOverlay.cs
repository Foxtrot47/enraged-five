﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using System.Windows.Media;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;
using RSG.Model.Common;
using RSG.Model.Common.Map;
using System.Threading;
using RSG.Base.Tasks;
using RSG.Base.Windows.Dialogs;
using RSG.Model.Common.Util;
using MapViewport.AddIn;
using Workbench.AddIn.Services.Model;
using System.Diagnostics;

namespace Workbench.AddIn.MapDebugging.Overlays
{
    [ExportExtension("Workbench.AddIn.MapDebugging.OverlayGroups.MapDebug", typeof(Viewport.AddIn.ViewportOverlay))]
    public class CarGenOverlay : LevelDependentViewportOverlay
    {
        #region Constants
        private const String NAME = "Car Generators";
        private const String DESC = "Displays the car generators on the map based on the current filtering settings.";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public bool ShowPolice
        {
            get { return _showPolice; }
            set { _showPolice = value; OnRenderOptionChanged(); }
        }
        private bool _showPolice;

        /// <summary>
        /// 
        /// </summary>
        public bool ShowFire
        {
            get { return _showFire; }
            set { _showFire = value; OnRenderOptionChanged(); }
        }
        private bool _showFire;

        /// <summary>
        /// 
        /// </summary>
        public bool ShowAmbulance
        {
            get { return _showAmbulance; }
            set { _showAmbulance = value; OnRenderOptionChanged(); }
        }
        private bool _showAmbulance;

        /// <summary>
        /// 
        /// </summary>
        public bool ShowSpecific
        {
            get { return _showSpecific; }
            set { _showSpecific = value; OnRenderOptionChanged(); }
        }
        private bool _showSpecific;

        /// <summary>
        /// 
        /// </summary>
        public bool ShowExpression
        {
            get { return _showExpression; }
            set { _showExpression = value; OnRenderOptionChanged(); }
        }
        private bool _showExpression;

        /// <summary>
        /// 
        /// </summary>
        public bool ShowAll
        {
            get { return _showAll; }
            set { _showAll = value; OnRenderOptionChanged(); }
        }
        private bool _showAll;

        /// <summary>
        /// 
        /// </summary>
        public string Specific
        {
            get { return _specific; }
            set { _specific = value; OnRenderOptionChanged(); }
        }
        private string _specific;

        /// <summary>
        /// 
        /// </summary>
        public string Expression
        {
            get { return _expression; }
            set { _expression = value; OnRenderOptionChanged(); }
        }
        private string _expression;

        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<string> Models
        {
            get;
            set;
        }
        #endregion

        #region MEF Imports
        /// <summary>
        /// A reference to view model for the map viewport
        /// </summary>
        [ImportExtension(Viewport.AddIn.CompositionPoints.MapViewport, typeof(Viewport.AddIn.IMapViewport))]
        public Viewport.AddIn.IMapViewport MapViewportProxy { get; set; }
        #endregion // MEF Imports
        
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public CarGenOverlay()
            : base(NAME, DESC)
        {
            this.Models = new ObservableCollection<string>();
            this.ShowAll = true;
            this.DataSourceModes[DataSource.Database] = true;
        }
        #endregion // Constructors

        #region Overrides
        /// <summary>
        /// Gets called when the user wants to view this overlay
        /// </summary>
        public override void Activated()
        {
            base.Activated();

            this.Models.Clear();

            CancellationTokenSource cts = new CancellationTokenSource();
            LevelTaskContext context = new LevelTaskContext(cts.Token, LevelBrowserProxy.Value.SelectedLevel);
            ITask loadDataTask = new ActionTask("Load Data", (ctx, progress) => EnsureDataLoaded((LevelTaskContext)ctx, progress));

            TaskExecutionDialog dialog = new TaskExecutionDialog();
            TaskExecutionViewModel vm = new TaskExecutionViewModel("Generating Overlay", loadDataTask, cts, context);
            vm.AutoCloseOnCompletion = true;
            dialog.DataContext = vm;
            Nullable<bool> selectionResult = dialog.ShowDialog();
            if (false == selectionResult)
            {
                return;
            }

            foreach (IMapSection sec in context.MapHierarchy.AllSections)
            {
                if (sec.CarGens == null)
                    continue;
                foreach (var car in sec.CarGens)
                {
                    if (!string.IsNullOrWhiteSpace(car.ModelName))
                    {
                        if (!this.Models.Contains(car.ModelName.ToLower()))
                            this.Models.Add(car.ModelName.ToLower());
                    }
                }
            }
            OnRenderOptionChanged();
        }

        /// <summary>
        /// Gets called when the user turns off this overlay
        /// </summary>
        public override void Deactivated()
        {
            base.Deactivated();
        }

        /// <summary>
        /// Gets the control that should be shown in the overlay details panel
        /// </summary>
        public override System.Windows.Controls.Control GetOverlayControl()
        {
            return new Workbench.AddIn.MapDebugging.OverlayViews.CargenOverlayView();
        }
        #endregion // Overrides

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="progress"></param>
        private void EnsureDataLoaded(LevelTaskContext context, IProgress<TaskProgress> progress)
        {
            IMapHierarchy hierarchy = context.MapHierarchy;
            Debug.Assert(hierarchy != null, "Unable to load map data for car gen overlay");
            if (hierarchy == null)
            {
                return;
            }

            float increment = 1.0f / context.MapHierarchy.AllSections.Count();

            // Load all the map section data
            int index = 0;
            foreach (IMapSection section in context.MapHierarchy.AllSections)
            {
                Debug.WriteLine("{0} - {1}", index++, section.Name);
                context.Token.ThrowIfCancellationRequested();

                // Update the progress information
                string message = String.Format("Loading {0}", section.Name);
                progress.Report(new TaskProgress(increment, true, message));

                // Only request that the entities for this section are loaded
                section.LoadStats(new StreamableStat[] { StreamableMapSectionStat.CarGens });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void OnRenderOptionChanged()
        {
            if (this.LevelBrowserProxy == null)
                return;
            if (this.LevelBrowserProxy.Value.SelectedLevel == null)
                return;

            IMapHierarchy hierarchy = LevelBrowserProxy.Value.SelectedLevel.MapHierarchy;
            if (hierarchy == null)
            {
                return;
            }

            int onCount = 0;
            if (ShowPolice)
                onCount++;
            if (ShowFire)
                onCount++;
            if (ShowAmbulance)
                onCount++;
            if (ShowSpecific)
                onCount++;
            if (ShowExpression)
                onCount++;
            if (ShowAll)
                onCount++;
            if (onCount != 1)
                return;

            this.Geometry.BeginUpdate();
            this.Geometry.Clear();
            List<ICarGen> cars = new List<ICarGen>();
            foreach (IMapSection sec in hierarchy.AllSections)
            {
                if (sec.CarGens == null)
                    continue;
                foreach (var car in sec.CarGens)
                {
                    cars.Add(car);
                }
            }

            RSG.Base.Math.Vector2i size = new RSG.Base.Math.Vector2i(966, 627);
            RSG.Base.Math.Vector2f pos = new RSG.Base.Math.Vector2f(-483.0f, 627.0f);
            ILevel level = LevelBrowserProxy.Value.SelectedLevel;
            if (level.ImageBounds != null)
            {
                size = new RSG.Base.Math.Vector2i((int)(level.ImageBounds.Max.X - level.ImageBounds.Min.X), (int)(level.ImageBounds.Max.Y - level.ImageBounds.Min.Y));
                pos = new RSG.Base.Math.Vector2f(level.ImageBounds.Min.X, level.ImageBounds.Max.Y);
            }
            Viewport2DImageOverlay image = new Viewport2DImageOverlay(etCoordSpace.World, "", new RSG.Base.Math.Vector2i((int)(size.X * 0.1f), (int)(size.Y * 0.1f)), pos, new RSG.Base.Math.Vector2f(size.X, size.Y));
            image.BeginUpdate();
            foreach (var car in cars)
            {
                if (ShowPolice && car.IsPolice == false)
                    continue;
                if (ShowFire && car.IsFire == false)
                    continue;
                if (ShowAmbulance && car.IsAmbulance == false)
                    continue;
                if (ShowSpecific)
                {
                    if (!string.IsNullOrWhiteSpace(car.ModelName))
                    {
                        if (string.Compare(car.ModelName.ToLower(), Specific) != 0)
                            continue;
                    }
                    else
                        continue;
                }
                if (ShowExpression)
                {
                    if (!string.IsNullOrWhiteSpace(car.ModelName))
                    {
                        if (Expression != null)
                        {
                            if (!Regex.IsMatch(car.ModelName.ToLower(), Expression))
                                continue;
                        }
                        else
                            continue;
                    }
                    else
                        continue;
                }
                if (this.ShowAll)
                {
                    if (string.IsNullOrWhiteSpace(car.ModelName))
                        image.RenderCircle(new RSG.Base.Math.Vector2f(car.Position.X, car.Position.Y), 10.0f, System.Drawing.Color.Green);
                    else
                        image.RenderCircle(new RSG.Base.Math.Vector2f(car.Position.X, car.Position.Y), 10.0f, System.Drawing.Color.Red);
                }
                else
                {
                    image.RenderCircle(new RSG.Base.Math.Vector2f(car.Position.X, car.Position.Y), 10.0f, System.Drawing.Color.Red);
                }
            }
            image.EndUpdate();
            this.Geometry.Add(image);
            this.Geometry.EndUpdate();
        }
        #endregion
    } // CarGenOverlay
} // Workbench.AddIn.MapDebugging.Overlays
