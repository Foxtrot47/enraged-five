﻿using System;
using System.Windows.Media.Imaging;

namespace Workbench.AddIn.UI.Controls
{

    /// <summary>
    /// 
    /// </summary>
    public interface IButton : ICommandControl
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        String Text { get; }

        /// <summary>
        /// 
        /// </summary>
        BitmapSource Icon { get; }
        
        /// <summary>
        /// 
        /// </summary>
        bool IsCancel { get; }

        /// <summary>
        /// 
        /// </summary>
        bool IsDefault { get;  }
        #endregion // Properties
    }

} // Workbench.AddIn.UI.Controls namespace
