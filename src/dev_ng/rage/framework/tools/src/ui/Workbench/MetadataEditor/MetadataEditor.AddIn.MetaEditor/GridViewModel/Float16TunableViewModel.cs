﻿using MetadataEditor.AddIn.MetaEditor.ViewModel;
using RSG.Metadata.Data;

namespace MetadataEditor.AddIn.MetaEditor.GridViewModel
{
    /// <summary>
    /// The view model that wraps around a 
    /// <see cref="Float16Tunable"/> object. Used
    /// for the grid view.
    /// </summary>
    public class Float16TunableViewModel : GridTunableViewModel
    {
        #region Constructor
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="MetadataEditor.AddIn.MetaEditor.GridViewModel.Float16TunableViewModel"/>
        /// class.
        /// </summary>
        public Float16TunableViewModel(GridTunableViewModel parent, Float16Tunable m, MetaFileViewModel root)
            : base(parent, m, root)
        {
        }
        #endregion // Constructor
    } // Float16TunableViewModel
} // MetadataEditor.AddIn.MetaEditor.GridViewModel
