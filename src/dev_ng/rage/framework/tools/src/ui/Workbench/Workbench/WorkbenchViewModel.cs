﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Reflection;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Controls;
using RSG.Base.Logging;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using RSG.Base.Windows.Extensions;
using Workbench.AddIn;
using Workbench.AddIn.Commands;
using Workbench.AddIn.Services;
using Workbench.AddIn.Services.File;
using Workbench.AddIn.UI;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.UI.Menu;
using Workbench.AddIn.UI.StatusBar;
using WidgetEditor.AddIn;
using Workbench.AddIn.Services.Model;
using System.IO;
using System.Xml;
using Workbench.UI;
using System.Windows.Threading;

namespace Workbench
{
    /// <summary>
    /// Workbench ViewModel class.
    /// </summary>
    /// This is the workbench controller that binds everything together.
    /// Where the magic happens an' all that.
#warning DHM FIX ME: something doesn't seem right here... name... MEF... interface.
    [ExportExtension(Workbench.AddIn.CompositionPoints.CoreServicesProvider, 
        typeof(ICoreServiceProvider))]
    class WorkbenchViewModel : 
        ViewModelBase, 
        ICoreServiceProvider, 
        IPartImportsSatisfiedNotification, 
        IDisposable
    {
        #region Attached Properties

        public static DependencyProperty RegisterInputBindingsProperty = DependencyProperty.RegisterAttached(
            "RegisterInputBindings", 
            typeof(InputBindingCollection),
            typeof(WorkbenchViewModel),
            new PropertyMetadata(null, OnRegisterInputBindingChanged));

        public static void SetRegisterInputBindings(UIElement element, InputBindingCollection value)
        {
            if (element != null)
                element.SetValue(RegisterInputBindingsProperty, value);
        }
        public static InputBindingCollection GetRegisterInputBindings(UIElement element)
        {
            return (element != null ? (InputBindingCollection)element.GetValue(RegisterInputBindingsProperty) : null);
        }
        private static void OnRegisterInputBindingChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            UIElement element = sender as UIElement;
            if (element != null)
            {
                element.InputBindings.Clear();
                element.CommandBindings.Clear();
                InputBindingCollection bindings = e.NewValue as InputBindingCollection;
                if (bindings != null)
                {
                    element.InputBindings.AddRange(bindings);
                }
            }
        }

        public static DependencyProperty RegisterToolBarsProperty = DependencyProperty.RegisterAttached(
            "RegisterToolBars",
            typeof(IEnumerable<ToolBar>),
            typeof(WorkbenchViewModel),
            new PropertyMetadata(null, OnRegisterToolBarsChanged));

        public static void SetRegisterToolBars(UIElement element, IEnumerable<ToolBar> value)
        {
            if (element != null)
                element.SetValue(RegisterToolBarsProperty, value);
        }
        public static IEnumerable<ToolBar> GetRegisterToolBars(UIElement element)
        {
            return (element != null ? (IEnumerable<ToolBar>)element.GetValue(RegisterToolBarsProperty) : null);
        }
        private static void OnRegisterToolBarsChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            ToolBarTray element = sender as ToolBarTray;
            if (element != null)
            {
                element.ToolBars.Clear();
                foreach (Object toolBar in e.NewValue as IEnumerable<ToolBar>)
                {
                    if (toolBar is ToolBar)
                        element.ToolBars.Add(toolBar as ToolBar);
                }
            }
        }

        #endregion // Attached Properties

        #region MEF Imports
        #region Tool Core Services
        /// <summary>
        /// MEF import for Bugstar service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.BugstarService, 
            typeof(IBugstarService))]
        public IBugstarService BugstarService { get; set; }

        /// <summary>
        /// MEF import for proxy UI
        /// </summary>
        [ImportExtension(WidgetEditor.AddIn.CompositionPoints.ProxyService, 
            typeof(IProxyService))]
        public IProxyService ProxyService { get; set; }
        #endregion // Tool Core Services


        /// <summary>
        /// MEF import for Configuration Service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(IConfigurationService))]
        public IConfigurationService ConfigurationService { get; set; }

        /// <summary>
        /// MEF import for User Interface Service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.UserInterfaceService,
            typeof(Workbench.AddIn.Services.IUserInterfaceService))]
        public IUserInterfaceService UserInterfaceService { get; set; }

        /// <summary>
        /// MEF import for Task Progress Service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.TaskProgressService,
            typeof(Workbench.AddIn.Services.ITaskProgressService))]
        public ITaskProgressService TaskProgressService { get; set; }

        /// <summary>
        /// MEF import for perforce service.
        /// </summary>
        [ImportExtension(PerforceBrowser.AddIn.CompositionPoints.PerforceService, typeof(PerforceBrowser.AddIn.IPerforceService))]
        public PerforceBrowser.AddIn.IPerforceService PerforceService { get; set; }

        
        /// <summary>
        /// MEF import for REST/web service
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.WebService, typeof(IWebService))]
        public IWebService WebService { get; set; }

        /// <summary>
        /// MEF import for Workbench Open Service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.WorkbenchOpenService, typeof(IWorkbenchOpenService))]
        public IWorkbenchOpenService OpenService { get; set; }
        
        /// <summary>
        /// Model data provider.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.DataContextProvider, typeof(IDataContextProvider))]
        public IDataContextProvider DataContextProvider { get; set; }
        
        /// <summary>
        /// MEF import for workbench UI.UI.Layout Manager component.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager, typeof(Workbench.AddIn.UI.Layout.ILayoutManager))]
        public ILayoutManager LayoutManager { get; set; }

        /// <summary>
        /// MEF import for generic extension service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ExtensionService, typeof(IExtensionService))]
        private IExtensionService ExtensionService { get; set; }

        /// <summary>
        /// MEF import for workbench root menu items.
        /// </summary>
        [ImportManyExtension(Workbench.AddIn.ExtensionPoints.ToolbarTray, typeof(IToolbar), AllowRecomposition = true)]
        private IEnumerable<IToolbar> ImportedToolBars { get; set; }

        /// <summary>
        /// MEF import for workbench root menu items.
        /// </summary>
        [ImportManyExtension(Workbench.AddIn.ExtensionPoints.MenuMain, typeof(IWorkbenchCommand), AllowRecomposition = true)]
        private IEnumerable<IWorkbenchCommand> Menus { get; set; }

        #endregion // MEF Imports

        #region Properties and Associated Member Data

        /// <summary>
        /// Main menu (sorted extensions).
        /// </summary>
        public IEnumerable<IWorkbenchCommand> MainMenu { get; private set; }

        /// <summary>
        /// Toolbars (sorted extensions).
        /// </summary>
        public IEnumerable<ToolBar> ToolBars { get; private set; }

        public InputBindingCollection InputCollection
        {
            get { return m_inputCollection; }
            set { m_inputCollection = value; }
        }
        private InputBindingCollection m_inputCollection;
 
        /// <summary>
        /// Task percentage complete.
        /// </summary>
        public int TaskPercentageComplete
        {
            get { return m_taskPercentComplete; }
            set
            {
                m_taskPercentComplete = value;
                OnPropertyChanged("TaskPercentageComplete");
            }
        }
        private int m_taskPercentComplete;

        /// <summary>
        /// Task message.
        /// </summary>
        public string TaskMessage
        {
            get { return m_taskMessage; }
            set
            {
                m_taskMessage = value;
                OnPropertyChanged("TaskMessage");
            }
        }

        private string m_taskMessage;

        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public WorkbenchViewModel()
        {
            Log.Log__Debug("WorkbenchViewModel constructed");
        }
        #endregion // Constructor(s)

        #region IPartImportsSatisfiedNotification Methods

        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {
            Log.Log__Profile("Workbench OnImportsSatisfied");
            // Sort main menu.
            this.MainMenu = ExtensionService.Sort(Menus);
            List<KeyBinding> keyBindings = new List<KeyBinding>();
            foreach (IWorkbenchCommand command in this.Menus)
            {
                GetKeyBindings(command, ref keyBindings);
            }
            this.InputCollection = new InputBindingCollection(keyBindings);

            // Sort Toolbars
            IEnumerable<IToolbar> sortedToolbars = ExtensionService.Sort(ImportedToolBars);
            List<ToolBar> toolbarControls = new List<ToolBar>();
            foreach (IToolbar toolbar in sortedToolbars)
            {
                ToolBar toolbarControl = new ToolBar();
                toolbarControl.ItemsSource = toolbar.Items;
                toolbarControls.Add(toolbarControl);
            }
            this.ToolBars = toolbarControls;

            this.LayoutManager.Loaded += LayoutManager_Loaded;
            this.LayoutManager.LayoutUpdated += new EventHandler(LayoutManager_LayoutUpdated);

            // Start the REST web services
            this.WebService.Start();

            Log.Log__ProfileEnd();
        }

        /// <summary>
        /// Syncs the input collection to the windows avaiable
        /// and makes sure any command bindings are setup correctly
        /// </summary>
        void LayoutManager_LayoutUpdated(Object sender, EventArgs e)
        {
            foreach (Window window in Application.Current.Windows)
            {
                window.CommandBindings.Clear();
                window.InputBindings.Clear();

                window.InputBindings.AddRange(this.InputCollection);

                CommandBinding undoCommand = new CommandBinding(ApplicationCommands.Undo);
                undoCommand.PreviewCanExecute += CommandBindingPreviewCanExecute;

                CommandBinding redoCommand = new CommandBinding(ApplicationCommands.Redo);
                redoCommand.PreviewCanExecute += CommandBindingPreviewCanExecute;

                window.CommandBindings.Add(undoCommand);
                window.CommandBindings.Add(redoCommand);
            }
        }

        /// <summary>
        /// Gets all the key bindings from a root command
        /// </summary>
        private void GetKeyBindings(IWorkbenchCommand root, ref List<KeyBinding> keyBindings)
        {
            if (root.KeyGesture != null)
            {
                keyBindings.Add(new KeyBinding(root, root.KeyGesture));
            }
            if (root.Items != null)
            {
                foreach (IWorkbenchCommand child in root.Items)
                {
                    GetKeyBindings(child, ref keyBindings);
                }
            }
        }

        #endregion // IPartImportsSatisfiedNotification Methods
        
        #region IDisposable Implementation
        /// <summary>
        /// Stop the web service
        /// </summary>
        public void Dispose()
        {
            WebService.Stop();
        }
        #endregion // IDisposable Implementation
        
        #region Application Commands

        /// <summary>
        /// Stops a application command getting executed with the default handlers and instead
        /// makes them bubble up to find our command system.
        /// </summary>
        private void CommandBindingPreviewCanExecute(Object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
            e.Handled = true;
            e.ContinueRouting = true;
        }

        #endregion // Application Commands

        #region Event Handlers
        internal void ExternalLayoutCommandRecieved(string layoutname)
        {
            string globalFile = Path.Combine(this.ConfigurationService.Config.ToolsConfig, "workbench", "GlobalLayouts.xml");
            List<LayoutOptionViewModel> globalViews = GetLayoutOptionViewModels(globalFile, this.LayoutManager);

            LayoutOptionViewModel option = null;
            foreach (LayoutOptionViewModel layout in globalViews)
            {
                if (layout.Name == layoutname)
                {
                    option = layout;
                    break;
                }
            }

            if (option == null)
            {
                return;
            }

            this.Dispatcher.Invoke(DispatcherPriority.Normal,
                    (Action)delegate()
                    {
                        this.LayoutManager.MergeToolWindows(option.Xml.Trim());
                    } );
        }

        /// <summary>
        /// Get called once the layout manager is loaded, this is used to restore the
        /// last active layout
        /// </summary>
        private void LayoutManager_Loaded(Object sender, EventArgs e)
        {
            string globalFile = Path.Combine(this.ConfigurationService.Config.ToolsConfig, "workbench", "GlobalLayouts.xml");
            List<LayoutOptionViewModel> globalViews = GetLayoutOptionViewModels(globalFile, this.LayoutManager);
            if ((Application.Current as App).layoutArgument != null)
            {
                LayoutOptionViewModel option = null;
                foreach (LayoutOptionViewModel layout in globalViews)
                {
                    if (layout.Name == (Application.Current as App).layoutArgument)
                    {
                        option = layout;
                        break;
                    }
                }

                if (option != null)
                {
                    Properties.Settings.Default.OpenedFiles = null;
                    Properties.Settings.Default.Save();
                    this.LayoutManager.RestoreLayout(option.Xml);
                    return;
                }
            }

            LayoutOptionViewModel savedOption = null;
            if (!string.IsNullOrWhiteSpace(Properties.Settings.Default.LayoutPickerSavedOption))
            {
                foreach (LayoutOptionViewModel layout in globalViews)
                {
                    if (layout.Name == Properties.Settings.Default.LayoutPickerSavedOption)
                    {
                        savedOption = layout;
                        break;
                    }
                }
            }

            if (Properties.Settings.Default.AskUserForLayout)
            {
                if (globalViews.Count > 0)
                {
                    LayoutPickerWindow window = new LayoutPickerWindow();
                    window.AskOnStartup.Visibility = Visibility.Visible;
                    window.LayoutList.ItemsSource = globalViews;
                    window.Owner = Application.Current.MainWindow;
                    if (savedOption != null)
                    {
                        window.LayoutList.SelectedItem = savedOption;
                        //window.LayoutList.SelectedIndex = globalViews.IndexOf(savedOption);
                    }

                    if (window.ShowDialog() != false)
                    {
                        LayoutOptionViewModel selected = window.LayoutList.SelectedItem as LayoutOptionViewModel;
                        string xml = selected.Xml;

                        Properties.Settings.Default.LayoutPickerSavedOption = selected.Name;
                        this.LayoutManager.RestoreLayout(xml);
                        Properties.Settings.Default.AskUserForLayout = (bool)window.AskOnStartup.IsChecked;
                        Properties.Settings.Default.LastActiveLayout = selected.Name;
                        if (selected.Name != "Layout used in previous session")
                        {
                            Properties.Settings.Default.OpenedFiles = null;
                        }

                        Properties.Settings.Default.Save();
                        return;
                    }
                }
            }
            else
            {
                if (savedOption != null)
                {
                    if (savedOption.Name != "Layout used in previous session")
                    {
                        Properties.Settings.Default.OpenedFiles = null;
                    }

                    Properties.Settings.Default.Save();

                    this.LayoutManager.RestoreLayout(savedOption.Xml);
                    return;
                }
            }

            this.LayoutManager.RestoreLayout(Workbench.UI.Layout.LayoutManager.DefaultLayout);
        }

        public static List<LayoutOptionViewModel> GetLayoutOptionViewModels(string globalFile, ILayoutManager layoutManager)
        {
            List<LayoutOptionViewModel> globalViews = new List<LayoutOptionViewModel>();
            if (File.Exists(globalFile))
            {
                try
                {
                    using (XmlReader reader = XmlReader.Create(globalFile))
                    {
                        reader.MoveToContent();
                        while (reader.NodeType != XmlNodeType.None)
                        {
                            if (!reader.IsStartElement())
                            {
                                reader.Read();
                                continue;
                            }

                            if (reader.Name == "layouts")
                            {
                                reader.Read();
                                continue;
                            }

                            if (reader.Name != "layout")
                            {
                                reader.Skip();
                                continue;
                            }

                            string layoutName = reader.GetAttribute("name");
                            string layoutXml = reader.ReadInnerXml();
                            globalViews.Add(new LayoutOptionViewModel(layoutName, layoutXml, true));

                            reader.Read();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.StaticLog.Exception(ex, "Failed to load global views");
                }
            }

            foreach (KeyValuePair<string, string> saved in layoutManager.SavedLayouts)
            {
                globalViews.Add(new LayoutOptionViewModel(saved.Key, saved.Value, false));
            }

            if (globalViews.Count != 0)
            {
                if (!String.IsNullOrEmpty(Properties.Settings.Default.LastPresentLayout))
                {
                    globalViews.Add(new LayoutOptionViewModel("Layout used in previous session", Properties.Settings.Default.LastPresentLayout, false));
                }
            }

            return globalViews;
        }
        #endregion // Event Handlers
    }
 
    /// <summary>
    /// 
    /// </summary>
    public class InputGestureConverter : IValueConverter
    {
        #region IValueConverter Members

        /// <summary>
        /// Converts a unsigned integer value to a Color.
        /// </summary>
        public Object Convert(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is KeyGesture)
            {
                KeyGesture keyGesture = value as KeyGesture;
                String text = String.Empty;
                if (keyGesture.Modifiers.HasFlag(ModifierKeys.Control))
                {
                    text += "Ctrl";
                }
                if (keyGesture.Modifiers.HasFlag(ModifierKeys.Shift))
                {
                    if (text != String.Empty)
                        text += "+Shift";
                    else
                        text += "Shift";
                }
                if (keyGesture.Modifiers.HasFlag(ModifierKeys.Alt))
                {
                    if (text != String.Empty)
                        text += "+Alt";
                    else
                        text += "Alt";
                }
                if (keyGesture.Modifiers.HasFlag(ModifierKeys.Windows))
                {
                    if (text != String.Empty)
                        text += "+Win";
                    else
                        text += "Win";
                }
                if (text != String.Empty)
                    text += "+" + keyGesture.Key.ToString();
                else
                    text += keyGesture.Key.ToString();

                return text;
            }

            return null;
        }

        /// <summary>
        /// Converts a Color to a unsigned integer value.
        /// </summary>
        public Object ConvertBack(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    } // InputGestureConverter

    /// <summary>
    /// 
    /// </summary>
    public class ToolBarItemDataTemplateSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(Object item, DependencyObject container)
        {
            FrameworkElement element = container as FrameworkElement;
            IWorkbenchCommand wbCommand = item as IWorkbenchCommand;

            if (element != null && item != null && wbCommand != null)
            {
                // Special case code for combo box items.
                if (item is WorkbenchCommandComboBoxItem)
                {
                    ComboBoxItem visualCbItem = container.GetVisualParent<ComboBoxItem>();
                    if (visualCbItem == null && wbCommand.CustomSelectedDataTemplateName != null)
                    {
                        return element.FindResource(wbCommand.CustomSelectedDataTemplateName) as DataTemplate;
                    }
                }

                if (!String.IsNullOrEmpty(wbCommand.CustomDataTemplateName))
                {
                    return element.FindResource(wbCommand.CustomDataTemplateName) as DataTemplate;
                }
                else if (wbCommand.IsLabel == true)
                {
                    return element.FindResource("ToolBarLabel") as DataTemplate;
                }
                else if (wbCommand.IsImage == true)
                {
                    return element.FindResource("ToolBarImage") as DataTemplate;
                }
                else if (wbCommand.IsSeparator == true)
                {
                    return element.FindResource("ToolBarSeparator") as DataTemplate;
                }
                else if (wbCommand.IsCombobox == true)
                {
                    return element.FindResource("ToolBarCombobox") as DataTemplate;
                }
                else if (wbCommand.IsToggle == true)
                {
                    return element.FindResource("ToolBarButtonWithToggle") as DataTemplate;
                }
                else
                {
                    return element.FindResource("ToolBarButton") as DataTemplate;
                }
            }

            return null;
        }
    } // ToolBarItemDataTemplateSelector

    /// <summary>
    /// 
    /// </summary>
    public class TooltipInputGestureConverter : IMultiValueConverter
    {
        #region IValueConverter Members

        public Object Convert(Object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values.Length == 2)
            {
                if (values[0] is String)
                {
                    String keyGesturetext = String.Empty;
                    if (values[1] is KeyGesture)
                    {
                        KeyGesture keyGesture = values[1] as KeyGesture;
                        if (keyGesture.Modifiers.HasFlag(ModifierKeys.Control))
                        {
                            keyGesturetext += "Ctrl";
                        }
                        if (keyGesture.Modifiers.HasFlag(ModifierKeys.Shift))
                        {
                            if (keyGesturetext != String.Empty)
                                keyGesturetext += "+Shift";
                            else
                                keyGesturetext += "Shift";
                        }
                        if (keyGesture.Modifiers.HasFlag(ModifierKeys.Alt))
                        {
                            if (keyGesturetext != String.Empty)
                                keyGesturetext += "+Alt";
                            else
                                keyGesturetext += "Alt";
                        }
                        if (keyGesture.Modifiers.HasFlag(ModifierKeys.Windows))
                        {
                            if (keyGesturetext != String.Empty)
                                keyGesturetext += "+Win";
                            else
                                keyGesturetext += "Win";
                        }
                        if (keyGesturetext != String.Empty)
                            keyGesturetext += "+" + keyGesture.Key.ToString();
                        else
                            keyGesturetext += keyGesture.Key.ToString();
                    }

                    return String.Format("{0} ({1})", values[0] as String, keyGesturetext);
                }
            }
            else if (values.Length == 1)
            {
                if (values[0] is String)
                {
                    return values[0] as String;
                }
            }
            return null;
        }

        /// <summary>
        /// Converts a Color to a unsigned integer value.
        /// </summary>
        public Object[] ConvertBack(Object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    } // TooltipInputGestureConverter

} // Workbench namespace
