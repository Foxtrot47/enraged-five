﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using RSG.Platform;

namespace MetadataEditor.RestData
{
    /// <summary>
    /// Texture data that comes from RAG
    /// </summary>
    [DataContract(Namespace="")]
    public class MetadataInfo : IExtensibleDataObject
    {
        #region Properties
        [DataMember]
        public string AssetName { get; set; }

        [DataMember(Name = "AssetType")]
        public string AssetTypeString { get; set; }

        [DataMember]
        public string RPFFilename { get; set; }

        [DataMember]
        public string TextureName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public FileType AssetType
        {
            get
            {
                switch (AssetTypeString)
                {
                    case "Txd":
                        return FileType.TextureDictionary;

                    case "Dwd":
                        return FileType.DrawableDictionary;

                    case "Draw":
                        return FileType.Drawable;

                    case "Frag":
                        return FileType.Fragment;

                    default:
                        throw new ArgumentException("Unknown asset type encountered");
                }
            }
        }
        #endregion // Properties

        #region IExtensibleDataObject Implementation
        /// <summary>
        /// Gets or sets the structure that contains extra data.
        /// </summary>
        public ExtensionDataObject ExtensionData { get; set; }
        #endregion
    } // RAGFileData
} // MetadataEditor.RestData
