﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn.Commands;
using Workbench.AddIn;
using WidgetEditor.AddIn;

namespace WidgetEditor.UI.Toolbar
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(WidgetEditor.AddIn.ExtensionPoints.LiveEditingToolbar, typeof(IWorkbenchCommand))]
    public class RefreshConnectionsToolbarButton : WorkbenchMenuItemBase
    {
        #region Constants
        /// <summary>
        /// Unique GUID for this button
        /// </summary>
        public static readonly Guid GUID = new Guid("1F319413-70E2-42B1-999B-20F9C61BED55");
        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(WidgetEditor.AddIn.CompositionPoints.ProxyService, typeof(IProxyService))]
        private IProxyService ProxyService { get; set; }
        #endregion // MEF Imports
        
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public RefreshConnectionsToolbarButton()
            : base()
        {
            this.ID = GUID;
            this.RelativeToolBarID = ConnectionsCombobox.GUID;
            this.Direction = Workbench.AddIn.Services.Direction.After;
            SetImageFromBitmap(WidgetEditor.Resources.Images.refresh);
            this.ToolTip = "Refresh Game Connections";
        }
        #endregion // Constructor(s)

        #region ICommand Implementation
        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
            ProxyService.RefreshGameConnections();
        }
        #endregion // ICommand Implementation
    } // RefreshConnectionsToolbarButton
}
