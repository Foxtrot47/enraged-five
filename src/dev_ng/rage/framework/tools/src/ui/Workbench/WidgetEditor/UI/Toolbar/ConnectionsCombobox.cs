﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn.Commands;
using Workbench.AddIn;
using Workbench.AddIn.Services;
using ragCore;
using WidgetEditor.AddIn;
using RSG.Base.Editor.Command;
using System.Net;
using RSG.Base.Net;
using System.ComponentModel;
using WdigetEditor.UI.Toolbar;
using RSG.Base.Logging;

namespace WidgetEditor.UI.Toolbar
{
    [ExportExtension(WidgetEditor.AddIn.ExtensionPoints.LiveEditingToolbar, typeof(IWorkbenchCommand))]
    [ExportExtension(WidgetEditor.AddIn.CompositionPoints.ProxyService, typeof(IProxyService))]
    public class ConnectionsCombobox : WorkbenchCommandCombobox, IProxyService
    {
        #region Constants
        /// <summary>
        /// Unique GUID for the build combo box
        /// </summary>
        public static readonly Guid GUID = new Guid("156EAB29-B442-456E-8882-7C920E02EE5F");
        #endregion // Constants

        #region Events
        /// <summary>
        /// Event for when the selected game connection has changed
        /// </summary>
        public event GameConnectionChangedEventHandler GameConnectionChanged;
        #endregion

        #region Properties
        /// <summary>
        /// Flag indicating whether we are connected to the RAG proxy
        /// </summary>
        public bool IsProxyConnected
        {
            get { return m_isProxyConnected; }
            set
            {
                SetPropertyValue(value, () => this.IsProxyConnected,
                    new PropertySetDelegate(delegate(Object newValue) { m_isProxyConnected = (bool)newValue; }));
            }
        }
        private bool m_isProxyConnected;

        /// <summary>
        /// Port that we are using to connect to the proxy
        /// </summary>
        public int ProxyPort
        {
            get { return m_proxyPort; }
            set
            {
                SetPropertyValue(value, () => this.ProxyPort,
                    new PropertySetDelegate(delegate(Object newValue) { m_proxyPort = (int)newValue; }));
            }
        }
        private int m_proxyPort;

        /// <summary>
        /// Flag indicating whether we are connected to a game
        /// </summary>
        public bool IsGameConnected
        {
            get
            {
                return (SelectedGame != null ? SelectedGame.Connected : false);
            }
        }

        /// <summary>
        /// The game we are currently connected to
        /// </summary>
        public GameConnection SelectedGame
        {
            get
            {
                ConnectionsComboboxItem label = SelectedItem as ConnectionsComboboxItem;
                return (label != null ? label.GameConnection : null);
            }
        }

        /// <summary>
        /// Platform of the game we are currently connected to
        /// </summary>
        public string SelectedGamePlatform
        {
            get
            {
                GameConnection currentGame = SelectedGame;
                if (currentGame != null)
                {
                    return currentGame.Platform;
                }
                else
                {
                    return "Not Connected";
                }
            }
        }

        /// <summary>
        /// IP address of the game we are currently connected to
        /// </summary>
        public IPAddress SelectedGameIP
        {
            get
            {
                GameConnection currentGame = SelectedGame;
                if (currentGame != null)
                {
                    IPAddress ip;
                    IPAddress.TryParse(currentGame.IPAddress, out ip);
                    return ip;
                }
                else
                {
                    return IPAddress.None;
                }
            }
        }

        /// <summary>
        /// Remote console for sending commands to RAG (via the proxy)
        /// </summary>
        public cRemoteConsole Console
        {
            get
            {
                return m_Console;
            }
        }
        private cRemoteConsole m_Console = new cRemoteConsole();
        #endregion

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ConnectionsCombobox()
            : base()
        {
            ID = GUID;
            RelativeToolBarID = LiveEditingToolbarLabel.GUID;
            Direction = Direction.After;
            Items = new List<IWorkbenchCommand>();

            // Temp until we can sort out the proxy connection side of things
            IsProxyConnected = true;

            RefreshGameConnections();
        }
        #endregion // Constructor(s)

        #region WorkbenchCommandCombobox Overrides
        /// <summary>
        /// Callback for whenever the selected item changes
        /// </summary>
        /// <param name="oldValue"></param>
        /// <param name="newValue"></param>
        protected override void PostSelectedItemChanged(IWorkbenchCommand oldValue, IWorkbenchCommand newValue)
        {
            ConnectionsComboboxItem oldLabel = oldValue as ConnectionsComboboxItem;
            ConnectionsComboboxItem newLabel = newValue as ConnectionsComboboxItem;

            GameConnection oldConnection = (oldLabel != null ? oldLabel.GameConnection : null);
            GameConnection newConnection = (newLabel != null ? newLabel.GameConnection : null);

            if (oldConnection != null && oldConnection.Connected && Console.IsConnected())
            {
                Console.DisconnectConsole();
            }

            if (newConnection != null && newConnection.Connected)
            {
                Console.Connect();
            }

            // Check whether the underlying game connection is different and whether we need to fire off the event
            if (oldConnection != newConnection && GameConnectionChanged != null)
            {
                GameConnectionChanged(this, new GameConnectionChangedEventArgs(oldConnection, newConnection));
            }
        }
        #endregion // WorkbenchCommandCombobox Overrides

        #region IProxyService Implementation
        /// <summary>
        /// 
        /// </summary>
        public void RefreshGameConnections()
        {
            Log.Log__Profile("ProxyService RefreshGameConnections()");
            UpdateGameList(null, null);
            Log.Log__ProfileEnd();
        }
        #endregion // IProxyService Implementation

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void UpdateGameList(object s, DoWorkEventArgs e)
        {
            // Get the updated list of game connections
            IList<GameConnection> gameConnections = GameConnection.AcquireGameList(LogFactory.ApplicationLog);

            // Get the list of existing connections
            IEnumerable<GameConnection> existingConnections = Items.Select(item => ((ConnectionsComboboxItem)item).GameConnection);

            // Check if the list has changed
            if (gameConnections.Count() != existingConnections.Count() ||
                gameConnections.Intersect(existingConnections).Count() != gameConnections.Count())
            {
                GameConnection previousConnection = SelectedGame;

                List<IWorkbenchCommand> newItems = new List<IWorkbenchCommand>();
                newItems.AddRange(gameConnections.Select(item => new ConnectionsComboboxItem(item)));
                Items = newItems;

                // Check if the previously selected item is still there
                if (!gameConnections.Contains(previousConnection) || !previousConnection.Connected)
                {
                    // Its not there, so attempt to select another connection (one that is connected)
                    IWorkbenchCommand newConnectionItem = Items.FirstOrDefault(item => ((ConnectionsComboboxItem)item).GameConnection.Connected);

                    if (newConnectionItem != null)
                    {
                        SelectedItem = newConnectionItem;
                    }
                    else
                    {
                        SelectedItem = Items.FirstOrDefault();
                    }
                }
                else
                {
                    // Update the selected item to point to the new ComboboxItem
                    SelectedItem = Items.First(item => ((ConnectionsComboboxItem)item).GameConnection.Equals(previousConnection));
                }
            }
        }
        #endregion // Private Methods
    } // ConnectionsCombobox
}
