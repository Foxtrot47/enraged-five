﻿namespace Workbench.AddIn.Bugstar.Reports
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using PerforceBrowser.AddIn;
    using RSG.Base.Tasks;
    using RSG.Interop.Bugstar.Search;
    using RSG.Model.Report;
    using RSG.SourceControl.Perforce;
    using Workbench.AddIn.Services;

    /// <summary>
    /// Defines the report used to list containers against outsource bug counts.
    /// </summary>
    internal class BugstarOutsourceReport : CSVReport, IDynamicReport
    {
        #region Fields
        /// <summary>
        /// The name of this report.
        /// </summary>
        private const string ReportName = "RDU Containers";

        /// <summary>
        /// The description for this report.
        /// </summary>
        private const string ReportDescription = "Bugstar Outsource Report";

        /// <summary>
        /// The private field used for the <see cref="GenerationTask"/> property.
        /// </summary>
        private ITask generationTask;

        /// <summary>
        /// The private reference to the configuration service to use while generating this
        /// report.
        /// </summary>
        private IConfigurationService configService;

        /// <summary>
        /// The private reference to the bugstar service to use while generating this report.
        /// </summary>
        private IBugstarService bugstarService;

        /// <summary>
        /// The private reference to the perforce service to use while generating this report.
        /// </summary>
        private IPerforceService perforceService;

        /// <summary>
        /// The private reference to the perforce service to use while generating this report.
        /// </summary>
        private IMessageService messageService;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="BugstarOutsourceReport"/> class.
        /// </summary>
        /// <param name="bugstarService">
        /// A reference to the bugstar service to use while generating this report.
        /// </param>
        /// <param name="configService">
        /// A reference to the configuration service to use while generating this report.
        /// </param>
        public BugstarOutsourceReport(
            IBugstarService bugstarService,
            IConfigurationService configService,
            IPerforceService perforceService,
            IMessageService messageService)
        {
            this.Name = ReportName;
            this.Description = ReportDescription;
            this.bugstarService = bugstarService;
            this.configService = configService;
            this.perforceService = perforceService;
            this.messageService = messageService;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the task to run for generating this report.
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (this.generationTask == null)
                {
                    this.generationTask = new ActionTask(
                        "Generating report", this.GenerateReport);
                }

                return this.generationTask;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Generates the report dynamically.
        /// </summary>
        /// <param name="context">
        /// The context for this asynchronous task which includes the cancellation token.
        /// </param>
        /// <param name="progress">
        /// The progress object to use to report the progress of the generation.
        /// </param>
        private void GenerateReport(ITaskContext context, IProgress<TaskProgress> progress)
        {
            DynamicReportContext dynamicContext = context as DynamicReportContext;
            if (dynamicContext == null)
            {
                return;
            }

            try
            {
                Graph outsourceGraph = null;
                foreach (Graph graph in this.bugstarService.GetUserGraphs())
                {
                    if (graph.Name != "ALL RDU BUGS")
                    {
                        continue;
                    }

                    outsourceGraph = graph;
                    break;
                }

                //if (outsourceGraph == null)
                //{
                    this.messageService.Show("Unable to show reprt as you don't have access to the correct bugstar graph.");
                    return;
                //}
 
                P4 p4 = this.perforceService.PerforceConnection;
                p4.Connect();
                string artDirectory = this.configService.Config.Project.DefaultBranch.Art;
                string cityE = Path.Combine(artDirectory, "Models", "_CityE", "...", "*.maxc");
                string cityW = Path.Combine(artDirectory, "Models", "_CityW", "...", "*.maxc");
                string hills = Path.Combine(artDirectory, "Models", "_Hills", "...", "*.maxc");
                string prologue = Path.Combine(artDirectory, "Models", "_prologue", "...", "*.maxc");

                Dictionary<string, FileState> fileStates = new Dictionary<string, FileState>();
                foreach (FileState fileState in FileState.Create(p4, cityE))
                {
                    if (fileState.HeadAction == FileAction.Delete ||
                        fileState.HeadAction == FileAction.MoveAndDelete)
                    {
                        continue;
                    }

                    if (fileState.ClientFilename.Contains("WIP") || fileState.ClientFilename.Contains("Old_Max_Files"))
                    {
                        continue;
                    }

                    string name = Path.GetFileNameWithoutExtension(fileState.ClientFilename);
                    fileStates.Add(name.ToLower(), fileState);
                }

                foreach (FileState fileState in FileState.Create(p4, cityW))
                {
                    if (fileState.HeadAction == FileAction.Delete ||
                        fileState.HeadAction == FileAction.MoveAndDelete)
                    {
                        continue;
                    }

                    if (fileState.ClientFilename.Contains("WIP") || fileState.ClientFilename.Contains("Old_Max_Files"))
                    {
                        continue;
                    }

                    string name = Path.GetFileNameWithoutExtension(fileState.ClientFilename);
                    fileStates.Add(name.ToLower(), fileState);
                }

                foreach (FileState fileState in FileState.Create(p4, hills))
                {
                    if (fileState.HeadAction == FileAction.Delete ||
                        fileState.HeadAction == FileAction.MoveAndDelete)
                    {
                        continue;
                    }

                    if (fileState.ClientFilename.Contains("WIP") || fileState.ClientFilename.Contains("Old_Max_Files"))
                    {
                        continue;
                    }

                    string name = Path.GetFileNameWithoutExtension(fileState.ClientFilename);
                    fileStates.Add(name.ToLower(), fileState);
                }

                foreach (FileState fileState in FileState.Create(p4, prologue))
                {
                    if (fileState.HeadAction == FileAction.Delete ||
                        fileState.HeadAction == FileAction.MoveAndDelete)
                    {
                        continue;
                    }

                    if (fileState.ClientFilename.Contains("WIP") || fileState.ClientFilename.Contains("Old_Max_Files"))
                    {
                        continue;
                    }

                    string name = Path.GetFileNameWithoutExtension(fileState.ClientFilename);
                    fileStates.Add(name.ToLower(), fileState);
                }

                using (StreamWriter sw = new StreamWriter(this.Filename))
                {
                    string header = "Container Name,Bug Count,Container Checked Out,Prop Group Checked Out";
                    sw.WriteLine(header);

                    foreach (KeyValuePair<string, int> data in outsourceGraph.DataPoints)
                    {
                        string containerName = data.Key.ToLower();
                        string propContainerName = containerName + "_props";
                        int bugCount = data.Value;
                        string line = string.Format("{0},{1},", data.Key, bugCount.ToString());
                        bool containerCheckedOut = false;
                        FileState containerState = null;
                        if (!fileStates.TryGetValue(containerName, out containerState))
                        {
                            containerCheckedOut = false;
                        }
                        else
                        {
                            containerCheckedOut =
                                containerState.OtherOpen ||
                                containerState.OpenAction == FileAction.Edit;
                        }

                        bool propContainerCheckedOut = false;
                        FileState propContainerState = null;
                        if (!fileStates.TryGetValue(propContainerName, out propContainerState))
                        {
                            propContainerCheckedOut = false;
                        }
                        else
                        {
                            propContainerCheckedOut =
                                propContainerState.OtherOpen ||
                                propContainerState.OpenAction == FileAction.Edit;
                        }

                        if (propContainerCheckedOut && containerCheckedOut)
                        {
                            continue;
                        }

                        line += string.Format("{0},{1}", containerCheckedOut.ToString(), propContainerCheckedOut.ToString());
                        sw.WriteLine(line);
                    }
                }
            }
            catch (System.Exception ex)
            {
                RSG.Base.Logging.Log.Log__Exception(ex, "Unhandled bugstar exception");
            }
        }
        #endregion
    } // Workbench.AddIn.Bugstar.Reports.BugstarOutsourceReport {Class}
} // Workbench.AddIn.Bugstar.Reports {Namespace}
