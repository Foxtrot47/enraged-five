﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Workbench.AddIn.MapStatistics
{
    /// <summary>
    /// Lists the importing GUIDs for the workbench to pickup
    /// </summary>
    internal static class CompositionPoints
    {
        #region Document GUID(s)

        /// <summary>
        /// The main GUID for the map statistics document
        /// </summary>
        public const String MapStatisticsDocument = "E9DAA58E-B97B-4561-BC68-7B14FCC12CAC";

        #endregion // Document GUID(s)

    } // CompositionPoints
} // Workbench.AddIn.MapStatistics
