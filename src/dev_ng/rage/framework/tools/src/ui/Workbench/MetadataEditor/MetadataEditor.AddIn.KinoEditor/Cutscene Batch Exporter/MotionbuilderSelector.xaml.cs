﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MetadataEditor.AddIn.KinoEditor.Cutscene_Batch_Exporter
{
    /// <summary>
    /// Interaction logic for MotionbuilderSelector.xaml
    /// </summary>
    public partial class MotionbuilderSelector : Window
    {
        public MotionbuilderSelector()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
