﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Report.Reports.GameStatsReports.Tests;
using RSG.Base.Editor.Command;

namespace Workbench.AddIn.GameStatistics.Reports.Automated.ViewModels
{
    /// <summary>
    /// 
    /// </summary>
    public class ThreadSmokeTestViewModel : SmokeTestViewModelBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public IDictionary<ThreadSmokeTest.ThreadNameKey, IList<ThreadSmokeTest.ThreadInfo>> HistoricalStats
        {
            get { return m_historicalThreadTimingResults; }
            set
            {
                SetPropertyValue(value, () => this.HistoricalStats,
                          new PropertySetDelegate(delegate(object newValue) { m_historicalThreadTimingResults = (IDictionary<ThreadSmokeTest.ThreadNameKey, IList<ThreadSmokeTest.ThreadInfo>>)newValue; }));
            }
        }
        private IDictionary<ThreadSmokeTest.ThreadNameKey, IList<ThreadSmokeTest.ThreadInfo>> m_historicalThreadTimingResults;
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ThreadSmokeTestViewModel(ThreadSmokeTest smokeTest, string testName)
            : base(smokeTest, testName)
        {
            HistoricalStats = smokeTest.GroupedHistoricalStats.Where(item => item.Key.TestName == testName).ToDictionary(item => item.Key, item => item.Value);
        }
        #endregion // Constructor(s)
    } // ThreadSmokeTestViewModel
}
