﻿using RSG.Model.Common.Map;

namespace Workbench.AddIn.MapStatistics.Overlays
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(ExtensionPoints.ObjectStatisticsOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class DrawableInstanceCountOverlay : StatisticOverlayBase
    {
        #region Constants
        private const string c_name = "Drawable Entity Count";
        private const string c_description = "Shows the number of the drawable entities in the map container.  Use the options below to filter certain entity types in and out.  A drawable entity is defined as any entity to a map drawable defined in the same max file.  (Drawable entities include internal references).";
        #endregion // Constants

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public DrawableInstanceCountOverlay()
            : base(c_name, c_description)
        {
            m_includeNonDynamic = true;
            m_includeDynamic = true;
            m_includeHighDetailed = true;
            m_includeLod = true;
            m_includeSuperLod = true;
            m_includeInterior = false;
            m_dontShowInteriorOptions = true;
            m_dontShowUniqueOptions = true;
            m_dontShowEntityTypeOption = true;
        }
        #endregion // Constructor

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        protected double GetStatisticValue(IMapSection section)
        {
            if (!this.IncludeDynamic && !this.IncludeNonDynamic)
            {
                return 0;
            }

            StatGroup group = StatGroup.DrawableEntities;
            StatEntityType type = StatEntityType.Total;
            if (this.IncludeDynamic && !this.IncludeNonDynamic)
            {
                type = StatEntityType.Dynamic;
            }
            else if (!this.IncludeDynamic && this.IncludeNonDynamic)
            {
                type = StatEntityType.NonDynamic;
            }

            // Gather the information
            double stat = 0;

            if (this.IncludeHighDetailed)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Hd, type)];
                stat += stats.TotalCount;
            }
            if (this.IncludeLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Lod, type)];
                stat += stats.TotalCount;
            }
            if (this.IncludeSuperLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.SLod1, type)];
                stat += stats.TotalCount;
            }

            return stat;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="propgroup"></param>
        /// <returns></returns>
        protected override double GetStatisticValue(IMapSection section, IMapSection propgroup)
        {
            double stat = 0;
            if (section != null)
                stat += GetStatisticValue(section);
            if (propgroup != null)
                stat += GetStatisticValue(propgroup);

            return stat;
        }
        #endregion // Overrides
    } // DrawableInstanceCountOverlay

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(ExtensionPoints.ObjectStatisticsOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class NonDrawableInstanceCountOverlay : StatisticOverlayBase
    {
        #region Constants
        private const string c_name = "Non-Drawable Entity Count";
        private const string c_description = "Show the number of drawable entities in the map container. Use the options below to filter certain entity types in and out. A non-drawable entity is defined as any entity to a drawable that is defined in another max file. (i.e. props).";
        #endregion // Constants

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public NonDrawableInstanceCountOverlay()
            : base(c_name, c_description)
        {
            m_includeNonDynamic = true;
            m_includeDynamic = true;
            m_includeHighDetailed = true;
            m_includeLod = true;
            m_includeSuperLod = true;
            m_includeInterior = false;
            m_dontShowInteriorOptions = true;
            m_dontShowUniqueOptions = true;
            m_dontShowEntityTypeOption = true;
        }
        #endregion // Constructor

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        protected double GetStatisticValue(IMapSection section)
        {
            if (!this.IncludeDynamic && !this.IncludeNonDynamic)
            {
                return 0;
            }

            StatGroup group = StatGroup.NonDrawableEntities;
            StatEntityType type = StatEntityType.Total;
            if (this.IncludeDynamic && !this.IncludeNonDynamic)
            {
                type = StatEntityType.Dynamic;
            }
            else if (!this.IncludeDynamic && this.IncludeNonDynamic)
            {
                type = StatEntityType.NonDynamic;
            }

            // Gather the information
            double stat = 0;

            if (this.IncludeHighDetailed)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Hd, type)];
                stat += stats.TotalCount;
            }
            if (this.IncludeLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Lod, type)];
                stat += stats.TotalCount;
            }
            if (this.IncludeSuperLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.SLod1, type)];
                stat += stats.TotalCount;
            }

            return stat;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="propgroup"></param>
        /// <returns></returns>
        protected override double GetStatisticValue(IMapSection section, IMapSection propgroup)
        {
            double stat = 0;
            if (section != null)
                stat += GetStatisticValue(section);
            if (propgroup != null)
                stat += GetStatisticValue(propgroup);

            return stat;
        }
        #endregion // Overrides
    } // NonDrawableInstanceCountOverlay

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(ExtensionPoints.ObjectStatisticsOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class InteriorCountOverlay : StatisticOverlayBase
    {
        #region Constants
        private const string c_name = "Interior Count";
        private const string c_description = "Shows the number of instances in the map container that are referencing interior drawables. This includes both drawable and non-drawable entities.";
        #endregion // Constants

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public InteriorCountOverlay()
            : base(c_name, c_description)
        {  
            m_includeHighDetailed = false;
            m_includeLod = false;
            m_includeSuperLod = false;
            m_includeDynamic = true;
            m_includeNonDynamic = true;
            m_includeInterior = true;

            m_dontShowObjectOptions = true;
            m_dontShowReferenceOptions = true;
            m_dontShowInteriorOptions = true;
            m_dontShowEntityTypeOption = true;
            m_dontShowUniqueOptions = true;
        }
        #endregion // Constructor

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        protected double GetStatisticValue(IMapSection section)
        {
            if (!this.IncludeDynamic && !this.IncludeNonDynamic)
            {
                return 0;
            }

            RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(StatGroup.InteriorEntities, StatLodLevel.Hd, StatEntityType.Total)];
            return stats.TotalCount;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="propgroup"></param>
        /// <returns></returns>
        protected override double GetStatisticValue(IMapSection section, IMapSection propgroup)
        {
            double stat = 0;
            if (section != null)
                stat += GetStatisticValue(section);
            if (propgroup != null)
                stat += GetStatisticValue(propgroup);

            return stat;
        }
        #endregion // Overrides
    } // InteriorCountOverlay

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(ExtensionPoints.ObjectStatisticsOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class TXDCountOverlay : StatisticOverlayBase
    {
        #region Constants
        private const string c_name = "Entity TXD Count";
        private const string c_description = "Shows the number of unique texture dictionaries that need to be loaded in order to render the entities in the map container.  Use the options below to filter certain entity types in and out.";
        #endregion // Constants

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public TXDCountOverlay()
            : base(c_name, c_description)
        {
            m_includeHighDetailed = true;
            m_includeLod = true;
            m_includeSuperLod = true;
            m_includeDynamic = true;
            m_includeNonDynamic = true;
            m_includeInterior = false;

            m_dontShowObjectOptions = false;
            m_dontShowReferenceOptions = false;
            m_dontShowInteriorOptions = true;
            m_dontShowUniqueOptions = true;
        }
        #endregion // Constructor

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        protected double GetStatisticValue(IMapSection section)
        {
            if (!this.IncludeDynamic && !this.IncludeNonDynamic)
            {
                return 0;
            }
            if (!this.IncludeDrawableEntities && !this.IncludeNonDrawableEntities)
            {
                return 0;
            }

            StatEntityType type = StatEntityType.Total;
            if (this.IncludeDynamic && !this.IncludeNonDynamic)
            {
                type = StatEntityType.Dynamic;
            }
            else if (!this.IncludeDynamic && this.IncludeNonDynamic)
            {
                type = StatEntityType.NonDynamic;
            }

            double stat = 0;
            if (this.IncludeDrawableEntities && !this.IncludeNonDrawableEntities)
            {
                stat = AddStatistic(section, StatGroup.DrawableEntities, type);
            }
            else if (!this.IncludeDrawableEntities && this.IncludeNonDrawableEntities)
            {
                stat = AddStatistic(section, StatGroup.NonDrawableEntities, type);
            }
            else
            {
                stat = AddStatistic(section, StatGroup.AllNonInteriorEntities, type);
            }

            return stat;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stat"></param>
        /// <param name="stats"></param>
        /// <param name="group"></param>
        /// <param name="type"></param>
        private double AddStatistic(IMapSection section, StatGroup group, StatEntityType type)
        {
            double stat = 0.0;

            if (this.IncludeHighDetailed)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Hd, type)];
                stat += stats.TXDCount;
            }
            if (this.IncludeLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Lod, type)];
                stat += stats.TXDCount;
            }
            if (this.IncludeSuperLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.SLod1, type)];
                stat += stats.TXDCount;
            }

            return stat;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="propgroup"></param>
        /// <returns></returns>
        protected override double GetStatisticValue(IMapSection section, IMapSection propgroup)
        {
            double stat = 0;
            if (section != null)
                stat += GetStatisticValue(section);
            if (propgroup != null)
                stat += GetStatisticValue(propgroup);

            return stat;
        }
        #endregion // Overrides
    } // TXDCountOverlay

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(ExtensionPoints.ObjectStatisticsOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class ShaderCountOverlay : StatisticOverlayBase
    {
        #region Constants
        private const string c_name = "Entity Shader Count";
        private const string c_description = "Shows the number of unique shaders that need to be loaded in order to render the entities in the map container.  Two shaders are said to be the same if they both use the same preset and both use the exact same texture combination.  Use the options below to filter certain entitty types in and out.";
        #endregion // Constants

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public ShaderCountOverlay()
            : base(c_name, c_description)
        {
            m_includeHighDetailed = true;
            m_includeLod = true;
            m_includeSuperLod = true;
            m_includeDynamic = true;
            m_includeNonDynamic = true;
            m_includeInterior = false;

            m_dontShowObjectOptions = false;
            m_dontShowReferenceOptions = false;
            m_dontShowInteriorOptions = true;
            m_dontShowUniqueOptions = true;
        }
        #endregion // Constructor

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        protected double GetStatisticValue(IMapSection section)
        {
            if (!this.IncludeDynamic && !this.IncludeNonDynamic)
            {
                return 0;
            }
            if (!this.IncludeDrawableEntities && !this.IncludeNonDrawableEntities)
            {
                return 0;
            }

            StatEntityType type = StatEntityType.Total;
            if (this.IncludeDynamic && !this.IncludeNonDynamic)
            {
                type = StatEntityType.Dynamic;
            }
            else if (!this.IncludeDynamic && this.IncludeNonDynamic)
            {
                type = StatEntityType.NonDynamic;
            }

            double stat = 0;
            if (this.IncludeDrawableEntities && !this.IncludeNonDrawableEntities)
            {
                stat = AddStatistic(section, StatGroup.DrawableEntities, type);
            }
            else if (!this.IncludeDrawableEntities && this.IncludeNonDrawableEntities)
            {
                stat = AddStatistic(section, StatGroup.NonDrawableEntities, type);
            }
            else
            {
                stat = AddStatistic(section, StatGroup.AllNonInteriorEntities, type);
            }

            return stat;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stat"></param>
        /// <param name="stats"></param>
        /// <param name="group"></param>
        /// <param name="type"></param>
        private double AddStatistic(IMapSection section, StatGroup group, StatEntityType type)
        {
            double stat = 0.0;

            if (this.IncludeHighDetailed)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Hd, type)];
                stat += stats.ShaderCount;
            }
            if (this.IncludeLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Lod, type)];
                stat += stats.ShaderCount;
            }
            if (this.IncludeSuperLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.SLod1, type)];
                stat += stats.ShaderCount;
            }

            return stat;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="propgroup"></param>
        /// <returns></returns>
        protected override double GetStatisticValue(IMapSection section, IMapSection propgroup)
        {
            double stat = 0;
            if (section != null)
                stat += GetStatisticValue(section);
            if (propgroup != null)
                stat += GetStatisticValue(propgroup);

            return stat;
        }
        #endregion // Overrides
    } // ShaderCountOverlay

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(ExtensionPoints.ObjectStatisticsOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class TextureCountOverlay : StatisticOverlayBase
    {
        #region Constants
        private const string c_name = "Entity Texture Count";
        private const string c_description = "Shows the number of unique textures that need to be loaded in order to render the entities in the map container.  The unique textures for a drawable are found as the .dds files which are created during an export.  Use the options below to filter certain entity types in and out.";
        #endregion // Constants

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public TextureCountOverlay()
            : base(c_name, c_description)
        {
            m_includeHighDetailed = true;
            m_includeLod = true;
            m_includeSuperLod = true;
            m_includeDynamic = true;
            m_includeNonDynamic = true;
            m_includeInterior = false;

            m_dontShowObjectOptions = false;
            m_dontShowReferenceOptions = false;
            m_dontShowInteriorOptions = true;
            m_dontShowUniqueOptions = true;
        }
        #endregion // Constructor

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        protected double GetStatisticValue(IMapSection section)
        {
            if (!this.IncludeDynamic && !this.IncludeNonDynamic)
            {
                return 0;
            }
            if (!this.IncludeDrawableEntities && !this.IncludeNonDrawableEntities)
            {
                return 0;
            }

            StatEntityType type = StatEntityType.Total;
            if (this.IncludeDynamic && !this.IncludeNonDynamic)
            {
                type = StatEntityType.Dynamic;
            }
            else if (!this.IncludeDynamic && this.IncludeNonDynamic)
            {
                type = StatEntityType.NonDynamic;
            }

            double stat = 0;
            if (this.IncludeDrawableEntities && !this.IncludeNonDrawableEntities)
            {
                stat = AddStatistic(section, StatGroup.DrawableEntities, type);
            }
            else if (!this.IncludeDrawableEntities && this.IncludeNonDrawableEntities)
            {
                stat = AddStatistic(section, StatGroup.NonDrawableEntities, type);
            }
            else
            {
                stat = AddStatistic(section, StatGroup.AllNonInteriorEntities, type);
            }

            return stat;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stat"></param>
        /// <param name="stats"></param>
        /// <param name="group"></param>
        /// <param name="type"></param>
        private double AddStatistic(IMapSection section, StatGroup group, StatEntityType type)
        {
            double stat = 0.0;

            if (this.IncludeHighDetailed)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Hd, type)];
                stat += stats.TextureCount;
            }
            if (this.IncludeLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Lod, type)];
                stat += stats.TextureCount;
            }
            if (this.IncludeSuperLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.SLod1, type)];
                stat += stats.TextureCount;
            }

            return stat;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="propgroup"></param>
        /// <returns></returns>
        protected override double GetStatisticValue(IMapSection section, IMapSection propgroup)
        {
            double stat = 0;
            if (section != null)
                stat += GetStatisticValue(section);
            if (propgroup != null)
                stat += GetStatisticValue(propgroup);

            return stat;
        }
        #endregion // Overrides
    } // TextureCountOverlay
} // Workbench.AddIn.MapStatistics.Overlays
 
