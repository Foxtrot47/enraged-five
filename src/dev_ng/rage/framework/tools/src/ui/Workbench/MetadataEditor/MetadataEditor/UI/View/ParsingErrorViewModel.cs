﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using MetadataEditor.ParsingErrors;
using System.Drawing;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace MetadataEditor.UI.View
{
    /// <summary>
    /// 
    /// </summary>
    public class ParsingErrorViewModel : ViewModelBase
    {
        #region Members

        private ObservableCollection<ParsingErrorGroup> m_errorGroups;
        private RelayCommand m_closeCommand;
        private BitmapSource m_iconSource;
        private bool? m_dialogCloser;
        private String m_message1;
        private String m_message2;

        #endregion // Members

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<ParsingErrorGroup> ErrorGroups
        {
            get { return m_errorGroups; }
            set
            {
                SetPropertyValue(value, () => this.ErrorGroups,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_errorGroups = (ObservableCollection<ParsingErrorGroup>)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public BitmapSource IconSource
        {
            get { return m_iconSource; }
            set
            {
                SetPropertyValue(value, () => this.IconSource,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_iconSource = (BitmapSource)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool? DialogResult
        {
            get { return m_dialogCloser; }
            set
            {
                SetPropertyValue(value, () => this.DialogResult,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_dialogCloser = (bool?)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public String MessageOne
        {
            get { return m_message1; }
            set
            {
                if (m_message1 == value)
                    return;

                SetPropertyValue(value, () => this.MessageOne,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_message1 = (String)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public String MessageTwo
        {
            get { return m_message2; }
            set
            {
                if (m_message2 == value)
                    return;

                SetPropertyValue(value, () => this.MessageTwo,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_message2 = (String)newValue;
                        }
                ));
            }
        }

        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        private ParsingErrorViewModel(IEnumerable<ParsingErrorGroup> errorGroups)
        {
            this.ErrorGroups = new ObservableCollection<ParsingErrorGroup>();
            foreach (ParsingErrorGroup errorGroup in errorGroups)
            {
                this.ErrorGroups.Add(errorGroup);
            }
        }

        #endregion // Constructors

        #region Commands

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (m_closeCommand == null)
                {
                    m_closeCommand = new RelayCommand(param => this.Close(param), param => this.CanClose(param));
                }
                return m_closeCommand;
            }
        }

        #endregion // Commands

        #region Command Callbacks

        /// <summary>
        /// 
        /// </summary>
        private Boolean CanClose(Object param)
        {
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        private void Close(Object param)
        {
            DialogResult = true;
        }

        #endregion // Command Callbacks

        #region Public Static Functions

        /// <summary>
        /// Creates and shows the parsing error window using the given error groups when showing the errors for the
        /// definitions
        /// </summary>
        public static void ShowParserErrorViewForDefinitions(IEnumerable<ParsingErrorGroup> errorGroups)
        {
            Application.Current.Dispatcher.Invoke(
                new Action(
                delegate()
                {
                    ParsingErrorViewModel viewModel = new ParsingErrorViewModel(errorGroups);
                    viewModel.MessageOne = "The metadata definitions have finished loading, however there were a " +
                                           "number of errors during the loading process that can be viewed below.";

                    viewModel.MessageTwo = "Having errors in the metadata definitions will result in the Metadata " +
                                            "Editor being less stable and could potentially result in certain meta files " +
                                            "failing to open. With this being said it is best to get these errors fixed as soon as possible.";

                    System.Drawing.Icon icon = System.Drawing.SystemIcons.Warning;
                    viewModel.IconSource = System.Windows.Interop.Imaging.CreateBitmapSourceFromHIcon(icon.Handle, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());


                    ParsingErrorView view = new ParsingErrorView();
                    try
                    {
                        view.Owner = Application.Current.MainWindow;
                    }
                    catch (InvalidOperationException)
                    {

                    }
                    view.DataContext = viewModel;
                    view.Title = "Metadata Definition Parsing Errors - " + Properties.Settings.Default.MetadataDefinitionRoot;

                    view.Show();
                }),
                null);
        }

        /// <summary>
        /// Creates and shows the parsing error window using the given error groups when showing the
        /// errors for the metadata file loading.
        /// </summary>
        public static void ShowParserErrorViewForFileOpening(IEnumerable<ParsingErrorGroup> errorGroups)
        {
            Application.Current.Dispatcher.Invoke(
                new Action(
                delegate()
                {
                    ParsingErrorViewModel viewModel = new ParsingErrorViewModel(errorGroups);
                    viewModel.MessageOne = "An error occured while trying to load the metadata file which can be viewed below.";

                    viewModel.MessageTwo = "";

                    System.Drawing.Icon icon = System.Drawing.SystemIcons.Error;
                    viewModel.IconSource = System.Windows.Interop.Imaging.CreateBitmapSourceFromHIcon(icon.Handle, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());


                    ParsingErrorView view = new ParsingErrorView();
                    view.Owner = Application.Current.MainWindow;
                    view.DataContext = viewModel;
                    view.Title = "Metadata Definition Parsing Errors - " + Properties.Settings.Default.MetadataDefinitionRoot;

                    view.Show();
                }),
                null);
        }

        #endregion // Public Static Functions
    } // ParsingErrorViewModel

    /// <summary>
    /// 
    /// </summary>
    public static class DialogCloser
    {
        public static readonly DependencyProperty DialogResultProperty = DependencyProperty.RegisterAttached(
            "DialogResult",
            typeof(bool?),
            typeof(DialogCloser),
            new PropertyMetadata(DialogResultChanged));

        private static void DialogResultChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var window = d as Window;
            if (window != null)
                window.DialogResult = e.NewValue as bool?;
        }

        public static void SetDialogResult(Window target, bool? value)
        {
            target.SetValue(DialogResultProperty, value);
        }
    } // DialogCloser
} // MetadataEditor.UI.View
