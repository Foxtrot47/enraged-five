﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Windows.Dialogs;

namespace Workbench.AddIn.Services
{
    /// <summary>
    /// 
    /// </summary>
    [Obsolete("Use ICoreServiceProvider.UserInterfaceService instead.")]
    public interface ILoginService
    {
        #region Methods
        /// <summary>
        /// Prompts the user for his login details, using the supplied predicate to
        /// determine whether the login was successful.
        /// </summary>
        /// <param name="loginPredicate"></param>
        /// <returns>true if the user connected, false if the user hit cancel.</returns>
        bool Login(Func<LoginDetails, LoginResults> loginPredicate);

        /// <summary>
        /// Prompts the user for his login details, using the supplied predicate to
        /// determine whether the login was successful.
        /// </summary>
        /// <param name="loginPredicate"></param>
        /// <param name="title"></param>
        /// <param name="message"></param>
        /// <returns>true if the user connected, false if the user hit cancel.</returns>
        bool Login(Func<LoginDetails, LoginResults> loginPredicate, string title, string message);
        #endregion // Methods
    }
} // Workbench.AddIn.Services
