﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Diagnostics;

namespace Workbench.AddIn.UI.Menu
{

    /// <summary>
    /// 
    /// </summary>
    //[Export(Workbench.AddIn.ExtensionPoints.Styles, typeof(ResourceDictionary))]
    public partial class ContextMenuWrapperView : ResourceDictionary
    {
        public ContextMenuWrapperView()
        {
            InitializeComponent();
        }

        private ContextMenuEventArgs m_args = null;

        private void contentPresenter_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            if (m_args != e)
            {
                m_args = e; // anti-repeat when these context menu wrappers are nested              
                ContentPresenter cp = e.Source as ContentPresenter;
                if (cp != null)
                {

                    IContextMenu contextMenuViewModel =
                        cp.DataContext as IContextMenu;
                    if (contextMenuViewModel != null)
                    {
                        if (contextMenuViewModel.ContextMenuEnabled)
                        {
                            IEnumerable<IMenuItem> items =
                                contextMenuViewModel.ContextMenu as IEnumerable<IMenuItem>;
                            if (items != null)
                            {
                                foreach (IMenuItem item in items)
                                {
                                    // will automatically set all 
                                    // child menu items' context as well
                                    item.Context = contextMenuViewModel;
                                }
                            }
                            else
                            {
                                e.Handled = true;
                            }
                        }
                        else
                        {
                            //e.Handled = true;
                        }
                    }
                    else
                    {
                        e.Handled = true;
                    }
                }
                else
                {
                    e.Handled = true;
                }
            }
        }
    }
}
