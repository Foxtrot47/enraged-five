﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows;
using RSG.Interop.Bugstar.Search;

namespace Workbench.AddIn.Bugstar.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public class GraphTemplateSelector : DataTemplateSelector
    {
        public DataTemplate PieGraphTemplate { get; set; }
        public DataTemplate BarGraphTemplate { get; set; }
        public DataTemplate ColumnGraphTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            FrameworkElement element = container as FrameworkElement;

            if (element != null && item != null && item is BugstarGraphViewModel)
            {
                BugstarGraphViewModel vm = item as BugstarGraphViewModel;

                switch (vm.Graph.GraphType)
                {
                    case GraphType.Pie:
                        return PieGraphTemplate;

                    case GraphType.Bar:
                        return BarGraphTemplate;

                    case GraphType.Column:
                        return ColumnGraphTemplate;
                }
            }

            return null;
        }
    } // GraphTemplateSelector
} // Workbench.AddIn.Bugstar.Reports
