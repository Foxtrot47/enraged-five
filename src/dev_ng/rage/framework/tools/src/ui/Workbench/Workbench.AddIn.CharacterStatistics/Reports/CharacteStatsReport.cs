﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Report;

namespace Workbench.AddIn.CharacterStatistics.Reports
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(ExtensionPoints.Report, typeof(IReport))]
    public class CharacterStatsReport : RSG.Model.Report.Reports.Characters.CharacterStatsReport
    {
    } // CharacterStatsReport
}
