﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using RSG.Model.Common;

namespace ContentBrowser.ViewModel
{
    /// <summary>
    /// Lowest level interface for any class that wants to
    /// represent a object in a browsers items source
    /// </summary>
    public interface IAssetViewModel : IViewModel
    {
        #region Properties

        /// <summary>
        /// A IAsset reference to the model the view model
        /// is representing
        /// </summary>
        IAsset Model { get; }

        /// <summary>
        /// Expanded property.
        /// </summary>
        Boolean IsExpanded { get; set; }

        /// <summary>
        /// The main name property which by default
        /// will be shown as the header for a browser item
        /// </summary>
        String Name { get; }

        /// <summary>
        /// A collection of all the commands that are attached to this view model
        /// </summary>
        ObservableCollection<Workbench.AddIn.Commands.IWorkbenchCommand> Commands { get; set; }

        /// <summary>
        /// The default command for this asset. This can be null but if not will be the command executed
        /// when the user double clicks and the command will be in bold in the right click menu.
        /// </summary>
        Workbench.AddIn.Commands.IWorkbenchCommand DefaultCommand { get; set; }

        #endregion // Properties
    } // IBrowserItemViewModel
} // ContentBrowser.ViewModel
