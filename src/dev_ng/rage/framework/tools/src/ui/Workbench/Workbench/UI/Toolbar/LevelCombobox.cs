﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn.Commands;
using System.ComponentModel.Composition;
using ContentBrowser.AddIn;
using Workbench.AddIn;
using RSG.Model.Common;
using Workbench.AddIn.Services;
using RSG.Model.Asset.Level;
using System.Windows;
using RSG.Base.Logging;
using Workbench.AddIn.Services.Model;
using RSG.Statistics.Common.Dto.GameAssets;

namespace Workbench.UI.Toolbar
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.DataModeToolbar, typeof(IWorkbenchCommand))]
    [ExportExtension(Workbench.AddIn.CompositionPoints.LevelBrowser, typeof(ILevelBrowser))]
    public class LevelCombobox : WorkbenchCommandCombobox, ILevelBrowser
    {
        #region Constants
        /// <summary>
        /// Unique GUID for the platform combo box
        /// </summary>
        public static readonly Guid GUID = new Guid("615B5130-99F7-4040-8A23-483BA456AF1B");
        #endregion // Constants

        #region Events
        /// <summary>
        /// Fired when the selected level changes
        /// </summary>
        public event LevelChangedEventHandler LevelChanged;
        #endregion // Events

        #region MEF Imports
        /// <summary>
        /// The data source browser
        /// </summary>
        [Workbench.AddIn.ImportExtension(Workbench.AddIn.CompositionPoints.DataSourceBrowser, typeof(IDataSourceBrowser))]
        private IDataSourceBrowser DataSourceBrowser { get; set; }

        /// <summary>
        /// The build browser
        /// </summary>
        [Workbench.AddIn.ImportExtension(Workbench.AddIn.CompositionPoints.BuildBrowser, typeof(IBuildBrowser))]
        private IBuildBrowser BuildBrowser { get; set; }

        /// <summary>
        /// The build browser
        /// </summary>
        [Workbench.AddIn.ImportExtension(Workbench.AddIn.CompositionPoints.ModelDataProvider, typeof(IModelDataProvider))]
        private IModelDataProvider ModelProvider { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(IConfigurationService))]
        private Lazy<IConfigurationService> Config { get; set; }

        /// <summary>
        /// MEF import for content browser settings
        /// </summary>
        [ImportExtension(ContentBrowser.AddIn.CompositionPoints.ContentBrowserSettings, typeof(ContentBrowser.AddIn.IContentBrowserSettings))]
        ContentBrowser.AddIn.IContentBrowserSettings Settings { get; set; }
        #endregion // MEF Imports

        #region Properties and Associated Member Data
        /// <summary>
        /// Contains the level that is currently selected
        /// as the level to show
        /// </summary>
        public ILevel SelectedLevel
        {
            get
            {
                LevelComboboxItem label = SelectedItem as LevelComboboxItem;
                return (label != null ? label.Level : null);
            }
        }

        /// <summary>
        /// The main level collection that contains the current levels
        /// </summary>
        public ILevelCollection LevelCollection
        {
            get { return m_levelCollection; }
            set
            {
                SetPropertyValue(value, () => LevelCollection,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_levelCollection = (ILevelCollection)newValue;
                        }
                ));
            }
        }
        private ILevelCollection m_levelCollection;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public LevelCombobox()
            : base()
        {
            this.ID = GUID;
            this.MinWidth = 70;
            this.RelativeToolBarID = LevelLabel.GUID;
            this.Direction = Direction.After;
        }
        #endregion // Constructor(s)

        #region WorkbenchCommandCombobox Overrides
        /// <summary>
        /// Callback for whenever the selected item changes
        /// </summary>
        /// <param name="oldValue"></param>
        /// <param name="newValue"></param>
        protected override void PostSelectedItemChanged(IWorkbenchCommand oldValue, IWorkbenchCommand newValue)
        {
            LevelComboboxItem oldLabel = oldValue as LevelComboboxItem;
            LevelComboboxItem newLabel = newValue as LevelComboboxItem;

            ILevel oldLevel = (oldLabel != null ? oldLabel.Level : null);
            ILevel newLevel = (newLabel != null ? newLabel.Level : null);

            if (newLevel != null)
            {
                // Part of "the plan"
                //ModelProvider.LoadStats(newLevel, new StreamableStat[] { StreamableLevelStat.MapHierarchy, StreamableLevelStat.Characters, StreamableLevelStat.Vehicles, StreamableLevelStat.Weapons });
                //newLevel.LoadStats(new StreamableStat[] { StreamableLevelStat.ExportDataPath, StreamableLevelStat.ProcessedDataPath, StreamableLevelStat.ImageBounds, StreamableLevelStat.BackgroundImage });

                Settings.Level = newLevel.Name;
                Settings.Apply();
            }

            if (LevelChanged != null)
            {
                LevelChanged(this, new LevelChangedEventArgs(oldLevel, newLevel));
            }
        }
        #endregion // WorkbenchCommandCombobox Overrides

        #region ILevelBrowser Implementation
        /// <summary>
        /// 
        /// </summary>
        public void UpdateLevels(DataSource source, BuildDto build)
        {
            LevelCollection = null;
            LevelCollection = ModelProvider.RetrieveLevelCollection(source, (build != null ? build.Identifier : null));

            List<IWorkbenchCommand> allItems = new List<IWorkbenchCommand>();
            if (LevelCollection != null)
            {
                LevelCollection.LoadStats(new StreamableStat[] { StreamableLevelStat.ExportDataPath, StreamableLevelStat.ProcessedDataPath, StreamableLevelStat.ImageBounds, StreamableLevelStat.BackgroundImage });

                foreach (ILevel level in LevelCollection)
                {
                    allItems.Add(new LevelComboboxItem(level));
                }
            }
            this.Items = allItems;

            //
            if (!String.IsNullOrEmpty(Settings.Level))
            {
                IWorkbenchCommand levelItem = allItems.Cast<LevelComboboxItem>().FirstOrDefault(item => item.Level.Name == Settings.Level);
                if (levelItem == null)
                {
                    Log.Log__Warning("Previous level with name '{0}' is no longer available.", Settings.Level);
                    levelItem = allItems.FirstOrDefault();
                }
                this.SelectedItem = levelItem;
            }
            else
            {
                this.SelectedItem = allItems.FirstOrDefault();
            }
        }

        /// <summary>
        /// Changes the selection of the content
        /// browser to the given level if it exists.
        /// </summary>
        public void SelectLevel(ILevel level)
        {
            IWorkbenchCommand levelLabel = Items.FirstOrDefault(item => (item as LevelComboboxItem).Level == level);

            if (levelLabel != null)
            {
                SelectedItem = levelLabel;
            }
        }

        /// <summary>
        /// Changes the selection of the content
        /// browser to the given level name if it exists.
        /// </summary>
        public void SelectLevel(String levelName)
        {
            ILevel level = LevelCollection.FirstOrDefault(item => item.Name == levelName);

            if (level != null)
            {
                SelectLevel(level);
            }
        }
        #endregion // ILevelBrowser Implementation
    } // ContentBrowserToolbarLevelCombobox
}
