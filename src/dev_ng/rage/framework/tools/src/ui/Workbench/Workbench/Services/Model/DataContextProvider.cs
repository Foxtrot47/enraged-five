﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using RSG.Base.Logging;
using RSG.Model.Common;
using RSG.Statistics.Common.Dto.GameAssets;
using Workbench.AddIn;
using Workbench.AddIn.Services;
using Workbench.AddIn.Services.Model;

namespace Workbench.Services.Model
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Workbench.AddIn.CompositionPoints.DataContextProvider, typeof(IDataContextProvider))]
    public class DataContextProvider : IDataContextProvider, IPartImportsSatisfiedNotification
    {
        #region MEF Imports
        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.DataSourceBrowser, typeof(IDataSourceBrowser))]
        private IDataSourceBrowser DataSourceBrowser { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.BuildBrowser, typeof(IBuildBrowser))]
        private IBuildBrowser BuildBrowser { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LevelBrowser, typeof(ILevelBrowser))]
        private ILevelBrowser LevelBrowser { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.PlatformBrowser, typeof(IPlatformBrowser))]
        private IPlatformBrowser PlatformBrowser { get; set; }

        /// <summary>
        /// MEF import for content browser settings.
        /// TODO: This should be part of the workbench settings not the content browser plugin settings.
        /// </summary>
        [ImportExtension(ContentBrowser.AddIn.CompositionPoints.ContentBrowserSettings, typeof(ContentBrowser.AddIn.IContentBrowserSettings))]
        ContentBrowser.AddIn.IContentBrowserSettings Settings { get; set; }
        #endregion // MEF Imports

        #region Properties

        public DataContext CurrentContext { get; private set; }
        #endregion // Properties

        #region Events
        /// <summary>
        /// Gets called when the current data context changes.
        /// </summary>
        public event DataContextChangedEventHandler DataContextChanged;
        #endregion // Events

        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private bool m_performingUpdate;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public DataContextProvider()
        {
            m_performingUpdate = false;
        }
        #endregion // Constructor(s)

        #region Event Handlers
        /// <summary>
        /// Callback for when the user has selected a different data source.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnDataSourceChanged(Object sender, DataSourceChangedEventArgs e)
        {
            if (!m_performingUpdate)
            {
                m_performingUpdate = true;

                // Check whether we should update the builds list.
                if (DataSourceBrowser.SelectedSource == DataSource.Database)
                {
                    BuildBrowser.UpdateBuilds(DataSourceBrowser.SelectedSource);
                }

                PlatformBrowser.UpdatePlatforms(DataSourceBrowser.SelectedSource);
                LevelBrowser.UpdateLevels(DataSourceBrowser.SelectedSource, BuildBrowser.SelectedBuild);

                m_performingUpdate = false;
                OnDataContextChanged();
            }
        }

        /// <summary>
        /// Callback for when the use has selected a different build.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnBuildChanged(Object sender, BuildChangedEventArgs e)
        {
            if (!m_performingUpdate)
            {
                m_performingUpdate = true;
                LevelBrowser.UpdateLevels(DataSourceBrowser.SelectedSource, BuildBrowser.SelectedBuild);
                m_performingUpdate = false;
                OnDataContextChanged();
            }
        }

        /// <summary>
        /// Callback for when the use has selected a different level.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnLevelChanged(Object sender, LevelChangedEventArgs e)
        {
            if (!m_performingUpdate)
            {
                OnDataContextChanged();
            }
        }

        /// <summary>
        /// Callback for when the use has selected a different platform.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnPlatformChanged(Object sender, PlatformChangedEventArgs e)
        {
            if (!m_performingUpdate)
            {
                m_performingUpdate = true;

                m_performingUpdate = false;
                OnDataContextChanged();
            }
        }
        #endregion // Event Handlers

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        public void OnDataContextChanged()
        {
            if (DataContextChanged != null)
            {
                // Update the context and send fire off the event.
                DataContext newContext = new DataContext();
                newContext.DataSource = DataSourceBrowser.SelectedSource;
                newContext.Build = BuildBrowser.SelectedBuild;
                newContext.Platform = PlatformBrowser.SelectedPlatform;
                newContext.Level = LevelBrowser.SelectedLevel;

                DataContext oldContext = CurrentContext;
                CurrentContext = newContext;

                DataContextChanged(this, new DataContextChangedEventArgs(oldContext, newContext));
            }
        }
        #endregion // Private Methods

        #region IPartImportsSatisfiedNotification Interface
        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {
            // Update the various browsers.
            DataSourceBrowser.UpdateDataSources();

            // Check whether we should update the builds list.
            if (DataSourceBrowser.SelectedSource == DataSource.Database)
            {
                BuildBrowser.UpdateBuilds(DataSourceBrowser.SelectedSource);
            }

            PlatformBrowser.UpdatePlatforms(DataSourceBrowser.SelectedSource);
            LevelBrowser.UpdateLevels(DataSourceBrowser.SelectedSource, BuildBrowser.SelectedBuild);

            CurrentContext = new DataContext();
            CurrentContext.DataSource = DataSourceBrowser.SelectedSource;
            CurrentContext.Build = BuildBrowser.SelectedBuild;
            CurrentContext.Platform = PlatformBrowser.SelectedPlatform;
            CurrentContext.Level = LevelBrowser.SelectedLevel;

            // Set up the callbacks for monitoring changes to the individual browsers.
            DataSourceBrowser.DataSourceChanged += OnDataSourceChanged;
            BuildBrowser.BuildChanged += OnBuildChanged;
            LevelBrowser.LevelChanged += OnLevelChanged;
            PlatformBrowser.PlatformChanged += OnPlatformChanged;
        }
        #endregion // IPartImportsSatisfiedNotification Interface
    } // DataContextProvider
}
