﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Workbench.AddIn.MapStatistics
{
    public class ExtensionPoints
    {
        /// <summary>
        /// Point to import/export viewport overlays for the cost statistics overlay group
        /// </summary>
        public const String CostStatisticsOverlayGroup = "0378DD40-C0A4-425C-98B1-492677746B94";
        
        /// <summary>
        /// Point to import/export viewport overlays for the object statistics overlay group
        /// </summary>
        public const String ObjectStatisticsOverlayGroup = "DC591982-56FD-4F31-8304-2E5827BEBC84";

        /// <summary>
        /// Point to import/export viewport overlays for the polygon statistics overlay group
        /// </summary>
        public const String PolygonStatisticsOverlayGroup = "28A64BB7-9B0C-4470-B9C3-AF4B3CD77A04";
 
        /// <summary>
        /// Point to import/export viewport overlays for the polygon statistics overlay group
        /// </summary>
        public const String NeighbourStatisticsOverlayGroup = "091191E2-4E82-4E09-A799-73831976AF7A";

        /// <summary>
        /// Point to import/export viewport overlays for the bounds statistics overlay group
        /// </summary>
        public const String BoundsStatisticsOverlayGroup = "9AD80581-B109-4270-958C-99491FC7B14A";
        
        /// <summary>
        /// Point to import/export viewport overlays for the density statistics overlay group
        /// </summary>
        public const String DensityStatisticsOverlayGroup = "C0ED9C28-CDE7-42FF-AF39-47775DA77BBF";
        
        /// <summary>
        /// Point to import/export viewport overlays for the collision statistics overlay group
        /// </summary>
        public const String CollisionStatisticsOverlayGroup = "8DF2BCD7-803E-4D36-86C8-0FE30773626C";

        /// <summary>
        /// Point to import/export viewport overlays for the lod distance statistics overlay group
        /// </summary>
        public const String LodDistanceStatisticsOverlayGroup = "3A66C6E0-99FE-4227-BC79-A090701DC3D7";

        /// <summary>
        /// Point to import/export viewport overlays for the prop types overlay group
        /// </summary>
        public const String PropTypeStatisticsOverlayGroup = "AFFA75D0-67BB-42A6-8EDF-3A7210BC3906";

        /// <summary>
        /// Point to import/export viewport overlays for the prop types overlay group
        /// </summary>
        public const String ReportsOverlayGroup = "65320082-77BC-450D-A54F-41DD15B04BEC";
        
        /// <summary>
        /// Point to import/export viewport overlays for the collision group overlay group
        /// </summary>
        public const String CollisionGroupOverlayGroup = "445349D4-50F5-4E7C-B020-3997E7DCD186";

        /// <summary>
        /// Export Stats Report category extension point.
        /// </summary>
        public const String ExportStatsReport = "22A5F882-4627-49E9-BBA4-7E0E34C94B1C";
    } // ExtensionPoints
} // Workbench.AddIn.MapStatistics
