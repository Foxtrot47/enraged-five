﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common.Map;
using RSG.Model.Common;

namespace ContentBrowser.ViewModel.MapViewModels3
{
    /// <summary>
    /// 
    /// </summary>
    public class ArchetypeGroupViewModel<T> : ObservableContainerViewModelBase<IMapArchetype> where T : IMapArchetype
    {
        #region Properties
        /// <summary>
        /// The main name property which by default will be shown as the header for a browser item
        /// </summary>
        public override String Name
        {
            get
            {
                return m_name;
            }
        }
        private string m_name;

        /// <summary>
        /// The number of viewmodels to create before displaying them on the
        /// screen. Less than or equal to 0 implies no batching
        /// </summary>
        protected override uint AssetCreationBatchSize
        {
            get
            {
                return 250;
            }
        }

        /// <summary>
        /// The number of viewmodels to create before displaying them on the
        /// screen. Less than or equal to 0 implies no batching
        /// </summary>
        protected override uint GridCreationBatchSize
        {
            get
            {
                return 250;
            }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public ArchetypeGroupViewModel(string name, IMapSection section)
            : base(section.Archetypes)
        {
            m_name = name;
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// Determines whether a model asset should be displayed as a
        /// child view model asset and a grid asset. Default is always true
        /// </summary>
        protected override bool IsAssetValid(IAsset asset)
        {
            return asset is T;
        }

        /// <summary>
        /// Determines whether a model asset should be displayed as a
        /// grid asset. Default is always true
        /// </summary>
        protected override bool IsGridAssetValid(IAsset asset)
        {
            return asset is T;
        }
        #endregion // Overrides
    } // ArchetypeGroupViewModel<T>
}
