﻿using System;
using System.Collections.Generic;
using RSG.Base.Editor;
using RSG.SourceControl.Perforce;

namespace Workbench.AddIn.Perforce.ViewModel
{

    /// <summary>
    /// 
    /// </summary>
    internal class ConnectionViewModel : ViewModelBase
    {
        #region Properties
        /// <summary>
        /// Perforce port string.
        /// </summary>
        public String Port
        {
            get { return m_Connection.Port; }
        }

        /// <summary>
        /// 
        /// </summary>
        public WorkspaceViewModel Workspace
        {
            get;
            protected set;
        }

        /// <summary>
        /// 
        /// </summary>
        public DepotViewModel[] Depots
        {
            get;
            protected set;
        }

        /// <summary>
        /// Perforce connection.
        /// </summary>
        private Connection m_Connection;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ConnectionViewModel(Connection connection)
        {
            this.m_Connection = connection;
            this.Workspace = new WorkspaceViewModel();
            List<DepotViewModel> depots = new List<DepotViewModel>();
            foreach (Depot depot in this.m_Connection.Workspace.Depots)
            {
                depots.Add(new DepotViewModel(depot));
            }
            this.Depots = depots.ToArray();
        }
        #endregion // Constructor(s)
    }

} // Workbench.AddIn.Perforce.ViewModel
