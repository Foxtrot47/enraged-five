﻿using System;
using System.ComponentModel.Composition;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Windows.Controls.WpfViewport2D;

namespace Workbench.AddIn.Bugstar
{
    /// <summary>
    /// The Miscellaneous overlay group. This is located in the map viewport project as it should be seen as a standard overlay
    /// that always shows.
    /// </summary>
    [ExportExtension(Viewport.AddIn.ExtensionPoints.OverlayGroup, typeof(Viewport.AddIn.IViewportOverlayGroup))]
    public class BugstarGroup : Viewport.AddIn.ViewportOverlayGroup, IPartImportsSatisfiedNotification
    {
        #region Constants
        internal static readonly String GroupName = "Bugstar";
        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// 
        /// </summary>
        [ImportManyExtension(ExtensionPoints.Overlay, typeof(Viewport.AddIn.IViewportOverlay))]
        IEnumerable<Viewport.AddIn.IViewportOverlay> ImportedOverlays
        {
            get;
            set;
        }
        #endregion // MEF Imports

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public BugstarGroup()
            : base(BugstarGroup.GroupName)
        {
        }
        #endregion // Constructor(s)

        #region IPartImportsSatisfiedNotification
        public void OnImportsSatisfied()
        {
            foreach (Viewport.AddIn.IViewportOverlay overlay in this.ImportedOverlays)
            {
                this.Overlays.Add(overlay);
            }
        }
        #endregion // IPartImportsSatisfiedNotification
    } // BugstarGroup
} // Workbench.AddIn.Bugstar
