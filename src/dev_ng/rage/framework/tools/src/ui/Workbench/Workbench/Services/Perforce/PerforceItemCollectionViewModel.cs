﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;

namespace Workbench.Services.Perforce
{
    /// <summary>
    /// 
    /// </summary>
    public class PerforceItemCollectionViewModel : ViewModelBase
    {
        #region Members

        private PerforceItemCollection m_model;
        private System.Collections.ObjectModel.ObservableCollection<PerforceItemViewModel> m_items;

        #endregion // Members

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public PerforceItemCollection Model
        {
            get { return m_model; }
            set
            {
                SetPropertyValue(value, () => this.Model,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_model = (PerforceItemCollection)newValue;
                        }
                    )
                );
            }
        }

        /// <summary>
        /// The observable collection of perforce items that belong to this collection
        /// </summary>
        public System.Collections.ObjectModel.ObservableCollection<PerforceItemViewModel> Items
        {
            get { return m_items; }
            set
            {
                SetPropertyValue(value, () => this.Items,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_items = (System.Collections.ObjectModel.ObservableCollection<PerforceItemViewModel>)newValue;
                        }
                    )
                );
            }
        }

        #endregion // Properties

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public PerforceItemCollectionViewModel(PerforceItemCollection model)
        {
            this.Model = model;
            this.Items = new System.Collections.ObjectModel.ObservableCollection<PerforceItemViewModel>();
            foreach (PerforceItem item in this.Model.Items)
            {
                this.Items.Add(new PerforceItemViewModel(item));
            }
        }

        #endregion // Constructor
    } // PerforceItemCollectionViewModel
} // Workbench.Services.Perforce
