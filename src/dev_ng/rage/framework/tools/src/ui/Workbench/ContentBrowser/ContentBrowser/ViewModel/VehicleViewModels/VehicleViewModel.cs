﻿using System;
using RSG.Base.Tasks;
using RSG.Model.Common;
using Workbench.AddIn.Services;

namespace ContentBrowser.ViewModel.VehicleViewModels
{

    /// <summary>
    /// 
    /// </summary>
    internal class VehicleViewModel : AssetContainerViewModelBase
    {
        #region Member Data
        private object m_syncObject = new object();
        private bool m_created = false;

        /// <summary>
        /// 
        /// </summary>
        private ITaskProgressService TaskProgressService { get; set; }
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public VehicleViewModel(IVehicle vehicle, ITaskProgressService taskProgressService)
            : base(vehicle)
        {
            TaskProgressService = taskProgressService;
        }
        #endregion // Constructor(s)

        #region AssetContainerViewModelBase Overrides
        /// <summary>
        /// This gets called whenever the expansion state for this view model
        /// has changed
        /// </summary>
        protected override void OnExpansionChanged(Boolean oldValue, Boolean newValue)
        {
            lock (m_syncObject)
            {
                if (!m_created)
                {
                    m_created = true;
                    ITask actionTask = new ActionTask("Vehicle Load", (context, progress) => (this.Model as IVehicle).LoadAllStats());
                    TaskProgressService.Add(actionTask, new TaskContext(), TaskPriority.Background);
                }
            }
            base.OnExpansionChanged(oldValue, newValue);
        }

        /// <summary>
        /// Get called when a level is selected. When this happens need to create the file asset
        /// view models for the grid view to display. Since this needs to happen dynamically when it
        /// gets deselected need to destroy them completely.
        /// </summary>
        protected override void OnSelectionChanged(Boolean oldValue, Boolean newValue)
        {
            lock (m_syncObject)
            {
                if (!m_created)
                {
                    m_created = true;
                    ITask actionTask = new ActionTask("Vehicle Load", (context, progress) => (this.Model as IVehicle).LoadAllStats());
                    TaskProgressService.Add(actionTask, new TaskContext(), TaskPriority.Background);
                }
            }
            base.OnSelectionChanged(oldValue, newValue);
        }
        #endregion // AssetContainerViewModelBase Overrides
    } // VehicleViewModel

} // ContentBrowser.ViewModel.VehicleViewModels namespace
