﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Workbench.AddIn.Commands
{
    /// <summary>
    /// A combobox that is displayed in a menu or toolbar
    /// </summary>
    public class WorkbenchCommandCombobox : WorkbenchMenuItemBase
    {
        #region Properties
        /// <summary>
        /// Width of the combobox as displayed in the menu
        /// </summary>
        public int MinWidth
        {
            get
            {
                return m_minWidth;
            }
            set
            {
                if (m_minWidth == value)
                    return;

                SetPropertyValue(value, () => this.MinWidth,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_minWidth = (int)newValue;
                        }
                    )
                );
            }
        }
        private int m_minWidth;

        /// <summary>
        /// Flag indicating whether the combobox is enabled or not
        /// </summary>
        public bool IsEnabled
        {
            get
            {
                return m_isEnabled;
            }
            set
            {
                SetPropertyValue(value, () => this.IsEnabled,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_isEnabled = (bool)newValue;
                        }
                    )
                );
            }
        }
        private bool m_isEnabled;

        /// <summary>
        /// The currently selected item
        /// </summary>
        public IWorkbenchCommand SelectedItem
        {
            get
            {
                return m_selectedItem;
            }
            set
            {   
                if (PreSelectedItemChanged(m_selectedItem, value))
                {
                    SetPropertyValue(value, () => this.SelectedItem,
                        new RSG.Base.Editor.Command.PropertySetDelegate(
                            delegate(Object newValue)
                            {
                                IWorkbenchCommand oldValue = m_selectedItem;
                                m_selectedItem = (IWorkbenchCommand)newValue;
                                PostSelectedItemChanged(oldValue, value);
                            }
                        )
                    );
                }
                else
                {
                    throw new ArgumentException("Value is not allowed.");
                }
            }
        }
        private IWorkbenchCommand m_selectedItem;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public WorkbenchCommandCombobox()
            : base()
        {
            this.IsCombobox = true;
            this.IsEnabled = true;
            this.MinWidth = 100;
        }
        #endregion // Constructor(s)

        #region Protected Functions
        /// <summary>
        /// Called prior to the selected item changing
        /// </summary>
        /// <param name="oldValue"></param>
        /// <param name="newValue"></param>
        /// <returns>True if the change is allowed, false otherwise</returns>
        protected virtual bool PreSelectedItemChanged(IWorkbenchCommand oldValue, IWorkbenchCommand newValue)
        {
            return true;
        }

        /// <summary>
        /// Called after the selected item changed
        /// </summary>
        /// <param name="oldValue"></param>
        /// <param name="newValue"></param>
        protected virtual void PostSelectedItemChanged(IWorkbenchCommand oldValue, IWorkbenchCommand newValue)
        {
        }
        #endregion

        #region ICommand Implementation
        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
        }
        #endregion // ICommand Implementation
    } // WorkbenchCommandCombobox
} // Workbench.AddIn.Commands
