﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Workbench.AddIn.Bugstar
{

    /// <summary>
    /// 
    /// </summary>
    public static class ExtensionPoints
    {
        /// <summary>
        /// A report item that appears in the bugstar category
        /// </summary>
        public const String Report =
            "B7B9080C-B6CC-405D-A3BF-4CB1032DF461";

        /// <summary>
        /// A report item that appears in the bugstar report category
        /// </summary>
        public const String BugstarReport =
            "B7B9080C-B6CC-405D-A3BF-4CB1032DF461";

        /// <summary>
        /// A report item that appears in the bugstar graph category
        /// </summary>
        public const String BugstarGraph =
            "1E3DC9A6-5BF4-4A61-B238-862E731A540D";

        /// <summary>
        /// 
        /// </summary>
        public const String Overlay =
            "3390B8F6-3B89-4422-B07D-2ECE9B3E8C08";
    }

} // Workbench.AddIn.Bugstar namespace
