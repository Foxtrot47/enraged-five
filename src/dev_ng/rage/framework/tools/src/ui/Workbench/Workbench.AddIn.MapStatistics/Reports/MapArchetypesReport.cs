﻿using RSG.Model.Report;

namespace Workbench.AddIn.MapStatistics.Reports
{
    /// <summary>
    /// Creates a mapping between the MEF import and the reports.
    /// </summary>
    [ExportExtension(Report.AddIn.ExtensionPoints.MapStatsReport, typeof(IReport))]
    public class MapArchetypesReport : RSG.Model.Report.Reports.Map.MapArchetypeReport
    {
    }
}
