﻿using System;
using System.Collections.Generic;
using RSG.Model.Report;
using System.ComponentModel.Composition;
using RSG.Base.Logging;
using Report.AddIn;


namespace Workbench.AddIn.MapStatistics.ReportCategories
{
    /// <summary>
    /// Concrete class for a report category - MEF constructed
    /// </summary>
    [ExportExtension(Report.AddIn.ExtensionPoints.ReportCategory, typeof(IReportCategory))]
    class MapStatsReportCategory
        : Report.AddIn.ReportCategory, IReportCategory, IPartImportsSatisfiedNotification
    {
        #region Constants

        private static readonly String NAME = "Map Statistics";
        private static readonly String DESC = "Some funky map statistic reports";

        #endregion // Constants

        #region MEF Imports
        
        /// <summary>
        /// 
        /// </summary>
        [ImportManyExtension(Report.AddIn.ExtensionPoints.MapStatsReport, typeof(IReport))]
        public override IEnumerable<IReportItem> Reports { get; protected set; }
        
        #endregion // MEF Imports

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public MapStatsReportCategory()
            : base(NAME, DESC)
        {
        }
        #endregion // Constructor(s)
    } // MapStatsReportCategory
} // Workbench.AddIn.MapStatistics.ReportCategories

