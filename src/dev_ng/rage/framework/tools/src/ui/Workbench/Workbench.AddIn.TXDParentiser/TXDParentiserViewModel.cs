﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Media.Imaging;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using RSG.Model.GlobalTXD;
using RSG.Model.Common.Extensions;
using Workbench.AddIn.Services;
using Workbench.AddIn.TXDParentiser.UI.Controls;
using Workbench.AddIn.TXDParentiser.ViewModel;
using PerforceBrowser.AddIn;
using Workbench.AddIn.Services.File;
using Workbench.AddIn.TXDParentiser.UI.Views;
using RSG.Model.Common;
using WidgetEditor.AddIn;
using ragCore;
using RSG.Model.Common.Map;
using RSG.Model.Common.Util;
using RSG.Base.Tasks;
using RSG.Base.Windows.Dialogs;
using Workbench.AddIn.Services.Model;
using P4API;
using RSG.SourceControl.Perforce;
using RSG.Base.Logging;
using RSG.Pipeline.Services.Platform;
using RSG.Base.Configuration;
using RSG.Platform;

namespace Workbench.AddIn.TXDParentiser
{
    /// <summary>
    /// 
    /// </summary>
    public class TXDParentiserViewModel : ViewModelBase, IWeakEventListener
    {
        #region Fields

        ReadOnlyCollection<RadioGroupViewModel> m_optionGroups;
        RadioGroupViewModel m_textureSortGroup;
        ReadOnlyCollection<CommandViewModel> m_commands;
        CommandViewModel m_addCommand;
        CommandViewModel m_deleteCommand;
        CommandViewModel m_renameCommand;
        CommandViewModel m_exportCommand;
        ILevelBrowser m_levelBrowser;
        IProxyService proxyService;
        #endregion // Fields

        #region Properties and Associated Member Data

        /// <summary>
        /// Child class for shuttling arguments to the parentiser background worker.
        /// </summary>
        class ParentiserWorkerArguments
        {
            public String ItypFilename;
            public String ZipFilename;
            public String SourceTxdPrefix;

            public IProgressService ProgressService;

            public ParentiserWorkerArguments(
                String itypFilename,
                String zipFilename,
                String sourceTxdPrefix,
                IProgressService progressService)
            {
                ItypFilename = itypFilename;
                ZipFilename = zipFilename;
                SourceTxdPrefix = sourceTxdPrefix;
                ProgressService = progressService;
            }
        }

        /// <summary>
        /// The global texture dictionaries root object
        /// </summary>
        public GlobalRootViewModel GlobalRoot
        {
            get
            {
                return m_globalRoot;
            }
            set
            {
                this.SetPropertyValue(ref this.m_globalRoot, value, "GlobalRoot");
            }
        }
        private GlobalRootViewModel m_globalRoot;

        /// <summary>
        /// A observable collection of the currently opened texture dictionaries in the form of their
        /// tab items from the global map data tree view, the data context from the tab control is the actual texture dictionary view model object
        /// </summary>
        public RSG.Base.Collections.ObservableCollection<MyTabItem> OpenedTextureDictionaries
        {
            get { return m_openedTextureDictionaries; }
            set
            {
                this.SetPropertyValue(
                    ref this.m_openedTextureDictionaries, value, "OpenedTextureDictionaries");
            }
        }
        private RSG.Base.Collections.ObservableCollection<MyTabItem> m_openedTextureDictionaries;

        /// <summary>
        /// A reference to the selected item in the global texture dictionaries tree view
        /// </summary>
        public RSG.Base.Collections.ObservableCollection<Object> SelectedItems
        {
            get { return m_selectedItems; }
            set
            {
                this.SetPropertyValue(ref this.m_selectedItems, value, "SelectedItems");
            }
        }
        private RSG.Base.Collections.ObservableCollection<Object> m_selectedItems;

        /// <summary>
        /// A reference to the selected item in the global texture dictionaries tree view
        /// </summary>
        public Object SelectedItem
        {
            get { return m_selectedItem; }
            set
            {
                this.SetPropertyValue(ref this.m_selectedItem, value, "SelectedItem");
            }
        }
        private Object m_selectedItem;

        /// <summary>
        /// The sort pattern that the textures are currently sorted in
        /// </summary>
        public TextureSortBase TextureSortPattern
        {
            get
            {
                return m_textureSortPattern;
            }
            set
            {
                this.SetPropertyValue(
                    ref this.m_textureSortPattern, value, "TextureSortPattern");
            }
        }
        private TextureSortBase m_textureSortPattern;

        /// <summary>
        /// The configuration service that has in it the game view
        /// </summary>
        private IConfigurationService ConfigService
        {
            get;
            set;
        }

        /// <summary>
        /// The perforce service
        /// </summary>
        private IPerforceService PerforceService
        {
            get;
            set;
        }

        /// <summary>
        /// The perforce service
        /// </summary>
        private IPerforceSyncService PerforceSyncService
        {
            get;
            set;
        }

        /// <summary>
        /// The Progress Service
        /// </summary>
        private IProgressService ProgressService
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public RSG.Base.Windows.Controls.MultiSelect.TreeView MultiSelectTreeView
        {
            get;
            set;
        }

        private IWorkbenchSaveCurrentService SaveService
        {
            get;
            set;
        }

        #region Options

        public ReadOnlyCollection<RadioGroupViewModel> OptionGroups
        {
            get
            {
                if (m_optionGroups == null)
                {
                    List<RadioGroupViewModel> groups = this.CreateOptionGroups();
                    m_optionGroups = new ReadOnlyCollection<RadioGroupViewModel>(groups);
                }
                return m_optionGroups;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        List<RadioGroupViewModel> CreateOptionGroups()
        {
            return new List<RadioGroupViewModel>()
            {
                TextureSortGroup,
            };
        }

        #region Texture Sort Group

        /// <summary>
        /// 
        /// </summary>
        private RadioGroupViewModel TextureSortGroup
        {
            get
            {
                if (m_textureSortGroup == null)
                {
                    m_textureSortGroup = new RadioGroupViewModel(
                        "Sort Texture By",
                        "Define how the textures are sorted in a dictionary",
                        new List<RadioOptionViewModel>{});
                }
                return m_textureSortGroup;
            }
        }

        #endregion // Texture Sort Group

        #endregion // Options

        #region Commands

        /// <summary>
        /// Returns a read-only list of commands 
        /// that the UI can display and execute.
        /// </summary>
        public ReadOnlyCollection<CommandViewModel> Commands
        {
            get
            {
                if (m_commands == null)
                {
                    List<CommandViewModel> cmds = this.CreateCommands();
                    m_commands = new ReadOnlyCollection<CommandViewModel>(cmds);
                }
                return m_commands;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        List<CommandViewModel> CreateCommands()
        {
            return new List<CommandViewModel>()
            {
                this.CopyCommand,
                this.ValidateCommand,
                this.ExportCommand,
                this.RenameCommand,
                this.DeleteCommand,
                this.AddCommand,
            };
        }

        #region Add Command

        /// <summary>
        /// 
        /// </summary>
        private CommandViewModel AddCommand
        {
            get
            {
                if (m_addCommand == null)
                {
                    String packUriString = "pack://application:,,,/Workbench.AddIn.TXDParentiser;component/Images/Icons/";
                    m_addCommand = new CommandViewModel(
                        new BitmapImage(new Uri(String.Format("{0}{1}", packUriString, "Add.png"))),
                        "Add New Global Dictionary",
                        new RelayCommand(param => this.AddExecute(param), param => this.AddCanExecute(param)));
                }
                return m_addCommand;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Boolean AddCanExecute(Object parameter)
        {
            if (this.SelectedItems.Count == 1)
            {
                Object selected = this.SelectedItems[0];
                if (selected is IDictionaryContainerViewModel)
                {
                    this.AddCommand.ToolTip = "Add a new Global Texture Dictionary";
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        public void AddExecute(Object parameter)
        {
            if (this.SelectedItems.Count != 1)
                return;

            IDictionaryContainerViewModel selected = this.SelectedItems[0] as IDictionaryContainerViewModel;
            if (selected == null)
                return;

            string path = Path.Combine(this.ConfigService.Config.ToolsConfig, "workbench", "txdParentiser.xml");
            IDictionary<string, object> parameters = new Dictionary<string, object>();
            RSG.Base.Xml.Parameters.Load(path, ref parameters, true);
            int limit = (int)parameters["hierarchydepthlimit"];

            int currentHierarchyDepth = 1;
            IDictionaryContainerViewModel parent = selected.Parent;
            while (parent != null)
            {
                currentHierarchyDepth++;
                parent = parent.Parent;
            }

            if (currentHierarchyDepth >= 7)
            {
                MessageBox.Show("Unable to add parent here as doing so will create a dependency chain greater than the 7 limit.", "Dependency Limit Reached");
                return;
            }

            AddDictionaryDialog dlg = new AddDictionaryDialog();
            dlg.ViewModel = this;
            dlg.Owner = Application.Current.MainWindow;

            bool result = (bool)dlg.ShowDialog();
            if (result == false)
                return;

            selected.AddNewGlobalDictionary(dlg.DictionaryName);
        }

        #endregion // Add Command

        #region Add Global Command

        /// <summary>
        /// 
        /// </summary>
        public Boolean AddGlobalCanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        public void AddGlobalExecute(Object parameter)
        {
            if (this.SelectedItems.Count != 1)
                return;

            IDictionaryContainerViewModel selected = this.GlobalRoot;
            if (selected == null)
                return;

            AddDictionaryDialog dlg = new AddDictionaryDialog();
            dlg.ViewModel = this;
            dlg.Owner = Application.Current.MainWindow;

            bool result = (bool)dlg.ShowDialog();
            if (result == false)
                return;

            if (selected.CanAcceptGlobalDictionaries)
            {
                selected.AddNewGlobalDictionary(dlg.DictionaryName);
            }
        }

        #endregion // Add Command

        #region Add New Parent Command
        /// <summary>
        /// 
        /// </summary>
        public Boolean AddNewParentCanExecute(Object parameter)
        {
            if (this.SelectedItems.Count == 1)
            {
                Object selected = this.SelectedItems[0];
                if (selected is GlobalTextureDictionaryViewModel)
                {
                    return true;
                }
            }

            return false;
        }

        private void GetGreatestChildCount(GlobalTextureDictionaryViewModel root, ref int count)
        {
            bool hasChildren = root.GlobalTextureDictionaries != null && root.GlobalTextureDictionaries.Count > 0;
            if (hasChildren)
            {
                count++;
                int maximumChildCount = 0;
                foreach (GlobalTextureDictionaryViewModel child in root.GlobalTextureDictionaries)
                {
                    int childCount = 0;
                    GetGreatestChildCount(child, ref childCount);
                    if (maximumChildCount < childCount)
                    {
                        maximumChildCount = childCount;
                    }
                }

                count += maximumChildCount;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void AddNewParentExecute(Object parameter)
        {
            if (this.SelectedItems.Count == 1)
            {
                GlobalTextureDictionaryViewModel selected = this.SelectedItems[0] as GlobalTextureDictionaryViewModel;
                if (selected == null)
                {
                    return;
                }

                string path = Path.Combine(this.ConfigService.Config.ToolsConfig, "workbench", "txdParentiser.xml");
                IDictionary<string, object> parameters = new Dictionary<string, object>();
                RSG.Base.Xml.Parameters.Load(path, ref parameters, true);
                int limit = (int)parameters["hierarchydepthlimit"];

                int currentHierarchyDepth = 1;
                IDictionaryContainerViewModel parent = selected.Parent;
                while (parent != null)
                {
                    currentHierarchyDepth++;
                    parent = parent.Parent;
                }

                int maximumChildCount = 0;
                GetGreatestChildCount(selected, ref maximumChildCount);
                currentHierarchyDepth += maximumChildCount;
                if (currentHierarchyDepth >= limit)
                {
                    MessageBox.Show("Unable to add parent here as doing so will create a dependency chain greater than the 7 limit.", "Dependency Limit Reached");
                    return;
                }

                Workbench.AddIn.TXDParentiser.UI.Views.AddDictionaryDialog dlg = new UI.Views.AddDictionaryDialog();
                dlg.ViewModel = this;
                dlg.Owner = Application.Current.MainWindow;

                if ((Boolean)dlg.ShowDialog())
                {
                    selected.AddNewGlobalDictionaryAsParent(dlg.DictionaryName);
                }
            }
        }

        #endregion // Add Command

        #region Add New Sibling Command
        /// <summary>
        /// 
        /// </summary>
        public Boolean AddNewSiblingCanExecute(Object parameter)
        {
            if (this.SelectedItems.Count == 1)
            {
                Object selected = this.SelectedItems[0];
                if (selected is GlobalTextureDictionaryViewModel)
                {
                    if ((selected as GlobalTextureDictionaryViewModel).Parent is GlobalTextureDictionaryViewModel)
                    {
                        return true;
                    }
                    else if ((selected as GlobalTextureDictionaryViewModel).Parent is GlobalRootViewModel)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        public void AddNewSiblingExecute(Object parameter)
        {
            if (this.SelectedItems.Count != 1)
                return;

            Object selected = this.SelectedItems[0];
            if (!(selected is GlobalTextureDictionaryViewModel))
                return;

            Workbench.AddIn.TXDParentiser.UI.Views.AddDictionaryDialog dlg = new UI.Views.AddDictionaryDialog();
            dlg.ViewModel = this;
            dlg.Owner = Application.Current.MainWindow;

            if (!(Boolean)dlg.ShowDialog())
                return;

            if ((selected as GlobalTextureDictionaryViewModel).Parent is GlobalTextureDictionaryViewModel)
            {
                GlobalTextureDictionaryViewModel parent = (selected as GlobalTextureDictionaryViewModel).Parent as GlobalTextureDictionaryViewModel;
                parent.AddNewGlobalDictionary(dlg.DictionaryName);
            }
            else if ((selected as GlobalTextureDictionaryViewModel).Parent is GlobalRootViewModel)
            {
                GlobalRootViewModel parent = (selected as GlobalTextureDictionaryViewModel).Parent as GlobalRootViewModel;
                parent.AddNewGlobalDictionary(dlg.DictionaryName);
            }
        }

        #endregion // Add Command

        #region Delete Command

        /// <summary>
        /// 
        /// </summary>
        private CommandViewModel DeleteCommand
        {
            get
            {
                if (m_deleteCommand == null)
                {
                    String packUriString = "pack://application:,,,/Workbench.AddIn.TXDParentiser;component/Images/Icons/";
                    m_deleteCommand = new CommandViewModel(
                        new BitmapImage(new Uri(String.Format("{0}{1}", packUriString, "Delete.png"))),
                        "Delete Selected Global Dictionary",
                        new RelayCommand(param => this.DeleteExecute(param), param => this.DeleteCanExecute(param)));
                }
                return m_deleteCommand;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Boolean DeleteCanExecute(Object parameter)
        {
            if (this != null)
            {
                if (this.SelectedItems.Count == 0)
                {
                    return false;
                }
                foreach (Object selected in this.SelectedItems)
                {
                    if (!(selected is GlobalTextureDictionaryViewModel) && !(selected is SourceTextureDictionaryViewModel) && !(selected is GlobalTextureViewModel))
                    {
                        return false;
                    }
                }
                this.DeleteCommand.ToolTip = this.SelectedItems.Count == 1 ? "Delete the selected global dictionary (Del)" : "Delete the selected global dictionaries (Del)";
                return true;
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        public void DeleteExecute(Object parameter)
        {
            if (this != null && Application.Current.MainWindow != null)
            {
                MessageBoxResult result = MessageBoxResult.Cancel;
                if (this.SelectedItems.Count > 1)
                {
                    result = MessageBox.Show("The selected items will be deleted permanently.", "TXD Parentiser", MessageBoxButton.OKCancel, MessageBoxImage.Asterisk);
                }
                else
                {
                    String message = String.Empty;
                    if (this.SelectedItems[0] is GlobalTextureDictionaryViewModel)
                    {
                        message = String.Format("'{0}' will be deleted permanently.", (this.SelectedItems[0] as GlobalTextureDictionaryViewModel).Model.Name);
                    }
                    else if (this.SelectedItems[0] is SourceTextureDictionaryViewModel)
                    {
                        message = String.Format("'{0}' will be deleted permanently.", (this.SelectedItems[0] as SourceTextureDictionaryViewModel).Model.Name);
                    }
                    else if (this.SelectedItems[0] is GlobalTextureViewModel)
                    {
                        message = String.Format("'{0}' will be fully unpromoted.", (this.SelectedItems[0] as GlobalTextureViewModel).Model.StreamName);
                    }
                    result = MessageBox.Show(message, "TXD Parentiser", MessageBoxButton.OKCancel, MessageBoxImage.Asterisk);
                }

                if (result == MessageBoxResult.OK)
                {
                    List<Object> selectedItems = new List<Object>(this.SelectedItems);
                    foreach (Object selected in selectedItems)
                    {
                        if (selected is GlobalTextureDictionaryViewModel)
                        {
                            GlobalTextureDictionaryViewModel dictionary = selected as GlobalTextureDictionaryViewModel;
                            dictionary.Parent.RemoveGlobalTextureDictionary(dictionary);
                        }
                        else if (selected is SourceTextureDictionaryViewModel)
                        {
                            SourceTextureDictionaryViewModel dictionary = selected as SourceTextureDictionaryViewModel;
                            dictionary.Parent.RemoveSourceTextureDictionary(dictionary);
                        }
                        else if (selected is GlobalTextureViewModel)
                        {
                            GlobalTextureViewModel texture = selected as GlobalTextureViewModel;
                            (texture.Parent as GlobalTextureDictionaryViewModel).FullyUnpromoteTexture(texture);
                        }
                    }
                }
            }
        }

        #endregion // Delete Command

        #region Delete Just Parent Command

        /// <summary>
        /// 
        /// </summary>
        public Boolean DeleteJustParentCanExecute(Object parameter)
        {
            if (this != null)
            {
                if (this.SelectedItems.Count != 1)
                    return false;

                Object selected = this.SelectedItems[0];
                if (!(selected is GlobalTextureDictionaryViewModel))
                    return false;

                GlobalTextureDictionaryViewModel gd = selected as GlobalTextureDictionaryViewModel;

                if (gd.GlobalTextureDictionaries != null && gd.GlobalTextureDictionaries.Count > 0)
                    return true;
                if (gd.SourceTextureDictionaries != null && gd.SourceTextureDictionaries.Count > 0)
                {
                    // If it has source texture dictionaries then the selected dictionary
                    // cannot have any siblings or this will create a invalid hierarchy.
                    if (gd.Parent != null && gd.Parent.GlobalTextureDictionaries != null)
                    {
                        foreach (var sibling in gd.Parent.GlobalTextureDictionaries)
                        {
                            if (sibling == gd)
                                continue;

                            return false;
                        }
                    }
                    return true;
                }

                return true;
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        public void DeleteJustParentExecute(Object parameter)
        {
            if (Application.Current.MainWindow == null)
                return;

            MessageBoxResult result = MessageBoxResult.Cancel;
            String message = String.Empty;
            if (this.SelectedItems[0] is GlobalTextureDictionaryViewModel)
            {
                message = String.Format("'{0}' will be deleted permanently but not the children.", (this.SelectedItems[0] as GlobalTextureDictionaryViewModel).Model.Name);
                result = MessageBox.Show(message, "TXD Parentiser", MessageBoxButton.OKCancel, MessageBoxImage.Asterisk);
            }

            if (result == MessageBoxResult.OK)
            {
                GlobalTextureDictionaryViewModel gd = this.SelectedItems[0] as GlobalTextureDictionaryViewModel;
                gd.RemoveButKeepChildren();
            }
        }

        #endregion // Delete Command

        #region Rename Command

        /// <summary>
        /// 
        /// </summary>
        private CommandViewModel RenameCommand
        {
            get
            {
                if (m_renameCommand == null)
                {
                    String packUriString = "pack://application:,,,/Workbench.AddIn.TXDParentiser;component/Images/Icons/";
                    m_renameCommand = new CommandViewModel(
                        new BitmapImage(new Uri(String.Format("{0}{1}", packUriString, "Pencil.png"))),
                        "Rename Selected Global Dictionary",
                        new RelayCommand(param => this.RenameExecute(param), param => this.RenameCanExecute(param)));
                }
                return m_renameCommand;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Boolean RenameCanExecute(Object parameter)
        {
            if (this.SelectedItems.Count == 1)
            {
                Object selected = this.SelectedItems[0];
                if (selected is GlobalTextureDictionaryViewModel)
                {
                    this.RenameCommand.ToolTip = "Rename the selected global texture dictionary (Ctrl+R)";
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        public void RenameExecute(Object parameter)
        {
            Workbench.AddIn.TXDParentiser.UI.Views.RenameDictionaryDialog dlg = new Workbench.AddIn.TXDParentiser.UI.Views.RenameDictionaryDialog();
            dlg.ViewModel = this;
            string oldName = (this.SelectedItems[0] as GlobalTextureDictionaryViewModel).Model.Name;;
            dlg.DictionaryName = (this.SelectedItems[0] as GlobalTextureDictionaryViewModel).Model.Name;
            dlg.HighlightWholeTextBox();
            dlg.Owner = Application.Current.MainWindow;
            if ((Boolean)dlg.ShowDialog())
            {
                GlobalTextureDictionaryViewModel selection = this.SelectedItems[0] as GlobalTextureDictionaryViewModel;
                selection.Model.Name = dlg.DictionaryName;
                selection.IsFocused = true;

                if (selection.Parent is GlobalRootViewModel)
                {
                    (selection.Parent as GlobalRootViewModel).Model.GlobalTextureDictionaries.Remove(oldName);
                    (selection.Parent as GlobalRootViewModel).Model.GlobalTextureDictionaries.Add(dlg.DictionaryName, selection.Model);
                }
                else if (selection.Parent is GlobalTextureDictionaryViewModel)
                {
                    (selection.Parent as GlobalTextureDictionaryViewModel).Model.GlobalTextureDictionaries.Remove(oldName);
                    (selection.Parent as GlobalTextureDictionaryViewModel).Model.GlobalTextureDictionaries.Add(dlg.DictionaryName, selection.Model);
                }

            }
        }

        #endregion // Rename Command

        #region Export Command

        /// <summary>
        /// 
        /// </summary>
        private CommandViewModel ExportCommand
        {
            get
            {
                if (m_exportCommand == null)
                {
                    String packUriString = "pack://application:,,,/Workbench.AddIn.TXDParentiser;component/Images/Icons/";
                    m_exportCommand = new CommandViewModel(
                        new BitmapImage(new Uri(String.Format("{0}{1}", packUriString, "Export.png"))),
                        "Export Global Dictionaries",
                        new RelayCommand(param => this.ExportExecute(param), param => this.ExportCanExecute(param)));
                }
                return m_exportCommand;
            }   
        }

        /// <summary>
        /// 
        /// </summary>
        public Boolean ExportCanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        public void ExportExecute(Object parameter)
        {
            // Determine if the game is running and if so warn and stop the export.
            var gameList = GameConnection.AcquireGameList(LogFactory.ApplicationLog);
            if (gameList.Count > 0)
            {
                MessageBox.Show(

                    "Unable to export the txd parentiser while the game is running. Shutdown the game and try again.",
                    "Fail",
                    MessageBoxButton.OK,
                    MessageBoxImage.Exclamation);

                return;
            }

            // Make sure the hierarchy is intact.
            foreach (GlobalTextureDictionaryViewModel dictionary in this.GlobalRoot.GetAllGlobalDictionaries())
            {
                if (dictionary.SourceTextureDictionaries.Count > 0)
                {
                    if (dictionary.GlobalTextureDictionaries.Count > 0)
                    {
                        string hierarchy = dictionary.Name;
                        IDictionaryContainerViewModel container = dictionary.Parent;
                        while (container != null)
                        {
                            hierarchy = container.Name + "->" + hierarchy;
                            container = container.Parent;
                        }

                        MessageBox.Show("Unable to export as there exists a global texture dictionary with the following path that contains both global and source texture dictionaries.\n\n" + hierarchy, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                }
            }

            // Make sure all of the promoted textures have been stripped from child/sibling
            // dictionaries.
            foreach (GlobalTextureDictionaryViewModel dictionary in this.GlobalRoot.GetAllGlobalDictionaries())
            {
                foreach (ITextureChildViewModel texture in dictionary.Textures)
                {
                    GlobalRootViewModel.CheckSingleGlobalTexture(dictionary, texture.StreamName, ValidationDirection.Down);
                    RSG.Model.GlobalTXD.GlobalRoot.CheckSingleGlobalTexture(dictionary.Model, texture.StreamName, ValidationDirection.Down);
                }
            }

            // Try and save the txd parentiser before exporting. If something goes wrong
            // stop the export.
            if (!SaveService.SaveCurrentContent())
            {
                MessageBox.Show("Unable to export as the file you are trying to export cannot be saved.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (m_levelBrowser.SelectedLevel == null)
            {
                MessageBox.Show("There needs to be a selected level in the content browser before exporting.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            string levelName = m_levelBrowser.SelectedLevel.Name;
            var cancelResult = MessageBox.Show(string.Format("This exports the gtxd data for the selected level in the content browser which currently is: {0}\nDo you wish to continue?", levelName), "Check", MessageBoxButton.OKCancel, MessageBoxImage.Question);
            if (cancelResult == MessageBoxResult.Cancel)
            {
                return;
            }

            string tempPath = Path.Combine(this.ConfigService.Config.ToolsTemp, "metadata");
            if (Directory.Exists(tempPath))
            {
                Directory.Delete(tempPath, true);
            }

            int missingTcsCount = 0;
            int incorrectTemplateParentCount = 0;
            int missingResourceCount = 0;
            int incorrectTextureExtension = 0;
            String currDir = System.IO.Directory.GetCurrentDirectory();
            try
            {
                // Create the changelist for the export and checkout the two files the rpf and ide.
                RSG.SourceControl.Perforce.P4 p4 = PerforceService.PerforceConnection;
                p4.Connect();
                try
                {
                    string gtxdZipName = Properties.Settings1.Default.GtxdZipName;
                    if (string.IsNullOrWhiteSpace(gtxdZipName))
                    {
                        gtxdZipName = "gtxd";
                    }

                    String zipFilename = System.IO.Path.Combine(this.ConfigService.Config.Project.DefaultBranch.Export, "levels", levelName, "generic", string.Format("{0}.zip", gtxdZipName));
                    string newCurrentDirectory = System.IO.Path.Combine(this.ConfigService.Config.Project.DefaultBranch.Export, "levels", levelName, "generic");
                    if (!Directory.Exists(newCurrentDirectory))
                    {
                        Directory.CreateDirectory(newCurrentDirectory);
                    }

                    System.IO.Directory.SetCurrentDirectory(System.IO.Path.Combine(this.ConfigService.Config.Project.DefaultBranch.Export, "levels", levelName, "generic"));
                    using (PerforceChangelist changelist = new PerforceChangelist("Automatic txd export", p4))
                    {
                        Log.Log__Message("TXD Export Changelist {0}", changelist.Number);
                        p4.Run("edit", "-c", changelist.Number, zipFilename);
                        p4.Run("reopen", "-c", changelist.Number, zipFilename);
                        p4.Run("add", "-c", changelist.Number, zipFilename);
                        if (this.IsCheckedOut(zipFilename, p4) == false)
                        {
                            return;
                        }

                        Dictionary<string, GlobalTexture> tcsPaths = new Dictionary<string, GlobalTexture>();
                        IEnumerable<GlobalTexture> textures = from g in RSG.Model.GlobalTXD.GlobalRoot.GetAllGlobalDictionaries(this.GlobalRoot.Model, true)
                                                              from t in g.Textures.Values.OfType<GlobalTexture>()
                                                              select t;

                        String absoluteAssetsDir = this.ConfigService.Config.Project.DefaultBranch.Assets;
                        String absoluteMetadataDir = this.ConfigService.Config.Project.DefaultBranch.Metadata;
                        string reverseSubstitutedAssetsDir = this.ConfigService.Config.Project.DefaultBranch.Environment.ReverseSubst(absoluteAssetsDir);
                        string mapDir = this.ConfigService.GameConfig.MapTexDir;
                        string templateRoot = Path.Combine(reverseSubstitutedAssetsDir, "metadata", "textures", "templates", mapDir);
                        string templatePath = Path.Combine(templateRoot, "Diffuse");
                        foreach (GlobalTexture texture in textures)
                        {
                            string filename = texture.StreamName + ".tcs";
                            string tcs = Path.Combine(absoluteMetadataDir, "textures", mapDir, "gtxd", filename);
                            if (tcsPaths.ContainsKey(tcs))
                            {
                                continue;
                            }

                            tcsPaths.Add(tcs, texture);
                        }

                        this.PerforceSync(tcsPaths.Keys, p4);

                        List<string> added = new List<string>();
                        List<string> needsUpdating = new List<string>();
                        Dictionary<string, string> resourceFilenames = new Dictionary<string, string>();

                        string reverseSubstitutedAssets =
                            this.ConfigService.Config.Project.DefaultBranch.Environment.ReverseSubst(this.ConfigService
                                .Config.Project.DefaultBranch.Assets);
                        string textureDirectory = Path.Combine(reverseSubstitutedAssets, "maps", "Textures");
                        foreach (KeyValuePair<string, GlobalTexture> tcs in tcsPaths)
                        {
                            if (!File.Exists(tcs.Key))
                            {
                                string mapTcs = Path.Combine(absoluteMetadataDir, "textures", mapDir, Path.GetFileName(tcs.Key));
                                if (File.Exists(mapTcs))
                                {
                                    File.Copy(mapTcs, tcs.Key, true);
                                }

                                if (!File.Exists(tcs.Key))
                                {
                                    GlobalTexture texture = tcs.Value;
                                    List<string> sourceTextures = new List<string>();
                                    if (texture.HasAlphaTexture)
                                    {
                                        sourceTextures.Add(texture.Filename + "+" + texture.AlphaFilepath);
                                    }
                                    else
                                    {
                                        sourceTextures.Add(texture.Filename);
                                    }

                                    string tcsDirectory = System.IO.Path.GetDirectoryName(tcs.Key);
                                    if (!System.IO.Directory.Exists(tcsDirectory))
                                    {
                                        System.IO.Directory.CreateDirectory(tcsDirectory);
                                    }

                                    RSG.Pipeline.Services.Platform.Texture.SpecificationFactory.Create(
                                        tcs.Key,
                                        templatePath,
                                        sourceTextures,
                                        Path.Combine(textureDirectory, texture.StreamName + ".tif"),
                                        false);
                                }

                                added.Add(tcs.Key);
                            }

                            // Validate
                            if (!File.Exists(tcs.Key))
                            {
                                missingTcsCount++;
                                Log.Log__Error("Unable to export the global texture dictionaries as the tcs '{0}' doesn't exist and cannot be created.", tcs.Key);
                                continue;
                            }

                            IBranch branch = this.ConfigService.Config.Project.DefaultBranch;
                            RSG.Pipeline.Services.Platform.Texture.Specification spec =
                                new RSG.Pipeline.Services.Platform.Texture.Specification(
                                branch.Targets.First().Value, tcs.Key);

                            // resource filename needs to be a tif.
                            string resourceFilename = spec.ResourceTextures.First().AbsolutePath;
                            string extension = Path.GetExtension(resourceFilename);
                            if (string.Compare(extension, ".tif", true) != 0 && string.Compare(extension, ".dds", true) != 0)
                            {
                                needsUpdating.Add(tcs.Key);
                                resourceFilename = Path.ChangeExtension(resourceFilename, ".tif");
                            }

                            resourceFilenames.Add(tcs.Key, resourceFilename);
                        }

                        if (added.Count > 0)
                        {
                            added.Insert(0, changelist.Number);
                            added.Insert(0, "-c");
                            p4.Run("add", added.ToArray());
                        }

                        if (needsUpdating.Count > 0)
                        {
                            List<string> checkoutArgs = new List<string>();
                            checkoutArgs.Add("-c");
                            checkoutArgs.Add(changelist.Number);
                            checkoutArgs.AddRange(needsUpdating);
                            p4.Run("edit", checkoutArgs.ToArray());

                            foreach (string filename in needsUpdating)
                            {
                                IBranch branch = this.ConfigService.Config.Project.DefaultBranch;
                                RSG.Pipeline.Services.Platform.Texture.Specification spec =
                                    new RSG.Pipeline.Services.Platform.Texture.Specification(
                                    branch.Targets.First().Value, filename);

                                string resourceFilename = spec.ResourceTextures.First().AbsolutePath;
                                string extension = Path.GetExtension(resourceFilename);
                                if (string.Compare(extension, ".tif", true) != 0 && string.Compare(extension, ".dds", true) != 0)
                                {
                                    resourceFilename = Path.ChangeExtension(resourceFilename, ".tif");
                                }

                                RSG.Pipeline.Services.Platform.Texture.SpecificationFactory.UpdateResourceTextures(
                                    filename, spec.SourceTextures, resourceFilename);
                            }
                        }

                        this.PerforceSync(resourceFilenames.Values, p4);
                        foreach (KeyValuePair<string, string> resourceFilename in resourceFilenames)
                        {
                            // The resource file needs to exist on the local computer.
                            if (!File.Exists(resourceFilename.Value))
                            {
                                missingResourceCount++;
                                Log.Log__Error("Unable to export the global texture dictionaries as the resource texture '{0}' doesn't exist.", resourceFilename.Value);
                            }
                        }
                    }

                    if (missingTcsCount > 0 || incorrectTemplateParentCount > 0 || missingResourceCount > 0 || incorrectTextureExtension > 0)
                    {
                        string errorMsg = "Unable to export the global texture dictionaries";
                        if (missingTcsCount > 0)
                        {
                            errorMsg += string.Format("\r\n{0} Missing tcs files.", missingTcsCount);
                        }
                        if (incorrectTemplateParentCount > 0)
                        {
                            errorMsg += string.Format("\r\n{0} Incorrect template parent references.", incorrectTemplateParentCount);
                        }
                        if (missingResourceCount > 0)
                        {
                            errorMsg += string.Format("\r\n{0} Missing texture resource files.", missingResourceCount);
                        }
                        if (incorrectTextureExtension > 0)
                        {
                            errorMsg += string.Format("\r\n{0} Resource files are still pointing to dds textures.", incorrectTextureExtension);
                        }

                        errorMsg += "\r\nMore detailed information can be found in the log";             
                        MessageBox.Show(errorMsg, "Resource Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }

                    BackgroundWorker exportTXDWorker = new BackgroundWorker();
                    exportTXDWorker.DoWork += new DoWorkEventHandler(this.ExportGlobalRoot);


                    // prepare arguments
                    string metadataFilename = Properties.Settings1.Default.GtxdMetaName;
                    if (string.IsNullOrWhiteSpace(metadataFilename))
                    {
                        metadataFilename = "gtxd";
                    }

                    string sourceTxdPrefix = Properties.Settings1.Default.SourceTxdPrefix;
                    string textureLocation = System.IO.Path.Combine(this.ConfigService.GameConfig.AssetsDir, "maps", "Textures", "*.tif");

                    IBranch defaultBranch = this.ConfigService.Config.Project.DefaultBranch;
                    string itypFilename = System.IO.Path.Combine(defaultBranch.Export, "levels", levelName, "generic", string.Format("{0}.meta", metadataFilename));

                    ParentiserWorkerArguments arguments = new ParentiserWorkerArguments(
                        itypFilename,
                        zipFilename,
                        sourceTxdPrefix,
                        ProgressService);
                    ProgressService.Show(exportTXDWorker, arguments, "Exporting GTXD", "Exporting global texture dictionaries...", true);
                }
                catch (Exception ex)
                {
                    RSG.Base.Logging.Log.Log__Exception(ex, "Unhandled Perforce exception");
                }
                finally
                {
                    p4.Disconnect();
                }
            }
            finally
            {
                System.IO.Directory.SetCurrentDirectory(currDir);
            }
        }

        private void PerforceSync(IEnumerable<string> filenames, P4 p4)
        {
            List<String> syncArgs = new List<String>();
            syncArgs.Add("-n"); // preview sync
            syncArgs.AddRange(filenames);
            P4RecordSet previewResult = p4.Run("sync", false, syncArgs.ToArray());
            IEnumerable<String> oldDepotFilenames = from r in previewResult.Records
                                                    where r.Fields.ContainsKey("depotFile")
                                                    select r.Fields["depotFile"];

            int count = oldDepotFilenames.Count();
            if (count == 0)
            {
                return;
            }
            
            P4RecordSet fileSyncRecords = p4.Run("sync", false, oldDepotFilenames.ToArray());
            if (fileSyncRecords.Errors.Count() == 0)
            {
                return;
            }
            
            System.Windows.MessageBoxResult userResult = MessageBox.Show(
                string.Format(
                "Perforce errors syncing {0} files.  Would you like to force-sync?\n\n{1}, ...",
                fileSyncRecords.Errors.Count(),
                String.Join("\n", fileSyncRecords.Errors.Take(Math.Min(fileSyncRecords.Errors.Count(), 5)))),
                "Perforce errors",
                System.Windows.MessageBoxButton.YesNo,
                System.Windows.MessageBoxImage.Question,
                System.Windows.MessageBoxResult.No);

            if (System.Windows.MessageBoxResult.Yes == userResult)
            {
                List<String> args = new List<String>();
                args.Add("-f");
                args.AddRange(oldDepotFilenames);
                fileSyncRecords = p4.Run("sync", false, args.ToArray());
            }
        }

        private bool IsCheckedOut(string filename, P4 p4)
        {
            if (File.Exists(filename) == true)
            {
                bool checkedout = false;
                FileState fs = new FileState(p4, filename);
                if (fs.OpenAction == FileAction.Edit)
                {
                    checkedout = true;
                }

                if (checkedout == false)
                {
                    // Need to warn that the file can't be checked out and whether the user wants to
                    // toggle the writable flag.
                    string msg = string.Format("Unable to check out {0} for export, this can be potentially dangerous. Would you like to make it writable or cancel?", filename);
                    MessageBoxResult writableResult = MessageBox.Show(msg, "Perforce Error", MessageBoxButton.OKCancel, MessageBoxImage.Warning);

                    if (writableResult == MessageBoxResult.OK)
                    {
                        System.IO.FileInfo fileInfo = new System.IO.FileInfo(filename);
                        fileInfo.IsReadOnly = false;
                        return true;
                    }
                }
                else
                {
                    System.IO.FileInfo fileInfo = new System.IO.FileInfo(filename);
                    fileInfo.IsReadOnly = false;
                    return true;
                }

                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// BackgroundWorker asynchronous delegate for exporting the global texture dictionary.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExportGlobalRoot(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker thisWorker = sender as BackgroundWorker;
            ParentiserWorkerArguments arguments = e.Argument as ParentiserWorkerArguments;

            //Export the root global texture dictionary.
            arguments.ProgressService.Status = "Building global texture dictionary...";

            Thread exportThread = new Thread(ExportGlobalRootThread);
            exportThread.Name = "Global Texture Dictionary Thread";
            exportThread.Start(arguments);

            //Obey the Cancel button in the ProgressSerivce.  
            while (thisWorker.CancellationPending == false && exportThread.IsAlive == true) { }

            //If we've canceled while in progress, kill the thread and any child process.
            if (exportThread.IsAlive == true)
            {
                RSG.Model.GlobalTXD.GlobalRoot.OnCancel();
                exportThread.Abort();
            }

            e.Cancel = thisWorker.CancellationPending;
        }

        /// <summary>
        /// Thread helper for exporting the global texture dictionaries.
        /// </summary>
        /// <param name="parameters"></param>
        private void ExportGlobalRootThread(object parameters)
        {
            ParentiserWorkerArguments arguments = parameters as ParentiserWorkerArguments;

            IProject project = this.ConfigService.Config.Project;
            IBranch branch = this.ConfigService.Config.Project.DefaultBranch;

            RSG.Model.GlobalTXD.GlobalRoot.ExportGlobalRoot(
                this.GlobalRoot.Model,
                arguments.ItypFilename,
                arguments.ZipFilename,
                project.Name,
                branch.Name,
                branch.Metadata,
                this.ConfigService.GameConfig.MapTexDir,
                project.Config.ToolsRoot,
                arguments.SourceTxdPrefix);
        }

        #endregion // Export Command

        #region Select Low Instance Textures

        /// <summary>
        /// 
        /// </summary>
        public Boolean SelectLowCanExecute(Object parameter)
        {
            if (this.SelectedItems.Count == 1)
            {
                Object selected = this.SelectedItems[0];
                if (selected is ITextureContainerViewModel)
                {
                    if (selected is GlobalTextureDictionaryViewModel)
                    {
                        foreach (GlobalTexture texture in (selected as GlobalTextureDictionaryViewModel).Model.Textures.Values)
                        {
                            if (texture.InstanceCount >= 2 && texture.InstanceCount < 5)
                            {
                                return true;
                            }
                        }
                    }
                    else if (selected is SourceTextureDictionaryViewModel)
                    {
                        foreach (SourceTexture texture in (selected as SourceTextureDictionaryViewModel).Model.Textures.Values)
                        {
                            if (texture.Promoted == false)
                            {
                                if (texture.InstanceCount >= 2 && texture.InstanceCount < 5)
                                {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        public void SelectLowExecute(Object parameter)
        {
            Object selected = this.SelectedItems[0];
            if (this.MultiSelectTreeView != null)
                MultiSelectTreeView.ClearSelection();
            
            if (selected is ITextureContainerViewModel)
            {
                if (selected is GlobalTextureDictionaryViewModel)
                {
                    (selected as GlobalTextureDictionaryViewModel).IsExpanded = true;
                    foreach (GlobalTextureViewModel texture in (selected as GlobalTextureDictionaryViewModel).Textures)
                    {
                        if (texture.Model.InstanceCount >= 2 && texture.Model.InstanceCount < 5)
                        {
                            texture.IsSelected = true;
                        }
                    }
                }
                else if (selected is SourceTextureDictionaryViewModel)
                {
                    (selected as SourceTextureDictionaryViewModel).IsExpanded = true;
                    foreach (SourceTextureViewModel texture in (selected as SourceTextureDictionaryViewModel).Children)
                    {
                        if (texture.Model.Promoted == false)
                        {
                            if (texture.Model.InstanceCount >= 2 && texture.Model.InstanceCount < 5)
                            {
                                texture.IsSelected = true;
                            }
                        }
                    }
                }
            }
        }

        #endregion // Select Low Instance Textures

        #region Select Medium Instance Textures

        /// <summary>
        /// 
        /// </summary>
        public Boolean SelectMediumCanExecute(Object parameter)
        {
            if (this.SelectedItems.Count == 1)
            {
                Object selected = this.SelectedItems[0];
                if (selected is ITextureContainerViewModel)
                {
                    if (selected is GlobalTextureDictionaryViewModel)
                    {
                        foreach (ITextureChild texture in (selected as GlobalTextureDictionaryViewModel).Model.Textures.Values)
                        {
                            if (texture.InstanceCount >= 5 && texture.InstanceCount < 10)
                            {
                                return true;
                            }
                        }
                    }
                    else if (selected is SourceTextureDictionaryViewModel)
                    {
                        foreach (SourceTexture texture in (selected as SourceTextureDictionaryViewModel).Model.Textures.Values)
                        {
                            if (texture.Promoted == false)
                            {
                                if (texture.InstanceCount >= 5 && texture.InstanceCount < 10)
                                {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        public void SelectMediumExecute(Object parameter)
        {
            Object selected = this.SelectedItems[0];
            if (this.MultiSelectTreeView != null)
                MultiSelectTreeView.ClearSelection();

            if (selected is ITextureContainerViewModel)
            {
                if (selected is GlobalTextureDictionaryViewModel)
                {
                    (selected as GlobalTextureDictionaryViewModel).IsExpanded = true;
                    foreach (GlobalTextureViewModel texture in (selected as GlobalTextureDictionaryViewModel).Textures)
                    {
                        if (texture.Model.InstanceCount >= 5 && texture.Model.InstanceCount < 10)
                        {
                            texture.IsSelected = true;
                        }
                    }
                }
                else if (selected is SourceTextureDictionaryViewModel)
                {
                    (selected as SourceTextureDictionaryViewModel).IsExpanded = true;
                    foreach (SourceTextureViewModel texture in (selected as SourceTextureDictionaryViewModel).Children)
                    {
                        if (texture.Model.Promoted == false)
                        {
                            if (texture.Model.InstanceCount >= 5 && texture.Model.InstanceCount < 10)
                            {
                                texture.IsSelected = true;
                            }
                        }
                    }
                }
            }
        }

        #endregion // Select Medium Instance Textures

        #region Select High Instance Textures

        /// <summary>
        /// 
        /// </summary>
        public Boolean SelectHighCanExecute(Object parameter)
        {
            if (this.SelectedItems.Count == 1)
            {
                Object selected = this.SelectedItems[0];
                if (selected is ITextureContainerViewModel)
                {
                    if (selected is GlobalTextureDictionaryViewModel)
                    {
                        foreach (ITextureChild texture in (selected as GlobalTextureDictionaryViewModel).Model.Textures.Values)
                        {
                            if (texture.InstanceCount >= 10)
                            {
                                return true;
                            }
                        }
                    }
                    else if (selected is SourceTextureDictionaryViewModel)
                    {
                        foreach (SourceTexture texture in (selected as SourceTextureDictionaryViewModel).Model.Textures.Values)
                        {
                            if (texture.Promoted == false)
                            {
                                if (texture.InstanceCount >= 10)
                                {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        public void SelectHighExecute(Object parameter)
        {
            Object selected = this.SelectedItems[0];
            if (this.MultiSelectTreeView != null)
                MultiSelectTreeView.ClearSelection();

            if (selected is ITextureContainerViewModel)
            {
                if (selected is GlobalTextureDictionaryViewModel)
                {
                    (selected as GlobalTextureDictionaryViewModel).IsExpanded = true;
                    foreach (GlobalTextureViewModel texture in (selected as GlobalTextureDictionaryViewModel).Textures)
                    {
                        if (texture.Model.InstanceCount >= 10)
                        {
                            texture.IsSelected = true;
                        }
                    }
                }
                else if (selected is SourceTextureDictionaryViewModel)
                {
                    (selected as SourceTextureDictionaryViewModel).IsExpanded = true;
                    foreach (SourceTextureViewModel texture in (selected as SourceTextureDictionaryViewModel).Children)
                    {
                        if (texture.Model.Promoted == false)
                        {
                            if (texture.Model.InstanceCount >= 10)
                            {
                                texture.IsSelected = true;
                            }
                        }
                    }
                }
            }
        }

        #endregion // Select High Instance Textures

        #region Set WIP Flag Command
        /// <summary>
        /// 
        /// </summary>
        public Boolean WIPCanExecute(Object parameter)
        {
            if (this.SelectedItems.Count == 1)
            {
                Object selected = this.SelectedItems[0];
                if (selected is GlobalTextureDictionaryViewModel)
                {
                    if ((selected as GlobalTextureDictionaryViewModel).Level == 0)
                    {
                        if (!(selected as GlobalTextureDictionaryViewModel).Model.WorkInProgress)
                        {
                            this.AddCommand.ToolTip = "Set as Work In Progress.";
                            return true;
                        }
                    }                  
                }
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        public void WIPExecute(Object parameter)
        {
            if (this.SelectedItems.Count == 1)
            {
                Object selected = this.SelectedItems[0];
                if (selected is GlobalTextureDictionaryViewModel)
                {
                    if ((selected as GlobalTextureDictionaryViewModel).Level == 0)
                    {
                        if (!(selected as GlobalTextureDictionaryViewModel).Model.WorkInProgress)
                        {
                            (selected as GlobalTextureDictionaryViewModel).Model.WorkInProgress = true;
                        }
                    }
                }
            }
        }
        #endregion // Set WIP Flag Command

        #region Unset WIP Flag Command
        /// <summary>
        /// 
        /// </summary>
        public Boolean UnWIPCanExecute(Object parameter)
        {
            if (this.SelectedItems.Count == 1)
            {
                Object selected = this.SelectedItems[0];
                if (selected is GlobalTextureDictionaryViewModel)
                {
                    if ((selected as GlobalTextureDictionaryViewModel).Level == 0)
                    {
                        if ((selected as GlobalTextureDictionaryViewModel).Model.WorkInProgress)
                        {
                            this.AddCommand.ToolTip = "Unset as Work In Progress.";
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        public void UnWIPExecute(Object parameter)
        {
            if (this.SelectedItems.Count == 1)
            {
                Object selected = this.SelectedItems[0];
                if (selected is GlobalTextureDictionaryViewModel)
                {
                    if ((selected as GlobalTextureDictionaryViewModel).Level == 0)
                    {
                        if ((selected as GlobalTextureDictionaryViewModel).Model.WorkInProgress)
                        {
                            (selected as GlobalTextureDictionaryViewModel).Model.WorkInProgress = false;
                        }
                    }
                }
            }
        }
        #endregion // Unset WIP Flag Command

        #region Promote Command

        /// <summary>
        /// 
        /// </summary>
        public Boolean PromoteCanExecute(Object parameter)
        {
            if (this != null)
            {
                if (this.SelectedItems.Count == 0)
                {
                    return false;
                }
                foreach (Object selected in this.SelectedItems)
                {
                    if ((selected is GlobalTextureViewModel) && (selected as GlobalTextureViewModel).Parent != null)
                    {                        
                        return true;
                    }
                    if ((selected is SourceTextureViewModel) && (selected as SourceTextureViewModel).Parent != null)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        public void PromoteExecute(Object parameter)
        {
            List<Object> selectedItems = new List<Object>(this.SelectedItems);
            foreach (Object selected in selectedItems)
            {
                if (selected is GlobalTextureViewModel)
                {
                    GlobalTextureDictionaryViewModel textureParent = (selected as GlobalTextureViewModel).Parent as GlobalTextureDictionaryViewModel;
                    if (textureParent == null)
                        return;

                    GlobalTextureDictionaryViewModel parent = textureParent.Parent as GlobalTextureDictionaryViewModel;
                    if (parent == null)
                        return;
                    parent.MoveTextureHere(selected as GlobalTextureViewModel);
                }
                else if (selected is SourceTextureViewModel)
                {
                    SourceTextureDictionaryViewModel sourceParent = (selected as SourceTextureViewModel).Parent as SourceTextureDictionaryViewModel;
                    if (sourceParent == null)
                        return;

                    GlobalTextureDictionaryViewModel parent = sourceParent.Parent as GlobalTextureDictionaryViewModel;
                    if (parent == null)
                        return;

                    parent.PromoteTextureHere(selected as SourceTextureViewModel);
                }
            }
        }

        #endregion // Promote Command

        #region Validate Command

        private CommandViewModel m_validateCommand = null;
        public CommandViewModel ValidateCommand
        {
            get
            {
                if (m_validateCommand == null)
                {
                    String packUriString = "pack://application:,,,/Workbench.AddIn.TXDParentiser;component/Images/Icons/";
                    m_validateCommand = new CommandViewModel(
                        new BitmapImage(new Uri(String.Format("{0}{1}", packUriString, "Validate.png"))),
                        "Validate Against Map Data",
                        new RelayCommand(param => this.ValidateExecute(param), param => this.ValidateCanExecute(param)));
                }
                return m_validateCommand;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Boolean ValidateCanExecute(Object parameter)
        {
            if (this.m_levelBrowser.SelectedLevel == null || !this.m_levelBrowser.SelectedLevel.Initialised)
                return false;

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        public void ValidateExecute(Object parameter)
        {
            CancellationTokenSource cts = new CancellationTokenSource();
            LevelTaskContext context = new LevelTaskContext(cts.Token, m_levelBrowser.SelectedLevel);
            ITask loadDataTask = new ActionTask("Load Data", (ctx, progress) => EnsureDataLoaded((LevelTaskContext)ctx, progress));

            TaskExecutionDialog dialog = new TaskExecutionDialog();
            TaskExecutionViewModel vm = new TaskExecutionViewModel("Loading Map Data", loadDataTask, cts, context);
            vm.AutoCloseOnCompletion = true;
            dialog.DataContext = vm;
            Nullable<bool> selectionResult = dialog.ShowDialog();
            if (false == selectionResult)
            {
                RSG.Base.Logging.Log.Log__Error("Failed loading txd map data.");
                return;
            }

            Dictionary<string, Dictionary<string, ITexture>> txds = GetTextureDictionaries(this.m_levelBrowser);
            List<SourceTextureDictionaryViewModel> sourceDictionaries = GlobalRoot.GetAllSourceDictionaries().ToList();

            foreach (SourceTextureDictionaryViewModel sd in sourceDictionaries)
            {
                GlobalTextureDictionaryViewModel parent = sd.Parent as GlobalTextureDictionaryViewModel;
                if (!txds.ContainsKey(sd.Name.ToLower()))
                {
                    parent.SourceTextureDictionaries.Remove(sd);
                    parent.Model.SourceTextureDictionaries.Remove(sd.Name);
                }
                else
                {
                    Dictionary<string, ITexture> txd = txds[sd.Name.ToLower()];
                    List<SourceTextureViewModel> removeList = new List<SourceTextureViewModel>();
                    foreach (SourceTextureViewModel texture in sd.Children)
                    {
                        if (!txd.ContainsKey(texture.StreamName.ToLower()))
                            removeList.Add(texture);
                    }
                    foreach (SourceTextureViewModel texture in removeList)
                    {
                        sd.Children.Remove(texture);
                        sd.Model.Textures.Remove(texture.StreamName.ToLower());
                    }

                    foreach (KeyValuePair<string, ITexture> texture in txd)
                    {
                        if (!sd.Model.Textures.ContainsKey(texture.Key.ToLower()))
                        {
                            SourceTexture newTexture = new SourceTexture(texture.Value, sd.Model);
                            SourceTextureViewModel newViewModel = new SourceTextureViewModel(newTexture, sd);
                            sd.Model.Textures.Add(texture.Key.ToLower(), newTexture);
                            sd.Children.Add(newViewModel);
                        }
                    }
                }
            }

            foreach (GlobalTextureDictionaryViewModel gd in this.GlobalRoot.GlobalTextureDictionaries)
            {
                GlobalRootViewModel.CheckPromotedTexturesAreValid(gd, ValidationDirection.Down);
                RSG.Model.GlobalTXD.GlobalRoot.CheckPromotedTexturesAreValid(gd.Model, ValidationDirection.Down);
                RSG.Model.GlobalTXD.GlobalRoot.CheckTexturesPromotionState(gd.Model);
                RSG.Model.GlobalTXD.GlobalRoot.CheckTextureDictionarySizes(gd.Model, ValidationDirection.Down);
                RSG.Model.GlobalTXD.GlobalRoot.CheckTextureInstanceCounts(gd.Model, ValidationDirection.Down);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="progress"></param>
        private void EnsureDataLoaded(LevelTaskContext context, IProgress<TaskProgress> progress)
        {
            float increment = 1.0f / context.MapHierarchy.AllSections.Count();

            // Load all the map section data
            foreach (IMapSection section in context.MapHierarchy.AllSections)
            {
                context.Token.ThrowIfCancellationRequested();

                // Update the progress information
                string message = String.Format("Loading {0}", section.Name);
                progress.Report(new TaskProgress(increment, true, message));

                section.RequestAllStatistics(false);
            }
        }

        #endregion // Validate Command

        #region Copy Command

        private CommandViewModel m_copyCommand = null;
        public CommandViewModel CopyCommand
        {
            get
            {
                if (m_copyCommand == null)
                {
                    m_copyCommand = new CommandViewModel(
                        "Copy Texture Names",
                        "Copy selected texture names to clipboard as list",
                        new RelayCommand(param => this.CopyExecute(param), param => this.CopyCanExecute(param)));
                }
                return m_copyCommand;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Boolean CopyCanExecute(Object parameter)
        {
            if (this.SelectedItems == null || this.SelectedItems.Count == 0)
                return false;

            foreach (var obj in this.SelectedItems)
            {
                if (!(obj is ITextureChildViewModel))
                    return false;
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        public void CopyExecute(Object parameter)
        {
            string textures = string.Empty;
            int index = 0;
            foreach (ITextureChildViewModel texture in this.SelectedItems)
            {
                if (index == 0)
                {
                    textures += texture.StreamName;
                }
                else
                {
                    textures += Environment.NewLine + texture.StreamName;
                }
                index++;
            }
            Clipboard.SetText(textures);
        }

        #endregion // Validate Command

        #endregion // Commands

        #endregion // Properties and Associated Member Data

        #region Left Pane Properties
        private int m_numberOfSharedTextures;
        public int NumberOfSharedTextures
        {
            get
            {
                return m_numberOfSharedTextures;
            }
            set
            {
                this.SetPropertyValue(
                    ref this.m_numberOfSharedTextures, value, "NumberOfSharedTextures");
            }
        }

        private int m_numberOfUniqueTextures;
        public int NumberOfUniqueTextures
        {
            get
            {
                return m_numberOfUniqueTextures;
            }
            set
            {
                this.SetPropertyValue(
                    ref this.m_numberOfUniqueTextures, value, "NumberOfUniqueTextures");
            }
        }

        private int m_totalNumberOfTextures;
        public int TotalNumberOfTextures
        {
            get
            {
                return m_totalNumberOfTextures;
            }
            set
            {
                this.SetPropertyValue(
                    ref this.m_totalNumberOfTextures, value, "TotalNumberOfTextures");
            }
        }

        private bool m_showPromotedTextures;
        public bool ShowPromotedTextures
        {
            get
            {
                return m_showPromotedTextures;
            }
            set
            {
                if (this.SetPropertyValue(
                    ref this.m_showPromotedTextures, value, "ShowPromotedTextures"))
                {
                    HandleSelectionChanged();
                }
            }
        }

        private string m_slectedItemType;
        public string SelectedItemType
        {
            get
            {
                return m_slectedItemType;
            }
            set
            {
                this.SetPropertyValue(
                    ref this.m_slectedItemType, value, "SelectedItemType");
            }
        }

        public ObservableCollection<ITextureChildViewModel> SharedTextures
        {
            get;
            set;
        }
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public TXDParentiserViewModel(IConfigurationService config, IPerforceService perforceService, IPerforceSyncService perforceSyncService, IProgressService progressService, ILevelBrowser levelBrowser, IWorkbenchSaveCurrentService saveService, IProxyService service, Boolean link)
        {
            this.proxyService = proxyService;
            this.m_levelBrowser = levelBrowser;
            this.ConfigService = config;
            this.PerforceService = perforceService;
            this.PerforceSyncService = perforceSyncService;
            this.ProgressService = progressService;
            this.SaveService = saveService;
            this.SelectedItems = new RSG.Base.Collections.ObservableCollection<Object>();
            this.SharedTextures = new ObservableCollection<ITextureChildViewModel>();
            this.SelectedItemType = "None";
            this.OpenedTextureDictionaries = new RSG.Base.Collections.ObservableCollection<MyTabItem>();

            Dictionary<string, List<ITexture>> txds = null;
            var global = new GlobalRoot(txds, Path.Combine(config.GameConfig.AssetsDir, "maps", "Textures"));

            this.GlobalRoot = new GlobalRootViewModel(global);
            System.Collections.Specialized.CollectionChangedEventManager.AddListener(this.SelectedItems, this);
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public TXDParentiserViewModel(Uri openedFilename, IConfigurationService config, IPerforceService perforceService, IPerforceSyncService perforceSyncService, IProgressService progressService, ILevelBrowser levelBrowser, IWorkbenchSaveCurrentService saveService, Boolean link)
        {
            this.m_levelBrowser = levelBrowser;
            this.ConfigService = config;
            this.PerforceService = perforceService;
            this.PerforceSyncService = perforceSyncService;
            this.ProgressService = progressService;
            this.SaveService = saveService;
            this.SelectedItems = new RSG.Base.Collections.ObservableCollection<Object>();
            this.SharedTextures = new ObservableCollection<ITextureChildViewModel>();
            this.SelectedItemType = "None";
            this.OpenedTextureDictionaries = new RSG.Base.Collections.ObservableCollection<MyTabItem>();

            Dictionary<string, List<ITexture>> txds = null;
            var global = new GlobalRoot(openedFilename.AbsolutePath, txds, Path.Combine(config.GameConfig.AssetsDir, "maps", "Textures"));

            this.GlobalRoot = new GlobalRootViewModel(global);
            System.Collections.Specialized.CollectionChangedEventManager.AddListener(this.SelectedItems, this);
        }
        #endregion // Constructor(s)

        #region Public Function(s)

        /// <summary>
        /// Opens the given texture dictionary or selects it if it has already been opened
        /// </summary>
        public void OpenTextureDictionary(TextureDictionaryViewModel textureDictionary)
        {
            MyTabItem item = IsTextureDictionaryOpened(textureDictionary);
            if (item != null)
            {
                item.IsSelected = true;
            }
            else
            {
                // Create a TabItem for the texture dictionary and add it to the opened collection
                MyTabItem newTabItem = new MyTabItem(CloseTextureDictionary);
                newTabItem.Header = textureDictionary.Model.Name;

                newTabItem.DataContext = textureDictionary;

                newTabItem.Content = textureDictionary;
                this.OpenedTextureDictionaries.Add(newTabItem);
                newTabItem.IsSelected = true;
            }
        }

        /// <summary>
        /// Closes the texture dictionary that sent the event
        /// </summary>
        private void CloseTextureDictionary(Object sender, RoutedEventArgs e)
        {
            TextureDictionaryViewModel textureDictionary = sender as TextureDictionaryViewModel;
            if (textureDictionary != null)
            {
                MyTabItem item = IsTextureDictionaryOpened(textureDictionary);
                if (item != null)
                {
                    this.OpenedTextureDictionaries.Remove(item);
                }
            }
        }

        #endregion // Public Function(s)

        #region Private Function(s)

        /// <summary>
        /// Determines if the given texture dictionary is already opened and if so returns
        /// true else returns false
        /// </summary>
        private MyTabItem IsTextureDictionaryOpened(TextureDictionaryViewModel textureDictionary)
        {
            foreach (MyTabItem item in this.OpenedTextureDictionaries)
            {
                if (item.DataContext is TextureDictionaryViewModel)
                {
                    if ((item.DataContext as TextureDictionaryViewModel) == textureDictionary)
                    {
                        return item;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="levelBrowser"></param>
        /// <returns></returns>
        private Dictionary<string, Dictionary<string, ITexture>> GetTextureDictionaries(ILevelBrowser levelBrowser)
        {
            Dictionary<string, Dictionary<string, ITexture>> txds = new Dictionary<string, Dictionary<string, ITexture>>();

            IMapHierarchy mapHierarchy = levelBrowser.SelectedLevel.GetMapHierarchy();
            foreach (IMapSection section in mapHierarchy.AllSections)
            {
                foreach (IMapArchetype archetype in section.Archetypes)
                {
                    if (archetype.TxdExportSizes == null || archetype.TxdExportSizes.Count == 0)
                    {
                        continue;
                    }

                    KeyValuePair<string, uint> txd = archetype.TxdExportSizes.FirstOrDefault();
                    string txdName = txd.Key;
                    if (!txds.ContainsKey(txdName))
                    {
                        txds.Add(txdName, new Dictionary<string, ITexture>());
                    }

                    if (archetype.Textures == null)
                    {
                        continue;
                    }

                    foreach (ITexture texture in archetype.Textures)
                    {
                        if (!txds[txdName].ContainsKey(texture.Name.ToLower()))
                        {
                            txds[txdName].Add(texture.Name.ToLower(), texture);
                        }
                    }
                }
            }

            return txds;
        }

        #endregion // Private Function(s)

        public Boolean ReceiveWeakEvent(Type managerType, Object sender, EventArgs e)
        {
            if (sender == this.SelectedItems)
            {
                this.SelectedItem = this.SelectedItems.Count > 0 ? this.SelectedItems[0] : null;
                this.OnPropertyChanged("SelectedItems");

                HandleSelectionChanged();
            }

            return true;
        }

        private void HandleSelectionChanged()
        {
            bool sourceDictionaries = false;
            bool globalDictionaries = false;
            bool globalTextures = false;
            bool sourceTextures = false;
            bool valid = true;
            foreach (var selection in this.SelectedItems)
            {
                if (selection is SourceTextureDictionaryViewModel)
                {
                    sourceDictionaries = true;
                    if (globalDictionaries)
                    {
                        valid = false;
                        break;
                    }
                }
                else if (selection is GlobalTextureDictionaryViewModel)
                {
                    globalDictionaries = true;
                    if (sourceDictionaries)
                    {
                        valid = false;
                        break;
                    }
                }
                else if (selection is GlobalTextureViewModel)
                {
                    globalTextures = true;
                    valid = false;
                    break;
                }
                else if (selection is SourceTextureViewModel)
                {
                    sourceTextures = true;
                    valid = false;
                    break;
                }
            }
            if (valid && sourceDictionaries)
                HandleSelectedSourceDictionaries();
            else if (valid && globalDictionaries)
                HandleSelectedGlobalDictionaries();
            else
                HandleInvalidSelection();

            if (sourceDictionaries)
            {
                if (!globalDictionaries && !globalTextures && !sourceTextures)
                    SelectedItemType = "Source Texture Dictionaries";
                else
                    SelectedItemType = "Mixed";

            }
            else if (globalDictionaries)
            {
                if (!sourceDictionaries && !globalTextures && !sourceTextures)
                    SelectedItemType = "Global Texture Dictionaries";
                else
                    SelectedItemType = "Mixed";
            }
            else if (globalTextures)
            {
                if (!globalDictionaries && !sourceDictionaries && !sourceTextures)
                    SelectedItemType = "Global Textures";
                else
                    SelectedItemType = "Mixed";

            }
            else if (sourceTextures)
            {
                if (!globalDictionaries && !sourceDictionaries && !globalTextures)
                    SelectedItemType = "Source Textures";
                else
                    SelectedItemType = "Mixed";

            }
            else
                this.SelectedItemType = "None";
        }

        private void HandleSelectedSourceDictionaries()
        {
            Dictionary<string, int> streamNames = new Dictionary<string, int>();
            Dictionary<string, bool> promotionState = new Dictionary<string, bool>();
            Dictionary<SourceTextureViewModel, int> textures = new Dictionary<SourceTextureViewModel, int>();

            IEnumerable<SourceTextureViewModel> allTextures = from gd in SelectedItems.OfType<SourceTextureDictionaryViewModel>()
                                                              from t in gd.Children.OfType<SourceTextureViewModel>()
                                                              select t;

            foreach (SourceTextureViewModel texture in allTextures)
            {
                if (streamNames.ContainsKey(texture.StreamName.ToLower()))
                    continue;

                int count = 0;
                bool fullyPromoted = true;
                SourceTextureViewModel shownTexture = null;
                foreach (SourceTextureViewModel sibling in allTextures)
                {
                    if (sibling.StreamName.ToLower() == texture.StreamName.ToLower())
                    {
                        count++;
                        if (!texture.Model.Promoted)
                        {
                            shownTexture = sibling;
                            fullyPromoted = false;
                        }
                    }
                }
                if (shownTexture == null)
                    shownTexture = texture;

                promotionState.Add(texture.StreamName.ToLower(), fullyPromoted);
                streamNames.Add(texture.StreamName.ToLower(), count);

                textures.Add(shownTexture, count);
            }

            SharedTextures.Clear();
            NumberOfSharedTextures = 0;
            NumberOfUniqueTextures = 0;
            TotalNumberOfTextures = allTextures.Count();
            foreach (var texture in textures)
            {
                if (texture.Value <= 1)
                {
                    NumberOfUniqueTextures++;
                }
                else
                {
                    NumberOfSharedTextures++;
                    if (!this.ShowPromotedTextures && promotionState[texture.Key.StreamName.ToLower()])
                        continue;

                    SharedTextures.Add(texture.Key);
                }
            }
            int totalCount = this.SelectedItems.Count;
            foreach (SourceTextureViewModel texture in allTextures)
            {
                string name = texture.StreamName.ToLower();
                if (!streamNames.ContainsKey(name))
                    continue;

                texture.SelectedSharedPercentage = (float)streamNames[name] / (float)totalCount;
                texture.SelectedSharedCount = streamNames[name];
                texture.SelectedSaveIfPromoted = (streamNames[name] - 1) * texture.Model.StreamSize;
                if (texture.Model.Promoted)
                    texture.SelectedSaveIfPromoted = -1;
            }
        }

        private void HandleSelectedGlobalDictionaries()
        {
            Dictionary<string, int> streamNames = new Dictionary<string, int>();
            Dictionary<GlobalTextureViewModel, int> textures = new Dictionary<GlobalTextureViewModel, int>();

            IEnumerable<GlobalTextureViewModel> allTextures = from gd in SelectedItems.OfType<GlobalTextureDictionaryViewModel>()
                                                              from t in gd.Textures.OfType<GlobalTextureViewModel>()
                                                              select t;

            foreach (GlobalTextureViewModel texture in allTextures)
            {
                if (streamNames.ContainsKey(texture.StreamName.ToLower()))
                    continue;

                int count = 0;
                foreach (GlobalTextureViewModel sibling in allTextures)
                {
                    if (sibling.StreamName.ToLower() == texture.StreamName.ToLower())
                        count++;
                }
                streamNames.Add(texture.StreamName.ToLower(), count);
                textures.Add(texture, count);
            }

            SharedTextures.Clear();
            NumberOfSharedTextures = 0;
            NumberOfUniqueTextures = 0;
            TotalNumberOfTextures = allTextures.Count();
            foreach (var texture in textures)
            {
                if (texture.Value <= 1)
                {
                    NumberOfUniqueTextures++;
                }
                else
                {
                    NumberOfSharedTextures++;
                    SharedTextures.Add(texture.Key);
                }
            }

            int totalCount = this.SelectedItems.Count;
            foreach (GlobalTextureViewModel texture in allTextures)
            {
                string name = texture.StreamName.ToLower();
                if (!streamNames.ContainsKey(name))
                    continue;

                texture.SelectedSharedPercentage = (float)streamNames[name] / (float)totalCount;
                texture.SelectedSharedCount = streamNames[name];
                texture.SelectedSaveIfPromoted = -1;
            }
        }

        private void HandleInvalidSelection()
        {
            SharedTextures.Clear();
            NumberOfSharedTextures = 0;
            NumberOfUniqueTextures = 0;
            TotalNumberOfTextures = 0;
        }

        private class PerforceChangelist : IDisposable
        {
           #region Fields
            /// <summary>
            /// The private event handler that is used for the <see cref="Disposing"/> event.
            /// </summary>
            private EventHandler disposing;

            /// <summary>
            /// The private event handler that is used for the <see cref="Disposed"/> event.
            /// </summary>
            private EventHandler disposed;

            /// <summary>
            /// The private field used for the <see cref="IsDisposed"/> property.
            /// </summary>
            private bool isDisposed;

            /// <summary>
            /// The description that this changelist has.
            /// </summary>
            private string description;

            /// <summary>
            /// The private field used for the <see cref="Number"/> property.
            /// </summary>
            private string number;

            /// <summary>
            /// The private reference to the p4 object used to create the changelist.
            /// </summary>
            private P4 p4;
            #endregion

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="PerforceChangelist"/> class.
            /// </summary>
            /// <param name="description">
            /// 
            /// </param>
            /// <param name="p4">
            /// 
            /// </param>
            public PerforceChangelist(string description, P4 p4)
            {
                this.p4 = p4;
                this.description = description;
                P4PendingChangelist cl = p4.CreatePendingChangelist(description);
                this.number = cl.Number.ToString();
            }

            /// <summary>
            /// Finalises an instance of the
            /// <see cref="Rockstar.DisposableObject" /> class.
            /// </summary>
            ~PerforceChangelist()
            {
                this.Dispose(false);
            }
            #endregion

            #region Events
            /// <summary>
            /// Occurs when this instance is being disposed by the garage collector or by a user.
            /// </summary>
            public event EventHandler Disposing
            {
                add
                {
                    this.ThrowIfDisposed();
                    this.disposing = (EventHandler)Delegate.Combine(this.disposing, value);
                }

                remove
                {
                    this.ThrowIfDisposed();
                    this.disposing = (EventHandler)Delegate.Remove(this.disposing, value);
                }
            }

            /// <summary>
            /// Occurs when this instance has been disposed by the garage collector or by a user.
            /// </summary>
            public event EventHandler Disposed
            {
                add
                {
                    this.disposed = (EventHandler)Delegate.Combine(this.disposed, value);
                }

                remove
                {
                    this.disposed = (EventHandler)Delegate.Remove(this.disposed, value);
                }
            }
            #endregion

            #region Properties
            /// <summary>
            /// Gets a value indicating whether this instance has been disposed of.
            /// </summary>
            public bool IsDisposed
            {
                get { return this.isDisposed; }
            }

            /// <summary>
            /// Gets the changelist number created as a string so that it can be used as a
            /// argument for perforce operations.
            /// </summary>
            public string Number
            {
                get { return this.number; }
            }
            #endregion

            #region Methods
            /// <summary>
            /// Performs application-defined tasks associated with freeing, releasing, or
            /// resetting unmanaged resources.
            /// </summary>
            public void Dispose()
            {
                this.Dispose(true);
                GC.SuppressFinalize(this);
            }

            /// <summary>
            /// Performs application-defined tasks associated with freeing, releasing, or
            /// resetting unmanaged resources.
            /// </summary>
            /// <param name="disposeManaged">
            /// If true the managed resources for this instance also get disposed of as well as
            /// the unmanaged resources.
            /// </param>
            private void Dispose(bool disposeManaged)
            {
                if (!this.IsDisposed)
                {
                    try
                    {
                        EventHandler handler = this.disposing;
                        if (handler != null)
                        {
                            handler(this, EventArgs.Empty);
                        }

                        this.disposing = null;
                        if (disposeManaged)
                        {
                            this.DisposeManagedResources();
                        }

                        this.DisposeNativeResources();
                    }
                    finally
                    {
                        this.isDisposed = true;
                        EventHandler handler = this.disposed;
                        if (handler != null)
                        {
                            handler(this, EventArgs.Empty);
                        }
                    }
                }
            }

            /// <summary>
            /// When overridden disposes of the managed resources.
            /// </summary>
            private void DisposeManagedResources()
            {
                P4RecordSet currentChangeLists = this.p4.Run(
                    "changelists",
                    "-l",
                    "-s",
                    "pending",
                    "-u",
                    this.p4.User,
                    "-c",
                    this.p4.Client);

                if (currentChangeLists.Records.Count() == 0)
                {
                    return;
                }

                foreach (P4Record record in currentChangeLists.Records)
                {
                    FieldDictionary fields = record.Fields;
                    if (fields == null)
                    {
                        continue;
                    }

                    if (!fields.ContainsKey("desc") || !fields.ContainsKey("change"))
                    {
                        continue;
                    }
                    
                    string description = record.Fields["desc"];
                    if (description != this.description)
                    {
                        continue;
                    }

                    p4.Run("change", "-d", record.Fields["change"]);
                }
            }

            /// <summary>
            /// When overridden disposes of the unmanaged resources.
            /// </summary>
            private void DisposeNativeResources()
            {
            }

            /// <summary>
            /// Throws a System.ObjectDisposedException exception if this instance has been
            /// already disposed of.
            /// </summary>
            protected void ThrowIfDisposed()
            {
                if (this.IsDisposed)
                {
                    throw new ObjectDisposedException(this.GetType().Name);
                }
            }
            #endregion
        }
    } // TXDParentiserViewModel
} // Workbench.AddIn.TXDParentiser
