﻿using System;

namespace ContentBrowser.AddIn
{
    /// <summary>
    /// Points inside the content browser that can be
    /// used for the composition of external plugins
    /// </summary>
    public static class CompositionPoints
    {
        /// <summary>
        /// The point for an object to use to import the content browser
        /// </summary>
        public const String ContentBrowser =
            "86E0359B-79C6-427B-8C3C-3BD6E9FDEE39";

        /// <summary>
        /// Settings composition point
        /// </summary>
        public const String ContentBrowserSettings =
            "E933E7C1-F5A0-401D-A771-38D01A7B678C";

        #region Browser Guids

        public static Guid LevelBrowserGuid = new Guid("B715139A-E73D-44BA-A1FE-BFB1B9299BE7");
        public static Guid MetadataBrowserGuid = new Guid("0E67BCCB-940D-49F0-9DB7-55DB250A4EF9");
        public static Guid PerforceBrowserGuid = new Guid("BBD161F2-D1CE-4E76-B792-2AA17A8EBF07");
        public static Guid ReportBrowserGuid = new Guid("275B454B-7A4C-4983-A1BF-7CC378755D15");
        public static Guid ContentBrowserGuid = new Guid("171A52EA-D967-4062-AE9B-4AFDFB7F3ED4");

        #endregion // Browser Guids

    } // CompositionPoints
} // ContentBrowser.AddIn
