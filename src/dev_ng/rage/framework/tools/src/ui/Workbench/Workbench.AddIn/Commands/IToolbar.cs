﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn.Services;

namespace Workbench.AddIn.Commands
{
    public interface IToolbar : IExtension
    {
        #region Properties

        /// <summary>
        /// The child commands to this one. Any child commands will be
        /// set as a sub menu to a menu item.
        /// </summary>
        IEnumerable<IWorkbenchCommand> Items { get; }
        
        #endregion // Properties
    } // IToolbar
} // Workbench.AddIn.Commands
