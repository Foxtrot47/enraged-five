﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RSG.Model.Common.Map;

namespace Workbench.AddIn.MapDebugging.OverlayViews
{
    /// <summary>
    /// Interaction logic for ObjctLocationOverlayView.xaml
    /// </summary>
    public partial class ObjectLocationOverlayView : UserControl
    {
        public ObjectLocationOverlayView()
        {
            InitializeComponent();
        }

        private void CollectionViewSource_Filter(Object sender, FilterEventArgs e)
        {
            if (e.Item is IEntity || e.Item is IArchetype || e.Item is RSG.Model.Asset.Texture)
                e.Accepted = true;
            else
                e.Accepted = false;
        }
    }
}
