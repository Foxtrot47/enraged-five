﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using RSG.Model.Report;
using System.ComponentModel.Composition;
using RSG.Base.Logging;
using Report.AddIn;

namespace Workbench.AddIn.CharacterStatistics.Reports
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Report.AddIn.ExtensionPoints.ReportCategory, typeof(IReportCategory))]
    class CharacterCategory :
        ReportCategory,
        IPartImportsSatisfiedNotification
    {
        #region Constants
        private const String NAME = "Characters";
        private const String DESC = "Character Reports.";
        #endregion // Constants

        #region Properties and Associated Member Data
        /// <summary>
        /// Category report objects.
        /// This is abstract since we wish your class to implement it with the appropriate attribute ( as defined by your class ) for MEF imports.
        /// </summary>
        [ImportManyExtension(ExtensionPoints.Report, typeof(IReport))]
        public override IEnumerable<IReportItem> Reports { get; protected set; }
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public CharacterCategory()
            : base(NAME, DESC)
        {
        }
        #endregion // Constructor(s)
    } // CharacterCategory
}
