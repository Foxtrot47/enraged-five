﻿using System;
using Workbench.AddIn.UI.Layout;
using WidgetEditor.ViewModel;


namespace WidgetEditor.Document
{
    /// <summary>
    /// 
    /// </summary>
    public class WidgetDocumentBase : Workbench.AddIn.UI.Layout.DocumentBase<WidgetDocumentViewModel>, IWidgetDocument
    {
        #region Properties

        /// <summary>
        /// Array of String Metadata types this Metadata Document can view
        /// (e.g. "*", "::rage::cutfCutSceneFile2").
        /// </summary>
        public virtual String[] SupportedMetadataTypes { get; set; }

        /// <summary>
        /// Type of this document.
        /// </summary>
        public virtual WidgetDocumentType DocumentType { get; set; }

        #endregion // Properties

        #region Constructors

        public WidgetDocumentBase()
        {
        }

        public WidgetDocumentBase( String name, WidgetDocumentViewModel viewModel )
            : base(name, viewModel)
        {
        }
            
        #endregion // Constructors
    }

    public enum WidgetDocumentType
    {
        Default,
        Favorite,
        Search
    }

    public interface IWidgetDocument
    {
        /// <summary>
        /// Metadata model being viewed/edited.
        /// </summary>
        WidgetDocumentViewModel ViewModel { get; }

        /// <summary>
        /// Array of String Metadata types this Metadata Document can view
        /// (e.g. "*", "::rage::cutfCutSceneFile2").
        /// </summary>
        String[] SupportedMetadataTypes { get; }

        /// <summary>
        /// Type of this document.
        /// </summary>
        WidgetDocumentType DocumentType { get; }
    }

} // MetadataEditor.AddIn.View namespace
