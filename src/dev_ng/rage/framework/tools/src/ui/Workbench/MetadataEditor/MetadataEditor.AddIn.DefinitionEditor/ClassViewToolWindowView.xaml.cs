﻿using System;
using System.Diagnostics;
using System.Windows;
using Workbench.AddIn;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.Services;
using RSG.Metadata.Model;
using RSG.Metadata.Parser;
using MetadataEditor.AddIn.DefinitionEditor.ViewModel;
using System.Collections.Generic;
using System.Windows.Controls;
using RSG.Base.Editor;

namespace MetadataEditor.AddIn.DefinitionEditor
{
    public class SearchResult : ISearchResult
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public object Data { get; set; }

        /// <summary>
        /// Represents whether the user can jump to the search result
        /// to see it in the view.
        /// </summary>
        public bool CanJumpTo { get { return true;} }

        /// <summary>
        /// Represents what to show the user in the search results
        /// window for this result.
        /// </summary>
        public string DisplayText { get; set; }

        /// <summary>
        /// A reference to the content
        /// the search result was found in.
        /// </summary>
        public IContentBase Content { get; set; }
        #endregion

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="text"></param>
        public SearchResult(object data, string text, IContentBase content)
        {
            Data = data;
            DisplayText = text;
            Content = content;
        }
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    public partial class ClassViewToolWindowView : ToolWindowBase<ClassViewToolWindowViewModel>
    {
        #region Properties
        ILayoutManager LayoutManager
        {
            get;
            set;
        }

        IPropertyInspector PropertyInspector
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ClassViewToolWindowView()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public ClassViewToolWindowView(ILayoutManager layoutManager, IStructureDictionary structures, IPropertyInspector propertyInspector)
            : base(ClassViewToolWindowViewModel.TOOLWINDOW_NAME, new ClassViewToolWindowViewModel(structures))
        {
            InitializeComponent();
            this.LayoutManager = layoutManager;
            this.PropertyInspector = propertyInspector;
        }
        #endregion // Constructor(s)

        #region Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TreeView_MouseDoubleClick(Object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (LayoutManager != null)
            {
                if (((sender as System.Windows.Controls.TreeView).SelectedItem is StructureViewModel))
                {
                    Structure model = ((sender as System.Windows.Controls.TreeView).SelectedItem as StructureViewModel).Model;
                    Debug.Assert(null != model, "No selected definition model.");
                    if (null == model)
                        return;

                    DefinitionEditorView newDocument = new DefinitionEditorView(model, this.PropertyInspector);
                    this.LayoutManager.ShowDocument(newDocument);
                }
            }
        }        
        #endregion // Event Handlers

        #region Overridden Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public override List<ISearchResult> FindAll(ISearchQuery query)
        {
            List<ISearchResult> results = new List<ISearchResult>();

            foreach (var ns in this.ViewModel.Structures.RootNamespaces)
            {
                foreach (StructureViewModel s in GetAllStructures(ns))
                {
                    if (query.Expression.IsMatch(s.Model.ShortDataType))
                    {
                        results.Add(new SearchResult(s, s.Model.ShortDataType, this));
                    }
                }
            }
            return results;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="result"></param>
        public override void SelectSearchResult(ISearchResult result)
        {
            if (result.Data is IViewModel)
            {
                (result.Data as IViewModel).IsSelected = true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private IEnumerable<StructureViewModel> GetAllStructures(NamespaceViewModel root)
        {
            foreach (var child in root.Children)
            {
                if (child is StructureViewModel)
                {
                    yield return child as StructureViewModel;
                    foreach (var grandchild in GetAllStructures(child as StructureViewModel))
                        yield return grandchild;
                }

                if (child is NamespaceViewModel)
                {
                    foreach (var grandchild in GetAllStructures(child as NamespaceViewModel))
                        yield return grandchild;
                }
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="root"></param>
        /// <returns></returns>
        private IEnumerable<StructureViewModel> GetAllStructures(StructureViewModel root)
        {
            foreach (var child in root.Children)
            {
                if (child is StructureViewModel)
                {
                    yield return child as StructureViewModel;
                    foreach (var grandchild in GetAllStructures(child as StructureViewModel))
                        yield return grandchild;
                }

                if (child is NamespaceViewModel)
                {
                    foreach (var grandchild in GetAllStructures(child as NamespaceViewModel))
                        yield return grandchild;
                }
            }
        }
        #endregion

        private void TreeViewItem_Selected(object sender, RoutedEventArgs e)
        {
            (sender as TreeViewItem).BringIntoView();
        }
    }
} // MetadataEditor.AddIn.DefinitionEditorViewModel namespace
