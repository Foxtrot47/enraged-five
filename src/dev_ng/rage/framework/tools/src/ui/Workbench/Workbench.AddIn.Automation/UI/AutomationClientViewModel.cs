﻿using System;
using System.Collections.Generic;
using System.Linq;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using RSG.Pipeline.Automation.Common.Client;

namespace Workbench.AddIn.Automation.UI
{

    /// <summary>
    /// Client view model.
    /// </summary>
    internal class AutomationClientViewModel : ViewModelBase
    {
        #region Properties
        /// <summary>
        /// Worker status object.
        /// </summary>
        public WorkerStatus Status
        {
            get { return m_Status; }
            private set
            {
                SetPropertyValue(value, () => this.Status,
                    new PropertySetDelegate(delegate(Object newValue) { m_Status = (WorkerStatus)newValue; }));
            }
        }
        private WorkerStatus m_Status;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="status"></param>
        public AutomationClientViewModel(WorkerStatus status)
        {
            this.Status = status;
        }
        #endregion // Constructor(s)
    }

} // Workbench.AddIn.Automation.UI namespace
