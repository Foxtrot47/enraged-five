﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using Workbench.AddIn;
using Workbench.AddIn.Commands;
using Workbench.AddIn.Services;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.UI.Menu;
using Workbench.UI.Toolbar;

namespace Workbench.UI.Menu
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.MenuMain, typeof(IWorkbenchCommand))]
    class EditMenu : WorkbenchMenuItemBase, IPartImportsSatisfiedNotification
    {
        #region MEF Imports
        /// <summary>
        /// Extensions service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ExtensionService)]
        private IExtensionService ExtensionService { get; set; }

        /// <summary>
        /// Edit menu subitems.
        /// </summary>
        [ImportManyExtension(Workbench.AddIn.ExtensionPoints.MenuEdit,
            typeof(IWorkbenchCommand))]
        private IEnumerable<IWorkbenchCommand> items { get; set; }
        #endregion // MEF Imports

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public EditMenu()
        {
            this.Header = "_Edit";
            this.ID = new Guid(Workbench.AddIn.ExtensionPoints.MenuEdit);
            this.RelativeID = new Guid(Workbench.AddIn.ExtensionPoints.MenuFile);
            this.Direction = Direction.After;
        }
        #endregion // Constructor(s)

        #region IPartImportsSatisfiedNotification Methods
        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {
            this.Items = ExtensionService.Sort(items);
        }
        #endregion // IPartImportsSatisfiedNotification Methods

        #region ICommand Implementation

        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
        }

        #endregion // ICommand Implementation
    } // WorkbenchMenuItemBase

    /// <summary>
    /// Undo menu item.
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.MenuEdit, typeof(IWorkbenchCommand))]
    [ExportExtension(Workbench.AddIn.ExtensionPoints.ToolbarStandard, typeof(IWorkbenchCommand))]
    class UndoMenuItem : WorkbenchMenuItemBase
    {
        #region Constants

        public static readonly Guid GUID = new Guid("358B461B-810B-420F-B218-E97384B0C541");

        #endregion // Constants

        #region MEF Imports
        [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager, typeof(ILayoutManager))]
        private ILayoutManager LayoutManager { get; set; }

        [ImportExtension(Workbench.AddIn.CompositionPoints.UndoRedoService, typeof(Workbench.AddIn.Services.IUndoRedoService))]
        private Workbench.AddIn.Services.IUndoRedoService UndoRedoService { get; set; }

        #endregion // MEF Imports

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public UndoMenuItem()
        {
            this.Header = "_Undo";
            this.ID = GUID;
            this.RelativeToolBarID = StandardToolbarSep1.GUID;
            this.Direction = AddIn.Services.Direction.After;
            SetImageFromBitmap(Workbench.Resources.Images.Undo);
            this.KeyGesture = new System.Windows.Input.KeyGesture(System.Windows.Input.Key.Z, System.Windows.Input.ModifierKeys.Control);
            this.ToolTip = "Undo";
        }
        #endregion // Constructor(s)

        #region ICommand Implementation

        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            IContentBase content = this.LayoutManager.GetActiveContent();

            if (this.UndoRedoService != null && content != null)
            {
                if (this.UndoRedoService.UndoCountForContent(content) > 0)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
            IContentBase content = this.LayoutManager.GetActiveContent();

            if (this.UndoRedoService != null && content != null)
            {
                this.UndoRedoService.Undo(content);
            }
        }

        #endregion // ICommand Implementation
    } // UndoMenuItem

    /// <summary>
    /// Redo menu item.
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.MenuEdit, typeof(IWorkbenchCommand))]
    [ExportExtension(Workbench.AddIn.ExtensionPoints.ToolbarStandard, typeof(IWorkbenchCommand))]
    class RedoMenuItem : WorkbenchMenuItemBase
    {
        #region Constants

        public static readonly Guid GUID = new Guid("95661695-CD1A-4263-84D6-2A82172D34F8");

        #endregion // Constants
 
        #region MEF Imports

        [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager, typeof(ILayoutManager))]
        private ILayoutManager LayoutManager { get; set; }

        [ImportExtension(Workbench.AddIn.CompositionPoints.UndoRedoService, typeof(Workbench.AddIn.Services.IUndoRedoService))]
        private Workbench.AddIn.Services.IUndoRedoService UndoRedoService { get; set; }

        #endregion // MEF Imports

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public RedoMenuItem()
        {
            this.Header = "_Redo";
            this.ID = GUID;
            this.RelativeToolBarID = UndoMenuItem.GUID;
            this.Direction = AddIn.Services.Direction.After;
            SetImageFromBitmap(Workbench.Resources.Images.Redo);
            this.KeyGesture = new System.Windows.Input.KeyGesture(System.Windows.Input.Key.Y, System.Windows.Input.ModifierKeys.Control);
            this.ToolTip = "Redo";
        }
        #endregion // Constructor(s)

        #region ICommand Implementation

        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            IContentBase content = this.LayoutManager.GetActiveContent();

            if (this.UndoRedoService != null && content != null)
            {
                if (this.UndoRedoService.RedoCountForContent(content) > 0)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
            IContentBase content = this.LayoutManager.GetActiveContent();

            if (this.UndoRedoService != null && content != null)
            {
                this.UndoRedoService.Redo(content);
            }
        }

        #endregion // ICommand Implementation
    } // RedoMenuItem

    /// <summary>
    /// Find menu item.
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.MenuEdit, typeof(IWorkbenchCommand))]
    class FindMenuItem : WorkbenchMenuItemBase, IPartImportsSatisfiedNotification
    {
        #region Constants

        public static readonly Guid GUID = new Guid("1214DAAB-77A9-4EB3-94B4-09BEC719003D");

        #endregion // Constants

        #region MEF Imports

        /// <summary>
        /// Extensions service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ExtensionService)]
        private IExtensionService ExtensionService { get; set; }

        [ImportManyExtension(Workbench.AddIn.ExtensionPoints.MenuFind, typeof(IWorkbenchCommand))]
        private IEnumerable<IWorkbenchCommand> items { get; set; }

        #endregion // MEF Imports

        #region Constructor(s)

        /// <summary>
        /// Default constructor.
        /// </summary>
        public FindMenuItem()
        {
            this.Header = "_Find";
            this.ID = GUID;
            this.RelativeToolBarID = RedoMenuItem.GUID;
            this.Direction = AddIn.Services.Direction.After;
        }

        #endregion // Constructor(s)

        #region IPartImportsSatisfiedNotification Methods

        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {
            this.Items = ExtensionService.Sort(items);
        }

        #endregion // IPartImportsSatisfiedNotification Methods

        #region ICommand Implementation

        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
        }

        #endregion // ICommand Implementation
    } // FindMenuItem

} // Workbench.UI.Menu namespace
