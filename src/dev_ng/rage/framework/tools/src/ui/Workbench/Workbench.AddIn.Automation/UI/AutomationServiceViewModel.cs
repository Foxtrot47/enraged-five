﻿using System;
using RSG.Base.Configuration.Automation;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;

namespace Workbench.AddIn.Automation.UI
{

    /// <summary>
    /// Automation Service View Model.
    /// </summary>
    internal class AutomationServiceViewModel : ViewModelBase
    {
        #region Properties
        /// <summary>
        /// Automation Service Friendly Name (from config data).
        /// </summary>
        public String FriendlyName
        {
            get { return m_sFriendlyName; }
            set
            {
                SetPropertyValue(value, () => this.FriendlyName,
                   new PropertySetDelegate(delegate(Object newValue) { m_sFriendlyName = (String)newValue; }));
            }
        }
        private String m_sFriendlyName;
        
        /// <summary>
        /// Automation Service URI (from config data).
        /// </summary>
        public Uri AutomationServiceConnection 
        {
            get { return m_uriAutomationServiceConnection; }
            private set
            {
                SetPropertyValue(value, () => this.AutomationServiceConnection,
                   new PropertySetDelegate(delegate(Object newValue) { m_uriAutomationServiceConnection = (Uri)newValue; }));
            }
        }
        private Uri m_uriAutomationServiceConnection;

        /// <summary>
        /// File Transfer Service URI (from config data).
        /// </summary>
        public Uri FileTransferServiceConnection
        {
            get { return m_uriFileTransferServiceConnection; }
            private set
            {
                SetPropertyValue(value, () => this.FileTransferServiceConnection,
                   new PropertySetDelegate(delegate(Object newValue) { m_uriFileTransferServiceConnection = (Uri)newValue; }));
            }
        }
        private Uri m_uriFileTransferServiceConnection;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="automationService"></param>
        public AutomationServiceViewModel(IAutomationServiceParameters automationService)
        {
            this.FriendlyName = automationService.FriendlyName;
            this.AutomationServiceConnection = automationService.AutomationService;
            this.FileTransferServiceConnection = automationService.FileTransferService;
        }
        #endregion // Constructor(s)
    }

} // Workbench.AddIn.Automation.UI namespace
