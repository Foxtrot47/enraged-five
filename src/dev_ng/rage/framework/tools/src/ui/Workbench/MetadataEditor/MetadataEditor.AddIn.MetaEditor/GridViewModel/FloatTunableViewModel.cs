﻿using MetadataEditor.AddIn.MetaEditor.ViewModel;
using RSG.Metadata.Data;

namespace MetadataEditor.AddIn.MetaEditor.GridViewModel
{
    /// <summary>
    /// The view model that wraps around a 
    /// <see cref="FloatTunable"/> object. Used
    /// for the grid view.
    /// </summary>
    public class FloatTunableViewModel : GridTunableViewModel
    {
        #region Constructor
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="MetadataEditor.AddIn.MetaEditor.GridViewModel.FloatTunableViewModel"/>
        /// class.
        /// </summary>
        public FloatTunableViewModel(GridTunableViewModel parent, FloatTunable m, MetaFileViewModel root)
            : base(parent, m, root)
        {
        }
        #endregion // Constructor
    } // FloatTunableViewModel
} // MetadataEditor.AddIn.MetaEditor.GridViewModel
