﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RSG.Base.Windows.Controls;

namespace MetadataEditor.AddIn.KinoEditor.Subtitle_Editor
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class UserControl1 : UserControl
    {
        /// <summary>
        /// Register the <see cref="TotalFrames"/> dependency property.
        /// /// </summary>
        public static readonly DependencyProperty TotalFramesProperty = DependencyProperty.Register("TotalFrames", typeof(double), typeof(UserControl1), new UIPropertyMetadata(new PropertyChangedCallback(Property_Changed)));

        /// <summary>
        /// Register the <see cref="CurrentFrame"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty CurrentFrameProperty = DependencyProperty.Register("CurrentFrame", typeof(double), typeof(UserControl1), new UIPropertyMetadata(new PropertyChangedCallback(Property_Changed)));

        /// <summary>
        /// Total number of frames in the clip.
        /// </summary>
        public double TotalFrames
        {
            get { return (double)GetValue(TotalFramesProperty); }
            set { SetValue(TotalFramesProperty, value); }
        }

        /// <summary>
        /// The current frame.
        /// </summary>
        public double CurrentFrame
        {
            get { return (double)GetValue(CurrentFrameProperty); }
            set { SetValue(CurrentFrameProperty, value); }
        }

        public UserControl1()
        {
            InitializeComponent();
            NameScope.SetNameScope(this.TextGrid.ContextMenu, NameScope.GetNameScope(this));
        }

        /// <summary>
        /// Handle property changes from the scrubber.
        /// </summary>
        /// <param name="target">Target.</param>
        /// <param name="e">Event arguments.</param>
        private static void Property_Changed(DependencyObject target, DependencyPropertyChangedEventArgs e)
        {
            var uc = target as UserControl1;
            if (uc != null)
            {
                uc.eventControl.IndicatorPosition = uc.CurrentFrame / uc.TotalFrames;
            }
        }

        private void RemoveFromTimelineClicked(object sender, RoutedEventArgs e)
        {
            DependencyObject parent = VisualTreeHelper.GetParent(sender as DependencyObject);
            ContextMenu contextMenu = parent as ContextMenu;
            while (parent != null && contextMenu == null)
            {
                parent = VisualTreeHelper.GetParent(parent);
                contextMenu = parent as ContextMenu;
            }

            if (contextMenu != null)
            {
                FrameworkElement element = contextMenu.PlacementTarget as FrameworkElement;
                if (element != null)
                {
                    EventViewModel vm = element.DataContext as EventViewModel;
                    SubtitleEditorViewModel subtitleViewModel = this.DataContext as SubtitleEditorViewModel;
                    if (vm != null && subtitleViewModel != null)
                    {
                        int trackIndex = 0;
                        EventTrackViewModel track = null;
                        for (int i = 0; i < subtitleViewModel.TrackEntries.Count; ++i)
                        {
                            if (subtitleViewModel.TrackEntries[i].Name.Trim() == vm.Speaker)
                            {
                                track = subtitleViewModel.TrackEntries[i];
                                trackIndex = i;
                                break;
                            }
                        }

                        if (track != null)
                        {
                            for (int i = 0; i < track.Events.Count; ++i)
                            {
                                if (track.Events[i].Equals(vm))
                                {
                                    track.Events[i].OnTimeline = false;
                                    track.Events.Remove(track.Events[i]);
                                    if (track.Events.Count == 0)
                                    {
                                        subtitleViewModel.TrackEntries.RemoveAt(trackIndex);
                                    }

                                    break;
                                }
                            }
                        }
                    }
                }            
            }
        }
    }
}
