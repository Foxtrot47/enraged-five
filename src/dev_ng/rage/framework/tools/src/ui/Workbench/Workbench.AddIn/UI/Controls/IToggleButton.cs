﻿using System;

namespace Workbench.AddIn.UI.Controls
{

    /// <summary>
    /// Toggle button interface.
    /// </summary>
    public interface IToggleButton : IButton
    {
        #region Properties
        /// <summary>
        /// Toggled state of the toggle button.
        /// </summary>
        bool IsToggled { get; }
        #endregion // Properties
    }

} // Workbench.AddIn.UI.Controls namespace
