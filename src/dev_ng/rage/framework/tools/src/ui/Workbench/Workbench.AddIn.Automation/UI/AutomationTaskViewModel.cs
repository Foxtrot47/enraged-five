﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Data;
using System.Linq;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using RSG.Interop.Bugstar.Organisation;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Tasks;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Base.Configuration;

namespace Workbench.AddIn.Automation.UI
{

    /// <summary>
    /// Automation task view model.
    /// </summary>
    internal class AutomationTaskViewModel : AutomationTabViewModelBase
    {
        #region Properties
        /// <summary>
        /// Automation Job View Models
        /// </summary>
        public ObservableCollection<AutomationJobViewModel> Jobs
        {
            get { return m_Jobs; }
            private set
            {
                SetPropertyValue(value, () => this.Jobs,
                    new PropertySetDelegate(delegate(Object newValue) { m_Jobs = (ObservableCollection<AutomationJobViewModel>)newValue; }));
            }
        }
        private ObservableCollection<AutomationJobViewModel> m_Jobs;

        /// <summary>
        /// Automation Jobs selected by the user in the ListView.
        /// </summary>
        public IEnumerable<AutomationJobViewModel> SelectedJobs
        {
            get { return (this.Jobs.Where(j => j.IsSelected)); }
        }

        /// <summary>
        /// Number of automation jobs selected by the user.
        /// </summary>
        public int SelectedJobCount
        {
            get { return (this.SelectedJobs.Count()); }
        }

        /// <summary>
        /// Are all selected jobs completed.
        /// </summary>
        public bool SelectedJobsCompleted
        {
            get 
            { 
                int selCompleted = this.SelectedJobs.Where(j => j.Job.State.Equals(JobState.Completed)).Count();
                return (selCompleted == this.SelectedJobCount); 
            }
        }

        /// <summary>
        /// Are all selected jobs errored.
        /// </summary>
        public bool SelectedJobsErrored
        {
            get
            {
                int selCompleted = this.SelectedJobs.Where(j => j.Job.State.Equals(JobState.Errors)).Count();
                return (selCompleted == this.SelectedJobCount);
            }
        }

        /// <summary>
        /// Are all selected jobs completed or errored.
        /// </summary>
        public bool SelectedJobsCompletedOrErrored
        {
            get
            {
                int selCompleted = this.SelectedJobs.Where(j => j.Job.State.Equals(JobState.Errors) ||
                    j.Job.State.Equals(JobState.Completed)).Count();
                return (selCompleted == this.SelectedJobCount);
            }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="status"></param>
        public AutomationTaskViewModel(User[] users, TaskStatus status)
            : base(status.Name)
        {
            IEnumerable<AutomationJobViewModel> sortedJobs = from j in status.Jobs
                                                             where j.Trigger is ITrigger
                                                             orderby (j.Trigger as ITrigger).TriggeredAt descending
                                                             select new AutomationJobViewModel(users, j);

            IEnumerable<AutomationJobViewModel> unsortedJobs = from j in status.Jobs
                                                               where !(j.Trigger is ITrigger)
                                                               select new AutomationJobViewModel(users, j);

            this.Jobs = new ObservableCollection<AutomationJobViewModel>(sortedJobs.Concat(unsortedJobs));
        }
        #endregion // Constructor(s)

        #region Methods
        public override void FilterUsername(string filterString)
        {
            foreach (AutomationJobViewModel job in this.Jobs)
            {
                if (string.IsNullOrEmpty(filterString))
                {
                    job.IsVisible = true;
                    continue;
                }

                if (job.Job.Trigger is IChangelistTrigger)
                {
                    IChangelistTrigger trigger = job.Job.Trigger as IChangelistTrigger;
                    if (trigger.Username == null || !trigger.Username.Contains(filterString))
                    {
                        job.IsVisible = false;
                    }
                    else
                    {
                        job.IsVisible = true;
                    }

                    continue;
                }
                else if (job.Job.Trigger is UserRequestTrigger)
                {
                    UserRequestTrigger trigger = job.Job.Trigger as UserRequestTrigger;
                    if (trigger.Username == null || !trigger.Username.Contains(filterString))
                    {
                        job.IsVisible = false;
                    }
                    else
                    {
                        job.IsVisible = true;
                    }

                    continue;
                }
                
                if (string.IsNullOrEmpty(filterString))
                {
                    job.IsVisible = true;
                }
                else
                {
                    job.IsVisible = false;
                }

                continue;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterString"></param>
        public override void FilterUsername(bool justCompleted)
        {
            IConfig config = ConfigFactory.CreateConfig();
            string filterString = config.Username;
            foreach (AutomationJobViewModel job in this.Jobs)
            {
                if (justCompleted && job.Job.State != JobState.Completed)
                {
                    job.IsVisible = false;
                    continue;
                }
                else
                {
                    if (string.IsNullOrEmpty(filterString))
                    {
                        job.IsVisible = true;
                        continue;
                    }
                    else
                    {
                        if (job.Job.Trigger is IChangelistTrigger)
                        {
                            IChangelistTrigger trigger = job.Job.Trigger as IChangelistTrigger;
                            if (trigger.Username == null || !trigger.Username.Contains(filterString))
                            {
                                job.IsVisible = false;
                            }
                            else
                            {
                                job.IsVisible = true;
                            }

                            continue;
                        }
                        else if (job.Job.Trigger is UserRequestTrigger)
                        {
                            UserRequestTrigger trigger = job.Job.Trigger as UserRequestTrigger;
                            if (trigger.Username == null || !trigger.Username.Contains(filterString))
                            {
                                job.IsVisible = false;
                            }
                            else
                            {
                                job.IsVisible = true;
                            }

                            continue;
                        }
                    }

                    if (string.IsNullOrEmpty(filterString))
                    {
                        job.IsVisible = true;
                    }
                    else
                    {
                        job.IsVisible = false;
                    }
                }
            }
        }
        #endregion
    }

} // Workbench.AddIn.Automation.UI namespace
