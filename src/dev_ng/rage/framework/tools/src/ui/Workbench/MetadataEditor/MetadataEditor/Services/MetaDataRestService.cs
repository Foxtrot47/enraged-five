﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using RSG.Base.ConfigParser;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Platform;
using Workbench.AddIn;
using Workbench.AddIn.REST;
using Workbench.AddIn.Services.File;
using MetadataEditor.RestData;
using Workbench.AddIn.Services;
using Ionic.Zip;

namespace MetadataEditor.Services
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Workbench.AddIn.REST.ExtensionPoints.RESTService, typeof(IRestService))]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class MetaDataRestService : IMetaDataRestService
    {
        #region MEF Imports
        /// <summary>
        /// Workbench open service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.WorkbenchOpenService, typeof(IWorkbenchOpenService))]
        private Lazy<IWorkbenchOpenService> WorkbenchOpenService { get; set; }

        /// <summary>
        /// Metadata open service
        /// </summary>
        [ImportExtension(MetadataEditor.AddIn.CompositionPoints.MetadataOpenService, typeof(IOpenService))]
        private Lazy<IOpenService> MetadataOpenService { get; set; }

        /// <summary>
        /// Configuration service
        /// </summary>
        [ImportExtension(CompositionPoints.ConfigurationService, typeof(Workbench.AddIn.Services.IConfigurationService))]
        private IConfigurationService Config { get; set; }

        /// <summary>
        /// MEF import for message box service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.MessageService, typeof(Workbench.AddIn.Services.IMessageService))]
        private IMessageService MessageService { get; set; }

        /// <summary>
        /// MEF import for perforce service.
        /// </summary>
        [ImportExtension(PerforceBrowser.AddIn.CompositionPoints.PerforceService, typeof(PerforceBrowser.AddIn.IPerforceService))]
        public PerforceBrowser.AddIn.IPerforceService PerforceService { get; set; }

        /// <summary>
        /// MEF import for perforce service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.PerforceSyncService, typeof(Workbench.AddIn.Services.IPerforceSyncService))]
        public Workbench.AddIn.Services.IPerforceSyncService PerforceSyncService { get; set; }
        #endregion

        #region IRestService Implementation
        /// <summary>
        /// Location at which to attach this REST service to the web host
        /// </summary>
        public string EndPointName
        {
            get
            {
                return "MetaDataService-1.0";
            }
        }
        #endregion

        #region IMetaDataRestService Implementation
        /// <summary>
        /// Opens up the requested file
        /// </summary>
        /// <param name="file"></param>
        public void OpenFile(string file)
        {
            ExecuteOnBackgroundThread(() =>
                {
                    PromptToCheckout(new List<string>(new string[] { file }));
                    WorkbenchOpenService.Value.OpenFileWithService(MetadataOpenService.Value, file);
                });
        }

        /// <summary>
        /// Opens up the requested file(s)
        /// </summary>
        /// <param name="file"></param>
        public void OpenFiles(FileList fileList)
        {
            ExecuteOnBackgroundThread(() =>
                {
                    PromptToCheckout(fileList);
                    WorkbenchOpenService.Value.OpenFileWithService(MetadataOpenService.Value, fileList);
                });
        }

        /// <summary>
        /// Attempts to figure out what file we should open based on information we received from RAG
        /// </summary>
        public void OpenFileFromGame(MetadataInfo info)
        {
            ExecuteOnBackgroundThread(() =>
                {
                    string tcsPath = null;

                    // We have two different cases to cater for now
                    // 1) Special case where the texture name has 'platform:' in it and we don't receive a RPF filename
                    // 2) Default case where we get a texture name, RPF file, asset type and asset name
                    if (info.RPFFilename.Length == 0 && info.AssetName.StartsWith("platform:"))
                    {
                        String platformZipFilename = info.AssetName.Replace("platform:", Config.GameConfig.ExportDir) + "." + info.AssetType.GetExportExtension() + ".zip";
                        EnsureFilesAreSynced(new String[] { platformZipFilename });
                        tcsPath = GetTcsNameFromPlatformFile(info, platformZipFilename);
                    }
                    else
                    {
                        IList<String> zipFilenames = GetZipFilenames(info);
                        EnsureFilesAreSynced(zipFilenames);
                        tcsPath = GetTcsNameFromAsset(info, zipFilenames);
                        if (!System.IO.File.Exists(tcsPath))
                        {
                            MessageService.Show(String.Format("Unable to open tcs file for the following texture: {0}.\n" +
                                "The tcs file has been located at the location {1} however, this doesn't seem to be in your local directory." +
                                "Please grab latest of this file through perforce and try again", info.TextureName, tcsPath));

                            return;
                        }
                    }

                    // Attempt to open the file if we successfully managed to determine the tcs path
                    if (!String.IsNullOrEmpty(tcsPath))
                    {
                        PromptToCheckout(new List<string>(new string[] { tcsPath }));
                        WorkbenchOpenService.Value.OpenFileWithService(MetadataOpenService.Value, tcsPath, info);
                    }
                    else
                    {
                        // Inform the user that we weren't able to figure out which file to open
                        MessageService.Show(String.Format("Unable to open tcs file for the following texture: {0}.\n" + 
                            "This could be because you haven't got latest on the exported zip file, the parent txd file, or the exported parent txd zip file, or " +
                            "because you've selected an unsupported containing asset type via the texture viewer (only drawables/txds are currently supported).", info.TextureName));
                    }
                });
        }
        #endregion // IMetadataRestService Interface

        #region Private Methods
        /// <summary>
        /// Helper function for executing an action on a background thread (this is to allow rest queries to return immediately)
        /// </summary>
        /// <param name="action"></param>
        private void ExecuteOnBackgroundThread(Action action)
        {
            BackgroundWorker bgw = new BackgroundWorker();
            bgw.DoWork += new DoWorkEventHandler(BackgroundWorker_DoWork);
            bgw.RunWorkerAsync(action);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void BackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            Action action = (Action)e.Argument;
            action.Invoke();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        private string GetTcsNameFromPlatformFile(MetadataInfo info, String platformZipFilename)
        {
            Log.Log__Message("Determining tcs filename from platform zip file.");
            string tcsPath = null;

            try
            {
                String tclname = info.TextureName + ".tcl";

                using (ZipFile zipFile = ZipFile.Read(platformZipFilename))
                {
                    MemoryStream tclStream = null;
                    if (zipFile.ContainsEntry(tclname))
                    {
                        tclStream = new MemoryStream();
                        zipFile[tclname].Extract(tclStream);
                        tcsPath = RSG.Model.Map3.Util.Metadata.GetTcsFilenameFromTclStream(tclStream, Config.GameConfig);
                    }
                    else
                    {
                        Log.Log__Warning("Tcl file wasn't found in the platform zip file ({0}).", platformZipFilename);
                    }
                }
            }
            catch (Exception e)
            {
                Log.Log__Exception(e, "Error occurred while attempting to process a zip file.");
            }

            return tcsPath;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="contentName"></param>
        /// <returns></returns>
        private IList<String> GetZipFilenames(MetadataInfo info)
        {
            IList<String> zipFiles = new List<String>();

            // I assume here that the content node name is the same as the RPF file name minus its extension
            string contentName = Path.GetFileNameWithoutExtension(info.RPFFilename).ToLower();

            foreach (ContentNode zipNode in GetContentNodes(contentName))
            {
                if (zipNode is ContentNodeFile)
                {
                    zipFiles.Add(Path.Combine((zipNode as ContentNodeFile).Path, zipNode.Name + ".zip"));
                }
            }

            return zipFiles;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rpfName"></param>
        /// <returns></returns>
        private IList<ContentNode> GetContentNodes(String contentName)
        {
            IList<ContentNode> zipNodes = new List<ContentNode>();

            // Get all processed map zip nodes for this item
            IList<ContentNode> processedMapZipNodes = Config.GameConfig.Content.Root.FindAll(contentName, "processedmapzip");

            // If we didn't find any then checked for "normal" non-processed map zip nodes
            if (!processedMapZipNodes.Any())
            {
                Log.Log__Message("No processed map zip files found for {0}.  Falling back on export map zip files.", contentName);
                zipNodes = Config.GameConfig.Content.Root.FindAll(contentName, "mapzip");

                // If we still didn't find any, we need to look for a zip node of the name
                // This is a special case for shared textures such as gtxd
                if (zipNodes.Count == 0)
                {
                    Log.Log__Message("No export map zip files found for {0}.  Falling back on zip files.", contentName);
                    zipNodes = Config.GameConfig.Content.Root.FindAll(contentName, "zip");
                }
            }
            else
            {
                // We should only have one (otherwise there is an error in the content tree)
                ContentNode processedNode = processedMapZipNodes.First();
                zipNodes = processedNode.Inputs.ToList();
            }

            return zipNodes;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="zipFilenames"></param>
        private void EnsureFilesAreSynced(IList<String> zipFilenames)
        {
            if (zipFilenames.Any())
            {
                IList<String> filenames = new List<String>();
                filenames.AddRange(zipFilenames.Select(item => Path.GetFullPath(item)));
                filenames.Add(Path.GetFullPath(Path.Combine(Config.GameConfig.AssetsDir, "maps", "ParentTxds.xml")));

                int syncedFiles = 0;
                PerforceSyncService.Show("Some of the files used to determine where the tcs file is located are out of date.\n" +
                                         "Would you like to grab latest on them now?",
                                         filenames.ToArray(), ref syncedFiles, false, true);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        private string GetTcsNameFromAsset(MetadataInfo info, IEnumerable<String> zipFilenames)
        {
#warning DHM FIX ME: need to migrate to the AP3 content-tree
            Log.Log__Message("Determining tcs filename from asset information/content tree zip files.");

            // Try and get the tcs path from the map zip nodes we found
            string txdName = null;
            string drawableName = null;

            switch (info.AssetType)
            {
                case FileType.Drawable:
                    drawableName = info.AssetName;
                    break;

                case FileType.TextureDictionary:
                    txdName = info.AssetName;
                    break;

                default:
                    throw new ArgumentException("Unsupported type encountered while attempting to open a file from a RAG query.");
            }

            Log.Log__Message("Searching through {0} zip file(s) for the {1} texture in the {2} {3} zip.", zipFilenames.Count(), info.TextureName, info.AssetName, info.AssetTypeString);

            string tcsPath = null;
            foreach (String zipFilename in zipFilenames)
            {
                Log.Log__Message("Searching through {0}.", zipFilename);
                tcsPath = RSG.Model.Map3.Util.Metadata.GetTcsFilename(info.TextureName, zipFilename, drawableName, txdName, Config.GameConfig, false);
                if (!String.IsNullOrEmpty(tcsPath))
                {
                    break;
                }
            }

            return tcsPath;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePaths"></param>
        private void PromptToCheckout(IEnumerable<string> filePaths)
        {
            RSG.SourceControl.Perforce.P4 p4 = PerforceService.PerforceConnection;
            if (p4 != null)
            {
                bool filesNeedCheckingOut = false;

                try
                {
                    foreach (String filePath in filePaths)
                    {
                        P4API.P4RecordSet results = p4.Run("fstat", filePath);

                        if (results.Records.Length == 1)
                        {
                            if (!results.Records[0].Fields.ContainsKey("action") ||
                                results.Records[0].Fields["action"] != "edit")
                            {
                                filesNeedCheckingOut = true;
                                break;
                            }
                        }
                    }
                }
                catch (System.Exception)
                {	
                }

                if (filesNeedCheckingOut)
                {
                    System.Windows.MessageBoxResult r = MessageService.Show("Do you want to automatically check out the tcs file(s) into the default changelist?", System.Windows.MessageBoxButton.YesNo, System.Windows.MessageBoxImage.Question);
                    if (r == System.Windows.MessageBoxResult.Yes)
                    {
                        String currDir = System.IO.Directory.GetCurrentDirectory();

                        try
                        {
                            foreach (String filePath in filePaths)
                            {
                                p4.Run("edit", filePath);
                            }
                        }
                        catch (P4API.Exceptions.P4APIExceptions ex)
                        {
                            this.MessageService.Show("Error checking out tcs file, it will remain read only.", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                            RSG.Base.Logging.Log.Log__Exception(ex, "Unhandled Perforce exception while trying to edit one of the files in {0}.", String.Join(", ", filePaths.ToArray()));
                        }
                        finally
                        {
                            System.IO.Directory.SetCurrentDirectory(currDir);
                        }
                    }
                }
            }
        }
        #endregion // Private Methods
    } // MetaDataRestService
} // MetadataEditor.Services
