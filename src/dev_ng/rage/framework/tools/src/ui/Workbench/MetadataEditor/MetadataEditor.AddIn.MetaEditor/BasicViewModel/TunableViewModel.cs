﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using RSG.Base.Editor;
using RSG.Metadata.Data;
using RSG.Metadata.Parser;
using RSG.Base.Editor.Command;
using MetadataEditor.AddIn.MetaEditor.View;
using MetadataEditor.AddIn.MetaEditor.ViewModel;

namespace MetadataEditor.AddIn.MetaEditor.BasicViewModel
{
    /// <summary>
    /// ViewModel for the ITunable series of classes.
    /// </summary>
    public class TunableViewModel : 
        ViewModelBase,
        ITunableViewModel
    {
        #region Fields
        private ITunable m_rawModel;
        #endregion

        #region Properties
        /// <summary>
        /// IsExpanded property for this tunable being expanded.
        /// </summary>
        public bool IsExpanded
        {
            get { return m_bIsExpanded; }
            set
            {
                SetPropertyValue(value, m_bIsExpanded, () => this.IsExpanded,
                   new PropertySetDelegate(delegate(Object newValue) 
                       {
                           bool oldValue = m_bIsExpanded;
                           m_bIsExpanded = (bool)newValue;
                           if (m_bIsExpanded)
                           {
                               if (this.Parent != null)
                                   this.Parent.IsExpanded = true;
                           }
                           if (m_bIsExpanded && oldValue == false)
                           {
                               if (this is HierarchicalTunableViewModel)
                               {
                                   HierarchicalTunableViewModel vm = this as HierarchicalTunableViewModel;
                                   int index = this.RootViewModel.AllTunableViewModels.IndexOf(this);
                                   if (index != -1)
                                   {
                                       foreach (ITunableViewModel child in vm.Members.Reverse())
                                       {
                                           this.RootViewModel.AllTunableViewModels.Insert(index + 1, child);
                                       }
                                   }
                               }
                           }
                           else if (!m_bIsExpanded && oldValue == true)
                           {
                               if (this is HierarchicalTunableViewModel)
                               {
                                   HierarchicalTunableViewModel vm = this as HierarchicalTunableViewModel;
                                   foreach (ITunableViewModel child in vm.Members)
                                   {
                                       this.RootViewModel.AllTunableViewModels.Remove(child);
                                       child.IsExpanded = false;
                                       child.IsSelected = false;
                                   }
                               }
                           }
                       }));
            }
        }
        private bool m_bIsExpanded = false;

        /// <summary>
        /// IsExpanded property for this tunable being expanded.
        /// </summary>
        public new bool IsSelected
        {
            get { return m_bIsSelected; }
            set
            {
                SetPropertyValue(value, m_bIsSelected, () => this.IsSelected,
                   new PropertySetDelegate(delegate(Object newValue)
                       {
                           bool oldValue = m_bIsSelected;
                           m_bIsSelected = (bool)newValue;
                           if (m_bIsSelected)
                           {
                               if (this.Parent != null)
                                   this.Parent.IsExpanded = true;

                               if (oldValue == false)
                               {
                                   if (this.RootViewModel != null)
                                       this.RootViewModel.OnItemSelected(this);
                               }
                           }
                       }
                   ));
            }
        }
        private bool m_bIsSelected = false;

        /// <summary>
        /// 
        /// </summary>
        public bool HasItems
        {
            get { return m_hasItems; }
            set
            {
                SetPropertyValue(value, m_hasItems, () => this.HasItems,
                   new PropertySetDelegate(delegate(Object newValue)
                   {
                       m_hasItems = (bool)newValue;
                   }
                   ));
            }
        }
        private bool m_hasItems = false;

        /// <summary>
        /// 
        /// </summary>
        public Object Key { get; protected set; }

        /// <summary>
        /// Reference to the Tunable model this represents.
        /// </summary>
        public ITunable Model 
        { 
            get
            {
                if (m_rawModel is MapItemTunable)
                    return (m_rawModel as MapItemTunable).Value;

                return m_rawModel;
            }      
        }

        /// <summary>
        /// Reference to the Tunable model this represents.
        /// </summary>
        public ITunable RawModel
        {
            get
            {
                return m_rawModel;
            }

        }

        /// <summary>
        /// Reference to the parent Tunable ViewModel (for hierarchy).
        /// </summary>
        public ITunableViewModel Parent { get; protected set; }

        public String Name { get; set; }

        public String FriendlyTypeName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Boolean? InheritParent
        {
            get { return this.Model.InheritParent; }
            set
            {
                if (value == this.Model.InheritParent)
                    return;

                this.Model.SetInheritParent(value);
                foreach (CommandViewModel command in this.Commands)
                {
                    command.RefreshIsEnabled();
                }
            }
        }

        /// <summary>
        /// Returns a read-only list of commands 
        /// that the UI can display and execute.
        /// </summary>
        public ReadOnlyCollection<CommandViewModel> Commands
        {
            get
            {
                if (_commands == null)
                {
                    List<CommandViewModel> cmds = this.CreateCommands();
                    _commands = new ReadOnlyCollection<CommandViewModel>(cmds);
                }
                return _commands;
            }
        }
        ReadOnlyCollection<CommandViewModel> _commands;


        public RelayCommand StringRefreshCommand
        {
            get
            {
                if (m_stringRefreshCommand == null)
                    m_stringRefreshCommand = new RelayCommand(param => this.RefreshStringPossibleValues(param));

                return m_stringRefreshCommand;
            }
        }
        private RelayCommand m_stringRefreshCommand;
        /// <summary>
        /// Reference to the root viewmodel for the tunable file.
        /// </summary>
        public MetaFileViewModel RootViewModel { get; set; }

        public Boolean BringIntoViewOnSelect
        {
            get;
            set;
        }
        
        public int IndentLevel
        {
            get
            {
                ITunableViewModel parent = this.Parent;
                int level = 0;
                while (parent != null)
                {
                    level++;
                    parent = parent.Parent;
                }
                return level * 20;
            }
        }

        public bool IsMapItem
        {
            get
            {
                return m_rawModel is MapItemTunable;
            }
        }

        public bool IsIntegerMapItem
        {
            get
            {
                if (m_rawModel is MapItemTunable)
                {
                    return (m_rawModel as MapItemTunable).IsIntegerKeyed;
                }
                return false;
            }
        }

        public bool IsStringMapItem
        {
            get
            {
                if (m_rawModel is MapItemTunable)
                {
                    return !(m_rawModel as MapItemTunable).IsIntegerKeyed;
                }
                return false;
            }
        }

        protected System.Collections.ObjectModel.ObservableCollection<ITunableViewModel> m_PreviousMembers;
        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="parent"></param>
        public TunableViewModel(ITunableViewModel parent, ITunable m, MetaFileViewModel root)
        {
            this.Parent = parent;
            this.RootViewModel = root;
            this.m_rawModel = m;
            this.Name = this.Model.Name;
            this.FriendlyTypeName = this.Model.Definition.FriendlyTypeName;
            this.Key = this.Model.Name;

            if (!string.IsNullOrWhiteSpace(this.Model.Definition.FriendlyUIName))
            {
                this.Key = this.Model.Definition.FriendlyUIName;
                this.Name = this.Model.Definition.FriendlyUIName;
            }
            m_PreviousMembers = new ObservableCollection<ITunableViewModel>();

            if (m.Definition is StringMember)
            {
                StringMember stringMember = m.Definition as StringMember;
                if (stringMember.SourceType == StringMember.StringSourceType.Directory)
                {
                    stringMember.CreateDirectoryPossibleValues(root.Branch);
                }
                else if (stringMember.SourceType == StringMember.StringSourceType.ParentMap)
                {
                    SetPossibleValuesForStringFromMapParent(stringMember);
                }
            }
        }

        #endregion // Constructor(s)

        #region Commands
        private void SetPossibleValuesForStringFromMapParent(StringMember stringMember)
        {
            ITunableViewModel parentMapViewModel = this.Parent;
            string mapName = stringMember.GetParentMapNameForValues();
            while (parentMapViewModel != null)
            {
                if (string.CompareOrdinal(parentMapViewModel.Name, mapName) == 0)
                {
                    if (parentMapViewModel is MapTunableViewModel)
                    {
                        break;
                    }
                }

                parentMapViewModel = parentMapViewModel.Parent;
            }

            if (parentMapViewModel == null)
            {
                return;
            }

            MapTunable map = parentMapViewModel.Model as MapTunable;
            if (map == null)
            {
                return;
            }

            List<string> names = new List<string>();
            foreach (MapItemTunable item in map.Items)
            {
                if (!stringMember.PossibleValues.Contains(item.Key))
                {
                    stringMember.PossibleValues.Add(item.Key);
                    names.Add(item.Key);
                }
            }

            List<string> removeList = new List<string>();
            foreach (var value in stringMember.PossibleValues)
            {
                if (!names.Contains(value))
                    removeList.Add(value);
            }

            foreach (var value in removeList)
            {
                stringMember.PossibleValues.Remove(value);
            }

            if (stringMember.DoUIValuesIncludeEmpty())
            {
                if (!stringMember.PossibleValues.Contains(string.Empty))
                {
                    stringMember.PossibleValues.Insert(0, string.Empty);
                }
            }
        }

        private List<CommandViewModel> CreateCommands()
        {
            CommandViewModel addCommand = new CommandViewModel("Add New Element", null, new RelayCommand());

            CommandViewModel removeSelectionCommand = new CommandViewModel("Remove Selected Element(s)", null,
                new RelayCommand(param => this.RequestRemoveElementsCommand(), param => CanRemoveElementsCommand()));

            CommandViewModel insertCommand = new CommandViewModel("Insert New Element", null, new RelayCommand());
            
            CommandViewModel duplicateCommand = new CommandViewModel("Duplicate Selected Element(s)", null,
                new RelayCommand(param => this.RequestDuplicateCommand(param), param => this.CanDuplicateCommand()));

            CommandViewModel moveUpCommand = new CommandViewModel("Move Element Up", null,
                new RelayCommand(param => this.RequestMoveElementUpCommand(param), param => CanMoveElementUpCommand(param)));

            CommandViewModel moveDownCommand = new CommandViewModel("Move Element Down", null,
                new RelayCommand(param => this.RequestMoveElementDownCommand(param), param => CanMoveElementDownCommand(param)));

            CommandViewModel changePointerCommand = new CommandViewModel("Change Pointer Type To...", null, new RelayCommand());

            CommandViewModel expandAllCommand = new CommandViewModel("Expand All", null,
                new RelayCommand(param => this.RequestExpandAllCommand(param), param => CanExpandAllCommand(param)));

            CommandViewModel collapseCommand = new CommandViewModel("Collapse All", null,
                new RelayCommand(param => this.RequestCollapseAllCommand(param), param => CanCollapseAllCommand(param)));

            if (this is ArrayTunableViewModel || this is MapTunableViewModel)
            {
                foreach (CommandViewModel child in this.ChildElementCommands())
                {
                    child.Command._execute = param => this.RequestAddCommand(param);
                    child.Command._canExecute = param => this.CanAddCommand();
                    addCommand.Children.Add(child);
                }
            }

            if (this.Parent is ArrayTunableViewModel || this.Parent is MapTunableViewModel)
            {
                var childElements = this.ChildElementCommands();
                if (childElements.Count == 1)
                {
                    insertCommand = new CommandViewModel("Insert New Element", childElements[0].CommandTag,
                        new RelayCommand(param => this.RequestInsertCommand(param), param => this.CanInsertCommand()));
                }
                else
                {
                    foreach (CommandViewModel child in this.ChildElementCommands())
                    {
                        child.Command._execute = param => this.RequestInsertCommand(param);
                        child.Command._canExecute = param => this.CanInsertCommand();
                        insertCommand.Children.Add(child);
                    }
                }
            }

            if (this.Model.ParentModel is PointerTunable)
            {
                foreach (CommandViewModel child in this.ChildElementCommands())
                {
                    child.Command._execute = param => this.RequestChangeCommand(param);
                    child.Command._canExecute = param => this.CanChangeCommand();
                    changePointerCommand.Children.Add(child);
                }
            }

            return new List<CommandViewModel>
            {
                addCommand,
                insertCommand,
                duplicateCommand,
                removeSelectionCommand,
                moveUpCommand,
                moveDownCommand,
                changePointerCommand,
                expandAllCommand,
                collapseCommand
            };
        }

        protected virtual List<CommandViewModel> ChildElementCommands()
        {
            if (this.Parent is ArrayTunableViewModel)
            {
                if ((this.Parent.Model.Definition as RSG.Metadata.Parser.ArrayMember).ElementType is RSG.Metadata.Parser.PointerMember)
                {
                    List<CommandViewModel> children = new List<CommandViewModel>();
                    Structure objectType = ((this.Parent.Model.Definition as RSG.Metadata.Parser.ArrayMember).ElementType as RSG.Metadata.Parser.PointerMember).ObjectType;
                    foreach (Structure structure in GetChildren(objectType, true, true))
                    {
                        if (structure.Constuctable == true)
                        {
                            children.Add(
                                new CommandViewModel(
                                    String.Format("{0}", structure.DataType),
                                    new StructMember(structure, this.Model.Definition.ParentStructure),
                                    new RelayCommand())
                            );
                        }
                    }
                    return children;
                }
                else
                {
                    return new List<CommandViewModel>
                    {
                        new CommandViewModel(
                            String.Format("{0}", (this.Parent.Model.Definition as RSG.Metadata.Parser.ArrayMember).ElementType.FriendlyTypeName), 
                            this.Model.Definition,
                            new RelayCommand())     
                    };
                }
            }
            else if (this.Parent is MapTunableViewModel)
            {
                if ((this.Parent.Model.Definition as RSG.Metadata.Parser.MapMember).ElementType is RSG.Metadata.Parser.PointerMember)
                {
                    List<CommandViewModel> children = new List<CommandViewModel>();
                    Structure objectType = ((this.Parent.Model.Definition as RSG.Metadata.Parser.MapMember).ElementType as RSG.Metadata.Parser.PointerMember).ObjectType;
                    foreach (Structure structure in GetChildren(objectType, true, true))
                    {
                        if (structure.Constuctable == true)
                        {
                            children.Add(
                                new CommandViewModel(
                                    String.Format("{0}", structure.DataType),
                                    new StructMember(structure, this.Model.Definition.ParentStructure),
                                    new RelayCommand())
                            );
                        }
                    }
                    return children;
                }
                else
                {
                    return new List<CommandViewModel>
                    {
                        new CommandViewModel(
                            String.Format("{0}", (this.Parent.Model.Definition as RSG.Metadata.Parser.MapMember).ElementType.FriendlyTypeName), 
                            this.Model.Definition,
                            new RelayCommand())
                    };
                }
            }

            return new List<CommandViewModel>
            {
            };
        }

        private IEnumerable<Structure> GetChildren(Structure root, Boolean includeSelf, Boolean recursive)
        {
            if (includeSelf == true)
                yield return root;

            foreach (Structure child in root.ImmediateDescendants)
            {
                yield return child;
                if (recursive == true)
                {
                    foreach (Structure grandChild in GetChildren(child, false, true))
                    {
                        yield return grandChild;
                    }
                }
            }
        }

        #region RequestAddCommand

        protected virtual void RequestAddCommand(Object parameter)
        {
        }

        protected virtual bool CanAddCommand()
        {
            if (!this.Model.Definition.CanLiveEdit && this.RootViewModel.IsLiveEditing)
            {
                return false;
            }

            if (this.Model.InheritParent == true)
            {
                return false;
            }
            return true;
        }

        #endregion // RequestAddCommand

        #region RemoveCommand

        protected virtual void RequestRemoveElementsCommand()
        {
            if (RootViewModel.SelectedItems[0].Parent is ArrayTunableViewModel)
            {
                ArrayTunableViewModel parent = RootViewModel.SelectedItems[0].Parent as ArrayTunableViewModel;
                SortedList<int, ITunableViewModel> removeList = new SortedList<int, ITunableViewModel>();
                foreach (ITunableViewModel vm in RootViewModel.SelectedItems)
                    removeList.Add(parent.Members.IndexOf(vm), vm);

                using (BatchUndoRedoBlock batch = new BatchUndoRedoBlock(parent.Model))
                {
                    foreach (ITunableViewModel vm in removeList.Values.Reverse())
                    {
                        int index = parent.Members.IndexOf(vm);
                        parent.RemoveElement(index);
                    }
                }
            }
            else if (RootViewModel.SelectedItems[0].Parent is MapTunableViewModel)
            {
                MapTunableViewModel parent = RootViewModel.SelectedItems[0].Parent as MapTunableViewModel;
                SortedList<int, ITunableViewModel> removeList = new SortedList<int, ITunableViewModel>();
                foreach (ITunableViewModel vm in RootViewModel.SelectedItems)
                    removeList.Add(parent.Members.IndexOf(vm), vm);

                using (BatchUndoRedoBlock batch = new BatchUndoRedoBlock(parent.Model))
                {
                    foreach (ITunableViewModel vm in removeList.Values.Reverse())
                    {
                        parent.RemoveElement(parent.Members.IndexOf(vm));
                    }
                }
            }
        }

        protected virtual bool CanRemoveElementsCommand()
        {
            if (this.RootViewModel.SelectedItems != null && RootViewModel.SelectedItems.Count >= 1)
            {
                if (RootViewModel.SelectedItems[0].Parent is ArrayTunableViewModel)
                {
                    ArrayTunableViewModel parent = RootViewModel.SelectedItems[0].Parent as ArrayTunableViewModel;
                    if (!parent.Model.Definition.CanLiveEdit && this.RootViewModel.IsLiveEditing)
                    {
                        return false;
                    }

                    foreach (ITunableViewModel vm in RootViewModel.SelectedItems)
                    {
                        if (vm.Parent != parent)
                            return false;
                    }
                    return true;
                }
                else if (RootViewModel.SelectedItems[0].Parent is MapTunableViewModel)
                {
                    MapTunableViewModel parent = RootViewModel.SelectedItems[0].Parent as MapTunableViewModel;
                    if (!parent.Model.Definition.CanLiveEdit && this.RootViewModel.IsLiveEditing)
                    {
                        return false;
                    }

                    foreach (ITunableViewModel vm in RootViewModel.SelectedItems)
                    {
                        if (vm.Parent != parent)
                            return false;
                    }
                    return true;
                }
            }
            return false;
        }

        #endregion

        #region InsertCommand

        protected virtual void RequestInsertCommand(Object parameter)
        {
            if (this.Parent is ArrayTunableViewModel)
            {
                (this.Parent as ArrayTunableViewModel).AddElement((this.Parent as ArrayTunableViewModel).Members.IndexOf(this), parameter as IMember);
            }
            else if (this.Parent is MapTunableViewModel)
            {
                (this.Parent as MapTunableViewModel).AddElement((this.Parent as MapTunableViewModel).Members.IndexOf(this), parameter as IMember);
            }
        }

        protected virtual bool CanInsertCommand()
        {
            return CanAddOrRemoveToParent();
        }

        #endregion

        #region DuplicateCommand

        private int duplicateStartIndex = 0;

        internal virtual void RequestDuplicateCommand(Object parameter)
        {
            var selectedItems = parameter is List<ITunableViewModel> ? parameter as List<ITunableViewModel> : RootViewModel.SelectedItems;

            if (this.Parent is ArrayTunableViewModel)
            {
                ArrayTunableViewModel parent = this.Parent as ArrayTunableViewModel;
                if (parent == null)
                    return;

                SortedList<int, ITunableViewModel> duplicateList = new SortedList<int, ITunableViewModel>();
                foreach (ITunableViewModel vm in selectedItems)
                    duplicateList.Add(parent.Members.IndexOf(vm), vm);

                using (BatchUndoRedoBlock block = new BatchUndoRedoBlock(CommandManager.CommandManagerForModel(this.RootViewModel.Model)))
                {
                    int index = duplicateStartIndex;
                    foreach (ITunableViewModel vm in duplicateList.Values)
                    {
                        parent.AddElement(index, vm.Model.Definition, vm.Model);
                        index++;
                    }
                    parent.IsExpanded = true;
                    for (int i = duplicateStartIndex; i < duplicateStartIndex + duplicateList.Count; i++)
                    {
                        if (parent.Members[i] is HierarchicalTunableViewModel)
                        {
                            (parent.Members[i] as HierarchicalTunableViewModel).IsExpanded = true;
                        }
                    }
                }
            }
            else if (this.Parent is MapTunableViewModel)
            {
                MapTunableViewModel parent = this.Parent as MapTunableViewModel;
                if (parent == null)
                    return;

                SortedList<int, ITunableViewModel> duplicateList = new SortedList<int, ITunableViewModel>();
                foreach (ITunableViewModel vm in selectedItems)
                {
                    if (vm is TunableViewModel)
                        duplicateList.Add(parent.Members.IndexOf(vm), vm);
                }
                if (duplicateList.Count == 0)
                    return;

                using (BatchUndoRedoBlock block = new BatchUndoRedoBlock(CommandManager.CommandManagerForModel(this.RootViewModel.Model)))
                {
                    int index = duplicateStartIndex;
                    foreach (TunableViewModel vm in duplicateList.Values)
                    {
                        parent.AddElement(index, vm.Model.Definition, vm.RawModel);
                        index++;
                    }
                    parent.IsExpanded = true;
                    for (int i = duplicateStartIndex; i < duplicateStartIndex + duplicateList.Count; i++)
                    {
                        if (parent.Members[i] is HierarchicalTunableViewModel)
                        {
                            (parent.Members[i] as HierarchicalTunableViewModel).IsExpanded = true;
                        }
                    }
                }
            }
        }

        protected virtual bool CanDuplicateCommand()
        {
            if (!CanAddOrRemoveToParent())
                return false;

            duplicateStartIndex = -1;
            if (this.RootViewModel.SelectedItems == null || RootViewModel.SelectedItems.Count < 1)
                return false;

            // Selected items need to be next to each other in the same list.
            if (RootViewModel.SelectedItems[0].Parent is HierarchicalTunableViewModel)
            {
                HierarchicalTunableViewModel parent = RootViewModel.SelectedItems[0].Parent as HierarchicalTunableViewModel;
                SortedList<int, ITunableViewModel> sorted = new SortedList<int, ITunableViewModel>();
                int count = 0;
                foreach (ITunableViewModel vm in RootViewModel.SelectedItems)
                {
                    if (vm.Parent != parent)
                        return false;
                    int index = parent.Members.IndexOf(vm);
                    if (index == -1)
                        return false;

                    if (sorted.ContainsKey(index))
                        return false;

                    sorted.Add(index, vm);
                    count++;
                    if (count == RootViewModel.SelectedItems.Count)
                    {
                        duplicateStartIndex = index + 1;
                    }
                }

                int previousIndex = -1;
                foreach (KeyValuePair<int, ITunableViewModel> kvp in sorted)
                {
                    if (previousIndex != -1)
                    {
                        if (kvp.Key != previousIndex + 1)
                            return false;
                    }
                    previousIndex = kvp.Key;
                }

                return true;
            }

            return false;
        }

        #endregion

        #region MoveElementUpCommand

        protected virtual void RequestMoveElementUpCommand(Object parameter)
        {
            if (this.Parent is ArrayTunableViewModel)
            {
                int oldIndex = (this.Parent as ArrayTunableViewModel).Members.IndexOf(this);
                int newIndex = oldIndex--;
                ((this.Parent as ArrayTunableViewModel).Model as ArrayTunable).Move(oldIndex, newIndex);
            }
            else if (this.Parent is MapTunableViewModel)
            {
                int oldIndex = (this.Parent as MapTunableViewModel).Members.IndexOf(this);
                int newIndex = oldIndex--;
                ((this.Parent as MapTunableViewModel).Model as MapTunable).Move(oldIndex, newIndex);
            }
        }

        protected virtual bool CanMoveElementUpCommand(Object parameter)
        {
            if (this.Model == null || this.Model.Definition == null)
                return false;



            if (this.Model.InheritParent == true || this.Parent== null)
            {
                return false;
            }

            if (!this.Parent.Model.Definition.CanLiveEdit && this.RootViewModel.IsLiveEditing)
            {
                return false;
            }

            if (this.Parent is ArrayTunableViewModel)
            {
                if ((this.Parent as ArrayTunableViewModel).Members.IndexOf(this) > 0)
                    return true;
            }
            else if (this.Parent is MapTunableViewModel)
            {
                if ((this.Parent as MapTunableViewModel).Members.IndexOf(this) > 0)
                    return true;
            }

            return false;
        }

        #endregion // MoveElementUpCommand

        #region MoveElementDownCommand

        protected virtual void RequestMoveElementDownCommand(Object parameter)
        {
            if (this.Parent is ArrayTunableViewModel)
            {
                int oldIndex = (this.Parent as ArrayTunableViewModel).Members.IndexOf(this);
                int newIndex = oldIndex++;
                ((this.Parent as ArrayTunableViewModel).Model as ArrayTunable).Move(oldIndex, newIndex);
            }
            else if (this.Parent is MapTunableViewModel)
            {
                int oldIndex = (this.Parent as MapTunableViewModel).Members.IndexOf(this);
                int newIndex = oldIndex++;
                ((this.Parent as MapTunableViewModel).Model as MapTunable).Move(oldIndex, newIndex);
            }
        }

        protected virtual bool CanMoveElementDownCommand(Object parameter)
        {
            if (this.Model == null || this.Model.Definition == null || this.Parent == null)
                return false;

            if (!this.Parent.Model.Definition.CanLiveEdit && this.RootViewModel.IsLiveEditing)
            {
                return false;
            }

            if (this.Model.InheritParent == true)
            {
                return false;
            }
            if (this.Parent is ArrayTunableViewModel)
            {
                if ((this.Parent as ArrayTunableViewModel).Members.IndexOf(this) < (this.Parent as ArrayTunableViewModel).Members.Count - 1)
                    return true;
            }
            else if (this.Parent is MapTunableViewModel)
            {
                if ((this.Parent as MapTunableViewModel).Members.IndexOf(this) < (this.Parent as MapTunableViewModel).Members.Count - 1)
                    return true;
            }
            return false;
        }

        #endregion // MoveElementDownCommand

        #region RequestChangeCommand

        protected virtual void RequestChangeCommand(Object parameter)
        {
        }

        protected virtual bool CanChangeCommand()
        {
            if (!this.Model.Definition.CanLiveEdit && this.RootViewModel.IsLiveEditing)
            {
                return false;
            }

            if (this.Model.InheritParent == true)
            {
                return false;
            }
            return true;
        }

        #endregion // RequestChangeCommand

        #region ExpandAllCommand
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        protected virtual void RequestExpandAllCommand(Object parameter)
        {
            if (this is HierarchicalTunableViewModel)
            {
                if ((this as HierarchicalTunableViewModel).Members.Count > 0)
                {
                    this.IsExpanded = true;
                    foreach (var tunable in (this as HierarchicalTunableViewModel).GetAllChildren())
                    {
                        tunable.IsExpanded = true;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        protected virtual bool CanExpandAllCommand(Object parameter)
        {
            if (this is HierarchicalTunableViewModel)
            {
                if ((this as HierarchicalTunableViewModel).Members.Count > 0)
                {
                    if (this.IsExpanded == false)
                        return true;
                    else
                        foreach (var tunable in (this as HierarchicalTunableViewModel).GetAllChildren())
                        {
                            if (!tunable.IsExpanded)
                                return true;
                        }
                }
            }
            return false;
        }
        #endregion // ExpandAllCommand

        #region CollapseAllCommand
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        protected virtual void RequestCollapseAllCommand(Object parameter)
        {
            if (this is HierarchicalTunableViewModel)
            {
                if ((this as HierarchicalTunableViewModel).Members.Count > 0)
                {
                    this.IsExpanded = false;
                    foreach (var tunable in (this as HierarchicalTunableViewModel).GetAllChildren())
                    {
                        tunable.IsExpanded = false;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        protected virtual bool CanCollapseAllCommand(Object parameter)
        {
            if (this is HierarchicalTunableViewModel)
            {
                if ((this as HierarchicalTunableViewModel).Members.Count > 0)
                {
                    if (this.IsExpanded == true)
                        return true;
                    else
                        foreach (var tunable in (this as HierarchicalTunableViewModel).GetAllChildren())
                        {
                            if (tunable.IsExpanded)
                                return true;
                        }
                }
            }
            return false;
        }
        #endregion // CollapseAllCommand

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool CanAddOrRemoveToParent()
        {
            if (this.Model == null || this.Model.Definition == null)
                return false;


            if ( this.Model.InheritParent == true )
            {
                return false;
            }

            if ( this.Parent is ArrayTunableViewModel )
            {
                ArrayTunableViewModel parent = (ArrayTunableViewModel)this.Parent;
                if (!parent.Model.Definition.CanLiveEdit && this.RootViewModel.IsLiveEditing)
                {
                    return false;
                }

                if ( parent.Model.Definition is ArrayMember )
                {
                    if ( ((ArrayMember)parent.Model.Definition).IsFixedSize() )
                    {
                        return false;
                    }
                }
                return true;
            }
            else if (this.Parent is MapTunableViewModel)
            {
                MapTunableViewModel parent = (MapTunableViewModel)this.Parent;
                if (!parent.Model.Definition.CanLiveEdit && this.RootViewModel.IsLiveEditing)
                {
                    return false;
                }

                return true;
            }

            return false;
        }
        #endregion // Commands

        #region Methods
        private void RefreshStringPossibleValues(object parameter)
        {
            if (this.Model.Definition is StringMember)
            {
                StringMember stringMember = this.Model.Definition as StringMember;
                if (stringMember.SourceType == StringMember.StringSourceType.Directory)
                {
                    stringMember.CreateDirectoryPossibleValues(this.RootViewModel.Branch);
                }
                else if (stringMember.SourceType == StringMember.StringSourceType.ParentMap)
                {
                    SetPossibleValuesForStringFromMapParent(stringMember);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public virtual ITunableViewModel FindNext(Regex expression)
        {
            return FindNext(expression, true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public virtual ITunableViewModel FindNext(Regex expression, bool checkParents)
        {
            if (this is HierarchicalTunableViewModel)
            {
                ITunableViewModel result = (this as HierarchicalTunableViewModel).FindNextInChildren(expression);
                if (result != null)
                    return result;
            }

            if (this.Parent is HierarchicalTunableViewModel && checkParents)
            {
                ITunableViewModel result = (this.Parent as HierarchicalTunableViewModel).FindNextInChildren(expression, this, true);
                if (result != null)
                    return result;
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public virtual ITunableViewModel FindPrevious(Regex expression)
        {
            return FindPrevious(expression, true, false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public virtual ITunableViewModel FindPrevious(Regex expression, bool checkParents, bool checkChildren)
        {
            if (this.Parent is HierarchicalTunableViewModel && checkParents)
            {
                ITunableViewModel result = (this.Parent as HierarchicalTunableViewModel).FindPreviousInChildren(expression, this, true, true);
                if (result != null)
                    return result;
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        internal virtual ITunableViewModel FindNextInChildren(Regex expression)
        {
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        internal ITunableViewModel FindNextInChildren(Regex expression, ITunableViewModel start, bool checkParents)
        {
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public virtual  List<ITunableViewModel> FindAll(Regex expression)
        {
            List<ITunableViewModel> found = new List<ITunableViewModel>();
            if (this is HierarchicalTunableViewModel)
            {
                foreach (var tunable in (this as HierarchicalTunableViewModel).GetAllChildren())
                {
                    if (tunable.IsMatch(expression))
                        found.Add(tunable);
                }
            }

            return found;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public virtual Boolean IsMatch(Regex expression)
        {
            List<String> values = new List<string>();
            if (this.Model is FloatTunable)
            {
                values.Add((this.Model as FloatTunable).Value.ToString());
            }
            else if (this.Model is S8Tunable)
            {
                values.Add((this.Model as S8Tunable).Value.ToString());
            }
            else if (this.Model is CharTunable)
            {
                values.Add((this.Model as CharTunable).Value.ToString());
            }
            else if (this.Model is U8Tunable)
            {
                values.Add((this.Model as U8Tunable).Value.ToString());
            }
            else if (this.Model is S16Tunable)
            {
                values.Add((this.Model as S16Tunable).Value.ToString());
            }
            else if (this.Model is ShortTunable)
            {
                values.Add((this.Model as ShortTunable).Value.ToString());
            }
            else if (this.Model is U16Tunable)
            {
                values.Add((this.Model as U16Tunable).Value.ToString());
            }
            else if (this.Model is S32Tunable)
            {
                values.Add((this.Model as S32Tunable).Value.ToString());
            }
            else if (this.Model is IntTunable)
            {
                values.Add((this.Model as IntTunable).Value.ToString());
            }
            else if (this.Model is U32Tunable)
            {
                values.Add((this.Model as U32Tunable).Value.ToString());
            }
            else if (this.Model is EnumTunable)
            {
                values.Add((this.Model as EnumTunable).ValueAsString);
            }
            else if (this.Model is Vector2TunableBase)
            {
                values.Add((this.Model as Vector2TunableBase).X.ToString());
                values.Add((this.Model as Vector2TunableBase).Y.ToString());
            }
            else if (this.Model is Vector3TunableBase)
            {
                values.Add((this.Model as Vector3TunableBase).X.ToString());
                values.Add((this.Model as Vector3TunableBase).Y.ToString());
                values.Add((this.Model as Vector3TunableBase).Z.ToString());
            }
            else if (this.Model is Vector4TunableBase)
            {
                values.Add((this.Model as Vector4TunableBase).X.ToString());
                values.Add((this.Model as Vector4TunableBase).Y.ToString());
                values.Add((this.Model as Vector4TunableBase).Z.ToString());
                values.Add((this.Model as Vector4TunableBase).W.ToString());
            }
            else if (this.Model is Matrix33TunableBase)
            {
                values.Add((this.Model as Matrix33TunableBase).XX.ToString());
                values.Add((this.Model as Matrix33TunableBase).XY.ToString());
                values.Add((this.Model as Matrix33TunableBase).XZ.ToString());

                values.Add((this.Model as Matrix33TunableBase).YX.ToString());
                values.Add((this.Model as Matrix33TunableBase).YY.ToString());
                values.Add((this.Model as Matrix33TunableBase).YZ.ToString());

                values.Add((this.Model as Matrix33TunableBase).ZX.ToString());
                values.Add((this.Model as Matrix33TunableBase).ZY.ToString());
                values.Add((this.Model as Matrix33TunableBase).ZZ.ToString());
            }
            else if (this.Model is Matrix34TunableBase)
            {
                values.Add((this.Model as Matrix34TunableBase).XX.ToString());
                values.Add((this.Model as Matrix34TunableBase).XY.ToString());
                values.Add((this.Model as Matrix34TunableBase).XZ.ToString());
                values.Add((this.Model as Matrix34TunableBase).XW.ToString());

                values.Add((this.Model as Matrix34TunableBase).YX.ToString());
                values.Add((this.Model as Matrix34TunableBase).YY.ToString());
                values.Add((this.Model as Matrix34TunableBase).YZ.ToString());
                values.Add((this.Model as Matrix34TunableBase).YW.ToString());

                values.Add((this.Model as Matrix34TunableBase).ZX.ToString());
                values.Add((this.Model as Matrix34TunableBase).ZY.ToString());
                values.Add((this.Model as Matrix34TunableBase).ZZ.ToString());
                values.Add((this.Model as Matrix34TunableBase).ZW.ToString());
            }
            else if (this.Model is Matrix44TunableBase)
            {
                values.Add((this.Model as Matrix44TunableBase).XX.ToString());
                values.Add((this.Model as Matrix44TunableBase).XY.ToString());
                values.Add((this.Model as Matrix44TunableBase).XZ.ToString());
                values.Add((this.Model as Matrix44TunableBase).XW.ToString());

                values.Add((this.Model as Matrix44TunableBase).YX.ToString());
                values.Add((this.Model as Matrix44TunableBase).YY.ToString());
                values.Add((this.Model as Matrix44TunableBase).YZ.ToString());
                values.Add((this.Model as Matrix44TunableBase).YW.ToString());

                values.Add((this.Model as Matrix44TunableBase).ZX.ToString());
                values.Add((this.Model as Matrix44TunableBase).ZY.ToString());
                values.Add((this.Model as Matrix44TunableBase).ZZ.ToString());
                values.Add((this.Model as Matrix44TunableBase).ZW.ToString());

                values.Add((this.Model as Matrix44TunableBase).WX.ToString());
                values.Add((this.Model as Matrix44TunableBase).WY.ToString());
                values.Add((this.Model as Matrix44TunableBase).WZ.ToString());
                values.Add((this.Model as Matrix44TunableBase).WW.ToString());
            }
            else if (this.Model is StructureTunable)
            {
            }
            else if (this.Model is PointerTunable)
            {
            }
            else if (this.Model is ArrayTunable)
            {
                ArrayTunable arrayTunable = (this.Model as ArrayTunable);

                foreach(var item in arrayTunable.Items)
                {
                    values.Add(item.Name);
                }
            }
            else if (this.Model is StringTunable)
            {
                values.Add((this.Model as StringTunable).Value);
            }

            foreach (String value in values)
            {
                if (value != null && expression.IsMatch(value))
                    return true;
            }

            return false;
        }
        #endregion // Methods
    }

    public class BitsetTunableValueViewModel :
        ViewModelBase, IComboWithCheckboxesItem
    {
        #region Properties

        public String DisplayName
        {
            get;
            set;
        }

        public Boolean IsChecked
        {
            get { return m_isChecked; }
            set
            {
                if (value == m_isChecked)
                    return;

                m_isChecked = value;
                OnPropertyChanged("IsChecked");
                this.Parent.ChildInclusionChanged();
            }
        }
        private Boolean m_isChecked;

        BitsetTunableViewModel Parent
        {
            get;
            set;
        }

        #endregion // Properties

        #region Constructor

        public BitsetTunableValueViewModel(BitsetTunableViewModel parent, String displayName, Boolean isChecked)
        {
            this.m_isChecked = isChecked;
            this.Parent = parent;
            this.DisplayName = displayName;
        }

        #endregion // Constructor
    }

    public class BitsetTunableViewModel :
        TunableViewModel, System.Windows.IWeakEventListener
    {
        #region Properties

        public ObservableCollection<BitsetTunableValueViewModel> Values
        {
            get;
            set;
        }

        public Boolean _updatingTunableValue = false;

        public String DisplayText
        {
            get { return m_displayText; }
            set
            {
                m_displayText = value;
                OnPropertyChanged("DisplayText");
            }
        }
        private String m_displayText;

        #endregion // Properties

        #region Constructor

        public BitsetTunableViewModel(ITunableViewModel parent, BitsetTunable t, MetaFileViewModel root)
            : base(parent, t, root)
        {
            System.ComponentModel.PropertyChangedEventManager.AddListener(t, this, "Value");
            this.Values = new ObservableCollection<BitsetTunableValueViewModel>();

            BitsetMember bitsetMemeber = t.Definition as BitsetMember;
            if (bitsetMemeber.Values == null)
            {
                this.ChildInclusionChanged();
                return;
            }

            IEnumerable<string> keys = null;
            if (bitsetMemeber.EnumerationValues != null && bitsetMemeber.EnumerationValues.Numerical)
            {
                keys = from k in bitsetMemeber.Values.Keys
                                           select k;
            }
            else
            {
                keys = from k in bitsetMemeber.Values.Keys
                                           orderby k ascending
                                           select k;
            }

                
            foreach (String bitsetValue in keys)
            {
                Boolean isChecked = false;
                if (t.Value != null && t.Value.Contains(bitsetValue))
                {
                    string[] valueParts = t.Value.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    if (valueParts.Contains(bitsetValue))
                    {
                        isChecked = true;
                    }
                }

                BitsetTunableValueViewModel newValue = new BitsetTunableValueViewModel(this, bitsetValue, isChecked);
                this.Values.Add(newValue);
            }

            this.ChildInclusionChanged();
        }

        #endregion // Constructor

        #region IWeakEventListener
         
        /// <summary>
        /// true if the listener handled the event. It is considered an error by the
        /// System.Windows.WeakEventManager handling in WPF to register a listener for 
        /// an event that the listener does not handle. Regardless, the method should
        /// return false if it receives an event that it does not recognize or handle.
        /// </summary>
        public Boolean ReceiveWeakEvent(Type managerType, Object sender, EventArgs e)
        {
            if (managerType == typeof(System.ComponentModel.PropertyChangedEventManager))
            {
                if (_updatingTunableValue == true)
                    return true;
                else
                {
                    // Property on model changed, need to update the view models.
                    if (sender is BitsetTunable)
                    {
                        BitsetTunable bitsetTunable = sender as BitsetTunable;
                        String[] setValues = new String[] { };
                        if (!String.IsNullOrEmpty(bitsetTunable.Value))
                            setValues = bitsetTunable.Value.Split(new String[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                        if ((bitsetTunable.Definition as RSG.Metadata.Parser.BitsetMember).Values != null)
                        {
                            this._updatingTunableValue = true;
                            foreach (BitsetTunableValueViewModel vm in this.Values)
                            {
                                vm.IsChecked = false;
                                if (setValues.Contains(vm.DisplayName))
                                {
                                    vm.IsChecked = true;
                                }
                            }
                            this._updatingTunableValue = false;
                        }
                        String value = String.Empty;
                        int index = 0;
                        foreach (BitsetTunableValueViewModel bitsetValue in this.Values)
                        {
                            if (bitsetValue.IsChecked == true)
                            {
                                if (index == 0)
                                {
                                    value += bitsetValue.DisplayName;
                                }
                                else
                                {
                                    value += " | " + bitsetValue.DisplayName;
                                }
                                index++;
                            }
                        }
                        this.DisplayText = value;
                    }
                }                
            }

            return true;
        }

        #endregion // IWeakEventListener

        #region Methods

        public void ChildInclusionChanged()
        {
            if (this._updatingTunableValue == true)
                return;

            String value = String.Empty;
            int index = 0;
            foreach (BitsetTunableValueViewModel bitsetValue in this.Values)
            {
                if (bitsetValue.IsChecked == true)
                {
                    if (index == 0)
                    {
                        value += bitsetValue.DisplayName;
                    }
                    else
                    {
                        value += " " + bitsetValue.DisplayName;
                    }
                    index++;
                }
            }

            this._updatingTunableValue = true;
            (this.Model as BitsetTunable).Value = value;
            this._updatingTunableValue = false;
        }

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="expression"></param>
        ///// <returns></returns>
        //public override Object FindNext(Regex expression)
        //{
        //    if (this.Parent is HierarchicalTunableViewModel)
        //    {
        //        return (this.Parent as HierarchicalTunableViewModel).FindNextFrom(expression, this);
        //    }
        //    return null;
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="expression"></param>
        ///// <returns></returns>
        //public override Object FindPrevious(Regex expression)
        //{
        //    if (this.Parent is HierarchicalTunableViewModel)
        //    {
        //        return (this.Parent as HierarchicalTunableViewModel).FindPreviousFrom(expression, this);
        //    }
        //    return null;
        //}

        public override Boolean IsMatch(Regex expression)
        {
            if ((this.Model as BitsetTunable).Value == null)
                return false;

            String[] parts = (this.Model as BitsetTunable).Value.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (String part in parts)
            {
                if (expression.Match(part).Success)
                {
                    return true;
                }
            }
            return false;
        }

        #endregion // Methods
    }
}
