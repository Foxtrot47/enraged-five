﻿using System;
using System.Windows.Media.Imaging;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using ContentBrowser.AddIn;
using RSG.Model.Common;
using System.Text.RegularExpressions;
using Workbench.AddIn.Services;
using Workbench.AddIn.UI.Layout;
using RSG.Model.Asset;
using RSG.Model.Common.Map;

namespace ContentBrowser.ViewModel
{
    /// <summary>
    /// A view model for a generic browser
    /// </summary>
    public class GenericBrowserViewModel : ViewModelBase, IBrowserViewModel
    {
        #region Members

        private ObservableCollection<ContentBrowserCommandViewModel> m_commands;
        private ObservableCollection<IAssetViewModel> m_assetHierarchy;
        private BrowserBase m_model;
        private Object m_selectedItem;

        #endregion // Members

        #region Properties

        /// <summary>
        /// The model that this viewmodel wraps
        /// </summary>
        public BrowserBase Model
        {
            get { return m_model; }
            set
            {
                if (m_model == value)
                    return;

                if (this.Model != null)
                {
                    CollectionChangedEventManager.RemoveListener(this.Model.AssetHierarchy, this);
                    PropertyChangedEventManager.RemoveListener(this.Model, this, "AssetHierarchy");
                }

                SetPropertyValue(value, () => Model,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_model = (BrowserBase)newValue;
                        }
                ));

                CollectionChangedEventManager.AddListener(this.Model.AssetHierarchy, this);
                PropertyChangedEventManager.AddListener(this.Model, this, "AssetHierarchy");
                SyncAssetHierarchies();
            }
        }

        /// <summary>
        /// A collection of commands that are displayed on the header of the browser
        /// </summary>
        public ObservableCollection<ContentBrowserCommandViewModel> Commands
        {
            get { return m_commands; }
            set
            {
                if (m_commands == value)
                    return;

                SetPropertyValue(value, () => this.Commands,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_commands = (ObservableCollection<ContentBrowserCommandViewModel>)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The asset hierarchy that is used to display the tree view inside the
        /// browser panel
        /// </summary>
        public ObservableCollection<IAssetViewModel> AssetHierarchy
        {
            get { return m_assetHierarchy; }
            set
            {
                if (m_assetHierarchy == value)
                    return;

                SetPropertyValue(value, () => this.AssetHierarchy,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_assetHierarchy = (ObservableCollection<IAssetViewModel>)newValue;
                        }
                ));
            }
        }      

        /// <summary>
        /// The name of the browser
        /// </summary>
        public String Name
        {
            get { return this.Model.Name; }
        }
                
        /// <summary>
        /// The current item that is selected in the item source
        /// </summary>
        public Object SelectedItem
        {
            get { return m_selectedItem; }
            set
            {
                if (value == m_selectedItem)
                    return;

                Object oldValue = m_selectedItem;

                SetPropertyValue(value, () => this.SelectedItem,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_selectedItem = (Object)newValue;
                        }
                ));

                this.OnSelectionChanged(oldValue, value);
            }
        }

        /// <summary>
        /// The current item that is selected in the item source
        /// </summary>
        public Control HeaderControl
        {
            get { return this.Model.HeaderControl; }
        }

        /// <summary>
        /// The icon that will be displayed to represent this browser
        /// </summary>
        public BitmapImage BrowserIcon
        {
            get { return this.Model.BrowserIcon; }
        }

        #endregion // Properties

        #region Constructor

        /// <summary>
        /// Default Constructor
        /// </summary>
        public GenericBrowserViewModel(BrowserBase model)
        {
            if (model == null)
                throw new ArgumentException("Unable to create a browser viewmodel without a model", "model");

            this.Model = model;

            this.Commands = new ObservableCollection<ContentBrowserCommandViewModel>();
            foreach (ContentBrowserCommand command in model.Commands)
            {
                this.Commands.Add(new ContentBrowserCommandViewModel(command));
            }
        }

        #endregion // Constructor

        #region Property Changed Functions
        
        /// <summary>
        /// Gets called after the selection for this browser has changed.
        /// This makes sure that only one browser has anything selected
        /// at a time
        /// </summary>
        private void OnSelectionChanged(Object oldValue, Object newValue)
        {
        }

        #endregion // Property Changed Functions

        #region Private Functions

        /// <summary>
        /// Makes sure that the model asset hierarchy and this view model
        /// asset hierarchy are in sync
        /// </summary>
        private void SyncAssetHierarchies()
        {
            if (this.AssetHierarchy == null)
                this.AssetHierarchy = new ObservableCollection<IAssetViewModel>();

            this.ClearSelection();

            Dispatcher.Invoke( (Action)delegate()
            {
                this.AssetHierarchy.Clear();
                foreach ( IAsset asset in this.Model.AssetHierarchy )
                {
                    this.AssetHierarchy.Add( ViewModelFactory.CreateViewModel( asset ) );
                }
            } );

        }

        /// <summary>
        /// Clears the selection of all the assets starting with the
        /// given root asset
        /// </summary>
        private void ClearSelection(IAssetViewModel rootAsset)
        {
            rootAsset.IsSelected = false;
            if (rootAsset is AssetContainerViewModelBase)
            {
                foreach (IAssetViewModel childAsset in (rootAsset as AssetContainerViewModelBase).AssetChildren)
                {
                    ClearSelection(childAsset);
                }
            }
        }

        #endregion // Private Functions

        #region Public Functions
        /// <summary>
        /// Clears the current selection in the tree view of assets
        /// </summary>
        public void ClearSelection()
        {
            foreach (IAssetViewModel asset in AssetHierarchy)
            {
                this.ClearSelection(asset);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public List<ISearchResult> FindResults(ISearchQuery query)
        {
            if (this.Model == null)
                return new List<ISearchResult>();

            IAsset root = null;
            if (SelectedItem is IAssetViewModel)
                root = (SelectedItem as IAssetViewModel).Model;


            return this.Model.FindResults(query, root);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="asset"></param>
        public void SelectAsset(IAsset asset)
        {
            if (asset is TextureDictionary)
            {
                SelectTextureDictionary(asset as TextureDictionary);
                return;
            }

            var parentHierarchy = GetParentHierarchyForAsset(asset);
            if (parentHierarchy.Count == 0)
                return;

            IAssetViewModel firstViewModel = null;
            foreach (var item in AssetHierarchy)
            {
                if (ReferenceEquals(item.Model, parentHierarchy[0]))
                {
                    firstViewModel = item;
                    break;
                }
            }
            if (firstViewModel == null)
                return;

            if (firstViewModel is AssetContainerViewModelBase && parentHierarchy.Count > 1)
            {
                (firstViewModel as AssetContainerViewModelBase).ExpandAndCreateChildren();
                IAssetViewModel previous = firstViewModel;
                for (int i = 1; i < parentHierarchy.Count; i++)
                {
                    if (previous is AssetContainerViewModelBase)
                    {
                        bool found = false;
                        foreach (var child in (previous as AssetContainerViewModelBase).AssetChildren)
                        {
                            if (ReferenceEquals(child.Model, parentHierarchy[i]))
                            {
                                if (child is AssetContainerViewModelBase)
                                {
                                    (child as AssetContainerViewModelBase).ExpandAndCreateChildren();
                                    previous = child;
                                    found = true;
                                    break;
                                }
                            }
                        }
                        if (!found)
                        {
                            foreach (var child in (previous as AssetContainerViewModelBase).AssetChildren)
                            {
                                if (child is AssetContainerViewModelBase)
                                {
                                    (child as AssetContainerViewModelBase).ExpandAndCreateChildren();
                                    foreach (var grandChild in (child as AssetContainerViewModelBase).AssetChildren)
                                    {
                                        if (ReferenceEquals(grandChild.Model, parentHierarchy[i]))
                                        {
                                            previous = grandChild;
                                            found = true;
                                            break;
                                        }
                                    }
                                    if (!found)
                                        (child as AssetContainerViewModelBase).CollapseAndDestroyChildren();
                                    else
                                        break;
                                }
                            }
                        }
                    }
                }
                previous.IsSelected = true;
            }
            else
            {
                firstViewModel.IsSelected = true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="asset"></param>
        public void SelectDummyAsset(IAsset asset)
        {
            List<IAsset> parentHierarchy = GetParentHierarchyForAsset(asset);
            if (parentHierarchy.Count == 0)
            {
                return;
            }

            IAssetViewModel firstViewModel = null;
            foreach (var item in AssetHierarchy)
            {
                if (ReferenceEquals(item.Model, parentHierarchy[0]))
                {
                    firstViewModel = item;
                    break;
                }
            }
            if (firstViewModel == null)
            {
                return;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="asset"></param>
        public void SelectMapObjectAsset(IAsset asset)
        {
            var parentHierarchy = GetParentHierarchyForAsset(asset);
            if (parentHierarchy.Count == 0)
                return;

            IAssetViewModel firstViewModel = null;
            foreach (var item in AssetHierarchy)
            {
                if (ReferenceEquals(item.Model, parentHierarchy[0]))
                {
                    firstViewModel = item;
                    break;
                }
            }
            if (firstViewModel == null)
                return;

            if (firstViewModel is AssetContainerViewModelBase && parentHierarchy.Count > 1)
            {
                (firstViewModel as AssetContainerViewModelBase).ExpandAndCreateChildren();
                IAssetViewModel previous = firstViewModel;
                for (int i = 1; i < parentHierarchy.Count; i++)
                {
                    if (previous is AssetContainerViewModelBase)
                    {
                        bool found = false;
                        foreach (var child in (previous as AssetContainerViewModelBase).AssetChildren)
                        {
                            if (ReferenceEquals(child.Model, parentHierarchy[i]))
                            {
                                if (child is AssetContainerViewModelBase)
                                {
                                    (child as AssetContainerViewModelBase).ExpandAndCreateChildren();
                                    previous = child;
                                    found = true;
                                    break;
                                }
                            }
                        }
                        if (!found)
                        {
                            foreach (var child in (previous as AssetContainerViewModelBase).AssetChildren)
                            {
                                if (child is AssetContainerViewModelBase)
                                {
                                    (child as AssetContainerViewModelBase).ExpandAndCreateChildren();
                                    foreach (var grandChild in (child as AssetContainerViewModelBase).AssetChildren)
                                    {
                                        if (ReferenceEquals(grandChild.Model, parentHierarchy[i]))
                                        {
                                            previous = grandChild;
                                            found = true;
                                            break;
                                        }
                                    }
                                    if (!found)
                                        (child as AssetContainerViewModelBase).CollapseAndDestroyChildren();
                                    else
                                        break;
                                }
                            }
                        }
                    }
                }
                if (previous is MapViewModels3.MapSectionViewModel)
                {
                    (previous as AssetContainerViewModelBase).ExpandAndCreateChildren();
                    foreach (var child in (previous as AssetContainerViewModelBase).AssetChildren)
                    {
                        if (child is MapViewModels3.MapSectionArchetypeGroupViewModel)
                        {
                            child.IsSelected = true;
                            break;
                        }
                    }
                }
                else
                {
                    previous.IsSelected = true;
                }
            }
            else
            {
                firstViewModel.IsSelected = true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="asset"></param>
        public void SelectTextureDictionariesAsset(IAsset asset)
        {
            var parentHierarchy = GetParentHierarchyForAsset(asset);
            if (parentHierarchy.Count == 0)
                return;

            IAssetViewModel firstViewModel = null;
            foreach (var item in AssetHierarchy)
            {
                if (ReferenceEquals(item.Model, parentHierarchy[0]))
                {
                    firstViewModel = item;
                    break;
                }
            }
            if (firstViewModel == null)
                return;

            if (firstViewModel is AssetContainerViewModelBase && parentHierarchy.Count > 1)
            {
                (firstViewModel as AssetContainerViewModelBase).ExpandAndCreateChildren();
                IAssetViewModel previous = firstViewModel;
                for (int i = 1; i < parentHierarchy.Count; i++)
                {
                    if (previous is AssetContainerViewModelBase)
                    {
                        bool found = false;
                        foreach (var child in (previous as AssetContainerViewModelBase).AssetChildren)
                        {
                            if (ReferenceEquals(child.Model, parentHierarchy[i]))
                            {
                                if (child is AssetContainerViewModelBase)
                                {
                                    (child as AssetContainerViewModelBase).ExpandAndCreateChildren();
                                    previous = child;
                                    found = true;
                                    break;
                                }
                            }
                        }
                        if (!found)
                        {
                            foreach (var child in (previous as AssetContainerViewModelBase).AssetChildren)
                            {
                                if (child is AssetContainerViewModelBase)
                                {
                                    (child as AssetContainerViewModelBase).ExpandAndCreateChildren();
                                    foreach (var grandChild in (child as AssetContainerViewModelBase).AssetChildren)
                                    {
                                        if (ReferenceEquals(grandChild.Model, parentHierarchy[i]))
                                        {
                                            previous = grandChild;
                                            found = true;
                                            break;
                                        }
                                    }
                                    if (!found)
                                        (child as AssetContainerViewModelBase).CollapseAndDestroyChildren();
                                    else
                                        break;
                                }
                            }
                        }
                    }
                }
                if (previous is MapViewModels3.MapSectionViewModel)
                {
                    (previous as AssetContainerViewModelBase).ExpandAndCreateChildren();
                    foreach (var child in (previous as AssetContainerViewModelBase).AssetChildren)
                    {
                        if (child is MapViewModels3.TextureDictionaryGroupViewModel)
                        {
                            child.IsSelected = true;
                            break;
                        }
                    }
                }
                else
                {
                    previous.IsSelected = true;
                }
            }
            else
            {
                firstViewModel.IsSelected = true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="asset"></param>
        public void SelectTextureDictionary(TextureDictionary asset)
        {
            var parentHierarchy = GetParentHierarchyForAsset(asset.SourceSectionName, typeof(IMapSection));
            if (parentHierarchy.Count == 0)
                return;

            IAssetViewModel firstViewModel = null;
            foreach (var item in AssetHierarchy)
            {
                if (ReferenceEquals(item.Model, parentHierarchy[0]))
                {
                    firstViewModel = item;
                    break;
                }
            }
            if (firstViewModel == null)
                return;

            if (firstViewModel is AssetContainerViewModelBase && parentHierarchy.Count > 1)
            {
                (firstViewModel as AssetContainerViewModelBase).ExpandAndCreateChildren();
                IAssetViewModel previous = firstViewModel;
                for (int i = 1; i < parentHierarchy.Count; i++)
                {
                    if (previous is AssetContainerViewModelBase)
                    {
                        bool found = false;
                        foreach (var child in (previous as AssetContainerViewModelBase).AssetChildren)
                        {
                            if (ReferenceEquals(child.Model, parentHierarchy[i]))
                            {
                                if (child is AssetContainerViewModelBase)
                                {
                                    (child as AssetContainerViewModelBase).ExpandAndCreateChildren();
                                    previous = child;
                                    found = true;
                                    break;
                                }
                            }
                        }
                        if (!found)
                        {
                            foreach (var child in (previous as AssetContainerViewModelBase).AssetChildren)
                            {
                                if (child is AssetContainerViewModelBase)
                                {
                                    (child as AssetContainerViewModelBase).ExpandAndCreateChildren();
                                    foreach (var grandChild in (child as AssetContainerViewModelBase).AssetChildren)
                                    {
                                        if (ReferenceEquals(grandChild.Model, parentHierarchy[i]))
                                        {
                                            previous = grandChild;
                                            found = true;
                                            break;
                                        }
                                    }
                                    if (!found)
                                        (child as AssetContainerViewModelBase).CollapseAndDestroyChildren();
                                    else
                                        break;
                                }
                            }
                        }
                    }
                }
                if (previous is MapViewModels3.MapSectionViewModel)
                {
                    (previous as AssetContainerViewModelBase).ExpandAndCreateChildren();
                    foreach (var child in (previous as AssetContainerViewModelBase).AssetChildren)
                    {
                        if (child is MapViewModels3.TextureDictionaryGroupViewModel)
                        {
                            (child as AssetContainerViewModelBase).ExpandAndCreateChildren();
                            foreach (var grandchild in (child as AssetContainerViewModelBase).AssetChildren)
                            {
                                if (grandchild.Name == asset.Name)
                                {
                                    grandchild.IsSelected = true;
                                    break;
                                }
                            }
                            break;
                        }
                    }
                }
                else
                {
                    previous.IsSelected = true;
                }
            }
            else
            {
                firstViewModel.IsSelected = true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="asset"></param>
        /// <returns></returns>
        private List<IAsset> GetParentHierarchyForAsset(IAsset asset)
        {
            var parentHierarchy = new List<IAsset>();
            GetParentHierarchyForAsset(asset, ref parentHierarchy);
            parentHierarchy.Reverse();
            return parentHierarchy;
        }

        private bool GetParentHierarchyForAsset(IAsset asset, ref List<IAsset> hierarchy)
        {
            foreach (var model in AssetHierarchy)
            {
                if (GetParentHierarchyForAsset(model.Model, asset, ref hierarchy))
                {
                    hierarchy.Insert(0, asset);
                    return true;
                }
            }
            return false;
        }

        private bool GetParentHierarchyForAsset(IAsset root, IAsset asset, ref List<IAsset> hierarchy)
        {
            if (ReferenceEquals(root, asset))
                return true;
            else if (root is IHasAssetChildren)
            {
                foreach (var child in (root as IHasAssetChildren).AssetChildren)
                {
                    if (GetParentHierarchyForAsset(child, asset, ref hierarchy))
                    {
                        hierarchy.Add(root);
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="asset"></param>
        /// <returns></returns>
        private List<IAsset> GetParentHierarchyForAsset(string name, Type type)
        {
            var parentHierarchy = new List<IAsset>();
            GetParentHierarchyForAsset(name, type, ref parentHierarchy);
            parentHierarchy.Reverse();
            return parentHierarchy;
        }

        private bool GetParentHierarchyForAsset(string name, Type type, ref List<IAsset> hierarchy)
        {
            foreach (var model in AssetHierarchy)
            {
                if (GetParentHierarchyForAsset(model.Model, name, type, ref hierarchy))
                {
                    //hierarchy.Insert(0, asset);
                    return true;
                }
            }
            return false;
        }

        private bool GetParentHierarchyForAsset(IAsset root, string name, Type type, ref List<IAsset> hierarchy)
        {
            if (root.GetType() == type && root.Name == name)
            {
                hierarchy.Insert(0, root);
                return true;
            }
            else if (root is IHasAssetChildren)
            {
                foreach (var child in (root as IHasAssetChildren).AssetChildren)
                {
                    if (GetParentHierarchyForAsset(child, name, type, ref hierarchy))
                    {
                        hierarchy.Add(root);
                        return true;
                    }
                }
            }
            return false;
        }
        #endregion // Functions

        #region IWeakEventListener

        /// <summary>
        /// Receives property changed events for the level selected changed
        /// so that the asset hierarchy can be updated
        /// </summary>
        public Boolean ReceiveWeakEvent(Type managerType, Object sender, EventArgs e)
        {
            if (managerType == typeof(PropertyChangedEventManager))
            {
                if (e is PropertyChangedEventArgs && (e as PropertyChangedEventArgs).PropertyName == "AssetHierarchy")
                {
                    CollectionChangedEventManager.AddListener(this.Model.AssetHierarchy, this);
                    SyncAssetHierarchies();
                }
            }
            else if (managerType == typeof(CollectionChangedEventManager))
            {
                this.SyncAssetHierarchies();
            }

            return true;
        }

        #endregion // IWeakEventListener
    } // GenericBrowserViewModel
} // ContentBrowser.ViewModel
