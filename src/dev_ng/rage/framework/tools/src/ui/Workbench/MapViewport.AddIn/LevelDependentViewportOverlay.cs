﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Viewport.AddIn;
using ContentBrowser.AddIn;
using Workbench.AddIn;
using System.ComponentModel.Composition;
using System.ComponentModel;
using System.Windows;
using Workbench.AddIn.Services.Model;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using RSG.Model.Common;
using RSG.Base.Math;

namespace MapViewport.AddIn
{
    /// <summary>
    /// Viewport overlay that depends on the selected level being initialised before being activated.
    /// </summary>
    public abstract class LevelDependentViewportOverlay : ViewportOverlay, IPartImportsSatisfiedNotification, IWeakEventListener
    {
        #region MEF Imports
        /// <summary>
        /// Level Browser
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LevelBrowser, typeof(ILevelBrowser))]
        protected Lazy<ILevelBrowser> LevelBrowserProxy { get; set; }
        #endregion // MEF Imports

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public LevelDependentViewportOverlay(String name, String description)
            : base(name, description)
        {
        }
        #endregion // Constructor(s)

        #region Property Changed Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void LevelChanged(Object sender, LevelChangedEventArgs args)
        {
            if (args.OldItem != null)
            {
                PropertyChangedEventManager.RemoveListener(args.OldItem, this, "Initialised");
            }

            if (args.NewItem != null)
            {
                PropertyChangedEventManager.AddListener(args.NewItem, this, "Initialised");
                OnPropertyChanged("CanActivate");
            }
        }
        #endregion // Property Changed Handlers

        #region Protected Methods
        /// <summary>
        /// Helper method to create an overlay image with the correct map dimensions.
        /// </summary>
        /// <param name="scale">Scale of the image.</param>
        /// <returns></returns>
        protected Viewport2DImageOverlay CreateOverlayImage(float scale = 0.5f)
        {
            Vector2i size = new Vector2i(1000, 1000);
            Vector2f pos = new Vector2f(-500, 500);
            ILevel level = LevelBrowserProxy.Value.SelectedLevel;
            if (level.ImageBounds != null)
            {
                size = new Vector2i((int)(level.ImageBounds.Max.X - level.ImageBounds.Min.X),
                    (int)(level.ImageBounds.Max.Y - level.ImageBounds.Min.Y));
                pos = new Vector2f(level.ImageBounds.Min.X, level.ImageBounds.Max.Y);
            }
            return new Viewport2DImageOverlay(etCoordSpace.World, "Overlay", 
                new Vector2i((int)(size.X * scale), (int)(size.Y * scale)), pos,
                new Vector2f(size.X, size.Y));
        }
        #endregion // Protected Methods

        #region IPartImportsSatisfiedNotification Methods
        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {
            // Add the callback for when the level changes
            LevelBrowserProxy.Value.LevelChanged += LevelChanged;

            if (LevelBrowserProxy.Value.SelectedLevel != null)
            {
                PropertyChangedEventManager.AddListener(LevelBrowserProxy.Value.SelectedLevel, this, "Initialised");
                OnPropertyChanged("CanActivate");
            }
        }
        #endregion // IPartImportsSatisfiedNotification Methods

        #region IWeakEventListener Implementation
        /// <summary>
        /// 
        /// </summary>
        public bool ReceiveWeakEvent(Type managerType, Object sender, EventArgs e)
        {
            Action action = () => OnPropertyChanged("CanActivate");
            Application.Current.Dispatcher.BeginInvoke(action);
            return true;
        }
        #endregion // IWeakEventListener
    } // LevelDependentViewportOverlay
}
