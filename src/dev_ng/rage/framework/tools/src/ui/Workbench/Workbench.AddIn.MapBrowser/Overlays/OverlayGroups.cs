﻿using System;
using System.ComponentModel.Composition;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Windows.Controls.WpfViewport2D;

namespace Workbench.AddIn.MapBrowser.Overlays
{
    //[ExportExtension(Map.AddIn.Services.ExtensionPoints.ViewportGeometry, typeof(Map.AddIn.Services.IViewportOverlayGroup))]
    //public class MiscellaneousGroup : ViewportOverlayGroup, Map.AddIn.Services.IViewportOverlayGroup, IPartImportsSatisfiedNotification
    //{
    //    #region MEF Imports

    //    /// <summary>
    //    /// Map Browser
    //    /// </summary>
    //    [ImportManyExtension("Workbench.AddIn.MapBrowser.OverlayGroups.MiscellaneousGroup", typeof(ViewportOverlay))]
    //    IEnumerable<ViewportOverlay> ImportedOverlays
    //    {
    //        get;
    //        set;
    //    }

    //    #endregion // MEF Imports

    //    #region Constructor

    //    public MiscellaneousGroup()
    //        : base("Miscellaneous")
    //    {
    //    }

    //    #endregion // Constructor

    //    #region IPartImportsSatisfiedNotification

    //    public void OnImportsSatisfied()
    //    {
    //        foreach (ViewportOverlay overlay in this.ImportedOverlays)
    //        {
    //            this.Overlays.Add(overlay);
    //        }   
    //    }

    //    #endregion // IPartImportsSatisfiedNotification
    //}

    //[ExportExtension(Map.AddIn.Services.ExtensionPoints.ViewportGeometry, typeof(Map.AddIn.Services.IViewportOverlayGroup))]
    //public class TextureDictionaryGroup : ViewportOverlayGroup, Map.AddIn.Services.IViewportOverlayGroup, IPartImportsSatisfiedNotification
    //{
    //    #region MEF Imports

    //    /// <summary>
    //    /// Map Browser
    //    /// </summary>
    //    [ImportManyExtension("Workbench.AddIn.MapBrowser.OverlayGroups.TextureDictionaryGroup", typeof(ViewportOverlay))]
    //    IEnumerable<ViewportOverlay> ImportedOverlays
    //    {
    //        get;
    //        set;
    //    }

    //    #endregion // MEF Imports

    //    #region Constructor

    //    public TextureDictionaryGroup()
    //        : base("Texture Dictionaries")
    //    {
    //    }

    //    #endregion // Constructor

    //    #region IPartImportsSatisfiedNotification

    //    public void OnImportsSatisfied()
    //    {
    //        foreach (ViewportOverlay overlay in this.ImportedOverlays)
    //        {
    //            this.Overlays.Add(overlay);
    //        }
    //    }

    //    #endregion // IPartImportsSatisfiedNotification
    //}
}
