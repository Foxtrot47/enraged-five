﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Collections;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using System.Reflection;
using MapViewport.AddIn;
using System.IO;
using RSG.Base.Logging;
using System.Xml.Linq;
using System.Windows;
using RSG.Model.Common;
using RSG.Base.Editor.Command;
using RSG.Base.Tasks;
using RSG.Model.Report;

namespace Viewport.AddIn
{
    /// <summary>
    /// A overlay is a collection of geometry that should be rendered on top
    /// of the viewport, it is abstract so that each application can determine the geometry gets called.
    /// </summary
    public abstract class ViewportOverlay : AssetBase, IViewportOverlay
    {
        #region Constants
        /// <summary>
        /// Where the overlay parameters get serialised out to
        /// </summary>
        private const string c_cachedParametersDirectory = @"cache:\Workbench\Overlays";
        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// Required for resolving the cache directory
        /// </summary>
        [Workbench.AddIn.ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(Workbench.AddIn.Services.IConfigurationService))]
        protected Lazy<Workbench.AddIn.Services.IConfigurationService> Config { get; set; }
        #endregion // MEF Imports

        #region Properties

        /// <summary>
        /// The name of this overlay, this will be the name shown
        /// in the viewport overlay drop down menu
        /// </summary>
        public String Name
        {
            get { return m_name; }
            set
            {
                SetPropertyValue(value, () => this.Name,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_name = (String)newValue;
                        }
                ));
            }
        }
        private String m_name;

        /// <summary>
        /// A string that can be used to add a description to the overlay that can
        /// then get displayed to the user.
        /// </summary>
        public String Description
        {
            get { return m_description; }
            set
            {
                SetPropertyValue(value, () => this.Description,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_description = (String)newValue;
                        }
                ));
            }
        }
        private String m_description;

        /// <summary>
        /// Which data source modes this report can be used in.
        /// </summary>
        public DataSourceCollection DataSourceModes
        {
            get;
            private set;
        }

        /// <summary>
        /// The geometry collection that the overlay is made up on
        /// </summary>
        public ObservableCollection<Viewport2DGeometry> Geometry
        {
            get { return m_geometry; }
            set
            {
                SetPropertyValue(value, () => this.Geometry,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_geometry = (ObservableCollection<Viewport2DGeometry>)newValue;
                        }
                ));
            }
        }
        private ObservableCollection<Viewport2DGeometry> m_geometry;

        /// <summary>
        /// Represents whether this overlay for the viewport is currently the active overlay
        /// </summary>
        public Boolean IsCurrentlyActive
        {
            get;
            set;
        }

        /// <summary>
        /// Flag indicating whether the overlay should be restored to be the open one the next time the workbench is reopened.
        /// </summary>
        public virtual Boolean RestoreIfOpen
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// List of stats that are required to generate this report.
        /// </summary>
        public virtual IEnumerable<StreamableStat> RequiredStats
        {
            get
            {
                if (m_requiredStats == null)
                {
                    m_requiredStats = new StreamableStat[] {};
                }
                return m_requiredStats;
            }
        }
        protected StreamableStat[] m_requiredStats;

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public ViewportOverlay(String name, String description)
        {
            this.Name = name;
            this.Description = description;
            this.IsCurrentlyActive = false;
            this.Geometry = new ObservableCollection<Viewport2DGeometry>();

            this.DataSourceModes = new DataSourceCollection();
            this.DataSourceModes[DataSource.SceneXml] = true;
            this.DataSourceModes[DataSource.Database] = false;
        }

        #endregion // Constructor(s)

        #region Virtual Function(s)

        /// <summary>
        /// Gets called when the user wants to view this overlay
        /// </summary>
        public virtual void Activated()
        {
            DeserialiseOverlaySettings();
            this.IsCurrentlyActive = true;
        }

        /// <summary>
        /// Gets called when the user turns off this overlay
        /// </summary>
        public virtual void Deactivated()
        {
            this.IsCurrentlyActive = false;
            this.Geometry.Clear();
            SerialiseOverlaySettings();
        }

        /// <summary>
        /// Gets the control that should be shown in the overlay details panel
        /// </summary>
        public virtual System.Windows.Controls.Control GetOverlayControl()
        {
            return null;
        }

        /// <summary>
        /// Gets the control that shows information at the bottom right of the viewport.
        /// </summary>
        /// <returns></returns>
        public virtual System.Windows.Controls.Control GetViewportControl()
        {
            return null;
        }

        /// <summary>
        /// Gets the list of controls that is displayed when the tool tip shows.
        /// </summary>
        /// <returns></returns>
        public virtual System.Windows.Controls.Control GetOverlayToolTipControl()
        {
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewPosition"></param>
        /// <param name="worldPosition"></param>
        public virtual void OnMouseMoved(Point viewPosition, Point worldPosition)
        {
        }
        #endregion // Virtual Function(s)

        #region Property Serialisation
        /// <summary>
        /// 
        /// </summary>
        private void SerialiseOverlaySettings()
        {
            Type overlayType = GetType();

            try
            {
                // Determine where to save this overlay
                string filepath = GetOverlayParameterFilepath(overlayType);

                // Get an enumeration of properties that have the overlayparameter attribute and aren't set to their default value.
                IEnumerable<KeyValuePair<PropertyInfo, OverlayParameterAttribute>> propertiesToSerialise =
                    GetOverlayParametersForType(overlayType).Where(item =>
                        {
                            object value = item.Key.GetValue(this, null);
                            return (value != null && !value.Equals(item.Value.DefaultValue));
                        });

                if (propertiesToSerialise.Any())
                {
                    XDocument paramDoc = 
                        new XDocument(
                            new XElement("Parameters",
                                new XAttribute("internalName", overlayType.AssemblyQualifiedName),
                                new XAttribute("title", this.Name),
                                propertiesToSerialise.Select(pair => 
                                    new XElement("Parameter",
                                        new XAttribute("exposed", true),
                                        new XAttribute("friendlyName", pair.Value.FriendlyName),
                                        new XAttribute("name", pair.Key.Name),
                                        new XAttribute("type", pair.Key.PropertyType.AssemblyQualifiedName),
                                        new XAttribute("unlocked", true),
                                        new XAttribute("value", pair.Key.GetValue(this, null))
                                    )
                                )
                            )
                        );

                    // Make sure the directory exists before saving out the file
                    if (!Directory.Exists(Path.GetDirectoryName(filepath)))
                    {
                        Directory.CreateDirectory(Path.GetDirectoryName(filepath));
                    }

                    paramDoc.Save(filepath);
                }
                else if (File.Exists(filepath))
                {
                    // If a parameters file exists, delete it
                    File.Delete(filepath);
                }
            }
            catch (Exception e)
            {
                Log.Log__Exception(e, "Unhandled exception occurred while serialising the overlay parameters for the {0} overlay.", overlayType.FullName);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void DeserialiseOverlaySettings()
        {
            Type overlayType = GetType();
            
            try
            {
                // To begin with, set all the properties to their default values
                IEnumerable<KeyValuePair<PropertyInfo, OverlayParameterAttribute>> overlayProperties = GetOverlayParametersForType(overlayType);
                IList<PropertyInfo> propertiesSet = new List<PropertyInfo>();

                // Next check if the user has a saved out properties xml file for this overlay
                string filepath = GetOverlayParameterFilepath(overlayType);
                if (File.Exists(filepath))
                {
                    // Parse the file extracting parameters.
                    XDocument paramDoc = XDocument.Load(filepath);

                    // Make sure that the type is correct
                    XAttribute overlayTypeAtt = paramDoc.Root.Attribute("internalName");
                    if (overlayTypeAtt != null && overlayTypeAtt.Value == overlayType.AssemblyQualifiedName)
                    {
                        Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
                        IDictionary<String, Assembly> assemblyLookup = new Dictionary<String, Assembly>();
                        foreach (Assembly assembly in assemblies)
                        {
                            if (!assemblyLookup.ContainsKey(assembly.FullName.ToLower()))
                            {
                                assemblyLookup.Add(assembly.FullName.ToLower(), assembly);
                            }
                        }

                        // Retrieve the parameters
                        foreach (XElement paramElem in paramDoc.Root.Elements("Parameter"))
                        {
                            // Get the name/type of this parameter
                            XAttribute nameAtt = paramElem.Attribute("name");
                            XAttribute typeAtt = paramElem.Attribute("type");
                            XAttribute valueAtt = paramElem.Attribute("value");

                            if (nameAtt != null && typeAtt != null && valueAtt != null)
                            {
                                // Try and get the property associated with this name/type combination
                                PropertyInfo propInfo = overlayProperties.Select(pair => pair.Key)
                                                                         .FirstOrDefault(item => item.Name == nameAtt.Value && item.PropertyType.AssemblyQualifiedName == typeAtt.Value);

                                if (propInfo != null)
                                {
                                    // Extract the assembly name from the value
                                    string assemblyFullName = typeAtt.Value.Substring(typeAtt.Value.IndexOf(',') + 1).Trim().ToLower();
                                    if (assemblyLookup.ContainsKey(assemblyFullName))
                                    {
                                        Assembly ass = assemblyLookup[assemblyFullName];
                                        Type propertyType = ass.GetType(typeAtt.Value.Substring(0, typeAtt.Value.IndexOf(',')));

                                        if (propertyType == typeof(Single))
                                        {
                                            propInfo.SetValue(this, Single.Parse(valueAtt.Value), null);
                                        }
                                        else if (propertyType == typeof(Boolean))
                                        {
                                            propInfo.SetValue(this, Boolean.Parse(valueAtt.Value), null);
                                        }
                                        else if (propertyType == typeof(Int32))
                                        {
                                            propInfo.SetValue(this, Int32.Parse(valueAtt.Value), null);
                                        }
                                        else if (propertyType == typeof(UInt32))
                                        {
                                            propInfo.SetValue(this, UInt32.Parse(valueAtt.Value), null);
                                        }
                                        else if (propertyType == typeof(String))
                                        {
                                            propInfo.SetValue(this, valueAtt.Value, null);
                                        }
                                        else if (propertyType.IsEnum)
                                        {
                                            propInfo.SetValue(this, Enum.Parse(propertyType, valueAtt.Value), null);
                                        }

                                        propertiesSet.Add(propInfo);
                                    }
                                    else
                                    {
                                        Log.Log__Warning("Unable to deserialise the {0} property for the {1} overlay as the assembly isn't loaded.", nameAtt.Value, overlayType.FullName);
                                    }
                                }
                                else
                                {
                                    Log.Log__Warning("Unable to deserialise the {0} property for the {1} overlay as the C# object doesn't have the corresponding property.", nameAtt.Value, overlayType.FullName);
                                }
                            }
                        }
                    }
                    else
                    {
                        // Missing type attribute or mismatched value
                        Log.Log__Warning("The saved parameters file for the {0} was invalid and thus the overlay's settings will not be restored.", overlayType.FullName);
                    }
                }

                // Set any of the properties that weren't deseriliased back to their default value.
                foreach (KeyValuePair<PropertyInfo, OverlayParameterAttribute> pair in overlayProperties.Where(item => !propertiesSet.Contains(item.Key)))
                {
                    pair.Key.SetValue(this, pair.Value.DefaultValue, null);
                }
            }
            catch (Exception e)
            {
                Log.Log__Exception(e, "Unhandled exception occurred while deserialising the overlay parameters for the {0} overlay.", overlayType.FullName);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private string GetOverlayParameterFilepath(Type overlayType)
        {
            string directory = c_cachedParametersDirectory.Replace("cache:", Config.Value.GameConfig.ToolsCacheDir);
            return Path.Combine(directory, overlayType.FullName + ".xml");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private IEnumerable<KeyValuePair<PropertyInfo, OverlayParameterAttribute>> GetOverlayParametersForType(Type type)
        {
            foreach (PropertyInfo propInfo in type.GetProperties())
            {
                // Check whether this property is flagged for serialisation
                OverlayParameterAttribute[] attributes = (OverlayParameterAttribute[])propInfo.GetCustomAttributes(typeof(OverlayParameterAttribute), true);
                if (attributes.Length > 0)
                {
                    yield return new KeyValuePair<PropertyInfo, OverlayParameterAttribute>(propInfo, attributes[0]);
                }
            }
        }
        #endregion // Property Serialisation
    } // ViewportOverlay
} // 
