﻿using System;
using System.Windows;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Windows.Threading;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using RSG.Model.Common;
using RSG.Base.Windows.DragDrop;
using RSG.Base.Collections;

namespace ContentBrowser.ViewModel
{
    /// <summary>
    /// A container view model base is an asset view model that has child asset view models
    /// </summary>
    public abstract class ContainerViewModelBase : AssetViewModelBase, IHasAssetChildrenViewModel
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// The collection of assets belonging to this container
        /// </summary>
        public ObservableCollection<IAssetViewModel> AssetChildren
        {
            get { return m_assetChildren; }
            set
            {
                SetPropertyValue(value, () => AssetChildren,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_assetChildren = (ObservableCollection<IAssetViewModel>)newValue;
                        }
                ));
            }
        }
        protected ObservableCollection<IAssetViewModel> m_assetChildren;

        /// <summary>
        /// Collection of the file assets this container has in it.
        /// </summary>
        public ObservableCollection<IGridViewModel> GridAssets
        {
            get { return m_gridAssets; }
            set
            {
                SetPropertyValue(value, () => GridAssets,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_gridAssets = (ObservableCollection<IGridViewModel>)newValue;
                        }
                ));
            }
        }
        protected ObservableCollection<IGridViewModel> m_gridAssets;
        #endregion // Properties and Associated Member Data

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public ContainerViewModelBase(IAsset model)
            : base(model)
        {
            this.AssetChildren = new ObservableCollection<IAssetViewModel>();
            this.AssetChildren.Add(new AssetDummyViewModel());

            this.GridAssets = new ObservableCollection<IGridViewModel>();
        }
        #endregion // Constructor

        #region IHasAssetChildrenViewModel Implementation
        /// <summary>
        /// Returns true if this container contains a asset child
        /// with the given name
        /// </summary>
        public Boolean ContainsAssetChild(String name)
        {
            lock (AssetChildren)
            {
                return (AssetChildren.FirstOrDefault(item => String.Compare(item.Name, name, false) == 0)) != null;
            }
        }

        /// <summary>
        /// Returns true if this container contains a asset child
        /// with the same model as the given
        /// </summary>
        public Boolean ContainsAssetChild(IAsset asset)
        {
            lock (AssetChildren)
            {
                return (AssetChildren.FirstOrDefault(item => item.Model == asset) != null);
            }
        }

        /// <summary>
        /// Returns the asset in this containers asset children
        /// with the given name or null if not present
        /// </summary>
        public IAssetViewModel GetAssetChild(String name)
        {
            lock (AssetChildren)
            {
                return AssetChildren.FirstOrDefault(item => String.Compare(item.Name, name, false) == 0);
            }
        }

        /// <summary>
        /// Returns the asset in this containers asset children
        /// with the given asset model or null if not present
        /// </summary>
        public IAssetViewModel GetAssetChild(IAsset asset)
        {
            lock (AssetChildren)
            {
                return AssetChildren.FirstOrDefault(item => item.Model == asset);
            }
        }

        /// <summary>
        /// Returns true if this container contains a grid asset
        /// with the given name
        /// </summary>
        public Boolean ContainsGridAsset(String name)
        {
            lock (GridAssets)
            {
                return (GridAssets.FirstOrDefault(item => String.Compare(item.Name, name, false) == 0) != null);
            }
        }

        /// <summary>
        /// Returns true if this container contains a grid asset
        /// with the model
        /// </summary>
        public Boolean ContainsGridAsset(IAsset asset)
        {
            lock (GridAssets)
            {
                return (GridAssets.FirstOrDefault(item => item.Model == asset) != null);
            }
        }

        /// <summary>
        /// Returns the grid asset in this containers grid assets
        /// with the given name or null if not present
        /// </summary>
        public IGridViewModel GetGridAsset(String name)
        {
            lock (GridAssets)
            {
                return GridAssets.FirstOrDefault(item => String.Compare(item.Name, name, false) == 0);
            }
        }

        /// <summary>
        /// Returns the grid asset in this containers grid assets
        /// with the given model or null if not present
        /// </summary>
        public IGridViewModel GetGridAsset(IAsset asset)
        {
            lock (GridAssets)
            {
                return GridAssets.FirstOrDefault(item => item.Model == asset);
            }
        }
        #endregion // IHasAssetChildrenViewModel Implementation
    } // ContainerViewModelBase
} // ContentBrowser.ViewModel
