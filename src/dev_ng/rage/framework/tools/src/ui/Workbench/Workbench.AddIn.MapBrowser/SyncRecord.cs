﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;

namespace Workbench.AddIn.MapBrowser
{
    /// <summary>
    /// Represents a sync record from perforce
    /// </summary>
    public class SyncRecord : ViewModelBase
    {
        #region Properties

        public Boolean Sync
        {
            get { return m_sync; }
            set
            {
                SetPropertyValue(value, () => this.Sync,
                    new PropertySetDelegate(delegate(Object newValue) { m_sync = (Boolean)newValue; }));
            }
        }
        private Boolean m_sync;

        public String Name
        {
            get;
            set;
        }

        public String FolderLocation
        {
            get;
            private set;
        }

        public String SyncPath
        {
            get;
            private set;
        }

        public String Size
        {
            get;
            private set;
        }

        public int SizeValue
        {
            get;
            private set;
        }

        public int HaveRevision
        {
            get;
            private set;
        }

        public int HeadRevision
        {
            get;
            private set;
        }

        public Boolean CanSync
        {
            get;
            private set;
        }

        #endregion // Properties

        #region Constructors

        /// <summary>
        /// 
        /// </summary>
        public SyncRecord(String depotFile, int size, int headRev, int haveRev)
        {
            this.HeadRevision = headRev;
            this.HaveRevision = haveRev;
            this.CanSync = headRev > haveRev;
            this.SizeValue = size;
            this.Name = System.IO.Path.GetFileName(depotFile);
            this.FolderLocation = System.IO.Path.GetDirectoryName(depotFile);
            this.SyncPath = depotFile;

            if (size < 1024 * 1024)
            {
                int kbytes = size >> 10;
                int kbyte = 1024;
                int leftover = size - (kbytes * kbyte);

                if (leftover > 0)
                {
                    this.Size = String.Format("{0} KB", ((double)size / (double)kbyte).ToString("F1"));
                }
                else
                {
                    this.Size = String.Format("{0} KB", size >> 10);
                }
            }
            else
            {
                int mbytes = size >> 20;
                int mbyte = 1024 * 1024;
                int leftover = size - (mbytes * mbyte);

                if (leftover > 0)
                {
                    this.Size = String.Format("{0} MB", ((double)size / (double)mbyte).ToString("F1"));
                }
                else
                {
                    this.Size = String.Format("{0} MB", size >> 20);
                }
            }
        }

        #endregion // Constructors
    }
}
