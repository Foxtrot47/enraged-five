﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Data;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using RSG.Pipeline.Automation.Common.Client;

namespace Workbench.AddIn.Automation.UI
{

    /// <summary>
    /// Automation clients view model.
    /// </summary>
    internal class AutomationClientsViewModel : AutomationTabViewModelBase
    {
        #region Properties
        /// <summary>
        /// Client view models.
        /// </summary>
        public ObservableCollection<AutomationClientViewModel> Clients
        {
            get { return m_Clients; }
            private set
            {
                SetPropertyValue(value, () => this.Clients,
                    new PropertySetDelegate(delegate(Object newValue) { m_Clients = (ObservableCollection<AutomationClientViewModel>)newValue; }));
            }
        }
        private ObservableCollection<AutomationClientViewModel> m_Clients;

        /// <summary>
        /// Automation Client View Clients View.
        /// </summary>
        public ICollectionView ClientsView
        {
            get { return m_ClientsView; }
            private set
            {
                SetPropertyValue(value, () => this.ClientsView,
                    new PropertySetDelegate(delegate(Object newValue) { m_ClientsView = (ICollectionView)newValue; }));
            }
        }
        private ICollectionView m_ClientsView;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public AutomationClientsViewModel(IEnumerable<WorkerStatus> status)
            : base("Clients")
        {
            IEnumerable<AutomationClientViewModel> clients = status.Select(s =>
                new AutomationClientViewModel(s));
            this.Clients = new ObservableCollection<AutomationClientViewModel>(clients);
            this.ClientsView = new CollectionView(this.Clients);
        }
        #endregion // Constructor(s)
    }

} // Workbench.AddIn.Automation.UI namespace
