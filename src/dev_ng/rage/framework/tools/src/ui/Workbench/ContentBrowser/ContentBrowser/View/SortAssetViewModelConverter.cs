﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using ContentBrowser.ViewModel;
using System.ComponentModel;

namespace ContentBrowser.View
{
    /// <summary>
    /// Converter for use in hierarchical data templates for sorting a list of asset container view model base objects
    /// </summary>
    public class SortAssetViewModelConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            IEnumerable<IAssetViewModel> vms = value as IEnumerable<IAssetViewModel>;
            CollectionViewSource cvs = new CollectionViewSource();
            cvs.Source = vms;
            cvs.SortDescriptions.Add(new SortDescription("Name", ListSortDirection.Ascending));
            return cvs.View;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
