﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Configuration;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;

namespace MetadataEditor.UI.View
{
    /// <summary>
    /// 
    /// </summary>
    public class MetadataOptionsViewModel : ViewModelBase
    {
        #region Members

        private String m_definitionRootLocation;
        private RelayCommand m_cancelCommand;
        private RelayCommand m_saveCommand;
        private RelayCommand m_browseCommand;
        private RelayCommand m_codeCommand;
        private RelayCommand m_assetCommand;
        private MetadataOptions m_windowView;
        private Boolean m_usingCodePath;
        private Boolean m_usingAssetPath;
        private Boolean m_usingAnotherPath;
        private String m_codePath;
        private String m_assetPath;

        #endregion // Members

        #region Properties

        /// <summary>
        /// The root directory that the definitions are to be loaded
        /// from.
        /// </summary>
        public String DefinitionRootLocation
        {
            get { return m_definitionRootLocation; }
            set
            {

                SetPropertyValue(value, () => this.DefinitionRootLocation,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_definitionRootLocation = (String)newValue;
                        }
                    )
                );
            }
        }

        public Boolean UsingCodePath
        {
            get { return m_usingCodePath; }
            set
            {
                if (m_usingCodePath == value)
                    return;

                SetPropertyValue(value, () => this.UsingCodePath,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_usingCodePath = (Boolean)newValue;
                        }
                ));
            }
        }

        public Boolean UsingAssetPath
        {
            get { return m_usingAssetPath; }
            set
            {
                if (m_usingAssetPath == value)
                    return;

                SetPropertyValue(value, () => this.UsingAssetPath,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_usingAssetPath = (Boolean)newValue;
                        }
                ));
            }
        }

        public Boolean UsingAnotherPath
        {
            get { return m_usingAnotherPath; }
            set
            {
                if (m_usingAnotherPath == value)
                    return;

                SetPropertyValue(value, () => this.UsingAnotherPath,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_usingAnotherPath = (Boolean)newValue;
                        }
                ));
            }
        }

        #endregion // Properties

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rootLocation"></param>
        /// <param name="view"></param>
        /// <param name="gv"></param>
        public MetadataOptionsViewModel(IBranch branch, String rootLocation, MetadataOptions view)
        {
            this.DefinitionRootLocation = rootLocation;
            this.m_assetPath = System.IO.Path.Combine(branch.Metadata, "definitions");
            this.m_codePath = branch.Code;
            this.m_windowView = view;
            if (this.DefinitionRootLocation == this.m_assetPath)
            {
                this.UsingAssetPath = true;
            }
            else if (this.DefinitionRootLocation == this.m_codePath)
            {
                this.UsingCodePath = true;
            }
            else
            {
                this.UsingAnotherPath = true;
            }
        }

        #endregion // Constructor

        #region Commands

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (m_cancelCommand == null)
                {
                    m_cancelCommand = new RelayCommand(param => this.Cancel(param), param => this.CanCancel(param));
                }
                return m_cancelCommand;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand SaveCommand
        {
            get
            {
                if (m_saveCommand == null)
                {
                    m_saveCommand = new RelayCommand(param => this.Save(param), param => this.CanSave(param));
                }
                return m_saveCommand;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand BrowseCommand
        {
            get
            {
                if (m_browseCommand == null)
                {
                    m_browseCommand = new RelayCommand(param => this.Browse(param), param => this.CanBrowse(param));
                }
                return m_browseCommand;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand CodeCommand
        {
            get
            {
                if (m_codeCommand == null)
                {
                    m_codeCommand = new RelayCommand(param => this.SetToCode(param), param => this.CanSetPredefined(param));
                }
                return m_codeCommand;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand AssetCommand
        {
            get
            {
                if (m_assetCommand == null)
                {
                    m_assetCommand = new RelayCommand(param => this.SetToAsset(param), param => this.CanSetPredefined(param));
                }
                return m_assetCommand;
            }
        }

        #endregion // Commands

        #region Command Callbacks

        /// <summary>
        /// 
        /// </summary>
        private Boolean CanCancel(Object param)
        {
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        private void Cancel(Object param)
        {
            this.m_windowView.DialogResult = false;
        }

        /// <summary>
        /// 
        /// </summary>
        private Boolean CanSave(Object param)
        {
            if (!String.IsNullOrEmpty(this.DefinitionRootLocation))
            {
                if (System.IO.Directory.Exists(this.DefinitionRootLocation))
                    return true;
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        private void Save(Object param)
        {
            this.m_windowView.DialogResult = true;
        }

        /// <summary>
        /// 
        /// </summary>
        private Boolean CanBrowse(Object param)
        {
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        private void Browse(Object param)
        {
            System.Windows.Forms.FolderBrowserDialog dlg = new System.Windows.Forms.FolderBrowserDialog();
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (!String.IsNullOrEmpty(dlg.SelectedPath) && System.IO.Directory.Exists(dlg.SelectedPath))
                {
                    this.DefinitionRootLocation = dlg.SelectedPath;
                }
            }
            else
            {
                if (this.DefinitionRootLocation == this.m_assetPath)
                {
                    this.UsingAssetPath = true;
                }
                else if (this.DefinitionRootLocation == this.m_codePath)
                {
                    this.UsingCodePath = true;
                }
                else
                {
                    this.UsingAnotherPath = true;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private Boolean CanSetPredefined(Object param)
        {
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        private void SetToCode(Object param)
        {
            this.DefinitionRootLocation = this.m_codePath;
        }

        /// <summary>
        /// 
        /// </summary>
        private void SetToAsset(Object param)
        {
            this.DefinitionRootLocation = this.m_assetPath;
        }

        #endregion // Command Callbacks
    } // MetadataOptionsViewModel

    /// <summary>
    /// A command whose sole purpose is to  relay its functionality 
    /// to other objects by invoking delegates. The default return 
    /// value for the CanExecute method is 'true'.
    /// </summary>
    public class RelayCommand : System.Windows.Input.ICommand
    {
        #region Fields

        private readonly Action<Object> m_execute;
        private readonly Predicate<Object> m_canExecute;

        #endregion // Fields

        #region Constructors

        /// <summary>
        /// Creates a new command that can always execute.
        /// </summary>
        public RelayCommand(Action<Object> execute)
            : this(execute, null)
        {
        }

        /// <summary>
        /// Creates a new command.
        /// </summary>
        public RelayCommand(Action<Object> execute, Predicate<Object> canExecute)
        {
            if (execute == null)
                throw new ArgumentNullException("execute");

            m_execute = execute;
            m_canExecute = canExecute;
        }

        #endregion // Constructors

        #region ICommand Members

        /// <summary>
        /// Gets called when the manager is deciding if this
        /// command can be executed. This returns true be default
        /// </summary>
        public bool CanExecute(Object parameter)
        {
            if (m_canExecute != null)
                return m_canExecute(parameter);

            return true;
        }

        /// <summary>
        /// Gets called when the command is executed. This just
        /// relays the execution to the delegate function
        /// </summary>
        public void Execute(Object parameter)
        {
            if (m_execute != null)
                m_execute(parameter);
        }

        /// <summary>
        /// Makes sure that this command is hooked up into the
        /// command manager
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add { System.Windows.Input.CommandManager.RequerySuggested += value; }
            remove { System.Windows.Input.CommandManager.RequerySuggested -= value; }
        }

        #endregion // ICommand Members
    } // RelayCommand
}
