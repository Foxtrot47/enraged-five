﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn.UI.Layout;

namespace Workbench.AddIn.Services.File
{
    public interface IWorkbenchOpenService
    {
        #region Methods

        void OpenFileWithService(IOpenService openService, String filename);

        /// <summary>
        /// Overload of the OpenFileWithService that allows the user to attach some generic data
        /// to the document
        /// </summary>
        /// <param name="openService"></param>
        /// <param name="filename"></param>
        /// <param name="data"></param>
        void OpenFileWithService(IOpenService openService, String filename, object data);

        void OpenFileWithService(IOpenService openService, List<String> filenames);

        #endregion // Methods

    } // IWorkbenchOpenService
} // Workbench.AddIn.Services.File
