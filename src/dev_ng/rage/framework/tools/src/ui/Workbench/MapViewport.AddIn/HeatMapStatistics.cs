﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using RSG.Base.Drawing;

namespace MapViewport.AddIn
{
    /// <summary>
    /// 
    /// </summary>
    public class HeatMapStatistics
    {
        #region Private member fields

        private Dictionary<object, double> m_values;
        private double m_min;
        private double m_max;

        #endregion

        #region Public properties

        /// <summary>
        /// When set to true, the percentage value is inversed; 1-n is used instead of n where n is between 0..1 inclusive. The default value is True.
        /// </summary>
        public bool InverseColouring
        {
            get;
            set;
        }

        /// <summary>
        /// Use a logarithmic scale to determine the colours for the map. Default value is false.
        /// </summary>
        public bool UseLogScale
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        public HeatMapStatistics()
        {
            InverseColouring = true;
            UseLogScale = false;

            m_values = new Dictionary<object, double>();
            m_min = double.MaxValue;
            m_max = double.MinValue;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Clear the values.
        /// </summary>
        public void Clear()
        {
            m_values.Clear();
        }

        /// <summary>
        /// Add an object and its value.
        /// </summary>
        /// <param name="obj">Object.</param>
        /// <param name="value">Value.</param>
        public void Add(object obj, double value)
        {
            m_values[obj] = value;

            if (value > m_max)
            {
                m_max = value;
                if(m_max > 40)
                {
                    int i = 5;
                    i++;
                }
            }

            if (value < m_min)
            {
                m_min = value;
            }
        }

        /// <summary>
        /// Get the colour associated with the value of the object.
        /// </summary>
        /// <param name="obj">Object.</param>
        /// <returns>The colour based upon it's value.</returns>
        public Color GetColor(object obj)
        {
            if (m_values.Count == 1 && m_values.ContainsKey(obj))
            {
                // If we only have one value, then we return 100% because that makes 
                // more sense than returning zero.
                return GetColor(1);
            }

            double range = m_max - m_min;
            double value = m_values.ContainsKey(obj) ? m_values[obj] - m_min : 0;
            double percentage = (value / range);
            if (UseLogScale)
            {
                percentage = LogScale(percentage);
            }
            
            return GetColor(percentage);
        }

        /// <summary>
        /// Get the colour associated with the percentage of the object.
        /// </summary>
        /// <param name="percentage">Percentage.</param>
        /// <returns>The colour associated with the percentage value.</returns>
        public Color GetColor(double percentage)
        {
            percentage = InverseColouring ? percentage : 1 - percentage;

            // Uses green -> red range (dividing by 3 below is same as *0.33333 which equates to red in hues between 0 and 1)
            byte h = (byte)((percentage / 3.0) * 255);
            byte s = (byte)(0.9 * 255);
            byte v = (byte)(0.9 * 255);
            var color = ColourSpaceConv.HSVtoColor(new HSV(h, s, v));
            return Color.FromRgb(color.R, color.G, color.B);
        }

        /// <summary>
        /// Get the statistic for the key.
        /// </summary>
        /// <param name="key">Key.</param>
        /// <returns>Statistic.</returns>
        public double GetStatistic(object key)
        {
            if (m_values.ContainsKey(key))
            {
                return m_values[key];
            }

            return 0;
        }

        private double LogScale(double percentage)
        {
            double start = 1;
            double ratio = percentage * 9;
            start += ratio;
            return Math.Log10(start);
        }

        #endregion
    }
}
