﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using System.Text.RegularExpressions;
using Workbench.AddIn.Services;

namespace Workbench.AddIn.UI.Layout
{
    public interface IContentBase : ISearchableContent
    {
        Guid ID { get; }

        IModel SaveModel { get; }

        String Path { get; set; }

        String Title { get; set; }

        Boolean IsModified { get; set; }
    }
}
