﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Workbench.AddIn.GameStatistics
{
    /// <summary>
    /// Common game statistics extension points
    /// </summary>
    public static class ExtensionPoints
    {
        /// <summary>
        /// 
        /// </summary>
        public const String MissionReport = "E53F0E31-3850-4528-AC3E-B4A1EBA33739";

        /// <summary>
        /// 
        /// </summary>
        public const String AutomatedReport = "E614AA7B-4AFD-4E03-9872-C77CA2C97DD9";

        /// <summary>
        /// 
        /// </summary>
        public const String AutomatedBuildReport = "528CF4EC-1B1F-4F3D-8DEB-575DF30BE739";

        /// <summary>
        /// 
        /// </summary>
        public const String AutomatedDebugReport = "2D0C689F-1248-43D7-A194-50F7EA240335";
    } // ExtensionPoints
}
