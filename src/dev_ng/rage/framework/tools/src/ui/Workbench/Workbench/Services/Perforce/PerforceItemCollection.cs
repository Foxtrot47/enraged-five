﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;

namespace Workbench.Services.Perforce
{
    public class PerforceItemCollection : ModelBase
    {
        #region Members

        private System.Collections.ObjectModel.ObservableCollection<PerforceItem> m_items;

        #endregion // Members

        #region Properties

        /// <summary>
        /// The observable collection of perforce items that belong to this collection
        /// </summary>
        public System.Collections.ObjectModel.ObservableCollection<PerforceItem> Items
        {
            get { return m_items; }
            set
            {
                SetPropertyValue(value, () => this.Items,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_items = (System.Collections.ObjectModel.ObservableCollection<PerforceItem>)newValue;
                        }
                    )
                );
            }
        }

        #endregion // Properties

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public PerforceItemCollection(List<P4API.P4Record> records)
        {
            this.Items = new System.Collections.ObjectModel.ObservableCollection<PerforceItem>();
            foreach (P4API.P4Record record in records)
            {
                this.Items.Add(new PerforceItem(record));
            }
        }

        #endregion // Constructor
    } // PerforceItemCollection
} // Workbench.Services.Perforce
