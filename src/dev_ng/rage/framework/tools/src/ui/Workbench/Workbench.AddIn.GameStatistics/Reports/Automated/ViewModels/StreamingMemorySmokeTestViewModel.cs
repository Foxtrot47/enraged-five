﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Report.Reports.GameStatsReports.Tests;
using RSG.Model.Statistics.Captures;
using RSG.Base.Editor.Command;

namespace Workbench.AddIn.GameStatistics.Reports.Automated.ViewModels
{
    /// <summary>
    /// 
    /// </summary>
    public class StreamingMemorySmokeTestViewModel : SmokeTestViewModelBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public IList<StreamingMemoryResult> LatestResults
        {
            get { return m_latestResults; }
            set
            {
                SetPropertyValue(value, () => this.LatestResults,
                          new PropertySetDelegate(delegate(object newValue) { m_latestResults = (IList<StreamingMemoryResult>)newValue; }));
            }
        }
        private IList<StreamingMemoryResult> m_latestResults;

        /// <summary>
        /// 
        /// </summary>
        public IDictionary<string, IDictionary<string, StreamingMemoryStat>> GroupedResults
        {
            get { return m_groupedResults; }
            set
            {
                SetPropertyValue(value, () => this.GroupedResults,
                          new PropertySetDelegate(delegate(object newValue) { m_groupedResults = (IDictionary<string, IDictionary<string, StreamingMemoryStat>>)newValue; }));
            }
        }
        private IDictionary<string, IDictionary<string, StreamingMemoryStat>> m_groupedResults;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public StreamingMemorySmokeTestViewModel(StreamingMemorySmokeTest smokeTest, string testName)
            : base(smokeTest, testName)
        {
            if (smokeTest.LatestStats.ContainsKey(testName))
            {
                LatestResults = smokeTest.LatestStats[testName];
            }

            GroupedResults = new Dictionary<string, IDictionary<string, StreamingMemoryStat>>();
            foreach (string categoryName in LatestResults.SelectMany(item => item.Categories.Keys).Distinct())
            {
                IDictionary<string, StreamingMemoryStat> stats = new Dictionary<string, StreamingMemoryStat>();
                foreach (StreamingMemoryResult result in LatestResults)
                {
                    if (result.Categories.ContainsKey(categoryName))
                    {
                        StreamingMemoryStat stat = result.Categories[categoryName];

                        if ((stat.Physical != 0 || stat.Virtual != 0) && !stats.ContainsKey(result.ModuleName))
                        {
                            stats.Add(result.ModuleName, stat);
                        }
                    }
                }

                if (stats.Any())
                {
                    GroupedResults.Add(categoryName, stats);
                }
            }
        }
        #endregion // Constructor(s)
    } // StreamingMemorySmokeTestViewModel
}
