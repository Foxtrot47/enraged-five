﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using RSG.Model.Common;

namespace Workbench.AddIn.MapViewport.MapSupport
{
    /// <summary>
    /// Container for the alternative maps.
    /// </summary>
    public class MapContainer
    {
        #region Constants

        private static readonly string c_mapsWildcard = "map_*.xml";

        #endregion

        #region Private member fields

        private string m_mapFolder;
        private ILevel m_level;

        #endregion

        #region Public properties

        /// <summary>
        /// The default map.
        /// </summary>
        public AlternativeMap DefaultMap { get; private set; }

        /// <summary>
        /// Maps.
        /// </summary>
        public IDictionary<string, AlternativeMap> Maps
        {
            get;
            private set;
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="level">Level.</param>
        /// <param name="mapFolder"></param>
        public MapContainer(ILevel level, string mapFolder)
        {
            m_level = level;
            m_mapFolder = mapFolder;
            Maps = new Dictionary<string, AlternativeMap>();
            LoadMaps();
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Get the map names.
        /// </summary>
        /// <returns>Enumeration of map keys.</returns>
        public IEnumerable<string> GetMapNames()
        {
            foreach(var key in Maps.Keys)
            {
                yield return key;
            }
        }

        #endregion

        #region Private helper methods

        /// <summary>
        /// Load maps.
        /// </summary>
        private void LoadMaps()
        {
            if (!m_level.ImageBounds.IsEmpty)
            {
                DefaultMap = AlternativeMap.Create(m_level.Image, m_level.ImageBounds);
                DefaultMap.Name = m_level.Name.ToUpper() + " Map (default)";
                Maps.Add(DefaultMap.Name, DefaultMap);
            }

            if (Directory.Exists(m_mapFolder))
            {
                string[] maps = Directory.GetFiles(m_mapFolder, c_mapsWildcard);
                foreach (string map in maps)
                {
                    LoadMap(map);
                }
            }
        }

        /// <summary>
        /// Load a single map.
        /// </summary>
        /// <param name="mapPath">Path to the map file.</param>
        private void LoadMap(string mapPath)
        {
            if (!File.Exists(mapPath))
            {
                return;
            }

            AlternativeMap map = AlternativeMap.Deserialize(mapPath);
            Maps.Add(map.Name, map);
        }

        #endregion
    }
}
