﻿using System;
using System.Collections.Generic;
using System.Windows;
using RSG.Metadata.Data;
using MetadataEditor.AddIn.View;
using Workbench.AddIn;
using MetadataEditor.AddIn.MetaEditor.ViewModel;
using System.Text.RegularExpressions;
using MetadataEditor.AddIn.MetaEditor.BasicViewModel;
using Workbench.AddIn.Services;
using Workbench.AddIn.UI.Layout;
using System.Windows.Media;
using System.Windows.Controls;
using RSG.Base.Windows.Controls;

namespace MetadataEditor.AddIn.MetaEditor
{
    /// <summary>
    /// Interaction logic for MetaEditorBasicView.xaml
    /// </summary>
    public partial class MetaEditorBasicView : Workbench.AddIn.UI.Layout.DocumentBase<MetaFileViewModel>, IMetadataView
    {
        #region Fields
        internal static bool SelectingSearchResult;
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public MetaEditorBasicView()
            : base(MetaEditorBasicViewModel.DOCUMENT_NAME, new MetaFileViewModel(null))
        {
            InitializeComponent();
            this.ID = new Guid(CompositionPoints.MetaEditorBasic);
            this.Name = MetaEditorBasicViewModel.DOCUMENT_NAME;
            this.SaveModel = this.ViewModel.Model;
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public MetaEditorBasicView(MetaFileViewModel viewModel)
            : base(MetaEditorBasicViewModel.DOCUMENT_NAME, viewModel)
        {
            InitializeComponent();
            this.ID = new Guid(CompositionPoints.MetaEditorBasic);
            this.Name = String.Format("{0}_{1}", MetaEditorBasicViewModel.DOCUMENT_NAME, viewModel.GetHashCode().ToString());

            String[] filenames = viewModel.Model.Filename.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (filenames.Length == 1)
                this.Title = System.IO.Path.GetFileName(filenames[0]);
            else if (filenames.Length > 1)
                this.Title = "Multi Document";
            this.SaveModel = viewModel.Model;
        }
        #endregion

        #region Methods
        protected override void OnClosed()
        {
            base.OnClosed();
            MetaFileViewModel viewModel = this.ViewModel;
            if (viewModel != null && viewModel.IsLiveEditing)
            {
                viewModel.DisableLiveLink();
            }
        }

        /// <summary>
        /// Gets the view model as a IMetadataViewModel to support live editing a generic
        /// metadata file.
        /// </summary>
        public IMetadataViewModel GetMetadataViewModel()
        {
            return this.ViewModel;
        }

        private IEnumerable<ITunableViewModel> GetMembers()
        {
            if (this.Gridview.IsChecked.HasValue && this.Gridview.IsChecked.Value)
            {
                foreach (var item in ViewModel.BreadCrumbs)
                {
                    yield return item;
                }
            }
            else
            {
                foreach (var item in ViewModel.Members.Values)
                {
                    yield return item;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="previousResult"></param>
        /// <returns></returns>
        public override object FindNext(ISearchQuery query, object startObject)
        {
            ITunableViewModel first = startObject as ITunableViewModel;
            ITunableViewModel found = null;
            if ((query.UseTag && query.Tag != "Tunables") || query.Expression == null)
                return found;

            ITunableViewModel topTunable = null;
            foreach (var tunable in GetMembers()) // this.ViewModel.Members.Values)
            {
                topTunable = tunable;
                if (first == null)
                {
                    first = tunable;
                }

                break;
            }

            if (first != null)
                found = first.FindNext(query.Expression);

            if (found == null)
            {
                if (topTunable != null && topTunable != first)
                {
                    found = topTunable.FindNext(query.Expression);
                }
            }

            if (found != null)
            {
                this.ViewModel.UnSelectAll(Gridview.IsChecked.HasValue && Gridview.IsChecked.Value);
                found.IsSelected = true;
                SetFocus(found);
            }

            return found;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="previousResult"></param>
        /// <returns></returns>
        public override object FindPrevious(ISearchQuery query, object startObject)
        {
            ITunableViewModel start = startObject as ITunableViewModel;
            ITunableViewModel found = null;
            if ((query.UseTag && query.Tag != "Tunables") || query.Expression == null)
                return found;

            if (start == null)
            {
                foreach (var tunable in GetMembers()) // this.ViewModel.Members.Values)
                {
                    start = tunable;
                    break;
                }
            }
            if (start != null)
                found = start.FindPrevious(query.Expression);

            if (found != null)
            {
                this.ViewModel.UnSelectAll(Gridview.IsChecked.HasValue && Gridview.IsChecked.Value);
                found.IsSelected = true;
                SetFocus(found);
            }

            return found;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public override List<ISearchResult> FindAll(ISearchQuery query)
        {
            if ((query.UseTag && query.Tag != "Tunables") || query.Expression == null)
                return new List<ISearchResult>();

            ITunableViewModel start = null;
            foreach (var tunable in  GetMembers()) // this.ViewModel.Members.Values)
            {
                start = tunable;
                break;
            }

            List<ISearchResult> results = new List<ISearchResult>();
            List<ITunableViewModel> found = new List<ITunableViewModel>();
            if (start != null)
                found = start.FindAll(query.Expression);

            foreach (var find in found)
            {
                string text = string.Empty;
                ITunableViewModel parent = find.Parent;
                while (parent != null)
                {
                    string name = parent.Name;
                    if (parent is StructureTunableViewModel)
                    {
                        StructureTunable model = (parent as StructureTunableViewModel).Model as StructureTunable;
                        if (model != null && model.TunableKey != null && model.TunableKey is StringTunable)
                        {
                            if (!string.IsNullOrWhiteSpace((model.TunableKey as StringTunable).Value))
                                name = (model.TunableKey as StringTunable).Value;
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(parent.Name))
                        text = name + "." + text;

                    parent = parent.Parent;
                }
                if (find is MapItemTunableViewModel)
                {
                    string key = ((find as MapItemTunableViewModel).RawModel as MapItemTunable).Key;
                    text += key;
                    text = "Map Entry - " + text;
                }
                else if (find is MapTunableViewModel)
                {
                    text += find.Name;
                    text = string.Format("{0} - {1}", (find as MapTunableViewModel).FriendlyTypeName, text);
                }
                else if (find is ArrayTunableViewModel)
                {
                    text += find.Name;
                    text = string.Format("{0} - {1}", (find as ArrayTunableViewModel).FriendlyTypeName, text);
                }
                else
                {
                    text += find.Name;
                    text = string.Format("{0} - {1}", find.FriendlyTypeName, text);

                    if (find.Model is StringTunable)
                    {
                        text += "." + (find.Model as StringTunable).Value;
                    }
                    else if (find.Model is EnumTunable)
                    {
                        text += "." + (find.Model as EnumTunable).ValueAsString;
                    }
                    else if (find.Model is BitsetTunable)
                    {
                        text += "." + (find.Model as BitsetTunable).Value;
                    }
                }
                results.Add(new SearchResult(find, text, this));
            }

            return results;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="result"></param>
        public override void SelectSearchResult(ISearchResult result)
        {
            SelectingSearchResult = true;
            if (result == null || result.Data is ITunableViewModel)
            {
                SelectViewModel(result.Data as ITunableViewModel);
            }
            SelectingSearchResult = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override List<object> GetCurrentSelection()
        {
            return GetSelection();
        }

        /// <summary>
        /// 
        /// </summary>
        private ITunableViewModel FindViewModelFromModel(StructureTunable model)
        {
            ITunableViewModel viewModel = null;
            foreach (ITunableViewModel tunableViewModel in  GetMembers()) // this.ViewModel.Members.Values)
            {
                viewModel = FindViewModelFromModel(tunableViewModel, model);
                if (viewModel != null)
                    return viewModel;
            }

            return viewModel;
        }

        /// <summary>
        /// 
        /// </summary>
        private ITunableViewModel FindViewModelFromModel(ITunableViewModel root, StructureTunable model)
        {
            if (root.Model == model)
                return root;

            if (root is HierarchicalTunableViewModel)
            {
                foreach (ITunableViewModel child in (root as HierarchicalTunableViewModel).Members)
                {
                    ITunableViewModel viewModel = FindViewModelFromModel(child, model);
                    if (viewModel != null)
                        return viewModel;
                }
            }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private List<object> GetSelection()
        {
            List<object> selection = new List<object>();
            foreach (ITunableViewModel tunableViewModel in GetMembers()) //   this.ViewModel.Members.Values)
            {
                selection.AddRange(GetSelection(tunableViewModel));
            }

            return selection;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="root"></param>
        /// <returns></returns>
        private List<object> GetSelection(ITunableViewModel root)
        {
            List<object> selection = new List<object>();
            if (root.IsSelected)
                selection.Add(root);

            if (root is HierarchicalTunableViewModel)
            {
                foreach (ITunableViewModel child in (root as HierarchicalTunableViewModel).Members)
                {
                    selection.AddRange(GetSelection(child));
                }
            }
            else if (root is MetadataEditor.AddIn.MetaEditor.GridViewModel.GridTunableViewModel)
            {
                foreach (ITunableViewModel child in (root as MetadataEditor.AddIn.MetaEditor.GridViewModel.GridTunableViewModel).Children)
                {
                    selection.AddRange(GetSelection(child));
                }
            }
            return selection;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="result"></param>
        public void SelectViewModel(ITunableViewModel viewModel)
        {
            bool useBreadcrumbs = Gridview.IsChecked.HasValue && Gridview.IsChecked.Value;
            this.ViewModel.UnSelectAll(useBreadcrumbs);
            viewModel.IsSelected = true;
            SetFocus(viewModel);
        }
        #endregion

        #region Private helper methods

        /// <summary>
        /// Find the control that has the ITunableViewModel as its data context and set the focus to it.
        /// </summary>
        /// <param name="found">View model.</param>
        private void SetFocus(ITunableViewModel found)
        {
            // TODO: Set focus to the control. Traipse through the hierarchy trying to find it.

            if (Gridview.IsChecked.HasValue && Gridview.IsChecked.Value)
            {
                SetFocus(found, gridView);
            }
            else
            {
                SetFocus(found, treeView);
            }
        }

        /// <summary>
        /// Give the control focus.
        /// </summary>
        /// <param name="dataContext">Data context of the control.</param>
        /// <param name="depObj">Dependency object.</param>
        /// <returns>True if a control was set focus.</returns>
        private bool SetFocus(ITunableViewModel dataContext, FrameworkElement depObj)
        {
            if (depObj == null)
            {
                return false;
            }

            object dcValue = depObj.GetValue(DataContextProperty);

            if (depObj is FloatUpDown)
            {
                var floatUpDown = depObj as FloatUpDown;
                depObj.Focus();
                depObj.InvalidateVisual();

                TextBox textBox = FindControl<TextBox>(depObj);
                if (textBox != null)
                {
                    depObj = textBox;
                }
            }

            if (dcValue != null && dcValue == dataContext && !(depObj is ContentPresenter || depObj is FloatUpDown) && depObj is Control) // depObj is TextBox || depObj is ComboBox || depObj is ListBox)  //
            {
                return ((Control)depObj).Focus();
            }

            int childCount = VisualTreeHelper.GetChildrenCount(depObj);
            for (int i = 0; i < childCount; i++)
            {
                bool result = SetFocus(dataContext, VisualTreeHelper.GetChild(depObj, i) as FrameworkElement);
                if (result)
                {
                    return result;
                }
            }

            return false;
        }

        /// <summary>
        /// Find a control of the specific type in a visual hierarchy.
        /// </summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="target">Object to search in.</param>
        /// <returns>The first instance of the specified type.</returns>
        private T FindControl<T>(DependencyObject target) where T : DependencyObject
        {
            int count = VisualTreeHelper.GetChildrenCount(target);
            for (int i = 0; i < count; i++)
            {
                var child = VisualTreeHelper.GetChild(target, i);

                if (child is T)
                {
                    return (T)child;
                }

                T grandChild = FindControl<T>(child);
                if (grandChild != null)
                {
                    return grandChild;
                }
            }

            return default(T);
        }

        #endregion
    }


    /// <summary>
    /// 
    /// </summary>
    public class SearchResult : ISearchResult
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public object Data
        {
            get { return m_data; }
        }
        private object m_data = null;

        /// <summary>
        /// 
        /// </summary>
        public bool CanJumpTo
        {
            get { return true; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string DisplayText
        {
            get { return m_displayText; }
        }
        private string m_displayText;

        /// <summary>
        /// 
        /// </summary>
        public IContentBase Content
        {
            get { return m_content; }
            set { m_content = value; }
        }
        private IContentBase m_content;
        #endregion // Properties

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="text"></param>
        /// <param name="content"></param>
        public SearchResult(object data, string text, IContentBase content)
        {
            this.m_data = data;
            this.m_displayText = text;
            this.m_content = content;
        }
        #endregion
    } // SearchResult
} // MetadataEditor.AddIn.MetaEditor namespace
