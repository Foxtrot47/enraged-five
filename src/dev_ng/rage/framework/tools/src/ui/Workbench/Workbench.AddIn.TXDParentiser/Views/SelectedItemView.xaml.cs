﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Workbench.AddIn.TXDParentiser.Views
{
    /// <summary>
    /// Interaction logic for SelectedItemView.xaml
    /// </summary>
    public partial class SelectedItemView : UserControl
    {
        public SelectedItemView()
        {
            InitializeComponent();
        }
    }

    public class SelectdItemTemplateSelector : DataTemplateSelector
    {
        #region DataTemplateSelector Members

        /// <summary>
        /// 
        /// </summary>
        public override DataTemplate SelectTemplate(Object item, DependencyObject container)
        {
            DataTemplate dt = null;
            ContentPresenter cp = container as ContentPresenter;
            if (item is Workbench.AddIn.TXDParentiser.ViewModel.GlobalTextureDictionaryViewModel)
            {
                dt = cp.FindResource("SelectedGlobalDictionaryView") as DataTemplate;
            }
            System.Collections.ObjectModel.Collection<Object> collection = item as System.Collections.ObjectModel.Collection<Object>;
            if (collection != null)
            {
                if (collection.Count > 0)
                {

                }
            }
            if (dt == null)
                dt = cp.FindResource("SelectedNothingView") as DataTemplate;
            
            return dt;
        }

        #endregion // DataTemplateSelector
    } // SelectdItemTemplateSelector
}
