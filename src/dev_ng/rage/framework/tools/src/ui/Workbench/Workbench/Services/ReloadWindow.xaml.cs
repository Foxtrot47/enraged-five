﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Workbench.Services
{
    /// <summary>
    /// Interaction logic for ReloadWindow.xaml
    /// </summary>
    public partial class ReloadWindow : Window
    {
        public ReloadWindow()
        {
            InitializeComponent();
        }

        private void YesClicked(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void NoClicked(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
