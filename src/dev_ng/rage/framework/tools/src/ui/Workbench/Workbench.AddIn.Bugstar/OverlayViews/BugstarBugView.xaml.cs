﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;

namespace Workbench.AddIn.Bugstar.OverlayViews
{
    /// <summary>
    /// Interaction logic for BugstarBugView.xaml
    /// </summary>
    public partial class BugstarBugView : UserControl
    {
        public BugstarBugView()
        {
            InitializeComponent();
        }

        private void BugHyperlink_RequestNavigate(object sender, System.Windows.Navigation.RequestNavigateEventArgs e)
        {
            string uri = e.Uri.AbsoluteUri;
            Process.Start(new ProcessStartInfo(uri));

            e.Handled = true;
        }
    }
}
