﻿using System;
using System.Windows;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn.UI.Layout;

namespace Workbench.AddIn.Services.File
{
    /// <summary>
    /// Responsible for mantain the states of the files that are currently open
    /// and making sure the correct messages are sent. Also responsible for registering
    /// and unregisting files with other services.
    /// </summary>
    public interface IFileManager
    {
        #region Controller Methods

        Boolean OnExiting();

        #endregion // Controller Methods
    } // IFileManager
} // Workbench.AddIn.Services.File
