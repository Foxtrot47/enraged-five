﻿using System;
using System.Collections.Generic;
using RSG.Model.Common.Map;

namespace Workbench.AddIn.MapStatistics.Overlays
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(ExtensionPoints.PolygonStatisticsOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class PolygonInstanceCountOverlay : StatisticOverlayBase
    {
        #region Constants
        private const string c_name = "Entity Polygon Count";
        private const string c_description = "";
        #endregion // Constants

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public PolygonInstanceCountOverlay()
            : base(c_name, c_description)
        {
            m_includeHighDetailed = true;
            m_includeLod = true;
            m_includeSuperLod = true;
            m_includeDynamic = true;
            m_includeNonDynamic = true;
            m_includeInterior = false;

            m_dontShowObjectOptions = false;
            m_dontShowReferenceOptions = false;
            m_dontShowInteriorOptions = true;
            m_dontShowUniqueOptions = true;
        }
        #endregion // Constructor

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        protected uint GetStatisticValue(IMapSection section)
        {
            if (!this.IncludeDynamic && !this.IncludeNonDynamic)
            {
                return 0;
            }
            if (!this.IncludeDrawableEntities && !this.IncludeNonDrawableEntities)
            {
                return 0;
            }

            StatEntityType type = StatEntityType.Total;
            if (this.IncludeDynamic && !this.IncludeNonDynamic)
            {
                type = StatEntityType.Dynamic;
            }
            else if (!this.IncludeDynamic && this.IncludeNonDynamic)
            {
                type = StatEntityType.NonDynamic;
            }

            uint stat = 0;
            if (this.IncludeDrawableEntities && !this.IncludeNonDrawableEntities)
            {
                stat = AddStatistic(section, StatGroup.DrawableEntities, type);
            }
            else if (!this.IncludeDrawableEntities && this.IncludeNonDrawableEntities)
            {
                stat = AddStatistic(section, StatGroup.NonDrawableEntities, type);
            }
            else
            {
                stat = AddStatistic(section, StatGroup.AllNonInteriorEntities, type);
            }
            return stat;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stat"></param>
        /// <param name="stats"></param>
        /// <param name="group"></param>
        /// <param name="type"></param>
        private uint AddStatistic(IMapSection section, StatGroup group, StatEntityType type)
        {
            uint stat = 0;

            if (this.IncludeHighDetailed)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Hd, type)];
                stat += stats.PolygonCount;
            }
            if (this.IncludeLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Lod, type)];
                stat += stats.PolygonCount;
            }
            if (this.IncludeSuperLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.SLod1, type)];
                stat += stats.PolygonCount;
            }

            return stat;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="propgroup"></param>
        /// <returns></returns>
        protected override double GetStatisticValue(IMapSection section, IMapSection propgroup)
        {
            uint polygonCount = 0;
            if (section != null)
                polygonCount += GetStatisticValue(section);
            if (propgroup != null)
                polygonCount += GetStatisticValue(propgroup);

            return polygonCount;
        }
        #endregion // Overrides
    } // PolygonInstanceCountOverlay

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(ExtensionPoints.PolygonStatisticsOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class CollisionPolygonInstanceCountOverlay : StatisticOverlayBase
    {
        #region Constants
        private const string c_name = "Collision Polygon Count";
        private const string c_description = "";
        #endregion // Constants

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public CollisionPolygonInstanceCountOverlay()
            : base(c_name, c_description)
        {
            m_includeHighDetailed = true;
            m_includeLod = true;
            m_includeSuperLod = true;
            m_includeDynamic = true;
            m_includeNonDynamic = true;
            m_includeInterior = false;

            m_dontShowObjectOptions = false;
            m_dontShowReferenceOptions = false;
            m_dontShowInteriorOptions = true;
            m_dontShowUniqueOptions = true;
        }
        #endregion // Constructor

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        protected uint GetStatisticValue(IMapSection section)
        {
            if (!this.IncludeDynamic && !this.IncludeNonDynamic)
            {
                return 0;
            }
            if (!this.IncludeDrawableEntities && !this.IncludeNonDrawableEntities)
            {
                return 0;
            }

            StatEntityType type = StatEntityType.Total;
            if (this.IncludeDynamic && !this.IncludeNonDynamic)
            {
                type = StatEntityType.Dynamic;
            }
            else if (!this.IncludeDynamic && this.IncludeNonDynamic)
            {
                type = StatEntityType.NonDynamic;
            }

            uint stat = 0;
            if (this.IncludeDrawableEntities && !this.IncludeNonDrawableEntities)
            {
                stat += AddStatistic(section, StatGroup.DrawableEntities, type);
            }
            else if (!this.IncludeDrawableEntities && this.IncludeNonDrawableEntities)
            {
                stat += AddStatistic(section, StatGroup.NonDrawableEntities, type);
            }
            else
            {
                stat += AddStatistic(section, StatGroup.AllNonInteriorEntities, type);
            }
            return stat;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stat"></param>
        /// <param name="stats"></param>
        /// <param name="group"></param>
        /// <param name="type"></param>
        private uint AddStatistic(IMapSection section, StatGroup group, StatEntityType type)
        {
            uint stat = 0;

            if (this.IncludeHighDetailed)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Hd, type)];
                stat += stats.CollisionCount;
            }
            if (this.IncludeLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Lod, type)];
                stat += stats.CollisionCount;
            }
            if (this.IncludeSuperLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.SLod1, type)];
                stat += stats.CollisionCount;
            }

            return stat;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="propgroup"></param>
        /// <returns></returns>
        protected override double GetStatisticValue(IMapSection section, IMapSection propgroup)
        {
            uint polygonCount = 0;
            if (section != null)
                polygonCount += GetStatisticValue(section);
            if (propgroup != null)
                polygonCount += GetStatisticValue(propgroup);

            return polygonCount;
        }
        #endregion // Overrides
    } // CollisionPolygonInstanceCountOverlay

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(ExtensionPoints.PolygonStatisticsOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class MoverPolygonInstanceCountOverlay : StatisticOverlayBase
    {
        #region Constants
        private const string c_name = "Mover Polygon Count";
        private const string c_description = "";
        #endregion // Constants

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public MoverPolygonInstanceCountOverlay()
            : base(c_name, c_description)
        {
            m_includeHighDetailed = true;
            m_includeLod = true;
            m_includeSuperLod = true;
            m_includeDynamic = true;
            m_includeNonDynamic = true;
            m_includeInterior = false;

            m_dontShowObjectOptions = false;
            m_dontShowReferenceOptions = false;
            m_dontShowInteriorOptions = true;
            m_dontShowUniqueOptions = true;
        }
        #endregion // Constructor

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        protected uint GetStatisticValue(IMapSection section)
        {
            if (!this.IncludeDynamic && !this.IncludeNonDynamic)
            {
                return 0;
            }
            if (!this.IncludeDrawableEntities && !this.IncludeNonDrawableEntities)
            {
                return 0;
            }

            StatEntityType type = StatEntityType.Total;
            if (this.IncludeDynamic && !this.IncludeNonDynamic)
            {
                type = StatEntityType.Dynamic;
            }
            else if (!this.IncludeDynamic && this.IncludeNonDynamic)
            {
                type = StatEntityType.NonDynamic;
            }

            uint stat = 0;
            if (this.IncludeDrawableEntities && !this.IncludeNonDrawableEntities)
            {
                stat += AddStatistic(section, StatGroup.DrawableEntities, type);
            }
            else if (!this.IncludeDrawableEntities && this.IncludeNonDrawableEntities)
            {
                stat += AddStatistic(section, StatGroup.NonDrawableEntities, type);
            }
            else
            {
                stat += AddStatistic(section, StatGroup.AllNonInteriorEntities, type);
            }
            return stat;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stat"></param>
        /// <param name="stats"></param>
        /// <param name="group"></param>
        /// <param name="type"></param>
        private uint AddStatistic(IMapSection section, StatGroup group, StatEntityType type)
        {
            uint stat = 0;

            if (this.IncludeHighDetailed)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Hd, type)];
                stat += stats.GetPolyCountForCollisionType(CollisionType.Mover);
            }
            if (this.IncludeLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Lod, type)];
                stat += stats.GetPolyCountForCollisionType(CollisionType.Mover);
            }
            if (this.IncludeSuperLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.SLod1, type)];
                stat += stats.GetPolyCountForCollisionType(CollisionType.Mover);
            }

            return stat;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="propgroup"></param>
        /// <returns></returns>
        protected override double GetStatisticValue(IMapSection section, IMapSection propgroup)
        {
            uint polygonCount = 0;
            if (section != null)
                polygonCount += GetStatisticValue(section);
            if (propgroup != null)
                polygonCount += GetStatisticValue(propgroup);

            return polygonCount;
        }
        #endregion // Overrides
    } // MoverPolygonInstanceCountOverlay

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(ExtensionPoints.PolygonStatisticsOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class WeaponPolygonInstanceCountOverlay : StatisticOverlayBase
    {
        #region Constants
        private const string c_name = "Shooter Polygon Count";
        private const string c_description = "";
        #endregion // Constants

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public WeaponPolygonInstanceCountOverlay()
            : base(c_name, c_description)
        {
            m_includeHighDetailed = true;
            m_includeLod = true;
            m_includeSuperLod = true;
            m_includeDynamic = true;
            m_includeNonDynamic = true;
            m_includeInterior = false;

            m_dontShowObjectOptions = false;
            m_dontShowReferenceOptions = false;
            m_dontShowInteriorOptions = true;
            m_dontShowUniqueOptions = true;
        }
        #endregion // Constructor

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        protected uint GetStatisticValue(IMapSection section)
        {
            if (!this.IncludeDynamic && !this.IncludeNonDynamic)
            {
                return 0;
            }
            if (!this.IncludeDrawableEntities && !this.IncludeNonDrawableEntities)
            {
                return 0;
            }

            StatEntityType type = StatEntityType.Total;
            if (this.IncludeDynamic && !this.IncludeNonDynamic)
            {
                type = StatEntityType.Dynamic;
            }
            else if (!this.IncludeDynamic && this.IncludeNonDynamic)
            {
                type = StatEntityType.NonDynamic;
            }

            uint stat = 0;
            if (this.IncludeDrawableEntities && !this.IncludeNonDrawableEntities)
            {
                stat += AddStatistic(section, StatGroup.DrawableEntities, type);
            }
            else if (!this.IncludeDrawableEntities && this.IncludeNonDrawableEntities)
            {
                stat += AddStatistic(section, StatGroup.NonDrawableEntities, type);
            }
            else
            {
                stat += AddStatistic(section, StatGroup.AllNonInteriorEntities, type);
            }
            return stat;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stat"></param>
        /// <param name="stats"></param>
        /// <param name="group"></param>
        /// <param name="type"></param>
        private uint AddStatistic(IMapSection section, StatGroup group, StatEntityType type)
        {
            uint stat = 0;

            if (this.IncludeHighDetailed)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Hd, type)];
                stat += stats.GetPolyCountForCollisionType(CollisionType.Weapon);
            }
            if (this.IncludeLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Lod, type)];
                stat += stats.GetPolyCountForCollisionType(CollisionType.Weapon);
            }
            if (this.IncludeSuperLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.SLod1, type)];
                stat += stats.GetPolyCountForCollisionType(CollisionType.Weapon);
            }

            return stat;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="propgroup"></param>
        /// <returns></returns>
        protected override double GetStatisticValue(IMapSection section, IMapSection propgroup)
        {
            uint polygonCount = 0;
            if (section != null)
                polygonCount += GetStatisticValue(section);
            if (propgroup != null)
                polygonCount += GetStatisticValue(propgroup);

            return polygonCount;
        }
        #endregion // Overrides
    } // WeaponPolygonInstanceCountOverlay

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(ExtensionPoints.PolygonStatisticsOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class CollisionRatioOverlay : StatisticOverlayBase
    {
        #region Constants
        private const string c_name = "Collision Polygon Ratio";
        private const string c_description = "";
        #endregion // Constants

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public CollisionRatioOverlay()
            : base(c_name, c_description)
        {
            m_includeHighDetailed = true;
            m_includeLod = true;
            m_includeSuperLod = true;
            m_includeDynamic = true;
            m_includeNonDynamic = true;
            m_includeInterior = false;

            m_dontShowObjectOptions = false;
            m_dontShowReferenceOptions = false;
            m_dontShowInteriorOptions = true;
            m_dontShowUniqueOptions = true;
        }
        #endregion // Constructor

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        protected void GetStatisticValue(IMapSection section, ref uint polygonStat, ref uint collisionStat)
        {
            if (!this.IncludeDynamic && !this.IncludeNonDynamic)
                return;
            if (!this.IncludeDrawableEntities && !this.IncludeNonDrawableEntities)
                return;

            StatEntityType type = StatEntityType.Total;
            if (this.IncludeDynamic && !this.IncludeNonDynamic)
            {
                type = StatEntityType.Dynamic;
            }
            else if (!this.IncludeDynamic && this.IncludeNonDynamic)
            {
                type = StatEntityType.NonDynamic;
            }

            if (this.IncludeDrawableEntities && !this.IncludeNonDrawableEntities)
            {
                AddStatistics(section, StatGroup.DrawableEntities, type, ref polygonStat, ref collisionStat);
            }
            else if (!this.IncludeDrawableEntities && this.IncludeNonDrawableEntities)
            {
                AddStatistics(section, StatGroup.NonDrawableEntities, type, ref polygonStat, ref collisionStat);
            }
            else
            {
                AddStatistics(section, StatGroup.AllNonInteriorEntities, type, ref polygonStat, ref collisionStat);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stat"></param>
        /// <param name="stats"></param>
        /// <param name="group"></param>
        /// <param name="type"></param>
        private void AddStatistics(IMapSection section, StatGroup group, StatEntityType type, ref uint polygonStat, ref uint collisionStat)
        {
            if (this.IncludeHighDetailed)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Hd, type)];
                polygonStat += stats.PolygonCount;
                collisionStat += stats.CollisionCount;
            }
            if (this.IncludeLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Lod, type)];
                polygonStat += stats.PolygonCount;
                collisionStat += stats.CollisionCount;
            }
            if (this.IncludeSuperLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.SLod1, type)];
                polygonStat += stats.PolygonCount;
                collisionStat += stats.CollisionCount;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="propgroup"></param>
        /// <returns></returns>
        protected override double GetStatisticValue(IMapSection section, IMapSection propgroup)
        {
            uint polygonCount = 0;
            uint collisionCount = 0;
            if (section != null)
                GetStatisticValue(section, ref polygonCount, ref collisionCount);
            if (propgroup != null)
                GetStatisticValue(propgroup, ref polygonCount, ref collisionCount);

            if (polygonCount == 0)
                return 0.0;

            return ((double)collisionCount / (double)polygonCount) * 100.0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sectionStat"></param>
        /// <returns></returns>
        protected override String GeometryUserText(KeyValuePair<double, IMapSection> sectionStat)
        {
            return String.Format("{0:0.00%}", sectionStat.Key * 0.01);
        }
        #endregion // Overrides
    } // CollisionRatioOverlay

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(ExtensionPoints.PolygonStatisticsOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class CollisionShooterRatioOverlay : StatisticOverlayBase
    {
        #region Constants
        private const string c_name = "Shooter Polygon Ratio";
        private const string c_description = "";
        #endregion // Constants

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public CollisionShooterRatioOverlay()
            : base(c_name, c_description)
        {
            m_includeHighDetailed = true;
            m_includeLod = true;
            m_includeSuperLod = true;
            m_includeDynamic = true;
            m_includeNonDynamic = true;
            m_includeInterior = false;

            m_dontShowObjectOptions = false;
            m_dontShowReferenceOptions = false;
            m_dontShowInteriorOptions = true;
            m_dontShowUniqueOptions = true;
        }
        #endregion // Constructor

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        protected void GetStatisticValue(IMapSection section, ref uint polygonStat, ref uint collisionStat)
        {
            if (!this.IncludeDynamic && !this.IncludeNonDynamic)
                return;
            if (!this.IncludeDrawableEntities && !this.IncludeNonDrawableEntities)
                return;

            StatEntityType type = StatEntityType.Total;
            if (this.IncludeDynamic && !this.IncludeNonDynamic)
            {
                type = StatEntityType.Dynamic;
            }
            else if (!this.IncludeDynamic && this.IncludeNonDynamic)
            {
                type = StatEntityType.NonDynamic;
            }

            if (this.IncludeDrawableEntities && !this.IncludeNonDrawableEntities)
            {
                AddStatistics(section, StatGroup.DrawableEntities, type, ref polygonStat, ref collisionStat);
            }
            else if (!this.IncludeDrawableEntities && this.IncludeNonDrawableEntities)
            {
                AddStatistics(section, StatGroup.NonDrawableEntities, type, ref polygonStat, ref collisionStat);
            }
            else
            {
                AddStatistics(section, StatGroup.AllNonInteriorEntities, type, ref polygonStat, ref collisionStat);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stat"></param>
        /// <param name="stats"></param>
        /// <param name="group"></param>
        /// <param name="type"></param>
        private void AddStatistics(IMapSection section, StatGroup group, StatEntityType type, ref uint polygonStat, ref uint collisionStat)
        {
            if (this.IncludeHighDetailed)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Hd, type)];
                polygonStat += stats.PolygonCount;
                collisionStat += stats.GetPolyCountForCollisionType(CollisionType.Weapon);
            }
            if (this.IncludeLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Lod, type)];
                polygonStat += stats.PolygonCount;
                collisionStat += stats.GetPolyCountForCollisionType(CollisionType.Weapon);
            }
            if (this.IncludeSuperLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.SLod1, type)];
                polygonStat += stats.PolygonCount;
                collisionStat += stats.GetPolyCountForCollisionType(CollisionType.Weapon);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="propgroup"></param>
        /// <returns></returns>
        protected override double GetStatisticValue(IMapSection section, IMapSection propgroup)
        {
            uint polygonCount = 0;
            uint collisionCount = 0;
            if (section != null)
                GetStatisticValue(section, ref polygonCount, ref collisionCount);
            if (propgroup != null)
                GetStatisticValue(propgroup, ref polygonCount, ref collisionCount);

            if (polygonCount == 0)
                return 0.0;

            return ((double)collisionCount / (double)polygonCount) * 100.0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sectionStat"></param>
        /// <returns></returns>
        protected override String GeometryUserText(KeyValuePair<double, IMapSection> sectionStat)
        {
            return String.Format("{0:0.00%}", sectionStat.Key * 0.01);
        }
        #endregion // Overrides
    } // CollisionShooterRatioOverlay

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(ExtensionPoints.PolygonStatisticsOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class CollisionMoverRatioOverlay : StatisticOverlayBase
    {
        #region Constants
        private const string c_name = "Mover Polygon Ratio";
        private const string c_description = "";
        #endregion // Constants

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public CollisionMoverRatioOverlay()
            : base(c_name, c_description)
        {
            m_includeHighDetailed = true;
            m_includeLod = true;
            m_includeSuperLod = true;
            m_includeDynamic = true;
            m_includeNonDynamic = true;
            m_includeInterior = false;

            m_dontShowObjectOptions = false;
            m_dontShowReferenceOptions = false;
            m_dontShowInteriorOptions = true;
            m_dontShowUniqueOptions = true;
        }
        #endregion // Constructor

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        protected void GetStatisticValue(IMapSection section, ref uint polygonStat, ref uint collisionStat)
        {
            if (!this.IncludeDynamic && !this.IncludeNonDynamic)
                return;
            if (!this.IncludeDrawableEntities && !this.IncludeNonDrawableEntities)
                return;

            StatEntityType type = StatEntityType.Total;
            if (this.IncludeDynamic && !this.IncludeNonDynamic)
            {
                type = StatEntityType.Dynamic;
            }
            else if (!this.IncludeDynamic && this.IncludeNonDynamic)
            {
                type = StatEntityType.NonDynamic;
            }

            if (this.IncludeDrawableEntities && !this.IncludeNonDrawableEntities)
            {
                AddStatistics(section, StatGroup.DrawableEntities, type, ref polygonStat, ref collisionStat);
            }
            else if (!this.IncludeDrawableEntities && this.IncludeNonDrawableEntities)
            {
                AddStatistics(section, StatGroup.NonDrawableEntities, type, ref polygonStat, ref collisionStat);
            }
            else
            {
                AddStatistics(section, StatGroup.AllNonInteriorEntities, type, ref polygonStat, ref collisionStat);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stat"></param>
        /// <param name="stats"></param>
        /// <param name="group"></param>
        /// <param name="type"></param>
        private void AddStatistics(IMapSection section, StatGroup group, StatEntityType type, ref uint polygonStat, ref uint collisionStat)
        {
            if (this.IncludeHighDetailed)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Hd, type)];
                polygonStat += stats.PolygonCount;
                collisionStat += stats.GetPolyCountForCollisionType(CollisionType.Mover);
            }
            if (this.IncludeLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Lod, type)];
                polygonStat += stats.PolygonCount;
                collisionStat += stats.GetPolyCountForCollisionType(CollisionType.Mover);
            }
            if (this.IncludeSuperLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.SLod1, type)];
                polygonStat += stats.PolygonCount;
                collisionStat += stats.GetPolyCountForCollisionType(CollisionType.Mover);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="propgroup"></param>
        /// <returns></returns>
        protected override double GetStatisticValue(IMapSection section, IMapSection propgroup)
        {
            uint polygonCount = 0;
            uint collisionCount = 0;
            if (section != null)
                GetStatisticValue(section, ref polygonCount, ref collisionCount);
            if (propgroup != null)
                GetStatisticValue(propgroup, ref polygonCount, ref collisionCount);

            if (polygonCount == 0)
                return 0.0;

            return ((double)collisionCount / (double)polygonCount) * 100.0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sectionStat"></param>
        /// <returns></returns>
        protected override String GeometryUserText(KeyValuePair<double, IMapSection> sectionStat)
        {
            return String.Format("{0:0.00%}", sectionStat.Key * 0.01);
        }
        #endregion // Overrides
    } // CollisionMoverRatioOverlay

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(ExtensionPoints.PolygonStatisticsOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class CollisionMoverShooterRatioOverlay : StatisticOverlayBase
    {
        #region Constants
        private const string c_name = "Mover/Shooter Polygon Ratio";
        private const string c_description = "";
        #endregion // Constants

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public CollisionMoverShooterRatioOverlay()
            : base(c_name, c_description)
        {
            m_includeHighDetailed = true;
            m_includeLod = true;
            m_includeSuperLod = true;
            m_includeDynamic = true;
            m_includeNonDynamic = true;
            m_includeInterior = false;

            m_dontShowObjectOptions = false;
            m_dontShowReferenceOptions = false;
            m_dontShowInteriorOptions = true;
            m_dontShowUniqueOptions = true;
        }
        #endregion // Constructor

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        protected void GetStatisticValue(IMapSection section, ref uint moverCount, ref uint weaponCount)
        {
            if (!this.IncludeDynamic && !this.IncludeNonDynamic)
                return;
            if (!this.IncludeDrawableEntities && !this.IncludeNonDrawableEntities)
                return;

            StatEntityType type = StatEntityType.Total;
            if (this.IncludeDynamic && !this.IncludeNonDynamic)
            {
                type = StatEntityType.Dynamic;
            }
            else if (!this.IncludeDynamic && this.IncludeNonDynamic)
            {
                type = StatEntityType.NonDynamic;
            }

            if (this.IncludeDrawableEntities && !this.IncludeNonDrawableEntities)
            {
                AddStatistics(section, StatGroup.DrawableEntities, type, ref moverCount, ref weaponCount);
            }
            else if (!this.IncludeDrawableEntities && this.IncludeNonDrawableEntities)
            {
                AddStatistics(section, StatGroup.NonDrawableEntities, type, ref moverCount, ref weaponCount);
            }
            else
            {
                AddStatistics(section, StatGroup.AllNonInteriorEntities, type, ref moverCount, ref weaponCount);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stat"></param>
        /// <param name="stats"></param>
        /// <param name="group"></param>
        /// <param name="type"></param>
        private void AddStatistics(IMapSection section, StatGroup group, StatEntityType type, ref uint moverCount, ref uint weaponCount)
        {
            if (this.IncludeHighDetailed)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Hd, type)];
                moverCount += stats.GetPolyCountForCollisionType(CollisionType.Mover);
                weaponCount += stats.GetPolyCountForCollisionType(CollisionType.Weapon);
            }
            if (this.IncludeLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.Lod, type)];
                moverCount += stats.GetPolyCountForCollisionType(CollisionType.Mover);
                weaponCount += stats.GetPolyCountForCollisionType(CollisionType.Weapon);
            }
            if (this.IncludeSuperLod)
            {
                RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, StatLodLevel.SLod1, type)];
                moverCount += stats.GetPolyCountForCollisionType(CollisionType.Mover);
                weaponCount += stats.GetPolyCountForCollisionType(CollisionType.Weapon);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="propgroup"></param>
        /// <returns></returns>
        protected override double GetStatisticValue(IMapSection section, IMapSection propgroup)
        {
            uint moverCount = 0;
            uint weaponCount = 0;
            if (section != null)
                GetStatisticValue(section, ref moverCount, ref weaponCount);
            if (propgroup != null)
                GetStatisticValue(propgroup, ref moverCount, ref weaponCount);

            if (moverCount == 0)
                return 0.0;

            return ((double)weaponCount / (double)moverCount) * 100.0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sectionStat"></param>
        /// <returns></returns>
        protected override String GeometryUserText(KeyValuePair<double, IMapSection> sectionStat)
        {
            return String.Format("{0:0.00%}", sectionStat.Key * 0.01);
        }
        #endregion // Overrides
    } // CollisionMoverShooterRatioOverlay
} // Workbench.AddIn.MapStatistics.Overlays
 
