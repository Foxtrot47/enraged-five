﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor.Command;
using RSG.Base.Collections;
using RSG.Model.GlobalTXD;
using RSG.Base.Editor;
using System.Text.RegularExpressions;

namespace Workbench.AddIn.TXDParentiser.ViewModel
{
    /// <summary>
    /// A view model that represents the global root model
    /// </summary>
    public class GlobalRootViewModel : RSG.Base.Editor.HierarchicalViewModelBase,
        IDictionaryContainerViewModel
    {
        #region Properties

        /// <summary>
        /// If this is true it means that source texture dictionaries can be added to
        /// this container as children
        /// </summary>
        public Boolean CanAcceptSourceDictionaries
        {
            get { return this.Model.CanAcceptSourceDictionaries; }
        }

        /// <summary>
        /// If this is true it means that global texture dictionaries can be added to
        /// this container as children
        /// </summary>
        public Boolean CanAcceptGlobalDictionaries
        {
            get { return this.Model.CanAcceptGlobalDictionaries; }
        }

        /// <summary>
        /// The model reference that this view model represents.
        /// </summary>
        public GlobalRoot Model
        {
            get { return m_model; }
            set { m_model = value; }
        }
        private GlobalRoot m_model;

        string IDictionaryContainerViewModel.Name
        {
            get { return this.Model.Name; }
        }

        /// <summary>
        /// The parent global dictionary if it has one
        /// </summary>
        public new IDictionaryContainerViewModel Parent
        {
            get { return m_parent; }
            set
            {
                this.SetPropertyValue(ref this.m_parent, value, "Parent");
            }
        }
        private IDictionaryContainerViewModel m_parent;

        /// <summary>
        /// Override the children property to make it private
        /// </summary>
        private new IList Children
        { 
            get;
            set;
        }

        /// <summary>
        /// A list of all the global texture dictionary that this container currently has.
        /// </summary>
        public ObservableCollection<GlobalTextureDictionaryViewModel> GlobalTextureDictionaries 
        { 
            get; 
            private set;
        }
  
        /// <summary>
        /// A dictionary of uniquely named source texture dictionaries that are children of this global root
        /// The dictionaries are indexed by names and these names need to be unique.
        /// </summary>
        public ObservableCollection<SourceTextureDictionaryViewModel> SourceTextureDictionaries
        {
            get { return m_sourceTextureDictionaries; }
            private set
            {
                this.SetPropertyValue(
                    ref this.m_sourceTextureDictionaries, value, "SourceTextureDictionaries");
            }
        }
        private ObservableCollection<SourceTextureDictionaryViewModel> m_sourceTextureDictionaries;

        /// <summary>
        /// The user data that can be attached to this view model
        /// </summary>
        public RSG.Base.Collections.UserData ViewModelUserData
        {
            get { return m_viewModelUserData; }
            set { m_viewModelUserData = value; }
        }
        private RSG.Base.Collections.UserData m_viewModelUserData;

        /// <summary>
        /// The name that will be used to display this in the UI
        /// </summary>
        public String DisplayName
        {
            get;
            set;
        }

        /// <summary>
        /// Returns this GlobalTextureDictionaryRoot as a enumerable so that item controls can bind to it
        /// </summary>
        public IEnumerable<GlobalRootViewModel> This
        {
            get
            {
                yield return this;
            }
        }

        /// <summary>
        /// Respresents where the UIElement bound to this object currently has focus
        /// </summary>
        public bool IsFocused
        {
            get { return m_isFocused; }
            set
            {
                this.SetPropertyValue(ref this.m_isFocused, value, "IsFocused");
            }
        }
        private bool m_isFocused = false;

        public int Level
        {
            get { return -1; }
        }

        public bool IsDropTarget
        {
            get { return false; }
        }
        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        public GlobalRootViewModel(GlobalRoot model)
        {
            this.Model = model;
            this.DisplayName = "Global Texture Dictionaries";
            this.Parent = null;
            this.ViewModelUserData = new RSG.Base.Collections.UserData();
            this.GlobalTextureDictionaries = new ObservableCollection<GlobalTextureDictionaryViewModel>();
            this.SourceTextureDictionaries = new ObservableCollection<SourceTextureDictionaryViewModel>();

            foreach (GlobalTextureDictionary dictionary in model.GlobalTextureDictionaries.Values)
            {
                GlobalTextureDictionaryViewModel newDictionary = new GlobalTextureDictionaryViewModel(dictionary, this);
                this.GlobalTextureDictionaries.Add(newDictionary);
            }
        }

        #endregion // Constructors

        #region IGlobalDictionaryContainerViewModel Implementation

        /// <summary>
        /// Returns true iff this root has a texture dictionary in it with
        /// the given name. Is recursive is true it will recursive through all the texture
        /// dictionaries as well
        /// </summary>
        public Boolean ContainsTextureDictionary(String name, Boolean recursive)
        {
            return this.Model.ContainsTextureDictionary(name, recursive);
        }

        /// <summary>
        /// Adds the given dictionary into the children texture dictionaries iff the name
        /// is unique.
        /// </summary>
        public Boolean AddExistingGlobalDictionary(GlobalTextureDictionaryViewModel dictionary)
        {
            if (this.Model.AddExistingGlobalDictionary(dictionary.Model))
            {
                this.GlobalTextureDictionaries.Add(new GlobalTextureDictionaryViewModel(dictionary.Model, this));
                this.IsExpanded = true;
                this.IsFocused = true;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Creates a new dictionary with the given name and adds it to this dictionary 
        /// iff the name is unique
        /// </summary>
        public Boolean AddNewGlobalDictionary(String name)
        {
            if (this.Model.AddNewGlobalDictionary(name))
            {
                GlobalTextureDictionaryViewModel newDictionary = new GlobalTextureDictionaryViewModel(this.Model[name], this);
                this.GlobalTextureDictionaries.Add(newDictionary);
                this.IsExpanded = true;
                this.IsFocused = true;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Creates a new source dictionary using the given texture dictionary as a 
        /// source guide.
        /// </summary>
        public Boolean AddNewSourceDictionary(TextureDictionaryViewModel dictionary)
        {
            return this.Model.AddNewSourceDictionary(dictionary.Model);
        }

        /// <summary>
        /// Removes the texture dictionary with the given name from the list iff
        /// it exists.
        /// </summary>
        public Boolean RemoveGlobalTextureDictionary(String name)
        {
            return true;
        }

        /// <summary>
        /// Removes the given texture dictionary from the list iff
        /// it exists.
        /// </summary>
        public Boolean RemoveGlobalTextureDictionary(GlobalTextureDictionaryViewModel dictionary)
        {
            if (this.Model.RemoveGlobalTextureDictionary(dictionary.Model))
            {
                if (this.GlobalTextureDictionaries.Contains(dictionary))
                {
                    this.GlobalTextureDictionaries.Remove(dictionary);
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Removes the given texture dictionary from the list iff
        /// it exists.
        /// </summary>
        public Boolean RemoveSourceTextureDictionary(SourceTextureDictionaryViewModel dictionary)
        {
            return this.Model.RemoveSourceTextureDictionary(dictionary.Model);
        }

        public void RaiseGlobalDictionariesChangedEvent()
        {
            OnPropertyChanged("GlobalTextureDictionaries");
        }

        public void RaiseSourceDictionariesChangedEvent()
        {
            OnPropertyChanged("SourceTextureDictionaries");
        }
        #endregion // IGlobalDictionaryContainerViewModel Implementation

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public IEnumerable<IViewModel> FindAll(Regex expression)
        {
            List<IViewModel> results = new List<IViewModel>();
            foreach (GlobalTextureDictionaryViewModel dictionary in GlobalTextureDictionaries)
            {
                if (expression.IsMatch(dictionary.Name))
                    yield return dictionary;

                foreach (IViewModel match in dictionary.FindAll(expression))
                {
                    yield return match;
                }
            }

            foreach (SourceTextureDictionaryViewModel dictionary in SourceTextureDictionaries)
            {
                if (expression.IsMatch(dictionary.Name))
                    yield return dictionary;

                foreach (IViewModel match in dictionary.FindAll(expression))
                {
                    yield return match;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Expand()
        {
            this.IsExpanded = true;        
        }

        public IEnumerable<IViewModel> GetAll()
        {
            List<IViewModel> results = new List<IViewModel>();
            foreach (GlobalTextureDictionaryViewModel dictionary in GlobalTextureDictionaries)
            {
                yield return dictionary;
                foreach (IViewModel match in dictionary.GetAll())
                {
                    yield return match;
                }
            }

            foreach (SourceTextureDictionaryViewModel dictionary in SourceTextureDictionaries)
            {
                yield return dictionary;
                foreach (IViewModel match in dictionary.GetAll())
                {
                    yield return match;
                }
            }
        }

        public IEnumerable<SourceTextureDictionaryViewModel> GetAllSourceDictionaries()
        {
            List<IViewModel> results = new List<IViewModel>();
            foreach (GlobalTextureDictionaryViewModel dictionary in GlobalTextureDictionaries)
            {
                foreach (SourceTextureDictionaryViewModel match in dictionary.GetAllSourceDictionaries())
                {
                    yield return match;
                }
            }

            foreach (SourceTextureDictionaryViewModel dictionary in SourceTextureDictionaries)
            {
                yield return dictionary;
            }
        }

        public IEnumerable<GlobalTextureDictionaryViewModel> GetAllGlobalDictionaries()
        {
            List<IViewModel> results = new List<IViewModel>();
            foreach (GlobalTextureDictionaryViewModel dictionary in GlobalTextureDictionaries)
            {
                yield return dictionary;
                foreach (GlobalTextureDictionaryViewModel match in dictionary.GetAllGlobalDictionaries())
                {
                    yield return match;
                }
            }
        }
        #endregion

        #region Static Helper Functions

        public static void CheckPromotedTexturesAreValid(IDictionaryContainerViewModel root, ValidationDirection direction)
        {
            IList<string> sourceTextures = new List<string>();
            GetAllSourceTextures(root, ref sourceTextures, true);

            if (root is ITextureContainerViewModel)
            {
                IList<ITextureChildViewModel> removeList = new List<ITextureChildViewModel>();
                foreach (ITextureChildViewModel texture in (root as ITextureContainerViewModel).Textures)
                {
                    if (!sourceTextures.Contains(texture.StreamName.ToLower()))
                        removeList.Add(texture);
                }
                foreach (ITextureChildViewModel texture in removeList)
                {
                    (root as ITextureContainerViewModel).RemoveTexture(texture);
                    if (texture.Parent is GlobalTextureDictionaryViewModel)
                    {
                        (texture.Parent as GlobalTextureDictionaryViewModel).OnAllChildrenChanged();
                    }
                }
            }
            if (direction == ValidationDirection.Up || direction == ValidationDirection.Both)
            {
                if (root.Parent != null)
                    CheckPromotedTexturesAreValid(root.Parent, ValidationDirection.Up);
            }
            if (direction == ValidationDirection.Down || direction == ValidationDirection.Both)
            {
                foreach (var child in root.GlobalTextureDictionaries)
                {
                    CheckPromotedTexturesAreValid(child, ValidationDirection.Down);
                }
            }
        }

        public static void CheckSingleGlobalTexture(IDictionaryContainerViewModel root, string streamName, ValidationDirection direction)
        {
            CheckSingleGlobalTexture(root, streamName, direction, false);
        }

        private static void CheckSingleGlobalTexture(IDictionaryContainerViewModel root, string streamName, ValidationDirection direction, bool remove)
        {
            if (root is ITextureContainerViewModel && remove == true)
            {
                IList<ITextureChildViewModel> removeList = new List<ITextureChildViewModel>();
                foreach (ITextureChildViewModel texture in (root as ITextureContainerViewModel).Textures)
                {
                    if (texture.StreamName.ToLower() == streamName.ToLower())
                        removeList.Add(texture);
                }
                foreach (ITextureChildViewModel texture in removeList)
                {
                    (root as ITextureContainerViewModel).RemoveTexture(texture);
                    if (texture.Parent is GlobalTextureDictionaryViewModel)
                    {
                        (texture.Parent as GlobalTextureDictionaryViewModel).OnAllChildrenChanged();
                    }
                }
            }
            if (direction == ValidationDirection.Up || direction == ValidationDirection.Both)
            {
                if (root.Parent != null)
                    CheckSingleGlobalTexture(root.Parent, streamName, ValidationDirection.Up, true);
            }
            if (direction == ValidationDirection.Down || direction == ValidationDirection.Both)
            {
                foreach (var child in root.GlobalTextureDictionaries)
                {
                    CheckSingleGlobalTexture(child, streamName, ValidationDirection.Down, true);
                }
            }
        }

        public static GlobalTextureDictionaryViewModel GetParent(GlobalTextureDictionaryViewModel dictionary1, GlobalTextureDictionaryViewModel dictionary2)
        {
            if (dictionary1 == dictionary2)
                return dictionary1;

            GlobalTextureDictionaryViewModel parent = dictionary1.Parent as GlobalTextureDictionaryViewModel;
            while (parent != null)
            {
                if (parent == dictionary2)
                {
                    return dictionary2;
                }
                parent = parent.Parent as GlobalTextureDictionaryViewModel;
            }

            parent = dictionary2.Parent as GlobalTextureDictionaryViewModel;
            while (parent != null)
            {
                if (parent == dictionary1)
                {
                    return dictionary1;
                }
                parent = parent.Parent as GlobalTextureDictionaryViewModel;
            }
            return null;
        }

        public static Boolean ContainsSourceTexture(String streamName, IDictionaryContainerViewModel root)
        {
            if (root.CanAcceptSourceDictionaries == true)
            {
                foreach (SourceTextureDictionaryViewModel sourceDictionary in root.SourceTextureDictionaries)
                {
                    SourceTextureViewModel texture = sourceDictionary[streamName];
                    if (texture != null)
                    {
                        return true;
                    }
                }
            }
            if (root.CanAcceptGlobalDictionaries == true)
            {
                foreach (GlobalTextureDictionaryViewModel globalDictionary in root.GlobalTextureDictionaries)
                {
                    if (GlobalRootViewModel.ContainsSourceTexture(streamName, globalDictionary))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static Boolean AreDictionariesParentChild(GlobalTextureDictionaryViewModel dictionary1, GlobalTextureDictionaryViewModel dictionary2)
        {
            if (dictionary1 == dictionary2)
                return false;

            GlobalTextureDictionaryViewModel parent = dictionary1.Parent as GlobalTextureDictionaryViewModel;
            while (parent != null)
            {
                if (parent == dictionary2)
                {
                    return true;
                }
                parent = parent.Parent as GlobalTextureDictionaryViewModel;
            }

            parent = dictionary2.Parent as GlobalTextureDictionaryViewModel;
            while (parent != null)
            {
                if (parent == dictionary1)
                {
                    return true;
                }
                parent = parent.Parent as GlobalTextureDictionaryViewModel;
            }

            return false;
        }

        public static Boolean AreDictionariesParentChild(GlobalTextureDictionaryViewModel dictionary1, SourceTextureDictionaryViewModel dictionary2)
        {
            if (dictionary1 == null || dictionary2 == null)
                return false;

            GlobalTextureDictionaryViewModel parent = dictionary2.Parent as GlobalTextureDictionaryViewModel;
            while (parent != null)
            {
                if (parent == dictionary1)
                {
                    return true;
                }
                parent = parent.Parent as GlobalTextureDictionaryViewModel;
            }

            return false;
        }

        public static Boolean AreDictionariesParentChild(GlobalTextureDictionaryViewModel dictionary1, SourceTextureDictionary dictionary2)
        {
            if (dictionary1 == null || dictionary2 == null)
                return false;

            GlobalTextureDictionary parent = dictionary2.Parent as GlobalTextureDictionary;
            while (parent != null)
            {
                if (parent == dictionary1.Model)
                {
                    return true;
                }
                parent = parent.Parent as GlobalTextureDictionary;
            }

            return false;
        }

        public static Boolean ContainsSourceDictionary(String name, IDictionaryContainerViewModel root)
        {
            if (root.CanAcceptSourceDictionaries == true)
            {
                foreach (SourceTextureDictionaryViewModel sourceDictionary in root.SourceTextureDictionaries)
                {
                    if (String.Compare(sourceDictionary.Model.Name, name, true) == 0)
                    {
                        return true;
                    }
                }
            }
            if (root.CanAcceptGlobalDictionaries == true)
            {
                foreach (GlobalTextureDictionaryViewModel globalDictionary in root.GlobalTextureDictionaries)
                {
                    if (GlobalRootViewModel.ContainsSourceDictionary(name, globalDictionary))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static void MoveTexturePostProcess(String streamName, GlobalTextureDictionaryViewModel oldParent, GlobalTextureDictionaryViewModel newParent)
        {
            // When a texture moves (i.e gets promoted/unpromoted by a certain number of levels we need to make sure that
            // the hierarchy is still valid.

            // Determine if we have promoted or unpromted the texture
            if (GlobalRootViewModel.IsParent(oldParent, newParent))
            {
                // PROMOTED
                IList<ITextureChildViewModel> textures = new List<ITextureChildViewModel>();
                int iteration = 0;
                GlobalRootViewModel.GetAllTexturesByName(streamName, newParent, ref textures, ref iteration, true);

                foreach (GlobalTextureViewModel texture in textures.Where(t => t is GlobalTextureViewModel == true))
                {
                    (texture.Parent as GlobalTextureDictionaryViewModel).MoveTextureFromHere(texture);
                }
            }
            else if (GlobalRootViewModel.IsParent(newParent, oldParent))
            {
                // UNPROMOTED
                IList<ITextureChildViewModel> textures = new List<ITextureChildViewModel>();
                int iteration = 0;
                GlobalRootViewModel.GetAllTexturesByName(streamName, newParent, ref textures, ref iteration, false);
                foreach (GlobalTextureViewModel texture in textures.Where(t => t is GlobalTextureViewModel == true))
                {
                    (texture.Parent as GlobalTextureDictionaryViewModel).MoveTextureFromHere(texture);
                }
            }
        }

        public static void RemoveSourceDictionaryPostProcess(GlobalTextureDictionaryViewModel sourceParent)
        {
            // Walk up the tree and make sure that all promoted textures are still valid if not remove them
            IDictionary<GlobalTextureDictionaryViewModel, IList<String>> textures = new Dictionary<GlobalTextureDictionaryViewModel, IList<String>>();
            GetAllPromotedTexturesWithParents(sourceParent, ref textures);

            foreach (KeyValuePair<GlobalTextureDictionaryViewModel, IList<String>> parents in textures)
            {
                GlobalTextureDictionaryViewModel parent = parents.Key;
                IList<String> sourceTextures = new List<String>();
                GetAllSourceTextures(parent as IDictionaryContainerViewModel, ref sourceTextures);

                foreach (String promotedTexture in parents.Value)
                {
                    if (!sourceTextures.Contains(promotedTexture))
                    {
                        (parent as GlobalTextureDictionaryViewModel).MoveTextureFromHere(promotedTexture);
                    }

                }
            }
        }

        private static void GetAllTexturesByName(String streamName, IDictionaryContainerViewModel root, ref IList<ITextureChildViewModel> textures, ref int iteration, Boolean down)
        {
            if (down == true)
            {
                if (root is GlobalTextureDictionaryViewModel && iteration != 0)
                {
                    foreach (GlobalTextureViewModel texture in (root as GlobalTextureDictionaryViewModel).Textures)
                    {
                        if (texture.Model.StreamName == streamName)
                        {
                            textures.Add(texture);
                        }
                    }
                }
                if (root.CanAcceptSourceDictionaries == true)
                {
                    foreach (SourceTextureDictionaryViewModel sourceDictionary in root.SourceTextureDictionaries)
                    {
                        SourceTextureViewModel texture = sourceDictionary[streamName];
                        if (texture != null)
                        {
                            textures.Add(texture);
                        }
                    }
                }
                if (root.CanAcceptGlobalDictionaries == true)
                {
                    foreach (GlobalTextureDictionaryViewModel globalDictionary in root.GlobalTextureDictionaries)
                    {
                        iteration++;
                        GlobalRootViewModel.GetAllTexturesByName(streamName, globalDictionary, ref textures, ref iteration, true);
                    }
                }
            }
            else
            {
                if (root is ITextureContainer && iteration != 0)
                {
                    foreach (GlobalTextureViewModel texture in (root as GlobalTextureDictionaryViewModel).Textures)
                    {
                        if (texture.Model.StreamName == streamName)
                        {
                            textures.Add(texture);
                        }
                    }
                }
                GlobalTextureDictionaryViewModel parent = (root as GlobalTextureDictionaryViewModel) != null ? (root as GlobalTextureDictionaryViewModel).Parent as GlobalTextureDictionaryViewModel : null;

                while (parent != null)
                {
                    foreach (GlobalTextureViewModel texture in parent.Textures)
                    {
                        if (texture.Model.StreamName == streamName)
                        {
                            textures.Add(texture);
                        }
                    }
                    parent = parent.Parent as GlobalTextureDictionaryViewModel;
                }  
            }
        }

        private static Boolean IsParent(GlobalTextureDictionaryViewModel root, GlobalTextureDictionaryViewModel parent)
        {
            if (root == parent)
                return false;

            GlobalTextureDictionaryViewModel rootParent = root.Parent as GlobalTextureDictionaryViewModel;
            while (parent != null)
            {
                if (rootParent == parent)
                {
                    return true;
                }
                parent = parent.Parent as GlobalTextureDictionaryViewModel;
            }

            return false;
        }

        private static void GetAllSourceTextures(IDictionaryContainerViewModel root, ref IList<String> textures, bool lowercase = false)
        {
            foreach (SourceTextureDictionaryViewModel sourceDictionary in root.SourceTextureDictionaries)
            {
                foreach (SourceTextureViewModel texture in sourceDictionary.Children)
                {
                    if (lowercase)
                    {
                        if (!textures.Contains(texture.Model.StreamName.ToLower()))
                        {
                            textures.Add(texture.Model.StreamName.ToLower());
                        }
                    }
                    else
                    {
                        if (!textures.Contains(texture.Model.StreamName))
                        {
                            textures.Add(texture.Model.StreamName);
                        }
                    }
                }
            }
            foreach (GlobalTextureDictionaryViewModel globalDictionary in root.GlobalTextureDictionaries)
            {
                GlobalRootViewModel.GetAllSourceTextures(globalDictionary, ref textures, lowercase);
            }
        }

        private static void GetAllPromotedTexturesWithParents(GlobalTextureDictionaryViewModel baseDictionary, ref IDictionary<GlobalTextureDictionaryViewModel, IList<String>> promotedTextures)
        {
            if (!promotedTextures.ContainsKey(baseDictionary))
            {
                promotedTextures.Add(baseDictionary, new List<String>());
                foreach (GlobalTextureViewModel texture in baseDictionary.Textures)
                {
                    if (!promotedTextures[baseDictionary].Contains(texture.Model.StreamName))
                    {
                        promotedTextures[baseDictionary].Add(texture.Model.StreamName);
                    }
                }
            }

            if (baseDictionary.Parent is GlobalTextureDictionaryViewModel)
            {
                GlobalRootViewModel.GetAllPromotedTexturesWithParents((baseDictionary.Parent as GlobalTextureDictionaryViewModel), ref promotedTextures);
            }
        }

        #endregion Static Helper Functions
    }
} // Workbench.AddIn.TXDParentiser.ViewModel
