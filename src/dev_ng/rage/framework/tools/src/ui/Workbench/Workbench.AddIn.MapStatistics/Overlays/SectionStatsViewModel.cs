﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using RSG.Model.Common.Map;
using RSG.Base.Collections;

namespace Workbench.AddIn.MapStatistics.Overlays
{
    /// <summary>
    /// 
    /// </summary>
    public class SectionStatsViewModel : ViewModelBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<SectionStatViewModelBase> StatisticsViewModels
        {
            get { return m_statisticsViewModels; }
            set
            {
                SetPropertyValue(value, () => StatisticsViewModels,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_statisticsViewModels = (ObservableCollection<SectionStatViewModelBase>)newValue;
                        }
                ));
            }
        }
        private ObservableCollection<SectionStatViewModelBase> m_statisticsViewModels;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="stats"></param>
        public SectionStatsViewModel(IDictionary<StatKey, IMapSectionStatistic> stats, float area)
        {
            m_statisticsViewModels = new ObservableCollection<SectionStatViewModelBase>();
            m_statisticsViewModels.Add(new DrawableCountStatViewModel(stats));
            m_statisticsViewModels.Add(new DrawableInstanceCountStatistic(stats));
            m_statisticsViewModels.Add(new NonDrawableInstanceCountStatistic(stats));
            m_statisticsViewModels.Add(new InteriorInstanceCountStatistic(stats));
            m_statisticsViewModels.Add(new TxdCountStatistic(stats));
            m_statisticsViewModels.Add(new ShaderCountStatistic(stats));
            m_statisticsViewModels.Add(new PolygonCountStatistic(stats));
            m_statisticsViewModels.Add(new CollisionPolygonCountStatistic(stats));
            m_statisticsViewModels.Add(new MoverPolygonCountStatistic(stats));
            m_statisticsViewModels.Add(new ShooterPolygonCountStatistic(stats));
            m_statisticsViewModels.Add(new CollisionPolygonRatioStatistic(stats));
            m_statisticsViewModels.Add(new MoverShooterPolygonRatioStatistic(stats));
            m_statisticsViewModels.Add(new DrawableDensityStatistic(stats, area));
            m_statisticsViewModels.Add(new DrawableInstanceDensityStatistic(stats, area));
            m_statisticsViewModels.Add(new NonDrawableDensityStatistic(stats, area));
            m_statisticsViewModels.Add(new TxdDensityStatistic(stats, area));
            m_statisticsViewModels.Add(new ShaderDensityStatistic(stats, area));
            m_statisticsViewModels.Add(new PolygonDensityStatistic(stats, area));
            m_statisticsViewModels.Add(new CollisionPolygonDensityStatistic(stats, area));
            m_statisticsViewModels.Add(new LodDistanceStatistic(stats));
        }
        #endregion // Constructor(s)
    } // SectionStatsViewModel

    /// <summary>
    /// 
    /// </summary>
    public enum SectionStatisticType
    {
        Size,
        Double,
        Distance,
        Percentage,
        Integer,
    }

    /// <summary>
    /// 
    /// </summary>
    public class SectionStatViewModelBase : ViewModelBase
    {
        #region Properties
        /// <summary>
        /// The name of the statistic
        /// </summary>
        public String Name
        {
            get { return m_name; }
            set
            {
                SetPropertyValue(value, () => Name,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_name = (String)newValue;
                        }
                ));
            }
        }
        protected String m_name;
        
        /// <summary>
        /// The description for the statistic
        /// </summary>
        public String Description
        {
            get { return m_description; }
            set
            {
                SetPropertyValue(value, () => Description,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_description = (String)newValue;
                        }
                ));
            }
        }
        protected String m_description;

        /// <summary>
        /// The type of statistic
        /// </summary>
        public SectionStatisticType StatisticType
        {
            get { return m_statisticType; }
            set
            {
                SetPropertyValue(value, () => StatisticType,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_statisticType = (SectionStatisticType)newValue;
                        }
                ));
            }
        }
        protected SectionStatisticType m_statisticType;
        #endregion // Properties
        
        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public SectionStatViewModelBase(String name, String description, SectionStatisticType type)
        {
            this.Name = name;
            this.Description = description;
            this.StatisticType = type;
        }
        #endregion // Constructors
    } // SectionStatViewModelBase

    /// <summary>
    /// 
    /// </summary>
    public class CountStatViewModelBase : SectionStatViewModelBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public virtual uint? TotalStatistic
        {
            get { return m_totalStatistic; }
            set
            {
                SetPropertyValue(value, () => TotalStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_totalStatistic = (uint?)newValue;
                        }
                ));
            }
        }
        protected uint? m_totalStatistic = null;

        /// <summary>
        /// 
        /// </summary>
        public virtual uint? HighDynamicStatistic
        {
            get { return m_highDynamicStatistic; }
            set
            {
                SetPropertyValue(value, m_highDynamicStatistic, () => HighDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_highDynamicStatistic = (uint?)newValue;
                        }
                ));
            }
        }
        protected uint? m_highDynamicStatistic = null;

        /// <summary>
        /// 
        /// </summary>
        public virtual uint? HighNonDynamicStatistic
        {
            get { return m_highNonDynamicStatistic; }
            set
            {
                SetPropertyValue(value, m_highNonDynamicStatistic, () => HighNonDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_highNonDynamicStatistic = (uint?)newValue;
                        }
                ));
            }
        }
        protected uint? m_highNonDynamicStatistic = null;

        /// <summary>
        /// 
        /// </summary>
        public virtual uint? LodDynamicStatistic
        {
            get { return m_lodDynamicStatistic; }
            set
            {
                SetPropertyValue(value, m_lodDynamicStatistic, () => LodDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_lodDynamicStatistic = (uint?)newValue;
                        }
                ));
            }
        }
        protected uint? m_lodDynamicStatistic = null;

        /// <summary>
        /// 
        /// </summary>
        public virtual uint? LodNonDynamicStatistic
        {
            get { return m_lodNonDynamicStatistic; }
            set
            {
                SetPropertyValue(value, m_lodNonDynamicStatistic, () => LodNonDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_lodNonDynamicStatistic = (uint?)newValue;
                        }
                ));
            }
        }
        protected uint? m_lodNonDynamicStatistic = null;

        /// <summary>
        /// 
        /// </summary>
        public virtual uint? SlodDynamicStatistic
        {
            get { return m_slodDynamicStatistic; }
            set
            {
                SetPropertyValue(value, m_slodDynamicStatistic, () => SlodDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_slodDynamicStatistic = (uint?)newValue;
                        }
                ));
            }
        }
        protected uint? m_slodDynamicStatistic = null;

        /// <summary>
        /// 
        /// </summary>
        public virtual uint? SlodNonDynamicStatistic
        {
            get { return m_slodNonDynamicStatistic; }
            set
            {
                SetPropertyValue(value, m_slodNonDynamicStatistic, () => SlodNonDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_slodNonDynamicStatistic = (uint?)newValue;
                        }
                ));
            }
        }
        protected uint? m_slodNonDynamicStatistic = null;
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public CountStatViewModelBase(String name, String description)
            : base(name, description, SectionStatisticType.Integer)
        {
        }
        #endregion // Constructors
    } // CountStatViewModelBase

    /// <summary>
    /// 
    /// </summary>
    public class DensityStatViewModelBase : SectionStatViewModelBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public double? TotalStatistic
        {
            get { return m_totalStatistic; }
            set
            {
                SetPropertyValue(value, () => TotalStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_totalStatistic = (double?)newValue;
                        }
                ));
            }
        }
        protected double? m_totalStatistic = null;

        /// <summary>
        /// 
        /// </summary>
        public double? HighDynamicStatistic
        {
            get { return m_highDynamicStatistic; }
            set
            {
                SetPropertyValue(value, m_highDynamicStatistic, () => HighDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_highDynamicStatistic = (double?)newValue;
                        }
                ));
            }
        }
        protected double? m_highDynamicStatistic = null;

        /// <summary>
        /// 
        /// </summary>
        public double? HighNonDynamicStatistic
        {
            get { return m_highNonDynamicStatistic; }
            set
            {
                SetPropertyValue(value, m_highNonDynamicStatistic, () => HighNonDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_highNonDynamicStatistic = (double?)newValue;
                        }
                ));
            }
        }
        protected double? m_highNonDynamicStatistic = null;

        /// <summary>
        /// 
        /// </summary>
        public double? LodDynamicStatistic
        {
            get { return m_lodDynamicStatistic; }
            set
            {
                SetPropertyValue(value, m_lodDynamicStatistic, () => LodDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_lodDynamicStatistic = (double?)newValue;
                        }
                ));
            }
        }
        protected double? m_lodDynamicStatistic = null;

        /// <summary>
        /// 
        /// </summary>
        public double? LodNonDynamicStatistic
        {
            get { return m_lodNonDynamicStatistic; }
            set
            {
                SetPropertyValue(value, m_lodNonDynamicStatistic, () => LodNonDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_lodNonDynamicStatistic = (double?)newValue;
                        }
                ));
            }
        }
        protected double? m_lodNonDynamicStatistic = null;

        /// <summary>
        /// 
        /// </summary>
        public double? SlodDynamicStatistic
        {
            get { return m_slodDynamicStatistic; }
            set
            {
                SetPropertyValue(value, m_slodDynamicStatistic, () => SlodDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_slodDynamicStatistic = (double?)newValue;
                        }
                ));
            }
        }
        protected double? m_slodDynamicStatistic = null;

        /// <summary>
        /// 
        /// </summary>
        public double? SlodNonDynamicStatistic
        {
            get { return m_slodNonDynamicStatistic; }
            set
            {
                SetPropertyValue(value, m_slodNonDynamicStatistic, () => SlodNonDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_slodNonDynamicStatistic = (double?)newValue;
                        }
                ));
            }
        }
        protected double? m_slodNonDynamicStatistic = null;
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public DensityStatViewModelBase(String name, String description)
            : base(name, description, SectionStatisticType.Double)
        {
        }
        #endregion // Constructors
    } // DensityStatisticBase

    /// <summary>
    /// 
    /// </summary>
    public class RatioStatViewModelBase : SectionStatViewModelBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public double? TotalStatistic
        {
            get { return m_totalStatistic; }
            set
            {
                SetPropertyValue(value, () => TotalStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_totalStatistic = (double?)newValue;
                        }
                ));
            }
        }
        protected double? m_totalStatistic = null;

        /// <summary>
        /// 
        /// </summary>
        public double? HighDynamicStatistic
        {
            get { return m_highDynamicStatistic; }
            set
            {
                SetPropertyValue(value, m_highDynamicStatistic, () => HighDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_highDynamicStatistic = (double?)newValue;
                        }
                ));
            }
        }
        protected double? m_highDynamicStatistic = null;

        /// <summary>
        /// 
        /// </summary>
        public double? HighNonDynamicStatistic
        {
            get { return m_highNonDynamicStatistic; }
            set
            {
                SetPropertyValue(value, m_highNonDynamicStatistic, () => HighNonDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_highNonDynamicStatistic = (double?)newValue;
                        }
                ));
            }
        }
        protected double? m_highNonDynamicStatistic = null;

        /// <summary>
        /// 
        /// </summary>
        public double? LodDynamicStatistic
        {
            get { return m_lodDynamicStatistic; }
            set
            {
                SetPropertyValue(value, m_lodDynamicStatistic, () => LodDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_lodDynamicStatistic = (double?)newValue;
                        }
                ));
            }
        }
        protected double? m_lodDynamicStatistic = null;

        /// <summary>
        /// 
        /// </summary>
        public double? LodNonDynamicStatistic
        {
            get { return m_lodNonDynamicStatistic; }
            set
            {
                SetPropertyValue(value, m_lodNonDynamicStatistic, () => LodNonDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_lodNonDynamicStatistic = (double?)newValue;
                        }
                ));
            }
        }
        protected double? m_lodNonDynamicStatistic = null;

        /// <summary>
        /// 
        /// </summary>
        public double? SlodDynamicStatistic
        {
            get { return m_slodDynamicStatistic; }
            set
            {
                SetPropertyValue(value, m_slodDynamicStatistic, () => SlodDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_slodDynamicStatistic = (double?)newValue;
                        }
                ));
            }
        }
        protected double? m_slodDynamicStatistic = null;

        /// <summary>
        /// 
        /// </summary>
        public double? SlodNonDynamicStatistic
        {
            get { return m_slodNonDynamicStatistic; }
            set
            {
                SetPropertyValue(value, m_slodNonDynamicStatistic, () => SlodNonDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_slodNonDynamicStatistic = (double?)newValue;
                        }
                ));
            }
        }
        protected double? m_slodNonDynamicStatistic = null;
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public RatioStatViewModelBase(String name, String description)
            : base(name, description, SectionStatisticType.Percentage)
        {
        }
        #endregion // Constructors
    } // RatioStatisticBase

    /// <summary>
    /// 
    /// </summary>
    public class DistanceStatViewModelBase : SectionStatViewModelBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public float? TotalStatistic
        {
            get { return m_totalStatistic; }
            set
            {
                SetPropertyValue(value, () => TotalStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_totalStatistic = (float?)newValue;
                        }
                ));
            }
        }
        protected float? m_totalStatistic = null;

        /// <summary>
        /// 
        /// </summary>
        public float? HighDynamicStatistic
        {
            get { return m_highDynamicStatistic; }
            set
            {
                SetPropertyValue(value, m_highDynamicStatistic, () => HighDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_highDynamicStatistic = (float?)newValue;
                        }
                ));
            }
        }
        protected float? m_highDynamicStatistic = null;

        /// <summary>
        /// 
        /// </summary>
        public float? HighNonDynamicStatistic
        {
            get { return m_highNonDynamicStatistic; }
            set
            {
                SetPropertyValue(value, m_highNonDynamicStatistic, () => HighNonDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_highNonDynamicStatistic = (float?)newValue;
                        }
                ));
            }
        }
        protected float? m_highNonDynamicStatistic = null;

        /// <summary>
        /// 
        /// </summary>
        public float? LodDynamicStatistic
        {
            get { return m_lodDynamicStatistic; }
            set
            {
                SetPropertyValue(value, m_lodDynamicStatistic, () => LodDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_lodDynamicStatistic = (float?)newValue;
                        }
                ));
            }
        }
        protected float? m_lodDynamicStatistic = null;

        /// <summary>
        /// 
        /// </summary>
        public float? LodNonDynamicStatistic
        {
            get { return m_lodNonDynamicStatistic; }
            set
            {
                SetPropertyValue(value, m_lodNonDynamicStatistic, () => LodNonDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_lodNonDynamicStatistic = (float?)newValue;
                        }
                ));
            }
        }
        protected float? m_lodNonDynamicStatistic = null;

        /// <summary>
        /// 
        /// </summary>
        public float? SlodDynamicStatistic
        {
            get { return m_slodDynamicStatistic; }
            set
            {
                SetPropertyValue(value, m_slodDynamicStatistic, () => SlodDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_slodDynamicStatistic = (float?)newValue;
                        }
                ));
            }
        }
        protected float? m_slodDynamicStatistic = null;

        /// <summary>
        /// 
        /// </summary>
        public float? SlodNonDynamicStatistic
        {
            get { return m_slodNonDynamicStatistic; }
            set
            {
                SetPropertyValue(value, m_slodNonDynamicStatistic, () => SlodNonDynamicStatistic,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_slodNonDynamicStatistic = (float?)newValue;
                        }
                ));
            }
        }
        protected float? m_slodNonDynamicStatistic = null;
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public DistanceStatViewModelBase(String name, String description)
            : base(name, description, SectionStatisticType.Distance)
        {
        }
        #endregion // Constructors
    } // DistanceStatisticBase

    /// <summary>
    /// 
    /// </summary>
    public class DrawableCountStatViewModel : CountStatViewModelBase
    {
        #region Constants
        private const string c_name = "Drawable Count";
        private const string c_description = "The number of drawables in the map container.";
        #endregion // Constants

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public DrawableCountStatViewModel(IDictionary<StatKey, IMapSectionStatistic> rawStats)
            : base(c_name, c_description)
        {
            this.m_highDynamicStatistic = rawStats[new StatKey(StatGroup.Archetypes, StatLodLevel.Total, StatEntityType.Dynamic)].TotalCount;
            this.m_lodDynamicStatistic = null;
            this.m_slodDynamicStatistic = null;

            this.m_highNonDynamicStatistic = rawStats[new StatKey(StatGroup.Archetypes, StatLodLevel.Total, StatEntityType.NonDynamic)].TotalCount;
            this.m_lodNonDynamicStatistic = null;
            this.m_slodNonDynamicStatistic = null;

            this.m_totalStatistic = rawStats[new StatKey(StatGroup.Archetypes, StatLodLevel.Total, StatEntityType.Total)].TotalCount;
        }
        #endregion // Constructors
    } // DrawableInstanceCountStatistic

    /// <summary>
    /// 
    /// </summary>
    public class DrawableInstanceCountStatistic : CountStatViewModelBase
    {
        #region Constants
        private const string c_name = "Drawable Instance Count";
        private const string c_description = "The number of drawable instances in the map container.";
        #endregion // Constants

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public DrawableInstanceCountStatistic(IDictionary<StatKey, IMapSectionStatistic> rawStats)
            : base(c_name, c_description)
        {

            StatKey key = new StatKey(StatGroup.DrawableEntities, StatLodLevel.Total, StatEntityType.Dynamic);

            this.m_highDynamicStatistic = rawStats[new StatKey(StatGroup.DrawableEntities, StatLodLevel.Hd, StatEntityType.Dynamic)].TotalCount;
            this.m_lodDynamicStatistic = rawStats[new StatKey(StatGroup.DrawableEntities, StatLodLevel.Lod, StatEntityType.Dynamic)].TotalCount;
            this.m_slodDynamicStatistic = rawStats[new StatKey(StatGroup.DrawableEntities, StatLodLevel.SLod1, StatEntityType.Dynamic)].TotalCount;

            this.m_highNonDynamicStatistic = rawStats[new StatKey(StatGroup.DrawableEntities, StatLodLevel.Hd, StatEntityType.NonDynamic)].TotalCount;
            this.m_lodNonDynamicStatistic = rawStats[new StatKey(StatGroup.DrawableEntities, StatLodLevel.Lod, StatEntityType.NonDynamic)].TotalCount;
            this.m_slodNonDynamicStatistic = rawStats[new StatKey(StatGroup.DrawableEntities, StatLodLevel.SLod1, StatEntityType.NonDynamic)].TotalCount;

            this.m_totalStatistic = rawStats[new StatKey(StatGroup.DrawableEntities, StatLodLevel.Total, StatEntityType.Total)].TotalCount;
        }
        #endregion // Constructors
    } // DrawableInstanceCountStatistic

    /// <summary>
    /// 
    /// </summary>
    public class NonDrawableInstanceCountStatistic : CountStatViewModelBase
    {
        #region Constants
        private const string c_name = "Non Drawable Instance Count";
        private const string c_description = "The number of non drawable instances in the map container.";
        #endregion // Constants

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public NonDrawableInstanceCountStatistic(IDictionary<StatKey, IMapSectionStatistic> rawStats)
            : base(c_name, c_description)
        {
            this.m_highDynamicStatistic = rawStats[new StatKey(StatGroup.NonDrawableEntities, StatLodLevel.Hd, StatEntityType.Dynamic)].TotalCount;
            this.m_lodDynamicStatistic = rawStats[new StatKey(StatGroup.NonDrawableEntities, StatLodLevel.Lod, StatEntityType.Dynamic)].TotalCount;
            this.m_slodDynamicStatistic = rawStats[new StatKey(StatGroup.NonDrawableEntities, StatLodLevel.SLod1, StatEntityType.Dynamic)].TotalCount;

            this.m_highNonDynamicStatistic = rawStats[new StatKey(StatGroup.NonDrawableEntities, StatLodLevel.Hd, StatEntityType.NonDynamic)].TotalCount;
            this.m_lodNonDynamicStatistic = rawStats[new StatKey(StatGroup.NonDrawableEntities, StatLodLevel.Lod, StatEntityType.NonDynamic)].TotalCount;
            this.m_slodNonDynamicStatistic = rawStats[new StatKey(StatGroup.NonDrawableEntities, StatLodLevel.SLod1, StatEntityType.NonDynamic)].TotalCount;

            this.m_totalStatistic = rawStats[new StatKey(StatGroup.NonDrawableEntities, StatLodLevel.Total, StatEntityType.Total)].TotalCount;
        }
        #endregion // Constructors
    } // NonDrawableInstanceCountStatistic

    /// <summary>
    /// 
    /// </summary>
    public class InteriorInstanceCountStatistic : CountStatViewModelBase
    {
        #region Constants
        private const string c_name = "Interior Instance Count";
        private const string c_description = "The number of interior instances in the map container.";
        #endregion // Constants

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public InteriorInstanceCountStatistic(IDictionary<StatKey, IMapSectionStatistic> rawStats)
            : base(c_name, c_description)
        {
            this.m_highDynamicStatistic = rawStats[new StatKey(StatGroup.InteriorEntities, StatLodLevel.Hd, StatEntityType.Dynamic)].TotalCount;
            this.m_lodDynamicStatistic = rawStats[new StatKey(StatGroup.InteriorEntities, StatLodLevel.Lod, StatEntityType.Dynamic)].TotalCount;
            this.m_slodDynamicStatistic = rawStats[new StatKey(StatGroup.InteriorEntities, StatLodLevel.SLod1, StatEntityType.Dynamic)].TotalCount;

            this.m_highNonDynamicStatistic = rawStats[new StatKey(StatGroup.InteriorEntities, StatLodLevel.Hd, StatEntityType.NonDynamic)].TotalCount;
            this.m_lodNonDynamicStatistic = rawStats[new StatKey(StatGroup.InteriorEntities, StatLodLevel.Lod, StatEntityType.NonDynamic)].TotalCount;
            this.m_slodNonDynamicStatistic = rawStats[new StatKey(StatGroup.InteriorEntities, StatLodLevel.SLod1, StatEntityType.NonDynamic)].TotalCount;

            this.m_totalStatistic = rawStats[new StatKey(StatGroup.InteriorEntities, StatLodLevel.Total, StatEntityType.Total)].TotalCount;
        }
        #endregion // Constructors
    } // InteriorInstanceCountStatistic

    /// <summary>
    /// 
    /// </summary>
    public class TxdCountStatistic : CountStatViewModelBase
    {
        #region Constants
        private const string c_name = "TXD Count";
        private const string c_description = "The number of unique texture dictionaries that need loading for the map container.";
        #endregion // Constants

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public TxdCountStatistic(IDictionary<StatKey, IMapSectionStatistic> rawStats)
            : base(c_name, c_description)
        {
            this.m_highDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Hd, StatEntityType.Dynamic)].TXDCount;
            this.m_lodDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Lod, StatEntityType.Dynamic)].TXDCount;
            this.m_slodDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.SLod1, StatEntityType.Dynamic)].TXDCount;

            this.m_highNonDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Hd, StatEntityType.NonDynamic)].TXDCount;
            this.m_lodNonDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Lod, StatEntityType.NonDynamic)].TXDCount;
            this.m_slodNonDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.SLod1, StatEntityType.NonDynamic)].TXDCount;

            this.m_totalStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Total, StatEntityType.Total)].TXDCount;
        }
        #endregion // Constructors
    } // TxdCountStatistic

    /// <summary>
    /// 
    /// </summary>
    public class ShaderCountStatistic : CountStatViewModelBase
    {
        #region Constants
        private const string c_name = "Shader Count";
        private const string c_description = "The number of unique shaders that need loading for the map container.";
        #endregion // Constants

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public ShaderCountStatistic(IDictionary<StatKey, IMapSectionStatistic> rawStats)
            : base(c_name, c_description)
        {
            this.m_highDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Hd, StatEntityType.Dynamic)].ShaderCount;
            this.m_lodDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Lod, StatEntityType.Dynamic)].ShaderCount;
            this.m_slodDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.SLod1, StatEntityType.Dynamic)].ShaderCount;

            this.m_highNonDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Hd, StatEntityType.NonDynamic)].ShaderCount;
            this.m_lodNonDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Lod, StatEntityType.NonDynamic)].ShaderCount;
            this.m_slodNonDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.SLod1, StatEntityType.NonDynamic)].ShaderCount;

            this.m_totalStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Total, StatEntityType.Total)].ShaderCount;
        }
        #endregion // Constructors
    } // ShaderCountStatistic

    /// <summary>
    /// 
    /// </summary>
    public class PolygonCountStatistic : CountStatViewModelBase
    {
        #region Constants
        private const string c_name = "Shader Count";
        private const string c_description = "The number of polygons in the map container.";
        #endregion // Constants

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public PolygonCountStatistic(IDictionary<StatKey, IMapSectionStatistic> rawStats)
            : base(c_name, c_description)
        {
            this.m_highDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Hd, StatEntityType.Dynamic)].PolygonCount;
            this.m_lodDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Lod, StatEntityType.Dynamic)].PolygonCount;
            this.m_slodDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.SLod1, StatEntityType.Dynamic)].PolygonCount;

            this.m_highNonDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Hd, StatEntityType.NonDynamic)].PolygonCount;
            this.m_lodNonDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Lod, StatEntityType.NonDynamic)].PolygonCount;
            this.m_slodNonDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.SLod1, StatEntityType.NonDynamic)].PolygonCount;

            this.m_totalStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Total, StatEntityType.Total)].PolygonCount;
        }
        #endregion // Constructors
    } // PolygonCountStatistic

    /// <summary>
    /// 
    /// </summary>
    public class CollisionPolygonCountStatistic : CountStatViewModelBase
    {
        #region Constants
        private const string c_name = "Collision Polygon Count";
        private const string c_description = "The number of collision polygons in the map container.";
        #endregion // Constants

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public CollisionPolygonCountStatistic(IDictionary<StatKey, IMapSectionStatistic> rawStats)
            : base(c_name, c_description)
        {
            this.m_highDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Hd, StatEntityType.Dynamic)].CollisionCount;
            this.m_highNonDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Hd, StatEntityType.NonDynamic)].CollisionCount;
            this.m_totalStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Total, StatEntityType.Total)].CollisionCount;
        }
        #endregion // Constructors
    } // CollisionPolygonCountStatistic

    /// <summary>
    /// 
    /// </summary>
    public class MoverPolygonCountStatistic : CountStatViewModelBase
    {
        #region Constants
        private const string c_name = "Mover Polygon Count";
        private const string c_description = "The number of mover collision polygons in the map container.";
        #endregion // Constants

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public MoverPolygonCountStatistic(IDictionary<StatKey, IMapSectionStatistic> rawStats)
            : base(c_name, c_description)
        {
            this.m_highDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Hd, StatEntityType.Dynamic)].GetPolyCountForCollisionType(CollisionType.Mover);
            this.m_highNonDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Hd, StatEntityType.NonDynamic)].GetPolyCountForCollisionType(CollisionType.Mover);
            this.m_totalStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Total, StatEntityType.Total)].GetPolyCountForCollisionType(CollisionType.Mover);
        }
        #endregion // Constructors
    } // MoverPolygonCountStatistic

    /// <summary>
    /// 
    /// </summary>
    public class ShooterPolygonCountStatistic : CountStatViewModelBase
    {
        #region Constants
        private const string c_name = "Shooter Polygon Count";
        private const string c_description = "The number of shooter collision polygons in the map container.";
        #endregion // Constants

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public ShooterPolygonCountStatistic(IDictionary<StatKey, IMapSectionStatistic> rawStats)
            : base(c_name, c_description)
        {
            this.m_highDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Hd, StatEntityType.Dynamic)].GetPolyCountForCollisionType(CollisionType.Weapon);
            this.m_highNonDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Hd, StatEntityType.NonDynamic)].GetPolyCountForCollisionType(CollisionType.Weapon);
            this.m_totalStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Total, StatEntityType.Total)].GetPolyCountForCollisionType(CollisionType.Weapon);
        }
        #endregion // Constructors
    } // ShooterPolygonCountStatistic

    /// <summary>
    /// 
    /// </summary>
    public class CollisionPolygonRatioStatistic : RatioStatViewModelBase
    {
        #region Constants
        private const string c_name = "Collision Polygon Ratio";
        private const string c_description = "The ratio between the number of collision polygons and the number of polygons.";
        #endregion // Constants

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public CollisionPolygonRatioStatistic(IDictionary<StatKey, IMapSectionStatistic> rawStats)
            : base(c_name, c_description)
        {
            StatKey key = new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Hd, StatEntityType.Dynamic);
            uint polygonCount = rawStats[key].PolygonCount;
            if (polygonCount != 0)
            {
                this.m_highDynamicStatistic = (double)rawStats[key].CollisionCount / (double)polygonCount;
            }
            else
            {
                this.m_highDynamicStatistic = 0.0;
            }

            key = new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Hd, StatEntityType.NonDynamic);
            polygonCount = rawStats[key].PolygonCount;
            if (polygonCount != 0)
            {
                this.m_highNonDynamicStatistic = (double)rawStats[key].CollisionCount / (double)polygonCount;
            }
            else
            {
                this.m_highNonDynamicStatistic = 0.0;
            }

            key = new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Total, StatEntityType.Total);
            polygonCount = rawStats[key].PolygonCount;
            if (polygonCount != 0)
            {
                this.m_totalStatistic = (double)rawStats[key].CollisionCount / (double)polygonCount;
            }
            else
            {
                this.m_totalStatistic = 0.0;
            }
        }
        #endregion // Constructors
    } // CollisionPolygonRatioStatistic

    /// <summary>
    /// 
    /// </summary>
    public class MoverShooterPolygonRatioStatistic : RatioStatViewModelBase
    {
        #region Constants
        private const string c_name = "Mover/Shooter Polygon Ratio";
        private const string c_description = "The ratio between the number of mover collision polygons and the number of shooter collision polygons.";
        #endregion // Constants

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public MoverShooterPolygonRatioStatistic(IDictionary<StatKey, IMapSectionStatistic> rawStats)
            : base(c_name, c_description)
        {
            StatKey key = new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Hd, StatEntityType.Dynamic);
            uint polygonCount = rawStats[key].GetPolyCountForCollisionType(CollisionType.Weapon);
            if (polygonCount != 0)
            {
                this.m_highDynamicStatistic = (double)rawStats[key].GetPolyCountForCollisionType(CollisionType.Mover) / (double)polygonCount;
            }
            else
            {
                this.m_highDynamicStatistic = 0.0;
            }

            key = new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Hd, StatEntityType.NonDynamic);
            polygonCount = rawStats[key].GetPolyCountForCollisionType(CollisionType.Weapon);
            if (polygonCount != 0)
            {
                this.m_highNonDynamicStatistic = (double)rawStats[key].GetPolyCountForCollisionType(CollisionType.Mover) / (double)polygonCount;
            }
            else
            {
                this.m_highNonDynamicStatistic = 0.0;
            }

            key = new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Total, StatEntityType.Total);
            polygonCount = rawStats[key].GetPolyCountForCollisionType(CollisionType.Weapon);
            if (polygonCount != 0)
            {
                this.m_totalStatistic = (double)rawStats[key].GetPolyCountForCollisionType(CollisionType.Mover) / (double)polygonCount;
            }
            else
            {
                this.m_totalStatistic = 0.0;
            }
        }
        #endregion // Constructors
    } // MoverShooterPolygonRatioStatistic

    /// <summary>
    /// 
    /// </summary>
    public class DrawableDensityStatistic : DensityStatViewModelBase
    {
        #region Constants
        private const string c_name = "Drawable Density";
        private const string c_description = "The number of drawables per meter squared.";
        #endregion // Constants

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public DrawableDensityStatistic(IDictionary<StatKey, IMapSectionStatistic> rawStats, float area)
            : base(c_name, c_description)
        {
            if (area == 0.0f)
                return;

            float inverseArea = 1.0f / area;
            this.m_highDynamicStatistic = rawStats[new StatKey(StatGroup.Archetypes, StatLodLevel.Hd, StatEntityType.Dynamic)].TotalCount * inverseArea;
            this.m_lodDynamicStatistic = rawStats[new StatKey(StatGroup.Archetypes, StatLodLevel.Lod, StatEntityType.Dynamic)].TotalCount * inverseArea;
            this.m_slodDynamicStatistic = rawStats[new StatKey(StatGroup.Archetypes, StatLodLevel.SLod1, StatEntityType.Dynamic)].TotalCount * inverseArea;

            this.m_highNonDynamicStatistic = rawStats[new StatKey(StatGroup.Archetypes, StatLodLevel.Hd, StatEntityType.NonDynamic)].TotalCount * inverseArea;
            this.m_lodNonDynamicStatistic = rawStats[new StatKey(StatGroup.Archetypes, StatLodLevel.Lod, StatEntityType.NonDynamic)].TotalCount * inverseArea;
            this.m_slodNonDynamicStatistic = rawStats[new StatKey(StatGroup.Archetypes, StatLodLevel.SLod1, StatEntityType.NonDynamic)].TotalCount * inverseArea;

            this.m_totalStatistic = rawStats[new StatKey(StatGroup.Archetypes, StatLodLevel.Total, StatEntityType.Total)].TotalCount * inverseArea;
        }
        #endregion // Constructors
    } // DrawableDensityStatistic

    /// <summary>
    /// 
    /// </summary>
    public class DrawableInstanceDensityStatistic : DensityStatViewModelBase
    {
        #region Constants
        private const string c_name = "Drawable Instance Density";
        private const string c_description = "The number of drawable instances per meter squared.";
        #endregion // Constants

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public DrawableInstanceDensityStatistic(IDictionary<StatKey, IMapSectionStatistic> rawStats, float area)
            : base(c_name, c_description)
        {
            if (area == 0.0f)
                return;

            float inverseArea = 1.0f / area;
            this.m_highDynamicStatistic = rawStats[new StatKey(StatGroup.DrawableEntities, StatLodLevel.Hd, StatEntityType.Dynamic)].TotalCount * inverseArea;
            this.m_lodDynamicStatistic = rawStats[new StatKey(StatGroup.DrawableEntities, StatLodLevel.Lod, StatEntityType.Dynamic)].TotalCount * inverseArea;
            this.m_slodDynamicStatistic = rawStats[new StatKey(StatGroup.DrawableEntities, StatLodLevel.SLod1, StatEntityType.Dynamic)].TotalCount * inverseArea;

            this.m_highNonDynamicStatistic = rawStats[new StatKey(StatGroup.DrawableEntities, StatLodLevel.Hd, StatEntityType.NonDynamic)].TotalCount * inverseArea;
            this.m_lodNonDynamicStatistic = rawStats[new StatKey(StatGroup.DrawableEntities, StatLodLevel.Lod, StatEntityType.NonDynamic)].TotalCount * inverseArea;
            this.m_slodNonDynamicStatistic = rawStats[new StatKey(StatGroup.DrawableEntities, StatLodLevel.SLod1, StatEntityType.NonDynamic)].TotalCount * inverseArea;

            this.m_totalStatistic = rawStats[new StatKey(StatGroup.DrawableEntities, StatLodLevel.Total, StatEntityType.Total)].TotalCount * inverseArea;
        }
        #endregion // Constructors
    } // DrawableInstanceDensityStatistic

    /// <summary>
    /// 
    /// </summary>
    public class NonDrawableDensityStatistic : DensityStatViewModelBase
    {
        #region Constants
        private const string c_name = "Non-Drawable Instance Density";
        private const string c_description = "The number of non-drawable instances per meter squared.";
        #endregion // Constants

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public NonDrawableDensityStatistic(IDictionary<StatKey, IMapSectionStatistic> rawStats, float area)
            : base(c_name, c_description)
        {
            if (area == 0.0f)
                return;

            float inverseArea = 1.0f / area;
            this.m_highDynamicStatistic = rawStats[new StatKey(StatGroup.NonDrawableEntities, StatLodLevel.Hd, StatEntityType.Dynamic)].TotalCount * inverseArea;
            this.m_lodDynamicStatistic = rawStats[new StatKey(StatGroup.NonDrawableEntities, StatLodLevel.Lod, StatEntityType.Dynamic)].TotalCount * inverseArea;
            this.m_slodDynamicStatistic = rawStats[new StatKey(StatGroup.NonDrawableEntities, StatLodLevel.SLod1, StatEntityType.Dynamic)].TotalCount * inverseArea;

            this.m_highNonDynamicStatistic = rawStats[new StatKey(StatGroup.NonDrawableEntities, StatLodLevel.Hd, StatEntityType.NonDynamic)].TotalCount * inverseArea;
            this.m_lodNonDynamicStatistic = rawStats[new StatKey(StatGroup.NonDrawableEntities, StatLodLevel.Lod, StatEntityType.NonDynamic)].TotalCount * inverseArea;
            this.m_slodNonDynamicStatistic = rawStats[new StatKey(StatGroup.NonDrawableEntities, StatLodLevel.SLod1, StatEntityType.NonDynamic)].TotalCount * inverseArea;

            this.m_totalStatistic = rawStats[new StatKey(StatGroup.NonDrawableEntities, StatLodLevel.Total, StatEntityType.Total)].TotalCount * inverseArea;
        }
        #endregion // Constructors
    } // NonDrawableDensityStatistic

    /// <summary>
    /// 
    /// </summary>
    public class TxdDensityStatistic : DensityStatViewModelBase
    {
        #region Constants
        private const string c_name = "TXD Density";
        private const string c_description = "The number of unique texture dictionaries per meter squared.";
        #endregion // Constants

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public TxdDensityStatistic(IDictionary<StatKey, IMapSectionStatistic> rawStats, float area)
            : base(c_name, c_description)
        {
            if (area == 0.0f)
                return;

            float inverseArea = 1.0f / area;
            this.m_highDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Hd, StatEntityType.Dynamic)].TXDCount * inverseArea;
            this.m_lodDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Lod, StatEntityType.Dynamic)].TXDCount * inverseArea;
            this.m_slodDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.SLod1, StatEntityType.Dynamic)].TXDCount * inverseArea;

            this.m_highNonDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Hd, StatEntityType.NonDynamic)].TXDCount * inverseArea;
            this.m_lodNonDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Lod, StatEntityType.NonDynamic)].TXDCount * inverseArea;
            this.m_slodNonDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.SLod1, StatEntityType.NonDynamic)].TXDCount * inverseArea;

            this.m_totalStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Total, StatEntityType.Total)].TXDCount * inverseArea;
        }
        #endregion // Constructors
    } // TxdDensityStatistic

    /// <summary>
    /// 
    /// </summary>
    public class ShaderDensityStatistic : DensityStatViewModelBase
    {
        #region Constants
        private const string c_name = "Shader Density";
        private const string c_description = "The number of unique shaders per meter squared.";
        #endregion // Constants

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public ShaderDensityStatistic(IDictionary<StatKey, IMapSectionStatistic> rawStats, float area)
            : base(c_name, c_description)
        {
            if (area == 0.0f)
                return;

            float inverseArea = 1.0f / area;
            this.m_highDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Hd, StatEntityType.Dynamic)].ShaderCount * inverseArea;
            this.m_lodDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Lod, StatEntityType.Dynamic)].ShaderCount * inverseArea;
            this.m_slodDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.SLod1, StatEntityType.Dynamic)].ShaderCount * inverseArea;

            this.m_highNonDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Hd, StatEntityType.NonDynamic)].ShaderCount * inverseArea;
            this.m_lodNonDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Lod, StatEntityType.NonDynamic)].ShaderCount * inverseArea;
            this.m_slodNonDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.SLod1, StatEntityType.NonDynamic)].ShaderCount * inverseArea;

            this.m_totalStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Total, StatEntityType.Total)].ShaderCount * inverseArea;
        }
        #endregion // Constructors
    } // ShaderDensityStatistic

    /// <summary>
    /// 
    /// </summary>
    public class PolygonDensityStatistic : DensityStatViewModelBase
    {
        #region Constants
        private const string c_name = "Polygon Density";
        private const string c_description = "The number of polygons per meter squared.";
        #endregion // Constants

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public PolygonDensityStatistic(IDictionary<StatKey, IMapSectionStatistic> rawStats, float area)
            : base(c_name, c_description)
        {
            if (area == 0.0f)
                return;
            
            float inverseArea = 1.0f / area;
            this.m_highDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Hd, StatEntityType.Dynamic)].PolygonCount * inverseArea;
            this.m_lodDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Lod, StatEntityType.Dynamic)].PolygonCount * inverseArea;
            this.m_slodDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.SLod1, StatEntityType.Dynamic)].PolygonCount * inverseArea;

            this.m_highNonDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Hd, StatEntityType.NonDynamic)].PolygonCount * inverseArea;
            this.m_lodNonDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Lod, StatEntityType.NonDynamic)].PolygonCount * inverseArea;
            this.m_slodNonDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.SLod1, StatEntityType.NonDynamic)].PolygonCount * inverseArea;

            this.m_totalStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Total, StatEntityType.Total)].PolygonCount * inverseArea;
        }
        #endregion // Constructors
    } // PolygonDensityStatistic

    /// <summary>
    /// 
    /// </summary>
    public class CollisionPolygonDensityStatistic : DensityStatViewModelBase
    {
        #region Constants
        private const string c_name = "Collision Polygon Density";
        private const string c_description = "The number of collision polygons per meter squared.";
        #endregion // Constants

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public CollisionPolygonDensityStatistic(IDictionary<StatKey, IMapSectionStatistic> rawStats, float area)
            : base(c_name, c_description)
        {
            if (area == 0.0f)
                return;

            float inverseArea = 1.0f / area;
            this.m_highDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Hd, StatEntityType.Dynamic)].CollisionCount * inverseArea;
            this.m_highNonDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Hd, StatEntityType.NonDynamic)].CollisionCount * inverseArea;
            this.m_totalStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Total, StatEntityType.Total)].CollisionCount * inverseArea;
        }
        #endregion // Constructors
    } // CollisionPolygonDensityStatistic

    /// <summary>
    /// 
    /// </summary>
    public class LodDistanceStatistic : DensityStatViewModelBase
    {
        #region Constants
        private const string c_name = "Maximum Lod Distance";
        private const string c_description = "The lod distance.";
        #endregion // Constants

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public LodDistanceStatistic(IDictionary<StatKey, IMapSectionStatistic> rawStats)
            : base(c_name, c_description)
        {
            this.m_highDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Hd, StatEntityType.Dynamic)].LodDistance;
            this.m_lodDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Lod, StatEntityType.Dynamic)].LodDistance;
            this.m_slodDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.SLod1, StatEntityType.Dynamic)].LodDistance;

            this.m_highNonDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Hd, StatEntityType.NonDynamic)].LodDistance;
            this.m_lodNonDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Lod, StatEntityType.NonDynamic)].LodDistance;
            this.m_slodNonDynamicStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.SLod1, StatEntityType.NonDynamic)].LodDistance;

            this.m_totalStatistic = rawStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Total, StatEntityType.Total)].LodDistance;
        }
        #endregion // Constructors
    } // LodDistanceStatistic

}
