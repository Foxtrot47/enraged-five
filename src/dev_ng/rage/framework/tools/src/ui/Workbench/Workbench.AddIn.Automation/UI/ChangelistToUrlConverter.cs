﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Workbench.AddIn.Automation.UI
{

    /// <summary>
    /// 
    /// </summary>
    internal class ChangelistToUrlConverter : IValueConverter
    {
#warning DHM FIX ME: this obviously should be read from data.
        private static readonly String BASE_URL = "http://rsgedip4s1:8080/@md=d&cd=//&c=7FM@/{0}?ac=10";

        public object Convert(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            uint changelist = 0;
            if (value is String)
                uint.TryParse((String)value, out changelist);
            else if (value is uint)
                changelist = (uint)value;

            Uri result;
            Uri.TryCreate(String.Format(BASE_URL, changelist), UriKind.RelativeOrAbsolute, out result);
            return (result);
        }

        public Object ConvertBack(Object value, Type targetTypes, Object parameter, CultureInfo culture)
        {
            throw new NotImplementedException("This converter cannot be used in two-way binding.");
        }

    }

} // Workbench.AddIn.Automation.UI namespace
