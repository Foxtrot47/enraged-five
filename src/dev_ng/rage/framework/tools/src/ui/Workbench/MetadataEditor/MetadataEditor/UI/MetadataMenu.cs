﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.IO;
using System.Linq;
using RSG.Base.Extensions;
using RSG.Platform;
using Workbench.AddIn;
using Workbench.AddIn.Commands;
using Workbench.AddIn.Services;
using Workbench.AddIn.Services.File;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.UI.Menu;
using MetadataEditor.AddIn;
using MetadataEditor.RestData;
using Ionic.Zip;

namespace MetadataEditor.UI
{

    /// <summary>
    /// Metadata Menu
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.MenuMain, typeof(IWorkbenchCommand))]
    class MetadataMenu : WorkbenchMenuItemBase,
        IPartImportsSatisfiedNotification
    {
        #region MEF Imports
        /// <summary>
        /// Extensions service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ExtensionService)]
        private IExtensionService ExtensionService { get; set; }

        /// <summary>
        /// Metadata menu subitems.
        /// </summary>
        [ImportManyExtension(MetadataEditor.AddIn.ExtensionPoints.MenuMetadata,
            typeof(IWorkbenchCommand))]
        private IEnumerable<IWorkbenchCommand> items { get; set; }
        #endregion // MEF Imports

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public MetadataMenu()
        {
            this.Header = "_Metadata";
            this.ID = new Guid(MetadataEditor.AddIn.ExtensionPoints.MenuMetadata);
            this.RelativeID = new Guid(Workbench.AddIn.ExtensionPoints.MenuEdit);
            this.Direction = Direction.After;
        }
        #endregion // Constructor(s)

        #region IPartImportsSatisfiedNotification Methods
        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {
            this.Items = ExtensionService.Sort(items);
        }
        #endregion // IPartImportsSatisfiedNotification Methods

        #region ICommand Implementation

        public override bool CanExecute(object parameter)
        {
            return true;
        }

        public override void Execute(object parameter)
        {
        }

        #endregion // ICommand Implementation
    }

    /// <summary>
    /// Metadata Menu
    /// </summary>
    [ExportExtension(MetadataEditor.AddIn.ExtensionPoints.MenuMetadata, typeof(IWorkbenchCommand))]
    class MetadataOptionsMenuItem :
        WorkbenchMenuItemBase
    {
        #region MEF Imports

        /// <summary>
        /// Message service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.MessageService)]
        private IMessageService MessageService { get; set; }

        /// <summary>
        /// Config service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService)]
        private IConfigurationService ConfigService { get; set; }

        [ImportExtension(MetadataEditor.AddIn.CompositionPoints.StructureDictionary,
            typeof(IStructureDictionary))]
        private Lazy<IStructureDictionary> StructureDictionary { get; set; }

        #endregion // MEF Imports

        #region Constants

        public static readonly Guid GUID = new Guid("5B516E9F-BD3A-4E24-B73B-9495E3DFBAB5");

        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public MetadataOptionsMenuItem()
        {
            this.Header = "_Set Definition Directory";
            this.ID = GUID;
        }
        #endregion // Constructor(s)

        #region ICommand Implementation

        public override bool CanExecute(object parameter)
        {
            return true;
        }

        public override void Execute(object parameter)
        {
            String previous = String.Copy(StructureDictionary.Value.DefinitionRootLocation);
            View.MetadataOptions view = new View.MetadataOptions();
            View.MetadataOptionsViewModel dc = new View.MetadataOptionsViewModel(
                this.ConfigService.Config.Project.DefaultBranch, 
                StructureDictionary.Value.DefinitionRootLocation, view);
            view.Owner = System.Windows.Application.Current.MainWindow;
            view.DataContext = dc;

            if (view.ShowDialog() == true && previous != dc.DefinitionRootLocation)
            {
                StructureDictionary.Value.SetDefinitionRootLocation(dc.DefinitionRootLocation);
                System.Windows.MessageBoxResult result = MessageService.Show("Do you want to refresh the metadata definitions now that the root location has changed?", System.Windows.MessageBoxButton.YesNo, System.Windows.MessageBoxImage.Question);
                if (result == System.Windows.MessageBoxResult.Yes)
                {
                    StructureDictionary.Value.RefreshDefinitions();
                }
            }
        }

        #endregion // ICommand Implementation
    }

    /// <summary>
    /// Clear Preview Folder Menu Item
    /// </summary>
    [ExportExtension(MetadataEditor.AddIn.ExtensionPoints.MenuMetadata, typeof(IWorkbenchCommand))]
    class MetadataClearPreviewFolderMenuItem :
        WorkbenchMenuItemBase
    {
        #region MEF Imports
        /// <summary>
        /// Configuration service
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(Workbench.AddIn.Services.IConfigurationService))]
        private IConfigurationService Config { get; set; }
        #endregion // MEF Imports

        #region Constants
        /// <summary>
        /// Unique GUID for the menu item
        /// </summary>
        public static readonly Guid GUID = new Guid("DA5058B5-B6CC-4EBF-BB65-38D18D6FA22E");
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public MetadataClearPreviewFolderMenuItem()
        {
            this.Header = "_Clear Preview Folder";
            this.ID = GUID;
        }
        #endregion // Constructor(s)

        #region ICommand Implementation
        public override bool CanExecute(object parameter)
        {
            return true;
        }

        public override void Execute(object parameter)
        {
            try
            {
                Directory.Delete(Config.GameConfig.PreviewDir, true);
            }
            catch (Exception e)
            {
            }
        }
        #endregion // ICommand Implementation
    }
} // MetadataEditor.UI namespace
