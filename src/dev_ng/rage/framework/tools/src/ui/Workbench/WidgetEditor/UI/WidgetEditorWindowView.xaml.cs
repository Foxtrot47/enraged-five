﻿using System;
using System.Windows;
using System.Windows.Data;
using System.Windows.Controls;
using Workbench.AddIn;
using Workbench.AddIn.UI.Layout;
using RSG.Base.Windows.Controls;
using Workbench.AddIn.Services.File;
using RSG.Base.Editor;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Net.Sockets;
using System.Text;
using WidgetEditor.ViewModel;
using RSG.Model.LiveEditing;
using System.Diagnostics;

namespace WidgetEditor.UI
{
    /// <summary>
    /// Interaction logic for LogToolWindowView.xaml
    /// </summary>
    public partial class WidgetEditorWindowView : ToolWindowBase<WidgetEditorWindowViewModel>
    {
        #region Constants

        public static readonly Guid GUID = new Guid( "5669849B-2936-4F70-86AF-C3E9C68750C7" );
        #endregion // Constants

        #region Constructor


        public WidgetEditorWindowView( 
            ILayoutManager LayoutManager )
            : base( "WidgetEditor",
                   new WidgetEditorWindowViewModel( LayoutManager ) )
        {
            InitializeComponent();

            this.ID = GUID;
            this.SaveModel = this.ViewModel;
        }


        #endregion // Constructor

        #region Events

        private void OnItemMouseDoubleClick(object sender, RoutedEventArgs args) 
        {
            Debug.Assert( sender is TreeViewItem );
            if ( sender is TreeViewItem )
            {
                TreeViewItem treeviewItem = (TreeViewItem)sender;
                object itemContext = treeviewItem.DataContext;
                RESTService service = (RESTService)itemContext;

                ViewModel.OpenServiceCommand.Execute( service );
            }
        }

        #endregion
    }

} // Workbench.UI namespace
