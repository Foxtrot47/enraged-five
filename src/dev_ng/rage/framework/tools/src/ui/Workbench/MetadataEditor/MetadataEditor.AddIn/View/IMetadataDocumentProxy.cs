﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn.UI.Layout;
using RSG.Metadata.Model;
using RSG.Metadata.Parser;

namespace MetadataEditor.AddIn.View
{
    /// <summary>
    /// Enumeration description of the type of Metadata Document.
    /// </summary>
    /// This enumeration describes to the Metadata Editor the type of document
    /// data that can be edited.  The 'Default' is a default view which should
    /// be used if no type specific document is found.  'Fallback' is a special
    /// simple document that relies on no type information.
    /// 
    /// 'TypeSpecific' uses the SupportedMetadataTypes property to determine
    /// which types can be edited by that document class.
    /// 
    public enum MetadataDocumentType
    {
        Default,
        Fallback,
        TypeSpecific
    }

    public interface IMetadataDocumentProxy
    {
        /// <summary>
        /// Array of String Metadata types this Metadata Document can view
        /// (e.g. "*", "::rage::cutfCutSceneFile2").
        /// </summary>
        String[] SupportedMetadataTypes { get; }

        /// <summary>
        /// Type of this document.
        /// </summary>
        MetadataDocumentType DocumentType { get; }
        
        /// <summary>
        /// Array of support document GUIDs.
        /// </summary>
        Guid[] SupportedContent { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="metaFile"></param>
        /// <returns></returns>
        bool CreateFile(out IMetaFile metaFile, string filename, StructureDictionary structureDictionary, Structure rootDefinition);

        /// <summary>
        /// Used to create a new instance of this document type from the given model
        /// </summary>
        Boolean OpenDocument(out IDocumentBase document, IMetaFile file);

        /// <summary>
        /// Used to create a new instance of this document type
        /// </summary>
        Boolean CreateNewDocument(out IDocumentBase document, out RSG.Base.Editor.IModel model, RSG.Metadata.Parser.Structure definition, StructureDictionary structureDictionary);
    }
}
