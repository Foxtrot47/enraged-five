﻿using System;
using System.ComponentModel.Composition;
using System.IO;
using System.Web.UI;
using RSG.Base.Logging;
using RSG.Interop.Bugstar.Game;
using RSG.Interop.Bugstar.Organisation;
using RSG.Model.Report;
using Workbench.AddIn.Services;
using RSG.Base.Tasks;

namespace Workbench.AddIn.Bugstar.Reports
{
    /// <summary>
    /// A dynamic report that queries bugstar for mission information and outputs the formatted results
    /// </summary>
    [ExportExtension(ExtensionPoints.Report, typeof(IReport))]
    class BugstarMissionsReport :
        HTMLReport,
        IDynamicReport
    {
        #region Constants
        private static readonly String NAME = "Bugstar Missions";
        private static readonly String DESC = "Bugstar Missions Report";
        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// MEF import for login service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LoginService, typeof(Workbench.AddIn.Services.ILoginService))]
        public Workbench.AddIn.Services.ILoginService LoginService { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(CompositionPoints.ConfigurationService, typeof(Workbench.AddIn.Services.IConfigurationService))]
        Lazy<Workbench.AddIn.Services.IConfigurationService> Config { get; set; }

        /// <summary>
        /// Bugstar Service MEF import.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.BugstarService, typeof(IBugstarService))]
        protected IBugstarService BugstarService { get; set; }
        #endregion // MEF Imports

        #region Properties
        /// <summary>
        /// Task to run for generating this report.
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (m_task == null)
                {
                    m_task = new ActionTask("Generating report", (context, progress) => GenerateReport((DynamicReportContext)context, progress));
                }
                return m_task;
            }
        }
        private ITask m_task;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// A ReportUri and a Builder Report concrete class - MEF constructed
        /// </summary>
        public BugstarMissionsReport()
            : base(NAME, DESC)
        {
        }
        #endregion // Constructor(s)

        #region DynamicReportStream Interface
        /// <summary>
        /// Generates the report dynamically
        /// </summary>
        private void GenerateReport(DynamicReportContext context, IProgress<TaskProgress> progress)
        {
            try
            {
                Mission[] missions = BugstarService.Project.Missions;

                string tempFilePath = Path.GetTempFileName();

                // Generate the html that we will be displaying
                MemoryStream stream = new MemoryStream();
                StreamWriter writer = new StreamWriter(stream);

                HtmlTextWriter htmlWriter = new HtmlTextWriter(writer);
                htmlWriter.RenderBeginTag(HtmlTextWriterTag.Html);
                htmlWriter.RenderBeginTag(HtmlTextWriterTag.Head);
                htmlWriter.RenderEndTag();
                htmlWriter.RenderBeginTag(HtmlTextWriterTag.Body);

                htmlWriter.RenderBeginTag(HtmlTextWriterTag.Table);

                // Header column
                htmlWriter.RenderBeginTag(HtmlTextWriterTag.Tr);
                htmlWriter.RenderBeginTag(HtmlTextWriterTag.Td);
                htmlWriter.RenderBeginTag(HtmlTextWriterTag.B);
                htmlWriter.Write("Name");
                htmlWriter.RenderEndTag();
                htmlWriter.RenderEndTag();
                htmlWriter.RenderBeginTag(HtmlTextWriterTag.Td);
                htmlWriter.RenderBeginTag(HtmlTextWriterTag.B);
                htmlWriter.Write("Mission ID");
                htmlWriter.RenderEndTag();
                htmlWriter.RenderEndTag();
                htmlWriter.RenderBeginTag(HtmlTextWriterTag.Td);
                htmlWriter.RenderBeginTag(HtmlTextWriterTag.B);
                htmlWriter.Write("Description");
                htmlWriter.RenderEndTag();
                htmlWriter.RenderEndTag();
                htmlWriter.RenderEndTag();

                foreach (Mission mission in missions)
                {
                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.Tr);
                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.Td);
                    htmlWriter.Write(mission.Name);
                    htmlWriter.RenderEndTag();

                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.Td);
                    htmlWriter.Write(mission.MissionId);
                    htmlWriter.RenderEndTag();

                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.Td);
                    htmlWriter.Write(mission.Description);
                    htmlWriter.RenderEndTag();
                    htmlWriter.RenderEndTag();
                }

                htmlWriter.RenderEndTag();

                htmlWriter.RenderEndTag();
                htmlWriter.RenderEndTag();
                htmlWriter.Flush();

                stream.Position = 0;
                Stream = stream;
            }
            catch (System.Exception ex)
            {
                RSG.Base.Logging.Log.Log__Exception(ex, "Unhandled bugstar exception");
            }
        }
        #endregion // DynamicReportUri Interface
    } // BugstarMissionsReport
}
