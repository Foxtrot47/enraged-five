﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using Workbench.AddIn.Services;

namespace Workbench.AddIn.PropViewer
{
    public class PropViewerViewModel : ViewModelBase
    {
        public List<RSG.Model.Map.Prop> Props
        {
            get { return m_props; }
            set { m_props = value; }
        }
        private List<RSG.Model.Map.Prop> m_props = new List<RSG.Model.Map.Prop>();


        public PropViewerViewModel()
        {
        }

        public PropViewerViewModel(RSG.Model.Map.MapSection section, IConfigurationService config)
        {
            List<RSG.SceneXml.ObjectDef> props = new List<RSG.SceneXml.ObjectDef>();
            RSG.SceneXml.Scene scene = RSG.Model.Map.SceneManager.GetScene(section.SceneXmlFilename);
            foreach (RSG.SceneXml.ObjectDef sceneObject in scene.Objects)
            {
                if (sceneObject.IsXRef())
                    continue;
                if (sceneObject.IsRefObject())
                    continue;
                if (sceneObject.IsInternalRef())
                    continue;
                if (sceneObject.DontExport())
                    continue;
                if (sceneObject.HasDrawableLODChildren())
                    continue; // Ignore objects that are drawable LODs.
                if (sceneObject.IsDummy())
                    continue;

                if (sceneObject.IsObject())
                {
                    props.Add(sceneObject);
                }

                if (sceneObject.IsContainer())
                {
                    foreach (RSG.SceneXml.ObjectDef child in sceneObject.Children)
                    {
                        props.Add(child);
                    }
                }
            }

            String streampath = System.IO.Path.Combine(config.GameConfig.NetworkGenericStreamDir, "propViewerRenders");
            foreach (RSG.SceneXml.ObjectDef prop in props)
            {
                this.Props.Add(new RSG.Model.Map.Prop(prop, section, streampath));
            }
        }
    }
}
