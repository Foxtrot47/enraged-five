﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml.Linq;
using System.Xml.XPath;
using Ionic.Zip;
using MapViewport.AddIn;
using RSG.Base.Drawing;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Base.Math;
using RSG.Base.Tasks;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using RSG.Base.Windows.Extensions;
using RSG.Base.Windows.Helpers;
using RSG.Model.Common;
using RSG.Model.Common.Util;
using RSG.Pipeline.Content;
using RSG.Pipeline.Core;
using RSG.Pipeline.Engine;
using RSG.Pipeline.Processor.Map;
using RSG.Pipeline.Services;
using RSG.Pipeline.Services.Platform.Manifest;
using RSG.Platform;
using Workbench.AddIn.Services;
using Workbench.AddIn.MapDebugging.OverlayViews;
using System.Windows.Controls;
using RSG.Base.Windows.Controls.WpfViewport2D.Controls;
using System.ComponentModel;
using RSG.Base.Configuration;

namespace Workbench.AddIn.MapDebugging.Overlays
{
    /// <summary>
    /// Model object for IMAP files.
    /// </summary>
    public class IMAPFile
    {
        public String Name { get; set; }
        public BoundingBox3f StreamingExtents { get; set; }
        public uint EntityCount { get; set; }

        private IMAPFile()
        {
            StreamingExtents = new BoundingBox3f();
        }

        public static IMAPFile CreateFromStream(Stream stream)
        {
            try
            {
                XDocument doc = XDocument.Load(stream);
                XElement nameElem = doc.XPathSelectElement("/CMapData/name");
                XElement minElem = doc.XPathSelectElement("/CMapData/streamingExtentsMin");
                XElement maxElem = doc.XPathSelectElement("/CMapData/streamingExtentsMax");
                IEnumerable<XElement> entityElems = doc.XPathSelectElements("/CMapData/entities/Item[@type='CEntityDef']");

                IMAPFile imap = new IMAPFile();
                imap.Name = nameElem.Value;
                imap.StreamingExtents.Min = new Vector3f(Single.Parse(minElem.Attribute("x").Value),
                    Single.Parse(minElem.Attribute("y").Value), Single.Parse(minElem.Attribute("z").Value));
                imap.StreamingExtents.Max = new Vector3f(Single.Parse(maxElem.Attribute("x").Value),
                    Single.Parse(maxElem.Attribute("y").Value), Single.Parse(maxElem.Attribute("z").Value));
                imap.EntityCount = (uint)entityElems.Count();

                return imap;
            }
            catch (Exception e)
            {
                Log.Log__Exception(e, "Failed to parse IMAP stream.");
                return null;
            }
        }
    } // IMAPFile

    /// <summary>
    /// Model object for ITYP files.
    /// </summary>
    public class ITYPFile
    {
        public String Name { get; set; }
        public uint ArchetypeCount { get; set; }

        public ITYPFile(String name, uint archetypeCount)
        {
            Name = name;
            ArchetypeCount = archetypeCount;
        }

        public static ITYPFile CreateFromStream(Stream stream)
        {
            try
            {
                XDocument doc = XDocument.Load(stream);
                XElement nameElem = doc.XPathSelectElement("/CMapTypes/name");
                IEnumerable<XElement> archetypeElems = doc.XPathSelectElements("/CMapTypes/archetypes/Item[@type='CBaseArchetypeDef']");

                return new ITYPFile(nameElem.Value, (uint)archetypeElems.Count());
            }
            catch (Exception e)
            {
                Log.Log__Exception(e, "Failed to parse ITYP stream.");
                return null;
            }
        }
    } // ITYPFile

    /// <summary>
    /// 
    /// </summary>
    public class MapExportAdditions
    {
        public IList<IMAPDependency2> IMAPDependencies { get; set; }

        public MapExportAdditions()
        {
            IMAPDependencies = new List<IMAPDependency2>();
        }

        public static MapExportAdditions CreateFromStream(Stream stream)
        {
            try
            {
                MapExportAdditions expAdditions = new MapExportAdditions();
                XDocument doc = XDocument.Load(stream);
                foreach (XElement depElem in doc.XPathSelectElements("/ManifestData/IMAPDependency2"))
                {
                    expAdditions.IMAPDependencies.Add(IMAPDependency2.FromMapExportAdditions(depElem));
                }
                return expAdditions;
            }
            catch (System.Exception ex)
            {
                Log.Log__Exception(ex, "Failed to parse MapExportAdditions stream.");
                return null;
            }
        }
    }
    /*
    /// <summary>
    /// Encapsulates information about a dependency between IMAP and ITYP files.
    /// </summary>
    public class IMAPDependency
    {
        public bool IsInterior { get; set; }
        public String IMAPName { get; set; }
        public IList<String> ITYPNames { get; set; }

        public IMAPDependency(XElement elem)
        {
            IsInterior = Boolean.Parse(elem.Attribute("isInterior").Value);
            IMAPName = elem.Attribute("imapName").Value;
            ITYPNames = elem.Attribute("itypeNames").Value.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
        }
    } // IMAPDependency
    */

    /// <summary>
    /// Object that contains a single piece of information about the selected position on the map.
    /// </summary>
    public class SelectionInfo
    {
        public IMAPFile IMAP { get; set; }
        public ITYPFile ITYP { get; set; }
    } // SelectionInfo

    public class SelectionInfoComparer : ListViewCustomComparer<SelectionInfo>
    {
        /// <summary>
        /// Compares the specified x to y.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <returns></returns>
        public override int Compare(SelectionInfo x, SelectionInfo y)
        {
            try
            {
                String valueX = String.Empty, valueY = String.Empty;
                switch (SortBy)
                {
                    default:
                    case "IMAP Name":
                        valueX = x.IMAP.Name;
                        valueY = y.IMAP.Name;
                        break;
                    case "ITYP Name":
                        valueX = x.ITYP.Name;
                        valueY = y.ITYP.Name;
                        break;
                    case "Archetype Count":
                        if (SortDirection.Equals(ListSortDirection.Ascending))
                        {
                            return x.ITYP.ArchetypeCount.CompareTo(y.ITYP.ArchetypeCount);
                        }
                        else
                        {
                            return (-1) * x.ITYP.ArchetypeCount.CompareTo(y.ITYP.ArchetypeCount);
                        }
                }

                if (SortDirection.Equals(ListSortDirection.Ascending))
                {
                    return String.Compare(valueX, valueY);
                }
                else
                {
                    return (-1) * String.Compare(valueX, valueY);
                }

            }
            catch (Exception)
            {
                return 0;
            }
        }
    }


    /// <summary>
    /// 
    /// </summary>
    [ExportExtension("Workbench.AddIn.MapDebugging.OverlayGroups.MapDebug", typeof(Viewport.AddIn.ViewportOverlay))]
    public class ArchetypeITYPOverlay : LevelDependentViewportOverlay
    {
        #region Constants
        private const String c_name = "Archetype ITYP";
        private const String c_description = "Overlay showing number of archetypes that will be loaded in various areas of the map.";

        /// <summary>
        /// Filename that contains the IMAP -> ITYP mappings.
        /// </summary>
        private const String c_additionsFilename = "mapexportadditions.xml";

        private const String Processor_MapDummyMetadataMerge = "RSG.Pipeline.Processor.Map.DummyMetadataMergeProcessor";
        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// The perforce sync service.
        /// </summary>
        [ImportExtension(CompositionPoints.PerforceSyncService, typeof(IPerforceSyncService))]
        private Lazy<IPerforceSyncService> PerforceSyncService { get; set; }

        /// <summary>
        /// MEF import for message box service.
        /// </summary>
        [ImportExtension(CompositionPoints.TaskProgressService, typeof(ITaskProgressService))]
        private Lazy<ITaskProgressService> TaskProgressService { get; set; }

        /// <summary>
        /// A reference to view model for the map viewport.
        /// </summary>
        [ImportExtension(Viewport.AddIn.CompositionPoints.MapViewport, typeof(Viewport.AddIn.IMapViewport))]
        private Lazy<Viewport.AddIn.IMapViewport> MapViewportProxy { get; set; }
        #endregion // MEF Imports
        
        #region Properties
        #region Stats
        /// <summary>
        /// Minimum value.
        /// </summary>
        public uint MinValue
        {
            get { return m_minValue; }
            set { SetPropertyValue(ref m_minValue, value, "MinValue"); }
        }
        private uint m_minValue;

        /// <summary>
        /// Maximum value.
        /// </summary>
        public uint MaxValue
        {
            get { return m_maxValue; }
            set { SetPropertyValue(ref m_maxValue, value, "MaxValue"); }
        }
        private uint m_maxValue;

        /// <summary>
        /// Average value.
        /// </summary>
        public double AverageValue
        {
            get { return m_averageValue; }
            set { SetPropertyValue(ref m_averageValue, value, "AverageValue"); }
        }
        private double m_averageValue;
        #endregion // Stats

        #region Render Options
        /// <summary>
        /// 
        /// </summary>
        [OverlayParameter(100, FriendlyName = "Resolution")]
        public int Resolution
        {
            get { return m_resolution; }
            set
            {
                if (SetPropertyValue(ref m_resolution, value, "Resolution"))
                {
                    // HACK :/
                    if (IsCurrentlyActive)
                    {
                        ResolutionChanged = true;
                    }
                }
            }
        }
        private int m_resolution;

        /// <summary>
        /// 
        /// </summary>
        public bool MouseClickMode
        {
            get { return m_mouseClickMode; }
            set
            {
                if (SetPropertyValue(ref m_mouseClickMode, value, "MouseClickMode") && value == false && IsCurrentlyActive)
                {
                    ResolutionChanged = true;
                }
            }
        }
        private bool m_mouseClickMode;

        /// <summary>
        /// Flag that gets set when the resolution has changed.
        /// </summary>
        [OverlayParameter(false, FriendlyName = "Resolution Changed")]
        public bool ResolutionChanged
        {
            get { return m_resolutionChanged; }
            set { SetPropertyValue(ref m_resolutionChanged, value, "ResolutionChanged"); }
        }
        private bool m_resolutionChanged;

        /// <summary>
        /// 
        /// </summary>
        [OverlayParameter(0u, FriendlyName = "Low Limit")]
        public uint LowValue
        {
            get { return m_lowValue; }
            set
            {
                if (SetPropertyValue(ref m_lowValue, value, "LowValue"))
                {
                    OnColoursChanged();
                }
            }
        }
        private uint m_lowValue;

        /// <summary>
        /// 
        /// </summary>
        [OverlayParameter(100u, FriendlyName = "High Limit")]
        public uint HighValue
        {
            get { return m_highValue; }
            set
            {
                if (SetPropertyValue(ref m_highValue, value, "HighValue"))
                {
                    OnColoursChanged();
                }
            }
        }
        private uint m_highValue;

        /// <summary>
        /// 
        /// </summary>
        [OverlayParameter(65.0f, FriendlyName = "Opacity")]
        public float OverlayOpacity
        {
            get { return m_overlayOpacity; }
            set
            {
                if (SetPropertyValue(ref m_overlayOpacity, value, "OverlayOpacity"))
                {
                    OnColoursChanged();
                }
            }
        }
        private float m_overlayOpacity;

        /// <summary>
        /// 
        /// </summary>
        public System.Windows.Media.Color LowColor
        {
            get { return m_lowColor; }
            set
            {
                if (SetPropertyValue(ref m_lowColor, value, "LowColor"))
                {
                    OnColoursChanged();
                }
            }
        }
        private System.Windows.Media.Color m_lowColor;

        /// <summary>
        /// Used for serialisation of the low color
        /// </summary>
        [OverlayParameter(4278255360u, FriendlyName = "Low Color")]
        public uint LowColor_SerialisationSurrogate
        {
            get
            {
                return LowColor.ToInteger();
            }
            set
            {
                LowColor = ColorExtensions.FromInteger(value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public System.Windows.Media.Color HighColor
        {
            get { return m_highColor; }
            set
            {
                if (SetPropertyValue(ref m_highColor, value, "HighColor"))
                {
                    OnColoursChanged();
                }
            }
        }
        private System.Windows.Media.Color m_highColor;

        /// <summary>
        /// Used for serialisation of the high color
        /// </summary>
        [OverlayParameter(4294901760u, FriendlyName = "High Color")]
        public uint HighColor_SerialisationSurrogate
        {
            get
            {
                return HighColor.ToInteger();
            }
            set
            {
                HighColor = ColorExtensions.FromInteger(value);
            }
        }
        #endregion // Render Options

        #region Legend
        /// <summary>
        /// 
        /// </summary>
        public System.Windows.Media.Brush GradientLegend
        {
            get { return m_gradientLegend; }
            set { SetPropertyValue(ref m_gradientLegend, value, "GradientLegend"); }
        }
        private System.Windows.Media.Brush m_gradientLegend;
        #endregion // Legend

        #region Commands
        /// <summary>
        /// Command for refreshing the overlay.
        /// </summary>
        public RelayCommand RefreshCommand
        {
            get
            {
                if (m_relayCommand == null)
                {
                    m_relayCommand = new RelayCommand(param => RefreshOverlay(), param => ResolutionChanged);
                }
                return m_relayCommand;
            }
        }
        private RelayCommand m_relayCommand;
        #endregion // Commands

        #region Mouse Based Info
        /// <summary>
        /// The value that is currently under the mouse.
        /// </summary>
        public uint? ValueUnderMouse
        {
            get { return m_valueUnderMouse; }
            set { SetPropertyValue(ref m_valueUnderMouse, value, "ValueUnderMouse"); }
        }
        private uint? m_valueUnderMouse;

        /// <summary>
        /// Information based off of what the user clicked on.
        /// </summary>
        public IList<SelectionInfo> SelectionInfos
        {
            get { return m_selectionInfos; }
            set { SetPropertyValue(ref m_selectionInfos, value, "SelectionInfos"); }
        }
        private IList<SelectionInfo> m_selectionInfos = new List<SelectionInfo>();

        /// <summary>
        /// The item the user selected in the list.
        /// </summary>
        public SelectionInfo SelectedInfo
        {
            get { return m_selectedInfo; }
            set
            {
                if (SetPropertyValue(ref m_selectedInfo, value, "SelectedInfo"))
                {
                    UpdateHighlightedBox();
                }
            }
        }
        private SelectionInfo m_selectedInfo;
        #endregion // Mouse Based Info
        #endregion // Properties

        #region Member Data
        private IList<IMAPFile> ImapFiles { get; set; }
        private IList<ITYPFile> ItypFiles { get; set; }
        private IList<IMAPDependency2> ImapDependencies { get; set; }

        private IDictionary<String, ITYPFile> ItypLookup { get; set; }
        private IDictionary<String, IEnumerable<String>> DependencyLookup { get; set; }

        private IDictionary<BoundingBox2f, uint> CachedStats { get; set; }

        private ITask LoadContentTreeTask { get; set; }
        private ITask SyncFilesTask { get; set; }
        private ITask LoadDataTask { get; set; }
        private ITask GenerateGeometryTask { get; set; }
        private ITask GenerateGeometryUnderMouseTask { get; set; }
        private ITask UpdateOverlayTask { get; set; }

        private Viewport2DMultiLine HighlightedBox { get; set; }
        #endregion // Member Data

        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public ArchetypeITYPOverlay()
            : base(c_name, c_description)
        {
            LoadContentTreeTask = new ActionTask("Loading Content Tree", (ctx, progress) => LoadContentTreeAction(ctx, progress));
            SyncFilesTask = new ActionTask("Syncing Files", (ctx, progress) => SyncFilesAction(ctx, progress));
            LoadDataTask = new ActionTask("Loading Data", (ctx, progress) => LoadOverlayDataAction(ctx, progress));
            GenerateGeometryTask = new ActionTask("Generating Geometry", (ctx, progress) => GenerateGeometryAction(ctx, progress));
            GenerateGeometryUnderMouseTask = new ActionTask("Generating Geometry", (ctx, progress) => GenerateGeometryUnderMouseAction(ctx, progress));
            UpdateOverlayTask = new ActionTask("Updating Overlay", (ctx, progress) => UpdateOverlayGeometryAction(ctx, progress));
        }
        #endregion // Constructors
        
        #region Overrides
        /// <summary>
        /// Gets called when the user wants to view this overlay
        /// </summary>
        public override void Activated()
        {
            base.Activated();
            MapViewportProxy.Value.ViewportClicked += ViewportClicked;
            UpdateLegend();

            // Create a task for loading the data required for the overlay.
            CancellationTokenSource cts = new CancellationTokenSource();
            TaskContext context = new TaskContext(cts.Token);

            CompositeTask compositeTask = new CompositeTask("Updating Overlay");
            compositeTask.AddSubTask(LoadContentTreeTask);
            compositeTask.AddSubTask(SyncFilesTask);
            compositeTask.AddSubTask(LoadDataTask);
            compositeTask.AddSubTask(GenerateGeometryTask);
            compositeTask.AddSubTask(UpdateOverlayTask);

            TaskProgressService.Value.Add(compositeTask, context, TaskPriority.Foreground, cts);
        }

        /// <summary>
        /// Gets called when the user turns off this overlay
        /// </summary>
        public override void Deactivated()
        {
            MapViewportProxy.Value.ViewportClicked -= ViewportClicked;
            base.Deactivated();
        }

        /// <summary>
        /// Gets the control that should be shown in the overlay details panel
        /// </summary>
        public override Control GetOverlayControl()
        {
            return new ArchetypeITYPOverlayView();
        }

        /// <summary>
        /// Gets the control that shows information at the bottom right of the viewport.
        /// </summary>
        public override Control GetViewportControl()
        {
            return new ArchetypeITYPViewportOverlayView();
        }

        /// <summary>
        /// 
        /// </summary>
        public override void OnMouseMoved(System.Windows.Point viewPosition, System.Windows.Point worldPosition)
        {
            // Try and find the value under the mouse
            Vector2f point = new Vector2f((float)worldPosition.X, (float)worldPosition.Y);

            if (CachedStats != null)
            {
                lock (CachedStats)
                {
                    if (CachedStats.Any(item => item.Key.Contains(point)))
                    {
                        KeyValuePair<BoundingBox2f, uint> pairUnderMouse = CachedStats.First(item => item.Key.Contains(point));
                        ValueUnderMouse = pairUnderMouse.Value;
                    }
                    else
                    {
                        ValueUnderMouse = null;
                    }
                }
            }
            else
            {
                ValueUnderMouse = null;
            }
        }
        #endregion // Overrides

        #region Private Methods
        /// <summary>
        /// Loads the context tree.
        /// </summary>
        private void LoadContentTreeAction(ITaskContext context, IProgress<TaskProgress> progress)
        {
            // Get the processed zip file paths that we will need to process.
            CommandOptions options = new CommandOptions();
            IContentTree tree = Factory.CreateTree(Config.Value.Config.Project.DefaultBranch);

            IEnumerable<IProcess> metadataMergeProcesses =
                tree.Processes.Where(p => p.ProcessorClassName.Equals(Processor_MapDummyMetadataMerge, StringComparison.OrdinalIgnoreCase));

            ISet<IContentNode> outputNodes = new HashSet<IContentNode>();
            outputNodes.AddRange(metadataMergeProcesses.SelectMany(item => item.Outputs));

            context.Argument = outputNodes.OfType<IFilesystemNode>().Select(item => item.AbsolutePath);
        }

        /// <summary>
        /// Checks whether we need to sync any files.
        /// </summary>
        private void SyncFilesAction(ITaskContext context, IProgress<TaskProgress> progress)
        {
            IEnumerable<String> zipPaths = (IEnumerable<String>)context.Argument;

            // Make sure the user has them synced.
            int syncedFiles = 0;
            PerforceSyncService.Value.Show("You current have some processed metadata zip files that are out of date.\n" +
                                           "Would you like to grab latest now?\n\n" +
                                           "You can select which files to grab using the table below.\n",
                                           zipPaths, ref syncedFiles, false, true);
        }

        /// <summary>
        /// Rips through the processed zip files getting the information we need.
        /// </summary>
        private void LoadOverlayDataAction(ITaskContext context, IProgress<TaskProgress> progress)
        {
            IEnumerable<String> zipPaths = (IEnumerable<String>)context.Argument;
            String imapExtension = FileType.IMAP.GetExportExtension();
            String itypExtension = FileType.ITYP.GetExportExtension();

            ImapFiles = new List<IMAPFile>();
            ItypFiles = new List<ITYPFile>();
            ImapDependencies = new List<IMAPDependency2>();

            double progressIncrement = 1.0 / zipPaths.Count();

            zipPaths.AsParallel().ForAll(zipPath =>
                {
                    try
                    {
                        if (System.IO.File.Exists(zipPath))
                        {
                            using (ZipFile file = ZipFile.Read(zipPath))
                            {
                                // Extract information about all the imaps
                                List<IMAPFile> tempImapFiles = new List<IMAPFile>();
                                foreach (ZipEntry entry in file.Entries.Where(item => item.FileName.EndsWith(imapExtension)))
                                {
                                    using (MemoryStream zipStream = new MemoryStream())
                                    {
                                        entry.Extract(zipStream);
                                        zipStream.Position = 0;

                                        IMAPFile imapFile = IMAPFile.CreateFromStream(zipStream);
                                        if (imapFile != null)
                                        {
                                            tempImapFiles.Add(imapFile);
                                        }
                                    }
                                }
                                lock (ImapFiles)
                                {
                                    ImapFiles.AddRange(tempImapFiles);
                                }

                                // Extract information about all the ityps.
                                List<ITYPFile> tempItypFiles = new List<ITYPFile>();
                                foreach (ZipEntry entry in file.Entries.Where(item => item.FileName.EndsWith(itypExtension)))
                                {
                                    using (MemoryStream zipStream = new MemoryStream())
                                    {
                                        entry.Extract(zipStream);
                                        zipStream.Position = 0;

                                        ITYPFile itypFile = ITYPFile.CreateFromStream(zipStream);
                                        if (itypFile != null)
                                        {
                                            tempItypFiles.Add(itypFile);
                                        }
                                    }
                                }
                                lock (ItypFiles)
                                {
                                    ItypFiles.AddRange(tempItypFiles);
                                }

                                // Try and get the mapping file.
                                List<IMAPDependency2> tempDependencies = new List<IMAPDependency2>();
                                ZipEntry additionsEntry = file.Entries.FirstOrDefault(item => String.Compare(item.FileName, c_additionsFilename, true) == 0);
                                if (additionsEntry != null)
                                {
                                    using (MemoryStream zipStream = new MemoryStream())
                                    {
                                        additionsEntry.Extract(zipStream);
                                        zipStream.Position = 0;

                                        MapExportAdditions exportAddition = MapExportAdditions.CreateFromStream(zipStream);
                                        if (exportAddition != null)
                                        {
                                            tempDependencies.AddRange(exportAddition.IMAPDependencies);
                                        }
                                    }
                                }
                                lock (ImapDependencies)
                                {
                                    ImapDependencies.AddRange(tempDependencies);
                                }
                            }
                        }
                    }
                    catch (System.Exception ex)
                    {
                        Log.Log__Exception(ex, "Error parsing {0} zip file.", zipPath);
                    }

                    context.Token.ThrowIfCancellationRequested();
                    progress.Report(new TaskProgress(progressIncrement, true, ""));
                });

            ItypLookup = ItypFiles.ToDictionary(item => item.Name.ToLower());
            /*
            IDictionary<String, IEnumerable<String>> dependencyLookup =
                ImapDependencies.Where(item => item.IsInterior == false)
                                .ToDictionary(item => item.IMAPBasename.ToLower(), item => item.ITYPDependencyFiles);
            */
            DependencyLookup = new Dictionary<String, IEnumerable<String>>();
            foreach (IMAPDependency2 dep in ImapDependencies.Where(item => !item.IsInterior))
            {
                if (!DependencyLookup.ContainsKey(dep.IMAPBasename.ToLower()))
                {
                    DependencyLookup[dep.IMAPBasename.ToLower()] = dep.ITYPDependencyFiles;
                }
            }

            context.Token.ThrowIfCancellationRequested();
        }

        /// <summary>
        /// Action for parsing the IMAP/ITYP data generating the geometry to be displayed on the overlay.
        /// </summary>
        private void GenerateGeometryAction(ITaskContext context, IProgress<TaskProgress> progress)
        {
            // Gather the readings across the map.
            ILevel level = LevelBrowserProxy.Value.SelectedLevel;

            int startX = (int)level.ImageBounds.Min.X / Resolution;
            int endX = (int)level.ImageBounds.Max.X / Resolution;
            int startY = (int)level.ImageBounds.Min.Y / Resolution;
            int endY = (int)level.ImageBounds.Max.X / Resolution;

            CachedStats = new Dictionary<BoundingBox2f, uint>();

            int xSteps = endX - startX;
            double progressIncrement = 1.0 / xSteps;

            Enumerable.Range(startX, xSteps).AsParallel().ForAll(x =>
            {
                IList<KeyValuePair<BoundingBox2f, uint>> tempValues = new List<KeyValuePair<BoundingBox2f, uint>>();

                for (int y = startY; y < endY; ++y)
                {
                    BoundingBox2f bbox = new BoundingBox2f(
                        new Vector2f(x * Resolution, y * Resolution),
                        new Vector2f(x * Resolution + Resolution, y * Resolution + Resolution));
                    uint archetypes = GetArchetypeCountForLocation(bbox, ItypLookup, DependencyLookup);

                    tempValues.Add(new KeyValuePair<BoundingBox2f, uint>(bbox, archetypes));
                }

                lock (CachedStats)
                {
                    CachedStats.AddRange(tempValues);
                }

                context.Token.ThrowIfCancellationRequested();
                progress.Report(new TaskProgress(progressIncrement, true, ""));
            });

            if (CachedStats.Any())
            {
                MinValue = CachedStats.Min(item => item.Value);
                MaxValue = CachedStats.Max(item => item.Value);
                AverageValue = CachedStats.Average(item => item.Value);
            }
            else
            {
                MinValue = 0;
                MaxValue = 0;
                AverageValue = 0.0;
            }

            ResolutionChanged = false;
            SelectionInfos = new List<SelectionInfo>();
            SelectedInfo = null;
            UpdateHighlightedBox();

            context.Token.ThrowIfCancellationRequested();
        }

        /// <summary>
        /// Action for updating the geometry rendered on the overlay.
        /// </summary>
        private void UpdateOverlayGeometryAction(ITaskContext context, IProgress<TaskProgress> progress)
        {
            // Create the overlay.
            Viewport2DImageOverlay overlay = CreateOverlayImage();
            overlay.BitmapScalingMode = System.Windows.Media.BitmapScalingMode.NearestNeighbor;
            overlay.BeginUpdate();

            int total = CachedStats.Count;
            int current = 0;
            int reportAt = total / 100;

            foreach (KeyValuePair<BoundingBox2f, uint> pair in CachedStats)
            {
                Vector2f[] points = new Vector2f[4];
                points[0] = pair.Key.Min;
                points[1] = new Vector2f(pair.Key.Max.X, pair.Key.Min.Y);
                points[2] = pair.Key.Max;
                points[3] = new Vector2f(pair.Key.Min.X, pair.Key.Max.Y);

                overlay.RenderPolygon(points, CreateColorForValue(pair.Value));
                current++;

                if (context != null && current % reportAt == 0)
                {
                    context.Token.ThrowIfCancellationRequested();
                    progress.Report(new TaskProgress((double)current / total, null));
                }
            }

            overlay.UpdateImageOpacity((byte)((OverlayOpacity / 100.0f) * 255));
            overlay.EndUpdate();

            this.Geometry.BeginUpdate();
            this.Geometry.Clear();
            this.Geometry.Add(overlay);
            this.Geometry.EndUpdate();

            context.Token.ThrowIfCancellationRequested();
        }

        /// <summary>
        /// Refreshes the overlay.
        /// </summary>
        private void RefreshOverlay()
        {
            CancellationTokenSource cts = new CancellationTokenSource();
            TaskContext context = new TaskContext(cts.Token);

            CompositeTask compositeTask = new CompositeTask("Updating Overlay");
            compositeTask.AddSubTask(GenerateGeometryTask);
            compositeTask.AddSubTask(UpdateOverlayTask);
            TaskProgressService.Value.Add(compositeTask, context, TaskPriority.Foreground, cts);
        }

        /// <summary>
        /// 
        /// </summary>
        private void OnColoursChanged()
        {
            // HACK :/
            if (IsCurrentlyActive)
            {
                UpdateLegend();
                UpdateOverlayGeometryAction(null, null);
            }
        }

        /// <summary>
        /// Determines how many archetypes are present within a particular bounding box.
        /// </summary>
        private uint GetArchetypeCountForLocation(BoundingBox2f bbox, IDictionary<String, ITYPFile> itypLookup, IDictionary<String, IEnumerable<String>> dependencyLookup)
        {
            // Get the IMAP files that this bounding box intersects.
            IEnumerable<IMAPFile> relevantImaps = ImapFiles.Where(item => 
                {
                    BoundingBox2f twodBox = new BoundingBox2f(
                        new Vector2f(item.StreamingExtents.Min.X, item.StreamingExtents.Min.Y),
                        new Vector2f(item.StreamingExtents.Max.X, item.StreamingExtents.Max.Y));
                    return twodBox.Intersects(bbox);
                });

            // Get the set of ITYP files
            ISet<ITYPFile> ityps = new HashSet<ITYPFile>();
            foreach (IMAPFile imapFile in relevantImaps)
            {
                if (dependencyLookup.ContainsKey(imapFile.Name.ToLower()))
                {
                    foreach (String itypFilename in dependencyLookup[imapFile.Name.ToLower()])
                    {
                        if (itypLookup.ContainsKey(itypFilename.ToLower()))
                        {
                            ityps.Add(itypLookup[itypFilename.ToLower()]);
                        }
                    }
                }
            }

            // Sum up the archetype counts.
            return (uint)ityps.Sum(item => item.ArchetypeCount);
        }

        /// <summary>
        /// Updates the brush used for rendering the gradient legend.
        /// </summary>
        private void UpdateLegend()
        {
            // Update the gradient legend.
            System.Windows.Media.LinearGradientBrush brush = new System.Windows.Media.LinearGradientBrush();
            brush.StartPoint = new System.Windows.Point(0.0, 0.5);
            brush.EndPoint = new System.Windows.Point(1.0, 0.5);

            double hueStart = RSG.Base.Windows.Controls.ColorUtilities.ConvertRgbToHsv(LowColor.R, LowColor.G, LowColor.B).H / 360.0;
            double hueEnd = RSG.Base.Windows.Controls.ColorUtilities.ConvertRgbToHsv(HighColor.R, HighColor.G, HighColor.B).H / 360.0;
            double hueRange = (hueEnd - hueStart) / 0.6666;

            double size = 1.0 / hueRange;
            double start = -(hueStart / 0.6666) * size;
            double increment = size / 4;

            brush.GradientStops = new System.Windows.Media.GradientStopCollection();
            brush.GradientStops.Add(new System.Windows.Media.GradientStop(System.Windows.Media.Colors.Red, start + (increment * 0)));
            brush.GradientStops.Add(new System.Windows.Media.GradientStop(System.Windows.Media.Colors.Yellow, start + (increment * 1)));
            brush.GradientStops.Add(new System.Windows.Media.GradientStop(System.Windows.Media.Color.FromRgb(0, 255, 0), start + (increment * 2)));
            brush.GradientStops.Add(new System.Windows.Media.GradientStop(System.Windows.Media.Colors.Cyan, start + (increment * 3)));
            brush.GradientStops.Add(new System.Windows.Media.GradientStop(System.Windows.Media.Colors.Blue, start + (increment * 4)));
            GradientLegend = brush;
        }

        /// <summary>
        /// Converts a value to a colour based on the selected render settings.
        /// </summary>
        private Color CreateColorForValue(uint value)
        {
            // Use HSV method
            // 0 -> red
            // 0.3 -> green
            uint minValue = LowValue;
            uint maxValue = HighValue;

            if (value < minValue)
            {
                value = minValue;
            }
            // Clamp values above max to white.
            if (value > maxValue)
            {
                return Color.White;
            }

            // Convert it to a 0->1 scale
            value -= minValue;
            double percent = (double)value / (maxValue - minValue);

            double lowHue = RSG.Base.Windows.Controls.ColorUtilities.ConvertRgbToHsv(LowColor.R, LowColor.G, LowColor.B).H / 360.0;
            double highHue = RSG.Base.Windows.Controls.ColorUtilities.ConvertRgbToHsv(HighColor.R, HighColor.G, HighColor.B).H / 360.0;
            double hueDiff = highHue - lowHue;

            byte h = (byte)((lowHue + (percent * hueDiff)) * 255);
            byte s = (byte)(0.9 * 255);
            byte b = (byte)(0.9 * 255);

            return ColourSpaceConv.HSVtoColor(new HSV(h, s, b));
        }

        /// <summary>
        /// 
        /// </summary>
        private void UpdateHighlightedBox()
        {
            if (HighlightedBox != null)
            {
                this.Geometry.Remove(HighlightedBox);
            }

            if (SelectedInfo != null)
            {
                Vector2f[] points = new Vector2f[] {
                    new Vector2f(SelectedInfo.IMAP.StreamingExtents.Min.X, SelectedInfo.IMAP.StreamingExtents.Min.Y),
                    new Vector2f(SelectedInfo.IMAP.StreamingExtents.Max.X, SelectedInfo.IMAP.StreamingExtents.Min.Y),
                    new Vector2f(SelectedInfo.IMAP.StreamingExtents.Max.X, SelectedInfo.IMAP.StreamingExtents.Max.Y),
                    new Vector2f(SelectedInfo.IMAP.StreamingExtents.Min.X, SelectedInfo.IMAP.StreamingExtents.Max.Y),
                    new Vector2f(SelectedInfo.IMAP.StreamingExtents.Min.X, SelectedInfo.IMAP.StreamingExtents.Min.Y)
                };
                HighlightedBox = new Viewport2DMultiLine(etCoordSpace.World, "", points, true, System.Windows.Media.Colors.Black, 1);
                this.Geometry.Add(HighlightedBox);
            }
            else
            {
                HighlightedBox = null;
            }
        }

        /// <summary>
        /// Gets called when the user clicked somewhere on the viewport other than on geometry
        /// </summary>
        private void ViewportClicked(object sender, ViewportClickEventArgs e)
        {
            // Get the imap files that would be loaded at the point where the user clicked.
            Vector2f worldPosition = new Vector2f((float)e.WorldPosition.X, (float)e.WorldPosition.Y);
            IEnumerable<IMAPFile> relevantImaps = ImapFiles.Where(item =>
            {
                BoundingBox2f twodBox = new BoundingBox2f(
                    new Vector2f(item.StreamingExtents.Min.X, item.StreamingExtents.Min.Y),
                    new Vector2f(item.StreamingExtents.Max.X, item.StreamingExtents.Max.Y));
                return twodBox.Contains(worldPosition);
            });

            // If we are in mouse click mode, update the overlay
            if (MouseClickMode)
            {
                CancellationTokenSource cts = new CancellationTokenSource();
                TaskContext context = new TaskContext(cts.Token);
                context.Argument = relevantImaps;

                CompositeTask compositeTask = new CompositeTask("Updating Overlay");
                compositeTask.AddSubTask(GenerateGeometryUnderMouseTask);
                compositeTask.AddSubTask(UpdateOverlayTask);
                TaskProgressService.Value.Add(compositeTask, context, TaskPriority.Foreground, cts);
            }

            // Get the set of ITYP files
            IList<SelectionInfo> newSelection = new List<SelectionInfo>();
            foreach (IMAPFile imapFile in relevantImaps)
            {
                if (DependencyLookup.ContainsKey(imapFile.Name.ToLower()))
                {
                    foreach (String itypFilename in DependencyLookup[imapFile.Name.ToLower()])
                    {
                        if (ItypLookup.ContainsKey(itypFilename.ToLower()))
                        {
                            newSelection.Add(new SelectionInfo {
                                IMAP = imapFile,
                                ITYP = ItypLookup[itypFilename.ToLower()]
                            });
                        }
                    }
                }
            }
            SelectionInfos = newSelection;
        }

        /// <summary>
        /// Action for parsing the IMAP/ITYP data generating the geometry to be displayed on the overlay.
        /// </summary>
        private void GenerateGeometryUnderMouseAction(ITaskContext context, IProgress<TaskProgress> progress)
        {
            IEnumerable<IMAPFile> validImaps = (IEnumerable<IMAPFile>)context.Argument;

            // Gather the readings across the map.
            ILevel level = LevelBrowserProxy.Value.SelectedLevel;

            int startX = (int)level.ImageBounds.Min.X / Resolution;
            int endX = (int)level.ImageBounds.Max.X / Resolution;
            int startY = (int)level.ImageBounds.Min.Y / Resolution;
            int endY = (int)level.ImageBounds.Max.X / Resolution;

            CachedStats = new Dictionary<BoundingBox2f, uint>();

            int xSteps = endX - startX;
            double progressIncrement = 1.0 / xSteps;

            Enumerable.Range(startX, xSteps).AsParallel().ForAll(x =>
            {
                IList<KeyValuePair<BoundingBox2f, uint>> tempValues = new List<KeyValuePair<BoundingBox2f, uint>>();

                for (int y = startY; y < endY; ++y)
                {
                    BoundingBox2f bbox = new BoundingBox2f(
                        new Vector2f(x * Resolution, y * Resolution),
                        new Vector2f(x * Resolution + Resolution, y * Resolution + Resolution));
                    IEnumerable<IMAPFile> relevantImaps = ImapFiles.Where(item =>
                    {
                        BoundingBox2f twodBox = new BoundingBox2f(
                            new Vector2f(item.StreamingExtents.Min.X, item.StreamingExtents.Min.Y),
                            new Vector2f(item.StreamingExtents.Max.X, item.StreamingExtents.Max.Y));
                        return twodBox.Intersects(bbox);
                    });

                    // Get the set of ITYP files
                    ISet<ITYPFile> ityps = new HashSet<ITYPFile>();
                    foreach (IMAPFile imapFile in relevantImaps.Intersect(validImaps))
                    {
                        if (DependencyLookup.ContainsKey(imapFile.Name.ToLower()))
                        {
                            foreach (String itypFilename in DependencyLookup[imapFile.Name.ToLower()])
                            {
                                if (ItypLookup.ContainsKey(itypFilename.ToLower()))
                                {
                                    ityps.Add(ItypLookup[itypFilename.ToLower()]);
                                }
                            }
                        }
                    }

                    // Sum up the archetype counts.
                    uint archetypes = (uint)ityps.Sum(item => item.ArchetypeCount);
                    tempValues.Add(new KeyValuePair<BoundingBox2f, uint>(bbox, archetypes));
                }

                lock (CachedStats)
                {
                    CachedStats.AddRange(tempValues);
                }

                context.Token.ThrowIfCancellationRequested();
                progress.Report(new TaskProgress(progressIncrement, true, ""));
            });

            if (CachedStats.Any())
            {
                MinValue = CachedStats.Min(item => item.Value);
                MaxValue = CachedStats.Max(item => item.Value);
                AverageValue = CachedStats.Average(item => item.Value);
            }
            else
            {
                MinValue = 0;
                MaxValue = 0;
                AverageValue = 0.0;
            }

            ResolutionChanged = false;
            SelectionInfos = new List<SelectionInfo>();
            SelectedInfo = null;
            UpdateHighlightedBox();

            context.Token.ThrowIfCancellationRequested();
        }
        #endregion // Private Methods
    } // ArchetypeITYPOverlay
}
