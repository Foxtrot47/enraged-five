﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using RSG.Base.Collections;
using ragCore;
using ragWidgets;
using System.Diagnostics;
using RSG.Base.Editor.Command;
using RSG.Base.Windows.Helpers;

namespace WidgetEditor.ViewModel
{
    

    public class WidgetGroupViewModel : WidgetViewModel
    {
        #region Properties
        #endregion

        #region Variables

        private WidgetGroupBase m_Widget;
        #endregion

        public List<Widget> WidgetList { get { return m_Widget.WidgetList; } }

        public WidgetGroupViewModel( WidgetGroupBase model )
            :base(model)
        {
            m_Widget = model;
        }



        #region Commands

        private RelayCommand m_ExpandCommand;
        public RelayCommand ExpandCommand
        {
            get
            {
                if ( m_ExpandCommand == null )
                {
                    m_ExpandCommand = new RelayCommand( param => this.Expand( param ), param => this.CanExpand( param ) );
                }
                return m_ExpandCommand;
            }
        }

        private void Expand( object param )
        {
            IsExpanded = !IsExpanded;
        }

        private Boolean CanExpand( Object param )
        {
            return true;
        }
        #endregion
    }
}
