﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Workbench.AddIn.TXDParentiser.ViewModel
{
    public class RadioOptionViewModel : RSG.Base.Editor.ViewModelBase
    {        
        #region Fields

        private Object m_toolTip;
        private Object m_tag;
        private Boolean m_isChecked;
        private String m_displayName;

        #endregion // Fields

        #region Properties

        /// <summary>
        /// The main command that the view binds to
        /// </summary>
        public RadioCommand Command
        {
            get;
            private set;
        }

        /// <summary>
        /// The tooltip for the command, this can be changed
        /// at anytime using the set method
        /// </summary>
        public Object ToolTip
        {
            get { return m_toolTip; }
            set
            {
                this.SetPropertyValue(ref this.m_toolTip, value, "ToolTip");
            }
        }

        /// <summary>
        /// The tooltip for the command, this can be changed
        /// at anytime using the set method
        /// </summary>
        public Object Tag
        {
            get { return m_tag; }
            set
            {
                this.SetPropertyValue(ref this.m_tag, value, "Tag");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public String DisplayName
        {
            get { return m_displayName; }
            set
            {
                this.SetPropertyValue(ref this.m_displayName, value, "DisplayName");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Boolean IsChecked
        {
            get { return m_isChecked; }
            set
            {
                this.SetPropertyValue(ref this.m_isChecked, value, "IsChecked");
            }
        }

        #endregion // Properties
        
        #region Constructors

        /// <summary>
        /// Default Constructor
        /// </summary>
        public RadioOptionViewModel(String displayName, String toolTip, Object tag, RadioCommand command)
        {
            this.Command = command;
            this.ToolTip = toolTip;
            this.Tag = tag;
            this.DisplayName = displayName;
            this.IsChecked = false;
        }

        #endregion // Constructors
    } // RadioOptionViewModel
}
