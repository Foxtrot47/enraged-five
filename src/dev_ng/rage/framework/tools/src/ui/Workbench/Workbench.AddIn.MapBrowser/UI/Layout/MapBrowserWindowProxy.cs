﻿using System;
using System.Windows;
using System.ComponentModel.Composition;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn.Services;
using Workbench.AddIn.UI.Layout;
using RSG.Model.Map.ViewModel;
using Map.AddIn.Services;

namespace Workbench.AddIn.MapBrowser.UI.Layout
{
    [ExportExtension(Workbench.AddIn.ExtensionPoints.ToolWindowProxy, typeof(IToolWindowProxy))]
    public class MapBrowserWindowProxy : IToolWindowProxy
    {
        #region MEF Imports

        /// <summary>
        /// The configuration service that has in it the game view
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(IConfigurationService))]
        Lazy<IConfigurationService> ConfigService
        {
            get;
            set;
        }

        /// <summary>
        /// Tool window proxys that are used as the map viewport (can be more than one)
        /// </summary>
        [ImportManyExtension(Map.AddIn.Services.ExtensionPoints.MapToolProxy, typeof(IToolWindowProxy))]
        private IEnumerable<IToolWindowProxy> MapToolWindows { get; set; }

        /// <summary>
        /// Layout manager reference.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager, typeof(ILayoutManager))]
        private Lazy<ILayoutManager> LayoutManager { get; set; }

        [ImportExtension(Map.AddIn.Services.CompositionPoints.MapBrowser, typeof(IMapBrowser))]
        Lazy<IMapBrowser> ViewModel
        {
            get;
            set;
        }

        /// <summary>
        /// Map user window proxys that are used as users so that we can automatically setup the userdata
        /// </summary>
        [ImportManyExtension(Map.AddIn.Services.ExtensionPoints.MapUserProxy, typeof(IMapBrowserUserProxy))]
        private IEnumerable<IMapBrowserUserProxy> MapUserProxies { get; set; }

        /// <summary>
        /// Tool window proxys that are used as the map viewport (can be more than one)
        /// </summary>
        [ImportManyExtension(Map.AddIn.Services.ExtensionPoints.MapStatisticsProxy, typeof(IToolWindowProxy))]
        private IEnumerable<IToolWindowProxy> MapStatisticsWindows { get; set; }

        /// <summary>
        /// Tool window proxys that are used as the map viewport (can be more than one)
        /// </summary>
        [ImportExtension(Map.AddIn.Services.ExtensionPoints.PropViewer, typeof(IPropViewer))]
        private IPropViewer PropViewer { get; set; }

        #endregion // MEF Imports

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public MapBrowserWindowProxy()
        {
            this.Name = Resources.Strings.MapBrowser_Name;
            this.Header = Resources.Strings.MapBrowser_Title;
        }

        #endregion // Constructor

        #region Public Functions

        /// <summary>
        /// A abstract function that is overriden by the plugin to create the
        /// actual instance of the tool window and pass it back as a out paramter
        /// </summary>
        protected override Boolean CreateToolWindow(out IToolWindowBase toolWindow)
        {
            MapBrowserViewModel viewModel = ViewModel.Value as MapBrowserViewModel;
            viewModel.ConfigurationService = this.ConfigService.Value;
            viewModel.LayoutManager = this.LayoutManager.Value;
            viewModel.MapUserProxies = this.MapUserProxies.ToList();

            toolWindow = new MapBrowserView(MapToolWindows, MapStatisticsWindows, viewModel, this.PropViewer);
            return true;
        }

        #endregion // Public Functions
    }

}
