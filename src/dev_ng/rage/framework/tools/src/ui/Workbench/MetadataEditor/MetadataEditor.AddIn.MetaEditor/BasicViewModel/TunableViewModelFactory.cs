﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Metadata.Data;
using MetadataEditor.AddIn.MetaEditor.ViewModel;

namespace MetadataEditor.AddIn.MetaEditor.BasicViewModel
{
    /// <summary>
    /// Tunable ViewModel object factory class.
    /// </summary>
    public static class TunableViewModelFactory
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tunable"></param>
        /// <returns></returns>
        public static ITunableViewModel Create(ITunableViewModel parent, ITunable tunable, MetaFileViewModel root)
        {
            if (tunable is BitsetTunable)
            {
                return (new BitsetTunableViewModel(parent, tunable as BitsetTunable, root));
            }
            else if (tunable is ITunableStructure)
            {
                return (new StructureTunableViewModel(parent, tunable as ITunableStructure, root));
            }
            else if (tunable is ITunableArray)
            {
                return (new ArrayTunableViewModel(parent, tunable as ITunableArray, root));
            }
            else if (tunable is MapTunable)
            {
                return (new MapTunableViewModel(parent, tunable as MapTunable, root));
            }
            else if (tunable is PointerTunable)
            {
                if (tunable.Definition as RSG.Metadata.Parser.PointerMember != null)
                {
                    if ((tunable.Definition as RSG.Metadata.Parser.PointerMember).Policy == RSG.Metadata.Parser.PointerMember.PointerPolicy.Owner ||
                        (tunable.Definition as RSG.Metadata.Parser.PointerMember).Policy == RSG.Metadata.Parser.PointerMember.PointerPolicy.SimpleOwner)
                    {
                        return (new PointerTunableViewModel(parent, tunable as PointerTunable, root));
                    }
                    else
                    {
                        return (new TunableViewModel(parent, tunable, root));
                    }
                }
                else
                {
                    return (new TunableViewModel(parent, tunable, root));
                }
            }
            else if (tunable is XIncludeTunable)
            {
                return new XiTunableViewModel(parent, tunable as XIncludeTunable, root);
            }
            else
            {
                return (new TunableViewModel(parent, tunable, root));
            }
        }
    }
} // MetadataEditor.AddIn.View
