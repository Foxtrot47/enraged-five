﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Interop.Bugstar.Game;
using RSG.Base.Logging;

namespace Workbench.AddIn.Bugstar.Overlays
{
    /// <summary>
    /// Heat map showing the bug counts per container.
    /// </summary>
    [ExportExtension(ExtensionPoints.Overlay, typeof(Viewport.AddIn.IViewportOverlay))]
    public class BugsAllContainersOverlay : BugsHeatMapOverlayBase
    {
        #region Constants

        private static readonly string c_name = "Bugs All Containers";
        private static readonly string c_description = "Heat map showing the bug counts per container. Uses a logarithmic scale to determine colours. This can take a long time to generate the data.";

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        public BugsAllContainersOverlay()
            : base(c_name, c_description, true, false)
        {
            DrawSections = true;
        }

        #endregion

        /// <summary>
        /// Get the statistics for each section.
        /// </summary>
        /// <param name="section">Map section.</param>
        /// <param name="statistic">Statistic.</param>
        /// <returns>True if successful.</returns>
        protected override bool GetStatistic(RSG.Model.Common.Map.IMapSection section, out double statistic)
        {
            statistic = 0;

            MapGrid grid = MapGrids.FirstOrDefault(item => item.Name.ToLower() == section.Name.ToLower());

            if (grid != null)
            {
                try
                {
                    statistic = grid.GetBugs("Id,Summary,Description,X,Y,Z,Developer,Tester,State,Component,DueDate,Priority,Category").Count;
                }
                catch (System.Exception ex)
                {
                    Log.Log__Exception(ex, "Unexpected exception while attempting to retrieve the bug list for section '{0}'.", section.Name);
                    return false;
                }
            }

            return true;
        }
    }
}
