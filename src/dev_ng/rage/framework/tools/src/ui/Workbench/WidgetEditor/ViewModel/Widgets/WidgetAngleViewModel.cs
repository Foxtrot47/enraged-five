﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using RSG.Base.Collections;
using ragCore;
using ragWidgets;
using System.Diagnostics;
using RSG.Base.Editor.Command;

namespace WidgetEditor.ViewModel
{
    

    public class WidgetAngleViewModel : WidgetViewModel
    {
        #region Properties
        private ObservableCollection<float>m_Value;
        public ObservableCollection<float> Value
        {
            get { return m_Value; }
            set
            {
                m_Widget.ValueChanged -= model_ValueChanged;
                m_Widget.Value[0] = value[0]; // hmm, not good enough I think, todo fix
                m_Widget.Value[1] = value[1];
                m_Widget.ValueChanged += model_ValueChanged;
                SetPropertyValue( value, () => this.Value,
                                new PropertySetDelegate( delegate( Object newValue ) { m_Value = (ObservableCollection<float>)newValue; } ) );
            }
        }
        #endregion

        #region Variables

        private WidgetAngle m_Widget;
        #endregion



        public WidgetAngleViewModel( WidgetAngle model )
            :base(model)
        {
            m_Widget = model;
            m_Widget.ValueChanged += model_ValueChanged;
            m_Value = new ObservableCollection<float>();
            for ( int i = 0; i < 2; ++i )
            {
                m_Value.Add( m_Widget.Value[i] );
            }
        }

        void model_ValueChanged( object sender, EventArgs e )
        {
            for ( int i = 0; i < 2; ++i )
            {
                Value[i] = m_Widget.Value[i];
            }
        }

    }
}
