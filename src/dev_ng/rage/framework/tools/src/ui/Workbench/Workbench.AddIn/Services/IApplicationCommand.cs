﻿using System;

namespace Workbench.AddIn.Services
{

    /// <summary>
    /// 
    /// </summary>
    public interface IApplicationCommand : IExtension
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        void Execute(params Object[] args);
    }

} // Workbench.AddIn.Services namespace
