﻿using System;
using System.Diagnostics;
using System.Windows.Input;
using RSG.Base.Editor;

namespace ContentBrowser.AddIn
{
    /// <summary>
    ///  A command whose sole purpose is to relay its functionality 
    ///  to other objects by invoking delegates
    /// </summary>
    public class ContentBrowserCommand : ModelBase, IContentBrowserCommand
    {
        #region Members

        private readonly Action<Object> m_execute;
        private readonly Predicate<Object> m_canExecute;
        private String m_description = null;
        private Object m_image = null;

        #endregion // Members

        #region Properties

        /// <summary>
        /// The description for this command, can also
        /// be through of as a tooltip
        /// </summary>
        public String Description
        {
            get { return m_description; }
            set
            {
                if (value == m_description)
                    return;

                SetPropertyValue(value, () => this.Description,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_description = (String)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The image that will be displayed on the command
        /// </summary>
        public Object Image
        {
            get { return m_image; }
            set
            {
                if (value == m_image)
                    return;

                SetPropertyValue(value, () => this.Image,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_image = newValue;
                        }
                ));
            }
        }

        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Creates a new command that can always execute.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        public ContentBrowserCommand(Action<Object> execute, String description, Object image)
            : this(execute, null, description, image)
        {
        }

        /// <summary>
        /// Creates a new command.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        /// <param name="canExecute">The execution status logic.</param>
        public ContentBrowserCommand(Action<Object> execute, Predicate<Object> canExecute, String description, Object image)
        {
            if (execute == null)
                throw new ArgumentNullException("execute");

            m_execute = execute;
            m_canExecute = canExecute;
            this.Description = String.IsNullOrEmpty(description) ? null : description;
            this.Image = image;
        }

        #endregion // Constructors

        #region ICommand Functions

        /// <summary>
        /// The can execute changed handlers, this is to make
        /// sure that the can execute function is called
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        /// <summary>
        /// Gets called whenever the command is determining
        /// whether or not it can be executed
        /// </summary>
        public Boolean CanExecute(Object parameter)
        {
            return m_canExecute == null ? true : m_canExecute(parameter);
        }

        /// <summary>
        /// Gets called whenever the command has been executed
        /// </summary>
        public void Execute(Object parameter)
        {
            m_execute(parameter);
        }

        #endregion // ICommand Functions
    } // BrowserCommand
} // ContentBrowser.Model
