﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Report.AddIn;
using RSG.Model.Report;

namespace Workbench.AddIn.GameStatistics.Reports.Automated
{
    /// <summary>
    /// Category for reports based on the stats gathered from the automated tests that run on cruise.
    /// </summary>
    [ExportExtension(Report.AddIn.ExtensionPoints.ReportCategory, typeof(IReportCategory))]
    public class AutomatedTestsCategory : ReportCategory
    {
        #region Constants
        private const String c_name = "Automated Tests";
        private const String c_description = "Reports based on the stats gathered from the automated tests that run on cruise.";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Imported reports for this category.
        /// </summary>
        [ImportManyExtension(ExtensionPoints.AutomatedReport, typeof(IReport))]
        public override IEnumerable<IReportItem> Reports
        {
            get;
            protected set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public AutomatedTestsCategory()
            : base(c_name, c_description)
        {
        }
        #endregion // Constructor(s)
    } // AutomatedTestsCategory
}
