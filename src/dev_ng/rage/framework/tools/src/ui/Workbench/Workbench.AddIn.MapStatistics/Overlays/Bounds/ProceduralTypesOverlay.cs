﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using RSG.Base.Collections;
using RSG.Base.Editor;
using RSG.Base.Math;
using RSG.Bounds;

namespace Workbench.AddIn.MapStatistics.Overlays.Bounds
{
    /// <summary>
    /// Represents a procedural type that can be displayed on the overlay
    /// </summary>
    public class ProceduralType : ViewModelBase
    {
        #region Properties
        /// <summary>
        /// The colour of the section when it is rendered onto the map.
        /// </summary>
        public Color Colour
        {
            get { return m_colour; }
            set
            {
                if (m_colour != value)
                {
                    m_colour = value;
                    OnPropertyChanged("Colour");
                }
            }
        }
        private Color m_colour;

        /// <summary>
        /// The name of the section
        /// </summary>
        public string Name
        {
            get { return m_name; }
            set
            {
                if (m_name != value)
                {
                    m_name = value;
                    OnPropertyChanged("Name");
                }
            }
        }
        private string m_name;

        /// <summary>
        /// Whether we should render the bounds for this type on the overlay
        /// </summary>
        public bool Render
        {
            get { return m_render; }
            set
            {
                if (m_render != value)
                {
                    m_render = value;
                    OnPropertyChanged("Render");
                }
            }
        }
        private bool m_render;
        #endregion

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public ProceduralType(string name, Color colour, bool render)
        {
            Name = name;
            Colour = colour;
            Render = render;
        }
        #endregion
    } // ProceduralType

    /// <summary>
    /// Overlay for visualising the procedural types
    /// </summary>
    [ExportExtension(ExtensionPoints.BoundsStatisticsOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class ProceduralTypesOverlay : BoundsOverlayBase
    {
        #region Constants
        private const string c_name = "Procedural Type Bounds";
        private const string c_description = "Shows an overlay of procedural types bounds.";
        private static readonly Color c_defaultColour = Color.FromArgb(255, 0, 127, 0);
        #endregion // Constants

        #region Properties
        /// <summary>
        /// List of procedural types that a particular map section uses
        /// </summary>
        public ObservableCollection<ProceduralType> ProceduralTypes
        {
            get { return m_proceduralTypes; }
            set
            {
                SetPropertyValue(value, () => ProceduralTypes,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_proceduralTypes = (ObservableCollection<ProceduralType>)newValue;
                        }
                ));
            }
        }
        private ObservableCollection<ProceduralType> m_proceduralTypes;

        /// <summary>
        /// List of bound files that are loaded for the currently selected map section
        /// </summary>
        private List<BNDFile> BoundFiles
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default Constructor
        /// </summary>
        public ProceduralTypesOverlay()
            : base(c_name, c_description)
        {
            m_proceduralTypes = new ObservableCollection<ProceduralType>();
            BoundFiles = new List<BNDFile>();
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// Gets called when the user turns off this overlay
        /// </summary>
        public override void Deactivated()
        {
            BoundFiles.Clear();
            ProceduralTypes.Clear();
            base.Deactivated();
        }

        /// <summary>
        /// Gets the control that should be shown in the overlay details panel
        /// </summary>
        public override System.Windows.Controls.Control GetOverlayControl()
        {
            return new ProceduralTypesOverlayView();
        }
        /// <summary>
        /// Called when the selected map section changes
        /// </summary>
        protected override void OnSelectedMapSectionChanged()
        {
            // Clear out the existing data
            BoundFiles.Clear();
            ProceduralTypes.Clear();

            // Make sure a map section was selected
            if (SelectedMapSection != null)
            {
                // Load in the relevant bounds files
                BoundFiles.AddRange(LoadBoundsDataForSection(SelectedMapSection));

                // Extract the list of materials that have proc. types for this section
                foreach (BNDFile file in BoundFiles)
                {
                    ExtractMaterials(file.BoundObject);
                }
            }

            // Update the overlay based on the material's found
            RefreshOverlay();
        }

        /// <summary>
        /// Refreshes the overlay image data
        /// </summary>
        protected override void RefreshOverlay()
        {
            OverlayImage.ClearImage();
            OverlayImage.BeginUpdate();

            foreach (BNDFile file in BoundFiles)
            {
                RenderProceduralTypeBounds(file.BoundObject);
            }

            OverlayImage.EndUpdate();

            // Kinda Hacky: Forces a collection changed notification to be sent which causes the UI to update
            Geometry.BeginUpdate();
            Geometry.EndUpdate();
        }
        #endregion // Overrides

        #region Private Methods
        /// <summary>
        /// Recursively processes bound object's to extract all the materials with procedural types
        /// </summary>
        private void ExtractMaterials(BoundObject obj)
        {
            if (obj is Composite)
            {
                Composite composite = (Composite)obj;

                foreach (CompositeChild child in composite.Children)
                {
                    ExtractMaterials(child.BoundObject);
                }
            }
            else
            {
                BVH bvh = BoundObjectConverter.ToBVH(obj);
                ExtractMaterials(bvh.Materials);
            }
        }

        /// <summary>
        /// Extracts all materials that have a procedural type applied to them
        /// </summary>
        /// <param name="materials"></param>
        private void ExtractMaterials(string[] materials)
        {
            foreach (string mat in materials)
            {
                BVHMaterial material = new BVHMaterial(mat);
                if (material.ProceduralModifier != "0" &&
                    ProceduralTypes.FirstOrDefault(item => item.Name == material.ProceduralModifier) == null)
                {
                    ProceduralTypes.Add(new ProceduralType(material.ProceduralModifier, c_defaultColour, true));
                }
            }
        }

        /// <summary>
        /// Renders the bounds of all the procedural types that were found (and selected for render)
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="colour"></param>
        protected void RenderProceduralTypeBounds(BoundObject obj)
        {
            if (obj is Composite)
            {
                Composite composite = (Composite)obj;

                foreach (CompositeChild child in composite.Children)
                {
                    RenderProceduralTypeBounds(child.BoundObject);
                }
            }
            else
            {
                // Convert the bound object to a BVH and then process
                BVH bvh = BoundObjectConverter.ToBVH(obj);
                RenderProceduralTypeBVH(bvh);
            }
        }

        /// <summary>
        /// Renders the bounds of all the procedural types that were found (and selected for render)
        /// </summary>
        /// <param name="bvh"></param>
        /// <param name="colour"></param>
        private void RenderProceduralTypeBVH(BVH bvh)
        {
            // Helper map for quickly looking up procedural types by name
            Dictionary<string, ProceduralType> nameMap = new Dictionary<string, ProceduralType>();
            foreach (ProceduralType type in ProceduralTypes)
            {
                nameMap.Add(type.Name, type);
            }

            // Go over all the triangle primitives checking if we should render them and in what colour
            foreach (BVHPrimitive prim in bvh.Primitives.Where(item => item is BVHTriangle))
            {
                BVHTriangle tri = (BVHTriangle)prim;
                BVHMaterial mat = new BVHMaterial(bvh.Materials[tri.MaterialIndex]);

                if (nameMap.ContainsKey(mat.ProceduralModifier) && nameMap[mat.ProceduralModifier].Render)
                {
                    Vector2f[] outline = new Vector2f[3];
                    outline[0] = new Vector2f(bvh.Verts[tri.Index0].Position.X, bvh.Verts[tri.Index0].Position.Y);
                    outline[1] = new Vector2f(bvh.Verts[tri.Index1].Position.X, bvh.Verts[tri.Index1].Position.Y);
                    outline[2] = new Vector2f(bvh.Verts[tri.Index2].Position.X, bvh.Verts[tri.Index2].Position.Y);

                    System.Windows.Media.Color mediaColor = nameMap[mat.ProceduralModifier].Colour;
                    System.Drawing.Color drawingColor = System.Drawing.Color.FromArgb(mediaColor.A, mediaColor.R, mediaColor.G, mediaColor.B);

                    OverlayImage.RenderPolygon(outline, drawingColor);
                }
            }
        }
        #endregion // Private Methods
    } // ProceduralTypesOverlay
} // Workbench.AddIn.MapStatistics.Overlays.Bounds
