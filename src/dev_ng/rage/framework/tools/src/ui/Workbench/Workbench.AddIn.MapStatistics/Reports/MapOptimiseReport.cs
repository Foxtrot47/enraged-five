﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Report;
using Workbench.AddIn.Services;

namespace Workbench.AddIn.MapStatistics.Reports
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Report.AddIn.ExtensionPoints.MapStatsReport, typeof(IReport))]
    public class MapOptimiseReport : RSG.Model.Report.Reports.Map.MapOptimiseReport
    {
    } // MapOptimiseReport
} // Workbench.AddIn.MapStatistics.Reports
