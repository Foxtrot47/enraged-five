﻿using System;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Workbench.AddIn.Services;
using ContentBrowser.AddIn;
using RSG.Base.Editor;
using RSG.Base.ConfigParser;

namespace ContentBrowser.Model
{
    /// <summary>
    /// A wrapper around a list of all the browsers that are currently
    /// loaded in.
    /// </summary>
    public class BrowserCollection : ModelBase, IEnumerable<BrowserBase>
    {
        #region Members

        private Dictionary<String, BrowserBase> m_browsers;

        #endregion // Members

        #region Properties

        /// <summary>
        /// The observable collection of browsers
        /// </summary>
        public Dictionary<String, BrowserBase> Browsers
        {
            get { return m_browsers; }
            set 
            {
                if (m_browsers == value)
                    return;

                SetPropertyValue(value, () => Browsers,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_browsers = (Dictionary<String, BrowserBase>)newValue;
                        }
                ));
            }
        }
    
        /// <summary>
        /// Returns the first browser with the given name,
        /// or null if none exist
        /// </summary>
        public BrowserBase this[String name]
        {
            get
            {
                if (this.Browsers.ContainsKey(name))
                    return this.Browsers[name];

                return null;
            }
        }
        
        #endregion // Properties

        #region Constructor
        
        /// <summary>
        /// Creates a browser collection using a collection of browser and a level dictionary
        /// </summary>
        public BrowserCollection(IEnumerable<BrowserBase> browsers)
        {
            this.Browsers = new Dictionary<String, BrowserBase>();
            foreach (BrowserBase browser in browsers)
            {
                Debug.Assert(!this.Browsers.ContainsKey(browser.Name), String.Format(
                            "Duplicate browser name found, currently browser names have to be unique and {0} is not", browser.Name));
                if (this.Browsers.ContainsKey(browser.Name))
                    continue;

                this.Browsers.Add(browser.Name, browser);
            }

        }
        
        #endregion // Constructor

        #region IEnumerable<BrowserBase>

        /// <summary>
        /// 
        /// </summary>
        IEnumerator<BrowserBase> IEnumerable<BrowserBase>.GetEnumerator()
        {
            foreach (BrowserBase browser in this.Browsers.Values)
            {
                yield return browser;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerator)this);
        }

        #endregion // IEnumerable<Level>
    } // BrowserCollection
} // ContentBrowser.Model
