﻿using System;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using RSG.Model.Common;

namespace ContentBrowser.AddIn
{
    /// <summary>
    /// The main interface for anything that wants to be a browser and
    /// be added to the content browser source panel as a separate
    /// browser with a separate header to it.
    /// </summary>
    public interface IBrowser : INotifyPropertyChanged
    {
        #region Properties

        /// <summary>
        /// The name of the browser. This will be displayed in the source
        /// panel next to the expander.
        /// </summary>
        String Name { get; }
                
        /// <summary>
        /// The main property for a browser, this contains the
        /// top level of the asset hierarchy for this browser
        /// </summary>
        ObservableCollection<IAsset> AssetHierarchy { get; }

        /// <summary>
        /// Determines if this is the default browser for the
        /// content browser.
        /// </summary>
        Boolean DefaultBrowser { get; }

        /// <summary>
        /// The icon that will be displayed to represent this browser
        /// </summary>
        BitmapImage BrowserIcon { get; }
        
        /// <summary>
        /// Returns the control to use for the
        /// browser header in the view or null to
        /// use the default one.
        /// </summary>
        Control HeaderControl { get; }

        #endregion // Properties

        #region Methods
        
        /// <summary>
        /// Gets called once the browser collection system has fully loaded
        /// </summary>
        void Initialise();

        #endregion // Methods
    } // IBrowser
} // ContentBrowser.AddIn
