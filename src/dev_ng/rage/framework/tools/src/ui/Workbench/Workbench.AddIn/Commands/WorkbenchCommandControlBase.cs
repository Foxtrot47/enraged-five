﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn.UI.Controls;

namespace Workbench.AddIn.Commands
{
    /// <summary>
    /// Base class for all the command controls in the workbench
    /// </summary>
    public abstract class WorkbenchCommandControlBase : ControlBase, ICommandControl
    {
        #region Members

        private Action<Object> m_executeMethod;
        private Predicate<Object> m_canExecuteMethod;

        #endregion // Members

        #region Properties

        /// <summary>
        /// The action method that gets called when this command fires.
        /// </summary>
        public Action<Object> ExecuteMethod
        {
            get { return m_executeMethod; }
            set { m_executeMethod = value; }
        }

        /// <summary>
        /// The action method that gets called when determining
        /// if this command is valid and can be executed
        /// </summary>
        public Predicate<Object> CanExecuteMethod
        {
            get { return m_canExecuteMethod; }
            set { m_canExecuteMethod = value; }
        }

        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        public WorkbenchCommandControlBase()
        {
        }

        /// <summary>
        /// Creates a command control with just a execute method
        /// </summary>
        public WorkbenchCommandControlBase(Action<Object> executeMethod)
            : this(null, executeMethod)
        {
        }

        /// <summary>
        /// Creates a command control with both a can execute and a execute method
        /// </summary>
        public WorkbenchCommandControlBase(Predicate<Object> canExecuteMethod, Action<Object> executeMethod)
        {
            this.m_canExecuteMethod = canExecuteMethod;
            this.m_executeMethod = executeMethod;
        }

        #endregion // Constructors
        
        #region ICommand Interface

        /// <summary>
        /// 
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add { System.Windows.Input.CommandManager.RequerySuggested += value; }
            remove { System.Windows.Input.CommandManager.RequerySuggested -= value; }
        }

        /// <summary>
        /// Gets called when the manager is deciding if this
        /// command can be executed. This returns true be default
        /// </summary>
        public abstract Boolean CanExecute(Object parameter);

        /// <summary>
        /// Gets called when the command is executed. This just
        /// relays the execution to the delegate function
        /// </summary>
        public abstract void Execute(Object parameter);

        #endregion // ICommand Interface
    } // WorkbenchCommandControlBase
} // Workbench.AddIn.Commands
