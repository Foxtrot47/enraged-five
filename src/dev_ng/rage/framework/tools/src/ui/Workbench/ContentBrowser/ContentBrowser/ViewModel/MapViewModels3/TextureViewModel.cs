﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using System.IO;

namespace ContentBrowser.ViewModel.MapViewModels3
{
    /// <summary>
    /// 
    /// </summary>
    public class TextureViewModel : FileAssetViewModelBase
    {
        #region Members

        private ITexture m_model = null;

        #endregion // Members

        #region Properties

        /// <summary>
        /// A reference to the types model that this
        /// view model is wrapping
        /// </summary>
        public new ITexture Model
        {
            get { return m_model; }
            set
            {
                if (m_model == value)
                    return;

                SetPropertyValue(value, () => Model,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_model = (ITexture)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The name of the level, this needs to be unique
        /// throughout all the loaded levels
        /// </summary>
        public override String Name
        {
            get { return this.Model.Name; }
        }

        /// <summary>
        /// 
        /// </summary>
        public override string Filename
        {
            get
            {
                return (File.Exists(Model.Filename) ? Model.Filename : null);
            }
        }

        #endregion // Properties

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public TextureViewModel(ITexture texture)
            :base(texture)
        {
            this.Model = texture;
        }

        /// <summary>
        /// Default destructor
        /// </summary>
        ~TextureViewModel()
        {

        }

        #endregion // Constructor
    } // MapInstanceViewModel
} // ContentBrowser.ViewModel.MapViewModels
