﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;

using RSG.Base.Editor;
using RSG.Base.Windows.Helpers;
using RSG.RESTServices;
using RSG.Model.Map.SceneOverride;

using PerforceBrowser.AddIn;
using WidgetEditor.AddIn;
using Workbench.AddIn.Services;
using RSG.GameRESTServices.Game.Map;
using RSG.Base.Configuration;
using RSG.Pipeline.Core;
using RSG.Pipeline.Content;

namespace Workbench.AddIn.SceneOverrideEditor
{
    public class SceneOverrideEditorViewModel : ViewModelBase, INotifyPropertyChanged
    {
        public SceneOverrideEditorViewModel(
            IConfigurationService configurationService, 
            IMessageService messageService, 
            IPerforceService perforceService, 
            IProgressService progressService, 
            IProxyService proxyService)
        {
            configurationService_ = configurationService;
            messageService_ = messageService;
            perforceService_ = perforceService;
            progressService_ = progressService;
            proxyService_ = proxyService;

            isConnected_ = false;
            status_ = "Not connected to the game";
            LODDistanceEdits = new ObservableCollection<LODDistanceEdit>();
            EntitiesMarkedForDeletion = new ObservableCollection<EntityMarkedForDeletion>();
            AttributeEdits = new ObservableCollection<AttributeEdit>();
        }

        #region Events
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Properties
        public bool IsConnected
        {
            get
            {
                return isConnected_;
            }
            set
            {
                if (value == true)
                {
                    isConnected_ = Connect();
                }
                else
                {
                    Disconnect();
                    isConnected_ = false;
                }

                OnPropertyChanged("IsConnected");
            }
        }

        public string Status
        {
            get
            {
                return status_;
            }
            set
            {
                status_ = value;
                OnPropertyChanged("Status");
            }
        }

        public ObservableCollection<LODDistanceEdit> LODDistanceEdits
        {
            get;
            set;
        }

        public ObservableCollection<EntityMarkedForDeletion> EntitiesMarkedForDeletion
        {
            get;
            set;
        }

        public ObservableCollection<AttributeEdit> AttributeEdits
        {
            get;
            set;
        }

        public RelayCommand SubmitCommand
        {
            get
            {
                if (submitCommand_ == null)
                {
                    submitCommand_ = new RelayCommand(param => this.Submit(param), param => this.CanSubmit(param));
                }
                return submitCommand_;
            }
        }
        #endregion

        #region Methods
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private bool Connect()
        {
            bool isConnected = false;

            if (proxyService_.SelectedGameIP != null)
            {
                if (runtimeService_ == null || runtimeService_.GameIPAddress != proxyService_.SelectedGameIP.ToString())
                {
                    string liveOverridesPathname = System.IO.Path.Combine(configurationService_.GameConfig.ToolsTempDir, "scene_overrides.xml");
                    runtimeService_ = new SceneOverrideServiceClient(proxyService_.SelectedGameIP.ToString(), liveOverridesPathname);
                }

                if (runtimeService_.IsAvailable)
                {
                    // Do we have any cached changes that need to be posted to the game?
                    if (LODDistanceEdits.Count > 0 || EntitiesMarkedForDeletion.Count > 0 || AttributeEdits.Count > 0)
                    {
                        System.Windows.MessageBoxResult result = messageService_.Show(
                            "You have cached changes.  Connecting to the game may lose these changes if the console has been rebooted.  Do you want to continue?", 
                            System.Windows.MessageBoxButton.YesNo);

                        if (result == System.Windows.MessageBoxResult.No)
                        {
                            return false;
                        }

                        LODDistanceEdits.Clear();
                        runtimeService_.ClearCachedLODOverrides();
                        EntitiesMarkedForDeletion.Clear();
                        runtimeService_.ClearCachedDeleteItems();
                        AttributeEdits.Clear();
                        runtimeService_.ClearCachedAttributeOverrides();
                    }

                    runtimeService_.OnConnectionLost += new Action(OnConnectionLost);
                    runtimeService_.OnLODOverrideAdded += new Action<RuntimeLODOverride>(OnLODOverrideAdded);
                    runtimeService_.OnLODOverrideUpdated += new Action<RuntimeLODOverride>(OnLODOverrideUpdated);
                    runtimeService_.OnDeleteItemAdded += new Action<RuntimeEntityDeleteItem>(OnDeleteItemAdded);
                    runtimeService_.OnAttributeOverrideAdded += new Action<RuntimeAttributeOverride>(OnAttributeOverrideAdded);
                    runtimeService_.OnAttributeOverrideUpdated += new Action<RuntimeAttributeOverride>(OnAttributeOverrideUpdated);
                    runtimeService_.Connect();

                    isConnected = true;
                    Status = String.Format("Connected to game at {0}", proxyService_.SelectedGameIP.ToString());
                }
                else
                {
                    string message = String.Format(
                        "Unable to establish a connection to Scene REST service at '{0}'.  " + 
                        "Have the you created the scene widgets in Rag?  " + 
                        "The game doesn't initialise the Scene REST service till these widgets are created.", 
                        proxyService_.SelectedGameIP.ToString());
                    messageService_.Show(message);
                }
            }
            else
            {
                messageService_.Show("Please select a valid game session to connect to.");
            }

            return isConnected;
        }

        private void Disconnect()
        {
            runtimeService_.OnConnectionLost -= new Action(OnConnectionLost);
            runtimeService_.OnLODOverrideAdded -= new Action<RuntimeLODOverride>(OnLODOverrideAdded);
            runtimeService_.OnLODOverrideUpdated -= new Action<RuntimeLODOverride>(OnLODOverrideUpdated);
            runtimeService_.OnDeleteItemAdded -= new Action<RuntimeEntityDeleteItem>(OnDeleteItemAdded);
            runtimeService_.OnAttributeOverrideAdded -= new Action<RuntimeAttributeOverride>(OnAttributeOverrideAdded);
            runtimeService_.OnAttributeOverrideUpdated -= new Action<RuntimeAttributeOverride>(OnAttributeOverrideUpdated);
            runtimeService_.Disconnect();

            Status = "Not connected to the game";
        }

        private void OnConnectionLost()
        {
            System.Windows.Application.Current.Dispatcher.Invoke(
                new Action
                (
                    delegate()
                    {
                        IsConnected = false;
                        messageService_.Show("The Scene Override Editor lost its connection to the game.");
                        Status = "The connection to the game was lost.";
                    }
                ),
                System.Windows.Threading.DispatcherPriority.Send);
        }

        private void OnLODOverrideAdded(RuntimeLODOverride obj)
        {
            System.Windows.Application.Current.Dispatcher.Invoke(
                new Action
                (
                    delegate()
                    {
                        LODDistanceEdits.Add(new LODDistanceEdit(obj.Hash, obj.Guid, obj.PosX, obj.PosY, obj.PosZ, obj.IMAPName, obj.ModelName, obj.Distance, obj.ChildDistance));
                    }
                ),
                System.Windows.Threading.DispatcherPriority.Send);
        }

        private void OnLODOverrideUpdated(RuntimeLODOverride obj)
        {
            System.Windows.Application.Current.Dispatcher.Invoke(
                new Action
                (
                    delegate()
                    {
                        for (int index = 0; index < LODDistanceEdits.Count; ++index)
                        {
                            if (obj.IMAPName == LODDistanceEdits[index].IMAPName &&
                               obj.Guid == LODDistanceEdits[index].Guid &&
                               obj.Hash == LODDistanceEdits[index].Hash)
                            {
                                LODDistanceEdits[index] = new LODDistanceEdit(obj.Hash, obj.Guid, obj.PosX, obj.PosY, obj.PosZ, obj.IMAPName, obj.ModelName, obj.Distance, obj.ChildDistance);
                                return;
                            }
                        }
                    }
                ),
                System.Windows.Threading.DispatcherPriority.Send);
        }

        private void OnDeleteItemAdded(RuntimeEntityDeleteItem obj)
        {
            System.Windows.Application.Current.Dispatcher.Invoke(
                new Action
                (
                    delegate()
                    {
                        EntitiesMarkedForDeletion.Add(new EntityMarkedForDeletion(obj.Hash, obj.Guid, obj.PosX, obj.PosY, obj.PosZ, obj.IMAPName, obj.ModelName));
                    }
                ),
                System.Windows.Threading.DispatcherPriority.Send);
        }

        private void OnAttributeOverrideAdded(RuntimeAttributeOverride obj)
        {
            System.Windows.Application.Current.Dispatcher.Invoke(
                new Action
                (
                    delegate()
                    {
                        AttributeEdits.Add(
                            new AttributeEdit(obj.Hash, obj.Guid, obj.PosX, obj.PosY, obj.PosZ, obj.IMAPName, obj.ModelName,
                                              obj.DontCastShadows, obj.DontRenderInShadows, 
                                              obj.DontRenderInReflections, obj.OnlyRenderInReflections,
                                              obj.DontRenderInWaterReflections, obj.OnlyRenderInWaterReflections,
                                              obj.DontRenderInMirrorReflections, obj.OnlyRenderInMirrorReflections,
                                              obj.StreamingPriorityLow, obj.Priority));
                    }
                ),
                System.Windows.Threading.DispatcherPriority.Send);
        }

        private void OnAttributeOverrideUpdated(RuntimeAttributeOverride obj)
        {
            System.Windows.Application.Current.Dispatcher.Invoke(
                new Action
                (
                    delegate()
                    {
                        for (int index = 0; index < AttributeEdits.Count; ++index)
                        {
                            if (obj.IMAPName == AttributeEdits[index].IMAPName &&
                                obj.Guid == AttributeEdits[index].Guid &&
                                obj.Hash == AttributeEdits[index].Hash)
                            {
                                AttributeEdits[index] = new AttributeEdit(
                                    obj.Hash, obj.Guid, obj.PosX, obj.PosY, obj.PosZ, obj.IMAPName, obj.ModelName,
                                    obj.DontCastShadows, obj.DontRenderInShadows,
                                    obj.DontRenderInReflections, obj.OnlyRenderInReflections,
                                    obj.DontRenderInWaterReflections, obj.OnlyRenderInWaterReflections,
                                    obj.DontRenderInMirrorReflections, obj.OnlyRenderInMirrorReflections,
                                    obj.StreamingPriorityLow, obj.Priority);
                                return;
                            }
                        }
                    }
                ),
                System.Windows.Threading.DispatcherPriority.Send);
        }

        private void Submit(Object parameter)
        {
            if (LODDistanceEdits.Count == 0 && EntitiesMarkedForDeletion.Count == 0 && AttributeEdits.Count == 0)
            {
                messageService_.Show("There are no overrides to submit");
                return;
            }

            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += SubmitAsync;
            progressService_.Show(worker, "Submitting Scene override data", "Submitting Scene override data");

            messageService_.Show("Scene overrides were successfully submitted.");
        }

        private void SubmitAsync(Object sender, DoWorkEventArgs e)
        {
            try
            {

                BackgroundWorker worker = (BackgroundWorker)sender;

                if (runtimeService_ == null)
                {
                    messageService_.Show("Can't submit unless connected to the game");
                    return;
                }

                // go via the service and manage P4 calls here.
                string baseDataDirectory = configurationService_.GameConfig.AssetsDir + @"\maps\scene_overrides\";

                // Sync latest data
                progressService_.Status = "Syncing Scene override data";
                perforceService_.PerforceConnection.Run("sync", baseDataDirectory + "...");
                worker.ReportProgress(16);

                // Check for locked files
                progressService_.Status = "Checking Perforce status of Scene override data";
                P4API.P4RecordSet fstatResults = perforceService_.PerforceConnection.Run("fstat", baseDataDirectory + "...");
                worker.ReportProgress(33);

                if (fstatResults.Records.Length > 0)
                {
                    foreach (P4API.P4Record fstatResult in fstatResults)
                    {
                        if (fstatResult.Fields.ContainsKey("action") && fstatResult["action"] == "edit")
                        {
                            string errorMessage = String.Format("Unable to submit data as the file '{0}' is checked out by '{1}'", fstatResult["depotFile"], fstatResult["actionOwner"]);
                            messageService_.Show(errorMessage);
                            return;
                        }
                    }
                }

                progressService_.Status = "Creating changelist for new Scene override data";
                P4API.P4PendingChangelist changelist = perforceService_.PerforceConnection.CreatePendingChangelist("Auto-generated Scene override changelist");
                worker.ReportProgress(50);

                // Here we add or edit depending on the fstat results (fstatResults.Records.Length)
                progressService_.Status = "Adding Scene override data to changelist";
                if (fstatResults.Records.Length > 0)
                {
                    perforceService_.PerforceConnection.Run("edit", "-c", changelist.Number.ToString(), baseDataDirectory + "...");
                }
                else
                {
                    foreach (string pathname in System.IO.Directory.EnumerateFiles(baseDataDirectory))
                    {
                        perforceService_.PerforceConnection.Run("add", "-c", changelist.Number.ToString(), pathname);
                    }
                }
                worker.ReportProgress(66);

                progressService_.Status = "Merging Scene override data";

                IConfig config = this.configurationService_.Config;
                IBranch branch = this.configurationService_.Config.Project.DefaultBranch;
                IContentTree tree = Factory.CreateTree(branch);
                ContentTreeHelper treeHelper = new ContentTreeHelper(tree);
                ISceneOverrideManager manager = SceneOverrideManagerFactory.CreateXmlSceneOverrideManager(
                    branch, treeHelper, System.IO.Path.Combine(branch.Assets, "maps", "scene_overrides"), true);

                RuntimeLODOverride[] lodOverridesToSubmit = runtimeService_.GetCachedLODOverrides();
                RuntimeEntityDeleteItem[] deletesToSubmit = runtimeService_.GetCachedDeleteItems();
                RuntimeAttributeOverride[] attributeOverridesToSubmit = runtimeService_.GetCachedAttributeOverrides();
                manager.Submit(lodOverridesToSubmit, deletesToSubmit, attributeOverridesToSubmit);
                worker.ReportProgress(83);

                progressService_.Status = "Submitting Scene override data changelist";
                changelist.Submit();
                worker.ReportProgress(100);
            }
            catch (Exception ex)
            {
                string errorMessage = String.Format("An unexpected error occured while trying to submit scene override data: {0}", ex.ToString());
                messageService_.Show(errorMessage);
            }
        }

        private bool CanSubmit(Object parameter)
        {
            return true;
        }
        #endregion

        #region Data
        // Services
        private readonly IConfigurationService configurationService_;
        private readonly IMessageService messageService_;
        private readonly IPerforceService perforceService_;
        private readonly IProgressService progressService_;
        private readonly IProxyService proxyService_;

        // View model data
        private bool isConnected_;
        private string status_;
        private RelayCommand submitCommand_;
        private SceneOverrideServiceClient runtimeService_;
        #endregion
    }
}
