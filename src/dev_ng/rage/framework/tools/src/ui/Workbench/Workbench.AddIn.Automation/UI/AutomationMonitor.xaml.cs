﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using RSG.Pipeline.Automation.Common.Jobs;
using Workbench.AddIn.Services;
using Workbench.AddIn.Services.File;
using Workbench.AddIn.UI.Layout;
using PerforceBrowser.AddIn;
using System.ComponentModel;
using System.Threading;
using WidgetEditor.AddIn;
using System.Windows.Threading;
using System.Windows.Media;
using RSG.Base.Windows.Helpers;

namespace Workbench.AddIn.Automation.UI
{
    /// <summary>
    /// Map Network Export Monitor Code-behind.
    /// </summary>
    internal partial class AutomationMonitor :
        ToolWindowBase<AutomationMonitorViewModel>
    {
        #region Constants

        private static readonly int REFRESH_TIME_MINS = 5; // Five minutes refresh time

        public static readonly Guid GUID = new Guid("F6A7AD7E-202E-4631-9F1A-FEA31C224B70");
        #endregion // Constants

        #region Private member fields
        /// <summary>
        /// Timer for refreshing the list.
        /// </summary>
        private readonly DispatcherTimer m_refreshTimer;

        private RelayCommand downloadCommand;

        private RelayCommand openUlogCommand;
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public AutomationMonitor(ICoreServiceProvider coreServices,
                IOpenService ulogOpenService,
                IAutomationMonitorSettings settings)
            : base(Workbench.AddIn.Automation.Resources.Strings.AutomationMonitor_Name,
            new AutomationMonitorViewModel(coreServices, ulogOpenService, settings))
        {
            InitializeComponent();
            this.ID = GUID;

            this.m_refreshTimer = new DispatcherTimer(DispatcherPriority.ContextIdle, Dispatcher);
            this.m_refreshTimer.Interval = new TimeSpan(0, 0, REFRESH_TIME_MINS, 0, 0);
            this.m_refreshTimer.Tick += HandleRefreshTick;
            this.OnlyMyCompleted.IsChecked = false;
            this.OnlyMyCompleted.Checked += OnlyMyCompleted_Checked;
            this.OnlyMyCompleted.Unchecked += OnlyMyCompleted_Unchecked;
            Refresh();
        }
        #endregion // Constructor(s)

        #region Properties
        public RelayCommand DownloadCommand
        {
            get
            {
                if (this.downloadCommand == null)
                {
                    this.downloadCommand = new RelayCommand(param => Download(), param => CanDownload());
                }

                return this.downloadCommand;
            }
        }

        public RelayCommand OpenUlogCommand
        {
            get
            {
                if (this.openUlogCommand == null)
                {
                    this.openUlogCommand = new RelayCommand(param => OpenUlog(), param => CanOpenUlog());
                }

                return this.openUlogCommand;
            }
        }
        #endregion Properties

        #region Event Handlers
        private void OnlyMyCompleted_Unchecked(object sender, RoutedEventArgs e)
        {
            AutomationMonitorViewModel vm = this.ViewModel as AutomationMonitorViewModel;
            if (vm == null)
            {
                return;
            }

            foreach (AutomationTabViewModelBase tab in vm.Tabs)
            {
                tab.FilterUsername(vm.FilterUsername);
            }
        }

        private void OnlyMyCompleted_Checked(object sender, RoutedEventArgs e)
        {
            AutomationMonitorViewModel vm = this.ViewModel as AutomationMonitorViewModel;
            if (vm == null)
            {
                return;
            }

            foreach (AutomationTabViewModelBase tab in vm.Tabs)
            {
                tab.FilterUsername(true);
            }
        }

        public bool CanDownload()
        {
            AutomationMonitorViewModel vm = this.ViewModel as AutomationMonitorViewModel;
            if (vm == null)
            {
                return false;
            }

            // Get the data grid currently shown to the user.
            DataGrid grid = this.GetVisualDescendents<DataGrid>(this.RootGrid).FirstOrDefault();
            if (grid == null)
            {
                return false;
            }

            IEnumerable<AutomationJobViewModel> selectedJobs = from s in grid.SelectedItems.OfType<AutomationJobViewModel>()
                                                               where s.Job.State == JobState.Completed
                                                               select s as AutomationJobViewModel;

            return selectedJobs.Count() == grid.SelectedItems.Count;
        }

        public void Download()
        {
            AutomationMonitorViewModel vm = this.ViewModel as AutomationMonitorViewModel;
            if (vm == null)
            {
                return;
            }

            // Get the data grid currently shown to the user.
            DataGrid grid = this.GetVisualDescendents<DataGrid>(this.RootGrid).FirstOrDefault();
            if (grid == null)
            {
                return;
            }

            IEnumerable<AutomationJobViewModel> selectedJobs = from s in grid.SelectedItems.OfType<AutomationJobViewModel>()
                                                               where s.Job.State == JobState.Completed
                                                               select s as AutomationJobViewModel;

            if (selectedJobs.Count() == grid.SelectedItems.Count)
            {
                vm.DownloadExportAndBuildData(selectedJobs);
            }
        }

        public bool CanOpenUlog()
        {
            AutomationMonitorViewModel vm = this.ViewModel as AutomationMonitorViewModel;
            if (vm == null)
            {
                return false;
            }

            // Get the data grid currently shown to the user.
            DataGrid grid = this.GetVisualDescendents<DataGrid>(this.RootGrid).FirstOrDefault();
            if (grid == null)
            {
                return false;
            }

            IEnumerable<AutomationJobViewModel> selectedJobs = from s in grid.SelectedItems.OfType<AutomationJobViewModel>()
                                                               where s.Job.State == JobState.Completed || s.Job.State == JobState.Errors
                                                               select s as AutomationJobViewModel;

            return selectedJobs.Count() == grid.SelectedItems.Count;
        }

        public void OpenUlog()
        {
            AutomationMonitorViewModel vm = this.ViewModel as AutomationMonitorViewModel;
            if (vm == null)
            {
                return;
            }

            // Get the data grid currently shown to the user.
            DataGrid grid = this.GetVisualDescendents<DataGrid>(this.RootGrid).FirstOrDefault();
            if (grid == null)
            {
                return;
            }

            IEnumerable<AutomationJobViewModel> selectedJobs = from s in grid.SelectedItems.OfType<AutomationJobViewModel>()
                                                               where s.Job.State == JobState.Completed || s.Job.State == JobState.Errors
                                                               select s as AutomationJobViewModel;

            if (selectedJobs.Count() == grid.SelectedItems.Count)
            {
                vm.DownloadUniversalLog(selectedJobs);
            }
        }

        /// <summary>
        /// Refresh button callback.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RefreshButton_Click(Object sender, RoutedEventArgs e)
        {
            Refresh();
        }
        
        /// <summary>
        /// Creates an Enumerable that can be used to get all or loop through all instances of
        /// a specific type in the visual tree below this dependency object.
        /// </summary>
        /// <typeparam name="T">
        /// The type of visual that you want to find in the visual tree.
        /// </typeparam>
        /// <param name="d">
        /// The dependency object to use as the starting root visual for the search.
        /// </param>
        /// <returns>
        /// Returns an Enumerable that can be used to get all or loop through all instances of
        /// a specific type in the visual tree below this dependency object.
        /// </returns>
        private IEnumerable<T> GetVisualDescendents<T>(DependencyObject d)
            where T : DependencyObject
        {
            if (d == null)
            {
                yield break;
            }

            int childCount = VisualTreeHelper.GetChildrenCount(d);
            for (int i = 0; i < childCount; i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(d, i);
                if (child is T)
                {
                    yield return (T)child;
                }

                foreach (T match in GetVisualDescendents<T>(child))
                {
                    yield return match;
                }
            }

            yield break;
        }

        /// <summary>
        /// High Priority callback.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HighPriority_Click(Object sender, RoutedEventArgs e)
        {

        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            if (!e.Cancel)
            {
                m_refreshTimer.Stop();
            }
        }

        protected override void OnContentLoaded()
        {
            base.OnContentLoaded();
            m_refreshTimer.Start();
        }

        /// <summary>
        /// Tick called by the refresh timer for refreshing the view model.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HandleRefreshTick(object sender, EventArgs e)
        {
            if (this.IsKeyboardFocused || this.IsKeyboardFocusWithin)
            {
                return;
            }

            Refresh();
        }

        #endregion // Event Handlers

        /// <summary>
        /// 
        /// </summary>
        private void Refresh()
        {
            AutomationMonitorViewModel vm = (AutomationMonitorViewModel)this.ViewModel;
            vm.Refresh();
        }

        /// <summary>
        /// Context menu opening event doesn't fire. This will make sure that the user can't choose an invalid option.
        /// See http://stackoverflow.com/questions/10622511/contextmenuopening-event-not-firing-in-wpf for details. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PopupMenu_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            ContextMenu menu = sender as ContextMenu;
            foreach (MenuItem item in menu.Items)
            {
                var be = item.GetBindingExpression(IsEnabledProperty);
                if (be != null)
                {
                    be.UpdateTarget();
                    item.InvalidateVisual();
                }
            }
        }
    }

} // Workbench.AddIn._3dsmax.Export namespace
