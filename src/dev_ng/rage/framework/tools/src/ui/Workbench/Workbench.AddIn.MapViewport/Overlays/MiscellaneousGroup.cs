﻿using System;
using System.ComponentModel.Composition;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Windows.Controls.WpfViewport2D;

namespace Workbench.AddIn.MapViewport.Overlays
{
    /// <summary>
    /// The Miscellaneous overlay group. This is located in the map viewport project as it should be seen as a standard overlay
    /// that always shows.
    /// </summary>
    [ExportExtension(Viewport.AddIn.ExtensionPoints.OverlayGroup, typeof(Viewport.AddIn.IViewportOverlayGroup))]
    public class MiscellaneousGroup : Viewport.AddIn.ViewportOverlayGroup, IPartImportsSatisfiedNotification
    {
        #region Constants

        internal static readonly String GroupName = "Miscellaneous";

        #endregion // Constants

        #region MEF Imports

        /// <summary>
        /// 
        /// </summary>
        [ImportManyExtension(Viewport.AddIn.ExtensionPoints.MiscellaneousOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
        IEnumerable<Viewport.AddIn.IViewportOverlay> ImportedOverlays
        {
            get;
            set;
        }

        #endregion // MEF Imports

        #region Constructor

        public MiscellaneousGroup()
            : base(MiscellaneousGroup.GroupName)
        {
        }

        #endregion // Constructor

        #region IPartImportsSatisfiedNotification

        public void OnImportsSatisfied()
        {
            foreach (Viewport.AddIn.IViewportOverlay overlay in this.ImportedOverlays)
            {
                this.Overlays.Add(overlay);
            }   
        }

        #endregion // IPartImportsSatisfiedNotification
    } // MiscellaneousGroup
} // Workbench.AddIn.MapViewport.Overlays
