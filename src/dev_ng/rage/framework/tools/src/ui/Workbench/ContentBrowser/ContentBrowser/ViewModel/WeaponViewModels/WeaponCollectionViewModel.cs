﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;

namespace ContentBrowser.ViewModel.WeaponViewModels
{
    /// <summary>
    /// 
    /// </summary>
    public class WeaponCollectionViewModel : ObservableContainerViewModelBase<IWeapon>
    {
        #region Properties
        /// <summary>
        /// The number of viewmodels to create before displaying them on the
        /// screen. Less than or equal to 0 implies no batching
        /// </summary>
        protected override uint AssetCreationBatchSize
        {
            get
            {
                return 5;
            }
        }

        /// <summary>
        /// The number of viewmodels to create before displaying them on the
        /// screen. Less than or equal to 0 implies no batching
        /// </summary>
        protected override uint GridCreationBatchSize
        {
            get
            {
                return 5;
            }
        }
        #endregion // Properties

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public WeaponCollectionViewModel(IWeaponCollection weapons)
            : base(weapons, weapons.Weapons)
        {
        }
        #endregion // Constructor(s)
    } // WeaponCollectionViewModel
}
