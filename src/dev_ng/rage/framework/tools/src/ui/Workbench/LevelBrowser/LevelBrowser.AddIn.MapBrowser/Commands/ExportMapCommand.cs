﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using Workbench.AddIn;
using Workbench.AddIn.Commands;
using Workbench.AddIn.Services;
using RSG.Base.Logging;
using RSG.Model.Common.Map;
using RSG.Model.Map3;
using System.IO;
using RSG.Interop.Autodesk3dsmax;
using RSG.Base.Win32;
using RSG.Base.Logging.Universal;
using Workbench.AddIn.Services.File;
using RSG.Model.Common;

namespace LevelBrowser.AddIn.MapBrowser.Commands
{

    /// <summary>
    /// Map export context-menu command.
    /// </summary>
    [ExportExtension(Extensions.MapSectionCommands, typeof(IWorkbenchCommand))]
    public class ExportMapCommand : WorkbenchMenuItemBase
    {
        #region Constants
        public static readonly String GUID = "F2176617-013D-4135-90C9-51D91AB83106";
        private static readonly String MAX_INI_FILE = @"X:\MapBatchExport.txt";
        private const String MXS_BATCH_SCRIPT = "pipeline/export/maps/mapBatchExport.ms";
        private const MaxVersion MAX_VERSION = MaxVersion.Autodesk3dsmax2012;
        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// Workbench messaging service import.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.MessageService, typeof(IMessageService))]
        private IMessageService MessageService { get; set; }

        /// <summary>
        /// The perforce sync service
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.PerforceSyncService, typeof(IPerforceSyncService))]
        private IPerforceSyncService PerforceSyncService { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(IConfigurationService))]
        private IConfigurationService Config { get; set; }

        /// <summary>
        /// MEF import for perforce service.
        /// </summary>
        [ImportExtension(PerforceBrowser.AddIn.CompositionPoints.PerforceService, typeof(PerforceBrowser.AddIn.IPerforceService))]
        public PerforceBrowser.AddIn.IPerforceService PerforceService { get; set; }

        /// <summary>
        /// MEF import for workbench configuration service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.WorkbenchOpenService, typeof(IWorkbenchOpenService))]
        private IWorkbenchOpenService WorkbenchOpenService { get; set; }

        /// <summary>
        /// MEF imports for Workbench Open services.
        /// </summary>
        [ImportManyExtension(Workbench.AddIn.ExtensionPoints.OpenService, typeof(IOpenService))]
        private IEnumerable<IOpenService> OpenServices { get; set; }
        #endregion // MEF Imports

        #region Constructor
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ExportMapCommand()
        {
            this.Header = "_Export Locally...";
            this.IsDefault = false;
            this.ID = new Guid(GUID);
            this.RelativeID = new Guid(SyncArtAndExportMapCommand.GUID);
            this.Direction = Direction.After;
        }
        #endregion // Constructor

        #region ICommand Implementation
        /// <summary>
        /// Gets called when deciding whether this menu items command can be 
        /// executed or not.
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return (Autodesk3dsmax.IsVersionInstalled(MAX_VERSION, true));
        }

        /// <summary>
        /// Gets called when executing the command for this menu item.
        /// </summary>
        public override void Execute(Object parameter)
        {
            if (m_worker != null && m_worker.IsRunning)
            {
                m_worker.Cancel();
                m_worker = null;
            }

            // Get the list of map sections that we will be processing
            IEnumerable<MapSection> sections;
            if (parameter is IList)
            {
                sections = (parameter as IList).Cast<MapSection>();
            }
            else
            {
                sections = new MapSection[] { parameter as MapSection };
            }

            // Make sure the required data is loaded for the map sections
            foreach (IMapSection section in sections)
            {
                section.RequestStatistics(new StreamableStat[] { StreamableMapSectionStat.ExportZipFilepath }, false);
            }

            // Attempt to sync and checkout the files in question
            if (this.SyncAndCheckout(sections))
            {
                this.ExportMaps(sections);
            }
        }
        #endregion // ICommand Implementation

        #region Virtual Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sections"></param>
        /// <returns></returns>
        protected virtual IEnumerable<String> GetFilesToSync(IEnumerable<IMapSection> sections)
        {
            foreach (IMapSection section in sections)
            {
                yield return Path.GetFullPath(section.ExportZipFilepath);
                yield return Path.GetFullPath(Path.ChangeExtension(section.ExportZipFilepath, ".xml"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sections"></param>
        /// <returns></returns>
        protected virtual IEnumerable<string> GetFilesToCheckout(IEnumerable<IMapSection> sections)
        {
            foreach (IMapSection section in sections)
            {
                yield return Path.GetFullPath(section.ExportZipFilepath);
                yield return Path.GetFullPath(Path.ChangeExtension(section.ExportZipFilepath, ".xml"));
            }
        }
        #endregion // Virtual Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="maps"></param>
        /// <returns></returns>
        private bool SyncAndCheckout(IEnumerable<MapSection> sections)
        {
            // Check if the user is missing some files locally
            int syncedFiles = 0;
            PerforceSyncResult result =
                PerforceSyncService.Show("You current have a number of export files that are out of date.\n" +
                                         "Would you like to grab latest now?",
                                         GetFilesToSync(sections).ToArray(), ref syncedFiles, true, true);

            if (result == PerforceSyncResult.SyncedAll || result == PerforceSyncResult.Ok)
            {
                // Attempt to check the files out
                String currDir = System.IO.Directory.GetCurrentDirectory();
                IList<string> checkoutFailed = new List<string>();

                foreach (String filePath in GetFilesToCheckout(sections))
                {
                    try
                    {
                        P4API.P4RecordSet results = PerforceService.PerforceConnection.Run("edit", filePath);

                        // If we don't get a result, check whether the file is already checked out by the local user
                        if (results.Records.Length != 1)
                        {
                            results = PerforceService.PerforceConnection.Run("fstat", filePath);
                            if (results.Records.Length == 1 &&
                                results.Records[0].Fields.ContainsKey("action") &&
                                results.Records[0].Fields.ContainsKey("actionOwner") &&
                                results.Records[0].Fields["action"] == "edit" &&
                                results.Records[0].Fields["actionOwner"] == Environment.UserName)
                            {
                            }
                            else
                            {
                                checkoutFailed.Add(filePath);
                            }
                        }
                    }
                    catch (P4API.Exceptions.P4APIExceptions)
                    {
                        checkoutFailed.Add(filePath);
                    }
                }
                
                System.IO.Directory.SetCurrentDirectory(currDir);

                if (checkoutFailed.Count > 0)
                {
                    if (MessageBoxResult.No == MessageService.Show("Unable to check out the following files:\n\n" +
                            String.Join("\n", checkoutFailed.ToArray()) +
                            "\n\nThis may be because they are checked out by someone else.  Do you want to continue with the export?",
                            System.Windows.MessageBoxButton.YesNo, System.Windows.MessageBoxImage.Error))
                    {
                        return false;
                    }
                }

                return true;
            }
            else
            {
                // Inform the user that the sync failed
                MessageService.Show("Unable to perform export as the syncing of files failed.",
                    MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        private void ExportMaps(IEnumerable<MapSection> maps)
        {
            if (null == maps || 0 == maps.Count())
            {
                MessageService.Show("No Map Section selected.  Aborting.",
                    MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (!Autodesk3dsmax.IsVersionInstalled(MAX_VERSION, true))
            {
                MessageService.Show("Autodesk 3dsmax is not installed.  Aborting.",
                MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

#warning DHM FIX ME: no longer giving the user the choice.  We need to drive MAX_VERSION from project config data
            string maxExecutable = Autodesk3dsmax.GetExecutablePath(MAX_VERSION, true);
#if false
            if (versions.Count > 1)
            {
                // Show dialog so user can choose the max version they want to use.
                MaxVersionPickerViewModel vm = new MaxVersionPickerViewModel(versions);
                MaxVersionPicker picker = new MaxVersionPicker();
                picker.DataContext = vm;
                picker.Owner = Application.Current.MainWindow;
                if (!(bool)picker.ShowDialog())
                    return;

                maxExecutable = versions[vm.SelectedVersion] + "\\3dsmax.exe";
            }
            else if (versions.Count == 1)
            {
                foreach (var version in versions.Values)
                {
                    maxExecutable = version + "\\3dsmax.exe";
                }
            }
            else
            {
                MessageService.Show("Autodesk 3dsmax is not installed.  Aborting.");
                return;
            }
#endif

            StringBuilder arguments = new StringBuilder();
            arguments.AppendFormat("-mxs \"global RsMapBatchExport_newExport = True; fileIn \\\"{0}\\\"; RsMapBatchExportStart #(",
                MXS_BATCH_SCRIPT);
            arguments.Append(String.Join(",", maps.Select(map => String.Format("\\\"{0}\\\"", map.Name))));
            arguments.Append(") CloseMaxOnFinish:false\"");

            if (m_worker==null)
            {
                m_worker = new ExportMapBackgroundWorker(maxExecutable, arguments.ToString(), MAX_INI_FILE);
                m_worker.TaskComplete += new EventHandler(Worker_TaskComplete);
            }

            if (!m_worker.IsRunning)
            {
                m_worker.Start();
            }
        }

        /// <summary>
        /// When the external task is completed, the generated batch export text file is read to determine what items
        /// were successfully exported, and which were not.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        void Worker_TaskComplete(object sender, EventArgs e)
        {
            m_worker.TaskComplete -= Worker_TaskComplete;

            if (!m_worker.Success)
            {
                String message = "Failed to invoke Autodesk 3dsmax for map export.";
                MessageService.Show(message, MessageBoxButton.OK, MessageBoxImage.Error);

                Log.Log__Error(message);
                Log.Log__Error("Command: {0}.", m_worker.Filename);
                Log.Log__Error("Arguments: {0}.", m_worker.Arguments);
            }
            else
            {
                MapExportWrapper wrapper = new MapExportWrapper(IniFile.Open(MAX_INI_FILE));
                Log.Log__Message("Container batch exports triggered.");
            }

            m_worker = null;

            MergeLogs();
        }

        /// <summary>
        /// Merge the log files into one combined log and show it.
        /// </summary>
        private void MergeLogs()
        {
            string pathToMapExport = Path.Combine(Config.GameConfig.ToolsCacheDir, "Workbench", "MapExport");
            string pathToCombinedLog = Path.Combine(pathToMapExport, "combined.ulog");
            string[] logFiles = Directory.GetFiles(pathToMapExport, "*.ulog");

            if (logFiles.Length == 0)
            {
                MessageService.Show("Failure during export. Please view the errors in the Workbench Universal log.");
                return;
            }

            if (!UniversalLogFile.MergeLogs(pathToCombinedLog, logFiles))
            {
                MessageService.Show(String.Format("Failure combining logs in {0} to {1}.", pathToMapExport, pathToCombinedLog));
            }
            else
            {
                IOpenService ulogOpenService = OpenServices.Where(os => os.Filter.Contains("*.ulog")).FirstOrDefault();
                if (ulogOpenService != null)
                {
                    WorkbenchOpenService.OpenFileWithService(ulogOpenService, pathToCombinedLog);
                }
            }
        }

        #endregion // Private Methods

        #region Private member fields

        private ExportMapBackgroundWorker m_worker;
        
        #endregion
    } // ExportMapSectionCommand

} // LevelBrowser.AddIn.MapBrowser.Commands
