﻿using System;
using RSG.Base.Editor.Command;
using Workbench.AddIn.Services;

namespace Workbench.AddIn.UI.Controls
{

    /// <summary>
    /// 
    /// </summary>
    public abstract class ControlBase : 
        ExtensionBase, 
        IControl
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// 
        /// </summary>
        public String ToolTip
        {
            get { return (m_sToolTip); }
            set
            {
                SetPropertyValue(value, () => this.ToolTip,
                    new PropertySetDelegate(delegate(Object newValue) { m_sToolTip = (String)newValue; }));
            }
        }
        private String m_sToolTip = null;

        /// <summary>
        /// 
        /// </summary>
        public bool Visible
        {
            get { return m_bVisible; }
            set
            {
                SetPropertyValue(value, () => this.Visible,
                    new PropertySetDelegate(delegate(Object newValue) { m_bVisible = (bool)newValue; }));
            }
        }
        private bool m_bVisible = true;

        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ControlBase()
        {
        }
        #endregion // Constructor(s)
    }

} // Workbench.AddIn.UI.Controls namespace
