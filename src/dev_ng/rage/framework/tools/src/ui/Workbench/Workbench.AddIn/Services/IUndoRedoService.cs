﻿using System;
using System.Windows;
using System.Collections.Generic;
using RSG.Base.Editor;
using Workbench.AddIn.UI.Layout;

namespace Workbench.AddIn.Services
{
    /// <summary>
    /// Undo/redo workbench management service.
    /// </summary>
    /// This interface provides plugins with methods to register and unregister
    /// Model and ViewModel objects with the Undo/Redo management service.
    /// 
    /// Once registered the workbench provides user-hooks into the Model/ViewModel
    /// system through the 'Edit' -> 'Undo'/'Redo' menuitems.  Toolbar buttons
    /// may also be provided.
    public interface IUndoRedoService : IWeakEventListener
    {
        #region Controller Methods

        /// <summary>
        /// Register a Model with the service.
        /// </summary>
        /// <param name="model"></param>
        void RegisterContent(IContentBase content, List<IModel> models);
        
        /// <summary>
        /// 
        /// </summary>
        void RegisterModels(IContentBase content, List<IModel> models);

        /// <summary>
        /// Unregister a Model with the service.
        /// </summary>
        /// <param name="model"></param>
        void UnregisterContent(IContentBase content);

        /// <summary>
        /// Unregister a Model with the service.
        /// </summary>
        /// <param name="model"></param>
        void UnregisterModels(IContentBase content, List<IModel> models);

        /// <summary>
        /// 
        /// </summary>
        Boolean ContainsContent(IContentBase content);

        /// <summary>
        /// 
        /// </summary>
        int UndoCountForContent(IContentBase content);

        /// <summary>
        /// 
        /// </summary>
        int RedoCountForContent(IContentBase content);

        /// <summary>
        /// 
        /// </summary>
        void Undo(IContentBase content);

        /// <summary>
        /// 
        /// </summary>
        void Redo(IContentBase content);

        /// <summary>
        /// 
        /// </summary>
        void OnDocumentSaved(IDocumentBase document);
        
        #endregion // Controller Methods
    } // IUndoRedoService
} // Workbench.AddIn.Services namespace
