﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media.Imaging;
using Workbench.AddIn.UI.Controls;

namespace Workbench.AddIn.UI.Menu
{

    /// <summary>
    /// 
    /// </summary>
    public interface IMenuItem : ICommandControl
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        String Header { get; }
    
        /// <summary>
        /// 
        /// </summary>
        IEnumerable<IMenuItem> Items { get; }

        /// <summary>
        /// 
        /// </summary>
        Object Image { get; }
        
        /// <summary>
        /// 
        /// </summary>
        bool IsCheckable { get; }
        
        /// <summary>
        /// 
        /// </summary>
        bool IsChecked { get; set; }

        /// <summary>
        /// 
        /// </summary>
        bool IsSeparator { get; }
        
        /// <summary>
        /// 
        /// </summary>
        bool IsSubmenuOpen { get; set; }

        /// <summary>
        /// 
        /// </summary>
        Object Context { get; set; } // only used for context menus
        #endregion // Properties
    }

} // Workbench.AddIn.UI.Menu namespace
