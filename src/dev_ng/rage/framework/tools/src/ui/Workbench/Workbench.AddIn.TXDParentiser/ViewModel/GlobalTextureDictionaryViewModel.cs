﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using RSG.Base.Collections;
using RSG.Base.Editor.Command;
using RSG.Base.Windows.DragDrop;
using RSG.Model.GlobalTXD;
using RSG.Base.Editor;
using System.Text.RegularExpressions;
using RSG.Model.Asset;

namespace Workbench.AddIn.TXDParentiser.ViewModel
{
    public class GlobalTextureDictionaryViewModel : Util.GlobalTXDViewModelBase,
        IDictionaryContainerViewModel, ITextureContainerViewModel, IDictionaryChildViewModel, IDropTarget
    {
        #region Fields

        String m_prettyCurrentSize;
        private GlobalTextureDictionary m_model;
        private IDictionaryContainerViewModel m_parent;
        private GlobalRootViewModel m_root;
        private ObservableCollection<GlobalTextureDictionaryViewModel> m_globalTextureDictionaries;
        private ObservableCollection<SourceTextureDictionaryViewModel> m_sourceTextureDictionaries;
        private ObservableCollection<ITextureChildViewModel> m_textures;
        private int m_level;
        private bool m_isFocused = false;

        #endregion // Fields

        #region Properties

        /// <summary>
        /// If this is true it means that source textures can be added to
        /// this container as children
        /// </summary>
        public Boolean CanAcceptTextures
        {
            get { return this.Model.CanAcceptTextures; }
        }

        /// <summary>
        /// If this is true it means that source texture dictionaries can be added to
        /// this container as children
        /// </summary>
        public Boolean CanAcceptSourceDictionaries
        {
            get { return this.Model.CanAcceptSourceDictionaries; }
        }

        /// <summary>
        /// If this is true it means that global texture dictionaries can be added to
        /// this container as children
        /// </summary>
        public Boolean CanAcceptGlobalDictionaries
        {
            get { return this.Model.CanAcceptGlobalDictionaries; }
        }

        /// <summary>
        /// The name of the texture dictionary, this name will be exported and used at runtime,
        /// which means that this needs to be unique.
        /// </summary>
        public String Name
        {
            get { return this.Model.Name; }
            set { this.Model.Name = value; }
        }

        /// <summary>
        /// The current size of the container based on the sum of the texture
        /// sizes inside it
        /// </summary>
        public int CurrentSize
        {
            get { return this.Model.CurrentSize; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String PrettyCurrentSize
        {
            get { return m_prettyCurrentSize; }
            set
            {
                this.SetPropertyValue(
                    ref this.m_prettyCurrentSize, value, "PrettyCurrentSize");
            }
        }

        /// <summary>
        /// The model reference that this view model represents.
        /// </summary>
        public GlobalTextureDictionary Model
        {
            get { return m_model; }
            set { m_model = value; }
        }

        /// <summary>
        /// The parent global dictionary if it has one
        /// </summary>
        public new IDictionaryContainerViewModel Parent
        {
            get { return m_parent; }
            set
            {
                this.SetPropertyValue(ref this.m_parent, value, "Parent");
            }
        }

        /// <summary>
        /// The global root object that this dictionary belongs to
        /// </summary>
        public GlobalRootViewModel Root
        {
            get { return m_root; }
            set
            {
                this.SetPropertyValue(ref this.m_root, value, "Root");
            }
        }

        /// <summary>
        /// A list of all the global texture dictionary that this container currently has.
        /// </summary>
        public ObservableCollection<GlobalTextureDictionaryViewModel> GlobalTextureDictionaries
        {
            get { return m_globalTextureDictionaries; }
            private set
            {
                this.SetPropertyValue(
                    ref this.m_globalTextureDictionaries, value, "GlobalTextureDictionaries");
            }
        }

        /// <summary>
        /// A dictionary of uniquely named source texture dictionaries that are children of this global root
        /// The dictionaries are indexed by names and these names need to be unique.
        /// </summary>
        public ObservableCollection<SourceTextureDictionaryViewModel> SourceTextureDictionaries
        {
            get { return m_sourceTextureDictionaries; }
            private set
            {
                this.SetPropertyValue(
                    ref this.m_sourceTextureDictionaries, value, "SourceTextureDictionaries");
            }
        }

        /// <summary>
        /// A dictionary of uniquely named source texture dictionaries that are children of this global root
        /// The dictionaries are indexed by names and these names need to be unique.
        /// </summary>
        public ObservableCollection<ITextureChildViewModel> Textures
        {
            get { return m_textures; }
            private set
            {
                this.SetPropertyValue(ref this.m_textures, value, "Textures");
            }
        }

        /// <summary>
        /// A dictionary of uniquely named source texture dictionaries that are children of this global root
        /// The dictionaries are indexed by names and these names need to be unique.
        /// </summary>
        IEnumerable<ITextureChildViewModel> ITextureContainerViewModel.Textures
        {
            get { return m_textures; }
        }

        /// <summary>
        /// Represents the level that this texture dictionary
        /// </summary>
        public int Level
        {
            get 
            {
                if (this.Parent != null)
                    return this.Parent.Level + 1;
                return 0;
            }
            set
            {
                this.SetPropertyValue(ref this.m_level, value, "Level");
            }
        }

        /// <summary>
        /// Respresents where the UIElement bound to this object currently has focus
        /// </summary>
        public Boolean IsFocused
        {
            get { return m_isFocused; }
            set
            {
                this.SetPropertyValue(ref this.m_isFocused, value, "IsFocused");
            }
        }

        public bool IsDropTarget
        {
            get { return true; }
        }
        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        public GlobalTextureDictionaryViewModel(GlobalTextureDictionary model, GlobalRootViewModel root)
        {
            PropertyChangedEventManager.AddListener(model, this, "");
            CollectionChangedEventManager.AddListener(model.SourceTextureDictionaries, this);
            CollectionChangedEventManager.AddListener(model.Textures, this);

            this.Model = model;
            this.Parent = root;
            this.Root = root;
            this.Level = 0;
            this.GlobalTextureDictionaries = new ObservableCollection<GlobalTextureDictionaryViewModel>();
            this.SourceTextureDictionaries = new ObservableCollection<SourceTextureDictionaryViewModel>();
            this.Textures = new ObservableCollection<ITextureChildViewModel>();
            this.PrettyCurrentSize = GetPrettySize(this.CurrentSize);

            foreach (GlobalTextureDictionary dictionary in model.GlobalTextureDictionaries.Values)
            {
                GlobalTextureDictionaryViewModel newDictionary = new GlobalTextureDictionaryViewModel(dictionary, this);
                this.GlobalTextureDictionaries.Add(newDictionary);
            }
            foreach (SourceTextureDictionary dictionary in model.SourceTextureDictionaries.Values)
            {
                SourceTextureDictionaryViewModel newDictionary = new SourceTextureDictionaryViewModel(dictionary, this);
                this.SourceTextureDictionaries.Add(newDictionary);
            }
            foreach (GlobalTexture texture in model.Textures.Values)
            {
                GlobalTextureViewModel newTexture = new GlobalTextureViewModel(texture, this);
                this.Textures.Add(newTexture);
            }
            OnPropertyChanged("GlobalTextureDictionaries");
            OnPropertyChanged("SourceTextureDictionaries");
            OnPropertyChanged("Textures");
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public GlobalTextureDictionaryViewModel(GlobalTextureDictionary model, GlobalTextureDictionaryViewModel parent)
        {
            PropertyChangedEventManager.AddListener(model, this, "");
            CollectionChangedEventManager.AddListener(model.SourceTextureDictionaries, this);
            CollectionChangedEventManager.AddListener(model.Textures, this);

            this.Model = model;
            this.Parent = parent;
            this.Root = parent.Root;
            this.Level = 0;
            this.GlobalTextureDictionaries = new ObservableCollection<GlobalTextureDictionaryViewModel>();
            this.SourceTextureDictionaries = new ObservableCollection<SourceTextureDictionaryViewModel>();
            this.Textures = new ObservableCollection<ITextureChildViewModel>();
            this.PrettyCurrentSize = GetPrettySize(this.CurrentSize);

            if (this.Model != null)
            {
                if (this.Model.Parent == null)
                {
                    this.Level = 0;
                }
                else
                {
                    this.Level = 0;
                    IDictionaryContainerViewModel parent1 = parent as IDictionaryContainerViewModel;
                    while (!(parent1 is GlobalRootViewModel))
                    {
                        parent1 = parent1.Parent;
                        this.Level++;
                    }
                }
            }

            foreach (GlobalTextureDictionary dictionary in model.GlobalTextureDictionaries.Values)
            {
                GlobalTextureDictionaryViewModel newDictionary = new GlobalTextureDictionaryViewModel(dictionary, this);
                this.GlobalTextureDictionaries.Add(newDictionary);
            }
            foreach (SourceTextureDictionary dictionary in model.SourceTextureDictionaries.Values)
            {
                SourceTextureDictionaryViewModel newDictionary = new SourceTextureDictionaryViewModel(dictionary, this);
                this.SourceTextureDictionaries.Add(newDictionary);
            }
            foreach (GlobalTexture texture in model.Textures.Values)
            {
                GlobalTextureViewModel newTexture = new GlobalTextureViewModel(texture, this);
                this.Textures.Add(newTexture);
            }
            OnPropertyChanged("GlobalTextureDictionaries");
            OnPropertyChanged("SourceTextureDictionaries");
            OnPropertyChanged("Textures");
        }

        #endregion // Constructors

        #region IGlobalDictionaryContainerViewModel Implementation

        /// <summary>
        /// Returns true iff this root has a texture dictionary in it with
        /// the given name. Is recursive is true it will recursive through all the texture
        /// dictionaries as well
        /// </summary>
        public Boolean ContainsTextureDictionary(String name, Boolean recursive)
        {
            return this.Model.ContainsTextureDictionary(name, recursive);
        }

        /// <summary>
        /// Adds the given dictionary into the children texture dictionaries iff the name
        /// is unique.
        /// </summary>
        public Boolean AddExistingGlobalDictionary(GlobalTextureDictionaryViewModel dictionary)
        {
            if (this.Model.AddExistingGlobalDictionary(dictionary.Model))
            {
                this.GlobalTextureDictionaries.Add(new GlobalTextureDictionaryViewModel(dictionary.Model, this));
                this.IsExpanded = true;
                this.IsFocused = true;
                OnPropertyChanged("GlobalTextureDictionaries");
                return true;
            }
            return false;
        }

        /// <summary>
        /// Creates a new dictionary with the given name and adds it to this dictionary 
        /// iff the name is unique
        /// </summary>
        public Boolean AddNewGlobalDictionary(String name)
        {
            if (this.Model.AddNewGlobalDictionary(name))
            {
                GlobalTextureDictionaryViewModel newDictionary = new GlobalTextureDictionaryViewModel(this.Model[name], this);
                newDictionary.SourceTextureDictionaries.Clear();
                this.GlobalTextureDictionaries.Add(newDictionary);
                if (this.SourceTextureDictionaries != null)
                { 
                    while (this.SourceTextureDictionaries.Count > 0)
                    {
                        SourceTextureDictionaryViewModel sd = this.SourceTextureDictionaries.FirstOrDefault();
                        sd.Parent = newDictionary;
                        newDictionary.SourceTextureDictionaries.Add(sd);
                        this.SourceTextureDictionaries.Remove(sd);
                    }
                }

                this.IsExpanded = true;
                this.IsFocused = true;
                newDictionary.IsExpanded = true;
                OnAllChildrenChanged();
                newDictionary.OnAllChildrenChanged();
                return true;
            }
            return false;
        }

        /// <summary>
        /// Creates a new dictionary with the given name and adds it to this dictionary 
        /// iff the name is unique
        /// </summary>
        public Boolean AddNewGlobalDictionaryAsParent(String name)
        {
            if (this.Parent.AddNewGlobalDictionary(name))
            {
                GlobalTextureDictionaryViewModel newViewModel = null;
                foreach (GlobalTextureDictionaryViewModel vm in this.Parent.GlobalTextureDictionaries)
                {
                    if (vm.Name == name)
                    {
                        newViewModel = vm;
                        break;
                    }
                }
                if (newViewModel == null)
                    return false;

                newViewModel.MoveGlobalDictionaryHere(this);
                newViewModel.IsExpanded = true;
                this.OnAllChildrenChanged();
                return true;
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dictionary"></param>
        /// <returns></returns>
        public bool MoveGlobalDictionaryHere(GlobalTextureDictionaryViewModel dictionary)
        {
            if (this.Model.MoveGlobalDictionaryHere(dictionary.Model))
            {
                IDictionaryContainerViewModel oldParent = dictionary.Parent;
                dictionary.Parent = this;
                this.GlobalTextureDictionaries.Add(dictionary);

                oldParent.GlobalTextureDictionaries.Remove(dictionary);
                GlobalRootViewModel.CheckPromotedTexturesAreValid(oldParent, ValidationDirection.Both);

                OnAllChildrenChanged();
                this.IsExpanded = true;
                if (oldParent is GlobalTextureDictionaryViewModel)
                    (oldParent as GlobalTextureDictionaryViewModel).OnAllChildrenChanged();
                else if (oldParent is GlobalRootViewModel)
                    (oldParent as GlobalRootViewModel).RaiseGlobalDictionariesChangedEvent();
                return true;
            }
            return false;
        }

        /// <summary>
        /// Creates a new source dictionary using the given texture dictionary as a 
        /// source guide.
        /// </summary>
        public Boolean AddNewSourceDictionary(TextureDictionaryViewModel dictionary)
        {
            if (this.Model.AddNewSourceDictionary(dictionary.Model))
            {
                SourceTextureDictionaryViewModel newDictionary = new SourceTextureDictionaryViewModel(this.Model.GetSourceDictionaryByName(dictionary.Model.Name), this);
                this.SourceTextureDictionaries.Add(newDictionary);
                this.IsExpanded = true;
                this.IsFocused = true;
                OnAllChildrenChanged();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Removes the texture dictionary with the given name from the list iff
        /// it exists.
        /// </summary>
        public Boolean RemoveGlobalTextureDictionary(String name)
        {
            return false;
        }

        /// <summary>
        /// Removes the given texture dictionary from the list iff
        /// it exists.
        /// </summary>
        public Boolean RemoveGlobalTextureDictionary(GlobalTextureDictionaryViewModel dictionary)
        {
            if (this.Model.RemoveGlobalTextureDictionary(dictionary.Model))
            {
                if (this.GlobalTextureDictionaries.Contains(dictionary))
                {
                    this.GlobalTextureDictionaries.Remove(dictionary);
                    GlobalRootViewModel.CheckPromotedTexturesAreValid(this, ValidationDirection.Up);
                    OnAllChildrenChanged();
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Removes the parent dictionary put keeps this and all siblings.
        /// </summary>
        public Boolean RemoveParentGlobalDictionary()
        {
            if (this.Parent == null || this.Parent.Parent == null)
                return false;

            if (this.Model.RemoveParentGlobalDictionary())
            {
                IDictionaryContainerViewModel newParent = this.Parent.Parent;
                this.Parent.Parent.GlobalTextureDictionaries.Remove(this.Parent as GlobalTextureDictionaryViewModel);
                GlobalTextureDictionaryViewModel oldParent = (this.Parent as GlobalTextureDictionaryViewModel);
                foreach (GlobalTextureDictionaryViewModel child in oldParent.GlobalTextureDictionaries)
                {
                    child.Parent = newParent;
                    newParent.GlobalTextureDictionaries.Add(child);
                }
                OnAllChildrenChanged();
                if (newParent is GlobalTextureDictionaryViewModel)
                {
                    (newParent as GlobalTextureDictionaryViewModel).OnAllChildrenChanged();
                    GlobalRootViewModel.CheckPromotedTexturesAreValid(newParent, ValidationDirection.Up);
                }
                else if (newParent is GlobalRootViewModel)
                {
                    (newParent as GlobalRootViewModel).RaiseGlobalDictionariesChangedEvent();
                    GlobalRootViewModel.CheckPromotedTexturesAreValid(newParent, ValidationDirection.Up);
                }
                return true;
            }

            return false;
        }

        /// <summary>
        /// Removes the parent dictionary put keeps this and all siblings.
        /// </summary>
        public Boolean RemoveButKeepChildren()
        {
            if (this.Parent == null)
                return false;

            if (this.Model.RemoveButKeepChildren())
            {
                IDictionaryContainerViewModel newParent = this.Parent;
                newParent.GlobalTextureDictionaries.Remove(this);
                GlobalTextureDictionaryViewModel oldParent = this;
                foreach (GlobalTextureDictionaryViewModel child in oldParent.GlobalTextureDictionaries)
                {
                    child.Parent = newParent;
                    newParent.GlobalTextureDictionaries.Add(child);
                }
                foreach (SourceTextureDictionaryViewModel child in oldParent.SourceTextureDictionaries)
                {
                    child.Parent = newParent;
                    newParent.SourceTextureDictionaries.Add(child);
                }
                OnAllChildrenChanged();
                if (newParent is GlobalTextureDictionaryViewModel)
                {
                    (newParent as GlobalTextureDictionaryViewModel).OnAllChildrenChanged();
                    GlobalRootViewModel.CheckPromotedTexturesAreValid(newParent, ValidationDirection.Up);
                }
                else if (newParent is GlobalRootViewModel)
                {
                    (newParent as GlobalRootViewModel).RaiseGlobalDictionariesChangedEvent();
                    GlobalRootViewModel.CheckPromotedTexturesAreValid(newParent, ValidationDirection.Up);
                }
                return true;
            }

            return false;
        }

        /// <summary>
        /// Removes the given texture dictionary from the list iff
        /// it exists.
        /// </summary>
        public Boolean RemoveSourceTextureDictionary(SourceTextureDictionaryViewModel dictionary)
        {
            if (this.Model.RemoveSourceTextureDictionary(dictionary.Model))
            {
                if (this.SourceTextureDictionaries.Contains(dictionary))
                {
                    this.SourceTextureDictionaries.Remove(dictionary);
                    GlobalRootViewModel.CheckPromotedTexturesAreValid(this, ValidationDirection.Up);
                    this.OnAllChildrenChanged();
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Removes the given texture dictionary from the list iff
        /// it exists.
        /// </summary>
        public Boolean MoveSourceDictionaryHere(SourceTextureDictionaryViewModel dictionary)
        {
            if (this.Model.MoveSourceDictionaryHere(dictionary.Model))
            {
                IDictionaryContainerViewModel oldParent = dictionary.Parent;
                dictionary.Parent = this;
                this.SourceTextureDictionaries.Add(dictionary);

                oldParent.SourceTextureDictionaries.Remove(dictionary);
                GlobalRootViewModel.CheckPromotedTexturesAreValid(oldParent, ValidationDirection.Up);
                (oldParent as GlobalTextureDictionaryViewModel).OnAllChildrenChanged();

                OnAllChildrenChanged();
                return true;
            }

            return false;
        }

        public void RaiseGlobalDictionariesChangedEvent()
        {
            OnPropertyChanged("GlobalTextureDictionaries");
        }

        public void RaiseSourceDictionariesChangedEvent()
        {
            OnPropertyChanged("SourceTextureDictionaries");
        }
        #endregion // IGlobalDictionaryContainerViewModel Implementation

        #region ITextureContainerViewModel Implementation

        public void RemoveTexture(ITextureChildViewModel texture)
        {
            this.Textures.Remove(texture);
        }

        /// <summary>
        /// Promotes the given texture (source or global) to here and makes sure that
        /// validation is done so that the texture is removed elsewhere
        /// </summary>
        public Boolean PromoteTextureHere(SourceTextureViewModel texture)
        {
            return PromoteTextureHere(texture.Model);
        }

        /// <summary>
        /// Promotes the given texture (source or global) to here and makes sure that
        /// validation is done so that the texture is removed elsewhere
        /// </summary>
        public Boolean PromoteTextureHere(SourceTexture texture)
        {
            if (this.Model.PromoteTextureHere(texture))
            {
                GlobalTextureViewModel newTexture = new GlobalTextureViewModel(this.Model.GetTextureByName(texture.StreamName) as GlobalTexture, this);
                this.Textures.Add(newTexture);
                GlobalRootViewModel.CheckSingleGlobalTexture(this, texture.StreamName, ValidationDirection.Down);
                this.IsExpanded = true;
                this.IsFocused = true;
                OnAllChildrenChanged();
                newTexture.IsSelected = true;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Unpromotes the given texture from this dictionary and makes sure that all instances
        /// of it inside source dictionaries have their promotion state reset.
        /// </summary>
        public Boolean FullyUnpromoteTexture(GlobalTextureViewModel texture)
        {
            if (this.Model.FullyUnpromoteTexture(texture.Model))
            {
                this.Textures.Remove(texture);
                OnPropertyChanged("Textures");
                return true;
            }
            return false;
        }

        /// <summary>
        /// Moves the given global texture from its previous global dictionary to this 
        /// global dictionary
        /// </summary>
        public Boolean MoveTextureHere(GlobalTextureViewModel texture)
        {
            if (this.Model.MoveTextureHere(texture.Model))
            {
                this.Textures.Add(texture);
                GlobalRootViewModel.CheckSingleGlobalTexture(this, texture.StreamName, ValidationDirection.Both);
                texture.Parent = this;
                texture.Root = this.Root;
                this.OnAllChildrenChanged();

                this.IsExpanded = true;
                texture.IsSelected = true;
                texture.IsFocused = true;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Moves the given global texture from this dictionary before it gets
        /// added to another dictionary
        /// </summary>
        public Boolean MoveTextureFromHere(GlobalTextureViewModel texture)
        {
            if (!this.Textures.Contains(texture))
                return false;

            this.Textures.Remove(texture);
            OnPropertyChanged("Textures");
            return true;
        }

        /// <summary>
        /// Moves the given global texture from this dictionary before it gets
        /// added to another dictionary
        /// </summary>
        public Boolean MoveTextureFromHere(String streamName)
        {
            List<GlobalTextureViewModel> temp = new List<GlobalTextureViewModel>();
            foreach (GlobalTextureViewModel texture in this.Textures)
            {
                temp.Add(texture);
            }

            foreach (GlobalTextureViewModel texture in temp)
            {
                if (texture.Model.StreamName == streamName)
                {
                    this.Textures.Remove(texture);
                    OnPropertyChanged("Textures");

                    return true;
                }
            }
            return false;
        }

        #endregion // ITextureContainerViewModel Implementation

        #region Private Functions

        /// <summary>
        /// 
        /// </summary>
        private String GetPrettySize(int bytes)
        {
            String size = "0 KB";
            if (bytes < 1024 * 1024)
            {
                int kbytes = bytes >> 10;
                int kbyte = 1024;
                int leftover = bytes - (kbytes * kbyte);

                if (leftover > 0)
                {
                    size = String.Format("{0} KB", ((double)bytes / (double)kbyte).ToString("F1"));
                }
                else
                {
                    size = String.Format("{0} KB", bytes >> 10);
                }
            }
            else
            {
                int mbytes = bytes >> 20;
                int mbyte = 1024 * 1024;
                int leftover = bytes - (mbytes * mbyte);

                if (leftover > 0)
                {
                    size = String.Format("{0} MB", ((double)bytes / (double)mbyte).ToString("F1"));
                }
                else
                {
                    size = String.Format("{0} MB", bytes >> 20);
                }
            }

            return size;
        }

        private bool ContainsSourceTexture(string streamName)
        {
            foreach (SourceTextureDictionaryViewModel sd in this.SourceTextureDictionaries)
            {
                foreach (ITextureChildViewModel texture in sd.Children.OfType<ITextureChildViewModel>())
                {
                    if (texture.StreamName.ToLower() == streamName)
                    {
                        return true;
                    }
                }
            }

            foreach (GlobalTextureDictionaryViewModel gd in this.GlobalTextureDictionaries)
            {
                if (gd.ContainsSourceTexture(streamName))
                    return true;
            }

            return false;
        }

        private GlobalTextureViewModel FindGlobalTexture(string streamName, ValidationDirection direction)
        {
            string name = streamName.ToLower();
            foreach (GlobalTextureViewModel vm in this.Textures)
            {
                if (vm.StreamName.ToLower() == name)
                    return vm;
            }

            if (direction == ValidationDirection.Up || direction == ValidationDirection.Both)
            {
                if (this.Parent != null && this.Parent is GlobalTextureDictionaryViewModel)
                {
                    GlobalTextureViewModel found = (this.Parent as GlobalTextureDictionaryViewModel).FindGlobalTexture(streamName, ValidationDirection.Up);
                    if (found != null)
                        return found;
                }
            }
            if (direction == ValidationDirection.Down || direction == ValidationDirection.Both)
            {
                foreach (GlobalTextureDictionaryViewModel dictionary in GlobalTextureDictionaries)
                {
                    GlobalTextureViewModel found = dictionary.FindGlobalTexture(streamName, ValidationDirection.Down);
                    if (found != null)
                        return found;
                }
            }

            return null;
        }

        #endregion // Private Functions

        #region IWeakEventListener Members

        /// <summary>
        /// 
        /// </summary>
        public override Boolean ReceiveWeakEvent(Type managerType, Object sender, EventArgs e)
        {
            if (managerType == typeof(PropertyChangedEventManager))
            {
                PropertyChangedEventArgs pcArgs = e as PropertyChangedEventArgs;
                if (pcArgs != null)
                {
                    this.OnPropertyChanged(pcArgs.PropertyName);
                    if (pcArgs.PropertyName == "CurrentSize")
                    {
                        this.PrettyCurrentSize = this.GetPrettySize(this.CurrentSize);
                    }
                }
            }

            return true;
        }

        #endregion

        #region IDropTarget Members

        public void DragOver(DropInfo dropInfo)
        {
            IEnumerable data = null;
            if (dropInfo.Data is IEnumerable && !(dropInfo.Data is string))
            {
                data = (IEnumerable)dropInfo.Data;
            }
            else
            {
                data = Enumerable.Repeat(dropInfo.Data, 1);
            }
            dropInfo.Effects = DragDropEffects.None;
            Boolean validTextures = true;
            Boolean validSourceDictionaries = true;
            foreach (Object draggedData in data)
            {
                if (draggedData is TextureDictionary)
                {
                    validTextures = false;
                    if (this.CanAcceptSourceDictionaries == true)
                    {
                        if (GlobalRootViewModel.ContainsSourceDictionary((draggedData as TextureDictionary).Name, this.Root))
                        {
                            validSourceDictionaries = false;
                        }
                    }
                }
                else if (draggedData is SourceTexture)
                {
                    validSourceDictionaries = false;
                    foreach (ITextureChildViewModel child in this.Textures)
                    {
                        if (child.StreamName.ToLower() == (draggedData as SourceTexture).StreamName.ToLower())
                        {
                            validTextures = false;
                            break;
                        }
                    }
                    if (validTextures)
                    {
                        if (!ContainsSourceTexture((draggedData as SourceTexture).StreamName.ToLower()))
                            validTextures = false;
                    }
                }
                else if (draggedData is GlobalTextureViewModel)
                {
                    validSourceDictionaries = false;
                    foreach (ITextureChildViewModel child in this.Textures)
                    {
                        if (child.StreamName.ToLower() == (draggedData as GlobalTextureViewModel).StreamName.ToLower())
                        {
                            validTextures = false;
                            break;
                        }
                    }
                    if (validTextures)
                    {
                        if (!this.Model.CanPromoteGlobalTexture((draggedData as GlobalTextureViewModel).StreamName, ValidationDirection.Both))
                            validTextures = false;
                    }
                }
                else if (draggedData is SourceTextureDictionaryViewModel)
                {
                    validTextures = false;
                    if (this.CanAcceptSourceDictionaries == true)
                    {
                        if ((draggedData as SourceTextureDictionaryViewModel).Parent == this)
                        {
                            validSourceDictionaries = false;
                        }
                    }
                    else
                    {
                        validSourceDictionaries = false;
                    }
                }
                else if (draggedData is GlobalTextureDictionaryViewModel)
                {
                    // Has to be different parent, not itself or a child
                    validTextures = false;
                    if ((draggedData as GlobalTextureDictionaryViewModel) == this)
                        validSourceDictionaries = false;
                    else if ((draggedData as GlobalTextureDictionaryViewModel).Parent == this)
                        validSourceDictionaries = false;
                    else if (GlobalRootViewModel.GetParent(this, (draggedData as GlobalTextureDictionaryViewModel)) == (draggedData as GlobalTextureDictionaryViewModel))
                        validSourceDictionaries = false;
                }
            }
            if (validTextures == true)
            {
                dropInfo.Effects = DragDropEffects.Move;
                return;
            }
            else if (validSourceDictionaries == true)
            {
                dropInfo.Effects = DragDropEffects.Move;
            }
        }

        public void Drop(DropInfo dropInfo)
        {
            IEnumerable data = null;
            if (dropInfo.Data is IEnumerable && !(dropInfo.Data is string))
            {
                data = (IEnumerable)dropInfo.Data;
            }
            else
            {
                data = Enumerable.Repeat(dropInfo.Data, 1);
            }
            foreach (Object draggedData in data)
            {
                if (draggedData is TextureDictionary)
                {
                    if (this.CanAcceptSourceDictionaries == true)
                    {
                        if (!GlobalRootViewModel.ContainsSourceDictionary((draggedData as TextureDictionary).Name, this.Root))
                        {
                            TextureDictionaryViewModel newViewModel = new TextureDictionaryViewModel(this, draggedData as TextureDictionary);
                            this.AddNewSourceDictionary(newViewModel);
                        }
                    }
                }
                else if (draggedData is SourceTexture)
                {
                    foreach (ITextureChildViewModel child in this.Textures)
                    {
                        if (child.StreamName.ToLower() == (draggedData as SourceTexture).StreamName.ToLower())
                        {
                            return;
                        }
                    }
                    if (ContainsSourceTexture((draggedData as SourceTexture).StreamName.ToLower()))
                        this.PromoteTextureHere(draggedData as SourceTexture);
                }
                else if (draggedData is GlobalTextureViewModel)
                {
                    if (this == (draggedData as GlobalTextureViewModel).Parent)
                        return;
                    else if (!this.Model.CanPromoteGlobalTexture((draggedData as GlobalTextureViewModel).StreamName, ValidationDirection.Both))
                        return;

                    // Need to make sure we are dragging one that is in our hierarchy
                    GlobalTextureViewModel found = this.FindGlobalTexture((draggedData as GlobalTextureViewModel).StreamName, ValidationDirection.Both);
                    if (found != null)
                        this.MoveTextureHere(found);
                }
                else if (draggedData is SourceTextureDictionaryViewModel)
                {
                    if (this.CanAcceptSourceDictionaries == false)
                        return;
                    if ((draggedData as SourceTextureDictionaryViewModel).Parent == this)
                        return;

                    SourceTextureDictionaryViewModel source = (draggedData as SourceTextureDictionaryViewModel);
                    this.MoveSourceDictionaryHere(source);
                }
                else if (draggedData is GlobalTextureDictionaryViewModel)
                {
                    if ((draggedData as GlobalTextureDictionaryViewModel) == this)
                        return;
                    else if ((draggedData as GlobalTextureDictionaryViewModel).Parent == this)
                        return;
                    else if (GlobalRootViewModel.GetParent(this, (draggedData as GlobalTextureDictionaryViewModel)) == (draggedData as GlobalTextureDictionaryViewModel))
                        return;

                    this.MoveGlobalDictionaryHere(draggedData as GlobalTextureDictionaryViewModel);
                }
            }
        }

        #endregion // IDropTarget Members

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="results"></param>
        public IEnumerable<IViewModel> FindAll(Regex expression)
        {
            foreach (GlobalTextureDictionaryViewModel dictionary in GlobalTextureDictionaries)
            {
                if (expression.IsMatch(dictionary.Name))
                    yield return dictionary;

                foreach (IViewModel match in dictionary.FindAll(expression))
                {
                    yield return match;
                }
            }

            foreach (SourceTextureDictionaryViewModel dictionary in SourceTextureDictionaries)
            {
                if (expression.IsMatch(dictionary.Name))
                    yield return dictionary;

                foreach (IViewModel match in dictionary.FindAll(expression))
                {
                    yield return match;
                }
            }
            foreach (GlobalTextureViewModel texture in Textures)
            {
                if (expression.IsMatch(texture.Model.StreamName))
                    yield return texture;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="results"></param>
        public IEnumerable<IViewModel> GetAll()
        {
            foreach (GlobalTextureDictionaryViewModel dictionary in GlobalTextureDictionaries)
            {
                yield return dictionary;
                foreach (IViewModel match in dictionary.GetAll())
                {
                    yield return match;
                }
            }

            foreach (SourceTextureDictionaryViewModel dictionary in SourceTextureDictionaries)
            {
                yield return dictionary;
                foreach (IViewModel match in dictionary.GetAll())
                {
                    yield return match;
                }
            }
            foreach (GlobalTextureViewModel texture in Textures)
            {
                yield return texture;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="results"></param>
        public IEnumerable<SourceTextureDictionaryViewModel> GetAllSourceDictionaries()
        {
            foreach (GlobalTextureDictionaryViewModel dictionary in GlobalTextureDictionaries)
            {
                foreach (SourceTextureDictionaryViewModel match in dictionary.GetAllSourceDictionaries())
                {
                    yield return match;
                }
            }

            foreach (SourceTextureDictionaryViewModel dictionary in SourceTextureDictionaries)
            {
                yield return dictionary;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="results"></param>
        public IEnumerable<GlobalTextureDictionaryViewModel> GetAllGlobalDictionaries()
        {
            foreach (GlobalTextureDictionaryViewModel dictionary in GlobalTextureDictionaries)
            {
                yield return dictionary;
                foreach (GlobalTextureDictionaryViewModel match in dictionary.GetAllGlobalDictionaries())
                {
                    yield return match;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Expand()
        {
            this.IsExpanded = true;
        }

        /// <summary>
        /// 
        /// </summary>
        public void ExpandedTo()
        {
            IDictionaryContainerViewModel parent = this.Parent;
            while (parent != null)
            {
                parent.Expand();
                parent = parent.Parent;
            }
        }

        public void OnChildrenDictionariesChanged()
        {
            OnPropertyChanged("GlobalTextureDictionaries");
            OnPropertyChanged("SourceTextureDictionaries");
        }

        public void OnAllChildrenChanged()
        {
            OnPropertyChanged("GlobalTextureDictionaries");
            OnPropertyChanged("SourceTextureDictionaries");
            OnPropertyChanged("Textures");
        }
        #endregion
    } // GlobalTextureDictionaryViewModel
} // Workbench.AddIn.TXDParentiser.ViewModel
