﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Model.Common.Map;
using System.Windows;
using System.ComponentModel;

namespace ContentBrowser.ViewModel.MapViewModels3
{
    /// <summary>
    /// 
    /// </summary>
    public class RoomViewModel : ObservableContainerViewModelBase<IEntity>, IWeakEventListener, IDisposable
    {
        #region Members
        /// <summary>
        /// Thread synchronisation helper object
        /// </summary>
        private object m_syncObject = new object();
        #endregion // Members

        #region Properties
        /// <summary>
        /// The number of viewmodels to create before displaying them on the
        /// screen. Less than or equal to 0 implies no batching
        /// </summary>
        protected override uint AssetCreationBatchSize
        {
            get
            {
                return 250;
            }
        }

        /// <summary>
        /// The number of viewmodels to create before displaying them on the
        /// screen. Less than or equal to 0 implies no batching
        /// </summary>
        protected override uint GridCreationBatchSize
        {
            get
            {
                return 250;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private IRoom Room
        {
            get
            {
                return (IRoom)Model;
            }
        }
        #endregion // Properties

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public RoomViewModel(IRoom room)
            : base(room, room.ChildEntities)
        {
            PropertyChangedEventManager.AddListener(Room, this, "ChildEntities");
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// This gets called whenever the selection state for this view model
        /// has changed
        /// </summary>
        protected override void OnSelectionChanged(Boolean oldValue, Boolean newValue)
        {
            if (IsSelected)
            {
                lock (m_syncObject)
                {
                    Room.RequestAllStatistics(true);
                }
            }

            base.OnSelectionChanged(oldValue, newValue);
        }

        /// <summary>
        /// This gets called whenever the expansion state for this view model
        /// has changed
        /// </summary>
        protected override void OnExpansionChanged(Boolean oldValue, Boolean newValue)
        {
            if (IsExpanded)
            {
                lock (m_syncObject)
                {
                    Room.RequestStatistics(new StreamableStat[] { StreamableRoomStat.Entities }, true);
                }
            }

            base.OnExpansionChanged(oldValue, newValue);
        }
        #endregion // Overrides

        #region IWeakEventListener Implementation
        /// <summary>
        /// 
        /// </summary>
        public bool ReceiveWeakEvent(Type managerType, Object sender, EventArgs e)
        {
            if (Application.Current != null)
            {
                Application.Current.Dispatcher.Invoke(
                    new Action
                    (
                        delegate()
                        {
                            MonitoredCollection = Room.ChildEntities;
                            OnExpansionChanged(!IsExpanded, IsExpanded);
                            OnSelectionChanged(!IsSelected, IsSelected);
                        }
                    ),
                    System.Windows.Threading.DispatcherPriority.ContextIdle
                );
            }
            else
            {
                MonitoredCollection = Room.ChildEntities;
            }
            return true;
        }
        #endregion // IWeakEventListener

        #region IDisposable Implementation
        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            PropertyChangedEventManager.RemoveListener(Room, this, "ChildEntities");
        }
        #endregion // IDisposable Implementation
    } // RoomViewModel
}
