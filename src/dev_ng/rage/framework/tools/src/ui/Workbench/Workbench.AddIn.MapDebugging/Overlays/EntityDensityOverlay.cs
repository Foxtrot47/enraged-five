﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using MapViewport.AddIn;
using RSG.Base.Attributes;
using RSG.Base.Collections;
using RSG.Base.Drawing;
using RSG.Base.Extensions;
using RSG.Base.Math;
using RSG.Base.Tasks;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using RSG.Base.Windows.Helpers;
using RSG.Model.Common;
using RSG.Model.Common.Map;
using RSG.Model.Common.Util;
using Workbench.AddIn.Services;

namespace Workbench.AddIn.MapDebugging.Overlays
{
    /// <summary>
    /// Which entities to include when generating the overlay..
    /// </summary>
    public enum EntityFilter
    {
        [FriendlyName("All Entities")]
        AllEntities,

        [FriendlyName("Static Only")]
        StaticOnly,

        [FriendlyName("Dynamic Only")]
        DynamicOnly
    }

    /// <summary>
    /// Resolutions that the overlay supports.
    /// </summary>
    public enum EntityDensityResolution
    {
        [FriendlyName("10x10")]
        TenByTen,

        [FriendlyName("20x20")]
        TwentyByTwenty,

        [FriendlyName("50x50")]
        FiftyByFifty,

        [FriendlyName("100x100")]
        HundredByHundred
    }

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension("Workbench.AddIn.MapDebugging.OverlayGroups.MapDebug", typeof(Viewport.AddIn.ViewportOverlay))]
    public class EntityDensityOverlay : LevelDependentViewportOverlay
    {
        #region Constants
        private const String c_name = "Entity Density";
        private const String c_description = "Displays the location of the entities associated with the selected archetype/prop section.";
        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// Content Browser
        /// </summary>
        [ImportExtension(ContentBrowser.AddIn.CompositionPoints.ContentBrowser, typeof(ContentBrowser.AddIn.IContentBrowser))]
        public Lazy<ContentBrowser.AddIn.IContentBrowser> ContentBrowserProxy { get; set; }

        /// <summary>
        /// A reference to view model for the map viewport
        /// </summary>
        [ImportExtension(Viewport.AddIn.CompositionPoints.MapViewport, typeof(Viewport.AddIn.IMapViewport))]
        public Lazy<Viewport.AddIn.IMapViewport> MapViewportProxy { get; set; }

        /// <summary>
        /// MEF import for message box service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.MessageService, typeof(Workbench.AddIn.Services.IMessageService))]
        private Lazy<IMessageService> MessageService { get; set; }

        /// <summary>
        /// MEF import for message box service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.TaskProgressService, typeof(Workbench.AddIn.Services.ITaskProgressService))]
        private Lazy<ITaskProgressService> TaskProgressService { get; set; }
        #endregion // MEF Imports

        #region Properties
        /// <summary>
        /// Selected entity filter.
        /// </summary>
        [OverlayParameter(EntityFilter.AllEntities, FriendlyName = "SelectedEntityFilter")]
        public EntityFilter SelectedEntityFilter
        {
            get { return m_selectedEntityFilter; }
            set
            {
                SetPropertyValue(value, () => SelectedEntityFilter,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_selectedEntityFilter = (EntityFilter)newValue;
                            OverlayDirty = true;
                        }
                ));
            }
        }
        private EntityFilter m_selectedEntityFilter;

        /// <summary>
        /// Selected resolution to use for the map.
        /// </summary>
        [OverlayParameter(EntityDensityResolution.HundredByHundred, FriendlyName = "SelectedResolution")]
        public EntityDensityResolution SelectedResolution
        {
            get { return m_selectedResolution; }
            set
            {
                SetPropertyValue(value, () => SelectedResolution,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_selectedResolution = (EntityDensityResolution)newValue;
                            OverlayDirty = true;
                        }
                ));
            }
        }
        private EntityDensityResolution m_selectedResolution;

        /// <summary>
        /// Content filter (based off of the data selected in the content browser).
        /// </summary>
        public String CurrentContentFilter
        {
            get { return m_currentContentFilter; }
            set
            {
                SetPropertyValue(value, () => CurrentContentFilter,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_currentContentFilter = (String)newValue;
                            OverlayDirty = true;
                        }
                ));
            }
        }
        private String m_currentContentFilter;

        // Flag indicating whether the overlay needs to be refreshed.
        public bool OverlayDirty
        {
            get { return m_overlayDirty; }
            set
            {
                SetPropertyValue(value, () => OverlayDirty,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_overlayDirty = (bool)newValue;
                        }
                ));
            }
        }
        private bool m_overlayDirty;

        /// <summary>
        /// The value that is currently under the mouse.
        /// </summary>
        public Pair<Vector2f, uint> StatUnderMouse
        {
            get { return m_statUnderMouse; }
            set
            {
                SetPropertyValue(value, () => StatUnderMouse,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_statUnderMouse = (Pair<Vector2f, uint>)newValue;
                        }
                ));
            }
        }
        private Pair<Vector2f, uint> m_statUnderMouse;

        #region Render Options
        /// <summary>
        /// 
        /// </summary>
        [OverlayParameter(0u, FriendlyName = "Low Color Value")]
        public uint LowColorValue
        {
            get { return m_lowColorValue; }
            set
            {
                SetPropertyValue(value, () => LowColorValue,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_lowColorValue = (uint)newValue;
                            OverlayDirty = true;
                        }
                ));
            }
        }
        private uint m_lowColorValue;

        /// <summary>
        /// 
        /// </summary>
        [OverlayParameter(50u, FriendlyName = "High Color Value")]
        public uint HighColorValue
        {
            get { return m_highColorValue; }
            set
            {
                SetPropertyValue(value, () => HighColorValue,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_highColorValue = (uint)newValue;
                            OverlayDirty = true;
                        }
                ));
            }
        }
        private uint m_highColorValue;

        /// <summary>
        /// 
        /// </summary>
        [OverlayParameter(false, FriendlyName = "Only Greater Than")]
        public bool OnlyGreaterThan
        {
            get { return m_onlyGreaterThan; }
            set
            {
                SetPropertyValue(value, () => OnlyGreaterThan,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_onlyGreaterThan = (bool)newValue;
                            OverlayDirty = true;
                        }
                ));
            }
        }
        private bool m_onlyGreaterThan;

        /// <summary>
        /// 
        /// </summary>
        [OverlayParameter(false, FriendlyName = "Only Greater Than Average")]
        public bool OnlyGreaterThanAverage
        {
            get { return m_onlyGreaterThanAverage; }
            set
            {
                SetPropertyValue(value, () => OnlyGreaterThanAverage,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_onlyGreaterThanAverage = (Boolean)newValue;
                            OverlayDirty = true;
                        }
                ));
            }
        }
        private bool m_onlyGreaterThanAverage;

        /// <summary>
        /// 
        /// </summary>
        [OverlayParameter(50u, FriendlyName = "Only Greater Than Limit")]
        public uint GreaterThanLimit
        {
            get { return m_greaterThanLimit; }
            set
            {
                SetPropertyValue(value, () => GreaterThanLimit,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_greaterThanLimit = (uint)newValue;

                            if (OnlyGreaterThan)
                            {
                                OverlayDirty = true;
                            }
                        }
                ));
            }
        }
        private uint m_greaterThanLimit;
        #endregion // Render Options

        #region Stats
        /// <summary>
        /// 
        /// </summary>
        public uint MinValue
        {
            get { return m_minValue; }
            set
            {
                SetPropertyValue(value, () => MinValue,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_minValue = (uint)newValue;
                        }
                ));
            }
        }
        private uint m_minValue;

        /// <summary>
        /// 
        /// </summary>
        public uint MaxValue
        {
            get { return m_maxValue; }
            set
            {
                SetPropertyValue(value, () => MaxValue,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_maxValue = (uint)newValue;
                        }
                ));
            }
        }
        private uint m_maxValue;

        /// <summary>
        /// 
        /// </summary>
        public float AverageValue
        {
            get { return m_averageValue; }
            set
            {
                SetPropertyValue(value, () => AverageValue,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_averageValue = (float)newValue;
                        }
                ));
            }
        }
        private float m_averageValue;
        #endregion // Stats
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Mapping of location to density stats.
        /// </summary>
        private IDictionary<Vector2f, uint> DensityStats { get; set; }
        #endregion // Member Data

        #region Commands
        /// <summary>
        /// Command for querying the database for the data.
        /// </summary>
        public RelayCommand RefreshOverlayCommand
        {
            get
            {
                if (m_refreshOverlayCommand == null)
                {
                    m_refreshOverlayCommand = new RelayCommand(param => RefreshOverlay());
                }
                return m_refreshOverlayCommand;
            }
        }
        private RelayCommand m_refreshOverlayCommand;
        #endregion // Commands

        #region Constructor(s)
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public EntityDensityOverlay()
            : base(c_name, c_description)
        {
            DensityStats = new Dictionary<Vector2f, uint>();
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// Gets called when the user wants to view this overlay
        /// </summary>
        public override void Activated()
        {
            base.Activated();
            this.ContentBrowserProxy.Value.GridSelectionChanged += GridSelectionChanged;

            CancellationTokenSource cts = new CancellationTokenSource();
            LevelTaskContext context = new LevelTaskContext(cts.Token, LevelBrowserProxy.Value.SelectedLevel);
            IMapHierarchy hierarchy = context.MapHierarchy.AllSections.First().MapHierarchy;

            StreamableStat[] statsToLoad = new StreamableStat[] {
                StreamableMapSectionStat.Archetypes,
                StreamableMapSectionStat.Entities,
                StreamableEntityStat.Position };
            ITask dataLoadTask = hierarchy.CreateStatLoadTask(context.MapHierarchy.AllSections, statsToLoad);

            // Is there any data that needs to be loaded?
            if (dataLoadTask != null)
            {
                TaskProgressService.Value.Add(dataLoadTask, context, TaskPriority.Foreground, cts);
            }

            if (dataLoadTask == null || dataLoadTask.Status == TaskStatus.Completed)
            {
                RefreshOverlay();
            }
        }

        /// <summary>
        /// Gets called when the user turns off this overlay
        /// </summary>
        public override void Deactivated()
        {
            this.ContentBrowserProxy.Value.GridSelectionChanged -= GridSelectionChanged;
            base.Deactivated();
        }

        /// <summary>
        /// Gets the control that should be shown in the overlay details panel
        /// </summary>
        public override System.Windows.Controls.Control GetOverlayControl()
        {
            return new OverlayViews.EntityDensityOverlayView();
        }

        /// <summary>
        /// Gets the control that shows information at the bottom right of the viewport.
        /// </summary>
        public override System.Windows.Controls.Control GetViewportControl()
        {
            return new OverlayViews.EntityDensityInfoView();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewPosition"></param>
        /// <param name="worldPosition"></param>
        public override void OnMouseMoved(System.Windows.Point viewPosition, System.Windows.Point worldPosition)
        {
            uint blockSize = GetBlockSize();

            // Try and find the dto that is under the mouse
            if (DensityStats.Any(item => (worldPosition.X > item.Key.X &&
                                            worldPosition.X < item.Key.X + blockSize &&
                                            worldPosition.Y > item.Key.Y &&
                                            worldPosition.Y < item.Key.Y + blockSize)))
            {
                KeyValuePair<Vector2f, uint> densityPair =
                    DensityStats.First(item => (worldPosition.X > item.Key.X &&
                                                worldPosition.X < item.Key.X + blockSize &&
                                                worldPosition.Y > item.Key.Y &&
                                                worldPosition.Y < item.Key.Y + blockSize));
                StatUnderMouse = new Pair<Vector2f,uint>(densityPair.Key, densityPair.Value);
            }
            else
            {
                StatUnderMouse = null;
            }
        }
        #endregion // Overrides

        #region Event Handlers
        /// <summary>
        /// Get called when the items selected in the content browser change
        /// </summary>
        void GridSelectionChanged(Object sender, ContentBrowser.AddIn.GridSelectionChangedEventArgs args)
        {
            CurrentContentFilter = null;

            if (args.NewItem.Any())
            {
                IList<String> contentFilters = new List<String>();

                foreach (IAsset asset in this.ContentBrowserProxy.Value.SelectedGridItems)
                {
                    if (asset is IMapArea || asset is IMapSection || asset is IArchetype)
                    {
                        contentFilters.Add(asset.Name);
                    }
                }

                if (contentFilters.Any())
                {
                    CurrentContentFilter = String.Join(", ", contentFilters);
                }
                else
                {
                    CurrentContentFilter = "Select an area, section or archetype.";
                }
            }
        }
        #endregion // Event Handlers

        #region Private Methods
        /// <summary>
        /// Performs a full refresh of the overlay.
        /// </summary>
        private void RefreshOverlay()
        {
            // We've now got the data, update the overlay
            Geometry.BeginUpdate();
            Geometry.Clear();

            // Create the overlay image.
            Viewport2DImageOverlay overlayImage = CreateOverlayImage();
            overlayImage.BeginUpdate();

            // Create the density stats based on the user's selected options.
            DensityStats = CreateDensityStats();

            if (DensityStats.Any())
            {
                MinValue = DensityStats.Min(item => item.Value);
                AverageValue = (float)DensityStats.Average(item => item.Value);
                MaxValue = DensityStats.Max(item => item.Value);
            }
            else
            {
                MinValue = 0;
                AverageValue = 0.0f;
                MaxValue = 0;
            }
            StatUnderMouse = null;
            uint blockSize = GetBlockSize();

            // Calculate the min/max/average values.
            foreach (KeyValuePair<Vector2f, uint> statPair in DensityStats)
            {
                // Check whether we should actually render this point
                if ((OnlyGreaterThanAverage && statPair.Value < AverageValue) ||
                    (OnlyGreaterThan && statPair.Value < GreaterThanLimit))
                {
                    continue;
                }
                
                Vector2f[] points = new Vector2f[4];
                points[0] = statPair.Key;
                points[1] = (statPair.Key + new Vector2f(blockSize, 0.0f));
                points[2] = (statPair.Key + new Vector2f(blockSize, blockSize));
                points[3] = (statPair.Key + new Vector2f(0.0f, blockSize));

                overlayImage.RenderPolygon(points, CreateColorForValue(statPair.Value));
            }

            // TODO: Make opacity an exposed parameter?
            overlayImage.UpdateImageOpacity((byte)((60.0f / 100.0f) * 255));
            overlayImage.EndUpdate();
            Geometry.Add(overlayImage);
            Geometry.EndUpdate();

            // Overlay is no longer dirty.
            OverlayDirty = false;
        }

        /// <summary>
        /// Creates a new overlay image based on the maps dimensions.
        /// </summary>
        private Viewport2DImageOverlay CreateOverlayImage()
        {
            Vector2i size = new Vector2i(1000, 1000);
            Vector2f pos = new Vector2f(-500.0f, 500.0f);

            ILevel level = LevelBrowserProxy.Value.SelectedLevel;
            if (level.ImageBounds != null)
            {
                size = new Vector2i((int)(level.ImageBounds.Max.X - level.ImageBounds.Min.X), (int)(level.ImageBounds.Max.Y - level.ImageBounds.Min.Y));
                pos = new Vector2f(level.ImageBounds.Min.X, level.ImageBounds.Max.Y);
            }

            // Determine how big the image should be based on the selected resolution
            int xRes = (int)Math.Ceiling((float)size.X / (float)GetBlockSize());
            int yRes = (int)Math.Ceiling((float)size.Y / (float)GetBlockSize());

            Viewport2DImageOverlay overlayImage = new Viewport2DImageOverlay(etCoordSpace.World, "", new Vector2i(xRes, yRes), pos, new Vector2f(size.X, size.Y));
            overlayImage.BitmapScalingMode = System.Windows.Media.BitmapScalingMode.NearestNeighbor;
            return overlayImage;
        }

        /// <summary>
        /// Converts the entity density resolution to it's integer representation
        /// </summary>
        private uint GetBlockSize()
        {
            switch (SelectedResolution)
            {
                case EntityDensityResolution.TenByTen:
                    return 10;
                case EntityDensityResolution.TwentyByTwenty:
                    return 20;
                case EntityDensityResolution.FiftyByFifty:
                    return 50;
                case EntityDensityResolution.HundredByHundred:
                    return 100;
                default:
                    Debug.Assert(false, String.Format("Can't convert {0} to its integer representation.", SelectedResolution));
                    throw new ArgumentException(String.Format("Can't convert {0} to its integer representation.", SelectedResolution));
            }
        }

        /// <summary>
        /// Goes over all entities grouping them per block based on the selected resolution.
        /// </summary>
        private IDictionary<Vector2f, uint> CreateDensityStats()
        {
            ISet<IArchetype> validArchetypes = GetArchetypeFilter();
            uint blockSize = GetBlockSize();

            IDictionary<Vector2f, uint> stats = new Dictionary<Vector2f, uint>();
            foreach (IMapSection section in this.LevelBrowserProxy.Value.SelectedLevel.MapHierarchy.AllSections)
            {
                foreach (IEntity entity in section.ChildEntities)
                {
                    if (IsValidEntity(entity, validArchetypes))
                    {
                        Vector2f location = new Vector2f();
                        location.X = (float)(Math.Floor(entity.Position.X / blockSize) * blockSize);
                        location.Y = (float)(Math.Floor(entity.Position.Y / blockSize) * blockSize);

                        if (!stats.ContainsKey(location))
                        {
                            stats[location] = 0;
                        }

                        stats[location]++;
                    }
                }
            }

            return stats;
        }

        /// <summary>
        /// Returns the list of archetypes that we wish to refine the data shown to.
        /// </summary>
        /// <returns></returns>
        private ISet<IArchetype> GetArchetypeFilter()
        {
            // Gather the sections that the user has selected by drilling down the hierarchy
            ISet<IArchetype> archetypeFilter = new HashSet<IArchetype>();

            if (ContentBrowserProxy.Value.SelectedGridItems.Any())
            {
                foreach (IAsset asset in ContentBrowserProxy.Value.SelectedGridItems)
                {
                    if (asset is IMapArea)
                    {
                        archetypeFilter.AddRange((asset as IMapArea).AllDescendentSections.SelectMany(item => item.Archetypes).Where(item => IsValidArchetype(item)));
                    }
                    else if (asset is IMapSection)
                    {
                        archetypeFilter.AddRange((asset as IMapSection).Archetypes.Where(item => IsValidArchetype(item)));
                    }
                    else if (asset is IArchetype)
                    {
                        archetypeFilter.Add(asset as IArchetype);
                    }
                }
            }
            else
            {
                archetypeFilter.AddRange(
                    LevelBrowserProxy.Value.SelectedLevel.MapHierarchy.AllSections
                        .SelectMany(item => item.Archetypes)
                        .Where(item => IsValidArchetype(item)));
            }

            return archetypeFilter;
        }

        /// <summary>
        /// Returns whether an archetype should be included based on the filter.
        /// </summary>
        private bool IsValidArchetype(IArchetype archetype)
        {
            if (SelectedEntityFilter == EntityFilter.AllEntities)
            {
                return true;
            }
            else if (SelectedEntityFilter == EntityFilter.DynamicOnly)
            {
                if (archetype is ISimpleMapArchetype)
                {
                    return (archetype as ISimpleMapArchetype).IsDynamic;
                }
                else
                {
                    return false;
                }
            }
            else if (SelectedEntityFilter == EntityFilter.StaticOnly)
            {
                if (archetype is ISimpleMapArchetype)
                {
                    return !(archetype as ISimpleMapArchetype).IsDynamic;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                Debug.Assert(false, String.Format("SelectedEntityFilter '{0}' is not supported.", SelectedEntityFilter));
                throw new ArgumentException(String.Format("SelectedEntityFilter '{0}' is not supported.", SelectedEntityFilter));
            }
        }

        /// <summary>
        /// Returns whether a particular entities referenced archetype is in the archetype set.
        /// </summary>
        private bool IsValidEntity(IEntity entity, ISet<IArchetype> archetypes)
        {
            return archetypes.Contains(entity.ReferencedArchetype);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fps"></param>
        /// <returns></returns>
        private Color CreateColorForValue(uint value)
        {
            // Use HSV method
            // 0 -> red
            // 0.3 -> green
            uint min = LowColorValue;
            uint max = HighColorValue;

            if (value < min)
            {
                value = min;
            }
            if (value > max)
            {
                value = max;
            }

            // Convert it to a 0->1 scale
            double fVal = value;
            fVal -= min;
            fVal /= (max - min);
            fVal = 1.0 - fVal;

            double lowHue = 0.0;
            double highHue = 1.0 / 3.0;
            double hueDiff = highHue - lowHue;

            byte h = (byte)((lowHue + (fVal * hueDiff)) * 255);
            byte s = (byte)(0.9 * 255);
            byte b = (byte)(0.9 * 255);

            return ColourSpaceConv.HSVtoColor(new HSV(h, s, b));
        }
        #endregion // Private Methods
    } // EntityDensityOverlay
}
