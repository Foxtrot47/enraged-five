﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Workbench.AddIn.Services
{


    /// <summary>
    /// Service interface.
    /// </summary>
    public interface IService
    {
        Guid ID { get; }
    }

} // Workbench.AddIn.Services namespace
