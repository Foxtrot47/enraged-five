﻿using System;
using System.Collections.Generic;
using RSG.Model.Perforce;

namespace ContentBrowser.ViewModel.PerforceViewModels
{

    /// <summary>
    /// 
    /// </summary>
    internal class WorkspaceViewModel : AssetContainerViewModelBase
    {
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public WorkspaceViewModel(Workspace ws)
            : base(ws)
        {
        }
        #endregion // Constructor(s)
    }

} // ContentBrowser.ViewModel.PerforceViewModels namespace
