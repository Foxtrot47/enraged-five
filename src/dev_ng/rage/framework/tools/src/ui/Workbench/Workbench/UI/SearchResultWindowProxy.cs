﻿using System;
using System.ComponentModel.Composition;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.Services.File;
using RSG.Base.Editor;

namespace Workbench.UI
{
    [ExportExtension(ExtensionPoints.ToolWindowProxy, typeof(IToolWindowProxy))]
    [ExportExtension("Workbench.UI.SearchResultWindowProxy", typeof(SearchResultWindowProxy))]
    public class SearchResultWindowProxy : IToolWindowProxy
    {
        #region MEF Imports
        [ImportExtension("Workbench.UI.SearchResultWindowViewModel", typeof(SearchResultWindowViewModel))]
        private SearchResultWindowViewModel SearchResultsViewModel { get; set; }
        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public SearchResultWindowProxy()
        {
            this.Name = "Search_Results";
            this.Header = "Search Results";
            this.DelayCreation = false;
        }

        #endregion // Constructor

        #region Public Functions

        /// <summary>
        /// A abstract function that is overriden by the plugin to create the
        /// actual instance of the tool window and pass it back as a out paramter
        /// </summary>
        protected override Boolean CreateToolWindow(out IToolWindowBase toolWindow)
        {
            toolWindow = new SearchResultWindowView(SearchResultsViewModel);

            return true;
        }

        #endregion // Public Functions
    }

    [ExportExtension(Workbench.AddIn.ExtensionPoints.SaveService, typeof(ISaveService))]
    class SearchSaveService : ISaveService
    {
        #region Constants
        private readonly Guid GUID =
            new Guid("1D8C266B-AE15-43CF-91F7-3F1E98E52D12");
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Service GUID.
        /// </summary>
        public Guid ID
        {
            get { return (GUID); }
        }

        /// <summary>
        /// Save dialog filter string.
        /// </summary>
        public String Filter
        {
            get { return "(Text File)|*.txt"; }
        }

        /// <summary>
        /// Save dialog default filter index.
        /// </summary>
        public int DefaultFilterIndex
        {
            get { return 0; }
        }

        /// <summary>
        /// Array of support document GUIDs.
        /// </summary>
        public Guid[] SupportedContent
        {
            get
            {
                List<Guid> array = new List<Guid>();
                array.Add(SearchResultWindowView.GUID);
                return array.ToArray();
            }
        }

        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public SearchSaveService()
        {
        }
        #endregion // Constructors(s)

        #region Methods
        /// <summary>
        /// Returns a value indicating if the save service
        /// can currently be executed
        /// </summary>
        public Boolean CanExecuteSave()
        {
            return true;
        }

        /// <summary>
        /// Returns a value indicating if the save service
        /// can currently be executed for a save as event
        /// </summary>
        public Boolean CanExecuteSaveAs()
        {
            return true;
        }

        /// <summary>
        /// Save the specified model data to disk file.
        /// </summary>
        public bool Save(IModel saveModel, String filename, int filterIndex, ref Boolean setPathToSavePath)
        {
            SearchResultWindowViewModel vm = saveModel as SearchResultWindowViewModel;
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(filename))
            {
                file.WriteLine("Search results: {0} Found.", Math.Max(vm.SearchOutput.Count - 2, 0));
                foreach (var result in vm.SearchOutput)
                {
                    file.WriteLine(result.DisplayText);
                }
            }

            return true;
        }
        #endregion // Methods
    }
}
