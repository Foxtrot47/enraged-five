﻿using System;
using System.Windows;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Workbench.AddIn.TXDParentiser.Util
{
    /// <summary>
    /// 
    /// </summary>
    public class NotifyPropertyChangedEventManager : WeakEventManager
    {
        public static NotifyPropertyChangedEventManager CurrentManager
        {
            get
            {
                var manager_type = typeof(NotifyPropertyChangedEventManager);
                var manager = WeakEventManager.GetCurrentManager(manager_type) as NotifyPropertyChangedEventManager;
                if (manager == null)
                {
                    manager = new NotifyPropertyChangedEventManager();
                    WeakEventManager.SetCurrentManager(manager_type, manager);
                }
                return manager;
            }
        }

        public static void AddListener(INotifyPropertyChanged source, IWeakEventListener listener)
        {
            CurrentManager.ProtectedAddListener(source, listener);
            return;
        }

        public static void RemoveListener(INotifyPropertyChanged source, IWeakEventListener listener)
        {
            CurrentManager.ProtectedRemoveListener(source, listener);
            return;
        }

        protected override void StartListening(object source)
        {
            ((INotifyPropertyChanged)source).PropertyChanged += Source_PropertyChanged; return;
        }

        protected override void StopListening(object source)
        {
            ((INotifyPropertyChanged)source).PropertyChanged -= Source_PropertyChanged;
            return;
        }

        void Source_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            { CurrentManager.DeliverEvent(sender, e); };
        }
    } // NotifyPropertyChangedEventManager
} // Workbench.AddIn.TXDParentiser.Util
