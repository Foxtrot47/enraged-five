﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using RSG.Base.Tasks;

namespace Workbench.AddIn.Services
{
    /// <summary>
    /// Task priority.
    /// </summary>
    public enum TaskPriority
    {
        /// <summary>
        /// Task is run in the foreground with a dialog box.
        /// </summary>
        Foreground,

        /// <summary>
        /// Task is run in the background. Progress is shown in the main window UI as a progress bar.
        /// </summary>
        Background
    }

    /// <summary>
    /// Interface for a task progress service that would allow callers to inform subscribers of the current 
    /// percentage completion of a task. 
    /// </summary>
    public interface ITaskProgressService
    {
        /// <summary>
        /// Add a task.
        /// </summary>
        /// <param name="task">Task.</param>
        /// <param name="context">Task context.</param>
        /// <param name="priority">Task priority.</param>
        /// <param name="cts">Cancellation token source required when TaskPriority is foreground.</param>
        void Add(ITask task, TaskContext context, TaskPriority priority, CancellationTokenSource cts = null);

        /// <summary>
        /// Stop the service.
        /// </summary>
        void Stop();

        /// <summary>
        /// Progress changed event.
        /// </summary>
        event TaskProgressEventHandler ProgressChanged;
    }

    /// <summary>
    /// Task progress delegate.
    /// </summary>
    /// <param name="sender">Sender.</param>
    /// <param name="e">Event arguments.</param>
    public delegate void TaskProgressEventHandler(object sender, TaskProgressServiceArgs e);

    /// <summary>
    /// Task progress arguments.
    /// </summary>
    public class TaskProgressServiceArgs : EventArgs
    {
        #region Public properties

        /// <summary>
        /// Percentage of task completed.
        /// </summary>
        public int PercentageComplete
        {
            get;
            private set;
        }

        /// <summary>
        /// Additional text message supplying more detail about the current task.
        /// </summary>
        public string Message
        {
            get;
            private set;
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="percentageComplete">Percentage complete in the range 0..100 inclusive.</param>
        /// <param name="message">Task message.</param>
        public TaskProgressServiceArgs(int percentageComplete, string message)
        {
            System.Diagnostics.Debug.Assert(percentageComplete >= 0 && percentageComplete <= 100, String.Format("Value {0} must be in the range 0..100 inclusive.", percentageComplete));

            PercentageComplete = percentageComplete;
            Message = message;
        }

        #endregion
    }
}
