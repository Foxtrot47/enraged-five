﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using RSG.Base;
using Workbench.AddIn;
using Workbench.AddIn.Services;

namespace Workbench.Services
{

    /// <summary>
    /// Application arguments service implementation.
    /// </summary>
    [ExportExtension(Workbench.AddIn.CompositionPoints.ApplicationArgumentsService,
        typeof(IApplicationArgumentsService))]
    internal class ApplicationArgumentsService : IApplicationArgumentsService
    {
        #region Events
        /// <summary>
        /// Event raised whenever the system wants the startup arguments processed.
        /// There may be multiple handlers for this in various components
        /// around the system.
        /// </summary>
        public event EventHandler<ArgumentsEventArgs> ProcessStartupArguments;

        /// <summary>
        /// Event raised whenever new arguments are injected into the Workbench
        /// application (if single instance set).
        /// </summary>
        public event EventHandler<ArgumentsEventArgs> InjectedArgumentsChanged;
        #endregion // Events

        #region Properties and Associated Member Data
        /// <summary>
        /// Dictionary hash of application arguments.
        /// </summary>
        public Dictionary<String, Object> StartupArguments
        {
            get { return m_StartupArguments; }
        }
        private static Dictionary<String, Object> m_StartupArguments;
        
        /// <summary>
        /// Array of application trailing arguments.
        /// </summary>
        public String[] StartupTrailingArguments 
        {
            get { return m_StartupTrailingArguments; }
        }
        private static String[] m_StartupTrailingArguments;

        /// <summary>
        /// Latest injected arguments from another instance (if single instance set).
        /// </summary>
        public Dictionary<String, Object> InjectedArguments
        {
            get;
            protected set;
        }

        /// <summary>
        /// Latest injected trailing arguments from another instance (if single instance set).
        /// </summary>
        public String[] InjectedTrailingArguments
        {
            get;
            protected set;
        }
        #endregion // Properties and Associated Member Data

        #region Private Member Data
        /// <summary>
        /// Static argument dictionary and array lock object.
        /// </summary>
        private static Object ms_ArgumentsLock = new Object();
        #endregion // Private Member Data

        #region Controller Methods
        /// <summary>
        /// External trigger for application to notify other systems to process
        /// the startup arguments (via the ProcessStartupArguments event).
        /// </summary>
        public void TriggerProcessStartupArguments()
        {
            // If we have startup arguments and we have any subscribed event 
            // handlers then send out our notification.
            if ((null != ProcessStartupArguments) &&
                (this.StartupArguments.Count > 0 || this.StartupTrailingArguments.Length > 0))
            {
                ArgumentsEventArgs e = new ArgumentsEventArgs(this.StartupArguments,
                    this.StartupTrailingArguments);
                ProcessStartupArguments(this, e);
            }
        }

        /// <summary>
        /// Inject arguments from another instance.
        /// </summary>
        /// <param name="args"></param>
        public void InjectArguments(String[] injectedArgs)
        {
            PrivateCommandLineParser parser = new PrivateCommandLineParser(injectedArgs);
            this.InjectedArguments = new Dictionary<String, Object>(parser.Parameters.Count);
            foreach (DictionaryEntry entry in parser.Parameters)
            {
                this.InjectedArguments.Add(entry.Key as String, entry.Value);
            }
            this.InjectedTrailingArguments = new String[parser.TrailingArguments.Count];
            parser.TrailingArguments.CopyTo(this.InjectedTrailingArguments);

            if (null != InjectedArgumentsChanged)
            {
                ArgumentsEventArgs e = new ArgumentsEventArgs(
                    this.InjectedArguments, this.InjectedTrailingArguments);
                InjectedArgumentsChanged(this, e);
            }
        }
        #endregion // Controller Methods

        #region Static Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="arguments"></param>
        internal static void SetArguments(String args)
        {
            lock (ms_ArgumentsLock)
            {
                PrivateCommandLineParser parser = new PrivateCommandLineParser(args);
                m_StartupArguments = new Dictionary<String, Object>(parser.Parameters.Count);
                foreach (DictionaryEntry entry in parser.Parameters)
                {
                    m_StartupArguments.Add(entry.Key as String, entry.Value);
                }

                m_StartupTrailingArguments = new String[parser.TrailingArguments.Count];
                parser.TrailingArguments.CopyTo(m_StartupTrailingArguments);
            }
        }
        #endregion // Static Controller Methods

        public class PrivateCommandLineParser
        {
            #region Properties
            /// <summary>
            /// Read-only access to the StringDictionary of arguments parsed
            /// </summary>
            public StringDictionary Parameters
            {
                get { return m_Parameters; }
            }

            /// <summary>
            /// Read-only access to the remainder non-prefixed arguments (e.g. list of files)
            /// </summary>
            public List<String> TrailingArguments
            {
                get { return m_asTrailingArguments; }
            }

            /// <summary>
            /// Read-only access to the Arguments used to initialise object
            /// </summary>
            public String[] Arguments
            {
                get { return m_asArguments; }
            }
            #endregion Properties

            #region Member Data
            private StringDictionary m_Parameters;
            private List<String> m_asTrailingArguments;
            private String[] m_asArguments;
            #endregion Member Data

            #region Constructors
            /// <summary>
            /// 
            /// </summary>
            /// <param name="asArgStrings">Array of strings representing each command line argument</param>
            public PrivateCommandLineParser(string[] asArgStrings)
            {
                m_asArguments = asArgStrings;
                Process();
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="sArgString">String representing all command line arguments</param>
            public PrivateCommandLineParser(string sArgString)
            {
                char[] charSeps = new char[] { ' ' };
                m_asArguments = sArgString.Split(charSeps);
                Process();
            }
            #endregion

            #region Public Methods
            /// <summary>
            /// 
            /// </summary>
            /// <param name="sParam"></param>
            /// <returns></returns>
            public string this[string sParam]
            {
                get { return (Parameters[sParam]); }
            }
            #endregion Public Methods

            #region Private Methods
            /// <summary>
            /// 
            /// </summary>
            private void Process()
            {
                m_Parameters = new StringDictionary();
                m_asTrailingArguments = new List<String>();
                Regex Spliter = new Regex(@"^-{1,2}|^/|=",
                                          RegexOptions.IgnoreCase | RegexOptions.Compiled);
                Regex Remover = new Regex(@"^['""]?(.*?)['""]?$",
                                          RegexOptions.IgnoreCase | RegexOptions.Compiled);
                string Parameter = null;
                string[] Parts;

                foreach (string sArg in m_asArguments)
                {
                    // Look for new parameter characters
                    Parts = Spliter.Split(sArg, 3);

                    switch (Parts.Length)
                    {
                        case 1:
                            // Found a value for the last parameter found
                            // using a space separator
                            if (null != Parameter)
                            {
                                if (!Parameters.ContainsKey(Parameter))
                                {
                                    Parts[0] = Remover.Replace(Parts[0], "$1");
                                    Parameters.Add(Parameter, Parts[0]);
                                }
                                Parameter = null;
                            }
                            else
                            {
                                // No waiting parameter so we assume its for our trailing
                                // args list.
                                if (!String.IsNullOrWhiteSpace(Parts[0]))
                                    m_asTrailingArguments.Add(Parts[0]);
                            }
                            break;
                        case 2:
                            // Found just a parameter
                            if (null != Parameter)
                            {
                                // Last parameter is still waiting, with no 
                                // associated value so set it to "true"
                                if (!Parameters.ContainsKey(Parameter))
                                    Parameters.Add(Parameter, "true");
                            }
                            Parameter = Parts[1];
                            break;
                        case 3:
                            // Parameter with enclosed value
                            if (null != Parameter)
                            {
                                // Last parameter is still waiting, with no
                                // associated value so set it to "true"
                                if (!Parameters.ContainsKey(Parameter))
                                    Parameters.Add(Parameter, "true");
                            }
                            Parameter = Parts[1];

                            // Remove possible enclosing characters (",')
                            if (!Parameters.ContainsKey(Parameter))
                            {
                                Parts[2] = Remover.Replace(Parts[2], "$1");
                                Parameters.Add(Parameter, Parts[2]);
                            }

                            Parameter = null;
                            break;
                    }
                } // End of switch
                // Mop up the case that there is a parameter still waiting
                // Has no associated value so set it to "true"
                if (null != Parameter)
                {
                    if (!Parameters.ContainsKey(Parameter))
                        Parameters.Add(Parameter, "true");
                }
            }
            #endregion Private Methods
        }
    }

} // Workbench.Services
