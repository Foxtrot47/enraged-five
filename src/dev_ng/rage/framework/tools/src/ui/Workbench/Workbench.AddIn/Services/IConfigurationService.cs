﻿using System;
using RSG.Base.ConfigParser;
using RSG.Base.Configuration;
using RSG.Base.Configuration.Automation;
using RSG.Base.Configuration.Bugstar;
using RSG.Base.Configuration.Reports;

namespace Workbench.AddIn.Services
{

    /// <summary>
    /// Workbench Configuration Service; for all plugin configuration data needs.
    /// </summary>
    /// Please use this service rather than creating your own configuration
    /// data objects; it saves parsing configuration data multiple times.
    /// 
    public interface IConfigurationService
    {
        #region Properties
        /// <summary>
        /// Game and tools configuration data (OLD, will become obsolete).
        /// </summary>
        [Obsolete("Use IConfigurationService.Config instead.")]
        ConfigGameView GameConfig { get; }

        /// <summary>
        /// Game and tools configuration data.
        /// </summary>
        IConfig Config { get; }

        /// <summary>
        /// Reports configuration data.
        /// </summary>
        IReportsConfig ReportsConfig { get; }

        /// <summary>
        /// Bugstar configuration data.
        /// </summary>
        IBugstarConfig BugstarConfig { get; }

        /// <summary>
        /// Automation Core Services configuration data.
        /// </summary>
        IAutomationCoreServices AutomationCoreServices { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Returns next new file filename (basename).
        /// </summary>
        /// <returns></returns>
        String GetNewFilename();
        #endregion // Methods
    }

} // Workbench.AddIn.Services
