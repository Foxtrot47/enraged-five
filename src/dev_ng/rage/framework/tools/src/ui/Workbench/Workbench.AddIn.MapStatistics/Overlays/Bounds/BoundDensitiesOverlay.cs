﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using RSG.Base.Collections;
using RSG.Bounds;
using RSG.Model.Common;
using RSG.Base.Windows.Helpers;
using System.Windows.Media;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using System.Threading;
using RSG.Model.Common.Util;
using RSG.Base.Windows.Dialogs;
using RSG.Base.Tasks;
using RSG.Model.Common.Extensions;
using RSG.Model.Common.Map;
using MapViewport.AddIn;
using RSG.Base.Attributes;
using System.Reflection;
using System.IO;
using Workbench.AddIn.Services.Model;

namespace Workbench.AddIn.MapStatistics.Overlays.Bounds
{
    /// <summary>
    /// 
    /// </summary>
    public class CollisionDensityInfo
    {
        [FriendlyName("Section Name")]
        public String SectionName { get; set; }

        [FriendlyName("Density")]
        public float Density { get; set; }

        [FriendlyName("Area")]
        public float SectionArea { get; set; }

        [FriendlyName("Weapon Collision")]
        public ulong WeaponCollision { get; set; }

        [FriendlyName("Weapon Mover Collision")]
        public ulong WeaponMoverCollision { get; set; }

        [FriendlyName("Mover Collision")]
        public ulong MoverCollision { get; set; }

        [FriendlyName("Horse Collision")]
        public ulong HorseCollision { get; set; }

        [FriendlyName("Horse Vehicle Collision")]
        public ulong HorseVehicleCollision { get; set; }

        [FriendlyName("Vehicle Collision")]
        public ulong VehicleCollision { get; set; }

        [FriendlyName("Cover Collision")]
        public ulong CoverCollision { get; set; }

        [FriendlyName("Foliage Collision")]
        public ulong FoliageCollision { get; set; }

        [FriendlyName("River Collision")]
        public ulong RiverCollision { get; set; }

        [FriendlyName("Stair Slope Collision")]
        public ulong StairSlopeCollision { get; set; }

        [FriendlyName("Other Collision")]
        public ulong OtherCollision { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(ExtensionPoints.BoundsStatisticsOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class BoundDensitiesOverlay : LevelDependentViewportOverlay
    {
        #region Constants
        private const string NAME = "Bound Densities";
        private const string DESC = "Shows an overlay of bounds densities per map section.";
        #endregion

        #region MEF Imports
        /// <summary>
        /// A reference to view model for the map viewport, used to get the currently selected geometry
        /// </summary>
        [ImportExtension(Viewport.AddIn.CompositionPoints.MapViewport, typeof(Viewport.AddIn.IMapViewport))]
        Lazy<Viewport.AddIn.IMapViewport> MapViewport { get; set; }
        #endregion // MEF Imports

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<CollisionTypeHelper> CollisionTypes
        {
            get { return m_collisionTypes; }
            set
            {
                SetPropertyValue(value, () => CollisionTypes,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_collisionTypes = (ObservableCollection<CollisionTypeHelper>)newValue;
                        }
                ));
            }
        }
        private ObservableCollection<CollisionTypeHelper> m_collisionTypes;

        /// <summary>
        /// Relay command for refreshing the overlay
        /// </summary>
        public RelayCommand RefreshOverlayCommand
        {
            get
            {
                if (m_refreshOverlayCommand == null)
                {
                    m_refreshOverlayCommand = new RelayCommand(param => RefreshOverlay());
                }

                return m_refreshOverlayCommand;
            }
        }
        private RelayCommand m_refreshOverlayCommand;

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand SaveToCsvCommand
        {
            get
            {
                if (m_saveToCsvCommand == null)
                {
                    m_saveToCsvCommand = new RelayCommand(param => SaveDataToCsv());
                }

                return m_saveToCsvCommand;
            }
        }
        private RelayCommand m_saveToCsvCommand;

        /// <summary>
        /// The name of the selected section
        /// </summary>
        public string SelectedSectionName
        {
            get { return m_selectedSectionName; }
            set
            {
                SetPropertyValue(value, () => SelectedSectionName,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_selectedSectionName = (string)newValue;
                        }
                ));
            }
        }
        private string m_selectedSectionName;

        /// <summary>
        /// Density information for the selected section
        /// </summary>
        public CollisionDensityInfo SelectedSectionPolyInfo
        {
            get { return m_selectedSectionDensityInfo; }
            set
            {
                SetPropertyValue(value, () => SelectedSectionPolyInfo,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_selectedSectionDensityInfo = (CollisionDensityInfo)newValue;
                        }
                ));
            }
        }
        private CollisionDensityInfo m_selectedSectionDensityInfo;

        /// <summary>
        /// 
        /// </summary>
        public bool OnlyWorstCases
        {
            get { return m_onlyWorstCases; }
            set
            {
                SetPropertyValue(value, () => OnlyWorstCases,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_onlyWorstCases = (Boolean)newValue;
                        }
                ));
            }
        }
        private bool m_onlyWorstCases;

        /// <summary>
        /// 
        /// </summary>
        public int WorstCaseCount
        {
            get { return m_worstCaseCount; }
            set
            {
                SetPropertyValue(value, () => WorstCaseCount,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_worstCaseCount = (int)newValue;
                        }
                ));
            }
        }
        private int m_worstCaseCount;

        /// <summary>
        /// 
        /// </summary>
        public int MaximumGeometryCount
        {
            get { return m_maximumGeometryCount; }
            set
            {
                SetPropertyValue(value, () => MaximumGeometryCount,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_maximumGeometryCount = (int)newValue;
                        }
                ));
            }
        }
        private int m_maximumGeometryCount;

        /// <summary>
        /// 
        /// </summary>
        public bool OnlyGreaterThan
        {
            get { return m_onlyGreaterThan; }
            set
            {
                SetPropertyValue(value, () => OnlyGreaterThan,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_onlyGreaterThan = (Boolean)newValue;
                        }
                ));
            }
        }
        private bool m_onlyGreaterThan;

        /// <summary>
        /// 
        /// </summary>
        public float GreaterThanLimit
        {
            get { return m_greaterThanLimit; }
            set
            {
                SetPropertyValue(value, () => GreaterThanLimit,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_greaterThanLimit = (float)newValue;
                        }
                ));
            }
        }
        private float m_greaterThanLimit;

        /// <summary>
        /// 
        /// </summary>
        public bool OnlyGreaterThanAverage
        {
            get { return m_onlyGreaterThanAverage; }
            set
            {
                SetPropertyValue(value, () => OnlyGreaterThanAverage,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_onlyGreaterThanAverage = (Boolean)newValue;
                        }
                ));
            }
        }
        private bool m_onlyGreaterThanAverage;

        /// <summary>
        /// 
        /// </summary>
        public double Average
        {
            get { return m_average; }
            set
            {
                SetPropertyValue(value, () => Average,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_average = (double)newValue;
                        }
                ));
            }
        }
        private double m_average;

        /// <summary>
        /// 
        /// </summary>
        private Dictionary<IMapSection, CollisionDensityInfo> SectionPolyDetails
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        private Dictionary<IMapSection, Viewport2DShape> SectionGeometry
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default Constructor
        /// </summary>
        public BoundDensitiesOverlay()
            : base(NAME, DESC)
        {
            SectionPolyDetails = new Dictionary<IMapSection, CollisionDensityInfo>();
            SectionGeometry = new Dictionary<IMapSection, Viewport2DShape>();

            // Setup the collision types collection
            m_collisionTypes = new ObservableCollection<CollisionTypeHelper>();

            foreach (BNDFile.CollisionType type in Enum.GetValues(typeof(BNDFile.CollisionType)))
            {
                if (type != BNDFile.CollisionType.Default)
                {
                    CollisionTypes.Add(new CollisionTypeHelper(type, true));
                }
            }

            OnlyWorstCases = false;
            WorstCaseCount = 1;
            MaximumGeometryCount = 1;
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// Gets called when the user wants to view this overlay
        /// </summary>
        public override void Activated()
        {
            base.Activated();
            RefreshOverlay();
            MapViewport.Value.SelectedOverlayGeometry.CollectionChanged += SelectedOverlayGeometryChanged;
        }

        /// <summary>
        /// Gets called when the user turns off this overlay
        /// </summary>
        public override void Deactivated()
        {
            MapViewport.Value.SelectedOverlayGeometry.CollectionChanged -= SelectedOverlayGeometryChanged;
            Geometry.Clear();
            SectionPolyDetails.Clear();
            SectionGeometry.Clear();
            base.Deactivated();
        }

        /// <summary>
        /// Gets the control that should be shown in the overlay details panel
        /// </summary>
        public override System.Windows.Controls.Control GetOverlayControl()
        {
            return new BoundDensitiesOverlayView();
        }
        #endregion // Overrides

        #region Event Callbacks
        /// <summary>
        /// Gets called when map geometry selection changes
        /// </summary>
        private void SelectedOverlayGeometryChanged(Object sender, NotifyCollectionChangedEventArgs e)
        {
            // Display details for the first item in the selected geometry
            IMapSection selectedSection = MapViewport.Value.SelectedOverlayGeometry.FirstOrDefault() as IMapSection;

            if (selectedSection == null)
            {
                SelectedSectionName = "";
                SelectedSectionPolyInfo = new CollisionDensityInfo();
            }
            else
            {
                SelectedSectionName = selectedSection.Name;
                SelectedSectionPolyInfo = SectionPolyDetails[selectedSection];
            }
        }
        #endregion // Event Callbacks

        #region Private Methods
        /// <summary>
        /// Function that is called when the user hits "Refresh Overlay" in the overlay details view
        /// </summary>
        private void RefreshOverlay()
        {
            // Gather and cache the information
            if (!GatherCollisionPolyInfo())
            {
                return;
            }

            // Calculate the densities we are going to be displaying
            float minDensity, maxDensity;
            CalculateCollisionDensities(out minDensity, out maxDensity);

            // Generate/Update the overlay geometry
            GenerateOverlayGeometry(minDensity, maxDensity);
        }

        /// <summary>
        /// 
        /// </summary>
        private bool GatherCollisionPolyInfo()
        {
            CancellationTokenSource cts = new CancellationTokenSource();
            LevelTaskContext context = new LevelTaskContext(cts.Token, LevelBrowserProxy.Value.SelectedLevel);
            ITask loadDataTask = new ActionTask("Load Data", (ctx, progress) => EnsureDataLoaded((LevelTaskContext)ctx, progress));

            TaskExecutionDialog dialog = new TaskExecutionDialog();
            TaskExecutionViewModel vm = new TaskExecutionViewModel("Loading Map Data", loadDataTask, cts, context);
            vm.AutoCloseOnCompletion = true;
            dialog.DataContext = vm;
            Nullable<bool> selectionResult = dialog.ShowDialog();
            if (false == selectionResult)
            {
                return false;
            }

            // Extract the poly count data
            foreach (IMapSection section in LevelBrowserProxy.Value.SelectedLevel.GetMapHierarchy().AllSections)
            {
                // Check if we need to get information for this section
                if (section.Type == SectionType.Container && !SectionPolyDetails.ContainsKey(section))
                {
                    CollisionDensityInfo polyInfo = new CollisionDensityInfo();
                    polyInfo.SectionName = section.Name;
                    polyInfo.SectionArea = section.Area;
                    GatherCollisionData(section, ref polyInfo);

                    // Add it to the map
                    SectionPolyDetails.Add(section, polyInfo);
                }
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        private void GatherCollisionData(IMapSection section, ref CollisionDensityInfo data)
        {
            foreach (IGrouping<BNDFile.CollisionType, CollisionInfo> group in section.CollisionPolygonCountBreakdown.GroupBy(item => item.CollisionFlags))
            {
                long count = group.Sum(item => item.Count);

                if (group.Key.HasFlag(BNDFile.CollisionType.Mover))
                {
                    if (group.Key.HasFlag(BNDFile.CollisionType.Weapons))
                    {
                        data.WeaponMoverCollision += (ulong)count;
                    }
                    else
                    {
                        data.MoverCollision += (ulong)count;
                    }
                }
                else if (group.Key == (BNDFile.CollisionType.Horse | BNDFile.CollisionType.Vehicle))
                {
                    data.HorseVehicleCollision += (ulong)count;
                }
                else if (group.Key == BNDFile.CollisionType.Weapons)
                {
                    data.WeaponCollision += (ulong)count;
                }
                else if (group.Key == BNDFile.CollisionType.Horse)
                {
                    data.HorseCollision += (ulong)count;
                }
                else if (group.Key == BNDFile.CollisionType.Vehicle)
                {
                    data.VehicleCollision += (ulong)count;
                }
                else if (group.Key == BNDFile.CollisionType.Cover)
                {
                    data.CoverCollision += (ulong)count;
                }
                else if (group.Key == BNDFile.CollisionType.Foliage)
                {
                    data.FoliageCollision += (ulong)count;
                }
                else if (group.Key == BNDFile.CollisionType.River)
                {
                    data.RiverCollision += (ulong)count;
                }
                else if (group.Key == BNDFile.CollisionType.StairSlope)
                {
                    data.StairSlopeCollision += (ulong)count;
                }
                else
                {
                    data.OtherCollision += (ulong)count;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="progress"></param>
        private void EnsureDataLoaded(LevelTaskContext context, IProgress<TaskProgress> progress)
        {
            float increment = 1.0f / context.MapHierarchy.AllSections.Count();

            // Load all the map section data
            foreach (IMapSection section in context.MapHierarchy.AllSections)
            {
                context.Token.ThrowIfCancellationRequested();

                // Update the progress information
                string message = String.Format("Loading {0}", section.Name);
                progress.Report(new TaskProgress(increment, true, message));

                section.LoadStats(new StreamableStat[] { StreamableMapSectionStat.CollisionPolygonCountBreakdown });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="values"></param>
        /// <param name="minDensity"></param>
        /// <param name="maxDensity"></param>
        private void CalculateCollisionDensities(out float minDensity, out float maxDensity)
        {
            // Helper map
            Dictionary<BNDFile.CollisionType, bool> collisionTypes = new Dictionary<BNDFile.CollisionType, bool>();
            foreach (CollisionTypeHelper helper in CollisionTypes)
            {
                collisionTypes[helper.Type] = helper.Enabled;
            }

            // Generate the density information
            foreach (IMapSection section in LevelBrowserProxy.Value.SelectedLevel.GetMapHierarchy().AllSections)
            {
                if (section.Type == SectionType.Container && section.Area != 0)
                {
                    CollisionDensityInfo info = SectionPolyDetails[section];
                    if (info != null)
                    {
                        BNDFile.CollisionType collisionFlags = 0;
                        foreach (KeyValuePair<BNDFile.CollisionType, bool> pair in collisionTypes)
                        {
                            if (pair.Value)
                            {
                                collisionFlags |= pair.Key;
                            }
                        }

                        // Calculate the density and store it
                        info.Density = 0.0f;
                        CollisionInfo sectionColInfo = section.CollisionPolygonCountBreakdown.FirstOrDefault(item => item.CollisionFlags == collisionFlags);
                        if (sectionColInfo != null)
                        {
                            info.Density = sectionColInfo.Count / section.Area;
                        }
                    }
                }
            }

            minDensity = SectionPolyDetails.Values.Min(item => item.Density);
            maxDensity = SectionPolyDetails.Values.Max(item => item.Density);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="values"></param>
        /// <param name="minDensity"></param>
        /// <param name="maxDensity"></param>
        private void GenerateOverlayGeometry(float minDensity, float maxDensity)
        {
            // Calculate the average
            double totalDensity = 0.0f;
            List<float> allDensities = new List<float>();
            foreach (KeyValuePair<IMapSection, CollisionDensityInfo> pair in SectionPolyDetails)
            {
                totalDensity += pair.Value.Density;
                allDensities.Add(pair.Value.Density);
            }
            Average = totalDensity / SectionPolyDetails.Count;

            // Determine what the cut off point is (for worst 'n' case filtering)
            allDensities.Sort();
            allDensities.Reverse();
            float worstCaseLimit = (WorstCaseCount > allDensities.Count ? allDensities.Last() : allDensities[WorstCaseCount]);

            // Calculate the range of the densities (for the colour computation)
            double densityRange = maxDensity - minDensity;

            // Render the geometry (based on the user's options)
            Geometry.Clear();
			Geometry.BeginUpdate();

            foreach (KeyValuePair<IMapSection, CollisionDensityInfo> pair in SectionPolyDetails)
            {
                byte red = (byte)((byte)128 * ((pair.Value.Density - minDensity) / densityRange));
                byte green = (byte)(128 + (-128 * ((pair.Value.Density - minDensity) / densityRange)));
                Color colour = Color.FromArgb(255, red, green, 0);

                Viewport2DShape sectionGeometry = null;

                // Check if we can reuse a piece of geometry for rendering this section's density
                if (!SectionGeometry.ContainsKey(pair.Key))
                {
                    if (pair.Key.VectorMapPoints == null)
                    {
                        continue;
                    }

                    sectionGeometry = new Viewport2DShape(etCoordSpace.World, "Filled shape for section", pair.Key.VectorMapPoints.ToArray(), Colors.Black, 1, colour);
                    sectionGeometry.PickData = pair.Key;

                    // Keep track of the geometry
                    SectionGeometry.Add(pair.Key, sectionGeometry);
                }
                else
                {
                    sectionGeometry = SectionGeometry[pair.Key];
                }

                // Update the geometry's details
                sectionGeometry.UserText = pair.Value.Density.ToString();
                sectionGeometry.FillColour = colour;

                // Check if we should add this piece of geometry (based on the overlay options)
                bool renderGeom = true;
                renderGeom &= (!OnlyGreaterThanAverage || pair.Value.Density > Average);
                renderGeom &= (!OnlyGreaterThan || pair.Value.Density > GreaterThanLimit);
                renderGeom &= (!OnlyWorstCases || pair.Value.Density >= worstCaseLimit);

                if (renderGeom)
                {
                    Geometry.Add(sectionGeometry);
                }

                if (Geometry.Count >= WorstCaseCount && OnlyWorstCases)
                {
                    break;
                }
            }

            Geometry.EndUpdate();

            MaximumGeometryCount = SectionPolyDetails.Count;
        }

        /// <summary>
        /// Saves the data present on the overlay to a csv file.
        /// </summary>
        private void SaveDataToCsv()
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.Filter = "CSV (Comma delimited)|*.csv";
            Nullable<bool> result = dlg.ShowDialog();
            if (false == result)
                return;

            using (Stream fileStream = new FileStream(dlg.FileName, FileMode.Create))
            {
                Stream csvStream = GenerateCSVStream(SectionPolyDetails.Values);
                csvStream.Position = 0;
                csvStream.CopyTo(fileStream);
            }
        }


        /// <summary>
        /// Creates a stream of CSV data from a list of items, optionally outputting a header.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="outputHeader"></param>
        /// <returns></returns>
        protected Stream GenerateCSVStream<T>(IEnumerable<T> items, bool includeHeader = true) where T : class
        {
            Stream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);

            // Check if we should output a header
            if (includeHeader)
            {
                OutputCsvHeader<T>(writer);
            }

            // Output each line
            foreach (T item in items)
            {
                OutputCsvLine<T>(writer, item);
            }

            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        /// <summary>
        /// Outputs a header line that would be included in a CSV file by iterating over all properties in the passed in type
        /// and using either the properties associated friendly name attribute or the properties name itself.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="writer"></param>
        private void OutputCsvHeader<T>(StreamWriter writer) where T : class
        {
            // Get the type
            Type type = typeof(T);
            StringBuilder sb = new StringBuilder();

            // Loop over all of the class's properties.
            foreach (PropertyInfo info in type.GetProperties())
            {
                if (sb.Length > 0)
                {
                    sb.Append(",");
                }

                // Use the friendly name if one exists, otherwise use the property's name
                FriendlyNameAttribute[] attributes = info.GetCustomAttributes(typeof(FriendlyNameAttribute), false) as FriendlyNameAttribute[];
                if (attributes.Length > 0)
                {
                    sb.Append(attributes[0].FriendlyName);
                }
                else
                {
                    sb.Append(info.Name);
                }
            }

            writer.WriteLine(sb.ToString());
        }

        /// <summary>
        /// Outputs a line of csv for the passed in items.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="writer"></param>
        /// <param name="values"></param>
        private void OutputCsvLine<T>(StreamWriter writer, T item) where T : class
        {
            // Get the type
            Type type = typeof(T);

            // Construct the line's data from the values passed in
            StringBuilder sb = new StringBuilder();

            // Loop over all of the class's properties.
            foreach (PropertyInfo info in type.GetProperties())
            {
                object value = info.GetValue(item, null);
                if (value != null)
                {
                    sb.Append(value);
                }
                sb.Append(",");
            }

            writer.WriteLine(sb.ToString().TrimEnd(new char[] { '.' }));
        }
        #endregion // Private Methods
    } // BoundDensityOverlay
} // Workbench.AddIn.MapStatistics.Overlays.Bounds
