﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ContentBrowser.ViewModel
{
    /// <summary>
    /// Lowest level interface for any class that wants to
    /// represent a object in a browsers items source that is
    /// a file asset
    /// </summary>
    public interface IFileAssetViewModel : IAssetViewModel
    {
        #region Properties
        
        /// <summary>
        /// The main filename property
        /// </summary>
        String Filename { get; }

        #endregion // Properties
    } // IFileAssetViewModel
} // ContentBrowser.ViewModel
