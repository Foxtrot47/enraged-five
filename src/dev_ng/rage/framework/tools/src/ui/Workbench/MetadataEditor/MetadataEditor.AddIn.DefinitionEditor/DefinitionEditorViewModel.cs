﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Windows.Media;
using RSG.Base.Editor;
using RSG.Metadata.Parser;
using RSG.Metadata.Model;
using Workbench.AddIn;
using Workbench.AddIn.UI.Layout;
using MetadataEditor.AddIn.View;
using MetadataEditor.AddIn.DefinitionEditor.ViewModel;

namespace MetadataEditor.AddIn.DefinitionEditor
{

    /// <summary>
    /// 
    /// </summary>
    public class DefinitionEditorViewModel : StructureViewModel
    {
        #region Constants
        public const String DOCUMENT_NAME = "DefinitionEditorViewModel";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        public DefinitionEditorViewModel(RSG.Metadata.Parser.Structure m)
            : base(m)
        {
        }
        #endregion // Constructor(s)
    }
} // MetadataEditor.AddIn.DefinitionEditorViewModel
