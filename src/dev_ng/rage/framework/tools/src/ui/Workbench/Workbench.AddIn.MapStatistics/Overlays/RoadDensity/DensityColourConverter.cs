﻿using System;
using System.Windows.Data;
using System.Windows.Media;

namespace Workbench.AddIn.MapStatistics.Overlays.RoadDensity
{
    /// <summary>
    /// This converter ensures that we can see the numbers in the traffic density colour swatches.
    /// </summary>
    class DensityColourConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int density = int.Parse(value.ToString());
            if (density > 12)
            {
                return Brushes.White;
            }

            return Brushes.Black;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
