﻿using MetadataEditor.AddIn.MetaEditor.ViewModel;
using RSG.Metadata.Data;

namespace MetadataEditor.AddIn.MetaEditor.GridViewModel
{
    /// <summary>
    /// The view model that wraps around a  <see cref="Matrix33Tunable"/> object. Used for
    /// the grid view.
    /// </summary>
    public class Matrix33TunableViewModel : GridTunableViewModel
    {
        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="tunable"></param>
        public Matrix33TunableViewModel(GridTunableViewModel parent, Matrix33TunableBase m, MetaFileViewModel root)
            : base(parent, m, root)
        {
        }
        #endregion // Constructor
    } // Matrix33TunableViewModel

    /// <summary>
    /// The view model that wraps around a  <see cref="Matrix33Tunable"/> object. Used for
    /// the grid view.
    /// </summary>
    public class Matrix34TunableViewModel : GridTunableViewModel
    {
        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="tunable"></param>
        public Matrix34TunableViewModel(GridTunableViewModel parent, Matrix34TunableBase m, MetaFileViewModel root)
            : base(parent, m, root)
        {
        }
        #endregion // Constructor
    } // Matrix34TunableViewModel

    /// <summary>
    /// The view model that wraps around a  <see cref="Matrix44Tunable"/> object. Used for
    /// the grid view.
    /// </summary>
    public class Matrix44TunableViewModel : GridTunableViewModel
    {
        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="tunable"></param>
        public Matrix44TunableViewModel(GridTunableViewModel parent, Matrix44TunableBase m, MetaFileViewModel root)
            : base(parent, m, root)
        {
        }
        #endregion // Constructor
    } // Matrix44TunableViewModel
}
