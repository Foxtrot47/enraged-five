﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Report;
using RSG.Base.ConfigParser;
using Workbench.AddIn.Services;
using System.ComponentModel;
using RSG.Base.Tasks;

namespace Workbench.AddIn.MapDebugging.Reports
{
    /// <summary>
    /// Exported workbench version of the script model enums report.
    /// Adds perforce sync requests prior to running the report.
    /// </summary>
    [ExportExtension(ExtensionPoints.Report, typeof(IReport))]
    public class ScriptModelEnumsReport : RSG.Model.Report.Reports.ScriptReports.ScriptModelEnumsReport
    {
        #region MEF Imports
        /// <summary>
        /// The perforce sync service
        /// </summary>
        [Workbench.AddIn.ImportExtension(Workbench.AddIn.CompositionPoints.PerforceSyncService, typeof(Workbench.AddIn.Services.IPerforceSyncService))]
        private Lazy<Workbench.AddIn.Services.IPerforceSyncService> PerforceSyncService { get; set; }

        /// <summary>
        /// Progress service for displaying while gathering the data
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ProgressService, typeof(Workbench.AddIn.Services.IProgressService))]
        private IProgressService ProgressService { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(IConfigurationService))]
        private IConfigurationService Config { get; set; }
        #endregion // MEF Imports

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ScriptModelEnumsReport()
            : base()
        {
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Wrap the base classes generate function to prompt the user to sync the required files.
        /// </summary>
        protected override void GenerateReport(DynamicReportContext context, IProgress<TaskProgress> progress)
        {
            List<String> perforceFiles = new List<String>();
            perforceFiles.Add(context.GameView.ScriptDir + @"\...");

            int syncedFiles = 0;
            PerforceSyncService.Value.Show("You current have a number of script files that are out of date.\n" +
                                           "Would you like to grab latest now?\n\n" +
                                           "You can select which files to grab using the table below.\n",
                                           perforceFiles.ToArray(), ref syncedFiles, false, true);

            base.GenerateReport(context, progress);
        }
        #endregion // Controller Methods
    } // ScriptModelEnumsReport
}
