﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.SourceControl.Perforce;
using RSG.Model.Report.Reports.GameStatsReports.Tests;
using System.IO;

namespace Workbench.AddIn.GameStatistics.Reports.Automated.Cutscene
{
    public class AutomatedCutsceneReport : AutomatedTestReport
    {
        #region Constants
        /// <summary>
        /// Report description.
        /// </summary>
        private const string c_description = "Generates a report showing information for a single cut scene's metrics.";

        /// <summary>
        /// Directory where cached version of the p4 files are stored.
        /// </summary>
        private const string c_cacheDir = @"cache:\Reports\Automated\CutScenes";
        #endregion // Constants

        #region Private Member Data
        /// <summary>
        /// Place to save cached p4 files to.
        /// </summary>
        protected override string CacheDir
        {
            get
            {
                return c_cacheDir;
            }
        }

        private P4 perforceConnection;
        #endregion // Private Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="graph"></param>
        public AutomatedCutsceneReport(string depotFile, P4 perforceConnection)
            : base(Path.GetFileNameWithoutExtension(depotFile), c_description, depotFile, perforceConnection)
        {
            this.perforceConnection = perforceConnection;
            SmokeTests.Add(new FpsSmokeTest());
            SmokeTests.Add(new CpuSmokeTest());
            SmokeTests.Add(new StreamingMemorySmokeTest());
            SmokeTests.Add(new ScriptMemSmokeTest());
            SmokeTests.Add(new PedAndVehicleSmokeTest());
            SmokeTests.Add(new ThreadSmokeTest());
        }

        #endregion // Constructor(s)

        #region Methods
        protected override P4 GetPerforceConnection()
        {
            return this.perforceConnection;
        }
        #endregion
    }
}
