﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using RSG.Model.Common.Map;
using Workbench.AddIn.MapDebugging.Overlays;
using WidgetEditor.AddIn;

namespace Workbench.AddIn.MapDebugging.ToolTips
{
    /// <summary>
    /// Interaction logic for SpawnPointOverlayToolTip.xaml
    /// </summary>
    public partial class SpawnPointOverlayToolTipView : UserControl
    {
        private SpawnPointOverlayToolTipViewModel m_viewModel;

		#region Constructors
        /// <summary>
        /// 
        /// </summary>
        public SpawnPointOverlayToolTipView()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="selectedSpawnPoints"></param>
        /// <param name="selectedChainingGraphs"></param>
        public SpawnPointOverlayToolTipView(Lazy<IProxyService> proxyService, List<SpawnPointInstance> selectedSpawnPoints, List<IChainingGraph> selectedChainingGraphs)
        {
            InitializeComponent();

            m_viewModel = new SpawnPointOverlayToolTipViewModel(proxyService, selectedSpawnPoints, selectedChainingGraphs);
            this.DataContext = m_viewModel;
        }
		#endregion 
		
		#region Event Callbacks
        /// <summary>
        /// Notify the runtime, if active, to warp and select the specified spawn point.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WarpToSpawnPoint_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (m_viewModel != null)
            {
                MenuItem menuItem = (MenuItem)sender;
                ContextMenu contextMenu = (ContextMenu)menuItem.Parent;
                SpawnPointDisplay spawnPointDisplay = (SpawnPointDisplay)contextMenu.Tag;
                m_viewModel.WarpToSpawnPoint(spawnPointDisplay.Instance);
            }
        }

        /// <summary>
        /// Notify the runtime, if active, to warp and select the specified spawn point.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WarpToChainingNode_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (m_viewModel != null)
            {
                MenuItem menuItem = (MenuItem)sender;
                ContextMenu contextMenu = (ContextMenu)menuItem.Parent;
                ChainingNodeDisplay chainingNodeDisplay = (ChainingNodeDisplay)contextMenu.Tag;
                m_viewModel.WarpToChainingNode(chainingNodeDisplay.ChainingNode);
            }
        }

        /// <summary>
        /// Opens the context menu for the specified object.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SpawnPointTreeView_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            TreeView treeView= sender as TreeView;

            if (treeView.SelectedItem != null)
            {
                SpawnPointDisplay spawnPointDisplay = null;
                if (treeView.SelectedItem is SpawnPointDisplayChild)
                {
                    SpawnPointDisplayChild child = (SpawnPointDisplayChild)treeView.SelectedItem;
                    spawnPointDisplay = child.Parent;
                }
                else
                {
                    spawnPointDisplay = (SpawnPointDisplay)treeView.SelectedItem;
                }

                treeView.ContextMenu = treeView.Resources["ToolTipContextMenu"] as System.Windows.Controls.ContextMenu;
                treeView.ContextMenu.Tag = spawnPointDisplay;
                treeView.ContextMenu.IsOpen = true;
                e.Handled = true;
            }
        }

        /// <summary>
        /// Opens the context menu for the specified chaining node, if valid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChainingNodeTreeView_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            TreeView treeView = sender as TreeView;

            if (treeView.SelectedItem != null)
            {
                if (treeView.SelectedItem is ChainingNodeDisplay)
                {
                    ChainingNodeDisplay chainingNodeDisplay = (ChainingNodeDisplay)treeView.SelectedItem;

                    treeView.ContextMenu = treeView.Resources["ToolTipContextMenu"] as System.Windows.Controls.ContextMenu;
                    treeView.ContextMenu.Tag = chainingNodeDisplay;
                    treeView.ContextMenu.IsOpen = true;
                    e.Handled = true;
                }
            }
        }
		#endregion
    }
}
