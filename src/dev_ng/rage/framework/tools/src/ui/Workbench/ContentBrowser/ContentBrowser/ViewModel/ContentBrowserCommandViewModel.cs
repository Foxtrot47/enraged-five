﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ContentBrowser.AddIn;

namespace ContentBrowser.ViewModel
{
    /// <summary>
    /// 
    /// </summary>
    public class ContentBrowserCommandViewModel : RSG.Base.Editor.ViewModelBase
    {
        #region Members

        private IContentBrowserCommand m_command = null;

        #endregion // Members

        #region Properties

        /// <summary>
        /// The command model for this view model
        /// </summary>
        public IContentBrowserCommand Command
        {
            get { return m_command; }
            set
            {
                if (value == m_command)
                    return;

                SetPropertyValue(value, () => this.Command,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_command = (IContentBrowserCommand)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// The description for this command, can also
        /// be through of as a tooltip
        /// </summary>
        public String Description
        {
            get { return this.Command.Description; }
        }

        /// <summary>
        /// The image that will be displayed on the command
        /// </summary>
        public Object Image
        {
            get { return this.Command.Image; }
        }

        #endregion // Properties

        #region Constructor

        /// <summary>
        ///  Default constructor
        /// </summary>
        public ContentBrowserCommandViewModel(IContentBrowserCommand command)
        {
            if (command == null)
                throw new ArgumentException("Unable to create a browser command view model without a model", "command");

            this.Command = command;
        }

        #endregion // Constructor
    } // ContentBrowserCommandViewModel
} // ContentBrowser.ViewModel
