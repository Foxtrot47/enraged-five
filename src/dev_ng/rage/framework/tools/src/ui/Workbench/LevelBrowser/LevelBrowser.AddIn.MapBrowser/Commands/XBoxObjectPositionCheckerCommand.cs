﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn;
using Workbench.AddIn.Commands;

namespace LevelBrowser.AddIn.MapBrowser.Commands
{
    
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Extensions.MapSectionCommands, typeof(IWorkbenchCommand))]
    class XBoxObjectPositionCheckerCommand : ObjectPositionCheckerBase
    {
        #region Constants
        public static readonly String GUID = "F24849EE-9618-49EA-97B2-5A4BB89F5BC6";
        public static readonly String XBOX_BAT = "game_xenon_beta.bat";
        #endregion // Constants
        
        #region Constructor
        /// <summary>
        /// Default constructor.
        /// </summary>
        public XBoxObjectPositionCheckerCommand()
            : base(new Guid(GUID), "_Object Position Checker [XBOX]", XBOX_BAT)
        {
        }
        #endregion // Constructor
    }

} // LevelBrowser.AddIn.MapBrowser.Commands namespace
