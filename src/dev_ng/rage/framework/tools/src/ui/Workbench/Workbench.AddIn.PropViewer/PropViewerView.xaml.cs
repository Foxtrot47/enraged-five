﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Workbench.AddIn.Services;
using Workbench.AddIn.UI.Layout;

namespace Workbench.AddIn.PropViewer
{
    /// <summary>
    /// Interaction logic for PropViewerView.xaml
    /// </summary>
    public partial class PropViewerView : DocumentBase<PropViewerViewModel>
    {
        public PropViewerView()
            : base(Workbench.AddIn.PropViewer.Resources.Strings.PropViewer_Name, new PropViewerViewModel())
        {
            InitializeComponent();
        }

        public PropViewerView(RSG.Model.Map.MapSection section, IConfigurationService config)
            : base(Workbench.AddIn.PropViewer.Resources.Strings.PropViewer_Name, new PropViewerViewModel(section, config))
        {
            InitializeComponent();
            this.Title = section.Name + " Props";
            this.IsReadOnly = true;
            this.Path = section.SourceArtPath;
        }
    }
}
