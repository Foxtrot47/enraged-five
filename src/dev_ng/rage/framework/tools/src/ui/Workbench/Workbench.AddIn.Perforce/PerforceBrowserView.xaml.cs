﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using RSG.Base.Editor;
using RSG.Base.Logging;
using RSG.SourceControl.Perforce;
using Workbench.AddIn;
using Workbench.AddIn.Services;
using Workbench.AddIn.UI.Layout;
using RSG.SourceControl.Perforce;
using RSG.SourceControl.Perforce;

namespace Workbench.AddIn.Perforce
{
    /// <summary>
    /// Interaction logic for PerforceBrowserView.xaml
    /// </summary>
    internal partial class PerforceBrowserView : 
        ToolWindowBase<PerforceBrowserViewModel>
    {
        #region Constants
        /// <summary>
        /// 
        /// </summary>
        public static readonly Guid GUID =
            new Guid("27E43C57-8DAB-4B23-A254-3D206D517E11");
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="layoutManager"></param>
        /// <param name="vm"></param>
        public PerforceBrowserView(ILayoutManager layoutManager, PerforceBrowserViewModel vm)
            : base(Workbench.AddIn.Perforce.Resources.Strings.PerforceBrowser_Name, vm)
        {
            InitializeComponent();
            this.ID = GUID;
        }
        #endregion // Constructor(s)

        #region Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenPerforceConnection(Object sender, RoutedEventArgs e)
        {
            RSG.SourceControl.Perforce.UI.ConnectionDialog dlg =
                new RSG.SourceControl.Perforce.UI.ConnectionDialog();
            dlg.ShowDialog();
        }
        #endregion // Event Handlers
    }

} // Workbench.AddIn.Perforce namespace
