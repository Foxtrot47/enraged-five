﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Weapon;
using ContentBrowser.ViewModel.GridViewModels;
using System.IO;
using RSG.Model.Common;

namespace ContentBrowser.ViewModel.WeaponViewModels
{
    /// <summary>
    /// 
    /// </summary>
    internal class WeaponGridViewModel : GridViewModelBase
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// 
        /// </summary>
        public string ModelName
        {
            get;
            protected set;
        }

        /// <summary>
        /// 
        /// </summary>
        public String RenderFilename
        {
            get
            {
                if (!String.IsNullOrEmpty(AssetsDir))
                {
                    String filename = Path.Combine(AssetsDir, "weapons", "renders", String.Format("{0}.jpg", this.ModelName));
                    if (File.Exists(filename))
                    {
                        return filename;
                    }
                }
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private String AssetsDir
        {
            get;
            set;
        }
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public WeaponGridViewModel(IWeapon weapon, string assetsDir)
            : base(weapon)
        {
            ModelName = weapon.ModelName;
            AssetsDir = assetsDir;
        }
        #endregion // Constructor(s)
    } // WeaponGridViewModel
} // ContentBrowser.ViewModel.WeaponViewModels
