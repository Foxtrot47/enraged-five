﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Report;
using Report.AddIn;
using System.IO;
using System.Text.RegularExpressions;
using RSG.Base.Logging;

namespace Workbench.AddIn.GameStatistics.Reports.Automated.Missions
{
    /// <summary>
    /// Category for mission related reports based on the stats gathered from the automated tests that run on cruise.
    /// </summary>
    [ExportExtension(ExtensionPoints.AutomatedReport, typeof(IReport))]
    public class AutomatedMissionsCategory : ReportCategory
    {
        #region Constants
        private const String c_name = "Missions";
        private const String c_description = "Mission reports based on the stats gathered from the automated tests that run on cruise.";
        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// For retrieving the path to the playthrough groups in which the mission stats.xml files live.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(Workbench.AddIn.Services.IConfigurationService))]
        protected Workbench.AddIn.Services.IConfigurationService Config { get; set; }

        /// <summary>
        /// Used for searching perforce for which mission stats.xml files exist for a particular playthrough.
        /// </summary>
        [ImportExtension(PerforceBrowser.AddIn.CompositionPoints.PerforceService, typeof(PerforceBrowser.AddIn.IPerforceService))]
        public PerforceBrowser.AddIn.IPerforceService PerforceService { get; set; }
        #endregion // MEF Imports

        #region Properties
        /// <summary>
        /// Mission reports for this category
        /// </summary>
        public override IEnumerable<IReportItem> Reports
        {
            
            get { return m_reports; }
            protected set
            {
                SetPropertyValue(value, () => Reports,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_reports = (IEnumerable<IReportItem>)newValue;
                        }
                ));
            }
        }
        private IEnumerable<IReportItem> m_reports;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public AutomatedMissionsCategory()
            : base(c_name, c_description)
        {
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// Method that gets called to intialise the category's reports.
        /// </summary>
        public override void InitialiseReports()
        {
            base.InitialiseReports();

            // Create the list of reports that will be present in this category.
            IList<IReport> missionReports = new List<IReport>();

            try
            {
                // Iterate over all the groups adding any missions we find
                Regex nameRegex = new Regex(Config.ReportsConfig.AutomatedReports.MissionReports.StatsFileRegexString);

                string basePath = Path.GetFullPath(Config.ReportsConfig.AutomatedReports.MissionReports.MissionsDirectory);
                foreach (string playthroughGroup in Config.ReportsConfig.AutomatedReports.MissionReports.PlaythroughGroups)
                {
                    string playthroughPath = Path.Combine(basePath, playthroughGroup);

                    // Use p4 to determine which files exist.
                    P4API.P4RecordSet recordSet = PerforceService.PerforceConnection.Run("files", String.Format("{0}\\metrics....xml", playthroughPath));

                    foreach (P4API.P4Record record in recordSet)
                    {
                        string depotPath = record["depotFile"];
                        Match regexMatch = nameRegex.Match(Path.GetFileName(depotPath));

                        if (regexMatch.Success)
                        {
                            Group missionGroup = regexMatch.Groups["mission"];
                            Group runGroup = regexMatch.Groups["run"];

                            if (missionGroup.Success && runGroup.Success)
                            {
                                string missionId = missionGroup.Value;
                                int runNumber = Int32.Parse(runGroup.Value);

                                // For now ignore run's other than 0
                                if (runNumber == 0)
                                {
                                    missionReports.Add(new AutomatedMissionsTestReport(missionId, depotPath, PerforceService.PerforceConnection));
                                }
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                Log.Log__Exception(ex, "Exception occurred while attempting to retrieve automated mission names.");
            }

            // Set the reports and let the base class do its thing.
            Reports = missionReports;
            AssetChildren.AddRange(Reports);
        }

        /// <summary>
        /// 
        /// </summary>
        public override void OnImportsSatisfied()
        {
        }
        #endregion // ReportCategory Overrides
    } // Missions
}
