﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Workbench.AddIn.SceneOverrideEditor
{
    public class EntityMarkedForDeletion
    {
        public EntityMarkedForDeletion(uint hash, uint guid, float posX, float posY, float posZ, String imapName, String modelName)
        {
            Guid = guid;
            Hash = hash;
            PosX = posX;
            PosY = posY;
            PosZ = posZ;
            IMAPName = imapName;
            ModelName = modelName;
        }

        public uint Guid { get; private set; }
        public uint Hash { get; private set; }
        public float PosX { get; private set; }
        public float PosY { get; private set; }
        public float PosZ { get; private set; }
        public String IMAPName { get; private set; }
        public String ModelName { get; private set; }

        public String PositionText
        {
            get
            {
                return String.Format("[{0}, {1}, {2}]", PosX.ToString("0.00"), PosY.ToString("0.00"), PosZ.ToString("0.00"));
            }
        }
    }
}
