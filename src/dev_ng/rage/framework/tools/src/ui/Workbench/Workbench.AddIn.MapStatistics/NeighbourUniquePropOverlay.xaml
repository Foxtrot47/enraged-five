﻿<UserControl x:Class="Workbench.AddIn.MapStatistics.NeighbourUniquePropOverlay"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             xmlns:converters="clr-namespace:RSG.Base.Windows.Converters;assembly=RSG.Base.Windows"
             xmlns:ctrls="clr-namespace:RSG.Base.Windows.Controls;assembly=RSG.Base.Windows"
             xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
             xmlns:m="clr-namespace:RSG.Model.Map3;assembly=RSG.Model.Map3"
             xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
             d:DesignHeight="300"
             d:DesignWidth="300"
             mc:Ignorable="d">

    <UserControl.Resources>

        <converters:BoolToVisibilityConverter x:Key="BooleanToVisibilityConverter" />
        <converters:BoolToVisibilityConverter x:Key="InverseBoolToVisibilityConverter"
                                              InvertVisibility="True" />

        <Style x:Key="DataGridStyle"
               TargetType="{x:Type DataGrid}">
            <Setter Property="SelectionMode"
                    Value="Extended" />
            <Setter Property="SelectionUnit"
                    Value="FullRow" />
            <Setter Property="AutoGenerateColumns"
                    Value="False" />
            <Setter Property="RowBackground"
                    Value="#F7F7F7" />
            <Setter Property="AlternatingRowBackground"
                    Value="#FFFFFF" />
            <Setter Property="AlternationCount"
                    Value="1" />
        </Style>

    </UserControl.Resources>

    <Grid>
        <DockPanel>
            <TextBlock DockPanel.Dock="Top"
                       FontWeight="Bold"
                       Text="Map Overlay Options:" />
            <StackPanel DockPanel.Dock="Top"
                        Orientation="Horizontal">
                <CheckBox Margin="5,5,0,0"
                          Content="Include Prop Group"
                          DockPanel.Dock="Top"
                          IsChecked="{Binding IncludePropGroup}" />
                <StackPanel Margin="5,5,0,0"
                            Orientation="Vertical">
                    <CheckBox Margin="5,0,0,0"
                              Content="Include Dynamic"
                              DockPanel.Dock="Left"
                              IsChecked="{Binding IncludeDynamic}"
                              Visibility="{Binding DontShowPropOptions,
                                                   Converter={StaticResource InverseBoolToVisibilityConverter}}" />
                    <CheckBox Margin="5,0,0,0"
                              Content="Include Non Dynamic"
                              DockPanel.Dock="Left"
                              IsChecked="{Binding IncludeNonDynamic}"
                              Visibility="{Binding DontShowPropOptions,
                                                   Converter={StaticResource InverseBoolToVisibilityConverter}}" />
                </StackPanel>
            </StackPanel>

            <DockPanel Margin="5,5,0,0"
                       DockPanel.Dock="Top"
                       LastChildFill="False">
                <CheckBox Content="Show only the worst"
                          DockPanel.Dock="Left"
                          IsChecked="{Binding OnlyWorstCases}" />
                <ctrls:IntegerUpDown Width="50"
                                     Margin="5,0,0,0"
                                     DockPanel.Dock="Left"
                                     Maximum="{Binding MaximumGeometryCount}"
                                     Minimum="1"
                                     Value="{Binding WorstCaseCount}" />
                <TextBlock Margin="5,0,0,0"
                           Text="offenders" />
            </DockPanel>
            <DockPanel Margin="5,0,0,0"
                       DockPanel.Dock="Top"
                       LastChildFill="False">
                <CheckBox Content="Show only if over the limit"
                          DockPanel.Dock="Left"
                          IsChecked="{Binding OnlyGreaterThan}" />
                <ctrls:UnsignedIntegerUpDown Width="100"
                                             Margin="5,0,0,0"
                                             DockPanel.Dock="Left"
                                             Value="{Binding GreaterThanLimit}" />
            </DockPanel>

            <TextBlock DockPanel.Dock="Top"
                       FontWeight="Bold"
                       Text="Selection Details:" />
            <ComboBox Name="SelectedSection"
                      Height="22"
                      Margin="0,2,0,0"
                      HorizontalAlignment="Stretch"
                      DockPanel.Dock="Top"
                      ItemsSource="{Binding MapViewport.SelectedOverlayGeometry}"
                      SelectedItem="{Binding SelectedSection,
                                             Mode=TwoWay}">

                <ComboBox.Style>
                    <Style TargetType="{x:Type ComboBox}">
                        <Setter Property="Visibility"
                                Value="Visible" />
                        <Style.Triggers>
                            <DataTrigger Binding="{Binding RelativeSource={RelativeSource Self},
                                                           Path=Items.Count}"
                                         Value="1">
                                <Setter Property="SelectedIndex"
                                        Value="0" />
                            </DataTrigger>
                        </Style.Triggers>
                    </Style>
                </ComboBox.Style>

                <ComboBox.Resources>

                    <DataTemplate DataType="{x:Type m:MapSection}">
                        <StackPanel Orientation="Horizontal">
                            <TextBlock FontFamily="Segoe UI Symbol"
                                       FontSize="11">
                                <TextBlock.Inlines>
                                    <Run FontWeight="Bold"
                                         Text="{Binding Path=Name,
                                                        Mode=OneWay,
                                                        UpdateSourceTrigger=PropertyChanged}" />
                                    <Run FontStyle="Normal"
                                         FontWeight="Normal"
                                         Text="(Map Section Statistics) " />
                                    <Run FontStyle="Normal"
                                         FontWeight="Normal"
                                         Text="Exported By: " />
                                    <Run FontStyle="Normal"
                                         FontWeight="Normal"
                                         Text="{Binding Path=LastExportUser,
                                                        Mode=OneWay,
                                                        UpdateSourceTrigger=PropertyChanged}" />
                                    <Run FontStyle="Normal"
                                         FontWeight="Normal"
                                         Text=" On: " />
                                    <Run FontStyle="Normal"
                                         FontWeight="Normal"
                                         Text="{Binding Path=LastExportTime,
                                                        Mode=OneWay,
                                                        UpdateSourceTrigger=PropertyChanged}" />
                                </TextBlock.Inlines>
                            </TextBlock>
                        </StackPanel>
                    </DataTemplate>

                </ComboBox.Resources>

            </ComboBox>
            <Grid>
                <Grid.RowDefinitions>
                    <RowDefinition Height="Auto" />
                    <RowDefinition Height="*" />
                    <RowDefinition Height="Auto" />
                    <RowDefinition Height="*" />
                </Grid.RowDefinitions>
                <TextBlock Grid.Row="0"
                           DockPanel.Dock="Top"
                           FontWeight="Bold"
                           Text="{Binding UniqueArchetypes.Count,
                                          StringFormat='Unique Props: {0}'}" />
                <DataGrid Grid.Row="1"
                          DockPanel.Dock="Top"
                          ItemsSource="{Binding UniqueArchetypes}"
                          Style="{StaticResource DataGridStyle}">
                    <DataGrid.Columns>
                        <DataGridTextColumn Width="*"
                                            Binding="{Binding Key.Name}"
                                            Header="Prop Definition"
                                            IsReadOnly="True" />
                        <DataGridTextColumn Width="*"
                                            Binding="{Binding Value}"
                                            Header="Instance Count"
                                            IsReadOnly="True" />
                    </DataGrid.Columns>
                </DataGrid>
                <TextBlock Grid.Row="2"
                           DockPanel.Dock="Top"
                           FontWeight="Bold"
                           Text="{Binding NonUniqueArchetypes.Count,
                                          StringFormat='Non Unique Props: {0}'}" />

                <DataGrid Grid.Row="3"
                          DockPanel.Dock="Top"
                          ItemsSource="{Binding NonUniqueArchetypes}"
                          Style="{StaticResource DataGridStyle}">
                    <DataGrid.Columns>
                        <DataGridTextColumn Width="*"
                                            Binding="{Binding Key.Name}"
                                            Header="Prop Definition"
                                            IsReadOnly="True" />
                        <DataGridTextColumn Width="*"
                                            Binding="{Binding Value}"
                                            Header="Block Count"
                                            IsReadOnly="True" />
                    </DataGrid.Columns>
                </DataGrid>
            </Grid>
        </DockPanel>
    </Grid>
</UserControl>
