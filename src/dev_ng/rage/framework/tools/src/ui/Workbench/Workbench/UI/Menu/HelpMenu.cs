﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Windows;
using System.Windows.Input;
using RSG.Base.Logging;
using Workbench.AddIn;
using Workbench.AddIn.Commands;
using Workbench.AddIn.Services;
using Workbench.AddIn.UI.Menu;

namespace Workbench.UI.Menu
{
    /// <summary>
    /// Help menu.
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.MenuMain, typeof(IWorkbenchCommand))]
    class HelpMenu : WorkbenchMenuItemBase, IPartImportsSatisfiedNotification
    {
        #region MEF Imports
        /// <summary>
        /// Extensions service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ExtensionService)]
        private IExtensionService ExtensionService { get; set; }

        /// <summary>
        /// File menu subitems.
        /// </summary>
        [ImportManyExtension(Workbench.AddIn.ExtensionPoints.MenuHelp,
            typeof(IWorkbenchCommand))]
        private IEnumerable<IWorkbenchCommand> items { get; set; }
        #endregion // MEF Imports

        #region Constructor(s)

        /// <summary>
        /// Default constructor.
        /// </summary>
        public HelpMenu()
        {
            this.Header = "_Help";
            ID = new Guid(Workbench.AddIn.ExtensionPoints.MenuHelp);
        }

        #endregion // Constructor(s)

        #region IPartImportsSatisfiedNotification Methods
        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {
            this.Items = ExtensionService.Sort(items);
        }
        #endregion // IPartImportsSatisfiedNotification Methods

        #region ICommand Implementation

        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
        }

        #endregion // ICommand Implementation
    }
    
    /// <summary>
    /// Help, Help Topics menu item.
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.MenuHelp, typeof(IWorkbenchCommand))]
    class HelpTopicsMenuItem : WorkbenchMenuItemBase
    {
        #region Constants
        public static readonly Guid GUID = new Guid("5E9C22BD-9598-4451-81AA-2FEF93B15574");
        public static readonly String URL_HELP = "https://devstar.rockstargames.com/wiki/index.php/Workbench";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public HelpTopicsMenuItem()
        {
            this.Header = "_Help Topics";
            this.ID = GUID;
            this.KeyGesture = new KeyGesture(Key.F1);
            SetImageFromBitmap(Resources.Images.Help);
        }
        #endregion // Constructor(s)

        #region ICommand Implementation
        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
            Log.Log__Debug("HelpTopicsMenuItem::Execute()");
            RSG.Base.Win32.API.ShellExecute(IntPtr.Zero, "open", URL_HELP, 
                String.Empty, String.Empty, 0);
        }
        #endregion // ICommand Implementation
    } // HelpTopicsMenuItem

    /// <summary>
    /// Help, About menu item.
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.MenuHelp, typeof(IWorkbenchCommand))]
    public class HelpAboutMenuItem : WorkbenchMenuItemBase
    {
        #region Constants

        public static readonly Guid GUID = new Guid("9B2DEFE3-2D82-4433-8EFE-0FCC116284B9");

        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public HelpAboutMenuItem()
        {
            this.Header = "_About...";
            this.ID = GUID;
            this.RelativeID = HelpTopicsMenuItem.GUID;
            this.Direction = Direction.After;
            SetImageFromBitmap(Resources.Images.About);
        }
        #endregion // Constructor(s)

        #region ICommand Implementation

        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
            Log.Log__Debug("HelpAboutMenuItem::Execute()");

            if (App.Current != null && App.Current.MainWindow != null)
                AboutBox.AboutBoxDialog.Owner = App.Current.MainWindow;

            AboutBox.AboutBoxDialog.ShowDialog();
        }

        #endregion // ICommand Implementation
    } // HelpAboutMenuItem
} // Workbench.UI.Menu namespace
