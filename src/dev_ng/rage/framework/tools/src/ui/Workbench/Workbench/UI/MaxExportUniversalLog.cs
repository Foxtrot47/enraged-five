﻿using Workbench.AddIn;
using Workbench.AddIn.Services;
using System.IO;

namespace Workbench.UI
{
    /// <summary>
    /// 
    /// </summary>
    //[ExportExtension(Workbench.AddIn.ExtensionPoints.LoggingOutput, typeof(ILoggingService))]
    public class MaxExportUniversalLog : IExternalLoggingService
    {
        #region Imports
        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(IConfigurationService))]
        private IConfigurationService ConfigurationService { get; set;}
        #endregion

        #region Properties
        public  string UIName 
        {
            get { return "Max Export"; }
        }

        public bool IsDefault
        {
            get { return false; }
        }

        public string LogDirectory 
        {
            get { return (this.ConfigurationService.Config.ToolsLogs); }
        }

        public string LogFilenamePattern
        {
            get { return "unilog.ulog"; }
        }
        #endregion
    } // WorkbenchUniversalLog
} // Workbench.UI
