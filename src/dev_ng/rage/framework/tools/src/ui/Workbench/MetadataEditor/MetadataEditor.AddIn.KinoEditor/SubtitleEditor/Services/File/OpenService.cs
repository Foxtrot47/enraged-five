﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn.Services.File;
using Workbench.AddIn;
using Workbench.AddIn.Services;
using System.Windows.Input;
using Workbench.AddIn.UI.Layout;
using RSG.Base.Editor;
using System.Windows;
using System.Drawing;

namespace MetadataEditor.AddIn.KinoEditor.Subtitle_Editor.Services.File
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.OpenService, typeof(IOpenService))]
    public class OpenService : IOpenService
    {
        #region Constants
        private readonly Guid GUID = new Guid("F91F63EE-A759-448D-A69C-A8C7863902C3");
        private readonly String COMMAND_LINE_KEY = String.Empty;
        #endregion // Constants

        #region Imports
        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(IConfigurationService))]
        private IConfigurationService Config { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.WebBrowserService, typeof(IWebBrowserService))]
        private IWebBrowserService WebBrowser { get; set; }
        #endregion

        #region Properties
        /// <summary>
        /// Service GUID.
        /// </summary>
        public Guid ID
        {
            get { return (GUID); }
        }

        /// <summary>
        /// Open service header string for UI display.
        /// </summary>
        public String Header
        {
            get { return "Open Subtitle Editor Files..."; }
        }

        /// <summary>
        /// Open service Bitmap for UI display.
        /// </summary>
        public Bitmap Image
        {
            get { return null; }
        }

        /// <summary>
        /// Open dialog filter string.
        /// </summary>
        public String Filter
        {
            get { return "Subtitle Editor Files (.projsub)|*.projsub"; }
        }

        /// <summary>
        /// Determines if the open dialog allows multiple items selected
        /// </summary>
        public Boolean AllowMultiSelect
        {
            get { return false; }
        }

        /// <summary>
        /// Open dialog default filter index.
        /// </summary>
        public int DefaultFilterIndex
        {
            get { return 0; }
        }

        /// <summary>
        /// The keybinding that is placed on this command.
        /// This binding is added to the applications bindings.
        /// (Only used if this is a main menu item)
        /// </summary>
        public KeyGesture KeyGesture
        {
            get { return null; }
        }

        /// <summary>
        /// Command line key.
        /// </summary>
        public String CommandLineKey
        {
            get { return COMMAND_LINE_KEY; }
        }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        public bool Open(out IDocumentBase document, out IModel model, String filename, int filterIndex)
        {
            document = new SubtitleEditorWindowView(filename);
            model = (document as DocumentBase<SubtitleEditorViewModel>).ViewModel;
            if (document != null && model != null)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documents"></param>
        /// <param name="models"></param>
        /// <param name="filenames"></param>
        /// <param name="filterIndex"></param>
        /// <returns></returns>
        public bool Open(out Dictionary<IDocumentBase, List<String>> documents, out List<IModel> models, List<String> filenames, int filterIndex)
        {
            documents = new Dictionary<IDocumentBase, List<String>>();
            models = new List<IModel>();
            //foreach (string filename in filenames)
            //{
            //    var document = new CutsceneEventDefEditorToolWindowView(filename);
            //    var model = (document as DocumentBase<CutsceneEventDefEditorToolWindowViewModel>).ViewModel;

            //    documents.Add(document, new List<string>() { filename });
            //    models.Add(model);
            //}
            //return documents.Count >= 1;
            return true;
        }

        /// <summary>
        /// Creates a new document based on the specified model
        /// </summary>
        /// <param name="model">
        /// The model that determines what document is created.
        /// </param>
        /// <returns>
        /// A new document that has its model/viewmodel setup to represent the specified model.
        /// </returns>
        public IDocumentBase Open(IModel model)
        {
            throw new NotImplementedException();
        }
        #endregion // Methods
    } // OpenService
}
