﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using Editor.AddIn;
using Editor.AddIn.Services;
using Editor.Workbench.AddIn;
using Editor.Workbench.AddIn.UI.Layout;
using Editor.Workbench.AddIn.UI.Menu;
using RSG.Base.Logging;

namespace Editor.Workbench.AddIn.RAG.UI.Menu
{

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Editor.Workbench.AddIn.ExtensionPoints.MenuTools,
        typeof(IMenuItem))]
    class StartRAGMenuItem : 
        MenuItemBase,
        IPartImportsSatisfiedNotification
    {
        #region MEF Imports
        [ImportExtension(Editor.Workbench.AddIn.CompositionPoints.LayoutManager,
            typeof(ILayoutManager))]
        private ILayoutManager LayoutManager { get; set; }
        
        [ImportExtension(Editor.Workbench.AddIn.RAG.CompositionPoints.RAG,
            typeof(WidgetViewModel))]
        private WidgetViewModel Model { get; set; }
        #endregion // MEF Imports

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public StartRAGMenuItem()
            : base("_RAG")
        {
            Direction = Direction.After;
            RelativeID = new Guid(Editor.Workbench.AddIn.ExtensionPoints.MenuToolsSeparator);
            //SetImageFromBitmap(Resources.Images.Exit);
        }
        #endregion // Constructor(s)

        #region ICommand Interface
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        public override void Execute(Object parameter)
        {
            Log.Log__Debug("StartRAGMenuItem::Execute()");

            IDocument document = Model.CreateDocument("RAG");
            LayoutManager.ShowDocument(document);
        }
        #endregion // ICommand Interface

        #region IPartImportsSatisfiedNotification Methods
        /// <summary>
        /// Invoked when MEF imports satisfied.
        /// </summary>
        public void OnImportsSatisfied()
        {
            Log.Log__Debug("StartRAGMenuItem::OnImportsSatisfied()");
        }
        #endregion // IPartImportsSatisfiedNotification Methods
    }

} // Editor.Workbench.AddIn.RAG.UI.Menu namespace
