﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Diagnostics;
using RSG.Metadata.Model;
using MetadataEditor.AddIn.View;
using RSG.Base.Editor.Command;
using System.IO;
using System.Xml;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Media;
using Microsoft.Win32;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn;

namespace MetadataEditor.AddIn.KinoEditor.Cutscene_Event_Def_Editor
{
    /// <summary>
    /// 
    /// </summary>
    public class CutsceneEventDefEditorToolWindowViewModel :
        RSG.Base.Editor.ViewModelBase,
        IPartImportsSatisfiedNotification
    {
        #region Constants
        public const String TOOLWINDOW_NAME = "Cutscene_Event_Def_Editor";
        public const String TOOLWINDOW_TITLE = "Cutscene Event Def Editor";
        #endregion // Constants

        #region Properties and Associated Member Data

        private string m_currentProject = String.Empty;

        /// <summary>
        /// 
        /// </summary>
        public EventCollection EventEntries
        {
            get { return m_EventEntries; }
            private set
            {
                SetPropertyValue(value, () => this.EventEntries,
                    new PropertySetDelegate(delegate(Object newValue) { m_EventEntries = (EventCollection)newValue; }));
            }
        }
        private EventCollection m_EventEntries = null;

        public EventArgCollection EventArgEntries
        {
            get { return m_EventArgEntries; }
            private set
            {
                SetPropertyValue(value, () => this.EventArgEntries,
                    new PropertySetDelegate(delegate(Object newValue) { m_EventArgEntries = (EventArgCollection)newValue; }));
            }
        }
        private EventArgCollection m_EventArgEntries = null;

        public ObservableCollection<Event> OppositeEventEntries
        {
            get { return m_OppositeEventEntries; }
        }
        private ObservableCollection<Event> m_OppositeEventEntries;

        public ObservableCollection<String> AttributeTypes
        {
            get { return m_AttributeTypes; }
        }
        private ObservableCollection<String> m_AttributeTypes;
        private ObservableCollection<AttributeType> m_AttributeTypesMapping;

        public Event SelectedEvent
        {
            get { return m_SelectedEvent; }
            set
            {
                SetPropertyValue(value, () => this.SelectedEvent,
                    new PropertySetDelegate(delegate(Object newValue) { m_SelectedEvent = (Event)newValue; }));
            }
        }
        private Event m_SelectedEvent;

        public EventArg SelectedEventArg
        {
            get { return m_SelectedEventArg; }
            set
            {
                SelectedAttribute = new Attribute();

                SetPropertyValue(value, () => this.SelectedEventArg,
                    new PropertySetDelegate(delegate(Object newValue) { m_SelectedEventArg = (EventArg)newValue; }));
            }
        }
        private EventArg m_SelectedEventArg;

        public Attribute SelectedAttribute
        {
            get { return m_SelectedAttribute; }
            set
            {
                SetPropertyValue(value, () => this.SelectedAttribute,
                    new PropertySetDelegate(delegate(Object newValue) { m_SelectedAttribute = (Attribute)newValue; }));
            }
        }
        private Attribute m_SelectedAttribute;

        public string HeaderFilename 
        {
            get { return m_HeaderFilename; }
            set
            {
                SetPropertyValue(value, () => this.HeaderFilename,
                    new PropertySetDelegate(delegate(Object newValue) { m_HeaderFilename = (string)newValue; }));
            }
        }

        private string m_HeaderFilename;

        public string HeaderNamespace
        {
            get { return m_HeaderNamespace; }
            set
            {
                SetPropertyValue(value, () => this.HeaderNamespace,
                    new PropertySetDelegate(delegate(Object newValue) { m_HeaderNamespace = (string)newValue; }));
            }
        }

        private string m_HeaderNamespace;

        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public CutsceneEventDefEditorToolWindowViewModel()
        {
            this.EventArgEntries = new EventArgCollection();
            this.EventEntries = new EventCollection();
            m_OppositeEventEntries = new ObservableCollection<Event>();
            m_AttributeTypes = new ObservableCollection<String>();
            m_AttributeTypesMapping = new ObservableCollection<AttributeType>();

            Reset();

            AttributeTypes.Add("Bool");
            AttributeTypes.Add("Int");
            AttributeTypes.Add("Float");
            AttributeTypes.Add("String");

            m_AttributeTypesMapping.Add(new AttributeType("Bool", 3));
            m_AttributeTypesMapping.Add(new AttributeType("Int", 1));
            m_AttributeTypesMapping.Add(new AttributeType("Float", 2));
            m_AttributeTypesMapping.Add(new AttributeType("String", 0));
        }
        #endregion // Constructor(s)

        #region IPartImportsSatisfiedNotification Interface
        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {

        }
        #endregion // IPartImportsSatisfiedNotification Interface

        public void Reset()
        {
            m_currentProject = String.Empty;

            if (EventEntries != null)
                EventEntries.Clear();

            if (EventArgEntries != null)
                EventArgEntries.Clear();

            SelectedEvent = new Event();
            SelectedEventArg = new EventArg();
            SelectedAttribute = new Attribute();
            HeaderFilename = String.Empty;
            HeaderNamespace = String.Empty;

            if (OppositeEventEntries != null)
            {
                OppositeEventEntries.Clear();
                Event e = new Event();
                e.FullEventName = "CUTSCENE_NO_OPPOSITE_EVENT";
                OppositeEventEntries.Add(e);
                Event e2 = new Event();
                e2.FullEventName = "CUTSCENE_OPPOSITE_EVENT_USE_PREVIOUS";
                OppositeEventEntries.Add(e2);
            }
        }

        public string GetCurrentProject()
        {
            return m_currentProject;
        }

        private void PreLoadEvents(string filename)
        {
            var xmlDoc = new XmlDocument();
            xmlDoc.Load(filename);

            XmlNodeList eventItemNodes = xmlDoc.SelectNodes("/rage__cutfEventDefs/eventDefList/Item");

            for (int i = 0; i < eventItemNodes.Count; ++i)
            {
                Event currentEvent = new Event();

                foreach (XmlNode n in eventItemNodes[i].ChildNodes)
                {
                    switch (n.Name)
                    {
                        case "cName":
                            currentEvent.EventName = n.InnerText;
                            break;
                        case "iRank":
                            currentEvent.Rank = n.Attributes["value"].InnerText;
                            break;
                        case "pEventArgsDef":
                            int index = 0;
                            if (Int32.TryParse(n.Attributes["ref"].InnerText, out index))
                            {
                                if (index < EventArgEntries.Count && index >= 0)
                                {
                                    currentEvent.AssociatedEventArg = EventArgEntries[index];
                                }
                            }
                            break;
                    }
                }

                EventEntries.Add(currentEvent);
                OppositeEventEntries.Add(currentEvent);
            }
        }

        public void Open(string filename)
        {
            Reset();

            var xmlDoc = new XmlDocument();
            xmlDoc.Load(filename);

            HeaderFilename = xmlDoc.SelectSingleNode("/rage__cutfEventDefs/cHeaderFilename").InnerText;
            HeaderNamespace = xmlDoc.SelectSingleNode("/rage__cutfEventDefs/cHeaderNamespace").InnerText;

            XmlNodeList eventArgItemNodes = xmlDoc.SelectNodes("/rage__cutfEventDefs/eventArgsDefList/Item");

            for (int i = 0; i < eventArgItemNodes.Count; ++i)
            {
                EventArg currentArg = new EventArg();

                foreach (XmlNode n in eventArgItemNodes[i].ChildNodes)
                {
                    switch (n.Name)
                    {
                        case "cName":
                            currentArg.Name = n.InnerText;
                            break;
                        case "attributeList":
                            foreach (XmlNode attr in n.ChildNodes)
                            {
                                Attribute currentAttribute = new Attribute();
                                currentAttribute.Name = attr.Name;
                                currentAttribute.DefaultValue = attr.Attributes["value"].InnerText;

                                int id = 0;
                                if (Int32.TryParse(attr.Attributes["type"].InnerText, out id))
                                {
                                    for (int j = 0; j < m_AttributeTypesMapping.Count; ++j)
                                    {
                                        if (m_AttributeTypesMapping[j].TypeID == id)
                                            currentAttribute.Type = m_AttributeTypesMapping[j].Name;
                                    }
                                }

                                currentArg.Attributes.Add(currentAttribute);
                            }
                            break;
                    }
                }

                EventArgEntries.Add(currentArg);
            }

            PreLoadEvents(filename);

            XmlNodeList eventItemNodes = xmlDoc.SelectNodes("/rage__cutfEventDefs/eventDefList/Item");

            for (int i = 0; i < eventItemNodes.Count; ++i)
            {
                string strName = String.Empty;

                foreach (XmlNode n in eventItemNodes[i].ChildNodes)
                {
                    switch (n.Name)
                    {
                        case "cName":
                            strName = n.InnerText;
                            break;
                        case "iOppositeEventIdOffset":
                            int id = 0;
                            if (Int32.TryParse(n.Attributes["value"].InnerText, out id))
                            {
                                if (id < EventEntries.Count)
                                {
                                    if (id < 0)
                                    {
                                        foreach (Event currentEvent in EventEntries)
                                        {
                                            if (currentEvent.EventName == strName)
                                            {
                                                if (id == -2)
                                                    currentEvent.OppositeEvent = OppositeEventEntries[0];
                                                else if (id == -1)
                                                    currentEvent.OppositeEvent = OppositeEventEntries[1];
                                            }
                                        }
                                    }
                                    else
                                    {
                                        foreach (Event currentEvent in EventEntries)
                                        {
                                            if (currentEvent.EventName == strName)
                                                currentEvent.OppositeEvent = EventEntries[id];
                                        }
                                    }
                                }
                            }
                            break;
                    }
                }
            }

            m_currentProject = filename;
        }

        public void Save(string filename)
        {
            using (XmlTextWriter textWriter = new XmlTextWriter(filename, null))
            {
                textWriter.Formatting = Formatting.Indented;

                textWriter.WriteStartDocument();
                textWriter.WriteStartElement("rage__cutfEventDefs");

                textWriter.WriteStartElement("cHeaderFilename");
                textWriter.WriteString(HeaderFilename);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("cHeaderNamespace");
                textWriter.WriteString(HeaderNamespace);
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("eventArgsDefList");

                int count = 1;
                foreach (EventArg arg in EventArgEntries)
                {
                    textWriter.WriteStartElement("Item");
                    textWriter.WriteAttributeString("type", "rage__cutfEventArgsDef");

                    textWriter.WriteStartElement("iEventArgsTypeOffset");
                    textWriter.WriteAttributeString("value", count.ToString());
                    textWriter.WriteEndElement();
                    count++;

                    textWriter.WriteStartElement("cName");
                    textWriter.WriteString(arg.Name);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("attributeList");

                    foreach (Attribute a in arg.Attributes)
                    {
                        textWriter.WriteStartElement(a.Name);
                        textWriter.WriteAttributeString("value", a.DefaultValue);

                        int index = 0;
                        for (int i = 0; i < m_AttributeTypesMapping.Count; ++i)
                        {
                            if (m_AttributeTypesMapping[i].Name == a.Type)
                            {
                                index = m_AttributeTypesMapping[i].TypeID;
                                break;
                            }
                        }

                        textWriter.WriteAttributeString("type", index.ToString());
                        textWriter.WriteEndElement();
                    }

                    textWriter.WriteEndElement();

                    textWriter.WriteEndElement();
                }

                textWriter.WriteEndElement(); // eventArgsDefList

                textWriter.WriteStartElement("eventDefList");

                count = 0;
                foreach (Event eve in EventEntries)
                {
                    textWriter.WriteStartElement("Item");
                    textWriter.WriteAttributeString("type", "rage__cutfEventDef");

                    textWriter.WriteStartElement("iEventIdOffset");
                    textWriter.WriteAttributeString("value", count.ToString());
                    textWriter.WriteEndElement();
                    count++;

                    textWriter.WriteStartElement("cName");
                    textWriter.WriteString(eve.EventName);
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("iRank");
                    textWriter.WriteAttributeString("value", ((int)Double.Parse(eve.Rank)).ToString());
                    textWriter.WriteEndElement();

                    int oppositeEventIndex = -3;
                    if (eve.OppositeEvent != null)
                    {
                        if (eve.OppositeEvent.FullEventName == "CUTSCENE_NO_OPPOSITE_EVENT")
                        {
                            oppositeEventIndex = -2;
                        }
                        else if (eve.OppositeEvent.FullEventName == "CUTSCENE_OPPOSITE_EVENT_USE_PREVIOUS")
                        {
                            oppositeEventIndex = -1;
                        }
                        else
                        {
                            for (int i = 0; i < EventEntries.Count; ++i)
                            {
                                if (EventEntries[i].FullEventName == eve.OppositeEvent.FullEventName)
                                {
                                    oppositeEventIndex = i;
                                    break;
                                }
                            }
                        }
                    }

                    textWriter.WriteStartElement("iOppositeEventIdOffset");
                    textWriter.WriteAttributeString("value", oppositeEventIndex.ToString());
                    textWriter.WriteEndElement();

                    textWriter.WriteStartElement("pEventArgsDef");

                    int index = -1;
                    for (int i = 0; i < EventArgEntries.Count; ++i)
                    {
                        if (EventArgEntries[i] == eve.AssociatedEventArg)
                        {
                            index = i;
                            break;
                        }
                    }

                    textWriter.WriteAttributeString("ref", index.ToString());
                    textWriter.WriteEndElement();

                    textWriter.WriteEndElement();
                }

                textWriter.WriteEndElement(); // eventDefList

                textWriter.WriteEndElement(); // rage__cutfEventDefs
                textWriter.WriteEndDocument();
            }

            m_currentProject = filename;
        }

        public void GenerateHeader()
        {
            if(String.IsNullOrEmpty(m_currentProject))
            {
                Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
                dlg.DefaultExt = ".xml";
                dlg.Filter = "Event Definition files (.xml)|*.xml";

                Nullable<bool> result = dlg.ShowDialog();

                if (result == true)
                {
                    Save(dlg.FileName);
                }
                else
                    return;
            }

            string strEventDefExe = Path.Combine(Environment.GetEnvironmentVariable("RS_TOOLSROOT"), "bin", "cutscene", "cutfeventdefedit.exe");

            System.Diagnostics.Process pExe = new System.Diagnostics.Process();
            pExe.StartInfo.FileName = strEventDefExe;
            pExe.StartInfo.Arguments = "-file \"" + m_currentProject + "\" -saveHeader";
            pExe.StartInfo.CreateNoWindow = true;
            pExe.StartInfo.UseShellExecute = false;
            pExe.StartInfo.RedirectStandardOutput = true;
            pExe.Start();

            pExe.WaitForExit();

            if (pExe.ExitCode != 0)
            {
                string standardOutput = pExe.StandardOutput.ReadToEnd();
                string[] outputLines = standardOutput.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

                foreach (string line in outputLines)
                {
                    if (line.StartsWith("Error"))
                    {
                        RSG.Base.Logging.Log.Log__Error(line.Replace("Error:", ""));
                    }
                }
            }
        }
    }

} // MetadataEditor.AddIn.KinoEditor.Cutscene_Event_Def_Editor

