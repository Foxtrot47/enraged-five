﻿using System;
using System.Collections.Generic;
using RSG.Base.Logging;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using Workbench.AddIn;
using Workbench.AddIn.UI.Layout;

namespace Workbench.UI
{
    /// <summary>
    /// 
    /// </summary>
    public class PropertyInspectorViewModel :
        ViewModelBase
    {
        public const String PROPERTYINSPECTOR_NAME = "PropertyInspector_View";
        public const String PROPERTYINSPECTOR_TITLE = "Property Inspector";

        public Object Selection
        {
            get { return m_selection; }
            set
            {
                SetPropertyValue(value, () => this.Selection,
                    new PropertySetDelegate(delegate(Object newValue) { m_selection = newValue; }));
            }
        }
        private Object m_selection;

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public PropertyInspectorViewModel()
        {
        }
        #endregion // Constructor(s)
    }
} // Workbench.UI
