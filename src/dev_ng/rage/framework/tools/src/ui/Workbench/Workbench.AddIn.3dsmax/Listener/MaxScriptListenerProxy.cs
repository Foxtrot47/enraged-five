﻿using System;
using System.ComponentModel.Composition;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn;
using Workbench.AddIn.Commands;
using Workbench.AddIn.UI.Layout;

namespace Workbench.AddIn._3dsmax.Listener
{

    /// <summary>
    /// MaxScript Listener Tool Window Proxy.
    /// </summary>
#warning DHM FIX ME: disabled until we have WCF 3dsmax plugin available.
    // [ExportExtension(ExtensionPoints.ToolWindowProxy, typeof(IToolWindowProxy))]
    // [ExportExtension(ExtensionPoints.ToolbarStandard, typeof(IWorkbenchCommand))]
    class MaxScriptListenerProxy : 
        IToolWindowProxy,
        IPartImportsSatisfiedNotification
    {
        #region MEF Imports
        /// <summary>
        /// Layout manager reference.
        /// </summary>
        [ImportExtension(CompositionPoints.LayoutManager, typeof(ILayoutManager))]
        private ILayoutManager LayoutManager { get; set; }
        #endregion // MEF Imports

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public MaxScriptListenerProxy()
        {
            this.IsToggle = true;
            this.Name = Resources.Strings.Listener_Name;
            this.Header = Resources.Strings.Listener_Title;
            this.DelayCreation = true;
            SetImageFromBitmap(Resources.Images.max);
        }
        #endregion // Constructor(s)

        #region IToolWindowProxy Methods
        /// <summary>
        /// Create and initialise the tool window.
        /// </summary>
        protected override bool CreateToolWindow(out IToolWindowBase toolWindow)
        {
            toolWindow = new MaxScriptListenerView();
            return true;
        }
        #endregion // IToolWindowProxy Methods

        #region ICommand Methods
        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
            IToolWindowBase toolWindow = this.GetToolWindow();
            if (toolWindow != null)
            {
                if (this.LayoutManager.IsVisible(toolWindow))
                    toolWindow.Hide();
                else
                {
                    if (toolWindow.WasFloatingWindowWhenHidden == true)
                        toolWindow.ShowAsFloatingWindow(true);
                    else
                        this.LayoutManager.ShowToolWindow(toolWindow);

                    if (toolWindow.FloatByDefault == true && toolWindow.HasBeenShown == false)
                    {
                        toolWindow.FloatingWindowSize = toolWindow.DefaultFloatSize;
                        toolWindow.ShowAsFloatingWindow(true);
                    }
                }
            }
        }
        #endregion // ICommand Methods

        #region IPartImportsSatisfiedNotification
        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {
            this.LayoutManager.LayoutUpdated += new EventHandler(LayoutManager_LayoutUpdated);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LayoutManager_LayoutUpdated(Object sender, EventArgs e)
        {
            if (this.HasBeenCreated == true)
            {
                this.IsChecked = LayoutManager.IsVisible(this.GetToolWindow());
            }
        }
        #endregion // IPartImportsSatisfiedNotification
    }

} // Workbench.AddIn._3dsmax.Listener
