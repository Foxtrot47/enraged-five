﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using Workbench.AddIn;
using Workbench.AddIn.Services;

namespace Workbench.Services
{

    /// <summary>
    /// Workbench undo/redo management service
    /// </summary>
    /// This is a core workbench service that provides the application with
    /// storage between the Model and ViewModel undo/redo managers and the
    /// 
    /// Its somewhat perverse, but this is a IViewModel itself.  This allows
    /// us to register ourselves for undo/redo the selection!
    /// 
    [ExportExtension(Workbench.AddIn.CompositionPoints.UndoRedoService,
        typeof(IUndoRedoService))]
    [ExportExtension(Workbench.AddIn.CompositionPoints.SelectionService,
        typeof(ISelectionService))]
    class UndoRedoAndSelectionService :
        ViewModelBase,
        IUndoRedoService,
        ISelectionService
    {
        #region Events
        /// <summary>
        /// Event raised when the selected Model changes.
        /// </summary>
        public event EventHandler<ModelEventArgs> SelectedModelChanged;

        /// <summary>
        /// Event raised when the selected ViewModel changes.
        /// </summary>
        public event EventHandler<ViewModelEventArgs> SelectedViewModelChanged;
        #endregion // Events

        #region Properties and Associated Member Data
        /// <summary>
        /// Currently selected Model.
        /// </summary>
        public IModel SelectedModel
        {
            get { return m_SelectedModel; }
            set
            {
                SetPropertyValue(value, () => this.SelectedModel,
                   new PropertySetDelegate(delegate(Object newValue) { m_SelectedModel = (IModel)newValue; }));
            }
        }
        protected IModel m_SelectedModel = null;

        /// <summary>
        /// Currently selected ViewModel.
        /// </summary>
        public IViewModel SelectedViewModel
        {
            get { return m_SelectedViewModel; }
            set
            {
                SetPropertyValue(value, () => this.SelectedViewModel,
                   new PropertySetDelegate(delegate(Object newValue) { m_SelectedViewModel = (IViewModel)newValue; }));
            }
        }
        protected IViewModel m_SelectedViewModel = null;
        #endregion // Properties and Associated Member Data

        #region Member Data
        /// <summary>
        /// Dictionary of managed Models and their CommandManager.
        /// </summary>
        protected Dictionary<IModel, ModelCommandManagerPair> Models;

        /// <summary>
        /// Dictionary of managed ViewModels and their CommandManager.
        /// </summary>
        protected Dictionary<IViewModel, ModelCommandManagerPair> ViewModels;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public UndoRedoAndSelectionService()
        {
            this.Models = new Dictionary<IModel, ModelCommandManagerPair>();
            this.ViewModels = new Dictionary<IViewModel, ModelCommandManagerPair>();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Register a Model with the service.
        /// </summary>
        /// <param name="model"></param>
        public void RegisterModel(IModel model)
        {
        }

        /// <summary>
        /// Unregister a Model with the service.
        /// </summary>
        /// <param name="model"></param>
        public void UnregisterModel(IModel model)
        {
        }

        /// <summary>
        /// Register a ViewModel with the service.
        /// </summary>
        /// <param name="vm"></param>
        public void RegisterViewModel(IViewModel vm)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Unregister a ViewModel with the service.
        /// </summary>
        /// <param name="vm"></param>
        public void UnregisterViewModel(IViewModel vm)
        {
            throw new NotImplementedException();
        }
        #endregion // Controller Methods
    }

} // Workbench.Services namespace
