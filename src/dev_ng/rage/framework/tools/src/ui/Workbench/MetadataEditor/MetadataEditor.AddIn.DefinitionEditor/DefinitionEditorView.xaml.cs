﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using Workbench.AddIn;
using Workbench.AddIn.Services;
using Workbench.AddIn.UI.Layout;
using RSG.Metadata.Parser;
using MetadataEditor.AddIn.View;

namespace MetadataEditor.AddIn.DefinitionEditor
{
    /// <summary>
    /// DefinitionEditorView class code.
    /// </summary>
    public partial class DefinitionEditorView : DocumentBase<DefinitionEditorViewModel>
    {
        #region Properties

        IPropertyInspector PropertyInspector
        {
            get;
            set;
        }

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Default constructor.
        /// </summary>
        public DefinitionEditorView()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public DefinitionEditorView(Structure model, IPropertyInspector propertyInspector)
            :base(String.Format("{0}_{1}", DefinitionEditorViewModel.DOCUMENT_NAME, model.GetHashCode().ToString()),
            new DefinitionEditorViewModel(model))
        {
            InitializeComponent();
            this.Title = model.DataType;
            this.Path = String.Format("{0} [{1}]", model.Filename, model.DataType);
            this.IsReadOnly = true;
            this.PropertyInspector = propertyInspector;
        }

        #endregion // Constructor(s)

        #region Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgMembers_SelectionChanged(Object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            this.PropertyInspector.Clear();

            List<Object> selection = new List<Object>();
            foreach (Object selected in (sender as System.Windows.Controls.DataGrid).SelectedItems)
            {
                if (selected is KeyValuePair<String, IMember>)
                {
                    selection.Add(((KeyValuePair<String, IMember>)selected).Value);
                }
            }
            if (selection.Count > 0)
                this.PropertyInspector.PushRange(selection);

            //if (e.AddedItems.Count != 1)
            //{
            //    this.PropertyInspector.Clear();
            //    return;
            //}
            //else if (e.AddedItems[0] is KeyValuePair<String, IMember>)
            //{
            //    this.PropertyInspector.SetPropertySelection(((KeyValuePair<String, IMember>)e.AddedItems[0]).Value);
            //    this.PropertyInspector.Push(((KeyValuePair<String, IMember>)e.AddedItems[0]).Value);
            //}
        }
        #endregion // Event Handlers
    }
} // MetadataEditor.AddIn.DefinitionEditorViewModel namespace
