﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using RSG.Base.Logging;
using Workbench.AddIn;
using Workbench.AddIn.Commands;
using Workbench.AddIn.Services;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.UI.Menu;
using Workbench.Model;

namespace Workbench.UI.Menu
{

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(ExtensionPoints.MenuMain, typeof(IWorkbenchCommand))]
    [ExportExtension(CompositionPoints.ExternalToolService, typeof(IExternalToolService))]
    class ToolsMenu : 
        WorkbenchMenuItemBase, 
        IExternalToolService,
        IPartImportsSatisfiedNotification
    {
        #region MEF Imports
        /// <summary>
        /// Extensions service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ExtensionService)]
        private IExtensionService ExtensionService { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager,
            typeof(ILayoutManager))]
        private ILayoutManager LayoutManager { get; set; }

        /// <summary>
        /// Window menu subitems.
        /// </summary>
        [ImportManyExtension(Workbench.AddIn.ExtensionPoints.MenuTools,
            typeof(IWorkbenchCommand))]
        private List<IWorkbenchCommand> items { get; set; }
        #endregion // MEF Imports

        #region Properties and Associated Member Data
        /// <summary>
        /// 
        /// </summary>
        public List<IExternalTool> Tools
        {
            get;
            set;
        }
        private List<IWorkbenchCommand> m_toolsCommands;
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ToolsMenu()
        {
            this.Header = "_Tools";
            ID = new Guid(Workbench.AddIn.ExtensionPoints.MenuTools);
            RelativeID = new Guid(Workbench.AddIn.ExtensionPoints.MenuWindow);
            Direction = Direction.Before;
            this.Tools = new List<IExternalTool>();
            this.m_toolsCommands = new List<IWorkbenchCommand>();
            Refresh();
        }
        #endregion // Constructor(s)

        #region IPartImportsSatisfiedNotification Methods
        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {
            if (null != items)
                m_toolsCommands.AddRange(items);
            this.Items = ExtensionService.Sort(m_toolsCommands);
        }
        #endregion // IPartImportsSatisfiedNotification Methods

        #region ICommand Implementation
        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
        }
        #endregion // ICommand Implementation

        #region IExternalToolsService Methods
        /// <summary>
        /// 
        /// </summary>
        public void Refresh()
        {
            Guid previous = Guid.Empty;
            this.Tools.Clear();
            this.m_toolsCommands.Clear();
            foreach (String input in Properties.Settings.Default.ExternalTools)
            {
                ExternalTool tool = new ExternalTool(input);
                CustomToolMenuItem mi = new CustomToolMenuItem(tool, previous);
                this.m_toolsCommands.Add(mi);
                previous = mi.ID;
            }
            if (null != ExtensionService)
                OnImportsSatisfied();
        }
        #endregion // IExternalToolsService Methods
    } // ToolsMenu
    
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.MenuTools, typeof(IWorkbenchCommand))]
    class ExternalToolsMenuItem :
        WorkbenchMenuItemBase
    {
        #region Constants
        public static readonly Guid GUID =
            new Guid("899A7B5F-19D4-4302-BDBE-363BCA194015");
        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(CompositionPoints.ExternalToolService, typeof(IExternalToolService))]
        private IExternalToolService ExternalToolService { get; set; }
        #endregion // MEF Imports
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ExternalToolsMenuItem()
        {
            this.Header = "External Tools...";
            this.ID = GUID;
        }
        #endregion // Constructor(s)

        #region ICommand Implementation
        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return (true);
        }

        /// <summary>
        /// Invoke the external tools command.
        /// </summary>
        public override void Execute(Object parameter)
        {
            ExternalToolsView view = new ExternalToolsView();
            ExternalToolCollectionViewModel vm = new ExternalToolCollectionViewModel();
            view.DataContext = vm;
            view.Owner = System.Windows.Application.Current.MainWindow;

            Nullable<bool> dialogResult = view.ShowDialog();
           
            if (true == dialogResult)
                ExternalToolService.Refresh();
        }
        #endregion // ICommand Implementation
    }

    #region CustomToolMenuItem Class
    /// <summary>
    /// CustomToolMenuItem represents a user-specified Custom Tool command line.
    /// </summary>
    class CustomToolMenuItem : 
        WorkbenchMenuItemBase
    {
        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private ExternalTool m_Tool;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public CustomToolMenuItem(ExternalTool tool, Guid previous)
        {
            m_Tool = tool;
            Header = tool.Title;
            ID = Guid.NewGuid();
            RelativeID = previous;
            Direction = Direction.After;
        }
        #endregion // Constructor(s)

        #region ICommand Implementation
        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return (true);
        }

        /// <summary>
        /// Invoke the external tools command.
        /// </summary>
        public override void Execute(Object parameter)
        {
            this.m_Tool.Execute(new Dictionary<String, String>());
        }
        #endregion // ICommand Implementation
    }
    #endregion // CustomToolMenuItem Class
    
    /// <summary>
    /// File, Separator.
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.MenuTools, typeof(IWorkbenchCommand))]
    class ToolsMenuSep1 : WorkbenchMenuItemBase
    {
        #region Constants
        public static readonly Guid GUID = new Guid("97B1C9FA-429E-4853-AF6F-77CD83453C27");
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ToolsMenuSep1()
        {
            this.IsSeparator = true;
            this.ID = GUID;
            this.RelativeID = ExternalToolsMenuItem.GUID;
            this.Direction = Direction.After;
        }
        #endregion // Constructor(s)

        #region ICommand Implementation
        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
        }
        #endregion // ICommand Implementation
    } // FileMenuSep1

    /// <summary>
    /// Metadata Menu
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.MenuTools,
        typeof(IWorkbenchCommand))]
    class ToolsOptionsMenu :
        WorkbenchMenuItemBase
    {
        #region MEF Imports
        /// <summary>
        /// Extensions service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ExtensionService)]
        private IExtensionService ExtensionService { get; set; }

        /// <summary>
        /// Tool window proxys, so that we can create tool windows that are being deserialized
        /// </summary>
        [ImportManyExtension(Workbench.AddIn.ExtensionPoints.Settings, typeof(ISettings))]
        private IEnumerable<ISettings> PreferenceServices { get; set; }

        #endregion // MEF Imports

        public static readonly Guid GUID = new Guid("8FD72B0A-34DB-4BF0-BEA5-644B063CFD05");

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ToolsOptionsMenu()
        {
            this.Header = "_Options...";
            this.ID = GUID;
            this.RelativeID = ToolsMenuSep1.GUID;
            Direction = Direction.After;
        }
        #endregion // Constructor(s)

        #region ICommand Implementation

        public override bool CanExecute(object parameter)
        {
            return true;
        }

        public override void Execute(object parameter)
        {
            SettingsView view = new SettingsView();
            PreferencesViewModel viewModel = new PreferencesViewModel(view, PreferenceServices);

            view.Owner = System.Windows.Application.Current.MainWindow;
            view.DataContext = viewModel;

            view.Show();
        }

        #endregion // ICommand Implementation
    }

} // Workbench.UI.Menu namespace
