﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Report;
using RSG.Model.Report.Reports;
using Workbench.AddIn.Services;
using System.ComponentModel;
using RSG.Base.Logging;
using System.IO;
using RSG.Base.ConfigParser;
using RSG.Model.Common;

namespace Workbench.AddIn.MapStatistics.Reports
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Report.AddIn.ExtensionPoints.MapStatsReport, typeof(IReport))]
    public class MapAssetCSVExportReport : RSG.Model.Report.Reports.Map.MapAssetReport
    {
    } // MapAssetCSVExportReport
} // Workbench.AddIn.MapStatistics.Reports
