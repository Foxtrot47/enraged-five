﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common.Map;
using System.Windows;
using System.ComponentModel;
using RSG.Model.Common;

namespace ContentBrowser.ViewModel.MapViewModels3
{
    /// <summary>
    /// 
    /// </summary>
    public class EntityViewModel : ContainerViewModelBase, IDisposable, IWeakEventListener
    {
        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private IEntity m_entity;

        /// <summary>
        /// Thread synchronisation helper object
        /// </summary>
        private object m_syncObject = new object();
        #endregion // Member Data

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public bool IsDrawableEntity
        {
            get
            {
                return (m_entity.ReferencedArchetype is IDrawableArchetype);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsFragmentEntity
        {
            get
            {
                return (m_entity.ReferencedArchetype is IFragmentArchetype);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsInteriorEntity
        {
            get
            {
                return (m_entity.ReferencedArchetype is IInteriorArchetype);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsStatedAnimEntity
        {
            get
            {
                return (m_entity.ReferencedArchetype is IStatedAnimArchetype);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public IMapArchetype MapArchetype
        {
            get
            {
                return (m_entity.ReferencedArchetype as IMapArchetype);
            }
        }
        #endregion // Properties

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public EntityViewModel(IEntity entity)
            : base(entity)
        {
            m_entity = entity;

            if (MapArchetype != null)
            {
                PropertyChangedEventManager.AddListener(MapArchetype, this, "Textures");
            }
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// This gets called whenever the selection state for this view model
        /// has changed
        /// </summary>
        protected override void OnSelectionChanged(Boolean oldValue, Boolean newValue)
        {
            if (IsSelected)
            {
                lock (m_syncObject)
                {
                    m_entity.RequestAllStatistics(true);
                    if (MapArchetype != null)
                    {
                        MapArchetype.RequestStatistics(new StreamableStat[] { StreamableMapArchetypeStat.Shaders, StreamableArchetypeStat.LodDistance }, true);
                    }
                }
            }

            RefreshGrid();
        }

        /// <summary>
        /// This gets called whenever the expansion state for this view model
        /// has changed
        /// </summary>
        protected override void OnExpansionChanged(Boolean oldValue, Boolean newValue)
        {
            if (IsExpanded)
            {
                lock (m_syncObject)
                {
                    MapArchetype.RequestStatistics(new StreamableStat[] { StreamableMapArchetypeStat.Shaders }, true);
                }
            }

            RefreshHierarchy();
        }
        #endregion // Overrides

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void RefreshHierarchy()
        {
            this.AssetChildren.BeginUpdate();
            this.AssetChildren.Clear();
            if (IsExpanded)
            {
                if (MapArchetype != null)
                {
                    if (MapArchetype.Shaders == null)
                    {
                        this.AssetChildren.Add(new AssetDummyViewModel("loading..."));
                    }
                    else
                    {
                        this.AssetChildren.AddRange(MapArchetype.Textures.Select(texture => ViewModelFactory.CreateViewModel(texture)));
                    }
                }
            }
            else
            {
                this.AssetChildren.Add(new AssetDummyViewModel());
            }
            this.AssetChildren.EndUpdate();
        }

        /// <summary>
        /// 
        /// </summary>
        private void RefreshGrid()
        {
            this.GridAssets.BeginUpdate();
            this.GridAssets.Clear();
            if (IsSelected)
            {
                if (MapArchetype != null && MapArchetype.Shaders != null)
                {
                    this.GridAssets.AddRange(MapArchetype.Textures.Select(texture => ViewModelFactory.CreateGridViewModel(texture)));
                }
            }
            this.GridAssets.EndUpdate();
        }
        #endregion // Private Methods

        #region IWeakEventListener Implementation
        /// <summary>
        /// 
        /// </summary>
        public bool ReceiveWeakEvent(Type managerType, Object sender, EventArgs e)
        {
            if (Application.Current != null)
            {
                Application.Current.Dispatcher.Invoke(
                    new Action
                    (
                        delegate()
                        {
                            RefreshHierarchy();
                            RefreshGrid();
                        }
                    ),
                    System.Windows.Threading.DispatcherPriority.ContextIdle
                );
            }
            else
            {
                RefreshHierarchy();
                RefreshGrid();
            }
            return true;
        }
        #endregion // IWeakEventListener

        #region IDisposable Implementation
        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            if (MapArchetype != null)
            {
                PropertyChangedEventManager.RemoveListener(MapArchetype, this, "Textures");
            }
        }
        #endregion // IDisposable Implementation
    } // EntityViewModel
}
