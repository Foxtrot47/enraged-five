﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.IO;
using System.Windows.Media;
using RSG.Base.Collections;
using RSG.Base.Math;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using RSG.Base.Windows.Helpers;
using RSG.Bounds;
using RSG.Model.Common.Extensions;
using RSG.Model.Common.Map;
using RSG.Model.Common.Util;
using MapViewport.AddIn;

namespace Workbench.AddIn.MapStatistics.Overlays.Bounds
{
    public class BoundPrimitiveInfo
    {
        public BoundPrimitiveInfo()
        {
            NumBakedInstances = 0;
            NumBakedPolys = 0;

            NumBoxes = 0;
            NumBVHs = 0;
            NumCapsules = 0;
            NumComposites = 0;
            NumCylinders = 0;
            NumGeometries = 0;
            NumSpheres = 0;
            NumTotalPrimitives = 0;
            NumTotalCollisionPolygons = 0;
        }

        public void SetTotalPrimitives()
        {
            NumTotalPrimitives = NumBoxes + NumBVHs + NumCapsules + NumComposites + NumCylinders + NumGeometries + NumSpheres;
        }

        public uint NumBakedInstances { get; set; }
        public uint NumBakedPolys { get; set; }

        public uint NumBoxes { get; set; }
        public uint NumBVHs { get; set; }
        public uint NumCapsules { get; set; }
        public uint NumComposites { get; set; }
        public uint NumCylinders { get; set; }
        public uint NumGeometries { get; set; }
        public uint NumSpheres { get; set; }
        public uint NumTotalPrimitives { get; set; }
        public uint NumTotalCollisionPolygons { get; set; }
    }

    /// <summary>
    /// Shows an overlay of primitives for each  bounds.
    /// </summary>
    [ExportExtension(ExtensionPoints.BoundsStatisticsOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class BoundPrimitivesOverlay : BoundsOverlayBase
    {
        #region Constants
        private const string c_name = "Primitive Bounds";
        private const string c_description = "Shows an overlay of bound primitives for each map section.";
        #endregion

        #region MEF Imports
        /// <summary>
        /// A reference to view model for the map viewport, used to get the currently selected geometry
        /// </summary>
        [ImportExtension(Viewport.AddIn.CompositionPoints.MapViewport, typeof(Viewport.AddIn.IMapViewport))]
        Lazy<Viewport.AddIn.IMapViewport> MapViewport { get; set; }
        #endregion // MEF Imports

        #region Properties

        /// <summary>
        /// Bound information for the selected section
        /// </summary>
        public BoundPrimitiveInfo SelectedSectionBoundInfo
        {
            get { return m_selectedSectionBoundInfo; }
            set
            {
                SetPropertyValue(value, () => SelectedSectionBoundInfo,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_selectedSectionBoundInfo = (BoundPrimitiveInfo)newValue;
                        }
                ));
            }
        }
        private BoundPrimitiveInfo m_selectedSectionBoundInfo;

        /// <summary>
        /// Keep previous selection if this is set to true. This will allow multiple sections to be drawn on the map.
        /// </summary>
        public bool ShowAllSelected
        {
            get { return m_keepPreviousSelection; }
            set
            {
                SetPropertyValue(value, () => ShowAllSelected,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_keepPreviousSelection = (bool)newValue;
                            OnSelectedMapSectionChanged();
                        }
                ));
            }
        }
        private bool m_keepPreviousSelection;

        /// <summary>
        /// Dictionary cache of bound files loaded for a particular map section.
        /// </summary>
        private Dictionary<IMapSection, List<BNDFile>> m_loadedBoundFiles;

        /// <summary>
        /// The name of the selected section
        /// </summary>
        public string SelectedSectionName
        {
            get { return m_selectedSectionName; }
            set
            {
                SetPropertyValue(value, () => SelectedSectionName,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_selectedSectionName = (string)newValue;
                        }
                ));
            }
        }
        private string m_selectedSectionName;

        /// <summary>
        /// Dictionary cache of section collision details
        /// </summary>
        private Dictionary<IMapSection, BoundPrimitiveInfo> SectionBoundsDetails
        {
            get;
            set;
        }

        /// <summary>
        /// Dictionary cache of map viewport geometry objects.
        /// </summary>
        private Dictionary<IMapSection, Viewport2DShape> SectionGeometry
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default Constructor
        /// </summary>
        public BoundPrimitivesOverlay()
            : base(c_name, c_description)
        {
            m_loadedBoundFiles = new Dictionary<IMapSection, List<BNDFile>>();
            SectionBoundsDetails = new Dictionary<IMapSection, BoundPrimitiveInfo>();
            SectionGeometry = new Dictionary<IMapSection, Viewport2DShape>();
            ShowAllSelected = false;
        }
        #endregion // Constructor(s)

        #region Event Callbacks
        /// <summary>
        /// Gets called when map geometry selection changes
        /// </summary>
        private void SelectedOverlayGeometryChanged(Object sender, NotifyCollectionChangedEventArgs e)
        {
            // Display details for the first item in the selected geometry
            IMapSection selectedSection = MapViewport.Value.SelectedOverlayGeometry.FirstOrDefault() as IMapSection;

            if (selectedSection == null)
            {
                SelectedSectionName = "";
                SelectedSectionBoundInfo = new BoundPrimitiveInfo();
            }
            else
            {
                SelectedSectionName = selectedSection.Name;
                SelectedSectionBoundInfo = SectionBoundsDetails[selectedSection];
            }
        }
        #endregion // Event Callbacks

        #region Overrides

        /// <summary>
        /// Gets called when the user wants to view this overlay
        /// </summary>
        public override void Activated()
        {
            base.Activated();
            RefreshOverlay();
            MapViewport.Value.SelectedOverlayGeometry.CollectionChanged += SelectedOverlayGeometryChanged;
        }

        /// <summary>
        /// Gets called when the user turns off this overlay
        /// </summary>
        public override void Deactivated()
        {
            MapViewport.Value.SelectedOverlayGeometry.CollectionChanged -= SelectedOverlayGeometryChanged;
            SectionBoundsDetails.Clear();
            m_loadedBoundFiles.Clear();
            base.Deactivated();
        }

        /// <summary>
        /// Gets the control that should be shown in the overlay details panel
        /// </summary>
        public override System.Windows.Controls.Control GetOverlayControl()
        {
            return new BoundPrimitivesOverlayView();
        }

        /// <summary>
        /// Called when the selected map section changes
        /// </summary>
        protected override void OnSelectedMapSectionChanged()
        {
            // Make sure a map section was selected
            if (SelectedMapSection != null)
            {
                // Load in all statistics related to this bound.
                SelectedMapSection.LoadAllStats();

                // Load in the relevant bounds files; cache this data.
                if (m_loadedBoundFiles.ContainsKey(SelectedMapSection) == false)
                {
                    m_loadedBoundFiles.Add(SelectedMapSection, LoadBoundsDataForSection(SelectedMapSection));
                }
            }

            // Update the overlay
            RefreshOverlay();
        }

        /// <summary>
        /// Function that is called when the user hits "Refresh Overlay" in the overlay details view
        /// </summary>
        protected override void RefreshOverlay()
        {
            // Update the overlay image
            OverlayImage.ClearImage();
            OverlayImage.BeginUpdate();

            if (SelectedMapSection != null)
            {
                BoundPrimitiveInfo info = GatherBoundPrimitiveInfo();
                SelectedSectionBoundInfo = info;
                SelectedSectionName = SelectedMapSection.Name;
            }
            else
            {
                SelectedSectionBoundInfo = new BoundPrimitiveInfo();
                SelectedSectionName = "";
            }

            OverlayImage.EndUpdate();

            // Generate/Update the overlay geometry
            GenerateOverlayGeometry();
        }
        #endregion // Overrides

        #region Private Methods

        /// <summary>
        /// Gather all information related to this overlay.
        /// </summary>
        private BoundPrimitiveInfo GatherBoundPrimitiveInfo()
        {
            if (SectionBoundsDetails.ContainsKey(SelectedMapSection) == false)
            {
                SectionBoundsDetails.Add(SelectedMapSection, new BoundPrimitiveInfo());
                SectionBoundsDetails[SelectedMapSection].NumTotalCollisionPolygons += (uint)SelectedMapSection.CollisionPolygonCount;

                //If the map section does not export entities, then tally up the archetype's data.
                if (SelectedMapSection.ExportEntities == false)
                {
                    foreach (IMapArchetype mapArchetype in SelectedMapSection.Archetypes)
                    {
                        ISimpleMapArchetype archetype = mapArchetype as ISimpleMapArchetype;

                        //Based on the archetype's collision group, find the bounds file.
                        string boundFileName = archetype.Name;
                        if (archetype.HasDefaultCollisionGroup == false)
                        {
                            boundFileName = archetype.CollisionGroup;
                        }

                        foreach (BNDFile boundFile in m_loadedBoundFiles[SelectedMapSection])
                        {
                            if (String.Compare(Path.GetFileNameWithoutExtension(boundFile.FileName), boundFileName, true) == 0)
                            {
                                ProcessBoundObject(boundFile.BoundObject);
                                ProcessBNDFile(boundFile);
                            }
                        }

                        if (archetype.ForceBakeCollision == true && archetype.NeverBakeCollision == false)
                        {
                            SectionBoundsDetails[SelectedMapSection].NumBakedInstances++;
                            SectionBoundsDetails[SelectedMapSection].NumBakedPolys += (uint)archetype.CollisionPolygonCount;
                            SectionBoundsDetails[SelectedMapSection].NumTotalCollisionPolygons += (uint)archetype.CollisionPolygonCount;
                        }
                    }
                }
                else
                {
                    //Load all bounds associated with this container.
                    foreach (BNDFile boundFile in m_loadedBoundFiles[SelectedMapSection])
                    {
                        ProcessBoundObject(boundFile.BoundObject);
                        ProcessBNDFile(boundFile);
                    }

                    foreach (IEntity entity in SelectedMapSection.ChildEntities.Where(item => item.ReferencedArchetype != null))
                    {
                        ISimpleMapArchetype archetype = entity.ReferencedArchetype as ISimpleMapArchetype;

                        if (archetype != null)
                        {
                            //Load any bound files associated with this archetype's map section.
                            if (m_loadedBoundFiles.ContainsKey(archetype.ParentSection) == false)
                            {
                                List<BNDFile> bndFiles = LoadBoundsDataForSection(archetype.ParentSection);
                                m_loadedBoundFiles.Add(archetype.ParentSection, bndFiles);
                            }

                            //Based on the archetype's collision group, find the bounds file.
                            string boundFileName = archetype.Name;
                            if (archetype.HasDefaultCollisionGroup == false)
                            {
                                boundFileName = archetype.CollisionGroup;
                            }

                            foreach (BNDFile boundFile in m_loadedBoundFiles[archetype.ParentSection])
                            {
                                if (String.Compare(Path.GetFileNameWithoutExtension(boundFile.FileName), boundFileName, true) == 0)
                                {
                                    ProcessBoundObject(boundFile.BoundObject);
                                    ProcessBNDFile(boundFile);
                                }
                            }

                            if (entity.ForceBakeCollision == true || (archetype.ForceBakeCollision == true && archetype.NeverBakeCollision == false))
                            {
                                SectionBoundsDetails[SelectedMapSection].NumBakedInstances++;
                                SectionBoundsDetails[SelectedMapSection].NumBakedPolys += (uint)archetype.CollisionPolygonCount;
                                SectionBoundsDetails[SelectedMapSection].NumTotalCollisionPolygons += (uint)archetype.CollisionPolygonCount;
                            }
                        }
                    }
                }

                SectionBoundsDetails[SelectedMapSection].SetTotalPrimitives();
            }

            return SectionBoundsDetails[SelectedMapSection];
        }

        /// <summary>
        /// Processes a single bound object
        /// </summary>
        /// <param name="obj"></param>
        private void ProcessBoundObject(BoundObject obj)
        {
            if (obj is Composite)
            {
                Composite composite = (Composite)obj;

                foreach (CompositeChild child in composite.Children)
                {
                    ProcessBoundObject(child.BoundObject);
                }
            }
            else
            {
                // Convert the bound object to a BVH and then process
                BVH bvh = BoundObjectConverter.ToBVH(obj);
                ProcessBVH(bvh);
            }
        }

        /// <summary>
        /// Processes a single bvh
        /// </summary>
        /// <param name="bvh"></param>
        private void ProcessBVH(BVH bvh)
        {
            foreach (BVHPrimitive prim in bvh.Primitives)
            {
                if (prim is BVHTriangle)
                {
                    BVHTriangle tri = (BVHTriangle)prim;

                    Vector2f[] outline = new Vector2f[3];
                    outline[0] = new Vector2f(bvh.Verts[tri.Index0].Position.X, bvh.Verts[tri.Index0].Position.Y);
                    outline[1] = new Vector2f(bvh.Verts[tri.Index1].Position.X, bvh.Verts[tri.Index1].Position.Y);
                    outline[2] = new Vector2f(bvh.Verts[tri.Index2].Position.X, bvh.Verts[tri.Index2].Position.Y);

                    OverlayImage.RenderPolygon(outline, System.Drawing.Color.LightCoral);
                }
            }
        }

        /// <summary>
        /// Acquires statistics from the bounds file.
        /// </summary>
        /// <param name="file"></param>
        private void ProcessBNDFile(BNDFile file)
        {
            if (file.BoundObject.GetType() == typeof(RSG.Bounds.Box))
            {
                SectionBoundsDetails[SelectedMapSection].NumBoxes++;
            }
            else if (file.BoundObject.GetType() == typeof(RSG.Bounds.BVHPrimitive))
            {
                SectionBoundsDetails[SelectedMapSection].NumBVHs++;
            }
            else if (file.BoundObject.GetType() == typeof(RSG.Bounds.Capsule))
            {
                SectionBoundsDetails[SelectedMapSection].NumCapsules++;
            }
            else if (file.BoundObject.GetType() == typeof(RSG.Bounds.Composite))
            {
                SectionBoundsDetails[SelectedMapSection].NumComposites++;

                RSG.Bounds.Composite composite = file.BoundObject as RSG.Bounds.Composite;
                ProcessCompositeBound(composite);
            }
            else if (file.BoundObject.GetType() == typeof(RSG.Bounds.Cylinder))
            {
                SectionBoundsDetails[SelectedMapSection].NumCylinders++;
            }
            else if (file.BoundObject.GetType() == typeof(RSG.Bounds.Geometry))
            {
                SectionBoundsDetails[SelectedMapSection].NumGeometries++;
            }
            else if (file.BoundObject.GetType() == typeof(RSG.Bounds.Sphere))
            {
                SectionBoundsDetails[SelectedMapSection].NumSpheres++;
            }
        }

        /// <summary>
        /// Recursively parse for primitves.
        /// </summary>
        /// <param name="composite"></param>
        private void ProcessCompositeBound(RSG.Bounds.Composite composite)
        {
            foreach (RSG.Bounds.CompositeChild child in composite.Children)
            {
                if (child.BoundObject.GetType() == typeof(RSG.Bounds.Box))
                {
                    SectionBoundsDetails[SelectedMapSection].NumBoxes++;
                }
                else if (child.BoundObject.GetType() == typeof(RSG.Bounds.BVHPrimitive))
                {
                    SectionBoundsDetails[SelectedMapSection].NumBVHs++;
                }
                else if (child.BoundObject.GetType() == typeof(RSG.Bounds.Capsule))
                {
                    SectionBoundsDetails[SelectedMapSection].NumCapsules++;
                }
                else if (child.BoundObject.GetType() == typeof(RSG.Bounds.Composite))
                {
                    RSG.Bounds.Composite compositeChild = child.BoundObject as RSG.Bounds.Composite;
                    ProcessCompositeBound(compositeChild);
                }
                else if (child.BoundObject.GetType() == typeof(RSG.Bounds.Cylinder))
                {
                    SectionBoundsDetails[SelectedMapSection].NumCylinders++;
                }
                else if (child.BoundObject.GetType() == typeof(RSG.Bounds.Geometry))
                {
                    SectionBoundsDetails[SelectedMapSection].NumGeometries++;
                }
                else if (child.BoundObject.GetType() == typeof(RSG.Bounds.Sphere))
                {
                    SectionBoundsDetails[SelectedMapSection].NumSpheres++;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="values"></param>
        /// <param name="minDensity"></param>
        /// <param name="maxDensity"></param>
        private void GenerateOverlayGeometry()
        {
            // Render the geometry (based on the user's options)
            Geometry.Clear();
            Geometry.BeginUpdate();

            List<Viewport2DShape> geometries = new List<Viewport2DShape>();

            foreach (KeyValuePair<IMapSection, BoundPrimitiveInfo> pair in SectionBoundsDetails)
            {
                if (!ShowAllSelected)
                {
                    if (pair.Key != SelectedMapSection)
                    {
                        continue;
                    }
                }


                Color colour = Color.FromArgb(255, 128, 128, 0);

                Viewport2DShape sectionGeometry = null;

                // Check if we can reuse a piece of geometry for rendering this section's density
                if (!SectionGeometry.ContainsKey(pair.Key))
                {
                    if (pair.Key.VectorMapPoints == null)
                    {
                        continue;
                    }

                    sectionGeometry = new Viewport2DShape(etCoordSpace.World, "Filled shape for section", pair.Key.VectorMapPoints.ToArray(), Colors.Black, 1, colour);
                    sectionGeometry.PickData = pair.Key;

                    // Keep track of the geometry
                    SectionGeometry.Add(pair.Key, sectionGeometry);
                }
                else
                {
                    sectionGeometry = SectionGeometry[pair.Key];
                }

                // Update the geometry's details
                sectionGeometry.UserText = pair.Value.NumTotalPrimitives.ToString();
                sectionGeometry.FillColour = colour;

                Geometry.Add(sectionGeometry);
            }

            Geometry.EndUpdate();
        }

        #endregion // Private Methods
    } // BoundExtentsOverlay
} // Workbench.AddIn.MapStatistics.Overlays.Bounds
