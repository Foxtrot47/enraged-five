﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ContentBrowser.AddIn;
using Workbench.AddIn.Services;
using RSG.Model.Common;
using RSG.Base.Extensions;
using Workbench.AddIn.MapDebugging.OverlayViews;
using System.Windows;
using RSG.Base.Logging;
using System.ComponentModel;
using RSG.Base.Math;
using RSG.Base.Windows.Helpers;
using WidgetEditor.AddIn;
using RSG.Model.Common.Map;
using RSG.Model.Common.Extensions;
using RSG.Base.Tasks;
using RSG.Base.Windows.Dialogs;
using System.Threading;
using RSG.Model.Common.Util;
using MapViewport.AddIn;
using Workbench.AddIn.Services.Model;

namespace Workbench.AddIn.MapDebugging.Overlays
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension("Workbench.AddIn.MapDebugging.OverlayGroups.MapDebug", typeof(Viewport.AddIn.ViewportOverlay))]
    public class ObjectIntersectionOverlay : LevelDependentViewportOverlay
    {
        #region Constants
        private const string NAME = "Object Intersections";
        private const string DESC = "Shows an overlay of where map instances are intersecting one another.";

        private const string RAG_WARPCOORDINATES = "Debug/Warp Player x y z h vx vy vz";
        private const string RAG_WARPNOWBUTTON = "Debug/Warp now";
        #endregion

        #region Classes
        /// <summary>
        /// 
        /// </summary>
        public class IntersectingEntityPair : IEquatable<IntersectingEntityPair>, IComparable<IntersectingEntityPair>, IComparable 
        {
            public IEntity EntityA { get; private set; }
            public IEntity EntityB { get; private set; }

            public IntersectingEntityPair(IEntity a, IEntity b)
            {
                EntityA = a;
                EntityB = b;
            }

            public override string ToString()
            {
                return String.Format("({0},{1})", EntityA, EntityB);
            }

            public override bool Equals(object obj)
            {
                if (obj == null)
                {
                    return base.Equals(obj);
                }

                return ((obj is IntersectingEntityPair) && Equals(obj as IntersectingEntityPair));
            }

            public override int GetHashCode()
            {
                return ((EntityA.GetHashCode() * 2) ^ EntityB.GetHashCode());
            }

            public bool Equals(IntersectingEntityPair other)
            {
                if (other == null)
                {
                    return false;
                }

                return (EntityA == other.EntityA && EntityB == other.EntityB);
            }

            public int CompareTo(IntersectingEntityPair other)
            {
                if (EntityA == other.EntityA)
                {
                    return EntityB.CompareTo(EntityA);
                }
                return EntityA.CompareTo(EntityB);
            }

            public int CompareTo(object obj)
            {
                if (obj == null) return 1;

                IntersectingEntityPair other = obj as IntersectingEntityPair;
                if (other != null)
                    return CompareTo(other);
                else
                    throw new ArgumentException("Object is not a IntersectingEntityPair");
            }
        }
        #endregion // Classes

        #region MEF Imports
        /// <summary>
        /// Content Browser
        /// </summary>
        [ImportExtension(ContentBrowser.AddIn.CompositionPoints.ContentBrowser, typeof(IContentBrowser))]
        private IContentBrowser ContentBrowserProxy { get; set; }

        /// <summary>
        /// A reference to view model for the map viewport
        /// </summary>
        [ImportExtension(Viewport.AddIn.CompositionPoints.MapViewport, typeof(Viewport.AddIn.IMapViewport))]
        private Viewport.AddIn.IMapViewport MapViewport { get; set; }

        /// <summary>
        /// Progress service for displaying while gathering the data
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ProgressService, typeof(Workbench.AddIn.Services.IProgressService))]
        private IProgressService ProgressService { get; set; }

        /// <summary>
        /// MEF import for proxy UI
        /// </summary>
        [ImportExtension(WidgetEditor.AddIn.CompositionPoints.ProxyService, typeof(IProxyService))]
        private Lazy<IProxyService> ProxyService { get; set; }

        /// <summary>
        /// MEF import for message box service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.MessageService, typeof(Workbench.AddIn.Services.IMessageService))]
        private Lazy<IMessageService> MessageService { get; set; }
        #endregion // MEF Imports

        #region Properties
        /// <summary>
        /// Threshold to use when doing th comparisons
        /// </summary>
        public float Threshold
        {
            get { return m_threshold; }
            set
            {
                SetPropertyValue(value, () => Threshold,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_threshold = (float)newValue;
                        }
                ));
            }
        }
        private float m_threshold;

        /// <summary>
        /// 
        /// </summary>
        public ISet<IntersectingEntityPair> IntersectingEntities
        {
            get { return m_intersectingEntities; }
            set
            {
                SetPropertyValue(value, () => IntersectingEntities,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_intersectingEntities = (ISet<IntersectingEntityPair>)newValue;
                        }
                ));
            }
        }
        private ISet<IntersectingEntityPair> m_intersectingEntities;

        /// <summary>
        /// Command that gets fired off when the "Warp to in Game" button is pressed for the selected map instance
        /// </summary>
        public RelayCommand WarpToInGameCommand
        {
            get
            {
                if (m_warpToInGameCommand == null)
                {
                    m_warpToInGameCommand = new RelayCommand(param => WarpToInGame(param));
                }

                return m_warpToInGameCommand;
            }
        }
        private RelayCommand m_warpToInGameCommand;
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// Default Constructor
        /// </summary>
        public ObjectIntersectionOverlay()
            : base(NAME, DESC)
        {
            Threshold = 0.05f;
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// Gets called when the user wants to view this overlay
        /// </summary>
        public override void Activated()
        {
            base.Activated();
            ContentBrowserProxy.GridSelectionChanged += GridSelectionChanged;
            SelectionChanged();
        }

        /// <summary>
        /// Gets called when the user turns off this overlay
        /// </summary>
        public override void Deactivated()
        {
            ContentBrowserProxy.GridSelectionChanged -= GridSelectionChanged;
            base.Deactivated();
        }

        /// <summary>
        /// Gets the control that should be shown in the overlay details panel
        /// </summary>
        public override System.Windows.Controls.Control GetOverlayControl()
        {
            return new ObjectIntersectionOverlayView();
        }
        #endregion // Overrides

        #region Event Handling
        /// <summary>
        /// Get called when the items selected in the content browser change
        /// </summary>
        private void GridSelectionChanged(Object sender, ContentBrowser.AddIn.GridSelectionChangedEventArgs args)
        {
            SelectionChanged();
        }

        /// <summary>
        /// Get called when the items selected in the content browser change and when
        /// the overlay is first shown
        /// </summary>
        private void SelectionChanged()
        {
            Geometry.Clear();
            IntersectingEntities = new HashSet<IntersectingEntityPair>();

            if (ContentBrowserProxy.SelectedGridItems != null && ContentBrowserProxy.SelectedGridItems.Count() > 0)
            {
                if (InitialiseAllSections())
                {
                    // Create a background worker for gathering the object intersection data
                    BackgroundWorker worker = new BackgroundWorker();
                    worker.DoWork += GatherObjectIntersectionData;
                    worker.RunWorkerCompleted += UpdateOverlay;

                    // Check that the user didn't cancel the operation
                    ProgressService.Show(worker, "Gathering Intersection Data", "The workbench is currently gathering the data required to show intersections on the overlay.");
                }
            }
        }
        #endregion // Event Handling

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool InitialiseAllSections()
        {
            // Make sure that the sections have the required data loaded
            CancellationTokenSource cts = new CancellationTokenSource();
            LevelTaskContext context = new LevelTaskContext(cts.Token, LevelBrowserProxy.Value.SelectedLevel);

            ITask loadDataTask = new ActionTask("Load Data", (ctx, progress) => EnsureDataLoaded((LevelTaskContext)ctx, progress));

            TaskExecutionDialog dialog = new TaskExecutionDialog();
            TaskExecutionViewModel vm = new TaskExecutionViewModel("Generating Overlay", loadDataTask, cts, context);
            vm.AutoCloseOnCompletion = true;
            dialog.DataContext = vm;
            Nullable<bool> selectionResult = dialog.ShowDialog();
            if (false == selectionResult)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="progress"></param>
        private void EnsureDataLoaded(LevelTaskContext context, IProgress<TaskProgress> progress)
        {
            IMapHierarchy hierarchy = context.MapHierarchy;

            if (hierarchy != null)
            {
                float increment = 1.0f / hierarchy.AllSections.Count();

                // Load all the map section data
                foreach (IMapSection section in hierarchy.AllSections)
                {
                    context.Token.ThrowIfCancellationRequested();

                    // Update the progress information
                    string message = String.Format("Loading {0}", section.Name);
                    progress.Report(new TaskProgress(increment, true, message));

                    // Only request that the entities for this section are loaded
                    section.LoadStats(new StreamableStat[] { StreamableMapSectionStat.Entities });

                    // Request the locations/bounding boxes of all the entities
                    /*
                    foreach (IEntity entity in section.ChildEntities)
                    {
                        entity.RequestStatistics(new StreamableEntityStat[] { StreamableEntityStat.Position, StreamableEntityStat.BoundingBox}, false);
                    }
                    */
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GatherObjectIntersectionData(Object sender, DoWorkEventArgs e)
        {
            // Get the BackgroundWorker that raised this event.
            BackgroundWorker worker = sender as BackgroundWorker;

            IMapHierarchy hierarchy = LevelBrowserProxy.Value.SelectedLevel.GetMapHierarchy();

            if (hierarchy != null)
            {
                // Get the selected sections
                IEnumerable<IMapSection> allSections = hierarchy.AllSections;
                ISet<IMapSection> selectedSections = new HashSet<IMapSection>();

                foreach (IMapNode mapNode in ContentBrowserProxy.SelectedGridItems.Where(item => item is IMapNode))
                {
                    if (mapNode is IMapSection)
                    {
                        selectedSections.Add((IMapSection)mapNode);
                    }
                    else if (mapNode is IMapArea)
                    {
                        selectedSections.AddRange((mapNode as IMapArea).AllDescendentSections);
                    }
                }

                // Get a list of all instances and a list of instances in the selected sections
                IEnumerable<IEntity> allEntites = allSections.AsParallel()
                                                             .SelectMany(section => section.ChildEntities)
                                                             .Where(item => item.ReferencedArchetype is ISimpleMapArchetype && (item.ReferencedArchetype as ISimpleMapArchetype).IsDynamic == true);

                IEnumerable<IEntity> selectedEntities = selectedSections.AsParallel()
                                                                        .SelectMany(section => section.ChildEntities)
                                                                        .Where(item => item.ReferencedArchetype is ISimpleMapArchetype && (item.ReferencedArchetype as ISimpleMapArchetype).IsDynamic == true);

                // Check each of the instances in the selected sections against all other instances
                ISet<IntersectingEntityPair> intersectingEntities = new HashSet<IntersectingEntityPair>();

                float percentPerInstance = 100.0f / selectedEntities.Count();
                uint idx = 0;
                Vector3f thresholdVec = new Vector3f(Threshold, Threshold, Threshold);

                foreach (IEntity entity in selectedEntities)
                {
                    // Adjust the instance's bounding box by the threshold value in each direction
                    BoundingBox3f bbox = new BoundingBox3f(entity.BoundingBox);
                    bbox.Min -= thresholdVec;
                    bbox.Max += thresholdVec;

                    // Check this instance's bounding box against all other instances
                    foreach (IEntity otherEntity in allEntites)
                    {
                        if (entity != otherEntity &&
                            bbox.Intersects(otherEntity.BoundingBox))
                        {
                            intersectingEntities.Add(new IntersectingEntityPair(entity, otherEntity));
                        }
                    }

                    // Report progress and check if the user cancelled the operation
                    float percentage = ++idx * percentPerInstance;
                    worker.ReportProgress((int)System.Math.Ceiling(percentage));
                    if (e.Cancel)
                    {
                        break;
                    }

                    if (worker.CancellationPending == true)
                    {
                        e.Cancel = true;
                        break;
                    }
                }

                e.Result = intersectingEntities;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdateOverlay(Object sender, RunWorkerCompletedEventArgs e)
        {
            this.Geometry.BeginUpdate();
            this.Geometry.Clear();

            if (!e.Cancelled)
            {
                ISet<IntersectingEntityPair> intersectingInstances = (ISet<IntersectingEntityPair>)e.Result;
                IntersectingEntities = intersectingInstances;
            }

            this.Geometry.EndUpdate();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="instance"></param>
        private void WarpToInGame(object instanceObj)
        {
            IEntity instance = instanceObj as IEntity;
            if (instance != null)
            {
                Vector3f position = new Vector3f(instance.Position);
                position.X -= 2.0f;
                position.Z += 1.0f;

                float heading = -(float)Math.PI / 2.0f;

                // Check whether the RAG proxy is connected
                if (!ProxyService.Value.IsGameConnected)
                {
                    MessageService.Value.Show("RAG is not connected. Is the game running?");
                }
                else
                {
                    ProxyService.Value.Console.WriteStringWidget(RAG_WARPCOORDINATES, String.Format("{0} {1} {2} {3}", position.X, position.Y, position.Z, heading));
                    ProxyService.Value.Console.PressWidgetButton(RAG_WARPNOWBUTTON);
                }
            }
        }
        #endregion // Private Methods
    } // ObjectIntersectionOverlay
}
