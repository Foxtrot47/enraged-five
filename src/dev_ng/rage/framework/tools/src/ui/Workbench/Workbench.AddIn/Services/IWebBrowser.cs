﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Workbench.AddIn.Services
{
    public interface IWebBrowserService
    {
        void OpenNewTabAtAddress(string address);
    }
}
