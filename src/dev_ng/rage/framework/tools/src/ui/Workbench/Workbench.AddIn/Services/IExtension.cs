﻿using System;
using System.Collections.Generic;
using RSG.Base.Editor;

namespace Workbench.AddIn.Services
{

    /// <summary>
    /// Extension interface.
    /// </summary>
    public interface IExtension : IViewModel
    {
        Guid ID { get; }
        Guid RelativeID { get; }
        Direction Direction { get; }
    }

    /// <summary>
    /// 
    /// </summary>
    public enum Direction
    {
        After,
        Before,
    }

    /// <summary>
    /// Basic IExtension comparer class; simply compares the extension GUIDs.
    /// </summary>
    public class ExtensionComparer : IComparer<IExtension>
    {
        #region IComparer Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public int Compare(IExtension a, IExtension b)
        {
            return (a.ID.CompareTo(b.ID));
        }
        #endregion // IComparer Methods
    }

} // Workbench.AddIn.Services
