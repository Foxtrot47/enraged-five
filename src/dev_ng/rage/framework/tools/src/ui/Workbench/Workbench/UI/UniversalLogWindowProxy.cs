﻿using System;
using Workbench.AddIn;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.Services;
using System.Collections.Generic;
using System.ComponentModel.Composition;

namespace Workbench.UI
{
    [ExportExtension(ExtensionPoints.ToolWindowProxy, typeof(IToolWindowProxy))]
    class UniversalLogWindowProxy : IToolWindowProxy, IPartImportsSatisfiedNotification
    {
        #region Imports
        /// <summary>
        /// MEF import for workbench universal log views
        /// </summary>
        [ImportManyExtension(Workbench.AddIn.ExtensionPoints.LoggingOutput, typeof(ILoggingService))]
        private IEnumerable<ILoggingService> LoggingOutputs { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(IConfigurationService))]
        private IConfigurationService Config { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.WebBrowserService, typeof(IWebBrowserService))]
        private IWebBrowserService WebBrowser { get; set; }
        #endregion

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public UniversalLogWindowProxy()
        {
            this.Name = "Universal_Log_Output";
            this.Header = "Universal Log Output";
            this.DelayCreation = false;
        }
        #endregion // Constructor

        #region Public Functions
        /// <summary>
        /// A abstract function that is overriden by the plugin to create the
        /// actual instance of the tool window and pass it back as a out paramter
        /// </summary>
        protected override Boolean CreateToolWindow(out IToolWindowBase toolWindow)
        {
            toolWindow = new UniversalLogWindowView(LoggingOutputs, Config, WebBrowser);
            return true;
        }
        #endregion // Public Functions

        #region IPartImportsSatisfiedNotification Methods
        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {
        }
        #endregion // IPartImportsSatisfiedNotification Methods
    } // UniversalLogWindowProxy
} // Workbench.UI
