﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Windows.Helpers;
using RSG.Model.Common;
using RSG.Statistics.Common.Dto.GameAssets;

namespace Workbench.AddIn.Services.Model
{
    /// <summary>
    /// 
    /// </summary>
    public interface IDataContextProvider
    {
        #region Events
        /// <summary>
        /// Fired when the selected data context changes.
        /// </summary>
        event DataContextChangedEventHandler DataContextChanged;
        #endregion // Events

        #region Properties
        /// <summary>
        /// Current data context.
        /// </summary>
        DataContext CurrentContext { get; }
        #endregion // Properties
    } // IDataContextProvider


    /// <summary>
    /// Context for the se
    /// </summary>
    public class DataContext
    {
        /// <summary>
        /// Selected datasource.
        /// </summary>
        public DataSource DataSource { get; set; }

        /// <summary>
        /// Selected build (only valid if the datasource is set to Database).
        /// </summary>
        public BuildDto Build { get; set; }

        /// <summary>
        /// Selected platform.
        /// </summary>
        public RSG.Platform.Platform Platform { get; set; }

        /// <summary>
        /// Selected level.
        /// </summary>
        public ILevel Level { get; set; }
    } // DataContext


    /// <summary>
    /// Platform changed
    /// </summary>
    public class DataContextChangedEventArgs : ItemChangedEventArgs<DataContext>
    {
        public DataContextChangedEventArgs(DataContext oldContext, DataContext newContext)
            : base(oldContext, newContext)
        {
        }
    } // DataContextChangedEventArgs


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="args"></param>
    public delegate void DataContextChangedEventHandler(Object sender, DataContextChangedEventArgs args);
}
