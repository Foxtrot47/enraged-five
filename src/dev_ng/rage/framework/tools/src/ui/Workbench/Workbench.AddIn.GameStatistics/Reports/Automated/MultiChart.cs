﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections;
using System.Windows.Controls.DataVisualization.Charting;
using System.Collections.Specialized;

namespace Workbench.AddIn.GameStatistics.Reports.Automated
{
    /// <summary>
    /// Based off of the code from:
    /// http://blog.thekieners.com/2010/02/07/databinding-multi-series-charts/
    /// </summary>
    public class MultiChart : System.Windows.Controls.DataVisualization.Charting.Chart, IWeakEventListener
    {
        #region SeriesSource (DependencyProperty)

        public IEnumerable SeriesSource
        {
            get { return (IEnumerable)GetValue(SeriesSourceProperty); }
            set { SetValue(SeriesSourceProperty, value); }
        }

        public static readonly DependencyProperty SeriesSourceProperty = DependencyProperty.Register("SeriesSource", typeof(IEnumerable), typeof(MultiChart), new PropertyMetadata(default(IEnumerable), new PropertyChangedCallback(OnSeriesSourceChanged)));

        private static void OnSeriesSourceChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            IEnumerable oldValue = (IEnumerable)e.OldValue;
            IEnumerable newValue = (IEnumerable)e.NewValue;
            MultiChart source = (MultiChart)d;
            source.OnSeriesSourceChanged(oldValue, newValue);
        }

        protected virtual void OnSeriesSourceChanged(IEnumerable oldValue, IEnumerable newValue)
        {
            // In order to prevent memory leak, we use a weak-eventing pattern.
            if (oldValue != null && oldValue is INotifyCollectionChanged)
            {
                CollectionChangedEventManager.RemoveListener(oldValue as INotifyCollectionChanged, this);
            }
            if (newValue != null && newValue is INotifyCollectionChanged)
            {
                CollectionChangedEventManager.AddListener(newValue as INotifyCollectionChanged, this);
            }

            // Update the series
            this.Series.Clear();

            if (newValue != null)
            {
                foreach (object item in newValue)
                {
                    CreateSeries(item);
                }
            }
        }
        #endregion //SeriesSource (DependencyProperty)

        #region SeriesTemplate (DependencyProperty)

        public DataTemplate SeriesTemplate
        {
            get { return (DataTemplate)GetValue(SeriesTemplateProperty); }
            set { SetValue(SeriesTemplateProperty, value); }
        }
        public static readonly DependencyProperty SeriesTemplateProperty = DependencyProperty.Register("SeriesTemplate", typeof(DataTemplate), typeof(MultiChart), new PropertyMetadata(default(DataTemplate), new PropertyChangedCallback(OnSeriesTemplateChanged)));


        private static void OnSeriesTemplateChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            MultiChart source = (MultiChart)d;
            source.OnSeriesTemplateChanged();
        }

        protected virtual void OnSeriesTemplateChanged()
        {
            this.Series.Clear();

            if (SeriesSource != null)
            {
                foreach (object item in SeriesSource)
                {
                    CreateSeries(item);
                }
            }
        }
        #endregion // SeriesTemplate (DependencyProperty)

        #region SeriesTemplateSelector (DependencyProperty)

        public DataTemplateSelector SeriesTemplateSelector
        {
            get { return (DataTemplateSelector)GetValue(SeriesTemplateSelectorProperty); }
            set { SetValue(SeriesTemplateSelectorProperty, value); }
        }
        public static readonly DependencyProperty SeriesTemplateSelectorProperty = DependencyProperty.Register("SeriesTemplateSelector", typeof(DataTemplateSelector), typeof(MultiChart), new PropertyMetadata(default(DataTemplateSelector)));

        #endregion

        #region Private methods

        private void CreateSeries(object item)
        {
            CreateSeries(item, this.Series.Count);
        }

        private void CreateSeries(object item, int index)
        {
            DataTemplate dataTemplate = null;

            // get data template
            if (this.SeriesTemplateSelector != null)
            {
                dataTemplate = this.SeriesTemplateSelector.SelectTemplate(item, this);
            }
            if (dataTemplate == null && this.SeriesTemplate != null)
            {
                dataTemplate = this.SeriesTemplate;
            }

            // load data template content
            if (dataTemplate != null)
            {
                Series series = dataTemplate.LoadContent() as Series;

                if (series != null)
                {
                    // set data context
                    series.DataContext = item;

                    this.Series.Insert(index, series);
                }
            }
        }
        #endregion // Private methods

        #region IWeakEventListener Implementation
        /// <summary>
        /// 
        /// </summary>
        public bool ReceiveWeakEvent(Type managerType, Object sender, EventArgs e)
        {
            if (managerType == typeof(CollectionChangedEventManager))
            {
                NotifyCollectionChangedEventArgs args = e as NotifyCollectionChangedEventArgs;

                switch (args.Action)
                {
                    case NotifyCollectionChangedAction.Add:

                        if (args.NewItems != null)
                        {
                            for (int i = 0; i < args.NewItems.Count; i++)
                            {
                                CreateSeries(args.NewItems[0], args.NewStartingIndex + i);
                            }
                        }

                        break;
                    case NotifyCollectionChangedAction.Remove:

                        if (args.OldItems != null)
                        {
                            for (int i = args.OldItems.Count; i > 0; i--)
                            {
                                this.Series.RemoveAt(i + args.OldStartingIndex - 1);
                            }
                        }

                        break;
                    case NotifyCollectionChangedAction.Replace:
                        throw new NotImplementedException("NotifyCollectionChangedAction.Replace is not implemented by MultiChart control.");

                    case NotifyCollectionChangedAction.Reset:

                        this.Series.Clear();

                        if (SeriesSource != null)
                        {
                            foreach (object item in SeriesSource)
                            {
                                CreateSeries(item);
                            }
                        }

                        break;
                    default:
                        break;
                }
            }

            return true;
        }
        #endregion // IWeakEventListener
    } // MultiChart

}
