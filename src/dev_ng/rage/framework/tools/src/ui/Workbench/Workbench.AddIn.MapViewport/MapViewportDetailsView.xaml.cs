﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Workbench.AddIn.UI.Layout;
using RSG.Base.Windows.Controls.WpfViewport2D;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using Workbench.AddIn.Services.Model;

namespace Workbench.AddIn.MapViewport
{
    /// <summary>
    /// Interaction logic for MapViewportView.xaml
    /// </summary>
    public partial class MapViewportDetailsView : ToolWindowBase<MapViewportDetailsViewModel>
    {
        #region Constants

        public static readonly Guid GUID = new Guid("EE0D0660-B9FC-4850-AD4E-3FD3491101A2");

        #endregion // Constants

        #region Properties
        
        /// <summary>
        /// Determines whether the tool window should be floating by default
        /// </summary>
        public override Boolean FloatByDefault
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gives the default size for a floating window. This 
        /// size will be used for the first time the window is set to float
        /// </summary>
        public override Size DefaultFloatSize
        {
            get
            {
                return new Size(270, 340);
            }
        }
        
        #endregion // Properties

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public MapViewportDetailsView(ILevelBrowser levelBrowser, Viewport.AddIn.IMapViewport mapViewport)
            : base(Workbench.AddIn.MapViewport.Resources.Strings.MapViewportDetails_Name,
                  new MapViewportDetailsViewModel(levelBrowser, mapViewport))
        {
            InitializeComponent();

            this.ID = GUID;
            this.SaveModel = this.ViewModel;
        }
        
        #endregion // Constructor
    } // MapViewportDetailsView

    /// <summary>
    /// This converter is used to combine geometry sources for the viewport
    /// </summary>
    public class GeometryConverter : IMultiValueConverter
    {
        public Object Convert(Object[] values, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            RSG.Base.Collections.ObservableCollection<Viewport2DGeometry> allGeometry = new RSG.Base.Collections.ObservableCollection<Viewport2DGeometry>();

            // First add in the background image for the selected level
            foreach (object obj in values)
            {
                if (obj is Viewport2DGeometry)
                {
                    allGeometry.Add((Viewport2DGeometry)obj);
                }
                else if (obj is RSG.Base.Collections.ObservableCollection<Viewport2DGeometry>)
                {
                    RSG.Base.Collections.ObservableCollection<Viewport2DGeometry> overlayGeometry = (RSG.Base.Collections.ObservableCollection<Viewport2DGeometry>)obj;
                    foreach (Viewport2DGeometry geometry in overlayGeometry)
                    {
                        allGeometry.Add(geometry);
                    }
                }
            }

            return allGeometry;
        }

        public Object[] ConvertBack(Object value, Type[] targetTypes, Object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
