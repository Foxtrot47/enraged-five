﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows.Media;
using Ionic.Zip;
using RSG.Base.Collections;
using RSG.Base.ConfigParser;
using RSG.Base.Editor;
using RSG.Base.Extensions;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using RSG.Bounds;
using RSG.Model.Common;
using RSG.Model.Common.Extensions;
using RSG.Model.Common.Map;
using Workbench.AddIn.Services;
using MapViewport.AddIn;
using System.Windows.Forms;
using System.Windows;
using Workbench.AddIn.Services.Model;

namespace Workbench.AddIn.MapStatistics.Overlays.Bounds
{
    /// <summary>
    /// Represents a procedural type that can be displayed on the overlay
    /// </summary>
    public class ProceduralTypeInfo : ViewModelBase
    {
        #region Properties
        /// <summary>
        /// The section
        /// </summary>
        public string ProceduralType
        {
            get { return m_proceduralType; }
            set
            {
                SetPropertyValue(value, () => ProceduralType,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_proceduralType = (string)newValue;
                        }
                ));
            }
        }
        private string m_proceduralType;

        /// <summary>
        /// Whether we should render the bounds for this type on the overlay
        /// </summary>
        public bool Enabled
        {
            get { return m_enabled; }
            set
            {
                SetPropertyValue(value, () => Enabled,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_enabled = (bool)newValue;
                        }
                ));
            }
        }
        private bool m_enabled;

        /// <summary>
        /// 
        /// </summary>
        public IList<IMapSection> Sections
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public ProceduralTypeInfo(string proceduralType)
        {
            ProceduralType = proceduralType;
            Enabled = false;
            Sections = new List<IMapSection>();
        }
        #endregion // Constructor
    } // ProceduralTypeInfo

    /// <summary>
    /// Overlay for visualising the procedural types
    /// </summary>
    [ExportExtension(ExtensionPoints.BoundsStatisticsOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class ProceduralTypesSectionOverlay : LevelDependentViewportOverlay
    {
        #region Constants
        private const string c_name = "Procedural Type Sections";
        private const string c_description = "Shows an overlay showing which procedural types are applied to which section.";

        private const string c_boundZipExtension = ".ibn.zip";
        private const string c_boundFileExtension = ".bnd";
        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// The perforce sync service
        /// </summary>
        [Workbench.AddIn.ImportExtension(Workbench.AddIn.CompositionPoints.PerforceSyncService, typeof(Workbench.AddIn.Services.IPerforceSyncService))]
        private Lazy<Workbench.AddIn.Services.IPerforceSyncService> PerforceSyncService { get; set; }

        /// <summary>
        /// Progress service for displaying while gathering the data
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ProgressService, typeof(Workbench.AddIn.Services.IProgressService))]
        private IProgressService ProgressService { get; set; }

        /// <summary>
        /// A reference to view model for the map viewport
        /// </summary>
        [ImportExtension(Viewport.AddIn.CompositionPoints.MapViewport, typeof(Viewport.AddIn.IMapViewport))]
        public Viewport.AddIn.IMapViewport MapViewport { get; set; }

        /// <summary>        
        /// MEF import for message box service.        
        /// </summary>        
        [ImportExtension(Workbench.AddIn.CompositionPoints.MessageService, typeof(Workbench.AddIn.Services.IMessageService))]
        private IMessageService MessageService { get; set; }
        #endregion // MEF Imports

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public bool ShowAny
        {
            get { return m_showAny; }
            set
            {
                SetPropertyValue(value, () => ShowAny,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_showAny = (bool)newValue;
                            UpdateOverlayGeometry();
                        }));
            }
        }
        private bool m_showAny;

        /// <summary>
        /// 
        /// </summary>
        public bool ShowFiltered
        {
            get { return m_showFiltered; }
            set
            {
                SetPropertyValue(value, () => ShowFiltered,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_showFiltered = (bool)newValue;
                            UpdateOverlayGeometry();
                        }));
            }
        }
        private bool m_showFiltered;

        /// <summary>
        /// Mapping of section to the procedural types that it contains
        /// </summary>
        public ObservableCollection<ProceduralTypeInfo> AllProceduralTypeInfo
        {
            get { return m_allProceduralTypeInfo; }
            set
            {
                SetPropertyValue(value, () => AllProceduralTypeInfo,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_allProceduralTypeInfo = (ObservableCollection<ProceduralTypeInfo>)newValue;
                        }));
            }
        }
        private ObservableCollection<ProceduralTypeInfo> m_allProceduralTypeInfo;

        /// <summary>
        /// 
        /// </summary>
        public IMapSection SelectedSection
        {
            get { return m_selectedSection; }
            set
            {
                SetPropertyValue(value, () => SelectedSection,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_selectedSection = (IMapSection)newValue;
                        }));
            }
        }
        private IMapSection m_selectedSection;

        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<string> SelectedSectionProceduralTypes
        {
            get { return m_selectedSectionProceduralTypes; }
            set
            {
                SetPropertyValue(value, () => SelectedSectionProceduralTypes,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_selectedSectionProceduralTypes = (ObservableCollection<string>)newValue;
                        }));
            }
        }
        private ObservableCollection<string> m_selectedSectionProceduralTypes;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default Constructor
        /// </summary>
        public ProceduralTypesSectionOverlay()
            : base(c_name, c_description)
        {
            m_showAny = true;
            AllProceduralTypeInfo = new ObservableCollection<ProceduralTypeInfo>();
            SelectedSectionProceduralTypes = new ObservableCollection<string>();
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// Gets called when the user wants to view this overlay
        /// </summary>
        public override void Activated()
        {
            base.Activated();

            // Make sure a level is selected
            if (LevelBrowserProxy.Value.SelectedLevel != null)
            {
                ILevel level = LevelBrowserProxy.Value.SelectedLevel;
                IEnumerable<IMapSection> containerSections = GetContainerSections(level);
                IEnumerable<string> processedZipFilenames = GetProcessedZipFilenames(containerSections);

                // We need to ensure that all the processed zip files are up to date in p4 for this section
                int syncedFiles = 0;
                PerforceSyncService.Value.Show("You current have some processed zip files that are out of date.\n" +
                                               "Would you like to grab latest now?",
                                               processedZipFilenames.ToArray(), ref syncedFiles, false, true);

                List<string> files = new List<string>();

                foreach (var file in processedZipFilenames)
                {
                    if (!File.Exists(file))
                    {
                        files.Add(file);
                    }
                }

                MessageBoxResult result = MessageBoxResult.Yes;

                if (files.Count > 0)
                {
                    string msg = String.Format("There are {0} file(s) that cannot be found. Are you sure you want to continue? If you continue the data that is gathered will NOT be accurate.\r\n\r\n{1}...", files.Count, string.Join(Environment.NewLine, files.Take(5)));
                    result = MessageService.Show(msg, MessageBoxButton.YesNo);
                }

                if (result == MessageBoxResult.No)
                {
                    return;
                }

                // Create a background worker for gathering the object intersection data
                BackgroundWorker worker = new BackgroundWorker();
                worker.DoWork += UpdateAsync;
                ProgressService.Show(worker, "Gathering Procedural Type Data", "The workbench is currently gathering the data required to generate the overlay. This can take around 5 minutes to do.");
            }

            MapViewport.SelectedOverlayGeometry.CollectionChanged += OnSelectedGeometryChanged;
        }

        /// <summary>
        /// Gets called when the user turns off this overlay
        /// </summary>
        public override void Deactivated()
        {
            Geometry.Clear();
            AllProceduralTypeInfo.Clear();


            MapViewport.SelectedOverlayGeometry.CollectionChanged -= OnSelectedGeometryChanged;

            base.Deactivated();
        }

        /// <summary>
        /// Gets the control that should be shown in the overlay details panel
        /// </summary>
        public override System.Windows.Controls.Control GetOverlayControl()
        {
            return new ProceduralTypesSectionOverlayView();
        }
        #endregion // Overrides

        #region Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FilterChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Enabled")
            {
                UpdateOverlayGeometry();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void OnSelectedGeometryChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            SelectedSectionProceduralTypes.BeginUpdate();
            SelectedSectionProceduralTypes.Clear();

            // Just show information for the first section
            SelectedSection = (IMapSection)MapViewport.SelectedOverlayGeometry.FirstOrDefault();

            if (SelectedSection != null)
            {
                foreach (ProceduralTypeInfo info in AllProceduralTypeInfo)
                {
                    if (info.Sections.Contains(SelectedSection))
                    {
                        SelectedSectionProceduralTypes.Add(info.ProceduralType);
                    }
                }
            }

            SelectedSectionProceduralTypes.EndUpdate();
        }
        #endregion // Event Handlers

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdateAsync(Object sender, DoWorkEventArgs e)
        {
            ILevel level = LevelBrowserProxy.Value.SelectedLevel;
            IEnumerable<IMapSection> containerSections = GetContainerSections(level);

            GatherProceduralTypeInformation(containerSections, (sender as BackgroundWorker));
            UpdateOverlayGeometry();
        }

        #region Data Gathering
        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        private IEnumerable<IMapSection> GetContainerSections(ILevel level)
        {
            IMapHierarchy hierarchy = level.GetMapHierarchy();
            if (hierarchy != null)
            {
                foreach (IMapSection section in hierarchy.AllSections)
                {
                    if (section.Type == SectionType.Container)
                    {
                        yield return section;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        private IEnumerable<string> GetProcessedZipFilenames(IEnumerable<IMapSection> sections)
        {
            foreach (IMapSection section in sections)
            {
                foreach (string filename in GetProcessedZipFilenames(section))
                {
                    yield return filename;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        private IEnumerable<string> GetProcessedZipFilenames(IMapSection section)
        {
            foreach (ContentNode exportNode in section.GetConfigMapNode(Config.Value.GameConfig).Outputs)
            {
                foreach (ContentNode processedNode in exportNode.Outputs)
                {
                    // Load in the processed zip file and extract the bounds data
                    ContentNodeMapProcessedZip processedZipNode = processedNode as ContentNodeMapProcessedZip;
                    if (processedZipNode != null)
                    {
                        String filename = processedZipNode.Filename;
                        filename = Path.Combine(Path.GetDirectoryName(filename), Path.GetFileNameWithoutExtension(filename) + "_collision" + Path.GetExtension(filename));
                        yield return Path.GetFullPath(filename);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void GatherProceduralTypeInformation(IEnumerable<IMapSection> containerSections, BackgroundWorker worker)
        {
            AllProceduralTypeInfo.BeginUpdate();
            AllProceduralTypeInfo.Clear();

            IDictionary<string, ISet<IMapSection>> proceduralTypeMap = new Dictionary<string, ISet<IMapSection>>();

            int total = containerSections.Count();
            int current = 0;

            foreach (IMapSection section in containerSections)
            {
                ProgressService.Status = "Processing " + section.Name;

                // Get all the bound files that are for this section
                foreach (BNDFile boundFile in GetBoundFilesForSection(section))
                {
                    foreach (string proceduralType in ExtractProceduralTypes(boundFile.BoundObject))
                    {
                        if (!proceduralTypeMap.ContainsKey(proceduralType))
                        {
                            proceduralTypeMap[proceduralType] = new HashSet<IMapSection>();
                        }

                        proceduralTypeMap[proceduralType].Add(section);
                    }
                }

                ++current;
                worker.ReportProgress((int)(((float)current / (float)total) * 100.0f));
            }

            // Convert the map to the procedural type info list
            foreach (KeyValuePair<string, ISet<IMapSection>> pair in proceduralTypeMap)
            {
                ProceduralTypeInfo info = new ProceduralTypeInfo(pair.Key);
                info.Sections.AddRange(pair.Value);
                info.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(FilterChanged);

                AllProceduralTypeInfo.Add(info);
            }

            AllProceduralTypeInfo.EndUpdate();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        private IEnumerable<BNDFile> GetBoundFilesForSection(IMapSection section)
        {
            foreach (string processedZipFile in GetProcessedZipFilenames(section))
            {
                if (File.Exists(processedZipFile))
                {
                    using (ZipFile zipFile = ZipFile.Read(processedZipFile))
                    {
                        IEnumerable<ZipEntry> boundEntries = zipFile.Where(item => (item.FileName.EndsWith(".ibr.zip") || item.FileName.EndsWith(".ibn.zip") || item.FileName.EndsWith("collision.zip")));
                        foreach (ZipEntry entry in boundEntries)
                        {
                            using (MemoryStream zipStream = new MemoryStream())
                            {
                                entry.Extract(zipStream);
                                zipStream.Position = 0;

                                string boundFileName = Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(entry.FileName)) + c_boundFileExtension;

                                BNDFile boundFile = LoadBoundsDataFromBoundsZipFile(boundFileName, zipStream);
                                if (boundFile != null)
                                {
                                    yield return boundFile;
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Loads the single bounds data file found in a particular .ibn.zip file
        /// </summary>
        /// <param name="boundFileName"></param>
        /// <param name="zipStream"></param>
        /// <returns></returns>
        private BNDFile LoadBoundsDataFromBoundsZipFile(string boundFileName, Stream zipStream)
        {
            using (ZipFile zipFile = ZipFile.Read(zipStream))
            {
                if (zipFile.ContainsEntry(boundFileName))
                {
                    using (MemoryStream fileStream = new MemoryStream())
                    {
                        zipFile[boundFileName].Extract(fileStream);
                        fileStream.Position = 0;
                        return BNDFile.Load(fileStream);
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Recursively processes bound object's to extract all the materials with procedural types
        /// </summary>
        private IEnumerable<string> ExtractProceduralTypes(BoundObject obj)
        {
            if (obj is Composite)
            {
                Composite composite = (Composite)obj;

                foreach (CompositeChild child in composite.Children)
                {
                    foreach (string proceduralType in ExtractProceduralTypes(child.BoundObject))
                    {
                        yield return proceduralType;
                    }
                }
            }
            else
            {
                BVH bvh = BoundObjectConverter.ToBVH(obj);
                foreach (string proceduralType in ExtractProceduralTypes(bvh.Materials))
                {
                    yield return proceduralType;
                }
            }
        }

        /// <summary>
        /// Extracts all materials that have a procedural type applied to them
        /// </summary>
        /// <param name="materials"></param>
        /// <returns></returns>
        private IEnumerable<string> ExtractProceduralTypes(string[] materials)
        {
            foreach (string mat in materials)
            {
                BVHMaterial material = new BVHMaterial(mat);
                if (material.ProceduralModifier != "0")
                {
                    yield return material.ProceduralModifier;
                }
            }
        }
        #endregion // Data Gathering

        /// <summary>
        /// 
        /// </summary>
        private void UpdateOverlayGeometry()
        {
            ObservableCollection<Viewport2DGeometry> newGeometry = new ObservableCollection<Viewport2DGeometry>();

            if (ShowAny)
            {
                foreach (IMapSection section in AllProceduralTypeInfo.SelectMany(item => item.Sections).Distinct())
                {
                    if (section.VectorMapPoints != null)
                    {
                        Viewport2DShape geom = new Viewport2DShape(etCoordSpace.World, section.Name, section.VectorMapPoints.ToArray(), Colors.Black, 1, Colors.Green);
                        geom.PickData = section;
                        geom.UserText = section.Name;

                        newGeometry.Add(geom);
                    }
                }
            }
            else
            {
                ISet<IMapSection> sections = new HashSet<IMapSection>();
                foreach (ProceduralTypeInfo info in AllProceduralTypeInfo.Where(item => item.Enabled))
                {
                    sections.AddRange(info.Sections);
                }

                foreach (IMapSection section in sections)
                {
                    if (section.VectorMapPoints != null)
                    {
                        Viewport2DShape geom = new Viewport2DShape(etCoordSpace.World, section.Name, section.VectorMapPoints.ToArray(), Colors.Black, 1, Colors.Green);
                        geom.PickData = section;
                        geom.UserText = section.Name;

                        newGeometry.Add(geom);
                    }
                }
            }

            Geometry = newGeometry;
        }
        #endregion // Private Methods
    } // ProceduralTypesSectionOverlay
}
