﻿using System;
using System.Windows.Input;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn.UI.Controls;
using RSG.Base.Windows.Controls;

namespace Workbench.AddIn.Commands
{
    /// <summary>
    /// The interface for all the workbench commands, this will
    /// make it possible to share commands between the different controls
    /// in the workbench (menu, toolbar, contextmenu).
    /// </summary>
    public interface IWorkbenchCommand : ICommandControl
    {
        #region Properties

        /// <summary>
        /// The text that will be displayed in the menu item or
        /// next to a checkable toolbar item.
        /// </summary>
        String Header { get; }

        /// <summary>
        /// The child commands to this one. Any child commands will be
        /// set as a sub menu to a menu item.
        /// </summary>
        IEnumerable<IWorkbenchCommand> Items { get; }

        /// <summary>
        /// The image to use if any in the menu item and
        /// the toolbar button
        /// </summary>
        Image Image { get; }

        /// <summary>
        /// Represents whether the image property is valid to show
        /// </summary>
        Boolean ImageValid { get; }

        /// <summary>
        /// Represents whether this command is a label
        /// </summary>
        Boolean IsLabel { get; }

        /// <summary>
        /// Represents whether this command is an image
        /// </summary>
        Boolean IsImage { get; }

        /// <summary>
        /// Set to true if this command can have a checkable state. For a menu item
        /// this adds a checkbox to it and for a toolbar the command gets drawn as a checkbox
        /// with the header to the right.
        /// </summary>
        Boolean IsCheckable { get; }

        /// <summary>
        /// A boolean property that represents whether this command is currently checked. The
        /// default value is false.
        /// </summary>
        Boolean IsChecked { get; set; }

        /// <summary>
        /// A boolean property that represents whether this command is currently checked. The
        /// default value is false.
        /// </summary>
        Boolean IsToggle { get; set; }

        /// <summary>
        /// Set to true if this command actual represents a separator in a command control (i.e menu, toolbar)
        /// </summary>
        Boolean IsSeparator { get; }

        /// <summary>
        /// Set to true if this command represents a dropdown
        /// </summary>
        Boolean IsCombobox { get; }

        /// <summary>
        /// Represents whether the submenu to this command is
        /// open (only used for menu items)
        /// </summary>
        Boolean IsSubmenuOpen { get; set; }

        /// <summary>
        /// 
        /// </summary>
        Object Context { get; set; }

        /// <summary>
        /// The keybinding that is placed on this command.
        /// This binding is added to the applications bindings.
        /// (Only used if this is a main menu item)
        /// </summary>
        KeyGesture KeyGesture { get; set; }

        /// <summary>
        /// The Guid that is the relative to this
        /// command if it is in a toolbar
        /// </summary>
        Guid RelativeToolBarID { get; }

        /// <summary>
        /// Used when this command is in a list of other commands
        /// </summary>
        Boolean IsDefault { get; }

        /// <summary>
        /// Custom data template to use for displaying this item.
        /// </summary>
        string CustomDataTemplateName { get; }

        /// <summary>
        /// Custom data template for combo box items that are selected.
        /// </summary>
        string CustomSelectedDataTemplateName { get; }

        #endregion // Properties
    } // IWorkbenchCommands
} // Workbench.AddIn.Commands
