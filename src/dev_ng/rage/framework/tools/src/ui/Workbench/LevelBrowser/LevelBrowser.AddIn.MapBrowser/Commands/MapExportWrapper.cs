﻿using System.Collections.Generic;
using RSG.Base.Win32;

namespace LevelBrowser.AddIn.MapBrowser.Commands
{
    /// <summary>
    /// Helper class to extract errors from the INI-style export text file.
    /// </summary>
    internal class MapExportWrapper
    {
        #region Export error class

        /// <summary>
        /// Export error value pair.
        /// </summary>
        public class ExportError
        {
            #region Public properties

            /// <summary>
            /// Map section.
            /// </summary>
            public string MapSection { get; set; }

            /// <summary>
            /// The reason the section failed to export.
            /// </summary>
            public string ReasonForError { get; set; }

            /// <summary>
            /// True if the export was successful.
            /// </summary>
            public bool Success { get; set; }

            #endregion

            #region Constructor

            /// <summary>
            /// Constructor.
            /// </summary>
            /// <param name="mapSection">Map section.</param>
            /// <param name="errorReason">The reason the section failed to export.</param>
            /// <param name="success">True if the export was successful.</param>
            internal ExportError(string mapSection, string errorReason, bool success)
            {
                MapSection = mapSection;
                ReasonForError = errorReason;
                Success = success;
            }

            #endregion
        }

        #endregion

        #region Public properties

        /// <summary>
        /// The export errors.
        /// </summary>
        public List<ExportError> ExportResult { get; private set; }

        /// <summary>
        /// The number of items that exported successfully.
        /// </summary>
        public int SuccessCount { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="iniFile">Inifile.</param>
        internal MapExportWrapper(IniFile iniFile)
        {
            ExportResult = new List<ExportError>();
            SuccessCount = 0;
// GD: Commenting out for now. We need the batch functionality, but this is triggered before the actual exports, so give false results.
//             foreach (KeyValuePair<string, string> kvp in iniFile.GetEntries("ExportResults"))
//             {
//                 if (kvp.Value.Contains("Exported"))
//                 {
//                     ExportResult.Add(new ExportError(kvp.Key, kvp.Value, true));
//                     SuccessCount = SuccessCount + 1;
//                 }
//                 else
//                 {
//                     ExportResult.Add(new ExportError(kvp.Key, kvp.Value, false));
//                 }
//             }
        }

        #endregion
    }
}
