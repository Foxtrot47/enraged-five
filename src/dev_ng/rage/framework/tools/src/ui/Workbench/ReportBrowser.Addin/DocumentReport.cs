﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn.UI.Layout;

namespace ReportBrowser.Addin
{
    /// <summary>
    /// DocumentReport - A report that generates a document view for display in avalon dock
    /// </summary>
    public abstract class DocumentReport : RSG.Model.Report.Report, IReportDocumentProvider
    {
        #region Properties
        /// <summary>
        /// Stream containing the report data
        /// </summary>
        public IDocumentBase Document
        {
            get;
            protected set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="desc"></param>
        public DocumentReport(string name, string desc)
            : base(name, desc)
        {
        }
        #endregion // Constructor(s)
    }
}
