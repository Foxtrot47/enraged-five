﻿using System;
using System.Xml;
using System.Xml.XPath;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Controls;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using RSG.Base.Editor;
using ContentBrowser.AddIn;
using Workbench.AddIn.Services;
using RSG.Base.Windows.Controls;
using RSG.Base.ConfigParser;
using RSG.Model.Common;
using RSG.Model.Common.Map;
using RSG.Base.Tasks;

namespace ContentBrowser.ViewModel
{
    /// <summary>
    /// The view model for the main content browser window
    /// </summary>
    [Workbench.AddIn.ExportExtension(CompositionPoints.ContentBrowser, typeof(IContentBrowser))]
    public class ContentBrowserDataContext : ViewModelBase, IContentBrowser, IPartImportsSatisfiedNotification, IWeakEventListener
    {
        #region Public Events

        public event SelectedPanelItemChangedHandler PanelItemChanged;
        public event GridSelectionChangedEventHandler GridSelectionChanged;

        #endregion // Public Events

        #region Constants

        private static readonly XPathExpression XPATH_VECTOR_SECTION = XPathExpression.Compile("/vector_map/section");

        #endregion // Constants

        #region MEF Imports

        /// <summary>
        /// Indiviual browsers that have been imported elsewhere
        /// </summary>
        [Workbench.AddIn.ImportManyExtension(ExtensionPoints.Browser, typeof(BrowserBase))]
        private IEnumerable<BrowserBase> Browsers { get; set; }

        /// <summary>
        /// Configuration service; used to create the available levels
        /// </summary>
        [Workbench.AddIn.ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(IConfigurationService))]
        private Lazy<IConfigurationService> ConfigService { get; set; }

        /// <summary>
        /// Configuration service; used to create the available levels
        /// </summary>
        [Workbench.AddIn.ImportExtension(Workbench.AddIn.CompositionPoints.ExtensionService, typeof(IExtensionService))]
        private Lazy<IExtensionService> ExtensionService { get; set; }

        [Workbench.AddIn.ImportExtension(ContentBrowser.AddIn.CompositionPoints.ContentBrowserSettings, typeof(ContentBrowser.AddIn.IContentBrowserSettings))]
        ContentBrowser.AddIn.IContentBrowserSettings Settings { get; set; }

        [Workbench.AddIn.ImportExtension(Workbench.AddIn.CompositionPoints.PropertyInspectorService, typeof(IPropertyInspector))]
        private Lazy<IPropertyInspector> PropertyInspector { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Workbench.AddIn.ImportExtension(Workbench.AddIn.CompositionPoints.TaskProgressService, typeof(ITaskProgressService))]
        private ITaskProgressService TaskProgressService { get; set; }

        #endregion // MEF Imports

        #region Members
        private BrowserCollectionViewModel m_browserCollection;
        public RSG.Model.Common.IAsset m_selectedItem;
        public RSG.Model.Common.IAsset m_detailedItem;
        private IEnumerable<IAsset> m_selectedGridItems;
        public IHasAssetChildrenViewModel m_selectedAssetContainer;
        private bool m_showGrid;
        private bool m_showGridPreview;
        private bool m_showDetailsView;
        private double m_renderSize;
        #endregion // Members

        #region Properties
        /// <summary>
        /// The collection of browser view models
        /// </summary>
        public BrowserCollectionViewModel BrowserCollection
        {
            get { return m_browserCollection; }
            set
            {
                SetPropertyValue(value, () => BrowserCollection,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_browserCollection = (BrowserCollectionViewModel)newValue;
                        }
                ));
            }
        }
                
        /// <summary>
        /// The currently selected item in the browser collection
        /// </summary>
        public RSG.Model.Common.IAsset SelectedItem
        {
            get { return m_selectedItem; }
            private set
            {
                SetPropertyValue(value, () => SelectedItem,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            RSG.Model.Common.IAsset oldValue = m_selectedItem;
                            m_selectedItem = (RSG.Model.Common.IAsset)newValue;
                            OnSelectedPanelItemChanged(oldValue, value);
                        }
                ));
            }
        }

        /// <summary>
        /// The currently selected item in the browser collection
        /// </summary>
        public IEnumerable<IAsset> SelectedGridItems
        {
            get { return m_selectedGridItems; }
            internal set
            {
                SetPropertyValue(value, () => SelectedGridItems,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            IEnumerable<IAsset> oldValue = m_selectedGridItems;
                            m_selectedGridItems = (IEnumerable<IAsset>)newValue;
                            OnSelectedGridItemsChanged(oldValue, value);
                        }
                ));
            }
        }

        /// <summary>
        /// The currently selected asset conatiner or null if there
        /// isn't one selected
        /// </summary>
        public IHasAssetChildrenViewModel SelectedAssetContainer
        {
            get { return m_selectedAssetContainer; }
            private set
            {
                SetPropertyValue(value, () => SelectedAssetContainer,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_selectedAssetContainer = (IHasAssetChildrenViewModel)newValue;
                            this.SelectedGridItems = Enumerable.Empty<IAsset>();
                        }
                ));
            }
        }

        /// <summary>
        /// The currently selected item that is shown in the details panel.
        /// </summary>
        public RSG.Model.Common.IAsset SelectedDetailedItem
        {
            get { return m_detailedItem; }
            internal set
            {
                SetPropertyValue(value, () => SelectedDetailedItem,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_detailedItem = (RSG.Model.Common.IAsset)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool ShowGrid
        {
            get { return m_showGrid; }
            private set
            {
                SetPropertyValue(value, m_showGrid, () => ShowGrid,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_showGrid = (bool)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool ShowGridPreview
        {
            get { return m_showGridPreview; }
            set
            {
                SetPropertyValue(value, m_showGridPreview, () => ShowGridPreview,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_showGridPreview = (bool)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool ShowingDetailsView
        {
            get { return m_showDetailsView; }
            set
            {
                SetPropertyValue(value, m_showDetailsView, () => ShowingDetailsView,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_showDetailsView = (bool)newValue;
                            this.OnPropertyChanged("ShowingIconView");
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool ShowingIconView
        {
            get { return !m_showDetailsView; }
        }

        /// <summary>
        /// 
        /// </summary>
        public double RenderSize
        {
            get { return m_renderSize; }
            set
            {
                SetPropertyValue(value, m_renderSize, () => RenderSize,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_renderSize = (double)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Toggle the preview visibility.
        /// </summary>
        public RelayCommand TogglePreviewCommand { get; private set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Default Constructor
        /// </summary>
        public ContentBrowserDataContext()
        {
            m_selectedGridItems = Enumerable.Empty<IAsset>();
            this.ShowGrid = true;
            this.ShowGridPreview = true;
            this.RenderSize = 64;
            this.ShowingDetailsView = false;

            TogglePreviewCommand = new RelayCommand(TogglePreview_Executed);
        }

        #endregion // Constructor

        #region IPartImportsSatisfiedNotification

        /// <summary>
        /// Called after all the imports have been satisfied
        /// </summary>
        public void OnImportsSatisfied()
        {
            this.BrowserCollection = new BrowserCollectionViewModel(new Model.BrowserCollection(this.ExtensionService.Value.Sort(this.Browsers)));
            this.BrowserCollection.HideShowChanged += new EventHandler(HideShowGridChanged);
            PropertyChangedEventManager.AddListener(this.BrowserCollection, this, "SelectedLevel");
            foreach (IBrowserViewModel browserViewModel in this.BrowserCollection.BrowserItems)
            {
                PropertyChangedEventManager.AddListener(browserViewModel, this, "SelectedItem");
            }

            if (Settings != null)
            {
                if (!Settings.PreviewPaneVisible)
                {
                    ShowGridPreview = false;
                }
            }
        }

        void HideShowGridChanged(object sender, EventArgs e)
        {
            this.ShowGrid = !this.ShowGrid;
            if (!this.ShowGrid)
                this.ShowGridPreview = false;
        }

        #endregion // IPartImportsSatisfiedNotification

        #region IWeakEventListener

        /// <summary>
        /// Receives property changed events for the attached viewmodels and
        /// passes the changed event on to any listerning objects
        /// </summary>
        public Boolean ReceiveWeakEvent(Type managerType, Object sender, EventArgs e)
        {
            if (e is PropertyChangedEventArgs)
            {
                if (sender is BrowserCollectionViewModel)
                {
                    this.OnPropertyChanged((e as PropertyChangedEventArgs).PropertyName);
                }
                else if (sender is IBrowserViewModel)
                {
                    if ((sender as IBrowserViewModel).SelectedItem is IAssetViewModel)
                    {
                        this.SelectedItem = ((sender as IBrowserViewModel).SelectedItem as IAssetViewModel).Model;
                        if ((sender as IBrowserViewModel).SelectedItem is IHasAssetChildrenViewModel)
                        {
                            this.SelectedAssetContainer = (sender as IBrowserViewModel).SelectedItem as IHasAssetChildrenViewModel;
                        }
                        else
                        {
                            this.SelectedAssetContainer = null;
                        }
                    }
                    else
                    {
                        this.SelectedItem = null;
                        this.SelectedAssetContainer = null;
                    }
                }
            }
            return true;
        }

        #endregion // IWeakEventListener

        #region Command callbacks

        /// <summary>
        /// Handle the TogglePreview command being fired. Saves the preview pane visibility to the settings file.
        /// </summary>
        /// <param name="o">Optional, but must be boolean if supplied. If NOT supplied assumes o == !ShowGridPreview.</param>
        private void TogglePreview_Executed(object o)
        {
            bool toggleValue = !ShowGridPreview;

            if (o != null)
            {
                toggleValue = (bool)o;
            }

            ShowGridPreview = toggleValue;

            if (Settings != null)
            {
                Settings.PreviewPaneVisible = ShowGridPreview;
                Settings.Apply();
            }
        }

        #endregion

        #region Property Changed Callbacks

        /// <summary>
        /// 
        /// </summary>
        private void OnSelectedPanelItemChanged(RSG.Model.Common.IAsset oldValue, RSG.Model.Common.IAsset newValue)
        {
            UpdatePropertyGrid(newValue);

            if (this.PanelItemChanged != null)
                this.PanelItemChanged(oldValue, newValue);
        }

        /// <summary>
        /// 
        /// </summary>
        private void OnSelectedGridItemsChanged(IEnumerable<IAsset> oldValue, IEnumerable<IAsset> newValue)
        {
            if (this.GridSelectionChanged != null)
                this.GridSelectionChanged(this, new GridSelectionChangedEventArgs(oldValue, newValue));
        }

        #endregion // Property Changed Callbacks

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void UpdatePropertyGrid(IAsset newValue)
        {
            this.PropertyInspector.Value.Clear();
            if (newValue is ICharacter)
            {
                ICharacter character = (ICharacter)newValue;

                ITask actionTask = new ActionTask("Character Load", (context, progress) => character.LoadAllStats());
                TaskProgressService.Add(actionTask, new TaskContext(), TaskPriority.Background);

                this.PropertyInspector.Value.Push(character);
            }
            else if (newValue is IVehicle)
            {
                IVehicle vehicle = (IVehicle)newValue;

                ITask actionTask = new ActionTask("Vehicle Load", (context, progress) => vehicle.LoadAllStats());
                TaskProgressService.Add(actionTask, new TaskContext(), TaskPriority.Background);

                this.PropertyInspector.Value.Push(vehicle as IVehicle);
            }
            else if (newValue is IWeapon)
            {
                IWeapon weapon = (IWeapon)newValue;

                ITask actionTask = new ActionTask("Weapon Load", (context, progress) => weapon.LoadAllStats());
                TaskProgressService.Add(actionTask, new TaskContext(), TaskPriority.Background);

                this.PropertyInspector.Value.Push(weapon);
            }
            else if (newValue is IMapSection)
            {
                IMapSection section = (IMapSection)newValue;
                this.PropertyInspector.Value.Push(section);
            }
            else if (newValue is IArchetype)
            {
                this.PropertyInspector.Value.Push(newValue as IArchetype);
            }
            else if (newValue is IEntity)
            {
                IEntity entity = (IEntity)newValue;
                this.PropertyInspector.Value.Push(newValue as IEntity);
            }
            else if (newValue is IRoom)
            {
                IRoom room = (IRoom)newValue;
                this.PropertyInspector.Value.Push(newValue as IRoom);
            }
        }
        #endregion // Private Methods

        #region IContentBrowser

        /// <summary>
        /// Returns a enumerable object around any currently selected
        /// items that have the given type
        /// </summary>
        public IEnumerable<T> GetGridSelectionForType<T>() where T : IAsset
        {
            foreach (IAsset asset in this.SelectedGridItems)
            {
                if (asset is T)
                    yield return (T)asset;
            }
        }

        /// <summary>
        /// Initialises all the attached browsers
        /// </summary>
        public void InitialiseBrowsers()
        {
            foreach (BrowserBase browser in this.Browsers)
            {
                browser.Initialise();
            }
        }

        #endregion // IContentBrowser
    } // ContentBrowserDataContext
} // ContentBrowser.ViewModel
