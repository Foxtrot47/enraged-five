﻿using System;
using System.Diagnostics;
using System.Windows.Input;

namespace Workbench.AddIn.TXDParentiser.Util
{
    /// <summary>
    /// A command whose sole purpose is to  relay its functionality 
    /// to other objects by invoking delegates. The default return 
    /// value for the CanExecute method is 'true'.
    /// </summary>
    public class GenericCommand<T> : ICommand
    {
        #region Fields

        private readonly Action<T> m_execute;
        private readonly Predicate<T> m_canExecute;

        #endregion // Fields

        #region Constructors

        /// <summary>
        /// Creates a new command that can always execute.
        /// </summary>
        public GenericCommand(Action<T> execute)
            : this(execute, null)
        {
        }

        /// <summary>
        /// Creates a new command.
        /// </summary>
        public GenericCommand(Action<T> execute, Predicate<T> canExecute)
        {
            if (execute == null)
                throw new ArgumentNullException("execute");

            m_execute = execute;
            m_canExecute = canExecute;
        }

        #endregion // Constructors

        #region ICommand Members

        /// <summary>
        /// Gets called when the manager is deciding if this
        /// command can be executed. This returns true be default
        /// </summary>
        public bool CanExecute(Object parameter)
        {
            if (m_canExecute != null)
                return m_canExecute((T)parameter);

            return true;
        }

        /// <summary>
        /// Gets called when the command is executed. This just
        /// relays the execution to the delegate function
        /// </summary>
        public void Execute(Object parameter)
        {
            if (m_execute != null)
                m_execute((T)parameter);
        }

        /// <summary>
        /// Makes sure that this command is hooked up into the
        /// command manager
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        #endregion // ICommand Members
    } // GenericCommand
} // Workbench.AddIn.TXDParentiser.Util
