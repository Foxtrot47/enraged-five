﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetadataEditor.AddIn
{
    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public interface IMetadataView
    {
        /// <summary>
        /// Gets the view model as a IMetadataViewModel to support live editing a generic
        /// metadata file.
        /// </summary>
        IMetadataViewModel GetMetadataViewModel();

        /// <summary>
        /// The path to the metadata file on the local disk.
        /// </summary>
        string Path { get; }
    }
}
