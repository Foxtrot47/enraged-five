﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using RSG.Base.Editor.Command;
using Editor.Utilities;
using Workbench.AddIn;
using Workbench.AddIn.Services;
using Workbench.AddIn.UI.Controls;
using Workbench.AddIn.UI.Menu;

namespace Workbench.AddIn.UI.Menu
{

    /// <summary>
    /// Abstract base menu item class.
    /// </summary>
    public abstract class MenuItemBase : 
        CommandControlBase, 
        IMenuItem
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Menu item header text.
        /// </summary>
        public String Header
        {
            get { return m_sHeader; }
            protected set
            {
                SetPropertyValue(value, () => this.Header,
                  new PropertySetDelegate(delegate(Object newValue) { m_sHeader = (String)newValue; }));
            }
        }
        private String m_sHeader = String.Empty;
        
        /// <summary>
        /// Menu item subitems.
        /// </summary>
        public IEnumerable<IMenuItem> Items
        {
            get { return m_Items; }
            protected set
            {
                SetPropertyValue(value, () => this.Items,
                    new PropertySetDelegate(delegate(Object newValue) { m_Items = (IEnumerable<IMenuItem>)newValue; }));
            }
        }
        private IEnumerable<IMenuItem> m_Items = null;
        
        /// <summary>
        /// 
        /// </summary>
        public Object Image
        {
            get
            {
                if (null == IconEnabled)
                    return (null);

                Image image = new Image();
                image.Source = IconEnabled;
                return (image);
            }
        }

        /// <summary>
        /// Menu item checkable state
        /// </summary>
        /// The 'OnIsCheckedChanged' can then be overridden and the IsChecked
        /// property can be used to determine the checked state of the IMenuItem.
        public bool IsCheckable
        {
            get { return m_bIsCheckable; }
            protected set
            {
                SetPropertyValue(value, () => this.IsCheckable,
                    new PropertySetDelegate(delegate(Object newValue) { m_bIsCheckable = (bool)newValue; }));
            }
        }
        private bool m_bIsCheckable = false;
        
        /// <summary>
        /// Menu item checked state
        /// </summary>
        public bool IsChecked
        {
            get { return m_bIsChecked; }
            set
            {
                SetPropertyValue(value, () => this.IsChecked,
                   new PropertySetDelegate(delegate(Object newValue) { m_bIsChecked = (bool)newValue; }));
            }
        }
        private bool m_bIsChecked = false;
        
        /// <summary>
        /// Menu item separator state.
        /// </summary>
        public bool IsSeparator
        {
            get { return m_bIsSeparator; }
            protected set
            {
                SetPropertyValue(value, () => this.IsSeparator,
                    new PropertySetDelegate(delegate(Object newValue) { m_bIsSeparator = (bool)newValue; }));
            }
        }
        private bool m_bIsSeparator = false;

        /// <summary>
        /// 
        /// </summary>
        public bool IsSubmenuOpen
        {
            get
            {
                return m_IsSubmenuOpen;
            }
            set
            {
                if (m_IsSubmenuOpen != value)
                {
                    SetPropertyValue(value, () => this.IsSubmenuOpen,
                        new PropertySetDelegate(delegate(Object newValue) { m_IsSubmenuOpen = (bool)newValue; }));
                    OnIsSubmenuOpenChanged();
                }
            }
        }
        private bool m_IsSubmenuOpen = false;
       
        /// <summary>
        /// 
        /// </summary>
        public Object Context
        {
            get { return m_Context; }
            set
            {
                SetPropertyValue(value, () => this.IsSubmenuOpen,
                    new PropertySetDelegate(delegate(Object newValue) { m_Context = newValue; }));
            }
        }
        private Object m_Context = null;

        #region Private Properties
        /// <summary>
        /// 
        /// </summary>
        private BitmapSource IconEnabled
        {
            get { return m_IconEnabled; }
            set
            {
                if (value == m_IconEnabled)
                    return;

                m_IconEnabled = value;
                if (null != m_IconEnabled)
                    IconDisabled = ConvertFullToGray(m_IconEnabled);
                else
                    IconDisabled = null;
            }
        }
        private BitmapSource m_IconEnabled = null;

        /// <summary>
        /// 
        /// </summary>
        private BitmapSource IconDisabled
        {
            get;
            set;
        }
        #endregion // Private Properties
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="header"></param>
        public MenuItemBase(String header)
            : base()
        {
            this.Header = (header.Clone() as String);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="header"></param>
        /// <param name="id"></param>
        public MenuItemBase(String header, Guid id)
            : base()
        {
            this.Header = (header.Clone() as String);
            this.ID = id;
        }
        #endregion // Constructor(s)

        #region Protected Methods
        /// <summary>
        /// This is a helper function so you can assign the Icon directly
        /// from a Bitmap, such as one from a resources file.
        /// </summary>
        /// <param name="value"></param>
        protected void SetImageFromBitmap(System.Drawing.Bitmap value)
        {
            if (value != null)
            {
                BitmapSource b = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                    value.GetHbitmap(),
                    IntPtr.Zero,
                    System.Windows.Int32Rect.Empty,
                    System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions());
                this.IconEnabled = b;
            }
        }

        /// <summary>
        /// This method is called only if the user changes the IsSubmenuOpen
        /// property.  Override it in the derived class to take an action when
        /// it does.
        /// </summary>
        protected virtual void OnIsSubmenuOpenChanged() 
        {
        }
        #endregion // Protected Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="full"></param>
        /// <returns></returns>
        private BitmapSource ConvertFullToGray(BitmapSource full)
        {
            FormatConvertedBitmap gray = new FormatConvertedBitmap();

            gray.BeginInit();
            gray.Source = full;
            gray.DestinationFormat = PixelFormats.Gray32Float;
            gray.EndInit();

            return (gray);
        }
        #endregion // Private Methods
    }

} // Workbench.AddIn.UI.Menu namespace
