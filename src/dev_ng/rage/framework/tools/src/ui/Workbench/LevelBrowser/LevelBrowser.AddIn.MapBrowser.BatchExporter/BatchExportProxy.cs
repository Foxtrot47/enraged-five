﻿using Workbench.AddIn;
using Workbench.AddIn.UI.Layout;
using LevelBrowser.AddIn.MapBrowser.BatchExporter.View;
using LevelBrowser.AddIn.MapBrowser.BatchExporter.ViewModel;

namespace LevelBrowser.AddIn.MapBrowser.BatchExporter
{
    /// <summary>
    /// Tool window proxy for the Batch Exporter.
    /// </summary>
    [Workbench.AddIn.ExportExtension(Workbench.AddIn.ExtensionPoints.ToolWindowProxy, typeof(IToolWindowProxy))]
    public class BatchExportProxy : IToolWindowProxy
    {
        #region Public properties

        /// <summary>
        /// Batch exporter view model.
        /// </summary>
        [ImportExtension("LevelBrowser.AddIn.MapBrowser.BatchExporter.ViewModel.BatchExportWindowViewModel", typeof(BatchExportWindowViewModel))]
        private BatchExportWindowViewModel BatchExportWindowViewModel { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        public BatchExportProxy()
        {
            Name = "Batch_Export";
            Header = "Batch Export";
            DelayCreation = false;
            KeyGesture = new System.Windows.Input.KeyGesture(System.Windows.Input.Key.X, System.Windows.Input.ModifierKeys.Control | System.Windows.Input.ModifierKeys.Shift);
        }

        #endregion

        #region Protected methods

        /// <summary>
        /// Create the tool window.
        /// </summary>
        /// <param name="toolWindow">Tool window instance that is created in this method.</param>
        /// <returns>True on success.</returns>
        protected override bool CreateToolWindow(out IToolWindowBase toolWindow)
        {
            toolWindow = new BatchExportWindow(BatchExportWindowViewModel);
            return true;
        }

        #endregion
    }
}
