﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Resources;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Controls;

namespace Editor.Utilities
{

    /// <summary>
    /// 
    /// </summary>
    public static class Resource
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path"></param>
        /// <returns></returns>
        /// <see cref="http://blogs.microsoft.co.il/blogs/tamir/archive/2007/04/05/Read-your-data-easily-from-application-resources.aspx"/>
        ///
        public static T LoadResource<T>(String path)
        {
            T c = default(T);
            StreamResourceInfo sri = Application.GetResourceStream(new Uri(path, UriKind.Relative));
            
            if (sri.ContentType == "application/xaml+xml")
            {

                c = (T)XamlReader.Load(sri.Stream);

            }

            else if (sri.ContentType.IndexOf("image") >= 0)
            {

                BitmapImage bi = new BitmapImage();

                bi.BeginInit();

                bi.StreamSource = sri.Stream;

                bi.EndInit();

                if (typeof(T) == typeof(ImageSource))
                {

                    c = (T)((object)bi);

                }

                else if (typeof(T) == typeof(Image))
                {



                    Image img = new Image();

                    img.Source = bi;

                    c = (T)((object)img);

                }

            }



            sri.Stream.Close();

            sri.Stream.Dispose();



            return c;
        }
    }

} // Editor.Utilities
