﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Workbench.AddIn.Services.File
{
    public delegate void DocumentSavedHandler(Workbench.AddIn.UI.Layout.IDocumentBase document);

    /// <summary>
    /// 
    /// </summary>
    public interface IWorkbenchSaveService
    {
        #region Events

        event DocumentSavedHandler DocumentSaved;

        #endregion // Events
    } // IWorkbenchSaveService
    
    /// <summary>
    /// 
    /// </summary>
    public interface IWorkbenchSaveCurrentService
    {
        #region Methods
        bool SaveCurrentContent();
        #endregion
    } // IWorkbenchSaveCurrentService
} // Workbench.AddIn.Services.File
