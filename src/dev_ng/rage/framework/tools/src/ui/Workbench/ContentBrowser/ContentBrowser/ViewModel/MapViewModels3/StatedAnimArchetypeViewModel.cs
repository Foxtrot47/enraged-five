﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using RSG.Model.Common.Map;
using System.ComponentModel;
using RSG.Model.Common;

namespace ContentBrowser.ViewModel.MapViewModels3
{
    /// <summary>
    /// 
    /// </summary>
    public class StatedAnimArchetypeViewModel : ContainerViewModelBase, IDisposable, IWeakEventListener
    {
        #region Members
        /// <summary>
        /// Thread synchronisation helper object
        /// </summary>
        private object m_syncObject = new object();
        #endregion // Members

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        private IStatedAnimArchetype StatedAnimArchetype
        {
            get
            {
                return (IStatedAnimArchetype)Model;
            }
        }
        #endregion // Properties

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public StatedAnimArchetypeViewModel(IStatedAnimArchetype archetype)
            : base(archetype)
        {
            PropertyChangedEventManager.AddListener(archetype, this, "ComponentArchetypes");
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// This gets called whenever the selection state for this view model
        /// has changed
        /// </summary>
        protected override void OnSelectionChanged(Boolean oldValue, Boolean newValue)
        {
            if (IsSelected)
            {
                lock (m_syncObject)
                {
                    StatedAnimArchetype.RequestAllStatistics(true);
                }
            }

            RefreshGrid();
        }

        /// <summary>
        /// This gets called whenever the expansion state for this view model
        /// has changed
        /// </summary>
        protected override void OnExpansionChanged(Boolean oldValue, Boolean newValue)
        {
            if (IsExpanded)
            {
                lock (m_syncObject)
                {
                    StatedAnimArchetype.RequestStatistics(new StreamableStat[] { StreamableStatedAnimArchetypeStat.ComponentArchetypes }, true);
                }
            }

            RefreshHierarchy();
        }

        /// <summary>
        /// 
        /// </summary>
        protected void RefreshHierarchy()
        {
            this.AssetChildren.BeginUpdate();
            this.AssetChildren.Clear();
            if (IsExpanded)
            {
                if (!StatedAnimArchetype.IsStatLoaded(StreamableStatedAnimArchetypeStat.ComponentArchetypes))
                {
                    this.AssetChildren.Add(new AssetDummyViewModel("loading..."));
                }
                else
                {
                    this.AssetChildren.AddRange(StatedAnimArchetype.ComponentArchetypes.Select(texture => ViewModelFactory.CreateViewModel(texture)));
                }
            }
            else
            {
                this.AssetChildren.Add(new AssetDummyViewModel());
            }
            this.AssetChildren.EndUpdate();
        }

        /// <summary>
        /// 
        /// </summary>
        protected void RefreshGrid()
        {
            this.GridAssets.BeginUpdate();
            this.GridAssets.Clear();
            if (IsSelected)
            {
                if (StatedAnimArchetype.IsStatLoaded(StreamableStatedAnimArchetypeStat.ComponentArchetypes))
                {
                    this.GridAssets.AddRange(StatedAnimArchetype.ComponentArchetypes.Select(texture => ViewModelFactory.CreateGridViewModel(texture)));
                }
            }
            this.GridAssets.EndUpdate();
        }
        #endregion // Overrides

        #region IWeakEventListener Implementation
        /// <summary>
        /// 
        /// </summary>
        public bool ReceiveWeakEvent(Type managerType, Object sender, EventArgs e)
        {
            if (Application.Current != null)
            {
                Application.Current.Dispatcher.Invoke(
                    new Action
                    (
                        delegate()
                        {
                            RefreshHierarchy();
                            RefreshGrid();
                        }
                    ),
                    System.Windows.Threading.DispatcherPriority.ContextIdle
                );
            }
            else
            {
                RefreshHierarchy();
                RefreshGrid();
            }
            return true;
        }
        #endregion // IWeakEventListener

        #region IDisposable Implementation
        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            PropertyChangedEventManager.RemoveListener(StatedAnimArchetype, this, "ComponentArchetypes");
        }
        #endregion // IDisposable Implementation
    } // StatedAnimArchetypeViewModel
}
