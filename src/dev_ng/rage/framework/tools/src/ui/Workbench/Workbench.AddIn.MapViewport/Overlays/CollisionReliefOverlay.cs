﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MapViewport.AddIn;
using System.Windows.Media.Imaging;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using RSG.Base.Math;
using System.IO;
using System.Windows.Media;
using Workbench.AddIn.Services.Model;

namespace Workbench.AddIn.MapViewport.Overlays
{
    /// <summary>
    /// Displays collision coverage map.
    /// </summary>
    [ExportExtension(Viewport.AddIn.ExtensionPoints.MiscellaneousOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class CollisionReliefOverlay : LevelDependentViewportOverlay
    {
        #region Constants

        /// <summary>
        /// Overlay name.
        /// </summary>
        private const String c_name = "Collision Hole Relief Map";

        /// <summary>
        /// Overlay description.
        /// </summary>
        private const String c_description = "Shows the collision mesh coverage as a relief map.";

        #endregion

        #region MEF Imports

        /// <summary>
        /// Level browser.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LevelBrowser, typeof(ILevelBrowser))]
        Lazy<ILevelBrowser> LevelBrowserProxy { get; set; }

        /// <summary>
        /// The main content browser export object; will also be used as the view model
        /// for the content browsers main window
        /// </summary>
        [Workbench.AddIn.ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(Workbench.AddIn.Services.IConfigurationService))]
        private Lazy<Workbench.AddIn.Services.IConfigurationService> ConfigurationService { get; set; }

        /// <summary>
        /// The perforce sync service
        /// </summary>
        [Workbench.AddIn.ImportExtension(Workbench.AddIn.CompositionPoints.PerforceSyncService, typeof(Workbench.AddIn.Services.IPerforceSyncService))]
        private Lazy<Workbench.AddIn.Services.IPerforceSyncService> PerforceSyncService { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        public CollisionReliefOverlay()
            : base(c_name, c_description)
        {

        }

        #endregion

        #region Public overridden methods

        /// <summary>
        /// Draw the image data (if available)
        /// </summary>
        public override void Activated()
        {
            base.Activated();

            // Make sure the user has the appropriate files synced
            String filepath = Path.Combine(ConfigurationService.Value.GameConfig.AssetsDir, "reports\\collision.png");
            int syncedFiles = 0;
            PerforceSyncService.Value.Show("You current have some processed zip files that are out of date.\n" +
                                           "Would you like to grab latest now?\n\n" +
                                           "You can select which files to grab using the table below.\n",
                                           new String[] { filepath }, ref syncedFiles, false, true);

            Geometry.BeginUpdate();
            Geometry.Clear();

            if (File.Exists(filepath))
            {
                DrawData();
            }

            Geometry.EndUpdate();
        }

        #endregion

        #region Private helper methods

        /// <summary>
        /// Draw the collision data image.
        /// </summary>
        private void DrawData()
        {
            if (LevelBrowserProxy.Value == null || LevelBrowserProxy.Value.SelectedLevel == null)
            {
                return;
            }

            var currentLevel = LevelBrowserProxy.Value.SelectedLevel;
            string pathToImage = Path.Combine(ConfigurationService.Value.GameConfig.AssetsDir, "reports\\collision.png");
           
            RSG.Base.Math.Vector2f size = new RSG.Base.Math.Vector2f(966.0f, 627.0f);
            RSG.Base.Math.Vector2f pos = new RSG.Base.Math.Vector2f(-483.0f, 627.0f);
            if (currentLevel.ImageBounds != null)
            {
                size = new RSG.Base.Math.Vector2f(currentLevel.ImageBounds.Max.X - currentLevel.ImageBounds.Min.X, currentLevel.ImageBounds.Max.Y - currentLevel.ImageBounds.Min.Y);
                pos = new RSG.Base.Math.Vector2f(currentLevel.ImageBounds.Min.X, currentLevel.ImageBounds.Max.Y);
            }

            BitmapImage image = new BitmapImage(new Uri(pathToImage));
            Viewport2DImage viewportImg = new Viewport2DImage(etCoordSpace.World, "Collision Relief Data", image, pos, size);
            Geometry.Add(viewportImg);
        }

        #endregion
    }
}
