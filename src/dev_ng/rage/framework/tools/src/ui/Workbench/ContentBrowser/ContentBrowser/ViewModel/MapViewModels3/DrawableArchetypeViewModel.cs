﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common.Map;

namespace ContentBrowser.ViewModel.MapViewModels3
{
    /// <summary>
    /// 
    /// </summary>
    public class DrawableArchetypeViewModel : MapArchetypeViewModel
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        private IDrawableArchetype DrawableArchetype
        {
            get
            {
                return (IDrawableArchetype)Model;
            }
        }
        #endregion // Properties

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public DrawableArchetypeViewModel(IDrawableArchetype archetype)
            : base(archetype)
        {
        }
        #endregion // Constructor(s)
    } // DrawableArchetypeViewModel
}
