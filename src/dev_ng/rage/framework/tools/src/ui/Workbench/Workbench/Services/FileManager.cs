﻿using System;
using System.IO;
using System.Windows;
using System.Diagnostics;
using System.Reflection;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using Workbench.AddIn;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.Services;
using Workbench.AddIn.Services.File;

namespace Workbench.Services
{
    /// <summary>
    /// Responsible for mantain the states of the files that are currently open
    /// and making sure the correct messages are sent.
    /// </summary>
    [ExportExtension(Workbench.AddIn.CompositionPoints.WorkbenchFileManager, typeof(IFileManager))]
    [ExportExtension(Workbench.AddIn.ExtensionPoints.Void, typeof(IFileManager))]
    public class FileManager : IFileManager, IPartImportsSatisfiedNotification
    {
        #region MEF Imports
        /// <summary>
        /// MEF Import for Workbench Core Service Provider.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.CoreServicesProvider,
            typeof(ICoreServiceProvider))]
        private ICoreServiceProvider CoreServiceProvider { get; set; }

        /// <summary>
        /// MEF import for all ISaveServices.
        /// </summary>
        [ImportManyExtension(Workbench.AddIn.ExtensionPoints.SaveService, 
            typeof(ISaveService))]
        private IEnumerable<ISaveService> SaveServices { get; set; }
        
        [ImportExtension(Workbench.AddIn.CompositionPoints.UndoRedoService, 
            typeof(Workbench.AddIn.Services.IUndoRedoService))]
        private Workbench.AddIn.Services.IUndoRedoService UndoRedoService { get; set; }

        [ImportManyExtension(Workbench.AddIn.CompositionPoints.WorkbenchSaveService, 
            typeof(IWorkbenchSaveService))]
        private IEnumerable<IWorkbenchSaveService> WorkbenchSaveServices { get; set; }
        #endregion // MEF Imports

        #region Properties

        /// <summary>
        /// A dictionary of filewatchers to documents
        /// </summary>
        private Dictionary<FileSystemWatcher, IDocumentBase> FileWatchers
        {
            get { return m_fileWatchers; }
            set { m_fileWatchers = value; }
        }
        private Dictionary<FileSystemWatcher, IDocumentBase> m_fileWatchers = new Dictionary<FileSystemWatcher, IDocumentBase>();

        /// <summary>
        /// Dictionary of documents and filenames
        /// </summary>
        private Dictionary<IDocumentBase, List<String>> DocumentFilenames
        {
            get { return m_documentFilenames; }
            set { m_documentFilenames = value; }
        }
        private Dictionary<IDocumentBase, List<String>> m_documentFilenames = new Dictionary<IDocumentBase, List<String>>();

        /// <summary>
        /// Dictionary of filenames and last write times
        /// </summary>
        private Dictionary<String, DateTime> FilenameTimes
        {
            get { return m_filenameTimes; }
            set { m_filenameTimes = value; }
        }
        private Dictionary<String, DateTime> m_filenameTimes = new Dictionary<String, DateTime>();

        /// <summary>
        /// Dictionary of documents and filenames
        /// </summary>
        private Dictionary<IModel, List<IDocumentBase>> Models
        {
            get { return m_models; }
            set { m_models = value; }
        }
        private Dictionary<IModel, List<IDocumentBase>> m_models = new Dictionary<IModel, List<IDocumentBase>>();

        /// <summary>
        /// Dictionary of documents and filenames
        /// </summary>
        private Dictionary<IDocumentBase, List<IModel>> Contents
        {
            get { return m_contents; }
            set { m_contents = value; }
        }
        private Dictionary<IDocumentBase, List<IModel>> m_contents = new Dictionary<IDocumentBase, List<IModel>>();
        
        #endregion // Properties
        
        #region IPartImportsSatisfiedNotification

        /// <summary>
        /// Called when a part's imports have been satisfied and it is safe to use.
        /// </summary>
        public void OnImportsSatisfied()
        {
            this.CoreServiceProvider.LayoutManager.DocumentCreated += OnDocumentCreated;

            foreach (var saveService in WorkbenchSaveServices)
            {
                saveService.DocumentSaved += OnDocumentSaved;
            }
        }
                
        #endregion // IPartImportsSatisfiedNotification

        #region Event Callbacks

        /// <summary>
        /// 
        /// </summary>
        private void OnDocumentCreated(IDocumentBase document)
        {
            document.PathChanged += OnDocumentPathChanged;
            document.Closing += OnDocumentClosing;
            document.Closed += OnDocumentClosed;

            System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();

            if (!String.IsNullOrEmpty(document.Path))
            {
                String[] filenames = document.Path.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);

                Debug.Assert(!this.DocumentFilenames.ContainsKey(document), "Dictionary shouldn't contain document if not already opened.");
                if (!this.DocumentFilenames.ContainsKey(document))
                    this.DocumentFilenames.Add(document, filenames.ToList());

                Boolean isReadOnly = false;
                foreach (String filename in filenames)
                {
                    String file = filename.EndsWith("[Read Only]") ? filename.Replace("[Read Only]", "") : filename;
                    string[] parts = file.Split(new char[1] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                    FileInfo info = new FileInfo(parts[0]);
                    if (info.Exists)
                    {
                        FileSystemWatcher fileWatcher = new FileSystemWatcher();
                        String directory = System.IO.Path.GetDirectoryName(file);
                        fileWatcher.Path = directory;
                        fileWatcher.Filter = System.IO.Path.GetFileName(file);
                        fileWatcher.NotifyFilter = NotifyFilters.Attributes | NotifyFilters.LastWrite;
                        fileWatcher.Changed += OnFileChanged;
                        fileWatcher.EnableRaisingEvents = true;

                        this.FileWatchers.Add(fileWatcher, document);
                        if (info.IsReadOnly)
                            isReadOnly = true;

                        if (!this.FilenameTimes.ContainsKey(file))
                            this.FilenameTimes.Add(file, info.LastWriteTime);
                    }
                }
                if (isReadOnly == true)
                    document.IsReadOnly = true;
                else
                    document.IsReadOnly = false;
            }

            if (document.SaveModel != null)
            {
                IModel model = document.SaveModel;
                model.PropertyChanged += OnModelPropertyChanged;
                model.ModifiedStateChanged += OnModifiedStateChanged;

                if (!this.Models.ContainsKey(model))
                    this.Models.Add(model, new List<IDocumentBase>());

                this.Models[model].Add(document);
                this.Contents.Add(document, new List<IModel>());
                this.Contents[document].Add(model);
                this.UndoRedoService.RegisterContent(document, new List<IModel>() { model });
            }
            sw.Stop();
            RSG.Base.Logging.Log.Log__Message("Registering document has finished and took {0} milliseconds", sw.ElapsedMilliseconds);
        }
        
        /// <summary>
        /// Get called when a document is closing
        /// </summary>
        private void OnDocumentClosing(Object sender, CancelEventArgs e)
        {
            IDocumentBase document = sender as IDocumentBase;
            if (!AllowDocumentToClose(document))
                e.Cancel = true;
        }

        /// <summary>
        /// 
        /// </summary>
        private void OnDocumentClosed(Object sender, EventArgs e)
        {
            IDocumentBase document = sender as IDocumentBase;

            DestroyDocumentApartFromWatcher(document);

            List<FileSystemWatcher> watchers = new List<FileSystemWatcher>();
            foreach (KeyValuePair<FileSystemWatcher, IDocumentBase> watcher in this.FileWatchers)
            {
                if (watcher.Value == document)
                    watchers.Add(watcher.Key);
            }
            foreach (FileSystemWatcher watcher in watchers)
            {
                watcher.Dispose();
                this.FileWatchers.Remove(watcher);
            }
        }

        /// <summary>
        /// Gets called when a documents path suddenly changes i.e. when
        /// the document is saved with a save as.
        /// </summary>
        private void OnDocumentPathChanged(Object sender, String oldValue, String newValue)
        {
            IDocumentBase document = (sender as IDocumentBase);
            if (document != null)
            {
                if (this.DocumentFilenames.ContainsKey(document))
                    this.DocumentFilenames.Remove(document);

                List<FileSystemWatcher> watchers = new List<FileSystemWatcher>();
                foreach (KeyValuePair<FileSystemWatcher, IDocumentBase> watcher in this.FileWatchers)
                {
                    if (watcher.Value == document)
                        watchers.Add(watcher.Key);
                }
                foreach (FileSystemWatcher watcher in watchers)
                {
                    watcher.Dispose();
                    this.FileWatchers.Remove(watcher);
                }

                if (oldValue != null)
                {
                    String[] oldFilenames = oldValue.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (String filename in oldFilenames)
                    {
                        if (this.FilenameTimes.ContainsKey(filename))
                            this.FilenameTimes.Remove(filename);
                    }
                }

                if (!String.IsNullOrEmpty(document.Path))
                {
                    String[] filenames = document.Path.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
                    Debug.Assert(!this.DocumentFilenames.ContainsKey(document), "Dictionary shouldn't contain document if not already opened.");
                    if (!this.DocumentFilenames.ContainsKey(document))
                        this.DocumentFilenames.Add(document, filenames.ToList());

                    Boolean isReadOnly = false;
                    foreach (String filename in filenames)
                    {
                        FileInfo info = new FileInfo(filename);
                        if (info.Exists)
                        {
                            FileSystemWatcher fileWatcher = new FileSystemWatcher();
                            String directory = System.IO.Path.GetDirectoryName(filename);
                            fileWatcher.Path = directory;
                            fileWatcher.Filter = System.IO.Path.GetFileName(filename);
                            fileWatcher.NotifyFilter = NotifyFilters.Attributes | NotifyFilters.LastWrite;
                            fileWatcher.Changed += OnFileChanged;
                            fileWatcher.EnableRaisingEvents = true;

                            this.FileWatchers.Add(fileWatcher, document);
                            if (info.IsReadOnly)
                                isReadOnly = true;

                            if (!this.FilenameTimes.ContainsKey(filename))
                                this.FilenameTimes.Add(filename, info.LastWriteTime);
                        }
                    }
                    if (isReadOnly == true)
                        document.IsReadOnly = true;
                    else
                        document.IsReadOnly = false;
                }
            }
        }

        /// <summary>
        /// Gets called if n attribute or if file itself changes that is currently
        /// attached to a document
        /// </summary>
        private void OnFileChanged(Object sender, FileSystemEventArgs e)
        {
            FileSystemWatcher watcher = (sender as FileSystemWatcher);
            if (watcher != null)
            {
                if (this.FileWatchers.ContainsKey(watcher) && this.DocumentFilenames.ContainsKey(this.FileWatchers[watcher]))
                {
                    bool removed = false;
                    IDocumentBase doc = this.FileWatchers[watcher];
                    try
                    {
                        this.FileWatchers[watcher].Dispatcher.Invoke
                        (
                            new Action
                            (
                                delegate
                                {
                                    Boolean isReadOnly = false;
                                    foreach (String filename in this.DocumentFilenames[doc])
                                    {
                                        FileInfo info = new FileInfo(filename);
                                        if (info.Exists)
                                        {
                                            if (info.IsReadOnly)
                                                isReadOnly = true;
                                        }
                                        if (this.FilenameTimes.ContainsKey(filename))
                                        {
                                            if (this.FilenameTimes[filename] < info.LastWriteTime)
                                            {
                                                this.FilenameTimes[filename] = info.LastWriteTime;

                                                // The file has been modified externally, need to show a message
                                                // box with the appropriate messages.
                                                ReloadWindow reloadWindow = new ReloadWindow();
                                                reloadWindow.DataContext = new ReloadWindowViewModel(filename);
                                                reloadWindow.Owner = App.Current.MainWindow;
                                                if (reloadWindow.ShowDialog() == true)
                                                {
                                                    if (doc.OpenService != null)
                                                    {
                                                        DestroyDocumentApartFromWatcher(doc);
                                                        this.CoreServiceProvider.LayoutManager.CloseDocument(doc);

                                                        IDocumentBase newDoc = null;
                                                        IModel model = null;
                                                        doc.OpenService.Open(out newDoc, out model, filename, 0);
                                                        if (newDoc != null)
                                                        {
                                                            newDoc.Path = doc.Path;
                                                            newDoc.OpenService = doc.OpenService;
                                                            newDoc.SaveModel = model;
                                                            this.CoreServiceProvider.LayoutManager.ShowDocument(newDoc);
                                                        }
                                                        removed = true;
                                                    }
                                                    else
                                                    {
                                                        this.CoreServiceProvider.UserInterfaceService.Show("Unable to reload document due to a internal error.", MessageBoxButton.OK, MessageBoxImage.Error);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (!removed)
                                    {
                                        if (isReadOnly == true)
                                            this.FileWatchers[watcher].IsReadOnly = true;
                                        else
                                            this.FileWatchers[watcher].IsReadOnly = false;
                                    }
                                }
                            )
                        );
                        if (removed)
                        {
                            List<FileSystemWatcher> watchers = new List<FileSystemWatcher>();
                            foreach (KeyValuePair<FileSystemWatcher, IDocumentBase> watch in this.FileWatchers)
                            {
                                if (watch.Value == doc)
                                    watchers.Add(watch.Key);
                            }
                            foreach (FileSystemWatcher watch in watchers)
                            {
                                watcher.Dispose();
                                this.FileWatchers.Remove(watcher);
                            }
                        }
                    }
                    catch
                    {
                    }
                }
            }
        }

        private void DestroyDocumentApartFromWatcher(IDocumentBase document)
        {
            document.PathChanged -= OnDocumentPathChanged;
            document.Closing -= OnDocumentClosing;
            document.Closed -= OnDocumentClosed;

            if (this.DocumentFilenames.ContainsKey(document))
                this.DocumentFilenames.Remove(document);

            if (this.Contents.ContainsKey(document))
            {
                foreach (IModel model in this.Contents[document])
                {
                    model.PropertyChanged -= OnModelPropertyChanged;
                }
                this.Contents.Remove(document);
            }

            List<IModel> clearedModels = new List<IModel>();
            foreach (var model in this.Models)
            {
                if (model.Value.Contains(document))
                    model.Value.Remove(document);
                if (model.Value.Count == 0)
                    clearedModels.Add(model.Key);
            }
            foreach (IModel model in clearedModels)
            {
                this.Models.Remove(model);
                model.PropertyChanged -= OnModelPropertyChanged;
                model.ModifiedStateChanged -= OnModifiedStateChanged;
            }
            if (!String.IsNullOrEmpty(document.Path))
            {
                String[] filenames = document.Path.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (String filename in filenames)
                {
                    if (this.FilenameTimes.ContainsKey(filename))
                        this.FilenameTimes.Remove(filename);
                }
            }

            this.UndoRedoService.UnregisterContent(document);
        }

        /// <summary>
        /// 
        /// </summary>
        private void OnDocumentSaved(IDocumentBase document)
        {
            foreach (var model in this.Models)
            {
                if (model.Value.Contains(document))
                {
                    model.Key.IsModified = false;
                }
            }
            if (!String.IsNullOrEmpty(document.Path))
            {
                String[] filenames = document.Path.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (String filename in filenames)
                {
                    if (this.FilenameTimes.ContainsKey(filename))
                    {
                        this.FilenameTimes.Remove(filename);
                        this.FilenameTimes.Add(filename, DateTime.Now);
                    }
                    else
                    {
                        this.FilenameTimes.Add(filename, DateTime.Now);
                    }
                }
            }

            document.IsModified = false;
            this.UndoRedoService.OnDocumentSaved(document);
        }

        #endregion // Event Callbacks

        #region Controller Methods

        /// <summary>
        /// Gets called just before the application exits
        /// </summary>
        public Boolean OnExiting()
        {
            foreach (IDocumentBase doc in this.CoreServiceProvider.LayoutManager.Documents)
            {
                if (!AllowDocumentToClose(doc))
                    return false;
            }
            return true;
        }

        #endregion // Controller Methods

        #region Private Functions

        private Dictionary<Type, List<String>> m_reflectedProperties = new Dictionary<Type, List<String>>();

        /// <summary>
        /// Goes through the model recursively and
        /// collects all the IModel objects
        /// </summary>
        private void GetModelsFromObject(Object obj, ref List<IModel> models)
        {
            if (obj != null && !models.Contains(obj))
            {
                if (obj is IModel)
                {
                    models.Add(obj as IModel);
                }

                // Determine if this is a dictionary/collection and walk through the children
                if (obj is System.Collections.IDictionary)
                {
                    Type dictionaryType = obj.GetType().GetInterface("IDictionary`2");
                    Boolean keyValid = dictionaryType.GetGenericArguments()[0].GetInterface("IModel", true) != null;
                    Boolean valueValid = dictionaryType.GetGenericArguments()[1].GetInterface("IModel", true) != null;
                    foreach (var entry in obj as System.Collections.IDictionary)
                    {
                        if (keyValid)
                        {
                            Object child = ((System.Collections.DictionaryEntry)entry).Key;
                            if (child is IModel)
                                GetModelsFromObject(child as IModel, ref models);
                        }
                        if (valueValid)
                        {
                            Object child = ((System.Collections.DictionaryEntry)entry).Value;
                            if (child is IModel)
                                GetModelsFromObject(child as IModel, ref models);
                        }
                    }
                }
                else if (obj is System.Collections.ICollection)
                {
                    foreach (var entry in (obj as System.Collections.ICollection))
                    {
                        if (entry is IModel)
                            GetModelsFromObject(entry as IModel, ref models);
                    }
                }

                if (obj is IModel)
                {
                    Type objType = obj.GetType();
                    if (!m_reflectedProperties.ContainsKey(objType))
                    {
                        m_reflectedProperties.Add(objType, new List<string>());
                        // Go through the properties
                        PropertyInfo[] properties = objType.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                        foreach (PropertyInfo property in properties)
                        {
                            try
                            {
                                Object[] dontRegisterAttrs = property.GetCustomAttributes(typeof(RSG.Base.Editor.Command.DontRegister), true);
                                if (dontRegisterAttrs.Length == 0)
                                {
                                    ParameterInfo[] parameters = property.GetIndexParameters();
                                    if (parameters.Length == 0)
                                    {
                                        m_reflectedProperties[objType].Add(property.Name);
                                        Object propertyValue = property.GetValue(obj, null);
                                        if (propertyValue != null)
                                            GetModelsFromObject(propertyValue, ref models);
                                    }
                                }
                            }
                            catch
                            {
                            }
                        }
                    }
                    else
                    {
                        foreach (String propertyName in m_reflectedProperties[objType])
                        {
                            Object propertyValue = objType.GetProperty(propertyName).GetValue(obj, null);
                            if (propertyValue != null)
                                GetModelsFromObject(propertyValue, ref models);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private ISaveService GetSaveService(IDocumentBase document)
        {
            if (document == null)
            {
                return null;
            }
            else
            {
                if (document.ID == Guid.Empty || document.ID == null)
                {
                    return null;
                }
                else
                {
                    // Check that there is a save service available for this
                    // content
                    foreach (ISaveService service in this.SaveServices)
                    {
                        List<Guid> supportedContent = new List<Guid>(service.SupportedContent);
                        if (supportedContent.Contains(document.ID))
                        {
                            if (service.CanExecuteSaveAs() == true)
                            {
                                return service;
                            }
                        }
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Handles the moment just before a document closes
        /// </summary>
        private Boolean AllowDocumentToClose(IDocumentBase document)
        {
            ISaveService saveService = GetSaveService(document);
            Boolean cancel = false;
            if (document.IsModified == true && saveService != null)
            {
                Boolean attamptSave = false;
                List<String> filenames = new List<string>();
                if (document.Path != null)
                {
                    filenames = document.Path.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                }
                else
                {
                    filenames.Add("Untitled");
                }
                SaveContentWindow window = new SaveContentWindow(filenames);
                window.Owner = App.Current.MainWindow;
                window.ShowDialog();
                switch (window.Result)
                {
                    case MessageBoxResult.Cancel:
                        cancel = true;
                        break;
                    case MessageBoxResult.No:
                        cancel = false;
                        break;

                    case MessageBoxResult.Yes:
                    default:
                        cancel = false;
                        attamptSave = true;
                        break;
                }

                if (cancel == false && attamptSave == true)
                {
                    List<FileSystemWatcher> watchers = new List<FileSystemWatcher>();
                    foreach (KeyValuePair<FileSystemWatcher, IDocumentBase> watcher in this.FileWatchers)
                    {
                        if (watcher.Value == document)
                            watchers.Add(watcher.Key);
                    }

                    foreach (String filename in filenames)
                    {
                        Boolean save = false;
                        String savePath = filename;
                        int filterIndex = -1;
                        FileInfo info = new FileInfo(filename);
                        if (info.Exists)
                        {
                            if (info.IsReadOnly == true)
                            {
                                SaveReadOnlyWindow readonlyWindow = new SaveReadOnlyWindow(filename);
                                readonlyWindow.Owner = App.Current.MainWindow;
                                readonlyWindow.ShowDialog();
                                switch (readonlyWindow.Result)
                                {
                                    case ReadOnlyResult.Cancel:
                                        {
                                            cancel = true;
                                        }
                                        break;
                                    case ReadOnlyResult.Overwrite:
                                        {
                                            List<FileSystemWatcher> fileWatchers = new List<FileSystemWatcher>();
                                            foreach (FileSystemWatcher watcher in watchers)
                                            {
                                                if (System.IO.Path.Combine(watcher.Path, watcher.Filter) == filename)
                                                {
                                                    watcher.EnableRaisingEvents = false;
                                                    fileWatchers.Add(watcher);
                                                }
                                            }
                                            foreach (FileSystemWatcher fileWatcher in fileWatchers)
                                            {
                                                fileWatcher.Dispose();
                                                this.FileWatchers.Remove(fileWatcher);
                                            }
                                            info.IsReadOnly = false;
                                            save = true;
                                        }
                                        break;
                                    case ReadOnlyResult.SaveAs:
                                        {
                                            String path = String.Empty;

                                            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
                                            dlg.Filter = saveService.Filter;
                                            dlg.FilterIndex = saveService.DefaultFilterIndex;

                                            Nullable<bool> result = dlg.ShowDialog();
                                            if (false == result)
                                            {
                                                save = false;
                                            }
                                            else
                                            {
                                                save = true;
                                                savePath = dlg.FileName;
                                                filterIndex = dlg.FilterIndex;
                                            }
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                        else
                        {
                            String path = String.Empty;

                            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
                            dlg.Filter = saveService.Filter;
                            dlg.FilterIndex = saveService.DefaultFilterIndex;

                            Nullable<bool> result = dlg.ShowDialog();
                            if (false == result)
                            {
                                cancel = true;
                                return !cancel;
                            }

                            filterIndex = dlg.FilterIndex;
                            savePath = dlg.FileName;
                            save = true;
                        }
                        if (save == true)
                        {
                            Boolean setPathToSavePath = true;
                            Boolean saveResult = saveService.Save(document.SaveModel, savePath, filterIndex, ref setPathToSavePath);
                            if (saveResult)
                            {
                                RSG.Base.Logging.Log.Log__Debug("SaveAsService {0} successful.", saveService.ID);
                            }
                            else
                            {
                                RSG.Base.Logging.Log.Log__Error("SaveAsService {0} failed.", saveService.ID);
                            }
                        }
                    }
                }
            }
            return !cancel;
        }

        /// <summary>
        /// A models modified state has changed
        /// </summary>
        private void OnModifiedStateChanged(Object sender, ModifiedStateChangedEventArgs e)
        {
            e.Model.IsModified = e.OriginalModel.IsModified;
            if (!m_handleModifyChanged)
                return;

            if (sender is IModel)
            {
                if (this.Models.ContainsKey(sender as IModel))
                {

                    List<IDocumentBase> documents = this.Models[sender as IModel];
                    if (documents != null)
                    {
                        if (e.OriginalModel.IsModified == false)
                        {
                            foreach (IDocumentBase document in documents)
                            {
                                document.IsModified = false;
                            }
                        }
                        else
                        {
                            foreach (IDocumentBase document in documents)
                                document.IsModified = true;
                        }
                    }
                }
            }
            e.Handled = true;
        }

        private bool m_handleModifyChanged = true;

        /// <summary>
        /// Called whenever a property value changes in a registered model
        /// </summary>
        private void OnModelPropertyChanged(Object sender, PropertyChangedEventArgs e)
        {
            if (e is RSG.Base.Editor.Command.EditorPropertyChangedEventArgs)
            {
                RSG.Base.Editor.Command.EditorPropertyChangedEventArgs editorArgs = e as RSG.Base.Editor.Command.EditorPropertyChangedEventArgs;
                editorArgs.Handled = true;
                editorArgs.Model.IsModified = true;

                m_handleModifyChanged = false;
                editorArgs.OriginalModel.IsModified = true;
                m_handleModifyChanged = true;
            }
        }

        #endregion // Private Functions
    } // FileManger
} // Workbench.Services
