﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn.Commands;
using ragCore;

namespace WidgetEditor.UI.Toolbar
{
    /// <summary>
    /// 
    /// </summary>
    public class ConnectionsComboboxItem : WorkbenchCommandLabel
    {
        #region Constants
        /// <summary>
        /// 
        /// </summary>
        public static readonly Guid GUID = new Guid("0B1BDB02-54F7-4BA7-A499-C0E6B9CE80AE");
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Build this label is for
        /// </summary>
        public GameConnection GameConnection
        {
            get;
            protected set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="connection"></param>
        public ConnectionsComboboxItem(GameConnection connection)
            : base()
        {
            this.ID = GUID;
            this.Header = String.Format("{0} ({1})", connection.Platform, connection.IPAddress);
            GameConnection = connection;
        }
        #endregion // Constructor(s)
    } // ConnectionsComboboxItem
}
