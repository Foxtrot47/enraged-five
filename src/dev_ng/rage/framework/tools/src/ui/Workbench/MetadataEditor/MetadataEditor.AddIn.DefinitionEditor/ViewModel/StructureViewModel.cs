﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using SIO = System.IO;
using System.Windows.Media.Imaging;
using RSG.Metadata.Model;
using RSG.Metadata.Parser;
using RSG.Base.Editor;

namespace MetadataEditor.AddIn.DefinitionEditor.ViewModel
{

    /// <summary>
    /// 
    /// </summary>
    public class StructureViewModel :
        RSG.Base.Editor.HierarchicalViewModelBase
    {
        #region Properties
        /// <summary>
        /// Modified property.
        /// </summary>
        public bool Modified { get; set; }

        /// <summary>
        /// Structure model data.
        /// </summary>
        public RSG.Metadata.Parser.Structure Model { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public new ObservableCollection<Object> Children { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        public StructureViewModel(RSG.Metadata.Parser.Structure m)
        {
            this.Model = m;
            this.Children = new ObservableCollection<Object>();
            if (m != null && m.ImmediateDescendants != null)
            {
                foreach (Structure child in m.ImmediateDescendants)
                {
                    this.Children.Add(new StructureViewModel(child, this));
                }
            }
        }

        public StructureViewModel(RSG.Metadata.Parser.Structure m, IViewModel parent)
        {
            this.Model = m;
            this.Parent = parent;
            this.Children = new ObservableCollection<Object>();
            if (m != null && m.ImmediateDescendants != null)
            {
                foreach (Structure child in m.ImmediateDescendants)
                {
                    this.Children.Add(new StructureViewModel(child));
                }
            }
        }
        #endregion // Constructor(s)

        #region Controller Methods
        
        ///// <summary>
        ///// Postprocess the member.
        ///// </summary>
        ///// Ensures all definitions are loaded prior to attempting to complete
        ///// the IMember structures.
        //public void Postprocess(StructureDictionary defDict)
        //{
        //    this.Model.Postprocess(defDict);

        //    this.Children = new ObservableCollection<Object>();
        //    if (this.Model != null && this.Model.ImmediateDescendants != null)
        //    {
        //        foreach (Structure child in this.Model.ImmediateDescendants)
        //        {
        //            this.Children.Add(new StructureViewModel(child));
        //        }
        //    }
        //}

        #endregion // Controller Methods
    }

} // RSG.Metadata.ViewModel namespace
