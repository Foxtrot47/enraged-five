﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Workbench.AddIn.UI.Layout;
using RSG.Base.Windows.Controls.WpfViewport2D;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using RSG.Base.Windows.Controls.WpfViewport2D.Controls;
using RSG.Model.Common;
using ContentBrowser.AddIn;
using Workbench.AddIn.Services.Model;

namespace Workbench.AddIn.MapViewport
{
    /// <summary>
    /// Interaction logic for MapViewportView.xaml
    /// </summary>
    public partial class MapViewportView : ToolWindowBase<MapViewportViewModel>, IWeakEventListener
    {
        #region Constants

        public static readonly Guid GUID = new Guid("40438167-5F76-4979-B096-49364AF6B35F");

        #endregion // Constants

        #region Properties

        /// <summary>
        /// Determines whether the tool window should be floating by default
        /// </summary>
        public override Boolean FloatByDefault
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gives the default size for a floating window. This 
        /// size will be used for the first time the window is set to float
        /// </summary>
        public override Size DefaultFloatSize
        {
            get
            {
                return new Size(460, 690);
            }
        }
        
        #endregion // Properties

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public MapViewportView(ILevelBrowser levelBrowser, List<Viewport.AddIn.IViewportOverlayGroup> overlayGroups, MapViewportViewModel viewModel)
            : base(Workbench.AddIn.MapViewport.Resources.Strings.MapViewport_Name,
                  viewModel)
        {
            InitializeComponent();

            this.ID = GUID;
            this.ViewModel.ViewportControl = this.Viewport;
            this.SaveModel = this.ViewModel;
            levelBrowser.LevelChanged += LevelChanged; 
            this.OnSelectedLevelChanged(null, levelBrowser.SelectedLevel);

            foreach (var group in this.ViewModel.OverlayGroups)
            {
                foreach (var overlay in group.Overlays)
                {
                    System.Collections.Specialized.CollectionChangedEventManager.AddListener(overlay.Geometry, this);
                    overlay.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(overlay_PropertyChanged);
                }
            }

            System.Collections.Specialized.CollectionChangedEventManager.AddListener(viewModel.GridGeometry, this);
        }

        void overlay_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Geometry")
            {
                System.Collections.Specialized.CollectionChangedEventManager.AddListener((sender as Viewport.AddIn.IViewportOverlay).Geometry, this);
                BindingOperations.GetMultiBindingExpression(Viewport, Viewport2D.GeometrySourceProperty).UpdateTarget();
            }
        }

        void LevelChanged(Object sender, LevelChangedEventArgs args)
        {
            this.OnSelectedLevelChanged(args.OldItem, args.NewItem);
        }

        public bool ReceiveWeakEvent(Type managerType, Object sender, EventArgs e)
        {
            if (managerType == typeof(System.Collections.Specialized.CollectionChangedEventManager))
            {
                if (Application.Current != null)
                {
                    Application.Current.Dispatcher.Invoke(
                        new Action
                        (
                            delegate()
                            {
                                UpdateGeomSourceProperty();
                            }
                        ),
                        System.Windows.Threading.DispatcherPriority.ContextIdle
                    );
                }
                else
                {
                    UpdateGeomSourceProperty();
                }
            }
            return true;
        }

        private void UpdateGeomSourceProperty()
        {
            BindingOperations.GetMultiBindingExpression(Viewport, Viewport2D.GeometrySourceProperty).UpdateTarget();
        }

        #endregion // Constructor

        #region Event Handlers

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oldLevel"></param>
        /// <param name="newLevel"></param>
        private void OnSelectedLevelChanged(ILevel oldLevel, ILevel newLevel)
        {
            if (newLevel != null)
            {
                this.Title = Char.ToUpper(newLevel.Name[0]) + newLevel.Name.Substring(1) + " Map";
            }
            else
            {
                this.Title = Workbench.AddIn.MapViewport.Resources.Strings.MapViewport_Title;
            }

            BindingOperations.GetMultiBindingExpression(Viewport, Viewport2D.GeometrySourceProperty).UpdateTarget();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MouseSelectionControl_SelectionChanged(Object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            this.ViewModel.SelectedOverlayGeometry.BeginUpdate();
            this.ViewModel.SelectedOverlayGeometry.Clear();

            foreach (var item in (sender as RSG.Base.Windows.Controls.WpfViewport2D.Controls.MouseSelectionControl).SelectedGeometryItems)
            {
                if (item != null)
                    this.ViewModel.SelectedOverlayGeometry.Add(item);
            }

            this.ViewModel.SelectedOverlayGeometry.EndUpdate();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MouseSelectionControl_Loaded(Object sender, RoutedEventArgs e)
        {
            this.ViewModel.MouseSelectControl = (sender as RSG.Base.Windows.Controls.WpfViewport2D.Controls.MouseSelectionControl);
        }

        /// <summary>
        /// Event handler for when the viewport is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MouseClickControl_ViewportClicked(Object sender, ViewportClickEventArgs e)
        {
            this.ViewModel.OnViewportClicked(sender, e);
        }

        /// <summary>
        /// Event handler for the mouse moving when over the viewport
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MouseLocationControl_MouseMoved(Object sender, MouseMoveEventArgs e)
        {
            this.ViewModel.OnMouseMoved(sender, e);
        }

        /// <summary>
        /// Event handler for when the active document changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnChangeActiveDocument(object sender, EventArgs e)
        {
            this.ViewModel.HideToolTip();
        }

        /// <summary>
        /// Event handler for losing keyboard focus.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnLostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            if (e.NewFocus == null)
            {
                this.ViewModel.HideToolTip();
            }
        }
        #endregion // Event Handlers

    } // MapViewportView

    /// <summary>
    /// 
    /// </summary>
    public static class TrackBehavior
    {
        public static readonly DependencyProperty TrackOpenLocationProperty = DependencyProperty.RegisterAttached("TrackOpenLocation", typeof(bool), typeof(TrackBehavior), new UIPropertyMetadata(false, OnTrackOpenLocationChanged));

        public static bool GetTrackOpenLocation(ContextMenu item)
        {
            return (bool)item.GetValue(TrackOpenLocationProperty);
        }

        public static void SetTrackOpenLocation(ContextMenu item, bool value)
        {
            item.SetValue(TrackOpenLocationProperty, value);
        }

        public static readonly DependencyProperty OpenLocationProperty = DependencyProperty.RegisterAttached("OpenLocation", typeof(Point), typeof(TrackBehavior), new UIPropertyMetadata(new Point()));

        public static Point GetOpenLocation(ContextMenu item)
        {
            return (Point)item.GetValue(OpenLocationProperty);
        }

        public static void SetOpenLocation(ContextMenu item, Point value)
        {
            item.SetValue(OpenLocationProperty, value);
        }

        static void OnTrackOpenLocationChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            var menu = dependencyObject as ContextMenu;
            if (menu == null)
            {
                return;
            }

            if (!(e.NewValue is bool))
            {
                return;
            }

            if ((bool)e.NewValue)
            {
                menu.Opened += menu_Opened;

            }
            else
            {
                menu.Opened -= menu_Opened;
            }
        }

        static void menu_Opened(object sender, RoutedEventArgs e)
        {
            if (!ReferenceEquals(sender, e.OriginalSource))
            {
                return;
            }

            var menu = e.OriginalSource as ContextMenu;
            if (menu != null)
            {
                SetOpenLocation(menu, Mouse.GetPosition(menu.PlacementTarget));
            }
        }
    }
}
