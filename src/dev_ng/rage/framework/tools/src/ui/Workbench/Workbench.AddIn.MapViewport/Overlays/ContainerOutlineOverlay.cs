﻿using System;
using System.Windows.Media;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Windows.Controls.WpfViewport2D;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using RSG.Base.Math;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Windows.Controls;
using System.Windows.Input;
using RSG.Base.Editor;
using RSG.Base.ConfigParser;
using RSG.Base.Windows.Helpers;
using RSG.Model.Common.Map;
using RSG.Model.Common.Extensions;
using RSG.Model.Common.Util;
using RSG.Base.Tasks;
using System.Threading;
using RSG.Base.Windows.Dialogs;
using MapViewport.AddIn;
using RSG.Model.Common;
using Workbench.AddIn.Services.Model;

namespace Workbench.AddIn.MapViewport.Overlays
{
    public class ContainerAttributeFilter : ViewModelBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get;
            set;
        }
        #endregion

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public ContainerAttributeFilter(string name)
        {
            this.Name = name;
        }
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Viewport.AddIn.ExtensionPoints.MiscellaneousOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class ContainerOutlineOverlay : LevelDependentViewportOverlay
    {
        #region Constants
        /// <summary>
        /// Name/Description for the overlay
        /// </summary>
        private const String c_name = "Container Outline";
        private const String c_description = "Shows the individual containers for the map, where each main first tier area is a different colour.";

        /// <summary>
        /// Colours to use for the various map areas
        /// </summary>
        private static readonly Color[] c_colours = {
                                                      Color.FromArgb(255, 128, 0, 0),
                                                      Color.FromArgb(255, 225, 198, 86),
                                                      Color.FromArgb(255, 0, 127, 0),
                                                      Colors.Purple,
                                                      Colors.PowderBlue,
                                                      Colors.SkyBlue,
                                                      Colors.Pink,
                                                      Colors.DeepSkyBlue,
                                                      Color.FromArgb(255, 128, 109, 198),
                                                      Color.FromArgb(255, 225, 128, 66),
                                                      Color.FromArgb(255, 225, 47, 66),
                                                    };

        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// A reference to view model for the map viewport
        /// </summary>
        [ImportExtension(Viewport.AddIn.CompositionPoints.MapViewport, typeof(Viewport.AddIn.IMapViewport))]
        public Viewport.AddIn.IMapViewport MapViewport { get; set; }

        /// <summary>
        /// Level Browser
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LevelBrowser, typeof(ILevelBrowser))]
        Lazy<ILevelBrowser> LevelBrowserProxy { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(CompositionPoints.ConfigurationService, typeof(Workbench.AddIn.Services.IConfigurationService))]
        Lazy<Workbench.AddIn.Services.IConfigurationService> Config { get; set; }
        #endregion // MEF Imports

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<ContainerAttributeFilter> Filters
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool ShowPropGroups
        {
            get { return m_showPropGroups; }
            set { m_showPropGroups = value; }
        }
        private bool m_showPropGroups;

        /// <summary>
        /// 
        /// </summary>
        public bool AttributeFilter
        {
            get { return m_attributeFilter; }
            set { m_attributeFilter = value; }
        }
        private bool m_attributeFilter;
                        
        /// <summary>
        /// 
        /// </summary>
        public bool FilterByDate
        {
            get { return m_filterByDate; }
            set { m_filterByDate = value; }
        }
        private bool m_filterByDate;
    
        /// <summary>
        /// 
        /// </summary>
        public bool FilterAfterDate
        {
            get { return m_filterAfterDate; }
            set { m_filterAfterDate = value; }
        }
        private bool m_filterAfterDate;
            
        /// <summary>
        /// 
        /// </summary>
        public bool FilterBeforeDate
        {
            get { return m_filterBeforeDate; }
            set { m_filterBeforeDate = value; }
        }
        private bool m_filterBeforeDate;
        
        /// <summary>
        /// 
        /// </summary>
        public DateTime? SelectedDateAfter
        {
            get { return m_selectedDateAfter; }
            set { m_selectedDateAfter = value; }
        }
        private DateTime? m_selectedDateAfter;
        
        /// <summary>
        /// 
        /// </summary>
        public DateTime? SelectedDateBefore
        {
            get { return m_selectedDateBefore; }
            set { m_selectedDateBefore = value; }
        }
        private DateTime? m_selectedDateBefore;

        /// <summary>
        /// 
        /// </summary>
        public bool FilterByUser
        {
            get { return m_filterByUser; }
            set { m_filterByUser = value; }
        }
        private bool m_filterByUser;

        /// <summary>
        /// 
        /// </summary>
        public string SelectedUser
        {
            get { return m_selectedUser; }
            set { m_selectedUser = value; }
        }
        private string m_selectedUser;

        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<string> Users
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        private Dictionary<IMapSection, Viewport2DShape> _cachedGeometry;

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand Refresh
        {
            get
            {
                lock (m_sync)
                {
                    if (this.m_refresh == null)
                    {
                        m_refresh = new RelayCommand(param => this.RefreshFiltering(param));
                    }
                }
                return m_refresh;
            }
        }
        private RelayCommand m_refresh;
        private object m_sync = new object();
        #endregion

        #region Constructors
        /// <summary>
        /// Default Constructor
        /// </summary>
        public ContainerOutlineOverlay()
            : base(c_name, c_description)
        {
            this.Filters = new ObservableCollection<ContainerAttributeFilter>();
            this.Users = new ObservableCollection<string>();

            DataSourceModes[DataSource.Database] = true;
        }
        #endregion // Constructors

        #region Overrides
        /// <summary>
        /// Gets called when the user wants to view this overlay
        /// </summary>
        public override void Activated()
        {
            this.Users.Clear();
            this.Filters.Clear();
            if (LevelBrowserProxy.Value.SelectedLevel == null)
            {
                return;
            }

            CancellationTokenSource cts = new CancellationTokenSource();
            LevelTaskContext context = new LevelTaskContext(cts.Token, LevelBrowserProxy.Value.SelectedLevel);
            ITask loadDataTask = new ActionTask("Load Data", (ctx, progress) => EnsureDataLoaded((LevelTaskContext)ctx, progress));

            TaskExecutionDialog dialog = new TaskExecutionDialog();
            TaskExecutionViewModel vm = new TaskExecutionViewModel("Generating Overlay", loadDataTask, cts, context);
            vm.AutoCloseOnCompletion = true;
            dialog.DataContext = vm;
            Nullable<bool> selectionResult = dialog.ShowDialog();
            if (false == selectionResult)
            {
                return;
            }

            ContentNodeLevel levelNode = (ContentNodeLevel)Config.Value.GameConfig.Content.Root.FindAll(LevelBrowserProxy.Value.SelectedLevel.Name, "level").FirstOrDefault();
            List<ContentNode> groups = levelNode.FindAll("Group");
            Dictionary<Color, List<String>> topLevelGroups = new Dictionary<System.Windows.Media.Color, List<String>>();
            foreach (ContentNode group in groups)
            {
                if (group.Parent == levelNode && group is ContentNodeGroup)
                {
                    if (topLevelGroups.Count() >= c_colours.Count())
                    {
                    }
                    else
                    {
                        topLevelGroups.Add(c_colours[topLevelGroups.Count], new List<String>());
                        List<ContentNode> sections = (group as ContentNodeGroup).FindAll("MapZip");
                        foreach (ContentNode section in sections)
                        {
                            topLevelGroups[c_colours[topLevelGroups.Count - 1]].Add(section.Name.ToLower());
                        }
                    }
                }
            }

            _cachedGeometry = new Dictionary<IMapSection, Viewport2DShape>();
            SortedSet<string> users = new SortedSet<string>();
            SortedDictionary<string, ContainerAttributeFilter> filters = new SortedDictionary<string, ContainerAttributeFilter>();
            IMapHierarchy hierarchy = LevelBrowserProxy.Value.SelectedLevel.GetMapHierarchy();
            foreach (IMapSection section in hierarchy.AllSections)
            {
                if (section.Type != SectionType.Container && section.Type != SectionType.ContainerLod)
                {
                    continue;
                }

                if (section.VectorMapPoints == null)
                {
                    continue;
                }

                Vector2f[] vectorMap = section.VectorMapPoints.ToArray();
                foreach (var attribute in section.ContainerAttributes)
                {
                    if (filters.ContainsKey(attribute.Key))
                    {
                        continue;
                    }

                    if (!(attribute.Value is bool))
                    {
                        continue;
                    }

                    filters.Add(attribute.Key, new ContainerAttributeFilter(attribute.Key));
                }

                Color colour = Colors.White;
                foreach (var topLevelGroup in topLevelGroups)
                {
                    if (topLevelGroup.Value.Contains(section.Name.ToLower()))
                    {
                        colour = topLevelGroup.Key;
                        break;
                    }
                }

                Viewport2DShape sectionGeometry = new Viewport2DShape(etCoordSpace.World, string.Empty, vectorMap, Colors.Black, 1, colour);
                sectionGeometry.UserText = section.Name;
                sectionGeometry.PickData = section;
                sectionGeometry.ToolTip = section.Name;
                _cachedGeometry.Add(section, sectionGeometry);
                if (section.ContainerAttributes != null)
                {
                    users.Add(section.LastExportUser);
                }

                if (section.PropGroup == null)
                {
                    continue;
                }

                Viewport2DShape propsGeometry = new Viewport2DShape(etCoordSpace.World, string.Empty, vectorMap, Colors.Black, 1, colour);
                propsGeometry.UserText = section.PropGroup.Name;
                propsGeometry.PickData = section.PropGroup;
                sectionGeometry.ToolTip = section.Name;
                _cachedGeometry.Add(section.PropGroup, propsGeometry);
                if (section.PropGroup.ContainerAttributes != null)
                {
                    users.Add(section.LastExportUser);
                }
            }

            foreach (string user in users)
            {
                this.Users.Add(user);
            }

            foreach (ContainerAttributeFilter filter in filters.Values)
            {
                this.Filters.Add(filter);
            }

            OnRenderOptionChanged();
        }

        /// <summary>
        /// Gets called when the user turns off this overlay
        /// </summary>
        public override void Deactivated()
        {
            this.Geometry = new RSG.Base.Collections.ObservableCollection<Viewport2DGeometry>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override System.Windows.Controls.Control GetOverlayControl()
        {
            return new ContainerOutlineDetailsView();
        }
        #endregion // Overrides

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="progress"></param>
        private void EnsureDataLoaded(LevelTaskContext context, IProgress<TaskProgress> progress)
        {
            IMapHierarchy hierarchy = context.MapHierarchy.AllSections.First().MapHierarchy;
            hierarchy.RequestStatisticsForSections(context.MapHierarchy.AllSections, new StreamableStat[] { StreamableMapSectionStat.ContainerAttributes }, false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        private void RefreshFiltering(object parameter)
        {
            this.OnRenderOptionChanged();
        }

        /// <summary>
        /// 
        /// </summary>
        private void OnRenderOptionChanged()
        {
            if (this.Geometry == null)
                this.Geometry = new RSG.Base.Collections.ObservableCollection<Viewport2DGeometry>();

            RSG.Base.Collections.ObservableCollection<Viewport2DGeometry> geo = new RSG.Base.Collections.ObservableCollection<Viewport2DGeometry>();
            foreach (var cachedGeometry in _cachedGeometry)
            {
                if (this.IsContainerValid(cachedGeometry.Key))
                {
                    geo.Add(cachedGeometry.Value);
                }
            }
            this.Geometry = geo;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        private bool IsContainerValid(IMapSection section)
        {
            if (section.Type != SectionType.Container && this.ShowPropGroups == false)
            {
                return false;
            }

            if (section.Type != SectionType.ContainerProp && this.ShowPropGroups == true)
            {
                return false;
            }

            if (!this.AttributeFilter && !this.FilterByDate && !this.FilterByUser)
                return true;

            if (section.ContainerAttributes == null)
                return false;

            if (this.FilterByUser)
            {
                if (!string.IsNullOrWhiteSpace(this.SelectedUser))
                {
                    if (section.LastExportUser != this.SelectedUser)
                        return false;
                }
            
            }
            if (this.AttributeFilter)
            {
                foreach (var filter in this.Filters)
                {
                    if (filter.IsSelected)
                    {
                        if (!section.ContainerAttributes.ContainsKey(filter.Name))
                            return false;
                    }
                }           
            }
            if (this.FilterByDate)
            {
                if (this.FilterBeforeDate)
                {
                    if (section.LastExportTime >= this.SelectedDateBefore)
                        return false;
                }
                if (this.FilterAfterDate)
                {
                    if (section.LastExportTime <= this.SelectedDateAfter)
                        return false;
                }
            }

            return true;
        }
        #endregion
    } // ContainerOutlineOverlay
} // Workbench.AddIn.MapViewport.Overlays
