﻿using System;
using System.Linq;
using System.Collections;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using PerforceBrowser.AddIn;
using RSG.Base.Windows.DragDrop.Utilities;
using Workbench.AddIn.Services;
using Workbench.AddIn.TXDParentiser.ViewModel;
using Workbench.AddIn.UI.Layout;
using System.Collections.Generic;
using RSG.Base.Editor;
using Workbench.AddIn.Services.File;
using WidgetEditor.AddIn;
using Workbench.AddIn.Services.Model;

namespace Workbench.AddIn.TXDParentiser
{
    /// <summary>
    /// Interaction logic for DevstarView.xaml
    /// </summary>
    public partial class TXDParentiserView : DocumentBase<TXDParentiserViewModel>
    {
        #region Constants
        public static readonly Guid GUID = new Guid("2E6DC273-62EF-4A8B-B6BE-E36072E0E3ED");
        #endregion // Constants

        #region Properties
        /// <summary>
        /// The configuration service that has in it the game view
        /// </summary>
        private IConfigurationService ConfigService
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public TXDParentiserView(String newFilename, IConfigurationService config, IPerforceService perforceService, IPerforceSyncService perforceSyncService, IProgressService progressService, ILevelBrowser levelBrowser, IWorkbenchSaveCurrentService saveService, IProxyService proxyService, Boolean link)
            : base("TXDParentiserView", new TXDParentiserViewModel(config, perforceService, perforceSyncService, progressService, levelBrowser, saveService, proxyService, link))
        {
            InitializeComponent();
            this.Title = newFilename;
            this.ConfigService = config;
            this.ID = GUID;
            this.IsModified = true;
            this.ViewModel.MultiSelectTreeView = this.TreeView;
            this.SaveModel = this.ViewModel.GlobalRoot;
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public TXDParentiserView(Uri openedFilename, IConfigurationService config, IPerforceService perforceService, IPerforceSyncService perforceSyncService, IProgressService progressService, ILevelBrowser levelBrowser, IWorkbenchSaveCurrentService saveService, Boolean link)
            : base("TXDParentiserView", new TXDParentiserViewModel(openedFilename, config, perforceService, perforceSyncService, progressService, levelBrowser, saveService, link))
        {
            InitializeComponent();
            this.Title = System.IO.Path.GetFileNameWithoutExtension(openedFilename.AbsolutePath);
            this.ConfigService = config;
            this.ID = GUID;
            this.Path = openedFilename.AbsolutePath;
            this.ViewModel.MultiSelectTreeView = this.TreeView;
            this.SaveModel = this.ViewModel.GlobalRoot;
        }
        #endregion // Constructor(s)
        
        #region Event Callbacks
        /// <summary>
        /// Gets called when the user selects a texture sort mode
        /// for the view
        /// </summary>
        private void OnTextureSortClicked(Object sender, RoutedEventArgs e)
        {
            if ((sender as MenuItem).Header as String == "Sort Textures By")
            {
                foreach (MenuItem item in (sender as MenuItem).Items)
                {
                    if (item == e.OriginalSource)
                    {
                        item.IsChecked = true;
                        if (this.ViewModel != null)
                        {
                            this.ViewModel.TextureSortPattern = item.Header as TextureSortBase;
                        }
                    }
                    else
                    {
                        item.IsChecked = false;
                    }
                }
            }
        }

        /// <summary>
        /// Need to make sure that the tree view item selected/expanded is actually in view. This is important because
        /// we can select a item in code without it being in view. 
        /// </summary>
        private void SelectedOrExpanded(Object sender, RoutedEventArgs e)
        {
            (e.OriginalSource as RSG.Base.Windows.Controls.MultiSelect.TreeViewItem).BringIntoView();
        }

        /// <summary>
        /// Gets called when any map component for the selected level gets a double click
        /// event
        /// </summary>
        void OnComponentDoubleClicked(Object sender, RoutedEventArgs e)
        {
            if (sender is RSG.Base.Windows.Controls.MultiSelect.TreeViewItem)
            {
                TextureDictionaryViewModel dictionary = (sender as RSG.Base.Windows.Controls.MultiSelect.TreeViewItem).Header as TextureDictionaryViewModel;
                if (dictionary != null)
                {
                    //this.InitialiseTextures(dictionary, null);
                    //this.ViewModel.OpenTextureDictionary(dictionary);
                    //e.Handled = true;
                }
            }
        }
        #endregion // Event Callbacks

        #region Drag & Drop
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TreeView_PreviewDragOver(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.None;
            var originalSource = (e.OriginalSource as DependencyObject).GetVisualAncestor(sender.GetType());
            if (sender == originalSource)
            {
                Scroll((DependencyObject)sender, e);
            }
        }

        private void TreeView_DragOver(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.None;
            e.Handled = true;
        }

        /// <summary>
        /// 
        /// </summary>
        private void Scroll(DependencyObject o, DragEventArgs e)
        {
            ScrollViewer scrollViewer = o.GetVisualDescendent<ScrollViewer>();

            if (scrollViewer != null)
            {
                Point position = e.GetPosition(scrollViewer);
                double scrollMargin = System.Math.Min(scrollViewer.FontSize * 2, scrollViewer.ActualHeight / 2);

                if (position.X >= scrollViewer.ActualWidth - scrollMargin &&
                    scrollViewer.HorizontalOffset < scrollViewer.ExtentWidth - scrollViewer.ViewportWidth)
                {
                    scrollViewer.LineRight();
                }
                else if (position.X < scrollMargin && scrollViewer.HorizontalOffset > 0)
                {
                    scrollViewer.LineLeft();
                }
                else if (position.Y >= scrollViewer.ActualHeight - scrollMargin &&
                    scrollViewer.VerticalOffset < scrollViewer.ExtentHeight - scrollViewer.ViewportHeight)
                {
                    scrollViewer.LineDown();
                }
                else if (position.Y < scrollMargin && scrollViewer.VerticalOffset > 0)
                {
                    scrollViewer.LineUp();
                }
            }
        }
        #endregion // Drag & Drop

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="previousResult"></param>
        /// <returns></returns>
        public override object FindNext(ISearchQuery query, object previousResult)
        {
            IViewModel start = previousResult as IViewModel;
            IViewModel found = null;

            bool canFind = start == null ? true : false;
            foreach (IViewModel result in this.ViewModel.GlobalRoot.FindAll(query.Expression))
            {
                if (canFind == true)
                {
                    if (query.UseTag == false)
                    {
                        found = result;
                        break;
                    }
                    else
                    {
                        if (result is GlobalTextureDictionaryViewModel && query.Tag == "Global Texture Dictionary" || query.Tag == "Texture Dictionaries")
                        {
                            found = result;
                            break;
                        }
                        else if (result is SourceTextureDictionaryViewModel && query.Tag == "Source Texture Dictionary" || query.Tag == "Texture Dictionaries")
                        {
                            found = result;
                            break;
                        }
                        else if (result is SourceTextureViewModel && query.Tag == "Source Texture" || query.Tag == "Textures")
                        {
                            found = result;
                            break;
                        }
                        else if (result is GlobalTextureViewModel && query.Tag == "Global Texture" || query.Tag == "Textures")
                        {
                            found = result;
                            break;
                        }
                    }
                }
                else if (result == start)
                {
                    canFind = true;
                }
            }

            if (found != null)
                SelectViewModel(found);

            return found;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="previousResult"></param>
        /// <returns></returns>
        public override object FindPrevious(ISearchQuery query, object previousResult)
        {
            IViewModel start = previousResult as IViewModel;
            IViewModel found = null;

            foreach (IViewModel result in this.ViewModel.GlobalRoot.FindAll(query.Expression))
            {
                if (result == start)
                {
                    break;
                }
                else
                {
                    if (query.UseTag == false)
                    {
                        found = result;
                    }
                    else
                    {
                        if (result is GlobalTextureDictionaryViewModel && query.Tag == "Global Texture Dictionary" || query.Tag == "Texture Dictionaries")
                            found = result;
                        else if (result is SourceTextureDictionaryViewModel && query.Tag == "Source Texture Dictionary" || query.Tag == "Texture Dictionaries")
                            found = result;
                        else if (result is SourceTextureViewModel && query.Tag == "Source Texture" || query.Tag == "Textures")
                            found = result;
                        else if (result is GlobalTextureViewModel && query.Tag == "Global Texture" || query.Tag == "Textures")
                            found = result;
                    }
                }
                
            }

            if (found != null)
                SelectViewModel(found);

            return found;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public override List<ISearchResult> FindAll(ISearchQuery query)
        {
            GlobalRootViewModel start = this.ViewModel.GlobalRoot;

            List<ISearchResult> results = new List<ISearchResult>();
            List<IViewModel> found = new List<IViewModel>();
            if (start != null)
                found = start.FindAll(query.Expression).ToList();

            foreach (var find in found)
            {
                if (query.UseTag == false)
                {
                    if (find is GlobalTextureDictionaryViewModel)
                        results.Add(new SearchResult((find as GlobalTextureDictionaryViewModel).Model, string.Format("Global Texture Dictionary - {0}", (find as GlobalTextureDictionaryViewModel).Name), this));
                    else if (find is SourceTextureDictionaryViewModel)
                        results.Add(new SearchResult((find as SourceTextureDictionaryViewModel).Model, string.Format("Source Texture Dictionary - {0}", (find as SourceTextureDictionaryViewModel).Name), this));
                    else if (find is SourceTextureViewModel)
                    {
                        if ((find as SourceTextureViewModel).Model.Promoted)
                            results.Add(new SearchResult((find as SourceTextureViewModel).Model, string.Format("Promoted Source Texture - {0}", (find as SourceTextureViewModel).Model.StreamName), this));
                        else
                            results.Add(new SearchResult((find as SourceTextureViewModel).Model, string.Format("Unpromoted Source Texture - {0}", (find as SourceTextureViewModel).Model.StreamName), this));
                    }
                    else if (find is GlobalTextureViewModel)
                        results.Add(new SearchResult((find as GlobalTextureViewModel).Model, string.Format("Global Texture - {0}", (find as GlobalTextureViewModel).Model.StreamName), this));
                }
                else
                {
                    if (find is GlobalTextureDictionaryViewModel && query.Tag == "Global Texture Dictionary" || query.Tag == "Texture Dictionaries")
                        results.Add(new SearchResult((find as GlobalTextureDictionaryViewModel).Model, string.Format("Global Texture Dictionary - {0}", (find as GlobalTextureDictionaryViewModel).Name), this));
                    else if (find is SourceTextureDictionaryViewModel && query.Tag == "Source Texture Dictionary" || query.Tag == "Texture Dictionaries")
                        results.Add(new SearchResult((find as SourceTextureDictionaryViewModel).Model, string.Format("Source Texture Dictionary - {0}", (find as SourceTextureDictionaryViewModel).Name), this));
                    else if (find is SourceTextureViewModel && query.Tag == "Source Texture" || query.Tag == "Textures")
                    {
                        if ((find as SourceTextureViewModel).Model.Promoted)
                            results.Add(new SearchResult((find as SourceTextureViewModel).Model, string.Format("Promoted Source Texture - {0}", (find as SourceTextureViewModel).Model.StreamName), this));
                        else
                            results.Add(new SearchResult((find as SourceTextureViewModel).Model, string.Format("Unpromoted Source Texture - {0}", (find as SourceTextureViewModel).Model.StreamName), this));
                    }
                    else if (find is GlobalTextureViewModel && query.Tag == "Global Texture" || query.Tag == "Textures")
                        results.Add(new SearchResult((find as GlobalTextureViewModel).Model, string.Format("Global Texture - {0}", (find as GlobalTextureViewModel).Model.StreamName), this));
                }
            }

            return results;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="result"></param>
        public override void SelectSearchResult(ISearchResult result)
        {
            foreach (IViewModel vm in this.ViewModel.GlobalRoot.GetAll())
            {
                if (vm is GlobalTextureDictionaryViewModel)
                {
                    if ((vm as GlobalTextureDictionaryViewModel).Model == result.Data)
                    {
                        SelectViewModel(vm as GlobalTextureDictionaryViewModel);
                    }
                }
                else if (vm is SourceTextureDictionaryViewModel)
                {
                    if ((vm as SourceTextureDictionaryViewModel).Model == result.Data)
                    {
                        SelectViewModel(vm as SourceTextureDictionaryViewModel);
                    }
                }
                else if (vm is SourceTextureViewModel)
                {
                    if ((vm as SourceTextureViewModel).Model == result.Data)
                    {
                        SelectViewModel(vm as SourceTextureViewModel);
                    }
                }
                else if (vm is GlobalTextureViewModel)
                {
                    if ((vm as GlobalTextureViewModel).Model == result.Data)
                    {
                        SelectViewModel(vm as GlobalTextureViewModel);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vm"></param>
        private void SelectViewModel(IViewModel vm)
        {
            while (this.ViewModel.SelectedItems.Count > 0)
            {
                if (this.ViewModel.SelectedItems[0] is IViewModel)
                    (this.ViewModel.SelectedItems[0] as IViewModel).IsSelected = false;
            }

            vm.IsSelected = true;
            if (vm is IDictionaryChildViewModel)
            {
                (vm as IDictionaryChildViewModel).ExpandedTo();
            }
            else if (vm is ITextureChildViewModel)
            {
                (vm as ITextureChildViewModel).ExpandedTo();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override List<object> GetCurrentSelection()
        {
            return new List<object>(this.ViewModel.SelectedItems);
        }
        #endregion
    } // TXDParentiserView

    #region Sorting

    /// <summary>
    /// The different sorting patterns that can be put on the textures themselves
    /// </summary>
    public class TextureSortBase
    {
        public String DisplayName
        {
            get;
            set;
        }
    };

    public class TextureNameSort : TextureSortBase
    {
        public TextureNameSort()
        {
            DisplayName = "Name";
        }
    }

    public class TextureSizeSort : TextureSortBase
    {
        public TextureSizeSort()
        {
            DisplayName = "Size";
        }
    }

    public class TextureCountSort : TextureSortBase
    {
        public TextureCountSort()
        {
            DisplayName = "Instance Count";
        }
    }

    #endregion // Sorting

    #region Converters
    /// <summary>
    /// Takes a number of collections and determines the final collection to return as children for a dictionary
    /// </summary>
    /// <values>
    /// [0] - Observable Collection - Child Global Texture Dictionaries
    /// [1] - Observable Collection - Child Source Texture Dictionaries
    /// [2] - Observable Collection - Child Textures
    /// </values>
    public class ChildrenConverter : IMultiValueConverter
    {
        public Object Convert(Object[] values, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            RSG.Base.Collections.ObservableCollection<Object> finalCollection = new RSG.Base.Collections.ObservableCollection<Object>();

            ListCollectionView globalView = null;
            if (values[0] != null)
            {
                globalView = new ListCollectionView(values[0] as IList);
                SortDescription sort = new SortDescription("Model.Name", ListSortDirection.Ascending);
                globalView.SortDescriptions.Add(sort);
            }

            ListCollectionView sourceView = null;
            if (values[1] != null)
            {
                sourceView = new ListCollectionView(values[1] as IList);
                SortDescription sort = new SortDescription("Model.Name", ListSortDirection.Ascending);
                sourceView.SortDescriptions.Add(sort);
            }

            ListCollectionView textureView = null;
            if (values[2] != null)
            {
                textureView = new ListCollectionView(values[2] as IList);
                TextureSortBase sortPattern = values[3] as TextureSortBase;
                if (sortPattern == null)
                {
                    sortPattern = new TextureNameSort();
                }
                if (sortPattern is TextureNameSort)
                {
                    SortDescription sort = new SortDescription("Model.StreamName", ListSortDirection.Ascending);
                    textureView.SortDescriptions.Add(sort);
                }
                else if (sortPattern is TextureSizeSort)
                {
                    SortDescription sort = new SortDescription("Model.StreamSize", ListSortDirection.Descending);
                    textureView.SortDescriptions.Add(sort);
                    sort = new SortDescription("Model.StreamName", ListSortDirection.Ascending);
                    textureView.SortDescriptions.Add(sort);
                }
                else if (sortPattern is TextureCountSort)
                {
                    SortDescription sort = new SortDescription("Model.InstanceCount", ListSortDirection.Descending);
                    textureView.SortDescriptions.Add(sort);
                    sort = new SortDescription("Model.StreamName", ListSortDirection.Ascending);
                    textureView.SortDescriptions.Add(sort);
                }
            }

            if (globalView != null)
            {
                foreach (var item in globalView)
                {
                    finalCollection.Add(item);
                }
            }
            if (sourceView != null)
            {
                foreach (var item in sourceView)
                {
                    finalCollection.Add(item);
                }
            }
            if (textureView != null)
            {
                foreach (var item in textureView)
                {
                    finalCollection.Add(item);
                }
            }

            return finalCollection;
        }

        public Object[] ConvertBack(Object value, Type[] targetTypes, Object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

    } // ChildrenConverter

    /// <summary>
    /// Takes a number of collections and determines the final collection to return as children for a dictionary
    /// </summary>
    /// <values>
    /// [0] - Observable Collection - Child Textures
    /// [1] - TextureSortBase - Texture sorting pattern
    /// </values>
    public class TextureChildrenConverter : IMultiValueConverter
    {
        public Object Convert(Object[] values, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            if (values[0] != null)
            {
                ListCollectionView view = new ListCollectionView(values[0] as IList);
                TextureSortBase sortPattern = values[1] as TextureSortBase;
                if (sortPattern == null)
                {
                    sortPattern = new TextureNameSort();
                }

                if (sortPattern is TextureNameSort)
                {
                    SortDescription sort = new SortDescription("Model.Promoted", ListSortDirection.Ascending);
                    view.SortDescriptions.Add(sort);
                    sort = new SortDescription("Model.StreamName", ListSortDirection.Ascending);
                    view.SortDescriptions.Add(sort);
                }
                else if (sortPattern is TextureSizeSort)
                {
                    SortDescription sort = new SortDescription("Model.Promoted", ListSortDirection.Ascending);
                    view.SortDescriptions.Add(sort);
                    sort = new SortDescription("Model.StreamSize", ListSortDirection.Descending);
                    view.SortDescriptions.Add(sort);
                    sort = new SortDescription("Model.StreamName", ListSortDirection.Ascending);
                    view.SortDescriptions.Add(sort);
                }
                else if (sortPattern is TextureCountSort)
                {
                    SortDescription sort = new SortDescription("Model.Promoted", ListSortDirection.Ascending);
                    view.SortDescriptions.Add(sort);
                    sort = new SortDescription("Model.InstanceCount", ListSortDirection.Descending);
                    view.SortDescriptions.Add(sort);
                    sort = new SortDescription("Model.StreamName", ListSortDirection.Ascending);
                    view.SortDescriptions.Add(sort);
                }

                return view;
            }

            return null;
        }

        public Object[] ConvertBack(Object value, Type[] targetTypes, Object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

    } // OptionsConverter

    public class SizeConverter : IValueConverter
    {
        #region Convert Functions

        /// <summary>
        /// 
        /// </summary>
        public Object Convert(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is int)
            {
                if ((int)value < 0)
                    return "n/a";

                String size = String.Empty;
                int bytes = (int)value;
                if (bytes == 0)
                {
                    size = String.Format("0 KB");
                }
                else if (bytes < 1024 * 1024)
                {
                    int kbytes = bytes >> 10;
                    int kbyte = 1024;
                    int leftover = bytes - (kbytes * kbyte);

                    if (leftover > 0)
                    {
                        size = String.Format("{0} KB", ((double)bytes / (double)kbyte).ToString("F1"));
                    }
                    else
                    {
                        size = String.Format("{0} KB", bytes >> 10);
                    }
                }
                else
                {
                    int mbytes = bytes >> 20;
                    int mbyte = 1024 * 1024;
                    int leftover = bytes - (mbytes * mbyte);

                    if (leftover > 0)
                    {
                        size = String.Format("{0} MB", ((double)bytes / (double)mbyte).ToString("F1"));
                    }
                    else
                    {
                        size = String.Format("{0} MB", bytes >> 20);
                    }
                }
                return size;
            }
            else
            {
                return String.Empty;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Object ConvertBack(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion // Convert Functions

    } // SizeConverter

    public class InstanceCountPromotionConverter : IMultiValueConverter
    {
        #region Convert Functions

        /// <summary>
        /// 
        /// </summary>
        public Object Convert(Object[] values, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            if (values.Length == 2)
            {
                if (values[0] is Boolean)
                {
                    if ((values[0] as bool?) == true)
                    {
                        return Brushes.Gray;
                    }
                }
                if (values[1] is int)
                {
                    Brush foreground = Brushes.Black;
                    if ((int)values[1] >= 10)
                    {
                        foreground = Brushes.Red;
                    }
                    else if ((int)values[1] >= 5)
                    {
                        foreground = Brushes.Orange;
                    }
                    else if ((int)values[1] > 1)
                    {
                        foreground = Brushes.Green;
                    }

                    return foreground;
                }
                else
                {
                    return Brushes.Black;
                }
            }

            return Brushes.Black;
        }

        /// <summary>
        /// 
        /// </summary>
        public Object[] ConvertBack(Object value, Type[] targetTypes, Object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion // Convert Functions

    } // InstanceCountConverter

    public class InstanceCountConverter : IValueConverter
    {
        #region Convert Functions

        /// <summary>
        /// 
        /// </summary>
        public Object Convert(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is int)
            {
                Brush foreground = Brushes.Black;
                if ((int)value >= 10)
                {
                    foreground = Brushes.Red;
                }
                else if ((int)value >= 5)
                {
                    foreground = Brushes.Orange;
                }
                else if ((int)value > 1)
                {
                    foreground = Brushes.Green;
                }

                return foreground;
            }
            else
            {
                return Brushes.Black;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Object ConvertBack(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion // Convert Functions

    } // InstanceCountConverter

    public class ColourConverter : IValueConverter
    {
        #region Convert Functions

        /// <summary>
        /// 
        /// </summary>
        public Object Convert(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            Brush foreground = Brushes.Black;
            if (value is bool)
            {
                if ((bool)value == true)
                {
                    foreground = Brushes.Red;
                }
                else
                {
                    foreground = Brushes.Black;
                }
                foreground.Freeze();
                return foreground;
            }
            else
            {
                foreground.Freeze();
                return foreground;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Object ConvertBack(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion // Convert Functions

    } // InstanceCountConverter

    [ValueConversion(typeof(bool), typeof(bool))]
    public class InverseBooleanConverter : IValueConverter
    {
        #region Convert Functions

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (targetType != typeof(bool))
                throw new InvalidOperationException("The target must be a boolean");

            return !(bool)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    } // InverseBooleanConverter
    #endregion // Converters

    #region Search
    /// <summary>
    /// 
    /// </summary>
    public class SearchResult : ISearchResult
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public object Data
        {
            get { return m_data; }
        }
        private object m_data = null;

        /// <summary>
        /// 
        /// </summary>
        public bool CanJumpTo
        {
            get { return true; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string DisplayText
        {
            get { return m_displayText; }
        }
        private string m_displayText;

        /// <summary>
        /// 
        /// </summary>
        public IContentBase Content
        {
            get { return m_content; }
            set { m_content = value; }
        }
        private IContentBase m_content;
        #endregion // Properties

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="text"></param>
        /// <param name="content"></param>
        public SearchResult(object data, string text, IContentBase content)
        {
            this.m_data = data;
            this.m_displayText = text;
            this.m_content = content;
        }
        #endregion
    } // SearchResult

    [ExportExtension(Workbench.AddIn.ExtensionPoints.SearchableTags, typeof(Workbench.AddIn.UI.Layout.ISearchableTags))]
    public class SearchableTags : Workbench.AddIn.UI.Layout.ISearchableTags
    {
        #region ISearchableTags Members

        public List<string> Tags
        {
            get
            {
                return new List<string>()
                {
                    "Global Texture",
                    "Source Texture",
                    "Global Texture Dictionary",
                    "Source Texture Dictionary",
                    "Texture Dicitonaries",
                    "Textures"
                };
            }
        }

        #endregion
    }
    #endregion
} // Workbench.AddIn.TXDParentiser
