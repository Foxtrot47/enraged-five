﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using RSG.Metadata.Characters;
using System.Collections.ObjectModel;

namespace MetadataEditor.AddIn.PedVariation.ViewModel
{
    /// <summary>
    /// 
    /// </summary>
    public class PropVariationViewModel : ViewModelBase
    {
        #region Classes
        /// <summary>
        /// 
        /// </summary>
        public class FloatExpression : ViewModelBase
        {
            #region Properties
            /// <summary>
            /// The variation model that this expression viewmodel
            /// is attached to.
            /// </summary>
            private PedPropVariation Model
            {
                get;
                set;
            }

            /// <summary>
            /// The value of this user expression
            /// </summary>
            public float Value
            {
                get { return Model.Expressions[ExpressionIndex]; }
                set
                {
                    Model.SetUserExpressionIdentifier(value, ExpressionIndex);
                    OnPropertyChanged("Value");
                }
            }

            /// <summary>
            /// The index of this expression in the expression
            /// array.
            /// </summary>
            private int ExpressionIndex
            {
                get;
                set;
            }
            #endregion

            #region Constructor
            /// <summary>
            /// Initialises a new instance of the 
            /// <see cref="FloatExpression"/> class.
            /// </summary>
            /// <param name="value"></param>
            /// <param name="index"></param>
            /// <param name="model"></param>
            public FloatExpression(float value, int index, PedPropVariation model)
            {
                this.ExpressionIndex = index;
                this.Model = model;
            }
            #endregion
        }

        /// <summary>
        /// 
        /// </summary>
        public class TextureVariationViewModel : ViewModelBase
        {
            #region Fields
            /// <summary>
            /// Private member for the distribution value to support
            /// conversion between float and integer.
            /// </summary>
            private float m_distribution;
            /// <summary>
            /// Flag to tell the view model not to update the internal
            /// distribution value when the model changes.
            /// </summary>
            private bool m_syncingDistribution;
            #endregion

            #region Properties
            /// <summary>
            /// The variation model that this expression viewmodel
            /// is attached to.
            /// </summary>
            private PropTextureVariation Model
            {
                get;
                set;
            }

            /// <summary>
            /// The identifiers for all of the texture variations.
            /// </summary>
            public int TextureIdentifier
            {
                get { return Model.TextureIdentifier; }
                set 
                { 
                    Model.SetTextureIdentifier(value);
                    OnPropertyChanged("TextureIdentifier");
                }
            }

            /// <summary>
            /// The distribution rate for this variation (spawn probability).
            /// This is represented with a float value where as in the model
            /// it is a integer so a conversion is needed.
            /// </summary>
            public float Distribution
            {
                get { return m_distribution; }
                set
                {
                    m_distribution = value;
                    m_syncingDistribution = true;
                    Model.SetDistribution((int)System.Math.Round(value * 2.55f));
                    m_syncingDistribution = false;
                    OnPropertyChanged("Distribution");
                }
            }

            /// <summary>
            /// The id to specify the inclusion set for this variation.
            /// </summary>
            public int InclusionIdentifier
            {
                get { return Model.InclusionIdentifier; }
                set
                {
                    Model.SetInclusionIdentifier(value);
                    OnPropertyChanged("InclusionIdentifier");
                }
            }

            /// <summary>
            /// The id to specify the exclusion set for this variation.
            /// </summary>
            public int ExclusionIdentifier
            {
                get { return Model.ExclusionIdentifier; }
                set
                {
                    Model.SetExclusionIdentifier(value);
                    OnPropertyChanged("ExclusionIdentifier");
                }
            }

            public BitsetTunableViewModel InclusionSet
            {
                get { return this.inclusionSet; }
                set
                {
                    this.inclusionSet = value;
                    OnPropertyChanged("InclusionSet");
                }
            }
            private BitsetTunableViewModel inclusionSet;


            public BitsetTunableViewModel ExclusionSet
            {
                get { return this.exclusionSet; }
                set
                {
                    this.exclusionSet = value;
                    OnPropertyChanged("ExclusionSet");
                }
            }
            private BitsetTunableViewModel exclusionSet;

            /// <summary>
            /// The index of this expression in the expression
            /// array.
            /// </summary>
            public int TextureIndex
            {
                get;
                set;
            }
            #endregion

            #region Constructor
            /// <summary>
            /// Initialises a new instance of the 
            /// <see cref="TextureVariationViewModel"/> class.
            /// </summary>
            /// <param name="value"></param>
            /// <param name="index"></param>
            /// <param name="model"></param>
            public TextureVariationViewModel(int index, PropTextureVariation model)
            {
                this.TextureIndex = index;
                this.Model = model;
                m_distribution = Model.Distribution / 2.55f;

                this.InclusionSet = new BitsetTunableViewModel(model.InclusionSetTunable, true);
                this.ExclusionSet = new BitsetTunableViewModel(model.ExclusionSetTunable, true);
                model.TextureIdentifierChanged += (s, e) => { OnPropertyChanged("TextureIdentifier"); };

                
                model.DistributionValueChanged += (s, e) => 
                {
                    if (!m_syncingDistribution)
                        m_distribution = Model.Distribution / 2.55f;
                    OnPropertyChanged("Distribution");
                };
            }
            #endregion
        }
        #endregion

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public PedPropVariation Model
        {
            get;
            set;
        }

        /// <summary>
        /// The name of this variation
        /// </summary>
        public string Name
        {
            get { return Model.Name; }
        }

        /// <summary>
        /// The id to specify the inclusion set for this variation.
        /// </summary>
        public int Stickyness
        {
            get { return Model.Stickyness; }
            set
            {
                Model.SetStickyness(value);
                OnPropertyChanged("Stickyness");
            }
        }

        /// <summary>
        /// An array of a specific type that represents the bit
        /// fields that can be set.
        /// </summary>
        public ObservableCollection<ComponentVariationViewModel.BitFlag> Flags
        {
            get;
            set;
        }

        /// <summary>
        /// An array of a specific type that represents the bit
        /// fields that can be set.
        /// </summary>
        public ObservableCollection<ComponentVariationViewModel.NewBitFlag> NewFlags
        {
            get;
            set;
        }

        /// <summary>
        /// An array of a specific type that represents the user
        /// expressions.
        /// </summary>
        public ObservableCollection<FloatExpression> Expressions
        {
            get;
            set;
        }

        /// <summary>
        /// An array of a specific type that represents the bit
        /// fields that can be set.
        /// </summary>
        public List<TextureVariationViewModel> Textures
        {
            get;
            set;
        }

        /// <summary>
        /// Gets a value that indicates whether this view model is in a odd position in
        /// its list.
        /// </summary>
        public bool IsOdd
        {
            get;
            private set;
        }
        #endregion

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        public PropVariationViewModel(PedPropVariation model, bool isOdd)
        {
            this.IsOdd = isOdd;
            this.Model = model;
            this.Flags = new ObservableCollection<ComponentVariationViewModel.BitFlag>();
            this.NewFlags = new ObservableCollection<ComponentVariationViewModel.NewBitFlag>();
            this.Expressions = new ObservableCollection<FloatExpression>();
            this.Textures = new List<TextureVariationViewModel>();
            CreateExpressions();
            CreateFlags();
            CreateNewFlags();

            int index = 0;
            foreach (ITextureVariation texture in model.TextureVariations)
            {
                if (texture is PropTextureVariation)
                {
                    Textures.Add(new TextureVariationViewModel(index++, texture as PropTextureVariation));
                }
            }

            model.FlagValueChanged += (s, e) => { CreateFlags(); OnPropertyChanged("Flags"); };
            model.NewFlagValueChanged += (s, e) => { CreateNewFlags(); OnPropertyChanged("NewFlags"); };
            model.StickynessValueChanged += (s, e) => { OnPropertyChanged("Stickyness"); };
            model.ExpressionValueChanged += (s, e) => { CreateExpressions(); OnPropertyChanged("Expressions"); };
        }
        #endregion

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        private void CreateFlags()
        {
            this.Flags.Clear();
            for (int i = 0; i < this.Model.FlagCount; i++)
            {
                this.Flags.Add(new ComponentVariationViewModel.BitFlag(Model.GetFlag(i), i, this.Model.ComponentFlagName(i), Model));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void CreateNewFlags()
        {
            this.NewFlags.Clear();
            for (int i = 0; i < this.Model.NewFlagCount; i++)
            {
                this.NewFlags.Add(new ComponentVariationViewModel.NewBitFlag(i, this.Model.ComponentFlagName(i), Model));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void CreateExpressions()
        {
            this.Expressions.Clear();
            for (int i = 0; i < Model.ExpressionCount; i++)
            {
                this.Expressions.Add(new FloatExpression(Model.Expressions[i], i, Model));
            }
        }
        #endregion
    } // ComponentVariationViewModel
} // MetadataEditor.AddIn.PedVariation.ViewModel
