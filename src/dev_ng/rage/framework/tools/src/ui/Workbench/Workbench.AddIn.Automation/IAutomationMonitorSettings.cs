﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn.Services;

namespace Workbench.AddIn.Automation
{
    /// <summary>
    /// Automation monitor settings interface.
    /// </summary>
    public interface IAutomationMonitorSettings : ISettings
    {
        /// <summary>
        /// The last used service. Includes the friendly name and the Uri of the resource.
        /// </summary>
        string LastUsedService { get; set; }
    }
}
