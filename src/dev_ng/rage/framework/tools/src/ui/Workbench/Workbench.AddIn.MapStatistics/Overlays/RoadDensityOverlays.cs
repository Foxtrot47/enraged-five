﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common.Map;
using RSG.SceneXml;
using RSG.Model.Map3;
using RSG.Base.Math;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using System.Windows.Media;
using Workbench.AddIn.MapStatistics.Overlays.RoadDensity;
using RSG.Base.Tasks;
using RSG.Base.Windows.Dialogs;
using RSG.Model.Common.Util;
using System.Threading;
using System.Windows.Forms;
using RSG.Base.Editor;
using RSG.Model.Common;

namespace Workbench.AddIn.MapStatistics.Overlays
{
    [ExportExtension(ExtensionPoints.DensityStatisticsOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class RoadDensityOverlay : StatisticOverlayBase
    {
        #region Constants
        private const string c_name = "Road Density";
        private const string c_description = "Shows the traffic density of each of the roads in the game. Uncheck the densities you don't want to see then click 'Update Map' to remove them.";
        private const float c_renderToTextureImageRatio = 0.3333f;
        #endregion // Constants

        #region Private member fields

        private RoadDensityModel m_model;
        private Dictionary<int, List<Vector2f[]>> m_chains;

        #endregion

        #region Public properties

        /// <summary>
        /// View model.
        /// </summary>
        public ViewModelBase ViewModel
        {
            get;
            private set;
        }

        #endregion

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public RoadDensityOverlay()
            : base(c_name, c_description)
        {
            m_includeNonDynamic = true;
            m_includeDynamic = true;
            m_includeHighDetailed = true;
            m_includeLod = true;
            m_includeSuperLod = true;
            m_includeInterior = false;
            m_dontShowInteriorOptions = true;
            m_dontShowUniqueOptions = true;
            m_dontShowEntityTypeOption = true;

            m_chains = new Dictionary<int, List<Vector2f[]>>();
        }
        #endregion // Constructor
        
        #region Overrides

        /// <summary>
        /// Get the overlay control.
        /// </summary>
        /// <returns>The overlay control.</returns>
        public override System.Windows.Controls.Control GetOverlayControl()
        {
            RoadDensityControl ctrl = new RoadDensityControl();
            return ctrl;
        }

        /// <summary>
        /// Activate the overlay.
        /// </summary>
        public override void Activated()
        {
            if (m_model == null)
            {
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.Filter = "Scene Xml Files (*.xml)|*.xml";
                if (ofd.ShowDialog() != DialogResult.OK)
                {
                    return;
                }

                string fileName = ofd.FileName;

                CancellationTokenSource cts = new CancellationTokenSource();
                TaskContext context = new TaskContext();
                context.Argument = fileName;

                CompositeTask compositeTask = new CompositeTask("Updating Overlay");
                compositeTask.AddSubTask(new ActionTask("Load Data", (ctx, progress) => LoadSceneXml((TaskContext)ctx, progress)));

                TaskExecutionDialog dialog = new TaskExecutionDialog();
                TaskExecutionViewModel vm = new TaskExecutionViewModel("Generating Overlay", compositeTask, cts, context);
                vm.AutoCloseOnCompletion = true;
                dialog.DataContext = vm;
                bool? selectionResult = dialog.ShowDialog();

                if (selectionResult.HasValue && !selectionResult.Value)
                {
                    return;
                }

            }

            var rdvm = new RoadDensityViewModel(m_model);
            rdvm.ShowPedestrianCrossings = false;
            ViewModel = rdvm;

            BucketizeChains(m_model.GetChains());
            DrawChains();
        }

        #endregion 

        #region Internal methods

        /// <summary>
        /// Refresh the overlay.
        /// </summary>
        internal void Refresh()
        {
            CancellationTokenSource cts = new CancellationTokenSource();
            TaskContext context = new TaskContext();

            CompositeTask compositeTask = new CompositeTask("Refreshing Overlay");
            compositeTask.AddSubTask(new ActionTask("Re-parsing the Data", (ctx, progress) => DoRefresh((TaskContext)ctx, progress)));

            TaskExecutionDialog dialog = new TaskExecutionDialog();
            TaskExecutionViewModel vm = new TaskExecutionViewModel("Refreshing Overlay", compositeTask, cts, context);
            vm.AutoCloseOnCompletion = true;
            dialog.DataContext = vm;
            bool? selectionResult = dialog.ShowDialog();

            if (selectionResult.HasValue && !selectionResult.Value)
            {
                return;
            }

            DrawChains();
        }

        /// <summary>
        /// Perform the refresh.
        /// </summary>
        /// <param name="taskContext">Context.</param>
        /// <param name="progress">Progress.</param>
        private void DoRefresh(TaskContext taskContext, IProgress<TaskProgress> progress)
        {
            progress.Report(new TaskProgress(0, "Parsing the model"));
            m_model.Parse();
            progress.Report(new TaskProgress(0.5, "Sorting the road densities"));
            BucketizeChains(m_model.GetChains());
            progress.Report(new TaskProgress(0.5, "Drawing the roads"));
        }

        /// <summary>
        /// Draw the selected paths.
        /// </summary>
        internal void DrawChains()
        {
            Geometry.BeginUpdate();
            Geometry.Clear();


            RSG.Base.Math.Vector2i size = new Vector2i(0, 0);
            RSG.Base.Math.Vector2f pos = new Vector2f(0, 0);
            ILevel level = LevelBrowserProxy.Value.SelectedLevel;
            if (level.ImageBounds != null)
            {
                size = new RSG.Base.Math.Vector2i((int)(level.ImageBounds.Max.X - level.ImageBounds.Min.X), (int)(level.ImageBounds.Max.Y - level.ImageBounds.Min.Y));
                pos = new RSG.Base.Math.Vector2f(level.ImageBounds.Min.X, level.ImageBounds.Max.Y);
            }
            Viewport2DImageOverlay image = new Viewport2DImageOverlay(etCoordSpace.World, "", new RSG.Base.Math.Vector2i((int)(size.X * c_renderToTextureImageRatio), (int)(size.Y * c_renderToTextureImageRatio)), pos, new RSG.Base.Math.Vector2f(size.X, size.Y));
            image.BeginUpdate();

            foreach (int key in m_chains.Keys)
            {
                if (IsLevelVisible(key))
                {
                    foreach (var pointArray in m_chains[key])
                    {
                        DrawChain(image, key, pointArray);
                    }
                }
            }

            image.EndUpdate();

            Geometry.Add(image);

            Geometry.EndUpdate();
        }

        #endregion

        #region Private helper methods

        /// <summary>
        /// Load the scene file.
        /// </summary>
        /// <param name="taskContext">Context.</param>
        /// <param name="progress">Progress.</param>
        /// <returns>The created object.</returns>
        private object LoadSceneXml(TaskContext taskContext, IProgress<TaskProgress> progress)
        {
            m_model = new RoadDensityModel(taskContext.Argument.ToString(), new Vector2f(0, 0), true);
            m_model.Parse();
            progress.Report(new TaskProgress(1, "Loaded"));
            return m_model;
        }

        /// <summary>
        /// Place all the node chains in buckets according to their density.
        /// </summary>
        /// <param name="chains">Chain collection.</param>
        private void BucketizeChains(IEnumerable<Chain> chains)
        {
            m_chains.Clear();

            foreach (Chain c in chains)
            {
                if (c.Points.Count == 0)
                {
                    continue;
                }

                DensityPointCollection pointArrays = c.ToPointArrays();
                foreach (var pointArray in pointArrays)
                {
                    if (!m_chains.ContainsKey(pointArray.Density))
                    {
                        m_chains[pointArray.Density] = new List<Vector2f[]>();
                    }

                    m_chains[pointArray.Density].Add(pointArray.Points);
                }
            }
        }

        /// <summary>
        /// Returns true if the level can be rendered.
        /// </summary>
        /// <param name="level">Level.</param>
        /// <returns>True if the level can be rendered.</returns>
        private bool IsLevelVisible(int level)
        {
            RoadDensityViewModel vm = ViewModel as RoadDensityViewModel;

            foreach (DensityViewModel dvm in vm.Items)
            {
                if (dvm.Value == level && dvm.IsChecked)
                {
                    return true;
                }
            }

            if (level == Chain.c_pedCrossingDensity && vm.ShowPedestrianCrossings)
            {
                return true;
            }
            else if (level == Chain.c_pedAssistedMovementDensity && vm.ShowAssistedMovementRoutes)
            {
                return true;
            }

            return false;
        }

        private bool IgnorePedestrianCrossings()
        {
            RoadDensityViewModel vm = ViewModel as RoadDensityViewModel;
            return !vm.ShowPedestrianCrossings;
        }

        /// <summary>
        /// Get the color from the level.
        /// </summary>
        /// <param name="level">Level.</param>
        /// <returns>Colour.</returns>
        private Color GetColor(int level)
        {
            Color color = Colors.Gray;
            RoadDensityViewModel vm = ViewModel as RoadDensityViewModel;
            DensityViewModel viewModel = vm.Items.FirstOrDefault(item => ((DensityViewModel)item).Value == level && ((DensityViewModel)item).IsChecked) as DensityViewModel;

            if (viewModel != null)
            {
                color = viewModel.Colour.Color;
            }

            return color;
        }
        
        /// <summary>
        /// Draw the chain.
        /// </summary>
        /// <param name="chain">Chain.</param>
        private void DrawChain(Viewport2DImageOverlay image, int density, Vector2f[] points)
        {
            Color colour;

            if (density == Chain.c_pedCrossingDensity)
            {
                colour = Color.FromRgb(255, 51, 204);
            }
            else if (density == Chain.c_pedAssistedMovementDensity)
            {
                colour = Color.FromRgb(255, 51, 102);
            }
            else
            {
                colour = GetColor(density);
            }

            image.RenderLines(points, System.Drawing.Color.FromArgb(colour.A, colour.R, colour.G, colour.B), 2);
        }

        #endregion
    }
}
