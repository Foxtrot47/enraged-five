﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using RSG.Base.Logging;
using RSG.Base.Collections;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using RSG.Metadata.Data;
using RSG.Metadata.Characters;
using RSG.Metadata.Parser;
using System.IO;
using RSG.Metadata.Model;
using System.Threading;
using System.Collections.Concurrent;
using System.Net;
using System.Collections;
using System.Windows;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Windows.Data;
using System.Collections.ObjectModel;

namespace MetadataEditor.AddIn.PedVariation.ViewModel
{
    /// <summary>
    /// The view model that wraps around a 
    /// <see cref="IMetaFile"/> object.
    /// </summary>
    public class MetaFileViewModel :
        ViewModelBase, IWeakEventListener, IMetadataViewModel
    {
        #region Members
        private PedVariationMetaFile m_model;
        private Uri m_liveLinkUrl;
        private Thread m_requestThread;
        private BlockingCollection<RestRequest> m_requestQueue;
        StructureDictionary structureDictionary;
        #endregion // Members

        #region Properties and Associated Member Data
        /// <summary>
        /// A reference to the wrapped 
        /// <see cref="IMetaFile"/> object.
        /// </summary>
        public PedVariationMetaFile Model
        {
            get { return m_model; }
            protected set
            {
                SetPropertyValue(value, () => this.Model,
                    new PropertySetDelegate(delegate(Object newValue) 
                        {
                            m_model = (PedVariationMetaFile)newValue; 
                        }
                    ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public System.Collections.ObjectModel.ObservableCollection<ComponentVariationViewModel> ComponentVariations
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public List<PropVariationViewModel> PropVariations
        {
            get;
            set;
        }

        public SelectionSetCollectionViewModel SelectionSets
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsLiveEditing
        {
            get;
            protected set;
        }
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        public MetaFileViewModel()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public MetaFileViewModel(PedVariationMetaFile model, StructureDictionary structureDictionary)
        {
            this.structureDictionary = structureDictionary;
            this.Model = model;
            this.ComponentVariations = new System.Collections.ObjectModel.ObservableCollection<ComponentVariationViewModel>();
            ICollectionView collectionView = CollectionViewSource.GetDefaultView(this.ComponentVariations);
            collectionView.SortDescriptions.Add(new SortDescription("Name", ListSortDirection.Ascending));
            collectionView.SortDescriptions.Add(new SortDescription("VariationIndex", ListSortDirection.Ascending));


            bool componentOdd = true;
            foreach (PedComponentVariation variation in model.PedVariation.ComponentVariations)
            {
                this.ComponentVariations.Add(new ComponentVariationViewModel(variation, componentOdd));
                componentOdd = !componentOdd;
            }
            this.PropVariations = new List<PropVariationViewModel>();
            bool propOdd = true;
            foreach (PedPropVariation variation in model.PedVariation.PropVariations)
            {
                this.PropVariations.Add(new PropVariationViewModel(variation, propOdd));
                propOdd = !propOdd;
            }

            this.SelectionSets = new SelectionSetCollectionViewModel(model.SelectionSets, this.ComponentVariations, this.PropVariations);
        }
        #endregion // Constructor(s)

        #region Static Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="definition"></param>
        /// <returns></returns>
        public static MetaFileViewModel Create(Structure definition, StructureDictionary dictionary)
        {
            try
            {
                PedVariationMetaFile metaFile = new PedVariationMetaFile(definition, dictionary);
                MetaFileViewModel model = new MetaFileViewModel(metaFile, dictionary);
                return (model);
            }
            catch (Exception ex)
            {
                Log.Log__ExceptionCtx(ex, RSG.Metadata.Consts.LOG_CONTEXT,
                    "Unhandled exception constructing new MetaFileViewModel {0}.",
                    definition.DataType);
                return (null);
            }
        }
        #endregion // Static Controller Methods

        #region Controller Methods
        /// <summary>
        /// Gets the model behind the view model as a MetaFile.
        /// </summary>
        /// <returns></returns>
        public MetaFile GetMetaFile()
        {
            return this.Model.GetRawModel();
        }

        /// <summary>
        /// 
        /// </summary>
        public void Save(String filename)
        {
            try
            {
                this.Model.Serialise(filename);
            }
            catch (Exception ex)
            {
                throw new ModelException("Unhandled exception saving MetaFileViewModel.", ex);
            }
        }
        #endregion // Controller Methods

        #region Live Link
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tunables"></param>
        private void AddListeners(IEnumerable<ITunable> tunables)
        {
            foreach (ITunable tunable in tunables)
            {
                AddListeners(tunable);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tunable"></param>
        private void AddListeners(ITunable tunable)
        {
            if (tunable == null)
            {
                Log.Log__Warning("Tried to add listener to tunable that was null");
                return;
            }

            if (tunable is ArrayTunable)
            {
                ArrayTunable array = tunable as ArrayTunable;
                CollectionChangedEventManager.AddListener(array, this);
                PropertyChangedEventManager.AddListener(array, this, "Items");
            }
            else if (tunable is MapTunable)
            {
                MapTunable map = tunable as MapTunable;
                CollectionChangedEventManager.AddListener(map, this);
                PropertyChangedEventManager.AddListener(map, this, "Items");
            }
            else if (tunable is TunableBase)
            {
                TunableBase tunableBase = tunable as TunableBase;
                PropertyChangedEventManager.AddListener(tunableBase, this, "Value");
            }
            else
            {
                Log.Log__Warning("Tried to add listener to unknown tunable type: {0}", RSG.Metadata.Util.Tunable.GetFullPath(tunable));
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tunables"></param>
        private void RemoveListeners(IEnumerable<ITunable> tunables)
        {
            foreach (ITunable tunable in tunables)
            {
                RemoveListeners(tunable);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tunable"></param>
        private void RemoveListeners(ITunable tunable)
        {
            //if (tunable == null)
            //{
            //    Log.Log__Warning("Tried to remove listener on tunable that was null");
            //    return;
            //}

            //if (tunable is StructureTunable)
            //{
            //    StructureTunable structure = tunable as StructureTunable;
            //    foreach (KeyValuePair<String, ITunable> subTunable in structure)
            //    {
            //        RemoveTunableListeners(subTunable.Value);
            //    }
            //}
            //else if (tunable is ArrayTunable)
            //{
            //    ArrayTunable array = tunable as ArrayTunable;
            //    CollectionChangedEventManager.RemoveListener(array, this);
            //    PropertyChangedEventManager.RemoveListener(array, this, "Items");
            //    foreach (ITunable subTunable in array)
            //    {
            //        RemoveTunableListeners(subTunable);
            //    }
            //}
            //else if (tunable is PointerTunable)
            //{
            //    PointerTunable pointer = tunable as PointerTunable;
            //    ITunable subTunable = pointer.Value as ITunable;
            //    if (subTunable != null)
            //        RemoveTunableListeners(subTunable);
            //}
            //else if (tunable is TunableBase)
            //{
            //    TunableBase tunableBase = tunable as TunableBase;
            //    PropertyChangedEventManager.RemoveListener(tunableBase, this, "Value");
            //}
            //else
            //{
            //    Log.Log__Warning("Tried to remove listener on unknown tunable type: {0}", Tunable.GetFullPath(tunable));
            //}
        }

        /// <summary>
        /// Thread that processes any requests that are in the queue
        /// </summary>
        private void WebRequestThread()
        {
            foreach (RestRequest request in m_requestQueue.GetConsumingEnumerable())
            {
                ProcessRequest(request);
            }
        }

        /// <summary>
        /// Process a single rest request
        /// </summary>
        /// <param name="request"></param>
        private void ProcessRequest(RestRequest request)
        {
            using (WebClient client = new WebClient())
            {
                try
                {
                    client.UploadDataCompleted += (data_sender, data_ev) =>
                    {
                        /*
                        Log.Log__Debug(
                            "Completed sending to {0}", 
                            Tunable.GetFullPath(tunable));
                        */
                        if (data_ev.Error != null)
                        {
                            WebResponse webResponse = ((System.Net.WebException)(data_ev.Error)).Response;
                            string response = null;

                            if (webResponse != null)
                            {
                                Stream responseStream = webResponse.GetResponseStream();
                                byte[] responseBuffer = new byte[responseStream.Length];
                                responseStream.Read(responseBuffer, 0, (int)responseStream.Length);
                                response = Encoding.UTF8.GetString(responseBuffer);
                                responseStream.Close();
                            }
                            else
                            {
                                response = "No response";
                            }

                            //Log.Log__Exception(
                            //    data_ev.Error,
                            //    ">>>> Failed sending updated data for tunable '{0}' to url {1}. Error was: {2} \n <<<<",
                            //    RSG.Metadata.Util.Tunable.GetFullPath(request.tunable),
                            //    request.url,
                            //    response);

                        }
                    };
                    client.UploadDataAsync(new Uri(request.url), request.method, request.data);
                }
                catch (WebException exception)
                {
                    Log.Log__Exception(
                        exception,
                        "Failed sending updated data for tunable '{0}' to url {1}",
                        request.tunable.Name,
                        request.url);
                }
            }
        }

        /// <summary>
        /// Enables the live-link without performing any additional fluff
        /// </summary>
        /// <param name="liveLinkUrl"></param>
        public void EnableLiveLink(Uri liveLinkUrl)
        {
            // Set up the tunable listeners for the currently visible tunable vm's
            AddListeners(this.Model.FileRootTunable.GetAllTunablesRecursive());

            // Setup the request queue and background processing thread
            m_liveLinkUrl = liveLinkUrl;
            m_requestQueue = new BlockingCollection<RestRequest>();
            m_requestThread = new Thread(new ThreadStart(WebRequestThread));
            m_requestThread.Name = "Live Request Thread";
            m_requestThread.Start();

            IsLiveEditing = true;
        }

        /// <summary>
        /// Enable the live-link replacing the local file with the passed in file
        /// </summary>
        /// <param name="newFile"></param>
        public void EnableLiveLink(Uri liveLinkUrl, IMetaFile newFile)
        {
            // Check if we need to swap out the file that is currently on display to the user
            if (newFile != null)
            {
                Model = new PedVariationMetaFile(newFile, this.structureDictionary, this.Model.AudioValuesFile);
                this.ComponentVariations.Clear();
                bool componentOdd = true;
                foreach (PedComponentVariation variation in Model.PedVariation.ComponentVariations)
                {
                    this.ComponentVariations.Add(new ComponentVariationViewModel(variation, componentOdd));
                    componentOdd = !componentOdd;
                }
                this.PropVariations.Clear();
                bool propOdd = true;
                foreach (PedPropVariation variation in Model.PedVariation.PropVariations)
                {
                    this.PropVariations.Add(new PropVariationViewModel(variation, propOdd));
                    propOdd = !propOdd;
                }
            }

            EnableLiveLink(liveLinkUrl);
        }

        /// <summary>
        /// Enable the live-link and push all the local data to the game (if so requested
        /// </summary>
        /// <param name="liveLinkUrl"></param>
        /// <param name="sendDataToGame"></param>
        public void EnableLiveLink(Uri liveLinkUrl, bool sendDataToGame)
        {
            EnableLiveLink(liveLinkUrl);

            if (sendDataToGame)
            {
                SendRootStructure(this.Model.FileRootTunable, liveLinkUrl.ToString());

            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void DisableLiveLink()
        {
            RemoveListeners(this.Model.FileRootTunable.GetAllTunablesRecursive());

            m_requestQueue.CompleteAdding();
            IsLiveEditing = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tunable"></param>
        /// <param name="url"></param>
        private void SendRootStructure(StructureTunable tunable, string url)
        {
            string value = tunable.ToXmlString();
            byte[] data = System.Text.Encoding.UTF8.GetBytes(value);

            AddWebRequest("PUT", url, data, tunable);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="method"></param>
        /// <param name="url"></param>
        /// <param name="data"></param>
        /// <param name="tunable"></param>
        private void AddWebRequest(string method, string url, byte[] data, ITunable tunable)
        {
            RestRequest request = new RestRequest();
            request.method = method;
            request.url = url;
            request.data = data;
            request.tunable = tunable;
            m_requestQueue.Add(request);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="managerType"></param>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
        {
            ITunable tunableSender = (ITunable)sender;
            string url = GetTunableURL(tunableSender);
            if (String.IsNullOrEmpty(url))
            {
                return true;
            }

            if (sender is TunableBase && e is PropertyChangedEventArgs)
            {
                TunableBase tunable = sender as TunableBase;

                PropertyChangedEventArgs pcArgs = e as PropertyChangedEventArgs;
                if (pcArgs != null && pcArgs.PropertyName == "Value")
                {
                    HandleTunableBaseModified(tunable, url);
                }
            }
            else if ((sender is ArrayTunable || sender is MapTunable) && e is NotifyCollectionChangedEventArgs)
            {
                NotifyCollectionChangedEventArgs pcArgs = e as NotifyCollectionChangedEventArgs;
                if (pcArgs != null)
                {
                    if (pcArgs.Action == NotifyCollectionChangedAction.Add)
                    {
                        HandleCollectionAdd(pcArgs.NewItems, pcArgs.NewStartingIndex, url);
                    }
                    else if (pcArgs.Action == NotifyCollectionChangedAction.Remove)
                    {
                        HandleCollectionRemove(pcArgs.OldItems, pcArgs.OldStartingIndex, url);
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tunable"></param>
        /// <param name="newItems"></param>
        /// <param name="startIndex"></param>
        /// <param name="url"></param>
        private void HandleCollectionAdd(IList newItems, int startIndex, string url)
        {
            byte[] data = new byte[] { };
            foreach (ITunable newTunable in newItems)
            {
                string requestUrl = url + "?insert=" + startIndex.ToString();
                ++startIndex;

                if (newTunable is StructureTunable || newTunable is PointerTunable)
                {
                    ITunableSerialisable structure = (ITunableSerialisable)newTunable;
                    XmlDocument root = new XmlDocument();
                    XmlElement element = structure.Serialise(root, true);

                    MemoryStream stream = new MemoryStream();

                    XmlWriter writer = new XmlTextWriter(stream, Encoding.UTF8);
                    element.WriteTo(writer);
                    writer.Flush();

                    //string value = newTunable.ToXmlString();
                    //value = value.Replace( "\"", "" );
                    //data =  System.Text.Encoding.UTF8.GetBytes( value );
                    data = new byte[(int)stream.Length];
                    stream.Position = 0;
                    stream.Read(data, 0, (int)stream.Length);
                }

                AddWebRequest("POST", requestUrl, data, newTunable);

                AddListeners(newTunable);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tunable"></param>
        /// <param name="oldItems"></param>
        /// <param name="startIndex"></param>
        /// <param name="url"></param>
        private void HandleCollectionRemove(IList oldItems, int startIndex, string url)
        {
            byte[] dummyData = new byte[] { };
            string requesturl = url + "?delete=" + startIndex.ToString();

            foreach (ITunable newTunable in oldItems)
            {
                AddWebRequest("POST", requesturl, dummyData, newTunable);

                RemoveListeners(newTunable);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tunable"></param>
        /// <param name="url"></param>
        private void HandleTunableBaseModified(TunableBase tunable, string url)
        {
            // Create data for URL request
            string value = tunable.ToXmlString();
            value = value.Replace("\"", "");
            byte[] data = System.Text.Encoding.UTF8.GetBytes(value);

            AddWebRequest("PUT", url, data, tunable);
        }

        /// <summary>
        /// Creates the url for a particular tunable
        /// </summary>
        /// <param name="tunable"></param>
        /// <returns></returns>
        private String GetTunableURL(ITunable tunable)
        {
            // Build url string.
            string tunablePath = RSG.Metadata.Util.Tunable.GetFullPath(tunable);
            return m_liveLinkUrl.ToString() + "/" + tunablePath;
        }
        #endregion

        #region Classes
        /// <summary>
        /// A rest request to send to the game
        /// </summary>
        private class RestRequest
        {
            public string method;
            public string url;
            public byte[] data;
            public ITunable tunable;
        }
        #endregion
    } // MetaFileViewModel
} // MetadataEditor.AddIn.MetaEditor.ViewModel
