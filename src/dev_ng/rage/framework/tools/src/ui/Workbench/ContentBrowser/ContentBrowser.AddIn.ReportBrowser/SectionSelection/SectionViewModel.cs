﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using RSG.Base.Editor;
using RSG.Model.Common;
using RSG.Model.Common.Map;

namespace ContentBrowser.AddIn.ReportBrowser.SectionSelection
{
    /// <summary>
    /// 
    /// </summary>
    public class SectionViewModel : ViewModelBase
    {
        #region Fields
        private bool m_isChecked;
        #endregion

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public IMapNode Model
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsChecked
        {
            get { return m_isChecked; }
            set
            {
                SetPropertyValue(value, m_isChecked, () => this.IsChecked,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_isChecked = (bool)newValue;
                        }
                    ));

                if (value == true && this.Model is IMapArea)
                    CheckChildren();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<SectionViewModel> Children
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        public SectionViewModel(IMapNode model)
        {
            this.Model = model;
            this.Name = model.Name;
            this.IsChecked = false;
            this.Children = new ObservableCollection<SectionViewModel>();

            if (model is IMapArea)
            {
                IMapArea mapArea = (IMapArea)model;
                foreach (IMapNode child in mapArea.ChildNodes)
                {
                    Children.Add(new SectionViewModel(child));
                }
            }
        }
        #endregion // Constructor(s)

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        private void CheckChildren()
        {
            foreach (SectionViewModel child in Children)
            {
                child.IsChecked = true;
            }
        }
        #endregion // Methods
    } // SectionViewModel
} // ContentBrowser.AddIn.ReportBrowser.SectionSelection
