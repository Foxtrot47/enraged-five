﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ContentBrowser.AddIn;
using Workbench.AddIn.Services;
using Workbench.AddIn;
using System.ComponentModel;

namespace ContentBrowser
{
    /// <summary>
    /// Settings for the content browser.
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.Settings, typeof(ISettings))]
    [ExportExtension(ContentBrowser.AddIn.CompositionPoints.ContentBrowserSettings, typeof(IContentBrowserSettings))]
    public class ContentBrowserSettings : SettingsBase, IContentBrowserSettings
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public string DataSource
        {
            get { return Properties.Settings.Default.DataSource; }
            set { Properties.Settings.Default.DataSource = value; }
        }
        
        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public string Build
        {
            get { return Properties.Settings.Default.Build; }
            set { Properties.Settings.Default.Build = value; }
        }
        
        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public string Level
        {
            get { return Properties.Settings.Default.Level; }
            set { Properties.Settings.Default.Level = value; }
        }
        
        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public string Platform
        {
            get { return Properties.Settings.Default.Platform; }
            set { Properties.Settings.Default.Platform = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public bool PreviewPaneVisible
        {
            get { return Properties.Settings.Default.PreviewPaneVisible; }
            set { Properties.Settings.Default.PreviewPaneVisible = value; }
        }

        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public ContentBrowserSettings()
            : base("Content Browser")
        {
        }
        #endregion // Constructor(s)

        #region Methods
        /// <summary>
        /// Loads settings
        /// </summary>
        public void Load()
        {
        }

        /// <summary>
        /// Applies settings
        /// </summary>
        public void Apply()
        {
            Properties.Settings.Default.Save();
        }

        /// <summary>
        /// Checks its current state for any invalid data.
        /// </summary>
        /// <param name="errors">The service will fill this up with one string per 
        /// error it finds in its data.</param>
        /// <returns>true if everything is fine, false if an error was found</returns>
        public bool Validate(List<String> errors)
        {
            // No validation needed
            return true;
        }
        #endregion // Methods
    } // ContentBrowserSettings
}
