﻿using RSG.Base.Math;
using RSG.Model.Map3;

namespace Workbench.AddIn.MapStatistics.Overlays.RoadDensity
{
    /// <summary>
    /// Point in the chain.
    /// </summary>
    class ChainPoint
    {
        /// <summary>
        /// Position.
        /// </summary>
        public Vector2f Position
        {
            get;
            set;
        }

        /// <summary>
        /// Traffic density.
        /// </summary>
        public int Density
        {
            get;
            set;
        }

        /// <summary>
        /// Special use flag.
        /// </summary>
        public VehicleNodeSpecialUse SpecialUse
        {
            get;
            set;
        }
    }
}
