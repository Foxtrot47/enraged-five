﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetadataEditor.AddIn
{
    public interface IStructureDictionary
    {
        String DefinitionRootLocation { get; }

        Boolean DidLastLoadHaveErrors { get; }

        RSG.Metadata.Model.StructureDictionary Structures { get; }

        void SetDefinitionRootLocation(String root);

        void RefreshDefinitions();

        event EventHandler DefinitionsRefreshed;

        void ShowDefinitionErrorView();
    }
}
