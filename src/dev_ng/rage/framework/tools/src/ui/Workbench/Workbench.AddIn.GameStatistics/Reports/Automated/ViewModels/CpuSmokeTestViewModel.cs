﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Report.Reports.GameStatsReports.Tests;
using RSG.Base.Editor.Command;
using RSG.Base.Extensions;

namespace Workbench.AddIn.GameStatistics.Reports.Automated.ViewModels
{
    /// <summary>
    /// 
    /// </summary>
    public class CpuSmokeTestViewModel : SmokeTestViewModelBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public IDictionary<CpuSmokeTest.CpuSetKey, IList<CpuSmokeTest.CpuInfo>> LatestBuildCpuStats
        {
            get { return m_latestCpuResults; }
            set
            {
                SetPropertyValue(value, () => this.LatestBuildCpuStats,
                          new PropertySetDelegate(delegate(object newValue) { m_latestCpuResults = (IDictionary<CpuSmokeTest.CpuSetKey, IList<CpuSmokeTest.CpuInfo>>)newValue; }));
            }
        }
        private IDictionary<CpuSmokeTest.CpuSetKey, IList<CpuSmokeTest.CpuInfo>> m_latestCpuResults;

        /// <summary>
        /// 
        /// </summary>
        public IDictionary<CpuSmokeTest.CpuSetKey, IList<KeyValuePair<CpuSmokeTest.CpuMetricKey, IList<CpuSmokeTest.CpuInfo>>>> HistoricalCpuStats
        {
            get { return m_historicalCpuResults; }
            set
            {
                SetPropertyValue(value, () => this.HistoricalCpuStats,
                          new PropertySetDelegate(delegate(object newValue) { m_historicalCpuResults = (IDictionary<CpuSmokeTest.CpuSetKey, IList<KeyValuePair<CpuSmokeTest.CpuMetricKey, IList<CpuSmokeTest.CpuInfo>>>>)newValue; }));
            }
        }
        private IDictionary<CpuSmokeTest.CpuSetKey, IList<KeyValuePair<CpuSmokeTest.CpuMetricKey, IList<CpuSmokeTest.CpuInfo>>>> m_historicalCpuResults;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public CpuSmokeTestViewModel(CpuSmokeTest smokeTest, string testName)
            : base(smokeTest, testName)
        {
            LatestBuildCpuStats = smokeTest.LatestBuildCpuStats.Where(item => item.Key.TestName == testName).ToDictionary(item => item.Key, item => item.Value);

            IDictionary<CpuSmokeTest.CpuSetKey, IList<KeyValuePair<CpuSmokeTest.CpuMetricKey, IList<CpuSmokeTest.CpuInfo>>>> historicalStats = new Dictionary<CpuSmokeTest.CpuSetKey, IList<KeyValuePair<CpuSmokeTest.CpuMetricKey, IList<CpuSmokeTest.CpuInfo>>>>();
            foreach (var pair in smokeTest.HistoricalCpuStats.Where(item => item.Key.TestName == testName).GroupBy(pair => new CpuSmokeTest.CpuSetKey(pair.Key.TestName, pair.Key.SetName)))
            {
                historicalStats[pair.Key] = pair.ToList();
            }
            HistoricalCpuStats = historicalStats;
        }
        #endregion // Constructor(s)
    } // CpuSmokeTestViewModel
}
