﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ContentBrowser.ViewModel;
using RSG.Base.Windows.Controls;

namespace ContentBrowser.View
{
    /// <summary>
    /// Interaction logic for BrowserBaseView.xaml
    /// </summary>
    public partial class GenericBrowserView : UserControl
    {
        public GenericBrowserView()
        {
            InitializeComponent();
        }

        private void HeaderedTreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (!(this.DataContext is GenericBrowserViewModel) || !(sender is HeaderedTreeView))
                return;

            (this.DataContext as GenericBrowserViewModel).SelectedItem = (sender as HeaderedTreeView).SelectedItem;
        }
    }
}
