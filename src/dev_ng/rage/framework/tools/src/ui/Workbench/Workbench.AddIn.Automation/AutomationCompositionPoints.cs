﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Workbench.AddIn.Automation
{
    /// <summary>
    /// Automation monitor add-in composition points.
    /// </summary>
    public static class AutomationCompositionPoints
    {
        /// <summary>
        /// Settings composition point.
        /// </summary>
        public const String AutomationMonitorSettings = "7565B597-EF3E-40ff-A521-116B1FFB98CB";
    }
}
