﻿using System;
using System.ComponentModel.Composition;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using RSG.Model.Asset;
using RSG.Model.Common;
using RSG.Model.Common.Map;
using Workbench.AddIn;
using Workbench.AddIn.Services;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.Commands;

namespace LevelBrowser.AddIn.MapBrowser.Commands
{

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Extensions.TextureDictionaryCommands, typeof(IWorkbenchCommand))]
    class OpenTextureDictionaryCommand : WorkbenchMenuItemBase
    {
        #region MEF Imports
        [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager, 
            typeof(ILayoutManager))]
        private ILayoutManager LayoutManager { get; set; }

        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, 
            typeof(IConfigurationService))]
        private IConfigurationService ConfigService { get; set; }

        [ImportExtension(Workbench.AddIn.CompositionPoints.MessageService, 
            typeof(IMessageService))]
        private IMessageService MessageService { get; set; }
        #endregion // MEF Imports

        #region Constructor

        /// <summary>
        /// Default constructor.
        /// </summary>
        public OpenTextureDictionaryCommand()
        {
            this.Header = "_Open";
            this.IsDefault = true;
        }

        #endregion // Constructor

        #region ICommand Implementation
        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
            if (parameter is IList)
            {
                foreach (Object dictionary in (parameter as IList))
                {
                    this.OpenDictionary(dictionary as TextureDictionary);
                }
            }
            else
            {
                this.OpenDictionary(parameter as TextureDictionary);
            }
        }
        #endregion // ICommand Implementation

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dictionary"></param>
        private void OpenDictionary(TextureDictionary dictionary)
        {
            if (dictionary != null)
            {
                RSG.Model.Common.IHasTextureChildren parent = null;
                foreach (RSG.Model.Common.IAsset asset in dictionary.AssetChildren)
                {
                    if (asset is ITexture)
                    {
                        parent = (asset as ITexture).Parent;
                        break;
                    }
                }
                if (parent != null && parent is IMapArchetype)
                {
                    IMapSection section = ((IMapArchetype)parent).ParentSection;
                    RSG.Base.ConfigParser.ConfigGameView gv = ConfigService.GameConfig;
                    if (System.IO.File.Exists(section.ExportZipFilepath))
                    {
                        ProcessStartInfo runRuby = new ProcessStartInfo();
                        runRuby.FileName = System.IO.Path.Combine(gv.ToolsLibDir, "util", "data_extract_singlefile_from_zip.rb");
                        runRuby.Arguments = @"--output=" +
                            System.IO.Path.Combine(gv.ProjectRootDir, "cache", "raw", "maps", section.Name) +
                            " --filter=" +
                            dictionary.Name + "[.]itd[.]zip " + "--overwrite " +
                            section.ExportZipFilepath;
                        System.Diagnostics.Process p = System.Diagnostics.Process.Start(runRuby);
                        p.WaitForExit();

                        String[] files = System.IO.Directory.GetFiles(System.IO.Path.Combine(gv.ProjectRootDir, "cache", "raw", "maps", section.Name), dictionary.Name + ".itd.zip");
                        if (files.Length > 0)
                        {
                            System.Diagnostics.Process.Start(files[0]);
                        }
                        else
                        {
                            this.MessageService.Show("Unable to open Texture Dictionary.",
                                MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                }
                else
                {
                    this.MessageService.Show("Unable to open Texture Dictionary.", 
                        MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
        #endregion // Private Methods

    } // OpenMapSectionCommand

} // LevelBrowser.AddIn.MapBrowser.Commands
