﻿using System;
using System.Collections.Generic;
using Workbench.AddIn.UI.Controls;

namespace Workbench.AddIn.UI.ToolBar
{

    /// <summary>
    /// ToolBarItem interface.
    /// </summary>
    public interface IToolBarItem : IControl
    {
    }

} // Workbench.AddIn.UI.ToolBar namespace
