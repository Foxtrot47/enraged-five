﻿using System;
using System.Collections.Generic;
using System.Windows;
using Editor.AddIn;
using Editor.Workbench.AddIn;
using MetadataEditor.AddIn.TextEditor.Controls;

namespace MetadataEditor.AddIn.TextEditor
{

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Editor.AddIn.ExtensionPoints.Views,
        typeof(ResourceDictionary))]
    public partial class XmlTextView : ResourceDictionary
    {
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public XmlTextView()
        {
            InitializeComponent();
        }
        #endregion // Constructor(s)

        private void textEditor_DocumentChanged(object sender, EventArgs e)
        {
            RSG.Base.Logging.Log.Log__Debug("textEditor::DocumentChanged()");
        }
    }

} // MetadataEditor.AddIn.TextEditor
