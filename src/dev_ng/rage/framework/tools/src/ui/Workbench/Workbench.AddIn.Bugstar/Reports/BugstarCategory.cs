﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using RSG.Model.Report;
using System.ComponentModel.Composition;
using RSG.Base.Logging;
using Report.AddIn;
using RSG.Interop.Bugstar;
using System.IO;
using System.Xml;
using RSG.Interop.Bugstar.Organisation;

namespace Workbench.AddIn.Bugstar.Reports
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Report.AddIn.ExtensionPoints.ReportCategory, typeof(IReportCategory))]
    class BugstarCategory :
        ReportCategory,
        IPartImportsSatisfiedNotification
    {
        #region Constants
        private const String NAME = "Bugstar";
        private const String DESC = "List of all reports that originate from Bugstar.";
        #endregion // Constants

        #region Properties and Associated Member Data
        /// <summary>
        /// Category report objects.
        /// This is abstract since we wish your class to implement it with the appropriate attribute ( as defined by your class ) for MEF imports.
        /// </summary>
        [ImportManyExtension(ExtensionPoints.Report, typeof(IReport))]
        public override IEnumerable<IReportItem> Reports { get; protected set; }

        /// <summary>
        /// MEF import for login service.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LoginService, typeof(Workbench.AddIn.Services.ILoginService))]
        public Workbench.AddIn.Services.ILoginService LoginService { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(CompositionPoints.ConfigurationService, typeof(Workbench.AddIn.Services.IConfigurationService))]
        Lazy<Workbench.AddIn.Services.IConfigurationService> Config { get; set; }
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public BugstarCategory()
            : base(NAME, DESC)
        {
        }
        #endregion // Constructor(s)
    } // BugstarCategory
} // Workbench.AddIn.Bugstar.Reports namespace
