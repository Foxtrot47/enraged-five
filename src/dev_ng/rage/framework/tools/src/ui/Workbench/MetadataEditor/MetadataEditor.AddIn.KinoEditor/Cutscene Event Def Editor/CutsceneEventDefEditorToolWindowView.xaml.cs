﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Diagnostics;
using Workbench.AddIn;
using Workbench.AddIn.UI.Layout;
using System.IO;

namespace MetadataEditor.AddIn.KinoEditor.Cutscene_Event_Def_Editor
{
    /// <summary>
    /// Interaction logic for CutsceneEventDefEditorToolWindowView.xaml
    /// </summary>
    public partial class CutsceneEventDefEditorToolWindowView : DocumentBase<CutsceneEventDefEditorToolWindowViewModel>
    {
        #region Constants
        public static readonly Guid GUID = new Guid("F1BD8C22-43B9-4708-9317-040507FA95B3");
        #endregion // Constants

        public CutsceneEventDefEditorToolWindowView(string filename)
            : base(CutsceneEventDefEditorToolWindowViewModel.TOOLWINDOW_NAME, new CutsceneEventDefEditorToolWindowViewModel())
        {
            InitializeComponent();
            this.ID = GUID;
            this.Title = System.IO.Path.GetFileNameWithoutExtension(filename);
            if (File.Exists(filename))
                ViewModel.Open(filename);
        }

        private CutsceneEventDefEditorToolWindowViewModel GetViewModel(object sender)
        {
            Button vmSender = (sender as Button);
            if (vmSender != null)
            {
                CutsceneEventDefEditorToolWindowViewModel vm = ((vmSender as Button).DataContext as CutsceneEventDefEditorToolWindowViewModel);
                return vm; 
            }

            MenuItem vnSender = (sender as MenuItem);
            if (vnSender != null)
            {
                CutsceneEventDefEditorToolWindowViewModel vm = ((vnSender as MenuItem).DataContext as CutsceneEventDefEditorToolWindowViewModel);
                return vm; 
            }

            return null;
        }

        #region Button Clicks

        private void New_Event_Click(object sender, EventArgs e)
        {
            GetViewModel(sender).SelectedEvent = new Event();
        }

        private void Add_Event_Click(object sender, EventArgs e)
        {
            Event e2 = new Event();
            e2.EventName = GetViewModel(sender).SelectedEvent.EventName;
            e2.Rank = GetViewModel(sender).SelectedEvent.Rank;
            e2.OppositeEvent = GetViewModel(sender).SelectedEvent.OppositeEvent;
            e2.AssociatedEventArg = GetViewModel(sender).SelectedEvent.AssociatedEventArg;

            GetViewModel(sender).OppositeEventEntries.Add(e2);

            GetViewModel(sender).EventEntries.Add(e2);
        }

        private void Delete_Event_Click(object sender, EventArgs e)
        {
            GetViewModel(sender).OppositeEventEntries.Remove(GetViewModel(sender).SelectedEvent);

            GetViewModel(sender).EventEntries.Remove(GetViewModel(sender).SelectedEvent);
            GetViewModel(sender).SelectedEvent = new Event();
            GetViewModel(sender).SelectedEvent.OppositeEvent = new Event();
        }

        private void New_EventArg_Click(object sender, EventArgs e)
        {
            GetViewModel(sender).SelectedEventArg = new EventArg();
        }

        private void Add_EventArg_Click(object sender, EventArgs e)
        {
            EventArg e2 = new EventArg();
            e2.Name = GetViewModel(sender).SelectedEventArg.Name;
            e2.Attributes = GetViewModel(sender).SelectedEventArg.Attributes.Clone();

            GetViewModel(sender).EventArgEntries.Add(e2);
        }

        private void Delete_EventArg_Click(object sender, EventArgs e)
        {
            GetViewModel(sender).EventArgEntries.Remove(GetViewModel(sender).SelectedEventArg);
            GetViewModel(sender).SelectedEventArg = new EventArg();
        }

        private void New_Attribute_Click(object sender, EventArgs e)
        {
            GetViewModel(sender).SelectedAttribute = new Attribute();
        }

        private void Add_Attribute_Click(object sender, EventArgs e)
        {
            Attribute a = new Attribute();
            a.Name = GetViewModel(sender).SelectedAttribute.Name;
            a.Type = GetViewModel(sender).SelectedAttribute.Type;
            a.DefaultValue = GetViewModel(sender).SelectedAttribute.DefaultValue;

            GetViewModel(sender).SelectedEventArg.Attributes.Add(a);
        }

        private void Delete_Attribute_Click(object sender, EventArgs e)
        {
            GetViewModel(sender).SelectedEventArg.Attributes.Remove(GetViewModel(sender).SelectedAttribute);
            GetViewModel(sender).SelectedAttribute = new Attribute();
        }

        private void Save_Click(object sender, EventArgs e)
        {
            if (GetViewModel(sender).GetCurrentProject() == String.Empty)
            {
                Save_As_Click(sender, e);
            }
            else
            {
                GetViewModel(sender).Save(GetViewModel(sender).GetCurrentProject());
            }
        }

        private void New_Click(object sender, EventArgs e)
        {
            GetViewModel(sender).Reset();
        }

        private void Save_As_Click(object sender, EventArgs e)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.DefaultExt = ".xml";
            dlg.Filter = "Event Definition files (.xml)|*.xml";

            Nullable<bool> result = dlg.ShowDialog();

            if (result == true)
            {
                GetViewModel(sender).Save(dlg.FileName);
            }
        }

        private void Open_Click(object sender, EventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.DefaultExt = ".xml";
            dlg.Filter = "Event Definition files (.xml)|*.xml";

            Nullable<bool> result = dlg.ShowDialog();

            if (result == true)
            {
                GetViewModel(sender).Open(dlg.FileName);
            }
        }

        private void Browse_Header_Click(object sender, EventArgs e)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.DefaultExt = ".h";
            dlg.Filter = "C++ Header Files (.h)|*.h";

            Nullable<bool> result = dlg.ShowDialog();

            if (result == true)
            {
                GetViewModel(sender).HeaderFilename = dlg.FileName;
            }
        }

        private void Generate_Header_Click(object sender, EventArgs e)
        {
            GetViewModel(sender).GenerateHeader();
        }

        #endregion
    }
}
