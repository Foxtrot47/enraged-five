﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using RSG.Base.Collections;
using System.Windows.Media;
using System.ComponentModel;

namespace Workbench.AddIn.MapStatistics.Overlays.RoadDensity
{
    /// <summary>
    /// Road density view model. Use to fill the control panel that allows the user to choos what roads are displayed.
    /// </summary>
    class RoadDensityViewModel : ViewModelBase, INotifyPropertyChanged
    {
        #region Private member fields

        private RoadDensityModel m_model;

        #endregion

        #region Public properties

        /// <summary>
        /// View model collection.
        /// </summary>
        public ObservableCollection<ViewModelBase> Items
        {
            get;
            private set;
        }

        /// <summary>
        /// Show / hide pedestrian crossings.
        /// </summary>
        public bool ShowPedestrianCrossings
        {
            get { return !m_model.IgnorePedCrossings; }
            set
            {
                SetPropertyValue(value, () => ShowPedestrianCrossings,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_model.IgnorePedCrossings = !(bool)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Show / hide pedestrian crossings.
        /// </summary>
        public bool ShowAssistedMovementRoutes
        {
            get { return !m_model.IgnoreAssistedMovementRoutes; }
            set
            {
                SetPropertyValue(value, () => ShowAssistedMovementRoutes,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_model.IgnoreAssistedMovementRoutes = !(bool)newValue;
                        }
                ));
            }
        }

        
        
        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        internal RoadDensityViewModel(RoadDensityModel model)
        {
            m_model = model;
    
            Items = new ObservableCollection<ViewModelBase>();
            Items.Add(new DensityViewModel("Freeway Busy", 15, Color.FromRgb(255, 0, 0)));
            Items.Add(new DensityViewModel("Freeway Busy", 14, Color.FromRgb(255, 47, 0)));
            Items.Add(new DensityViewModel("Freeway Normal", 13, Color.FromRgb(255, 95, 0)));
            Items.Add(new DensityViewModel("Freeway Normal", 12, Color.FromRgb(255, 143, 0)));
            Items.Add(new DensityViewModel("Very High", 11, Color.FromRgb(255, 191, 0)));
            Items.Add(new DensityViewModel("Very High", 10, Color.FromRgb(255, 239, 0)));
            Items.Add(new DensityViewModel("Quite Busy", 9, Color.FromRgb(223, 255, 0)));
            Items.Add(new DensityViewModel("Quite Busy", 8, Color.FromRgb(175, 255, 0)));
            Items.Add(new DensityViewModel("Average", 7, Color.FromRgb(127, 255, 0)));
            Items.Add(new DensityViewModel("Average", 6, Color.FromRgb(79, 255, 0)));
            Items.Add(new DensityViewModel("Quite Low", 5, Color.FromRgb(31, 255, 0)));
            Items.Add(new DensityViewModel("Quite Low", 5, Color.FromRgb(0, 255, 15)));
            Items.Add(new DensityViewModel("Very Low", 2, Color.FromRgb(0, 255, 63)));
            Items.Add(new DensityViewModel("Very Low", 1, Color.FromRgb(0, 255, 111)));
            Items.Add(new DensityViewModel("None", 0, Color.FromRgb(0, 255, 159)));
        }

        #endregion
    }
}
