﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Report.Reports.GameStatsReports.Tests;
using RSG.Base.Editor.Command;

namespace Workbench.AddIn.GameStatistics.Reports.Automated.ViewModels
{
    /// <summary>
    /// 
    /// </summary>
    public class SummaryViewModel : SmokeTestViewModelBase
    {
        #region Constants
        private const string c_name = "Summary";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public List<SmokeTestResultViewModel> SmokeTestResults
        {
            get { return m_smokeTestResults; }
            set
            {
                SetPropertyValue(value, () => this.SmokeTestResults,
                          new PropertySetDelegate(delegate(object newValue) { m_smokeTestResults = (List<SmokeTestResultViewModel>)newValue; }));
            }
        }
        private List<SmokeTestResultViewModel> m_smokeTestResults;

        /// <summary>
        /// 
        /// </summary>
        public FpsSmokeTest FpsSmokeTest
        {
            get { return m_smokeTest; }
            set
            {
                SetPropertyValue(value, () => this.FpsSmokeTest,
                          new PropertySetDelegate(delegate(object newValue) { m_smokeTest = (FpsSmokeTest)newValue; }));
            }
        }
        private FpsSmokeTest m_smokeTest;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public SummaryViewModel(IList<ISmokeTest> smokeTests)
            : base(c_name)
        {
            FpsSmokeTest = smokeTests.OfType<FpsSmokeTest>().FirstOrDefault();

            List<SmokeTestResultViewModel> results = new List<SmokeTestResultViewModel>();
            foreach (ISmokeTest test in smokeTests)
            {
                results.AddRange(test.Errors.Select(item => new SmokeTestResultViewModel("Error", item)));
                results.AddRange(test.Warnings.Select(item => new SmokeTestResultViewModel("Warning", item)));
                results.AddRange(test.Messages.Select(item => new SmokeTestResultViewModel("Message", item)));
            }
            results.Sort();
            SmokeTestResults = results;
        }
        #endregion // Constructor(s)
    } // SummaryViewModel
}
