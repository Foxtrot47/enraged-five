﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Map3;
using RSG.Model.Common.Map;

namespace LevelBrowser.AddIn.MapBrowser.BatchExporter.Model
{
    /// <summary>
    /// Interface for map section export collections.
    /// </summary>
    public interface IBatchExportCollection
    {
        #region Public properties

        /// <summary>
        /// Returns true if the collection contains at least one item.
        /// </summary>
        bool HasItems { get; }

        #endregion

        #region Add and remove methods

        /// <summary>
        /// Add a map section to the internal collection.
        /// </summary>
        /// <param name="mapName">Map name.</param>
        /// <param name="mapSection">Map section.</param>
        void Add(string mapName, MapSection mapSection);

        /// <summary>
        /// Remove all map sections from the internal collection.
        /// </summary>
        void RemoveAll();

        /// <summary>
        /// Remove a single item from the list.
        /// </summary>
        /// <param name="section">Section.</param>
        void Remove(MapSection section);

        #endregion

        #region Get methods

        /// <summary>
        /// Enumerates the internal collection of MapSection instances.
        /// </summary>
        /// <returns>An enumeration of MapSection instances.</returns>
        IEnumerable<MapSection> GetSections();

        /// <summary>
        /// Enumerates the key value pair of section name and section data.
        /// </summary>
        /// <returns>An enumeration of string and MapSection key value pairs.</returns>
        IEnumerable<KeyValuePair<string, MapSection>> GetAll();

        #endregion

        #region Serialization methods

        /// <summary>
        /// Populate map section data from the map hierarchy.
        /// </summary>
        /// <param name="mapHierarchy">Map hierarchy containing MapSection data.</param>
        void PopulateMapSections(IMapHierarchy mapHierarchy);

        /// <summary>
        /// Deserialize the data stored on disk.
        /// </summary>
        /// <param name="pathToFile">Path to file.</param>
        /// <param name="mapHierarchy">Map hierarchy that contains MapSection data.</param>
        void Deserialize(string pathToFile, IMapHierarchy mapHierarchy);

        /// <summary>
        /// Serialize the data to disk.
        /// </summary>
        /// <param name="pathToFile">Path to file.</param>
        void Serialize(string pathToFile);

        #endregion
    }
}
