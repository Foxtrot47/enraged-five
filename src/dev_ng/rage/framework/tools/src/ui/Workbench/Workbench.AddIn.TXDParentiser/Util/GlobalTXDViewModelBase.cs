﻿using System;
using System.Windows;
using System.Windows.Media.Imaging;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using Workbench.AddIn.TXDParentiser.ViewModel;

namespace Workbench.AddIn.TXDParentiser.Util
{
    public abstract class GlobalTXDViewModelBase :
        HierarchicalViewModelBase, IWeakEventListener
    {
        #region Add Command

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand AddCommand
        {
            get
            {
                if (m_addCommand == null)
                {
                    m_addCommand = new RelayCommand(param => this.AddExecute(param), param => this.AddCanExecute(param));
                }
                return m_addCommand;
            }
        }
        RelayCommand m_addCommand;

        /// <summary>
        /// 
        /// </summary>
        public virtual Boolean AddCanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual void AddExecute(Object parameter)
        {
        }

        #endregion // Add Command

        #region IWeakEventListener Members

        /// <summary>
        /// 
        /// </summary>
        public virtual Boolean ReceiveWeakEvent(Type managerType, Object sender, EventArgs e)
        {
            return true;
        }

        #endregion
    }
}
