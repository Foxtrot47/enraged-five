﻿using System;
using System.Collections.Generic;
using Workbench.AddIn;
using Workbench.AddIn.Services;
using System.Net;

namespace ContentBrowser.AddIn.Perforce
{

    /// <summary>
    /// Perforce settings.
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.Settings,
        typeof(ISettings))]
    public class Preferences : SettingsBase, ISettings
    {
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public Preferences()
            : base("Perforce")
        {
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        public void Load()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public void Apply()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="errors"></param>
        /// <returns></returns>
        public bool Validate(List<String> errors)
        {
            return (true);
        }
        #endregion // Controller Methods
    }

} // ContentBrowser.AddIn.Perforce namespace
