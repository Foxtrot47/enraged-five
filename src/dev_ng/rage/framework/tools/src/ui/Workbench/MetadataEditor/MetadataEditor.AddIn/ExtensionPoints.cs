﻿using System;

namespace MetadataEditor.AddIn
{

    /// <summary>
    /// Metadata Editor extension point String GUIDs contracts.
    /// </summary>
    public static class ExtensionPoints
    {
        #region Extension Points
        /// <summary>
        /// Metadata Document view or editor extension point contract.
        /// </summary>
        public const String MetadataDocument =
            "4821CB2C-8E97-4152-B9BB-E1A78275DBFE";

        /// <summary>
        /// Metadata Document view or editor extension point contract for the 
        /// deafult metadata document view
        /// </summary>
        public const String DefaultMetadataDocument =
            "2A3BF883-38F1-438C-ABE4-E93A75BAE13F";

        /// <summary>
        /// Definition Document view or editor extension point contract.
        /// </summary>
        public const String DefinitionDocument =
            "01551B9C-1EA1-455B-ADEF-372A825B06C9";

        #region Menus
        /// <summary>
        /// Metadata menu item; IMenuItem interface.
        /// </summary>
        public const String MenuMetadata =
            "701CE379-9BC9-4392-A73F-CE4041452AE6";
        #endregion // Menus

        #region Toolbars
        /// <summary>
        /// Metadata Editor Toolbar
        /// </summary>
        public const String MetadataToolbar = "D9D3C203-681B-495A-A505-CDE30612E008";
        #endregion // Toolbars
        #endregion // Extension Points
    }

} // MetadataEditor.AddIn.ExtensionPoints namespace
