﻿using System;
using System.Windows.Input;

namespace Workbench.AddIn.UI.Controls
{

    /// <summary>
    /// 
    /// </summary>
    public interface ICommandControl : IControl, ICommand
    {
    }

} // Workbench.AddIn.UI.Controls namespace
