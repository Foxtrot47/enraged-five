﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ContentBrowser.AddIn
{
    /// <summary>
    /// Contants a set of static strings that are used to extend the
    /// content browser.
    /// </summary>
    public static class ExtensionPoints
    {
        /// <summary>
        /// The point for a item to use to add itself to the
        /// content browsers source panel
        /// </summary>
        public const String Browser =
            "C2CC41A6-DBFD-4104-86D5-A8C09DA5292C";

        #region Commands

        public const String AssetCommandContainer = "437F1D41-CD57-498A-BFC0-47C13C0E2EB0";

        public const String ReportAssetCommands = "F1128823-B17D-40E7-8049-009DFBE27FFC";

        public const String MetadataAssetCommands = "F843A4CD-05D2-4D1D-A84E-A01D971D89D6";

        #endregion // Commands
        
    } // ExtensionPoints
} // ContentBrowser.AddIn
