﻿using System;

namespace Workbench.AddIn.MapDebugging
{

    /// <summary>
    /// Extension Points
    /// </summary>
    public static class ExtensionPoints
    {
        /// <summary>
        /// 
        /// </summary>
        public const String Report = 
            "2A32B815-0E40-49CB-8A9D-E241AE9C7DF6";

        /// <summary>
        /// 
        /// </summary>
        public const String GameStatReports =
            "EC96FA88-0186-464C-9DE0-D580C9F4723C";
    }

} // Workbench.AddIn.MapDebugging namespace
