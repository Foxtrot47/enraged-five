﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;

namespace Workbench.AddIn.MapStatistics.Overlays.Bounds
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Viewport.AddIn.ExtensionPoints.OverlayGroup, typeof(Viewport.AddIn.IViewportOverlayGroup))]
    public class BoundsStatisticsGroup : Viewport.AddIn.ViewportOverlayGroup, IPartImportsSatisfiedNotification
    {
        #region MEF Imports
        /// <summary>
        /// Imported bounds statistics overlays
        /// </summary>
        [ImportManyExtension(Workbench.AddIn.MapStatistics.ExtensionPoints.BoundsStatisticsOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
        IEnumerable<Viewport.AddIn.IViewportOverlay> ImportedOverlays
        {
            get;
            set;
        }
        #endregion // MEF Imports

        #region Constructor
        /// <summary>
        /// Default Constructor
        /// </summary>
        public BoundsStatisticsGroup()
            : base("Bounds Statistics")
        {
        }
        #endregion // Constructor

        #region IPartImportsSatisfiedNotification
        /// <summary>
        /// Enables implementers to be notified when a part's imports have been satisfied.
        /// </summary>
        public void OnImportsSatisfied()
        {
            foreach (Viewport.AddIn.IViewportOverlay overlay in this.ImportedOverlays)
            {
                this.Overlays.Add(overlay);
            }
        }
        #endregion // IPartImportsSatisfiedNotification
    } // BoundsStatisticsGroup
} // Workbench.AddIn.MapStatistics.Overlays.Bounds
