﻿using System.Collections.Generic;
using RSG.Base.Collections;
using RSG.Base.Editor;
using RSG.Metadata.Characters;

namespace MetadataEditor.AddIn.PedVariation.ViewModel
{
    public class SelectionSetCollectionViewModel : ViewModelBase
    {
        #region Fields
        /// <summary>
        /// The private reference to the model data provider for this view model.
        /// </summary>
        private SelectionSets _selectionSets;

        private IDictionary<int, SortedList<int, string>> _components;

        private IDictionary<int, Dictionary<int, string>> _props;

        private IDictionary<int, List<string>> _componentTextures;

        private IDictionary<int, List<string>> _propTextures;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SelectionSetViewModel"/> class.
        /// </summary>
        /// <param name="selectionSets">
        /// The model that provides the data for this view model.
        /// </param>
        public SelectionSetCollectionViewModel(SelectionSets selectionSets, IEnumerable<ComponentVariationViewModel> componentViewModels, List<PropVariationViewModel> propViewModels)
        {
            this.SelectionSets = new ObservableCollection<SelectionSetViewModel>();
            this._selectionSets = selectionSets;

            this._components = new SortedDictionary<int, SortedList<int, string>>();
            this._componentTextures = new Dictionary<int, List<string>>();
            foreach (ComponentVariationViewModel component in componentViewModels)
            {
                int index = component.ComponentIndex;

                SortedList<int, string> names;
                if (!this._components.TryGetValue(index, out names))
                {
                    names = new SortedList<int, string>();
                    this._components.Add(index, names);
                }

                names.Add(component.VariationIndex, component.Name);

                List<string> textureNames;
                if (!this._componentTextures.TryGetValue(index, out textureNames))
                {
                    textureNames = new List<string>();
                    textureNames.Add("Random");
                    this._componentTextures.Add(index, textureNames);
                }

                for (int i = 0; i < component.Textures.Count; i++)
                {
                    string textureCode = ((char)(97 + i)).ToString();
                    if (!textureNames.Contains(textureCode))
                    {
                        textureNames.Add(textureCode);
                    }
                }
            }

            this._props = new SortedDictionary<int, Dictionary<int, string>>();
            this._propTextures = new Dictionary<int, List<string>>();
            foreach (PropVariationViewModel prop in propViewModels)
            {
                int index = prop.Model.PropAnchorIndex;

                Dictionary<int, string> names;
                if (!this._props.TryGetValue(index, out names))
                {
                    names = new Dictionary<int, string>();
                    names.Add(255, "Random");
                    this._props.Add(index, names);
                }

                if (!names.ContainsKey(prop.Model.VariationIndex))
                {
                    names.Add(prop.Model.VariationIndex, prop.Name);
                }

                List<string> textureNames;
                if (!this._propTextures.TryGetValue(index, out textureNames))
                {
                    textureNames = new List<string>();
                    textureNames.Add("Random");
                    this._propTextures.Add(index, textureNames);
                }

                for (int i = 0; i < prop.Textures.Count; i++)
                {
                    string textureCode = ((char)(97 + i)).ToString();
                    if (!textureNames.Contains(textureCode))
                    {
                        textureNames.Add(textureCode);
                    }
                }
            }

            foreach (SelectionSet set in this._selectionSets.Sets)
            {
                this.SelectionSets.Add(new SelectionSetViewModel(set, this));
            }
        }
        #endregion Constructors

        #region Properties
        public ObservableCollection<SelectionSetViewModel> SelectionSets
        {
            get;
            set;
        }

        internal IDictionary<int, SortedList<int, string>> Components
        {
            get { return this._components; }
        }

        internal IDictionary<int, Dictionary<int, string>> Props
        {
            get { return this._props; }
        }

        internal int AvailableAnchorPointCount
        {
            get { return this._props.Count; }
        }

        internal IEnumerable<string> AvailableAnchorPoints
        {
            get
            {
                foreach (byte index in this._props.Keys)
                {
                    yield return PropSetViewModel._anchorPoints[index];
                }

                yield return "Empty";
            }
        }
        #endregion Properties

        #region Methods
        public void AddNewSelectionSet()
        {
            SelectionSet newSet = this._selectionSets.AddSelectionSet("New Selection Set");
            SelectionSetViewModel vm = new SelectionSetViewModel(newSet, this);
            vm.IsExpanded = true;
            this.SelectionSets.Add(vm);
        }

        public void Remove(SelectionSetViewModel set)
        {
            int index = this.SelectionSets.IndexOf(set);
            this._selectionSets.RemoveSelectionSet(index);
            this.SelectionSets.RemoveAt(index);
        }

        internal IEnumerable<string> ComponentTextures(int index)
        {
            List<string> textureNames;
            if (!this._componentTextures.TryGetValue(index, out textureNames))
            {
                yield break;
            }

            foreach (string textureName in textureNames)
            {
                yield return textureName;
            }
        }

        internal Dictionary<byte, string> PropTextures(int index)
        {
            List<string> textureNames;
            if (!this._propTextures.TryGetValue(index, out textureNames))
            {
                return new Dictionary<byte, string>();
            }

            Dictionary<byte, string> result = new Dictionary<byte, string>();
            byte textureIndex = 0;
            foreach (string textureName in textureNames)
            {
                if (textureName == "Random")
                {
                    result.Add(255, textureName);
                }
                else
                {
                    result.Add(textureIndex++, textureName);
                }
            }

            return result;
        }

        internal Dictionary<int, string> PropsForIndex(int index)
        {
            Dictionary<int, string> drawableNames;
            if (!this._props.TryGetValue(index, out drawableNames))
            {
                return new Dictionary<int, string>();
            }

            return drawableNames;
        }
        #endregion Methods
    }
}
