﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using RSG.Base.Collections;
using ragCore;
using ragWidgets;
using System.Diagnostics;
using RSG.Base.Editor.Command;

namespace WidgetEditor.ViewModel
{


    public class WidgetDocumentViewModel : ViewModelBase
    {
        #region Properties
        //public IModel Model { get; set; }

        public ObservableCollection<WidgetViewModel> Widgets { get; set; }
        

        private WidgetGroupViewModel m_RootWidget;
        public WidgetGroupViewModel RootWidget
        {
            get { return m_RootWidget; }
            set
            {
                SetPropertyValue( value, () => this.RootWidget,
                                new PropertySetDelegate( delegate( Object newValue ) { m_RootWidget = (WidgetGroupViewModel)newValue; } ) );
            }
        }

        private string m_Path;
        public string Path
        {
            get { return m_Path; }
            set
            {
                SetPropertyValue( value, () => this.Path,
                                new PropertySetDelegate( delegate( Object newValue ) { m_Path = (string)newValue; } ) );
            }
        }

        public WidgetDocumentViewModel()
        {
             Widgets = new ObservableCollection<WidgetViewModel>();
        }




        #endregion

    }
}
