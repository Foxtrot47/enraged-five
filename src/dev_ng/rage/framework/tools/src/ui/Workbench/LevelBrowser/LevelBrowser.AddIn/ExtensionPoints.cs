﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LevelBrowser.AddIn
{
    /// <summary>
    /// Contants a set of static strings that are used to extend the
    /// level browser.
    /// </summary>
    public class ExtensionPoints
    {
        /// <summary>
        /// The point for a item to use to add itself to the
        /// level browsers item source hierarchy
        /// </summary>
        public const String LevelBrowserComponent =
            "EA2355D6-5C8B-4D22-B457-9BB1128B7ED6";

        /// <summary>
        /// The point for a item to use to add itself to the
        /// level browsers main menu item
        /// </summary>
        public const String LevelBrowserMenu =
            "D1A36E52-E2D1-4ADB-B820-2E1C94353E0F";
    } // ExtensionPoints
} // LevelBrowser.AddIn
