﻿using System;
using System.ComponentModel.Composition;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn;
using Workbench.AddIn.Services;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.Commands;
using RSG.Model.Common;

namespace LevelBrowser.Commands
{
    [ExportExtension(Extensions.TextureCommands, typeof(IWorkbenchCommand))]
    class EditTextureCommand : WorkbenchMenuItemBase
    {
        #region MEF Imports

        [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager, typeof(ILayoutManager))]
        private ILayoutManager LayoutManager { get; set; }

        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(IConfigurationService))]
        private IConfigurationService ConfigServiceImport { get; set; }

        [ImportExtension(Workbench.AddIn.CompositionPoints.MessageService, typeof(IMessageService))]
        private IMessageService MessageService { get; set; }

        /// <summary>
        /// Open service for metadata editor
        /// </summary>
        [ImportExtension(MetadataEditor.AddIn.CompositionPoints.MetadataOpenService, typeof(Workbench.AddIn.Services.File.IOpenService))]
        private Workbench.AddIn.Services.File.IOpenService MetadataOpenService
        {
            get;
            set;
        }

        /// <summary>
        /// Open service for metadata editor
        /// </summary>
        [ImportExtension(MetadataEditor.AddIn.CompositionPoints.StructureDictionary, typeof(MetadataEditor.AddIn.IStructureDictionary))]
        private MetadataEditor.AddIn.IStructureDictionary StructureDictionary
        {
            get;
            set;
        }

        /// <summary>
        /// Open service for the workbench
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.WorkbenchOpenService, typeof(Workbench.AddIn.Services.File.IWorkbenchOpenService))]
        private Workbench.AddIn.Services.File.IWorkbenchOpenService WorkbenchOpenService
        {
            get;
            set;
        }

        /// <summary>
        /// MEF import for login service.
        /// </summary>
        [ImportExtension(PerforceBrowser.AddIn.CompositionPoints.PerforceService, typeof(PerforceBrowser.AddIn.IPerforceService))]
        public PerforceBrowser.AddIn.IPerforceService PerforceService { get; set; }

        #endregion // MEF Imports

        #region Constructor

        /// <summary>
        /// Default constructor.
        /// </summary>
        public EditTextureCommand()
        {
            this.Header = "_Edit Metadata";
            this.IsDefault = true;
        }

        #endregion // Constructor

        #region ICommand Implementation

        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
            int currentDocuments = LayoutManager.Documents.Count;
            List<String> filenames = new List<String>();
            if (parameter is IList)
            {
                if ((parameter as IList).Count == 1)
                {
                    this.EditTexture((parameter as IList)[0] as ITexture, ref filenames);
                }
                else if ((parameter as IList).Count > 1)
                {
                    List<ITexture> textures = new List<ITexture>();
                    foreach (Object texture in (parameter as IList))
                    {
                        if (texture is ITexture)
                            textures.Add(texture as ITexture);
                    }
                    EditTextures(textures, ref filenames);
                }
            }
            else
            {
                this.EditTexture(parameter as ITexture, ref filenames);
            }

            if (filenames.Count > 0 && LayoutManager.Documents.Count > currentDocuments)
            {
                System.Windows.MessageBoxResult r = MessageService.Show("Do you want to automatically check out the tcs file(s) into the deault changelist?", System.Windows.MessageBoxButton.YesNo, System.Windows.MessageBoxImage.Question);
                if (r == System.Windows.MessageBoxResult.Yes)
                {
                    String currDir = System.IO.Directory.GetCurrentDirectory();
                    RSG.SourceControl.Perforce.P4 p4 = PerforceService.PerforceConnection;
                    if (p4 == null)
                    {
                        return;
                    }

                    try
                    {
                        foreach (String filename in filenames)
                        {
                            try
                            {
                                p4.Run("edit", filename);
                            }
                            catch (P4API.Exceptions.P4APIExceptions ex)
                            {
                                RSG.Base.Logging.Log.Log__Exception(ex, "Unhandled Perforce exception while trying to edit the file {0}.", filename);
                            }
                        }
                    }
                    catch (P4API.Exceptions.P4APIExceptions ex)
                    {
                        this.MessageService.Show("Error checking out tcs files, some files will remain read only.", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                        RSG.Base.Logging.Log.Log__Exception(ex, "Unhandled Perforce exception while trying to connect to the p4 server.");
                    }
                    finally
                    {
                        System.IO.Directory.SetCurrentDirectory(currDir);
                    }
                }
            }
        }

        private void EditTexture(ITexture texture, ref List<String> filenames)
        {
            if (texture != null)
            {
                String filename = String.Empty;
                filename = RSG.Model.Asset.Util.TextureUtil.GetTcsFilenameForTexture(texture, this.ConfigServiceImport.GameConfig);
                if (!String.IsNullOrEmpty(filename))
                {
                    filenames.Add(filename);
                    this.WorkbenchOpenService.OpenFileWithService(this.MetadataOpenService, filename);
                }
                else
                {
                    String message = "Unable to open tcs for the following textures:";
                    message += "\n\t:- " + texture.Name;
                    message += "This could be because you haven't got latest on the exported zip file, the parent txd file, or the exported parent txd zip file.";
                    this.MessageService.Show(message, System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);

                    this.MessageService.Show(String.Format("Unable to open tcs for the texture {0} as it doesn't appear to exist. If the texture has been promoted this isn't currently supported.", texture.Name), System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                }
            }
        }

        private void EditTextures(List<ITexture> textures, ref List<String> filenames)
        {
            List<String> tcsFilenames = new List<String>();
            List<String> missingFiles = new List<String>();
            foreach (ITexture texture in textures)
            {
                String filename = String.Empty;
                filename = RSG.Model.Asset.Util.TextureUtil.GetTcsFilenameForTexture(texture, this.ConfigServiceImport.GameConfig);
                if (!String.IsNullOrEmpty(filename))
                {
                    tcsFilenames.Add(filename);                    
                }
                else
                {
                    missingFiles.Add(texture.Name);
                }
            }
            if (missingFiles.Count > 0)
            {
                String message = "Unable to open tcs for the following textures:";
                foreach (String missingFile in missingFiles)
                {
                    message += "\n\t:- " + missingFile;
                }
                message += "\nNote: If the texture has been promoted this isn't currently supported.";
                this.MessageService.Show(message, System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
            }
            if (tcsFilenames.Count > 0) 
            {
                filenames.AddRange(tcsFilenames);
                this.WorkbenchOpenService.OpenFileWithService(this.MetadataOpenService, tcsFilenames);
            }
        }

        #endregion // ICommand Implementation
    } // OpenMapSectionCommand
} // LevelBrowser.Commands
