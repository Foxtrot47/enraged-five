﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Report.Reports.GameStatsReports.Tests;
using RSG.Model.Statistics.Captures;
using RSG.Base.Editor.Command;

namespace Workbench.AddIn.GameStatistics.Reports.Automated.ViewModels
{
    /// <summary>
    /// 
    /// </summary>
    public class DrawListSmokeTestViewModel : SmokeTestViewModelBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public IList<DrawListResult> LatestResults
        {
            get { return m_latestResults; }
            set
            {
                SetPropertyValue(value, () => this.LatestResults,
                          new PropertySetDelegate(delegate(object newValue) { m_latestResults = (IList<DrawListResult>)newValue; }));
            }
        }
        private IList<DrawListResult> m_latestResults;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public DrawListSmokeTestViewModel(DrawListSmokeTest smokeTest, string testName)
            : base(smokeTest, testName)
        {
            LatestResults = smokeTest.LatestStats.Where(item => item.Key == testName).Select(item => item.Value).FirstOrDefault();
        }
        #endregion // Constructor(s)
    } // DrawListSmokeTestViewModel
}
