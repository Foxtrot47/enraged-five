﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using RSG.Base.Math;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using RSG.Model.Common.Map;

namespace Workbench.AddIn.MapStatistics.Overlays
{
    /// <summary>
    /// 
    /// </summary>
    public enum EntityDynamicType
    {
        Both,
        Dynamic,
        NonDynamic,
    }

    /// <summary>
    /// 
    /// </summary>
    public enum EntityType
    {
        Both,
        DrawableEntity,
        NonDrawableEntity,
    }

    /// <summary>
    /// 
    /// </summary>
    public enum LodDistanceType
    {
        Hd,
        Lod,
        SLod1,
    }

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(ExtensionPoints.LodDistanceStatisticsOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    class LodDistanceStatisticsOverlays : StatisticOverlayBase
    {
        #region Constants
        private const string c_name = "Lod Distance";
        private const string c_description = "Shows the lod distances for the different object group in a map section and it's prop group.";
        #endregion // Constants

        #region Members
        private EntityDynamicType m_dynamicType;
        private EntityType m_entityType;
        private LodDistanceType m_lodDistanceType;
        #endregion // Members

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public LodDistanceType LodDistanceChoice
        {
            get { return m_lodDistanceType; }
            set
            {
                SetPropertyValue(value, m_lodDistanceType, () => LodDistanceChoice,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_lodDistanceType = (LodDistanceType)newValue;
                            OnRenderOptionChanged();
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public EntityType InstanceTypeChoice
        {
            get { return m_entityType; }
            set
            {
                SetPropertyValue(value, m_entityType, () => InstanceTypeChoice,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_entityType = (EntityType)newValue;
                            OnRenderOptionChanged();
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public EntityDynamicType DynamicTypeChoice
        {
            get { return m_dynamicType; }
            set
            {
                SetPropertyValue(value, m_dynamicType, () => DynamicTypeChoice,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_dynamicType = (EntityDynamicType)newValue;
                            OnRenderOptionChanged();
                        }
                ));
            }
        }
        #endregion // Properties

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public LodDistanceStatisticsOverlays()
            : base(c_name, c_description)
        {
        }
        #endregion // Constructor

        #region Overrides
        /// <summary>
        /// Gets the control that should be shown in the overlay details panel
        /// </summary>
        public override System.Windows.Controls.Control GetOverlayControl()
        {
            return new LodDistanceOverlayDetailsView();
        }
        #endregion // Overrides

        #region Private Functions
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="brush"></param>
        /// <param name="colour"></param>
        /// <param name="value"></param>
        protected override void CreateGeometryForSection(IMapSection section, Color outlineColour, Color fillColour, double value)
        {
            IList<Vector2f> points = section.VectorMapPoints;
            Viewport2DShape newGeometry = new Viewport2DShape(etCoordSpace.World, "", points.ToArray(), outlineColour, 1, fillColour);
            Viewport2DCircle newRadius = new Viewport2DCircle(etCoordSpace.World,
                                String.Empty, newGeometry.Center,
                                (float)value, outlineColour, 2);

            newGeometry.PickData = section;
            newGeometry.UserText = String.Format("{0:0m}", value);
            this.Geometry.Add(newGeometry);
            this.Geometry.Add(newRadius);
        }

        /// <summary>
        /// Gets the statistic value for the given map section and corresponding prop group
        /// </summary>
        protected override double GetStatisticValue(IMapSection section, IMapSection propgroup)
        {
            double stat = 0;
            if (section != null)
            {
                double sectionStat = GetStatisticValue(section);
                stat = System.Math.Max(sectionStat, stat);
            }
            if (propgroup != null)
            {
                double sectionStat = GetStatisticValue(propgroup);
                stat = System.Math.Max(sectionStat, stat);
            }

            return stat;
        }

        /// <summary>
        /// Gets the statistic value for the given map section
        /// </summary>
        private double GetStatisticValue(IMapSection section)
        {
            StatLodLevel lodLevel = StatLodLevel.Hd;
            switch (LodDistanceChoice)
            {
                case LodDistanceType.Lod:
                    lodLevel = StatLodLevel.Lod;
                    break;
                case LodDistanceType.SLod1:
                    lodLevel = StatLodLevel.SLod1;
                    break;
            }
            StatEntityType type = StatEntityType.Total;
            switch (DynamicTypeChoice)
            {
                case EntityDynamicType.Dynamic:
                    type = StatEntityType.Dynamic;
                    break;
                case EntityDynamicType.NonDynamic:
                    type = StatEntityType.NonDynamic;
                    break;
            }
            StatGroup group = StatGroup.AllNonInteriorEntities;
            switch (InstanceTypeChoice)
            {
                case EntityType.DrawableEntity:
                    group = StatGroup.DrawableEntities;
                    break;
                case EntityType.NonDrawableEntity:
                    group = StatGroup.NonDrawableEntities;
                    break;
            }

            RSG.Model.Common.Map.IMapSectionStatistic stats = section.AggregatedStats[new StatKey(group, lodLevel, type)];
            return stats.LodDistance;
        }
        #endregion // Private Functions
    } // LodDistanceStatisticsOverlays
} // Workbench.AddIn.MapStatistics.Overlays
