﻿using System;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.IO;
using System.Xml;
using System.Windows.Data;
using RSG.Metadata.Model;
using Editor.AddIn;
using Editor.AddIn.Services;
using Editor.Workbench.AddIn;
using Editor.Workbench.AddIn.Services;
using Editor.Workbench.AddIn.UI.Layout;
using MetadataEditor.AddIn.View;

namespace MetadataEditor.AddIn.TextEditor
{

    /// <summary>
    /// XML Text Editor view model class.
    /// </summary>
    [ExportExtension(MetadataEditor.AddIn.ExtensionPoints.MetadataDocument,
        typeof(IMetadataDocument))]
    [ExportExtension(MetadataEditor.AddIn.TextEditor.CompositionPoints.TextEditor,
        typeof(XmlTextEditor))]
    [Document(Name = XmlTextEditor.DOCUMENT_NAME)]
    class XmlTextEditor : 
        MetadataDocumentBase, 
        IPartImportsSatisfiedNotification
    {
        #region Constants
        public const String DOCUMENT_NAME = "XmlTextEditor";
        #endregion // Constants

        #region MEF Imports
        [ImportExtension(MetadataEditor.AddIn.CompositionPoints.Controller,
            typeof(IController))]
        private IController Controller { get; set; }
        #endregion // MEF Imports
        
        #region Properties and Associated Member Data
        /// <summary>
        /// 
        /// </summary>
#warning DHM FIX ME should not be writeable everywhere.
        public ICSharpCode.AvalonEdit.Document.TextDocument TextDocument
        {
            get { return m_TextDocument; }
            set
            {
                if (value == m_TextDocument)
                    return;
                m_TextDocument = value;
                OnPropertyChanged("TextDocument");
            }
        }
        private ICSharpCode.AvalonEdit.Document.TextDocument m_TextDocument;

        /// <summary>
        /// 
        /// </summary>
        public override IMetadataViewModel Model
        {
            get { return m_Model; }
        }
        private IMetadataViewModel m_Model;
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public XmlTextEditor()
        {
        }
        
        /// <summary>
        /// IMetadataViewModel constructor.
        /// </summary>
        /// <param name="filename"></param>
        public XmlTextEditor(IMetadataViewModel model)
        {
            this.Name = String.Format("XmlTextEditor_{0}",
                Math.Abs(model.Filename.GetHashCode()).ToString());
            this.Title = Path.GetFileName(model.Filename);
            this.m_Model = model;
        }
        #endregion // Constructor(s)

        #region IPartImportsSatisfiedNotification Methods
        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {

        }
        #endregion // IPartImportsSatisfiedNotification Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public override IDocument CreateDocument(String filename)
        {
            // Lookup our model in our Controller; the owner of the models.
            if (this.Controller.MetadataModels.ContainsKey(filename))
                return (new XmlTextEditor(this.Controller.MetadataModels[filename]));
            //else if (this.Controller.DefinitionDictionary.ContainsKey(filename))
            //    return (new XmlTextEditor(this.Controller.DefinitionDictionary[filename]));
            else
                throw new NotImplementedException();
        }
    }

} // MetadataEditor.AddIn.TextEditor
