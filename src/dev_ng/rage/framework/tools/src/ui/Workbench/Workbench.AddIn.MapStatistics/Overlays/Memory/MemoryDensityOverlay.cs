﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common.Map;

namespace Workbench.AddIn.MapStatistics.Overlays.Memory
{
    /// <summary>
    /// Overlay for showing memory density on a per platform/file type basis.
    /// </summary>
    [ExportExtension(ExtensionPoints.DensityStatisticsOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class MemoryDensityOverlay : MemoryOverlayBase
    {
        #region Constants
        private const String c_name = "Memory Density (per platform)";
        private const String c_description = "Shows an overlay of memory densities per map section and platform/file type.";
        #endregion

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public MemoryDensityOverlay()
            : base(c_name, c_description)
        {
        }
        #endregion // Constructor(s)

        #region MemoryOverlayBase Overrides
        /// <summary>
        /// Retrieves the value to display for a particular section.
        /// </summary>
        /// <param name="section"></param>
        /// <param name="platform"></param>
        /// <param name="stat"></param>
        /// <param name="fileType"></param>
        /// <returns></returns>
        protected override double GetValueForSection(IMapSection section, RSG.Platform.Platform platform, MemoryStatType stat, RSG.Platform.FileType fileType)
        {
            double value = 0.0;

            if (section.PlatformStats.ContainsKey(platform) && section.Area > 0.0f)
            {
                if (section.PlatformStats[platform].FileTypeSizes.ContainsKey(fileType))
                {
                    if (stat == MemoryStatType.PhysicalSize)
                    {
                        value = section.PlatformStats[platform].FileTypeSizes[fileType].PhysicalSize / section.Area;
                    }
                    else if (stat == MemoryStatType.VirtualSize)
                    {
                        value = section.PlatformStats[platform].FileTypeSizes[fileType].VirtualSize / section.Area;
                    }
                }
            }

            return value;
        }

        /// <summary>
        /// Gets the total value for a particular section.
        /// </summary>
        /// <param name="section"></param>
        /// <param name="platform"></param>
        /// <param name="stat"></param>
        /// <returns></returns>
        protected override double GetTotalValueForSection(IMapSection section, RSG.Platform.Platform platform, MemoryStatType stat)
        {
            double value = 0.0;

            if (section.PlatformStats.ContainsKey(platform) && section.Area > 0.0f)
            {
                if (stat == MemoryStatType.PhysicalSize)
                {
                    value = section.PlatformStats[platform].TotalPhysicalSize / section.Area;
                }
                else if (stat == MemoryStatType.VirtualSize)
                {
                    value = section.PlatformStats[platform].TotalVirtualSize / section.Area;
                }
            }

            return value;
        }

        /// <summary>
        /// Retrieves the text to add as a part of the viewport geometry.
        /// </summary>
        /// <param name="section"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        protected override string GetUserTextForSection(IMapSection section, double value)
        {
            return String.Format("{0}\n{1}/m^2", section.Name, StatisticsUtil.SizeToString((ulong)value));
        }
        #endregion // MemoryOverlayBase Overrides
    } // MemoryDensityOverlay
}
