﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using RSG.Model.Common;
using RSG.Base.Windows.DragDrop;
using RSG.Base.Windows.DragDrop.Utilities;

namespace ContentBrowser.ViewModel.GridViewModels
{
    public class GridViewModelBase : ModelBase, IGridViewModel, IDragSource
    {
        #region Members

        private IAsset m_assetModel;
        private Boolean m_isSelected = false;

        #endregion // Members

        #region Properties

        /// <summary>
        /// A IAsset reference to the model the view model
        /// is representing
        /// </summary>
        public IAsset Model
        {
            get { return m_assetModel; }
            set
            {
                if (m_assetModel == value)
                    return;

                SetPropertyValue(value, () => Model,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_assetModel = (IAsset)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Selection property for this ViewModel.
        /// </summary>
        public Boolean IsSelected
        {
            get { return m_isSelected; }
            set
            {
                if (m_isSelected == value)
                    return;

                SetPropertyValue(value, () => IsSelected,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_isSelected = (Boolean)newValue;
                        }
                ));

                this.OnSelectionChanged(!value, value);
            }
        }

        /// <summary>
        /// The main name property which by default
        /// will be shown as the header for a browser item
        /// </summary>
        public virtual String Name
        {
            get { return this.Model.Name; }
        }

        /// <summary>
        /// A collection of all the commands that are attached to this view model
        /// </summary>
        public ObservableCollection<Workbench.AddIn.Commands.IWorkbenchCommand> Commands
        {
            get;
            set;
        }

        /// <summary>
        /// The default command for this asset. This can be null but if not will be the command executed
        /// when the user double clicks and the command will be in bold in the right click menu.
        /// </summary>
        public Workbench.AddIn.Commands.IWorkbenchCommand DefaultCommand
        {
            get;
            set;
        }

        #endregion // Properties

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public GridViewModelBase(IAsset model)
        {
            this.Model = model;
        }

        #endregion // Constructor

        #region Virtual Functions

        /// <summary>
        /// This gets called whenever the selection state for this view model
        /// has changed
        /// </summary>
        protected virtual void OnSelectionChanged(Boolean oldValue, Boolean newValue)
        {
        }

        /// <summary>
        /// Gets called when a drag operation is started on a UI Element bound to this 
        /// </summary>
        public virtual Object StartDrag(DragInfo dragInfo)
        {
            dragInfo.Effects = DragDropEffects.Move;
            return this.Model;
        }

        #endregion // Virtual Functions
    } // GridViewModelBase
} // ContentBrowser.ViewModel.GridViewModels
