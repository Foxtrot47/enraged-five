﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Workbench.AddIn.TXDParentiser.UI.Views
{
    /// <summary>
    /// Interaction logic for AddDictionaryDialog.xaml
    /// </summary>
    public partial class AddDictionaryDialog : Window, INotifyPropertyChanged
    {
        #region Property change events

        /// <summary>
        /// Property changed event fired when the any of the properties change
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Create the OnPropertyChanged method to raise the event
        /// </summary>
        /// <param name="name"></param>
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        #endregion // Property change events

        public String DictionaryName
        {
            get { return (String)GetValue(DictionaryNameProperty); }
            set { SetValue(DictionaryNameProperty, value); OnPropertyChanged("DictionaryName"); }
        }
        static DependencyProperty DictionaryNameProperty = DependencyProperty.Register("DictionaryName", 
            typeof(String), typeof(AddDictionaryDialog));

        public Boolean IsNameValid
        {
            get { return m_isNameValid; }
            set { m_isNameValid = value; OnPropertyChanged("IsNameValid"); }
        }
        private Boolean m_isNameValid;

        public String ValidationMesssage
        {
            get { return m_validationMessage; }
            set { m_validationMessage = value; OnPropertyChanged("ValidationMesssage"); }
        }
        private String m_validationMessage;

        public TXDParentiserViewModel ViewModel
        {
            get { return m_viewModel; }
            set { m_viewModel = value; }
        }
        private TXDParentiserViewModel m_viewModel;

        public AddDictionaryDialog()
        {
            InitializeComponent();

            this.DictionaryTextBox.Focus();
            ValidateDictionaryName();
        }

        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void DictionaryName_TextChanged(Object sender, TextChangedEventArgs e)
        {
            ValidateDictionaryName();
        }

        private void ValidateDictionaryName()
        {
            if (this.DictionaryName == null)
            {
                this.ValidationMesssage = "Invalid name: Name has to be more than 0 characters long!";
            }
            else
            {
                if (this.DictionaryName.Length > 0)
                {
                    if (this.DictionaryName.Length <= 30)
                    {
                        if (!this.ViewModel.GlobalRoot.Model.ContainsTextureDictionary(this.DictionaryName, true))
                        {
                            // make sure that the dictionary is alphanumeric
                            Boolean IsAlphaNumeric = Regex.IsMatch(this.DictionaryName, "^[a-zA-Z0-9]*$");
                            if (IsAlphaNumeric)
                            {
                                this.IsNameValid = true;
                                this.ValidationMesssage = "";
                                return;
                            }
                            else
                            {
                                this.ValidationMesssage = "Invalid name: Name has to be made up of only alpha-numeric characters!";
                            }
                        }
                        else
                        {
                            this.ValidationMesssage = "Invalid name: Name has to be unique!";
                        }
                    }
                    else
                    {
                        this.ValidationMesssage = "Invalid name: Name cannot be more than 30 characters long!";
                    }
                }
                else
                {
                    this.ValidationMesssage = "Invalid name: Name has to be more than 0 characters long!";
                }
            }

            this.IsNameValid = false;
        }

        private void Window_KeyDown(Object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (this.IsNameValid == true)
                {
                    DialogResult = true;
                }
            }
        }
    }
}
