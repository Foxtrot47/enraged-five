﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Windows;

namespace Workbench.AddIn.UI.Menu
{

    /// <summary>
    /// 
    /// </summary>
    //[Export(Workbench.AddIn.ExtensionPoints.Styles, typeof(ResourceDictionary))]
    public partial class ContextMenuView : ResourceDictionary
    {
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ContextMenuView()
        {
            InitializeComponent();
        }
        #endregion // Constructor(s)
    }

} // Workbench.AddIn.UI.Menu namespace
