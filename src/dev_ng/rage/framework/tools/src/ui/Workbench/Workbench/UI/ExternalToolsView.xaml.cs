﻿using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Interop;
using RSG.Base.Win32;

namespace Workbench.UI
{
    /// <summary>
    /// Interaction logic for ExternalToolsView.xaml
    /// </summary>
    public partial class ExternalToolsView : Window
    {
        #region Constants
        /// <summary>
        /// Devstar Help URL.
        /// </summary>
        private const String URL_HELP = "https://devstar.rockstargames.com/wiki/index.php/Workbench#External_Tools";
        #endregion // Constants

        #region Static Member Data
        static int m_newToolId = 1;
        #endregion // Static Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ExternalToolsView()
        {
            InitializeComponent();
        }
        #endregion // Constructor(s)

        #region Event Handlers
        /// <summary>
        /// Add new external tool.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExternalTool_Add(Object sender, RoutedEventArgs e)
        {
            ExternalToolCollectionViewModel vm = (this.DataContext as ExternalToolCollectionViewModel);
            Model.ExternalTool tool = new Model.ExternalTool(
                String.Format("[New Tool{0}]", m_newToolId++), String.Empty, String.Empty, String.Empty, false, false);

            vm.Tools.Add(new ExternalToolViewModel(tool));
        }

        /// <summary>
        /// Remove selected tool.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExternelTool_Delete(Object sender, RoutedEventArgs e)
        {
            ExternalToolCollectionViewModel vm = (this.DataContext as ExternalToolCollectionViewModel);
            vm.Tools.Remove(vm.SelectedTool);
        }

        /// <summary>
        /// Move selected tool up one position.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExternalTool_MoveUp(Object sender, RoutedEventArgs e)
        {
            ExternalToolCollectionViewModel vm = (this.DataContext as ExternalToolCollectionViewModel);
            int index = vm.Tools.IndexOf(vm.SelectedTool);
            vm.Tools.Move(index, index - 1);
            vm.SelectedTool = vm.SelectedTool;
        }
        
        /// <summary>
        /// Move selected tool down one position.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExternalTool_MoveDown(Object sender, RoutedEventArgs e)
        {
            ExternalToolCollectionViewModel vm = (this.DataContext as ExternalToolCollectionViewModel);
            int index = vm.Tools.IndexOf(vm.SelectedTool);
            vm.Tools.Move(index, index + 1);
            vm.SelectedTool = vm.SelectedTool;
        }

        /// <summary>
        /// Commit changes to external tools.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExternalTools_Commit(Object sender, RoutedEventArgs e)
        {
            ExternalToolCollectionViewModel vm = (this.DataContext as ExternalToolCollectionViewModel);
            vm.Save();

            DialogResult = true;
            Close();
        }

        /// <summary>
        /// Cancel changes to external tools.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExternalTools_Cancel(Object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        #region Dialog Help Button and Handler
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        /// See http://stackoverflow.com/questions/1009983/help-button.
        /// 
        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);

            IntPtr hwnd = new System.Windows.Interop.WindowInteropHelper(this).Handle;
            uint styles = API.GetWindowLong(hwnd, API.GWL_STYLE);
            styles &= 0xFFFFFFFF ^ (API.WS_MINIMIZEBOX | API.WS_MAXIMIZEBOX);
            API.SetWindowLong(hwnd, API.GWL_STYLE, styles);
            styles = API.GetWindowLong(hwnd, API.GWL_EXSTYLE);
            styles |= API.WS_EX_CONTEXTHELP;
            API.SetWindowLong(hwnd, API.GWL_EXSTYLE, styles);
            API.SetWindowPos(hwnd, IntPtr.Zero, 0, 0, 0, 0, API.SWP_NOMOVE | API.SWP_NOSIZE | API.SWP_NOZORDER | API.SWP_FRAMECHANGED);
            ((HwndSource)PresentationSource.FromVisual(this)).AddHook(HelpHook);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hwnd"></param>
        /// <param name="msg"></param>
        /// <param name="wParam"></param>
        /// <param name="lParam"></param>
        /// <param name="handled"></param>
        /// <returns></returns>
        private IntPtr HelpHook(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            if (API.WM_SYSCOMMAND == msg && ((int)wParam & 0xFFF0) == API.SC_CONTEXTHELP)
            {
                API.ShellExecute(IntPtr.Zero, "open", URL_HELP, String.Empty, String.Empty, 0);
                handled = true;
            }
            return IntPtr.Zero;
        }
        #endregion // Dialog Help Button and Handler
        #endregion // Event Handlers
    }

} // Workbench.UI namespace
