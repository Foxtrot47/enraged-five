﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Xml;
using System.Diagnostics;
using Microsoft.Win32;
using Workbench.AddIn;
using Workbench.AddIn.UI.Layout;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Base.Configuration;
using RSG.Base.Configuration.Automation;
using RSG.Pipeline.Automation.Common;
using System.Collections;
using Workbench.AddIn.Services;
using System.Windows.Controls.Primitives;

namespace MetadataEditor.AddIn.KinoEditor.Cutscene_Batch_Exporter
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class CutsceneBatchExporterToolWindowView : DocumentBase<CutsceneBatchExportToolWindowViewModel>
    {
        #region Constants
        public static readonly Guid GUID = new Guid("074223DD-1D83-4C93-BEA7-97D9B8EA7108");
        const string DEFAULT_ENTRY = "Blank";
        #endregion // Constants

        //const string MOTIONBUILDER_EXE = "C:\\Program Files\\Autodesk\\MotionBuilder 2010\\bin\\x64\\motionbuilder.exe";

        public CutsceneBatchExporterToolWindowView(string filename, ITaskProgressService taskService) :
            base(CutsceneBatchExportToolWindowViewModel.TOOLWINDOW_NAME, new CutsceneBatchExportToolWindowViewModel(taskService, filename))
        {
            InitializeComponent();
            this.ID = GUID;
        }

        private CutsceneBatchExportToolWindowViewModel GetViewModel(object sender)
        {
            CutsceneBatchExportToolWindowViewModel vm = ((sender as ButtonBase).DataContext as CutsceneBatchExportToolWindowViewModel);

            Debug.Assert(vm != null);

            return vm;
        }

        #region Button Clicks

        private void Run_Button_Click(object sender, EventArgs e)
        {
            GetViewModel(sender).CreateAndExecutePythonScripts();
        }

        private void Flatten_Button_Click(object sender, EventArgs e)
        {
            GetViewModel(sender).CreatePythonScripts();
        }

        private void Help_Button_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process pExe = new System.Diagnostics.Process();
            pExe.StartInfo.FileName = "https://devstar.rockstargames.com/wiki/index.php/CutSceneBatchExporter";
            pExe.Start();
        }

        private void Jobs_Button_Click(object sender, EventArgs e)
        {
            GetViewModel(sender).SendJobs();
        }

        private void ToggleButton_Click(object sender, RoutedEventArgs e)
        {
            var toggle = sender as System.Windows.Controls.Primitives.ToggleButton;
            GetViewModel(sender).ToggleVisibility(toggle.IsChecked.GetValueOrDefault());
        }

        #endregion

        private void Alternate_Selection_Click(object sender, RoutedEventArgs e)
        {
            var toggle = sender as System.Windows.Controls.Primitives.ToggleButton;
            GetViewModel(sender).ToggleSelection();
        }
    }

}
