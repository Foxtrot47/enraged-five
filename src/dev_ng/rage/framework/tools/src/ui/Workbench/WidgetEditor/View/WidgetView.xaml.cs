﻿using System;
using System.Collections.Generic;
using System.Windows;
using Workbench.AddIn;
using WidgetEditor.ViewModel;
using WidgetEditor.Document;

namespace WidgetEditor.View
{
    /// <summary>
    /// Interaction logic for WidgetView.xaml
    /// </summary>
    public partial class WidgetView : WidgetDocumentBase
    {
        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public override WidgetDocumentType DocumentType
        {
            get { return (WidgetDocumentType.Default); }
        }

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Default constructor
        /// </summary>
        public WidgetView() 
            : base( "Test name", new WidgetDocumentViewModel() )
        {
            InitializeComponent();
            this.ID = new Guid( CompositionPoints.WidgetEditor );
            this.Name = "Test name";
        }

        public WidgetView(string name, WidgetDocumentViewModel vm)
            : base( name, vm )
        {
            InitializeComponent();
            this.ID = new Guid( CompositionPoints.WidgetEditor );
        }


        #endregion // Constructor(s)

    }

} // MetadataEditor.AddIn.MetaEditor namespace
