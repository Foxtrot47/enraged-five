﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Windows.Controls.WpfViewport2D;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using RSG.Model.Map.ViewModel;
using RSG.Base.Math;

namespace Workbench.AddIn.MapBrowser.Overlays
{
    //[ExportExtension("Workbench.AddIn.MapBrowser.OverlayGroups.MiscellaneousGroup", typeof(ViewportOverlay))]
    //public class ContainerOutlineOverlay : ViewportOverlay
    //{
    //    #region MEF Imports

    //    /// <summary>
    //    /// Map Browser
    //    /// </summary>
    //    [ImportExtension(Map.AddIn.Services.CompositionPoints.MapBrowser, typeof(Map.AddIn.Services.IMapBrowser))]
    //    Lazy<Map.AddIn.Services.IMapBrowser> MapBrowser
    //    {
    //        get;
    //        set;
    //    }

    //    #endregion // MEF Imports

    //    #region Constructors

    //    /// <summary>
    //    /// Default Constructor
    //    /// </summary>
    //    public ContainerOutlineOverlay()
    //    {
    //        this.Name = "Container Outline";
    //        this.Description = "Shows the indiviual containers for the map, where each main first tier area is a different colour.";
    //    }

    //    #endregion // Constructors

    //    #region Overrides

    //    /// <summary>
    //    /// Gets called when the user wants to view this overlay
    //    /// </summary>
    //    public override void Activated()
    //    {
    //        if (MapBrowser.Value.LevelGeometry != null && MapBrowser.Value.SelectedLevel != null)
    //        {
    //            Dictionary<MapAreaViewModel, System.Windows.Media.Color> topLevelAreas = new Dictionary<MapAreaViewModel, System.Windows.Media.Color>();

    //            System.Windows.Media.Color[] colours = {
    //                                                       System.Windows.Media.Color.FromArgb(255, 127, 0, 0),
    //                                                       System.Windows.Media.Color.FromArgb(255, 225, 198, 86),
    //                                                       System.Windows.Media.Color.FromArgb(255, 0, 127, 0),
    //                                                   };

    //            int index = 0;
    //            foreach (var area in MapBrowser.Value.SelectedLevel.ContainerChildren)
    //            {
    //                if (area.Parent == MapBrowser.Value.SelectedLevel && area is MapAreaViewModel)
    //                {
    //                    if (!topLevelAreas.ContainsKey(area as MapAreaViewModel))
    //                    {
    //                        if (index > colours.Length - 1)
    //                        {
    //                            index = colours.Length - 1;
    //                        }
    //                        topLevelAreas.Add(area as MapAreaViewModel, colours[index++]);
    //                    }
    //                }
    //            }

    //            System.Windows.Media.Brush brush = System.Windows.Media.Brushes.Black;
    //            RSG.Base.Collections.ObservableCollection<Viewport2DGeometry> geometry = new RSG.Base.Collections.ObservableCollection<Viewport2DGeometry>();
    //            foreach (KeyValuePair<MapSectionViewModel, List<Vector2f>> kvp in MapBrowser.Value.LevelGeometry)
    //            {
    //                System.Windows.Media.Color colour = System.Windows.Media.Colors.White;
    //                foreach (KeyValuePair<MapAreaViewModel, System.Windows.Media.Color> area in topLevelAreas)
    //                {
    //                    if (ViewModelUtilities.ContainsComponent(area.Key, kvp.Key, true))
    //                    {
    //                        colour = area.Value;
    //                        break;
    //                    }
    //                }

    //                Viewport2DShape newGeometry = new Viewport2DShape(etCoordSpace.World, "Filled shape for section", kvp.Value.ToArray(), brush, 1, colour);
    //                newGeometry.UserText = kvp.Key.Name;
    //                newGeometry.PickData = kvp.Key;
    //                geometry.Add(newGeometry);
    //            }

    //            this.Geometry = geometry;
    //        }
    //    }

    //    /// <summary>
    //    /// Gets called when the user turns off this overlay
    //    /// </summary>
    //    public override void Deactivated()
    //    {
    //        this.Geometry = new RSG.Base.Collections.ObservableCollection<Viewport2DGeometry>();
    //    }

    //    #endregion // Overrides
    //}

    //[ExportExtension("Workbench.AddIn.MapBrowser.OverlayGroups.TextureDictionaryGroup", typeof(ViewportOverlay))]
    //public class TextureDictionaryOverlayOverlay : ViewportOverlay
    //{
    //    #region MEF Imports

    //    /// <summary>
    //    /// Map Browser
    //    /// </summary>
    //    [ImportExtension(Map.AddIn.Services.CompositionPoints.MapBrowser, typeof(Map.AddIn.Services.IMapBrowser))]
    //    Lazy<Map.AddIn.Services.IMapBrowser> MapBrowser
    //    {
    //        get;
    //        set;
    //    }

    //    #endregion // MEF Imports

    //    #region Constructors

    //    /// <summary>
    //    /// Default Constructor
    //    /// </summary>
    //    public TextureDictionaryOverlayOverlay()
    //    {
    //        this.Name = "TXD Regions";
    //        this.Description = "Shows the region of any selected texture dictionaries.";
    //        this.Geometry = new RSG.Base.Collections.ObservableCollection<Viewport2DGeometry>();
    //    }

    //    #endregion // Constructors

    //    #region Overrides

    //    /// <summary>
    //    /// Gets called when the user wants to view this overlay
    //    /// </summary>
    //    public override void Activated()
    //    {
    //        this.MapBrowser.Value.SelectedItems.CollectionChanged += SelectedItems_CollectionChanged;
    //        if (this.MapBrowser.Value.SelectedItems != null)
    //        {
    //            List<TextureDictionaryViewModel> dictionaries = new List<TextureDictionaryViewModel>();
    //            foreach (var selection in this.MapBrowser.Value.SelectedItems)
    //            {
    //                if (selection is TextureDictionaryViewModel)
    //                {
    //                    dictionaries.Add(selection as TextureDictionaryViewModel);
    //                }
    //            }
    //            this.CreateGeometryFromDictionaries(dictionaries);
    //        }
    //    }
        
    //    /// <summary>
    //    /// Gets called when the user turns off this overlay
    //    /// </summary>
    //    public override void Deactivated()
    //    {
    //        this.MapBrowser.Value.SelectedItems.CollectionChanged -= SelectedItems_CollectionChanged;
    //        this.Geometry.Clear();
    //    }

    //    #endregion // Overrides

    //    #region Event Handlering

    //    void SelectedItems_CollectionChanged(Object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
    //    {
    //        List<TextureDictionaryViewModel> dictionaries = new List<TextureDictionaryViewModel>();
    //        if (this.MapBrowser.Value.SelectedItems != null)
    //        {
    //            foreach (var selection in this.MapBrowser.Value.SelectedItems)
    //            {
    //                if (selection is TextureDictionaryViewModel)
    //                {
    //                    dictionaries.Add(selection as TextureDictionaryViewModel);
    //                }
    //            }
    //            this.CreateGeometryFromDictionaries(dictionaries);
    //        }
    //    }

    //    #endregion // Event Handlering

    //    #region Private Functions

    //    private void CreateGeometryFromDictionaries(List<TextureDictionaryViewModel> dictionaries)
    //    {
    //        this.Geometry.Clear();
    //        Dictionary<String, List<BoundingBox3f>> boundingBoxes = new Dictionary<String, List<BoundingBox3f>>();
    //        foreach (TextureDictionaryViewModel dictionary in dictionaries)
    //        {
    //            String txdName = dictionary.Model.Name.ToLower();
    //            boundingBoxes.Add(txdName, new List<BoundingBox3f>());
    //            foreach (MapInstanceViewModel instance in ((dictionary.Parent as TextureDictionarySetViewModel).Parent as MapSectionViewModel).Instances)
    //            {
    //                if (instance.Model.ResolvedDefinition != null &&
    //                    instance.Model.ResolvedDefinition.Attributes.ContainsKey(RSG.SceneXml.AttrNames.OBJ_TXD) &&
    //                    (instance.Model.ResolvedDefinition.Attributes[RSG.SceneXml.AttrNames.OBJ_TXD] as String).ToLower() == txdName)
    //                {
    //                    boundingBoxes[txdName].Add(instance.Model.WorldBoundingBox);
    //                }
    //            }
    //        }

    //        foreach (List<BoundingBox3f> boxes in boundingBoxes.Values)
    //        {
    //            BoundingBox3f totalBoundary = new BoundingBox3f();
    //            foreach (BoundingBox3f box in boxes)
    //            {
    //                totalBoundary.Expand(box);
    //            }

    //            Vector2f[] points = new Vector2f[]
    //            {
    //                new Vector2f(totalBoundary.Min.X, totalBoundary.Min.Y),
    //                new Vector2f(totalBoundary.Min.X, totalBoundary.Max.Y),
    //                new Vector2f(totalBoundary.Max.X, totalBoundary.Max.Y),
    //                new Vector2f(totalBoundary.Max.X, totalBoundary.Min.Y)                    
    //            };

    //            Viewport2DShape newGeometry = new Viewport2DShape(etCoordSpace.World, "Filled shape for section", points, System.Windows.Media.Brushes.Black, 1, System.Windows.Media.Colors.White);
    //            this.Geometry.Add(newGeometry);
    //        }
    //    }

    //    #endregion // Private Functions
    //}
}
