﻿using System;
using System.ComponentModel.Composition;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn.Commands;

namespace ContentBrowser.AddIn
{
    /// <summary>
    /// A command container is responsible for getting together
    /// all of the commands for a certain asset type.
    /// </summary>
    public interface IAssetCommandContainer : IPartImportsSatisfiedNotification
    {
        #region Properties

        /// <summary>
        /// The type that this command will be attached to.
        /// </summary>
        Type AssetType { get; }

        /// <summary>
        /// The list of workbench commands that are 
        /// attached to the asset type.
        /// </summary>
        IEnumerable<IWorkbenchCommand> Commands { get; }
        
        #endregion // Properties
    } // IResourceCommandContainer
} // ContentBrowser.AddIn
