﻿using System;
using System.Windows.Controls;
using System.Windows;
using System.IO;

namespace Workbench.AddIn.ReportBrowser
{
    /// <summary>
    // Webrowser.Source is not a Dependency property, a property is attached and when it is changed it sets the browser.Source - pah...
    // http://stackoverflow.com/questions/263551/databind-the-source-property-of-the-webbrowser-in-wpf
    /// </summary>
    public static class WebBrowserUtility
    {
        public static readonly DependencyProperty BindableSourceProperty = DependencyProperty.RegisterAttached("BindableSource", typeof(string), typeof(WebBrowserUtility), new UIPropertyMetadata(null, BindableSourcePropertyChanged));

        public static string GetBindableSource(DependencyObject obj)
        {
            return (string)obj.GetValue(BindableSourceProperty);
        }

        public static void SetBindableSource(DependencyObject obj, string value)
        {
            obj.SetValue(BindableSourceProperty, value);
        }

        public static void BindableSourcePropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            WebBrowser browser = o as WebBrowser;
            if (browser != null)
            {
                string uri = e.NewValue as string;

                if (uri != null)
                {
                    browser.Source = new Uri(uri);
                }
            }
        }

        public static readonly DependencyProperty BindableStreamProperty = DependencyProperty.RegisterAttached("BindableStream", typeof(Stream), typeof(WebBrowserUtility), new UIPropertyMetadata(null, BindableStreamPropertyChanged));

        public static Stream GetBindableStream(DependencyObject obj)
        {
            return (Stream)obj.GetValue(BindableStreamProperty);
        }

        public static void SetBindableStream(DependencyObject obj, Stream value)
        {
            obj.SetValue(BindableStreamProperty, value);
        }

        public static void BindableStreamPropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            WebBrowser browser = o as WebBrowser;
            if (browser != null)
            {
                Stream stream = e.NewValue as Stream;

                if (stream != null)
                {
                    browser.NavigateToStream(stream);
                }
            }
        }
    } // WebBrowserUtility
} // Workbench.AddIn.ReportBrowser
