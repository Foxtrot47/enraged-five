﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Report;

namespace Workbench.AddIn.MapStatistics.Reports
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Report.AddIn.ExtensionPoints.MapStatsReport, typeof(IReport))]
    public class ReferenceComparisonReport : RSG.Model.Report.Reports.Map.ReferenceComparisonReport
    {
    } // ReferenceComparisonReport
} // Workbench.AddIn.MapStatistics.Reports
