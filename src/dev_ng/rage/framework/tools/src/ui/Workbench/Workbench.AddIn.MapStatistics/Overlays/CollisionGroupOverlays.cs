﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Data;
using RSG.Base.Collections;
using RSG.Base.Math;
using RSG.Base.Tasks;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using RSG.Base.Windows.Dialogs;
using RSG.Model.Common.Extensions;
using RSG.Model.Common.Map;
using RSG.Model.Common.Util;
using Viewport.AddIn;
using MapViewport.AddIn;

namespace Workbench.AddIn.MapStatistics.Overlays
{
    /// <summary>
    /// 
    /// </summary>
    public class EntityViewModel
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public string Container
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public string DefinitionName
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public float X
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public float Y
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public float Z
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsExpanded
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="instance"></param>
        public EntityViewModel(IEntity entity)
        {
            this.Container = entity.Parent.Name;
            this.Name = entity.Name;
            this.DefinitionName = (entity.ReferencedArchetype != null ? entity.ReferencedArchetype.Name : null);
            this.X = entity.Position.X;
            this.Y = entity.Position.Y;
            this.Z = entity.Position.Z;
        }
        #endregion // Constructor
    } // EntityViewModel

    /// <summary>
    /// 
    /// </summary>
    public class CollisionGroupViewModel
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public string Container
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="instance"></param>
        public CollisionGroupViewModel(string container, string group)
        {
            this.Container = container;
            this.Name = group;
        }
        #endregion // Constructor
    } // CollisionGroupViewModel

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(ExtensionPoints.CollisionGroupOverlayGroup, typeof(Viewport.AddIn.IViewportOverlay))]
    public class CollisionGroupRegionOverlay : LevelDependentViewportOverlay
    {
        #region Private member fields

        private List<IMapSection> m_selectedMaps;

        #endregion

        #region Constants
        private const String c_name = "Collision Group Regions";
        private const String c_description = "Shows the bound extent of the selected collision group. (A bounding box containing every instance that uses the selected collision group).";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<string> CollisionGroups
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<IEntity> Entities
        {
            get;
            set;
        }
        
        /// <summary>
        /// 
        /// </summary>
        public ListCollectionView GroupedEntities
        {
            get { return m_groupedEntities; }
            set
            {
                SetPropertyValue(value, () => GroupedEntities,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_groupedEntities = (ListCollectionView)newValue;
                        }
                ));
            }
        }
        private ListCollectionView m_groupedEntities;

        /// <summary>
        /// 
        /// </summary>
        public ListCollectionView GroupedGroups
        {
            get { return m_groupedGroups; }
            set
            {
                SetPropertyValue(value, () => GroupedGroups,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_groupedGroups = (ListCollectionView)newValue;
                        }
                ));
            }
        }
        private ListCollectionView m_groupedGroups;

        /// <summary>
        /// 
        /// </summary>
        public string SelectedCollisionGroup
        {
            get { return m_selectedGroup; }
            set
            {
                SetPropertyValue(value, () => SelectedCollisionGroup,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_selectedGroup = (string)newValue;
                            OnSelectedGroupChanged();
                        }
                ));
            }
        }
        private string m_selectedGroup;

        private Dictionary<string, List<string>> m_sortedCollisionGroups = new Dictionary<string, List<string>>();
        #endregion

        #region MEF Imports
        /// <summary>
        /// A reference to view model for the map viewport
        /// </summary>
        [ImportExtension(Viewport.AddIn.CompositionPoints.MapViewport, typeof(Viewport.AddIn.IMapViewport))]
        public Viewport.AddIn.IMapViewport MapViewport { get; set; }

        /// <summary>
        /// A reference to the content browser.
        /// </summary>
        [ImportExtension(ContentBrowser.AddIn.CompositionPoints.ContentBrowser, typeof(ContentBrowser.AddIn.IContentBrowser))]
        public ContentBrowser.AddIn.IContentBrowser Content { get; set; }
        #endregion // MEF Imports

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public CollisionGroupRegionOverlay()
            : base(c_name, c_description)
        {
            this.CollisionGroups = new ObservableCollection<string>();
            this.Entities = new ObservableCollection<IEntity>();
            m_selectedMaps = new List<IMapSection>();
        }

        #endregion // Constructor

        #region Overrides
        /// <summary>
        /// Gets called when the user wants to view this overlay
        /// </summary>
        public override void Activated()
        {
            base.Activated();

            Content.GridSelectionChanged += new ContentBrowser.AddIn.GridSelectionChangedEventHandler(Content_GridSelectionChanged);

            GetGridSelection();
            RefreshData();
        }

        /// <summary>
        /// Gets called when the user turns off this overlay
        /// </summary>
        public override void Deactivated()
        {
            base.Deactivated();
            Content.GridSelectionChanged -= Content_GridSelectionChanged;
        }

        /// <summary>
        /// Gets the control that should be shown in the overlay details panel
        /// </summary>
        public override System.Windows.Controls.Control GetOverlayControl()
        {
            return new CollisionGroupRegionView();
        }
        #endregion // Overrides

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="progress"></param>
        protected void EnsureDataLoaded(TaskContext context, IProgress<TaskProgress> progress)
        {
            var sections =  context.Argument as List<IMapSection>;

            float increment = 1.0f / sections.Count();

            // Load all the map section data
            foreach (IMapSection section in sections)
            {
                context.Token.ThrowIfCancellationRequested();

                // Update the progress information
                string message = String.Format("Loading {0}", section.Name);
                progress.Report(new TaskProgress(increment, true, message));

                section.LoadAllStats();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void OnSelectedGroupChanged()
        {
            MapViewport.ClearSelectedGeometry();
            this.Entities.BeginUpdate();
            this.Entities.Clear();

            IMapHierarchy hierarchy = LevelBrowserProxy.Value.SelectedLevel.GetMapHierarchy();
            if (hierarchy != null)
            {
                foreach (IMapSection section in m_selectedMaps)
                {
                    foreach (IEntity entity in section.ChildEntities.Where(item => item.ReferencedArchetype != null))
                    {
                        ISimpleMapArchetype archetype = entity.ReferencedArchetype as ISimpleMapArchetype;
                        if (archetype != null && String.Compare(archetype.CollisionGroup.ToLower(), this.SelectedCollisionGroup) == 0)
                        {
                            this.Entities.Add(entity);
                        }
                    }
                }
            }
            this.Entities.EndUpdate();

            ISet<string> containers = CreateGeometry();
            CreateGroups(containers);
        }

        /// <summary>
        /// 
        /// </summary>
        private ISet<string> CreateGeometry()
        {
            ISet<string> containers = new HashSet<string>();

            this.Geometry.BeginUpdate();
            this.Geometry.Clear();

            if (this.Entities.Count > 0)
            {
                BoundingBox2f fullBoundingBox = new BoundingBox2f();
                Vector2f location = new Vector2f();
                ISet<string> drawingContainers = new HashSet<string>();
                foreach (IEntity instance in Entities)
                {
                    location.X = instance.Position.X;
                    location.Y = instance.Position.Y;
                    fullBoundingBox.Expand(location);

                    string containerName = instance.Parent.Name;
                    containerName = containerName.Replace("_props", "");

                    drawingContainers.Add(containerName);
                    containers.Add(instance.Parent.Name);
                }

                Vector2f[] points = new Vector2f[]
                {
                    fullBoundingBox.Max,
                    new RSG.Base.Math.Vector2f(fullBoundingBox.Max.X, fullBoundingBox.Min.Y),
                    fullBoundingBox.Min,
                    new RSG.Base.Math.Vector2f(fullBoundingBox.Min.X, fullBoundingBox.Max.Y),
                };

                Viewport2DShape newGeometry = new Viewport2DShape(etCoordSpace.World, String.Empty, points, System.Windows.Media.Colors.Black, 2);
                this.Geometry.Add(newGeometry);

                IMapHierarchy hierarchy = LevelBrowserProxy.Value.SelectedLevel.GetMapHierarchy();
                
                foreach (string container in drawingContainers)
                {
                    IMapSection section = hierarchy.AllSections.FirstOrDefault(item => String.Compare(item.Name, container, true) == 0);
                    if (section != null && section.VectorMapPoints != null)
                    {
                        Viewport2DShape containerGeometry = new Viewport2DShape(etCoordSpace.World, String.Empty, section.VectorMapPoints.ToArray(), System.Windows.Media.Colors.Black, 2);
                        containerGeometry.UserText = container;
                        this.Geometry.Add(containerGeometry);
                    }
                }
            }
            this.Geometry.EndUpdate();
            return containers;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="containers"></param>
        private void CreateGroups(ISet<string> containers)
        {
            IDictionary<string, IList<EntityViewModel>> sortedViewModels = new SortedList<string, IList<EntityViewModel>>();
            foreach (IEntity entity in this.Entities)
            {
                if (!sortedViewModels.ContainsKey(entity.Parent.Name))
                {
                    sortedViewModels.Add(entity.Parent.Name, new List<EntityViewModel>());
                }
                sortedViewModels[entity.Parent.Name].Add(new EntityViewModel(entity));
            }

            List<EntityViewModel> viewModels = new List<EntityViewModel>();
            if (this.Entities.Count > 4000)
            {
                viewModels.AddRange(sortedViewModels.Select(item => item.Value[0]));
            }
            else
            {
                viewModels.AddRange(sortedViewModels.SelectMany(item => item.Value));
            }

            this.GroupedEntities = new ListCollectionView(viewModels);
            this.GroupedEntities.GroupDescriptions.Add(new PropertyGroupDescription("Container"));

            IDictionary<string, IList<string>> sortedGroupViewModels = new SortedList<string, IList<string>>();
            foreach (string container in containers)
            {
                List<string> groups = new List<string>();
                if (m_sortedCollisionGroups.TryGetValue(container, out groups))
                {
                    sortedGroupViewModels.Add(container, groups);
                }
            }

            List<CollisionGroupViewModel> groupViewModels = new List<CollisionGroupViewModel>();
            
            foreach (KeyValuePair<string, IList<string>> vm in sortedGroupViewModels)
            {
                foreach (string group in vm.Value)
                {
                    groupViewModels.Add(new CollisionGroupViewModel(vm.Key, group));
                }
            }

            this.GroupedGroups = new ListCollectionView(groupViewModels);
            this.GroupedGroups.GroupDescriptions.Add(new PropertyGroupDescription("Container"));
        }


        /// <summary>
        /// Refresh the data.
        /// </summary>
        private void RefreshData()
        {
            CancellationTokenSource cts = new CancellationTokenSource();
            TaskContext context = new TaskContext(cts.Token, m_selectedMaps);
            ITask loadDataTask = new ActionTask("Load Data", (ctx, progress) => EnsureDataLoaded((TaskContext)ctx, progress));

            TaskExecutionDialog dialog = new TaskExecutionDialog();
            TaskExecutionViewModel vm = new TaskExecutionViewModel("Generating Overlay", loadDataTask, cts, context);
            vm.AutoCloseOnCompletion = true;
            dialog.DataContext = vm;
            Nullable<bool> selectionResult = dialog.ShowDialog();
            if (false == selectionResult)
            {
                return;
            }

            this.CollisionGroups.Clear();
            m_sortedCollisionGroups.Clear();
            ISet<string> groups = new SortedSet<string>();

            foreach (IMapSection section in m_selectedMaps)
            {
                if (!m_sortedCollisionGroups.ContainsKey(section.Name))
                {
                    m_sortedCollisionGroups.Add(section.Name, new List<string>());
                }

                foreach (IEntity entity in section.ChildEntities.Where(item => item.ReferencedArchetype != null))
                {
                    if (!(entity.Parent is IMapSection))
                    {
                        continue;
                    }

                    //
                    ISimpleMapArchetype archetype = entity.ReferencedArchetype as ISimpleMapArchetype;
                    if (archetype != null)
                    {
                        if (!groups.Contains(archetype.CollisionGroup.ToLower()))
                        {
                            groups.Add(archetype.CollisionGroup.ToLower());
                        }

                        if (!m_sortedCollisionGroups[section.Name].Contains(archetype.CollisionGroup.ToLower()))
                        {
                            m_sortedCollisionGroups[section.Name].Add(archetype.CollisionGroup.ToLower());
                        }
                    }
                }
            }

            this.CollisionGroups.AddRange(groups);

            if (this.CollisionGroups.Any())
            {
                SelectedCollisionGroup = this.CollisionGroups[0];
            }
        }

        /// <summary>
        /// Get the selected items from the content browser.
        /// </summary>
        private void GetGridSelection()
        {
            m_selectedMaps.Clear();
            foreach (var item in Content.SelectedGridItems)
            {
                if (item is IMapSection)
                {
                    m_selectedMaps.Add(item as IMapSection);
                }
            }

            if (m_selectedMaps.Count == 0)
            {
                m_selectedMaps.AddRange(LevelBrowserProxy.Value.SelectedLevel.GetMapHierarchy().AllSections);
            }
        }

        /// <summary>
        /// Event handler for grid selection changes.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="args">Arguments.</param>
        private void Content_GridSelectionChanged(object sender, ContentBrowser.AddIn.GridSelectionChangedEventArgs args)
        {
            GetGridSelection();
            RefreshData();
        }
        #endregion
    } // TotalCostOverlay
}
