﻿using System;
using System.IO;
using RSG.Model.Vehicle;
using ContentBrowser.ViewModel.GridViewModels;
using RSG.Model.Common;

namespace ContentBrowser.ViewModel.VehicleViewModels
{

    /// <summary>
    /// 
    /// </summary>
    internal class VehicleGridViewModel : GridViewModelBase
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// 
        /// </summary>
        public String RenderFilename
        {
            get
            {
                if (!String.IsNullOrEmpty(AssetsDir))
                {
                    String filename = Path.Combine(AssetsDir, "vehicles", "renders", String.Format("{0}.jpg", this.Name));
                    if (File.Exists(filename))
                    {
                        return filename;
                    }
                }
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private String AssetsDir
        {
            get;
            set;
        }
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public VehicleGridViewModel(IVehicle vehicle, string assetsDir)
            : base(vehicle)
        {
            AssetsDir = assetsDir;
        }
        #endregion // Constructor(s)
    }

} // ContentBrowser.ViewModel.VehicleViewModels namespace
