﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Windows.Helpers;
using RSG.Model.Common;

namespace Workbench.AddIn.Services.Model
{
    /// <summary>
    /// 
    /// </summary>
    public interface IPlatformBrowser
    {
        #region Events
        /// <summary>
        /// Fired when the selected platform changes
        /// </summary>
        event PlatformChangedEventHandler PlatformChanged;
        #endregion // Events

        #region Properties
        /// <summary>
        /// Platforms that are currently on display
        /// </summary>
        IPlatformCollection PlatformCollection { get; }

        /// <summary>
        /// The currently selected item in the platform collection
        /// </summary>
        RSG.Platform.Platform SelectedPlatform { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Updates the platforms.
        /// </summary>
        void UpdatePlatforms(DataSource source);
        #endregion // Methods
    } // IPlatformBrowser

    /// <summary>
    /// Platform changed
    /// </summary>
    public class PlatformChangedEventArgs : ItemChangedEventArgs<RSG.Platform.Platform?>
    {
        public PlatformChangedEventArgs(RSG.Platform.Platform? oldPlatform, RSG.Platform.Platform? newPlatform)
            : base(oldPlatform, newPlatform)
        {
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="args"></param>
    public delegate void PlatformChangedEventHandler(Object sender, PlatformChangedEventArgs args);
}
