﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn;
using Workbench.AddIn.Services;
using Workbench.AddIn.UI.Layout;

namespace MetadataEditor.AddIn.DefinitionEditor.UI.Layout
{
    [ExportExtension(Workbench.AddIn.ExtensionPoints.ToolWindowProxy, typeof(IToolWindowProxy))]
    public class DefinitionExplorerProxy : IToolWindowProxy
    {
        #region MEF Imports
        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService,
            typeof(IConfigurationService))]
        private IConfigurationService ConfigurationService { get; set; }

        [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager,
            typeof(ILayoutManager))]
        private Lazy<ILayoutManager> LayoutManager { get; set; }

        [ImportExtension(MetadataEditor.AddIn.CompositionPoints.StructureDictionary,
            typeof(IStructureDictionary))]
        private IStructureDictionary StructureDictionary { get; set; }

        [ImportExtension(Workbench.AddIn.CompositionPoints.PropertyInspectorService,
            typeof(IPropertyInspector))]
        private Lazy<IPropertyInspector> PropertyInspector { get; set; }

        #endregion // MEF Imports

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public DefinitionExplorerProxy()
        {
            this.Name = DefinitionExplorerToolWindowViewModel.TOOLWINDOW_NAME;
            this.Header = DefinitionExplorerToolWindowViewModel.TOOLWINDOW_TITLE;
        }

        #endregion // Constructor

        #region Public Functions

        /// <summary>
        /// A abstract function that is overriden by the plugin to create the
        /// actual instance of the tool window and pass it back as a out paramter
        /// </summary>
        protected override Boolean CreateToolWindow(out IToolWindowBase toolWindow)
        {
            toolWindow = new DefinitionExplorerToolWindowView(ConfigurationService, this.LayoutManager.Value, this.StructureDictionary, this.PropertyInspector.Value);

            return true;
        }

        #endregion // Public Functions
    }
}
