﻿using System;
using System.ComponentModel.Composition;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.Services.File;

namespace WidgetEditor.UI
{
    [ExportExtension(ExtensionPoints.ToolWindowProxy, typeof(IToolWindowProxy))]
    class WidgetEditorWindowProxy : IToolWindowProxy
    {
        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public WidgetEditorWindowProxy()
        {
            this.Name = Resources.Strings.Name;
            this.Header = Resources.Strings.Title;
            this.DelayCreation = false;
        }

        #endregion // Constructor

        #region Public Functions

        /// <summary>
        /// Tool window proxys that are used as the TODO
        /// </summary>
       
        /// <summary>
        /// 
        /// </summary>
        [ImportExtension( Workbench.AddIn.CompositionPoints.LayoutManager,
            typeof( ILayoutManager ) )]
        private ILayoutManager LayoutManager { get; set; }














        /// <summary>
        /// A abstract function that is overriden by the plugin to create the
        /// actual instance of the tool window and pass it back as a out paramter
        /// </summary>
        protected override Boolean CreateToolWindow(out IToolWindowBase toolWindow)
        {
            toolWindow = new WidgetEditorWindowView( LayoutManager);

            return true;
        }

        #endregion // Public Functions
    }
}
