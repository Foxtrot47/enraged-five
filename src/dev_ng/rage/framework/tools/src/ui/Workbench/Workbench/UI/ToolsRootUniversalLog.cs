﻿using Workbench.AddIn;
using Workbench.AddIn.Services;
using System.IO;

namespace Workbench.UI
{
    /// <summary>
    /// 
    /// </summary>
    //[ExportExtension(Workbench.AddIn.ExtensionPoints.LoggingOutput, typeof(ILoggingService))]
    public class ToolsRootUniversalLog : IExternalLoggingService
    {
        #region Imports
        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(IConfigurationService))]
        private IConfigurationService Config { get; set;}
        #endregion

        #region Properties
        public  string UIName
        {
            get { return "All Tools Root"; }
        }

        public bool IsDefault
        {
            get { return false; }
        }

        public string LogDirectory
        { 
            get { return Path.Combine(Config.GameConfig.ToolsLogDir); }
        }

        public string LogFilenamePattern
        {
            get { return "*.ulog"; }
        }
        #endregion
    } // WorkbenchUniversalLog
} // Workbench.UI
