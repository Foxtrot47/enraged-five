﻿using System;
using System.Windows;
using RSG.Base.Windows.Dialogs;

namespace Workbench.AddIn.Services
{

    /// <summary>
    /// User-Interface Service.
    /// </summary>
    public interface IUserInterfaceService : IService
    {
        #region Message Box Methods
        /// <summary>
        /// Show message box with simple message (OK button, no icon).
        /// </summary>
        /// <param name="messageBoxText"></param>
        /// <returns></returns>
        MessageBoxResult Show(String messageBoxText);

        /// <summary>
        /// Show message box with message and configurable buttons.
        /// </summary>
        /// <param name="messageBoxText"></param>
        /// <param name="buttons"></param>
        /// <returns></returns>
        MessageBoxResult Show(String messageBoxText, MessageBoxButton buttons);

        /// <summary>
        /// Show message box with message, configurable buttons and icon.
        /// </summary>
        /// <param name="messageBoxText"></param>
        /// <param name="buttons"></param>
        /// <param name="image"></param>
        /// <returns></returns>
        MessageBoxResult Show(String messageBoxText, MessageBoxButton buttons, MessageBoxImage image);
        #endregion // Message Box Methods

        #region Login Methods
        /// <summary>
        /// Prompts the user for his login details, using the supplied predicate to
        /// determine whether the login was successful.
        /// </summary>
        /// <param name="loginPredicate"></param>
        /// <returns>true if the user connected, false if the user hit cancel.</returns>
        bool Login(Func<LoginDetails, LoginResults> loginPredicate);

        /// <summary>
        /// Prompts the user for his login details, using the supplied predicate to
        /// determine whether the login was successful.
        /// </summary>
        /// <param name="loginPredicate"></param>
        /// <param name="title"></param>
        /// <param name="message"></param>
        /// <returns>true if the user connected, false if the user hit cancel.</returns>
        bool Login(Func<LoginDetails, LoginResults> loginPredicate, string title, string message);
        #endregion // Login Methods
    }

} // Workbench.AddIn.Services namespace
