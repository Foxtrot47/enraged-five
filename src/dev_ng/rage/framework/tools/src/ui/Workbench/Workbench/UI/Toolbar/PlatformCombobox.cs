﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn;
using Workbench.AddIn.Commands;
using System.ComponentModel.Composition;
using RSG.Base.Logging;
using ContentBrowser.AddIn;
using RSG.Model.Common;
using RSG.Model.Asset.Platform;
using Workbench.AddIn.Services;
using Workbench.AddIn.Services.Model;

namespace Workbench.UI.Toolbar
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Workbench.AddIn.ExtensionPoints.DataModeToolbar, typeof(IWorkbenchCommand))]
    [ExportExtension(Workbench.AddIn.CompositionPoints.PlatformBrowser, typeof(IPlatformBrowser))]
    class PlatformCombobox : WorkbenchCommandCombobox, IPlatformBrowser
    {
        #region Constants
        /// <summary>
        /// Unique GUID for the platform combo box
        /// </summary>
        public static readonly Guid GUID = new Guid("A50ED3F0-D446-4ACD-913D-5A33B3EF0BB4");
        #endregion // Constants

        #region MEF Imports
        /// <summary>
        /// 
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService, typeof(Workbench.AddIn.Services.IConfigurationService))]
        Lazy<Workbench.AddIn.Services.IConfigurationService> Config { get; set; }

        /// <summary>
        /// MEF import for content browser settings
        /// </summary>
        [ImportExtension(ContentBrowser.AddIn.CompositionPoints.ContentBrowserSettings, typeof(ContentBrowser.AddIn.IContentBrowserSettings))]
        ContentBrowser.AddIn.IContentBrowserSettings Settings { get; set; }
        #endregion // MEF Imports

        #region Events
        /// <summary>
        /// Gets called when the current platform changes
        /// </summary>
        public event PlatformChangedEventHandler PlatformChanged;
        #endregion

        #region Properties
        /// <summary>
        /// Platforms that are currently on display
        /// </summary>
        public IPlatformCollection PlatformCollection
        {
            get;
            protected set;
        }

        /// <summary>
        /// The currently selected item in the platform collection
        /// </summary>
        public RSG.Platform.Platform SelectedPlatform
        {
            get
            {
                PlatformComboboxItem label = SelectedItem as PlatformComboboxItem;
                return (label != null ? label.Platform : PlatformCollection.First());
            }
        }

        /// <summary>
        /// Factory to use for creating the platforms
        /// </summary>
        private IPlatformFactory PlatformFactory
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public PlatformCombobox()
            : base()
        {
            this.ID = GUID;
            this.RelativeToolBarID = PlatformLabel.GUID;
            this.Direction = Direction.After;
            this.MinWidth = 70;
            PlatformFactory = new PlatformFactory();
        }
        #endregion // Constructor(s)

        #region IPlatformBrowser Implementation
        /// <summary>
        /// 
        /// </summary>
        public void UpdatePlatforms(DataSource source)
        {
            List<IWorkbenchCommand> allItems = new List<IWorkbenchCommand>();

            PlatformCollection = PlatformFactory.CreateCollection(source, Config.Value.Config);
            if (PlatformCollection != null)
            {
                allItems.AddRange(PlatformCollection.Select(item => new PlatformComboboxItem(item)));
            }

            this.Items = allItems;

            // Try and restore the previously selected platform
            RSG.Platform.Platform previousPlatform = RSG.Platform.Platform.PS3;

            if (!String.IsNullOrEmpty(Settings.Platform))
            {
                try
                {
                    previousPlatform = (RSG.Platform.Platform)Enum.Parse(typeof(RSG.Platform.Platform), Settings.Platform);
                }
                catch (Exception)
                {
                    Log.Log__Warning("Previously selected platform '{0}' isn't a valid platform.", Settings.Platform);
                }
            }

            IWorkbenchCommand previousComboItem = allItems.Cast<PlatformComboboxItem>().FirstOrDefault(item => item.Platform == previousPlatform);
            if (previousComboItem == null)
            {
                Log.Log__Warning("Previously selected platform '{0}' is no longer available.", previousPlatform);
                previousComboItem = allItems.FirstOrDefault();
            }
            this.SelectedItem = previousComboItem;
        }
        #endregion // IPlatformBrowser Implementation

        #region WorkbenchCommandCombobox Overrides
        /// <summary>
        /// Callback for whenever the selected item changes
        /// </summary>
        /// <param name="oldValue"></param>
        /// <param name="newValue"></param>
        protected override void PostSelectedItemChanged(IWorkbenchCommand oldValue, IWorkbenchCommand newValue)
        {
            PlatformComboboxItem oldLabel = oldValue as PlatformComboboxItem;
            PlatformComboboxItem newLabel = newValue as PlatformComboboxItem;

            RSG.Platform.Platform? oldPlatform = (oldLabel != null ? (RSG.Platform.Platform?)oldLabel.Platform : null);
            RSG.Platform.Platform? newPlatform = (newLabel != null ? (RSG.Platform.Platform?)newLabel.Platform : null);

            if (newPlatform != null)
            {
                Settings.Platform = newPlatform.ToString();
                Settings.Apply();
            }

            if (PlatformChanged != null)
            {
                PlatformChanged(this, new PlatformChangedEventArgs(oldPlatform, newPlatform));
            }
        }
        #endregion
    } // ContentBrowserToolbarPlatformCombobox
} // ContentBrowser.UI.Toolbar
