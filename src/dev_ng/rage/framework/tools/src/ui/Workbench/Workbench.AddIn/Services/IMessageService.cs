﻿using System;
using System.Windows;

namespace Workbench.AddIn.Services
{

    /// <summary>
    /// 
    /// </summary>
    [Obsolete("Use ICoreServiceProvider.UserInterfaceService instead.")]
    public interface IMessageService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="messageBoxText"></param>
        /// <returns></returns>
        MessageBoxResult Show(String messageBoxText);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="messageBoxText"></param>
        /// <param name="buttons"></param>
        /// <returns></returns>
        MessageBoxResult Show(String messageBoxText, MessageBoxButton buttons);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="messageBoxText"></param>
        /// <param name="buttons"></param>
        /// <param name="image"></param>
        /// <returns></returns>
        MessageBoxResult Show(String messageBoxText, MessageBoxButton buttons, MessageBoxImage image);
    }

} // Workbench.AddIn.Services namespace
