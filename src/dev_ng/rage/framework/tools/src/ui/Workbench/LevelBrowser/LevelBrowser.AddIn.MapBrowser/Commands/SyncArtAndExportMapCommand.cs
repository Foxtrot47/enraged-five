﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using RSG.Model.Common.Map;
using Workbench.AddIn;
using Workbench.AddIn.Commands;

namespace LevelBrowser.AddIn.MapBrowser.Commands
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Extensions.MapSectionCommands, typeof(IWorkbenchCommand))]
    class SyncArtAndExportMapCommand : ExportMapCommand
    {
        #region Constants
        public static readonly String GUID = "926CB4C8-5B61-49C3-9724-619AF1105304";
        #endregion // Constants

        #region Constructor
        /// <summary>
        /// Default constructor.
        /// </summary>
        public SyncArtAndExportMapCommand()
            : base()
        {
            this.Header = "_Sync Source Art and Export Locally...";
            this.ID = new Guid(GUID);
            this.RelativeID = new Guid(OpenMapSectionCommand.GUID);
        }
        #endregion // Constructor

        #region Overridden Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sections"></param>
        /// <returns></returns>
        protected override IEnumerable<string> GetFilesToSync(IEnumerable<IMapSection> sections)
        {
            // Add source art files
            foreach (IMapSection section in sections)
            {
                yield return Path.GetFullPath(section.DCCSourceFilename + ".max");
                yield return Path.GetFullPath(section.DCCSourceFilename + ".maxc");
            }

            // Add the files the base class needs
            foreach (string file in base.GetFilesToSync(sections))
            {
                yield return file;
            }
        }
        #endregion // Virtual Methods

    } // SyncArtAndExportMapCommand
}
