﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using RSG.Base.Collections;
using ragCore;
using ragWidgets;
using System.Diagnostics;
using RSG.Base.Editor.Command;

namespace WidgetEditor.ViewModel
{
    

    public class WidgetToggleViewModel : WidgetViewModel
    {
        #region Properties
        private bool m_Value;
        public bool Value
        {
            get { return m_Value; }
            set
            {
                m_Widget.ValueChanged -= model_ValueChanged;
                m_Widget.Checked = value;
                m_Widget.ValueChanged += model_ValueChanged;
                SetPropertyValue( value, () => this.Value,
                                new PropertySetDelegate( delegate( Object newValue ) { m_Value = (bool)newValue; } ) );
            }
        }
        #endregion

        #region Variables

        private WidgetToggle m_Widget;
        #endregion



        public WidgetToggleViewModel( WidgetToggle model )
            :base(model)
        {
            m_Widget = model;
            m_Widget.ValueChanged += model_ValueChanged;
            Value = m_Widget.Checked;
        }

        void model_ValueChanged( object sender, EventArgs e )
        {
            if ( m_Value != m_Widget.Checked )
            {
                Value = m_Widget.Checked;
            }
        }

    }
}
