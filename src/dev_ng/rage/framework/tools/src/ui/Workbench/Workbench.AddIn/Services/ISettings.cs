﻿using System;
using System.Collections.Generic;
using Workbench.AddIn.Services;

namespace Workbench.AddIn.Services
{

    /// <summary>
    /// AddIn settings interface.
    /// </summary>
    public interface ISettings : IExtension
    {
        #region Properties
        /// <summary>
        /// A descriptive string that will be shown to the user
        /// </summary>
        String Title { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Checks its current state for any invalid data.
        /// </summary>
        /// <param name="errors">The service will fill this up with one string per 
        /// error it finds in its data.</param>
        /// <returns>true if everything is fine, false if an error was found</returns>
        bool Validate(List<String> errors);

        /// <summary>
        /// Loads settings
        /// </summary>
        void Load();

        /// <summary>
        /// 
        /// </summary>
        void Apply();
        #endregion // Methods
    }

} // Workbench.AddIn.Services namespace
