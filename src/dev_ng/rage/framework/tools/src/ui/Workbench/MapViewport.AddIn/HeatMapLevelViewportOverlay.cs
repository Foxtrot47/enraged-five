﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using RSG.Model.Common.Util;
using RSG.Base.Tasks;
using RSG.Base.Windows.Dialogs;
using RSG.Model.Common.Extensions;
using RSG.Model.Common.Map;
using Workbench.AddIn;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using System.Windows.Media;
using System.Collections.Specialized;
using RSG.Base.Math;

namespace MapViewport.AddIn
{
    /// <summary>
    /// Heat map overlay. Generates heat-map style overlays (Green to Red) on a per-section (per-container) basis.
    /// </summary>
    public abstract class HeatMapLevelViewportOverlay : LevelDependentViewportOverlay
    {
        #region Private member fields

        private HeatMapStatistics m_heatMap;
        private List<IMapSection> m_availableSections;

        private bool m_loadDataUpFront;
        private bool m_drawSections;

        #endregion

        #region MEF Imports
        /// <summary>
        /// Content Browser
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LevelBrowser, typeof(Workbench.AddIn.Services.Model.ILevelBrowser))]
        Lazy<Workbench.AddIn.Services.Model.ILevelBrowser> LevelBrowserProxy { get; set; }

        /// <summary>
        /// A reference to view model for the map viewport, used to get the currently selected geometry
        /// </summary>
        [ImportExtension(Viewport.AddIn.CompositionPoints.MapViewport, typeof(Viewport.AddIn.IMapViewport))]
        Lazy<Viewport.AddIn.IMapViewport> MapViewport { get; set; }

        /// <summary>
        /// Content Browser
        /// </summary>
        [ImportExtension(ContentBrowser.AddIn.CompositionPoints.ContentBrowser, typeof(ContentBrowser.AddIn.IContentBrowser))]
        Lazy<ContentBrowser.AddIn.IContentBrowser> ContentBrowserProxy { get; set; }
        #endregion // MEF Imports

        /// <summary>
        /// The selected section's name.
        /// </summary>
        public string SelectedSectionName { get; protected set; }

        /// <summary>
        /// The heat map.
        /// </summary>
        protected HeatMapStatistics HeatMap { get { return m_heatMap; } }

        /// <summary>
        /// True if the sections should be drawn.
        /// </summary>
        public bool DrawSections
        {
            get { return m_drawSections; }
            set
            {
                SetPropertyValue(value, () => DrawSections,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_drawSections = (bool)newValue;
                            this.OnDrawSectionsChanged();
                        }
                ));
            }
        }

        /// <summary>
        /// Section geometry.
        /// </summary>
        private IDictionary<IMapSection, Viewport2DShape> SectionGeometry
        {
            get;
            set;
        }
        
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name">Name of the overlay.</param>
        /// <param name="description">Overlay description.</param>
        /// <param name="invertColour">True if the colours are to be inverted (red is good, green is bad).</param>
        /// <param name="useLogarithmicScale">True if the colours are to be on a logarithmic scale. Use if you have lots of small numbers and a handful of large numbers.</param>
        /// <param name="loadMapSectionDataOnActivation">True if the section data is to be loaded when Activated() method is called.</param>
        public HeatMapLevelViewportOverlay(string name, string description, bool invertColour, bool useLogarithmicScale, bool loadMapSectionDataOnActivation)
            : base(name, description)
        {
            SectionGeometry = new Dictionary<IMapSection, Viewport2DShape>();
            m_availableSections = new List<IMapSection>();
            m_heatMap = new HeatMapStatistics();
            m_heatMap.InverseColouring = invertColour;
            m_heatMap.UseLogScale = useLogarithmicScale;
            m_loadDataUpFront = loadMapSectionDataOnActivation;
        }

        protected virtual void OnDrawSectionsChanged()
        {
        }

        /// <summary>
        /// Fires when the overlay is first activated.
        /// </summary>
        public override void Activated()
        {
            base.Activated();
            GetMapData();
            OnMapDataGathered();
            RenderMap();
            MapViewport.Value.SelectedOverlayGeometry.CollectionChanged += SelectedOverlayGeometryChanged;
        }

        /// <summary>
        /// Fired when the overlay is deactivated.
        /// </summary>
        public override void Deactivated()
        {
            MapViewport.Value.SelectedOverlayGeometry.CollectionChanged -= SelectedOverlayGeometryChanged;
            Geometry.Clear();
            SectionGeometry.Clear();
            base.Deactivated();
        }

        /// <summary>
        /// Get the user text that will be displayed in the overlay map for each section. 
        /// </summary>
        /// <param name="section">Section.</param>
        /// <param name="sectionGeometry">Section geometry.</param>
        /// <returns>The text that will be displayed over the top of each section in the overlay.</returns>
        protected virtual string GetUserText(IMapSection section, Viewport2DShape sectionGeometry)
        {
            return String.Empty;
        }

        /// <summary>
        /// After the map data has been gathered, this method is called. 
        /// </summary>
        protected virtual void OnMapDataGathered()
        {

        }

        /// <summary>
        /// Custom code in derived classes to render alternative data to the section heat map.
        /// </summary>
        protected virtual void OnMapRendered()
        {

        }

        /// <summary>
        /// Get the statistics for the map section. This is a number that represents it's weight on the Green-Red colour scale.
        /// </summary>
        /// <param name="section">Section.</param>
        /// <param name="statistic">Statistical weight.</param>
        /// <returns>True if there was data gathered from the section.</returns>
        protected abstract bool GetStatistic(IMapSection section, out double statistic);

        /// <summary>
        /// Get the map data.
        /// </summary>
        private void GetMapData()
        {
            if (m_loadDataUpFront)
            {
                RefreshSectionData();
            }
            else
            {
                GetSelectedMapData();
            }
        }

        /// <summary>
        /// Refresh the section data.
        /// </summary>
        protected virtual void RefreshSectionData()
        {
            CancellationTokenSource cts = new CancellationTokenSource();
            LevelTaskContext context = new LevelTaskContext(cts.Token, LevelBrowserProxy.Value.SelectedLevel);
            ITask loadDataTask = new ActionTask("Load Data", (ctx, progress) => EnsureDataLoaded((LevelTaskContext)ctx, progress));

            TaskExecutionDialog dialog = new TaskExecutionDialog();
            TaskExecutionViewModel vm = new TaskExecutionViewModel("Loading Map Data", loadDataTask, cts, context);
            vm.AutoCloseOnCompletion = true;
            dialog.DataContext = vm;
            Nullable<bool> selectionResult = dialog.ShowDialog();
            if (false == selectionResult)
            {
                return;
            }
        }

        /// <summary>
        /// Get the selected section data. 
        /// </summary>
        /// <returns>True if successful.</returns>
        protected bool GetSelectedMapData()
        {
            if (ContentBrowserProxy == null || ContentBrowserProxy.Value == null)
            {
                return false;
            }

            if (ContentBrowserProxy.Value.SelectedGridItems != null && ContentBrowserProxy.Value.SelectedGridItems.Count() > 0)
            {
                foreach (var item in ContentBrowserProxy.Value.SelectedGridItems)
                {
                    if (item is IMapSection)
                    {
                        IMapSection section = item as IMapSection;
                        if (section != null)
                        {
                            double statistic = 0;
                            if (GetStatistic(section, out statistic))
                            {
                                if (!m_availableSections.Contains(section))
                                {
                                    m_availableSections.Add(section);
                                    m_heatMap.Add(section, statistic);
                                }
                            }
                        }
                    }

                }
                return true;
            }

            return false;
        }

        /// <summary>
        /// Gives the developers the ability to turn off / on the rendering of certain sections.
        /// </summary>
        /// <param name="section">Section.</param>
        /// <returns>True if the section is to be rendered.</returns>
        protected virtual bool RenderSection(IMapSection section)
        {
            return true;
        }

        /// <summary>
        /// Render the map.
        /// </summary>
        protected void RenderMap()
        {
            Geometry.Clear();
            Geometry.BeginUpdate();

            if (DrawSections)
            {
                foreach (var section in m_availableSections)
                {
                    Viewport2DShape sectionGeometry = null;

                    if (RenderSection(section))
                    {
                        // Check if we can reuse a piece of geometry for rendering this section's density
                        if (!SectionGeometry.ContainsKey(section))
                        {
                            if (section.VectorMapPoints == null)
                            {
                                continue;
                            }

                            sectionGeometry = new Viewport2DShape(etCoordSpace.World, "Filled shape for section", section.VectorMapPoints.ToArray(), Colors.Black, 1, HeatMap.GetColor(section));
                            sectionGeometry.PickData = section;

                            // Keep track of the geometry
                            SectionGeometry.Add(section, sectionGeometry);
                        }
                        else
                        {
                            sectionGeometry = SectionGeometry[section];
                        }

                        sectionGeometry.UserText = GetUserText(section, sectionGeometry);
                        sectionGeometry.FillColour = HeatMap.GetColor(section);
                        Geometry.Add(sectionGeometry);
                    }
                }
            }
            else
            {
                OnMapRendered();
            }
            
            Geometry.EndUpdate();
        }

        /// <summary>
        /// Gets called when map geometry selection changes
        /// </summary>
        private void SelectedOverlayGeometryChanged(Object sender, NotifyCollectionChangedEventArgs e)
        {
            // Display details for the first item in the selected geometry
            IMapSection selectedSection = MapViewport.Value.SelectedOverlayGeometry.FirstOrDefault() as IMapSection;

            if (selectedSection == null)
            {
                SelectedSectionName = "";
            }
            else
            {
                SelectedSectionName = selectedSection.Name;
            }
        }

        #region Private helper methods

        /// <summary>
        /// Load the section data.
        /// </summary>
        /// <param name="context">Context.</param>
        /// <param name="progress">Progress.</param>
        private void EnsureDataLoaded(LevelTaskContext context, IProgress<TaskProgress> progress)
        {
            float increment = 1.0f / context.MapHierarchy.AllSections.Count();

            // Load all the map section data
            foreach (IMapSection section in context.MapHierarchy.AllSections)
            {
                context.Token.ThrowIfCancellationRequested();

                // Update the progress information
                string message = String.Format("Loading {0}", section.Name);
                progress.Report(new TaskProgress(increment, true, message));

                if (!m_availableSections.Contains(section))
                {
                    section.RequestAllStatistics(false);
                }

                double statistic = 0;
                if (GetStatistic(section, out statistic))
                {
                    m_availableSections.Add(section);
                    m_heatMap.Add(section, statistic);
                }
            }
        }

        #endregion
    }
}
