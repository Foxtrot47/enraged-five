﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using RSG.Base.Collections;
using ragCore;
using ragWidgets;
using System.Diagnostics;
using RSG.Base.Editor.Command;

namespace WidgetEditor.ViewModel
{
    

    public class WidgetTextViewModel : WidgetViewModel
    {
        #region Properties
        private string m_Value;
        public string Value
        {
            get { return m_Value; }
            set
            {
                m_Widget.ValueChanged -= model_ValueChanged;
                m_Widget.String = value;
                m_Widget.ValueChanged += model_ValueChanged;
                SetPropertyValue( value, () => this.Value,
                                new PropertySetDelegate( delegate( Object newValue ) { m_Value = (string)newValue; } ) );
            }
        }
        #endregion

        #region Variables

        private WidgetText m_Widget;
        #endregion



        public WidgetTextViewModel( WidgetText model )
            :base(model)
        {
            m_Widget = model;
            m_Widget.ValueChanged += model_ValueChanged;
            Value = m_Widget.String;
        }

        void model_ValueChanged( object sender, EventArgs e )
        {
            if ( m_Value != m_Widget.String )
            {
                Value = m_Widget.String;
            }
        }

    }
}
