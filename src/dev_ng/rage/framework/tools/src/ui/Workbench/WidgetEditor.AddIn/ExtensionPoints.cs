﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WidgetEditor.AddIn
{
    /// <summary>
    /// 
    /// </summary>
    public static class ExtensionPoints
    {
        /// <summary>
        /// Live Editing toolbar.
        /// </summary>
        public const String LiveEditingToolbar =
            "77CC29DE-1411-4EC9-9201-9B49CA1B43FA";

        /// <summary>
        /// 
        /// </summary>
        public const String LiveEditingToolbarSeperator =
            "ADBE8079-F757-4663-9AAF-37C59C038C57";
    } // ExtensionPoints
}
