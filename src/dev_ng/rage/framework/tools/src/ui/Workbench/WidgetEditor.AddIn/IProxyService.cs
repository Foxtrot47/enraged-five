﻿using System;
using System.Collections.Generic;
using RSG.Base.Net;
using WidgetEditor.AddIn;
using System.Net;
using ragCore;

namespace WidgetEditor.AddIn
{
    /// <summary>
    /// AddIn settings interface.
    /// </summary>
    public interface IProxyService
    {
        #region Events
        /// <summary>
        /// Event for when the selected game connection has changed
        /// </summary>
        event GameConnectionChangedEventHandler GameConnectionChanged;
        #endregion // Events

        #region Properties
        /// <summary>
        /// Flag indicating whether we are connected to the RAG proxy
        /// </summary>
        bool IsProxyConnected { get; }

        /// <summary>
        /// The port that we are connected to the proxy on
        /// </summary>
        int ProxyPort { get; }

        /// <summary>
        /// Flag indicating whether we are connected to a game
        /// </summary>
        bool IsGameConnected { get; }

        /// <summary>
        /// The game we are currently connected to
        /// </summary>
        GameConnection SelectedGame { get; }

        /// <summary>
        /// The platform's friendly name of the currently game connection
        /// </summary>
        string SelectedGamePlatform { get; }

        /// <summary>
        /// The IP address of the currently selected game connection
        /// </summary>
        IPAddress SelectedGameIP { get; }

        /// <summary>
        /// Remote console connection to the RAG proxy
        /// </summary>
        cRemoteConsole Console { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        void RefreshGameConnections();
        #endregion // Methods
    } // IProxyService
}
