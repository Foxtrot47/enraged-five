﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Logging.Universal;

namespace Workbench.AddIn.Services
{
    /// <summary>
    /// 
    /// </summary>
    public interface ILoggingService
    {
        #region Properties
        string UIName { get; }

        bool IsDefault { get; }
        #endregion
    } // ILoggingService

    /// <summary>
    /// 
    /// </summary>
    public interface IExternalLoggingService : ILoggingService
    {
        #region Properties
        string LogDirectory { get; }

        string LogFilenamePattern { get; }
        #endregion
    } // IExternalLoggingService

    /// <summary>
    /// 
    /// </summary>
    public interface IInternalLoggingService : ILoggingService
    {
        #region Properties
        IUniversalLog LogObject { get; }
        #endregion
    } // IInternalLoggingService
}
