﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.GlobalTXD;

namespace Workbench.AddIn.TXDParentiser.ViewModel
{
    public interface ITextureChildViewModel
    {
        float SelectedSharedPercentage { get; set; }

        int SelectedSharedCount { get; set; }

        int SelectedSaveIfPromoted { get; set; }

        /// <summary>
        /// The parent container that conatins this object
        /// </summary>
        ITextureContainerViewModel Parent { get; }

        /// <summary>
        /// The root object for this object
        /// </summary>
        GlobalRootViewModel Root { get; }

        string StreamName { get; }

        void ExpandedTo();
    }
} // Workbench.AddIn.TXDParentiser.ViewModel
