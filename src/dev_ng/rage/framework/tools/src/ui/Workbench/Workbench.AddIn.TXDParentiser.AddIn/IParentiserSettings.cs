﻿namespace Workbench.AddIn.TXDParentiser.AddIn
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Workbench.AddIn.Services;

    public interface IParentiserSettings : ISettings
    {
        string GtxdZipName { get; set; }
        string GtxdMetaName { get; set; }
        string SourceTxdPrefix { get; set; }
    }
}
