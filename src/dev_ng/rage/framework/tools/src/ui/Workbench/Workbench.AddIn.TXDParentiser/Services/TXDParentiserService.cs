﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn.TXDParentiser.AddIn;
using System.ComponentModel.Composition;
using Workbench.AddIn.UI.Layout;
using RSG.Base.Editor;
using System.Collections.Specialized;
using System.Windows;
using Workbench.AddIn.TXDParentiser.ViewModel;
using RSG.Model.GlobalTXD;

namespace Workbench.AddIn.TXDParentiser.Services
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(Workbench.AddIn.TXDParentiser.AddIn.CompositionPoints.TxdParentiser, typeof(ITXDParentiserService))]
    public class TXDParentiserService : ITXDParentiserService, IPartImportsSatisfiedNotification, IWeakEventListener
    {
        #region MEF Imports
        /// <summary>
        /// MEF import for workbench Layout Manager component.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LayoutManager, typeof(ILayoutManager))]
        private ILayoutManager LayoutManager { get; set; }
        #endregion // MEF Imports

        #region Properties
        /// <summary>
        /// The items that are currently selected in the active TXD parentiser view
        /// </summary>
        public IEnumerable<ModelBase> SelectedItems
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Events
        /// <summary>
        /// Event that gets fired whenever the selection changes
        /// </summary>
        public event TxdSelectionChangedEventHandler SelectionChanged;
        #endregion // Events

        #region IPartImportsSatisfiedNotification Implementation
        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {
            LayoutManager.ActiveDocumentChanging += LayoutManager_ActiveDocumentChanging;
            LayoutManager.ActiveDocumentChanged += LayoutManager_ActiveDocumentChanged;
        }
        #endregion // IPartImportsSatisfiedNotification Implementation

        #region Event Handlers
        /// <summary>
        /// Callback for when the active document is about change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LayoutManager_ActiveDocumentChanging(object sender, EventArgs e)
        {
            DocumentBase<TXDParentiserViewModel> txdDocument = LayoutManager.ActiveDocument() as DocumentBase<TXDParentiserViewModel>;
            if (txdDocument != null)
            {
                CollectionChangedEventManager.RemoveListener(txdDocument.ViewModel.SelectedItems, this);
                OnSelectionChanged(new List<object>());
            }
        }

        /// <summary>
        /// Callback for when the active document has changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LayoutManager_ActiveDocumentChanged(object sender, EventArgs e)
        {
            DocumentBase<TXDParentiserViewModel> txdDocument = LayoutManager.ActiveDocument() as DocumentBase<TXDParentiserViewModel>;
            if (txdDocument != null)
            {
                CollectionChangedEventManager.AddListener(txdDocument.ViewModel.SelectedItems, this);
                OnSelectionChanged(txdDocument.ViewModel.SelectedItems);
            }
            else
            {
                OnSelectionChanged(new List<object>());
            }
        }
        #endregion // Event Handlers

        #region IWeakEventListener
        /// <summary>
        /// 
        /// </summary>
        /// <param name="managerType"></param>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
        {
            DocumentBase<TXDParentiserViewModel> txdDocument = LayoutManager.ActiveDocument() as DocumentBase<TXDParentiserViewModel>;
            if (txdDocument != null && sender == txdDocument.ViewModel.SelectedItems)
            {
                OnSelectionChanged(txdDocument.ViewModel.SelectedItems);
                return true;
            }

            return false;
        }
        #endregion // IWeakEventListener

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="newSelection"></param>
        private void OnSelectionChanged(IEnumerable<object> newSelection)
        {
            List<ModelBase> modelSelection = new List<ModelBase>();

            // Convert the newSelection from view models to plain models
            foreach (object obj in newSelection)
            {
                if (obj is GlobalTextureViewModel)
                {
                    modelSelection.Add(((GlobalTextureViewModel)obj).Model);
                }
                else if (obj is SourceTextureViewModel)
                {
                    modelSelection.Add(((SourceTextureViewModel)obj).Model);
                }
                else if (obj is GlobalTextureDictionaryViewModel)
                {
                    modelSelection.Add(((GlobalTextureDictionaryViewModel)obj).Model);
                }
                else if (obj is SourceTextureDictionaryViewModel)
                {
                    modelSelection.Add(((SourceTextureDictionaryViewModel)obj).Model);
                }
            }

            // Update the selection, and fire off the selection changed event
            IEnumerable<ModelBase> previousSelection = SelectedItems;
            SelectedItems = modelSelection;

            if (SelectionChanged != null)
            {
                SelectionChanged(this, new TxdSelectionChangedEventArgs(previousSelection, SelectedItems));
            }
        }
        #endregion // Private Methods
    } // TXDParentiserService
}
