﻿using System;
using System.Windows;
using System.ComponentModel.Composition;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn.Services;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.Services.Model;

namespace Workbench.AddIn.MapViewport.UI.Layout
{
    [Export(Workbench.AddIn.ExtensionPoints.ToolWindowProxy, typeof(IToolWindowProxy))]
    public class MapViewportWindowProxy : IToolWindowProxy
    {
        #region MEF Imports
        
        /// <summary>
        /// Tool window proxys that are used as the map viewport (can be more than one)
        /// </summary>
        [ImportManyExtension(Viewport.AddIn.ExtensionPoints.OverlayGroup, typeof(Viewport.AddIn.IViewportOverlayGroup))]
        private IEnumerable<Viewport.AddIn.IViewportOverlayGroup> OverlayGroups { get; set; }

        /// <summary>
        /// A reference to the level browser interface
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.LevelBrowser, typeof(ILevelBrowser))]
        private Lazy<ILevelBrowser> LevelBrowserProxy { get; set; }
        
        /// <summary>
        /// A reference to view model for the map viewport
        /// </summary>
        [ImportExtension(Viewport.AddIn.CompositionPoints.MapViewport, typeof(Viewport.AddIn.IMapViewport))]
        private Lazy<Viewport.AddIn.IMapViewport> MapViewportViewModel { get; set; }

        #endregion // MEF Imports

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public MapViewportWindowProxy()
        {
            this.Name = Resources.Strings.MapViewport_Name;
            this.Header = Resources.Strings.MapViewport_Title;
        }

        #endregion // Constructor

        #region Public Functions

        /// <summary>
        /// A abstract function that is overriden by the plugin to create the
        /// actual instance of the tool window and pass it back as a out paramter
        /// </summary>
        protected override Boolean CreateToolWindow(out IToolWindowBase toolWindow)
        {
            MapViewportViewModel.Value.SetOverlays(this.OverlayGroups.ToList());
            toolWindow = new MapViewportView(this.LevelBrowserProxy.Value, this.OverlayGroups.ToList(), this.MapViewportViewModel.Value as MapViewportViewModel);
            return true;
        }

        #endregion // Public Functions
    } // MapViewportWindowProxy
} // Workbench.AddIn.MapViewport.UI.Layout
