﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Workbench.AddIn.TXDParentiser.UI.Controls
{
    public class MyTabItem : 
        TabItem
    {
        private RoutedEventHandler CloseAction
        {
            get;
            set;
        }

        public MyTabItem(RoutedEventHandler closeButtonAction)
        {
            CloseAction = closeButtonAction;
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            DependencyObject d = GetTemplateChild("PART_CloseButton");

            if (d != null)
            {
                (d as Button).Click += CloseButtonClick;
            }
        }

        void CloseButtonClick(Object sender, RoutedEventArgs e)
        {
            if (this.CloseAction != null)
            {
                this.CloseAction(DataContext, e);
            }
        }

    }
}
