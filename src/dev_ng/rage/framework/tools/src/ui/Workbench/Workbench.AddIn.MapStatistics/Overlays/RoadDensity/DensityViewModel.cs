﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using System.Windows.Media;
using System.ComponentModel;

namespace Workbench.AddIn.MapStatistics.Overlays.RoadDensity
{
    /// <summary>
    /// Traffic density view model.
    /// </summary>
    public class DensityViewModel : ViewModelBase, INotifyPropertyChanged
    {
        #region Private member fields

        private bool m_isChecked;
        private string m_caption;
        private int m_value;
        private SolidColorBrush m_colour;

        #endregion

        #region Public properties

        /// <summary>
        /// Colour swatch.
        /// </summary>
        public SolidColorBrush Colour
        {
            get { return m_colour; }
            set
            {
                SetPropertyValue(value, () => this.Colour,
                    new PropertySetDelegate(delegate(object newValue) { m_colour = (SolidColorBrush)newValue; }));
            }
        }

        /// <summary>
        /// Caption.
        /// </summary>
        public string Caption
        {
            get { return m_caption; }
            set
            {
                SetPropertyValue(value, () => this.Caption,
                    new PropertySetDelegate(delegate(object newValue) { m_caption = newValue.ToString(); }));
            }
        }

        /// <summary>
        /// Traffic density value.
        /// </summary>
        public int Value
        {
            get
            {
                return m_value;
            }
            set
            {
                SetPropertyValue(value, () => this.Value,
                    new PropertySetDelegate(delegate(object newValue) { m_value = (int)newValue; }));
            }
        }

        /// <summary>
        /// True if the user has the item selected.
        /// </summary>
        public bool IsChecked
        {
            get { return m_isChecked; }
            set
            {
                SetPropertyValue(value, () => this.IsChecked,
                    new PropertySetDelegate(delegate(object newValue) { m_isChecked = (bool)newValue; }));
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="caption">Caption.</param>
        /// <param name="density">Traffic density.</param>
        /// <param name="colour">Colour.</param>
        public DensityViewModel(string caption, int density, Color colour)
        {
            IsChecked = true;
            Caption = caption;
            Value = density;
            Colour = new SolidColorBrush(colour);
        }

        #endregion
    }
}
