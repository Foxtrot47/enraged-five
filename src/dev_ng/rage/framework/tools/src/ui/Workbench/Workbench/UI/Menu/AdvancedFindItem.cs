﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Windows.Input;
using Workbench.AddIn;
using Workbench.AddIn.Commands;
using Workbench.AddIn.Services;
using Workbench.AddIn.UI.Layout;
using Workbench.AddIn.UI.Menu;
using Workbench.UI.Toolbar;

namespace Workbench.UI.Menu
{
    /// <summary>
    /// Basic Text Find menu item.
    /// </summary>
    //[ExportExtension(Workbench.AddIn.ExtensionPoints.MenuFind, typeof(IWorkbenchCommand))]
    public class AdvancedFindItem : WorkbenchMenuItemBase
    {
        #region Constants

        public static readonly Guid GUID = new Guid("5EE27831-61FE-471C-BF41-402A919C79D3");

        #endregion // Constants

        #region Constructor(s)

        /// <summary>
        /// Default constructor.
        /// </summary>
        public AdvancedFindItem()
        {
            this.Header = "_Advanced Find";
            this.ID = GUID;
            this.RelativeID = BasicFindItem.GUID;
            this.Direction = Direction.After;
            this.KeyGesture = new KeyGesture(Key.F, ModifierKeys.Control | ModifierKeys.Shift);
        }

        #endregion // Constructor(s)

        #region ICommand Implementation

        /// <summary>
        /// Gets called when deciding whether this menu items
        /// command can be executed or not
        /// </summary>
        public override Boolean CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// Gets called when executing the command for this
        /// menu item
        /// </summary>
        public override void Execute(Object parameter)
        {
        }

        #endregion // ICommand Implementation
    } // AdvancedFindItem
} // Workbench.UI.Menu
