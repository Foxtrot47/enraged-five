﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace MetadataEditor.AddIn.KinoEditor.Cutscene_Batch_Exporter
{
    /// <summary>
    /// Cut scene batch entry.
    /// </summary>
    public class BatchEntry :  INotifyPropertyChanged
    {
        #region Constructors

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="fbxfile">FBX file.</param>
        /// <param name="cutfile">Cut part file.</param>
        /// <param name="mission">Mission name.</param>
        /// <param name="exportcutfile">True if the cut file is to be exported.</param>
        public BatchEntry(string fbxfile, string cutfile, string mission, bool importcutfile, bool useSourceControlFiles, string groupName)
        {
            IsSelected = false;
            CutFile = cutfile;
            FBXFile = fbxfile;
            ImportCutFile = importcutfile;
            UseSourceControlFiles = useSourceControlFiles;
            Mission = mission;
            GroupName = groupName;
        }

        #endregion 

        #region Properties
        
        /// <summary>
        /// Cut part file.
        /// </summary>
        public string CutFile 
        {
            get
            {
                return _CutFile;
            }
            set
            {
                _CutFile = value;
                OnPropertyChanged("CutFile");
            }
        }
        string _CutFile;

        /// <summary>
        /// True if the batch entry has been selected by the user.
        /// </summary>
        public bool IsSelected
        {
            get { return _IsSelected; }
            set
            {
                _IsSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }
        private bool _IsSelected;

        /// <summary>
        /// The FBX file.
        /// </summary>
        public string FBXFile 
        {
            get
            {
                return _FBXFile;
            }
            set
            {
                _FBXFile = value;
                OnPropertyChanged("FBXFile");
            }
        }
        string _FBXFile;

        /// <summary>
        /// Mission name.
        /// </summary>
        public string Mission
        {
            get { return _Mission; }
            set
            {
                _Mission = value;
                OnPropertyChanged("Mission");
            }
        }
        string _Mission;

        /// <summary>
        /// Group name.
        /// </summary>
        public string GroupName
        {
            get { return _GroupName; }
            set
            {
                _GroupName = value;
                OnPropertyChanged("GroupName");
            }
        }
        string _GroupName;

        /// <summary>
        /// True if the files are to be concatenated.
        /// </summary>
        public bool Concat
        {
            get
            {
                return _Concat;
            }
            set
            {
                _Concat = value;
                OnPropertyChanged("Concat");
            }
        }
        bool _Concat;

        public bool ImportCutFile
        {
            get
            {
                return _ImportCutFile;
            }
            set
            {
                _ImportCutFile = value;
                OnPropertyChanged("ImportCutFile");
            }
        }
        bool _ImportCutFile;

        public bool UseSourceControlFiles
        {
            get
            {
                return _UseSourceControlFiles;
            }
            set
            {
                _UseSourceControlFiles = value;
                OnPropertyChanged("UseSourceControlFiles");
            }
        }
        bool _UseSourceControlFiles;
        #endregion 

        #region INotifyPropertyChanged Members

        /// <summary>
        /// Raised when a property on this object has a new value.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises this object's PropertyChanged event.
        /// </summary>
        /// <param name="propertyName">The property that has a new value.</param>
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName);
                handler(this, e);
            }
        }

        #endregion // INotifyPropertyChanged Members
    }
}
