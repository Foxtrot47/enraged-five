﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using RSG.Base.Win32;

namespace LevelBrowser.AddIn.MapBrowser.Commands
{
    /// <summary>
    /// Background worker task to allow the UI to continue to work smoothly during and export. Also allows us to wait for the
    /// Process to finish on a separate thread.
    /// </summary>
    internal class ExportMapBackgroundWorker
    {
        #region Internal properties

        /// <summary>
        /// True if the export was successful.
        /// </summary>
        internal bool Success
        {
            get;
            private set;
        }

        /// <summary>
        /// True if the background worker is running.
        /// </summary>
        internal bool IsRunning
        {
            get;
            private set;
        }

        /// <summary>
        /// File to execute.
        /// </summary>
        internal string Filename
        {
            get;
            private set;
        }

        /// <summary>
        /// Command line arguments.
        /// </summary>
        internal string Arguments
        {
            get;
            private set;
        }

        #endregion

        #region Public events

        /// <summary>
        /// The task has completed.
        /// </summary>
        public event EventHandler TaskComplete;

        #endregion

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="commandName">Command name.</param>
        /// <param name="arguments">Command arguments.</param>
        public ExportMapBackgroundWorker(string commandName, string arguments, string iniFile)
        {
            Filename = commandName;
            Arguments = arguments;
            m_iniFile = iniFile;

            m_worker = new BackgroundWorker();
            m_worker.DoWork += new DoWorkEventHandler(Worker_DoWork);

            m_worker.WorkerSupportsCancellation = false;
            m_worker.WorkerReportsProgress = false;
        }

        /// <summary>
        /// Start the command.
        /// </summary>
        public void Start()
        {
            string folder = Path.GetDirectoryName(m_iniFile);
            string fileName = Path.GetFileName(m_iniFile);

            m_watcher = new FileSystemWatcher(folder, fileName);
            m_watcher.Changed += new FileSystemEventHandler(Watcher_Changed);
            m_watcher.EnableRaisingEvents = true;

            IsRunning = true;
            m_worker.RunWorkerAsync(new string[] { Filename, Arguments });
        }

        /// <summary>
        /// Cancel the command.
        /// </summary>
        public void Cancel()
        {
            TidyUp();
        }

        #region Event handlers

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Watcher_Changed(object sender, FileSystemEventArgs e)
        {
            IniFile iniFile = IniFile.TryOpen(m_iniFile);

            if (!iniFile.HasBlankEntries)
            {
                MarkTaskComplete();
            }
        }

        /// <summary>
        /// Starts the external process and waits for it to finish.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            string[] commandArgs = e.Argument as string[];
            string cmd = commandArgs[0];
            string args = commandArgs[1];

            using (Process p = new Process())
            {
                p.StartInfo.FileName = cmd;
                p.StartInfo.Arguments = args;

                Success = p.Start();
                p.WaitForExit();
            }

        }

        #endregion

        #region Private helper methods

        /// <summary>
        /// Mark the task as completed.
        /// </summary>
        private void MarkTaskComplete()
        {
            TidyUp();

            if (TaskComplete != null)
            {
                TaskComplete(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Remove event handlers and reset everything to null.
        /// </summary>
        private void TidyUp()
        {
            m_watcher.Changed -= Watcher_Changed;
            m_worker.DoWork -= Worker_DoWork;

            m_watcher.Dispose();
            m_worker.Dispose();

            m_watcher = null;
            m_worker = null;

            IsRunning = false;
        }

        #endregion

        #region Private member fields

        private BackgroundWorker m_worker;
        private FileSystemWatcher m_watcher;

        private string m_iniFile;

        #endregion
    }
}
