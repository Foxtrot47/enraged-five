﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using RSG.Model.Common.Map;
using System.ComponentModel;
using RSG.Model.Common;

namespace ContentBrowser.ViewModel.MapViewModels3
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class MapArchetypeViewModel : ContainerViewModelBase, IDisposable, IWeakEventListener
    {
        #region Members
        /// <summary>
        /// Thread synchronisation helper object
        /// </summary>
        private object m_syncObject = new object();
        #endregion // Members

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        private IMapArchetype MapArchetype
        {
            get
            {
                return (IMapArchetype)Model;
            }
        }
        #endregion // Properties

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public MapArchetypeViewModel(IMapArchetype archetype)
            : base(archetype)
        {
            PropertyChangedEventManager.AddListener(archetype, this, "Textures");
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// This gets called whenever the selection state for this view model
        /// has changed
        /// </summary>
        protected override void OnSelectionChanged(Boolean oldValue, Boolean newValue)
        {
            if (IsSelected)
            {
                lock (m_syncObject)
                {
                    MapArchetype.RequestAllStatistics(true);
                }
            }

            RefreshGrid();
        }

        /// <summary>
        /// This gets called whenever the expansion state for this view model
        /// has changed
        /// </summary>
        protected override void OnExpansionChanged(Boolean oldValue, Boolean newValue)
        {
            if (IsExpanded)
            {
                lock (m_syncObject)
                {
                    MapArchetype.RequestStatistics(new StreamableStat[] { StreamableMapArchetypeStat.Shaders }, true);
                }
            }

            RefreshHierarchy();
        }
        #endregion // Overrides

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        protected void RefreshHierarchy()
        {
            this.AssetChildren.BeginUpdate();
            this.AssetChildren.Clear();
            if (IsExpanded)
            {
                if (!MapArchetype.IsStatLoaded(StreamableMapArchetypeStat.Shaders))
                {
                    this.AssetChildren.Add(new AssetDummyViewModel("loading..."));
                }
                else
                {
                    this.AssetChildren.AddRange(MapArchetype.Textures.Select(texture => ViewModelFactory.CreateViewModel(texture)));
                }
            }
            else
            {
                this.AssetChildren.Add(new AssetDummyViewModel());
            }
            this.AssetChildren.EndUpdate();
        }

        /// <summary>
        /// 
        /// </summary>
        protected void RefreshGrid()
        {
            this.GridAssets.BeginUpdate();
            this.GridAssets.Clear();
            if (IsSelected)
            {
                if (MapArchetype.IsStatLoaded(StreamableMapArchetypeStat.Shaders))
                {
                    this.GridAssets.AddRange(MapArchetype.Textures.Select(texture => ViewModelFactory.CreateGridViewModel(texture)));
                }
            }
            this.GridAssets.EndUpdate();
        }
        #endregion // Private Methods

        #region IWeakEventListener Implementation
        /// <summary>
        /// 
        /// </summary>
        public bool ReceiveWeakEvent(Type managerType, Object sender, EventArgs e)
        {
            if (Application.Current != null)
            {
                Application.Current.Dispatcher.Invoke(
                    new Action
                    (
                        delegate()
                        {
                            RefreshHierarchy();
                            RefreshGrid();
                        }
                    ),
                    System.Windows.Threading.DispatcherPriority.ContextIdle
                );
            }
            else
            {
                RefreshHierarchy();
                RefreshGrid();
            }
            return true;
        }
        #endregion // IWeakEventListener

        #region IDisposable Implementation
        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            PropertyChangedEventManager.RemoveListener(MapArchetype, this, "Textures");
        }
        #endregion // IDisposable Implementation
    } // SimpleMapArchetypeViewModel
}
