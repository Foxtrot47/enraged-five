﻿using System;
using System.Linq;
using MetadataEditor.AddIn.MetaEditor.View;
using MetadataEditor.AddIn.MetaEditor.ViewModel;
using RSG.Base.Editor;
using RSG.Metadata.Data;
using RSG.Metadata.Parser;

namespace MetadataEditor.AddIn.MetaEditor.GridViewModel
{
    public class BitsetTunableValueViewModel : ViewModelBase, IComboWithCheckboxesItem
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="DisplayName"/> property.
        /// </summary>
        private string displayName;

        /// <summary>
        /// The private field used for the <see cref="IsChecked"/> property.
        /// </summary>
        private bool isChecked;

        /// <summary>
        /// The private field used for the <see cref="BitsetOwner"/> property.
        /// </summary>
        private BitsetTunableViewModel bitsetOwner;
        #endregion

        #region Constructor
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="MetadataEditor.AddIn.MetaEditor.GridViewModel.BitsetTunableValueViewModel"/>
        /// class.
        /// </summary>
        /// <param name="parent">
        /// The bitset tunable view model that owns this value.
        /// </param>
        /// <param name="displayName">
        /// The name of this value that will be displayed to the user.
        /// </param>
        /// <param name="isChecked">
        /// A value indicating whether this value is initially apart of the bitset or not.
        /// </param>
        public BitsetTunableValueViewModel(BitsetTunableViewModel parent, string displayName, bool isChecked)
        {
            this.isChecked = isChecked;
            this.bitsetOwner = parent;
            this.displayName = displayName;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets name that is displayed to the user for this bitset value.
        /// </summary>
        public string DisplayName
        {
            get { return this.displayName; }
            private set { this.displayName = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this bitset value is currently
        /// apart of the bitset field.
        /// </summary>
        public bool IsChecked
        {
            get { return this.isChecked; }
            set
            {
                if (value == this.isChecked)
                    return;

                this.isChecked = value;
                OnPropertyChanged("IsChecked");
                this.BitsetOwner.ChildInclusionChanged();
            }
        }

        /// <summary>
        /// Gets the bitset view model that owns this bitset value.
        /// </summary>
        public BitsetTunableViewModel BitsetOwner
        {
            get { return this.bitsetOwner; }
            private set { this.bitsetOwner = value; }
        }
        #endregion
    } // BitsetTunableValueViewModel

    public class BitsetTunableViewModel :GridTunableViewModel, System.Windows.IWeakEventListener
    {
        #region Fields
        private bool updatingTunableValue = false;

        private string displayText;
        #endregion

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public BitsetTunableValueViewModel[] Values
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public string DisplayText
        {
            get { return displayText; }
            set
            {
                displayText = value;
                OnPropertyChanged("DisplayText");
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="MetadataEditor.AddIn.MetaEditor.GridViewModel.BitsetTunableViewModel"/>
        /// class.
        /// </summary>
        /// <param name="m">
        /// </param>
        /// <param name="parent">
        /// </param>
        /// <param name="root">
        /// </param>
        public BitsetTunableViewModel(GridTunableViewModel parent, BitsetTunable m, MetaFileViewModel root)
            : base(parent, m, root)
        {
            System.ComponentModel.PropertyChangedEventManager.AddListener(m, this, "Value");
            BitsetMember member = m.Definition as RSG.Metadata.Parser.BitsetMember;
            if (member == null)
            {
                this.Values = new BitsetTunableValueViewModel[0];
                return;
            }

            if (member.Values == null)
            {
                this.Values = new BitsetTunableValueViewModel[0];
                return;
            }

            int valueCount = member.Values.Count;
            this.Values = new BitsetTunableValueViewModel[valueCount];

            int index = 0;
            foreach (string bitsetValue in member.Values.Keys)
            {
                bool isChecked = false;
                if (m.Value != null && m.Value.Contains(bitsetValue))
                {
                    string[] valueParts = m.Value.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    if (valueParts.Contains(bitsetValue))
                    {
                        isChecked = true;
                    }
                }

                BitsetTunableValueViewModel newValue = new BitsetTunableValueViewModel(this, bitsetValue, isChecked);
                this.Values[index] = newValue;
                index++;
            }

            this.ChildInclusionChanged();
        }
        #endregion

        #region Methods
        public void ChildInclusionChanged()
        {
            if (this.updatingTunableValue == true)
                return;

            String value = String.Empty;
            int index = 0;
            foreach (BitsetTunableValueViewModel bitsetValue in this.Values)
            {
                if (bitsetValue.IsChecked == true)
                {
                    if (index == 0)
                    {
                        value += bitsetValue.DisplayName;
                    }
                    else
                    {
                        value += " " + bitsetValue.DisplayName;
                    }
                    index++;
                }
            }

            this.updatingTunableValue = true;
            (this.Model as BitsetTunable).Value = value;
            this.updatingTunableValue = false;
        }

        /// <summary>
        /// true if the listener handled the event. It is considered an error by the
        /// System.Windows.WeakEventManager handling in WPF to register a listener for 
        /// an event that the listener does not handle. Regardless, the method should
        /// return false if it receives an event that it does not recognize or handle.
        /// </summary>
        public bool ReceiveWeakEvent(Type managerType, Object sender, EventArgs e)
        {
            if (managerType == typeof(System.ComponentModel.PropertyChangedEventManager))
            {
                if (updatingTunableValue == true)
                    return true;
                else
                {
                    // Property on model changed, need to update the view models.
                    if (sender is BitsetTunable)
                    {
                        BitsetTunable bitsetTunable = sender as BitsetTunable;
                        String[] setValues = new String[] { };
                        if (!String.IsNullOrEmpty(bitsetTunable.Value))
                            setValues = bitsetTunable.Value.Split(new String[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                        if ((bitsetTunable.Definition as RSG.Metadata.Parser.BitsetMember).Values != null)
                        {
                            this.updatingTunableValue = true;
                            foreach (BitsetTunableValueViewModel vm in this.Values)
                            {
                                vm.IsChecked = false;
                                if (setValues.Contains(vm.DisplayName))
                                {
                                    vm.IsChecked = true;
                                }
                            }
                            this.updatingTunableValue = false;
                        }
                        String value = String.Empty;
                        int index = 0;
                        foreach (BitsetTunableValueViewModel bitsetValue in this.Values)
                        {
                            if (bitsetValue.IsChecked == true)
                            {
                                if (index == 0)
                                {
                                    value += bitsetValue.DisplayName;
                                }
                                else
                                {
                                    value += " | " + bitsetValue.DisplayName;
                                }
                                index++;
                            }
                        }
                        this.DisplayText = value;
                    }
                }
            }

            return true;
        }
        #endregion
    } // BitsetTunableViewModel
} // MetadataEditor.AddIn.MetaEditor.GridViewModel
