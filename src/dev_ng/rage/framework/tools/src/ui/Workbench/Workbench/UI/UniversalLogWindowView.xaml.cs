﻿using System;
using Workbench.AddIn.UI.Layout;
using System.Collections.Generic;
using Workbench.AddIn.Services;
using RSG.UniversalLog;
using System.Windows.Controls;
using RSG.Base.Net;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows;
using System.Text.RegularExpressions;

namespace Workbench.UI
{
    /// <summary>
    /// Interaction logic for UniversalLogWindowView.xaml
    /// </summary>
    public partial class UniversalLogWindowView : ToolWindowBase<UniversalLogWindowViewModel>
    {
        #region Constants
        public static readonly Guid GUID = new Guid("B762D3F9-0347-4873-9FCB-C9BD3AF9D4B0");
        #endregion // Constants

        #region Constructor
        public UniversalLogWindowView(IEnumerable<ILoggingService> loggingOutputs, IConfigurationService config, IWebBrowserService webBrowser)
            : base("Universal_Log_Output",
                   new UniversalLogWindowViewModel(loggingOutputs, config, webBrowser))
        {
            InitializeComponent();

            this.ID = GUID;
            this.SaveModel = this.ViewModel;
        }

        #endregion // Constructor
    } // UniversalLogWindowView
} // Workbench.UI
