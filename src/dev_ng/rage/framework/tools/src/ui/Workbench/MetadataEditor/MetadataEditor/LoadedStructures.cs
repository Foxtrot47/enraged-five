﻿using System;
using System.ComponentModel.Composition;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Workbench.AddIn;
using Workbench.AddIn.Services;
using MetadataEditor.AddIn;
using MetadataEditor.ParsingErrors;
using RSG.Base.Configuration;
using RSG.Base.Editor;
using RSG.Base.Tasks;
using RSG.Metadata.Parser;
using RSG.Metadata.Model;
using System.Threading.Tasks;
using MetadataEditor.UI.View;

namespace MetadataEditor
{

    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(MetadataEditor.AddIn.CompositionPoints.StructureDictionary, typeof(IStructureDictionary))]
    public class LoadedStructures : ModelBase, IStructureDictionary, IPartImportsSatisfiedNotification
    {
        #region MEF Imports

        [ImportExtension(Workbench.AddIn.CompositionPoints.ConfigurationService,
            typeof(IConfigurationService))]
        private Lazy<IConfigurationService> ConfigurationService { get; set; }

        [ImportExtension(Workbench.AddIn.CompositionPoints.PerforceSyncService,
            typeof(IPerforceSyncService))]
        private Lazy<IPerforceSyncService> PerforceSyncService { get; set; }

        [ImportExtension(Workbench.AddIn.CompositionPoints.MessageService,
            typeof(IMessageService))]
        private Lazy<IMessageService> MessageService { get; set; }

        [ImportExtension(Workbench.AddIn.CompositionPoints.ProgressService,
            typeof(IProgressService))]
        private Lazy<IProgressService> ProgressService { get; set; }

        #endregion // MEF Imports

        #region Public Events

        public event EventHandler DefinitionsRefreshed;

        #endregion // Public Events

        #region Members

        private RSG.Metadata.Model.StructureDictionary m_structures = null;
        private ParsingErrorGroup m_duplicateEnumErrors;
        private ParsingErrorGroup m_duplicateStructureErrors;
        private ParsingErrorGroup m_fileParsingErrors;
        private ParsingErrorGroup m_criticalErrors;
        private Boolean m_didLastLoadHaveErrors;

        #endregion // Members

        #region Properties

        /// <summary>
        /// Determine whether the definitions have actual been loaded yet
        /// </summary>
        public Boolean DefinitionsLoaded
        {
            get;
            private set;
        }

        /// <summary>
        /// The actual structures that make up all the definitions
        /// </summary>
        public RSG.Metadata.Model.StructureDictionary Structures
        {
            get
            {
                if (DefinitionsLoaded == false)
                {
                    LoadStructures();
                }
                return m_structures;
            }
            set
            {
                m_structures = value;
            }
        }

        /// <summary>
        /// The root directory that the definitions are to be loaded
        /// from.
        /// </summary>
        public String DefinitionRootLocation
        {
            get;
            set;
        }

        /// <summary>
        /// The root directory that the core definitions are to be loaded
        /// from.
        /// </summary>
        public String CoreDefinitionRootLocation
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public ParsingErrorGroup DuplicateEnumErrors
        {
            get { return m_duplicateEnumErrors; }
            set { m_duplicateEnumErrors = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public ParsingErrorGroup DuplicateStructureErrors
        {
            get { return m_duplicateStructureErrors; }
            set { m_duplicateStructureErrors = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public ParsingErrorGroup FileParsingErrors
        {
            get { return m_fileParsingErrors; }
            set { m_fileParsingErrors = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public ParsingErrorGroup CriticalErrors
        {
            get { return m_criticalErrors; }
            set { m_criticalErrors = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Boolean DidLastLoadHaveErrors
        {
            get { return m_didLastLoadHaveErrors; }
            set
            {
                SetPropertyValue(value, () => this.DidLastLoadHaveErrors,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_didLastLoadHaveErrors = (Boolean)newValue;
                        }
                ));
            }
        }

        #endregion // Properties

        #region Constructor

        /// <summary>
        /// Default Constructor
        /// </summary>
        public LoadedStructures()
        {
            Structures = new RSG.Metadata.Model.StructureDictionary();
            this.DefinitionsLoaded = false;
            this.DefinitionRootLocation = Properties.Settings.Default.MetadataDefinitionRoot;
            this.CoreDefinitionRootLocation = Properties.Settings.Default.MetadataCoreDefinitionRoot;
        }

        #endregion // Constructor

        #region Private Methods

        /// <summary>
        /// 
        /// </summary>
        private void LoadStructures()
        {
            System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();

            this.DefinitionsLoaded = true;
            Structures = new RSG.Metadata.Model.StructureDictionary();

            List<string> paths = new List<string>();
            paths.Add(this.DefinitionRootLocation);
            paths.Add(this.CoreDefinitionRootLocation);

            List<String> filenames = new List<String>();
            filenames.Add(System.IO.Path.Combine(this.DefinitionRootLocation, "....psc"));
            filenames.Add(System.IO.Path.Combine(this.CoreDefinitionRootLocation, "....psc"));
            
            String syncMessage = "You currently have a number of .psc files out of date on your machine.\n" +
                "It is recommended that you are on the latest psc files before loading the definitions for the metadata editor.\n\n" +
                "You can select which files to get using the table below.";

            int testing = 0;
            this.PerforceSyncService.Value.Show(syncMessage, filenames.ToArray(), ref testing, false, false);

            IBranch branch = this.ConfigurationService.Value.Config.Project.DefaultBranch;
            Structures.Load(branch, paths.ToArray());

            //CancellationTokenSource cts = new CancellationTokenSource();
            //TaskContext context = new TaskContext(cts.Token);
            //IBranch branch = this.ConfigurationService.Value.Config.Project.DefaultBranch;
            //ITask task = new ActionTask("Loading", (ctx, progress) => 
            //    Structures.Load(branch, paths.ToArray()));
            //task.ReportsProgress = false;
            //ProgressService.Value.Show(task, context, cts, "Loading Metadata Definitions");

            //if (StructureDictionary.Errors.Count > 0)
            //{
            //    DidLastLoadHaveErrors = true;
            //}
            //else
            //{
            //    DidLastLoadHaveErrors = false;
            //}
            //this.ShowDefinitionErrorView();

            //if (!System.IO.Directory.Exists(this.DefinitionRootLocation))
            //{
            //    String errorMessage = String.Format("Unable to load metadata definitions from the root location {0} as path doesn't exist", this.DefinitionRootLocation);
            //    RSG.Base.Logging.Log.Log__Error(errorMessage);
            //    this.CriticalErrors = new ParsingErrorGroup("Critical Errors");
            //    ParsingError error = new ParsingError(errorMessage);
            //    this.CriticalErrors.Errors.Add(error);
            //    DidLastLoadHaveErrors = true;
            //    this.ShowDefinitionErrorView();
            //}

            sw.Stop();
            RSG.Base.Logging.Log.Log__Message("The definitions have finished loading and took {0} milliseconds to do it.", sw.ElapsedMilliseconds);
        }

        /// <summary>
        /// 
        /// </summary>
        public void SetDefinitionRootLocation(String root)
        {
            if (System.IO.Directory.Exists(root))
            {
                this.DefinitionRootLocation = root;
                Properties.Settings.Default.MetadataDefinitionRoot = this.DefinitionRootLocation;
                Properties.Settings.Default.Save();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void RefreshDefinitions()
        {
            this.LoadStructures();
            if (this.DefinitionsRefreshed != null)
                this.DefinitionsRefreshed(this, EventArgs.Empty);
        }

        public void ShowDefinitionErrorView()
        {
            CancellationTokenSource cts = new CancellationTokenSource();
            TaskContext context = new TaskContext(cts.Token);
            IBranch branch = this.ConfigurationService.Value.Config.Project.DefaultBranch;
            ITask task = new ActionTask(
                "Loading",
                (ctx, progress) =>
                    {
                        Parallel.ForEach(
                            this.Structures.Values,
                            structure =>
                            {
                                structure.EnsureLoaded();
                            });

                        Parallel.ForEach(
                            this.Structures.Enumerations.Values,
                            enumeration =>
                            {
                                enumeration.EnsureLoaded();
                            });
                    });

            task.ReportsProgress = false;
            ProgressService.Value.Show(task, context, cts, "Loading Metadata Definitions");
            if (this.Structures.HasWarning || this.Structures.HasErrors)
            {
                ErrorEncounteredWindow window = new ErrorEncounteredWindow();
                window.Message.Text = string.Format("{0} errors and {1} warnings has been located inside the psc data.", this.Structures.ErrorCount, this.Structures.WarningCount);
                window.ErrorList.ItemsSource = this.Structures.Errors.Concat(this.Structures.Warnings);
                window.Title = string.Format("PSC Data Validation");
                window.ShowDialog();
            }
        }

        #endregion // Private Methods

        #region IPartImportsSatisfiedNotification

        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {
            if (String.IsNullOrEmpty(this.DefinitionRootLocation))
            {
                String assetPath = this.ConfigurationService.Value.GameConfig.AssetsDir;
                this.DefinitionRootLocation = System.IO.Path.Combine(assetPath, "metadata", "definitions");
                Properties.Settings.Default.MetadataDefinitionRoot = this.DefinitionRootLocation;
                Properties.Settings.Default.Save();
            }
            if (String.IsNullOrEmpty(this.CoreDefinitionRootLocation))
            {
                String assetPath = this.ConfigurationService.Value.GameConfig.AssetsDir;
                this.CoreDefinitionRootLocation = System.IO.Path.Combine(assetPath, "metadata", "definitions", "workbench");
                Properties.Settings.Default.MetadataCoreDefinitionRoot = this.CoreDefinitionRootLocation;
                Properties.Settings.Default.Save();
            }
        }

        #endregion // IPartImportsSatisfiedNotification
    } // LoadedStructures
} // MetadataEditor
