﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn;
using Workbench.AddIn.Services;
using Workbench.Services.Perforce;
using RSG.SourceControl.Perforce;
using System.Windows.Threading;
using System.Windows;

namespace Workbench.Services
{
    /// <summary>
    /// The perforce service that is used to sync a set of files to the
    /// head revision
    /// </summary>
    [ExportExtension(Workbench.AddIn.CompositionPoints.PerforceSyncService,
                     typeof(Workbench.AddIn.Services.IPerforceSyncService))]
    public class PerforceSyncService : IPerforceSyncService
    {
        #region MEF Imports
        /// <summary>
        /// MEF import for login service.
        /// </summary>
        [ImportExtension(PerforceBrowser.AddIn.CompositionPoints.PerforceService, typeof(PerforceBrowser.AddIn.IPerforceService))]
        public PerforceBrowser.AddIn.IPerforceService PerforceService { get; set; }

        /// <summary>
        /// Task progress service, for use by the VM.
        /// </summary>
        [ImportExtension(Workbench.AddIn.CompositionPoints.TaskProgressService, typeof(ITaskProgressService))]
        private ITaskProgressService TaskProgressService { get; set; }
        #endregion // MEF Imports

        /// <summary>
        /// Shows the perforce sync dialog window that will allow the user to choose which
        /// files from the given set to sync to if any of them need to be synced to.
        /// </summary>
        /// <param name="message">The message you wish to display above the grid showing
        /// the user which files need syncing</param>
        /// <param name="filenames">An array of filenames that you would like perforce to
        /// check for you</param>
        /// <param name="syncedFilesCount">This parameter will be set to the number of files
        /// that were sycned to during the operation</param>
        /// <param name="forceSync">If this parameter is false it means that the user
        /// has the option to continue without syncing to any files else it means that the user has to
        /// sync to all the given files.</param>
        /// <returns></returns>
        public PerforceSyncResult Show(String message, IEnumerable<String> filenames, ref int syncedFilesCount, Boolean forceSync, Boolean allowCancel)
        {
            if (filenames != null && filenames.Any())
            {
                // Get the perforce connection object
                P4 perforceObject = PerforceService.PerforceConnection;
                if (perforceObject == null)
                {
                    return PerforceSyncResult.Failed;
                }

                PerforceSyncServiceViewModel dc = null;

                Application.Current.Dispatcher.Invoke(
                    new Action(
                        delegate()
                        {
                            PerforceSyncServiceView view = new PerforceSyncServiceView();
                            if (view != App.Current.MainWindow)
                                view.Owner = App.Current.MainWindow;
                            else
                                App.Current.MainWindow = null;
                            dc = new PerforceSyncServiceViewModel(filenames, message, forceSync, allowCancel, perforceObject, TaskProgressService);
                            view.DataContext = dc;

                            bool? dlgResult = view.ShowDialog();
                        }),
                    null);

                if (dc.OptionChosen == true)
                {
                    return dc.SyncResult;
                }
                else if (dc.OptionChosen == null)
                {
                    syncedFilesCount = 0;
                    return PerforceSyncResult.SyncedAll;
                }
                else
                {
                    syncedFilesCount = 0;
                    return PerforceSyncResult.Cancelled;
                }
            }

            return PerforceSyncResult.Ok;
        }
    } // PerforceSyncService
} // Workbench.Services
