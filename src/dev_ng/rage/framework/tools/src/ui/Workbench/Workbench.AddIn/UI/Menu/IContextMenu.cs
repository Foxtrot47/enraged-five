﻿using System;
using System.Collections.Generic;

namespace Workbench.AddIn.UI.Menu
{
    
    /// <summary>
    /// 
    /// </summary>
    public interface IContextMenu
    {
        IEnumerable<IMenuItem> ContextMenu { get; set; }
        bool ContextMenuEnabled { get; set; }
    }

} // Workbench.AddIn.UI.Menu namespace
