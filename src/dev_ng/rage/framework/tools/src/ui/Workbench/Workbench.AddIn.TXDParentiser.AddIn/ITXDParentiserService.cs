﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;

namespace Workbench.AddIn.TXDParentiser.AddIn
{
    /// <summary>
    /// 
    /// </summary>
    public interface ITXDParentiserService
    {
        /// <summary>
        /// The items that are currently selected in the active TXD parentiser view
        /// </summary>
        IEnumerable<ModelBase> SelectedItems { get; }

        /// <summary>
        /// Event that gets fired whenever the selection changes
        /// </summary>
        event TxdSelectionChangedEventHandler SelectionChanged;
    } // ITXDParentiserSelectionService
}
