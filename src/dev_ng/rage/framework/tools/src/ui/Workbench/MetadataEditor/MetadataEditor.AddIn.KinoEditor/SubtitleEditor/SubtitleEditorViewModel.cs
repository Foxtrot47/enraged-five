﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using RSG.Base.Editor.Command;
using MetadataEditor.AddIn.KinoEditor.Subtitle_Editor;
using RSG.Base.Collections;
using RSG.Base.Configuration;
using RSG.Base.Windows.Helpers;
using System.Windows.Input;
using System.IO;
using RSG.Model.Dialogue.Config;
using RSG.Model.Dialogue;
using Microsoft.Win32;
using RSG.Base.Windows.Controls;
using System.Xml.Serialization;
using System.Xml;
using System.Windows.Forms;
using System.Collections;

namespace MetadataEditor.AddIn.KinoEditor.Subtitle_Editor
{
    // Host for serializing the project data to disk
    public class Container
    {
        public ObservableCollection<EventViewModel> EventEntries;
        public string Media;
    }

    public class SubtitleEditorViewModel:
        RSG.Base.Editor.ViewModelBase,
        IPartImportsSatisfiedNotification
    {
        #region Fields
        private string currentDStarFile;
        #endregion
        #region Constants
        public const String TOOLWINDOW_NAME = "Subtitle_Editor";
        public const String TOOLWINDOW_TITLE = "Subtitle Editor";
        #endregion // Constants

        #region IPartImportsSatisfiedNotification Interface
        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {

        }
        #endregion // IPartImportsSatisfiedNotification Interface

        public SubtitleEditorViewModel()
        {
        }

        #region Functions

        private string GetName(string Identifier, int index)
        {
            try
            {
                return Identifier.Split(':')[index];
            }
            catch (Exception /*e*/)
            {
                return "Undefined";
            }
        }

        public void WriteCutSub()
        {
            // Validate we actually have events here

            Microsoft.Win32.SaveFileDialog dialog = new Microsoft.Win32.SaveFileDialog();
            dialog.Filter = "cutscene subtitle files (*.cutsub)|*.cutsub";
            dialog.Title = "Save";
            Nullable<bool> result = dialog.ShowDialog();
            if (result == true)
            {
                System.IO.StreamWriter file = new System.IO.StreamWriter(dialog.FileName);

                file.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                file.WriteLine("<rage__cutfCutsceneFile2 v=\"1.0\">");
                file.WriteLine("\t<pCutsceneObjects>");
                file.WriteLine("\t\t<Item type=\"rage__cutfSubtitleObject\">");
                file.WriteLine("\t\t\t<iObjectId value=\"1\" />");
                file.WriteLine("\t\t\t<attributeList />");

                file.WriteLine("\t\t\t<cName>" + ((EventEntries.Count > 0) ? GetName(EventEntries[0].Identifier, 1) : "Scene") + "</cName>");
 
                file.WriteLine("\t\t</Item>");
                file.WriteLine("\t</pCutsceneObjects>");

                file.WriteLine("\t<pCutsceneEventArgsList>");

                file.WriteLine("\t\t<Item type=\"rage__cutfFinalNameEventArgs\">");
                file.WriteLine("\t\t\t<attributeList />");
                file.WriteLine("\t\t\t<cName>" + ((EventEntries.Count > 0) ? GetName(EventEntries[0].Identifier, 1) : "Scene") + "</cName>");
                file.WriteLine("\t\t</Item>");

                for (int i = 0; i < TrackEntries.Count; ++i)
                {
                    for (int j = 0; j < TrackEntries[i].Events.Count; ++j)
                    {
                        file.WriteLine("\t\t<Item type=\"rage__cutfSubtitleEventArgs\">");
                        file.WriteLine("\t\t\t<cName>" + GetName(TrackEntries[i].Events[j].Identifier, 0) + "</cName>");
                        file.WriteLine("\t\t\t<fSubtitleDuration value=\"" + (TrackEntries[i].Events[j].UnitLength / 30.0f) + "\" />");
                        file.WriteLine("\t\t</Item>");
                    }
                }

                file.WriteLine("\t</pCutsceneEventArgsList>");

                file.WriteLine("\t<pCutsceneLoadEventList>");

                file.WriteLine("\t\t<Item type=\"rage__cutfObjectIdEvent\">");
                file.WriteLine("\t\t\t<fTime value=\"0\" />");
                file.WriteLine("\t\t\t<iEventId value=\"12\" />");
                file.WriteLine("\t\t\t<pEventArgs ref=\"0\" />");
                file.WriteLine("\t\t\t<iObjectId value=\"0\" />");
                file.WriteLine("\t\t</Item>");

                file.WriteLine("\t</pCutsceneLoadEventList>");

                file.WriteLine("\t<pCutsceneEventList>");

                int argRef = 1;
                for (int i = 0; i < TrackEntries.Count; ++i)
                {
                    for (int j = 0; j < TrackEntries[i].Events.Count; ++j)
                    {
                        file.WriteLine("\t\t<Item type=\"rage__cutfObjectIdEvent\">");
                        file.WriteLine("\t\t\t<fTime value=\"" + (TrackEntries[i].Events[j].Start / 30.0f) + "\" />");
                        file.WriteLine("\t\t\t<iEventId value=\"30\" />");
                        file.WriteLine("\t\t\t<pEventArgs ref=\"" + argRef + "\" />");
                        file.WriteLine("\t\t\t<iObjectId value=\"1\" />");
                        file.WriteLine("\t\t</Item>");
                        argRef++;
                    }
                }

                file.WriteLine("\t</pCutsceneEventList>");

                file.WriteLine("</rage__cutfCutsceneFile2>");

                file.Close();
            }
        }

        public bool OpenScriptFile()
        {
            Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog();
            dialog.Filter = "text files (*.txt)|*.txt";
            dialog.Title = "Open";
            Nullable<bool> result = dialog.ShowDialog();
            if (result == true)
            {
                Script = File.ReadAllText(dialog.FileName);
            }
            return true;
        }

        public void RemoveTimeLine()
        {
            this.RemoveEventFromTimeLine(SelectedEvent);
        }

        public void RemoveEventFromTimeLine(EventViewModel viewModel)
        {
            if (viewModel == null)
            {
                return;
            }
            
            int trackIndex = 0;
            EventTrackViewModel track = null;
            for (int i = 0; i < TrackEntries.Count; ++i)
            {
                if (TrackEntries[i].Name.Trim() == viewModel.Speaker)
                {
                    track = TrackEntries[i];
                    trackIndex = i;
                    break;
                }
            }

            if (track == null)
            {
                return;
            }
            
            for (int i = 0; i < track.Events.Count; ++i)
            {
                if (track.Events[i].Equals(viewModel))
                {
                    track.Events[i].OnTimeline = false;
                    track.Events.Remove(track.Events[i]);
                    if (track.Events.Count == 0)
                    {
                        TrackEntries.RemoveAt(trackIndex);
                    }

                    break;
                }
            }
        }

        public void AddEvent()
        {
            EventEntries.Add(new EventViewModel(this));
        }

        private void RemoveEvent(object parameter)
        {
            IList genericSelected = parameter as IList;
            if (genericSelected == null)
            {
                genericSelected = new List<EventViewModel>() { this.SelectedEvent };
            }

            List<EventViewModel> selected = new List<EventViewModel>(genericSelected.Cast<EventViewModel>());
            foreach (EventViewModel viewModel in selected)
            {
                EventTrackViewModel removeTrack = null;
                foreach (EventTrackViewModel track in this.TrackEntries)
                {
                    if (track.Name.Trim() != viewModel.Speaker)
                    {
                        continue;
                    }

                    track.Events.Remove(viewModel);
                    if (track.Events.Count == 0)
                    {
                        removeTrack = track;
                    }

                    break;
                }

                if (removeTrack != null)
                {
                    this.TrackEntries.Remove(removeTrack);
                }

                EventEntries.Remove(viewModel);
                this.SelectedEvent = null;
            }
        }

        public event EventHandler LoadRequested;

        public void AddTimeLine(bool bResetScale = true)
        {
            double currentFrame = (int)(Math.Ceiling(this.Seconds * 30));
            if (SelectedEvent != null)
            {
                EventTrackViewModel track = null;
                for (int i = 0; i < TrackEntries.Count; ++i)
                {
                    if (TrackEntries[i].Name.Trim() == SelectedEvent.Speaker)
                    {
                        track = TrackEntries[i];
                        break;
                    }
                }

                if (track == null)
                {
                    track = new EventTrackViewModel();
                    track.Name = SelectedEvent.Speaker;

                    if (bResetScale)
                    {
                        SelectedEvent.ResetScale();
                        SelectedEvent.Start = currentFrame;
                    }

                    SelectedEvent.OnTimeline = true;
                    track.Events.Add(SelectedEvent);

                    TrackEntries.Add(track);
                }
                else
                {
                    bool bFoundEvent = false;
                    for (int i = 0; i < track.Events.Count; ++i)
                    {
                        if (track.Events[i].Equals(SelectedEvent))
                        {
                            bFoundEvent = true;
                            break;
                        }
                    }

                    if (!bFoundEvent)
                    {
                        if (bResetScale)
                        {
                            SelectedEvent.ResetScale();
                            SelectedEvent.Start = currentFrame;
                        }

                        SelectedEvent.OnTimeline = true;
                        track.Events.Add(SelectedEvent);
                    }
                }
            }
        }

        // Open media file to play in main subtitle window
        public void OpenMediaFile()
        {
            Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog();
            //dialog.Filter = "Media Files(*.MOV;*.MP4;*.WMV;*.MP3)|*.MOV;*.MP4;*.WMV;*.MP3";
            dialog.Filter = "Media Files(*.MOV;*.MP4;*.WMV;)|*.MOV;*.MP4;*.WMV;";
            dialog.Title = "Open Media File";

            if (VideoSource != null)
            {
                dialog.InitialDirectory = Path.GetDirectoryName(VideoSource.LocalPath);
                dialog.FileName = Path.GetFileName(VideoSource.LocalPath);
            }

            Nullable<bool> result = dialog.ShowDialog();
            if (result == true)
            {
                VideoSource = new Uri(dialog.FileName, UriKind.Absolute);
                EnabledState = true;
                LoadRequested(null, null);
            }
        }

        // Open a D* file, this will populate the event data
        public bool OpenDialogStarFile()
        {
            IConfig config = ConfigFactory.CreateConfig();
            Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog();
            dialog.Filter = "Dialogue Star Files (*.Dstar)|*.Dstar";
            dialog.Title = "Open Dialogue Star File";
            dialog.InitialDirectory = Path.Combine(config.Project.DefaultBranch.Assets, "Dialogue", "American");

            Nullable<bool> result = dialog.ShowDialog();
            if (result == true)
            {
                string configFilePath = Path.Combine(config.Project.DefaultBranch.Assets, "Dialogue", "Config", "Configurations.xml");
                RSG.Base.Logging.Log.Log__Message("Loading dialogue config file '{0}'", configFilePath);
                if(!File.Exists(configFilePath))
                {
                    MessageBox.Show(configFilePath + " does not exist", "Error");
                    return false;
                }

                if (!string.IsNullOrWhiteSpace(this.currentDStarFile))
                {
                    if (dialog.FileName != this.currentDStarFile)
                    {
                        EventEntries.Clear();
                        TrackEntries.Clear();
                    }
                }

                DialogueConfigurations newConfiguration = new DialogueConfigurations(configFilePath, config); // characters live in here
                MissionDialogue newDialogue = new MissionDialogue(dialog.FileName, newConfiguration);
                ISet<EventViewModel> eventSet = new HashSet<EventViewModel>(EventEntries);
                if (newDialogue != null)
                {
                    for (int i = 0; i < newDialogue.Conversations.Count; ++i)
                    {
                        int lineNumber = 1;
                        foreach (RSG.Model.Dialogue.Line line in newDialogue.Conversations[i].Lines)
                        {
                            string identifier = newDialogue.Conversations[i].Root + "_" + lineNumber + ":" + newDialogue.MissionSubtitleId;
                            EventViewModel present = this.FindEventByFilename(line.Filename);
                            if (present == null)
                            {
                                present = new EventViewModel(this);
                                present.Filename = line.Filename;
                                EventEntries.Add(present);
                            }
                            else
                            {
                                eventSet.Remove(present);
                            }

                            present.Identifier = identifier;
                            string speaker = null;
                            if (!newConfiguration.Characters.TryGetValue(line.CharacterGuid, out speaker))
                            {
                                speaker = "Unknown";
                            }

                            present.Speaker = speaker;
                            present.String = line.LineDialogue;
                            AddSpeaker(present.Speaker);

                            lineNumber++;
                        }
                    }
                }

                foreach (EventViewModel evt in eventSet)
                {
                    this.EventEntries.Remove(evt);
                    foreach (EventTrackViewModel track in this.TrackEntries)
                    {
                        track.Events.Remove(evt);
                    }
                }

                this.currentDStarFile = dialog.FileName;
                return true;
            }

            this.currentDStarFile = dialog.FileName;
            return false;
        }

        private EventViewModel FindEventByFilename(string filename)
        {
            return (from e in this.EventEntries where e.Filename == filename select e).FirstOrDefault();
        }

        // Calculate which subtitle (if any) should be on displayed on screen at the specific time.
        private void CalculateSubtitleToShow(float seconds)
        {
            int frame = (int)(Math.Ceiling(seconds * 30.0));
            string strSubtitleString = String.Empty;

            for (int i = 0; i < EventEntries.Count; ++i)
            {
                if (EventEntries[i].OnTimeline) // Only display events that are on the timeline
                {
                    double Start = EventEntries[i].Start;
                    double End = EventEntries[i].Start + EventEntries[i].UnitLength;

                    if (frame >= Start && frame <= End)
                    {
                        if (strSubtitleString == String.Empty)
                        {
                            strSubtitleString = EventEntries[i].String;
                        }
                        else
                        {
                            strSubtitleString += "\n" + EventEntries[i].String;
                        }
                    }
                }
            }

            CurrentSubtitle = strSubtitleString;
        }

        // Open subtitle editor project file
        public bool OpenProjectFile(string filename)
        {
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(Container));

            using (TextReader textReader = new StreamReader(filename))
            {
                Container host = (Container)serializer.Deserialize(textReader);
                EventEntries = host.EventEntries;

                // Iterate the events and add them to the timeline, keep references
                for (int i = 0; i < EventEntries.Count; ++i)
                {
                    (EventEntries[i]).subtitleEditor = this;
                    if (EventEntries[i].OnTimeline)
                    {
                        SelectedEvent = EventEntries[i];
                        AddTimeLine(false);
                    }
                }

                SelectedEvent = null;

                if (host.Media != null)
                {
                    if (File.Exists(host.Media))
                    {
                        VideoSource = new Uri(host.Media, UriKind.Absolute);
                        
                    }
                    else
                    {
                        MessageBox.Show(host.Media + " does not exist", "Error");
                    }
                }
            }

            for (int i = 0; i < EventEntries.Count; ++i)
            {
                AddSpeaker(EventEntries[i].Speaker);
            }

            //LoadRequested(VideoSource, null);

            return true;
        }

        /// <summary>
        /// Performs a full save. Saves the project file and the sub cut files.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public bool FullSave(string filename)
        {
            bool success = this.SaveProjectFile(filename);
            this.WriteCutSub();
            return success;
        }

        // Save subtitle editor project file
        public bool SaveProjectFile(string filename)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Container));

            Container host = new Container();
            host.EventEntries = EventEntries;
            if (VideoSource != null)
            {
                host.Media = VideoSource.LocalPath;
            }

            using (TextWriter writer = new StreamWriter(filename))
            {
                serializer.Serialize(writer, host);
            }

            return true;
        }

        // Move event from one speaker to another
        void MoveEvent(string speaker)
        {
            if (speaker == String.Empty || this.SelectedEvent == null)
            {
                return;
            }
            
            for (int i = 0; i < TrackEntries.Count; ++i)
            {
                for (int j = 0; j < TrackEntries[i].Events.Count; ++j)
                {
                    if (SelectedEvent.Equals(TrackEntries[i].Events[j]))
                    {
                        TrackEntries[i].Events.Remove(TrackEntries[i].Events[j]);

                        if (TrackEntries[i].Events.Count == 0)
                        {
                            TrackEntries.Remove(TrackEntries[i]);
                            --i;
                            break;
                        }
                    }
                }
            }

            if (SelectedEvent.OnTimeline)
            {
                AddTimeLine(false);
            }      
        }

        // Add speaker the list, remove if necessary
        void AddSpeaker(string speaker)
        {
            if (speaker == String.Empty) return;

            bool bFoundSpeaker = false;
            for (int i = 0; i < SpeakerEntries.Count; ++i)
            {
                if (SpeakerEntries[i] == speaker)
                {
                    bFoundSpeaker = true;
                }
            }

            if (bFoundSpeaker == false)
            {
                SpeakerEntries.Add(speaker);
            }

            if (SelectedEvent != null)
            {
                SelectedEvent.Speaker = speaker;
            }

            ///

            for (int i = 0; i < SpeakerEntries.Count; ++i)
            {
                bFoundSpeaker = false;
                for (int j = 0; j < EventEntries.Count; ++j)
                {
                    if (SpeakerEntries[i] == EventEntries[j].Speaker)
                    {
                        bFoundSpeaker = true;
                    }
                }

                if (bFoundSpeaker == false)
                {
                    for (int j = 0; j < TrackEntries.Count; ++j)
                    {
                        if (TrackEntries[j].Name == SpeakerEntries[i])
                        {
                            TrackEntries.Remove(TrackEntries[j]);
                            break;
                        }
                    }

                    SpeakerEntries.Remove(SpeakerEntries[i]);
                    i--;
                }
            }

        }

        #endregion

        #region Commands

        RelayCommand _openDialogueStarCommand;
        public ICommand OpenDialogueStarCommand
        {
            get
            {
                if (_openDialogueStarCommand == null)
                {
                    _openDialogueStarCommand = new RelayCommand(param => this.OpenDialogStarFile());
                    NotifyPropertyChanged("OpenDialogueStarCommand");
                }
                return _openDialogueStarCommand;
            }
        }

        RelayCommand _openMediaCommand;
        public ICommand OpenMediaCommand
        {
            get
            {
                if (_openMediaCommand == null)
                {
                    _openMediaCommand = new RelayCommand(param => this.OpenMediaFile());
                    NotifyPropertyChanged("OpenMediaCommand");
                }
                return _openMediaCommand;
            }
        }

        RelayCommand _openScriptCommand;
        public ICommand OpenScriptCommand
        {
            get
            {
                if (_openScriptCommand == null)
                {
                    _openScriptCommand = new RelayCommand(param => this.OpenScriptFile());
                    NotifyPropertyChanged("OpenScriptCommand");
                }
                return _openScriptCommand;
            }
        }

        RelayCommand _saveCutFileCommand;
        public ICommand SaveCutFileCommand
        {
            get
            {
                if (_saveCutFileCommand == null)
                {
                    _saveCutFileCommand = new RelayCommand(param => this.WriteCutSub());
                    NotifyPropertyChanged("SaveCutFileCommand");
                }
                return _saveCutFileCommand;
            }
        }

        RelayCommand _addeventCommand;
        public ICommand AddEventCommand
        {
            get
            {
                if (_addeventCommand == null)
                {
                    _addeventCommand = new RelayCommand(param => this.AddEvent());
                    NotifyPropertyChanged("AddEventCommand");
                }
                return _addeventCommand;
            }
        }

        RelayCommand _removeeventCommand;
        public ICommand RemoveEventCommand
        {
            get
            {
                if (_removeeventCommand == null)
                {
                    _removeeventCommand = new RelayCommand(param => this.RemoveEvent(param));
                    NotifyPropertyChanged("RemoveEventCommand");
                }
                return _removeeventCommand;
            }
        }

        RelayCommand _addtimelineCommand;
        public ICommand AddTimeLineCommand
        {
            get
            {
                if (_addtimelineCommand == null)
                {
                    _addtimelineCommand = new RelayCommand(param => this.AddTimeLine());
                    NotifyPropertyChanged("AddTimeLineCommand");
                }
                return _addtimelineCommand;
            }
        }

        RelayCommand _removetimelineCommand;
        public ICommand RemoveTimeLineCommand
        {
            get
            {
                if (_removetimelineCommand == null)
                {
                    _removetimelineCommand = new RelayCommand(param => this.RemoveTimeLine());
                    NotifyPropertyChanged("RemoveTimeLineCommand");
                }
                return _removetimelineCommand;
            }
        }

        #endregion

        #region Properties

        private bool m_Enabled = false;
        public bool EnabledState
        {
            get { return m_Enabled; }
            set
            {
                SetPropertyValue(value, () => this.EnabledState,
                    new PropertySetDelegate(delegate(Object newValue) { m_Enabled = (bool)newValue; }));
            }
        }

        private Uri m_VideoSource = null;
        public Uri VideoSource
        {
            get { return m_VideoSource; }
            set
            {
                SetPropertyValue(value, () => this.VideoSource,
                    new PropertySetDelegate(delegate(Object newValue) { m_VideoSource = (Uri)newValue; }));
            }
        }

        private float m_Seconds = 0;
        public float Seconds
        {
            get { return m_Seconds; }
            set
            {
                SetPropertyValue(value, () => this.Seconds,
                    new PropertySetDelegate(delegate(Object newValue) { m_Seconds = (float)newValue; }));

                CalculateSubtitleToShow(m_Seconds);
            }
        }

        private float m_SceneLength = 1000;
        public float SceneLength
        {
            get { return m_SceneLength; }
            set
            {
                SetPropertyValue(value, () => this.SceneLength,
                    new PropertySetDelegate(delegate(Object newValue) { m_SceneLength = (float)newValue; }));

                SceneLengthFrames = Helper.SecondsToFrames(SceneLength);
            }
        }

        private int m_SceneLengthFrames = 0;
        public int SceneLengthFrames
        {
            get { return m_SceneLengthFrames; }
            set
            {
                SetPropertyValue(value, () => this.SceneLengthFrames,
                    new PropertySetDelegate(delegate(Object newValue) { m_SceneLengthFrames = (int)newValue; }));
            }
        }

        private string m_CurrentSubtitle = String.Empty;
        public string CurrentSubtitle
        {
            get { return m_CurrentSubtitle; }
            set
            {
                SetPropertyValue(value, () => this.CurrentSubtitle,
                    new PropertySetDelegate(delegate(Object newValue) { m_CurrentSubtitle = (string)newValue; }));
            }
        }

        private EventViewModel m_SelectedEvent = null;
        public EventViewModel SelectedEvent
        {
            get { return m_SelectedEvent; }
            set
            {
                SetPropertyValue(value, () => this.SelectedEvent,
                    new PropertySetDelegate(delegate(Object newValue) { m_SelectedEvent = (EventViewModel)newValue; }));
            }
        }

        private string m_SpeakerText = String.Empty;
        public string SpeakerText
        {
            get { return m_SpeakerText; }
            set
            {
                SetPropertyValue(value, () => this.SpeakerText,
                    new PropertySetDelegate(delegate(Object newValue) { m_SpeakerText = (string)newValue; }));

                AddSpeaker(SpeakerText);
                MoveEvent(SpeakerText);
            }
        }

        private string m_Script = String.Empty;
        public string Script
        {
            get { return m_Script; }
            set
            {
                SetPropertyValue(value, () => this.Script,
                    new PropertySetDelegate(delegate(Object newValue) { m_Script = (string)newValue; }));
            }
        }

        private string m_Log = String.Empty;
        public string Log
        {
            get { return m_Log; }
            set
            {
                SetPropertyValue(value, () => this.Log,
                    new PropertySetDelegate(delegate(Object newValue) { m_Log = (string)newValue; }));
            }
        }

        private ObservableCollection<EventTrackViewModel> m_TrackEntries = new ObservableCollection<EventTrackViewModel>();
        public ObservableCollection<EventTrackViewModel> TrackEntries
        {
            get { return m_TrackEntries; }
            private set
            {
                SetPropertyValue(value, () => this.TrackEntries,
                    new PropertySetDelegate(delegate(Object newValue) { m_TrackEntries = (ObservableCollection<EventTrackViewModel>)newValue; }));
            }
        }
       
        private ObservableCollection<String> m_SpeakerEntries = new ObservableCollection<String>();
        public ObservableCollection<String> SpeakerEntries
        {
            get { return m_SpeakerEntries; }
            private set
            {
                SetPropertyValue(value, () => this.SpeakerEntries,
                    new PropertySetDelegate(delegate(Object newValue) { m_SpeakerEntries = (ObservableCollection<String>)newValue; }));
            }
        }

        private ObservableCollection<EventViewModel> m_EventEntries = new ObservableCollection<EventViewModel>();
        public ObservableCollection<EventViewModel> EventEntries
        {
            get { return m_EventEntries; }
            private set
            {
                SetPropertyValue(value, () => this.EventEntries,
                    new PropertySetDelegate(delegate(Object newValue) { m_EventEntries = (ObservableCollection<EventViewModel>)newValue; }));
            }
        }

        #endregion
    }
}
