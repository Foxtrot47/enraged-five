﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using Workbench.AddIn.Services;
using System.Collections;

namespace Workbench.AddIn.UI.Layout
{
    /// <summary>
    /// 
    /// </summary>
    public interface ISearchableContent
    {
        #region Events
        /// <summary>
        /// 
        /// </summary>
        event ContentSelectionChangedEventHandler SelectionChanged;
        #endregion

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="previousResult"></param>
        /// <returns></returns>
        object FindNext(ISearchQuery query, object previousResult);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="previousResult"></param>
        /// <returns></returns>
        object FindPrevious(ISearchQuery query, object previousResult);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        List<ISearchResult> FindAll(ISearchQuery query);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="result"></param>
        void SelectSearchResult(ISearchResult result);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        List<object> GetCurrentSelection();
        #endregion
    } // ISearchableContent

    public delegate void ContentSelectionChangedEventHandler(object sender, ContentSelectionChangedEventArgs e);

    public class ContentSelectionChangedEventArgs : EventArgs
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public IList AddedItems
        { 
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public IList RemovedItems
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public IList CurrentSelection
        {
            get;
            private set;
        }
        #endregion

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="removedItems"></param>
        /// <param name="addedItems"></param>
        /// <param name="currentSelection"></param>
        public ContentSelectionChangedEventArgs(IList removedItems, IList addedItems, IList currentSelection)
        {
            this.RemovedItems = removedItems;
            this.AddedItems = addedItems;
            this.CurrentSelection = currentSelection;
        }
        #endregion
    } // ContentSelectionChangedEventArgs

    /// <summary>
    /// 
    /// </summary>
    public interface ISearchQuery
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        Regex Expression { get; }

        /// <summary>
        /// 
        /// </summary>
        bool UseTag { get; }

        /// <summary>
        /// 
        /// </summary>
        string Tag { get; }
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    public class SearchQuery : ISearchQuery
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public Regex Expression { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public bool UseTag { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public string Tag { get; private set; }
        #endregion

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Expression"></param>
        /// <param name="useTag"></param>
        /// <param name="tag"></param>
        public SearchQuery(Regex expression, bool useTag, string tag)
        {
            this.Expression = expression;
            this.UseTag = useTag;
            this.Tag = tag;
        }
        #endregion
    }
} // Workbench.AddIn.UI.Layout
