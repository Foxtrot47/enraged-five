﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Report;

namespace Workbench.AddIn.VehicleStatistics.Reports
{
    /// <summary>
    /// 
    /// </summary>
    [ExportExtension(ExtensionPoints.Report, typeof(IReport))]
    public class VehicleStatsReport : RSG.Model.Report.Reports.Vehicles.VehicleStatsReport
    {
    } // VehicleCSVExportReport
}
