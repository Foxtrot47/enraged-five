﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Workbench.AddIn.UI.Layout;
using System.IO;

namespace MetadataEditor.AddIn.KinoEditor.Subtitle_Editor
{
    /// <summary>
    /// Interaction logic for SubtitleEditorWindowView.xaml
    /// </summary>
    public partial class SubtitleEditorWindowView : DocumentBase<SubtitleEditorViewModel>
    {
        #region Constants
        public static readonly Guid GUID = new Guid("A916721A-15E5-40E2-B271-309BB4672277");
        #endregion // Constants

        public SubtitleEditorWindowView(string filename)
            : base(SubtitleEditorViewModel.TOOLWINDOW_NAME, new SubtitleEditorViewModel())
        {
            InitializeComponent();
            this.ID = GUID;

            if (File.Exists(filename))
                ViewModel.OpenProjectFile(filename);

            media.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(Media_PropertyChanged);

        }

        /// <summary>
        /// Handle property changes from the media control.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void Media_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            userControl11.TotalFrames = media.TotalFrames;
            userControl11.CurrentFrame = media.CurrentFrame;
        }

        private void userControl11_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }
}
