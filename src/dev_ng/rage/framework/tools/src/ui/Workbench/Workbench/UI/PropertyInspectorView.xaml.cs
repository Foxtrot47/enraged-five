﻿using System;
using System.Windows;
using Workbench.AddIn;
using Workbench.AddIn.UI.Layout;

namespace Workbench.UI
{
    /// <summary>
    /// 
    /// </summary>
    public partial class PropertyInspectorView : ToolWindowBase<PropertyInspectorViewModel>
    {        
        #region Constructor(s)

        /// <summary>
        /// Default constructor.
        /// </summary>
        public PropertyInspectorView()
            : base(PropertyInspectorViewModel.PROPERTYINSPECTOR_NAME, new PropertyInspectorViewModel())
        {
            InitializeComponent();
        }

        #endregion // Constructor(s)
    } // PropertyInspectorView
} // Workbench.UI
