﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ContentBrowser.AddIn
{
    public interface INotfiyPanelSelectionChanged
    {
        event SelectedPanelItemChangedHandler PanelItemChanged;
    }
}
