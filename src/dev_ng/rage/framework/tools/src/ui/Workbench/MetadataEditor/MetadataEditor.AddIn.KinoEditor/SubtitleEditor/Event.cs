﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Collections;

namespace MetadataEditor.AddIn.KinoEditor.SubtitleEditor
{
    public class EventCollection : ObservableCollection<Event>
    {
        protected override void InsertItem(int index, Event item)
        {
            base.InsertItem(index, item);
        }

        protected override void RemoveItem(int index)
        {
            base.RemoveItem(index);
        }

        protected override void SetItem(int index, Event item)
        {
            base.SetItem(index, item);
        }
    }

    public class Event : RSG.Base.Editor.ModelBase
    {
        public string Identifier
        {
            get;
            set;
        }

        public string String
        {
            get;
            set;
        }

        public string Speaker
        {
            get;
            set;
        }
    }
}
