using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace MetadataEditor.AddIn.KinoEditor
{
	/// <summary>
	/// A Canvas which manages dragging of the UIElements it contains.  
	/// </summary>
	public class DragCanvas : Canvas
	{
		#region Data

        private class ObjectData
        {
            public UIElement control;
            public Point origCursorLocation;
            public double origHorizOffset;
            public double origVertOffset;
        }

		// Stores a reference to the UIElement currently being dragged by the user.
		private UIElement _elementBeingDragged;

        private double _scale = 1.1;

		// Keeps track of where the mouse cursor was when a drag operation began.		
		private Point _origCursorLocation;

		// Keeps track of which horizontal and vertical offset should be modified for the drag element.
		private bool _modifyLeftOffset, _modifyTopOffset;

		// True if a drag operation is underway, else false.
		private bool _isDragInProgress;

        // List of object data for each child, contains positional data
        private List<ObjectData> _objectList = new List<ObjectData>();

		#endregion // Data

		#region Attached Properties

			#region CanBeDragged

		public static readonly DependencyProperty CanBeDraggedProperty;

		public static bool GetCanBeDragged( UIElement uiElement )
		{
			if( uiElement == null )
				return false;

			return (bool)uiElement.GetValue( CanBeDraggedProperty );
		}

		public static void SetCanBeDragged( UIElement uiElement, bool value )
		{
			if( uiElement != null )
				uiElement.SetValue( CanBeDraggedProperty, value );
		}

			#endregion // CanBeDragged

		#endregion // Attached Properties

		#region Static Constructor

		static DragCanvas()
		{
			CanBeDraggedProperty = DependencyProperty.RegisterAttached(
				"CanBeDragged",
				typeof( bool ),
				typeof( DragCanvas ),
				new UIPropertyMetadata( true ) );
		}

		#endregion // Static Constructor

		#region Constructor

		/// <summary>
		/// Initializes a new instance of DragCanvas.  UIElements in
		/// the DragCanvas will immediately be draggable by the user.
		/// </summary>
		public DragCanvas()
		{
            Scale = 1.1;
		}

		#endregion // Constructor

		#region Interface

            #region Scale

        public double Scale
        {
            get { return _scale; }
            set
            {
                if (_scale != value)
                {
                    if (value <= 1) return;

                    _scale = value;
                    LayoutTransform = new ScaleTransform(_scale, _scale);
                }
            }
        }

        #endregion

            #region ElementBeingDragged

        /// <summary>
		/// Returns the UIElement currently being dragged, or null.
		/// </summary>
		/// <remarks>
		/// Note to inheritors: This property exposes a protected 
		/// setter which should be used to modify the drag element.
		/// </remarks>
		public UIElement ElementBeingDragged
		{
			get
			{
				return this._elementBeingDragged;
			}
			protected set
			{
				if( this._elementBeingDragged != null )
					this._elementBeingDragged.ReleaseMouseCapture();

				if( DragCanvas.GetCanBeDragged( value ) )
				{
					this._elementBeingDragged = value;
					this._elementBeingDragged.CaptureMouse();
				}
				else
					this._elementBeingDragged = null;
			}
        }

			#endregion // ElementBeingDragged

			#region FindCanvasChild

		/// <summary>
		/// Walks up the visual tree starting with the specified DependencyObject, 
		/// looking for a UIElement which is a child of the Canvas.  If a suitable 
		/// element is not found, null is returned.  If the 'depObj' object is a 
		/// UIElement in the Canvas's Children collection, it will be returned.
		/// </summary>
		/// <param name="depObj">
		/// A DependencyObject from which the search begins.
		/// </param>
		public UIElement FindCanvasChild( DependencyObject depObj )
		{
			while( depObj != null )
			{
				// If the current object is a UIElement which is a child of the
				// Canvas, exit the loop and return it.
				UIElement elem = depObj as UIElement;
				if( elem != null && base.Children.Contains( elem ) )
					break;

				// VisualTreeHelper works with objects of type Visual or Visual3D.
				// If the current object is not derived from Visual or Visual3D,
				// then use the LogicalTreeHelper to find the parent element.
				if( depObj is Visual || depObj is Visual3D )
					depObj = VisualTreeHelper.GetParent( depObj );
				else
					depObj = LogicalTreeHelper.GetParent( depObj );
			}
			return depObj as UIElement;
		}

			#endregion // FindCanvasChild

            #region IsMouseOverChildren

        /// <summary>
        /// Checks if the mouse is over a child UIElement. This is used by external objects to see if a Mouseclick/
        /// MouseMove occured on a UIElement.
        /// </summary>
        public bool IsMouseOverChildren()
        {
            foreach (UIElement child in Children)
            {
                if (child.IsMouseOver)
                    return true;
            }

            return false;
        }

        #endregion

            #region InternalPreviewMouseLeftButtonDown

        /// <summary>
        /// Allows external objects to use the canvas drag functionality
        /// </summary>
        /// <param name="currentLocation">
        /// Position of the cursor when mouse was clicked. 
        /// </param>
        public void InternalPreviewMouseLeftButtonDown(Point currentLocation)
        {
            this._isDragInProgress = false;

            // Collect data for each child, we need this if we are going to scroll the canvas and want to move
            // multiple children at a time, otherwise we would only have one position and everything would
            // draw upon itself.
            CreateObjectData(currentLocation);

            this._isDragInProgress = true;
        }

        #endregion

            #region InternalPreviewMouseMove

        /// <summary>
        /// Allows external objects to use the canvas drag functionality
        /// </summary>
        /// <param name="control">
        /// UIElement which is requested to move.
        /// </param>
        /// <param name="currentLocation">
        /// Current location of the cursor at this momement in time.
        /// </param>
        public void InternalPreviewMouseMove(UIElement control, Point currentLocation)
        {
            // If no element is being dragged, there is nothing to do.
            if (control == null || !this._isDragInProgress)
                return;

            // We should always find an object, but safety first.
            ObjectData currentControl = FindCurrentControl(control);
            if (currentControl == null)
                return;

            // Get the position of the mouse cursor, relative to the Canvas.
            Point cursorLocation = currentLocation;

            // These values will store the new offsets of the drag element.
            double newHorizontalOffset, newVerticalOffset;

            #region Calculate Offsets

            // Invert the movement to simulate canvas moving, clicking on the canvas and moving our objects move
            // in the opposite direction
            if (this._modifyLeftOffset)
                newHorizontalOffset = currentControl.origHorizOffset + (cursorLocation.X - currentControl.origCursorLocation.X);
            else
                newHorizontalOffset = currentControl.origHorizOffset - (cursorLocation.X - currentControl.origCursorLocation.X);

            // Determine the vertical offset.
            if (this._modifyTopOffset)
                newVerticalOffset = currentControl.origVertOffset + (cursorLocation.Y - currentControl.origCursorLocation.Y);
            else
                newVerticalOffset = currentControl.origVertOffset - (cursorLocation.Y - currentControl.origCursorLocation.Y);

            #endregion // Calculate Offsets

            #region Move Drag Element

            if (this._modifyLeftOffset)
            {
                Canvas.SetLeft(control, newHorizontalOffset);
                Console.WriteLine("left:" + newHorizontalOffset.ToString());
            }
            else
            {
                Canvas.SetRight(control, newHorizontalOffset);
                Console.WriteLine("right:" + newHorizontalOffset.ToString());
            }

            if (this._modifyTopOffset)
                Canvas.SetTop(control, newVerticalOffset);
            else
                Canvas.SetBottom(control, newVerticalOffset);

            #endregion Move Drag Element
        }

        #endregion

            #region InternalOnPreviewMouseUp

        /// <summary>
        /// Allows external objects to use the canvas drag functionality
        /// </summary>
        public void InternalOnPreviewMouseUp()
        {
            // Reset the field whether the left or right mouse button was 
            // released, in case a context menu was opened on the drag element.            
            this.ElementBeingDragged = null;
            this._isDragInProgress = false;
        }

        #endregion

        #region InternalOnPreviewMouseWheel

        public void InternalOnPreviewMouseWheel(int delta)
        {
            if (0 < delta)
            {
                Scale += 0.1;
            }
            else
            {
                Scale -= 0.1;
            }
        }

        #endregion

        #endregion // Interface

        #region Overrides

        #region OnPreviewMouseWheel

        protected override void OnPreviewMouseWheel(MouseWheelEventArgs e)
        {
            base.OnPreviewMouseWheel(e);

            InternalOnPreviewMouseWheel(e.Delta);

            e.Handled = true;
        }

        #endregion

            #region OnPreviewMouseLeftButtonDown

        protected override void OnPreviewMouseLeftButtonDown(MouseButtonEventArgs e)
		{
			base.OnPreviewMouseLeftButtonDown( e );

            Cursor = Cursors.Arrow;

			// Walk up the visual tree from the element that was clicked, 
			// looking for an element that is a direct child of the Canvas.
			this.ElementBeingDragged = this.FindCanvasChild( e.Source as DependencyObject );
			if( this.ElementBeingDragged == null )
				return;

            InternalPreviewMouseLeftButtonDown(e.GetPosition(this));

			// Set the Handled flag so that a control being dragged 
			// does not react to the mouse input.
			e.Handled = true;
		}

			#endregion // OnPreviewMouseLeftButtonDown

			#region OnPreviewMouseMove

		protected override void OnPreviewMouseMove( MouseEventArgs e )
		{
			base.OnPreviewMouseMove( e );

            InternalPreviewMouseMove(this.ElementBeingDragged, e.GetPosition(this));

		}

			#endregion // OnPreviewMouseMove

            #region OnPreviewMouseUp

		protected override void OnPreviewMouseUp( MouseButtonEventArgs e )
		{
			base.OnPreviewMouseUp( e );

            InternalOnPreviewMouseUp();
		}

			#endregion // OnPreviewMouseUp

        #endregion // Overrides

        #region Private Helpers

			#region ResolveOffset

		/// <summary>
		/// Determines one component of a UIElement's location 
		/// within a Canvas (either the horizontal or vertical offset).
		/// </summary>
		/// <param name="side1">
		/// The value of an offset relative to a default side of the 
		/// Canvas (i.e. top or left).
		/// </param>
		/// <param name="side2">
		/// The value of the offset relative to the other side of the 
		/// Canvas (i.e. bottom or right).
		/// </param>
		/// <param name="useSide1">
		/// Will be set to true if the returned value should be used 
		/// for the offset from the side represented by the 'side1' 
		/// parameter.  Otherwise, it will be set to false.
		/// </param>
		private static double ResolveOffset( double side1, double side2, out bool useSide1 )
		{
			// If the Canvas.Left and Canvas.Right attached properties 
			// are specified for an element, the 'Left' value is honored.
			// The 'Top' value is honored if both Canvas.Top and 
			// Canvas.Bottom are set on the same element.  If one 
			// of those attached properties is not set on an element, 
			// the default value is Double.NaN.
			useSide1 = true;
			double result;
			if( Double.IsNaN( side1 ) )
			{
				if( Double.IsNaN( side2 ) )
				{
					// Both sides have no value, so set the
					// first side to a value of zero.
					result = 0;
				}
				else
				{
					result = side2;
					useSide1 = false;
				}
			}
			else
			{
				result = side1;
			}
			return result;
		}

			#endregion // ResolveOffset

            #region CreateObjectData

        private void CreateObjectData(Point currentLocation)
        {
            _objectList.Clear();

            // Create object data for each child, then we can look this up later
            for (int i = 0; i < Children.Count; ++i)
            {
                ObjectData newObjectData = new ObjectData();
                newObjectData.control = Children[i];

                // Cache the mouse cursor location.
                this._origCursorLocation = currentLocation;

                // Get the element's offsets from the four sides of the Canvas.
                double left = Canvas.GetLeft(Children[i]);
                double right = Canvas.GetRight(Children[i]);
                double top = Canvas.GetTop(Children[i]);
                double bottom = Canvas.GetBottom(Children[i]);

                // Calculate the offset deltas and determine for which sides
                // of the Canvas to adjust the offsets.
                newObjectData.origHorizOffset = ResolveOffset(left, right, out this._modifyLeftOffset);
                newObjectData.origVertOffset = ResolveOffset(top, bottom, out this._modifyTopOffset);
                newObjectData.origCursorLocation = this._origCursorLocation;

                _objectList.Add(newObjectData);
            }
        }

        #endregion

            #region FindCurrentControl
        
        private ObjectData FindCurrentControl(UIElement control)
        {
            for (int i = 0; i < _objectList.Count; ++i)
            {
                if (_objectList[i].control == control)
                {
                    return _objectList[i];
                }
            }

            return null;
        }

        #endregion

        #endregion // Private Helpers
    }
}