﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workbench.AddIn.UI.Layout;

namespace ReportBrowser.Addin
{
    /// <summary>
    /// Interface for reports that generate an avalon document
    /// </summary>
    public interface IReportDocumentProvider
    {
        /// <summary>
        /// Document to use for displaying this report
        /// </summary>
        IDocumentBase Document
        {
            get;
        }
    }
} // ReportBrowser.Addin
