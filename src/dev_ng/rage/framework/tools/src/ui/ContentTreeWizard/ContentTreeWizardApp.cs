﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using RSG.Base.Logging;
using RSG.Editor.Controls;

namespace ContentTreeWizard
{
    /// <summary>
    /// 
    /// </summary>
    public class ContentTreeWizardApp : RsApplication
    {
        #region Program Entry Point
        /// <summary>
        /// Main entry point into the application. The first thing to get called.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            ContentTreeWizardApp app = new ContentTreeWizardApp();
            app.ShutdownMode = ShutdownMode.OnLastWindowClose;
            RsApplication.OnEntry(app, ApplicationMode.Single);
        }
        #endregion

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ContentTreeWizardApp()
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        protected override RSG.Editor.Controls.Exceptions.ExceptionWindowButtons ExceptionWindowButtons
        {
            get
            {
                return RSG.Editor.Controls.Exceptions.ExceptionWindowButtons.All;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override string UniqueName
        {
            get { return "Content Tree Wizard"; }
        }

        /// <summary>
        /// 
        /// </summary>
        public override string AuthorEmailAddress
        {
            get { return "*tools@rockstarnorth.com"; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Override to create the main window for the application.
        /// </summary>
        /// <returns>
        /// The System.Windows.Window that will be the main window for this application.
        /// </returns>
        protected override Window CreateMainWindow()
        {
            return new MainWindow(LogFactory.ApplicationLog);
        }

        /// <summary>
        /// Makes sure that the <see cref="HandleWindowsClosing"/> method is called if the
        /// main window is closing.
        /// </summary>
        /// <param name="windows">
        /// The windows that are to be tested.
        /// </param>
        /// <returns>
        /// True if the specified windows can be closed without user interaction; otherwise,
        /// false.
        /// </returns>
        public override bool CanCloseWithoutInteraction(ISet<Window> windows)
        {
            // Always need to handle the main window closing.
            return !windows.Contains(this.MainWindow);
        }

        /// <summary>
        /// Handles the specified windows being closed. This is called after the method
        /// <see cref="CanCloseWithoutInteraction"/> returns false.
        /// </summary>
        /// <param name="windows">
        /// The windows to handle.
        /// </param>
        /// <param name="e">
        /// A instance of CancelEventArgs that can be used to cancel the closing of the
        /// specified windows.
        /// </param>
        public override void HandleWindowsClosing(ISet<Window> windows, CancelEventArgs e)
        {
            if (e.Cancel)
            {
                return;
            }

            MainWindow mainWindow = this.MainWindow as MainWindow;
            if (mainWindow == null)
            {
                Debug.Fail("Incorrect cast of main window.");
                return;
            }

            if (!mainWindow.WizardCompleted)
            {
                MessageBoxResult result =
                    RsMessageBox.Show(
                    ContentTreeWizard.Resources.StringTable.MainWindow_ConfirmCancelMessage,
                    this.ProductName,
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Question,
                    MessageBoxResult.No);

                if (result == MessageBoxResult.No)
                {
                    e.Cancel = true;
                    return;
                }
            }

            Environment.ExitCode = 2;
        }
        #endregion // Methods
    }
}
