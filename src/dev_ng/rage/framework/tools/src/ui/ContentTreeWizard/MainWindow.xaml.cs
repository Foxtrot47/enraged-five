﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using P4API;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Editor.Controls;
using RSG.Editor.Controls.Wizard;
using RSG.Pipeline.Content;
using RSG.SourceControl.Perforce;

namespace ContentTreeWizard
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : RsMainWindow
    {
        #region Field
        /// <summary>
        /// Private field for the <see cref="WizardCompleted"/> property.
        /// </summary>
        private bool _wizardCompleted = false;

        /// <summary>
        /// Local log object.
        /// </summary>
        private readonly ILog _log;
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Initialise a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow(ILog log)
        {
            _log = log;

            InitializeComponent();

            this.DataContext = new MainWindowViewModel();
            this.Wizard.ActivePage = this.Wizard.Pages.First();
            this.Wizard.PageChanging += WizardPageChanging;
            this.Wizard.PageChanged += WizardPageChanged;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Whether the wizard has run to completion.
        /// </summary>
        public bool WizardCompleted
        {
            get { return _wizardCompleted; }
        }
        #endregion

        #region Event Handlers
        /// <summary>
        /// Wizard page changing event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WizardPageChanging(Object sender, WizardPageChangingEventArgs e)
        {
            MainWindowViewModel vm = (MainWindowViewModel)this.DataContext;
            if (e.FromPage.Header == ContentTreeWizard.Resources.StringTable.BranchSelectionPage_Header &&
                e.ToPage.Header == ContentTreeWizard.Resources.StringTable.FileSelectionPage_Header)
            {
                OptionallySyncContentTree(vm.SelectedProject.Project, vm.SelectedBranch.Branch);
                vm.UpdateFileViewModelFlags();
            }
            else if (e.FromPage.Header == ContentTreeWizard.Resources.StringTable.FileSelectionPage_Header &&
                e.ToPage.Header == ContentTreeWizard.Resources.StringTable.TargetFolderPage_Header)
            {
                IEnumerable<FileViewModel> nonRootedFiles = vm.Files.Where(item => item.NotRootedUnderArt);
                if (nonRootedFiles.Any())
                {
                    String message = String.Format(
                        ContentTreeWizard.Resources.StringTable.FileSelectionPage_NonRootedFilesMessage,
                        vm.SelectedBranch.Branch.Art,
                        String.Join("\n", nonRootedFiles.Select(item => item.Filepath)));

                    RsMessageBox.Show(
                        this,
                        message,
                        ContentTreeWizard.Resources.StringTable.FileSelectionPage_NonRootedFilesCaption,
                        MessageBoxButton.OK,
                        MessageBoxImage.Stop);
                    e.Cancel = true;
                }
                else
                {
                    // Inform the user if some of the items are already in the content tree.
                    IEnumerable<FileViewModel> existingFiles = vm.Files.Where(item => item.ExistsInContentTree);
                    int existingFileCount = existingFiles.Count();

                    if (existingFileCount == vm.Files.Count)
                    {
                        RsMessageBox.Show(
                            this,
                            ContentTreeWizard.Resources.StringTable.FileSelectionPage_AllFilesInContentTreeMessage,
                            ContentTreeWizard.Resources.StringTable.FileSelectionPage_AllFilesInContentTreeCaption,
                            MessageBoxButton.OK,
                            MessageBoxImage.Stop);
                        e.Cancel = true;
                    }
                    else if (existingFiles.Any())
                    {
                        String message = String.Format(
                            ContentTreeWizard.Resources.StringTable.FileSelectionPage_SomeFilesInContentTreeMessage,
                            existingFileCount,
                            vm.Files.Count,
                            String.Join("\n", existingFiles.Select(item => item.Filepath)));

                        MessageBoxResult result = RsMessageBox.Show(
                            this,
                            message,
                            ContentTreeWizard.Resources.StringTable.FileSelectionPage_SomeFilesInContentTreeCaption,
                            MessageBoxButton.YesNo,
                            MessageBoxImage.Question,
                            MessageBoxResult.No);
                        if (result == MessageBoxResult.No)
                        {
                            e.Cancel = true;
                        }
                    }
                }
            }
            else if (e.FromPage.Header == ContentTreeWizard.Resources.StringTable.TargetFolderPage_Header &&
                e.ToPage.Header == ContentTreeWizard.Resources.StringTable.SummaryPage_Header)
            {
                if (!vm.ValidateTargetFolder())
                {
                    e.Cancel = true;
                }
            }
        }

        /// <summary>
        /// Wizard page changed event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WizardPageChanged(Object sender, WizardPageChangedEventArgs e)
        {
        }
        #endregion

        #region Wizard Command Handlers
        /// <summary>
        /// Wizard next navigation.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WizardNext(Object sender, ExecutedRoutedEventArgs e)
        {
            if (this.Wizard.NavigateNext())
            {
                return;
            }

            if (this.Wizard.ActivePage == null)
            {
                return;
            }
        }

        /// <summary>
        /// Wizard previous navigation.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WizardPrevious(Object sender, ExecutedRoutedEventArgs e)
        {
            this.Wizard.NavigatePrevious();
        }

        /// <summary>
        /// Wizard cancellation.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WizardCancel(Object sender, ExecutedRoutedEventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Wizard finish; add the required items to the content tree.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WizardFinish(Object sender, ExecutedRoutedEventArgs e)
        {
            MainWindowViewModel vm = (MainWindowViewModel)this.DataContext;
            if (vm.AddFilesToContentTree())
            {
                RsMessageBox.Show(ContentTreeWizard.Resources.StringTable.SummaryPage_UpdateSuccess,
                           this.Title, MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                RsMessageBox.Show(ContentTreeWizard.Resources.StringTable.SummaryPage_UpdateFailed,
                    this.Title, MessageBoxButton.OK, MessageBoxImage.Error);
            }

            _wizardCompleted = true;
            Close();
        }
        #endregion

        #region Content Tree Syncing
        /// <summary>
        /// Optionally sync (prompt) the Content-Tree XML data.
        /// </summary>
        /// <param name="project"></param>
        /// <param name="branch"></param>
        private void OptionallySyncContentTree(IProject project, IBranch branch)
        {
            String[] contentTreeFilenames = GetAllContentTreeFilenames(project, branch);
            using (RSG.SourceControl.Perforce.P4 p4 = project.SCMConnect())
            {
                IEnumerable<FileMapping> fms = FileMapping.Create(p4, contentTreeFilenames);
                IEnumerable<String> depotFilenames = fms.Select(fm => fm.DepotFilename);
                List<String> syncArgs = new List<String>();
                syncArgs.Add("-n"); // preview sync
                syncArgs.AddRange(depotFilenames);
                P4RecordSet previewResult = p4.Run("sync", false, syncArgs.ToArray());
                List<String> oldDepotFilenames = previewResult.Records.
                     Where(record => record.Fields.ContainsKey("depotFile")).
                     Select(record => record.Fields["depotFile"]).
                     ToList();

                System.Windows.MessageBoxResult userResult = System.Windows.MessageBoxResult.Yes;
                while (oldDepotFilenames.Count() > 0 && System.Windows.MessageBoxResult.Yes == userResult)
                {
                    int taken = Math.Min(oldDepotFilenames.Count(), 5);
                    String message = String.Format(
                        ContentTreeWizard.Resources.StringTable.FileSelectionPage_ContentTreeSyncMessage,
                        project.FriendlyName,
                        branch.Name,
                        oldDepotFilenames.Count(),
                        String.Join("\n", oldDepotFilenames.Take(taken)),
                        oldDepotFilenames.Count() > taken ? ", ..." : String.Empty);

                    userResult = RsMessageBox.Show(
                        this,
                        message,
                        ContentTreeWizard.Resources.StringTable.FileSelectionPage_ContentTreeSyncCaption,
                        System.Windows.MessageBoxButton.YesNo,
                        System.Windows.MessageBoxImage.Question,
                        System.Windows.MessageBoxResult.No);
                    if (System.Windows.MessageBoxResult.Yes == userResult)
                    {
                        TrySyncFiles(p4, depotFilenames.ToArray());
                        contentTreeFilenames = GetAllContentTreeFilenames(project, branch);

                        fms = FileMapping.Create(p4, contentTreeFilenames);
                        depotFilenames = fms.Select(fm => fm.DepotFilename);
                        syncArgs = new List<String>();
                        syncArgs.Add("-n"); // preview sync
                        syncArgs.AddRange(depotFilenames);
                        previewResult = p4.Run("sync", false, syncArgs.ToArray());
                        oldDepotFilenames = previewResult.Records.
                             Where(record => record.Fields.ContainsKey("depotFile")).
                             Select(record => record.Fields["depotFile"]).
                             ToList();
                    }
                }
            }
        }

        /// <summary>
        /// Attempt to sync files from Perforce; logging errors if required.
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="depotFilenames"></param>
        /// <returns></returns>
        private bool TrySyncFiles(P4 p4, String[] depotFilenames)
        {
            bool result = true;
            P4RecordSet fileSyncRecords = p4.Run("sync", depotFilenames);
            if (fileSyncRecords.Errors.Any())
            {
                _log.Error("{0} errors syncing dependencies from Perforce:",
                    fileSyncRecords.Errors.Count());
                fileSyncRecords.Errors.ForEachWithIndex((s, i) =>
                    _log.Error("\t{0}: {1}", i + 1, s));
                result = false;
            }
            return (result);
        }

        /// <summary>
        /// Return all content-tree files for both core and DLC branches.
        /// </summary>
        /// <param name="project"></param>
        /// <param name="branch"></param>
        /// <returns></returns>
        private String[] GetAllContentTreeFilenames(IProject project, IBranch branch)
        {
            List<String> contentTreeFilenames = new List<String>();
            if (project != project.Config.CoreProject)
            {
                IBranch coreBranch = project.Config.CoreProject.Branches[branch.Name];
                contentTreeFilenames.AddRange(Factory.GetFilenames(coreBranch));
            }
            contentTreeFilenames.AddRange(Factory.GetFilenames(branch));
            return (contentTreeFilenames.ToArray());
        }
        #endregion
    }
}
