﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Editor;

namespace ContentTreeWizard
{
    /// <summary>
    /// View model for a single 3ds max file.
    /// </summary>
    public class FileViewModel : NotifyPropertyChangedBase
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Filepath"/> property.
        /// </summary>
        private readonly String _filepath;

        /// <summary>
        /// Private field for the <see cref="ExistsInContentTree"/> property.
        /// </summary>
        private bool _existsInContentTree;

        /// <summary>
        /// Private field for the <see cref="IsRootedUnderArt"/> property.
        /// </summary>
        private bool _isRootedUnderArt;

        /// <summary>
        /// Private field for the <see cref="SceneType"/> property.
        /// </summary>
        private SceneType _sceneType = SceneType.Map;

        /// <summary>
        /// Private field for the <see cref="IsContainerProps"/> property.
        /// </summary>
        private readonly bool _isContainerProps = false;

        /// <summary>
        /// Private field for the <see cref="Prefix"/> property.
        /// </summary>
        private String _prefix = String.Empty;
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="FileViewModel"/> class
        /// using the specified filepath.
        /// </summary>
        /// <param name="filepath"></param>
        /// <param name="existsInContentTree"></param>
        public FileViewModel(String filepath, bool existsInContentTree, bool isRootedUnderArt)
        {
            _filepath = filepath.ToLower();
            _existsInContentTree = existsInContentTree;
            _isRootedUnderArt = isRootedUnderArt;
            _prefix = Path.GetFileNameWithoutExtension(filepath);

            String[] components = _filepath.Split(new char[] { '\\' });

            // Try and determine the type of container this is based on the filepath.
            if (components.Contains("interiors"))
            {
                _sceneType = SceneType.Interior;
            }
			else if (components.Contains("weapons"))
			{
				_sceneType = SceneType.Weapon;
			}
            else if (components.Contains("props"))
            {
                _sceneType = SceneType.Props;
            }
			else if (components.Contains("peds"))
            {
                _sceneType = SceneType.PedOutfit;
			}

            String filename = Path.GetFileNameWithoutExtension(_filepath);
            if (filename.EndsWith("props"))
            {
                _isContainerProps = true;
            }
            else if (filename.EndsWith("lod"))
            {
                _sceneType = SceneType.LOD;
            }
            else if (filename.EndsWith("occl"))
            {
                _sceneType = SceneType.Occlusion;
            }
        }
        #endregion

        #region Properties
        /// <summary>
        /// Path to the max file that is being added.
        /// </summary>
        public String Filepath
        {
            get { return _filepath; }
        }

        /// <summary>
        /// Flag indicating whether a file already exists in the content tree.
        /// </summary>
        public bool ExistsInContentTree
        {
            get { return _existsInContentTree; }
            set { SetProperty(ref _existsInContentTree, value); }
        }

        /// <summary>
        /// Flag indicating whether the file is located under the project's $(art) directory.
        /// </summary>
        public bool IsRootedUnderArt
        {
            get { return _isRootedUnderArt; }
            set { SetProperty(ref _isRootedUnderArt, value, "IsRootedUnderArt", "NotRootedUnderArt"); }
        }

        /// <summary>
        /// Inverse of the <see cref="IsRootedUnderArt"/> property.
        /// </summary>
        public bool NotRootedUnderArt
        {
            get { return !_isRootedUnderArt; }
        }

        /// <summary>
        /// Type of container this file is.
        /// </summary>
        public SceneType SceneType
        {
            get { return _sceneType; }
            set { SetProperty(ref _sceneType, value); }
        }

        /// <summary>
        /// Flag indicating whether this is a container props file.
        /// </summary>
        public bool IsContainerProps
        {
            get { return _isContainerProps; }
        }

        /// <summary>
        /// Prefix to give this element.
        /// </summary>
        public String Prefix
        {
            get { return _prefix; }
            set { SetProperty(ref _prefix, value); }
        }
        #endregion
    }
}
