﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Attributes;

namespace ContentTreeWizard
{
    /// <summary>
    /// Specifies the type of scene for a 3ds max file.
    /// This is a subset of SceneXml's SceneType enum.  Ideally we would make use of
    /// that instead, but I didn't want to bring in a project dependency on managed rage.
    /// </summary>
	public enum SceneType
	{
		[FieldDisplayName("Map Container")]
		[XmlConstant("map_container")]
		Map,

		[FieldDisplayName("Interior")]
		[XmlConstant("interior")]
		Interior,

		[FieldDisplayName("Props")]
		[XmlConstant("props")]
		Props,

		[FieldDisplayName("LOD Container")]
		[XmlConstant("lod_container")]
		LOD,

		[FieldDisplayName("Occlusion Container")]
		[XmlConstant("occl_container")]
		Occlusion,

		[FieldDisplayName("Ped (Whole)")]
		[XmlConstant("character")]
		Ped,

		[FieldDisplayName("Ped (Outfit)")]
		[XmlConstant("character")]
		PedOutfit,

		[FieldDisplayName("Weapon")]
		[XmlConstant("weapon")]
		Weapon

	}
}
