﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Editor;

namespace ContentTreeWizard
{
    /// <summary>
    /// View model for a <see cref="IProject"/> configuration object.
    /// </summary>
    public class ProjectViewModel : NotifyPropertyChangedBase
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Project"/> property.
        /// </summary>
        private readonly IProject _project;

        /// <summary>
        /// Private field for the <see cref="Branches"/> property.
        /// </summary>
        private readonly IList<BranchViewModel> _branches = new List<BranchViewModel>();
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="ProjectViewModel"/> class using
        /// the specified project.
        /// </summary>
        /// <param name="project"></param>
        public ProjectViewModel(IProject project)
        {
            _project = project;

            foreach (IBranch branch in project.Branches.Values)
            {
                _branches.Add(new BranchViewModel(branch));
            }
        }
        #endregion

        #region Properties
        /// <summary>
        /// Reference to the project this view model is for.
        /// </summary>
        public IProject Project
        {
            get { return _project; }
        }

        /// <summary>
        /// Friendly name for this project.
        /// </summary>
        public String Name
        {
            get { return _project.FriendlyName; }
        }

        /// <summary>
        /// List of branches this project supports.
        /// </summary>
        public IReadOnlyList<BranchViewModel> Branches
        {
            get { return (IReadOnlyList<BranchViewModel>)_branches; }
        }
        #endregion
    }
}
