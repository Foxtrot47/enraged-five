﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RSG.Editor.Controls.Wizard;

namespace ContentTreeWizard.WizardPages
{
    /// <summary>
    /// Interaction logic for FileSelectionPage.xaml
    /// </summary>
    public partial class FileSelectionPage : WizardPage
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="FileSelectionPage"/> control.
        /// </summary>
        public FileSelectionPage()
        {
            InitializeComponent();

            // Make sure the files data grid is sorted by the filepath by default.
            SortDescription description = new SortDescription("Filepath", ListSortDirection.Ascending);
            FilesDataGrid.Items.SortDescriptions.Add(description);
        }
        #endregion

        #region Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextureDataGrid_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                String[] files = (String[])e.Data.GetData(DataFormats.FileDrop);
                MainWindowViewModel vm = this.DataContext as MainWindowViewModel;
                if (vm == null)
                {
                    Debug.Fail("Data context isn't valid.");
                    throw new ArgumentException("Data context isn't valid.");
                }
                vm.AddFiles(files.Where(file => MaxFileUtil.HasSupportedFileExtension(file)));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextureDataGrid_DragOver(object sender, DragEventArgs e)
        {
            if (!e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effects = DragDropEffects.None;
                e.Handled = true;
            }
            else
            {
                String[] files = (String[])e.Data.GetData(DataFormats.FileDrop);
                if (!files.Any(file => MaxFileUtil.HasSupportedFileExtension(file)))
                {
                    e.Effects = DragDropEffects.None;
                    e.Handled = true;
                }
            }
        }
        #endregion // Event Handlers
    }
}
