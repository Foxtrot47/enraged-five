﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContentTreeWizard
{
    /// <summary>
    /// Utility class for dealing with texture files.
    /// </summary>
    public static class MaxFileUtil
    {
        /// <summary>
        /// Supported max file extensions.
        /// </summary>
        public static readonly String[] SupportedExtensions =
            new String[]
            {
                ".max",
                ".maxc"
            };

        /// <summary>
        /// Filter string for the open file dialog box.
        /// </summary>
        public static readonly String OpenFileDialogFilter =
            "All Supported File Types (*.max, *.maxc)|*.max;*.maxc|3ds Max File (*.max)|*.max|3ds Max Container Definition (*.maxc)|*.maxc|All Files (*.*)|*.*";

        /// <summary>
        /// Returns whether the provided filepath has one of the supported extensions.
        /// </summary>
        /// <param name="filepath"></param>
        /// <returns></returns>
        public static bool HasSupportedFileExtension(String filepath)
        {
            String ext = Path.GetExtension(filepath).ToLower();
            return SupportedExtensions.Contains(ext);
        }

    } // TextureUtil
}
