﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Configuration;
using RSG.Editor;

namespace ContentTreeWizard
{
    /// <summary>
    /// View model for a <see cref="IBranch"/> configuration object.
    /// </summary>
    public class BranchViewModel : NotifyPropertyChangedBase
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Branch"/> property.
        /// </summary>
        private readonly IBranch _branch;
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="BranchViewModel"/> class
        /// using the specified branch.
        /// </summary>
        /// <param name="branch"></param>
        public BranchViewModel(IBranch branch)
        {
            _branch = branch;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Reference to the branch config object this view model is for.
        /// </summary>
        public IBranch Branch
        {
            get { return _branch; }
        }

        /// <summary>
        /// Name of the branch.
        /// </summary>
        public String Name
        {
            get { return _branch.Name; }
        }
        #endregion
    }
}
