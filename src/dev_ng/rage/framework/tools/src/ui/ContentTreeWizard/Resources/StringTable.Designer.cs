﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ContentTreeWizard.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class StringTable {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal StringTable() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("ContentTreeWizard.Resources.StringTable", typeof(StringTable).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Branch:.
        /// </summary>
        public static string BranchSelectionPage_Branch {
            get {
                return ResourceManager.GetString("BranchSelectionPage_Branch", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Branch Selection.
        /// </summary>
        public static string BranchSelectionPage_Header {
            get {
                return ResourceManager.GetString("BranchSelectionPage_Header", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Project:.
        /// </summary>
        public static string BranchSelectionPage_Project {
            get {
                return ResourceManager.GetString("BranchSelectionPage_Project", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please select the project/branch that you wish to add these files to..
        /// </summary>
        public static string BranchSelectionPage_Text {
            get {
                return ResourceManager.GetString("BranchSelectionPage_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add File(s).
        /// </summary>
        public static string FileSelectionPage_AddFileButton {
            get {
                return ResourceManager.GetString("FileSelectionPage_AddFileButton", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to All files in Content Tree.
        /// </summary>
        public static string FileSelectionPage_AllFilesInContentTreeCaption {
            get {
                return ResourceManager.GetString("FileSelectionPage_AllFilesInContentTreeCaption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unable to continue as all of the max files you have specified are already in the content tree.
        ///
        ///Please add files that aren&apos;t yet in the content tree before attempting to continue..
        /// </summary>
        public static string FileSelectionPage_AllFilesInContentTreeMessage {
            get {
                return ResourceManager.GetString("FileSelectionPage_AllFilesInContentTreeMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sync Content Tree Files?.
        /// </summary>
        public static string FileSelectionPage_ContentTreeSyncCaption {
            get {
                return ResourceManager.GetString("FileSelectionPage_ContentTreeSyncCaption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} {1} Content-Tree is out-of-date.  Would you like to sync {2} files?
        ///
        ///{3}{4}.
        /// </summary>
        public static string FileSelectionPage_ContentTreeSyncMessage {
            get {
                return ResourceManager.GetString("FileSelectionPage_ContentTreeSyncMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to File already exists in content tree..
        /// </summary>
        public static string FileSelectionPage_FileAlreadyInContentTree {
            get {
                return ResourceManager.GetString("FileSelectionPage_FileAlreadyInContentTree", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The file you have selected isn&apos;t under the selected branches art directory (&apos;{0}&apos;).
        ///
        ///Please verify that you&apos;ve selected the correct file..
        /// </summary>
        public static string FileSelectionPage_FileNotRootedUnderArt {
            get {
                return ResourceManager.GetString("FileSelectionPage_FileNotRootedUnderArt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to File Selection.
        /// </summary>
        public static string FileSelectionPage_Header {
            get {
                return ResourceManager.GetString("FileSelectionPage_Header", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Files not in Branch Art Directory.
        /// </summary>
        public static string FileSelectionPage_NonRootedFilesCaption {
            get {
                return ResourceManager.GetString("FileSelectionPage_NonRootedFilesCaption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Some of the files you have selected aren&apos;t under the branches art directory (&apos;{0}&apos;):
        ///{1}
        ///
        ///Please remove these files prior to moving to the next step..
        /// </summary>
        public static string FileSelectionPage_NonRootedFilesMessage {
            get {
                return ResourceManager.GetString("FileSelectionPage_NonRootedFilesMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Remove Selected.
        /// </summary>
        public static string FileSelectionPage_RemoveSelectedButton {
            get {
                return ResourceManager.GetString("FileSelectionPage_RemoveSelectedButton", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Some files in Content Tree.
        /// </summary>
        public static string FileSelectionPage_SomeFilesInContentTreeCaption {
            get {
                return ResourceManager.GetString("FileSelectionPage_SomeFilesInContentTreeCaption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} of {1} selected max files are already in the content tree:
        ///{2}
        ///
        ///Do you wish to continue and add only those that aren&apos;t yet in the content tree?.
        /// </summary>
        public static string FileSelectionPage_SomeFilesInContentTreeMessage {
            get {
                return ResourceManager.GetString("FileSelectionPage_SomeFilesInContentTreeMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please specify the files that you wish to add to the content tree either through using the &apos;Add File(s)&apos; button or by dragging and dropping max files from explorer into the datagrid.
        ///
        ///Once files have been added, please check that the container type is correct before proceeding..
        /// </summary>
        public static string FileSelectionPage_Text {
            get {
                return ResourceManager.GetString("FileSelectionPage_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Welcome.
        /// </summary>
        public static string IntroductionPage_Header {
            get {
                return ResourceManager.GetString("IntroductionPage_Header", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Welcome to the Rockstar Games Content Tree Wizard.
        ///
        ///This wizard will help you to add new containers to the content tree.
        ///
        ///Click Next to continue..
        /// </summary>
        public static string IntroductionPage_Text {
            get {
                return ResourceManager.GetString("IntroductionPage_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Are you sure you want to exit the content tree wizard?
        ///
        ///The content tree won&apos;t be updated with any changes you have selected so far..
        /// </summary>
        public static string MainWindow_ConfirmCancelMessage {
            get {
                return ResourceManager.GetString("MainWindow_ConfirmCancelMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Content Tree Wizard.
        /// </summary>
        public static string MainWindow_Title {
            get {
                return ResourceManager.GetString("MainWindow_Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Summary.
        /// </summary>
        public static string SummaryPage_Header {
            get {
                return ResourceManager.GetString("SummaryPage_Header", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ContentTreeWizard autogenerated content tree modifications.
        ///
        ///Adding:
        ///{0}.
        /// </summary>
        public static string SummaryPage_P4ChangelistDescription {
            get {
                return ResourceManager.GetString("SummaryPage_P4ChangelistDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The wizard has now gathered the required information to add the {0} max files to the content tree.
        ///
        ///Click &apos;Finish&apos; to update the content tree..
        /// </summary>
        public static string SummaryPage_Text {
            get {
                return ResourceManager.GetString("SummaryPage_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The wizard was not able to successfully update the content tree.
        ///
        ///Contact your local tools support group..
        /// </summary>
        public static string SummaryPage_UpdateFailed {
            get {
                return ResourceManager.GetString("SummaryPage_UpdateFailed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The content tree was successfully updated.
        ///
        ///The wizard will now close..
        /// </summary>
        public static string SummaryPage_UpdateSuccess {
            get {
                return ResourceManager.GetString("SummaryPage_UpdateSuccess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please specify the folder where you would like the converted RPFs to be output to.
        ///
        ///This needs to start with $(target) which will be automatically replaced by the per platform output folder during conversion by the asset pipeline (e.g. $(target)\levels\gta5\citye would result in {0}\ps4\levels\gta5\citye for the Playstation 4).
        ///
        ///If you browse for the folder via the provided button it will automatically attempt to determine this for you if you select a platform specific folder..
        /// </summary>
        public static string TargetFolderPage_DestinationText {
            get {
                return ResourceManager.GetString("TargetFolderPage_DestinationText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Target Folder.
        /// </summary>
        public static string TargetFolderPage_Header {
            get {
                return ResourceManager.GetString("TargetFolderPage_Header", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Folder must start with $(target)..
        /// </summary>
        public static string TargetFolderPage_InvalidFolder {
            get {
                return ResourceManager.GetString("TargetFolderPage_InvalidFolder", resourceCulture);
            }
        }
    }
}
