using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace FSTriggerScript
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();

            Program.Controller.EnableChanged(this.cbChanged.Checked);
            Program.Controller.EnableCreated(this.cbCreated.Checked);
            Program.Controller.EnableDeleted(this.cbDeleted.Checked);
            Program.Controller.EnableRenamed(this.cbRenamed.Checked);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Instantiate a Filesystem watcher.
            if (this.button1.Text == "Start Watch")
            {
                Program.Controller.StartWatch();
                this.button1.Text = "Stop Watch";      
            }
            else
            {
                this.button1.Text = "Start Watch";
                Program.Controller.StopWatch();
            }
        }

        private delegate void AddOutputCallback(String str, eOutputLevel eLevel);

        public void AddOutputSafe(String str, eOutputLevel eLevel)
        {
            if (this.listBox1.InvokeRequired)
            {
                AddOutputCallback d = new AddOutputCallback(AddOutputSafe);
                this.Invoke(d, new object[] { str, eLevel });
            }
            else
            {
                String prefix = "";
                switch (eLevel)
                {
                    case eOutputLevel.Info:
                        prefix = String.Format("INFO : ");
                        break;
                    case eOutputLevel.Warning:
                        prefix = String.Format("WARN : ");
                        break;
                    case eOutputLevel.Error:
                        prefix = String.Format("ERROR : ");
                        break;
                    default: // deliberately blank
                        break;
                }
                this.listBox1.Items.Add(prefix + str);
                this.listBox1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
                this.listBox1.DrawItem += new System.Windows.Forms.DrawItemEventHandler(listBox1_DrawItem);       
            }
        }        

        private void listBox1_DrawItem(object sender, DrawItemEventArgs e)
        {
            Brush brush = Brushes.Blue;            
            if (this.listBox1.Items[e.Index].ToString().StartsWith("INFO"))
                brush = Brushes.Green;
            else if (this.listBox1.Items[e.Index].ToString().StartsWith("WARN"))
                brush = Brushes.Orange;
            else if (this.listBox1.Items[e.Index].ToString().StartsWith("ERROR"))
                brush = Brushes.Red;

            e.Graphics.DrawString(this.listBox1.Items[e.Index].ToString(), e.Font, brush, e.Bounds);
        }

        private void Changed(object sender, EventArgs e)
        {
            Program.Controller.EnableChanged(this.cbChanged.Checked);
        }

        private void Create(object sender, EventArgs e)
        {
            Program.Controller.EnableCreated(this.cbCreated.Checked);
        }

        private void Deleted(object sender, EventArgs e)
        {
            Program.Controller.EnableDeleted(this.cbDeleted.Checked);
        }

        private void Renamed(object sender, EventArgs e)
        {
            Program.Controller.EnableRenamed(this.cbRenamed.Checked);
        }        
        
        public System.Windows.Forms.ListBox ListBox1 { get { return listBox1; } }
        public System.Windows.Forms.TextBox TextBox2 { get { return textBox2; } }
        public System.Windows.Forms.Button Button1 { get { return button1; } }
        public System.Windows.Forms.PropertyGrid PropertyGrid1 { get { return propertyGrid1; } }
    }
}