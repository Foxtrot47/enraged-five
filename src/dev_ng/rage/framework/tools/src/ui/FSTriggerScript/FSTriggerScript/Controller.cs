using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Diagnostics;

namespace FSTriggerScript
{
    #region enum
    public enum eOutputLevel
    {
        Regular = 0,
        Info,
        Warning,
        Error
    };
    #endregion // enum

    public class Controller
    {
        public FileSystemWatcher FileSystemWatcher { get { return m_fileSystemWatcher; } }
        private FileSystemWatcher m_fileSystemWatcher;

        public Controller()
        {
            m_fileSystemWatcher = new FileSystemWatcher();
            m_fileSystemWatcher.Path = "C:\\";
            m_fileSystemWatcher.Filter = "*.txt";
            m_fileSystemWatcher.IncludeSubdirectories=true;
        }

        public void EnableChanged(bool b)
        {
            if (b)
                m_fileSystemWatcher.Changed += new System.IO.FileSystemEventHandler(fileSystemWatcher_Changed);
            else
                m_fileSystemWatcher.Changed -= new System.IO.FileSystemEventHandler(fileSystemWatcher_Changed);
        }

        public void EnableCreated(bool b)
        {
            if (b)
                m_fileSystemWatcher.Created += new System.IO.FileSystemEventHandler(fileSystemWatcher_Created);
            else
                m_fileSystemWatcher.Created -= new System.IO.FileSystemEventHandler(fileSystemWatcher_Created);
        }

        public void EnableDeleted(bool b)
        {
            if (b)
                m_fileSystemWatcher.Deleted += new System.IO.FileSystemEventHandler(fileSystemWatcher_Deleted);
            else
                m_fileSystemWatcher.Deleted -= new System.IO.FileSystemEventHandler(fileSystemWatcher_Deleted);
        }

        public void EnableRenamed(bool b)
        {
            if (b)
                m_fileSystemWatcher.Renamed += new System.IO.RenamedEventHandler(fileSystemWatcher_Renamed);
            else
                m_fileSystemWatcher.Renamed -= new System.IO.RenamedEventHandler(fileSystemWatcher_Renamed);
        }

        private void ExecuteScript(String script)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            String[] strs = script.Split();
            String args = script.Substring(strs[0].Length, script.Length - strs[0].Length);
            startInfo.FileName = strs[0];
            startInfo.Arguments = args;
            startInfo.UseShellExecute = false;
            Process.Start(startInfo);
        }

        private void RunScript(String fullPath, String file, String text)
        {
            String script = String.Format("{0}", text);

            script = script.Replace("%1", fullPath);
            script = script.Replace("%2", file);

            if (text != "")
            {
                Program.MainForm.AddOutputSafe(System.String.Format("Running script {0}", script), eOutputLevel.Warning);
                ExecuteScript(script);
                Program.MainForm.AddOutputSafe(System.String.Format("Script finished"), eOutputLevel.Warning);
            }
        }

        public void StartWatch()
        {           
            Program.MainForm.AddOutputSafe(String.Format("Watching started {0} {1}{2}", DateTime.Now.ToString(), m_fileSystemWatcher.Path, m_fileSystemWatcher.Filter), eOutputLevel.Info);
            m_fileSystemWatcher.EnableRaisingEvents = true;
        }

        public void StopWatch()
        {
            Program.MainForm.AddOutputSafe(String.Format("Watching stopped {0} {1}{2}", DateTime.Now.ToString(), m_fileSystemWatcher.Path, m_fileSystemWatcher.Filter), eOutputLevel.Warning);
            m_fileSystemWatcher.EnableRaisingEvents = false;
        }

        private String TimeStampedFile(FileSystemEventArgs e)
        {
            return String.Format("{0} {1}", DateTime.Now.ToString(), e.FullPath);
        }

        private void fileSystemWatcher_Changed(object sender, FileSystemEventArgs e)
        {
            Program.MainForm.AddOutputSafe(String.Format("{0} {1}.", TimeStampedFile(e), "changed"), eOutputLevel.Regular);
            RunScript(e.FullPath, e.Name, Program.MainForm.TextBox2.Text);
        }
        private void fileSystemWatcher_Created(object sender, FileSystemEventArgs e)
        {
            Program.MainForm.AddOutputSafe(String.Format("{0} {1}.", TimeStampedFile(e), "created"), eOutputLevel.Regular);
            RunScript(e.FullPath, e.Name, Program.MainForm.TextBox2.Text);
        }
        private void fileSystemWatcher_Deleted(object sender, FileSystemEventArgs e)
        {
            Program.MainForm.AddOutputSafe(String.Format("{0} {1}.", TimeStampedFile(e), "deleted"), eOutputLevel.Error);
            RunScript(e.FullPath, e.Name, Program.MainForm.TextBox2.Text);
        }
        private void fileSystemWatcher_Renamed(object sender, FileSystemEventArgs e)
        {
            Program.MainForm.AddOutputSafe(String.Format("{0} {1}.", TimeStampedFile(e), "renamed"), eOutputLevel.Regular);
            RunScript(e.FullPath, e.Name, Program.MainForm.TextBox2.Text);
        }
    }
}
