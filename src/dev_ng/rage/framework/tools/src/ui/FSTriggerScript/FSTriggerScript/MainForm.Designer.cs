namespace FSTriggerScript
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
            this.cbChanged = new System.Windows.Forms.CheckBox();
            this.cbCreated = new System.Windows.Forms.CheckBox();
            this.cbDeleted = new System.Windows.Forms.CheckBox();
            this.cbRenamed = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(12, 32);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(382, 524);
            this.listBox1.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.Control;
            this.button1.Location = new System.Drawing.Point(363, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(128, 19);
            this.button1.TabIndex = 2;
            this.button1.Text = "Start Watch";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(55, 6);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(302, 20);
            this.textBox2.TabIndex = 20;
            this.textBox2.Text = "notepad.exe %1";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(9, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "Script";
            // 
            // propertyGrid1
            // 
            this.propertyGrid1.Location = new System.Drawing.Point(400, 57);
            this.propertyGrid1.Name = "propertyGrid1";
            this.propertyGrid1.Size = new System.Drawing.Size(371, 513);
            this.propertyGrid1.TabIndex = 22;
            // 
            // cbChanged
            // 
            this.cbChanged.AutoSize = true;
            this.cbChanged.Checked = true;
            this.cbChanged.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbChanged.Location = new System.Drawing.Point(497, 3);
            this.cbChanged.Name = "cbChanged";
            this.cbChanged.Size = new System.Drawing.Size(69, 17);
            this.cbChanged.TabIndex = 23;
            this.cbChanged.Text = "Changed";
            this.cbChanged.UseVisualStyleBackColor = true;
            this.cbChanged.Click += new System.EventHandler(this.Changed);
            // 
            // cbCreated
            // 
            this.cbCreated.AutoSize = true;
            this.cbCreated.Checked = true;
            this.cbCreated.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbCreated.Location = new System.Drawing.Point(572, 3);
            this.cbCreated.Name = "cbCreated";
            this.cbCreated.Size = new System.Drawing.Size(63, 17);
            this.cbCreated.TabIndex = 24;
            this.cbCreated.Text = "Created";
            this.cbCreated.UseVisualStyleBackColor = true;
            this.cbCreated.Click += new System.EventHandler(this.Create);
            // 
            // cbDeleted
            // 
            this.cbDeleted.AutoSize = true;
            this.cbDeleted.Location = new System.Drawing.Point(641, 3);
            this.cbDeleted.Name = "cbDeleted";
            this.cbDeleted.Size = new System.Drawing.Size(63, 17);
            this.cbDeleted.TabIndex = 25;
            this.cbDeleted.Text = "Deleted";
            this.cbDeleted.UseVisualStyleBackColor = true;
            this.cbDeleted.Click += new System.EventHandler(this.Deleted);
            // 
            // cbRenamed
            // 
            this.cbRenamed.AutoSize = true;
            this.cbRenamed.Location = new System.Drawing.Point(710, 3);
            this.cbRenamed.Name = "cbRenamed";
            this.cbRenamed.Size = new System.Drawing.Size(72, 17);
            this.cbRenamed.TabIndex = 26;
            this.cbRenamed.Text = "Renamed";
            this.cbRenamed.UseVisualStyleBackColor = true;
            this.cbRenamed.Click += new System.EventHandler(this.Renamed);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(783, 571);
            this.Controls.Add(this.cbRenamed);
            this.Controls.Add(this.cbDeleted);
            this.Controls.Add(this.cbCreated);
            this.Controls.Add(this.cbChanged);
            this.Controls.Add(this.propertyGrid1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.listBox1);
            this.Name = "MainForm";
            this.Text = "FSTriggerScript";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PropertyGrid propertyGrid1;
        private System.Windows.Forms.CheckBox cbChanged;
        private System.Windows.Forms.CheckBox cbCreated;
        private System.Windows.Forms.CheckBox cbDeleted;
        private System.Windows.Forms.CheckBox cbRenamed;
    }
}

