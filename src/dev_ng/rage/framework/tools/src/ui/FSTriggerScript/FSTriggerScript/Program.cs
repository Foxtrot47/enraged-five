using System;
using System.Collections.Generic;
using System.Windows.Forms;
using RSG.Base.Forms;

namespace FSTriggerScript
{
    static class Program
    {
        #region Constants
        public static readonly String AUTHOR = "Derek Ward";
        public static readonly String EMAIL = "derek.ward@rockstarnorth.com";
        #endregion // Constants

        #region Members
        internal static MainForm    MainForm;
        internal static Controller  Controller;
        #endregion // Members

        #region Main
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.DoEvents();
                Controller = new Controller();
                MainForm = new MainForm();
                MainForm.PropertyGrid1.SelectedObject = Controller.FileSystemWatcher;
                Application.Run(MainForm);
            }
            catch (Exception ex)
            {
                using (ExceptionStackTraceDlg dlg =
                    new ExceptionStackTraceDlg(ex, AUTHOR, EMAIL))
                {
                    dlg.ShowDialog();
                }
            }
        }
        #endregion // Main
    }
}