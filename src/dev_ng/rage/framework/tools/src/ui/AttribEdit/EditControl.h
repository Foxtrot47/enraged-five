#if !defined(AFX_EDITCONTROL_H__3D532897_3A8F_11D3_98AF_00902709243B__INCLUDED_)
#define AFX_EDITCONTROL_H__3D532897_3A8F_11D3_98AF_00902709243B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// EditControl.h : header file
//
#pragma warning (disable : 4786)

#include "ComboBoxDialog.h"
#include "SpinnerDialog.h"
#include "FileBrowserDialog.h"
#include <AttribDialog.h>
#include "AttributeEx.h"

/////////////////////////////////////////////////////////////////////////////
// CEditControl dialog

class CEditControl : public CDialog
{
// Construction
public:
	CEditControl(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CEditControl)
	enum { IDD = IDD_EDITCONTROL };
	CString	m_controlName;
	//}}AFX_DATA


	CComboBoxDialog m_comboboxDialog;
	CSpinnerDialog m_spinnerDialog;
	CFileBrowserDialog m_fileBrowserDialog;
	dmat::AttributeControlDesc::ControlType m_type;

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEditControl)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CEditControl)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EDITCONTROL_H__3D532897_3A8F_11D3_98AF_00902709243B__INCLUDED_)
