// LeftView.cpp : implementation of the CLeftView class
//

#include "stdafx.h"
#include "AttribEdit.h"
#include "AttribEditDoc.h"
#include "AttribEditView.h"
#include "LeftView.h"
#include "TreeProcess.h"

#include "AttribExProcess.h"
// STD headers
#include <strstream>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLeftView

IMPLEMENT_DYNCREATE(CLeftView, CTreeView)

BEGIN_MESSAGE_MAP(CLeftView, CTreeView)
	//{{AFX_MSG_MAP(CLeftView)
	ON_NOTIFY_REFLECT(TVN_SELCHANGED, OnSelchanged)
	ON_NOTIFY_REFLECT(TVN_ENDLABELEDIT, OnEndlabeledit)
	ON_NOTIFY_REFLECT(TVN_KEYDOWN, OnKeydown)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLeftView construction/destruction

CLeftView::CLeftView() : m_deletingTree(false)
{
	m_imageList.Create(16, 16, ILC_COLOR8, 4, 10);

	m_imageList.SetBkColor(RGB(255,255,255));

	m_imageList.Add(theApp.LoadIcon(IDI_CLOSEFOLD));
	m_imageList.Add(theApp.LoadIcon(IDI_OPENFOLD));
	m_imageList.Add(theApp.LoadIcon(IDI_CLASS));
	m_imageList.Add(theApp.LoadIcon(IDI_DIALOG));
}

CLeftView::~CLeftView()
{
}

BOOL CLeftView::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= TVS_HASLINES|TVS_LINESATROOT|TVS_HASBUTTONS|TVS_EDITLABELS|TVS_SHOWSELALWAYS;

	return CTreeView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CLeftView drawing

void CLeftView::OnDraw(CDC* pDC)
{
	CAttribEditDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	// TODO: add draw code for native data here
}


//
//        name: CLeftView::ConstructTreeCtrl
// description: Construct tree control from AttributeMgrEx in class
//
void CLeftView::ConstructTreeCtrl()
{
	CAttribEditDoc* pDoc = GetDocument();

	CTreeCtrl &tree = GetTreeCtrl();
	const AttributeClassExMap& classMap = pDoc->m_mgr.GetClassMap();
	const AttributeDialogExMap& dialogMap = pDoc->m_mgr.GetDialogMap();
	AttributeClassExMapConstIterator iClass;
	AttributeDialogExMapConstIterator iDialog;

	DeleteTree();

	// add class and dialog folders
	m_hClassItem = tree.InsertItem("Classes");
	tree.SetItem(m_hClassItem, TVIF_IMAGE|TVIF_SELECTEDIMAGE|TVIF_PARAM, NULL, 0, 1, 0,0,0);
	m_hDialogItem = tree.InsertItem("Dialogs");
	tree.SetItem(m_hDialogItem, TVIF_IMAGE|TVIF_SELECTEDIMAGE|TVIF_PARAM, NULL, 0, 1, 0,0,0);

	// add classes to tree view
	for(iClass = classMap.begin(); iClass != classMap.end(); iClass++)
	{
		AddClass(tree, &(*iClass).second, m_hClassItem);
	}

	// add dialogs to tree view
	for(iDialog = dialogMap.begin(); iDialog != dialogMap.end(); iDialog++)
	{
		AddDialog(tree, &(*iDialog).second, m_hDialogItem);
	}
}

//
//        name: CLeftView::DeleteTreeItem
// description: Delete a tree item
//          in: item = index of item
//
bool CLeftView::DeleteTreeItem(HTREEITEM hItem)
{
	CAttribEditDoc* pDoc = GetDocument();
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hParent = tree.GetParentItem(hItem);

	ObjectEx *pObjectEx = (ObjectEx *)tree.GetItemData(hItem);

	if(pObjectEx == NULL)
		return false;

	// if deleting base item delete everything
	if(hParent == m_hClassItem || hParent == m_hDialogItem)
	{
		return pDoc->m_oper.DeleteObject(pObjectEx);
	}
	
	// deleting a child object
	ObjectEx *pParentObj = (ObjectEx *)tree.GetItemData(hParent);

	return pDoc->m_oper.DeleteChildObjectPrompt(pParentObj, pObjectEx);
}

//
//        name: CLeftView::DeleteTree
// description: Delete contents of tree control
//
void CLeftView::DeleteTree()
{
	m_deletingTree = true;
	GetTreeCtrl().DeleteAllItems();
	m_deletingTree = false;
}

//
//        name: CLeftView::OnInitialUpdate
// description: Called when a new attributes file is loaded
//
void CLeftView::OnInitialUpdate()
{
	CAttribEditDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	CTreeView::OnInitialUpdate();

	pDoc->SetLeftView(this);

	GetTreeCtrl().SetImageList(&m_imageList, TVSIL_NORMAL);

	ConstructTreeCtrl();
}

//
//        name: *CLeftView::GetSelectedClass
// description: Return a pointer to the selected class if one is selected
//
AttributeClassEx *CLeftView::GetSelectedClass()
{
	HTREEITEM hSelected = GetTreeCtrl().GetSelectedItem();
	if(hSelected)
	{
		ObjectEx *pObjectEx = (ObjectEx *)GetTreeCtrl().GetItemData(hSelected);
		if(pObjectEx)
			return pObjectEx->AsClass();
	}
	return NULL;
}

//
//        name: *CLeftView::GetSelectedDialog
// description: Return a pointer to the selected dialog description if one is selected
//          in: 
//         out: 
//
AttributeDialogDescEx *CLeftView::GetSelectedDialog()
{
	HTREEITEM hSelected = GetTreeCtrl().GetSelectedItem();
	if(hSelected)
	{
		ObjectEx *pObjectEx = (ObjectEx *)GetTreeCtrl().GetItemData(hSelected);
		if(pObjectEx)
			return pObjectEx->AsDialog();
	}
	return NULL;
}

//
//        name: CLeftView::AddClass
// description: Add class and children to tree view
//          in: 
//
HTREEITEM CLeftView::AddClass(CTreeCtrl &tree, const AttributeClassEx *pClass, HTREEITEM hParent)
{
	HTREEITEM hItem;

	if(hParent == NULL)
		hParent = m_hClassItem;

	hItem = tree.InsertItem(pClass->GetName().c_str(), hParent);
	tree.SetItem(hItem, TVIF_IMAGE|TVIF_SELECTEDIMAGE|TVIF_PARAM, NULL, 2, 2, 0,0,(unsigned int)pClass);

	AddClassChildren(tree, pClass, hItem);

	return hItem;
}

//
//        name: CLeftView::AddDialog
// description: Add dialog and children to tree view
//          in: 
//
HTREEITEM CLeftView::AddDialog(CTreeCtrl &tree, const AttributeDialogDescEx *pDialog, HTREEITEM hParent)
{
	HTREEITEM hItem;

	if(hParent == NULL)
		hParent = m_hDialogItem;

	hItem = tree.InsertItem(pDialog->GetName().c_str(), hParent);
	tree.SetItem(hItem, TVIF_IMAGE|TVIF_SELECTEDIMAGE|TVIF_PARAM, NULL, 3, 3, 0,0,(unsigned int)pDialog);

	AddDialogChildren(tree, pDialog, hItem);

	return hItem;
}

//
//        name: AddClassChildren
// description: Add tree children for classes
//          in: tree = reference to tree control
//				desc = attribute class 
//				hParent = tree item handle of parent
//         out: 
//
void CLeftView::AddClassChildren(CTreeCtrl &tree, const AttributeClassEx *pDesc, HTREEITEM hParent)
{
	assert(pDesc);

	CAttribEditDoc* pDoc = GetDocument();
	const std::vector<std::string>& children = pDesc->GetChildrenVector();
	std::vector<std::string>::const_iterator iControl;
	HTREEITEM hItem;
	const AttributeClassEx *pChild;

	for(iControl = children.begin(); iControl != children.end(); iControl++)
	{
		hItem = tree.InsertItem((*iControl).c_str(), hParent);
		pChild = pDoc->m_mgr.GetClass((*iControl));

		tree.SetItem(hItem, TVIF_IMAGE|TVIF_SELECTEDIMAGE|TVIF_PARAM, NULL, 2, 2, 0,0, (unsigned int)pChild);
		AddClassChildren(tree, pChild, hItem);
	}
}

//
//        name: AddDialogChildren
// description: Add tree children for dialogs
//          in: tree = reference to tree control
//				desc = attribute dialog description
//				hParent = tree item handle of parent
//         out: 
//
void CLeftView::AddDialogChildren(CTreeCtrl &tree, const AttributeDialogDescEx *pDesc, HTREEITEM hParent)
{
	assert(pDesc);

	CAttribEditDoc* pDoc = GetDocument();
	const std::vector<std::string>& children = pDesc->GetChildrenVector();
	std::vector<std::string>::const_iterator iControl;
	HTREEITEM hItem;
	const AttributeDialogDescEx *pChild;

	for(iControl = children.begin(); iControl != children.end(); iControl++)
	{
		hItem = tree.InsertItem((*iControl).c_str(), hParent);
		pChild = pDoc->m_mgr.GetDialogDesc((*iControl));

		tree.SetItem(hItem, TVIF_IMAGE|TVIF_SELECTEDIMAGE|TVIF_PARAM, NULL, 3, 3, 0,0, (unsigned int)pChild);
		AddDialogChildren(tree, pChild, hItem);
	}
}

//
//        name: CLeftView::AddNewClass
// description: Add a new empty class to tree and manager
//
/*bool CLeftView::AddNewClass(const std::string& className)
{
	CTreeCtrl &tree = GetTreeCtrl();
	AttributeClassEx aClassEx(className);
	dmat::AttributeClass aClass;
	AttributeClassEx *pAClass;
	CAttribEditDoc* pDoc = GetDocument();
	HTREEITEM hItem;

	if(pDoc->m_mgr.GetClass(className))
	{
		std::ostrstream msg;
		// construct message
		msg << "The Class \"" << className << "\" already exists." << std::ends;

		MessageBox(msg.str(), 
			"Creation failed", 
			MB_ICONERROR|MB_OK);
		return false;
	}
	pDoc->m_mgr.AddClass(aClassEx, aClass);
	// get pointer to copy of class in manager
	pAClass = pDoc->m_mgr.GetClass(className);

	hItem = tree.InsertItem(className.c_str(), m_hClassItem);
	tree.SetItem(hItem, TVIF_IMAGE|TVIF_SELECTEDIMAGE|TVIF_PARAM, NULL, 2, 2, 0,0,(unsigned int)pAClass);
	tree.SelectItem(hItem);

	return true;
}

//
//        name: CLeftView::AddNewDialog
// description: Add a new empty dialog to tree and manager
//
bool CLeftView::AddNewDialog(const std::string& dialogName)
{
	CTreeCtrl &tree = GetTreeCtrl();
	AttributeDialogDescEx dialogEx(dialogName);
	dmat::AttributeDialogDesc dialog;
	AttributeDialogDescEx *pDialog;
	CAttribEditDoc* pDoc = GetDocument();
	HTREEITEM hItem;

	if(pDoc->m_mgr.GetDialogDesc(dialogName))
	{
		std::ostrstream msg;
		// construct message
		msg << "The Dialog \"" << dialogName << "\" already exists." << std::ends;

		MessageBox(msg.str(), 
			"Creation failed", 
			MB_ICONERROR|MB_OK);
		return false;
	}
	pDoc->m_mgr.AddDialogDesc(dialogEx, dialog);
	// get pointer to copy of class in manager
	pDialog = pDoc->m_mgr.GetDialogDesc(dialogName);

	hItem = tree.InsertItem(dialogName.c_str(), m_hDialogItem);
	tree.SetItem(hItem, TVIF_IMAGE|TVIF_SELECTEDIMAGE|TVIF_PARAM, NULL, 3, 3, 0,0,(unsigned int)pDialog);
	tree.SelectItem(hItem);

	return true;
}*/

/////////////////////////////////////////////////////////////////////////////
// CLeftView diagnostics

#ifdef _DEBUG
void CLeftView::AssertValid() const
{
	CTreeView::AssertValid();
}

void CLeftView::Dump(CDumpContext& dc) const
{
	CTreeView::Dump(dc);
}

CAttribEditDoc* CLeftView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CAttribEditDoc)));
	return (CAttribEditDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CLeftView message handlers

void CLeftView::OnSelchanged(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
	
	CAttribEditDoc* pDoc = GetDocument();

	if(m_deletingTree == false)
		pDoc->m_pRightView->SetCurrentObject((ObjectEx *)(pNMTreeView->itemNew.lParam));

	*pResult = 0;
}

void CLeftView::OnEndlabeledit(NMHDR* pNMHDR, LRESULT* pResult) 
{
	TV_DISPINFO* pTVDispInfo = (TV_DISPINFO*)pNMHDR;
	TVITEM tvItem;
	CTreeCtrl &tree = GetTreeCtrl();
	CAttribEditDoc* pDoc = GetDocument();
//	int param;

	tvItem.hItem = pTVDispInfo->item.hItem;
	tvItem.mask = LVIF_PARAM;
	tree.GetItem(&tvItem);
	ObjectEx *pObjectEx = (ObjectEx *)tvItem.lParam;
	
	*pResult = 0;
	// if text wasn't changed
	if(pTVDispInfo->item.pszText == NULL)
		return;
	if(pObjectEx == NULL)
		return;
	if(*pTVDispInfo->item.pszText == '\0')
		return;

	*pResult = pDoc->m_oper.RenameObject(pObjectEx, pTVDispInfo->item.pszText);
	pDoc->m_pRightView->SetCurrentObject((ObjectEx*)tree.GetItemData(tvItem.hItem));

/*	if(pObjectEx->AsClass())
	{
		std::string name = pObjectEx->GetName();
		*pResult = pDoc->m_mgr.RenameClass(name, pTVDispInfo->item.pszText);

		if(*pResult)
		{
			param = tvItem.lParam;
			// set pointer to class
			tvItem.lParam = (int)pDoc->m_mgr.GetClass(pTVDispInfo->item.pszText);
			tree.SetItem(&tvItem);
			// update tree items
			ProcessTree(tree, pDoc, 
						TreeRenameClass(param, pTVDispInfo->item.pszText));
			ProcessAllClasses(RenameObjectClass(name, pTVDispInfo->item.pszText));

			pDoc->m_pRightView->SetCurrentObject((ObjectEx*)tree.GetItemData(tvItem.hItem));
		}
	}
	else if(pObjectEx->AsDialog())
	{
		std::string name = pObjectEx->GetName();
		*pResult = pDoc->m_mgr.RenameDialogDesc(name, pTVDispInfo->item.pszText);

		if(*pResult)
		{
			param = tvItem.lParam;
			// set pointer to class
			tvItem.lParam = (int)pDoc->m_mgr.GetDialogDesc(pTVDispInfo->item.pszText);
			tree.SetItem(&tvItem);
			// update tree items
			ProcessTree(tree, pDoc, 
						TreeRenameDialog(param, pTVDispInfo->item.pszText));
			ProcessAllDialogs(RenameObjectClass(name, pTVDispInfo->item.pszText));

			pDoc->m_pRightView->SetCurrentObject((ObjectEx*)tree.GetItemData(tvItem.hItem));
		}
	}
	else
		assert(0);*/
}

void CLeftView::OnKeydown(NMHDR* pNMHDR, LRESULT* pResult) 
{
	TV_KEYDOWN* pTVKeyDown = (TV_KEYDOWN*)pNMHDR;
	*pResult = 0;

	// has delete key been pressed
	if(pTVKeyDown->wVKey == VK_DELETE)
	{
		HTREEITEM hSelected = GetTreeCtrl().GetSelectedItem();
		if(hSelected)
			DeleteTreeItem(hSelected);
		*pResult = 1;
	}
}
