# Microsoft Developer Studio Project File - Name="AttribEdit" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=AttribEdit - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "AttribEdit.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "AttribEdit.mak" CFG="AttribEdit - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "AttribEdit - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "AttribEdit - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/users/adam/AttribEdit", DTGAAAAA"
# PROP Scc_LocalPath "."
CPP=cwcl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "AttribEdit - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /I "..\dmat" /I "x:\tools\include" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "WIN32" /D "_MBCS" /D "PC_DMA" /D "ATTRIBUTE_STATIC" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x809 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=cwlink.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 htmlhelp.lib /nologo /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == "AttribEdit - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /I "..\dmat" /I "x:\tools\include" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "WIN32" /D "_MBCS" /D "PC_DMA" /D "ATTRIBUTE_STATIC" /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x809 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=cwlink.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 htmlhelp.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "AttribEdit - Win32 Release"
# Name "AttribEdit - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\AttribEdit.cpp
# End Source File
# Begin Source File

SOURCE=.\AttribEdit.rc
# End Source File
# Begin Source File

SOURCE=.\AttribEditDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\AttribEditView.cpp
# End Source File
# Begin Source File

SOURCE=.\LeftView.cpp
# End Source File
# Begin Source File

SOURCE=.\MainFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\Operation.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\AttribEdit.h
# End Source File
# Begin Source File

SOURCE=.\AttribEditDoc.h
# End Source File
# Begin Source File

SOURCE=.\AttribEditView.h
# End Source File
# Begin Source File

SOURCE=.\LeftView.h
# End Source File
# Begin Source File

SOURCE=.\MainFrm.h
# End Source File
# Begin Source File

SOURCE=.\Operation.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\TreeProcess.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\AttribEdit.ico
# End Source File
# Begin Source File

SOURCE=.\res\AttribEdit.rc2
# End Source File
# Begin Source File

SOURCE=.\res\AttribEditDoc.ico
# End Source File
# Begin Source File

SOURCE=.\Res\bitmap1.bmp
# End Source File
# Begin Source File

SOURCE=.\Res\bitmap2.bmp
# End Source File
# Begin Source File

SOURCE=.\Res\checkbox.ico
# End Source File
# Begin Source File

SOURCE=.\res\class.bmp
# End Source File
# Begin Source File

SOURCE=.\Res\Clsdfold.ico
# End Source File
# Begin Source File

SOURCE=.\Res\combobox.ico
# End Source File
# Begin Source File

SOURCE=.\Res\control.ico
# End Source File
# Begin Source File

SOURCE=.\Res\Delete.bmp
# End Source File
# Begin Source File

SOURCE=.\res\dialog.bmp
# End Source File
# Begin Source File

SOURCE=.\dmaman48x48.bmp
# End Source File
# Begin Source File

SOURCE=.\Res\editbox.ico
# End Source File
# Begin Source File

SOURCE=.\res\filebrow.ico
# End Source File
# Begin Source File

SOURCE=.\Res\Form.ico
# End Source File
# Begin Source File

SOURCE=.\Res\groupbox.ico
# End Source File
# Begin Source File

SOURCE=.\Res\Misc14.ico
# End Source File
# Begin Source File

SOURCE=.\Res\move.bmp
# End Source File
# Begin Source File

SOURCE=.\Res\Note01.ico
# End Source File
# Begin Source File

SOURCE=.\Res\Openfold.ico
# End Source File
# Begin Source File

SOURCE=.\Res\spinner.ico
# End Source File
# Begin Source File

SOURCE=.\res\Toolbar.bmp
# End Source File
# End Group
# Begin Group "AttributesEx"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\AttribExProcess.h
# End Source File
# Begin Source File

SOURCE=.\AttribMgrEx.cpp
# SUBTRACT CPP /YX /Yc /Yu
# End Source File
# Begin Source File

SOURCE=.\AttribMgrEx.h
# End Source File
# Begin Source File

SOURCE=.\AttribScriptEx.cpp
# SUBTRACT CPP /YX /Yc /Yu
# End Source File
# Begin Source File

SOURCE=.\AttribScriptEx.h
# End Source File
# Begin Source File

SOURCE=.\AttributeEx.cpp
# SUBTRACT CPP /YX /Yc /Yu
# End Source File
# Begin Source File

SOURCE=.\AttributeEx.h
# End Source File
# End Group
# Begin Group "Dialogs"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\AddControlDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\AddControlDialog.h
# End Source File
# Begin Source File

SOURCE=.\Classname.cpp
# End Source File
# Begin Source File

SOURCE=.\Classname.h
# End Source File
# Begin Source File

SOURCE=.\ComboBoxDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\ComboBoxDialog.h
# End Source File
# Begin Source File

SOURCE=.\EditControl.cpp
# End Source File
# Begin Source File

SOURCE=.\EditControl.h
# End Source File
# Begin Source File

SOURCE=.\FileBrowserDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\FileBrowserDialog.h
# End Source File
# Begin Source File

SOURCE=.\GroupBoxDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\GroupBoxDialog.h
# End Source File
# Begin Source File

SOURCE=.\ListCtrlDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\ListCtrlDialog.h
# End Source File
# Begin Source File

SOURCE=.\NewDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\NewDialog.h
# End Source File
# Begin Source File

SOURCE=.\NewMemberDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\NewMemberDialog.h
# End Source File
# Begin Source File

SOURCE=.\SpinnerDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\SpinnerDialog.h
# End Source File
# End Group
# Begin Source File

SOURCE="..\..\..\Web\Tools release\Entries\attribedit.htm"
# End Source File
# Begin Source File

SOURCE="..\..\..\Web\Tools release\Help\attribedithelp.htm"
# End Source File
# End Target
# End Project
