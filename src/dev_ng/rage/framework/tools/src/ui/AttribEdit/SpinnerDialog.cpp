// SpinnerDialog.cpp : implementation file
//

#include "stdafx.h"
#include "AttribEdit.h"
#include "SpinnerDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSpinnerDialog dialog


CSpinnerDialog::CSpinnerDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CSpinnerDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSpinnerDialog)
	m_max = 0.0f;
	m_min = 0.0f;
	//}}AFX_DATA_INIT
}


void CSpinnerDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSpinnerDialog)
	DDX_Text(pDX, IDC_MAXIMUM, m_max);
	DDX_Text(pDX, IDC_MINIMUM, m_min);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSpinnerDialog, CDialog)
	//{{AFX_MSG_MAP(CSpinnerDialog)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSpinnerDialog message handlers

void CSpinnerDialog::OnOK()
{
}
void CSpinnerDialog::OnCancel()
{
}
