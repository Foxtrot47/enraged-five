#if !defined(AFX_CLASSNAME_H__C832EF84_32BF_11D3_8E9A_00902709243B__INCLUDED_)
#define AFX_CLASSNAME_H__C832EF84_32BF_11D3_8E9A_00902709243B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Classname.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CClassname dialog

class CClassname : public CDialog
{
// Construction
public:
	CClassname(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CClassname)
	enum { IDD = IDD_CLASSNAME_DIALOG };
	CString	m_name;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CClassname)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CClassname)
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CLASSNAME_H__C832EF84_32BF_11D3_8E9A_00902709243B__INCLUDED_)
