// MainFrm.cpp : implementation of the CMainFrame class
//
#pragma warning (disable : 4786)

#include "stdafx.h"
#include <htmlhelp.h>
#include "AttribEdit.h"

#include "MainFrm.h"
#include "AttribEditDoc.h"
#include "LeftView.h"
#include "AttribEditView.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_UPDATE_COMMAND_UI(ID_CLASS_DELETE, OnUpdateClassDelete)
	ON_UPDATE_COMMAND_UI(ID_CLASS_DELETE_MEMBER, OnUpdateClassDeleteMember)
	ON_UPDATE_COMMAND_UI(ID_CLASS_NEW_MEMBER, OnUpdateClassNewMember)
	ON_UPDATE_COMMAND_UI(ID_CLASS_INCLUDE, OnUpdateClassInclude)
	ON_COMMAND(ID_CLASS_DELETE, OnClassDelete)
	ON_COMMAND(ID_CLASS_DELETE_MEMBER, OnClassDeleteMember)
	ON_COMMAND(ID_CLASS_INCLUDE, OnClassInclude)
	ON_COMMAND(ID_CLASS_NEW, OnClassNew)
	ON_COMMAND(ID_CLASS_NEW_MEMBER, OnClassNewMember)
	ON_UPDATE_COMMAND_UI(ID_DIALOG_DELETE, OnUpdateDialogDelete)
	ON_UPDATE_COMMAND_UI(ID_DIALOG_DELETE_CONTROL, OnUpdateDialogDeleteControl)
	ON_UPDATE_COMMAND_UI(ID_DIALOG_NEW, OnUpdateDialogNew)
	ON_COMMAND(ID_DIALOG_DELETE, OnDialogDelete)
	ON_COMMAND(ID_DIALOG_DELETE_CONTROL, OnDialogDeleteControl)
	ON_COMMAND(ID_DIALOG_NEW, OnDialogNew)
	ON_UPDATE_COMMAND_UI(ID_DIALOG_CHECKBOX, OnUpdateDialogControl)
	ON_COMMAND(ID_DIALOG_CHECKBOX, OnDialogCheckbox)
	ON_COMMAND(ID_DIALOG_COMBOBOX, OnDialogCombobox)
	ON_COMMAND(ID_DIALOG_GROUPBOX, OnDialogGroupbox)
	ON_COMMAND(ID_DIALOG_SPINNER, OnDialogSpinner)
	ON_COMMAND(ID_DIALOG_EDITBOX, OnDialogEditbox)
	ON_COMMAND(ID_DIALOG_FILEBROWSER, OnDialogFilebrowser)
	ON_UPDATE_COMMAND_UI(ID_MOVE_UP, OnUpdateMoveUp)
	ON_UPDATE_COMMAND_UI(ID_MOVE_DOWN, OnUpdateMoveDown)
	ON_COMMAND(ID_MOVE_UP, OnMoveUp)
	ON_COMMAND(ID_MOVE_DOWN, OnMoveDown)
	ON_UPDATE_COMMAND_UI(ID_DIALOG_TEST, OnUpdateDialogTest)
	ON_COMMAND(ID_DIALOG_TEST, OnDialogTest)
	ON_UPDATE_COMMAND_UI(ID_CLASS_DEFAULT, OnUpdateClassDefault)
	ON_COMMAND(ID_CLASS_DEFAULT, OnClassEditDefault)
	ON_UPDATE_COMMAND_UI(ID_DIALOG_ATTRIBUTES, OnUpdateDialogAttributes)
	ON_COMMAND(ID_DIALOG_ATTRIBUTES, OnDialogAttributes)
	ON_COMMAND(ID_HELP_ATTRIBEDIT, OnHelpAttribedit)
	ON_COMMAND(ID_HELP_ATTRIBUTE, OnHelpAttribute)
	ON_UPDATE_COMMAND_UI(ID_DIALOG_COMBOBOX, OnUpdateDialogControl)
	ON_UPDATE_COMMAND_UI(ID_DIALOG_GROUPBOX, OnUpdateDialogControl)
	ON_UPDATE_COMMAND_UI(ID_DIALOG_SPINNER, OnUpdateDialogControl)
	ON_UPDATE_COMMAND_UI(ID_DIALOG_EDITBOX, OnUpdateDialogControl)
	ON_UPDATE_COMMAND_UI(ID_DIALOG_FILEBROWSER, OnUpdateDialogControl)
	ON_COMMAND(ID_DIALOG_AUTO, OnDialogAuto)
	ON_UPDATE_COMMAND_UI(ID_DIALOG_AUTO, OnUpdateDialogNew)
	//}}AFX_MSG_MAP
	ON_UPDATE_COMMAND_UI_RANGE(AFX_ID_VIEW_MINIMUM, AFX_ID_VIEW_MAXIMUM, OnUpdateViewStyles)
	ON_COMMAND_RANGE(AFX_ID_VIEW_MINIMUM, AFX_ID_VIEW_MAXIMUM, OnViewStyle)
END_MESSAGE_MAP()

static unsigned int indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame() : m_pDoc(NULL)
{
	// TODO: add member initialization code here
	
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_wndToolBar.CreateEx(this) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}
	if (!m_classToolBar.CreateEx(this) ||
		!m_classToolBar.LoadToolBar(IDR_CLASS))
	{
		TRACE0("Failed to create class toolbar\n");
		return -1;      // fail to create
	}
	if (!m_dialogToolBar.CreateEx(this) ||
		!m_dialogToolBar.LoadToolBar(IDR_DIALOG))
	{
		TRACE0("Failed to create dialog toolbar\n");
		return -1;      // fail to create
	}
	if (!m_moveToolBar.CreateEx(this) ||
		!m_moveToolBar.LoadToolBar(IDR_MOVE))
	{
		TRACE0("Failed to create move toolbar\n");
		return -1;      // fail to create
	}

	if (!m_wndReBar.Create(this) ||
		!m_wndReBar.AddBar(&m_wndToolBar) ||
		!m_wndReBar.AddBar(&m_classToolBar) ||
		!m_wndReBar.AddBar(&m_dialogToolBar) ||
		!m_wndReBar.AddBar(&m_moveToolBar))
	{
		TRACE0("Failed to create rebar\n");
		return -1;      // fail to create
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(unsigned int)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	m_wndToolBar.SetBarStyle(m_wndToolBar.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY);
	m_classToolBar.SetBarStyle(m_classToolBar.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY);
	m_dialogToolBar.SetBarStyle(m_dialogToolBar.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY);
	m_moveToolBar.SetBarStyle(m_moveToolBar.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY);

	return 0;
}

BOOL CMainFrame::OnCreateClient(LPCREATESTRUCT /*lpcs*/,
	CCreateContext* pContext)
{
	// create splitter window
	if (!m_wndSplitter.CreateStatic(this, 1, 2))
		return FALSE;

	if (!m_wndSplitter.CreateView(0, 0, RUNTIME_CLASS(CLeftView), CSize(200, 100), pContext) ||
		!m_wndSplitter.CreateView(0, 1, RUNTIME_CLASS(CAttribEditView), CSize(100, 100), pContext))
	{
		m_wndSplitter.DestroyWindow();
		return FALSE;
	}

	m_pDoc = (CAttribEditDoc *)pContext->m_pCurrentDoc;

	return TRUE;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

CLeftView* CMainFrame::GetLeftPane()
{
	CWnd* pWnd = m_wndSplitter.GetPane(0, 0);
	CLeftView* pView = DYNAMIC_DOWNCAST(CLeftView, pWnd);
	return pView;
}
CAttribEditView* CMainFrame::GetRightPane()
{
	CWnd* pWnd = m_wndSplitter.GetPane(0, 1);
	CAttribEditView* pView = DYNAMIC_DOWNCAST(CAttribEditView, pWnd);
	return pView;
}

void CMainFrame::OnUpdateViewStyles(CCmdUI* pCmdUI)
{
	// TODO: customize or extend this code to handle choices on the
	// View menu.

	CAttribEditView* pView = GetRightPane(); 

	// if the right-hand pane hasn't been created or isn't a view,
	// disable commands in our range

	if (pView == NULL)
		pCmdUI->Enable(FALSE);
	else
	{
		DWORD dwStyle = pView->GetStyle() & LVS_TYPEMASK;

		// if the command is ID_VIEW_LINEUP, only enable command
		// when we're in LVS_ICON or LVS_SMALLICON mode

		if (pCmdUI->m_nID == ID_VIEW_LINEUP)
		{
			if (dwStyle == LVS_ICON || dwStyle == LVS_SMALLICON)
				pCmdUI->Enable();
			else
				pCmdUI->Enable(FALSE);
		}
		else
		{
			// otherwise, use dots to reflect the style of the view
			pCmdUI->Enable();
			BOOL bChecked = FALSE;

			switch (pCmdUI->m_nID)
			{
			case ID_VIEW_DETAILS:
				bChecked = (dwStyle == LVS_REPORT);
				break;

			case ID_VIEW_SMALLICON:
				bChecked = (dwStyle == LVS_SMALLICON);
				break;

			case ID_VIEW_LARGEICON:
				bChecked = (dwStyle == LVS_ICON);
				break;

			case ID_VIEW_LIST:
				bChecked = (dwStyle == LVS_LIST);
				break;

			default:
				bChecked = FALSE;
				break;
			}

			pCmdUI->SetRadio(bChecked ? 1 : 0);
		}
	}
}


void CMainFrame::OnViewStyle(unsigned int nCommandID)
{
	// TODO: customize or extend this code to handle choices on the
	// View menu.
	CAttribEditView* pView = GetRightPane();

	// if the right-hand pane has been created and is a CAttribEditView,
	// process the menu commands...
	if (pView != NULL)
	{
		DWORD dwStyle = -1;

		switch (nCommandID)
		{
		case ID_VIEW_LINEUP:
			{
				// ask the list control to snap to grid
				CListCtrl& refListCtrl = pView->GetListCtrl();
				refListCtrl.Arrange(LVA_SNAPTOGRID);
			}
			break;

		// other commands change the style on the list control
		case ID_VIEW_DETAILS:
			dwStyle = LVS_REPORT;
			break;

		case ID_VIEW_SMALLICON:
			dwStyle = LVS_SMALLICON;
			break;

		case ID_VIEW_LARGEICON:
			dwStyle = LVS_ICON;
			break;

		case ID_VIEW_LIST:
			dwStyle = LVS_LIST;
			break;
		}

		// change the style; window will repaint automatically
		if (dwStyle != -1)
			pView->ModifyStyle(LVS_TYPEMASK, dwStyle);
	}
}

void CMainFrame::OnUpdateClassDelete(CCmdUI* pCmdUI) 
{
	CLeftView *pLeftPane = GetLeftPane();

	if(pLeftPane->GetFocus() == pLeftPane && pLeftPane->GetSelectedClass())
		pCmdUI->Enable();
	else
		pCmdUI->Enable(FALSE);
}

void CMainFrame::OnUpdateClassDeleteMember(CCmdUI* pCmdUI) 
{
	CAttribEditView *pRightPane = GetRightPane();
	CListCtrl& list = pRightPane->GetListCtrl();

	if(pRightPane->GetFocus() == pRightPane &&
		pRightPane->GetEditedClass() &&
		list.GetSelectedCount() > 0)
		pCmdUI->Enable();
	else
		pCmdUI->Enable(FALSE);
}

void CMainFrame::OnUpdateClassNewMember(CCmdUI* pCmdUI) 
{
	CAttribEditView *pRightPane = GetRightPane();
	if(pRightPane->GetEditedClass())
		pCmdUI->Enable();
	else
		pCmdUI->Enable(FALSE);
}

void CMainFrame::OnUpdateClassInclude(CCmdUI* pCmdUI) 
{
	CAttribEditView *pRightPane = GetRightPane();
	pCmdUI->Enable(pRightPane->GetEditedClass() != NULL);
}

void CMainFrame::OnClassDelete() 
{
	CLeftView *pLeftPane = GetLeftPane();
	HTREEITEM hSelected = pLeftPane->GetTreeCtrl().GetSelectedItem();
	if(hSelected)
		pLeftPane->DeleteTreeItem(hSelected);
}

void CMainFrame::OnClassDeleteMember() 
{
	CAttribEditView *pRightPane = GetRightPane();

	pRightPane->DeleteSelected();
}

void CMainFrame::OnClassInclude() 
{
	CAttribEditView *pRightPane = GetRightPane();
	AttributeClassEx *pClassEx = pRightPane->GetEditedClass();
	if(pClassEx)
		m_pDoc->m_oper.IncludeClassPrompt(pClassEx);

}

void CMainFrame::OnClassNew() 
{
	m_pDoc->m_oper.AddNewClassPrompt();
}

void CMainFrame::OnClassNewMember() 
{
	CAttribEditView *pRightPane = GetRightPane();
	AttributeClassEx *pClassEx = pRightPane->GetEditedClass();
	if(pClassEx)
		m_pDoc->m_oper.AddNewMemberPrompt(pClassEx);
}

//
//        name: CMainFrame::OnUpdateDialogDelete etc
// description: Update functions for dialog functions toolbar and menu
//
void CMainFrame::OnUpdateDialogDelete(CCmdUI* pCmdUI) 
{
	CLeftView *pLeftPane = GetLeftPane();

	if(pLeftPane->GetFocus() == pLeftPane && pLeftPane->GetSelectedDialog())
		pCmdUI->Enable();
	else
		pCmdUI->Enable(FALSE);
}

void CMainFrame::OnUpdateDialogDeleteControl(CCmdUI* pCmdUI) 
{
	CAttribEditView *pRightPane = GetRightPane();
	CListCtrl& list = pRightPane->GetListCtrl();

	if(pRightPane->GetFocus() == pRightPane &&
		pRightPane->GetEditedDialog() &&
		list.GetSelectedCount() > 0)
		pCmdUI->Enable();
	else
		pCmdUI->Enable(FALSE);
}

void CMainFrame::OnUpdateDialogNew(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_pDoc->m_mgr.GetClassMap().size() != 0);
}

//
//        name: CMainFrame::OnDialogDelete etc
// description: Command functions for dialog functions toolbar and menu
//
void CMainFrame::OnDialogDelete() 
{
	CLeftView *pLeftPane = GetLeftPane();
	HTREEITEM hSelected = pLeftPane->GetTreeCtrl().GetSelectedItem();
	if(hSelected)
		pLeftPane->DeleteTreeItem(hSelected);
}

void CMainFrame::OnDialogDeleteControl() 
{
	CAttribEditView *pRightPane = GetRightPane();

	pRightPane->DeleteSelected();
}

void CMainFrame::OnDialogNew() 
{
	m_pDoc->m_oper.AddNewDialogPrompt();
}

void CMainFrame::OnUpdateDialogControl(CCmdUI* pCmdUI) 
{
	CAttribEditView *pRightPane = GetRightPane();
	pCmdUI->Enable(pRightPane->GetEditedDialog() != NULL);
}

void CMainFrame::OnDialogCheckbox() 
{
	CAttribEditView *pRightPane = GetRightPane();
	AttributeDialogDescEx *pDialogEx = pRightPane->GetEditedDialog();
	if(pDialogEx)
		m_pDoc->m_oper.AddNewCheckboxPrompt(pDialogEx);
}

void CMainFrame::OnDialogCombobox() 
{
	CAttribEditView *pRightPane = GetRightPane();
	AttributeDialogDescEx *pDialogEx = pRightPane->GetEditedDialog();
	if(pDialogEx)
		m_pDoc->m_oper.AddNewComboboxPrompt(pDialogEx);
}

void CMainFrame::OnDialogGroupbox() 
{
	CAttribEditView *pRightPane = GetRightPane();
	AttributeDialogDescEx *pDialogEx = pRightPane->GetEditedDialog();
	if(pDialogEx)
		m_pDoc->m_oper.AddNewGroupboxPrompt(pDialogEx);
}

void CMainFrame::OnDialogSpinner() 
{
	CAttribEditView *pRightPane = GetRightPane();
	AttributeDialogDescEx *pDialogEx = pRightPane->GetEditedDialog();
	if(pDialogEx)
		m_pDoc->m_oper.AddNewSpinnerPrompt(pDialogEx);
}

void CMainFrame::OnDialogEditbox() 
{
	CAttribEditView *pRightPane = GetRightPane();
	AttributeDialogDescEx *pDialogEx = pRightPane->GetEditedDialog();
	if(pDialogEx)
		m_pDoc->m_oper.AddNewEditboxPrompt(pDialogEx);
}

void CMainFrame::OnDialogFilebrowser() 
{
	CAttribEditView *pRightPane = GetRightPane();
	AttributeDialogDescEx *pDialogEx = pRightPane->GetEditedDialog();
	if(pDialogEx)
		m_pDoc->m_oper.AddNewFileBrowserPrompt(pDialogEx);
}
//
//        name: CMainFrame::OnUpdateMoveControlUp
// description: Control moving functions
//
void CMainFrame::OnUpdateMoveUp(CCmdUI* pCmdUI) 
{
	CAttribEditView *pRightPane = GetRightPane();
	CListCtrl& list = pRightPane->GetListCtrl();
	POSITION p = list.GetFirstSelectedItemPosition();
	int index = list.GetNextSelectedItem(p);

	if(list.GetSelectedCount() == 1 && 
		index > pRightPane->GetFirstElement())
	{
		pCmdUI->Enable();
		return;
	}
	pCmdUI->Enable(FALSE);
}
void CMainFrame::OnUpdateMoveDown(CCmdUI* pCmdUI) 
{
	CAttribEditView *pRightPane = GetRightPane();
	CListCtrl& list = pRightPane->GetListCtrl();
	POSITION p = list.GetFirstSelectedItemPosition();
	int index = list.GetNextSelectedItem(p);

	if(list.GetSelectedCount() == 1 && 
		index < list.GetItemCount() - 1 &&
		index >= pRightPane->GetFirstElement())
	{
		pCmdUI->Enable();
		return;
	}
	pCmdUI->Enable(FALSE);
}

void CMainFrame::OnMoveUp() 
{
	CAttribEditView *pRightPane = GetRightPane();
	CListCtrl& list = pRightPane->GetListCtrl();
	POSITION p = list.GetFirstSelectedItemPosition();
	int index = list.GetNextSelectedItem(p);

	if(pRightPane->GetEditedDialog())
	{
		m_pDoc->m_oper.SwapControls(pRightPane->GetEditedDialog(), 
									list.GetItemData(index), list.GetItemData(index-1));
	}
	else if(pRightPane->GetEditedClass())
	{
		m_pDoc->m_oper.SwapAttributes(pRightPane->GetEditedClass(), 
									list.GetItemData(index), list.GetItemData(index-1));
	}
	list.SetItemState(index-1, LVIS_SELECTED, LVIS_SELECTED);
}

void CMainFrame::OnMoveDown() 
{
	CAttribEditView *pRightPane = GetRightPane();
	CListCtrl& list = pRightPane->GetListCtrl();
	POSITION p = list.GetFirstSelectedItemPosition();
	int index = list.GetNextSelectedItem(p);

	if(pRightPane->GetEditedDialog())
	{
		m_pDoc->m_oper.SwapControls(pRightPane->GetEditedDialog(), 
									list.GetItemData(index), list.GetItemData(index+1));
	}
	else if(pRightPane->GetEditedClass())
	{
		m_pDoc->m_oper.SwapAttributes(pRightPane->GetEditedClass(), 
									list.GetItemData(index), list.GetItemData(index+1));
	}
	list.SetItemState(index+1, LVIS_SELECTED, LVIS_SELECTED);
}

//
//        name: CMainFrame::OnUpdateDialogTest
// description: Test dialog button
//
void CMainFrame::OnUpdateDialogTest(CCmdUI* pCmdUI) 
{
	CAttribEditView *pRightPane = GetRightPane();
	pCmdUI->Enable(pRightPane->GetEditedDialog() != NULL);
}

void CMainFrame::OnDialogTest() 
{
	CAttribEditView *pRightPane = GetRightPane();

	m_pDoc->m_oper.TestDialog(pRightPane->GetEditedDialog());
}

//
//        name: CMainFrame::OnUpdateClassDefault/OnClassEditDefault
// description: Edit default value for a class member
//
void CMainFrame::OnUpdateClassDefault(CCmdUI* pCmdUI) 
{
	CAttribEditView *pRightPane = GetRightPane();
	CListCtrl& list = pRightPane->GetListCtrl();
	POSITION p = list.GetFirstSelectedItemPosition();
	int index = list.GetNextSelectedItem(p);

	if( pRightPane->GetEditedClass() && 
		list.GetSelectedCount() == 1 && 
		index >= pRightPane->GetFirstElement())
	{
		pCmdUI->Enable();
		return;
	}
	pCmdUI->Enable(FALSE);
}

void CMainFrame::OnClassEditDefault() 
{
	CAttribEditView *pRightPane = GetRightPane();
	CListCtrl& list = pRightPane->GetListCtrl();
	POSITION p = list.GetFirstSelectedItemPosition();
	int index = list.GetNextSelectedItem(p);

	m_pDoc->m_oper.EditDefaultValue(pRightPane->GetEditedClass(), index);
}

//
//        name: CMainFrame::OnUpdateDialogAttributes
// description: Edit control attributes (e.g. spinner min and max values)
//
void CMainFrame::OnUpdateDialogAttributes(CCmdUI* pCmdUI) 
{
	CAttribEditView *pRightPane = GetRightPane();
	AttributeDialogDescEx *pDialogEx = pRightPane->GetEditedDialog();
	CListCtrl& list = pRightPane->GetListCtrl();
	POSITION p = list.GetFirstSelectedItemPosition();
	int index = list.GetNextSelectedItem(p);

	if(	pDialogEx && 
		list.GetSelectedCount() == 1 && 
		index >= pRightPane->GetFirstElement())
	{
		dmat::AttributeDialogDesc *pDialog = pDialogEx->GetAttributeDialog();
		dmat::AttributeControlDesc::ControlType type = pDialog->GetControl(list.GetItemData(index))->GetType();
		if(	type == dmat::AttributeControlDesc::SPINNER ||
			type == dmat::AttributeControlDesc::COMBOBOX || 
			type == dmat::AttributeControlDesc::TCOMBOBOX)
		{
			pCmdUI->Enable();
			return;
		}
	}
	pCmdUI->Enable(FALSE);
}

void CMainFrame::OnDialogAttributes() 
{
	CAttribEditView *pRightPane = GetRightPane();
	CListCtrl& list = pRightPane->GetListCtrl();
	POSITION p = list.GetFirstSelectedItemPosition();
	int index = list.GetNextSelectedItem(p);

	m_pDoc->m_oper.EditControlProperties(pRightPane->GetEditedDialog(), index);
}

void CMainFrame::OnHelpAttribedit() 
{
	::HtmlHelp(*this, "s:\\Tools_Release\\tr.chm", HH_DISPLAY_TOPIC, (DWORD)"Help\\attribedithelp.htm");
}

void CMainFrame::OnHelpAttribute() 
{
	::HtmlHelp(*this, "s:\\Tools_Release\\tr.chm", HH_DISPLAY_TOPIC, (DWORD)"Help\\attributes.htm");
}


void CMainFrame::OnDialogAuto() 
{
	m_pDoc->m_oper.AddNewAutomaticDialogPrompt();
}
