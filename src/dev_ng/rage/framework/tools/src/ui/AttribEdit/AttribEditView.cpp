// AttribEditView.cpp : implementation of the CAttribEditView class
//

#include "stdafx.h"
#include "AttribEdit.h"
#include "AttribEditDoc.h"
#include "LeftView.h"
#include "AttribEditView.h"
#include "TreeProcess.h"

#include "AttribMgrEx.h"
#include "AttribExProcess.h"

// STD headers
#include <set>
#include <strstream>



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAttribEditView

IMPLEMENT_DYNCREATE(CAttribEditView, CListView)

BEGIN_MESSAGE_MAP(CAttribEditView, CListView)
	//{{AFX_MSG_MAP(CAttribEditView)
	ON_WM_LBUTTONDBLCLK()
	ON_NOTIFY_REFLECT(LVN_ENDLABELEDIT, OnEndlabeledit)
	ON_NOTIFY_REFLECT(LVN_KEYDOWN, OnKeydown)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAttribEditView construction/destruction

CAttribEditView::CAttribEditView() : m_pObjectEx(NULL)
{
	m_imageList.Create(16, 16, ILC_COLOR8, 8, 10);

	m_imageList.SetBkColor(RGB(255,255,255));

	m_imageList.Add(theApp.LoadIcon(IDI_CLASS));
	m_imageList.Add(theApp.LoadIcon(IDI_ELEMENT));
	m_imageList.Add(theApp.LoadIcon(IDI_DIALOG));
	m_imageList.Add(theApp.LoadIcon(IDI_SPINNER));
	m_imageList.Add(theApp.LoadIcon(IDI_GROUPBOX));
	m_imageList.Add(theApp.LoadIcon(IDI_EDITBOX));
	m_imageList.Add(theApp.LoadIcon(IDI_COMBOBOX));
	m_imageList.Add(theApp.LoadIcon(IDI_CHECKBOX));
	m_imageList.Add(theApp.LoadIcon(IDI_FILEBROWSER));
}

CAttribEditView::~CAttribEditView()
{
}

BOOL CAttribEditView::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_EDITLABELS | LVS_REPORT | LVS_NOSORTHEADER | LVS_NOCOLUMNHEADER | LVS_SHOWSELALWAYS;

	return CListView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CAttribEditView drawing

void CAttribEditView::OnDraw(CDC* pDC)
{
	CAttribEditDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
}

//
//        name: CAttribEditView::DeleteItem
// description: Delete one item 
//          in: item = index of item
//
void CAttribEditView::DeleteItem(int item)
{
	CAttribEditDoc* pDoc = GetDocument();
	CListCtrl& listCtrl = GetListCtrl();
	LVITEM lvItem;

	if(m_pObjectEx)
	{
		lvItem.iItem = item;
		lvItem.mask = LVIF_PARAM;
		listCtrl.GetItem(&lvItem);
		// delete class included
		if(item < m_firstElement)
		{
			pDoc->m_oper.DeleteChildObject(m_pObjectEx, (ObjectEx *)lvItem.lParam);
		}
		// delete element
		else
		{
			pDoc->m_oper.DeleteElement(m_pObjectEx, lvItem.lParam);
		}
	}

}

//
//        name: CAttribEditView::DeleteSelected
// description: Delete selected items
//
bool CAttribEditView::DeleteSelected()
{
	CListCtrl& listCtrl = GetListCtrl();
	POSITION p = listCtrl.GetFirstSelectedItemPosition();
	int index;
	std::set<int> m_selected;		

	if(listCtrl.GetSelectedCount() > 1)
	{
		if(MessageBox("Are you sure you want to delete the selected items?", 
			"Confirm multiple delete", 
			MB_ICONWARNING|MB_YESNO) == IDNO)
			return false;
	}
	else if(listCtrl.GetSelectedCount() == 1)
	{
		std::ostrstream msg;
		index = listCtrl.GetNextSelectedItem(p);
		p = listCtrl.GetFirstSelectedItemPosition();

		// if editing dialog and the first list item is selected then ignore
		if(m_pObjectEx && m_pObjectEx->AsDialog() && index == 0)
		{
			return false;
		}

		msg << "Are you sure you want to delete \"" << (const char *)(listCtrl.GetItemText(index, 0)) 
			<< "\"?" << std::ends;
		if(MessageBox(msg.str(), 
			"Confirm delete", 
			MB_ICONQUESTION|MB_YESNO) == IDNO)
			return false;
	}
	else 
		return true;

	while(p)
	{
		index = listCtrl.GetNextSelectedItem(p);
		m_selected.insert(index);
	}

	// delete items in reverse order to stop the application crashing
	std::set<int>::reverse_iterator iIndex;

	for(iIndex = m_selected.rbegin(); iIndex != m_selected.rend(); iIndex++)
	{
		DeleteItem(*iIndex);
	}

	RefreshObject();
	return true;
}


//
//        name: GetEditedClass/GetEditedDialog
// description: Return pointer to class or dialog being edited
//
AttributeClassEx* CAttribEditView::GetEditedClass()
{
	if(m_pObjectEx)
		return m_pObjectEx->AsClass();
	return NULL;
}
AttributeDialogDescEx* CAttribEditView::GetEditedDialog()
{
	if(m_pObjectEx)
		return m_pObjectEx->AsDialog();
	return NULL;
}

//
//        name: CAttribEditView::DeleteAllColumns
// description: Delete all columns in view
//
void CAttribEditView::DeleteAllColumns()
{
	CListCtrl& listCtrl = GetListCtrl();

	CHeaderCtrl* pHeader = listCtrl.GetHeaderCtrl();

	if(pHeader)
	{
		int nColumnCount = pHeader->GetItemCount();

		// Delete all of the columns.
		for (int i=0;i < nColumnCount;i++)
		{
			listCtrl.DeleteColumn(0);
		}
	}
	ModifyStyle(0, LVS_NOCOLUMNHEADER);
}

//
//        name: CAttribEditView::AddClassColumns
// description: Add columns descibing a class
//
void CAttribEditView::SetClassColumns()
{
	CListCtrl& listCtrl = GetListCtrl();

	DeleteAllColumns();
	listCtrl.InsertColumn(0, "Field name", LVCFMT_LEFT, 150);
	listCtrl.InsertColumn(1, "Type", LVCFMT_LEFT, 80);
	listCtrl.InsertColumn(2, "Default value", LVCFMT_LEFT, 200);
	ModifyStyle(LVS_NOCOLUMNHEADER, 0);
}

//
//        name: CAttribEditView::AddClassColumns
// description: Add columns descibing a dialog
//
void CAttribEditView::SetDialogColumns()
{
	CListCtrl& listCtrl = GetListCtrl();

	DeleteAllColumns();
	listCtrl.InsertColumn(0, "Field name", LVCFMT_LEFT, 150);
	listCtrl.InsertColumn(1, "Control", LVCFMT_LEFT, 100);
	listCtrl.InsertColumn(2, "Control specific information", LVCFMT_LEFT, 200);
	ModifyStyle(LVS_NOCOLUMNHEADER, 0);
}

//
//        name: CAttribEditView::RefreshObject
// description: Redraw the contents of object
//
void CAttribEditView::RefreshObject()
{
	CListCtrl& listCtrl = GetListCtrl();

	// remove all items
	listCtrl.DeleteAllItems();

	if(m_pObjectEx == NULL)
	{
		DeleteAllColumns();
		return;
	}

	CAttribEditDoc* pDoc = GetDocument();
	AttributeClassEx *pClassEx = m_pObjectEx->AsClass();
	AttributeDialogDescEx *pDialogEx = m_pObjectEx->AsDialog();
	int i;


	// if object is a class
	if(pClassEx)
	{
		dmat::AttributeClass *pClass = pClassEx->GetAttributeClass();
		std::vector<std::string> &children = pClassEx->GetChildrenVector();
		int numChildren = children.size();
		int index;

		SetClassColumns();

		// add all classes that have been included
		for(i=0; i<numChildren; i++)
		{
			listCtrl.InsertItem(i, children[i].c_str(), 0);
			listCtrl.SetItemData(i, (unsigned int)(pDoc->m_mgr.GetClass(children[i])));
		}

		// set index of first element
		m_firstElement = numChildren;
		index = numChildren;

		// add all the attributes in the class that aren't in child classes
		for(i=0; i<pClass->GetSize(); i++)
		{
			int id = pClass->GetId(i);
			char buffer[1024];
			const dmat::Attribute& attr = (*pClass)[id];

			listCtrl.InsertItem(index, attr.GetName().c_str(), 1);
			// print type and default value
			switch(attr.GetType())
			{
			case dmat::Attribute::FLOAT:
				listCtrl.SetItemText(index, 1, "Float");
				sprintf(buffer, "%f", (float)attr.GetDefault());
				listCtrl.SetItemText(index, 2, buffer);
				break;
			case dmat::Attribute::INT:
				listCtrl.SetItemText(index, 1, "Int");
				sprintf(buffer, "%i", (int)attr.GetDefault());
				listCtrl.SetItemText(index, 2, buffer);
				break;
			case dmat::Attribute::BOOL:
				listCtrl.SetItemText(index, 1, "Boolean");
				if((bool)attr.GetDefault())
					listCtrl.SetItemText(index, 2, "True");
				else
					listCtrl.SetItemText(index, 2, "False");
				break;
			case dmat::Attribute::STRING:
				listCtrl.SetItemText(index, 1, "String");
				sprintf(buffer, "\"%s\"", (const char *)attr.GetDefault());
				listCtrl.SetItemText(index, 2, buffer);
				break;
			}
			listCtrl.SetItemData(index, id);
			index++;
		}
	}
	// if object is a dialog
	else if(pDialogEx)
	{
		dmat::AttributeDialogDesc* pDialog = pDialogEx->GetAttributeDialog();
		dmat::AttributeClass* pClass = pDoc->m_mgr.GetManager().GetClass(pDialogEx->GetClassEdited());
		std::vector<std::string> &children = pDialogEx->GetChildrenVector();
		int numChildren = children.size();
		int index;

		SetDialogColumns();

		// add class being edited
		listCtrl.InsertItem(0, (std::string("Editing \"") + pDialogEx->GetClassEdited() + "\"").c_str(), 0);
		listCtrl.SetItemData(0, (unsigned int)(pDoc->m_mgr.GetClass(pDialogEx->GetClassEdited())));


		// set index of first element
		m_firstElement = 1;
		index = 1;

		for(i=0; i<pDialog->GetSize(); i++)
		{
			std::strstream info;
			dmat::AttributeControlDesc* pControl = pDialog->GetControl(i);
			dmat::AttributeValueControlDesc* pVControl = pControl->AsValueControl();
			const dmat::Attribute *pAttr;
			if(pVControl != NULL)
				pAttr = &(*pClass)[pVControl->GetAttributeId()];
			// print type and default value
			switch(pControl->GetType())
			{
			case dmat::AttributeControlDesc::SPINNER:
				listCtrl.InsertItem(index, pAttr->GetName().c_str(), 3);
				listCtrl.SetItemText(index, 1, "Spinner");
				// Spinner can either edit integers or floating point
				if(pVControl->GetEditType() == dmat::Attribute::INT)
				{
					int min,max;
					((dmat::IntSpinnerControlDesc *)pControl)->GetLimits(min,max);
					info << "Min: " << min << ", Max: " << max << std::ends;
				}
				else
				{
					float min,max;
					((dmat::FloatSpinnerControlDesc *)pControl)->GetLimits(min,max);
					info << "Min: " << min << ", Max: " << max << std::ends;
				}
				listCtrl.SetItemText(index, 2, info.str());
				break;
			case dmat::AttributeControlDesc::GROUPBOX:
				listCtrl.InsertItem(index, pControl->GetName().c_str(), 4);
				listCtrl.SetItemText(index, 1, "GroupBox");
				info << "Using Dialog: \"";
				info << pDialogEx->GetDialogNameFromPtr(pControl->GetControlList()) << "\"" << std::ends;
				listCtrl.SetItemText(index, 2, info.str());
				break;
			case dmat::AttributeControlDesc::EDITBOX:
				listCtrl.InsertItem(index, pAttr->GetName().c_str(), 5);
				listCtrl.SetItemText(index, 1, "EditBox");
				break;
			case dmat::AttributeControlDesc::COMBOBOX:
			case dmat::AttributeControlDesc::TCOMBOBOX:
				listCtrl.InsertItem(index, pAttr->GetName().c_str(), 6);
				listCtrl.SetItemText(index, 1, "ComboBox");
				info << "Entries: " << ((dmat::ComboBoxControlDesc *)pControl)->GetEntriesString() << std::ends;
				listCtrl.SetItemText(index, 2, info.str());
				break;
			case dmat::AttributeControlDesc::CHECKBOX:
				listCtrl.InsertItem(index, pAttr->GetName().c_str(), 7);
				listCtrl.SetItemText(index, 1, "CheckBox");
				break;
			case dmat::AttributeControlDesc::FILEBROWSER:
				listCtrl.InsertItem(index, pAttr->GetName().c_str(), 8);
				listCtrl.SetItemText(index, 1, "File Browser");
				info << "Ext: *." << ((dmat::FileBrowserControlDesc *)pControl)->GetExtension();
				info << ", Desc: \"" << ((dmat::FileBrowserControlDesc *)pControl)->GetFileDescription() << "\"" << std::ends;
				listCtrl.SetItemText(index, 2, info.str());
				break;
			}
			listCtrl.SetItemData(index, i);
			index++;
		}
	}
	else
		assert(0);
}

//
//        name: CAttribEditView::SetCurrentObject
// description: pObj = pointer to object
//
void CAttribEditView::SetCurrentObject(ObjectEx *pObj)
{
	m_pObjectEx = pObj;

	RefreshObject();
}

//
//        name: CAttribEditView::OnInitialUpdate
// description: Called just before file is displayed for the first time
//
void CAttribEditView::OnInitialUpdate()
{
	CAttribEditDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	CListView::OnInitialUpdate();

	pDoc->SetRightView(this);

	GetListCtrl().SetImageList(&m_imageList, LVSIL_NORMAL);
	GetListCtrl().SetImageList(&m_imageList, LVSIL_SMALL);

	SetCurrentObject(NULL);
}


/////////////////////////////////////////////////////////////////////////////
// CAttribEditView diagnostics

#ifdef _DEBUG
void CAttribEditView::AssertValid() const
{
	CListView::AssertValid();
}

void CAttribEditView::Dump(CDumpContext& dc) const
{
	CListView::Dump(dc);
}

CAttribEditDoc* CAttribEditView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CAttribEditDoc)));
	return (CAttribEditDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CAttribEditView message handlers
void CAttribEditView::OnStyleChanged(int nStyleType, LPSTYLESTRUCT lpStyleStruct)
{
	//TODO: add code to react to the user changing the view style of your window
}


void CAttribEditView::OnLButtonDblClk(unsigned int nFlags, CPoint point) 
{
	CListCtrl& listCtrl = GetListCtrl();
	unsigned int flags;
	int index;
	ObjectEx *pObj;
	CAttribEditDoc* pDoc = GetDocument();

	index = listCtrl.HitTest(point, &flags);

	if(index != -1 && index < m_firstElement)
	{
		pObj = (ObjectEx *)listCtrl.GetItemData(index);
		if(pObj)
		{
			pDoc->m_pLeftView->GetTreeCtrl().SelectItem(NULL);
			SetCurrentObject(pObj);
		}
	}
	else if(index >= m_firstElement)
	{
		AttributeClassEx *pClass = GetEditedClass();
		AttributeDialogDescEx *pDialog = GetEditedDialog();

		if(pClass)
		{
			pDoc->m_oper.EditDefaultValue(pClass, index);
		}
		else if(pDialog)
		{
			pDoc->m_oper.EditControlProperties(pDialog, index);
		}
	}

	CListView::OnLButtonDblClk(nFlags, point);
}

void CAttribEditView::OnEndlabeledit(NMHDR* pNMHDR, LRESULT* pResult) 
{
	LV_DISPINFO* pDispInfo = (LV_DISPINFO*)pNMHDR;
	CListCtrl& listCtrl = GetListCtrl();
	LVITEM lvItem;
	CAttribEditDoc* pDoc = GetDocument();

	*pResult = 0;
	// if text wasn't changed
	if(pDispInfo->item.pszText == NULL)
		return;
	if(*pDispInfo->item.pszText == '\0')
		return;

	// there is two cases: one where a class name is being changed and one where an attribute
	// name is being changed
	// if index is less than first element it must be a class
	lvItem.iItem = pDispInfo->item.iItem;
	lvItem.mask = LVIF_PARAM;
	listCtrl.GetItem(&lvItem);
	if(lvItem.iItem < m_firstElement)
	{
		*pResult = pDoc->m_oper.RenameObject((ObjectEx *)lvItem.lParam, pDispInfo->item.pszText);
		RefreshObject();
	}
	else
	{
		if(m_pObjectEx->AsClass())
		{
			*pResult = m_pObjectEx->AsClass()->RenameAttribute(pDispInfo->item.pszText, lvItem.lParam);
		}
		else
		{
			*pResult = 0;
		}
	}
}

//
//        name: CAttribEditView::OnKeydown
// description: Called when a key is pressed. Needed for when delete is pressed
//
void CAttribEditView::OnKeydown(NMHDR* pNMHDR, LRESULT* pResult) 
{
	LV_KEYDOWN* pLVKeyDow = (LV_KEYDOWN*)pNMHDR;

	*pResult = 0;

	// has delete key been pressed
	if(pLVKeyDow->wVKey == VK_DELETE)
	{
		DeleteSelected();
		*pResult = 1;
	}
}
