// CFileBrowserDialog.cpp : implementation file
//

#include "stdafx.h"
#include "AttribEdit.h"
#include "FileBrowserDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFileBrowserDialog dialog


CFileBrowserDialog::CFileBrowserDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CFileBrowserDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CFileBrowserDialog)
	m_extension = _T("");
	m_description = _T("");
	//}}AFX_DATA_INIT
}


void CFileBrowserDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFileBrowserDialog)
	DDX_Text(pDX, IDC_EXTENSION, m_extension);
	DDV_MaxChars(pDX, m_extension, 3);
	DDX_Text(pDX, IDC_DESCRIPTION, m_description);
	DDV_MaxChars(pDX, m_description, 19);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFileBrowserDialog, CDialog)
	//{{AFX_MSG_MAP(CFileBrowserDialog)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFileBrowserDialog message handlers

void CFileBrowserDialog::OnOK()
{
}
void CFileBrowserDialog::OnCancel()
{
}
