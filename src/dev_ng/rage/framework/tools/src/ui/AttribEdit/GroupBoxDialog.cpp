// GroupBoxDialog.cpp : implementation file
//

#include "stdafx.h"
#include "AttribEdit.h"
#include "GroupBoxDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGroupBoxDialog dialog


CGroupBoxDialog::CGroupBoxDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CGroupBoxDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CGroupBoxDialog)
	m_name = _T("");
	//}}AFX_DATA_INIT
}


void CGroupBoxDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGroupBoxDialog)
	DDX_Text(pDX, IDC_NAME, m_name);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CGroupBoxDialog, CDialog)
	//{{AFX_MSG_MAP(CGroupBoxDialog)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGroupBoxDialog message handlers

void CGroupBoxDialog::OnOK()
{
}
void CGroupBoxDialog::OnCancel()
{
}