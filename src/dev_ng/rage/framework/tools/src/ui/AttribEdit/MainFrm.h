// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__A53575FD_2ECF_11D3_98A5_00902709243B__INCLUDED_)
#define AFX_MAINFRM_H__A53575FD_2ECF_11D3_98A5_00902709243B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CAttribEditDoc;
class CAttribEditView;
class CLeftView;

class CMainFrame : public CFrameWnd
{
	
protected: // create from serialization only
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

// Attributes
protected:
	CSplitterWnd m_wndSplitter;
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	public:
	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMainFrame();
	CAttribEditView* GetRightPane();
	CLeftView* GetLeftPane();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members
	CStatusBar  m_wndStatusBar;
	CToolBar    m_wndToolBar;
	CToolBar    m_classToolBar;
	CToolBar    m_dialogToolBar;
	CToolBar    m_moveToolBar;
	CReBar      m_wndReBar;
	CAttribEditDoc* m_pDoc;
	//CDialogBar      m_wndDlgBar;

// Generated message map functions
protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnUpdateClassDelete(CCmdUI* pCmdUI);
	afx_msg void OnUpdateClassDeleteMember(CCmdUI* pCmdUI);
	afx_msg void OnUpdateClassNewMember(CCmdUI* pCmdUI);
	afx_msg void OnUpdateClassInclude(CCmdUI* pCmdUI);
	afx_msg void OnClassDelete();
	afx_msg void OnClassDeleteMember();
	afx_msg void OnClassInclude();
	afx_msg void OnClassNew();
	afx_msg void OnClassNewMember();
	afx_msg void OnUpdateDialogDelete(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDialogDeleteControl(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDialogNew(CCmdUI* pCmdUI);
	afx_msg void OnDialogDelete();
	afx_msg void OnDialogDeleteControl();
	afx_msg void OnDialogNew();
	afx_msg void OnUpdateDialogControl(CCmdUI* pCmdUI);
	afx_msg void OnDialogCheckbox();
	afx_msg void OnDialogCombobox();
	afx_msg void OnDialogGroupbox();
	afx_msg void OnDialogSpinner();
	afx_msg void OnDialogEditbox();
	afx_msg void OnDialogFilebrowser();
	afx_msg void OnUpdateMoveUp(CCmdUI* pCmdUI);
	afx_msg void OnUpdateMoveDown(CCmdUI* pCmdUI);
	afx_msg void OnMoveUp();
	afx_msg void OnMoveDown();
	afx_msg void OnUpdateDialogTest(CCmdUI* pCmdUI);
	afx_msg void OnDialogTest();
	afx_msg void OnUpdateClassDefault(CCmdUI* pCmdUI);
	afx_msg void OnClassEditDefault();
	afx_msg void OnUpdateDialogAttributes(CCmdUI* pCmdUI);
	afx_msg void OnDialogAttributes();
	afx_msg void OnHelpAttribedit();
	afx_msg void OnHelpAttribute();
	afx_msg void OnDialogAuto();
	//}}AFX_MSG
	afx_msg void OnUpdateViewStyles(CCmdUI* pCmdUI);
	afx_msg void OnViewStyle(unsigned int nCommandID);
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__A53575FD_2ECF_11D3_98A5_00902709243B__INCLUDED_)
