//
//
//    Filename: AttribScriptEx.h
//     Creator: Adam Fowler
//     $Author: Adam $
//       $Date: 9/07/99 15:54 $
//   $Revision: 1 $
// Description: Class for reading extra information from an attribute script file
//
//
#ifndef INC_ATTRIB_SCRIPT_EX_H_
#define INC_ATTRIB_SCRIPT_EX_H_

// my headers
//#include <dma.h>
#include "AttribMgrEx.h"
// STD headers
#include <fstream>
#include <set>


//
//   Class Name: AttributeScriptFileEx
// Base Classes: std::basic_ifstream<char>
//  Description: 
//    Functions: 
//
//
class AttributeScriptFileEx : private std::basic_ifstream<char>
{
public:
	AttributeScriptFileEx(const char *pFilename);
	
	void Read(AttributeManagerEx &manager);
	
private:
	void ReadClass();
	void ReadDialog();
	
	AttributeManagerEx *m_pManager;
};

//
//   Class Name: AttributeScriptFileEx
// Base Classes: std::basic_ifstream<char>
//  Description: 
//    Functions: 
//
//
class AttributeScriptFileOutEx : private std::basic_ofstream<char>
{
public:
	AttributeScriptFileOutEx(const char *pFilename);
	
	void Write(AttributeManagerEx &manager);
	
private:
	void WriteHeader();
	void WriteClass(AttributeClassEx *pClassEx);
	void WriteDialog(AttributeDialogDescEx *pDialogEx);
	
	std::string m_filename;
	std::set<std::string> m_writtenClasses;
	std::set<std::string> m_writtenDialogs;
};



#endif // INC_ATTRIB_SCRIPT_H_

