// AttribEditDoc.cpp : implementation of the CAttribEditDoc class
//

#include "stdafx.h"
#include "AttribEdit.h"
#include "LeftView.h"
#include "AttribEditView.h"
#include "AttribEditDoc.h"

// STD headers
#include <strstream>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAttribEditDoc

IMPLEMENT_DYNCREATE(CAttribEditDoc, CDocument)

BEGIN_MESSAGE_MAP(CAttribEditDoc, CDocument)
	//{{AFX_MSG_MAP(CAttribEditDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAttribEditDoc construction/destruction

CAttribEditDoc::CAttribEditDoc() : m_oper(m_mgr, this)
{
	m_pLeftView = NULL;
	m_pRightView = NULL;
}

CAttribEditDoc::~CAttribEditDoc()
{
}

BOOL CAttribEditDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	if(m_pLeftView)
		m_pLeftView->DeleteTree();
	m_mgr.Clear();
	ObjectEx::SetManager(&m_mgr);
	m_oper.Clear();

	return TRUE;
}

//
//        name: CAttribEditDoc::SetLeftView/SetRightView
// description: Set pointers to views
//
void CAttribEditDoc::SetLeftView(CLeftView *pLeftView)
{
	m_pLeftView = pLeftView;
	m_oper.SetTreeCtrl(&pLeftView->GetTreeCtrl());
}
void CAttribEditDoc::SetRightView(CAttribEditView *pRightView)
{
	m_pRightView = pRightView;
	m_oper.SetListCtrl(&pRightView->GetListCtrl());
}


/////////////////////////////////////////////////////////////////////////////
// CAttribEditDoc diagnostics

#ifdef _DEBUG
void CAttribEditDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CAttribEditDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CAttribEditDoc commands

BOOL CAttribEditDoc::OnOpenDocument(LPCTSTR lpszPathName) 
{
	if (!CDocument::OnOpenDocument(lpszPathName))
		return FALSE;
	
	m_mgr.Clear();
	m_oper.Clear();
	try
	{
		m_mgr.ReadScriptFile(lpszPathName);
	}
	catch (dmat::AttributeScriptError err)
	{
		std::ostrstream str;
		str << "Line " << err.GetLineNumber() << ": " << err.GetError() << std::ends;
		MessageBox(NULL, str.str(), "Error", MB_OK);
		if(m_pLeftView)
			m_pLeftView->DeleteTree();
		m_mgr.Clear();
		return FALSE;
	}
	catch (dmat::AttributeFileError err)
	{
		MessageBox(NULL, err.GetError().c_str(), "Error", MB_OK);
		if(m_pLeftView)
			m_pLeftView->DeleteTree();
		m_mgr.Clear();
		return FALSE;
	}

	ObjectEx::SetManager(&m_mgr);

	return TRUE;
}

BOOL CAttribEditDoc::OnSaveDocument(LPCTSTR lpszPathName) 
{
	try
	{
		m_mgr.WriteScriptFile(lpszPathName);
	}
	catch (dmat::AttributeFileError err)
	{
		return FALSE;
	}
	SetModifiedFlag(FALSE);
	return TRUE;
}
