//
//
//    Filename: AttribMgrEx.h
//     Creator: Adam Fowler
//     $Author: Adam $
//       $Date: 9/07/99 15:54 $
//   $Revision: 1 $
// Description: This is a class which controls the extended versions of the attributes classes
//
//
#ifndef INC_ATTRIBMGR_EX_H_
#define INC_ATTRIBMGR_EX_H_


#pragma warning (disable : 4786)

// my headers
#include "AttribMgr.h"
#include "AttributeEx.h"
// STD headers
#include <map>
#include <string>

typedef std::map<std::string, AttributeClassEx> AttributeClassExMap;
typedef std::map<std::string, AttributeClassEx>::iterator AttributeClassExMapIterator;
typedef std::map<std::string, AttributeClassEx>::const_iterator AttributeClassExMapConstIterator;

typedef std::map<std::string, AttributeDialogDescEx> AttributeDialogExMap;
typedef std::map<std::string, AttributeDialogDescEx>::iterator AttributeDialogExMapIterator;
typedef std::map<std::string, AttributeDialogDescEx>::const_iterator AttributeDialogExMapConstIterator;

//
//   Class Name: AttributeManager
// Base Classes: 
//  Description: Class controlling the attribute classes
//    Functions: ReadScriptFile(): read attributes and dialogs from script file
//				AddClass(): add a new attribute class and associate it with a name
//				AddClassEx(): add just the extended information for an attribute class
//				GetClassMap(): return reference to name,class map
//				GetClass(): return class with associated name
//				AddDialogDesc(): add a new dialog description and associate it with a name
//				AddDialogDescEx(): add the extended information for a dialog description
//				GetDialogDesc(): return dialog with associated name
//				GetDialogMap(): return reference to name,dialog map
//				Clear(): remove all classes and dialogs from the Manager
//
//
class AttributeManagerEx
{
public:
	AttributeManagerEx() : m_nextId(0) {}

	void ReadScriptFile(const char *pFilename) throw(dmat::AttributeFileError);
	void WriteScriptFile(const char *pFilename);
	
	dmat::AttributeManager& GetManager() {return m_mgr;}
	
	void AddClass(AttributeClassEx &aClassEx, dmat::AttributeClass &aClass);
	void AddClassEx(AttributeClassEx &aClassEx);
	void RemoveClass(const std::string &name);
	bool RenameClass(const std::string &name, const std::string &newName);
	AttributeClassEx *GetClass(const std::string &className) ;
	AttributeClassExMap &GetClassMap() {return m_classMap;}
	
	void AddDialogDesc(AttributeDialogDescEx &dialogEx, dmat::AttributeDialogDesc &dialog);
	void AddDialogDescEx(AttributeDialogDescEx &dialogEx);
	void RemoveDialogDesc(const std::string &name);
	bool RenameDialogDesc(const std::string &name, const std::string &newName);
	AttributeDialogDescEx *GetDialogDesc(const std::string &dialogName) ;
	AttributeDialogExMap &GetDialogMap() {return m_dialogMap;}
	
	void Clear();
	int GetNextId() {return ++m_nextId;}

private:
	dmat::AttributeManager m_mgr;
	AttributeClassExMap m_classMap;
	AttributeDialogExMap m_dialogMap;
	int m_nextId;
};



#endif