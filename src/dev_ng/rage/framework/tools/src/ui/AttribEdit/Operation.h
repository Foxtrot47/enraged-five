// Operation.h: interface for the COperation class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_OPERATION_H__9AB20EE5_337F_11D3_8E9B_00902709243B__INCLUDED_)
#define AFX_OPERATION_H__9AB20EE5_337F_11D3_8E9B_00902709243B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#pragma warning (disable : 4786)

#include <string>
#include <map>

class AttributeManagerEx;
class CAttribEditDoc;
class CTreeView;
class CListView;
class ObjectEx;
class CAddControlDialog;


typedef std::multimap<std::string, std::pair<int, dmat::Attribute> > ClassAttributeMMap;
typedef std::map<std::string, dmat::AttributeClass> NameClassMap;

class COperation
{
public:
	COperation(AttributeManagerEx& mgr, CAttribEditDoc* pDoc) : m_mgr(mgr), m_pDoc(pDoc), 
		m_pTree(NULL),  m_pList(NULL) {}
	virtual ~COperation();

	void SetTreeCtrl(CTreeCtrl* pTree) {m_pTree = pTree;}
	void SetListCtrl(CListCtrl* pList) {m_pList = pList;}

	bool DeleteObject(ObjectEx *pObjectEx);
	bool DeleteChildObjectPrompt(ObjectEx *pParentObj, ObjectEx *pObjectEx);
	void DeleteChildObject(ObjectEx *pParentObj, ObjectEx *pObjectEx);
	void DeleteDialogsEditingClass(AttributeClassEx *pClassEx);
	void DeleteChildDialogsEditingClass(AttributeClassEx *pClassEx, AttributeClassEx *pChildClassEx);
	void DeleteControlsEditingAttribute(AttributeClassEx *pClassEx, int id);
	void DeleteElement(ObjectEx *pObjectEx, int id);
	bool RenameObject(ObjectEx *pObjectEx, const char *pNewName);

	void AddNewClassPrompt();
	void AddNewDialogPrompt();
	void AddNewAutomaticDialogPrompt();
	bool AddNewClass(const std::string& className);
	void CopyDeletedClass(dmat::AttributeClass& aClass, const std::string& className);
	bool AddNewDialog(const std::string& dialogName, const std::string& className);

	void IncludeClassPrompt(AttributeClassEx *pClassEx);
	void IncludeClass(AttributeClassEx *pClassEx, AttributeClassEx *pChildClassEx);
	void AddNewMemberPrompt(AttributeClassEx *pClassEx);
	int GetAttributeId(const AttributeClassEx *pClassEx, const dmat::Attribute& attr);
	void IncludeDialog(AttributeDialogDescEx *pDialogEx, AttributeDialogDescEx *pChildDialogEx);

	void AddCorrectMembers(CAddControlDialog& dialog, AttributeDialogDescEx *pDialogEx, 
							dmat::Attribute::Type type);
	bool RunControlDialog(CAddControlDialog& dialog, const char *pControlName);
	void AddNewEditboxPrompt(AttributeDialogDescEx *pDialogEx);
	void AddNewGroupboxPrompt(AttributeDialogDescEx *pDialogEx);
	void AddNewComboboxPrompt(AttributeDialogDescEx *pDialogEx);
	void AddNewCheckboxPrompt(AttributeDialogDescEx *pDialogEx);
	void AddNewSpinnerPrompt(AttributeDialogDescEx *pDialogEx);
	void AddNewFileBrowserPrompt(AttributeDialogDescEx *pDialogEx);

	void SwapControls(AttributeDialogDescEx *pDialogEx, int index1, int index2);
	void SwapAttributes(AttributeClassEx *pClassEx, int id1, int id2);

	void TestDialog(AttributeDialogDescEx *pDialogEx);
	void EditDefaultValue(AttributeClassEx *pClassEx, int posn);
	void EditControlProperties(AttributeDialogDescEx *pDialogEx, int posn);

	void Clear();

protected:
	CTreeCtrl* m_pTree;
	CListCtrl* m_pList;
	AttributeManagerEx& m_mgr;
	CAttribEditDoc* m_pDoc;

	ClassAttributeMMap m_deletedAttributes;
	NameClassMap m_deletedClasses;
};

#endif // !defined(AFX_OPERATION_H__9AB20EE5_337F_11D3_8E9B_00902709243B__INCLUDED_)
