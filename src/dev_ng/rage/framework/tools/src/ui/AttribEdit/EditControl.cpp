// EditControl.cpp : implementation file
//

#include "stdafx.h"
#include "AttribEdit.h"
#include "EditControl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CEditControl dialog


CEditControl::CEditControl(CWnd* pParent /*=NULL*/)
	: CDialog(CEditControl::IDD, pParent)
{
	//{{AFX_DATA_INIT(CEditControl)
	m_controlName = _T("");
	//}}AFX_DATA_INIT
}


void CEditControl::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEditControl)
	DDX_Text(pDX, IDC_NAME_EDIT, m_controlName);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CEditControl, CDialog)
	//{{AFX_MSG_MAP(CEditControl)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEditControl message handlers

BOOL CEditControl::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	RECT wndRect;

	// add extra dialog
	GetDlgItem(IDC_DLG)->GetWindowRect(&wndRect);
	ScreenToClient(&wndRect);
	switch(m_type)
	{
	case dmat::AttributeControlDesc::SPINNER:
		m_spinnerDialog.Create(IDD_SPINNER, this);
		m_spinnerDialog.MoveWindow(&wndRect);
		break;
	case dmat::AttributeControlDesc::COMBOBOX:
		m_comboboxDialog.Create(IDD_COMBOBOX, this);
		m_comboboxDialog.MoveWindow(&wndRect);
		break;
	case dmat::AttributeControlDesc::FILEBROWSER:
		m_fileBrowserDialog.Create(IDD_FILEBROWSE, this);
		m_fileBrowserDialog.MoveWindow(&wndRect);
		break;
	default:
		return FALSE;
	}
	
	GetDlgItem(IDC_NAME_EDIT)->EnableWindow(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CEditControl::OnOK() 
{
	switch(m_type)
	{
	case dmat::AttributeControlDesc::SPINNER:
		m_spinnerDialog.UpdateData();
		break;
	case dmat::AttributeControlDesc::COMBOBOX:
		m_comboboxDialog.UpdateData();
		break;
	case dmat::AttributeControlDesc::FILEBROWSER:
		m_fileBrowserDialog.UpdateData();
		break;
	}

	
	CDialog::OnOK();
}
