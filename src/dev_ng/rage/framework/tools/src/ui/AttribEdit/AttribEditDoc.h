// AttribEditDoc.h : interface of the CAttribEditDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_ATTRIBEDITDOC_H__A53575FF_2ECF_11D3_98A5_00902709243B__INCLUDED_)
#define AFX_ATTRIBEDITDOC_H__A53575FF_2ECF_11D3_98A5_00902709243B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "AttribMgrEx.h"
#include "Operation.h"


class CLeftView;
class CAttribEditView;


class CAttribEditDoc : public CDocument
{
protected: // create from serialization only
	CAttribEditDoc();
	DECLARE_DYNCREATE(CAttribEditDoc)

// Attributes
public:

	COperation m_oper;
	AttributeManagerEx m_mgr;
	CLeftView *m_pLeftView;
	CAttribEditView *m_pRightView;

// Operations
	void SetLeftView(CLeftView *pLeftView);
	void SetRightView(CAttribEditView *pRightView);

	
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAttribEditDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	virtual BOOL OnSaveDocument(LPCTSTR lpszPathName);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CAttribEditDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CAttribEditDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ATTRIBEDITDOC_H__A53575FF_2ECF_11D3_98A5_00902709243B__INCLUDED_)
