#if !defined(AFX_GROUPBOXDIALOG_H__EF1A9F8B_344A_11D3_8E9B_00902709243B__INCLUDED_)
#define AFX_GROUPBOXDIALOG_H__EF1A9F8B_344A_11D3_8E9B_00902709243B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GroupBoxDialog.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CGroupBoxDialog dialog

class CGroupBoxDialog : public CDialog
{
// Construction
public:
	CGroupBoxDialog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CGroupBoxDialog)
	enum { IDD = IDD_GROUPBOX };
	CString	m_name;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGroupBoxDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CGroupBoxDialog)
	virtual void OnOK();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GROUPBOXDIALOG_H__EF1A9F8B_344A_11D3_8E9B_00902709243B__INCLUDED_)
