#if !defined(AFX_NEWMEMBERDIALOG_H__C832EF89_32BF_11D3_8E9A_00902709243B__INCLUDED_)
#define AFX_NEWMEMBERDIALOG_H__C832EF89_32BF_11D3_8E9A_00902709243B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// NewMemberDialog.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CNewMemberDialog dialog

class CNewMemberDialog : public CDialog
{
// Construction
public:
	CNewMemberDialog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CNewMemberDialog)
	enum { IDD = IDD_NEWMEMBER_DIALOG };
	CString	m_default;
	CString	m_name;
	int		m_true;
	int		m_type;
	//}}AFX_DATA

	void UpdateUI();
	void SetEditingDefaultValue() {m_editDefault = true;}

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNewMemberDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	bool m_editDefault;

	// Generated message map functions
	//{{AFX_MSG(CNewMemberDialog)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeTypeCombo();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NEWMEMBERDIALOG_H__C832EF89_32BF_11D3_8E9A_00902709243B__INCLUDED_)
