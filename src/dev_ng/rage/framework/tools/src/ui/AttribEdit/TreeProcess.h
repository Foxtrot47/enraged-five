//
//
//    Filename: TreeProcess.h
//     Creator: Adam Fowler
//     $Author: Adam $
//       $Date: 9/07/99 15:54 $
//   $Revision: 1 $
// Description: Functions to process the tree control
//
//
#ifndef INC_TREE_PROCESS_H_
#define INC_TREE_PROCESS_H_

#include "AttribEditDoc.h"
#include "LeftView.h"
// STD headers
#include <string>



//
//        name: ProcessTreeCtrl
// description: Template function for processing the tree control
//          in: tree = reference to tree control
//				pDoc = pointer to document
//				f = templated function object
//         out: 
//
template<class Func>
void ProcessTree(CTreeCtrl& tree, CAttribEditDoc* pDoc, Func& f, HTREEITEM hItem = TVI_ROOT)
{
	HTREEITEM hChildItem;
	HTREEITEM hNext;

	hChildItem = tree.GetChildItem(hItem);

	while(hChildItem)
	{
		hNext = tree.GetNextSiblingItem(hChildItem);
		if(tree.ItemHasChildren(hChildItem))
			ProcessTree(tree, pDoc, f, hChildItem);
		f(tree, pDoc, hChildItem);
		hChildItem = hNext;
	} 
}

//
//   Class Name: TreeRenameClass
//  Description: Rename tree entries and updates their parameter
//
class TreeRenameClass
{
public:
	TreeRenameClass(int param, const std::string& newName) : m_param(param), m_newName(newName) {}
	void operator() (CTreeCtrl& tree, CAttribEditDoc* pDoc, HTREEITEM hItem)
	{
		if(tree.GetItemData(hItem) == m_param)
		{
			tree.SetItemText(hItem, m_newName.c_str());
			tree.SetItemData(hItem, (int)(pDoc->m_mgr.GetClass(m_newName)));
		}

	}
protected:
	int m_param;
	std::string m_newName;
};

//
//   Class Name: TreeRenameDialog
//  Description: Rename tree entries and updates their parameter
//
class TreeRenameDialog
{
public:
	TreeRenameDialog(int param, const std::string& newName) : m_param(param), m_newName(newName) {}
	void operator() (CTreeCtrl& tree, CAttribEditDoc* pDoc, HTREEITEM hItem)
	{
		if(tree.GetItemData(hItem) == m_param)
		{
			tree.SetItemText(hItem, m_newName.c_str());
			tree.SetItemData(hItem, (int)(pDoc->m_mgr.GetDialogDesc(m_newName)));
		}

	}
protected:
	int m_param;
	std::string m_newName;
};

//
//   Class Name: TreeAddChildClass
//  Description: Add children to tree
//    Functions: 
//
//
class TreeAddChildClass
{
public:
	TreeAddChildClass(unsigned int param, AttributeClassEx *pClass) : m_param(param), m_pClass(pClass) {}
	void operator() (CTreeCtrl& tree, CAttribEditDoc* pDoc, HTREEITEM hItem)
	{
		if(tree.GetItemData(hItem) == m_param)
		{
			pDoc->m_pLeftView->AddClass(tree, m_pClass, hItem);
		}

	}
protected:
	unsigned int m_param;
	AttributeClassEx *m_pClass;
};

//
//   Class Name: TreeAddChildDialog
//  Description: Add children to tree
//    Functions: 
//
//
class TreeAddChildDialog
{
public:
	TreeAddChildDialog(unsigned int param, AttributeDialogDescEx *pDialog) : m_param(param), m_pDialog(pDialog) {}
	void operator() (CTreeCtrl& tree, CAttribEditDoc* pDoc, HTREEITEM hItem)
	{
		if(tree.GetItemData(hItem) == m_param)
		{
			pDoc->m_pLeftView->AddDialog(tree, m_pDialog, hItem);
		}
	}
protected:
	unsigned int m_param;
	AttributeDialogDescEx *m_pDialog;
};

//
//   Class Name: TreeDeleteObject
//  Description: Delete all incidences of an object
//
class TreeDeleteObject
{
public:
	TreeDeleteObject(unsigned int param) : m_param(param) {}
	void operator() (CTreeCtrl& tree, CAttribEditDoc* pDoc, HTREEITEM hItem)
	{
		// if correct class
		if(tree.GetItemData(hItem) == m_param)
		{
			tree.DeleteItem(hItem);
		}
	}
protected:
	unsigned int m_param;
};

//
//   Class Name: TreeDeleteChildObject
//  Description: Delete children of a class with a defined parameter
//
class TreeDeleteChildObject
{
public:
	TreeDeleteChildObject(unsigned int param, ObjectEx *pObject) : m_param(param), m_pObject(pObject) {}
	void operator() (CTreeCtrl& tree, CAttribEditDoc* pDoc, HTREEITEM hItem)
	{
		// if correct class
		if(tree.GetItemData(hItem) == m_param)
		{
			HTREEITEM hChildItem = tree.GetChildItem(hItem);
			HTREEITEM hNextItem;

			while(hChildItem)
			{
				hNextItem = tree.GetNextSiblingItem(hChildItem);
				// if correct child class then delete
				if(tree.GetItemData(hChildItem) == (unsigned int)m_pObject)
					tree.DeleteItem(hChildItem);
				hChildItem = hNextItem;
			} 
		}

	}
protected:
	unsigned int m_param;
	ObjectEx *m_pObject;
};


#endif // INC_TREE_PROCESS_H_