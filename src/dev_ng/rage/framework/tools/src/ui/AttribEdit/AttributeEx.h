//
//
//    Filename: AttributeEx.h
//     Creator: Adam Fowler
//     $Author: Adam $
//       $Date: 9/07/99 15:54 $
//   $Revision: 1 $
// Description: Extended version of AttributeClass which stores information about what class an
//				AttributeClass includes
//
//
#ifndef INC_ATTRIBUTE_EX_H_
#define INC_ATTRIBUTE_EX_H_

// my headers
#include "Attribute.h"
#include "AttribDialog.h"

// STD headers
#include <algorithm>

class ObjectEx
{
	friend class AttributeManagerEx;

public:
	ObjectEx(const std::string& name) : m_name(name) {}

	virtual class AttributeClassEx* AsClass() {return NULL;}
	virtual class AttributeDialogDescEx* AsDialog() {return NULL;}

	static void SetManager(class AttributeManagerEx *pManager) {m_pManager = pManager;}
	static class AttributeManagerEx *GetManager() {return m_pManager;}

	std::vector<std::string>& GetChildrenVector() {return m_children;}
	const std::vector<std::string>& GetChildrenVector() const {return m_children;}

	virtual void AddChild(ObjectEx *pChild) {m_children.push_back(pChild->GetName());}
	const std::string& GetName() const {return m_name;}
	void RenameChildren(const std::string& name, const std::string& newName);
	virtual void DeleteChild(const std::string& name);
	bool IsDependent(ObjectEx *pObj) const
		{return (std::find(m_children.begin(), m_children.end(), pObj->GetName()) != m_children.end());}

protected:
	static class AttributeManagerEx *m_pManager;

	std::string m_name;
	std::vector<std::string> m_children;
};


//
//   Class Name: AttributeClassEx
// Base Classes: 
//  Description: 
//    Functions: 
//
//
class AttributeClassEx : public ObjectEx
{
public:
	AttributeClassEx(const std::string& name) : ObjectEx(name) {}

	virtual AttributeClassEx* AsClass() {return this;}
	dmat::AttributeClass* GetAttributeClass();

	bool RenameAttribute(const std::string& name, int id);
};


//
//   Class Name: AttributeDialogDescEx
// Base Classes: 
//  Description: 
//    Functions: 
//
//
class AttributeDialogDescEx : public ObjectEx
{
public:
	AttributeDialogDescEx(const std::string& name) : ObjectEx(name) {}

	virtual AttributeDialogDescEx* AsDialog() {return this;}

	dmat::AttributeDialogDesc* GetAttributeDialog();
	//virtual void AddChild(ObjectEx *pChild);
	virtual void DeleteChild(const std::string& name);

	void SetClassEdited(const std::string& className) {m_classEdited = className;}
	const std::string& GetClassEdited() {return m_classEdited;}

	const std::string& GetDialogNameFromPtr(const dmat::AttributeDialogDesc *pDialog) const;
	const std::string& GetDialogNameFromPtr(const dmat::AttributeControlDescList *pList) const;

protected:
	std::string m_classEdited;
};


#endif //INC_ATTRIBUTE_EX_H_