#if !defined(AFX_ADDCONTROLDIALOG_H__EF1A9F8A_344A_11D3_8E9B_00902709243B__INCLUDED_)
#define AFX_ADDCONTROLDIALOG_H__EF1A9F8A_344A_11D3_8E9B_00902709243B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AddControlDialog.h : header file
//
#pragma warning (disable : 4786)

#include "ComboBoxDialog.h"
#include "GroupBoxDialog.h"
#include "SpinnerDialog.h"
#include "FileBrowserDialog.h"
#include <AttribDialog.h>
#include "AttributeEx.h"

// STD headers
#include <list>

/////////////////////////////////////////////////////////////////////////////
// CAddControlDialog dialog

// ListCtrlEntry: structure storing entry information for the list 
typedef struct 
{
	std::string m_name;
	int m_image;
	int m_data;
} AddControlEntry;


class CAddControlDialog : public CDialog
{
// Construction
public:
	CAddControlDialog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CAddControlDialog)
	enum { IDD = IDD_ADDCONTROL };
	CListCtrl	m_list;
	//}}AFX_DATA

	CGroupBoxDialog m_groupboxDialog;
	CComboBoxDialog m_comboboxDialog;
	CSpinnerDialog m_spinnerDialog;
	CFileBrowserDialog m_fileBrowserDialog;
	dmat::AttributeControlDesc::ControlType m_type;
	dmat::AttributeClass *m_pClass;
	AttributeDialogDescEx *m_pDialogEx;

	void AddItem(const std::string item, int data, int image = 0);
	int GetNumItems() {return m_entries.size();}
	int GetSelectedItemData(void);

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAddControlDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	int m_selectedData;
	std::list<AddControlEntry> m_entries;
	CImageList m_imageList;

	// Generated message map functions
	//{{AFX_MSG(CAddControlDialog)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ADDCONTROLDIALOG_H__EF1A9F8A_344A_11D3_8E9B_00902709243B__INCLUDED_)
