#ifndef __FORCEINCLUDE_H__
#define __FORCEINCLUDE_H__

namespace rage
{

	typedef unsigned char u8;
	typedef unsigned short u16;
	typedef unsigned int u32;
	typedef unsigned __int64 u64;
	typedef struct __declspec(intrin_type) __declspec(align(16)) __u128 {
		float m128_f32[4];
	} __u128;
	typedef __u128 u128;

	typedef signed char s8;
	typedef signed short s16;
	typedef signed int s32;
	typedef signed __int64 s64;

	typedef float f32;
#if defined(_M_X64)
	typedef u64 uptr;
#else
	typedef u32 uptr;
#endif

} // rage namespace

#if 0
#if defined(_DEBUG) && defined(_X_64)
#include "forceinclude/win64_tooldebug.h"
#elif defined(_DEBUG)
#include "forceinclude/win32_tooldebug.h"
#elif defined(NDEBUG) && defined(_X_64)
#include "forceinclude/win64_toolrelease.h"
#elif defined(NDEBUG)
#include "forceinclude/win32_toolrelease.h"
#elif defined(_X_64)
#include "forceinclude/win64_toolbeta.h"
#else
#include "forceinclude/win32_toolbeta.h"
#endif
#endif

#endif // __FORCEINCLUDE_H__
