// NewDialog.cpp : implementation file
//

#include "stdafx.h"
#include "AttribEdit.h"
#include "NewDialog.h"

//#include <dma.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CNewDialog dialog


CNewDialog::CNewDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CNewDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CNewDialog)
	m_classNameIndex = -1;
	m_dialogName = _T("");
	m_enterName = -1;
	//}}AFX_DATA_INIT
}


void CNewDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CNewDialog)
	DDX_Control(pDX, IDC_CLASS_COMBO, m_classCombo);
	DDX_CBIndex(pDX, IDC_CLASS_COMBO, m_classNameIndex);
	DDX_Text(pDX, IDC_DIALOGNAME_EDIT, m_dialogName);
	DDX_Radio(pDX, IDC_ENTERNAME_RADIO, m_enterName);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CNewDialog, CDialog)
	//{{AFX_MSG_MAP(CNewDialog)
	ON_BN_CLICKED(IDC_ENTERNAME_RADIO, OnEnterNameRadio)
	ON_BN_CLICKED(IDC_CLASSNAME_RADIO, OnClassNameRadio)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNewDialog message handlers

BOOL CNewDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	for(int i=0; i<m_classList.size(); i++)
		m_classCombo.AddString(m_classList[i].c_str());
	m_classCombo.SetCurSel(0);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CNewDialog::OnEnterNameRadio() 
{
	::EnableWindow(*GetDlgItem(IDC_DIALOGNAME_EDIT), TRUE);
}

void CNewDialog::OnClassNameRadio() 
{
	::EnableWindow(*GetDlgItem(IDC_DIALOGNAME_EDIT), FALSE);
}
