//
//
//    Filename: AttribMgrEx.cpp
//     Creator: Adam Fowler
//     $Author: Adam $
//       $Date: 9/07/99 15:54 $
//   $Revision: 1 $
// Description: Extended version of Attribute Manager. Stores extended information about attributes
//
//

#include "AttribMgrEx.h"
#include "AttribScriptEx.h"
#include "AttribExProcess.h"

//
//        name: AttributeManager::AddClassEx
// description: Adds a new class description to manager.
//          in: aClassEx = reference to extended class description
//
void AttributeManagerEx::AddClassEx(AttributeClassEx &aClassEx)
{
	if(!m_classMap.insert(std::make_pair(aClassEx.GetName(), aClassEx)).second)
		throw dmat::ClassNameClashError(aClassEx.GetName());
}

//
//        name: AttributeManager::AddClass
// description: Adds a new class description to manager.
//          in: aClass = reference to class description
//				aClassEx = reference to extended class description
//
void AttributeManagerEx::AddClass(AttributeClassEx &aClassEx, dmat::AttributeClass &aClass)
{
	m_mgr.AddClass(aClassEx.GetName(), aClass);
	if(!m_classMap.insert(std::make_pair(aClassEx.GetName(), aClassEx)).second)
		throw dmat::ClassNameClashError(aClassEx.GetName());
}

//
//        name: AttributeManagerEx::RemoveClass
// description: Remove class from manager
//          in: name = name of class
//
void AttributeManagerEx::RemoveClass(const std::string &name)
{
	m_classMap.erase(name);
	m_mgr.RemoveClass(name);
}

//
//        name: AttributeManagerEx::RenameClass
// description: Rename a class in the manager
//          in: name = name of class
//				newName = new name of class
//
bool AttributeManagerEx::RenameClass(const std::string& name, const std::string& newName)
{
	AttributeClassEx *pClassEx;
	dmat::AttributeClass *pClass;

	// if both names are the same return true
	if(name == newName)
		return true;

	// check if a class already exists with this name
	pClassEx = GetClass(newName);
	if(pClassEx)
	{
		return false;
	}

	// get copies of class and extended class
	pClassEx = GetClass(name);
	assert(pClassEx);
	pClass = pClassEx->GetAttributeClass();
	assert(pClass);

	AttributeClassEx aClassEx = *pClassEx;
	dmat::AttributeClass aClass = *pClass;

	// remove class with old name
	RemoveClass(name);

	// add in class with new name
	aClassEx.m_name = newName;
	AddClass(aClassEx, aClass);

	//
	// anything using the old name has to now use the new name
	// 

	return true;
}

//
//        name: &AttributeManager::GetClass
// description: Return class descriptor from a name
//          in: className = class name string
//         out: 
//
AttributeClassEx *AttributeManagerEx::GetClass(const std::string &className) 
{
	AttributeClassExMapIterator iClass = m_classMap.find(className);
	if(iClass == m_classMap.end())
		return NULL;
	return &((*iClass).second);
}

//
//        name: AttributeManager::AddDialogDescEx
// description: Make another dialog descriptor available to the manager and associate it with a name
//          in: dialogEx = reference to extended dialog to add
//
void AttributeManagerEx::AddDialogDescEx(AttributeDialogDescEx &dialogEx)
{
	if(!m_dialogMap.insert(std::make_pair(dialogEx.GetName(), dialogEx)).second)
		throw dmat::ClassNameClashError(dialogEx.GetName());
}

//
//        name: AttributeManager::AddDialogDescEx
// description: Make another dialog descriptor available to the manager and associate it with a name
//          in: dialogEx = reference to extended dialog to add
//				dialog = reference to dialog to add
//
void AttributeManagerEx::AddDialogDesc(AttributeDialogDescEx &dialogEx, dmat::AttributeDialogDesc &dialog)
{
	m_mgr.AddDialogDesc(dialogEx.GetName(), dialog);
	if(!m_dialogMap.insert(std::make_pair(dialogEx.GetName(), dialogEx)).second)
		throw dmat::ClassNameClashError(dialogEx.GetName());
}

//
//        name: AttributeManagerEx::RemoveDialogDesc
// description: Remove dialog from manager
//          in: name = name of class
//
void AttributeManagerEx::RemoveDialogDesc(const std::string &name)
{
	m_dialogMap.erase(name);
	m_mgr.RemoveDialogDesc(name);
}

//
//        name: AttributeManagerEx::RenameDialogDesc
// description: Rename a dialog in the manager
//          in: name = name of class
//				newName = new name of dialog
//
bool AttributeManagerEx::RenameDialogDesc(const std::string &name, const std::string &newName)
{
	AttributeDialogDescEx *pDialogEx;
	dmat::AttributeDialogDesc *pDialog;

	// if both names are the same return true
	if(name == newName)
		return true;

	// check if a class already exists with this name
	pDialogEx = GetDialogDesc(newName);
	if(pDialogEx)
	{
		return false;
	}

	// get copies of class and extended class
	pDialogEx = GetDialogDesc(name);
	assert(pDialogEx);
	pDialog = pDialogEx->GetAttributeDialog();
	assert(pDialog);

	AttributeDialogDescEx dialogEx = *pDialogEx;
	dmat::AttributeDialogDesc dialog = *pDialog;

	// remove class with old name
	RemoveDialogDesc(name);

	// add in class with new name
	dialogEx.m_name = newName;
	AddDialogDesc(dialogEx, dialog);

	ProcessAllDialogs(RedirectDialogPtrsClass(pDialog, dialogEx.GetAttributeDialog()));

	return true;

}

//
//        name: &AttributeManager::GetDialogDesc
// description: Return dialog descriptor from a name
//          in: dialogName = dialog name string
//         out: pointer to dialog descriptor
//
AttributeDialogDescEx *AttributeManagerEx::GetDialogDesc(const std::string &dialogName) 
{
	AttributeDialogExMapIterator iDialog = m_dialogMap.find(dialogName);
	if(iDialog == m_dialogMap.end())
		return NULL;
	return &((*iDialog).second);
}

//
//        name: AttributeManager::clear
// description: Delete all classes and dialog descriptions
//
void AttributeManagerEx::Clear()
{
	m_mgr.Clear();
	m_classMap.clear();
	m_dialogMap.clear();
	m_nextId = 0;
}


//
//        name: AttributeManagerEx::ReadScriptFile
// description: Read a script file
//          in: pFilename = pointer to file name
//
void AttributeManagerEx::ReadScriptFile(const char *pFilename)
{
	m_mgr.ReadScriptFile(pFilename);

	AttributeScriptFileEx file(pFilename);
	file.Read(*this);

	//remove replicate elements from classes that include other classes
	AttributeClassExMapIterator iClassEx;
	dmat::AttributeClassMapConstIterator iClass;
	int id;
	int i;
	
	for(iClassEx = m_classMap.begin(); iClassEx != m_classMap.end(); iClassEx++)
	{
		ProcessAllDependents(&(*iClassEx).second, RemoveAttributeReplicationClass((*iClassEx).second));
	}

	// Find out the highest ID used
	for(iClass = m_mgr.GetClassMap()->begin(); iClass != m_mgr.GetClassMap()->end(); iClass++)
	{
		// get the highest id
		for(i=0; i<(*iClass).second.GetSize(); i++)
		{
			id = (*iClass).second.GetId(i);
			if(id > m_nextId)
				m_nextId = id;
		}
	}
}


//
//        name: AttributeManagerEx::WriteScriptFile
// description: Write a script file from manager
//          in: pFilename = filename
//
void AttributeManagerEx::WriteScriptFile(const char *pFilename)
{
	AttributeScriptFileOutEx file(pFilename);

	file.Write(*this);
}

