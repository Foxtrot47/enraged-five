//
//
//    Filename: AttributeEx.cpp
//     Creator: Adam Fowler
//     $Author: Adam $
//       $Date: 9/07/99 15:54 $
//   $Revision: 1 $
// Description: 
//
//
#include "AttributeEx.h"
#include "AttribMgrEx.h"


AttributeManagerEx *ObjectEx::m_pManager = NULL;


// --- ObjectEx ----------------------------------------------------------------------------

//
//        name: AttributeClassEx::RenameClass
// description: Rename classes
//
void ObjectEx::RenameChildren(const std::string &name, const std::string &newName)
{
	for(int i=0; i<m_children.size(); i++)
	{
		if(m_children[i] == name)
			m_children[i] = newName;
	}
}

//
//        name: ObjectEx::DeleteChild
// description: name = name of child to delete
//          in: name = name of child
//
void ObjectEx::DeleteChild(const std::string &name)
{
	std::vector<std::string>::iterator iChild;
	iChild = std::find(m_children.begin(), m_children.end(), name);
	if(iChild != m_children.end())
		m_children.erase(iChild);
}

// --- AttributeClassEx ----------------------------------------------------------------------------

dmat::AttributeClass* AttributeClassEx::GetAttributeClass()
{
	return m_pManager->GetManager().GetClass(m_name);
}

//
//        name: AttributeClassEx::RenameAttribute
// description: Rename an attribute
//          in: name = new name of attribute
//				id = id of attribute
//         out: successful
//
bool AttributeClassEx::RenameAttribute(const std::string& name, int id)
{
	dmat::AttributeClass *pClass = GetAttributeClass();
	assert(pClass);

	dmat::Attribute& attr = (*pClass)[id];
	int i;

	// if attribute already has that name return true
	if(name == attr.GetName())
		return true;

	// if another attribute has that name return false
	for(i = 0; i<pClass->GetSize(); i++)
	{
		if(name == (*pClass)[pClass->GetId(i)].GetName())
			return false;
	}

	attr = dmat::Attribute(name, attr.GetDefault(), attr.GetType());
	return true;
}

// --- AttributeDialogDescEx ----------------------------------------------------------------------------

dmat::AttributeDialogDesc* AttributeDialogDescEx::GetAttributeDialog()
{
	return m_pManager->GetManager().GetDialogDesc(m_name);
}

//
//        name: AttributeDialogDescEx::AddChild
// description: Add a child dialog
//          in: pChild = pointer to child dialog
//
/*void AttributeDialogDescEx::AddChild(ObjectEx *pChild)
{
	ObjectEx::AddChild(pChild);

	dmat::GroupBoxControlDesc groupBox;
	dmat::AttributeDialogDesc *pDialogDesc = m_pManager->GetManager().GetDialogDesc(GetName());
	dmat::AttributeDialogDesc *pChildDialogDesc = m_pManager->GetManager().GetDialogDesc(pChild->GetName());

	groupBox.SetAttributeDialogDesc(pChildDialogDesc);
	groupBox.SetName(pChild->GetName());

	pDialogDesc->AddControl(&groupBox);
}*/

//
//        name: AttributeDialogDescEx::DeleteChild
// description: name = name of child to delete
//          in: name = name of child
//
void AttributeDialogDescEx::DeleteChild(const std::string &name)
{
	ObjectEx::DeleteChild(name);

	// remove groupbox pointing to child
	dmat::AttributeDialogDesc* pDialog = GetAttributeDialog();
	dmat::AttributeDialogDesc* pToDelete = m_pManager->GetManager().GetDialogDesc(name);
	int i;

	for(i = 0; i < pDialog->GetSize() ; i++)
	{
		// delete control if it contains a control list the same as the control list of the 
		// child dialog being removed
		if(pDialog->GetControl(i)->GetControlList() == &(pToDelete->GetList()))
		{
			pDialog->DeleteControl(i);
			i--;
		}
	}
}


//
//        name: AttributeDialogDescEx::GetDialogNameFromPtr
// description: Useful functions to related group boxes to the dialogs they use
//          in: 
//         out: 
//
const std::string& AttributeDialogDescEx::GetDialogNameFromPtr(const dmat::AttributeDialogDesc *pDialog) const
{
	const dmat::AttributeDialogMap* pDialogMap = m_pManager->GetManager().GetDialogMap();
	dmat::AttributeDialogMapConstIterator iDialog;

	for(iDialog = pDialogMap->begin(); iDialog != pDialogMap->end(); iDialog++)
	{
		if(&(*iDialog).second == pDialog)
			return (*iDialog).first;
	}
	// should never get here
	assert(0);
}
const std::string& AttributeDialogDescEx::GetDialogNameFromPtr(const dmat::AttributeControlDescList *pList) const
{
	const dmat::AttributeDialogMap* pDialogMap = m_pManager->GetManager().GetDialogMap();
	dmat::AttributeDialogMapConstIterator iDialog;

	for(iDialog = pDialogMap->begin(); iDialog != pDialogMap->end(); iDialog++)
	{
		if(&(*iDialog).second.GetList() == pList)
			return (*iDialog).first;
	}
	// should never get here
	assert(0);
}
