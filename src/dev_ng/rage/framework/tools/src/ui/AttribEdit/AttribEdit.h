// AttribEdit.h : main header file for the ATTRIBEDIT application
//

#if !defined(AFX_ATTRIBEDIT_H__A53575F9_2ECF_11D3_98A5_00902709243B__INCLUDED_)
#define AFX_ATTRIBEDIT_H__A53575F9_2ECF_11D3_98A5_00902709243B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CAttribEditApp:
// See AttribEdit.cpp for the implementation of this class
//

class CAttribEditApp : public CWinApp
{
public:
	CAttribEditApp();

	BOOL SetRegKey(LPCTSTR lpszKey, LPCTSTR lpszValue, LPCTSTR lpszValueName = NULL);
	void RegisterFileTypeIcons();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAttribEditApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CAttribEditApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

extern CAttribEditApp theApp;

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ATTRIBEDIT_H__A53575F9_2ECF_11D3_98A5_00902709243B__INCLUDED_)
