// LeftView.h : interface of the CLeftView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_LEFTVIEW_H__A5357603_2ECF_11D3_98A5_00902709243B__INCLUDED_)
#define AFX_LEFTVIEW_H__A5357603_2ECF_11D3_98A5_00902709243B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//#include <dma.h>
// STD header
#pragma warning (disable : 4786)
#include <string>

class CAttribEditDoc;
class AttributeDialogDescEx;
class AttributeClassEx;
class ObjectEx;

class CLeftView : public CTreeView
{
protected: // create from serialization only
	CLeftView();
	DECLARE_DYNCREATE(CLeftView)

public:
	CAttribEditDoc* GetDocument();

	CImageList m_imageList;
	HTREEITEM m_hClassItem;
	HTREEITEM m_hDialogItem;

	// functions
	void ConstructTreeCtrl();
	void DeleteTree();
	bool DeleteTreeItem(HTREEITEM hItem);
	HTREEITEM AddClass(CTreeCtrl &tree, const AttributeClassEx *pClass, HTREEITEM hParent);
	HTREEITEM AddDialog(CTreeCtrl &tree, const AttributeDialogDescEx *pClass, HTREEITEM hParent);
	void AddClassChildren(CTreeCtrl &tree, const AttributeClassEx *pClass, HTREEITEM hParent);
	void AddDialogChildren(CTreeCtrl &tree, const AttributeDialogDescEx *pDesc, HTREEITEM hParent);
	//bool AddNewClass(const std::string& className);
	//bool AddNewDialog(const std::string& dialogName);

	AttributeClassEx *GetSelectedClass();
	AttributeDialogDescEx *GetSelectedDialog();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLeftView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual void OnInitialUpdate(); // called first time after construct
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CLeftView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	bool m_deletingTree;

// Generated message map functions
protected:
	//{{AFX_MSG(CLeftView)
	afx_msg void OnSelchanged(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnEndlabeledit(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnKeydown(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in LeftView.cpp
inline CAttribEditDoc* CLeftView::GetDocument()
   { return (CAttribEditDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LEFTVIEW_H__A5357603_2ECF_11D3_98A5_00902709243B__INCLUDED_)
