// Operation.cpp: implementation of the COperation class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "AttribEdit.h"
#include "AttribEditDoc.h"
#include "AttribEditView.h"
#include "TreeProcess.h"
#include "Operation.h"
#include "Classname.h"
#include "NewDialog.h"
#include "ListCtrlDialog.h"
#include "NewMemberDialog.h"
#include "AddControlDialog.h"
#include "EditControl.h"

#include "AttribMgrEx.h"
#include "AttribExProcess.h"

// STD headers
#include <strstream>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

COperation::~COperation()
{
	
}


//
//        name: COperation::DeleteObject
// description: Delete an object from the manager and tree
//          in: pObjectEx = pointer to object to delete
//         out: successful
//
bool COperation::DeleteObject(ObjectEx *pObjectEx)
{
	std::ostrstream msg;
	
	// construct message
	if(pObjectEx->AsClass())
		msg << "Are you sure you want to delete class \"" << pObjectEx->GetName() 
		<< "\"?" << std::ends;
	else
		msg << "Are you sure you want to delete dialog description \"" << pObjectEx->GetName()
		<< "\"?" << std::ends;
	
	if(m_pTree->MessageBox(msg.str(), 
		"Confirm delete", 
		MB_ICONQUESTION|MB_YESNO) == IDNO)
		return false;
	
	std::string name = pObjectEx->GetName();
	ProcessTree(*m_pTree, m_pDoc, TreeDeleteObject((unsigned int)pObjectEx));
	if(pObjectEx->AsClass())
	{
		ProcessAllDependents(pObjectEx->AsClass(), RemoveChildClass(pObjectEx));
		DeleteDialogsEditingClass(pObjectEx->AsClass());

		m_deletedClasses.insert(std::make_pair(name, *(pObjectEx->AsClass()->GetAttributeClass())));
		m_mgr.RemoveClass(name);
	}
	else
	{
		ProcessAllDependents(pObjectEx->AsDialog(), RemoveChildClass(pObjectEx));
		m_mgr.RemoveDialogDesc(name);
	}

	m_pDoc->SetModifiedFlag();

	return true;
}

//
//        name: DeleteChildObject
// description: Delete a child object from another object
//          in: pParentObj = pointer to parent object
//				pObjectEx = pointer to child object
//         out: successful
//
bool COperation::DeleteChildObjectPrompt(ObjectEx *pParentObj, ObjectEx *pObjectEx)
{
	std::ostrstream msg;
	
	// construct message
	if(pParentObj->AsClass())
		msg << "Are you sure you want to delete \"" << pObjectEx->GetName() << "\" from class \"" 
		<< pParentObj->GetName() << "\"?" << std::ends;
	else 
		msg << "Are you sure you want to delete \"" << pObjectEx->GetName() << "\" from dialog \"" 
		<< pParentObj->GetName() << "\"?" << std::ends;
	
	if(m_pTree->MessageBox(msg.str(), 
		"Confirm delete", 
		MB_ICONQUESTION|MB_YESNO) == IDNO)
		return false;

	DeleteChildObject(pParentObj, pObjectEx);

	return true;
}

//
//        name: DeleteChildObject
// description: Delete a child object from another object
//          in: pParentObj = pointer to parent object
//				pObjectEx = pointer to child object
//         out: successful
//
void COperation::DeleteChildObject(ObjectEx *pParentObj, ObjectEx *pObjectEx)
{
	std::ostrstream msg;
	
	pParentObj->DeleteChild(pObjectEx->GetName());
	ProcessTree(*m_pTree, m_pDoc, TreeDeleteChildObject((unsigned int)pParentObj, pObjectEx));
	if(pParentObj->AsClass())
		DeleteChildDialogsEditingClass(pParentObj->AsClass(), pObjectEx->AsClass());
	m_pDoc->SetModifiedFlag();
}

//
//        name: COperation::DeleteDialogsEditingClass
// description: Delete all the dialogs that edit the given class
//          in: pClassEx = pointer to class
//
void COperation::DeleteDialogsEditingClass(AttributeClassEx *pClassEx)
{
	std::set<AttributeDialogDescEx *> dialogSet;
	std::set<AttributeDialogDescEx *>::iterator iDialog;

	ProcessAllDialogs(AddSetDialogsEditingClass(pClassEx->GetName(), dialogSet));

	for(iDialog = dialogSet.begin(); iDialog != dialogSet.end(); iDialog++)
	{
		ProcessAllDependents(*iDialog, RemoveChildClass(*iDialog));
		ProcessTree(*m_pTree, m_pDoc, TreeDeleteObject((unsigned int)*iDialog));
		std::string name = (*iDialog)->GetName();
		m_mgr.RemoveDialogDesc(name);
	}
}

//
//        name: COperation::DeleteChildDialogsEditingClass
// description: Delete all the dialogs that edit the given class
//          in: pClassEx = pointer to class
//
void COperation::DeleteChildDialogsEditingClass(AttributeClassEx *pClassEx, AttributeClassEx *pChildClassEx)
{
	std::set<AttributeDialogDescEx *> dialogSet;
	std::set<AttributeDialogDescEx *>::iterator iDialog;

	ProcessAllDialogs(AddSetDialogsEditingClass(pClassEx->GetName(), dialogSet));

	for(iDialog = dialogSet.begin(); iDialog != dialogSet.end(); iDialog++)
	{
		std::set<AttributeDialogDescEx *> childSet;
		std::set<AttributeDialogDescEx *>::iterator iChild;
		std::vector<std::string> &children = (*iDialog)->GetChildrenVector();
		AttributeDialogDescEx *pChild;

		for(int i=0; i<children.size(); i++)
		{
			pChild = m_mgr.GetDialogDesc(children[i]);
			EqualsClass equals(pChild->GetClassEdited());


			// find if class child dialog edits is class begin deleted or a child of the class being 
			// deleted, if so add to set to be deleted from dialog
			ProcessAllChildren(pChildClassEx, equals);
			if(pChild->GetClassEdited() == pChildClassEx->GetName() || equals.m_is)
				childSet.insert(pChild);
		}

		// remove children from dialog
		for(iChild = childSet.begin(); iChild != childSet.end(); iChild++)
		{
			(*iDialog)->DeleteChild((*iChild)->GetName());
			ProcessTree(*m_pTree, m_pDoc, TreeDeleteChildObject((unsigned int)(*iDialog), (*iChild)));
		}
	}
}

//
//        name: COperation::DeleteControlsEditingAttribute
// description: Delete all the dialogs that edit the given attribute in a class
//          in: pClassEx = pointer to class
//				id = id of attribute
//
void COperation::DeleteControlsEditingAttribute(AttributeClassEx *pClassEx, int id)
{
	std::set<AttributeDialogDescEx *> dialogSet;
	std::set<AttributeDialogDescEx *>::iterator iDialog;
	int i;

	// get set of dialog editing class
	ProcessAllDialogs(AddSetDialogsEditingClass(pClassEx->GetName(), dialogSet));

	// for each of these dialogs, check if any of its controls are editing an attribute with 
	// the given id. If so delete it
	for(iDialog = dialogSet.begin(); iDialog != dialogSet.end(); iDialog++)
	{
		dmat::AttributeDialogDesc *pDialog = (*iDialog)->GetAttributeDialog();
		for(i=0; i<pDialog->GetSize(); i++)
		{
			dmat::AttributeControlDesc *pControl = pDialog->GetControl(i);
			dmat::AttributeValueControlDesc *pValueControl = pControl->AsValueControl();

			if(pValueControl && pValueControl->GetAttributeId() == id)
			{
				pDialog->DeleteControl(i);
				i--;
			}
		}
	}
}

//
//        name: COperation::DeleteElement
// description: Delete element from a class or dialog
//          in: pObjectEx = pointer to class or dialog
//				id = id of element
//
void COperation::DeleteElement(ObjectEx *pObjectEx, int id)
{
	if(pObjectEx->AsClass())
	{
		m_deletedAttributes.insert(std::make_pair(pObjectEx->GetName(), std::make_pair(id, (*pObjectEx->AsClass()->GetAttributeClass())[id])));

		pObjectEx->AsClass()->GetAttributeClass()->DeleteItem(id);
		DeleteControlsEditingAttribute(pObjectEx->AsClass(), id);
	}
	else if(pObjectEx->AsDialog())
	{
		dmat::AttributeDialogDesc *pDialog = pObjectEx->AsDialog()->GetAttributeDialog();
		dmat::AttributeControlDesc *pControl = pDialog->GetControl(id);
		const dmat::AttributeControlDescList *pControlList = pControl->GetControlList();
		
		// delete control
		pDialog->DeleteControl(id);
		// if control contains another dialog then delete the child dialog
		if(pControlList)
		{
			const dmat::AttributeDialogDesc *pChildDialog = pControl->AsListControl()->GetDialogDesc();
			std::string name = pObjectEx->AsDialog()->GetDialogNameFromPtr(pChildDialog);
			
			pObjectEx->DeleteChild(name);
			ProcessTree(*m_pTree, m_pDoc, 
				TreeDeleteChildObject((unsigned int)pObjectEx, m_mgr.GetClass(name)));
		}
	}
	
	m_pDoc->SetModifiedFlag();
}

//
//        name: COperation::RenameObject
// description: Rename an object
//          in: pObjectEx = pointer to object to rename
//				pNewName = pointer to new name
//         out: successful
//
bool COperation::RenameObject(ObjectEx *pObjectEx, const char *pNewName)
{
	if(pObjectEx->AsClass())
	{
		std::string name = pObjectEx->GetName();
		
		if(m_pDoc->m_mgr.RenameClass(name, pNewName) == false)
			return false;
		
		// update tree items
		ProcessTree(*m_pTree, m_pDoc, TreeRenameClass((int)pObjectEx, pNewName));
		ProcessAllClasses(RenameObjectClass(name, pNewName));
		ProcessAllDialogs(RenameEditedClassClass(name, pNewName));
	}
	else 
	{
		std::string name = pObjectEx->GetName();
		
		if(m_pDoc->m_mgr.RenameDialogDesc(name, pNewName) == false)
			return false;
		
		// update tree items
		ProcessTree(*m_pTree, m_pDoc, TreeRenameDialog((int)pObjectEx, pNewName));
		ProcessAllDialogs(RenameObjectClass(name, pNewName));
	}
	m_pDoc->SetModifiedFlag();
	return true;
}


//
//        name: COperation::AddNewClass/AddNewDialog
// description: Add a new class/dialog 
//          in: className = name of class
//         out: 
//
void COperation::AddNewClassPrompt()
{
	CClassname dialog;
	
	if(dialog.DoModal() == IDOK)
	{
		AddNewClass((const char *)dialog.m_name);
	}
}
void COperation::AddNewDialogPrompt()
{
	CNewDialog dialog;
	AttributeClassExMap& classMap = ObjectEx::GetManager()->GetClassMap();
	AttributeClassExMapIterator iClass;

	// add class names to dialog
	for(iClass = classMap.begin(); iClass != classMap.end(); iClass++)
		dialog.m_classList.push_back((*iClass).first);

	dialog.m_dialogName = "Dialog";
	dialog.m_classNameIndex = 0;
	dialog.m_enterName = 0;

	if(dialog.DoModal() == IDOK)
	{
		std::string className(dialog.m_classList[dialog.m_classNameIndex]);
		std::string dialogName;

		if(dialog.m_enterName == 0)
			dialogName = (const char *)dialog.m_dialogName;
		else
			dialogName = className;

		AddNewDialog(dialogName, className);
	}
}

//
//        name: COperation::AddNewAutomaticDialogPrompt
// description: Similar to AddNewDialog(), except it adds in controls for all the members of the class
//
void COperation::AddNewAutomaticDialogPrompt()
{
	CNewDialog dialog;
	AttributeClassExMap& classMap = ObjectEx::GetManager()->GetClassMap();
	AttributeClassExMapIterator iClass;

	// add class names to dialog
	for(iClass = classMap.begin(); iClass != classMap.end(); iClass++)
		dialog.m_classList.push_back((*iClass).first);

	dialog.m_dialogName = "Dialog";
	dialog.m_classNameIndex = 0;
	dialog.m_enterName = 0;

	if(dialog.DoModal() == IDOK)
	{
		std::string dialogName;
		std::string className = dialog.m_classList[dialog.m_classNameIndex].c_str();

		if(dialog.m_enterName == 0)
			dialogName = (const char *)dialog.m_dialogName;
		else
			dialogName = className;

		if(AddNewDialog(dialogName, className))
		{
			AttributeClassEx *pClassEx = m_mgr.GetClass(className);
			AttributeDialogDescEx *pDialogEx = m_mgr.GetDialogDesc(dialogName);
			assert(pDialogEx);
			assert(pClassEx);
			
			dmat::AttributeClass* pClass = pClassEx->GetAttributeClass();
			dmat::AttributeDialogDesc* pDialog = pDialogEx->GetAttributeDialog();
			dmat::IntSpinnerControlDesc intSpin;
			dmat::FloatSpinnerControlDesc floatSpin;
			dmat::CheckBoxControlDesc checkBox;
			dmat::EditBoxControlDesc editBox;
			int i;

			// add group boxes for all included children if they have dialogs
			for(i=0; i<pClassEx->GetChildrenVector().size(); i++)
			{
				std::string name = pClassEx->GetChildrenVector()[i];
				std::set<AttributeDialogDescEx *> dialogSet;
				ProcessAllDialogs(AddSetDialogsEditingClass(name, dialogSet));
				if(!dialogSet.empty())
				{
					dmat::GroupBoxControlDesc groupBox;
					groupBox.SetName(name);
					groupBox.SetAttributeDialogDesc((*dialogSet.begin())->GetAttributeDialog());
					pDialog->AddControl(&groupBox);
				}

			}

			// add controls for all the class members
			for(i=0; i<pClass->GetSize(); i++)
			{
				dmat::Attribute& attr = (*pClass)[pClass->GetId(i)];
				switch(attr.GetType())
				{
				case dmat::Attribute::INT:
					intSpin.SetAttributeId(pClass->GetId(i));
					intSpin.SetLimits(0, 100);
					pDialog->AddControl(&intSpin);
					break;
				case dmat::Attribute::FLOAT:
					floatSpin.SetAttributeId(pClass->GetId(i));
					floatSpin.SetLimits(0.0f, 100.0f);
					pDialog->AddControl(&floatSpin);
					break;
				case dmat::Attribute::BOOL:
					checkBox.SetAttributeId(pClass->GetId(i));
					pDialog->AddControl(&checkBox);
					break;
				case dmat::Attribute::STRING:
					editBox.SetAttributeId(pClass->GetId(i));
					pDialog->AddControl(&editBox);
					break;

				}
			}

			m_pDoc->m_pRightView->RefreshObject();
		}
	}
}

//
//        name: COperation::AddNewClass/AddNewDialog
// description: Add a new class/dialog 
//          in: className = name of class
//         out: 
//
bool COperation::AddNewClass(const std::string& className)
{
	AttributeClassEx aClassEx(className);
	dmat::AttributeClass aClass;
	AttributeClassEx *pAClass;
	HTREEITEM hItem;

	if(m_mgr.GetClass(className))
	{
		std::ostrstream msg;
		// construct message
		msg << "The Class \"" << className << "\" already exists." << std::ends;

		m_pTree->MessageBox(msg.str(), 
			"Creation failed", 
			MB_ICONERROR|MB_OK);
		return false;
	}

	CopyDeletedClass(aClass, className);

	m_mgr.AddClass(aClassEx, aClass);
	// get pointer to copy of class in manager
	pAClass = m_mgr.GetClass(className);

	hItem = m_pDoc->m_pLeftView->AddClass(*m_pTree, pAClass, NULL);
	m_pTree->SelectItem(hItem);

	m_pDoc->SetModifiedFlag();
	return true;
}

//
//        name: COperation::CopyDeletedClass
// description: If a class with the same name exists in the deleted class list then ask if user wants to resurrect it
//
void COperation::CopyDeletedClass(dmat::AttributeClass& aClass, const std::string& className)
{
	NameClassMap::iterator iNameClass = m_deletedClasses.find(className);

	if(iNameClass == m_deletedClasses.end())
		return;

	if(m_pTree->MessageBox("A class with the same name was deleted earlier. Do you want it back?", "Create Class", MB_YESNO|MB_ICONQUESTION) == IDYES)
	{
		aClass = (*iNameClass).second;
		m_deletedClasses.erase(iNameClass);
	}
}

bool COperation::AddNewDialog(const std::string& dialogName, const std::string& className)
{
	AttributeDialogDescEx dialogEx(dialogName);
	dmat::AttributeDialogDesc dialog;
	AttributeDialogDescEx *pDialog;
	HTREEITEM hItem;

	dialogEx.SetClassEdited(className);

	if(m_mgr.GetDialogDesc(dialogName))
	{
		std::ostrstream msg;
		// construct message
		msg << "The Dialog \"" << dialogName << "\" already exists." << std::ends;

		m_pTree->MessageBox(msg.str(), 
			"Creation failed", 
			MB_ICONERROR|MB_OK);
		return false;
	}
	m_mgr.AddDialogDesc(dialogEx, dialog);
	// get pointer to copy of class in manager
	pDialog = m_mgr.GetDialogDesc(dialogName);

	hItem = m_pDoc->m_pLeftView->AddDialog(*m_pTree, pDialog, NULL);
	m_pTree->SelectItem(hItem);

	m_pDoc->SetModifiedFlag();
	return true;
}


//
//        name: COperation::IncludeClass
// description: Include another class in the current class
//
void COperation::IncludeClassPrompt(AttributeClassEx *pClassEx)
{
	CListCtrlDialog dialog;
	CImageList imageList;
	AttributeClassEx *pChildClassEx;
	int data;

	imageList.Create(16, 16, ILC_COLOR8, 4, 10);
	imageList.SetBkColor(RGB(255,255,255));
	imageList.Add(theApp.LoadIcon(IDI_CLASS));
	
	dialog.SetImageList(&imageList);

	// create a set of all the available classes and then remove all the classes that are children of
	// the selected class, all the classes that depend on the selected class and the selected class
	std::set<ObjectEx *> objectSet;
	std::set<ObjectEx *>::iterator iObj;

	ProcessAllClasses(AddToSetClass(objectSet));
	ProcessAllChildren(pClassEx, RemoveRelatedClassesClass(objectSet));
	ProcessAllDependents(pClassEx, RemoveRelatedClassesClass(objectSet));
	objectSet.erase(pClassEx);

	if(objectSet.empty())
	{
		std::ostrstream msg;
		// construct message
		msg << "There are no classes available to include in \"" << pClassEx->GetName() << "\"" << std::ends;

		m_pTree->MessageBox(msg.str(), 
			"Creation failed", 
			MB_ICONERROR|MB_OK);
		return;
	}

	// add items left in set to dialog list
	for(iObj = objectSet.begin(); iObj != objectSet.end(); iObj++)
		dialog.AddItem((*iObj)->GetName(), (int)(*iObj));

	dialog.SetTitle("Include Class");
	if(dialog.DoModal() != IDOK)
		return;

	// get the selected item data
	data = dialog.GetSelectedItemData();
	if(data == -1 || data == 0)
		return;
	pChildClassEx = (AttributeClassEx *)data;

	IncludeClass(pClassEx, pChildClassEx);

	m_pDoc->m_pRightView->RefreshObject();

}


//
//        name: COperation::IncludeClass
// description: Include a given class in another class
//          in: pClassEx = parent class
//				pChildClassEx = class to include
//
void COperation::IncludeClass(AttributeClassEx *pClassEx, AttributeClassEx *pChildClassEx)
{
	pClassEx->AddChild(pChildClassEx);
	ProcessTree(*m_pTree, m_pDoc, TreeAddChildClass((unsigned int)pClassEx, pChildClassEx));
	m_pDoc->SetModifiedFlag();
}

//
//        name: COperation::AddNewMemberPrompt
// description: Add a new member to a class
//          in: pClassEx = pointer to class to add member to
//
void COperation::AddNewMemberPrompt(AttributeClassEx *pClassEx)
{
	CNewMemberDialog dialog;
	int id;
	int i;

	if(pClassEx == NULL)
		return;

	dialog.m_name = "Name";
	dialog.m_type = 0;
	dialog.m_default = "0";
	dialog.m_true = 0;

	if(dialog.DoModal() != IDOK)
		return;

	dmat::AttributeClass *pAClass = pClassEx->GetAttributeClass();
	for(i=0; i<pAClass->GetSize(); i++)
	{
		if((*pAClass)[pAClass->GetId(i)].GetName() == (const char *)dialog.m_name)
		{
			std::ostrstream msg;
			// construct message
			msg << "A member of class \"" << pClassEx->GetName() << "\" already uses name \"" << (const char *)dialog.m_name << "\"." << std::ends;
			m_pTree->MessageBox(msg.str(), 
				"Creation failed", 
				MB_ICONERROR|MB_OK);
			return;
		}
	}

	switch(dialog.m_type)
	{
	case dmat::Attribute::INT:
		{
			dmat::Attribute attr((const char *)dialog.m_name, (int)atoi(dialog.m_default));
			id = GetAttributeId(pClassEx, attr);
			pAClass->AddAttribute(id, attr);
			break;
		}
	case dmat::Attribute::FLOAT:
		{
			dmat::Attribute attr((const char *)dialog.m_name, (float)atof(dialog.m_default));
			id = GetAttributeId(pClassEx, attr);
			pAClass->AddAttribute(id, attr);
			break;
		}
	case dmat::Attribute::BOOL:
		{
			bool value = false;
			if(dialog.m_true == 0)
				value = true;
			dmat::Attribute attr((const char *)dialog.m_name, value);
			id = GetAttributeId(pClassEx, attr);
			pAClass->AddAttribute(id, attr);
			break;
		}
	case dmat::Attribute::STRING:
		{
			dmat::Attribute attr((const char *)dialog.m_name, dialog.m_default);
			id = GetAttributeId(pClassEx, attr);
			pAClass->AddAttribute(id, attr);
			break;
		}
	}

	m_pDoc->m_pRightView->RefreshObject();
	m_pDoc->SetModifiedFlag();
}

//
//        name: COperation::GetAttributeId
// description: Get the Id for attribute. If there is a deleted attribute that is the same as this attribute then 
//				ask if the user wants to use that one
//          in: attr = reference to attribute
//         out: id of attribute
//
int COperation::GetAttributeId(const AttributeClassEx *pClassEx, const dmat::Attribute& attr)
{
	ClassAttributeMMap::iterator iClassAttr = m_deletedAttributes.find(pClassEx->GetName());
	ClassAttributeMMap::iterator iBound = m_deletedAttributes.upper_bound(pClassEx->GetName());

	if(iClassAttr == m_deletedAttributes.end())
		return ObjectEx::GetManager()->GetNextId();

	while(iClassAttr != iBound)
	{
		if((*iClassAttr).second.second.GetName() == attr.GetName() &&
			(*iClassAttr).second.second.GetType() == attr.GetType())
		{
			if(m_pList->MessageBox("An attribute with the same name and type was\ndeleted earlier. Do you want to use it's id?", "Create Member", MB_YESNO|MB_ICONQUESTION) == IDYES)
			{
				int id = (*iClassAttr).second.first;
				m_deletedAttributes.erase(iClassAttr);
				return id;
			}
		}
		iClassAttr++;
	}
	return ObjectEx::GetManager()->GetNextId();
}

//
//        name: COperation::IncludeDialog
// description: Include a given dialog in another dialog
//          in: pDialogEx = parent class
//				pChildDialogEx = class to include
//
void COperation::IncludeDialog(AttributeDialogDescEx *pDialogEx, AttributeDialogDescEx *pChildDialogEx)
{
	pDialogEx->AddChild(pChildDialogEx);
	ProcessTree(*m_pTree, m_pDoc, TreeAddChildDialog((unsigned int)pDialogEx, pChildDialogEx));
	m_pDoc->SetModifiedFlag();
}

//
//        name: COperation::AddCorrectMembers
// description: Add correct members of edited class to list. Get all members of class that are of
//				a certain type
//          in: 
//
void COperation::AddCorrectMembers(CAddControlDialog& dialog, AttributeDialogDescEx *pDialogEx,
								   dmat::Attribute::Type type)
{
	dmat::AttributeClass *pClass = m_mgr.GetManager().GetClass(pDialogEx->GetClassEdited());
	int id;
	for(int i=0; i<pClass->GetSize(); i++)
	{
		id = pClass->GetId(i);
		if((*pClass)[id].GetType() == type)
		{
			dialog.AddItem((*pClass)[id].GetName(), id);
		}
	}
}

//
//        name: COperation::RunControlDialog
// description: Check dialog is valid and display dialog
//          in: dialog = reference to dialog
//         out: success
//
bool COperation::RunControlDialog(CAddControlDialog& dialog, const char *pControlName)
{
	if(dialog.GetNumItems() == 0)
	{
		std::strstream msg;
		msg << "Cannot construct a " << pControlName << " for the class being edited." << std::ends;
		m_pTree->MessageBox(msg.str(), "Error", MB_OK|MB_ICONERROR);
		return false;
	}

	return dialog.DoModal() == IDOK;
}

//
//        name: COperation::AddNewGroupboxPrompt etc
// description: Add control to dialog
//          in: pDialogEx = opinter to dialog
//
void COperation::AddNewGroupboxPrompt(AttributeDialogDescEx *pDialogEx)
{
	CAddControlDialog dialog;

	dialog.m_type = dmat::AttributeControlDesc::GROUPBOX;
	dialog.m_groupboxDialog.m_name = "GroupBox";

	// create a set of all the available dialogs and then remove all the dialogs that are children of
	// the selected dialog, all the dialogs that depend on the selected dialog and the selected dialog
	AttributeClassEx *pClassEx = m_mgr.GetClass(pDialogEx->GetClassEdited());
	std::set<ObjectEx *> objectSet;
	std::set<ObjectEx *> relatedSet;
	std::set<ObjectEx *>::iterator iObj;

	ProcessAllDialogs(AddToSetClass(objectSet));
	ProcessAllChildren(pDialogEx, RemoveRelatedDialogsClass(objectSet));
	ProcessAllDependents(pDialogEx, RemoveRelatedDialogsClass(objectSet));
	objectSet.erase(pDialogEx);

	// get set of dialogs which we can use (ie dialogs that edit classes that are related)
	for(iObj = objectSet.begin(); iObj != objectSet.end(); iObj++)
	{
		std::string editedClass = (*iObj)->AsDialog()->GetClassEdited();
		if(editedClass == pDialogEx->GetClassEdited())
		{
			relatedSet.insert(*iObj);
			continue;
		}
		EqualsClass equals(editedClass);
		ProcessAllChildren(pClassEx, equals);
		if(equals.m_is)
			relatedSet.insert(*iObj);
	}

	// add items left in set to dialog list
	for(iObj = relatedSet.begin(); iObj != relatedSet.end(); iObj++)
		dialog.AddItem((*iObj)->GetName(), (int)(*iObj), 1);

	if(RunControlDialog(dialog, "GroupBox"))
	{
		AttributeDialogDescEx *pChildDialogEx;
		dmat::GroupBoxControlDesc groupbox;
		dmat::AttributeDialogDesc *pDialog, *pChildDialog;

		// get the selected item data
		unsigned int data = dialog.GetSelectedItemData();
		if(data == -1 || data == 0)
			return;
		pChildDialogEx = (AttributeDialogDescEx *)data;
		
		// Add dialog as a child and add a group box control to contain child dialog
		IncludeDialog(pDialogEx, pChildDialogEx);

		pDialog = m_mgr.GetManager().GetDialogDesc(pDialogEx->GetName());
		pChildDialog = m_mgr.GetManager().GetDialogDesc(pChildDialogEx->GetName());
		groupbox.SetAttributeDialogDesc(pChildDialog);
		groupbox.SetName((const char *)dialog.m_groupboxDialog.m_name);
		pDialog->AddControl(&groupbox);

		m_pDoc->m_pRightView->RefreshObject();
		m_pDoc->SetModifiedFlag();
	}

}
void COperation::AddNewEditboxPrompt(AttributeDialogDescEx *pDialogEx)
{
	CAddControlDialog dialog;

	dialog.m_type = dmat::AttributeControlDesc::EDITBOX;
	AddCorrectMembers(dialog, pDialogEx, dmat::Attribute::STRING);

	if(RunControlDialog(dialog, "EditBox"))
	{
		dmat::EditBoxControlDesc editbox;
		dmat::AttributeDialogDesc *pDialog = m_mgr.GetManager().GetDialogDesc(pDialogEx->GetName());

		editbox.SetAttributeId(dialog.GetSelectedItemData());
		pDialog->AddControl(&editbox);
		m_pDoc->m_pRightView->RefreshObject();
		m_pDoc->SetModifiedFlag();
	}
}
void COperation::AddNewComboboxPrompt(AttributeDialogDescEx *pDialogEx)
{
	CAddControlDialog dialog;

	dialog.m_type = dmat::AttributeControlDesc::COMBOBOX;
	dialog.m_comboboxDialog.m_entries = "Entry1\015\nEntry2";
	AddCorrectMembers(dialog, pDialogEx, dmat::Attribute::INT);
	AddCorrectMembers(dialog, pDialogEx, dmat::Attribute::STRING);

	if(RunControlDialog(dialog, "ComboBox"))
	{
		dmat::AttributeClass *p_aclass = m_mgr.GetManager().GetClass(pDialogEx->GetClassEdited());
		
		if((*p_aclass)[dialog.GetSelectedItemData()].GetType() == dmat::Attribute::INT)
		{
			dmat::ComboBoxControlDesc combobox;
			dmat::AttributeDialogDesc *pDialog = m_mgr.GetManager().GetDialogDesc(pDialogEx->GetName());

			combobox.SetAttributeId(dialog.GetSelectedItemData());
			dialog.m_comboboxDialog.m_entries.Replace("\015\n",",");
			combobox.SetEntriesString((const char *)dialog.m_comboboxDialog.m_entries);
			pDialog->AddControl(&combobox);
		}
		else
		{
			dmat::StringComboBoxControlDesc combobox;
			dmat::AttributeDialogDesc *pDialog = m_mgr.GetManager().GetDialogDesc(pDialogEx->GetName());

			combobox.SetAttributeId(dialog.GetSelectedItemData());
			dialog.m_comboboxDialog.m_entries.Replace("\015\n",",");
			combobox.SetEntriesString((const char *)dialog.m_comboboxDialog.m_entries);
			pDialog->AddControl(&combobox);
		}

		m_pDoc->m_pRightView->RefreshObject();
		m_pDoc->SetModifiedFlag();
	}
}
void COperation::AddNewCheckboxPrompt(AttributeDialogDescEx *pDialogEx)
{
	CAddControlDialog dialog;

	dialog.m_type = dmat::AttributeControlDesc::CHECKBOX;
	AddCorrectMembers(dialog, pDialogEx, dmat::Attribute::BOOL);

	if(RunControlDialog(dialog, "CheckBox"))
	{
		dmat::CheckBoxControlDesc checkbox;
		dmat::AttributeDialogDesc *pDialog = m_mgr.GetManager().GetDialogDesc(pDialogEx->GetName());

		checkbox.SetAttributeId(dialog.GetSelectedItemData());
		pDialog->AddControl(&checkbox);
		m_pDoc->m_pRightView->RefreshObject();
	}
	m_pDoc->SetModifiedFlag();
}
void COperation::AddNewSpinnerPrompt(AttributeDialogDescEx *pDialogEx)
{
	CAddControlDialog dialog;

	dialog.m_type = dmat::AttributeControlDesc::SPINNER;
	dialog.m_spinnerDialog.m_min = 0;
	dialog.m_spinnerDialog.m_max = 1;
	AddCorrectMembers(dialog, pDialogEx, dmat::Attribute::INT);
	AddCorrectMembers(dialog, pDialogEx, dmat::Attribute::FLOAT);

	if(RunControlDialog(dialog, "Spinner"))
	{
		dmat::IntSpinnerControlDesc intspin;
		dmat::FloatSpinnerControlDesc floatspin;
		dmat::AttributeDialogDesc *pDialog = m_mgr.GetManager().GetDialogDesc(pDialogEx->GetName());
		dmat::AttributeClass *pClass = m_mgr.GetManager().GetClass(pDialogEx->GetClassEdited());

		// Add either an spinner editing an integer or a floating point value
		if((*pClass)[dialog.GetSelectedItemData()].GetType() == dmat::Attribute::INT)
		{
			intspin.SetAttributeId(dialog.GetSelectedItemData());
			intspin.SetLimits((int)dialog.m_spinnerDialog.m_min, (int)dialog.m_spinnerDialog.m_max);
			pDialog->AddControl(&intspin);
		}
		else
		{
			floatspin.SetAttributeId(dialog.GetSelectedItemData());
			floatspin.SetLimits(dialog.m_spinnerDialog.m_min, dialog.m_spinnerDialog.m_max);
			pDialog->AddControl(&floatspin);
		}
		m_pDoc->m_pRightView->RefreshObject();
		m_pDoc->SetModifiedFlag();
	}
}
void COperation::AddNewFileBrowserPrompt(AttributeDialogDescEx *pDialogEx)
{
	CAddControlDialog dialog;

	dialog.m_type = dmat::AttributeControlDesc::FILEBROWSER;
	dialog.m_fileBrowserDialog.m_extension = "txt";
	dialog.m_fileBrowserDialog.m_description = "Text files";
	AddCorrectMembers(dialog, pDialogEx, dmat::Attribute::STRING);

	if(RunControlDialog(dialog, "File Browser"))
	{
		dmat::FileBrowserControlDesc fileBrowser;
		dmat::AttributeDialogDesc *pDialog = m_mgr.GetManager().GetDialogDesc(pDialogEx->GetName());

		fileBrowser.SetAttributeId(dialog.GetSelectedItemData());
		fileBrowser.SetExtension(dialog.m_fileBrowserDialog.m_extension);
		fileBrowser.SetFileDescription((const char *)dialog.m_fileBrowserDialog.m_description);
		pDialog->AddControl(&fileBrowser);
		m_pDoc->m_pRightView->RefreshObject();
		m_pDoc->SetModifiedFlag();
	}
}


//
//        name: COperation::SwapControls
// description: Swap two controls about
//          in: index1,index2 = the indices of the two controls to swap
//
void COperation::SwapControls(AttributeDialogDescEx *pDialogEx, int index1, int index2)
{
	dmat::AttributeDialogDesc *pDialog = pDialogEx->GetAttributeDialog();
	dmat::AttributeControlDesc *pControl = pDialog->GetControl(index1);

	pDialog->GetList()[index1] = pDialog->GetList()[index2];
	pDialog->GetList()[index2] = pControl;
	m_pDoc->m_pRightView->RefreshObject();
	m_pDoc->SetModifiedFlag();
}


//
//        name: COperation::SwapControls
// description: Swap two controls about
//          in: index1,index2 = the indices of the two controls to swap
//
void COperation::SwapAttributes(AttributeClassEx *pClassEx, int id1, int id2)
{
	dmat::AttributeClass *pClass = pClassEx->GetAttributeClass();

	pClass->SwapItem(pClass->GetIndex(id1), pClass->GetIndex(id2));
	m_pDoc->m_pRightView->RefreshObject();

	m_pDoc->SetModifiedFlag();
}

//
//        name: DialogProc
// description: Dialog procedure for testing dialog
//          in: 
//         out: 
//
BOOL CALLBACK DialogProc( HWND hwndDlg, unsigned int msg, WPARAM wParam, LPARAM lParam )
{
	static dmat::AttributeDialog *pDialog;
	switch(msg)
	{
	case WM_INITDIALOG:
		{
			pDialog = (dmat::AttributeDialog *)lParam;
			pDialog->InitDialog(hwndDlg);
		}
		break;
	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDOK:
			pDialog->SyncWithControls();
			EndDialog(hwndDlg, 1);
			return TRUE;
		case IDCANCEL:
			EndDialog(hwndDlg, 0);
			return TRUE;
		}
		break;
	}
	return FALSE;
}
 
//
//        name: COperation::TestDialog
// description: Test the dialog by displaying it
//          in: pDialogEx = pointer to dialog
//
void COperation::TestDialog(AttributeDialogDescEx *pDialogEx)
{
	// build attribute class
	AttributeClassEx *pClassEx = m_mgr.GetClass(pDialogEx->GetClassEdited());
	dmat::AttributeClass *pClass = pClassEx->GetAttributeClass();
	dmat::AttributeClass finalClass = *pClass;

	ProcessAllChildren(pClassEx, BuildAttributeClass(finalClass));

	// create instance and dialog
	dmat::AttributeDialogDesc *pDialog = pDialogEx->GetAttributeDialog();
	dmat::AttributeInst inst(finalClass);
	dmat::AttributeDialog dialog;

	dialog.ConstructDialog(AfxGetInstanceHandle(), &inst, pDialog);

	DialogBoxIndirectParam(AfxGetInstanceHandle(), dialog.GetTemplate(), 
				*AfxGetMainWnd(), &DialogProc, (LPARAM)&dialog);
}



//
//        name: COperation::EditDefaultValue
// description: Bring up dialog to edit default value of member
//          in: pClassEx = pointer to class
//				id = ID of member to edit
//
void COperation::EditDefaultValue(AttributeClassEx *pClassEx, int posn)
{
	int id = m_pList->GetItemData(posn);
	CNewMemberDialog dialog;

	if(pClassEx == NULL)
		return;

	dmat::AttributeClass *pClass = pClassEx->GetAttributeClass();
	dmat::Attribute& attr = (*pClass)[id];

	dialog.m_name = attr.GetName().c_str();
	dialog.m_type = attr.GetType();
	dialog.SetEditingDefaultValue();

	switch(attr.GetType())
	{
	case dmat::Attribute::INT:
		dialog.m_default.Format("%d", (int)attr.GetDefault());
		break;
	case dmat::Attribute::FLOAT:
		dialog.m_default.Format("%f", (float)attr.GetDefault());
		break;
	case dmat::Attribute::STRING:
		dialog.m_default = (const char *)attr.GetDefault();
		break;
	case dmat::Attribute::BOOL:
		if((bool)attr.GetDefault())
			dialog.m_true = 0;
		else
			dialog.m_true = 1;
		break;
	default:
		break;
	}

	if(dialog.DoModal() != IDOK)
		return;

	switch(dialog.m_type)
	{
	case dmat::Attribute::INT:
		attr = dmat::Attribute((const char *)dialog.m_name, (int)atoi(dialog.m_default));
		break;
	case dmat::Attribute::FLOAT:
		attr = dmat::Attribute((const char *)dialog.m_name, (float)atof(dialog.m_default));
		break;
	case dmat::Attribute::BOOL:
		if(dialog.m_true == 0)
			attr = dmat::Attribute((const char *)dialog.m_name, true);
		else
			attr = dmat::Attribute((const char *)dialog.m_name, false);
		break;
	case dmat::Attribute::STRING:
		attr = dmat::Attribute((const char *)dialog.m_name, dialog.m_default);
		break;
	}

	m_pDoc->m_pRightView->RefreshObject();

	// select item again
	m_pList->SetItemState(posn, LVIS_SELECTED, LVIS_SELECTED);
	m_pDoc->SetModifiedFlag();
}


//
//        name: COperation::EditControlProperties
// description: Edit the control's properties e.g min and max of a spinner
//          in: pDialogEx = pointer to dialog
//				posn = position of control in list
//
void COperation::EditControlProperties(AttributeDialogDescEx *pDialogEx, int posn)
{
	int id = m_pList->GetItemData(posn);
	CEditControl dialog;

	if(pDialogEx == NULL)
		return;

	dmat::AttributeDialogDesc *pDialog = pDialogEx->GetAttributeDialog();
	dmat::AttributeControlDesc *pControl = pDialog->GetControl(id);
	dmat::AttributeValueControlDesc *pValueControl = pControl->AsValueControl();
	AttributeClassEx *pClassEx = m_mgr.GetClass(pDialogEx->GetClassEdited());
	dmat::AttributeClass *pClass = pClassEx->GetAttributeClass();

	if(pValueControl == NULL)
		return;
	
	// set dialog entries
	dialog.m_type = pControl->GetType();
	dialog.m_controlName = (*pClass)[pValueControl->GetAttributeId()].GetName().c_str();
	
	switch(dialog.m_type)
	{
	case dmat::AttributeControlDesc::SPINNER:
		if(pValueControl->GetEditType() == dmat::Attribute::INT)
		{
			int min, max;
			((dmat::IntSpinnerControlDesc *)pControl)->GetLimits(min, max);
			dialog.m_spinnerDialog.m_min = min;
			dialog.m_spinnerDialog.m_max = max;
		}
		else
		{
			((dmat::FloatSpinnerControlDesc *)pControl)->GetLimits(dialog.m_spinnerDialog.m_min, 
				dialog.m_spinnerDialog.m_max);
		}
		break;
	case dmat::AttributeControlDesc::COMBOBOX:
		dialog.m_comboboxDialog.m_entries = ((dmat::ComboBoxControlDesc *)pControl)->GetEntriesString().c_str();
		dialog.m_comboboxDialog.m_entries.Replace(",","\015\n");
		break;
	case dmat::AttributeControlDesc::TCOMBOBOX:
		dialog.m_comboboxDialog.m_entries = ((dmat::StringComboBoxControlDesc *)pControl)->GetEntriesString().c_str();
		dialog.m_comboboxDialog.m_entries.Replace(",","\015\n");
		break;
	case dmat::AttributeControlDesc::FILEBROWSER:
		dialog.m_fileBrowserDialog.m_extension = ((dmat::FileBrowserControlDesc *)pControl)->GetExtension();
		dialog.m_fileBrowserDialog.m_description = ((dmat::FileBrowserControlDesc *)pControl)->GetFileDescription().c_str();
		break;
	default:
		return;
	}
	
	if(dialog.DoModal() != IDOK)
		return;

	// read dialog entries
	switch(dialog.m_type)
	{
	case dmat::AttributeControlDesc::SPINNER:
		if(pValueControl->GetEditType() == dmat::Attribute::INT)
		{
			((dmat::IntSpinnerControlDesc *)pControl)->SetLimits((int)dialog.m_spinnerDialog.m_min, 
																(int)dialog.m_spinnerDialog.m_max);
		}
		else
		{
			((dmat::FloatSpinnerControlDesc *)pControl)->SetLimits(dialog.m_spinnerDialog.m_min, 
																dialog.m_spinnerDialog.m_max);
		}
		break;
	case dmat::AttributeControlDesc::COMBOBOX:
		dialog.m_comboboxDialog.m_entries.Replace("\015\n",",");
		((dmat::StringComboBoxControlDesc *)pControl)->SetEntriesString((const char *)dialog.m_comboboxDialog.m_entries);
		break;
	case dmat::AttributeControlDesc::TCOMBOBOX:
		dialog.m_comboboxDialog.m_entries.Replace("\015\n",",");
		((dmat::ComboBoxControlDesc *)pControl)->SetEntriesString((const char *)dialog.m_comboboxDialog.m_entries);
		break;
	case dmat::AttributeControlDesc::FILEBROWSER:
		((dmat::FileBrowserControlDesc *)pControl)->SetExtension(dialog.m_fileBrowserDialog.m_extension);
		((dmat::FileBrowserControlDesc *)pControl)->SetFileDescription((const char *)dialog.m_fileBrowserDialog.m_description);
		break;
	default:
		return;
	}
	

	m_pDoc->m_pRightView->RefreshObject();

	// select item again
	m_pList->SetItemState(posn, LVIS_SELECTED, LVIS_SELECTED);
	m_pDoc->SetModifiedFlag();

}


//
//        name: COperation::Clear
// description: Clear container classes
//
void COperation::Clear()
{
	m_deletedAttributes.clear();
	m_deletedClasses.clear();
}
