#if !defined(AFX_NEWDIALOG_H__EF1A9F81_344A_11D3_8E9B_00902709243B__INCLUDED_)
#define AFX_NEWDIALOG_H__EF1A9F81_344A_11D3_8E9B_00902709243B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// NewDialog.h : header file
//
#pragma warning (disable : 4786)

// STD headers
#include <string>
#include <vector>

/////////////////////////////////////////////////////////////////////////////
// CNewDialog dialog

class CNewDialog : public CDialog
{
// Construction
public:
	CNewDialog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CNewDialog)
	enum { IDD = IDD_NEWDIALOG_DIALOG };
	CComboBox	m_classCombo;
	int		m_classNameIndex;
	CString	m_dialogName;
	int		m_enterName;
	//}}AFX_DATA

	std::vector<std::string> m_classList;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNewDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CNewDialog)
	virtual BOOL OnInitDialog();
	afx_msg void OnEnterNameRadio();
	afx_msg void OnClassNameRadio();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NEWDIALOG_H__EF1A9F81_344A_11D3_8E9B_00902709243B__INCLUDED_)
