// ComboBoxDialog.cpp : implementation file
//

#include "stdafx.h"
#include "AttribEdit.h"
#include "ComboBoxDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CComboBoxDialog dialog


CComboBoxDialog::CComboBoxDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CComboBoxDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CComboBoxDialog)
	m_entries = _T("");
	//}}AFX_DATA_INIT
}


void CComboBoxDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CComboBoxDialog)
	DDX_Text(pDX, IDC_NAME, m_entries);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CComboBoxDialog, CDialog)
	//{{AFX_MSG_MAP(CComboBoxDialog)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CComboBoxDialog message handlers

void CComboBoxDialog::OnOK()
{
}
void CComboBoxDialog::OnCancel()
{
}
