//
//
//    Filename: AttribExProcess.h
//     Creator: Adam Fowler
//     $Author: Adam $
//       $Date: 9/07/99 15:54 $
//   $Revision: 1 $
// Description: Bunch of function classes for processing attribute classes etc
//
//
#ifndef INC_ATTRIB_EX_PROCESS_H_
#define INC_ATTRIB_EX_PROCESS_H_

// my headers
#include "AttribMgrEx.h"

// STD headers
#include <set>

//
//        name: ProcessAllChildren
// description: Template functions to process all the class/all a class' children
//
template<class Func> 
void ProcessAllClasses(Func& f)
{
	AttributeClassExMap& classMap = ObjectEx::GetManager()->GetClassMap();
	AttributeClassExMapIterator iClass;

	for(iClass = classMap.begin(); iClass != classMap.end(); iClass++)
	{
		f(&((*iClass).second));
	}
}

template<class Func> 
void ProcessAllChildren(AttributeClassEx *pClass, Func& f)
{
	std::vector<std::string> &children = pClass->GetChildrenVector();
	AttributeClassEx *pChild;
	for(int i=0; i<children.size(); i++)
	{
		pChild = ObjectEx::GetManager()->GetClass(children[i]);
		ProcessAllChildren(pChild, f);
		f(pChild);
	}
}

template<class Func> 
void ProcessAllDependents(AttributeClassEx *pClass, Func& f)
{
	AttributeClassExMap& classMap = ObjectEx::GetManager()->GetClassMap();
	AttributeClassExMapIterator iClass;

	for(iClass = classMap.begin(); iClass != classMap.end(); iClass++)
	{
		AttributeClassEx& aClassEx = (*iClass).second;
		if(aClassEx.IsDependent(pClass))
		{
			ProcessAllDependents(&aClassEx, f);
			f(&aClassEx);
		}
	}
}

//
//        name: ProcessAllChildren
// description: Template functions to process all the dialogs/all a dialog's children
//
template<class Func> 
void ProcessAllDialogs(Func& f)
{
	AttributeDialogExMap& dialogMap = ObjectEx::GetManager()->GetDialogMap();
	AttributeDialogExMapIterator iDialog;

	for(iDialog = dialogMap.begin(); iDialog != dialogMap.end(); iDialog++)
	{
		f(&((*iDialog).second));
	}
}

template<class Func> 
void ProcessAllChildren(AttributeDialogDescEx *pDialog, Func& f)
{
	std::vector<std::string> &children = pDialog->GetChildrenVector();
	AttributeDialogDescEx *pChild;
	for(int i=0; i<children.size(); i++)
	{
		pChild = ObjectEx::GetManager()->GetDialogDesc(children[i]);
		ProcessAllChildren(pChild, f);
		f(pChild);
	}
}

template<class Func> 
void ProcessAllDependents(AttributeDialogDescEx *pDialog, Func& f)
{
	AttributeDialogExMap& dialogMap = ObjectEx::GetManager()->GetDialogMap();
	AttributeDialogExMapIterator iDialog;

	for(iDialog = dialogMap.begin(); iDialog != dialogMap.end(); iDialog++)
	{
		AttributeDialogDescEx& dialogEx = (*iDialog).second;
		if(dialogEx.IsDependent(pDialog))
		{
			ProcessAllDependents(&dialogEx, f);
			f(&dialogEx);
		}
	}
}

// --- Function objects to use in above template functions --------------------------------------------

//
//   Class Name: AddToSet
//  Description: Function class to add object to set
//
class AddToSetClass
{
public:
	std::set<ObjectEx *>& m_objectSet;
	AddToSetClass(std::set<ObjectEx *>& set) : m_objectSet(set) {}
	void operator()(ObjectEx *pObjEx)
	{
		m_objectSet.insert(pObjEx);
	}
};

//
//   Class Name: RemoveFromSet
//  Description: Function class to remove object pointer from a set
//
class RemoveFromSetClass
{
public:
	std::set<ObjectEx *>& m_objectSet;
	RemoveFromSetClass(std::set<ObjectEx *>& set) : m_objectSet(set) {}
	void operator()(ObjectEx *pObjEx)
	{
		m_objectSet.erase(pObjEx);
	}
};

//
//   Class Name: RemoveRelatedClasses
//  Description: Remove related classes from a set
//
class RemoveRelatedClassesClass
{
public:
	std::set<ObjectEx *>& m_objectSet;
	RemoveRelatedClassesClass(std::set<ObjectEx *>& set) : m_objectSet(set) {}
	void operator()(AttributeClassEx *pClassEx)
	{
		if(m_objectSet.find(pClassEx) != m_objectSet.end())
		{
			ProcessAllChildren(pClassEx, RemoveFromSetClass(m_objectSet));
			ProcessAllDependents(pClassEx, RemoveFromSetClass(m_objectSet));
			m_objectSet.erase(pClassEx);
		}
	}

};

//
//   Class Name: RemoveRelatedDialogs
//  Description: Remove related dialogs from a set
//
class RemoveRelatedDialogsClass
{
public:
	std::set<ObjectEx *>& m_objectSet;
	RemoveRelatedDialogsClass(std::set<ObjectEx *>& set) : m_objectSet(set) {}
	void operator()(AttributeDialogDescEx *pDialogEx)
	{
		if(m_objectSet.find(pDialogEx) != m_objectSet.end())
		{
			ProcessAllChildren(pDialogEx, RemoveFromSetClass(m_objectSet));
			ProcessAllDependents(pDialogEx, RemoveFromSetClass(m_objectSet));
			m_objectSet.erase(pDialogEx);
		}
	}

};

//
//   Class Name: RenameObject
//  Description: Function class to rename the children of an object
//
class RenameObjectClass
{
public:
	RenameObjectClass(const std::string& name, const std::string& newName) : m_name(name), m_newName(newName) {}
	void operator()(ObjectEx *pObjEx)
	{
		pObjEx->RenameChildren(m_name, m_newName);
	}
protected:
	std::string m_name;
	std::string m_newName;
};

//
//   Class Name: RemoveChild
//  Description: Remove child object from object
//
class RemoveChildClass
{
public:
	RemoveChildClass(ObjectEx *pObjectEx) : m_pObjectEx(pObjectEx) {}
	void operator()(ObjectEx *pObjectEx)
	{
		pObjectEx->DeleteChild(m_pObjectEx->GetName());
	}
protected:
	ObjectEx* m_pObjectEx;
};

//
//   Class Name: RemoveAttributeReplication
//  Description: Remove the attributes from one class
//
class RemoveAttributeReplicationClass
{
public:
	RemoveAttributeReplicationClass(AttributeClassEx &aClassEx) : m_pClass(aClassEx.GetAttributeClass()) {}
	void operator()(AttributeClassEx *pClassEx)
	{
		dmat::AttributeClass* pClass = pClassEx->GetAttributeClass();
		int i;
		if(pClass == NULL || m_pClass == NULL)
			return;
		for(i=0; i<m_pClass->GetSize(); i++)
			pClass->DeleteItem(m_pClass->GetId(i));
	}
protected:
	dmat::AttributeClass* m_pClass;
};


//
//   Class Name: RedirectDialogPtrs
//  Description: Function class used to redirect old dialog pointers in groupbox controls to 
//				new dialog pointers
//
class RedirectDialogPtrsClass
{
public:
	RedirectDialogPtrsClass(dmat::AttributeDialogDesc *pOld, dmat::AttributeDialogDesc *pNew) : m_pOld(pOld), \
		m_pNew(pNew) {}
	void operator()(AttributeDialogDescEx *pDialogEx)
	{
		dmat::AttributeDialogDesc *pDialog = pDialogEx->GetAttributeDialog();
		dmat::AttributeControlDescList& controlList = pDialog->GetList();
		dmat::AttributeControlDescListIterator iControl;
		dmat::AttribListControlDesc *pControl;

		// if control uses old dialog description then use new dialog
		for(iControl = controlList.begin(); iControl != controlList.end(); iControl++)
		{
			if((*iControl)->GetControlList())
			{
				pControl = (dmat::AttribListControlDesc *)(*iControl);
				if(pControl->GetDialogDesc() == m_pOld)
					pControl->SetAttributeDialogDesc(m_pNew);
			}
		}
	}
protected:
	dmat::AttributeDialogDesc *m_pOld;
	dmat::AttributeDialogDesc *m_pNew;
};

//
//   Class Name: AddSetDialogsEditing
//  Description: Add all the dialogs editing a class to a set
//
class AddSetDialogsEditingClass
{
public:
	AddSetDialogsEditingClass(const std::string name, std::set<AttributeDialogDescEx *>& dialogSet) \
		: m_name(name), m_dialogSet(dialogSet) {}
	void operator()(AttributeDialogDescEx *pDialogEx)
	{
		if(pDialogEx->GetClassEdited() == m_name)
			m_dialogSet.insert(pDialogEx);
	}
protected:
	std::set<AttributeDialogDescEx *>& m_dialogSet;
	std::string m_name;
};

//
//   Class Name: RenameEditedClassClass
//  Description: Function class to rename the class being edited by a dialog
//
class RenameEditedClassClass
{
public:
	RenameEditedClassClass(const std::string& name, const std::string& newName) : m_name(name), m_newName(newName) {}
	void operator()(AttributeDialogDescEx *pDialogEx)
	{
		if(pDialogEx->GetClassEdited() == m_name)
			pDialogEx->SetClassEdited(m_newName);
	}
protected:
	std::string m_name;
	std::string m_newName;
};

//
//   Class Name: EqualsClass
//  Description: Function class indicating if object is the same as the given object
//
class EqualsClass
{
public:
	EqualsClass(const std::string& name) : m_name(name), m_is(false) {}
	void operator()(ObjectEx *pObjectEx)
	{
		if(pObjectEx->GetName() == m_name)
			m_is = true;
	}
	bool m_is;
protected:
	std::string m_name;
};


//
//   Class Name: BuildAttributeClass
//  Description: 
//
class BuildAttributeClass
{
public:
	BuildAttributeClass(dmat::AttributeClass& aClass) : m_aClass(aClass) {}
	void operator()(AttributeClassEx *pClassEx)
	{
		dmat::AttributeClass *pClass = pClassEx->GetAttributeClass();
		int i, id;

		for(i=0; i<pClass->GetSize(); i++)
		{
			id = pClass->GetId(i);
			m_aClass.AddAttribute(id, (*pClass)[id]);
		}
	}
protected:
	dmat::AttributeClass& m_aClass;
};

#endif // INC_ATTRIB_EX_PROCESS_H_