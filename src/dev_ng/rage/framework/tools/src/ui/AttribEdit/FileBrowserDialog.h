#if !defined(AFX_CFILEBROWSERDIALOG_H__958E4076_AD4D_11D3_995B_00902709243B__INCLUDED_)
#define AFX_CFILEBROWSERDIALOG_H__958E4076_AD4D_11D3_995B_00902709243B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CFileBrowserDialog.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CFileBrowserDialog dialog

class CFileBrowserDialog : public CDialog
{
// Construction
public:
	CFileBrowserDialog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CFileBrowserDialog)
	enum { IDD = IDD_FILEBROWSE };
	CString	m_extension;
	CString	m_description;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFileBrowserDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CFileBrowserDialog)
	virtual void OnOK();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CFILEBROWSERDIALOG_H__958E4076_AD4D_11D3_995B_00902709243B__INCLUDED_)
