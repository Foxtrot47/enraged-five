// NewMemberDialog.cpp : implementation file
//

#include "stdafx.h"
#include "AttribEdit.h"
#include "NewMemberDialog.h"

//#include <dma.h>
// STD headers
#include <strstream>
#include <locale>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CNewMemberDialog dialog


CNewMemberDialog::CNewMemberDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CNewMemberDialog::IDD, pParent), m_editDefault(false)
{
	//{{AFX_DATA_INIT(CNewMemberDialog)
	m_default = _T("");
	m_name = _T("");
	m_true = -1;
	m_type = -1;
	//}}AFX_DATA_INIT
}


void CNewMemberDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CNewMemberDialog)
	DDX_Text(pDX, IDC_DEFAULT_EDIT, m_default);
	DDX_Text(pDX, IDC_NAME_EDIT, m_name);
	DDX_Radio(pDX, IDC_TRUE_RADIO, m_true);
	DDX_CBIndex(pDX, IDC_TYPE_COMBO, m_type);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CNewMemberDialog, CDialog)
	//{{AFX_MSG_MAP(CNewMemberDialog)
	ON_CBN_SELCHANGE(IDC_TYPE_COMBO, OnSelchangeTypeCombo)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

//
//        name: CNewMemberDialog::UpdateUI
// description: Update UI, disable true/false radio if boolean not selected and disable edit box
//				if boolean is selected
//
void CNewMemberDialog::UpdateUI()
{
	if(m_type == 2)
	{
		GetDlgItem(IDC_DEFAULT_EDIT)->EnableWindow(FALSE);
		GetDlgItem(IDC_TRUE_RADIO)->EnableWindow(TRUE);
		GetDlgItem(IDC_FALSE_RADIO)->EnableWindow(TRUE);
	}
	else
	{
		GetDlgItem(IDC_DEFAULT_EDIT)->EnableWindow(TRUE);
		GetDlgItem(IDC_TRUE_RADIO)->EnableWindow(FALSE);
		GetDlgItem(IDC_FALSE_RADIO)->EnableWindow(FALSE);
	}

	if(m_editDefault)
	{
		GetDlgItem(IDC_NAME_EDIT)->EnableWindow(FALSE);
		GetDlgItem(IDC_NAME_STATIC)->EnableWindow(FALSE);
		GetDlgItem(IDC_TYPE_COMBO)->EnableWindow(FALSE);
		GetDlgItem(IDC_TYPE_STATIC)->EnableWindow(FALSE);
		SetWindowText("Set default value");
	}
}

/////////////////////////////////////////////////////////////////////////////
// CNewMemberDialog message handlers

BOOL CNewMemberDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	UpdateUI();
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CNewMemberDialog::OnSelchangeTypeCombo() 
{
	// TODO: Add your control notification handler code here
	UpdateData();
	UpdateUI();	
}

void CNewMemberDialog::OnOK() 
{
	CString name;
	int length;

	length = GetDlgItemText(IDC_NAME_EDIT, name);

	if(length == 0)
	{
		MessageBox("You forgot to type in a member name", 
			"Creation failed", 
			MB_ICONERROR|MB_OK);
		return;
	}
	std::locale loc;
	if(!std::use_facet< std::ctype<char> >(loc).is(std::ctype_base::alnum, name[0]))
	{
		std::ostrstream msg;
		// construct message
		msg << "The member name \"" << (const char *)name << "\" does not start with a valid character." << std::ends;

		MessageBox(msg.str(), 
			"Creation failed", 
			MB_ICONERROR|MB_OK);
		return;
	}

	
	CDialog::OnOK();
}
