// AttribEditView.h : interface of the CAttribEditView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_ATTRIBEDITVIEW_H__A5357601_2ECF_11D3_98A5_00902709243B__INCLUDED_)
#define AFX_ATTRIBEDITVIEW_H__A5357601_2ECF_11D3_98A5_00902709243B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class AttributeDialogDescEx;
class AttributeClassEx;
class ObjectEx;

class CAttribEditView : public CListView
{
protected: // create from serialization only
	CAttribEditView();
	DECLARE_DYNCREATE(CAttribEditView)

public:
	CAttribEditDoc* GetDocument();

	CImageList m_imageList;

	void DeleteAllColumns();
	void SetClassColumns();
	void SetDialogColumns();
	void RefreshObject();
	void SetCurrentObject(class ObjectEx *pObj);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAttribEditView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual void OnInitialUpdate(); // called first time after construct
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CAttribEditView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	void SetNewClassName(unsigned int param, const char *pNewName);
	void DeleteItem(int item);
	bool DeleteSelected();

	AttributeClassEx *GetEditedClass();
	AttributeDialogDescEx *GetEditedDialog();
	int GetFirstElement() {return m_firstElement;}

protected:

	ObjectEx *m_pObjectEx;
	int m_firstElement;

// Generated message map functions
protected:
	//{{AFX_MSG(CAttribEditView)
	afx_msg void OnLButtonDblClk(unsigned int nFlags, CPoint point);
	afx_msg void OnEndlabeledit(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnKeydown(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	afx_msg void OnStyleChanged(int nStyleType, LPSTYLESTRUCT lpStyleStruct);
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in AttribEditView.cpp
inline CAttribEditDoc* CAttribEditView::GetDocument()
   { return (CAttribEditDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ATTRIBEDITVIEW_H__A5357601_2ECF_11D3_98A5_00902709243B__INCLUDED_)
