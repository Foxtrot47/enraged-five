//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by AttribEdit.rc
//
#define IDD_ABOUTBOX                    100
#define IDB_DMAMAN                      111
#define ID_VIEW_ARRANGE                 127
#define IDR_MAINFRAME                   128
#define IDR_ATTRIBTYPE                  129
#define IDI_OPENFOLD                    133
#define IDI_CLOSEFOLD                   134
#define IDI_DIALOG                      136
#define IDI_CLASS                       137
#define IDI_ELEMENT                     138
#define IDR_CLASS                       141
#define IDD_NEWDIALOG_DIALOG            144
#define IDD_LISTCTRL_DIALOG             145
#define IDD_NEWMEMBER_DIALOG            146
#define IDD_CLASSNAME_DIALOG            147
#define IDR_DIALOG                      149
#define IDI_EDITBOX                     152
#define IDI_GROUPBOX                    153
#define IDI_CHECKBOX                    154
#define IDI_COMBOBOX                    155
#define IDI_SPINNER                     156
#define IDD_ADDCONTROL                  157
#define IDD_GROUPBOX                    158
#define IDD_COMBOBOX                    159
#define IDD_SPINNER                     160
#define IDD_FLOAT_SPINNER               161
#define IDR_MOVE                        161
#define IDB_BITMAP1                     163
#define IDD_EDITCONTROL                 164
#define IDI_FILEBROWSER                 165
#define IDD_FILEBROWSE                  165
#define IDB_BITMAP2                     166
#define IDC_CLASSNAME_EDIT              1000
#define IDC_LIST1                       1001
#define IDC_NAME_EDIT                   1002
#define IDC_DEFAULT_EDIT                1003
#define IDC_TRUE_RADIO                  1004
#define IDC_FALSE_RADIO                 1005
#define IDC_TYPE_COMBO                  1006
#define IDC_CLASS_COMBO                 1007
#define IDC_DIALOGNAME_EDIT             1008
#define IDC_LIST2                       1009
#define IDC_NAME                        1012
#define IDC_MINIMUM                     1013
#define IDC_MAXIMUM                     1014
#define IDC_DLG                         1014
#define IDC_PROPERTIES                  1015
#define IDC_CHOOSE                      1016
#define IDC_NAME_STATIC                 1017
#define IDC_TYPE_STATIC                 1018
#define IDC_EXTENSION                   1020
#define IDC_DESCRIPTION                 1021
#define IDC_ENTERNAME_RADIO             1022
#define IDC_CLASSNAME_RADIO             1023
#define ID_CLASS_DELETE                 32777
#define ID_CLASS_NEW                    32779
#define ID_CLASS_NEW_MEMBER             32780
#define ID_CLASS_INCLUDE                32781
#define ID_CLASS_DELETE_MEMBER          32782
#define ID_DIALOG_NEW                   32783
#define ID_DIALOG_NEW_CONTROL           32784
#define ID_DIALOG_INCLUDE               32785
#define ID_DIALOG_DELETE                32786
#define ID_DIALOG_DELETE_CONTROL        32787
#define ID_DIALOG_GROUPBOX              32789
#define ID_DIALOG_CHECKBOX              32790
#define ID_DIALOG_COMBOBOX              32791
#define ID_DIALOG_SPINNER               32792
#define ID_DIALOG_EDITBOX               32793
#define ID_MOVE_DOWN                    32796
#define ID_MOVE_UP                      32797
#define ID_DIALOG_TEST                  32798
#define ID_BUTTON32799                  32799
#define ID_CLASS_DEFAULT                32801
#define ID_DIALOG_ATTRIBUTES            32802
#define ID_HELP_ATTRIBEDIT              32803
#define ID_HELP_ATTRIBUTE               32804
#define ID_DIALOG_FILEBROWSER           32807
#define ID_DIALOG_AUTO                  32808

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        167
#define _APS_NEXT_COMMAND_VALUE         32811
#define _APS_NEXT_CONTROL_VALUE         1023
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
