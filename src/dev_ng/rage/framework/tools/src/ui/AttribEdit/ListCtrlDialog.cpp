// ListCtrlDialog.cpp : implementation file
//

#include "stdafx.h"
#include "AttribEdit.h"
#include "ListCtrlDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CListCtrlDialog dialog


CListCtrlDialog::CListCtrlDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CListCtrlDialog::IDD, pParent), m_title("Dialog"), m_pImageList(NULL)
{
	//{{AFX_DATA_INIT(CListCtrlDialog)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CListCtrlDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CListCtrlDialog)
	DDX_Control(pDX, IDC_LIST1, m_list);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CListCtrlDialog, CDialog)
	//{{AFX_MSG_MAP(CListCtrlDialog)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

//
//        name: CListCtrlDialog::SetImageList
// description: Set pointer to image list
//          in: pImageList = pointer to image list
//
void CListCtrlDialog::SetImageList(CImageList *pImageList)
{
	m_pImageList = pImageList;
}

void CListCtrlDialog::AddItem(const std::string item, int data, int image)
{
	ListCtrlEntry entry;
	entry.m_name = item;
	entry.m_image = image;
	entry.m_data = data;

	m_entries.push_back(entry);
}

//
//        name: CListCtrlDialog::GetSelectedItemData
// description: Get information about the selected item
//         out: data
//
int CListCtrlDialog::GetSelectedItemData(void)
{
	return m_selectedData;
}

/////////////////////////////////////////////////////////////////////////////
// CListCtrlDialog message handlers


BOOL CListCtrlDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	int row=0;
	int item;
	
	m_list.ModifyStyle(LVS_TYPEMASK, LVS_LIST);
	m_list.ModifyStyle(LVS_SORTDESCENDING, LVS_SHOWSELALWAYS | LVS_SINGLESEL |LVS_SORTASCENDING);
	m_list.SetItemState(0, LVIS_SELECTED, LVIS_SELECTED);

	while(!m_entries.empty())
	{
		item = m_list.InsertItem(row++, m_entries.front().m_name.c_str(), m_entries.front().m_image);
		m_list.SetItemData(item, m_entries.front().m_data);
		m_entries.pop_front();
	}
	// set image list
	if(m_pImageList)
		m_list.SetImageList(m_pImageList, LVSIL_SMALL);
	// set title
	SetWindowText(m_title.c_str());


	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CListCtrlDialog::OnOK() 
{
	if(m_list.GetSelectedCount() == 0)
	{
		MessageBox("You haven't selected a Class", "Warning", MB_OK);
		return;
	}
	POSITION p = m_list.GetFirstSelectedItemPosition();
	int index = m_list.GetNextSelectedItem(p);

	m_selectedData = m_list.GetItemData(index);

	CDialog::OnOK();
}
