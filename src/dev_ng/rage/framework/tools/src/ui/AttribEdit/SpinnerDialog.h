#if !defined(AFX_SPINNERDIALOG_H__EF1A9F88_344A_11D3_8E9B_00902709243B__INCLUDED_)
#define AFX_SPINNERDIALOG_H__EF1A9F88_344A_11D3_8E9B_00902709243B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SpinnerDialog.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSpinnerDialog dialog

class CSpinnerDialog : public CDialog
{
// Construction
public:
	CSpinnerDialog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSpinnerDialog)
	enum { IDD = IDD_SPINNER };
	float	m_max;
	float	m_min;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSpinnerDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL


// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSpinnerDialog)
	virtual void OnOK();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SPINNERDIALOG_H__EF1A9F88_344A_11D3_8E9B_00902709243B__INCLUDED_)
