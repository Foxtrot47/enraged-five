#if !defined(AFX_COMBOBOXDIALOG_H__EF1A9F87_344A_11D3_8E9B_00902709243B__INCLUDED_)
#define AFX_COMBOBOXDIALOG_H__EF1A9F87_344A_11D3_8E9B_00902709243B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ComboBoxDialog.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CComboBoxDialog dialog

class CComboBoxDialog : public CDialog
{
// Construction
public:
	CComboBoxDialog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CComboBoxDialog)
	enum { IDD = IDD_COMBOBOX };
	CString	m_entries;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CComboBoxDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CComboBoxDialog)
	virtual void OnOK();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COMBOBOXDIALOG_H__EF1A9F87_344A_11D3_8E9B_00902709243B__INCLUDED_)
