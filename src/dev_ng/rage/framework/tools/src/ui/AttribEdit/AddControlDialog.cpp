// AddControlDialog.cpp : implementation file
//

#include "stdafx.h"
#include "AttribEdit.h"
#include "AddControlDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAddControlDialog dialog


CAddControlDialog::CAddControlDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CAddControlDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CAddControlDialog)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_imageList.Create(16, 16, ILC_COLOR8, 2, 2);
	m_imageList.SetBkColor(RGB(255,255,255));
	m_imageList.Add(theApp.LoadIcon(IDI_ELEMENT));
	m_imageList.Add(theApp.LoadIcon(IDI_DIALOG));
}


void CAddControlDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAddControlDialog)
	DDX_Control(pDX, IDC_LIST2, m_list);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAddControlDialog, CDialog)
	//{{AFX_MSG_MAP(CAddControlDialog)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CAddControlDialog::AddItem(const std::string item, int data, int image)
{
	AddControlEntry entry;
	entry.m_name = item;
	entry.m_image = image;
	entry.m_data = data;

	m_entries.push_back(entry);
}

//
//        name: CListCtrlDialog::GetSelectedItemData
// description: Get information about the selected item
//         out: data
//
int CAddControlDialog::GetSelectedItemData(void)
{
	return m_selectedData;
}

/////////////////////////////////////////////////////////////////////////////
// CAddControlDialog message handlers

BOOL CAddControlDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();

	int item;
	int row = 0;
	RECT wndRect;

	// add extra dialog
	GetDlgItem(IDC_DLG)->GetWindowRect(&wndRect);
	ScreenToClient(&wndRect);
	switch(m_type)
	{
	case dmat::AttributeControlDesc::SPINNER:
		m_spinnerDialog.Create(IDD_SPINNER, this);
		m_spinnerDialog.MoveWindow(&wndRect);
		m_spinnerDialog.ModifyStyle(0, DS_CONTROL);
		break;
	case dmat::AttributeControlDesc::GROUPBOX:
		m_groupboxDialog.Create(IDD_GROUPBOX, this);
		m_groupboxDialog.MoveWindow(&wndRect);
		m_groupboxDialog.ModifyStyle(0, DS_CONTROL);
		SetDlgItemText(IDC_CHOOSE, "Choose the dialog to place inside the groupbox");
		break;
	case dmat::AttributeControlDesc::COMBOBOX:
		m_comboboxDialog.Create(IDD_COMBOBOX, this);
		m_comboboxDialog.MoveWindow(&wndRect);
		m_comboboxDialog.ModifyStyle(0, DS_CONTROL);
		break;
	case dmat::AttributeControlDesc::FILEBROWSER:
		m_fileBrowserDialog.Create(IDD_FILEBROWSE, this);
		m_fileBrowserDialog.MoveWindow(&wndRect);
		m_fileBrowserDialog.ModifyStyle(0, DS_CONTROL);
		break;
	case dmat::AttributeControlDesc::CHECKBOX:
	case dmat::AttributeControlDesc::EDITBOX:
		GetDlgItem(IDC_PROPERTIES)->ShowWindow(FALSE);
		break;
	}
	
	// update list ctrl
	m_list.ModifyStyle(LVS_TYPEMASK, LVS_LIST);
	m_list.ModifyStyle(0, LVS_SHOWSELALWAYS | LVS_SINGLESEL);
	m_list.SetItemState(0, LVIS_SELECTED, LVIS_SELECTED);

	while(!m_entries.empty())
	{
		item = m_list.InsertItem(row++, m_entries.front().m_name.c_str(), m_entries.front().m_image);
		m_list.SetItemData(item, m_entries.front().m_data);
		m_entries.pop_front();
	}

	m_list.SetImageList(&m_imageList, LVSIL_SMALL);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CAddControlDialog::OnOK() 
{
	switch(m_type)
	{
	case dmat::AttributeControlDesc::SPINNER:
		m_spinnerDialog.UpdateData();
		break;
	case dmat::AttributeControlDesc::GROUPBOX:
		m_groupboxDialog.UpdateData();
		SetDlgItemText(IDC_CHOOSE, "Choose the dialog to place inside the groupbox");
		break;
	case dmat::AttributeControlDesc::COMBOBOX:
		m_comboboxDialog.UpdateData();
		break;
	case dmat::AttributeControlDesc::FILEBROWSER:
		m_fileBrowserDialog.UpdateData();
		break;
	}

	if(m_list.GetSelectedCount() == 0)
	{
		MessageBox("You haven't selected anything", "Warning", MB_OK);
		return;
	}
	POSITION p = m_list.GetFirstSelectedItemPosition();
	int index = m_list.GetNextSelectedItem(p);

	m_selectedData = m_list.GetItemData(index);

	CDialog::OnOK();
}
