//
//
//    Filename: AttribScriptEx.cpp
//     Creator: Adam Fowler
//     $Author: Adam $
//       $Date: 9/07/99 15:54 $
//   $Revision: 1 $
// Description: Class used to load extra information from an attribute script file
//
//

// my headers
#include "AttribScriptEx.h"
#include "AttribScript.h"
#include "script.h"
// STD headers
#include <cassert>

using std::endl;

//
//        name: AttributeScriptReader::AttributeScriptReader
// description: Constructor
//          in: pFilename = name of file
//
AttributeScriptFileEx::AttributeScriptFileEx(const char *pFilename) : 
std::basic_ifstream<char>(pFilename), m_pManager(NULL)
{
}

//
//        name: AttributeScriptFileEx::ReadClass
// description: Parse script to load in one attribute class
//
void AttributeScriptFileEx::ReadClass()
{
	assert(m_pManager);

	AttributeClassEx *pInheritedClass;
	std::string className;
	std::string line;
	std::string typeName;

	// get name
	if(!GetValidString(*this, className))
		throw dmat::AttributeFileError("Error reading class name");

	AttributeClassEx attributeClassEx(className);

	while(!eof())
	{
		if(!GetValidLine(*this, line))
			break;
		
		std::istringstream is(line);

		if(!(is >> typeName))
			throw dmat::AttributeFileError("Error reading type");
		// check type 
		// if type is inherited class then add inherited class to class
		if(typeName == INHERIT_TAG)
		{
			if(!GetValidString(is, typeName))
				throw dmat::AttributeFileError("Error reading inherited class");
			pInheritedClass = m_pManager->GetClass(typeName);
			if(pInheritedClass == NULL)
				throw dmat::AttributeFileError("Unrecognised inherited class : " + typeName);
			attributeClassEx.AddChild(pInheritedClass);
			continue;
		}
		// if type is the endclass tag then finish, by adding this class to the manager and exiting
		else if(typeName == ENDCLASS_TAG)
		{
			m_pManager->AddClassEx(attributeClassEx);
			return;
		}
	}

	// did not find an end class indentifier
	throw dmat::AttributeFileError("Reached end of file before class definition finished");
}

//
//        name: AttributeScriptFileEx::ReadDialog
// description: Parse script to load in one attribute dialog
//
void AttributeScriptFileEx::ReadDialog()
{
	assert(m_pManager);

	AttributeDialogDescEx *pChildDialogDesc;
	std::string dialogName;
	std::string className;
	std::string typeName;
	std::string line;
	pos_type filePosn;

	// get name
	if(!GetValidString(*this, dialogName))
		throw dmat::AttributeFileError("Error reading dialog name");

	AttributeDialogDescEx attributeDialogEx(dialogName);

	filePosn = tellg();

	// get class that is being edited
	if(*this >> typeName)
	{
		if(typeName == "#editing" && GetValidString(*this, className))
			attributeDialogEx.SetClassEdited(className);
	}

	seekg(filePosn);

	while(!eof())
	{
		if(!GetValidLine(*this, line))
			break;
		
		std::istringstream is(line);

		if(!(is >> typeName))
			throw dmat::AttributeFileError("Error reading control type");
		
		// if end of dialog description
		if(typeName == ENDDIALOG_TAG)
		{
			m_pManager->AddDialogDescEx(attributeDialogEx);
			return;
		}
		else if(typeName == GROUPBOX_TAG)
		{
			std::string groupName;
			if(!GetValidString(is, typeName))
				throw dmat::AttributeFileError("Error reading groupbox UI class");
			pChildDialogDesc = m_pManager->GetDialogDesc(typeName);
			if(pChildDialogDesc == NULL)
				throw dmat::AttributeFileError("Unrecognised UI name : " + typeName);
			attributeDialogEx.AddChild(pChildDialogDesc);
		}
	}
		
	// did not find an end dialog indentifier
	throw dmat::AttributeFileError("Reached end of file before dialog definition finished");
}

//
//        name: AttributeScriptReader::Read
// description: Read a script file and copy data into attribute manager
//          in: manager = reference to manager
//
void AttributeScriptFileEx::Read(AttributeManagerEx &manager)
{
	if(fail())
	{
		throw dmat::AttributeFileError("Script file does not exist");
	}

	m_pManager = &manager;
	ObjectEx::SetManager(&manager);

	std::string token;
	bool valid = false;

	*this >> token;
	if(token != "#att")
	{
		throw dmat::AttributeFileError("This is not a file created by Attribute Edit");
	}

	try
	{
		// read in classes
		while(!eof())
		{
			if(!GetValidString(*this, token))
				break;

			if(token == STARTCLASS_TAG)
			{
				ReadClass();
				valid = true;
			}
			else if(token == STARTDIALOG_TAG)
			{
				ReadDialog();
				valid = true;
			}
		}

	}
	catch(dmat::AttributeError)
	{
		throw dmat::AttributeFileError("Error");
	}
	if(!valid)
	{
		throw dmat::AttributeFileError("Error");
	}
}


// --- AttributeScriptFileOutEx ----------------------------------------------------------------------

//
//        name: AttributeScriptFileOutEx::AttributeScriptFileOutEx
// description: Constructor
//
AttributeScriptFileOutEx::AttributeScriptFileOutEx(const char *pFilename) :
std::basic_ofstream<char>(pFilename), m_filename(pFilename)
{
}


//
//        name: AttributeScriptFileOutEx::WriteHeader
// description: Write header for attribute script file
//
void AttributeScriptFileOutEx::WriteHeader()
{
	(*this) << "#att" << endl;
	(*this) << "#" << endl;
	(*this) << "# Attribute definition script file " << m_filename << endl;
	(*this) << "# output by AttributeEdit. AttributeEdit was written by Adam Fowler." << endl;
	(*this) << "#" << endl << endl;
}

//
//        name: AttributeScriptFileOutEx::WriteClass
// description: Output definition for given attribute class
//          in: pClassEx = pointer to class
//
void AttributeScriptFileOutEx::WriteClass(AttributeClassEx *pClassEx)
{
	dmat::AttributeClass *pClass = pClassEx->GetAttributeClass();
	std::vector<std::string> children = pClassEx->GetChildrenVector();
	std::vector<std::string>::iterator iChild;
	int i, id;

	// if class has already been output then do nothing
	if(m_writtenClasses.find(pClassEx->GetName()) != m_writtenClasses.end())
		return;
	// set class as output
	m_writtenClasses.insert(pClassEx->GetName());

	// output included classes
	for(iChild = children.begin(); iChild != children.end(); iChild++)
	{
		WriteClass(pClassEx->GetManager()->GetClass(*iChild));
	}


	(*this) << STARTCLASS_TAG << " \"" << pClassEx->GetName() << "\"" << endl;

	// output included classes
	for(iChild = children.begin(); iChild != children.end(); iChild++)
	{
		(*this) << "    " << INHERIT_TAG << " \"" << (*iChild) << "\"" << endl;
	}

	(*this) << endl;

	// output members
	for(i=0; i<pClass->GetSize(); i++)
	{
		id = pClass->GetId(i);
		dmat::Attribute &attr = (*pClass)[id];

		(*this) << "    ";
		switch (attr.GetType())
		{
		case dmat::Attribute::INT:
			(*this) << INT_TAG <<" "<< id <<" \""<< attr.GetName() <<"\" "<< (int)attr.GetDefault();
			break;
		case dmat::Attribute::FLOAT:
			(*this) << FLOAT_TAG <<" "<< id <<" \""<< attr.GetName() <<"\" "<< (float)attr.GetDefault();
			break;
		case dmat::Attribute::BOOL:
			(*this) << BOOL_TAG <<" "<< id <<" \""<< attr.GetName() <<"\" "<< (bool)attr.GetDefault();
			break;
		case dmat::Attribute::STRING:
			(*this) << STRING_TAG <<" "<< id <<" \""<< attr.GetName() <<"\" \""<< (const char *)attr.GetDefault() << "\"";
			break;
		}
		(*this) << endl;
	}

	(*this) << ENDCLASS_TAG << endl << endl;
}

//
//        name: AttributeScriptFileOutEx::WriteDialog
// description: Output definition for given dialog
//          in: pDialogEx = pointer to dialog
//
void AttributeScriptFileOutEx::WriteDialog(AttributeDialogDescEx *pDialogEx)
{
	dmat::AttributeDialogDesc *pDialog = pDialogEx->GetAttributeDialog();
	std::vector<std::string> children = pDialogEx->GetChildrenVector();
	std::vector<std::string>::iterator iChild;
	int i;

	// if class has already been output then do nothing
	if(m_writtenDialogs.find(pDialogEx->GetName()) != m_writtenDialogs.end())
		return;
	// set class as output
	m_writtenDialogs.insert(pDialogEx->GetName());

	// output included classes
	for(iChild = children.begin(); iChild != children.end(); iChild++)
	{
		WriteDialog(pDialogEx->GetManager()->GetDialogDesc(*iChild));
	}

	(*this) << STARTDIALOG_TAG << " \"" << pDialogEx->GetName() << "\" ";
	(*this) << "#editing \"" << pDialogEx->GetClassEdited() << "\"" << endl;

	// output controls
	for(i=0; i<pDialog->GetSize(); i++)
	{
		dmat::AttributeControlDesc *pControl = pDialog->GetControl(i);
		dmat::AttribListControlDesc *pListControl = pControl->AsListControl();
		dmat::AttributeValueControlDesc *pValueControl = pControl->AsValueControl();
		int id;

		if(pValueControl)
			id = pValueControl->GetAttributeId();

		(*this) << "    ";
		switch(pControl->GetType())
		{
		case dmat::AttributeControlDesc::GROUPBOX:
			assert(pListControl);
			(*this) << GROUPBOX_TAG << " \"";
			(*this) << pDialogEx->GetDialogNameFromPtr(pListControl->GetDialogDesc());
			(*this) << "\" \"" << pListControl->GetName() << "\""; 
			break;
		case dmat::AttributeControlDesc::SPINNER:
			assert(pValueControl);
			if(pValueControl->GetEditType() == dmat::Attribute::INT)
			{
				int min,max;
				((dmat::IntSpinnerControlDesc *)pControl)->GetLimits(min,max);

				(*this) << INTSPIN_TAG <<" "<< id <<" "<< min <<" " << max;
			}
			else if(pValueControl->GetEditType() == dmat::Attribute::FLOAT)
			{
				float min,max;
				((dmat::FloatSpinnerControlDesc *)pControl)->GetLimits(min,max);

				(*this) << FLOATSPIN_TAG <<" "<< id <<" "<< min <<" " << max;
			}
			else
				assert(0);
			break;
		case dmat::AttributeControlDesc::EDITBOX:
			assert(pValueControl);
			(*this) << EDITBOX_TAG <<" "<< id;
			break;
		case dmat::AttributeControlDesc::COMBOBOX:
			assert(pValueControl);
			(*this) << COMBOBOX_TAG <<" "<< id << " \"";
			(*this) << ((dmat::ComboBoxControlDesc *)pControl)->GetEntriesString() << "\"";
			break;
		case dmat::AttributeControlDesc::CHECKBOX:
			assert(pValueControl);
			(*this) << CHECKBOX_TAG <<" "<< id;
			break;
		case dmat::AttributeControlDesc::FILEBROWSER:
			assert(pValueControl);
			(*this) << FILEBROWSER_TAG <<" "<< id;
			(*this) << " \"" << ((dmat::FileBrowserControlDesc *)pControl)->GetFileDescription() << "\" \"";
			(*this) << ((dmat::FileBrowserControlDesc *)pControl)->GetExtension() << "\"" << std::ends;
			break;
		}
		(*this) << endl;
	}
	
	(*this) << ENDDIALOG_TAG << endl << endl;
}

//
//        name: AttributeScriptFileOutEx::Write
// description: Output script file based on given AttributeManager
//          in: manager = reference to attribute manager
//
void AttributeScriptFileOutEx::Write(AttributeManagerEx &manager)
{
	if(fail())
	{
		throw dmat::AttributeFileError("Cannot write script file");
	}


	WriteHeader();

	AttributeClassExMap classMap = manager.GetClassMap();
	AttributeClassExMapIterator iClass;
	AttributeDialogExMap dialogMap = manager.GetDialogMap();
	AttributeDialogExMapIterator iDialog;

	// output classes
	for(iClass = classMap.begin(); iClass != classMap.end(); iClass++)
	{
		WriteClass(&(*iClass).second);
	}

	// output dialogs
	for(iDialog = dialogMap.begin(); iDialog != dialogMap.end(); iDialog++)
	{
		WriteDialog(&(*iDialog).second);
	}
}

