// Classname.cpp : implementation file
//

#include "stdafx.h"
#include "AttribEdit.h"
#include "Classname.h"

//#include <dma.h>
//STD headers
#include <iostream>
#include <strstream>
#include <locale>
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CClassname dialog


CClassname::CClassname(CWnd* pParent /*=NULL*/)
	: CDialog(CClassname::IDD, pParent)
{
	//{{AFX_DATA_INIT(CClassname)
	m_name = _T("");
	//}}AFX_DATA_INIT
}


void CClassname::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CClassname)
	DDX_Text(pDX, IDC_CLASSNAME_EDIT, m_name);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CClassname, CDialog)
	//{{AFX_MSG_MAP(CClassname)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CClassname message handlers

void CClassname::OnOK() 
{
	CString name;
	int length;
//	std::ctype<char> type;

	length = GetDlgItemText(IDC_CLASSNAME_EDIT, name);

	if(length == 0)
	{
		MessageBox("You forgot to type in a class name", 
			"Creation failed", 
			MB_ICONERROR|MB_OK);
		return;
	}
	locale loc;
	if(!use_facet< ctype<char> >(loc).is(std::ctype_base::alnum, name[0]))
	{
		std::ostrstream msg;
		// construct message
		msg << "The class name \"" << (const char *)name << "\" does not start with a valid character." << std::ends;

		MessageBox(msg.str(), 
			"Creation failed", 
			MB_ICONERROR|MB_OK);
		return;
	}

	CDialog::OnOK();
}
