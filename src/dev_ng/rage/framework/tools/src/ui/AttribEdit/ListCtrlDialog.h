#if !defined(AFX_LISTCTRLDIALOG_H__C832EF85_32BF_11D3_8E9A_00902709243B__INCLUDED_)
#define AFX_LISTCTRLDIALOG_H__C832EF85_32BF_11D3_8E9A_00902709243B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ListCtrlDialog.h : header file
//

//#include <dma.h>
// STD headers
#include <list>
#include <string>

// ListCtrlEntry: structure storing entry information for the list 
typedef struct 
{
	std::string m_name;
	int m_image;
	int m_data;
} ListCtrlEntry;

/////////////////////////////////////////////////////////////////////////////
// CListCtrlDialog dialog

class CListCtrlDialog : public CDialog
{
// Construction
public:
	CListCtrlDialog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CListCtrlDialog)
	enum { IDD = IDD_LISTCTRL_DIALOG };
	CListCtrl	m_list;
	//}}AFX_DATA

	void SetTitle(std::string title) {m_title = title;}
	void SetImageList(CImageList *pImageList);
	void AddItem(const std::string item, int data, int image = 0);
	int GetSelectedItemData(void);
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CListCtrlDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	int m_selectedData;
	std::string m_title;
	CImageList *m_pImageList;
	std::list<ListCtrlEntry> m_entries;

	// Generated message map functions
	//{{AFX_MSG(CListCtrlDialog)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LISTCTRLDIALOG_H__C832EF85_32BF_11D3_8E9A_00902709243B__INCLUDED_)
