﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using RSG.Base.Logging;
using RSG.Editor;
using RSG.Editor.Controls;
using RSG.Editor.Controls.Helpers;
using RSG.Editor.SharedCommands;
using RSG.UniversalLog.Commands;
using RSG.UniversalLog.View;
using RSG.UniversalLog.View.Commands;
using RSG.UniversalLog.ViewModel;

namespace UniversalLogViewer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : RsMainWindow
    {
        #region Static Fields
        /// <summary>
        /// The global identifier used for the edit menu.
        /// </summary>
        private static readonly Guid _editMenu = new Guid("FB6395F0-ABBA-48DF-8995-3264BE70286F");

        /// <summary>
        /// The global identifier used for the filter tool bar.
        /// </summary>
        private static readonly Guid _filterToolBar = new Guid("CC372B15-1EC5-4726-88F7-362B608F95DA");
        #endregion Static Fields

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        public MainWindow(RsApplication application)
        {
            InitializeComponent();
            DataContext = new MainWindowDataContext();
            AddCommandInstances(application);
            AttachCommandBindings();
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Adds the commands/menus to the command manager.
        /// </summary>
        internal void AddCommandInstances(RsApplication application)
        {
            // Register the mru list.
            application.RegisterMruList(
                "Files",
                true,
                RockstarCommands.OpenFileFromMru,
                new Guid("FE2926AE-5067-478D-949A-557B76C41804"),
                new Guid("2C28F26B-2AD7-4D17-91C7-7D8901699A54"),
                new Guid("9B950624-0E34-4DE1-ADA2-2EE0656366D0"));

            // File menu.
            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.OpenFile,
                0,
                false,
                "Open",
                new Guid("AA9A7A22-0861-44C9-8551-754EF7A92B1E"),
                RockstarCommandManager.FileMenuId);
            
            RockstarCommandManager.AddCommandInstance(
                UniversalLogCommands.MergeFile,
                1,
                false,
                "Open Merged",
                new Guid("D73F5522-D822-4102-88D9-D44486AC3B0D"),
                RockstarCommandManager.FileMenuId);

            RockstarCommandManager.AddCommandInstance(
                UniversalLogCommands.Refresh,
                2,
                false,
                "Reload from Disk",
                new Guid("0DEF42D7-D40E-4C98-8428-AFB362A2952E"),
                RockstarCommandManager.FileMenuId);
            
            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.SaveAs,
                3,
                true,
                "Save As",
                new Guid("3BCFF04F-BE3B-4103-AADD-96C3232BC60D"),
                RockstarCommandManager.FileMenuId);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Close,
                4,
                true,
                "Close",
                new Guid("E7B6382D-D675-4045-9D11-A1B9F1AC2C59"),
                RockstarCommandManager.FileMenuId);

            // Edit menu.
            RockstarCommandManager.AddEditMenu(_editMenu);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Copy,
                0,
                false,
                "Copy",
                new Guid("C1B51EC5-2A57-4B41-939E-0091C7397F17"),
                _editMenu);

            // Help menu.
            RockstarCommandManager.AddCommandInstance(
                UniversalLogCommands.Email,
                0,
                true,
                "Email",
                new Guid("75BA025F-F911-4FD1-AE37-854DD335304C"),
                RockstarCommandManager.HelpMenuId);

            // Filter toolbar.
            RockstarCommandManager.AddToolbar(0, 0, "Filter", _filterToolBar);

            RockstarCommandManager.AddCommandInstance(
                UniversalLogCommands.FilterByLogLevel,
                0,
                true,
                "Filter by Log Level",
                new Guid("CD2F05E7-D5BD-4042-8B63-51DD99E69AA7"),
                _filterToolBar,
                new CommandInstanceCreationArgs()
                {
                    AreMultiItemsShown = true,
                    EachItemStartsGroup = false,
                });

            RockstarCommandManager.AddCommandInstance(
                UniversalLogCommands.FilterByFile,
                1,
                true,
                "Filter by File",
                new Guid("CB2091A4-99E7-4268-8158-FDD03680B16D"),
                _filterToolBar,
                new CommandInstanceCreationArgs()
                {
                    AreMultiItemsShown = false,
                    EachItemStartsGroup = true,
                });

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Find,
                2,
                true,
                "Find Text...",
                new Guid("77C0109E-D225-43D6-9F6C-58C9A47F3D2B"),
                _filterToolBar);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.FindPrevious,
                2,
                false,
                "Find Previous",
                new Guid("88EE9376-C6FA-4693-8561-04D8FD2867D5"),
                _filterToolBar);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.FindNext,
                2,
                false,
                "Find Next",
                new Guid("157D02AC-08D3-4036-88D0-5FB734BDC5DC"),
                _filterToolBar);

            // Help toolbar.
            RockstarCommandManager.AddCommandInstance(
                UniversalLogCommands.Email,
                1,
                false,
                "Email",
                new Guid("7D50C33B-A928-4410-9B11-74F9D809E852"),
                RockstarCommandManager.HelpToolBarId);

            // Message List Context Menu
            //TODO: Should this be in the view's control?
            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Copy,
                0,
                false,
                "Copy",
                new Guid("6FEEC9F1-96E0-4E5F-9C36-E9B2120350DD"),
                UniversalLogCommandIds.ContextMenu);
        }

        /// <summary>
        /// Attaches all of the necessary command bindings to this instance.
        /// </summary>
        private void AttachCommandBindings()
        {
            // File management commands.
            new OpenFileAction(this.PrimaryViewer.DataContextResolver)
                .AddBinding(RockstarCommands.OpenFile, this);
            new MergeFileAction(this.PrimaryViewer.DataContextResolver)
                .AddBinding(UniversalLogCommands.MergeFile, this);
            new RefreshAction(this.PrimaryViewer.DataContextResolver)
                .AddBinding(UniversalLogCommands.Refresh, this);
            new SaveAsAction(this.PrimaryViewer.DataContextResolver)
                .AddBinding(RockstarCommands.SaveAs, this);
            new CloseFileAction(this.PrimaryViewer.DataContextResolver)
                .AddBinding(RockstarCommands.Close, this);
            new OpenFromMruAction(this.PrimaryViewer.DataContextResolver)
                .AddBinding(RockstarCommands.OpenFileFromMru, this);
            new EmailAction(this.PrimaryViewer.DataContextResolver)
                .AddBinding(UniversalLogCommands.Email, this);

            // Universal Log Control Commands
            this.PrimaryViewer.CopyAction.AddBinding(RockstarCommands.Copy, this);
            this.PrimaryViewer.FilterByLogLevelAction.AddBinding(UniversalLogCommands.FilterByLogLevel, this);
            this.PrimaryViewer.FilterByFileAction.AddBinding(UniversalLogCommands.FilterByFile, this);
            this.PrimaryViewer.SearchAction.AddBinding(this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        private MainWindowDataContext DataContextResolver(ICommand command)
        {
            MainWindowDataContext dc = null;
            if (!this.Dispatcher.CheckAccess())
            {
                dc = this.Dispatcher.Invoke<MainWindowDataContext>(() => this.DataContext as MainWindowDataContext);
            }
            else
            {
                dc = this.DataContext as MainWindowDataContext;
            }

            if (dc == null)
            {
                Debug.Fail("Data context isn't valid.");
                throw new ArgumentException("Data context isn't valid.");
            }
            return dc;
        }

        /// <summary>
        /// Represents the method that is used to retrieve a command parameter object for the
        /// view commands.
        /// </summary>
        /// <param name="command">
        /// The command whose command parameter needs to be resolved.
        /// </param>
        /// <returns>
        /// The command parameter object for the specified command.
        /// </returns>
        private UniversalLogControl ControlResolver(ICommand command)
        {
            return PrimaryViewer;
        }
        #endregion // Methods

        #region Drag & Drop Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RsMainWindow_DragEnterAndOver(object sender, DragEventArgs e)
        {
            bool containsValidFiles = false;

            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                String[] filenames = (String[])e.Data.GetData(DataFormats.FileDrop, true);
                containsValidFiles = true;

                foreach (String filename in filenames)
                {
                    String extension = System.IO.Path.GetExtension(filename);
                    if (extension != ".ulog" && extension != ".ulogzip")
                    {
                        containsValidFiles = false;
                        break;
                    }
                }
            }

            // Update the cursor effect based on whether the drop file list contains a valid ulog file.
            if (containsValidFiles)
            {
                e.Effects = DragDropEffects.All;
            }
            else
            {
                e.Effects = DragDropEffects.None;
            }
            e.Handled = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RsMainWindow_Drop(object sender, DragEventArgs e)
        {
            // Get the main window data context.
            MainWindowDataContext dc = null;
            if (!this.Dispatcher.CheckAccess())
            {
                dc = this.Dispatcher.Invoke<MainWindowDataContext>(() => this.DataContext as MainWindowDataContext);
            }
            else
            {
                dc = this.DataContext as MainWindowDataContext;
            }

            if (dc == null)
            {
                Debug.Fail("Data context isn't valid.");
                throw new ArgumentException("Data context isn't valid.");
            }
            
            // Get the list of files and determine whether we should open it merged with the current content.
            String[] filenames = (String[])e.Data.GetData(DataFormats.FileDrop, true);
            bool openMerged = ((Keyboard.Modifiers & ModifierKeys.Shift) != 0);

            dc.UniversalLogDataContext.OpenFiles(filenames, openMerged);
        }
        #endregion // Drag & Drop Event Handlers
    } // MainWindow
}
