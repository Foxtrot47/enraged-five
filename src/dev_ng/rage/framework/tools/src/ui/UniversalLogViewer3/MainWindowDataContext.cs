﻿//---------------------------------------------------------------------------------------------
// <copyright file="MainWindowDataContext.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using RSG.Editor;
using RSG.Editor.Controls;
using RSG.Editor.SharedCommands;
using RSG.UniversalLog.ViewModel;

namespace UniversalLogViewer
{
    /// <summary>
    /// Represents the data context that should be attached to the main window.
    /// </summary>
    internal sealed class MainWindowDataContext : NotifyPropertyChangedBase
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MainWindowDataContext"/> class.
        /// </summary>
        public MainWindowDataContext()
            : base()
        {
            _universalLogDataContext = new UniversalLogDataContext();
            _universalLogDataContext.LoadedFiles += this.OnLoad;
            _universalLogDataContext.UnloadedFiles += this.OnUnload;
        }
        #endregion Constructors

        #region Properties and associated member data
        /// <summary>
        /// Gets the title that should be set on the main window.
        /// </summary>
        public String Title
        {
            get { return _title; }
            private set { SetProperty(ref _title, value); }
        }
        private String _title;

        /// <summary>
        /// Gets the status text that should be displayed in the status bar.
        /// </summary>
        public String StatusText
        {
            get { return _statusText; }
            private set { SetProperty(ref _statusText, value); }
        }
        private String _statusText;

        /// <summary>
        /// Universal log viewer data context.
        /// </summary>
        public UniversalLogDataContext UniversalLogDataContext
        {
            get { return _universalLogDataContext; }
            private set { SetProperty(ref _universalLogDataContext, value); }
        }
        private UniversalLogDataContext _universalLogDataContext;
        #endregion // Properties and associated member data

        #region Methods
        /// <summary>
        /// Called whenever a ulog file has been loaded, successfully or unsuccessfully.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The data used for this event.
        /// </param>
        private void OnLoad(object sender, UlogLoadedEventArgs e)
        {
            RsApplication app = Application.Current as RsApplication;
            if (app != null && e.Filepaths != null)
            {
                foreach (String filepath in e.Filepaths)
                {
                    app.AddToMruList("Files", filepath, new string[0]);
                }
            }

            UpdateTitle();
        }

        /// <summary>
        /// Called whenever a RPF file has been unloaded.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The data used for this event.
        /// </param>
        private void OnUnload(object sender, EventArgs e)
        {
            UpdateTitle();
        }

        /// <summary>
        /// Sets the window's title.
        /// </summary>
        private void UpdateTitle()
        {
            // Set the title based on the nubmer of loaded files.
            if (_universalLogDataContext.LoadedFileViewModels.Count == 1)
            {
                this.Title = _universalLogDataContext.LoadedFileViewModels.First().Filepath;
            }
            else if (_universalLogDataContext.LoadedFileViewModels.Count > 1)
            {
                this.Title = String.Format("{0} Files Loaded", _universalLogDataContext.LoadedFileViewModels.Count);
            }
            else
            {
                this.Title = "";
            }
        }
        #endregion // Methods
    } // MainWindowDataContext
}
