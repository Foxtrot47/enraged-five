﻿//---------------------------------------------------------------------------------------------
// <copyright file="App.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime;
using System.Threading.Tasks;
using System.Windows;
using Microsoft.Win32;
using RSG.Base.Extensions;
using RSG.Editor;
using RSG.Editor.Controls;
using RSG.Editor.SharedCommands;

namespace UniversalLogViewer
{
    /// <summary>
    /// Defines the main entry point to the application and is responsible for creating and
    /// running the application.
    /// </summary>
    public class App : RsApplication
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="App"/> class.
        /// </summary>
        public App()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the unique name for this application that is used for mutex creation, and
        /// folder organisation inside the local app data folder and the registry.
        /// </summary>
        public override string UniqueName
        {
            get { return "UniversalLogViewer"; }
        }

        /// <summary>
        /// Gets a value indicating whether the layout for the main window get serialised on
        /// exit and deserialised during loading.
        /// </summary>
        protected override bool MakeLayoutPersistent
        {
            get { return true; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Main entry point into the application. The first thing to get called.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            App app = new App();
            app.ShutdownMode = ShutdownMode.OnMainWindowClose;
            RsApplication.OnEntry(app, ApplicationMode.Multiple);
        }

        /// <summary>
        /// Override to create the main window for the application.
        /// </summary>
        /// <returns>
        /// The System.Windows.Window that will be the main window for this application.
        /// </returns>
        protected override Window CreateMainWindow()
        {
            return new MainWindow(this);
        }

        /// <summary>
        /// Called immediately after the main windows Show method has been called.
        /// </summary>
        /// <param name="mainWindow">
        /// A reference to the main window that was shown.
        /// </param>
        /// <returns>
        /// A task representing the work done by this method.
        /// </returns>
        protected async override Task OnMainWindowShown(Window mainWindow)
        {
            MainWindowDataContext dc = this.MainWindow.DataContext as MainWindowDataContext;
            if (dc == null)
            {
                Debug.Assert(
                    dc != null,
                    "Unable to complete startup operations as the data context is missing");
                return;
            }
            
            // Determine the list of files to open.
            IList<String> filenames = new List<String>();
            foreach (String pathname in this.TrailingArguments)
            {
                if (String.IsNullOrEmpty(pathname))
                {
                    continue;
                }

                if (Directory.Exists(pathname))
                {
                    filenames.AddRange(Directory.GetFiles(pathname, "*.ulog"));
                }
                else if (File.Exists(pathname))
                {
                    filenames.Add(pathname);
                }
            }

            dc.UniversalLogDataContext.OpenFiles(filenames, false);
            await Task.Delay(0);
        }
        #endregion Methods
    } // UniversalLogViewer.App {Class}
} // UniversalLogViewer {Namespace}
