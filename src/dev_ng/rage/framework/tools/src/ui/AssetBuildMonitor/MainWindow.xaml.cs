﻿//---------------------------------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace AssetBuildMonitor
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Windows;
    using AssetBuildMonitor.Resources;
    using RSG.Base.Extensions;
    using RSG.Configuration;
    using RSG.Configuration.Bugstar;
    using RSG.Editor;
    using RSG.Editor.Controls;
    using RSG.AssetBuildMonitor.Commands;
    using RSG.Project.View;
    using RSG.Project.ViewModel;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : RsMainWindow
    {
        #region Member Data
        /// <summary>
        /// Global identifier used for the standard toolbar.
        /// </summary>
        private static Guid _standardToolbar;

        /// <summary>
        /// Global identifier used for the File New submenu.
        /// </summary>
        private static Guid _newFileMenu;

        /// <summary>
        /// Global identifier used for the File open submenu.
        /// </summary>
        private static Guid _openFileMenu;

        /// <summary>
        /// Global identifier used for the File Add submenu.
        /// </summary>
        private static Guid _addFileMenu;

        /// <summary>
        /// Global identifier used for the actions menu inside the main menu.
        /// </summary>
        private static Guid _actionsMenu;

        /// <summary>
        /// Global identifier used for the project menu inside the main menu.
        /// </summary>
        private static Guid _projectMenu;

        /// <summary>
        /// Global identifier used for the view menu inside the main menu.
        /// </summary>
        private static Guid _viewMenu;

        /// <summary>
        /// Global identifier used for the Project Add menu inside the host explorer.
        /// </summary>
        private static Guid _projectAddProjectContextMenu;

        /// <summary>
        /// Global identifier used for the Item Add menu inside the host explorer.
        /// </summary>
        private static Guid _itemAddProjectContextMenu;

        /// <summary>
        /// Global identifier used for the Folder Add menu inside the host explorer.
        /// </summary>
        private static Guid _folderAddProjectContextMenu;

        /// <summary>
        /// Host explorer.
        /// </summary>
        private ProjectCollectionExplorer _hostExplorer;

        /// <summary>
        /// File watcher (for project and collections only).
        /// </summary>
        private FileWatcherManager _fileWatcher;
        #endregion // Member Data

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="MainWindow" /> class.
        /// </summary>
        static MainWindow()
        {
            _standardToolbar = new Guid("ECC03646-2B7E-4208-82D8-880FBC82A49B");
            _newFileMenu = new Guid("89CFBD5B-6DB9-4A04-8859-0E48069612FE");
            _openFileMenu = new Guid("C2D1229B-B546-4D77-B221-04B4566ED32E");
            _addFileMenu = new Guid("DC33A02C-9454-490E-8A90-62F7CD60458D");
            _projectMenu = new Guid("78B49579-B8AD-4934-9CCD-88B663FFDAD8");
            _actionsMenu = new Guid("BEDBDCFD-7A7B-473F-A1CA-647D18B3BDDC");
            _viewMenu = new Guid("FAE56765-D5C9-419D-8840-3D47676EA8FC");
            _projectAddProjectContextMenu = new Guid("0838AC45-12BE-4E5F-8800-6BDE256DC902");
            _itemAddProjectContextMenu = new Guid("31858B45-1920-40CC-834D-FD77EE9A1613");
            _folderAddProjectContextMenu = new Guid("1DCFA6A9-0D2A-4B93-9C2C-6E2A5724F30C");
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        /// <param name="application">
        /// The instance to the application this is going to be the main window for.
        /// </param>
        public MainWindow(RsApplication application)
        {
            this.InitializeComponent();
            Object hostExplorerResource = this.TryFindResource("HostExplorer");
            this._hostExplorer = hostExplorerResource as ProjectCollectionExplorer;
            if (this._hostExplorer == null)
            {
                throw new NotSupportedException();
            }

            // Setup View-Model and MainWindow DataContext.
            CommandOptions options = new CommandOptions();
            IBugstarConfig bugstarConfig = ConfigFactory.CreateBugstarConfig(
                RSG.Base.Logging.LogFactory.ApplicationLog, options.CoreProject);
            MainWindowDataContext dc = new MainWindowDataContext(bugstarConfig);
            this.DataContext = dc;

            this.AddCommandInstances(application);
            this.AddStandardToolbar();
            this.AttachCommandBindings();

            // Initialise FileWatcher.
            this._fileWatcher = new FileWatcherManager();
            this._fileWatcher.ModifiedTimeChanged += this.OnModifiedTimeChanged;

            dc.CollectionNode.ProjectLoaded += this.OnProjectLoaded;
            dc.CollectionNode.ProjectUnloaded += this.OnProjectUnloaded;
            dc.CollectionNode.ProjectFailed += this.OnProjectFailed;
            dc.CollectionNode.LoadedStateChanged += this.CollectionLoadedStateChanged;
        }
        #endregion // Constructors

        #region Private Methods
        #region Command Setup
        /// <summary>
        /// The action resolver that returns a instance of the <seealso cref="CommandArgs"/> 
        /// class that host commands can use to execute their logic.
        /// </summary>
        /// <param name="commandData">
        /// The command data sent with the executing command.
        /// </param>
        /// <returns>
        /// The currently active document item inside the view site.
        /// </returns>
        internal CommandArgs CommandResolver(CommandData commandData)
        {
            Debug.Assert(this.DataContext is MainWindowDataContext);
            if (!(this.DataContext is MainWindowDataContext))
                throw (new NotSupportedException());

            MainWindowDataContext dc = (MainWindowDataContext)this.DataContext;
            CommandArgs args = new CommandArgs();
            args.CurrentProjectCollection = dc.CollectionNode;
            args.AllDocuments = dc.ViewSite.AllDocuments;
            args.ActiveDocument = dc.ViewSite.ActiveDocument;
            args.BugstarUsers = dc.BugstarUsers;
            args.ViewSite = dc.ViewSite;

            Debug.Assert(null != this._hostExplorer);
            if (null == this._hostExplorer)
                throw (new NotSupportedException());
            args.SelectedNodes = this._hostExplorer.SelectedItems;
            
            return args;
        }
        
        /// <summary>
        /// The action resolver that returns a instance of the project command args class that
        /// project commands can use to execute their logic.
        /// </summary>
        /// <param name="commandData">
        /// The command data sent with the executing command.
        /// </param>
        /// <returns>
        /// The currently active document item inside the view site.
        /// </returns>
        internal RSG.Project.Commands.ProjectCommandArgs CommandResolverProject(CommandData commandData)
        {
            Debug.Assert(this.DataContext is MainWindowDataContext);
            if (!(this.DataContext is MainWindowDataContext))
                throw (new NotSupportedException());

            MainWindowDataContext dc = (MainWindowDataContext)this.DataContext;
            RSG.Project.Commands.ProjectCommandArgs args = new RSG.Project.Commands.ProjectCommandArgs();
            args.CollectionNode = dc.CollectionNode;
            args.FileWatcher = this._fileWatcher;
            args.OpenedDocuments = dc.ViewSite.AllDocuments;
            args.ViewSite = dc.ViewSite;
            if (this._hostExplorer != null)
            {
                args.SelectedNodes = this._hostExplorer.SelectedItems;
            }

            return args;
        }

        /// <summary>
        /// Adds all of the command instances to the command manager.
        /// </summary>
        /// <param name="application"></param>
        private void AddCommandInstances(RsApplication application)
        {
            // Main Menus
            this.AddCommandInstanceToMainMenu();
            this.AddCommandInstanceToFileMenu();
            this.AddCommandInstanceToActionsMenu();
            this.AddCommandInstanceToProjectMenu();
            this.AddCommandInstanceToViewMenu();

            // Context Menus
            this.AddCommandInstancesToProjectContextMenu();
            this.AddCommandInstancesToBuildItemContextMenu();
        }

        /// <summary>
        /// Adds command instances to the main menu.
        /// </summary>
        private void AddCommandInstanceToMainMenu()
        {
            RockstarCommandManager.AddProjectMenu(_projectMenu);
            RockstarCommandManager.AddViewMenu(_viewMenu);
            RockstarCommandManager.AddActionsMenu(_actionsMenu);
        }

        /// <summary>
        /// Add command instances to the actions menu.
        /// </summary>
        private void AddCommandInstanceToActionsMenu()
        {
            ushort index = 0;
            RockstarCommandManager.AddCommandInstance(
                Commands.OpenLog,
                index++,
                true,
                "Open Log",
                new Guid("8C60317F-E208-4D49-A9D7-9877D8368A4B"),
                _actionsMenu);

            RockstarCommandManager.AddCommandInstance(
                Commands.ClearHistory,
                index++,
                true,
                "Clear History",
                new Guid("DC1A160F-0422-4169-A0DE-CD123BC40360"),
                _actionsMenu);
        }

        /// <summary>
        /// Add command instances to the project menu.
        /// </summary>
        private void AddCommandInstanceToProjectMenu()
        {
            ushort index = 0;
            RockstarCommandManager.AddCommandInstance(
                Commands.ConnectToHost,
                index++,
                true,
                "Connect to Host",
                new Guid("56DB821D-ED46-4F49-8014-CB362FC2ACFC"),
                _projectMenu);

            RockstarCommandManager.AddCommandInstance(
                Commands.ConnectToNewHost,
                index++,
                false,
                "Connect to New Host...",
                new Guid("F54E30ED-09DD-42F8-B540-057E425C9D17"),
                _projectMenu);

            RockstarCommandManager.AddCommandInstance(
                RSG.Project.ViewModel.ProjectCommands.ReloadProject,
                index++,
                true,
                "Reload",
                new Guid("A4FAC66E-7729-42EA-94FE-29ECAF029F55"),
                _projectMenu);

            RockstarCommandManager.AddCommandInstance(
                RSG.Project.ViewModel.ProjectCommands.UnloadProject,
                index++,
                false,
                "Unload",
                new Guid("BADDE815-C884-45A5-A5C6-30D4F5E61D93"),
                _projectMenu);

            RockstarCommandManager.AddCommandInstance(
                RSG.Project.ViewModel.ProjectCommands.AddCollectionFolder,
                index++,
                true,
                "Add New Solution Folder",
                new Guid("19B5B258-0EC8-4BC2-B2A5-C89130F0F96B"),
                _projectMenu);

            RockstarCommandManager.AddCommandInstance(
                RSG.Project.ViewModel.ProjectCommands.AddProjectFolder,
                index++,
                false,
                "New Folder",
                new Guid("92ECC2AE-C6BD-4F20-8604-16785DD77CA2"),
                _projectMenu);
        }

        /// <summary>
        /// Add command instances to the File Menu.
        /// </summary>
        private void AddCommandInstanceToFileMenu()
        {
            ushort index = 0;
            RockstarCommandManager.AddMenu(
                index++, "New", _newFileMenu, RockstarCommandManager.FileMenuId);

            RockstarCommandManager.AddMenu(
                index++, "Open", _openFileMenu, RockstarCommandManager.FileMenuId);

            RockstarCommandManager.AddMenu(
                index++, true, "Add", _addFileMenu, RockstarCommandManager.FileMenuId);
            
            this.AddFileMenuNewCommands();
            this.AddFileMenuOpenCommands();
            this.AddFileMenuAddCommands();
            
            RockstarCommandManager.AddCommandInstance(
                RSG.Editor.Controls.Dock.DockingCommands.CloseItem,
                index++,
                true,
                "Close",
                new Guid("3C920F77-E443-45CA-A030-FF7E19FBE65A"),
                RockstarCommandManager.FileMenuId);

            RockstarCommandManager.AddCommandInstance(
                RSG.Project.ViewModel.ProjectCommands.CloseProjectCollection,
                index++,
                false,
                "Close Project Collection",
                new Guid("324C3328-2283-4217-99E0-306F34F2DA02"),
                RockstarCommandManager.FileMenuId);

            RockstarCommandManager.AddCommandInstance(
                RSG.Editor.SharedCommands.RockstarCommands.Save,
                index++,
                true,
                "Save Selected Items",
                new Guid("3BAA7D00-DAF6-410C-A109-3DAB40A783E1"),
                RockstarCommandManager.FileMenuId);

            RockstarCommandManager.AddCommandInstance(
                RSG.Editor.SharedCommands.RockstarCommands.SaveAs,
                index++,
                false,
                "Save Selected Items As...",
                new Guid("123F7582-27AD-40CC-ABDE-D8D9D546FC29"),
                RockstarCommandManager.FileMenuId);

            RockstarCommandManager.AddCommandInstance(
                RSG.Editor.SharedCommands.RockstarCommands.SaveAll,
                index++,
                false,
                "Save All",
                new Guid("1C1F7391-2F12-4A09-8D5E-BDFC118A7AD1"),
                RockstarCommandManager.FileMenuId);
        }

        /// <summary>
        /// Adds command instances to the command manager for commands that are located inside
        /// the New menu off of the File menu.
        /// </summary>
        private void AddFileMenuNewCommands()
        {
            ushort index = 0;
            RockstarCommandManager.AddCommandInstance(
                RSG.Project.ViewModel.ProjectCommands.CreateNewProject,
                index++,
                false,
                "Project...",
                new Guid("A78C4AB0-7F8B-4930-AC65-CDBF62272900"),
                _newFileMenu);

            RockstarCommandManager.AddCommandInstance(
                Commands.ConnectToNewHost,
                index++,
                true,
                "Connect to New Host...",
                new Guid("89F2808B-288D-4E2F-ACF2-CF00E7C8DF30"),
                _newFileMenu);

            RockstarCommandManager.AddCommandInstance(
                RSG.Project.ViewModel.ProjectCommands.AddProjectFolder,
                index++,
                false,
                "New Folder...",
                new Guid("7F890DD7-6788-44C4-885C-E77597C87156"),
                _newFileMenu);
        }

        /// <summary>
        /// Adds command instances to the command manager for commands that are located inside
        /// the Open menu off of the File menu.
        /// </summary>
        private void AddFileMenuOpenCommands()
        {
            ushort index = 0;
            RockstarCommandManager.AddCommandInstance(
                RSG.Project.ViewModel.ProjectCommands.OpenProjectCollectionFile,
                index++,
                false,
                "Project Collection...",
                new Guid("E4FF77F9-20FE-41C7-81DC-22837A1EEB22"),
                _openFileMenu);

            RockstarCommandManager.AddCommandInstance(
                RSG.Project.ViewModel.ProjectCommands.OpenProjectFile,
                index++,
                false,
                "Project Inside New Collection...",
                new Guid("81AA2E50-9D88-46E9-BDD9-35EA2DEBE2DC"),
                _openFileMenu);
        }

        /// <summary>
        /// Adds command instances to the command manager for commands that are located inside
        /// the Add menu off of the project node context menu.
        /// </summary>
        private void AddFileMenuAddCommands()
        {
            ushort index = 0;
            RockstarCommandManager.AddCommandInstance(
                RSG.Project.ViewModel.ProjectCommands.AddNewProject,
                index++,
                false,
                "New Project...",
                new Guid("208A3026-33E7-42C9-82B5-BABBBD70C9B4"),
                _addFileMenu);

            RockstarCommandManager.AddCommandInstance(
                RSG.Project.ViewModel.ProjectCommands.AddExistingProject,
                index++,
                false,
                "Existing Project...",
                new Guid("4EC56518-50CD-421A-94CF-972495C3BB6E"),
                _addFileMenu);
        }

        /// <summary>
        /// Add command instances to the View Menu.
        /// </summary>
        private void AddCommandInstanceToViewMenu()
        {
            RockstarCommandManager.AddCommandInstance(
                Commands.UseUTC,
                0,
                true,
                "Use UTC",
                new Guid("D78B53D8-EDD9-4D39-B51E-D580A8F3C77B"),
                _viewMenu);

            RockstarCommandManager.AddCommandInstance(
                Commands.Refresh,
                0,
                true,
                "Refresh",
                new Guid("311F292D-DCC8-483B-9AEC-00BB9BD7AFDA"),
                _viewMenu);
        }

        /// <summary>
        /// Add Project Context Menu commands.
        /// </summary>
        private void AddCommandInstancesToProjectContextMenu()
        {
            ushort index = 0;

            // Project Add Menu
            RockstarCommandManager.AddMenu(
                index,
                "Add",
                _projectAddProjectContextMenu,
                RSG.Project.ViewModel.ProjectCommandIds.ProjectContextMenu);
            RockstarCommandManager.AddCommandInstance(
                Commands.ConnectToNewHost,
                index++,
                false,
                "Connect to New Host...",
                new Guid("047ACA3F-6BFB-43CF-9AB7-FC87EB53D2F1"),
                _projectAddProjectContextMenu);
            RockstarCommandManager.AddCommandInstance(
                RSG.Project.ViewModel.ProjectCommands.AddProjectFolder,
                index++,
                false,
                "New Folder...",
                new Guid("1AB272A4-6528-490C-AACE-529C3C4067F2"),
                _projectAddProjectContextMenu);
            RockstarCommandManager.AddCommandInstance(
                RSG.Project.ViewModel.ProjectCommands.OpenContainingFolder,
                index++,
                true,
                "Open Containing Folder",
                new Guid("441E1EDF-8387-4CA2-B0D7-65F286D753EC"),
                RSG.Project.ViewModel.ProjectCommandIds.ProjectContextMenu);
            RockstarCommandManager.AddCommandInstance(
                RSG.Project.ViewModel.ProjectCommands.OpenCommandPrompt,
                index++,
                false,
                "Open Command Prompt",
                new Guid("B07BA291-4F41-46E4-9E5D-B4B2799DB6E6"),
                RSG.Project.ViewModel.ProjectCommandIds.ProjectContextMenu);

            // Project Item Menu
            index = 0;
            RockstarCommandManager.AddCommandInstance(
                Commands.ConnectToHost,
                index++,
                false,
                "Connect to Host",
                new Guid("D050FB33-2690-446B-859A-EC9A390FF9AC"),
                RSG.Project.ViewModel.ProjectCommandIds.ProjectItemContextMenu);
            RockstarCommandManager.AddCommandInstance(
                RSG.Editor.SharedCommands.RockstarCommands.Delete,
                index++,
                true,
                "Delete",
                new Guid("C38C31BC-923D-4705-82C2-7008F97927EC"),
                RSG.Project.ViewModel.ProjectCommandIds.ProjectItemContextMenu);

            // Project Folder Add Menu
            index = 0;
            RockstarCommandManager.AddMenu(
                index,
                "Add",
                _folderAddProjectContextMenu,
                RSG.Project.ViewModel.ProjectCommandIds.ProjectFolderContextMenu); 
            RockstarCommandManager.AddCommandInstance(
                Commands.ConnectToNewHost,
                index++,
                false,
                "Connect to New Host...",
                new Guid("95FB81E4-2E2A-447D-B760-88B1BAACF15C"),
                _folderAddProjectContextMenu);
            RockstarCommandManager.AddCommandInstance(
                RSG.Project.ViewModel.ProjectCommands.AddProjectFolder,
                index++,
                false,
                "New Folder...",
                new Guid("6DA0C7CC-7557-4D82-B570-38183FB6D13F"),
                _folderAddProjectContextMenu);
            RockstarCommandManager.AddCommandInstance(
                RSG.Project.ViewModel.ProjectCommands.OpenContainingFolder,
                index++,
                true,
                "Open Containing Folder",
                new Guid("840BCAE3-EDC0-4C15-83A4-ED4D61ECE593"),
                RSG.Project.ViewModel.ProjectCommandIds.ProjectFolderContextMenu);
            RockstarCommandManager.AddCommandInstance(
                RSG.Project.ViewModel.ProjectCommands.OpenCommandPrompt,
                index++,
                false,
                "Open Command Prompt",
                new Guid("EF9797FA-856B-43F1-B39B-6E6ABEB29FCB"),
                RSG.Project.ViewModel.ProjectCommandIds.ProjectFolderContextMenu);
            RockstarCommandManager.AddCommandInstance(
                RSG.Editor.SharedCommands.RockstarCommands.Delete,
                index++,
                true,
                "Delete",
                new Guid("B0E1429A-D296-4121-83AC-B8AB512B04AF"),
                RSG.Project.ViewModel.ProjectCommandIds.ProjectFolderContextMenu);

            // 
            index = 0;
            RockstarCommandManager.AddCommandInstance(
                RSG.Editor.SharedCommands.RockstarCommands.Delete,
                index++,
                true,
                "Delete",
                new Guid("68377700-2821-4E59-9703-8103ABD42A3C"),
                RSG.Project.ViewModel.ProjectCommandIds.CollectionContextMenu);

            //
            index = 0;
            RockstarCommandManager.AddCommandInstance(
                RSG.Editor.SharedCommands.RockstarCommands.Delete,
                index++,
                true,
                "Delete",
                new Guid("4ABF8054-45AB-4A0A-BF6E-9AA23E40607F"),
                RSG.Project.ViewModel.ProjectCommandIds.CollectionFolderContextMenu);
        }

        /// <summary>
        /// Add Build Item Context Menu commands.
        /// </summary>
        private void AddCommandInstancesToBuildItemContextMenu()
        {
            ushort index = 0;

            RockstarCommandManager.AddCommandInstance(
                Commands.OpenLog,
                index++,
                false,
                "Open Log",
                new Guid("83B75795-EEE6-40F0-8C65-000560B55CAF"),
                Commands.BuildItemContextMenu);
            RockstarCommandManager.AddCommandInstance(
                Commands.ClearHistory,
                index++,
                true,
                "Clear History",
                new Guid("DE1BAE46-D0EE-414D-B994-1F546C3D694E"),
                Commands.BuildItemContextMenu);
        }

        /// <summary>
        /// Add standard toolbar.
        /// </summary>
        private void AddStandardToolbar()
        {
            RockstarCommandManager.AddToolbar(0, 0, "Standard", _standardToolbar);

            ushort index = 0; 
            RockstarCommandManager.AddCommandInstance(
                Commands.ConnectToNewHost,
                index++,
                false,
                "Connect to New Host...",
                new Guid("4F38BA1D-B8C1-4E7D-8826-0A09137D0828"),
                _standardToolbar);
            RockstarCommandManager.AddCommandInstance(
                Commands.ConnectToHost,
                index++,
                false,
                "Connect to Host",
                new Guid("F87FD85B-DA4D-45AD-BDDE-40ED7C2E5891"),
                _standardToolbar);
        }

        /// <summary>
        /// Attach command bindings.
        /// </summary>
        private void AttachCommandBindings()
        {
            ICommandAction action = new ConnectToHostAction(this.CommandResolver);
            action.AddBinding(Commands.ConnectToHost, this);

            action = new ConnectToNewHostAction(this.CommandResolver);
            action.AddBinding(Commands.ConnectToNewHost, this);

            action = new OpenLogAction(this.CommandResolver);
            action.AddBinding(Commands.OpenLog, this);

            action = new RefreshAction(this.CommandResolver);
            action.AddBinding(Commands.Refresh, this);

            action = new OpenHostFromProjectAction(null, null, this.CommandResolverProject);
            action.AddBinding(RSG.Project.ViewModel.ProjectCommands.OpenFileFromProject, this);
            
            action = new ClearHistoryAction(this.CommandResolver);
            action.AddBinding(Commands.ClearHistory, this);

            action = new UseUTCAction(this.CommandResolver);
            action.AddBinding(Commands.UseUTC, this);

            action = new RSG.Project.Commands.CloseAllButActiveDocumentAction(this.CommandResolverProject);
            action.AddBinding(RSG.Editor.Controls.Dock.DockingCommands.CloseAllButItem, this);

            action = new RSG.Project.Commands.CloseAllDocumentsAction(this.CommandResolverProject);
            action.AddBinding(RSG.Editor.Controls.Dock.DockingCommands.CloseAll, this);

            action = new RSG.Project.Commands.CloseDocumentAction(this.CommandResolverProject);
            action.AddBinding(RSG.Editor.Controls.Dock.DockingCommands.CloseItem, this);

            action = new RSG.Project.Commands.SaveAllAction(this.CommandResolverProject);
            action.AddBinding(RSG.Editor.SharedCommands.RockstarCommands.SaveAll, this);

            action = new RSG.Project.Commands.OpenCollectionAction(this.CommandResolverProject);
            action.AddBinding(RSG.Project.ViewModel.ProjectCommands.OpenProjectCollectionFile, this);

            action = new RSG.Project.Commands.CloseCollectionAction(this.CommandResolverProject);
            action.AddBinding(RSG.Project.ViewModel.ProjectCommands.CloseProjectCollection, this);

            action = new RSG.Project.Commands.AddCollectionFolderAction(this.CommandResolverProject);
            action.AddBinding(RSG.Project.ViewModel.ProjectCommands.AddCollectionFolder, this);

            action = new RSG.Project.Commands.AddNewProjectAction(this.CommandResolverProject);
            action.AddBinding(RSG.Project.ViewModel.ProjectCommands.AddNewProject, this);

            action = new RSG.Project.Commands.AddExistingProjectAction(this.CommandResolverProject);
            action.AddBinding(RSG.Project.ViewModel.ProjectCommands.AddExistingProject, this);

            action = new RSG.Project.Commands.CreateNewProjectAction(this.CommandResolverProject);
            action.AddBinding(RSG.Project.ViewModel.ProjectCommands.CreateNewProject, this);

            action = new RSG.Project.Commands.OpenProjectAction(this.CommandResolverProject);
            action.AddBinding(RSG.Project.ViewModel.ProjectCommands.OpenProjectFile, this);

            action = new RSG.Project.Commands.ReloadProjectAction(this.CommandResolverProject);
            action.AddBinding(RSG.Project.ViewModel.ProjectCommands.ReloadProject, this);

            action = new RSG.Project.Commands.UnloadProjectAction(this.CommandResolverProject);
            action.AddBinding(RSG.Project.ViewModel.ProjectCommands.UnloadProject, this);

            action = new RSG.Project.Commands.CloseAllButActiveDocumentAction(this.CommandResolverProject);
            action.AddBinding(RSG.Editor.Controls.Dock.DockingCommands.CloseAllButItem, this);

            action = new RSG.Project.Commands.CloseAllDocumentsAction(this.CommandResolverProject);
            action.AddBinding(RSG.Editor.Controls.Dock.DockingCommands.CloseAll, this);

            action = new RSG.Project.Commands.CloseDocumentAction(this.CommandResolverProject);
            action.AddBinding(RSG.Editor.Controls.Dock.DockingCommands.CloseItem, this);

            action = new RSG.Project.Commands.DeleteItemAction(this.CommandResolverProject);
            action.AddBinding(RSG.Editor.SharedCommands.RockstarCommands.Delete, this);

            action = new RSG.Project.Commands.AddProjectFolderAction(this.CommandResolverProject);
            action.AddBinding(RSG.Project.ViewModel.ProjectCommands.AddProjectFolder, this);

            action = new RSG.Project.Commands.OpenContainingFolderAction(this.CommandResolverProject);
            action.AddBinding(RSG.Project.ViewModel.ProjectCommands.OpenContainingFolder, this);

            action = new RSG.Project.Commands.OpenCommandPromptAction(this.CommandResolverProject);
            action.AddBinding(RSG.Project.ViewModel.ProjectCommands.OpenCommandPrompt, this);
        }
        #endregion // Command Setup

        #region Event Handlers
        /// <summary>
        /// Called whenever the last write time for a registered file gets updated.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The RSG.Editor.ModifiedTimeChangedEventArgs containing the event data including the
        /// full path to the file that has been modified.
        /// </param>
        private void OnModifiedTimeChanged(Object sender, ModifiedTimeChangedEventArgs e)
        {
            if (this.Dispatcher.CheckAccess())
            {
                this.OnModifiedTimeChanged(e.FullPath);
            }
            else
            {
                this.Dispatcher.BeginInvoke(
                    new Action(
                        delegate
                        {
                            this.OnModifiedTimeChanged(e.FullPath);
                        }));
            }
        }
        
        /// <summary>
        /// Called whenever the last write time for a registered file gets updated.
        /// </summary>
        /// <param name="fullPath">
        /// The full path of the file that was modified.
        /// </param>
        private void OnModifiedTimeChanged(String fullPath)
        {            
            Debug.Assert(this.DataContext is MainWindowDataContext);
            if (!(this.DataContext is MainWindowDataContext))
                throw (new NotSupportedException());

            MainWindowDataContext dc = (MainWindowDataContext)this.DataContext;
            IMessageBox messageBox = new RsMessageBox();
            messageBox.Caption = "";
            messageBox.Text = String.Format(StringTable.ProjectModifiedMessage, Path.GetFileName(fullPath));
            messageBox.AddButton("Yes", 0, true, false);
            messageBox.AddButton("No", 1, false, true);

            throw (new NotImplementedException());
        }

        /// <summary>
        /// Called whenever a project is loaded in the attached collection.
        /// </summary>
        /// <param name="sender">
        /// The collection this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The RSG.Project.ViewModel.ProjectLoadedStateChangedEventArgs containing the event
        /// data.
        /// </param>
        private void OnProjectLoaded(Object sender, ProjectLoadedStateChangedEventArgs e)
        {
            lock (this._fileWatcher)
            {
                this._fileWatcher.RegisterFile(e.Project.FullPath);
            }
        }

        /// <summary>
        /// Called whenever a project is unloaded or removed from the attached collection.
        /// </summary>
        /// <param name="sender">
        /// The collection this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The RSG.Project.ViewModel.ProjectLoadedStateChangedEventArgs containing the event
        /// data.
        /// </param>
        private void OnProjectUnloaded(object sender, ProjectLoadedStateChangedEventArgs e)
        {
            lock (this._fileWatcher)
            {
                this._fileWatcher.UnregisterFile(e.Project.FullPath);
            }
        }

        /// <summary>
        /// Called whenever a project failed to load either the first time or every time the
        /// user tries to reload it.
        /// </summary>
        /// <param name="sender">
        /// The collection this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The RSG.Project.ViewModel.ProjectLoadFailedEventArgs containing the event data.
        /// </param>
        private void OnProjectFailed(Object sender, ProjectLoadFailedEventArgs e)
        {
            if (this.Dispatcher.CheckAccess())
            {
                ProjectNode node = e.Project;

                String name = Path.GetFileNameWithoutExtension(node.Model.Include);
                e.Project.Text = StringTable.ProjectLoadFailedTextFormat.FormatCurrent(name);
                e.Project.NotifyPropertyChanged("Icon");
                e.Project.ClearChildren();
                HierarchyNode failedNode = new HierarchyNode();
                failedNode.Text = StringTable.ProjectLoadFailedNodeText;
                node.AddChild(failedNode);
                this.SelectNode(e.Project);

                if (e.Reloaded == true)
                {
                    RsMessageBox.Show(
                        e.Exception.Message,
                        null,
                        MessageBoxButton.OK,
                        MessageBoxImage.Error);
                }
            }
            else
            {
                this.Dispatcher.BeginInvoke(
                    new Action(
                        delegate
                        {
                            this.OnProjectFailed(sender, e);
                        }));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender">
        /// 
        /// </param>
        /// <param name="e">
        /// 
        /// </param>
        private void CollectionLoadedStateChanged(object sender, ValueChangedEventArgs<bool> e)
        {
            ProjectCollectionNode collection = sender as ProjectCollectionNode;
            if (collection == null)
            {
                return;
            }

            if (e.NewValue)
            {
                this.Title = collection.Model.FullPath ?? String.Empty;
            }
            else
            {
                this.Title = String.Empty;
            }
        }
        #endregion // Event Handlers
        
        /// <summary>
        /// Selects the specified hierarchy node inside the project explorer.
        /// </summary>
        /// <param name="node">
        /// The node that should be selected.
        /// </param>
        private void SelectNode(IHierarchyNode node)
        {
            if (node == null)
            {
                return;
            }

            if (this.Dispatcher.CheckAccess())
            {
                if (this._hostExplorer != null)
                {
                    this._hostExplorer.SelectItem(node);
                }
            }
            else
            {
                this.Dispatcher.BeginInvoke(
                    new Action(
                        delegate
                        {
                            this.SelectNode(node);
                        }));
            }
        }
        #endregion // Private Methods
    }

} // AssetBuildMonitor namespace
