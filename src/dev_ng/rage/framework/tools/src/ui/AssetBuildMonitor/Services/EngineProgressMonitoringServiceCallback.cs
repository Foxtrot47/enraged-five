﻿//---------------------------------------------------------------------------------------------
// <copyright file="EngineProgressMonitoringServiceCallback.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace AssetBuildMonitor.Services
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.ServiceModel;
    using System.Threading.Tasks;
    using RSG.Pipeline.Services.Engine;
    using RSG.AssetBuildMonitor.ViewModel;
    
    /// <summary>
    /// IEngineProgressMonitoringServiceCallback implementation.
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single,
        ConcurrencyMode = ConcurrencyMode.Multiple)]
    internal class EngineProgressMonitoringServiceCallback : 
        IEngineProgressMonitoringServiceCallback
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public EngineProgressMonitoringServiceCallback()
        {
        }
        #endregion // Constructor(s)

        /// <summary>
        /// Method callback executed when Engine starts a build.
        /// </summary>
        /// <param name="e"></param>
        public void NotifyMonitorBuildStarted(BuildStartedEventArgs e)
        {
            throw (new NotImplementedException());
        }

        /// <summary>
        /// Method callback executed when Engine completes a build.
        /// </summary>
        /// <param name="e"></param>
        public void NotifyMonitorBuildComplete(BuildCompleteEventArgs e)
        {
            throw (new NotImplementedException());
        }

        /// <summary>
        /// Method callback executed when Engine aborts a build.
        /// </summary>
        /// <param name="e"></param>
        public void NotifyMonitorBuildAborted(BuildAbortedEventArgs e)
        {
            throw (new NotImplementedException());
        }

        /// <summary>
        /// Method callback executed when Engine stage changes.
        /// </summary>
        /// <param name="newStage"></param>
        public void NotifyMonitorBuildStageChange(AssetBuildStage newStage)
        {
            throw (new NotImplementedException());
        }

        /// <summary>
        /// Method callback executed when Engine step progress changes.
        /// </summary>
        /// <param name="progress"></param>
        public void NotifyMonitorEstimateOfStepProgressChange(Tuple<int, int> progress)
        {
            throw (new NotImplementedException());
        }

        /// <summary>
        /// Method callback executed when Engine invokes log profile call.  This
        /// can be used to get back micro-managed messages about what the engine
        /// is doing.
        /// </summary>
        /// <param name="message"></param>
        public void NotifyMonitorProfileMessage(String message)
        {
            throw (new NotImplementedException());
        }
    }

} // AssetBuildMonitor.Services
