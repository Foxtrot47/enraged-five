﻿//---------------------------------------------------------------------------------------------
// <copyright file="MainWindowDataContext.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace AssetBuildMonitor
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows;
    using RSG.AssetBuildMonitor.ViewModel;
    using RSG.Base.Logging;
    using RSG.Configuration.Bugstar;
    using RSG.Editor;
    using RSG.Editor.Controls;
    using RSG.Editor.Controls.Dock;
    using RSG.Editor.Controls.Dock.ViewModel;
    using Bugstar = RSG.Interop.Bugstar;
    using RSG.Pipeline.Services.Engine;
    using RSG.Pipeline.Services.Engine.Consumers;
    using AssetBuildMonitor.Services;
    using AssetBuildMonitor.ViewModel;
    using RSG.Project.ViewModel.Definitions;
    using AssetBuildMonitor.Project;
    using RSG.Project.Model;
    using RSG.AssetBuildMonitor.ViewModel.Project;

    /// <summary>
    /// Represents the data context that is attached to the main window for this application.
    /// This class cannot be inherited.
    /// </summary>
    internal sealed class MainWindowDataContext :
        NotifyPropertyChangedBase
    {
        #region Properties
        /// <summary>
        /// Gets the collection node that represents the root of the project system.
        /// </summary>
        public RSG.Project.ViewModel.ProjectCollectionNode CollectionNode
        {
            get { return this._collectionNode; }
        }

        /// <summary>
        /// Gets the view site that represents the root group of the docking manager in the
        /// main window.
        /// </summary>
        public ViewSite ViewSite
        {
            get { return _viewSite; }
            private set { this.SetProperty(ref _viewSite, value); }
        }

        /// <summary>
        /// Gets the current cache of Bugstar user data.
        /// </summary>
        public IEnumerable<Bugstar.Organisation.User> BugstarUsers
        {
            get { return _bugstarUsers; }
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// The private field used for the <see cref="CurrentProjectCollection"/> property.
        /// </summary>
        private RSG.Project.ViewModel.ProjectCollectionNode _collectionNode;

        /// <summary>
        /// The private reference to the tool window item that is representing the project
        /// explorer.
        /// </summary>
        private HostExplorerToolWindow _hostExplorer;

        /// <summary>
        /// The private field used for the <see cref="ViewSite"/> property.
        /// </summary>
        private ViewSite _viewSite;
        
        /// <summary>
        /// Bugstar user objects.
        /// </summary>
        private IEnumerable<Bugstar.Organisation.User> _bugstarUsers;
        #endregion // Member Data

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MainWindowDataContext"/> class.
        /// </summary>
        /// <param name="window">
        /// A reference to the main window that this class is being attached to.
        /// </param>
        public MainWindowDataContext(IBugstarConfig bugstarConfig)
        {
            try
            {
                // Bugstar User initialisation.
                Bugstar.BugstarConnection c = new Bugstar.BugstarConnection(bugstarConfig.RESTService, bugstarConfig.AttachmentService);
                c.Login(bugstarConfig.ReadOnlyUsername, bugstarConfig.ReadOnlyPassword, String.Empty);
                Bugstar.Organisation.Project p = Bugstar.Organisation.Project.GetProjectById(c, bugstarConfig.ProjectId);
                _bugstarUsers = p.Users;
            }
            catch (Exception)
            {
                _bugstarUsers = Enumerable.Empty<Bugstar.Organisation.User>();
            }

            this.InitialiseProjectCollection();
            this.InitialiseViewSite();
        }
        #endregion // Constructors

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        public void DisconnectAll()
        {

        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Creates the definitions that are used by the project manager.
        /// </summary>
        /// <returns>
        /// The project collection definition that this application uses.
        /// </returns>
        private ProjectCollectionDefinition InitialiseProjectDefinitions()
        {
            ProjectCollectionDefinition collection = new CollectionDefinition(this._bugstarUsers);
            return collection;
        }

        /// <summary>
        /// Creates the project collection using the metadata editor project item definitions.
        /// </summary>
        private void InitialiseProjectCollection()
        {
            ProjectCollectionDefinition definition = this.InitialiseProjectDefinitions();
            this._collectionNode = new RSG.Project.ViewModel.ProjectCollectionNode(definition);

            // Load user-defined application state.
            String userAppData = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            String userCollectionFilename = Path.Combine(userAppData, "Rockstar Games", "Asset Pipeline", 
                "AssetBuildMonitor.assetbuildmonprojs");
            String userProjectFilename = Path.Combine(userAppData, "Rockstar Games", "Asset Pipeline",
                "Asset Build Monitor.assetbuildmonproj");
            if (File.Exists(userCollectionFilename))
            {            
                this._collectionNode.Load(userCollectionFilename);
            }
            else
            {
                Project.ProjectDefinition def = new Project.ProjectDefinition(this._bugstarUsers);
                ProjectItem newItem = this._collectionNode.Model.AddNewProjectItem("Project", 
                    userProjectFilename);
                RSG.Project.ViewModel.ProjectNode project = def.CreateProjectNode();
                project.SetParentAndModel(this._collectionNode, newItem);
                project.SetDependentBoolProperty("Expanded", true);

                Byte[] template = def.Template;
                Byte[] modifiedTemplate = new byte[template.Length];
                template.CopyTo(modifiedTemplate, 0);
                def.ReplaceVariables(ref modifiedTemplate, userProjectFilename);
                using (FileStream fs = new FileStream(userProjectFilename, FileMode.Create, FileAccess.Write))
                    fs.Write(modifiedTemplate, 0, modifiedTemplate.Length);

                using (FileStream fs = new FileStream(userCollectionFilename, FileMode.Create, FileAccess.Write))
                    this._collectionNode.Save(fs);

                this._collectionNode.Load(userCollectionFilename);
            }
        }

        /// <summary>
        /// Initialise Docking Manager view site.
        /// </summary>
        private void InitialiseViewSite()
        {
            this._viewSite = ViewSite.CreateMainViewSite();
            ToolWindowPane pane = new ToolWindowPane(this._viewSite);
            this._hostExplorer = new HostExplorerToolWindow(this._collectionNode, pane);

            pane.SplitterLength = new SplitterLength(2, SplitterUnitType.Stretch);
            pane.InsertChild(0, this._hostExplorer);

            DocumentPane documentPane = new DocumentPane(this._viewSite);
            documentPane.SplitterLength = new SplitterLength(7, SplitterUnitType.Stretch);

            this._viewSite.RootGroup.InsertChild(0, pane);
            this._viewSite.RootGroup.InsertChild(1, documentPane);
        }
        #endregion // Private Methods
    }

} // AssetBuildMonitor namespace
