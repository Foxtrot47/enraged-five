﻿//---------------------------------------------------------------------------------------------
// <copyright file="App.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace AssetBuildMonitor
{
    using System;
    using System.Windows;
    using RSG.AssetBuildMonitor.View;
    using RSG.AssetBuildMonitor.ViewModel;
    using RSG.Editor.Controls;

    /// <summary>
    /// 
    /// </summary>
    public class App : RsApplication
    {
        #region Properties
        /// <summary>
        /// Gets the unique name for this application that is used for mutex creation, and
        /// folder organisation inside the local app data folder and the registry.
        /// </summary>
        public override String UniqueName
        {
            get { return (AssetBuildMonitor.Resources.StringTable.WindowTitle); }
        }

        /// <summary>
        /// Gets a value indicating whether the layout for the main window get serialised on
        /// exit and deserialised during loading.
        /// </summary>
        protected override bool MakeLayoutPersistent
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a semi-colon separate list of the application authors email addresses. These
        /// are the addresses which are used by the unhandled exception dialog to determine
        /// where the mail should be sent by default.
        /// e.g.
        /// *tools@rockstarnorth.com
        /// michael.taschler@rockstarnorth.com;dave.evans@rockstarnorth.com
        /// </summary>
        public override String AuthorEmailAddress
        {
            get { return "*tools@rockstarnorth.com"; }
        }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Main entry point into the application. The first thing to get called.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            App app = new App();
            app.ShutdownMode = ShutdownMode.OnMainWindowClose;
            RsApplication.OnEntry(app, ApplicationMode.Single);
        }

        /// <summary>
        /// Override to create the main window for the application.
        /// </summary>
        /// <returns>
        /// The System.Windows.Window that will be the main window for this application.
        /// </returns>
        protected override Window CreateMainWindow()
        {
            MainWindow mainWindow = new MainWindow(this);
            return mainWindow;
        }

        /// <summary>
        /// Performs all tasks related to the application being shutdown.
        /// </summary>
        /// <param name="persistentDirectory">
        /// The path to the directory all persistent files should be saved.
        /// </param>
        protected override void HandleOnExiting(String persistentDirectory)
        {
            // Disconnect from listener.
            MainWindowDataContext ctx = (MainWindowDataContext)RsApplication.Current.MainWindow.DataContext;
            ctx.DisconnectAll();

            base.HandleOnExiting(persistentDirectory);
        }

        /// <summary>
        /// Gets the service object of the specified type making sure that before the base is
        /// called the <see cref="RSG.AssetBuildMonitor.ViewModel.IDialogServices"/> interface is
        /// handled.
        /// </summary>
        /// <param name="serviceType">
        /// An object that specifies the type of service object to get.
        /// </param>
        /// <returns>
        /// The service of the specified type if found; otherwise, null.
        /// </returns>
        public override object GetService(Type serviceType)
        {
            if (serviceType == typeof(IDialogServices))
                return (new DialogServices());
            else if (serviceType == typeof(RSG.Project.ViewModel.IProjectAddViewService))
                return (new RSG.Project.View.ProjectAddViewService());

            return (base.GetService(serviceType));
        }
        #endregion // Methods
    }

} // AssetBuildMonitor namespace
