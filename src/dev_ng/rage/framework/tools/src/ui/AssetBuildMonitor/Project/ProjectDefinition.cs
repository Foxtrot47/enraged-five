﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProjectDefinition.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace AssetBuildMonitor.Project
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Media.Imaging;
    using AssetBuildMonitor;
    using RSG.AssetBuildMonitor.ViewModel.Project;

    /// <summary>
    /// 
    /// </summary>
    internal sealed class ProjectDefinition : 
        RSG.Project.ViewModel.Definitions.ProjectDefinition
    {
        #region Properties
        /// <summary>
        /// Gets the description for this defined project.
        /// </summary>
        public override String Description
        {
            get { return Resources.StringTable.ProjectDescription; }
        }

        /// <summary>
        /// Gets the name that is used as the default value for a new instance of this
        /// definition.
        /// </summary>
        public override String DefaultName
        {
            get { return Resources.StringTable.ProjectDefaultName; }
        }

        /// <summary>
        /// Gets the extension used for this defined project.
        /// </summary>
        public override String Extension
        {
            get { return Resources.StringTable.ProjectExtension; }
        }

        /// <summary>
        /// Gets the string used as the filter in a open or save dialog for this defined
        /// project.
        /// </summary>
        public override String Filter
        {
            get { return Resources.StringTable.ProjectFilter; }
        }

        /// <summary>
        /// Gets the icon that is used to display this project inside the add new project
        /// dialog window.
        /// </summary>
        public override BitmapSource Icon
        {
            get { return Icons.ProjectIcon; }
        }

        /// <summary>
        /// Gets the name for this defined project.
        /// </summary>
        public override String Name
        {
            get { return Resources.StringTable.ProjectName; }
        }        
        
        /// <summary>
        /// Gets the filter to use in the Add Existing Item dialog window for this project.
        /// </summary>
        public override string NewItemFilter
        {
            get { return Resources.StringTable.NewItemFilter; }
        }

        /// <summary>
        /// Gets a byte array containing the data that is used to create a new instance of the
        /// defined project.
        /// </summary>
        public override byte[] Template
        {
            get
            {
                byte[] buffer = new byte[0];
                System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
                string name = "AssetBuildMonitor.Project.ProjectTemplate.xml";
                using (System.IO.Stream stream = assembly.GetManifestResourceStream(name))
                {
                    buffer = new byte[stream.Length];
                    stream.Read(buffer, 0, buffer.Length);
                }

                return buffer;
            }
        }

        /// <summary>
        /// Gets the type of this item to be displayed in the add dialog.
        /// </summary>
        public override String Type
        {
            get { return Resources.StringTable.ProjectType; }
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private IEnumerable<RSG.Interop.Bugstar.Organisation.User> _bugstarUsers;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="ProjectDefinition"/> class.
        /// </summary>
        public ProjectDefinition(IEnumerable<RSG.Interop.Bugstar.Organisation.User> bugstarUsers)
            : base()
        {
            this._bugstarUsers = bugstarUsers;
        }
        #endregion // Constructor(s)

        #region Methods
        /// <summary>
        /// Creates the project node that represents a standard metadata project inside the
        /// project explorer.
        /// </summary>
        /// <returns>
        /// A new <see cref="ProjectNode"/> object that represents a standard metadata project
        /// inside the project explorer.
        /// </returns>
        public override RSG.Project.ViewModel.ProjectNode CreateProjectNode()
        {
            return (new ProjectNode(this, this._bugstarUsers));
        }

        /// <summary>
        /// Gives the definition a opportunity to run a wizard before the project is created so
        /// that the user can set certain properties into the template before the file gets
        /// saved and replace all other variables.
        /// </summary>
        /// <param name="template">
        /// The template that is being used to create the project.
        /// </param>
        /// <param name="fullPath">
        /// The fullPath the new project is being saved to. So that relative paths can be
        /// determined.
        /// </param>
        /// <returns>
        /// A value indicating whether the wizard finished successfully. If false is returned
        /// the creation process is cancelled.
        /// </returns>
        /// <remarks>
        /// This method should modify the specified template with any additional properties
        /// from the wizard and replaced variables.
        /// </remarks>
        public override bool ReplaceVariables(ref byte[] template, String fullPath)
        {
            String text = System.Text.Encoding.UTF8.GetString(template);

            text = text.Replace("$(*NewGuid)", Guid.NewGuid().ToString("D"));
            text = text.Replace("$(Name)", System.IO.Path.GetFileNameWithoutExtension(fullPath));
            text = text.Replace("$(Environment.MachineName)", Environment.MachineName);

            template = System.Text.Encoding.UTF8.GetBytes(text);
            return (true);
        }
        #endregion // Methods
    }

} // AssetBuildMonitor.Project namespace
