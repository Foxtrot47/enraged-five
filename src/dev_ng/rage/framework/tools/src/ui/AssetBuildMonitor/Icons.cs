﻿//---------------------------------------------------------------------------------------------
// <copyright file="Icons.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace AssetBuildMonitor
{
    using System;
    using System.Windows.Media.Imaging;
    using RSG.Base.Extensions;

    /// <summary>
    /// Provides static properties that give access to the icons that are used at the Metadata
    /// Editor application level.
    /// </summary>
    internal static class Icons
    {
        #region Properties
        /// <summary>
        /// Gets the icon that is used for the standard metadata editor project type inside the
        /// add/create new project view.
        /// </summary>
        public static BitmapSource ProjectIcon
        {
            get
            {
                if (_projectIcon == null)
                {
                    lock (_syncRoot)
                    {
                        if (_projectIcon == null)
                        {
                            EnsureLoaded(ref _projectIcon, "ProjectIcon");
                        }
                    }
                }

                return _projectIcon;
            }
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// The private field used for the <see cref="ProjectIcon"/> property.
        /// </summary>
        private static BitmapSource _projectIcon;

        /// <summary>
        /// A generic object that provides thread safety access to the image sources.
        /// </summary>
        private static object _syncRoot = new object();
        #endregion // Member Data
        
        #region Methods
        /// <summary>
        /// Ensures that the specified image source is loaded with the specified resource name.
        /// </summary>
        /// <param name="source">
        /// The image source that should be loaded.
        /// </param>
        /// <param name="resourceName">
        /// The name of the resource the loaded image source should set as its source.
        /// </param>
        private static void EnsureLoaded(ref BitmapSource source, string resourceName)
        {
            if (source != null)
            {
                return;
            }

            string assemblyName = typeof(Icons).Assembly.GetName().Name;
            string path = "Resources/" + resourceName + ".png";
            string bitmapPath = "pack://application:,,,/{0};component/{1}";
            bitmapPath = bitmapPath.FormatInvariant(assemblyName, path);
            Uri uri = new Uri(bitmapPath, UriKind.RelativeOrAbsolute);

            source = new BitmapImage(uri);
            source.Freeze();
        }
        #endregion // Methods
    }

} // AssetBuildMonitor namespace
