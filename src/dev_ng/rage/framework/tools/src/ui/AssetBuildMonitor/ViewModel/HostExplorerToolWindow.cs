﻿//---------------------------------------------------------------------------------------------
// <copyright file="HostExplorerToolWindow.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace AssetBuildMonitor.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using RSG.AssetBuildMonitor.ViewModel;
    using RSG.Editor.Controls.Dock.ViewModel;
    using RSG.Project.ViewModel;

    /// <summary>
    /// 
    /// </summary>
    internal sealed class HostExplorerToolWindow : ToolWindowItem
    {
        #region Properties
        /// <summary>
        /// Gets the collection node that represents the root of the project system.
        /// </summary>
        public ProjectCollectionNode CollectionNode
        {
            get { return this._collectionNode; }
        }
        #endregion // Properties
        
        #region Member Data
        /// <summary>
        /// The private field used for the <see cref="CurrentProjectCollection"/> property.
        /// </summary>
        private ProjectCollectionNode _collectionNode;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public HostExplorerToolWindow(ProjectCollectionNode collectionNode, DockingPane parentPane)
            : base(parentPane, Resources.StringTable.ProjectExplorerTitle)
        {
            this._collectionNode = collectionNode;
            MainWindow mainWindow = Application.Current.MainWindow as MainWindow;
            this.Content = mainWindow.TryFindResource("HostExplorer");
        }
        #endregion // Constructor(s)
    }

} // AssetBuildMonitor.ViewModel
