﻿//---------------------------------------------------------------------------------------------
// <copyright file="CollectionDefinition.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace AssetBuildMonitor.Project
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// 
    /// </summary>
    internal sealed class CollectionDefinition :
        RSG.Project.ViewModel.Definitions.ProjectCollectionDefinition
    {
        #region Properties
        /// <summary>
        /// Gets the extension used for this defined collection.
        /// </summary>
        public override string Extension
        {
            get { return Resources.StringTable.CollectionExtension; }
        }

        /// <summary>
        /// Gets the string used as the filter in a open or save dialog for this defined
        /// collection.
        /// </summary>
        public override string Filter
        {
            get { return Resources.StringTable.CollectionFilter; }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="CollectionDefinition"/> class.
        /// </summary>
        public CollectionDefinition(IEnumerable<RSG.Interop.Bugstar.Organisation.User> bugstarUsers)
        {
            this.AddProjectDefinition(new ProjectDefinition(bugstarUsers));
        }
        #endregion // Constructor(s)
    }

} // AssetBuildMonitor.Project namespace
