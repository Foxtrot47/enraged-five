﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using RSG.Base.OS;
using RSG.Base.Configuration;

namespace AnimationPreviewer
{
    static class Program
    {
        //#region Constants
        ////private static readonly String OPTION_INPUTDIR = "inputdir";
        //#endregion

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            LongOption[] opts = new LongOption[] {};

            CommandOptions options = new CommandOptions(args, opts);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1(options));
        }
    }
}
