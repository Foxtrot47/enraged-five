﻿namespace AnimationPreviewer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private string filterTooltipText = "Filter uses wildcarg notion:\n\r * - Matches any sequence or string of characters.\n\r ? - Matches any one character.";

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.clbFolders = new System.Windows.Forms.CheckedListBox();
            this.btnPreview = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.populateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.populateSourceFoldersAppendToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.populateZipsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.populateZipFoldersAppendToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.populateClipFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.resetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deSelectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.btnCreateZips = new System.Windows.Forms.Button();
            this.btnBuildRPF = new System.Windows.Forms.Button();
            this.clbZips = new System.Windows.Forms.CheckedListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnFilterZip = new System.Windows.Forms.Button();
            this.txtFilterZip = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnFilterRPF = new System.Windows.Forms.Button();
            this.txtFilterRPF = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btnClipFilter = new System.Windows.Forms.Button();
            this.txtFilterClips = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnPreviewDirectory = new System.Windows.Forms.Button();
            this.btnBuildRPFS = new System.Windows.Forms.Button();
            this.btnCreateZip = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtZipFolder = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtSourceFolder = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cmdboxCompression = new System.Windows.Forms.ComboBox();
            this.btnApplyClips = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtCurrentClipCompression = new System.Windows.Forms.TextBox();
            this.btnSaveCompression = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtClipCompressed = new System.Windows.Forms.TextBox();
            this.txtRatio = new System.Windows.Forms.TextBox();
            this.txtClipUncompressed = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.clbClips = new System.Windows.Forms.CheckedListBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.txtLogging = new System.Windows.Forms.TextBox();
            this.clipToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.txtFilterToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.cmboDLC = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.cmboBranch = new System.Windows.Forms.ComboBox();
            this.menuStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // clbFolders
            // 
            this.clbFolders.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.clbFolders.CheckOnClick = true;
            this.clbFolders.FormattingEnabled = true;
            this.clbFolders.Location = new System.Drawing.Point(6, 53);
            this.clbFolders.Name = "clbFolders";
            this.clbFolders.Size = new System.Drawing.Size(575, 634);
            this.clbFolders.TabIndex = 0;
            // 
            // btnPreview
            // 
            this.btnPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPreview.Location = new System.Drawing.Point(401, 692);
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.Size = new System.Drawing.Size(87, 29);
            this.btnPreview.TabIndex = 1;
            this.btnPreview.Text = "Preview";
            this.btnPreview.UseVisualStyleBackColor = true;
            this.btnPreview.Click += new System.EventHandler(this.btnPreview_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(617, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.populateToolStripMenuItem,
            this.populateSourceFoldersAppendToolStripMenuItem,
            this.toolStripSeparator3,
            this.populateZipsToolStripMenuItem,
            this.populateZipFoldersAppendToolStripMenuItem,
            this.toolStripSeparator4,
            this.populateClipFilesToolStripMenuItem,
            this.toolStripSeparator1,
            this.resetToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // populateToolStripMenuItem
            // 
            this.populateToolStripMenuItem.Name = "populateToolStripMenuItem";
            this.populateToolStripMenuItem.Size = new System.Drawing.Size(254, 22);
            this.populateToolStripMenuItem.Text = "Populate Source Folders";
            this.populateToolStripMenuItem.Click += new System.EventHandler(this.populateToolStripMenuItem_Click);
            // 
            // populateSourceFoldersAppendToolStripMenuItem
            // 
            this.populateSourceFoldersAppendToolStripMenuItem.Name = "populateSourceFoldersAppendToolStripMenuItem";
            this.populateSourceFoldersAppendToolStripMenuItem.Size = new System.Drawing.Size(254, 22);
            this.populateSourceFoldersAppendToolStripMenuItem.Text = "Populate Source Folders (Append)";
            this.populateSourceFoldersAppendToolStripMenuItem.Click += new System.EventHandler(this.populateSourceFoldersAppendToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(251, 6);
            // 
            // populateZipsToolStripMenuItem
            // 
            this.populateZipsToolStripMenuItem.Name = "populateZipsToolStripMenuItem";
            this.populateZipsToolStripMenuItem.Size = new System.Drawing.Size(254, 22);
            this.populateZipsToolStripMenuItem.Text = "Populate Zip Folders";
            this.populateZipsToolStripMenuItem.Click += new System.EventHandler(this.populateZipsToolStripMenuItem_Click);
            // 
            // populateZipFoldersAppendToolStripMenuItem
            // 
            this.populateZipFoldersAppendToolStripMenuItem.Name = "populateZipFoldersAppendToolStripMenuItem";
            this.populateZipFoldersAppendToolStripMenuItem.Size = new System.Drawing.Size(254, 22);
            this.populateZipFoldersAppendToolStripMenuItem.Text = "Populate Zip Folders (Append)";
            this.populateZipFoldersAppendToolStripMenuItem.Click += new System.EventHandler(this.populateZipFoldersAppendToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(251, 6);
            // 
            // populateClipFilesToolStripMenuItem
            // 
            this.populateClipFilesToolStripMenuItem.Name = "populateClipFilesToolStripMenuItem";
            this.populateClipFilesToolStripMenuItem.Size = new System.Drawing.Size(254, 22);
            this.populateClipFilesToolStripMenuItem.Text = "Populate Clip Files";
            this.populateClipFilesToolStripMenuItem.Click += new System.EventHandler(this.populateClipFilesToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(251, 6);
            // 
            // resetToolStripMenuItem
            // 
            this.resetToolStripMenuItem.Name = "resetToolStripMenuItem";
            this.resetToolStripMenuItem.Size = new System.Drawing.Size(254, 22);
            this.resetToolStripMenuItem.Text = "Reset";
            this.resetToolStripMenuItem.Click += new System.EventHandler(this.resetToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(254, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectAllToolStripMenuItem,
            this.deSelectAllToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // selectAllToolStripMenuItem
            // 
            this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
            this.selectAllToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.selectAllToolStripMenuItem.Text = "Select All";
            this.selectAllToolStripMenuItem.Click += new System.EventHandler(this.selectAllToolStripMenuItem_Click);
            // 
            // deSelectAllToolStripMenuItem
            // 
            this.deSelectAllToolStripMenuItem.Name = "deSelectAllToolStripMenuItem";
            this.deSelectAllToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.deSelectAllToolStripMenuItem.Text = "Deselect All";
            this.deSelectAllToolStripMenuItem.Click += new System.EventHandler(this.deSelectAllToolStripMenuItem_Click);
            // 
            // btnCreateZips
            // 
            this.btnCreateZips.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCreateZips.Location = new System.Drawing.Point(494, 692);
            this.btnCreateZips.Name = "btnCreateZips";
            this.btnCreateZips.Size = new System.Drawing.Size(87, 29);
            this.btnCreateZips.TabIndex = 3;
            this.btnCreateZips.Text = "Create Zip(s)";
            this.btnCreateZips.UseVisualStyleBackColor = true;
            this.btnCreateZips.Click += new System.EventHandler(this.btnCreateZips_Click);
            // 
            // btnBuildRPF
            // 
            this.btnBuildRPF.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBuildRPF.Location = new System.Drawing.Point(494, 692);
            this.btnBuildRPF.Name = "btnBuildRPF";
            this.btnBuildRPF.Size = new System.Drawing.Size(87, 29);
            this.btnBuildRPF.TabIndex = 4;
            this.btnBuildRPF.Text = "Build RPF(s)";
            this.btnBuildRPF.UseVisualStyleBackColor = true;
            this.btnBuildRPF.Click += new System.EventHandler(this.btnBuildRPF_Click);
            // 
            // clbZips
            // 
            this.clbZips.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.clbZips.CheckOnClick = true;
            this.clbZips.FormattingEnabled = true;
            this.clbZips.Location = new System.Drawing.Point(6, 53);
            this.clbZips.Name = "clbZips";
            this.clbZips.Size = new System.Drawing.Size(575, 634);
            this.clbZips.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Source Folders:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "_________";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Zip Folders:";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(12, 58);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(595, 753);
            this.tabControl1.TabIndex = 9;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnFilterZip);
            this.tabPage1.Controls.Add(this.txtFilterZip);
            this.tabPage1.Controls.Add(this.clbFolders);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.btnCreateZips);
            this.tabPage1.Controls.Add(this.btnPreview);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(587, 727);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "ZIP Processing";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnFilterZip
            // 
            this.btnFilterZip.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFilterZip.Location = new System.Drawing.Point(474, 24);
            this.btnFilterZip.Name = "btnFilterZip";
            this.btnFilterZip.Size = new System.Drawing.Size(75, 23);
            this.btnFilterZip.TabIndex = 8;
            this.btnFilterZip.Text = "Filter";
            this.btnFilterZip.UseVisualStyleBackColor = true;
            this.btnFilterZip.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // txtFilterZip
            // 
            this.txtFilterZip.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFilterZip.Location = new System.Drawing.Point(6, 27);
            this.txtFilterZip.Name = "txtFilterZip";
            this.txtFilterZip.Size = new System.Drawing.Size(461, 20);
            this.txtFilterZip.TabIndex = 7;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnFilterRPF);
            this.tabPage2.Controls.Add(this.txtFilterRPF);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.btnBuildRPF);
            this.tabPage2.Controls.Add(this.clbZips);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(587, 727);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "RPF Processing";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnFilterRPF
            // 
            this.btnFilterRPF.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFilterRPF.Location = new System.Drawing.Point(474, 24);
            this.btnFilterRPF.Name = "btnFilterRPF";
            this.btnFilterRPF.Size = new System.Drawing.Size(75, 23);
            this.btnFilterRPF.TabIndex = 10;
            this.btnFilterRPF.Text = "Filter";
            this.btnFilterRPF.UseVisualStyleBackColor = true;
            this.btnFilterRPF.Click += new System.EventHandler(this.btnFilterRPF_Click);
            // 
            // txtFilterRPF
            // 
            this.txtFilterRPF.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFilterRPF.Location = new System.Drawing.Point(6, 27);
            this.txtFilterRPF.Name = "txtFilterRPF";
            this.txtFilterRPF.Size = new System.Drawing.Size(461, 20);
            this.txtFilterRPF.TabIndex = 9;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.btnClipFilter);
            this.tabPage3.Controls.Add(this.txtFilterClips);
            this.tabPage3.Controls.Add(this.groupBox4);
            this.tabPage3.Controls.Add(this.groupBox3);
            this.tabPage3.Controls.Add(this.groupBox2);
            this.tabPage3.Controls.Add(this.label4);
            this.tabPage3.Controls.Add(this.clbClips);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(587, 727);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "CLIP Editing";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // btnClipFilter
            // 
            this.btnClipFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClipFilter.Location = new System.Drawing.Point(474, 24);
            this.btnClipFilter.Name = "btnClipFilter";
            this.btnClipFilter.Size = new System.Drawing.Size(75, 23);
            this.btnClipFilter.TabIndex = 24;
            this.btnClipFilter.Text = "Filter";
            this.btnClipFilter.UseVisualStyleBackColor = true;
            this.btnClipFilter.Click += new System.EventHandler(this.btnClipFilter_Click);
            // 
            // txtFilterClips
            // 
            this.txtFilterClips.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFilterClips.Location = new System.Drawing.Point(6, 27);
            this.txtFilterClips.Name = "txtFilterClips";
            this.txtFilterClips.Size = new System.Drawing.Size(460, 20);
            this.txtFilterClips.TabIndex = 23;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnPreviewDirectory);
            this.groupBox4.Controls.Add(this.btnBuildRPFS);
            this.groupBox4.Controls.Add(this.btnCreateZip);
            this.groupBox4.Location = new System.Drawing.Point(444, 424);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(137, 106);
            this.groupBox4.TabIndex = 22;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Build Actions";
            // 
            // btnPreviewDirectory
            // 
            this.btnPreviewDirectory.Location = new System.Drawing.Point(6, 19);
            this.btnPreviewDirectory.Name = "btnPreviewDirectory";
            this.btnPreviewDirectory.Size = new System.Drawing.Size(114, 23);
            this.btnPreviewDirectory.TabIndex = 12;
            this.btnPreviewDirectory.Text = "Preview";
            this.btnPreviewDirectory.UseVisualStyleBackColor = true;
            this.btnPreviewDirectory.Click += new System.EventHandler(this.btnPreviewDirectory_Click);
            // 
            // btnBuildRPFS
            // 
            this.btnBuildRPFS.Location = new System.Drawing.Point(6, 77);
            this.btnBuildRPFS.Name = "btnBuildRPFS";
            this.btnBuildRPFS.Size = new System.Drawing.Size(114, 23);
            this.btnBuildRPFS.TabIndex = 14;
            this.btnBuildRPFS.Text = "Build RPF(s)";
            this.btnBuildRPFS.UseVisualStyleBackColor = true;
            this.btnBuildRPFS.Click += new System.EventHandler(this.btnBuildRPFS_Click);
            // 
            // btnCreateZip
            // 
            this.btnCreateZip.Location = new System.Drawing.Point(6, 48);
            this.btnCreateZip.Name = "btnCreateZip";
            this.btnCreateZip.Size = new System.Drawing.Size(114, 23);
            this.btnCreateZip.TabIndex = 13;
            this.btnCreateZip.Text = "Create Zip(s)";
            this.btnCreateZip.UseVisualStyleBackColor = true;
            this.btnCreateZip.Click += new System.EventHandler(this.btnCreateZip_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.txtZipFolder);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.txtSourceFolder);
            this.groupBox3.Location = new System.Drawing.Point(9, 423);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(428, 107);
            this.groupBox3.TabIndex = 21;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Directories";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(4, 56);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(59, 13);
            this.label11.TabIndex = 3;
            this.label11.Text = "Zip Folders";
            // 
            // txtZipFolder
            // 
            this.txtZipFolder.Location = new System.Drawing.Point(6, 72);
            this.txtZipFolder.Name = "txtZipFolder";
            this.txtZipFolder.Size = new System.Drawing.Size(412, 20);
            this.txtZipFolder.TabIndex = 2;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(4, 16);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(78, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Source Folders";
            // 
            // txtSourceFolder
            // 
            this.txtSourceFolder.Location = new System.Drawing.Point(6, 32);
            this.txtSourceFolder.Name = "txtSourceFolder";
            this.txtSourceFolder.Size = new System.Drawing.Size(412, 20);
            this.txtSourceFolder.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cmdboxCompression);
            this.groupBox2.Controls.Add(this.btnApplyClips);
            this.groupBox2.Controls.Add(this.groupBox1);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Location = new System.Drawing.Point(8, 536);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(573, 191);
            this.groupBox2.TabIndex = 20;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Compression Options";
            // 
            // cmdboxCompression
            // 
            this.cmdboxCompression.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmdboxCompression.FormattingEnabled = true;
            this.cmdboxCompression.Location = new System.Drawing.Point(6, 39);
            this.cmdboxCompression.Name = "cmdboxCompression";
            this.cmdboxCompression.Size = new System.Drawing.Size(543, 21);
            this.cmdboxCompression.TabIndex = 10;
            // 
            // btnApplyClips
            // 
            this.btnApplyClips.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnApplyClips.Location = new System.Drawing.Point(480, 150);
            this.btnApplyClips.Name = "btnApplyClips";
            this.btnApplyClips.Size = new System.Drawing.Size(87, 29);
            this.btnApplyClips.TabIndex = 11;
            this.btnApplyClips.Text = "Apply";
            this.btnApplyClips.UseVisualStyleBackColor = true;
            this.btnApplyClips.Click += new System.EventHandler(this.btnApplyClips_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txtCurrentClipCompression);
            this.groupBox1.Controls.Add(this.btnSaveCompression);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txtClipCompressed);
            this.groupBox1.Controls.Add(this.txtRatio);
            this.groupBox1.Controls.Add(this.txtClipUncompressed);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Location = new System.Drawing.Point(6, 66);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(422, 113);
            this.groupBox1.TabIndex = 19;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Clip Dictionary Compression";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 68);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(171, 13);
            this.label9.TabIndex = 22;
            this.label9.Text = "Current Clip Compression Template";
            // 
            // txtCurrentClipCompression
            // 
            this.txtCurrentClipCompression.Location = new System.Drawing.Point(3, 87);
            this.txtCurrentClipCompression.Name = "txtCurrentClipCompression";
            this.txtCurrentClipCompression.Size = new System.Drawing.Size(313, 20);
            this.txtCurrentClipCompression.TabIndex = 21;
            // 
            // btnSaveCompression
            // 
            this.btnSaveCompression.Location = new System.Drawing.Point(325, 26);
            this.btnSaveCompression.Name = "btnSaveCompression";
            this.btnSaveCompression.Size = new System.Drawing.Size(87, 29);
            this.btnSaveCompression.TabIndex = 20;
            this.btnSaveCompression.Text = "Save CSV";
            this.btnSaveCompression.UseVisualStyleBackColor = true;
            this.btnSaveCompression.Click += new System.EventHandler(this.btnSaveCompression_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Compressed";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(219, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(32, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Ratio";
            // 
            // txtClipCompressed
            // 
            this.txtClipCompressed.Location = new System.Drawing.Point(6, 35);
            this.txtClipCompressed.Name = "txtClipCompressed";
            this.txtClipCompressed.Size = new System.Drawing.Size(100, 20);
            this.txtClipCompressed.TabIndex = 13;
            // 
            // txtRatio
            // 
            this.txtRatio.Location = new System.Drawing.Point(219, 35);
            this.txtRatio.Name = "txtRatio";
            this.txtRatio.Size = new System.Drawing.Size(100, 20);
            this.txtRatio.TabIndex = 17;
            // 
            // txtClipUncompressed
            // 
            this.txtClipUncompressed.Location = new System.Drawing.Point(112, 35);
            this.txtClipUncompressed.Name = "txtClipUncompressed";
            this.txtClipUncompressed.Size = new System.Drawing.Size(100, 20);
            this.txtClipUncompressed.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(109, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Uncompressed";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(122, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Compression Templates:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Clip Files:";
            // 
            // clbClips
            // 
            this.clbClips.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.clbClips.CheckOnClick = true;
            this.clbClips.FormattingEnabled = true;
            this.clbClips.HorizontalScrollbar = true;
            this.clbClips.Location = new System.Drawing.Point(6, 53);
            this.clbClips.Name = "clbClips";
            this.clbClips.Size = new System.Drawing.Size(575, 349);
            this.clbClips.TabIndex = 1;
            this.clbClips.SelectedIndexChanged += new System.EventHandler(this.clbClips_SelectedIndexChanged);
            this.clbClips.MouseMove += new System.Windows.Forms.MouseEventHandler(this.clbClips_MouseMove);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.txtLogging);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(587, 727);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Log";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // txtLogging
            // 
            this.txtLogging.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLogging.Location = new System.Drawing.Point(6, 27);
            this.txtLogging.Multiline = true;
            this.txtLogging.Name = "txtLogging";
            this.txtLogging.ReadOnly = true;
            this.txtLogging.Size = new System.Drawing.Size(575, 687);
            this.txtLogging.TabIndex = 0;
            // 
            // txtFilterToolTip
            // 
            this.txtFilterToolTip.AutoPopDelay = 5000;
            this.txtFilterToolTip.InitialDelay = 500;
            this.txtFilterToolTip.ReshowDelay = 500;
            this.txtFilterToolTip.ShowAlways = true;
            // 
            // cmboDLC
            // 
            this.cmboDLC.FormattingEnabled = true;
            this.cmboDLC.Location = new System.Drawing.Point(58, 31);
            this.cmboDLC.Name = "cmboDLC";
            this.cmboDLC.Size = new System.Drawing.Size(186, 21);
            this.cmboDLC.TabIndex = 10;
            this.cmboDLC.SelectedIndexChanged += new System.EventHandler(this.cmboDLC_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(12, 34);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(40, 13);
            this.label12.TabIndex = 11;
            this.label12.Text = "Config:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(250, 34);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(44, 13);
            this.label13.TabIndex = 12;
            this.label13.Text = "Branch:";
            // 
            // cmboBranch
            // 
            this.cmboBranch.FormattingEnabled = true;
            this.cmboBranch.Location = new System.Drawing.Point(297, 31);
            this.cmboBranch.Name = "cmboBranch";
            this.cmboBranch.Size = new System.Drawing.Size(80, 21);
            this.cmboBranch.TabIndex = 13;
            this.cmboBranch.SelectedIndexChanged += new System.EventHandler(this.cmboBranch_SelectedIndexChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(617, 823);
            this.Controls.Add(this.cmboBranch);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.cmboDLC);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.label2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(633, 861);
            this.Name = "Form1";
            this.Text = "Animation Processor";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckedListBox clbFolders;
        private System.Windows.Forms.Button btnPreview;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem populateToolStripMenuItem;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.ToolStripMenuItem resetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deSelectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem;
        private System.Windows.Forms.Button btnCreateZips;
        private System.Windows.Forms.Button btnBuildRPF;
        private System.Windows.Forms.CheckedListBox clbZips;
        private System.Windows.Forms.ToolStripMenuItem populateZipsToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem populateSourceFoldersAppendToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.CheckedListBox clbClips;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmdboxCompression;
        private System.Windows.Forms.Button btnApplyClips;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ToolStripMenuItem populateClipFilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TextBox txtLogging;
        private System.Windows.Forms.ToolStripMenuItem populateZipFoldersAppendToolStripMenuItem;
        private System.Windows.Forms.Button btnFilterZip;
        private System.Windows.Forms.TextBox txtFilterZip;
        private System.Windows.Forms.Button btnFilterRPF;
        private System.Windows.Forms.TextBox txtFilterRPF;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtClipUncompressed;
        private System.Windows.Forms.TextBox txtClipCompressed;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtRatio;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSaveCompression;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtCurrentClipCompression;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtZipFolder;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtSourceFolder;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnPreviewDirectory;
        private System.Windows.Forms.Button btnBuildRPFS;
        private System.Windows.Forms.Button btnCreateZip;
        private System.Windows.Forms.ToolTip clipToolTip;
        private System.Windows.Forms.Button btnClipFilter;
        private System.Windows.Forms.TextBox txtFilterClips;
        private System.Windows.Forms.ToolTip txtFilterToolTip;
        private System.Windows.Forms.ComboBox cmboDLC;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cmboBranch;
    }
}

