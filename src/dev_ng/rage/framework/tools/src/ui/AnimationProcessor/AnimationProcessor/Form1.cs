﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Xml;
using XGE = RSG.Interop.Incredibuild.XGE;
using RSG.ManagedRage.ClipAnimation;
using RSG.Base.Configuration;
using RSG.Base.OS;
using System.Collections;

namespace AnimationPreviewer
{
    public partial class Form1 : Form
    {
        #region Constants
        /// <summary>
        /// 
        /// </summary>
        static readonly string CONVERT_EXE = Environment.GetEnvironmentVariable("RS_TOOLSROOT") + @"\ironlib\lib\RSG.Pipeline.Convert.exe";

        /// <summary>
        /// 
        /// </summary>
        static readonly string PATH_PLATFORM_CLIP_RPF = Environment.GetEnvironmentVariable("RS_BUILDBRANCH") + @"\<platform>\anim\ingame\";

        /// <summary>
        /// 
        /// </summary>
        static readonly string PATH_CLIP_METADATA_DICTIONARY = Environment.GetEnvironmentVariable("RS_BUILDBRANCH") + @"\common\data\anim\clip_dictionary_metadata\";

        /// <summary>
        /// 
        /// </summary>
        static readonly string EXE_CLIP_DUMP = Environment.GetEnvironmentVariable("RS_TOOLSROOT") + @"\bin\anim\clipdump.exe";
        #endregion // Constants

        private string strPreviousFolder = String.Empty;
        List<String> lstSourceFolders = new List<string>();
        List<String> lstZipFolders = new List<string>();
        List<String> lstClipFolders = new List<string>();

        static string PATH_RAW_ZIPS = String.Empty;
        static string PATH_RAW_DATA = String.Empty;
        static string PATH_AP3_TEMP_FILE = String.Empty;

        private int previousToolTipIndex = -1;

        public class CompressionData
        {
            public int uncompressedDataSize;
            public int compressedDataSize;

            public CompressionData(int uncompressed, int compressed)
            {
                uncompressedDataSize = uncompressed;
                compressedDataSize = compressed;
            }
        }

        public class PreviewData
        {
            public string rootAssetPath;
            public string dictionaryName;

            public PreviewData(string assetPath, string dictName)
            {
                rootAssetPath = assetPath;
                dictionaryName = dictName;
            }
        }

        private void SetPaths(CommandOptions options)
        {
            PATH_RAW_ZIPS = String.Format("{0}\\anim\\ingame", options.Branch.Export);
            PATH_RAW_DATA = String.Format("{0}\\anim\\export_mb", options.Branch.Art);
            PATH_AP3_TEMP_FILE = String.Format("{0}\\temp", options.Branch.Export);

            // Build up the default paths
            txtSourceFolder.Text = PATH_RAW_DATA;
            txtZipFolder.Text = PATH_RAW_ZIPS;
        }

        private void SetBranches(CommandOptions options)
        {
            foreach (KeyValuePair<string, IBranch> branch in options.CoreProject.Branches)
            {
                cmboBranch.Items.Add(branch.Key);
            }

            if (cmboBranch.Items.Count > 0)
                cmboBranch.SelectedIndex = 0;
        }

        public Form1(CommandOptions options)
        {
            InitializeComponent();

            SetBranches(options);

            cmboDLC_SelectedIndexChanged(null, null);

            this.Text = this.Text + " - Project: " + options.Branch.Project.Name.ToUpper();

            cmboDLC.Items.Add("root");
            foreach (KeyValuePair<string, IProject> entry in options.Config.DLCProjects)
            {
                cmboDLC.Items.Add(entry.Key);
            }
            cmboDLC.SelectedIndex = 0;

            MClip.Init();
        }

        private bool ContainsAnimations(string dir)
        {
            return (Directory.GetFiles(dir, "*.anim").Length > 0);
        }

        private bool ContainsZips(string dir)
        {
            return (Directory.GetFiles(dir, "*.icd.zip").Length > 0);
        }

        private void AddFolderToUI(string dir, CheckedListBox clbControl)
        {
            if (ContainsAnimations(dir) || ContainsZips(dir) )
                clbControl.Items.Add(dir);
        }

        private void ListFolders(string dir, CheckedListBox clbControl)
        {
            string[] dirs = Directory.GetDirectories(dir);

            for (int i = 0; i < dirs.Length; ++i)
            {
                AddFolderToUI(dirs[i], clbControl);
                ListFolders(dirs[i], clbControl);
            }
        }

        private bool BuildPreviewDictionary(string strDictionaryName, string strRootDir)
        {
            string process = CONVERT_EXE;
            string cmdline = " --preview --branch" + cmboBranch.SelectedItem.ToString() + " " + Path.Combine(PATH_RAW_ZIPS, strRootDir, strDictionaryName + ".icd.zip");
            LogMsg(process + " " + cmdline);
            Process P = Process.Start(process, cmdline);
            P.WaitForExit();
            return P.ExitCode == 0;
        }

        private XmlDocument BuildAP3XmlFile()
        {
            XmlDocument ap3Document = new XmlDocument();
            XmlElement rootElement = ap3Document.CreateElement("Assets");
            ap3Document.AppendChild(rootElement);
            return ap3Document;
        }

        private void AddDictionaryToAP3File(XmlDocument xmlDoc, string strDirectory, string strRootDir, string strDictionaryName)
        {
            XmlElement element = xmlDoc.CreateElement("ZipArchive");
            XmlAttribute pathAttribute = xmlDoc.CreateAttribute("path");

            string outputFile = Path.Combine(PATH_RAW_ZIPS, strRootDir, strDictionaryName + ".icd.zip");
            pathAttribute.Value = outputFile;

            element.Attributes.Append(pathAttribute);

            XmlElement directoryElement = xmlDoc.CreateElement("Directory");
            XmlAttribute directoryPathAttribute = xmlDoc.CreateAttribute("path");
            directoryPathAttribute.Value = strDirectory;
            directoryElement.Attributes.Append(directoryPathAttribute);

            element.AppendChild(directoryElement);

            xmlDoc.DocumentElement.AppendChild(element);
        }

        private bool BuildAP3Zip(string ap3Filename)
        {
            string process = CONVERT_EXE;
            string cmdline = " --rebuild --branch " + cmboBranch.SelectedItem.ToString() + " " + ap3Filename;
            LogMsg(process + " " + cmdline);
            Process P = Process.Start(process, cmdline);
            P.WaitForExit();
            return P.ExitCode == 0;
        }

        private void BuildXGEPreview(List<string> dictionaries)
        {
            string process = CONVERT_EXE;
            string cmdline = " --preview --branch " + cmboBranch.SelectedItem.ToString() + " ";

            string arguments = String.Join(" ", dictionaries);
            cmdline += arguments;

            LogMsg(process + " " + cmdline);
            Process P = Process.Start(process, cmdline);
            P.WaitForExit();
        }

        private void BuildXGEClipXML(List<string> clips)
        {
            for (int i = 0; i < clips.Count; i++)
            {
                if (!File.Exists(clips[i]) || IsFileLocked(clips[i]))
                {
                    MessageBox.Show(string.Format("Cannot proceed with clip processing: {0} is read only.\nPlease check out from perforce.", clips[i]));
                    return;
                }
            }

            XGE.Environment clipEnv = new XGE.Environment("clip_env");
            XGE.Project proj = new XGE.Project("Clip Processing");
            proj.DefaultEnvironment = clipEnv;
            string toolArguments = "-clip=$(SourcePath) -out=$(SourcePath) -editproperty=Compressionfile_DO_NOT_RESOURCE,Compressionfile_DO_NOT_RESOURCE," + cmdboxCompression.SelectedItem.ToString();
            XGE.Tool clipEditTool = new XGE.Tool("clipEdit", Path.Combine(Environment.GetEnvironmentVariable("RS_TOOLSROOT"), "bin\\anim\\clipedit.exe"), toolArguments);
            clipEnv.Tools.Add(clipEditTool);

            XGE.ITask[] taskArray = new XGE.ITask[clips.Count];

            for (int i = 0; i < clips.Count; i++)
            {
                string clipFilename = Path.GetFileNameWithoutExtension(clips[i]);
                XGE.TaskJob clipJob = new XGE.TaskJob(clipFilename, clips[i]);
                taskArray[i] = clipJob;
            }

            XGE.TaskGroup clipGroup = new XGE.TaskGroup("task_clipEdit", taskArray, null, clipEditTool);

            string xgeFilename = PATH_AP3_TEMP_FILE + "XGEClipEdit.xml";
            string logFilename = PATH_AP3_TEMP_FILE + "XGEClipLogFile.txt";

            proj.Tasks.Add(clipGroup);
            proj.Write(xgeFilename);

            XGE.XGE.Start("Update Clip Files", xgeFilename, logFilename);
        }

        private bool IsFileLocked(string filename)
        {
            FileStream stream = null;

            try
            {
                stream = File.OpenWrite(filename);
            }
            catch (IOException)
            {
                return true;
            }
            catch (UnauthorizedAccessException)
            {
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            return false;
        }

        private bool CheckClipRPFFileForWriting(string clipPath)
        {
            List<string> platformList = new List<string>() {"xbox360", "ps3", "pc", "x64"};

            bool writableFile = false;

            for (int i = 0; i < platformList.Count; i++)
            {
                string animFolder = PATH_PLATFORM_CLIP_RPF.Replace("<platform>", platformList[i]);
                string[] clipPathSplit = clipPath.Split('\\');

                // Make sure we have a valid file.
                if (clipPathSplit.Length == 0)
                    continue;

                string clipFile = clipPathSplit[clipPathSplit.Length - 1];
                string platformClipFile = Path.Combine(animFolder, string.Format("clip_{0}.rpf", clipFile));

                if (!File.Exists(platformClipFile))
                {
                    writableFile = true;
                }
                else
                {
                    // Warn the user what clip file is currently locked.
                    if (IsFileLocked(platformClipFile))
                        MessageBox.Show(string.Format("Clip RPF file ({0}) is currently locked. Cannot build RPF.", platformClipFile));

                    writableFile = !IsFileLocked(platformClipFile);
                }

                // As soon as we find 1 platform specific file that is locked, bail out!
                if (!writableFile)
                    break;
            }

            return writableFile;
        }

        private string GetClipMetaDataFile(string clipPath)
        {
            string strippedPath = clipPath.ToLower().Replace(PATH_RAW_DATA, "");
            string[] clipPathSplit = strippedPath.Split('\\');
            string dictionaryName = clipPathSplit[0];
            string clipMetadataFile = Path.Combine(PATH_CLIP_METADATA_DICTIONARY, string.Format("clip_{0}.xml", dictionaryName));

            return clipMetadataFile;
        }

        private bool ParseMetadataFileForCompression(string clipPath, out int compressedSize, out int uncompressedSize)
        {
            string metaDataFilename = GetClipMetaDataFile(clipPath);
            string clipDictionary = GenerateDictionaryName(clipPath);
            if (clipDictionary == String.Empty)
            {
                compressedSize = 0;
                uncompressedSize = 0;
                return false;
            }
            string clipName = clipDictionary + ".icd.zip";

            if (File.Exists(metaDataFilename))
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(metaDataFilename);

                if (xmlDoc.DocumentElement.Name == "fwClipRpfBuildMetadata")
                {
                    for (int i = 0; i < xmlDoc.DocumentElement.ChildNodes.Count; i++)
                    {
                        XmlNode node = xmlDoc.DocumentElement.ChildNodes[i];
                        if (node.Name.ToLower() == "dictionaries")
                        {
                            for (int clip = 0; clip < node.ChildNodes.Count; clip++)
                            {
                                XmlNode itemNode = node.ChildNodes[clip];
                                string dictionaryName = itemNode.Attributes["key"].Value;
                                if (dictionaryName.ToLower() == clipName.ToLower())
                                {
                                    int uncompressedSizeValue = 0;
                                    int compressedSizeValue = 0;

                                    for (int sizes = 0; sizes < itemNode.ChildNodes.Count; sizes++)
                                    {
                                        XmlNode sizeNode = itemNode.ChildNodes[sizes];

                                        if (sizeNode.Name == "sizeBefore")
                                            int.TryParse(sizeNode.Attributes["value"].Value, out uncompressedSizeValue);
                                        else if (sizeNode.Name == "sizeAfter")
                                            int.TryParse(sizeNode.Attributes["value"].Value, out compressedSizeValue);
                                    }

                                    uncompressedSize = uncompressedSizeValue;
                                    compressedSize = compressedSizeValue;

                                    return true;
                                }
                            }
                        }
                    }
                }

                compressedSize = 0;
                uncompressedSize = 0;
            }
            else
            {
                compressedSize = 0;
                uncompressedSize = 0;
            }

            return true;
        }

        private void SaveCompressionData()
        {
            if (clbClips.Items.Count > 0)
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.DefaultExt = ".csv";
                sfd.AddExtension = true;

                if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    StreamWriter compressionFile = new StreamWriter(sfd.FileName);
                    Dictionary<string, CompressionData> clipFileCompression = new Dictionary<string, CompressionData>();

                    for (int i = 0; i < clbClips.Items.Count; i++)
                    {
                        int compressed = 0;
                        int uncompressed = 0;

                        string dictionaryName = GenerateDictionaryName(clbClips.Items[i].ToString());
                        if (dictionaryName == String.Empty) return;

                        // Already have an entry for this dictionary?
                        if (!clipFileCompression.ContainsKey(dictionaryName))
                        {
                            if (!ParseMetadataFileForCompression(clbClips.Items[i].ToString(), out compressed, out uncompressed)) return;
                            CompressionData compressionData = new CompressionData(uncompressed, compressed);
                            clipFileCompression.Add(dictionaryName, compressionData);
                        }
                    }

                    foreach (KeyValuePair<string, CompressionData> kvp in clipFileCompression)
                    {
                        CompressionData data = kvp.Value;
                        string dictionaryEntry = string.Format("{0}, {1}, {2}", kvp.Key, data.compressedDataSize, data.uncompressedDataSize);
                        compressionFile.WriteLine(dictionaryEntry);
                    }

                    compressionFile.Close();
                }
            }
        }

        private bool BuildRPF(List<string> lstZips)
        {
            // process all the folders in one go
            string strArgs = "--branch " + cmboBranch.SelectedItem.ToString() + " ";
            for (int i = 0; i < lstZips.Count; ++i)
            {
                // Make sure we can update the clip file before actually building.
                if (!CheckClipRPFFileForWriting(lstZips[i]))
                    return false;
                strArgs += lstZips[i] + "\\ ";
            }

            string process = CONVERT_EXE;
            string cmdline = strArgs;
            LogMsg(process + " " + cmdline);
            Process P = Process.Start(process, cmdline);
            P.WaitForExit();
            return P.ExitCode == 0;
        }

        private string GetRootAssetPath(string strAnimationPath)
        {
            string rootAssetPath = PATH_RAW_DATA;
            string[] rootAssetPathTokens;
	        string[] outputPathTokens;

            rootAssetPathTokens = rootAssetPath.Split(new char[] { '\\' }, StringSplitOptions.RemoveEmptyEntries);
            outputPathTokens = strAnimationPath.Split(new char[] { '\\' }, StringSplitOptions.RemoveEmptyEntries);

            return outputPathTokens[rootAssetPathTokens.Length];
        }

        private string GenerateDictionaryName(string strAnimationPath)
        {
            string rootAssetPath = PATH_RAW_DATA.ToLower();
	        string[] outputPathTokens;
            string cAnimRelativePath = "";

            strAnimationPath = strAnimationPath.ToLower();

            if (strAnimationPath.StartsWith(rootAssetPath,StringComparison.OrdinalIgnoreCase))
            {
                strAnimationPath = strAnimationPath.Replace(rootAssetPath, "");
            }
            else
            {
                String msg = String.Format("Output path '{0}' is not under expected path of '{1}'.", strAnimationPath, rootAssetPath);
                MessageBox.Show(null, msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error );
                return String.Empty;
            }

	        outputPathTokens = strAnimationPath.Split( '\\' );

            foreach (string entry in outputPathTokens)
            {
                cAnimRelativePath += entry;
            }

            return cAnimRelativePath;	
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
            // Don't process anything is there is nothing to process.
            if (clbFolders.CheckedItems.Count == 0)
                return;

            LogReset();

            XmlDocument ap3Doc = BuildAP3XmlFile();
            List<string> previewDictionaries = new List<string>();

            for (int i = 0; i < clbFolders.CheckedItems.Count; ++i)
            {
                string strAnimationFolder = (string)clbFolders.CheckedItems[i] + "\\";
                string strDictionaryName = GenerateDictionaryName(strAnimationFolder);
                string strRootAssetPath = GetRootAssetPath(strAnimationFolder);
                string previewDictionary = Path.Combine(PATH_RAW_ZIPS, strRootAssetPath, strDictionaryName + ".icd.zip");
                previewDictionaries.Add(previewDictionary);
                AddDictionaryToAP3File(ap3Doc, strAnimationFolder, strRootAssetPath, strDictionaryName);
            }

            if (!Directory.Exists(PATH_RAW_ZIPS))
                Directory.CreateDirectory(PATH_RAW_ZIPS);

            string ap3AssetFilename = PATH_RAW_ZIPS + "\\master_icd_list.xml";

            ap3Doc.Save(ap3AssetFilename);

            if (BuildAP3Zip(ap3AssetFilename))
            {
                BuildXGEPreview(previewDictionaries);
            }
        }

        private void Populate(string strRoot, bool bAddRoot, CheckedListBox clbControl)
        {
            string[] dirs = Directory.GetDirectories(strRoot);

            if (bAddRoot)
            {
                AddFolderToUI(strRoot, clbControl);
            }

            for (int i = 0; i < dirs.Length; ++i)
            {
                AddFolderToUI(dirs[i], clbControl);
                ListFolders(dirs[i], clbControl);
            }
        }

        private void PopulateFiles(string strRoot)
        {
            string[] files = Directory.GetFiles(strRoot, "*.clip");

            for (int i = 0; i < files.Length; ++i)
            {
                clbClips.Items.Add(files[i]);
            }

            string[] dirs = Directory.GetDirectories(strRoot);

            for (int i = 0; i < dirs.Length; ++i)
            {
                PopulateFiles(dirs[i]);
            }
        }

        private void populateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.RootFolder = Environment.SpecialFolder.MyComputer;
            if (strPreviousFolder == String.Empty)
                folderBrowserDialog1.SelectedPath = PATH_RAW_DATA;
            else
                folderBrowserDialog1.SelectedPath = strPreviousFolder;

            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                clbFolders.Items.Clear();
                Populate(folderBrowserDialog1.SelectedPath, true, clbFolders);

                lstSourceFolders = clbFolders.Items.Cast<String>().ToList();

                strPreviousFolder = folderBrowserDialog1.SelectedPath;
            }
        }

        private void populateAppendToolStripMenuItem_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.RootFolder = Environment.SpecialFolder.MyComputer;
            folderBrowserDialog1.SelectedPath = PATH_RAW_DATA;

            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                Populate(folderBrowserDialog1.SelectedPath, true, clbFolders);

                lstSourceFolders = clbFolders.Items.Cast<String>().ToList();
            }
        }

        private void resetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            clbFolders.Items.Clear();
            clbZips.Items.Clear();
            clbClips.Items.Clear();
            LogReset();
        }

        private void SetControlCheckedState(CheckState state)
        {
            if (tabControl1.SelectedTab.Text == "ZIP Processing")
            {
                for (int i = 0; i < clbFolders.Items.Count; ++i)
                {
                    clbFolders.SetItemCheckState(i, state);
                }
            }
            else if (tabControl1.SelectedTab.Text == "RPF Processing")
            {
                for (int i = 0; i < clbZips.Items.Count; ++i)
                {
                    clbZips.SetItemCheckState(i, state);
                }
            }
            else if (tabControl1.SelectedTab.Text == "CLIP Editing")
            {
                for (int i = 0; i < clbClips.Items.Count; ++i)
                {
                    clbClips.SetItemCheckState(i, state);
                }
            }
        }

        private void deSelectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetControlCheckedState(CheckState.Unchecked);
        }

        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetControlCheckedState(CheckState.Checked);
        }

        private void btnCreateZips_Click(object sender, EventArgs e)
        {
            // Don't process anything is there is nothing to process.
            if (clbFolders.CheckedItems.Count == 0)
                return;

            LogReset();

            XmlDocument ap3Doc = BuildAP3XmlFile();

            for (int i = 0; i < clbFolders.CheckedItems.Count; ++i)
            {
                string strAnimationFolder = (string)clbFolders.CheckedItems[i] + "\\";
                string strDictionaryName = GenerateDictionaryName(strAnimationFolder);
                if (strDictionaryName == String.Empty) return;
                string strRootPath = GetRootAssetPath(strAnimationFolder);
                AddDictionaryToAP3File(ap3Doc, strAnimationFolder, strRootPath, strDictionaryName);
            }

            if (!Directory.Exists(PATH_RAW_ZIPS))
                Directory.CreateDirectory(PATH_RAW_ZIPS);

            string ap3AssetFilename = PATH_RAW_ZIPS + "\\master_icd_list.xml";

            ap3Doc.Save(ap3AssetFilename);
            BuildAP3Zip(ap3AssetFilename);
        }

        private void populateZipsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.RootFolder = Environment.SpecialFolder.MyComputer;
            folderBrowserDialog1.SelectedPath = PATH_RAW_ZIPS;

            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                clbZips.Items.Clear();
                Populate(folderBrowserDialog1.SelectedPath, true, clbZips);

                lstZipFolders = clbZips.Items.Cast<String>().ToList();
            }
        }

        private void btnBuildRPF_Click(object sender, EventArgs e)
        {
            LogReset();

            if (clbZips.CheckedItems.Count == 0) return;

            List<string> rpfFiles = new List<string>();

            for (int i = 0; i < clbZips.CheckedItems.Count; i++)
                rpfFiles.Add(clbZips.CheckedItems[i].ToString());

            BuildRPF(rpfFiles);
        }

        private void deselectAllZipFoldersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < clbZips.Items.Count; ++i)
            {
                clbZips.SetItemCheckState(i, CheckState.Unchecked);
            }
        }

        private void selectAllZipFoldersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < clbZips.Items.Count; ++i)
            {
                clbZips.SetItemCheckState(i, CheckState.Checked);
            }
        }

        private void populateSourceFoldersAppendToolStripMenuItem_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.RootFolder = Environment.SpecialFolder.MyComputer;
            if (strPreviousFolder == String.Empty)
                folderBrowserDialog1.SelectedPath = PATH_RAW_DATA;
            else
                folderBrowserDialog1.SelectedPath = strPreviousFolder;

            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                Populate(folderBrowserDialog1.SelectedPath, true, clbFolders);

                strPreviousFolder = folderBrowserDialog1.SelectedPath;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string strCompressionFolder = Environment.GetEnvironmentVariable("RS_TOOLSROOT") + "\\etc\\config\\anim\\compression_templates";

            string[] compressionFiles = Directory.GetFiles(strCompressionFolder,"*.txt");

            for(int i=0; i < compressionFiles.Length; ++i)
            {
                cmdboxCompression.Items.Add(Path.GetFileName(compressionFiles[i]));
            }
        }

        private void btnApplyClips_Click(object sender, EventArgs e)
        {
            LogReset();

            List<string> updateClips = new List<string>();

            for (int i = 0; i < clbClips.CheckedItems.Count; ++i)
            {
                updateClips.Add((string)clbClips.CheckedItems[i]);
            }

            BuildXGEClipXML(updateClips);
        }

        private void LogReset()
        {
            txtLogging.Text = String.Empty;
        }

        private void LogMsg(string strLog)
        {
            txtLogging.Text += strLog + "\r\n\r\n";
        }

        private void populateClipFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.RootFolder = Environment.SpecialFolder.MyComputer;
            if (strPreviousFolder == String.Empty)
                folderBrowserDialog1.SelectedPath = PATH_RAW_DATA;
            else
                folderBrowserDialog1.SelectedPath = strPreviousFolder;

            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                clbClips.Items.Clear();
                PopulateFiles(folderBrowserDialog1.SelectedPath);
                lstClipFolders = clbClips.Items.Cast<String>().ToList();
                strPreviousFolder = folderBrowserDialog1.SelectedPath;
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void populateZipFoldersAppendToolStripMenuItem_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.RootFolder = Environment.SpecialFolder.MyComputer;
            folderBrowserDialog1.SelectedPath = PATH_RAW_ZIPS;

            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                Populate(folderBrowserDialog1.SelectedPath, true, clbZips);

                lstZipFolders = clbZips.Items.Cast<String>().ToList();
            }
        }

        public static string WildcardToRegex(string pattern)
        {
            return "^" + Regex.Escape(pattern).
            Replace("\\*", ".*").
            Replace("\\?", ".") + "$";
        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            string strRegex = WildcardToRegex(txtFilterZip.Text);

            clbFolders.Items.Clear();

            for (int i = 0; i < lstSourceFolders.Count; ++i)
            {
                if (txtFilterZip.Text != String.Empty)
                {
                    if (Regex.IsMatch(lstSourceFolders[i], strRegex, RegexOptions.IgnoreCase))
                    {
                        clbFolders.Items.Add(lstSourceFolders[i]);
                    }
                }
                else
                    clbFolders.Items.Add(lstSourceFolders[i]);
            }
        }

        private void btnFilterRPF_Click(object sender, EventArgs e)
        {
            string strRegex = WildcardToRegex(txtFilterRPF.Text);

            clbZips.Items.Clear();

            for (int i = 0; i < lstZipFolders.Count; ++i)
            {
                if (txtFilterRPF.Text != String.Empty)
                {
                    if (Regex.IsMatch(lstZipFolders[i], strRegex, RegexOptions.IgnoreCase))
                    {
                        clbZips.Items.Add(lstZipFolders[i]);
                    }
                }
                else
                    clbZips.Items.Add(lstZipFolders[i]);
            }
        }

        private void clbClips_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedClipFile = clbClips.SelectedItem.ToString();

            int compressedSize = 0;
            int uncompressedSize = 0;

            if (!string.IsNullOrEmpty(selectedClipFile))
                ParseMetadataFileForCompression(selectedClipFile, out compressedSize, out uncompressedSize);

            txtClipCompressed.Text = compressedSize.ToString();
            txtClipUncompressed.Text = uncompressedSize.ToString();

            float ratio = 0;
            
            if (compressedSize != 0 && uncompressedSize != 0)
                ratio = (float)((float)uncompressedSize / (float)compressedSize);

            txtRatio.Text = ratio.ToString();

            MClip clip = new MClip(MClip.ClipType.Normal);
            clip.Load(selectedClipFile);

            MProperty compressionProperty = clip.FindProperty("Compressionfile_DO_NOT_RESOURCE");
            if (compressionProperty != null)
            {
                MPropertyAttributeString compressionAttribute = (MPropertyAttributeString)compressionProperty.GetPropertyAttributes()[0];

                if (compressionAttribute != null)
                    txtCurrentClipCompression.Text = compressionAttribute.GetString();
                else
                    txtCurrentClipCompression.Text = "";
            }

            clip.Dispose();
        }

        private void btnSaveCompression_Click(object sender, EventArgs e)
        {
            SaveCompressionData();
        }

        // New Methods!!
        // This is to use one tab in the tool to allow all the building to be done from the clip page.
        // Existing functionality is still preserved but this allows 1 location to do all the building.
        private void btnPreviewDirectory_Click(object sender, EventArgs e)
        {
            BuildZip(true);
        }

        private void btnCreateZip_Click(object sender, EventArgs e)
        {
            BuildZip(false);
        }

        private void BuildZip(bool preview)
        {
            // Don't process anything is there is nothing to process.
            if (clbClips.CheckedItems.Count == 0)
                return;

            LogReset();

            XmlDocument ap3Doc = BuildAP3XmlFile();
            List<string> previewDictionaries = new List<string>();
            Dictionary<string, PreviewData> previewDataDictionary = new Dictionary<string, PreviewData>();

            for (int i = 0; i < clbClips.CheckedItems.Count; ++i)
            {
                string strAnimationFolder = Path.GetDirectoryName((string)clbClips.CheckedItems[i]) + "\\";
                string strDictionaryName = GenerateDictionaryName(strAnimationFolder);
                if (strDictionaryName == String.Empty) return;
                string strRootAssetPath = GetRootAssetPath(strAnimationFolder);
                string previewDictionary = Path.Combine(PATH_RAW_ZIPS, strRootAssetPath, strDictionaryName + ".icd.zip");

                // Check to see if we already have an entry for this animation folder.
                if (!previewDataDictionary.ContainsKey(strAnimationFolder))
                {
                    PreviewData newPreviewData = new PreviewData(strRootAssetPath, strDictionaryName);
                    previewDataDictionary.Add(strAnimationFolder, newPreviewData);
                    previewDictionaries.Add(previewDictionary);
                }
            }

            foreach (KeyValuePair<string, PreviewData> kvp in previewDataDictionary)
            {
                string animationFolder = kvp.Key;
                AddDictionaryToAP3File(ap3Doc, animationFolder, kvp.Value.rootAssetPath, kvp.Value.dictionaryName);
            }

            if (!Directory.Exists(PATH_AP3_TEMP_FILE))
                Directory.CreateDirectory(PATH_AP3_TEMP_FILE);

            string ap3AssetFilename = PATH_RAW_ZIPS + "\\master_icd_list.xml";

            ap3Doc.Save(ap3AssetFilename);

            bool success = BuildAP3Zip(ap3AssetFilename);

            if (success && preview)
            {
                BuildXGEPreview(previewDictionaries);
            }
        }

        private void btnBuildRPFS_Click(object sender, EventArgs e)
        {
            LogReset();

            if (clbClips.CheckedItems.Count == 0) return;

            List<string> zipFiles = new List<string>();

            for (int i = 0; i < clbClips.CheckedItems.Count; i++)
            {
                string strAnimationFolder = Path.GetDirectoryName((string)clbClips.CheckedItems[i]) + "\\";
                string strRootAssetPath = GetRootAssetPath(strAnimationFolder);
                string rpfDirectory = Path.Combine(PATH_RAW_ZIPS, strRootAssetPath);

                if (!zipFiles.Contains(rpfDirectory))
                    zipFiles.Add(rpfDirectory);
            }

            BuildRPF(zipFiles);
        }

        private void clbClips_MouseMove(object sender, MouseEventArgs e)
        {
            if (sender is ListBox)
            {
                ListBox listBox = (ListBox)sender;
                Point point = new Point(e.X, e.Y);
                int hoverIndex = listBox.IndexFromPoint(point);

                if (hoverIndex != previousToolTipIndex && hoverIndex >= 0 && hoverIndex < listBox.Items.Count)
                {
                    previousToolTipIndex = hoverIndex;
                    string clipFile = listBox.Items[hoverIndex].ToString();
                    string strAnimationFolder = Path.GetDirectoryName(clipFile + "\\");
                    string strDictionaryName = GenerateDictionaryName(strAnimationFolder);
                    if (strDictionaryName == String.Empty) return;
                    string strRootAssetPath = GetRootAssetPath(strAnimationFolder);
                    string caption = string.Format("Zip File: {0}\nRPF File: {1}", strDictionaryName, strRootAssetPath);

                    clipToolTip.SetToolTip(listBox, caption);
                }
            }
        }

        private void btnClipFilter_Click(object sender, EventArgs e)
        {
            string strRegex = WildcardToRegex(txtFilterClips.Text);

            clbClips.Items.Clear();

            for (int i = 0; i < lstClipFolders.Count; ++i)
            {
                if (txtFilterClips.Text != String.Empty)
                {
                    if (Regex.IsMatch(lstClipFolders[i], strRegex, RegexOptions.IgnoreCase))
                    {
                        clbClips.Items.Add(lstClipFolders[i]);
                    }
                }
                else
                    clbClips.Items.Add(lstClipFolders[i]);
            }
        }

        private void cmboDLC_SelectedIndexChanged(object sender, EventArgs e)
        {
            LongOption[] opts = new LongOption[] {};

            ArrayList lst = new ArrayList();

            if (cmboDLC.SelectedIndex > 0)
            {
                lst.Add("--dlc");
                lst.Add(cmboDLC.Items[cmboDLC.SelectedIndex].ToString()); 
            }

            lst.Add("--branch");
            lst.Add(cmboBranch.Items[cmboBranch.SelectedIndex].ToString());

            CommandOptions options = new CommandOptions((string[])lst.ToArray(typeof(string)), opts);

            SetPaths(options);
        }

        private void cmboBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmboDLC_SelectedIndexChanged(sender, e);
        }
    }
}
