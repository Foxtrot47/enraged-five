﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Editor;
using RSG.Interop.Microsoft.Windows;

namespace GameViewportSample.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public class SimulateKeyPressAction : ButtonAction<MainWindow>
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="SimulateKeyPressAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public SimulateKeyPressAction(ParameterResolverDelegate<MainWindow> resolver)
            : base(resolver)
        {
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mainWindow"></param>
        /// <returns></returns>
        public override bool CanExecute(MainWindow mainWindow)
        {
            return (mainWindow.GameViewport.GameHandle != IntPtr.Zero);
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        public override void Execute(MainWindow mainWindow)
        {
            MainWindowDataContext dc = mainWindow.DataContext as MainWindowDataContext;
            if (dc == null)
            {
                throw new ArgumentException("Invalid datacontext.");
            }

            mainWindow.GameViewport.SimulateKeyPress(dc.SelectedKey);
        }
        #endregion // Overrides
    } // SimulateKeyPressAction
}
