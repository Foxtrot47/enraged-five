﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Editor;
using RSG.Editor.Controls;

namespace GameViewportSample.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public class LaunchGameAction : ButtonAction<MainWindow>
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="LaunchGameAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public LaunchGameAction(ParameterResolverDelegate<MainWindow> resolver)
            : base(resolver)
        {
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mainWindow"></param>
        /// <returns></returns>
        public override bool CanExecute(MainWindow mainWindow)
        {
            MainWindowDataContext dc = mainWindow.DataContext as MainWindowDataContext;
            if (dc == null)
            {
                throw new ArgumentException("Invalid datacontext.");
            }
            
            try
            {
                return File.Exists(dc.GameExecutable);
            }
            catch (System.Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        public override void Execute(MainWindow mainWindow)
        {
            MainWindowDataContext dc = mainWindow.DataContext as MainWindowDataContext;
            if (dc == null)
            {
                throw new ArgumentException("Invalid datacontext.");
            }

            try
            {
                ProcessStartInfo startInfo = new ProcessStartInfo(dc.GameExecutable, dc.GameArguments);
                startInfo.WorkingDirectory = Path.GetDirectoryName(dc.GameExecutable);
                startInfo.Arguments += String.Format(" -setHwndMain={0}", mainWindow.GameViewport.Handle);

                Process.Start(startInfo);
            }
            catch (System.Exception ex)
            {
                dc.StatusText = String.Format("Failed to launch game: {0}", ex.Message);
            }
        }
        #endregion // Overrides
    } // LaunchGameAction
}
