﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CurveEditorSample.Commands;
using Microsoft.Win32;
using RSG.Editor;
using RSG.Editor.Controls;
using RSG.Editor.SharedCommands;
using RSG.Editor.Model;

namespace CurveEditorSample
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : RsMainWindow
    {
        #region Constants
        /// <summary>
        /// 
        /// </summary>
        private static readonly Guid _editMenuId = new Guid("A74452FD-A8B7-4927-9536-5888E1E71B9B");
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            AddCommandInstances();
            AttachCommandBindings();
        }
        #endregion // Constructor(s)

        #region Editor Commands
        /// <summary>
        /// Adds the commands/menus to the command manager.
        /// </summary>
        internal void AddCommandInstances()
        {
            // File menu.
            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.OpenFile,
                0,
                false,
                "Open",
                new Guid("EF6D3DF7-241F-44C5-8061-977EC579824C"),
                RockstarCommandManager.FileMenuId);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Save,
                1,
                false,
                "Save",
                new Guid("F1F0AE52-F0FF-4D0E-810A-CE63E2448173"),
                RockstarCommandManager.FileMenuId);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Close,
                2,
                false,
                "Close",
                new Guid("502D89C6-9B12-47D5-80E5-5398E9B30266"),
                RockstarCommandManager.FileMenuId);

            // Edit menu.
            RockstarCommandManager.AddEditMenu(_editMenuId);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Undo,
                0,
                false,
                "Undo",
                new Guid("1FFDC53A-79E9-47FF-92D9-E2EB44137FB6"),
                _editMenuId);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Redo,
                1,
                false,
                "Redo",
                new Guid("E5C4CF02-7D53-4300-9AFE-E8D9C02ADEAC"),
                _editMenuId);
        }

        /// <summary>
        /// Attaches all of the necessary command bindings to this instance.
        /// </summary>
        private void AttachCommandBindings()
        {
            // File management.
            new OpenFileAction(this.DataContextResolver)
                .AddBinding(RockstarCommands.OpenFile, this);
            new SaveFileAction(this.DataContextResolver)
                .AddBinding(RockstarCommands.Save, this);
            new CloseFileAction(this.DataContextResolver)
                .AddBinding(RockstarCommands.Close, this);

            // Undo/Redo
            new CurveEditorSample.Commands.UndoAction(this.UndoEngineResolver)
                .AddBinding(RockstarCommands.Undo, this);

            new RedoAction(this.UndoEngineResolver)
                .AddBinding(RockstarCommands.Redo, this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public MainWindowDataContext DataContextResolver(CommandData data)
        {
            MainWindowDataContext dc = null;
            if (!this.Dispatcher.CheckAccess())
            {
                dc = this.Dispatcher.Invoke<MainWindowDataContext>(() => this.DataContext as MainWindowDataContext);
            }
            else
            {
                dc = this.DataContext as MainWindowDataContext;
            }

            if (dc == null)
            {
                Debug.Fail("Data context isn't valid.");
                throw new ArgumentException("Data context isn't valid.");
            }
            return dc;
        }

        /// <summary>
        /// Represents the method that is used to retrieve a command parameter object for the
        /// view commands.
        /// </summary>
        /// <param name="data">
        /// The command whose command parameter needs to be resolved.
        /// </param>
        /// <returns>
        /// The command parameter object for the specified command.
        /// </returns>
        private UndoEngine UndoEngineResolver(CommandData data)
        {
            return DataContextResolver(data).UndoEngine;
        }
        #endregion // Editor Commands
    } // MainWindow
}
