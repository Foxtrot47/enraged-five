﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using RSG.Editor;
using RSG.Editor.SharedCommands;

namespace GameViewportSample.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public class GameViewportSampleCommands
    {
        #region Fields
        /// <summary>
        /// String table resource manager for getting at the command related strings.
        /// </summary>
        private static readonly CommandResourceManager _commandResourceManager =
            new CommandResourceManager(
                "GameViewportSample.Resources.CommandStringTable",
                typeof(GameViewportSampleCommands).Assembly);

        /// <summary>
        /// The private cache of System.Windows.Input.RoutedCommand objects.
        /// </summary>
        private static readonly RockstarRoutedCommand[] _internalCommands =
            new RockstarRoutedCommand[Enum.GetValues(typeof(CommandId)).Length];
        #endregion // Fields

        #region Enumerations
        /// <summary>
        /// Defines all of the command identifiers for the different core Rockstar commands.
        /// </summary>
        private enum CommandId
        {
            /// <summary>
            /// Used to identify the <see cref="GameViewportSampleCommands.LaunchGame"/>
            /// routed command.
            /// </summary>
            LaunchGame,

            /// <summary>
            /// Used to identify the <see cref="GameViewportSampleCommands.Browse"/>
            /// routed command.
            /// </summary>
            Browse,

            /// <summary>
            /// Used to identify the <see cref="GameViewportSampleCommands.SimulateKeyPress"/>
            /// routed command.
            /// </summary>
            SimulateKeyPress
        }
        #endregion // Enumerations

        #region Properties
        /// <summary>
        /// Gets the command that is used to launch the game.
        /// </summary>
        public static RockstarRoutedCommand LaunchGame
        {
            get { return EnsureCommandExists(CommandId.LaunchGame); }
        }

        /// <summary>
        /// Gets the command that is used to browse for the game executable.
        /// </summary>
        public static RockstarRoutedCommand Browse
        {
            get { return EnsureCommandExists(CommandId.Browse); }
        }

        /// <summary>
        /// Gets the command that is used to simulate key presses in the running game.
        /// </summary>
        public static RockstarRoutedCommand SimulateKeyPress
        {
            get { return EnsureCommandExists(CommandId.SimulateKeyPress); }
        }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Creates a new System.Windows.Input.RoutedCommand object that is associated with the
        /// specified identifier.
        /// </summary>
        /// <param name="id">
        /// The identifier for the command to create.
        /// </param>
        /// <param name="provider">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        /// <returns>
        /// The newly create System.Windows.Input.RoutedCommand object.
        /// </returns>
        private static RockstarRoutedCommand CreateCommand(String id, BitmapSource icon)
        {
            String name = _commandResourceManager.GetString(id, CommandStringCategory.Name);
            String category = _commandResourceManager.GetString(id, CommandStringCategory.Category);
            String description = _commandResourceManager.GetString(id, CommandStringCategory.Description);
            InputGestureCollection gestures = _commandResourceManager.GetGestures(id);

            return new RockstarRoutedCommand(
                name, typeof(GameViewportSampleCommands), gestures, category, description, icon);
        }

        /// <summary>
        /// Makes sure the internal value for the command associated with the specified
        /// identifier exists and returns it.
        /// </summary>
        /// <param name="commandId">
        /// The identifier for the command that should be created and returned.
        /// </param>
        /// <param name="icon">
        /// The icon that the command should be using.
        /// </param>
        /// <returns>
        /// The <see cref="RSG.Editor.RockstarRoutedCommand"/> object associated with the
        /// specified identifier.
        /// </returns>
        private static RockstarRoutedCommand EnsureCommandExists(CommandId commandId, BitmapSource icon = null)
        {
            int commandIndex = (int)commandId;
            if (commandIndex < -1 || commandIndex >= _internalCommands.Length)
            {
                return null;
            }

            RockstarRoutedCommand command = _internalCommands[commandIndex];
            if (command == null)
            {
                lock (_internalCommands)
                {
                    if (command == null)
                    {
                        command = CreateCommand(commandId.ToString(), icon);
                        _internalCommands[commandIndex] = command;
                    }
                }
            }

            return command;
        }
        #endregion // Methods
    } // GameViewportSampleCommands
}
