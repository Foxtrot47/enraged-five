﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Editor;
using System.IO;
using RSG.Editor.SharedCommands;
using RSG.Editor.Model;
using CurveEditorSample.Model;
using RSG.Editor.Controls.CurveEditor.ViewModel;

namespace CurveEditorSample
{
    /// <summary>
    /// 
    /// </summary>
    public class MainWindowDataContext : NotifyPropertyChangedBase
    {
        #region Fields
        /// <summary>
        /// 
        /// </summary>
        private CurveCollection _curveCollection;

        /// <summary>
        /// The undo redo engine to be used by the model.
        /// </summary>
        private UndoEngine _undoEngine;
        #endregion // Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MainWindowDataContext"/> class.
        /// </summary>
        public MainWindowDataContext()
        {
            _undoEngine = new UndoEngine();
        }
        #endregion // Constructors
        
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<CurveViewModel> Curves
        {
            get { return _curves; }
            private set { SetProperty(ref _curves, value, "Curves", "Loaded"); }
        }
        private IEnumerable<CurveViewModel> _curves;

        /// <summary>
        /// 
        /// </summary>
        public bool Loaded
        {
            get { return (Curves != null); }
        }

        /// <summary>
        /// 
        /// </summary>
        public UndoEngine UndoEngine
        {
            get { return _undoEngine; }
        }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Loads the file located at the specified file path.
        /// </summary>
        /// <param name="options">
        /// File load options.
        /// </param>
        /// <returns>
        /// The task that represents the asynchronous Load operation.
        /// </returns>
        internal async Task OpenAsync(String filepath)
        {
            if (!File.Exists(filepath))
            {
                throw new FileNotFoundException(
                    "Unable to load a curve file from a path that doesn't exist.", filepath);
            }

            // Open up the curve file.
            Task<CurveCollection> task =
                Task.Factory.StartNew<CurveCollection>(LoadCurveFile, filepath);
            await Task.WhenAll(task);

            if (task.Result != null)
            {
                _curveCollection = task.Result;
                Curves = _curveCollection.Select(curve => new CurveViewModel(curve)).ToList();
            }
            else
            {
                _curveCollection = null;
                Curves = null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filepath"></param>
        /// <returns></returns>
        internal async Task<bool> SaveAsync(String filepath)
        {
            // Open up the curve file.
            return await Task.Factory.StartNew<bool>(SaveCurveFile, filepath);
        }

        /// <summary>
        /// 
        /// </summary>
        internal void CloseFile()
        {
            _curveCollection = null;
            _undoEngine = null;
            Curves = null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filepath"></param>
        /// <returns></returns>
        private CurveCollection LoadCurveFile(object parameter)
        {
            try
            {
                _undoEngine = new UndoEngine();
                return CurveCollection.ParseFromFile(parameter as String, _undoEngine);
            }
            catch (System.Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        private bool SaveCurveFile(object parameter)
        {
            try
            {
                return _curveCollection.SaveToFile(parameter as String);
            }
            catch (System.Exception ex)
            {
                return false;
            }
        }
        #endregion // Methods
    } // MainWindowViewModel
}
