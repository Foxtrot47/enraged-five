﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using RSG.Editor;
using RSG.Interop.Microsoft.Windows;

namespace GameViewportSample
{
    /// <summary>
    /// 
    /// </summary>
    public class MainWindowDataContext : NotifyPropertyChangedBase
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="StatusText"/> property.
        /// </summary>
        private String _statusText;

        /// <summary>
        /// Private field for the <see cref="GameExecutable"/> property.
        /// </summary>
        private String _gameExecutable;

        /// <summary>
        /// Private field for the <see cref="GameArguments"/> property.
        /// </summary>
        private String _gameArguments;

        /// <summary>
        /// Private field for the <see cref="SelectedKey"/> property.
        /// </summary>
        private VirtualKey _selectedKey;
        #endregion // Fields

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public MainWindowDataContext()
        {
            _selectedKey = VirtualKey.A;
            _statusText = "Ready";
#if DEBUG
            _gameExecutable = @"X:\gta5\build\dev_ng\game_win64_beta.exe";
            _gameArguments = "-noautoload -useCompatPacks -noProfileSignin -width=1280 -height=720";
#endif
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Text to display in the windows status bar.
        /// </summary>
        public String StatusText
        {
            get { return _statusText; }
            set { SetProperty(ref _statusText, value); }
        }

        /// <summary>
        /// Executable to start up.
        /// </summary>
        public String GameExecutable
        {
            get { return _gameExecutable; }
            set { SetProperty(ref _gameExecutable, value); }
        }

        /// <summary>
        /// List of arguments that will be passed to the game.
        /// </summary>
        public String GameArguments
        {
            get { return _gameArguments; }
            set { SetProperty(ref _gameArguments, value); }
        }

        /// <summary>
        /// Key to send to the game.
        /// </summary>
        public VirtualKey SelectedKey
        {
            get { return _selectedKey; }
            set { SetProperty(ref _selectedKey, value); }
        }
        #endregion // Properties
    } // MainWindowDataContext
}
