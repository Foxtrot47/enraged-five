﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using RSG.Editor.Controls;

namespace GameViewportSample
{

    /// <summary>
    /// 
    /// </summary>
    public class App : RsApplication
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="App"/> class.
        /// </summary>
        public App()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a value indicating whether the layout for the main window get serialised on
        /// exit and deserialised during loading.
        /// </summary>
        protected override bool MakeLayoutPersistent
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a semi-colon separate list of the application authors email addresses. These
        /// are the addresses which are used by the unhandled exception dialog to determine
        /// where the mail should be sent by default.
        /// e.g.
        /// *tools@rockstarnorth.com
        /// michael.taschler@rockstarnorth.com;dave.evans@rockstarnorth.com
        /// </summary>
        protected override string AuthorEmailAddress
        {
            get { return "*tools@rockstarnorth.com"; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Main entry point into the application. The first thing to get called.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            App app = new App();
            app.ShutdownMode = ShutdownMode.OnMainWindowClose;
            RsApplication.OnEntry(app, ApplicationMode.Multiple);
        }
        /*
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnStartup(StartupEventArgs e)
        {
            System.Windows.Media.RenderOptions.ProcessRenderMode = System.Windows.Interop.RenderMode.SoftwareOnly;
            base.OnStartup(e);
        }
        */
        /// <summary>
        /// Override to create the main window for the application.
        /// </summary>
        /// <returns>
        /// The System.Windows.Window that will be the main window for this application.
        /// </returns>
        protected override Window CreateMainWindow()
        {
            MainWindow window = new MainWindow();
            window.DataContext = new MainWindowDataContext();
            return window;
        }
        #endregion Methods
    } // App
}
