﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Editor;

namespace CurveEditorSample.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public class OpenFileAction : ButtonAction<MainWindowDataContext>
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="OpenFileAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public OpenFileAction(ParameterResolverDelegate<MainWindowDataContext> resolver)
            : base(resolver)
        {
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="dc">The command parameter that has been requested.</param>
        public override void Execute(MainWindowDataContext dc)
        {
            ICommonDialogService dlgService = this.GetService<ICommonDialogService>();
            if (dlgService == null)
            {
                Debug.Assert(false, "Unable to open file as service is missing.");
                return;
            }

            String filter = "Curve Files (*.xml)|*.xml|All Files (*.*)|*.*";
            String filepath;
            if (!dlgService.ShowOpenFile(null, null, filter, 0, out filepath))
            {
                return;
            }

            // Open the file.
            dc.OpenAsync(filepath);
        }
        #endregion // Overrides
    } // OpenFileAction
}
