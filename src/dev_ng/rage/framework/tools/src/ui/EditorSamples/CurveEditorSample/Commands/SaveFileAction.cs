﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Editor;

namespace CurveEditorSample.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public class SaveFileAction : ButtonActionAsync<MainWindowDataContext>
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="SaveFileAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public SaveFileAction(ParameterResolverDelegate<MainWindowDataContext> resolver)
            : base(resolver)
        {
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="engine">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(MainWindowDataContext dc)
        {
            return dc.Loaded;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="dc">The command parameter that has been requested.</param>
        public override async Task Execute(MainWindowDataContext dc)
        {
            ICommonDialogService dlgService = this.GetService<ICommonDialogService>();
            IMessageBoxService msgService = this.GetService<IMessageBoxService>();
            if (dlgService == null || msgService == null)
            {
                Debug.Assert(false, "Unable to save file as service is missing.");
                return;
            }

            String filter = "Curve Files (*.xml)|*.xml";
            String destination;
            if (!dlgService.ShowSaveFile(null, null, filter, 0, out destination))
            {
                return;
            }

            // Save the file
            bool task = await dc.SaveAsync(destination);

            if (!task)
            {
                msgService.Show(
                    "An unexpected error occurred while attempting to save the file out.",
                    String.Empty,
                    System.Windows.MessageBoxButton.OK,
                    System.Windows.MessageBoxImage.Error);
            }
            else
            {
                msgService.Show(
                    String.Format("File successfully saved to '{0}'.", destination),
                    String.Empty,
                    System.Windows.MessageBoxButton.OK,
                    System.Windows.MessageBoxImage.Information);
            }
        }
        #endregion // Overrides
    } // SaveFileAction
}
