﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using RSG.Editor.Controls.CurveEditor.Model;
using RSG.Editor.Model;

namespace CurveEditorSample.Model
{
    /// <summary>
    /// Root of a curve hierarchy
    /// </summary>
    public class CurveCollection : ModelCollection<Curve>
    {
        #region Fields
        /// <summary>
        /// 
        /// </summary>
        private UndoEngine _undoEngine;
        #endregion // Fields
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlElem"></param>
        public CurveCollection(UndoEngine undoEngine, XElement xmlElem)
            : base(null)
        {
            // Parse the child curves.
            foreach (XElement curveElem in xmlElem.Elements("Curve"))
            {
                this.Add(new Curve(undoEngine, curveElem));
            }

            _undoEngine = undoEngine;
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        public bool SaveToFile(String filepath)
        {
            try
            {
                XDocument xmlDoc = new XDocument(
                        new XDeclaration("1.0", "UTF-8", "yes"),
                        this.ToXElement("CurveCollection"));
                xmlDoc.Save(filepath);
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }
        }
        #endregion // Public Methods

        #region Serialisation
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        internal XElement ToXElement(XName name)
        {
            return new XElement("Curves", this.Select(item => item.ToXElement("Curve")));
        }
        #endregion // Serialisation

        #region Overrides
        /// <summary>
        /// Gets the undo engine this collection should use to store changes in.
        /// </summary>
        protected override UndoEngine UndoEngine
        {
            get { return _undoEngine; }
        }
        #endregion // Overrides

        #region Static Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filepath"></param>
        /// <returns></returns>
        public static CurveCollection ParseFromFile(String filepath, UndoEngine undoEngine)
        {
            try
            {
                XDocument xmlDoc = XDocument.Load(filepath);
                XElement curveElement = xmlDoc.Element("Curves");
                return new CurveCollection(undoEngine, curveElement);
            }
            catch (Exception)
            {
                return null;
            }
        }
        #endregion // Static Controller Methods
    } // CurveCollection
}
