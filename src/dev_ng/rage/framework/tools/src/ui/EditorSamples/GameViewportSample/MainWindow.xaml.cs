﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GameViewportSample.Commands;
using RSG.Editor;
using RSG.Editor.Controls;

namespace GameViewportSample
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : RsMainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            AttachCommandBindings();
        }

        /// <summary>
        /// 
        /// </summary>
        private void AttachCommandBindings()
        {
            new LaunchGameAction(this.ControlResolver)
                .AddBinding(GameViewportSampleCommands.LaunchGame, this);
            new BrowseAction(this.DataContextResolver)
                .AddBinding(GameViewportSampleCommands.Browse, this);
            new SimulateKeyPressAction(this.ControlResolver)
                .AddBinding(GameViewportSampleCommands.SimulateKeyPress, this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        private MainWindow ControlResolver(CommandData data)
        {
            return this;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private MainWindowDataContext DataContextResolver(CommandData data)
        {
            return DataContext as MainWindowDataContext;
        }
    }
}
