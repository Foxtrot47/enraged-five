﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Editor;

namespace GameViewportSample.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public class BrowseAction : ButtonAction<MainWindowDataContext>
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="BrowseAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public BrowseAction(ParameterResolverDelegate<MainWindowDataContext> resolver)
            : base(resolver)
        {
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="dc">The main window data context.</param>
        public override void Execute(MainWindowDataContext dc)
        {
            ICommonDialogService dlgService = this.GetService<ICommonDialogService>();
            if (dlgService == null)
            {
                Debug.Assert(false, "Unable to browse for exe as service is missing.");
                return;
            }

            String filter = "Executables (*.exe)|*.exe|All Files (*.*)|*.*";
            String filepath;
            if (!dlgService.ShowOpenFile(null, dc.GameExecutable, filter, 0, out filepath))
            {
                return;
            }

            dc.GameExecutable = filepath;
        }
        #endregion // Overrides
    } // BrowseAction
}
