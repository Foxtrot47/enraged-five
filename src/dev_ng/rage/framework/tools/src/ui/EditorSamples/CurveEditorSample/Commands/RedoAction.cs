﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Editor;
using RSG.Editor.Model;

namespace CurveEditorSample.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public class RedoAction : ButtonAction<UndoEngine>
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="RedoAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public RedoAction(ParameterResolverDelegate<UndoEngine> resolver)
            : base(resolver)
        {
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="engine">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(UndoEngine engine)
        {
            return engine.CanRedo;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="engine">The command parameter that has been requested.</param>
        public override void Execute(UndoEngine engine)
        {
            engine.Redo();
        }
        #endregion // Overrides
    } // RedoAction
}
