﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using RSG.Editor.Controls;
using RSG.Editor.Controls.CurveEditor.Model;

namespace CurveEditorSample
{
    /// <summary>
    /// 
    /// </summary>
    public class App : RsApplication
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="App"/> class.
        /// </summary>
        public App()
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a value indicating whether the layout for the main window get serialised on
        /// exit and deserialised during loading.
        /// </summary>
        protected override bool MakeLayoutPersistent
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a semi-colon separate list of the application authors email addresses. These
        /// are the addresses which are used by the unhandled exception dialog to determine
        /// where the mail should be sent by default.
        /// e.g.
        /// *tools@rockstarnorth.com
        /// michael.taschler@rockstarnorth.com;dave.evans@rockstarnorth.com
        /// </summary>
        protected override string AuthorEmailAddress
        {
            get { return "*tools@rockstarnorth.com"; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Main entry point into the application. The first thing to get called.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            App app = new App();
            app.ShutdownMode = ShutdownMode.OnMainWindowClose;
            RsApplication.OnEntry(app, ApplicationMode.Multiple);
        }

        /// <summary>
        /// Override to create the main window for the application.
        /// </summary>
        /// <returns>
        /// The System.Windows.Window that will be the main window for this application.
        /// </returns>
        protected override Window CreateMainWindow()
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.DataContext = new MainWindowDataContext();
            return mainWindow;
        }
        
        /// <summary>
        /// Called immediately after the main windows Show method has been called.
        /// </summary>
        /// <param name="mainWindow">
        /// A reference to the main window that was shown.
        /// </param>
        /// <returns>
        /// A task representing the work done by this method.
        /// </returns>
        protected async override Task OnMainWindowShown(Window mainWindow)
        {
            MainWindowDataContext vm = this.MainWindow.DataContext as MainWindowDataContext;
            if (vm == null)
            {
                Debug.Fail("Unable to complete startup operations as the data context is missing");
                return;
            }

            // Check if we need to load a file off the bat.
            String commandline = this.TrailingArguments.FirstOrDefault();
            if (!File.Exists(commandline))
            {
                return;
            }

            await vm.OpenAsync(commandline);
        }
        #endregion Methods
    } // App
}
