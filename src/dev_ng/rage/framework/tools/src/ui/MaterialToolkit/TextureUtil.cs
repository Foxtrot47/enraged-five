﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaterialToolkit
{
    /// <summary>
    /// Utility class for dealing with texture files.
    /// </summary>
    public static class TextureUtil
    {
        /// <summary>
        /// Supported texture extensions.
        /// </summary>
        public static readonly String[] SupportedTextureExtensions =
            new String[]
            {
                ".tif",
                ".tiff",
                ".dds",
                ".bmp"
            };

        /// <summary>
        /// Filter string for the open file dialog box.
        /// </summary>
        public static readonly String OpenFileDialogFilter =
            "Tagged Image File Format (*.tif;*.tiff)|*.tif;*.tiff|DirectDraw Surface (*.dds)|*.dds|Bitmap (*.bmp)|*.bmp";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filepath"></param>
        /// <returns></returns>
        public static bool HasSupportedFileExtension(String filepath)
        {
            String ext = Path.GetExtension(filepath).ToLower();
            return SupportedTextureExtensions.Contains(ext);
        }

    } // TextureUtil
}
