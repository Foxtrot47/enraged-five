﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Editor;

namespace MaterialToolkit
{
    /// <summary>
    /// 
    /// </summary>
    public class SceneViewModel : NotifyPropertyChangedBase
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Filepath"/> property.
        /// </summary>
        private readonly String _filepath;

        /// <summary>
        /// Private field for the <see cref="SceneName"/> property.
        /// </summary>
        private readonly String _sceneName;

        /// <summary>
        /// Private field for the <see cref="Convert"/> property.
        /// </summary>
        private bool _convert;
        #endregion // Fields

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filepath"></param>
        public SceneViewModel(String filepath)
        {
            _filepath = filepath;
            _sceneName = Path.GetFileNameWithoutExtension(filepath);
            _convert = true;
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Path to the file on disk for this scene xml file.
        /// </summary>
        public String Filepath
        {
            get { return _filepath; }
        }

        /// <summary>
        /// Name of the scene.
        /// </summary>
        public String SceneName
        {
            get { return _sceneName; }
        }

        /// <summary>
        /// Flag indicating whether the user wishes to convert this particular scene.
        /// </summary>
        public bool Convert
        {
            get { return _convert; }
            set { SetProperty(ref _convert, value); }
        }
        #endregion // Properties
    } // SceneViewModel
}
