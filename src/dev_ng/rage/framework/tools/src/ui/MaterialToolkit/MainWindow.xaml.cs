﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using RSG.Base.Configuration.Services;
using RSG.Editor;
using RSG.Editor.Controls;
using MaterialToolkit.Commands;
using RSG.Statistics.Common.Config;

namespace MaterialToolkit
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : RsMainWindow
    {
        #region Constants
        /// <summary>
        /// The global identifier used for the data menu.
        /// </summary>
        private static readonly Guid _dataMenu = new Guid("44B8CA29-261F-42AB-9776-F980619832A6");
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        /// <param name="options"></param>
        public MainWindow(ICommandOptions options)
        {
            InitializeComponent();

            IStatisticsConfig config = new StatisticsConfig(options.Branch);
            IServer server = config.DefaultServer;
            if (options.AllOptions.HasOption("server"))
            {
                String serverName = (String)options.AllOptions["server"];
                if (config.Servers.ContainsKey(serverName))
                {
                    server = config.Servers[serverName];
                }
            }

            DataContext = new MainWindowDataContext(server, options.Branch);

            // Make sure the two datagrids are sorted by the filepath by default.
            SortDescription description = new SortDescription("Filepath", ListSortDirection.Ascending);
            SceneDataGrid.Items.SortDescriptions.Add(description);
            TextureDataGrid.Items.SortDescriptions.Add(description);

            AddCommandInstances();
            AttachCommandBindings();
        }
        #endregion

        #region Command Registration & Resolvers
        /// <summary>
        /// 
        /// </summary>
        private void AddCommandInstances()
        {
            // Data menu.
            RockstarCommandManager.AddCommandBarItem(new TopLevelMenu(1, "Data", _dataMenu));

            RockstarCommandManager.AddCommandInstance(
                MaterialToolkitCommands.AddTexture,
                0,
                false,
                "Add Texture",
                new Guid("CE03CF0C-F1A6-4644-B649-622E255C0344"),
                _dataMenu);

            RockstarCommandManager.AddCommandInstance(
                MaterialToolkitCommands.RemoveTexture,
                1,
                false,
                "Remove Texture",
                new Guid("BBFEF60A-DA25-4270-93A8-1DC5B6D6EF9F"),
                _dataMenu);

            RockstarCommandManager.AddCommandInstance(
                MaterialToolkitCommands.Refresh,
                2,
                false,
                "Refresh",
                new Guid("896B61BE-AA1B-4867-8124-6E6FA342CD89"),
                _dataMenu);

            RockstarCommandManager.AddCommandInstance(
                MaterialToolkitCommands.LocalConvert,
                2,
                true,
                "Local Convert",
                new Guid("5E91F35F-D266-453A-9DF6-878C7C0C21A9"),
                _dataMenu);

            RockstarCommandManager.AddCommandInstance(
                MaterialToolkitCommands.QueueOnAssetBuilder,
                3,
                false,
                "Queue on Asset Builder",
                new Guid("10AC4294-7FC2-4201-9E7E-B44A258534F4"),
                _dataMenu);

            RockstarCommandManager.AddCommandInstance(
                MaterialToolkitCommands.CancelConvert,
                4,
                false,
                "Cancel Conversion",
                new Guid("CCF3369E-92E9-4698-ABBF-E2D5B7C75C85"),
                _dataMenu);

            // Options menu
            Guid optionsMenu = new Guid("823CB88F-45A2-47AD-9656-018E94C13A4D");
            RockstarCommandManager.AddMenu(5, true, "Options", optionsMenu, _dataMenu);

            RockstarCommandManager.AddCommandInstance(
                MaterialToolkitCommands.ForceTextureExportOption,
                0,
                false,
                "Force Texture Export",
                new Guid("A269D367-5DCB-47D4-81A5-C85205AF9037"),
                optionsMenu);
        }

        /// <summary>
        /// Attaches all of the necessary command bindings to this instance.
        /// </summary>
        private void AttachCommandBindings()
        {
            // File management commands.
            new AddTextureAction(this.DataContextResolver)
                .AddBinding(MaterialToolkitCommands.AddTexture, this);
            new RemoveTextureAction(this.RemoveTextureResolver)
                .AddBinding(MaterialToolkitCommands.RemoveTexture, this);
            new LocalConvertAction(this.DataContextResolver)
                .AddBinding(MaterialToolkitCommands.LocalConvert, this);
            new QueueOnAssetBuilderAction(this.DataContextResolver)
                .AddBinding(MaterialToolkitCommands.QueueOnAssetBuilder, this);
            new CancelConvertAction(this.DataContextResolver)
                .AddBinding(MaterialToolkitCommands.CancelConvert, this);
            new RefreshAction(this.DataContextResolver)
                .AddBinding(MaterialToolkitCommands.Refresh, this);

            // Options.
            new ForceTextureExportOptionAction(this.DataContextResolver)
                .AddBinding(MaterialToolkitCommands.ForceTextureExportOption, this);
        }

        /// <summary>
        /// Command resolver for retrieving the main window's data context.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public MainWindowDataContext DataContextResolver(CommandData data)
        {
            MainWindowDataContext dc = null;
            if (!this.Dispatcher.CheckAccess())
            {
                dc = this.Dispatcher.Invoke<MainWindowDataContext>(() => this.DataContext as MainWindowDataContext);
            }
            else
            {
                dc = this.DataContext as MainWindowDataContext;
            }

            if (dc == null)
            {
                Debug.Fail("Data context isn't valid.");
                throw new ArgumentException("Data context isn't valid.");
            }
            return dc;
        }

        /// <summary>
        /// Command resolver for retrieving the command args for removing a list of textures.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private RemoveTextureCommandArgs RemoveTextureResolver(CommandData data)
        {
            MainWindowDataContext dc = DataContextResolver(data);

            IList<TextureViewModel> textureVms;
            if (!this.Dispatcher.CheckAccess())
            {
                textureVms = this.Dispatcher.Invoke<IList<TextureViewModel>>(
                    () => this.TextureDataGrid.SelectedItems.OfType<TextureViewModel>().ToList());
            }
            else
            {
                textureVms = this.TextureDataGrid.SelectedItems.OfType<TextureViewModel>().ToList();
            }

            return new RemoveTextureCommandArgs(dc, textureVms);
        }
        #endregion

        #region Drag & Drop Event Handlers
        /// <summary>
        /// Event handler for processing a drop operation of the texture data grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void TextureDataGrid_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                String[] files = (String[])e.Data.GetData(DataFormats.FileDrop);
                MainWindowDataContext dataContext = DataContextResolver(null);

                // Make sure the locally modified files have been loaded.
                RsApplication app = Application.Current as RsApplication;
                if (app == null)
                {
                    throw new ArgumentNullException("app");
                }
                IPerforceService perforceService = app.GetService<IPerforceService>();
                if (perforceService == null)
                {
                    throw new ArgumentNullException("perforceService");
                }

                await dataContext.AddTextures(files.Where(file => TextureUtil.HasSupportedFileExtension(file)), perforceService);
            }
        }

        /// <summary>
        /// Event handler for processing the drag over texture data grid event to
        /// determine whether the drag operation contains valid data.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextureDataGrid_DragOver(object sender, DragEventArgs e)
        {
            if (!e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effects = DragDropEffects.None;
                e.Handled = true;
            }
            else
            {
                String[] files = (String[])e.Data.GetData(DataFormats.FileDrop);
                if (!files.Any(file => TextureUtil.HasSupportedFileExtension(file)))
                {
                    e.Effects = DragDropEffects.None;
                    e.Handled = true;
                }
            }
        }
        #endregion
    }
}
