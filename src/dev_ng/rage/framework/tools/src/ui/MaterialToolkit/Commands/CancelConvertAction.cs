﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Editor;

namespace MaterialToolkit.Commands
{
    /// <summary>
    /// Action for cancelling a conversion that is currently in progress.
    /// </summary>
    public class CancelConvertAction : ButtonAction<MainWindowDataContext>
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="CancelConvertAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public CancelConvertAction(ParameterResolverDelegate<MainWindowDataContext> resolver)
            : base(resolver)
        {
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// Override to set logic against the can execute command handler.
        /// </summary>
        /// <param name="vm"></param>
        /// <returns></returns>
        public override bool CanExecute(MainWindowDataContext vm)
        {
            return vm.ConversionInProgress;
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="vm">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(MainWindowDataContext vm)
        {
            vm.AbortConversion();
        }
        #endregion
    }
}
