﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Editor;

namespace MaterialToolkit.Commands
{
    /// <summary>
    /// Action for refreshing the texture usage information.
    /// </summary>
    public class RefreshAction : ButtonAction<MainWindowDataContext>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="RefreshAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public RefreshAction(ParameterResolverDelegate<MainWindowDataContext> resolver)
            : base(resolver)
        {
        }
        #endregion

        #region Methods
        /// <summary>
        /// Override to set logic against the can execute command handler. By default this
        /// returns true.
        /// </summary>
        /// <param name="vm">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// True if the command can be fired; otherwise, false.
        /// </returns>
        public override bool CanExecute(MainWindowDataContext vm)
        {
            return !vm.ConversionInProgress && vm.Textures.Any();
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="vm">
        /// The command parameter that has been requested.
        /// </param>
        /// <returns>
        /// A task that represents the work done by the command handler.
        /// </returns>
        public override async void Execute(MainWindowDataContext vm)
        {
            IPerforceService perforceService = this.GetService<IPerforceService>();
            if (perforceService == null)
            {
                throw new ArgumentNullException("perforceService");
            }

            await vm.RefreshTextureUsage(perforceService);
        }
        #endregion
    }
}
