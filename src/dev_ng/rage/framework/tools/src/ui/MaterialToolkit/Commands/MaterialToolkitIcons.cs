﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace MaterialToolkit.Commands
{
    /// <summary>
    /// Static class fro accessing the icons used by the material toolit.
    /// </summary>
    public static class MaterialToolkitIcons
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="AddTexture"/> property.
        /// </summary>
        private static BitmapSource _addTexture;

        /// <summary>
        /// The private field used for the <see cref="RemoveTexture"/> property.
        /// </summary>
        private static BitmapSource _removeTexture;

        /// <summary>
        /// The private field used for the <see cref="LocalConvert"/> property.
        /// </summary>
        private static BitmapSource _localConvert;

        /// <summary>
        /// The private field used for the <see cref="QueueOnAssetBuilder"/> property.
        /// </summary>
        private static BitmapSource _queueOnAssetBuilder;

        /// <summary>
        /// A generic object that provides thread safety access to the image sources.
        /// </summary>
        private static object _syncRoot = new object();
        #endregion

        #region Properties
        /// <summary>
        /// Gets the icon that shows an add texture icon.
        /// </summary>
        public static BitmapSource AddTexture
        {
            get
            {
                if (_addTexture == null)
                {
                    lock (_syncRoot)
                    {
                        if (_addTexture == null)
                        {
                            _addTexture = EnsureLoaded("addTexture16.png");
                        }
                    }
                }

                return _addTexture;
            }
        }

        /// <summary>
        /// Gets the icon that shows a remove texture icon.
        /// </summary>
        public static BitmapSource RemoveTexture
        {
            get
            {
                if (_removeTexture == null)
                {
                    lock (_syncRoot)
                    {
                        if (_removeTexture == null)
                        {
                            _removeTexture = EnsureLoaded("removeSelected16.png");
                        }
                    }
                }

                return _removeTexture;
            }
        }

        /// <summary>
        /// Gets the icon that shows a local convert icon.
        /// </summary>
        public static BitmapSource LocalConvert
        {
            get
            {
                if (_localConvert == null)
                {
                    lock (_syncRoot)
                    {
                        if (_localConvert == null)
                        {
                            _localConvert = EnsureLoaded("localConvert16.png");
                        }
                    }
                }

                return _localConvert;
            }
        }

        /// <summary>
        /// Gets the icon that shows a queue on asset builder icon.
        /// </summary>
        public static BitmapSource QueueOnAssetBuilder
        {
            get
            {
                if (_queueOnAssetBuilder == null)
                {
                    lock (_syncRoot)
                    {
                        if (_queueOnAssetBuilder == null)
                        {
                            _queueOnAssetBuilder = EnsureLoaded("queueOnAssetBuilder16.png");
                        }
                    }
                }

                return _queueOnAssetBuilder;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Ensures that the specified image source is loaded with the specified resource name.
        /// </summary>
        /// <param name="resourceName">
        /// The name of the resource the loaded image source should set as its source.
        /// </param>
        private static BitmapSource EnsureLoaded(String resourceName)
        {
            String assemblyName = typeof(MaterialToolkitIcons).Assembly.GetName().Name;
            String bitmapPath = String.Format("pack://application:,,,/{0};component/Resources/{1}", assemblyName, resourceName);
            Uri uri = new Uri(bitmapPath, UriKind.RelativeOrAbsolute);

            BitmapSource source = new BitmapImage(uri);
            source.Freeze();
            return source;
        }
        #endregion
    }
}
