﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Editor;

namespace MaterialToolkit.Commands
{
    /// <summary>
    /// Action for converting the selected scenes.
    /// </summary>
    public class LocalConvertAction : ButtonAction<MainWindowDataContext>
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="LocalConvertAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public LocalConvertAction(ParameterResolverDelegate<MainWindowDataContext> resolver)
            : base(resolver)
        {
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// Override to set logic against the can execute command handler.
        /// </summary>
        /// <param name="vm"></param>
        /// <returns></returns>
        public override bool CanExecute(MainWindowDataContext vm)
        {
            // We can execute this if there isn't a conversion already in progress and
            // the user has some scenes selected for conversion.
            return !vm.ConversionInProgress && vm.Scenes.Any(item => item.Convert);
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="vm">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(MainWindowDataContext vm)
        {
            IPerforceService perforceService = this.GetService<IPerforceService>();
            if (perforceService == null)
            {
                throw new ArgumentNullException("perforceService");
            }

            vm.StartConversion(perforceService);
        }
        #endregion
    }
}
