﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Editor;

namespace MaterialToolkit.Commands
{
    /// <summary>
    /// Action for removing a list of textures from the texture list.
    /// </summary>
    public class RemoveTextureAction : ButtonAction<RemoveTextureCommandArgs>
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="RemoveTextureAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public RemoveTextureAction(ParameterResolverDelegate<RemoveTextureCommandArgs> resolver)
            : base(resolver)
        {
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// Override to set logic against the can execute command handler.
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public override bool CanExecute(RemoveTextureCommandArgs args)
        {
            return !args.MainWindowDataContext.ConversionInProgress && args.SelectedTextureViewModels.Any();
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="args">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(RemoveTextureCommandArgs args)
        {
            args.MainWindowDataContext.RemoveTextures(args.SelectedTextureViewModels);
        }
        #endregion
    }
}
