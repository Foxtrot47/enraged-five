﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using RSG.Base.OS;
using RSG.Base.Extensions;
using RSG.Editor.Controls;
using RSG.Base.Logging.Universal;
using RSG.Base.Logging;
using RSG.Base.Configuration;
using RSG.Editor;
using RSG.Editor.Controls.Perforce;

namespace MaterialToolkit
{

    /// <summary>
    /// Defines the main entry point to the application and is responsible for creating and
    /// running the application.
    /// </summary>
    public class App : RsApplication, ICommandOptions
    {
        #region Program Entry Point
        /// <summary>
        /// Main entry point into the application. The first thing to get called.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            App app = new App();
            app.ShutdownMode = ShutdownMode.OnMainWindowClose;
            RsApplication.OnEntry(app, ApplicationMode.Multiple);
        }
        #endregion

        #region Fields
        /// <summary>
        /// The private reference to the perforce service that this application exposes to the
        /// command actions.
        /// </summary>
        private readonly IPerforceService _perforceService;

        /// <summary>
        /// Private field for the <see cref="Config"/> property.
        /// </summary>
        private IConfig _config;

        /// <summary>
        /// Private field for the <see cref="CoreProject"/> property.
        /// </summary>
        private IProject _coreProject;

        /// <summary>
        /// Private field for the <see cref="Project"/> property.
        /// </summary>
        private IProject _project;

        /// <summary>
        /// Private field for the <see cref="Branch"/> property.
        /// </summary>
        private IBranch _branch;
        #endregion // Fields

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="App"/> class.
        /// </summary>
        public App()
            : base()
        {
            // Register command line arguments.
            RegisterCommandlineArg("server", LongOption.ArgType.Required, "Statistics server to connect to.");
            RegisterCommandlineArg("project", LongOption.ArgType.Required, "Project to use (e.g. gta5, rdr3, gta5_liberty.");
            RegisterCommandlineArg("branch", LongOption.ArgType.Required, "Project branch to use (e.g. dev, release).");
            RegisterCommandlineArg("dlc", LongOption.ArgType.Required, "Project DLC to use (e.g. dlc_w_ar_heavyrifle).");

            // Setup the perforce service.
            this._perforceService = new PerforceService(TimeSpan.FromMinutes(15));
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the unique name for this application that is used for mutex creation, and
        /// folder organisation inside the local app data folder and the registry.
        /// </summary>
        public override string UniqueName
        {
            get { return "RSG.Pipeline.MaterialToolkit"; }
        }

        /// <summary>
        /// Gets a value indicating whether the layout for the main window get serialised on
        /// exit and deserialised during loading.
        /// </summary>
        protected override bool MakeLayoutPersistent
        {
            get { return true; }
        }

        /// <summary>
        /// Config data.
        /// </summary>
        public IConfig Config
        {
            get { return _config; }
        }

        /// <summary>
        /// Active core project.
        /// </summary>
        public IProject CoreProject
        {
            get { return _coreProject; }
        }

        /// <summary>
        /// Active project.
        /// </summary>
        public IProject Project
        {
            get { return _project; }
        }

        /// <summary>
        /// Set branch.
        /// </summary>
        public IBranch Branch
        {
            get { return _branch; }
        }

        /// <summary>
        /// Flag indicating whether the user wants to accept the default value for all message
        /// boxes by default.
        /// </summary>
        public bool NoPopups
        {
            get { return CommandLineOptions.HasOption("nopopups"); }
        }

        /// <summary>
        /// Retrieve the parsed command line options.
        /// </summary>
        public Getopt AllOptions
        {
            get { return CommandLineOptions; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Override to create the main window for the application.
        /// </summary>
        /// <returns>
        /// The System.Windows.Window that will be the main window for this application.
        /// </returns>
        protected override Window CreateMainWindow()
        {
            return new MainWindow(this);
        }

        /// <summary>
        /// Called immediately after the main windows Show method has been called.
        /// </summary>
        /// <param name="mainWindow">
        /// A reference to the main window that was shown.
        /// </param>
        /// <returns>
        /// A task representing the work done by this method.
        /// </returns>
        protected async override Task OnMainWindowShown(Window mainWindow)
        {
            MainWindowDataContext dc = this.MainWindow.DataContext as MainWindowDataContext;
            if (dc == null)
            {
                Debug.Assert(
                    dc != null,
                    "Unable to complete startup operations as the data context is missing");
                return;
            }

            // Determine the list of files to open.
            IList<String> filenames = new List<String>();
            foreach (String pathname in this.TrailingArguments)
            {
                if (String.IsNullOrEmpty(pathname))
                {
                    continue;
                }

                if (Directory.Exists(pathname))
                {
                    filenames.AddRange(Directory.GetFiles(pathname).Where(item => TextureUtil.HasSupportedFileExtension(item)));
                }
                else if (File.Exists(pathname))
                {
                    filenames.Add(pathname);
                }
            }

            IPerforceService perforceService = this.GetService<IPerforceService>();
            if (perforceService == null)
            {
                throw new ArgumentNullException("perforceService");
            }

            if (filenames.Any())
            {
                await dc.AddTextures(filenames, perforceService);
            }
        }

        /// <summary>
        /// Parses the command line options initialising the branch/project/config information.
        /// </summary>
        /// <param name="e">
        /// The event data.
        /// </param>
        protected override void OnStartup(StartupEventArgs e)
        {
            // Parse the command line options we are interested in.
            try
            {
                String projectKey = CommandLineOptions.HasOption("project") ? (String)CommandLineOptions["project"] : String.Empty;
                String dlcKey = CommandLineOptions.HasOption("dlc") ? (String)CommandLineOptions["dlc"] : String.Empty;
                _config = ConfigFactory.CreateConfig(dlcKey);

                _coreProject = this.Config.CoreProject;
                _project = this.Config.Project;
                if (CommandLineOptions.HasOption("branch"))
                {
                    String branchKey = (String)CommandLineOptions["branch"];
                    Debug.Assert(this.Config.Project.Branches.ContainsKey(branchKey),
                        "Branch key not found.",
                        "Branch key '{0}' not found; default branch {1} will be used.",
                        branchKey,
                        this.Config.Project.DefaultBranchName);

                    if (this.Config.Project.Branches.ContainsKey(branchKey))
                    {
                        _branch = this.Config.Project.Branches[branchKey];
                    }
                    else
                    {
                        _branch = this.Config.Project.DefaultBranch;
                    }
                }
                else
                {
                    _branch = this.Config.Project.DefaultBranch;
                }
            }
            catch (ConfigurationVersionException ex)
            {
                Log.ToolException(ex,
                    "Configuration version error: run tools installer.  Installed version: {0}, expected version: {1}.",
                    ex.ActualVersion, ex.ExpectedVersion);
                if (!this.NoPopups)
                {
                    String msg = String.Format("An important update has been made to the tool chain.  Install version: {0}, expected version: {1}.{2}{2}Sync latest or labelled tools, run {3} and then restart the application.",
                        ex.ActualVersion, ex.ExpectedVersion, System.Environment.NewLine,
                        Path.Combine(System.Environment.GetEnvironmentVariable("RS_TOOLSROOT"), "install.bat"));
                    RsMessageBox.Show(msg, "Tools Version Problem", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                ForceExit();
            }
            catch (ConfigurationException ex)
            {
                Log.ToolException(ex, "Configuration parse error.");
                if (!this.NoPopups)
                {
                    String msg = String.Format("There was an error initialising configuration data.{0}{0}Sync latest or labelled tools and restart the application.",
                        System.Environment.NewLine);
                    RsMessageBox.Show(msg, "Tools Version Problem", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                ForceExit();
            }

            // Perform other startup stuff after this point.
            base.OnStartup(e);
        }

        /// <summary>
        /// Gets the service object of the specified type making sure that before the base is
        /// called the <see cref="RSG.Project.ViewModel.IProjectAddViewService"/> interface is
        /// handled.
        /// </summary>
        /// <param name="serviceType">
        /// An object that specifies the type of service object to get.
        /// </param>
        /// <returns>
        /// The service of the specified type if found; otherwise, null.
        /// </returns>
        public override object GetService(Type serviceType)
        {
            if (serviceType == typeof(IPerforceService))
            {
                return this._perforceService;
            }
            else
            {
                return base.GetService(serviceType);
            }
        }
        #endregion
    }
}
