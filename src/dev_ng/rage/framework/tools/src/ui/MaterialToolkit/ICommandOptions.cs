﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Configuration;
using RSG.Base.OS;

namespace MaterialToolkit
{
    /// <summary>
    /// Common command line options.
    /// </summary>
    public interface ICommandOptions
    {
        /// <summary>
        /// Config data.
        /// </summary>
        IConfig Config { get; }

        /// <summary>
        /// Active core project.
        /// </summary>
        IProject CoreProject { get; }

        /// <summary>
        /// Active project.
        /// </summary>
        IProject Project { get; }

        /// <summary>
        /// Set branch.
        /// </summary>
        IBranch Branch { get; }

        /// <summary>
        /// Set when user specifies no popups option.
        /// </summary>
        bool NoPopups { get; }

        /// <summary>
        /// Retrieve the parsed command line options.
        /// </summary>
        Getopt AllOptions { get; }
    }
}
