﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using RSG.Editor;

namespace MaterialToolkit.Commands
{
    /// <summary>
    /// Action for converting and submitting exported textures and then queuing up the
    /// conversion of the required containers on the asset builder.
    /// </summary>
    public class QueueOnAssetBuilderAction : ButtonAction<MainWindowDataContext>
    {
        #region Constants
        /// <summary>
        /// Path to the remote convert executable.
        /// </summary>
        private const String _remoteConvertExe = @"%RS_TOOLSROOT%\ironlib\lib\RSG.Pipeline.Automation.RemoteConvert.exe";
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="QueueOnAssetBuilderAction"/> class.
        /// </summary>
        /// <param name="resolver">
        /// The resolver that this class uses to obtain the command parameter from.
        /// </param>
        public QueueOnAssetBuilderAction(ParameterResolverDelegate<MainWindowDataContext> resolver)
            : base(resolver)
        {
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Override to set logic against the can execute command handler.
        /// </summary>
        /// <param name="vm"></param>
        /// <returns></returns>
        public override bool CanExecute(MainWindowDataContext vm)
        {
            // We can execute this if there isn't a conversion already in progress and
            // the user has some scenes selected for conversion.
            return !vm.ConversionInProgress && vm.Scenes.Any(item => item.Convert);
        }

        /// <summary>
        /// Override to set logic against the execute command handler.
        /// </summary>
        /// <param name="vm">
        /// The command parameter that has been requested.
        /// </param>
        public override void Execute(MainWindowDataContext vm)
        {
            // Get references to the services we require.
            IMessageBoxService messageService = this.GetService<IMessageBoxService>();
            if (messageService == null)
            {
                throw new ArgumentException("messageService");
            }

            IPerforceService perforceService = this.GetService<IPerforceService>();
            if (perforceService == null)
            {
                throw new ArgumentNullException("perforceService");
            }

            // First convert the textures locally.
            IList<Process> textureConvertProcesses = new List<Process>();

            IList<KeyValuePair<String, String>> texturesToExport =
                vm.Textures.SelectMany(texVm => texVm.ExportPaths).Distinct().ToList();

            // Check out the files from p4.
            IList<String> exportPaths = texturesToExport.Select(item => item.Value).ToList();
            perforceService.GetLatest(exportPaths);
            perforceService.EditFiles(exportPaths);

            foreach (KeyValuePair<String, String> exportPair in texturesToExport)
            {
                Process textureConvertProcess = new Process();
                textureConvertProcess.StartInfo.FileName = Environment.ExpandEnvironmentVariables(MainWindowDataContext.TextureExportExe);
                textureConvertProcess.StartInfo.Arguments = String.Format(
                    "-input={0} -output={1}",
                    exportPair.Key,
                    exportPair.Value);
                textureConvertProcess.StartInfo.CreateNoWindow = true;
                textureConvertProcesses.Add(textureConvertProcess);
            }

            // Start the texture conversions.
            foreach (Process textureConvertProcess in textureConvertProcesses)
            {
                textureConvertProcess.Start();
            }

            // Revert any of the textures that haven't changed.
            perforceService.RevertUnchangedFiles(exportPaths);

            // Inform the user which textures will be checked in.
            IList<string> checkedOutFiles = perforceService.GetCheckedOutFiles(exportPaths);
            if (checkedOutFiles.Any())
            {
                String texturesToConvertMessage;
                if (checkedOutFiles.Count > 10)
                {
                    texturesToConvertMessage = String.Format("{0}\n+{1} more",
                        String.Join("\n", checkedOutFiles.Take(10)),
                        checkedOutFiles.Count - 10);
                }
                else
                {
                    texturesToConvertMessage = String.Join("\n", checkedOutFiles);
                }

                String textureMessage = String.Format("The following textures have been updated and need to be submitted " +
                    "to perforce before queueing the convert on the asset builder:\n\n{0}\n\nDo you wish to " + 
                    "check these files in and continue?",
                    String.Join("\n", exportPaths));
                String textureCaption = "Textures to Submit";

                MessageBoxResult textureResult =
                    messageService.Show(textureMessage, textureCaption, MessageBoxButton.YesNoCancel, MessageBoxImage.Question);

                if (textureResult != MessageBoxResult.Yes)
                {
                    return;
                }

                int changelistId = perforceService.CreateChangelist("Material toolkit texture auto submit");
                perforceService.MoveFilesIntoChangelist(checkedOutFiles, changelistId);
                perforceService.SubmitChangelist(changelistId);
            }

            // Final confirmation with the user as to which containers will be sent for confirmation.
            IList<String> scenesToConvert =
                vm.Scenes.Where(item => item.Convert).Select(item => Path.ChangeExtension(item.Filepath, "zip")).ToList();

            String scenesToConvertMessage;
            if (scenesToConvert.Count > 10)
            {
                scenesToConvertMessage = String.Format("{0}\n+{1} more",
                    String.Join("\n", scenesToConvert.Take(10)),
                    scenesToConvert.Count - 10);
            }
            else
            {
                scenesToConvertMessage = String.Join("\n", scenesToConvert);
            }

            String convertMessage = String.Format("The following containers will be queued for conversion on the asset " +
                "builder:\n\n{0}\n\nPlease confirm this operation by selecting 'Yes' below.",
                    scenesToConvertMessage);
            String convertCaption = "Confirm Send to Asset Builder";

            MessageBoxResult convertResult =
                messageService.Show(convertMessage, convertCaption, MessageBoxButton.YesNoCancel, MessageBoxImage.Question);

            if (convertResult != MessageBoxResult.Yes)
            {
                return;
            }

            // Kick off the remote conversion.
            Process remoteConvertProcess = new Process();
            remoteConvertProcess.StartInfo.FileName = Environment.ExpandEnvironmentVariables(_remoteConvertExe);
            remoteConvertProcess.StartInfo.Arguments = String.Format("--branch {0} {1}", vm.Branch.Name,
                String.Join(" ", vm.Scenes.Where(item => item.Convert).Select(item => Path.ChangeExtension(item.Filepath, "zip"))));
            remoteConvertProcess.Start();

            remoteConvertProcess.WaitForExit();

            if (remoteConvertProcess.ExitCode == 0)
            {
                messageService.Show("The containers were successfully queued up on the asset builder.");
            }
            else
            {
                String failureMessage = String.Format("A problem occurred while attempting to queue the containers on the " +
                    "asset builder.\n\nThe {0} process returned a non-zero return code: {1}.",
                    remoteConvertProcess.StartInfo.FileName,
                    remoteConvertProcess.ExitCode);
                messageService.Show(failureMessage, "Asset Builder Queue Failed", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        #endregion
    }
}
