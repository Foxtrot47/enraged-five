﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using SDiag = System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using RSG.Base.Configuration;
using RSG.Base.Configuration.Services;
using RSG.Base.Extensions;
using RSG.Editor;
using RSG.Pipeline.Content;
using RSG.Pipeline.Core;
using RSG.SceneXml;
using RSG.SceneXml.Material;
using RSG.Statistics.Client2;
using RSG.Statistics.Common.Config;
using SceneXmlDto = RSG.Statistics.Common.Dto.SceneXml;
using System.Threading;

namespace MaterialToolkit
{
    /// <summary>
    /// View model for the <see cref="MainWindow"/> class.
    /// </summary>
    public class MainWindowDataContext : NotifyPropertyChangedBase
    {
        #region Constants
        /// <summary>
        /// Path to the pipeline convert executable.
        /// </summary>
        private const String _pipelineConvertExe = @"%RS_TOOLSROOT%\ironlib\lib\RSG.Pipeline.Convert.exe";

        /// <summary>
        /// Path to the texture export executable.
        /// </summary>
        public const String TextureExportExe = @"%RS_TOOLSROOT%\ironlib\bin\TextureExport.exe";
        #endregion

        #region Fields
        /// <summary>
        /// Reference to the statistics server that we should connect to.
        /// </summary>
        private readonly IServer _statisticsServer;

        /// <summary>
        /// Private field for the <see cref="Branch"/> property.
        /// </summary>
        private readonly IBranch _branch;

        /// <summary>
        /// Private field for the <see cref="Textures"/> property.
        /// </summary>
        private readonly ObservableCollection<TextureViewModel> _textures = new ObservableCollection<TextureViewModel>();

        /// <summary>
        /// Private field for the <see cref="Scenes"/> property.
        /// </summary>
        private readonly ObservableCollection<SceneViewModel> _scenes = new ObservableCollection<SceneViewModel>();

        /// <summary>
        /// Private field for the <see cref="StatusText"/> property.
        /// </summary>
        private String _statusText;

        /// <summary>
        /// Process for the current conversion.
        /// </summary>
        private SDiag.Process _convertProcess;

        /// <summary>
        /// Texture conversion processes.
        /// </summary>
        private readonly IList<SDiag.Process> _textureConversions = new List<SDiag.Process>();

        /// <summary>
        /// Number of texture conversions that have finished.
        /// </summary>
        private uint _textureConversionsCompleted;

        /// <summary>
        /// Private field for the <see cref="ConvertAllScenes"/> property.
        /// </summary>
        private bool _convertAllScenes = true;

        /// <summary>
        /// List of files that we detected as being locally modified.
        /// </summary>
        private IList<Scene> _locallyModifiedScenes;

        /// <summary>
        /// Lock object for loading the content tree.
        /// </summary>
        private object _locallyModifiedScenesLock = new object();

        /// <summary>
        /// Private field for the <see cref="ForceTextureExport"/> property.
        /// </summary>
        private bool _forceTextureExport = false;
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="MainWindowDataContext"/> class.
        /// </summary>
        /// <param name="statisticsServer"></param>
        /// <param name="branch"></param>
        public MainWindowDataContext(IServer statisticsServer, IBranch branch)
        {
            _statisticsServer = statisticsServer;
            _branch = branch;
            _statusText = "Ready";
        }
        #endregion

        #region Properties
        /// <summary>
        /// Reference to the branch we are working on.
        /// </summary>
        public IBranch Branch
        {
            get { return _branch; }
        }

        /// <summary>
        /// List of textures the user is interested in updating.
        /// </summary>
        public IReadOnlyCollection<TextureViewModel> Textures
        {
            get { return _textures; }
        }

        /// <summary>
        /// List of scenes that will be converted for the selected list of textures.
        /// </summary>
        public IReadOnlyCollection<SceneViewModel> Scenes
        {
            get { return _scenes; }
        }

        /// <summary>
        /// Text to display in the status bar.
        /// </summary>
        public String StatusText
        {
            get { return _statusText; }
            set { SetProperty(ref _statusText, value); }
        }

        /// <summary>
        /// Property for determining whether a conversion is currently in progress.
        /// </summary>
        public bool ConversionInProgress
        {
            get { return _textureConversions.Any() || _convertProcess != null; }
        }

        /// <summary>
        /// Property for determining whether no conversions are currently in progress.
        /// </summary>
        public bool NoConversionInProgress
        {
            get { return !_textureConversions.Any() && _convertProcess == null; }
        }

        /// <summary>
        /// Helper property for enabling/disabling the conversion for all scenes.
        /// </summary>
        public bool ConvertAllScenes
        {
            get { return _convertAllScenes; }
            set
            {
                if (SetProperty(ref _convertAllScenes, value))
                {
                    foreach (SceneViewModel sceneVm in Scenes)
                    {
                        sceneVm.Convert = value;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the force texture export option.
        /// </summary>
        public bool ForceTextureExport
        {
            get { return _forceTextureExport; }
            set { SetProperty(ref _forceTextureExport, value); }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Adds a list of textures that we wish to convert.
        /// </summary>
        /// <param name="textures"></param>
        /// <param name="perforceService"></param>
        public async Task AddTextures(IEnumerable<String> textures, IPerforceService perforceService)
        {   
            // Create the list of tasks to get the data we are after.
            IList<TextureViewModel> textureVms = new List<TextureViewModel>();
            foreach (String texture in textures)
            {
                String texturePath = Path.GetFullPath(texture).ToLower();

                if (!Textures.Any(item => item.Filepath == texturePath))
                {
                    TextureViewModel textureVm = new TextureViewModel(texturePath);
                    _textures.Add(textureVm);
                    textureVms.Add(textureVm);
                }
            }
            
            // Wait for all the query tasks to complete.
            Task dbTask = QueryDatabase(textureVms);
            Task localTask = QueryLocal(textureVms, perforceService);
            await Task.WhenAll(dbTask, localTask);
        }

        /// <summary>
        /// Queries the database for the list of scenes that the specified textures are used in.
        /// </summary>
        /// <param name="textureVms"></param>
        /// <returns></returns>
        private async Task QueryDatabase(IEnumerable<TextureViewModel> textureVms)
        {
            // Create the client we'll be using to query the service.
            SceneXmlQueryClient client = new SceneXmlQueryClient(_statisticsServer);

            try
            {
                // Create a filepath => vm lookup.
                IDictionary<string, TextureViewModel> vmLookup = textureVms.ToDictionary(item => item.Filepath);

                // Query the server of the texture usage information.
                IList<SceneXmlDto.TextureUsage> textureUsage = await client.GetTextureUsageAsync(vmLookup.Keys);

                // Group the results by texture and then set the vm values.
                foreach (IGrouping<string, SceneXmlDto.TextureUsage> group in textureUsage.GroupBy(item => item.TextureName))
                {
                    TextureViewModel vm;
                    if (vmLookup.TryGetValue(group.Key.ToLower(), out vm))
                    {
                        vm.SetDatabaseScenes(group.Select(item => item.SceneName.ToLower()));
                    }
                }

                // Get the texture pair information.
                IList<SceneXmlDto.TexturePair> texturePairs = await client.GetTexturePairsAsync(vmLookup.Keys);

                foreach (SceneXmlDto.TexturePair texturePair in texturePairs)
                {
                    string inputPath;
                    String exportPath;
                    if (!String.IsNullOrEmpty(texturePair.AlphaTexturePath))
                    {
                        inputPath = String.Format("{0}+{1}", texturePair.TexturePath, texturePair.AlphaTexturePath);
                        exportPath = Path.Combine(
                            _branch.Assets,
                            "maps",
                            "textures",
                            String.Format("{0}{1}.tif",
                                Path.GetFileNameWithoutExtension(texturePair.TexturePath),
                                Path.GetFileNameWithoutExtension(texturePair.AlphaTexturePath)));
                    }
                    else
                    {
                        inputPath = texturePair.TexturePath;
                        exportPath = Path.Combine(
                            _branch.Assets,
                            "maps",
                            "textures",
                            Path.GetFileNameWithoutExtension(texturePair.TexturePath) + ".tif");
                    }

                    TextureViewModel vm;
                    if (vmLookup.TryGetValue(texturePair.TexturePath.ToLower(), out vm))
                    {
                        vm.AddExportPath(inputPath, exportPath);
                    }

                    if (!String.IsNullOrEmpty(texturePair.AlphaTexturePath))
                    {
                        if (vmLookup.TryGetValue(texturePair.AlphaTexturePath.ToLower(), out vm))
                        {
                            vm.AddExportPath(inputPath, exportPath);
                        }
                    }
                }

                foreach (TextureViewModel textureVm in textureVms)
                {
                    textureVm.DatabaseValueSet = true;
                }

                // Make sure we have scene view models for all the scenes that were returned.
                AddScenes(textureUsage.Select(item => item.SceneName).Distinct());
            }
            finally
            {
                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }
            }
        }

        /// <summary>
        /// Checks your locally modified scene xml files to determine where the specified texture
        /// is being used.
        /// </summary>
        /// <param name="textureVm"></param>
        /// <param name="perforceService"></param>
        /// <returns></returns>
        private async Task QueryLocal(IEnumerable<TextureViewModel> textureVms, IPerforceService perforceService)
        {
            // Make sure the locally modified files have been loaded.
            await Task.Factory.StartNew(() => LoadLocallyModifiedFiles(perforceService));

            // Parse the scene xml files.
            // Does this need to be async?
            foreach (TextureViewModel textureVm in textureVms)
            {
                IList<String> scenes = new List<String>();
                foreach (Scene scene in _locallyModifiedScenes)
                {
                    IEnumerable<MaterialDef> materialsUsingTexture = GetMaterialsUsingTexture(scene, textureVm.Filepath);
                    if (materialsUsingTexture.Any())
                    {
                        scenes.Add(scene.Filename);

                        foreach (MaterialDef materialDef in materialsUsingTexture)
                        {
                            foreach (KeyValuePair<String, String> pair in DetermineExportPaths(materialDef, textureVm.Filepath))
                            {
                                textureVm.AddExportPath(pair.Key, pair.Value);
                            }
                        }
                    }
                }

                textureVm.SetLocalScenes(scenes);
                AddScenes(scenes);
            }

        }

        /// <summary>
        /// Determines which scenes the user has locally checked out and loads them into memory.
        /// </summary>
        /// <param name="p4Service"></param>
        private void LoadLocallyModifiedFiles(IPerforceService perforceService)
        {
            if (_locallyModifiedScenes == null)
            {
                lock (_locallyModifiedScenesLock)
                {
                    if (_locallyModifiedScenes == null)
                    {
                        // Get the list of scene xml files from the content tree.
                        IProcessorCollection processors = new ProcessorCollection(_branch.Project.Config);
                        IContentTree contentTree = RSG.Pipeline.Content.Factory.CreateTree(_branch);
                        ContentTreeHelper helper = new ContentTreeHelper(contentTree);

                        IContentNode[] sceneXmlNodes = helper.GetAllMapSceneXmlNodes();
                        IList<String> sceneXmlFiles = sceneXmlNodes.OfType<IFilesystemNode>().Select(item => item.AbsolutePath).ToList();

                        // Get the list of files the user has checked out under the export path.
                        String fstatPath = String.Format("{0}....xml", _branch.Export);
                        IList<String> checkedOutFiles = perforceService.GetCheckedOutFiles(new String[] { fstatPath });

                        // TODO: Parallelise this?
                        _locallyModifiedScenes = new List<Scene>();

                        foreach (String sceneXmlFile in checkedOutFiles.Intersect(sceneXmlFiles))
                        {
                            Scene scene = new Scene(contentTree, helper, sceneXmlFile, LoadOptions.All, true);
                            _locallyModifiedScenes.Add(scene);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Determines whether a material (or any of its sub-materials) makes use of the
        /// specified texture.
        /// </summary>
        /// <param name="materialDef"></param>
        /// <param name="texture"></param>
        /// <returns></returns>
        private IEnumerable<MaterialDef> GetMaterialsUsingTexture(Scene scene, String texture)
        {
            IList<MaterialDef> materials = new List<MaterialDef>();
            foreach (MaterialDef materialDef in scene.Materials)
            {
                GetMaterialsUsingTexture(materialDef, texture, materials);
            }
            return materials;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="materialDef"></param>
        /// <param name="texture"></param>
        /// <param name="materials"></param>
        private void GetMaterialsUsingTexture(MaterialDef materialDef, String texture, IList<MaterialDef> materials)
        {
            if (materialDef.HasSubMaterials)
            {
                foreach (MaterialDef subMaterialDef in materialDef.SubMaterials)
                {
                    GetMaterialsUsingTexture(subMaterialDef, texture, materials);
                }
            }

            if (materialDef.HasTextures)
            {
                foreach (TextureDef textureDef in materialDef.Textures)
                {
                    String textureDefPath = Path.GetFullPath(textureDef.FilePath);

                    if (String.Equals(textureDefPath, texture, StringComparison.InvariantCultureIgnoreCase))
                    {
                        materials.Add(materialDef);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="material"></param>
        /// <param name="filepath"></param>
        /// <returns></returns>
        private IEnumerable<KeyValuePair<String, String>> DetermineExportPaths(MaterialDef material, String filename)
        {
            for (int i = 0; i < material.Textures.Length; i++)
            {
                TextureDef currentTexture = material.Textures[i];
                TextureDef nextTexture = ((i + 1) < material.Textures.Length ? material.Textures[i + 1] : null);

                TextureTypes currentType = currentTexture.Type;
                String currentFilepath = Path.GetFullPath(currentTexture.FilePath);

                if (nextTexture != null)
                {
                    TextureTypes nextType = nextTexture.Type;
                    String nextFilepath = Path.GetFullPath(nextTexture.FilePath);

                    if (currentType == TextureTypes.DiffuseMap && nextType == TextureTypes.DiffuseAlpha ||
                        currentType == TextureTypes.SpecularMap && nextType == TextureTypes.SpecularAlpha ||
                        currentType == TextureTypes.BumpMap && nextType == TextureTypes.BumpAlpha)
                    {
                        if (String.Equals(currentFilepath, filename, StringComparison.InvariantCultureIgnoreCase) ||
                            String.Equals(nextFilepath, filename, StringComparison.InvariantCultureIgnoreCase))
                        {

                            String inputPath = String.Format("{0}+{1}", currentFilepath, nextFilepath);

                            String exportPath = Path.Combine(
                                _branch.Assets,
                                "maps",
                                "textures",
                                String.Format("{0}{1}.tif",
                                    Path.GetFileNameWithoutExtension(currentFilepath),
                                    Path.GetFileNameWithoutExtension(nextFilepath)));

                            yield return new KeyValuePair<String, String>(inputPath, exportPath);
                        }
                        ++i;
                    }
                    else if (currentType == TextureTypes.DiffuseAlpha && nextType == TextureTypes.DiffuseMap ||
                        currentType == TextureTypes.SpecularAlpha && nextType == TextureTypes.SpecularMap ||
                        currentType == TextureTypes.BumpAlpha && nextType == TextureTypes.BumpMap)
                    {
                        if (String.Equals(currentFilepath, filename, StringComparison.InvariantCultureIgnoreCase) ||
                            String.Equals(nextFilepath, filename, StringComparison.InvariantCultureIgnoreCase))
                        {
                            String inputPath = String.Format("{0}+{1}", nextFilepath, currentFilepath);

                            String exportPath = Path.Combine(
                                _branch.Assets,
                                "maps",
                                "textures",
                                String.Format("{0}{1}.tif",
                                    Path.GetFileNameWithoutExtension(nextFilepath),
                                    Path.GetFileNameWithoutExtension(currentFilepath)));

                            yield return new KeyValuePair<String, String>(inputPath, exportPath);
                        }
                        ++i;
                    }
                    else if (String.Equals(currentFilepath, filename, StringComparison.InvariantCultureIgnoreCase))
                    {
                        String inputPath = filename;
                        String exportPath = Path.Combine(
                            _branch.Assets,
                            "maps",
                            "textures",
                            Path.GetFileNameWithoutExtension(filename) + ".tif");

                        yield return new KeyValuePair<String, String>(inputPath, exportPath);
                    }
                }
                else if (String.Equals(currentFilepath, filename, StringComparison.InvariantCultureIgnoreCase))
                {
                    String inputPath = filename;
                    String exportPath = Path.Combine(
                        _branch.Assets,
                        "maps",
                        "textures",
                        Path.GetFileNameWithoutExtension(filename) + ".tif");

                    yield return new KeyValuePair<String, String>(inputPath, exportPath);
                }
            }
        }

        /// <summary>
        /// Removes a list of texture view models.
        /// </summary>
        /// <param name="vms"></param>
        public void RemoveTextures(IEnumerable<TextureViewModel> vms)
        {
            foreach (TextureViewModel vm in vms)
            {
                _textures.Remove(vm);
            }

            ISet<String> validSceneNames = _textures.SelectMany(item => item.DatabaseScenes).ToSet();
            validSceneNames.AddRange(_textures.SelectMany(item => item.LocalScenes));

            // Determine the list of scenes that are no longer present.
            IList<SceneViewModel> scenesToRemove = new List<SceneViewModel>();
            foreach (SceneViewModel scene in _scenes)
            {
                if (!validSceneNames.Contains(scene.Filepath))
                {
                    scenesToRemove.Add(scene);
                }
            }

            foreach (SceneViewModel sceneToRemove in scenesToRemove)
            {
                _scenes.Remove(sceneToRemove);
            }

            CommandManager.InvalidateRequerySuggested();
        }

        /// <summary>
        /// Requeries the database for texture usage and refreshes the locally modified
        /// file scene xml.
        /// </summary>
        /// <returns></returns>
        public async Task RefreshTextureUsage(IPerforceService perforceService)
        {
            // Create the client we'll be using to query the service.
            SceneXmlQueryClient client = new SceneXmlQueryClient(_statisticsServer);

            // Clear out the scenes the textures are using.
            foreach (TextureViewModel vm in _textures)
            {
                vm.ResetScenes();
            }
            _scenes.Clear();

            // Clear the locally modified scenes to force them to be reloaded.
            lock (_locallyModifiedScenesLock)
            {
                _locallyModifiedScenes = null;
            }

            // Create the list of tasks to get the data we are after.
            IList<Task> tasks = new List<Task>();
            foreach (TextureViewModel vm in _textures)
            {
                //Task dbTask = QueryDatabase(vm, client);
                //tasks.Add(dbTask);

                //Task localTask = QueryLocal(vm, perforceService);
                //tasks.Add(localTask);
            }

            // Wait for all the query tasks to complete before closing the client.
            try
            {
                await Task.WhenAll(tasks);
            }
            finally
            {
                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }
            }
        }

        /// <summary>
        /// Adds a list of scene filepaths to the scene view model collection.
        /// </summary>
        /// <param name="scenes"></param>
        private void AddScenes(IEnumerable<String> scenes)
        {
            ISet<String> existingSceneNames = _scenes.Select(item => item.Filepath.ToLower()).ToSet();
            foreach (String scene in scenes)
            {
                if (!existingSceneNames.Contains(scene))
                {
                    _scenes.Add(new SceneViewModel(scene));
                }
            }
        }

        /// <summary>
        /// Kicks off the conversion for the selected scenes.
        /// </summary>
        /// <param name="perforceService"></param>
        public void StartConversion(IPerforceService perforceService)
        {
            // Convert all the textures first.
            _textureConversions.Clear();
            _textureConversionsCompleted = 0;

            IList<KeyValuePair<String, String>> texturesToExport =
                Textures.SelectMany(vm => vm.ExportPaths).Distinct().ToList();
            foreach (KeyValuePair<String, String> exportPair in texturesToExport)
            {
                String args = String.Format(
                    "-input={0} -output={1}",
                    exportPair.Key,
                    exportPair.Value);
                if (_forceTextureExport)
                {
                    args += " -force";
                }

                SDiag.Process textureConvertProcess = new SDiag.Process();
                textureConvertProcess.StartInfo.FileName = Environment.ExpandEnvironmentVariables(TextureExportExe);
                textureConvertProcess.StartInfo.Arguments = args;
                textureConvertProcess.StartInfo.CreateNoWindow = true;
                textureConvertProcess.StartInfo.UseShellExecute = false;
                textureConvertProcess.EnableRaisingEvents = true;
                textureConvertProcess.Exited += TextureConvertProcess_Exited;
                _textureConversions.Add(textureConvertProcess);
            }

            NotifyPropertyChanged("ConversionInProgress", "NoConversionInProgress");
            
            // Check out the files from p4.
            IList<String> exportPaths = texturesToExport.Select(item => item.Value).ToList();
            perforceService.GetLatest(exportPaths);
            perforceService.EditFiles(exportPaths);

            // Start the texture conversions.
            foreach (SDiag.Process process in _textureConversions)
            {
                process.Start();
            }

            // Start the conversion.
            StatusText = "Conversion in progress.";
        }

        /// <summary>
        /// Aborts a conversion if one is currently in progress.
        /// </summary>
        public void AbortConversion()
        {
            IList<SDiag.Process> textureProcesses = new List<SDiag.Process>(_textureConversions);
            _textureConversions.Clear();

            foreach (SDiag.Process textureConvertProcess in textureProcesses)
            {
                if (!textureConvertProcess.HasExited)
                {
                    textureConvertProcess.Kill();
                }
            }

            if (_convertProcess != null)
            {
                _convertProcess.Kill();
            }

            StatusText = String.Format("Conversion failed.");
            NotifyPropertyChanged("ConversionInProgress", "NoConversionInProgress");
        }

        /// <summary>
        /// Event handler for when a texture conversion has completed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextureConvertProcess_Exited(object sender, EventArgs e)
        {
            ++_textureConversionsCompleted;
            if (_textureConversionsCompleted == _textureConversions.Count)
            {
                // Create the process and start it.
                SDiag.ProcessStartInfo startInfo = new SDiag.ProcessStartInfo();
                startInfo.FileName = Environment.ExpandEnvironmentVariables(_pipelineConvertExe);
                startInfo.Arguments = String.Join(" ", Scenes.Where(item => item.Convert).Select(item => Path.ChangeExtension(item.Filepath, "zip")));

                _convertProcess = new SDiag.Process();
                _convertProcess.StartInfo = startInfo;
                _convertProcess.EnableRaisingEvents = true;
                _convertProcess.Exited += ConvertProcess_Exited;
                _convertProcess.Start();

                _textureConversions.Clear();
            }
        }

        /// <summary>
        /// Event handler for when the conversion completed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConvertProcess_Exited(object sender, EventArgs e)
        {
            // TODO: Display completion message box?
            StatusText = String.Format("Conversion {0}.", _convertProcess.ExitCode == 0 ? "succeeded" : "failed");

            _convertProcess = null;
            NotifyPropertyChanged("ConversionInProgress", "NoConversionInProgress");
        }
        #endregion
    }
}
