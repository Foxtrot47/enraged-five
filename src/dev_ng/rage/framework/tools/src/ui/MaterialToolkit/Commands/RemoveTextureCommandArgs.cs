﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaterialToolkit.Commands
{
    /// <summary>
    /// Event arguments to use for the RemoveTextureAction.
    /// </summary>
    public class RemoveTextureCommandArgs
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="MainWindowDataContext"/> property.
        /// </summary>
        private readonly MainWindowDataContext _mainWindowDataContext;

        /// <summary>
        /// Private field for the <see cref="SelectedTextureViewModels"/> property.
        /// </summary>
        private readonly IEnumerable<TextureViewModel> _selectedTextureViewModels;
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="RemoveTextureCommandArgs"/> class
        /// using the provided main window data context and selected texture view models.
        /// </summary>
        public RemoveTextureCommandArgs(
            MainWindowDataContext mainWindowDataContext,
            IEnumerable<TextureViewModel> textureViewModels)
        {
            _mainWindowDataContext = mainWindowDataContext;
            _selectedTextureViewModels = textureViewModels.ToList();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Reference to the main window data context.
        /// </summary>
        public MainWindowDataContext MainWindowDataContext
        {
            get { return _mainWindowDataContext; }
        }

        /// <summary>
        /// List of textures that the user selected.
        /// </summary>
        public IEnumerable<TextureViewModel> SelectedTextureViewModels
        {
            get { return _selectedTextureViewModels; }
        }
        #endregion
    }
}
