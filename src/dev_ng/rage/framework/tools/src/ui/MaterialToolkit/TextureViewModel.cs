﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Extensions;
using RSG.Editor;

namespace MaterialToolkit
{
    /// <summary>
    /// View model class representing a single texture in the texture list.
    /// </summary>
    public class TextureViewModel : NotifyPropertyChangedBase
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Filepath"/> property.
        /// </summary>
        private readonly String _filepath;

        /// <summary>
        /// Private field for the <see cref="DatabaseScenes"/> property.
        /// </summary>
        private IList<String> _databaseScenes;

        /// <summary>
        /// Private field for the <see cref="LocalScenes"/> property.
        /// </summary>
        private IList<String> _localScenes;

        /// <summary>
        /// Private field for the <see cref="ExportPaths"/> property.
        /// </summary>
        private readonly Dictionary<String, String> _exportPaths;

        /// <summary>
        /// Flag indicating whether all required data has been retrieved from the DB for this texture.
        /// </summary>
        private bool _databaseValueSet = false;
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the.
        /// </summary>
        /// <param name="filepath"></param>
        public TextureViewModel(String filepath)
        {
            _filepath = filepath;
            _databaseScenes = new List<String>();
            _exportPaths = new Dictionary<String, String>();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Path to the file on disk for this scene xml file.
        /// </summary>
        public String Filepath
        {
            get { return _filepath; }
        }

        /// <summary>
        /// Scenes that this texture is used in according to the database.
        /// </summary>
        public IReadOnlyList<String> DatabaseScenes
        {
            get { return _databaseScenes as IReadOnlyList<String>; }
        }

        /// <summary>
        /// Flag indicating whether the database scene information has been set.
        /// </summary>
        public bool DatabaseValueSet
        {
            get { return _databaseValueSet; }
            set { SetProperty(ref _databaseValueSet, value, "DatabaseValueSet", "DatabaseToolTipText"); }
        }

        /// <summary>
        /// Flag indicating whether this texture is used in the database.
        /// </summary>
        public bool InDatabase
        {
            get { return _databaseScenes.Any(); }
        }

        /// <summary>
        /// Tooltip for the database icon.
        /// </summary>
        public String DatabaseToolTipText
        {
            get
            {
                if (DatabaseValueSet)
                {
                    if (InDatabase)
                    {
                        return String.Format("Texture is used in the following scene xml files according to the database:\n\n{0}",
                            String.Join("\n", DatabaseScenes));
                    }
                    else
                    {
                        return "Texture not in use according to the database.";
                    }
                }
                else
                {
                    return "Refreshing...";
                }
            }
        }

        /// <summary>
        /// Scenes that this texture is used in according to the locally modified scene xml data.
        /// </summary>
        public IReadOnlyList<String> LocalScenes
        {
            get { return _localScenes as IReadOnlyList<String>; }
        }

        /// <summary>
        /// Flag indicating whether the local scene data has been set.
        /// </summary>
        public bool LocalValueSet
        {
            get { return _localScenes != null; }
        }

        /// <summary>
        /// Flag indicating whether this texture is used in the locally modified scene xml data.
        /// </summary>
        public bool InLocal
        {
            get { return _localScenes != null  && _localScenes.Any(); }
        }

        /// <summary>
        /// Tooltip for the local scene icon.
        /// </summary>
        public String LocalToolTipText
        {
            get
            {
                if (LocalValueSet)
                {
                    if (InLocal)
                    {
                        return String.Format("Texture is used in the following scene xml files according to your locally modified files:\n\n{0}",
                            String.Join("\n", LocalScenes));
                    }
                    else
                    {
                        return "Texture not in use according to your locally modified scene xml files.";
                    }
                }
                else
                {
                    return "Refreshing...";
                }
            }
        }

        /// <summary>
        /// Mappings of rex style texture names to export paths.
        /// </summary>
        public IReadOnlyDictionary<String, String> ExportPaths
        {
            get { return _exportPaths; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Sets the scenes that the texture is being used in according to the database.
        /// </summary>
        /// <param name="scenes"></param>
        public void SetDatabaseScenes(IEnumerable<String> scenes)
        {
            _databaseScenes.Clear();
            _databaseScenes.AddRange(scenes);
            NotifyPropertyChanged("DatabaseScenes", "InDatabase");
        }

        /// <summary>
        /// Sets the scenes that the texture is being used in according to the users locally
        /// modified scene xml files.
        /// </summary>
        /// <param name="scenes"></param>
        public void SetLocalScenes(IEnumerable<String> scenes)
        {
            _localScenes = new List<String>(scenes);
            NotifyPropertyChanged("LocalScenes", "LocalValueSet", "InLocal", "LocalToolTipText");
        }

        /// <summary>
        /// Resets the scenes the texture is used in..
        /// </summary>
        public void ResetScenes()
        {
            _databaseScenes.Clear();
            DatabaseValueSet = false;

            _localScenes = null;
            NotifyPropertyChanged("DatabaseScenes", "InDatabase", "DatabaseToolTipText");
            NotifyPropertyChanged("LocalScenes", "LocalValueSet", "InLocal", "LocalToolTipText");
        }

        /// <summary>
        /// Adds an import to export mapping.
        /// </summary>
        /// <param name="input"></param>
        /// <param name="output"></param>
        public void AddExportPath(String input, String output)
        {
            lock (_exportPaths)
            {
                _exportPaths[input] = output;
            }
        }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return _filepath;
        }
        #endregion
    }
}
