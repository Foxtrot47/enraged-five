﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using RSG.Editor;
using RSG.Editor.SharedCommands;

namespace MaterialToolkit.Commands
{
    /// <summary>
    /// Defines core commands for the material toolkit.
    /// </summary>
    public static class MaterialToolkitCommands
    {
        #region Fields
        /// <summary>
        /// String table resource manager for getting at the command related strings.
        /// </summary>
        private static readonly CommandResourceManager _commandResourceManager =
            new CommandResourceManager(
                "MaterialToolkit.Resources.CommandStringTable",
                typeof(MaterialToolkitCommands).Assembly);

        /// <summary>
        /// The private cache of System.Windows.Input.RoutedCommand objects.
        /// </summary>
        private static readonly RockstarRoutedCommand[] _internalCommands =
            new RockstarRoutedCommand[Enum.GetValues(typeof(CommandId)).Length];
        #endregion // Fields

        #region Enumerations
        /// <summary>
        /// Defines all of the command identifiers for the different core Rockstar commands.
        /// </summary>
        private enum CommandId
        {
            /// <summary>
            /// Used to identifier the <see cref="MaterialToolkitCommands.AddTexture"/>
            /// routed command.
            /// </summary>
            AddTexture,

            /// <summary>
            /// Used to identifier the <see cref="MaterialToolkitCommands.RemoveTexture"/>
            /// routed command.
            /// </summary>
            RemoveTexture,

            /// <summary>
            /// Used to identifier the <see cref="MaterialToolkitCommands.Convert"/>
            /// routed command.
            /// </summary>
            LocalConvert,

            /// <summary>
            /// Used to identifier the <see cref="MaterialToolkitCommands.CancelConvert"/>
            /// routed command.
            /// </summary>
            CancelConvert,

            /// <summary>
            /// Used to identifier the <see cref="MaterialToolkitCommands.Refresh"/>
            /// routed command.
            /// </summary>
            Refresh,

            /// <summary>
            /// Used to identifier the <see cref="MaterialToolkitCommands.QueueOnAssetBuilder"/>
            /// routed command.
            /// </summary>
            QueueOnAssetBuilder,

            /// <summary>
            /// Used to identifier the <see cref="MaterialToolkitCommands.ForceTextureExportOption"/>
            /// routed command.
            /// </summary>
            ForceTextureExportOption
        }
        #endregion // Enumerations

        #region Properties
        /// <summary>
        /// Gets the value that represents the add texture command.
        /// </summary>
        public static RockstarRoutedCommand AddTexture
        {
            get { return EnsureCommandExists(CommandId.AddTexture, MaterialToolkitIcons.AddTexture); }
        }

        /// <summary>
        /// Gets the value that represents the remove texture command.
        /// </summary>
        public static RockstarRoutedCommand RemoveTexture
        {
            get { return EnsureCommandExists(CommandId.RemoveTexture, MaterialToolkitIcons.RemoveTexture); }
        }

        /// <summary>
        /// Gets the value that represents the convert command.
        /// </summary>
        public static RockstarRoutedCommand LocalConvert
        {
            get { return EnsureCommandExists(CommandId.LocalConvert, MaterialToolkitIcons.LocalConvert); }
        }

        /// <summary>
        /// Gets the value that represents the cancel convert command.
        /// </summary>
        public static RockstarRoutedCommand CancelConvert
        {
            get { return EnsureCommandExists(CommandId.CancelConvert); }
        }

        /// <summary>
        /// Gets the value that represents the refresh command.
        /// </summary>
        public static RockstarRoutedCommand Refresh
        {
            get { return EnsureCommandExists(CommandId.Refresh); }
        }

        /// <summary>
        /// Gets the value that represents the queue on asset builder command.
        /// </summary>
        public static RockstarRoutedCommand QueueOnAssetBuilder
        {
            get { return EnsureCommandExists(CommandId.QueueOnAssetBuilder, MaterialToolkitIcons.QueueOnAssetBuilder); }
        }

        /// <summary>
        /// Gets the value that represents the force texture export option command.
        /// </summary>
        public static RockstarRoutedCommand ForceTextureExportOption
        {
            get { return EnsureCommandExists(CommandId.ForceTextureExportOption); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a new System.Windows.Input.RoutedCommand object that is associated with the
        /// specified identifier.
        /// </summary>
        /// <param name="id">
        /// The identifier for the command to create.
        /// </param>
        /// <param name="provider">
        /// An object that supplies culture-specific formatting information.
        /// </param>
        /// <returns>
        /// The newly create System.Windows.Input.RoutedCommand object.
        /// </returns>
        private static RockstarRoutedCommand CreateCommand(String id, BitmapSource icon)
        {
            String name = _commandResourceManager.GetString(id, CommandStringCategory.Name);
            String category = _commandResourceManager.GetString(id, CommandStringCategory.Category);
            String description = _commandResourceManager.GetString(id, CommandStringCategory.Description);
            InputGestureCollection gestures = _commandResourceManager.GetGestures(id);

            return new RockstarRoutedCommand(
                name, typeof(MaterialToolkitCommands), gestures, category, description, icon);
        }

        /// <summary>
        /// Makes sure the internal value for the command associated with the specified
        /// identifier exists and returns it.
        /// </summary>
        /// <param name="commandId">
        /// The identifier for the command that should be created and returned.
        /// </param>
        /// <param name="icon">
        /// The icon that the command should be using.
        /// </param>
        /// <returns>
        /// The <see cref="RSG.Editor.RockstarRoutedCommand"/> object associated with the
        /// specified identifier.
        /// </returns>
        private static RockstarRoutedCommand EnsureCommandExists(CommandId commandId, BitmapSource icon = null)
        {
            int commandIndex = (int)commandId;
            if (commandIndex < -1 || commandIndex >= _internalCommands.Length)
            {
                return null;
            }

            RockstarRoutedCommand command = _internalCommands[commandIndex];
            if (command == null)
            {
                lock (_internalCommands)
                {
                    if (command == null)
                    {
                        command = CreateCommand(commandId.ToString(), icon);
                        _internalCommands[commandIndex] = command;
                    }
                }
            }

            return command;
        }
        #endregion // Methods
    } // UniversalLogCommands
}
