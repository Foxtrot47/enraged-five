﻿using System;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sandbox.Extensions.UI.Menu
{
    /// <summary>
    /// The base interface for any menu item that needs to be exported
    /// </summary>
    public interface IMenuItem : ICommand
    {
        #region Properties

        ItemCollection Items { get; }

        /// <summary>
        /// Get the reference to the menu item this interface
        /// represents
        /// </summary>
        MenuItem MenuItem { get; }

        /// <summary>
        /// The displayed string for the menu item
        /// </summary>
        String Header { get; }

        /// <summary>
        /// The icon for the menu item, this can be null
        /// </summary>
        Object Icon { get; }

        /// <summary>
        /// Represents whether the user can check/uncheck this
        /// menu item
        /// </summary>
        Boolean IsCheckable { get; }

        /// <summary>
        /// Represents whether this menu item is currently checked.
        /// Only really has an effect if the IsCheckable property is
        /// set to true
        /// </summary>
        Boolean IsChecked { get; set; }

        /// <summary>
        /// Represents whether this menu item is actually a menu item
        /// separator, determines the way this is styled.
        /// </summary>
        Boolean IsSeparator { get; }

        /// <summary>
        /// Represents whether this menu item items are currently visible
        /// to the user, (i.e whether the popup is visible)
        /// </summary>
        Boolean IsSubmenuOpen { get; set; }

        #endregion // Properties
    }
}
