﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using Sandbox.Extensions.Services;

namespace Sandbox
{
    /// <summary>
    /// A class that exports the default settings for the application 
    /// so that other extensions can use the settings
    /// </summary>
    [Export(Sandbox.Extensions.CompositionPoints.AppSettings, typeof(ISettings))]
    public class Settings : SettingsBase
    {
        #region Constructors

        /// <summary>
        /// Default constructor that makes sure the
        /// default settings property is assigned to
        /// </summary>
        public Settings()
            : base(Sandbox.Properties.Settings.Default)
        {
        }

        #endregion // Constructors
    }
}
