﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sandbox.Extensions
{
    public static class CompositionPoints
    {
        /// <summary>
        /// Used to get the main window from anywhere in the application
        /// including the extensions
        /// </summary>
        public const String MainWindow = "E30D77B9-7E6C-4A7E-A1B9-84E19D696EE1";

        /// <summary>
        /// Used to get the applications default settings from anywhere in the
        /// application including the extensions
        /// </summary>
        public const String AppSettings = "050EBC1C-87EE-43C2-8788-610280EBFB30";

        /// <summary>
        /// Used to get the applications default settings from anywhere in the
        /// application including the extensions
        /// </summary>
        public const String AssemblyInformation = "E60507AC-DEBB-4DF3-A10B-5EF956A2CDF3";

        /// <summary>
        /// </summary>
        public const String Configuration = "D0E96A0B-DA27-4C95-ABE8-C5A83B909BD9";

        /// <summary>
        /// Used to get the current application
        /// </summary>
        public const String Application = "E9AB1626-8642-465E-8AB3-7B8C5858E11A";
    }
}
