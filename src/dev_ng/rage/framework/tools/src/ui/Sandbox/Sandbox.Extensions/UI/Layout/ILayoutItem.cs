﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using System.Linq;
using System.Text;

namespace Sandbox.Extensions.UI.Layout
{
    public interface ILayoutItem
    {
        #region Properties

        /// <summary>
        /// Unique window name; used for layout serialisation (alpha-numeric only).
        /// </summary>
        String Name { get; set; }

        /// <summary>
        /// Window title for UI display.
        /// </summary>
        String Title { get; set; }

        /// <summary>
        /// Window icon for UI display.
        /// </summary>
        ImageSource Icon { get; set; }

        /// <summary>
        /// Resprents whether this document is
        /// currently locked, which means that the 
        /// documents location is locked in place in the
        /// UI
        /// </summary>
        Boolean IsLocked { get; set; }

        /// <summary>
        /// Represents whether this document has been
        /// modified by the user.
        /// </summary>
        Boolean IsModified { get; set; }

        /// <summary>
        /// Represents whether this document is
        /// currently in a readonly state. This basically
        /// means whether the file the document is
        /// representing has the readonly attribute set.
        /// </summary>
        Boolean IsReadOnly { get; set; }

        /// <summary>
        /// This is the path to the file that the
        /// document is representing, this is so
        /// that the IsReadOnly property can be updated
        /// and set
        /// </summary>
        String Path { get; set; }

        #endregion // Properties
    }
}
