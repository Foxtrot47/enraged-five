﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using Sandbox.Extensions.UI.Layout;
using AvalonDock;

namespace Sandbox.Layout
{
    /// <summary>
    /// The main layout manager for the sandboxes
    /// ide.
    /// </summary>
    [Export(Extensions.ExtensionPoints.LayoutManager, typeof(ILayoutManager))]
    public class LayoutManager : ILayoutManager
    {
        #region Public Events

        /// <summary>
        /// Gets fired when the layout manager has
        /// been loaded and is ready to be rendered
        /// </summary>
        public event EventHandler Loaded;

        /// <summary>
        /// Gets fired when the layout manager has
        /// been unloaded and no longer valid for
        /// rendering
        /// </summary>
        public event EventHandler Unloaded;

        /// <summary>
        /// Gets fired when anything about the 
        /// layout has changed and has been updated
        /// </summary>
        public event EventHandler LayoutUpdated;

        /// <summary>
        /// Gets fired when the document that currently 
        /// has focus has changed
        /// </summary>
        public event EventHandler ActiveDocumentChanged;

        /// <summary>
        /// Gets fired when the document that currently 
        /// has focus is about to change
        /// </summary>
        public event EventHandler ActiveDocumentChanging;

        #endregion // Public Events

        #region Properties

        /// <summary>
        /// This document pane is where all the documents are put.
        /// </summary>
        private DocumentPane DocumentPane
        {
            get { return m_documentPane; }
            set { m_documentPane = value; }
        }
        private DocumentPane m_documentPane = new DocumentPane();

        /// <summary>
        /// This resizing panel is where the document pane gets placed.
        /// </summary>
        private ResizingPanel ResizingPanel
        {
            get { return m_resizingPanel; }
            set { m_resizingPanel = value; }
        }
        private ResizingPanel m_resizingPanel = new ResizingPanel();

        /// <summary>
        /// The View binds to this property, and this manager is where the
        /// main resizing panel gets put and managed.
        /// </summary>
        public DockingManager DockManager
        {
            get
            {
                return m_dockManager;
            }
        }
        private readonly DockingManager m_dockManager = new DockingManager();

        /// <summary>
        /// All currently open IDocument's.
        /// </summary>
        public ReadOnlyCollection<IDocument> Documents
        {
            get { return new ReadOnlyCollection<IDocument>(m_documents.Keys.ToList()); }
        }
        private readonly Dictionary<IDocument, DocumentContent> m_documents = new Dictionary<IDocument, DocumentContent>();

        /// <summary>
        /// All currently opened tool windows
        /// </summary>
        public ReadOnlyCollection<IToolWindow> ToolWindows 
        {
            get { return new ReadOnlyCollection<IToolWindow>(m_toolWindows.Keys.ToList()); }
        }
        private readonly Dictionary<IToolWindow, DockableContent> m_toolWindows = new Dictionary<IToolWindow, DockableContent>();

        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        public LayoutManager()
        {
            this.DocumentPane.Name = "DocumentPane";
            this.ResizingPanel.Name = "ResizingPanel";
            this.ResizingPanel.Children.Add(this.DocumentPane);
            DockManager.Content = this.ResizingPanel;

            DockManager.Loaded += new System.Windows.RoutedEventHandler(OnDockManagerLoaded);
            DockManager.LayoutUpdated += new EventHandler(OnDockManagerLayoutUpdated);
        }

        #endregion // Constructors

        #region Document Methods

        /// <summary>
        /// Makes sure that the given IDocument is
        /// opened. The given IDocument needs to be
        /// registered to this layout manager to be closed
        /// </summary>
        public void ShowDocument(IDocument document)
        {
            if (document == null)
                return;

            if (!this.m_documents.ContainsKey(document))
            {
                DocumentContent content = document.DocumentContent;
                m_documents.Add(document, content);
                this.DocumentPane.Items.Add(content);

                content.Closing += document.OnClosing;
                content.Closed += OnDocumentClosed;
                content.GotFocus += document.OnFocus;
                content.LostFocus += document.OnLostFocus;

                document.OnOpened(content, EventArgs.Empty);
            }

            if (this.m_documents.ContainsKey(document))
            {
                m_documents[document].Show(this.DockManager);
                this.DockManager.ActiveDocument = m_documents[document];
            }
        }

        /// <summary>
        /// Makes sure that the given IDocument is
        /// closed. The given IDocument needs to be
        /// registered to this layout manager to be closed
        /// </summary>
        public void CloseDocument(IDocument document) { }

        /// <summary>
        /// Closes all of the currently opened
        /// documents registered to this layout
        /// manager
        /// </summary>
        public void CloseAllDocuments() { }

        /// <summary>
        /// Returns the active IDocument, the IDocument
        /// that has focus
        /// </summary>
        public IDocument ActiveDocument() { return null; }

        /// <summary>
        /// Returns a valid that indicates whether
        /// the given IDocument is currently active
        /// i.e has focus. If the document isn't registered
        /// it returns false
        /// </summary>
        public Boolean IsActive(IDocument document) { return false; }

        /// <summary>
        /// Returns a valid that indicates whether
        /// the given IDocument is currently visible.
        /// If the document isn't registered it returns false
        /// </summary>
        public Boolean IsVisible(IDocument document) { return false; }

        #endregion // Document Methods

        #region Tool Window Methods

        public void ShowToolWindow(IToolWindow tool)
        {
            if (!m_toolWindows.ContainsKey(tool))
            {
                DockableContent content = tool.DockableContent;
                m_toolWindows.Add(tool, content);
                DockablePane dp = new DockablePane();
                dp.Items.Add(content);
                m_resizingPanel.Children.Add(dp);

            }

            if (this.m_toolWindows.ContainsKey(tool))
            {
                m_toolWindows[tool].Show(DockManager);
            }
        }

        #endregion // Tool Window Methods

        #region Event Handling

        /// <summary>
        /// Passes the Loaded event along through this layout manager
        /// </summary>
        void OnDockManagerLoaded(Object sender, System.Windows.RoutedEventArgs e)
        {
            EventHandler handler = this.Loaded;
            if (handler != null)
            {
                Loaded(sender, new EventArgs());
            }
        }

        /// <summary>
        /// Passes the LayoutUpdated event along through this layout manager
        /// </summary>
        void OnDockManagerLayoutUpdated(Object sender, EventArgs e)
        {
            if (LayoutUpdated != null)
            {
                LayoutUpdated(sender, e);
            }
        }

        /// <summary>
        /// Document closed handler; removing documents from our internal data 
        /// structures, unsubscribe event handlers etc.
        /// </summary>
        private void OnDocumentClosed(Object sender, EventArgs e)
        {
            IDocument document = (sender as IDocument);
            DocumentContent content = document.DocumentContent;

            content.Closing -= document.OnClosing;
            content.Closed -= OnDocumentClosed;
            content.GotFocus -= document.OnFocus;
            content.LostFocus -= document.OnLostFocus;

            m_documents.Remove(document);
            document.OnClosed(sender, e);
        }

        #endregion // Event Handling

        #region Private Functions

        #endregion // Private Functions

    }
}
