﻿using System;
using System.ComponentModel.Composition;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AvalonDock;
using Sandbox.Extensions;
using Sandbox.Extensions.Services.File;
using Sandbox.Extensions.UI.Layout;

namespace Sandbox.Layout
{
    public class TestTreeClass
    {
        public String Name { get; set; }

        public ObservableCollection<TestTreeClass> Children { get; set; }

        public TestTreeClass()
        {
            this.Name = "Test";
            this.Children = new ObservableCollection<TestTreeClass>();
        }
    }

    /// <summary>
    /// Interaction logic for Test.xaml
    /// </summary>
    public partial class Test : DocumentBase
    {
        public ObservableCollection<TestTreeClass> TreeItems { get; private set; }

        public Test()
        {
            InitializeComponent();
            this.TreeItems = new ObservableCollection<TestTreeClass>();
            TestTreeClass testObject = new TestTreeClass();
            testObject.Children.Add(new TestTreeClass());
            testObject.Children.Add(new TestTreeClass());
            testObject.Children.Add(new TestTreeClass());
            testObject.Children.Add(new TestTreeClass());
            testObject.Children.Add(new TestTreeClass());
            this.TreeItems.Add(testObject);
            TestTreeClass testObject1 = new TestTreeClass();
            testObject1.Children.Add(new TestTreeClass());
            testObject1.Children.Add(new TestTreeClass());
            testObject1.Children.Add(new TestTreeClass());
            testObject1.Children.Add(new TestTreeClass());
            testObject1.Children.Add(new TestTreeClass());
            this.TreeItems.Add(testObject1);

            this.DataContext = this;

            //Binding newBinding = new Binding();
            //newBinding.Source = this;
            //newBinding.Path = new PropertyPath("TreeItems");
            //newBinding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;

            //this.TreeViewControl.SetBinding(TreeView.ItemsSourceProperty, newBinding);
        }
    }

    [Export(ExtensionPoints.NewService, typeof(INewService))]
    public class TestNewService : INewService
    {
        #region Properties

        /// <summary>
        /// New service header string for UI display.
        /// </summary>
        public String Header { get; set; }

        /// <summary>
        /// New service Bitmap for UI display.
        /// </summary>
        public Object Icon { get; set; }

        #endregion // Properties

        public TestNewService()
        {
            this.Header = "New Test File...";
        }

        #region Methods
        
        /// <summary>
        /// 
        /// </summary>
        public bool New(out IDocument document, String title)
        {
            document = new Test();
            document.Title = title;

            return true;
        }

        #endregion // Methods

    }
}
