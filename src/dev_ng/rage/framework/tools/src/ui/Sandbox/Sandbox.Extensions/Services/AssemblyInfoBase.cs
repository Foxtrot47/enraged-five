﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Sandbox.Extensions.Services
{
    /// <summary>
    /// The base class for any class wanting to implement the 
    /// IAssemblyAttributes interface
    /// </summary>
    public class AssemblyInfoBase : IAssemblyAttributes
    {
        #region Properties

        /// <summary>
        /// A dictionary of attribute values indexed by the
        /// assemby attribute enum value
        /// </summary>
        protected virtual Dictionary<AssemblyAttributes, Object> Attributes
        {
            get;
            set;
        }

        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Default constructor that makes sure the
        /// default settings property is assigned to
        /// </summary>
        public AssemblyInfoBase(Assembly assembly)
        {
            this.Attributes = new Dictionary<AssemblyAttributes, Object>();
            this.Attributes.Add(AssemblyAttributes.FULLNAME, null);
            this.Attributes.Add(AssemblyAttributes.TITLE, null);
            this.Attributes.Add(AssemblyAttributes.DESCRIPTION, null);
            this.Attributes.Add(AssemblyAttributes.COMPANY, null);
            this.Attributes.Add(AssemblyAttributes.PRODUCT, null);
            this.Attributes.Add(AssemblyAttributes.COPYRIGHT, null);
            this.Attributes.Add(AssemblyAttributes.TRADEMARK, null);
            this.Attributes.Add(AssemblyAttributes.ASSEMBLYVERSION, null);
            this.Attributes.Add(AssemblyAttributes.FILEVERSION, null);
            this.Attributes.Add(AssemblyAttributes.GUID, null);
            this.Attributes.Add(AssemblyAttributes.DEBUG, null);

            try
            {
                if (assembly != null)
                {
                    this.Attributes[AssemblyAttributes.FULLNAME] = assembly.FullName;
                    this.Attributes[AssemblyAttributes.ASSEMBLYVERSION] = assembly.GetName().Version;
                    this.Attributes[AssemblyAttributes.DEBUG] = false;

                    Object[] attributes = assembly.GetCustomAttributes(false);
                    foreach (Object attribute in attributes)
                    {
                        if (attribute is AssemblyTitleAttribute)
                            this.Attributes[AssemblyAttributes.TITLE] = attribute as AssemblyTitleAttribute;

                        if (attribute is AssemblyDescriptionAttribute)
                            this.Attributes[AssemblyAttributes.DESCRIPTION] = attribute as AssemblyDescriptionAttribute;

                        if (attribute is AssemblyCompanyAttribute)
                            this.Attributes[AssemblyAttributes.COMPANY] = attribute as AssemblyCompanyAttribute;

                        if (attribute is AssemblyProductAttribute)
                            this.Attributes[AssemblyAttributes.PRODUCT] = attribute as AssemblyProductAttribute;

                        if (attribute is AssemblyCopyrightAttribute)
                            this.Attributes[AssemblyAttributes.COPYRIGHT] = attribute as AssemblyCopyrightAttribute;

                        if (attribute is AssemblyTrademarkAttribute)
                            this.Attributes[AssemblyAttributes.TRADEMARK] = attribute as AssemblyTrademarkAttribute;

                        if (attribute is AssemblyFileVersionAttribute)
                            this.Attributes[AssemblyAttributes.FILEVERSION] = attribute as AssemblyFileVersionAttribute;

                        if (attribute is GuidAttribute)
                            this.Attributes[AssemblyAttributes.GUID] = attribute as GuidAttribute;

                        if (attribute is DebuggableAttribute)
                            if ((attribute as DebuggableAttribute).IsJITTrackingEnabled == true)
                                this.Attributes[AssemblyAttributes.DEBUG] = true;
                    }
                }
            }
            catch
            {
            }
        }

        #endregion // Constructors

        #region Members

        /// <summary>
        /// Returns the string value for the given assembly attribute
        /// </summary>
        public String GetAttributeAsString(AssemblyAttributes attribute)
        {
            Object value = null;
            this.Attributes.TryGetValue(attribute, out value);
            
            if (value != null)
            {
                switch (attribute)
                {
                    case AssemblyAttributes.FULLNAME:
                        return value as String;

                    case AssemblyAttributes.TITLE:
                        return (value as AssemblyTitleAttribute).Title;

                    case AssemblyAttributes.DESCRIPTION:
                        return (value as AssemblyDescriptionAttribute).Description;

                    case AssemblyAttributes.COMPANY:
                        return (value as AssemblyCompanyAttribute).Company;

                    case AssemblyAttributes.PRODUCT:
                        return (value as AssemblyProductAttribute).Product;

                    case AssemblyAttributes.COPYRIGHT:
                        return (value as AssemblyCopyrightAttribute).Copyright;

                    case AssemblyAttributes.TRADEMARK:
                        return (value as AssemblyTrademarkAttribute).Trademark;

                    case AssemblyAttributes.ASSEMBLYVERSION:
                        return (value as Version).ToString();

                    case AssemblyAttributes.FILEVERSION:
                        return (value as AssemblyFileVersionAttribute).Version;

                    case AssemblyAttributes.GUID:
                        return (value as GuidAttribute).Value;

                    case AssemblyAttributes.DEBUG:
                        return (value as bool?) == true ? "true" : "false";

                    default:
                        return String.Empty;
                }
            }

            return String.Empty;
        }

        /// <summary>
        /// Returns the value of the given assembly attribute
        /// </summary>
        public Object GetAttribute(AssemblyAttributes attribute)
        {
            Object value = null;
            this.Attributes.TryGetValue(attribute, out value);
            return value;
        }

        #endregion // Members
    }
}
