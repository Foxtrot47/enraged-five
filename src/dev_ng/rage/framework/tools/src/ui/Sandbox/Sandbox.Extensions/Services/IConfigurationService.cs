﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sandbox.Extensions.Services
{
    public interface IConfigurationService
    {
        #region Methods

        /// <summary>
        /// Returns next new file filename (basename),
        /// this is used when creating new documents
        /// </summary>
        String GetNewFilename();

        #endregion // Methods
    }
}
