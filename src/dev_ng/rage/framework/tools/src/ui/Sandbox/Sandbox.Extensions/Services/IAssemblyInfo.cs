﻿using System;
using System.Collections.Generic;

namespace Sandbox.Extensions.Services
{
    /// <summary>
    /// Lists all the different assembly
    /// attributes you can retrive from a IAssemblyInfo interface
    /// </summary>
    public enum AssemblyAttributes
    {
        FULLNAME,
        TITLE,
        DESCRIPTION,
        COMPANY,
        PRODUCT,
        COPYRIGHT,
        TRADEMARK,
        ASSEMBLYVERSION,
        FILEVERSION,
        GUID,
        DEBUG,
    }

    /// <summary>
    /// Interface for the assembly Information service
    /// </summary>
    public interface IAssemblyAttributes
    {
        #region Members

        /// <summary>
        /// Returns the string value for the given assembly attribute
        /// </summary>
        String GetAttributeAsString(AssemblyAttributes attribute);

        /// <summary>
        /// Returns the value of the given assembly attribute
        /// </summary>
        Object GetAttribute(AssemblyAttributes attribute);

        #endregion // Members
    }
}
