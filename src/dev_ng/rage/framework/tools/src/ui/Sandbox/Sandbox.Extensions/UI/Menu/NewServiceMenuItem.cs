﻿using System;
using System.Windows.Input;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sandbox.Extensions.UI.Layout;
using Sandbox.Extensions.Services;
using Sandbox.Extensions.Services.File;

namespace Sandbox.Extensions.UI.Menu
{
    public class NewServiceMenuItem : MenuItemBase
    {
        /// <summary>
        /// Associated INewService.
        /// </summary>
        private INewService Service;

        /// <summary>
        /// 
        /// </summary>
        private ILayoutManager LayoutManager;

        private IConfigurationService ConfigurationService { get; set; }

        #region Constructor(s)

        /// <summary>
        /// Constructor.
        /// </summary>
        public NewServiceMenuItem(INewService service, ILayoutManager layoutManager, IConfigurationService configurationService)
            : base(service.Header)
        {
            this.Service = service;
            this.LayoutManager = layoutManager;
            this.ConfigurationService = configurationService;
            this.Icon = service.Icon;
        }

        #endregion // Constructor(s)

        #region Command Methods

        /// <summary>
        /// 
        /// </summary>
        public override bool CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        public override void Execute(Object parameter)
        {
            Sandbox.Extensions.UI.Layout.IDocument newDocument = null;
            Boolean newResult = this.Service.New(out newDocument, this.ConfigurationService.GetNewFilename());

            if (newResult == true && newDocument != null)
            {
                this.LayoutManager.ShowDocument(newDocument);
            }
        }
        #endregion // CommandControl Methods

    }
}
