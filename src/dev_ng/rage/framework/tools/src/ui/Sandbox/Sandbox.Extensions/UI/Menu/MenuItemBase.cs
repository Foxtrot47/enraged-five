﻿using System;
using System.Windows;
using System.ComponentModel.Composition;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sandbox.Extensions.UI.Menu
{
    /// <summary>
    /// The base class for any menu item
    /// </summary>
    public class MenuItemBase : MenuItem, IMenuItem, IPartImportsSatisfiedNotification
    {
        RoutedUICommand OnExecute
        {
            get { return m_onExecuteCommand; }
        }
        private static RoutedUICommand m_onExecuteCommand = new RoutedUICommand("On Execute", "OnExecute", typeof(MenuItemBase));

        #region MEF Imports

        /// <summary>
        /// File menu subitems.
        /// </summary>
        [Import(CompositionPoints.MainWindow, typeof(Window))]
        private Window MainWindow { get; set; }

        #endregion // MEF Imports

        #region Properties

        public new ItemCollection Items
        {
            get { return base.Items; }
        }

        /// <summary>
        /// Get the reference to the menu item this interface
        /// represents
        /// </summary>
        public MenuItem MenuItem 
        {
            get 
            {
                MenuItem item = new MenuItem();
                item = this as MenuItem;
                return item;
            }
        }

        /// <summary>
        /// The displayed string for the menu item
        /// </summary>
        public new String Header
        {
            get { return base.Header.ToString(); }
            protected set { base.Header = value; }
        }

        /// <summary>
        /// The icon for the menu item, this can be null
        /// </summary>
        public new Object Icon
        {
            get { return base.Icon; }
            protected set { base.Icon = value; }
        }

        /// <summary>
        /// Represents whether the user can check/uncheck this
        /// menu item
        /// </summary>
        public new Boolean IsCheckable
        {
            get { return base.IsCheckable; }
            protected set { base.IsCheckable = value; }
        }

        /// <summary>
        /// Represents whether this menu item is currently checked.
        /// Only really has an effect if the IsCheckable property is
        /// set to true
        /// </summary>
        public new Boolean IsChecked
        {
            get { return base.IsChecked; }
            set { base.IsChecked = value; }
        }

        /// <summary>
        /// Represents whether this menu item is actually a menu item
        /// separator, determines the way this is styled.
        /// </summary>
        public Boolean IsSeparator
        {
            get;
            protected set;
        }

        /// <summary>
        /// Represents whether this menu item items are currently visible
        /// to the user, (i.e whether the popup is visible)
        /// </summary>
        public new Boolean IsSubmenuOpen
        {
            get { return base.IsSubmenuOpen; }
            set { base.IsSubmenuOpen = value; }
        }

        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        public MenuItemBase(String header)
        {
            this.Header = (header.Clone() as String);
            this.Command = this as ICommand;
        }

        #endregion // Constructors

        #region ICommand Interface

        /// <summary>
        /// 
        /// </summary>
        public event EventHandler CanExecuteChanged = delegate { };

        /// <summary>
        /// Command can excute method
        /// </summary>
        public virtual bool CanExecute(Object parameter)
        {
            return true;
        }

        /// <summary>
        /// Command execute method.
        /// </summary>
        public virtual void Execute(Object parameter)
        {
        }

        #endregion // ICommand Interface

        #region IPartImportsSatisfiedNotification Methods

        /// <summary>
        /// 
        /// </summary>
        public virtual void OnImportsSatisfied()
        {
            if (this.IsSeparator == false)
            {
                this.Style = this.MainWindow.TryFindResource("MenuItemStyleKey") as Style;
            }
            else
            {
                this.Style = this.MainWindow.TryFindResource("MenuSeparatorStyleKey") as Style;
            }
        }

        #endregion // IPartImportsSatisfiedNotification Methods
    }
}
