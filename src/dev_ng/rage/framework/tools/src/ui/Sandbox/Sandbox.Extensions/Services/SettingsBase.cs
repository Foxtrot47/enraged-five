﻿using System;
using System.Configuration;

namespace Sandbox.Extensions.Services
{
    /// <summary>
    /// The base class for any class wanting to implement the 
    /// ISettings interface
    /// </summary>
    public class SettingsBase : ISettings
    {
        #region Properties

        /// <summary>
        /// The defult settings for this settings
        /// instance
        /// </summary>
        public virtual ApplicationSettingsBase Default
        {
            get;
            protected set;
        }

        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Default constructor that makes sure the
        /// default settings property is assigned to
        /// </summary>
        public SettingsBase(ApplicationSettingsBase settings)
        {
            this.Default = settings;
        }

        #endregion // Constructors
    }
}
