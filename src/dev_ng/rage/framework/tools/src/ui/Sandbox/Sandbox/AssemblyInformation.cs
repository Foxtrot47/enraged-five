﻿using System;
using System.Reflection;
using System.ComponentModel.Composition;
using Sandbox.Extensions.Services;

namespace Sandbox
{
    /// <summary>
    /// A class that exports the assembly information
    /// for this host assembly so that other extensions
    /// can use the attributes
    /// </summary>
    [Export(Sandbox.Extensions.CompositionPoints.AssemblyInformation, typeof(IAssemblyAttributes))]
    class AssemblyInformation : AssemblyInfoBase
    {
        #region Constructors

        /// <summary>
        /// Default constructor that creates the
        /// assembly information from the 
        /// current assembly
        /// </summary>
        public AssemblyInformation()
            : base(Assembly.GetExecutingAssembly())
        {
        }

        #endregion // Constructors
    }
}
