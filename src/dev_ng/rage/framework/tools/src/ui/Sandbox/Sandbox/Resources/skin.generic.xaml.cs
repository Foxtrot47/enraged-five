﻿using System;
using System.ComponentModel.Composition;
using System.Windows;
using Sandbox.Extensions;
using Sandbox.Extensions.UI.Themes;

namespace Sandbox.Resources
{
    /// <summary>
    /// Interaction logic for skin.xaml
    /// </summary>
    [Export(ExtensionPoints.Skin, typeof(ISkinResource))]
    public partial class SkinGeneric : SkinResourceBase
    {
        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        public SkinGeneric()
            : base("pack://application:,,,/Sandbox;component/Resources/skin.generic.xaml", "Default")
        {
            this.IsDefault = true;
        }

        #endregion // Constructors
    }
}
