﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Sandbox.Extensions.UI.Layout
{
    /// <summary>
    /// The base interface for the LayoutManager
    /// class
    /// </summary>
    public interface ILayoutManager
    {
        #region Public Events

        /// <summary>
        /// Gets fired when the layout manager has
        /// been loaded and is ready to be rendered
        /// </summary>
        event EventHandler Loaded;

        /// <summary>
        /// Gets fired when the layout manager has
        /// been unloaded and no longer valid for
        /// rendering
        /// </summary>
        event EventHandler Unloaded;

        /// <summary>
        /// Gets fired when anything about the 
        /// layout has changed and has been updated
        /// </summary>
        event EventHandler LayoutUpdated;

        /// <summary>
        /// Gets fired when the document that currently 
        /// has focus has changed
        /// </summary>
        event EventHandler ActiveDocumentChanged;

        /// <summary>
        /// Gets fired when the document that currently 
        /// has focus is about to change
        /// </summary>
        event EventHandler ActiveDocumentChanging;

        #endregion // Public Events

        #region Properties

        /// <summary>
        /// All currently open IDocument's.
        /// </summary>
        ReadOnlyCollection<IDocument> Documents { get; }

        /// <summary>
        /// All currently opened tool windows
        /// </summary>
        ReadOnlyCollection<IToolWindow> ToolWindows { get; }

        #endregion // Properties

        #region Document Methods

        /// <summary>
        /// Makes sure that the given IDocument is
        /// opened. The given IDocument needs to be
        /// registered to this layout manager to be closed
        /// </summary>
        void ShowDocument(IDocument document);

        /// <summary>
        /// Makes sure that the given IDocument is
        /// closed. The given IDocument needs to be
        /// registered to this layout manager to be closed
        /// </summary>
        void CloseDocument(IDocument document);

        /// <summary>
        /// Closes all of the currently opened
        /// documents registered to this layout
        /// manager
        /// </summary>
        void CloseAllDocuments();

        /// <summary>
        /// Currently active IDocument, the IDocument
        /// that has focus
        /// </summary>
        IDocument ActiveDocument();

        /// <summary>
        /// Returns a valid that indicates whether
        /// the given IDocument is currently active
        /// i.e has focus. If the document isn't registered
        /// it returns false
        /// </summary>
        Boolean IsActive(IDocument document);

        /// <summary>
        /// Returns a valid that indicates whether
        /// the given IDocument is currently visible.
        /// If the document isn't registered it returns false
        /// </summary>
        Boolean IsVisible(IDocument document);

        #endregion // Document Methods

        #region Tool Window Methods

        void ShowToolWindow(IToolWindow pad);

        #endregion // Tool Window Methods
    }
}
