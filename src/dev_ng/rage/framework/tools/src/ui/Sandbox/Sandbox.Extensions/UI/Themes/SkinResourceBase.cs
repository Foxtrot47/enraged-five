﻿using System;
using System.Windows;
using System.ComponentModel.Composition;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sandbox.Theme;

namespace Sandbox.Extensions.UI.Themes
{
    /// <summary>
    /// The base class for any class that wants
    /// to be a resource theme
    /// </summary>
    public class SkinResourceBase : DependencyObject, ISkinResource
    {
        #region Properties

        /// <summary>
        /// A reference to the default skin that is used by the application
        /// </summary>
        public static SkinResourceBase DefaultSkin
        {
            get;
            set;
        }

        /// <summary>
        /// Represents whether this skin resource is the
        /// default resource
        /// </summary>
        public Boolean IsDefault
        {
            get { return (Boolean)GetValue(IsDefaultProperty); }
            protected set { SetValue(IsDefaultProperty, value); }
        }
        public static DependencyProperty IsDefaultProperty = DependencyProperty.Register("IsDefault", typeof(Boolean), typeof(SkinResourceBase)
            , new PropertyMetadata(false, OnIsDefaultChanged));

        /// <summary>
        /// Gets called if the dependency property IsDefaultProperty changes. We need to make sure there is
        /// no more than one default dictionary (can be none)
        /// </summary>
        private static void OnIsDefaultChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            if ((Boolean)e.NewValue == true)
            {
                SkinResourceBase.DefaultSkin = sender as SkinResourceBase;
            }
        }

        /// <summary>
        /// The Uri path where the resource is
        /// located.
        /// </summary>
        public Uri ResourcePath
        {
            get;
            protected set;
        }

        /// <summary>
        /// The name that this resource will have
        /// it is were to be displayed on the 
        /// UI
        /// </summary>
        public String DisplayName
        {
            get;
            protected set;
        }

        /// <summary>
        /// A instance of the actual resource dictionary for this skin resource,
        /// this is here so that the resource only gets constructed once since
        /// multiple extension could need it.
        /// </summary>
        private SkinResourceDictionary ResourceDictionary
        {
            get;
            set;
        }

        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Default constructor that makes sure the
        /// uri path and name are set
        /// </summary>
        public SkinResourceBase(String path, String displayName)
        {
            this.ResourcePath = new Uri(path, UriKind.RelativeOrAbsolute);
            this.DisplayName = displayName;
            this.IsDefault = false;

            this.ResourceDictionary = new SkinResourceDictionary();
            if (this.ResourcePath != null)
                this.ResourceDictionary.Source = this.ResourcePath;
        }

        #endregion // Constructors

        #region Members

        /// <summary>
        /// Returns the actual resource dictionary that the
        /// path is pointing to.
        /// </summary>
        public SkinResourceDictionary GetResourceDictionary()
        {
            return this.ResourceDictionary;    
        }

        #endregion // Members
    }
}
