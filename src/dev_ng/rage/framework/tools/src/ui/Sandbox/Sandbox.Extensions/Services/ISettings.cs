﻿using System;
using System.Configuration;

namespace Sandbox.Extensions.Services
{
    /// <summary>
    /// Interface for the setting service
    /// </summary>
    public interface ISettings
    {
        #region Properties

        /// <summary>
        /// The defult settings for this settings
        /// instance
        /// </summary>
        ApplicationSettingsBase Default { get; }

        #endregion // Properties
    }
}
