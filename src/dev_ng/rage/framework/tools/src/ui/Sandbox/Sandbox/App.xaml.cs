﻿using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Reflection;
using System.Diagnostics;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Collections.Generic;
using Sandbox.Extensions;
using Sandbox.Extensions.UI.Themes;
using Sandbox.Theme;

namespace Sandbox
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application, IPartImportsSatisfiedNotification
    {
        /// <summary>
        /// Application startup window
        /// </summary>
        [Import(Sandbox.Extensions.CompositionPoints.MainWindow, typeof(Window))]
        public Window MainAppWindow
        {
            get { return base.MainWindow; }
            set { base.MainWindow = value; }
        }

        /// <summary>
        /// Imports all of the skin resources. This is so that all of the
        /// skin resources are constructored before the application if rendered
        /// for the first time
        /// </summary>
        [ImportMany(ExtensionPoints.Skin, typeof(ISkinResource))]
        public IEnumerable<ISkinResource> SkinResources
        {
            get;
            set;
        }

        /// <summary>
        /// The main container that is to be used as the host
        /// for the compositions.
        /// </summary>
        private CompositionContainer Container
        {
            get;
            set;
        }

        /// <summary>
        /// Gets called when the application first starts up,
        /// need to make sure the compose operation happens
        /// in here.
        /// </summary>
        protected override void OnStartup(StartupEventArgs e)
        {
            // Make sure the extentsions folder exists, it might not exist if
            // we haven't built any yet.
            if (!Directory.Exists(Sandbox.Properties.Settings.Default.PluginPath))
            {
                Directory.CreateDirectory(Sandbox.Properties.Settings.Default.PluginPath);
            }

            base.OnStartup(e);

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            if (Compose())
            {
                stopWatch.Stop();
                Debug.Print("Composition took {0} milliseconds to complete", stopWatch.ElapsedMilliseconds);

                MainWindow.Show();
            }
            else
            {
                Shutdown();
            }
        }

        /// <summary>
        /// Gets called when the application has exited, we just have to
        /// make sure that the container is disposed of properly
        /// </summary>
        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);

            if (this.Container != null)
                this.Container.Dispose();
        }
        
        /// <summary>
        ///  Creates the composition container that the app owns
        ///  to make sure that the app is setup as the host.
        /// </summary>
        private Boolean Compose()
        {
            try
            {
                // Make sure we tell the host where the plugins are located, any class
                // that want to be exposted needs to belong to a assembly located in these
                // paths
                AggregateCatalog catalog = new AggregateCatalog();
                catalog.Catalogs.Add(new AssemblyCatalog(Assembly.GetExecutingAssembly()));
                catalog.Catalogs.Add(new DirectoryCatalog("."));
                catalog.Catalogs.Add(new DirectoryCatalog(Sandbox.Properties.Settings.Default.PluginPath));

                this.Container = new CompositionContainer(catalog);
                this.Container.ComposeParts(this);

                return true;
            }
            catch (CompositionException compositionException)
            {
                MessageBox.Show(compositionException.ToString());
                return false;
            }
        }

        /// <summary>
        /// Gets called once all the imports have been satisfied
        /// </summary>
        public void OnImportsSatisfied()
        {
            // Just need to find the default skin, since we have imported them all,
            // all of them will have been constructed and the static field for
            // the default skin will be resolved
            if (SkinResourceBase.DefaultSkin != null)
            {
                this.Resources.MergedDictionaries.RemoveAll<SkinResourceDictionary>();
                this.Resources.MergedDictionaries.Add(SkinResourceBase.DefaultSkin.GetResourceDictionary());
            }
        }
    }


    public static class CollectionExtenstions
    {
        public static void RemoveAll<T>(this System.Collections.ObjectModel.Collection<ResourceDictionary> collection)
        {
            List<ResourceDictionary> match = new List<ResourceDictionary>(collection.Where(d => d is T == true));
            foreach (var item in match)
            {
                collection.Remove(item);
            }
        }
    }

}
