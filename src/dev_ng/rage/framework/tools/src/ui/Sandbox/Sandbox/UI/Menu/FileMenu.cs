﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using Sandbox.Extensions;
using Sandbox.Extensions.Services;
using Sandbox.Extensions.Services.File;
using Sandbox.Extensions.UI.Layout;
using Sandbox.Extensions.UI.Menu;

namespace Sandbox.UI.Menu
{
    /// <summary>
    /// The mainmenu item File
    /// </summary>
    [Export(ExtensionPoints.MainMenu, typeof(IMenuItem))]
    class FileMenu : MenuItemBase
    {
        #region MEF Imports

        /// <summary>
        /// File menu subitems.
        /// </summary>
        [Import(CompositionPoints.MainWindow, typeof(Window))]
        private Window MainWindow { get; set; }

        /// <summary>
        /// File menu subitems.
        /// </summary>
        [ImportMany(ExtensionPoints.FileMenu, typeof(IMenuItem))]
        private IEnumerable<IMenuItem> items { get; set; }

        #endregion // MEF Imports

        #region Constructor(s)

        /// <summary>
        /// Default constructor
        /// </summary>
        public FileMenu()
            : base("_File")
        {
        }

        #endregion // Constructor(s)

        #region IPartImportsSatisfiedNotification Methods

        /// <summary>
        /// 
        /// </summary>
        public override void OnImportsSatisfied()
        {
            foreach (IMenuItem item in items)
            {
                this.Items.Add(item);
            }

            base.OnImportsSatisfied();
        }

        #endregion // IPartImportsSatisfiedNotification Methods
    }


    [Export(ExtensionPoints.FileMenu, typeof(IMenuItem))]
    class NewMenuItem : MenuItemBase
    {
        #region MEF Imports

        /// <summary>
        /// File menu subitems.
        /// </summary>
        [Import(CompositionPoints.MainWindow, typeof(Window))]
        private Window MainWindow { get; set; }

        /// <summary>
        /// File menu subitems.
        /// </summary>
        [Import(ExtensionPoints.LayoutManager, typeof(ILayoutManager))]
        private ILayoutManager LayoutManager { get; set; }

        /// <summary>
        /// File menu subitems.
        /// </summary>
        [Import(CompositionPoints.Configuration, typeof(IConfigurationService))]
        private IConfigurationService ConfigurationService { get; set; }

        /// <summary>
        /// New menu subitems.
        /// </summary>
        [ImportMany(ExtensionPoints.NewService, typeof(INewService))]
        private IEnumerable<INewService> newServices { get; set; }

        #endregion // MEF Imports

        #region Constructor(s)

        /// <summary>
        /// Default constructor
        /// </summary>
        public NewMenuItem()
            : base("_New")
        {
        }

        #endregion // Constructor(s)

        #region IPartImportsSatisfiedNotification Methods

        /// <summary>
        /// Gets called when all the imports have been resolved
        /// </summary>
        public override void OnImportsSatisfied()
        {
            foreach (INewService newService in this.newServices)
            {
                NewServiceMenuItem newItem = new NewServiceMenuItem(newService, LayoutManager, ConfigurationService);
                newItem.Style = this.MainWindow.TryFindResource("MenuItemStyleKey") as Style;
                this.Items.Add(newItem);
            }

            base.OnImportsSatisfied();
        }

        #endregion // IPartImportsSatisfiedNotification Methods
    }

    [Export(ExtensionPoints.FileMenu, typeof(IMenuItem))]
    class SeparatorOneMenuItem : MenuSeparatorBase
    {
        #region Constructor(s)

        /// <summary>
        /// Default constructor
        /// </summary>
        public SeparatorOneMenuItem()
        {
        }

        #endregion // Constructor(s)
    }

    [Export(ExtensionPoints.FileMenu, typeof(IMenuItem))]
    class ExitMenuItem : MenuItemBase
    {
        #region MEF Imports

        /// <summary>
        /// File menu subitems.
        /// </summary>
        [Import(CompositionPoints.MainWindow, typeof(Window))]
        private Lazy<Window> MainWindow { get; set; }

        #endregion // MEF Imports

        #region Constructor(s)

        /// <summary>
        /// Default constructor
        /// </summary>
        public ExitMenuItem()
            : base("Exit")
        {
            this.Icon = new Uri("pack://application:,,,/Sandbox;component/Resources/Images/Exit.png");
            this.InputGestureText = "Ctrl+Q";
            this.ToolTip = "Exit Application";
        }

        #endregion // Constructor(s)

        #region ICommand Interface
 
        /// <summary>
        /// This gets called when the menu item is presed
        /// </summary>
        /// <param name="parameter"></param>
        public override void Execute(Object parameter)
        {
            this.MainWindow.Value.Close();
        }

        #endregion // ICommand Interface
    }
}
