﻿using System;
using System.Windows;

namespace Sandbox.Theme
{
    public class SkinColours
    {
        public static ComponentResourceKey CommandShelfHighlightGradientBeginKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "CommandShelfHighlightGradientBegin");
            }
        }

        public static ComponentResourceKey CommandShelfHighlightGradientMiddleKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "CommandShelfHighlightGradientMiddle");
            }
        }

        public static ComponentResourceKey CommandShelfHighlightGradientEndKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "CommandShelfHighlightGradientEnd");
            }
        }

        public static ComponentResourceKey CommandBarTextActiveKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "CommandBarTextActive");
            }
        }

        public static ComponentResourceKey CommandBarTextSelectedKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "CommandBarTextSelected");
            }
        }

        public static ComponentResourceKey CommandBarTextInactiveKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "CommandBarTextInactive");
            }
        }

        public static ComponentResourceKey CommandBarTextHoverKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "CommandBarTextHover");
            }
        }

        public static ComponentResourceKey CommandBarMouseOverBackgroundBeginKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "CommandBarMouseOverBackgroundBegin");
            }
        }

        public static ComponentResourceKey CommandBarMouseOverBackgroundMiddle1Key
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "CommandBarMouseOverBackgroundMiddle1");
            }
        }

        public static ComponentResourceKey CommandBarMouseOverBackgroundMiddle2Key
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "CommandBarMouseOverBackgroundMiddle2");
            }
        }

        public static ComponentResourceKey CommandBarMouseOverBackgroundEndKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "CommandBarMouseOverBackgroundEnd");
            }
        }

        public static ComponentResourceKey CommandBarBorderKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "CommandBarBorder");
            }
        }

        public static ComponentResourceKey CommandBarMenuBorderKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "CommandBarMenuBorder");
            }
        }

        public static ComponentResourceKey CommandBarMenuBackgroundGradientBeginKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "CommandBarMenuBackgroundGradientBegin");
            }
        }

        public static ComponentResourceKey CommandBarMenuBackgroundGradientEndKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "CommandBarMenuBackgroundGradientEnd");
            }
        }

        public static ComponentResourceKey CommandBarMenuIconBackgroundKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "CommandBarMenuIconBackground");
            }
        }

        public static ComponentResourceKey CommandBarCheckBoxKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "CommandBarCheckBox");
            }
        }

        public static ComponentResourceKey CommandBarSelectedKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "CommandBarSelected");
            }
        }

        public static ComponentResourceKey CommandBarOverSelectedIconKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "CommandBarOverSelectedIcon");
            }
        }

        public static ComponentResourceKey CommandBarSelectedBorderKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "CommandBarSelectedBorder");
            }
        }

        public static ComponentResourceKey CommandBarMenuSeparatorKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "CommandBarMenuSeparator");
            }
        }

        public static ComponentResourceKey CommandBarOptionsGlyphKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "CommandBarOptionsGlyph");
            }
        }

        public static ComponentResourceKey CommandBarOptionsMouseOverGlyphKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "CommandBarOptionsMouseOverGlyph");
            }
        } 

        public static ComponentResourceKey CommandBarMenuSubmenuGlyphKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "CommandBarMenuSubmenuGlyph");
            }
        }

        public static ComponentResourceKey CommandBarMenuMouseOverSubmenuGlyphKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "CommandBarMenuMouseOverSubmenuGlyph");
            }
        }
        
        public static ComponentResourceKey CommandBarShadowKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "CommandBarShadow");
            }
        }

        public static ComponentResourceKey EnvironmentBackgroundGradientBeginKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "EnvironmentBackgroundGradientBegin");
            }
        }

        public static ComponentResourceKey EnvironmentBackgroundGradientMiddle1Key
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "EnvironmentBackgroundGradientMiddle1");
            }
        }

        public static ComponentResourceKey EnvironmentBackgroundGradientMiddle2Key
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "EnvironmentBackgroundGradientMiddle2");
            }
        }

        public static ComponentResourceKey EnvironmentBackgroundGradientEndKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "EnvironmentBackgroundGradientEnd");
            }
        }

        public static ComponentResourceKey EnvironmentBackgroundTexture1Key
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "EnvironmentBackgroundTexture1");
            }
        }

        public static ComponentResourceKey EnvironmentBackgroundTexture2Key
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "EnvironmentBackgroundTexture2");
            }
        }

        public static ComponentResourceKey FileTabTextKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "FileTabText");
            }
        }

        public static ComponentResourceKey FileTabInactiveTextKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "FileTabInactiveText");
            }
        }

        public static ComponentResourceKey FileTabSelectedGradientBeginKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "FileTabSelectedGradientBegin");
            }
        }

        public static ComponentResourceKey FileTabSelectedGradientMiddle1Key
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "FileTabSelectedGradientMiddle1");
            }
        }

        public static ComponentResourceKey FileTabSelectedGradientMiddle2Key
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "FileTabSelectedGradientMiddle2");
            }
        }

        public static ComponentResourceKey FileTabSelectedGradientEndKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "FileTabSelectedGradientEnd");
            }
        }

        public static ComponentResourceKey FileTabHotBorderKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "FileTabHotBorderKey");
            }
        }

        public static ComponentResourceKey FileTabHotGradientBeginKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "FileTabHotGradientBegin");
            }
        }

        public static ComponentResourceKey FileTabHotGradientEndKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "FileTabHotGradientBegin");
            }
        }

        public static ComponentResourceKey FileTabHotTextKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "FileTabHotText");
            }
        }

        public static ComponentResourceKey FileTabLastActiveTextKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "FileTabLastActiveText");
            }
        }

        public static ComponentResourceKey FileTabLastActiveGradientBeginKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "FileTabLastActiveGradientBegin");
            }
        }

        public static ComponentResourceKey FileTabLastActiveGradientMiddle1Key
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "FileTabLastActiveGradientMiddle1");
            }
        }

        public static ComponentResourceKey FileTabLastActiveGradientMiddle2Key
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "FileTabLastActiveGradientMiddle2");
            }
        }

        public static ComponentResourceKey FileTabLastActiveGradientEndKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "FileTabLastActiveGradientEnd");
            }
        }

        public static ComponentResourceKey EnvironmentGlyphMouseOverKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "EnvironmentGlyphMouseOver");
            }
        }

        public static ComponentResourceKey EnvironmentGlyphMouseOverBackgroundKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "EnvironmentGlyphMouseOverBackground");
            }
        }

        public static ComponentResourceKey EnvironmentGlyphMouseOverBorderKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "EnvironmentGlyphMouseOverBorder");
            }
        }

        public static ComponentResourceKey EnvironmentGlyphMouseDownKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "EnvironmentGlyphMouseDown");
            }
        }

        public static ComponentResourceKey EnvironmentGlyphMouseDownBackgroundKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "EnvironmentGlyphMouseDownBackground");
            }
        }

        public static ComponentResourceKey EnvironmentGlyphMouseDownBorderKey
        {
            get
            {
                return new ComponentResourceKey(typeof(SkinColours), "EnvironmentGlyphMouseDownBorder");
            }
        }

    }
}
