﻿using System;
using System.ComponentModel;
using System.Windows.Media;
using AvalonDock;

namespace Sandbox.Extensions.UI.Layout
{
    /// <summary>
    /// The base interface for any document
    /// in the layout.
    /// </summary>
    public interface IDocument : ILayoutItem
    {
        #region Properties

        /// <summary>
        /// A reference to the actual content for this document,
        /// this content represents the actual UI elements defined
        /// in xaml
        /// </summary>
        DocumentContent DocumentContent { get; }

        #endregion // Properties

        #region Event Handler Functions

        /// <summary>
        /// Gets called when the document is about to be opened
        /// </summary>
        void OnOpening(Object sender, EventArgs e);

        /// <summary>
        /// Gets called when the document has just opened
        /// </summary>
        void OnOpened(Object sender, EventArgs e);

        /// <summary>
        /// 
        /// </summary>
        void OnClosing(Object sender, CancelEventArgs e);

        /// <summary>
        /// 
        /// </summary>
        void OnClosed(Object sender, EventArgs e);

        /// <summary>
        /// Gets called when the document has just finished
        /// being created
        /// </summary>
        void OnCreated(Object sender, EventArgs e);

        /// <summary>
        /// Window receive focus handler.
        /// </summary>
        void OnFocus(Object sender, EventArgs e);

        /// <summary>
        /// Window lose focus handler.
        /// </summary>
        void OnLostFocus(Object sender, EventArgs e);

        #endregion // Event Handler Functions
    }
}
