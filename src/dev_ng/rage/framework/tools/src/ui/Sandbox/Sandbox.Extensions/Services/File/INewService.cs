﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sandbox.Extensions.UI.Layout;

namespace Sandbox.Extensions.Services.File
{
    /// <summary>
    /// The base interface for a new service that can be used to
    /// create new documents of a certain type
    /// </summary>
    public interface INewService
    {
        #region Properties

        /// <summary>
        /// New service header string for UI display.
        /// </summary>
        String Header { get; }

        /// <summary>
        /// New service Bitmap for UI display.
        /// </summary>
        Object Icon { get; }

        #endregion // Properties

        #region Methods
        
        /// <summary>
        /// 
        /// </summary>
        bool New(out IDocument document, String title);

        #endregion // Methods

    }
}
