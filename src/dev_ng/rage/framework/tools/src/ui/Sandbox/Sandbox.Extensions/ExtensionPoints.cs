﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sandbox.Extensions
{
    /// <summary>
    /// Defines extension points that other
    /// plugins can add on to.
    /// </summary>
    public class ExtensionPoints
    {
        #region Themes

        /// <summary>
        /// Extension point for the skins used in the application
        /// </summary>
        public const String Skin = "F6209503-840D-40E2-8334-7C4E3BD97503";

        /// <summary>
        /// Extension point for the styles used in the application
        /// </summary>
        public const String Styles = "C62E4370-6BC6-4A50-8358-24FDE4ED9197";

        #endregion // Themes

        #region UI Elements

        public const String LayoutManager = "DBCAA235-BD7D-4CB5-BA96-DF959F7450D2";

        public const String Document = "BDA8F618-3C32-4DF5-B86A-724209318053";

        #endregion // UI Element

        #region Menus

        /// <summary>
        /// Extension point to add menu items to the main menu
        /// </summary>
        public const String MainMenu = "316305CA-CF6D-4BBD-B0BC-B127631F553F";

        /// <summary>
        /// Extension point to add menu items to the main menu
        /// </summary>
        public const String FileMenu = "AA7CC40C-4A8F-420D-A333-2B6339D29B64";

        /// <summary>
        /// Extension point to add menu items to the main menu
        /// </summary>
        public const String EditMenu = "A6C832F5-C36F-442D-BB83-77F8831C9827";

        #endregion // Menus

        #region File Services

        /// <summary>
        /// Extension point to add new services to the sandbox
        /// </summary>
        public const String NewService = "0C3E96E0-BC49-4D94-885C-0D9D43201242";

        #endregion // File Services
    }
}
