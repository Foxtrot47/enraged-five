﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sandbox.Theme
{
    /// <summary>
    /// The class that should be used to create the skin resource
    /// dictionaries
    /// </summary>
    public class SkinResourceDictionary : System.Windows.ResourceDictionary
    {
    }
}
