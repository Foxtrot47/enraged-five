﻿using System;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel.Composition;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Sandbox.Extensions;
using Sandbox.Extensions.Services;
using Sandbox.Extensions.UI.Layout;
using Sandbox.Extensions.UI.Menu;

namespace Sandbox
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    //[Export(Sandbox.Extensions.CompositionPoints.MainAppWindow, typeof(Window))]
    [Export(Sandbox.Extensions.CompositionPoints.MainWindow, typeof(Window))]
    public partial class MainWindow : Window, IPartImportsSatisfiedNotification
    {
        /// <summary>
        /// The applications default settings
        /// </summary>
        [Import(CompositionPoints.AppSettings, typeof(ISettings))]
        public ISettings Settings
        {
            get;
            set;
        }

        /// <summary>
        /// The applications default settings
        /// </summary>
        [Import(CompositionPoints.AssemblyInformation, typeof(IAssemblyAttributes))]
        public IAssemblyAttributes AssemblyAttributes
        {
            get;
            set;
        }

        /// <summary>
        /// The layout manager for this application, used to display all of the documents
        /// and too windows
        /// </summary>
        [Import(ExtensionPoints.LayoutManager, typeof(ILayoutManager))]
        public ILayoutManager LayoutManager { get; set; }

        /// <summary>
        /// All of the IMenuItem interfaces that need to be added to the main menu
        /// </summary>
        [ImportMany(ExtensionPoints.MainMenu, typeof(IMenuItem))]
        public IEnumerable<IMenuItem> MainMenuItems { get; set; }

        public ObservableCollection<IMenuItem> MainMenu
        {
            get;
            set;
        }

        /// <summary>
        /// The main title for this window
        /// </summary>
        public new String Title
        {
            get { return base.Title; }
            set { base.Title = value; }
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Gets called once all the imports have been satisfied
        /// </summary>
        public void OnImportsSatisfied()
        {
            this.Title = Settings.Default.Properties["HostTitle"].DefaultValue as String;
            Boolean isDebug = (Boolean)AssemblyAttributes.GetAttribute(Extensions.Services.AssemblyAttributes.DEBUG);
            if (isDebug == true)
            {
                this.Title += " DEBUG";
            }

            this.Title += "- v" + AssemblyAttributes.GetAttributeAsString(Extensions.Services.AssemblyAttributes.ASSEMBLYVERSION);

            this.MainMenu = new ObservableCollection<IMenuItem>();
            foreach (IMenuItem item in this.MainMenuItems)
            {
                this.MainMenu.Add(item);
            }

            Layout.TestToolWindow tool = new Layout.TestToolWindow();
            LayoutManager.ShowToolWindow(tool);
        }
    }
}
