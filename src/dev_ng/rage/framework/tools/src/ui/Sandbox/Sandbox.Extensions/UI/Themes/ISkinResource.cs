﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sandbox.Theme;

namespace Sandbox.Extensions.UI.Themes
{
    /// <summary>
    /// A theme resource is a resource dictionary that
    /// can be used as a collection of swappable 
    /// theme resources
    /// </summary>
    public interface ISkinResource
    {
        #region Properties

        /// <summary>
        /// Represents whether this skin resource is the
        /// default resource
        /// </summary>
        Boolean IsDefault { get; }

        /// <summary>
        /// The Uri path where the resource is
        /// located.
        /// </summary>
        Uri ResourcePath { get; }

        /// <summary>
        /// The name that this resource will have
        /// it is were to be displayed on the 
        /// UI
        /// </summary>
        String DisplayName { get; }

        #endregion // Properties

        #region Members

        /// <summary>
        /// Returns the actual resource dictionary that the
        /// path is pointing to.
        /// </summary>
        SkinResourceDictionary GetResourceDictionary();

        #endregion // Members
    }
}
