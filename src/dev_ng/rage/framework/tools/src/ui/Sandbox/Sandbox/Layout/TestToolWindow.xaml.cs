﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AvalonDock;
using Sandbox.Extensions.UI.Layout;

namespace Sandbox.Layout
{
    /// <summary>
    /// Interaction logic for TestToolWindow.xaml
    /// </summary>
    public partial class TestToolWindow : ToolWindowBase
    {
        public TestToolWindow()
        {
            InitializeComponent();

            TreeViewItem first = new TreeViewItem();
            for (int i = 0; i < 100; i++)
            {
                TreeViewItem child = new TreeViewItem();
                child.Header = i.ToString();
                first.Items.Add(child);
            }
            TreeViewItem second = new TreeViewItem();
            for (int i = 0; i < 100; i++)
            {
                TreeViewItem child = new TreeViewItem();
                child.Header = i.ToString();
                second.Items.Add(child);
            }
            this.Tree.Items.Add(first);
            this.Tree.Items.Add(second);
        }
    }
}
