﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using Sandbox.Extensions;
using Sandbox.Extensions.Services;

namespace Sandbox
{
    [Export(CompositionPoints.Configuration, typeof(IConfigurationService))]
    class ConfigurationService : IConfigurationService
    {
        #region Properties

        private static int NewFileCounter = 1;

        #endregion // Properties

        #region Methods

        /// <summary>
        /// Returns next new file filename (basename).
        /// </summary>
        public String GetNewFilename()
        {
            return (String.Format("{0}{1}", Properties.Settings.Default.NewFilePrefix, NewFileCounter++));
        }

        #endregion // Methods
    }
}
