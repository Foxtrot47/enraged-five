﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.ComponentModel;
using System.Collections.Generic;
using System.Windows.Media;
using AvalonDock;

namespace Sandbox.Extensions.UI.Layout
{
    public class ToolWindowBase : DockableContent, IToolWindow
    {
        #region Properties

        /// <summary>
        /// A reference to the actual content for this tool window,
        /// this content represents the actual UI elements defined
        /// in xaml
        /// </summary>
        public DockableContent DockableContent
        {
            get { return this as DockableContent; }
        }

        /// <summary>
        /// Unique window name; used for layout serialisation (alpha-numeric only).
        /// </summary>
        public new String Name
        {
            get { return base.Name; }
            set { base.Name = value; }
        }

        /// <summary>
        /// Window title for UI display.
        /// </summary>
        public new String Title
        {
            get { return base.Title; }
            set { base.Title = value; }
        }

        /// <summary>
        /// Window icon for UI display.
        /// </summary>
        public new ImageSource Icon
        {
            get { return base.Icon; }
            set { base.Icon = value; }
        }

        /// <summary>
        /// Resprents whether this document is
        /// currently locked, which means that the 
        /// documents location is locked in place in the
        /// UI
        /// </summary>
        public new Boolean IsLocked
        {
            get { return base.IsLocked; }
            set { base.IsLocked = value; }
        }

        /// <summary>
        /// Represents whether this document has been
        /// modified by the user.
        /// </summary>
        public new Boolean IsModified
        {
            get { return base.IsModified; }
            set { base.IsModified = value; }
        }

        /// <summary>
        /// Represents whether this document is
        /// currently in a readonly state. This basically
        /// means whether the file the document is
        /// representing has the readonly attribute set.
        /// </summary>
        public new Boolean IsReadOnly
        {
            get { return base.IsReadOnly; }
            set { base.IsReadOnly = value; }
        }

        /// <summary>
        /// This is the path to the file that the
        /// document is representing, this is so
        /// that the IsReadOnly property can be updated
        /// and set
        /// </summary>
        public new String Path
        {
            get { return base.Path; }
            set { base.Path = value; }
        }

        #endregion // Properties

        #region Event Handler Functions

        /// <summary>
        /// Gets called when the document is about to be opened
        /// </summary>
        public virtual void OnOpening(Object sender, EventArgs e) { }

        /// <summary>
        /// Gets called when the document has just opened
        /// </summary>
        public virtual void OnOpened(Object sender, EventArgs e) { }

        /// <summary>
        /// Gets called when the document is about to close
        /// </summary>
        public virtual void OnClosing(Object sender, CancelEventArgs e) { }

        /// <summary>
        /// Gets called when the document has just closed
        /// </summary>
        public virtual void OnClosed(Object sender, EventArgs e) { }

        /// <summary>
        /// Gets called when the document has just finished
        /// being created
        /// </summary>
        public virtual void OnCreated(Object sender, EventArgs e) { }

        /// <summary>
        /// Window receive focus handler.
        /// </summary>
        public virtual void OnFocus(Object sender, EventArgs e) { }

        /// <summary>
        /// Window lose focus handler.
        /// </summary>
        public virtual void OnLostFocus(Object sender, EventArgs e) { }

        #endregion // Event Handler Functions
    }
}
