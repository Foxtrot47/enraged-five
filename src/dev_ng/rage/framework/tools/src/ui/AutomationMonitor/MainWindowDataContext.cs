﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Configuration;
using RSG.Base.Configuration.Automation;
using RSG.Base.Configuration.Bugstar;
using RSG.Automation.ViewModel;
using RSG.Editor;
using RSG.Automation.Commands;

namespace AutomationMonitor
{

    /// <summary>
    /// Main window data-context.
    /// </summary>
    internal sealed class MainWindowDataContext : NotifyPropertyChangedBase
    {
        #region Properties
        /// <summary>
        /// Automation monitor data context.
        /// </summary>
        public AutomationMonitorDataContext AutomationMonitorDataContext
        {
            get { return _automationMonitorDataContext; }
            private set { SetProperty(ref _automationMonitorDataContext, value); }
        }
        private AutomationMonitorDataContext _automationMonitorDataContext;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="config"></param>
        /// <param name="bugstarConfig"></param>
        public MainWindowDataContext(IConfig config, 
            IBugstarConfig bugstarConfig)
            : base()
        {
            _automationMonitorDataContext = new AutomationMonitorDataContext(config, bugstarConfig);
        }
        #endregion // Constructor(s)
    }

} // AutomationMonitor namespace
