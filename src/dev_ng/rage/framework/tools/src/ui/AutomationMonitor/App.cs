﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using RSG.Base.Configuration;
using RSG.Base.Configuration.Automation;
using RSG.Base.Configuration.Bugstar;
using RSG.Editor.Controls;

namespace AutomationMonitor
{

    /// <summary>
    /// 
    /// </summary>
    class App : RsApplication
    {
        #region Properties
        /// <summary>
        /// Gets the unique name for this application that is used for mutex creation, and
        /// folder organisation inside the local app data folder and the registry.
        /// </summary>
        public override string UniqueName
        {
            get { return (AutomationMonitor.Resources.StringTable.DefaultTitle); }
        }

        /// <summary>
        /// Gets a value indicating whether the layout for the main window get serialised on
        /// exit and deserialised during loading.
        /// </summary>
        protected override bool MakeLayoutPersistent
        {
            get { return true; }
        }

        /// <summary>
        /// Author E-mail addresses
        /// </summary>
        /// <returns></returns>
        public override String AuthorEmailAddress
        {
            get { return "adam.munson@rockstarnorth.com"; }
        }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="App"/> class.
        /// </summary>
        public App()
        {
        }
        #endregion Constructors

        #region Controller Methods
        /// <summary>
        /// Main entry point into the application. The first thing to get called.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            App app = new App();
            app.ShutdownMode = ShutdownMode.OnLastWindowClose;
            RsApplication.OnEntry(app, ApplicationMode.Single);
        }
        #endregion // Controller Methods

        #region RsApplication Overridden Methods
        /// <summary>
        /// Override to create the main window for the application.
        /// </summary>
        /// <returns>
        /// The System.Windows.Window that will be the main window for this application.
        /// </returns>
        protected override Window CreateMainWindow()
        {
#warning DHM FIX ME: pull this from application CommandOptions.
            IConfig config = ConfigFactory.CreateConfig();
            IBugstarConfig bugstarConfig = ConfigFactory.CreateBugstarConfig(config.Project);

            Window mainWindow = new MainWindow(this);
            mainWindow.DataContext = new MainWindowDataContext(config, bugstarConfig);
            return (mainWindow);
        }

        /// <summary>
        /// Gets called when an exception is fired from either the application or the
        /// dispatcher thread and it goes unhandled.
        /// </summary>
        /// <param name="exception">
        /// The exception that was thrown and not handled.
        /// </param>
        protected override void OnUnhandledException(Exception exception)
        {
#if false
            RSG.Base.Windows.ExceptionStackTraceDlg dlg = new RSG.Base.Windows.ExceptionStackTraceDlg(exception,
                "*tools@rockstarnorth.com", "RSGEDI Tools");
            dlg.ShowDialog();

            Environment.Exit(1);
#endif
        }
        #endregion // RsApplication Overridden Methods
    }
}
