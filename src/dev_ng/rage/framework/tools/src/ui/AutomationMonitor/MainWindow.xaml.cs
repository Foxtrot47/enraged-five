﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RSG.Automation.View;
using RSG.Automation.Commands;
using RSG.Editor;
using RSG.Editor.Controls;
using RSG.Editor.SharedCommands;
using RSG.Pipeline.Automation.Common;

namespace AutomationMonitor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : RsMainWindow
    {
        #region Constants
        /// <summary>
        /// DevStar help URL link.
        /// </summary>
        private static readonly String HELP_URL = "https://devstar.rockstargames.com/wiki/index.php/Automation_Monitor";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// DevStar Help URL.
        /// </summary>
        protected override string HelpUrl
        {
            get { return (HELP_URL); }
        }

        /// <summary>
        /// A private instance to a generic object used to support thread access.
        /// </summary>
        private static Object _syncRoot = new Object();

        /// <summary>
        /// Gets the global identifier used for the actions menu.
        /// </summary>
        public static Guid ActionsMenu
        {
            get
            {
                if (!_actionsMenu.HasValue)
                {
                    lock (_syncRoot)
                    {
                        if (!_actionsMenu.HasValue)
                        {
                            _actionsMenu = new Guid("1DCBC037-5FD7-498F-84A2-55824144F3B9");
                        }
                    }
                }

                return _actionsMenu.Value;
            }
        }
        private static Guid? _actionsMenu;

        /// <summary>
        /// Gets the global identifier used for the standard tool bar.
        /// </summary>
        public static Guid StandardToolBar
        {
            get
            {
                if (!_standardToolBar.HasValue)
                {
                    lock (_syncRoot)
                    {
                        if (!_standardToolBar.HasValue)
                        {
                            _standardToolBar =
                                new Guid("FAF1C592-8110-41AB-928E-311C4C5989F3");
                        }
                    }
                }

                return _standardToolBar.Value;
            }
        }
        private static Guid? _standardToolBar;

        /// <summary>
        /// Gets the global identifier used for the actions tool bar.
        /// </summary>
        public static Guid ActionsToolBar
        {
            get
            {
                if (!_actionsToolBar.HasValue)
                {
                    lock (_syncRoot)
                    {
                        if (!_actionsToolBar.HasValue)
                        {
                            _actionsToolBar =
                                new Guid("2C48556E-0EE9-47D2-B274-BBA678730276");
                        }
                    }
                }

                return _actionsToolBar.Value;
            }
        }
        private static Guid? _actionsToolBar;

        /// <summary>
        /// Job Filtering toolbar id.
        /// </summary>
        public static Guid JobFilterToolBar
        {
            get
            {
                if (!_jobFilterToolBar.HasValue)
                {
                    lock (_syncRoot)
                    {
                        if (!_jobFilterToolBar.HasValue)
                        {
                            _jobFilterToolBar =
                                new Guid("6997FCA4-4BE5-4DD6-AB64-0AD894C39CC6");
                        }
                    }
                }

                return _jobFilterToolBar.Value;
            }
        }
        private static Guid? _jobFilterToolBar;

        /// <summary>
        /// Gets the global identifier used for the view menu.
        /// </summary>
        public static Guid ViewMenu
        {
            get
            {
                if (!_viewMenu.HasValue)
                {
                    lock (_syncRoot)
                    {
                        if (!_viewMenu.HasValue)
                        {
                            _viewMenu =
                                new Guid("E0C59105-A327-4859-BA29-7BB540C3DBD0");
                        }
                    }
                }

                return _viewMenu.Value;
            }
        }
        private static Guid? _viewMenu;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public MainWindow(RsApplication application)
        {
            InitializeComponent();
            AddCommandInstances(application);
            AttachCommandBindings();
        }
        #endregion // Constructor(s)
        
        #region Private Methods
        /// <summary>
        /// Adds the commands/menus to the command manager.
        /// </summary>
        /// <param name="application"></param>
        private void AddCommandInstances(RsApplication application)
        {
            // Actions menu.
            RockstarCommandManager.AddActionsMenu(ActionsMenu);

            RockstarCommandManager.AddCommandInstance(
                AutomationMonitorCommands.SkipJob,
                0,
                true,
                "Skip Job",
                new Guid("42E55AE8-68FD-424A-9283-D6A01501B2E1"),
                ActionsMenu);

            RockstarCommandManager.AddCommandInstance(
                AutomationMonitorCommands.DownloadJobLog,
                0,
                true,
                "Download Job Log",
                new Guid("C2653EE1-F661-4794-9B62-F6704DFD9E09"),
                ActionsMenu);

            RockstarCommandManager.AddCommandInstance(
                AutomationMonitorCommands.DownloadJobOutput,
                0,
                false,
                "Download Job Output",
                new Guid("F70EC64B-F406-4ED4-879A-7751260D1107"),
                ActionsMenu);

            RockstarCommandManager.AddCommandInstance(
                AutomationMonitorCommands.OpenAutomationConsole,
                0,
                true,
                "Open Automation Console",
                new Guid("110DE13B-2433-4A0D-9219-C2AC31BEF16E"),
                ActionsMenu);

            RockstarCommandManager.AddCommandInstance(
                AutomationMonitorCommands.Refresh,
                0,
                true,
                "Refresh",
                new Guid("C1B51EC5-2A57-4B41-939E-0091C7397F17"),
                ActionsMenu);

            RockstarCommandManager.AddCommandInstance(
                AutomationMonitorCommands.UseUTC,
                0,
                true,
                "Use UTC",
                new Guid("9A6658B8-7F1A-44C8-9B03-9CC770BA8C7C"),
                ActionsMenu);

            // Right click menu for jobs
            RockstarCommandManager.AddCommandInstance(
                AutomationMonitorCommands.SkipJob,
                0,
                true,
                "Skip Job",
                new Guid("A043E899-9802-4525-9281-64538364DF91"),
                AutomationMonitorCommandIds.JobContextMenu);

            RockstarCommandManager.AddCommandInstance(
                AutomationMonitorCommands.DownloadJobLog,
                0,
                true,
                "Download Job Log",
                new Guid("95100484-0CE5-4F5F-B903-3087607ED3B5"),
                AutomationMonitorCommandIds.JobContextMenu);

            RockstarCommandManager.AddCommandInstance(
                AutomationMonitorCommands.DownloadJobOutput,
                0,
                false,
                "Download Job Output",
                new Guid("9348EDA6-8C9A-4AA4-8CED-78DF0F0B1657"),
                AutomationMonitorCommandIds.JobContextMenu);

            RockstarCommandManager.AddCommandInstance(
                AutomationMonitorCommands.Refresh,
                0,
                true,
                "Refresh",
                new Guid("0C910A71-3145-4819-BEDF-981F08048BEE"),
                AutomationMonitorCommandIds.JobContextMenu);

            /*
            RockstarCommandManager.AddCommandInstance(
                AutomationMonitorCommands.OpenJobJog,
                0,
                true,
                "Open Job Log",
                new Guid("9FAC9B0B-7D2C-4A4D-A176-520BE692342E"),
                AutomationMonitorCommandIds.JobContextMenu);

            RockstarCommandManager.AddCommandInstance(
                AutomationMonitorCommands.OpenJobFolder,
                0,
                true,
                "Open Job Folder",
                new Guid("C8A7DB8E-976C-401C-AC57-79A478E1DF12"),
                AutomationMonitorCommandIds.JobContextMenu);
            */

            // Job State Filtering
            RockstarCommandManager.AddToolbar(0, 0, "Filter", JobFilterToolBar);

            RockstarCommandManager.AddCommandInstance(
                AutomationMonitorCommands.FilterByJobState,
                0,
                true,
                "Filter by Job State",
                new Guid("18FB05A4-1DD6-413F-A8DE-5ED4B2BE28C7"),
                JobFilterToolBar,
                new CommandInstanceCreationArgs()
                {
                    AreMultiItemsShown = true,
                    EachItemStartsGroup = false,
                });

            // Actions toolbar
            RockstarCommandManager.AddToolbar(0, 0, "Actions", ActionsToolBar);

            RockstarCommandManager.AddCommandInstance(
                AutomationMonitorCommands.SkipJob,
                0,
                true,
                "Skip Job",
                new Guid("508F1547-2AB8-424A-B9BE-480463EDF885"),
                ActionsToolBar);

            RockstarCommandManager.AddCommandInstance(
                AutomationMonitorCommands.DownloadJobLog,
                0,
                true,
                "Download Job Log",
                new Guid("799E2CD7-82EE-4216-AA93-6C699BA82829"),
                ActionsToolBar);

            RockstarCommandManager.AddCommandInstance(
                AutomationMonitorCommands.DownloadJobOutput,
                0,
                false,
                "Download Job Output",
                new Guid("D1189FA4-C185-4EBF-B8D8-2E57D9804FCA"),
                ActionsToolBar);

            RockstarCommandManager.AddCommandInstance(
                AutomationMonitorCommands.Refresh,
                0,
                true,
                "Refresh",
                new Guid("63592066-34BF-4251-9FCE-5C19F2A09F22"),
                ActionsToolBar);

            RockstarCommandManager.AddCommandInstance(
                AutomationMonitorCommands.UseUTC,
                0,
                true,
                "Use UTC",
                new Guid("6D053D05-7E81-44AB-8D27-CE761EF3B6F4"),
                ActionsToolBar,
                new CommandInstanceCreationArgs()
                {
                    DisplayStyle = CommandItemDisplayStyle.TextOnly
                });

        }

        /// <summary>
        /// Attaches all of the necessary command bindings to this instance.
        /// </summary>
        private void AttachCommandBindings()
        {
            new RefreshAction(this.AutomationMonitorControl.DataContextResolver).AddBinding(AutomationMonitorCommands.Refresh, this);
            new SkipJobAction(this.AutomationMonitorControl.DataContextResolver).AddBinding(AutomationMonitorCommands.SkipJob, this);       
            new DownloadJobLogAction(this.AutomationMonitorControl.DataContextResolver).AddBinding(AutomationMonitorCommands.DownloadJobLog, this);
            new DownloadJobOutputAction(this.AutomationMonitorControl.DataContextResolver).AddBinding(AutomationMonitorCommands.DownloadJobOutput, this);
            new OpenAutomationConsoleAction(this.AutomationMonitorControl.DataContextResolver).AddBinding(AutomationMonitorCommands.OpenAutomationConsole, this);
            new UseUTCAction(this.AutomationMonitorControl.DataContextResolver).AddBinding(AutomationMonitorCommands.UseUTC, this);

            this.AutomationMonitorControl.GetJobDataGridControl().FilterByJobStateAction.AddBinding(AutomationMonitorCommands.FilterByJobState, this);
        }
        #endregion // Private Methods
    }

} // AutomationMonitor namespace
