﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StreamingVisualize.Model
{
    public class Box
    {
        public Vector BoxMin {get; set; }
        public Vector BoxMax {get; set; }

        public Box()
        {

        }

        public Box(Vector boxMin, Vector boxMax)
        {
            BoxMin = boxMin;
            BoxMax = boxMax;
        }

        public void Normalize()
        {
            Vector min = BoxMin.Min(BoxMax);
            Vector max = BoxMax.Max(BoxMax);

            BoxMin = min;
            BoxMax = max;
        }

        /* Returns the size of the bounding box as the length of X or Y, whichever is higher.
         * 0 if the bounding box doesn't have a current state.
         */
        public float GetSize()
        {
            return Math.Max(BoxMax.X - BoxMin.X, BoxMax.Y - BoxMax.Y);
        }

        public bool Contains(Vector point)
        {
            return (point.X >= BoxMin.X
                && point.Y >= BoxMin.Y
                && point.Z >= BoxMin.Z
                && point.X <= BoxMax.X
                && point.Y <= BoxMax.Y
                && point.Z <= BoxMax.Z);
        }
    }
}
