﻿namespace StreamingVisualize.View
{
    partial class DependentsView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ResourceName = new System.Windows.Forms.Label();
            this.DependentList = new System.Windows.Forms.DataGridView();
            this.currentNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.streamableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.DependentList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.streamableBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // ResourceName
            // 
            this.ResourceName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ResourceName.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ResourceName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ResourceName.Location = new System.Drawing.Point(0, 0);
            this.ResourceName.Name = "ResourceName";
            this.ResourceName.Size = new System.Drawing.Size(319, 13);
            this.ResourceName.TabIndex = 1;
            this.ResourceName.Text = "DEPENDENTS";
            // 
            // DependentList
            // 
            this.DependentList.AllowUserToAddRows = false;
            this.DependentList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DependentList.AutoGenerateColumns = false;
            this.DependentList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DependentList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.currentNameDataGridViewTextBoxColumn});
            this.DependentList.DataSource = this.streamableBindingSource;
            this.DependentList.Location = new System.Drawing.Point(3, 16);
            this.DependentList.Name = "DependentList";
            this.DependentList.ReadOnly = true;
            this.DependentList.RowHeadersVisible = false;
            this.DependentList.Size = new System.Drawing.Size(313, 175);
            this.DependentList.TabIndex = 2;
            this.DependentList.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DependentList_CellClick);
            this.DependentList.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DependentList_CellContentClick);
            this.DependentList.SelectionChanged += new System.EventHandler(this.DependentList_SelectionChanged);
            // 
            // currentNameDataGridViewTextBoxColumn
            // 
            this.currentNameDataGridViewTextBoxColumn.DataPropertyName = "CurrentName";
            this.currentNameDataGridViewTextBoxColumn.HeaderText = "CurrentName";
            this.currentNameDataGridViewTextBoxColumn.Name = "currentNameDataGridViewTextBoxColumn";
            this.currentNameDataGridViewTextBoxColumn.ReadOnly = true;
            this.currentNameDataGridViewTextBoxColumn.Width = 300;
            // 
            // streamableBindingSource
            // 
            this.streamableBindingSource.DataSource = typeof(StreamingVisualize.Model.Streamable);
            // 
            // DependentsView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.DependentList);
            this.Controls.Add(this.ResourceName);
            this.Name = "DependentsView";
            this.Size = new System.Drawing.Size(319, 194);
            this.Load += new System.EventHandler(this.DependentsView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DependentList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.streamableBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label ResourceName;
        private System.Windows.Forms.DataGridView DependentList;
        private System.Windows.Forms.DataGridViewTextBoxColumn currentNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource streamableBindingSource;
    }
}
