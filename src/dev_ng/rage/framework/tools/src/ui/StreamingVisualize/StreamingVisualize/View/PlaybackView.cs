﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StreamingVisualize.Controller;
using StreamingVisualize.Model;
using StreamingVisualize.Properties;

namespace StreamingVisualize.View
{
    public partial class PlaybackView : Form
    {
        private static PlaybackView Instance;

        public MainController Controller;



        public PlaybackView()
        {
            InitializeComponent();

            // See if we have a setting here.
            if (Settings.Default.PlaybackIP != null)
            {
                IPAddress.Text = Settings.Default.PlaybackIP;
            }
        }

        private void EnablePlayback_Click(object sender, EventArgs e)
        {
            // Update the settings.
            Settings.Default["PlaybackIP"] = IPAddress.Text;
            Settings.Default.Save();

            Controller.SetPlaybackIP(IPAddress.Text);
            Hide();
        }

        static public void ShowView(MainController controller, Scene scene)
        {
            if (Instance == null)
            {
                Instance = new PlaybackView();
            }

            // Update the controller - we may have been spawned from a different window.
            Instance.Controller = controller;

            if (Instance.IPAddress.Text.Length == 0 && scene.RestIP != null && scene.RestIP.Length > 0)
            {
                Instance.IPAddress.Text = scene.RestIP;
            }
            Instance.Show();
        }

        private void PlaybackView_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                Hide();
            }
        }
    }
}
