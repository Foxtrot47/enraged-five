﻿namespace StreamingVisualize.View
{
    partial class FrameView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            this.Requests = new System.Windows.Forms.DataGridView();
            this.Context = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Score = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FrameNumber = new System.Windows.Forms.Label();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Unrequests = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.CamPos = new System.Windows.Forms.Label();
            this.StreamingVolume1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.MasterCutoff = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.FailedAllocs = new System.Windows.Forms.DataGridView();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.BoxSearches = new System.Windows.Forms.DataGridView();
            this.BoxX1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BoxY1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BoxZ1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BoxX2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BoxY2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BoxZ2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label7 = new System.Windows.Forms.Label();
            this.StreamingVolume2 = new System.Windows.Forms.Label();
            this.MarkerText = new System.Windows.Forms.Label();
            this.CameraSpeed = new System.Windows.Forms.Label();
            this.Streamable = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Flags = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.requestBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.frameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.streamableDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unrequestBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.searchTypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.boxSearchBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.streamableDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reasonDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.failedAllocBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.boxSearchBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.Requests)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Unrequests)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FailedAllocs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BoxSearches)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.requestBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.unrequestBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.boxSearchBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.failedAllocBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.boxSearchBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // Requests
            // 
            this.Requests.AllowUserToAddRows = false;
            this.Requests.AllowUserToDeleteRows = false;
            this.Requests.AllowUserToResizeRows = false;
            this.Requests.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Requests.AutoGenerateColumns = false;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Requests.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.Requests.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Requests.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Streamable,
            this.Context,
            this.Score,
            this.Flags,
            this.contextDataGridViewTextBoxColumn1});
            this.Requests.DataSource = this.requestBindingSource;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Requests.DefaultCellStyle = dataGridViewCellStyle14;
            this.Requests.Location = new System.Drawing.Point(0, 19);
            this.Requests.Name = "Requests";
            this.Requests.ReadOnly = true;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Requests.RowHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.Requests.RowHeadersVisible = false;
            this.Requests.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Requests.Size = new System.Drawing.Size(325, 105);
            this.Requests.TabIndex = 0;
            this.Requests.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Requests_CellClick);
            this.Requests.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.Requests_CellFormatting);
            this.Requests.SelectionChanged += new System.EventHandler(this.Requests_SelectionChanged);
            // 
            // Context
            // 
            this.Context.DataPropertyName = "Context";
            this.Context.HeaderText = "Context";
            this.Context.Name = "Context";
            this.Context.ReadOnly = true;
            // 
            // Score
            // 
            this.Score.DataPropertyName = "Score";
            this.Score.HeaderText = "Score";
            this.Score.Name = "Score";
            this.Score.ReadOnly = true;
            // 
            // FrameNumber
            // 
            this.FrameNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FrameNumber.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.FrameNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FrameNumber.Location = new System.Drawing.Point(0, 3);
            this.FrameNumber.Name = "FrameNumber";
            this.FrameNumber.Size = new System.Drawing.Size(595, 13);
            this.FrameNumber.TabIndex = 1;
            this.FrameNumber.Text = "FRAME NUMBER";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Streamable";
            this.dataGridViewTextBoxColumn1.HeaderText = "Streamable";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Streamable";
            this.dataGridViewTextBoxColumn2.HeaderText = "Streamable";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // Unrequests
            // 
            this.Unrequests.AllowUserToAddRows = false;
            this.Unrequests.AllowUserToDeleteRows = false;
            this.Unrequests.AllowUserToResizeRows = false;
            this.Unrequests.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Unrequests.AutoGenerateColumns = false;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Unrequests.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle16;
            this.Unrequests.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Unrequests.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.frameDataGridViewTextBoxColumn,
            this.streamableDataGridViewTextBoxColumn,
            this.contextDataGridViewTextBoxColumn});
            this.Unrequests.DataSource = this.unrequestBindingSource;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Unrequests.DefaultCellStyle = dataGridViewCellStyle17;
            this.Unrequests.Location = new System.Drawing.Point(0, 20);
            this.Unrequests.Name = "Unrequests";
            this.Unrequests.ReadOnly = true;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Unrequests.RowHeadersDefaultCellStyle = dataGridViewCellStyle18;
            this.Unrequests.RowHeadersVisible = false;
            this.Unrequests.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Unrequests.Size = new System.Drawing.Size(257, 104);
            this.Unrequests.TabIndex = 3;
            this.Unrequests.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Unrequests_CellClick);
            this.Unrequests.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Unrequests_CellContentClick);
            this.Unrequests.SelectionChanged += new System.EventHandler(this.Unrequests_SelectionChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "REQUESTS:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "UNREQUESTS:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Camera Pos:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(3, 37);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(123, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Streaming Volume 1:";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Streamable";
            this.dataGridViewTextBoxColumn3.HeaderText = "Streamable";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.Requests);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.Unrequests);
            this.splitContainer1.Panel2.Controls.Add(this.label2);
            this.splitContainer1.Size = new System.Drawing.Size(590, 126);
            this.splitContainer1.SplitterDistance = 327;
            this.splitContainer1.TabIndex = 8;
            // 
            // CamPos
            // 
            this.CamPos.AutoSize = true;
            this.CamPos.Location = new System.Drawing.Point(121, 20);
            this.CamPos.Name = "CamPos";
            this.CamPos.Size = new System.Drawing.Size(46, 13);
            this.CamPos.TabIndex = 9;
            this.CamPos.Text = "             ";
            // 
            // StreamingVolume1
            // 
            this.StreamingVolume1.AutoSize = true;
            this.StreamingVolume1.Location = new System.Drawing.Point(121, 37);
            this.StreamingVolume1.Name = "StreamingVolume1";
            this.StreamingVolume1.Size = new System.Drawing.Size(58, 13);
            this.StreamingVolume1.TabIndex = 10;
            this.StreamingVolume1.Text = "                 ";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(412, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Master Cutoff:";
            // 
            // MasterCutoff
            // 
            this.MasterCutoff.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.MasterCutoff.AutoSize = true;
            this.MasterCutoff.Location = new System.Drawing.Point(505, 19);
            this.MasterCutoff.Name = "MasterCutoff";
            this.MasterCutoff.Size = new System.Drawing.Size(91, 13);
            this.MasterCutoff.TabIndex = 12;
            this.MasterCutoff.Text = "                            ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Failed Allocations:";
            // 
            // FailedAllocs
            // 
            this.FailedAllocs.AllowUserToAddRows = false;
            this.FailedAllocs.AllowUserToDeleteRows = false;
            this.FailedAllocs.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.FailedAllocs.AutoGenerateColumns = false;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.FailedAllocs.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle19;
            this.FailedAllocs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.FailedAllocs.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.streamableDataGridViewTextBoxColumn2,
            this.reasonDataGridViewTextBoxColumn1});
            this.FailedAllocs.DataSource = this.failedAllocBindingSource;
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.FailedAllocs.DefaultCellStyle = dataGridViewCellStyle20;
            this.FailedAllocs.Location = new System.Drawing.Point(0, 23);
            this.FailedAllocs.Name = "FailedAllocs";
            this.FailedAllocs.ReadOnly = true;
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle21.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle21.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle21.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle21.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.FailedAllocs.RowHeadersDefaultCellStyle = dataGridViewCellStyle21;
            this.FailedAllocs.RowHeadersVisible = false;
            this.FailedAllocs.Size = new System.Drawing.Size(326, 66);
            this.FailedAllocs.TabIndex = 14;
            this.FailedAllocs.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.FailedAllocs_CellClick);
            // 
            // splitContainer2
            // 
            this.splitContainer2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer2.Location = new System.Drawing.Point(6, 96);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.splitContainer1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.BoxSearches);
            this.splitContainer2.Panel2.Controls.Add(this.FailedAllocs);
            this.splitContainer2.Panel2.Controls.Add(this.label4);
            this.splitContainer2.Size = new System.Drawing.Size(590, 219);
            this.splitContainer2.SplitterDistance = 126;
            this.splitContainer2.TabIndex = 15;
            // 
            // BoxSearches
            // 
            this.BoxSearches.AllowUserToAddRows = false;
            this.BoxSearches.AllowUserToDeleteRows = false;
            this.BoxSearches.AllowUserToResizeRows = false;
            this.BoxSearches.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BoxSearches.AutoGenerateColumns = false;
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.BoxSearches.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle22;
            this.BoxSearches.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.BoxSearches.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.searchTypeDataGridViewTextBoxColumn,
            this.BoxX1,
            this.BoxY1,
            this.BoxZ1,
            this.BoxX2,
            this.BoxY2,
            this.BoxZ2});
            this.BoxSearches.DataSource = this.boxSearchBindingSource1;
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.BoxSearches.DefaultCellStyle = dataGridViewCellStyle23;
            this.BoxSearches.Location = new System.Drawing.Point(332, 23);
            this.BoxSearches.Name = "BoxSearches";
            this.BoxSearches.ReadOnly = true;
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle24.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle24.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.BoxSearches.RowHeadersDefaultCellStyle = dataGridViewCellStyle24;
            this.BoxSearches.RowHeadersVisible = false;
            this.BoxSearches.Size = new System.Drawing.Size(261, 69);
            this.BoxSearches.TabIndex = 15;
            // 
            // BoxX1
            // 
            this.BoxX1.DataPropertyName = "BoxX1";
            this.BoxX1.HeaderText = "BoxX1";
            this.BoxX1.Name = "BoxX1";
            this.BoxX1.ReadOnly = true;
            // 
            // BoxY1
            // 
            this.BoxY1.DataPropertyName = "BoxY1";
            this.BoxY1.HeaderText = "BoxY1";
            this.BoxY1.Name = "BoxY1";
            this.BoxY1.ReadOnly = true;
            // 
            // BoxZ1
            // 
            this.BoxZ1.DataPropertyName = "BoxZ1";
            this.BoxZ1.HeaderText = "BoxZ1";
            this.BoxZ1.Name = "BoxZ1";
            this.BoxZ1.ReadOnly = true;
            // 
            // BoxX2
            // 
            this.BoxX2.DataPropertyName = "BoxX2";
            this.BoxX2.HeaderText = "BoxX2";
            this.BoxX2.Name = "BoxX2";
            this.BoxX2.ReadOnly = true;
            // 
            // BoxY2
            // 
            this.BoxY2.DataPropertyName = "BoxY2";
            this.BoxY2.HeaderText = "BoxY2";
            this.BoxY2.Name = "BoxY2";
            this.BoxY2.ReadOnly = true;
            // 
            // BoxZ2
            // 
            this.BoxZ2.DataPropertyName = "BoxZ2";
            this.BoxZ2.HeaderText = "BoxZ2";
            this.BoxZ2.Name = "BoxZ2";
            this.BoxZ2.ReadOnly = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 54);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(123, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Streaming Volume 2:";
            // 
            // StreamingVolume2
            // 
            this.StreamingVolume2.AutoSize = true;
            this.StreamingVolume2.Location = new System.Drawing.Point(121, 54);
            this.StreamingVolume2.Name = "StreamingVolume2";
            this.StreamingVolume2.Size = new System.Drawing.Size(58, 13);
            this.StreamingVolume2.TabIndex = 17;
            this.StreamingVolume2.Text = "                 ";
            // 
            // MarkerText
            // 
            this.MarkerText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MarkerText.Location = new System.Drawing.Point(3, 71);
            this.MarkerText.Name = "MarkerText";
            this.MarkerText.Size = new System.Drawing.Size(592, 13);
            this.MarkerText.TabIndex = 18;
            this.MarkerText.Text = "MarkerText";
            // 
            // CameraSpeed
            // 
            this.CameraSpeed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CameraSpeed.Location = new System.Drawing.Point(415, 37);
            this.CameraSpeed.Name = "CameraSpeed";
            this.CameraSpeed.Size = new System.Drawing.Size(180, 13);
            this.CameraSpeed.TabIndex = 19;
            this.CameraSpeed.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // Streamable
            // 
            this.Streamable.DataPropertyName = "Streamable";
            this.Streamable.HeaderText = "Streamable";
            this.Streamable.Name = "Streamable";
            this.Streamable.ReadOnly = true;
            // 
            // Flags
            // 
            this.Flags.DataPropertyName = "Flags";
            this.Flags.HeaderText = "Flags";
            this.Flags.Name = "Flags";
            this.Flags.ReadOnly = true;
            // 
            // contextDataGridViewTextBoxColumn1
            // 
            this.contextDataGridViewTextBoxColumn1.DataPropertyName = "Context";
            this.contextDataGridViewTextBoxColumn1.HeaderText = "Context";
            this.contextDataGridViewTextBoxColumn1.Name = "contextDataGridViewTextBoxColumn1";
            this.contextDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // requestBindingSource
            // 
            this.requestBindingSource.DataSource = typeof(StreamingVisualize.Model.Request);
            // 
            // frameDataGridViewTextBoxColumn
            // 
            this.frameDataGridViewTextBoxColumn.DataPropertyName = "Frame";
            this.frameDataGridViewTextBoxColumn.HeaderText = "Frame";
            this.frameDataGridViewTextBoxColumn.Name = "frameDataGridViewTextBoxColumn";
            this.frameDataGridViewTextBoxColumn.ReadOnly = true;
            this.frameDataGridViewTextBoxColumn.Visible = false;
            // 
            // streamableDataGridViewTextBoxColumn
            // 
            this.streamableDataGridViewTextBoxColumn.DataPropertyName = "Streamable";
            this.streamableDataGridViewTextBoxColumn.HeaderText = "Streamable";
            this.streamableDataGridViewTextBoxColumn.Name = "streamableDataGridViewTextBoxColumn";
            this.streamableDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // contextDataGridViewTextBoxColumn
            // 
            this.contextDataGridViewTextBoxColumn.DataPropertyName = "Context";
            this.contextDataGridViewTextBoxColumn.HeaderText = "Context";
            this.contextDataGridViewTextBoxColumn.Name = "contextDataGridViewTextBoxColumn";
            this.contextDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // unrequestBindingSource
            // 
            this.unrequestBindingSource.DataSource = typeof(StreamingVisualize.Model.Unrequest);
            // 
            // searchTypeDataGridViewTextBoxColumn
            // 
            this.searchTypeDataGridViewTextBoxColumn.DataPropertyName = "SearchType";
            this.searchTypeDataGridViewTextBoxColumn.HeaderText = "SearchType";
            this.searchTypeDataGridViewTextBoxColumn.Name = "searchTypeDataGridViewTextBoxColumn";
            this.searchTypeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // boxSearchBindingSource1
            // 
            this.boxSearchBindingSource1.DataSource = typeof(StreamingVisualize.Model.BoxSearch);
            // 
            // streamableDataGridViewTextBoxColumn2
            // 
            this.streamableDataGridViewTextBoxColumn2.DataPropertyName = "Streamable";
            this.streamableDataGridViewTextBoxColumn2.FillWeight = 200F;
            this.streamableDataGridViewTextBoxColumn2.HeaderText = "Streamable";
            this.streamableDataGridViewTextBoxColumn2.Name = "streamableDataGridViewTextBoxColumn2";
            this.streamableDataGridViewTextBoxColumn2.ReadOnly = true;
            this.streamableDataGridViewTextBoxColumn2.Width = 200;
            // 
            // reasonDataGridViewTextBoxColumn1
            // 
            this.reasonDataGridViewTextBoxColumn1.DataPropertyName = "Reason";
            this.reasonDataGridViewTextBoxColumn1.FillWeight = 300F;
            this.reasonDataGridViewTextBoxColumn1.HeaderText = "Reason";
            this.reasonDataGridViewTextBoxColumn1.Name = "reasonDataGridViewTextBoxColumn1";
            this.reasonDataGridViewTextBoxColumn1.ReadOnly = true;
            this.reasonDataGridViewTextBoxColumn1.Width = 300;
            // 
            // failedAllocBindingSource
            // 
            this.failedAllocBindingSource.DataSource = typeof(StreamingVisualize.Model.FailedAlloc);
            // 
            // boxSearchBindingSource
            // 
            this.boxSearchBindingSource.DataSource = typeof(StreamingVisualize.Model.BoxSearch);
            // 
            // FrameView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.CameraSpeed);
            this.Controls.Add(this.MarkerText);
            this.Controls.Add(this.StreamingVolume2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.splitContainer2);
            this.Controls.Add(this.MasterCutoff);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.StreamingVolume1);
            this.Controls.Add(this.CamPos);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.FrameNumber);
            this.Name = "FrameView";
            this.Size = new System.Drawing.Size(599, 318);
            this.Load += new System.EventHandler(this.FrameView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Requests)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Unrequests)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.FailedAllocs)).EndInit();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BoxSearches)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.requestBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.unrequestBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.boxSearchBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.failedAllocBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.boxSearchBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.DataGridView Requests;
        public System.Windows.Forms.BindingSource requestBindingSource;
        private System.Windows.Forms.Label FrameNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridView Unrequests;
        private System.Windows.Forms.BindingSource unrequestBindingSource;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label CamPos;
        private System.Windows.Forms.Label StreamingVolume1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label MasterCutoff;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView FailedAllocs;
        private System.Windows.Forms.BindingSource failedAllocBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn streamableDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn reasonDataGridViewTextBoxColumn1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.DataGridViewTextBoxColumn frameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn streamableDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn contextDataGridViewTextBoxColumn;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label StreamingVolume2;
        private System.Windows.Forms.Label MarkerText;
        private System.Windows.Forms.DataGridViewTextBoxColumn Streamable;
        private System.Windows.Forms.DataGridViewTextBoxColumn Context;
        private System.Windows.Forms.DataGridViewTextBoxColumn Score;
        private System.Windows.Forms.DataGridViewTextBoxColumn Flags;
        private System.Windows.Forms.DataGridViewTextBoxColumn contextDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridView BoxSearches;
        private System.Windows.Forms.BindingSource boxSearchBindingSource;
        private System.Windows.Forms.BindingSource boxSearchBindingSource1;
        private System.Windows.Forms.DataGridViewTextBoxColumn searchTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn BoxX1;
        private System.Windows.Forms.DataGridViewTextBoxColumn BoxY1;
        private System.Windows.Forms.DataGridViewTextBoxColumn BoxZ1;
        private System.Windows.Forms.DataGridViewTextBoxColumn BoxX2;
        private System.Windows.Forms.DataGridViewTextBoxColumn BoxY2;
        private System.Windows.Forms.DataGridViewTextBoxColumn BoxZ2;
        private System.Windows.Forms.Label CameraSpeed;
    }
}
