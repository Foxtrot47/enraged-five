﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StreamingVisualize.Model
{
    public class StreamingVolume : IVersionedSerializable
    {
        public enum VolumeType
        {
            NONE,
            SPHERE,
            FRUSTUM,
            LINE,
        }

		public const int FLAG_STATICBOUNDS_MOVER = (1 << 0);
		public const int FLAG_STATICBOUNDS_WEAPONS = (1 << 1);
		public const int FLAG_MAPDATA_HIGH = (1 << 2);
		public const int FLAG_MAPDATA_MEDIUM = (1 << 3);
		public const int FLAG_MAPDATA_LOW = ( 1<< 4);
        public const int FLAG_METADATA = (1 << 5);

        private Dictionary<int, String> FlagNames = new Dictionary<int, String>()
        {
            { FLAG_STATICBOUNDS_MOVER, "MoverBnds" },
            { FLAG_STATICBOUNDS_WEAPONS, "WpnBnds" },
            { FLAG_MAPDATA_HIGH, "MapDataHigh" },
            { FLAG_MAPDATA_MEDIUM, "MapDataMed" },
            { FLAG_MAPDATA_LOW, "MapDataLow" },
            { FLAG_METADATA, "Metadata" },
        };

        public VolumeType Type { get; set; }

        public Vector StreamVolPos = new Vector();
        public Vector StreamVolDir = new Vector();

        public float FarClip { get; set; }
        public float Radius { get; set; }
        public uint AssetFlags { get; set; }

        public StreamingVolume()
        {
            Type = VolumeType.NONE;
        }

        private String CreateFlagString()
        {
            String result = "";

            foreach (KeyValuePair<int, String> kvp in FlagNames)
            {
                if ((AssetFlags & kvp.Key) != 0)
                {
                    result += kvp.Value + " ";
                }
            }

            return result;
        }

        public override string ToString()
        {
            switch (Type)
            {
                case VolumeType.NONE:
                    return "NONE";

                case VolumeType.SPHERE:
                    return String.Format("SPHERE: {0} {1} {2}, Radius {3}, {4}",
                        StreamVolPos.X, StreamVolPos.Y, StreamVolPos.Z, Radius, CreateFlagString());

                case VolumeType.FRUSTUM:
                    return String.Format("FRUSTUM: {0} {1} {2}, Dir {3}, {4}, {5}, {6}",
                        StreamVolPos.X, StreamVolPos.Y, StreamVolPos.Z,
                        StreamVolDir.X, StreamVolDir.Y, StreamVolDir.Z,
                        CreateFlagString());

                case VolumeType.LINE:
                    return String.Format("LINE: {0} {1} {2}, Dir {3}, {4}, {5}, {6}",
                        StreamVolPos.X, StreamVolPos.Y, StreamVolPos.Z,
                        StreamVolDir.X, StreamVolDir.Y, StreamVolDir.Z,
                        CreateFlagString());
            }

            return "";
        }

        public void Serialize(VersionedWriter writer)
        {
            writer.Write((byte) Type);
            writer.Write(StreamVolPos);
            writer.Write(StreamVolDir);

            writer.Write(FarClip);
            writer.Write(Radius);
            writer.Write(AssetFlags);
        }

        public void Deserialize(VersionedReader reader, int version)
        {
            Type = (VolumeType)reader.ReadByte();
            StreamVolPos = reader.ReadVector();
            StreamVolDir = reader.ReadVector();
            FarClip = reader.ReadFloat();
            Radius = reader.ReadFloat();
            AssetFlags = reader.ReadUint();
        }
    }
}
