﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StreamingVisualize.Model;
using StreamingVisualize.Controller;
using StreamingVisualize.View.Components;

namespace StreamingVisualize.View
{
    public partial class RscListView : UserControl, IViewController
    {
        private MainController Controller;
        private Scene Scene;
        private ViewContextMenu viewContextMenu = new ViewContextMenu();

        private IList<Streamable> Streamables;

        private bool ChangingSelection = false;

        private int StatesToShow = 0xff & ~((1 << (int) Streamable.Status.NOTLOADED) | (1 << (int) Streamable.Status.UNREGISTERED) | (1 << (int) Streamable.Status.INVALID));
        private ContextMenuHelper.ToggleIntBoolDictionary ShowFlagTypes = new ContextMenuHelper.ToggleIntBoolDictionary();



        public RscListView()
        {
            InitializeComponent();
        }


        public void PopulateList()
        {
            if (Scene == null)
            {
                return;
            }

            ChangingSelection = true;

            List<Streamable> list = new List<Streamable>();

            lock (Scene.StrIndexToIndex)
            {
                foreach (int strIndex in Scene.StrIndexToIndex.Keys)
                {
                    Streamable streamable = Scene.GetStreamable(strIndex);

                    if (streamable != null)
                    {
                        if (((1 << (int)streamable.GetCurrentStatus()) & StatesToShow) != 0)
                        {
                            //                        if (!Scene.Filter.IsFilteredOut(streamable))
                            if (!streamable.Filtered)
                            {
                                int streamableFlags = streamable.History.CurrentState.Flags;
                                bool filtered = false;

                                foreach (int flag in ShowFlagTypes.Value.Keys)
                                {
                                    if (ShowFlagTypes.Value[flag])
                                    {
                                        if ((streamableFlags & flag) == 0)
                                        {
                                            filtered = true;
                                            continue;
                                        }
                                    }
                                }

                                if (!filtered)
                                {
                                    list.Add(streamable);
                                }
                            }
                        }
                    }
                }
            }

            RscList.DataSource = Streamables = list;
            ChangingSelection = false;
        }


        // Switch the display to focus on a certain frame
        public void SetToFrame(int frameNumber)
        {
            PopulateList();
        }

        // A new device was registered, or a device has changed.
        public void OnDeviceChanged(int deviceIndex, Device device) { }

        // A streamable has been selected
        public void OnSelectStreamable(Streamable streamable)
        {
        }

        // The client information has been updated
        public void ClientInfoUpdated() { }

        // We're in follow mode, and a new frame has been added.
        public void OnNewFrame(int frameNumber) { }

        // A new entity type was registered
        public void OnNewEntityType(int entityTypeId, EntityType type) { }

        // The connection has been terminated
        public void OnConnectionLost() { }

        // Register this view with the system
        public void Register(MainController controller, Scene scene)
        {
            this.Controller = controller;
            this.Scene = scene;

            PopulateList();
        }

        public void OnFilterChanged()
        {
            PopulateList();
        }

        public void OnFrameRangeChanged(int firstFrame, int lastFrame)
        {
        }

        private void ToggleStateMask(int bitMask)
        {
            StatesToShow ^= bitMask;
            RecreateContextMenu();
            PopulateList();
        }

        private void PopulateContextMenu(ContextMenu menu)
        {
            Array stateValues = Enum.GetValues(typeof(Streamable.Status));
            MenuItem[] statesMenu = new MenuItem[stateValues.Length];
            int index = 0;

            foreach(Streamable.Status val in stateValues)
            {
                int bitMask = 1 << (int) val;
                MenuItem item = new MenuItem(Enum.GetName(typeof(Streamable.Status), val));
                item.Click += (sender, e) => ToggleStateMask(bitMask);
                item.Checked = (StatesToShow & bitMask) != 0;
                statesMenu[index++] = item;
            }

            MenuItem state = new MenuItem("Show Only", statesMenu);
            menu.MenuItems.Add(state);

            menu.MenuItems.Add(ContextMenuHelper.CreateCheckboxSubmenu("&Flags", ShowFlagTypes, Request.FlagTypes, () => PopulateList(), false));

        }

        public void OnSelectEntity(Entity entity) { }

        private void DumpList()
        {
            StringBuilder result = new StringBuilder(32768);
            result.Append("Name\tStatus\tFlags\tstrIndex\tModule\tVirtSize\tPhysSize\tFileSize\tLSN\tClosest Entity\tEntity Distance\r\n");

            foreach (Streamable streamable in Streamables)
            {
                Entity closestEntity = null;
                float closestDistance = float.MaxValue;
                GetClosestEntity(streamable, ref closestEntity, ref closestDistance);

                string closestEntityName = (closestEntity != null) ? closestEntity.Streamable.CurrentName : "";
                string closestDistanceString = (closestDistance < float.MaxValue) ? string.Format("{0}", closestDistance) : "";


                result.Append(String.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\r\n",
                    streamable.CurrentName, streamable.CurrentStatus, streamable.CurrentFlags,
                    streamable.Index, streamable.Module.Name, streamable.VirtSize, streamable.PhysSize, streamable.FileSize,
                    streamable.LSN, closestEntityName, closestDistanceString));
            }

            Clipboard.SetText(result.ToString());
            MessageBox.Show("List copied to clipboard.");
        }


        private void RecreateContextMenu()
        {
            ContextMenu menu = viewContextMenu.CreateContextMenu(Controller, this);
            PopulateContextMenu(menu);

            menu.MenuItems.Add(ContextMenuHelper.CreateActionMenu("Dump List", DumpList));

            this.ContextMenu = menu;
        }

        private void RscListView_Load(object sender, EventArgs e)
        {
            RecreateContextMenu();
        }

        private void RscList_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (RscList.Columns[e.ColumnIndex].Name == "Module")
            {
                if (e.Value != null)
                {
                    e.Value = ((Module)e.Value).Name;
                }
            }
            else if (RscList.Columns[e.ColumnIndex].Name == "LSN")
            {
                if (e.Value != null)
                {
                    e.Value = String.Format("{0:X}", e.Value);
                }
            }
        }

        private void RscList_SelectionChanged(object sender, EventArgs e)
        {
            if (ChangingSelection)
            {
                return;
            }

            if (RscList.SelectedCells.Count > 0 && Streamables != null)
            {
                int index = RscList.SelectedCells[0].RowIndex;

                if (index >= 0 && index < Streamables.Count)
                {
                    Controller.OnSelectStreamable(Streamables[index], true);
                }
            }
        }

        private void GetClosestEntity(Streamable streamable, ref Entity bestEntity, ref float bestDist)
        {
            Module archModule = Scene.GetModule(Scene.ArchetypeModuleIndex);

            if (streamable.Module == archModule)
            {
                // This is an archetype. Try to find the best entity.
                foreach (Entity entity in Scene.CurrentFrame.Entities)
                {
                    if (entity.Streamable == streamable)
                    {
                        float dist = Scene.GetFrame(Scene.CurrentFrameNumber).CamPos.Dist(entity.PositionHistory.CurrentState.GetCenter());
                        if (dist < bestDist)
                        {
                            bestDist = dist;
                            bestEntity = entity;
                        }
                    }
                }
            }
            else
            {
                // Look at the dependent streamables.
                foreach (Streamable dependent in streamable.IdentityHistory.GetStateAt(Scene.CurrentFrameNumber).Dependents)
                {
                    GetClosestEntity(dependent, ref bestEntity, ref bestDist);
                }
            }
        }

        private void RscList_CellValueNeeded(object sender, DataGridViewCellValueEventArgs e)
        {
            if (e.ColumnIndex >= 4 || e.ColumnIndex <= 7)
            {
                // Closest entity.
                // Find the archetypes of this resource.
                int row = e.RowIndex;

                if (row >= 0 && row < Streamables.Count)
                {
                    Streamable streamable = Streamables[row];

                    if (e.ColumnIndex == 4 || e.ColumnIndex == 5)
                    {
                        e.Value = ((e.ColumnIndex == 4) ? streamable.VirtSize : streamable.PhysSize) / 1024;
                        //                        Streamable.Identity identity = streamable.IdentityHistory.GetStateAt(Scene.CurrentFrameNumber);
                    }
                    else
                    {
                        Entity entity = null;
                        float bestDist = float.MaxValue;
                        GetClosestEntity(streamable, ref entity, ref bestDist);

                        if (entity != null)
                        {
                            if (e.ColumnIndex == 6)
                            {
                                e.Value = entity.Streamable.CurrentName;
                            }
                            else
                            {
                                e.Value = bestDist;
                            }

                            return;
                        }

                        e.Value = "";
                    }
                }
            }
        }
    }
}
