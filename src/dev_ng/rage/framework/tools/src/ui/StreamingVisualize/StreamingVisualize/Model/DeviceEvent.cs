﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace StreamingVisualize.Model
{
    public class DeviceEvent : IVersionedSerializable
    {
        public enum ActionType
        {
            QUEUE,
            READ,

            OPEN,
            CLOSE,
            LOWREAD,
            LOWWRITE,
            LOWSEEK,
            LOWREADEND,

            DECOMPRESS,
            DECOMPRESSEND,

            IDLE,
            MARKER,
        }

        public enum LayerType
        {
            PHYSICAL,
            STREAMER,
            MAIN,
            META,
        };
        
        public int Frame { get; set; }
        public float Time { get; set; }
        public int LSN { get; set; }
        public int Size { get; set; }
        public int Handle { get; set; }

        public ActionType Action { get; set; }
        public Streamable Streamable { get; set; }

        public String Marker { get; set; }

        public LayerType Layer
        {
            get
            {
                switch (Action)
                {
                    case ActionType.QUEUE:
                    case ActionType.MARKER:
                        return LayerType.MAIN;

                    case ActionType.OPEN:
                    case ActionType.CLOSE:
                    case ActionType.LOWREAD:
                    case ActionType.LOWWRITE:
                    case ActionType.LOWSEEK:
                    case ActionType.LOWREADEND:
                        return LayerType.PHYSICAL;

                    case ActionType.DECOMPRESS:
                    case ActionType.DECOMPRESSEND:
                    case ActionType.READ:
                        return LayerType.STREAMER;

                    case ActionType.IDLE:
                        return LayerType.META;

                    default:
                        Debug.Assert(false);
                        return LayerType.META;
                }
            }
        }



        public DeviceEvent()
        {
            Marker = "";
        }

        public void Serialize(VersionedWriter writer)
        {
            writer.Write(Frame);
            writer.Write(Time);
            writer.Write(LSN);
            writer.Write((byte)Action);
            writer.WriteStreamableRef(Streamable);

            // Version 4
            writer.Write(Size);

            // Version 5
            writer.Write(Handle);

            // Version 33
            writer.Write(Marker);
        }

        public void Deserialize(VersionedReader reader, int version)
        {
            Frame = reader.ReadInt();
            Time = reader.ReadFloat();
            LSN = reader.ReadInt();
            Action = (ActionType)reader.ReadByte();
            Streamable = reader.ReadStreamableRef();

            if (version > 3)
            {
                Size = reader.ReadInt();
            }

            if (version > 4)
            {
                Handle = reader.ReadInt();
            }

            if (version >= 33)
            {
                Marker = reader.ReadString();
            }

            if (Action == ActionType.READ && Streamable != null && Streamable.LSN == 0)
            {
                Streamable.LSN = LSN;
            }
        }
    }
}
