﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StreamingVisualize.Model
{
    public class FailedAlloc : IVersionedSerializable
    {
        public enum ReasonType
        {
            FRAGMENTED,
            OOM,
            CHURN_PROTECTION,
        }

        public ReasonType Reason { get; set; }
        public Streamable Streamable { get; set; }

        public void Serialize(VersionedWriter writer)
        {
            writer.Write((byte) Reason);
            writer.WriteStreamableRef(Streamable);
        }

        public void Deserialize(VersionedReader reader, int version)
        {
            Reason = (ReasonType) reader.ReadByte();
            Streamable = reader.ReadStreamableRef();
        }
    }
}
