﻿namespace StreamingVisualize.View
{
    partial class FrameOverview
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // FrameOverview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.DoubleBuffered = true;
            this.Name = "FrameOverview";
            this.Size = new System.Drawing.Size(661, 136);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.FrameOverview_Paint);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrameOverview_KeyDown);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FrameOverview_MouseDown);
            this.MouseEnter += new System.EventHandler(this.FrameOverview_MouseEnter);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FrameOverview_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FrameOverview_MouseUp);
            this.ResumeLayout(false);

        }

        #endregion
    }
}
