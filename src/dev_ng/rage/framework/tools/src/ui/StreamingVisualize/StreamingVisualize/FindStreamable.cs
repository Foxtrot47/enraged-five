﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StreamingVisualize.Model;

namespace StreamingVisualize.View
{
    public partial class FindStreamable : Form
    {
        public Scene Scene { get; set; }

        public IList<Streamable> FilteredList;

        public delegate void OnItemSelected(Streamable streamable);
        public OnItemSelected OnItemSelectedCb;

        public FindStreamable(Scene scene)
        {
            this.Scene = scene;
            InitializeComponent();
        }

        private void StreamableName_TextChanged(object sender, EventArgs e)
        {
            String searchString = StreamableName.Text;

            if (searchString.Length < 3)
            {
                Results.DataSource = null;
                return;
            }

            IList<Streamable> it = Scene.StreamableLookup.Where(kvp => kvp.Key.StartsWith(searchString)).Select(kvp => kvp.Value).ToList();

            Results.DataSource = it;
            FilteredList = it;
        }

        private void Results_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void StreamableName_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyData)
            {
                case Keys.Up:
                    if (Results.SelectedIndex > 0)
                    {
                        Results.SelectedIndex--;
                    }
                    break;

                case Keys.Down:
                    if (FilteredList != null && Results.SelectedIndex < FilteredList.Count - 1)
                    {
                        Results.SelectedIndex++;
                    }
                    break;

                case Keys.Return:
                    int row = Results.SelectedIndex;
                    if (FilteredList != null && row >= 0 && row < FilteredList.Count)
                    {
                        Streamable streamable = FilteredList[row];

                        if (OnItemSelectedCb != null)
                        {
                            OnItemSelectedCb(streamable);
                        }

                        Dispose();
                    }
                    break;
            }
        }

        protected override bool IsInputKey(Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Up:
                case Keys.Down:
                case Keys.Return:
                    return true;
            }
            return base.IsInputKey(keyData);
        }

        private void Results_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int row = Results.SelectedIndex;
            if (FilteredList != null && row >= 0 && row < FilteredList.Count)
            {
                Streamable streamable = FilteredList[row];

                if (OnItemSelectedCb != null)
                {
                    OnItemSelectedCb(streamable);
                }

                Dispose();
            }
        }
    }
}
