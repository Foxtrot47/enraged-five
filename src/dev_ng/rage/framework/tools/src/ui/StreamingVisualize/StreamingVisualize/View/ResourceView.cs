﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StreamingVisualize.Model;
using StreamingVisualize.Controller;
using StreamingVisualize.View.Components;

namespace StreamingVisualize.View
{
    public partial class ResourceView : UserControl, IViewController
    {
        private ViewContextMenu viewContextMenu = new ViewContextMenu();

        public MainController Controller;
        private Scene Scene;

        private List<RscEvent> eventList;
        private Font boldFont;

        private Streamable CurrentParent;
        private Streamable CurrentStreamable;

        // The frame we're trying to focus on.
        private int CurrentFrame;

        // If true, we're changing lists around. Don't respond to selection changes.
        private bool ChangingSelection;

        // If true, skip the check to see whether or not we're still in the same frame when
        // updating the visual data.
        private bool ForceFrameUpdate;

        private bool ShowMapDeps;

        private ContextMenuHelper.ToggleBool ShowRelativeTimes = new ContextMenuHelper.ToggleBool(true);



        public ResourceView()
        {
            InitializeComponent();
        }

        private void ResourceView_Load(object sender, EventArgs e)
        {
            ContextMenu = viewContextMenu.CreateContextMenu(Controller, this);

            ContextMenu.MenuItems.Add(ContextMenuHelper.CreateToggleMenu("Show &Relative Times", ShowRelativeTimes, () => Refresh()));
			ContextMenu.MenuItems.Add(ContextMenuHelper.CreateActionMenu("What Blocked This?", DumpEntityAnalysis));
        }

        private String MakeNiceSizeString(int size)
        {
            if (size < 4096)
            {
                return String.Format("{0}b", size);
            }

            return String.Format("{0}K", size / 1024);
        }

        private void SetToStreamable(Streamable streamable)
        {
            CurrentStreamable = streamable;
            ChangingSelection = true;

            ResourceName.Text = streamable.GetNameAt(CurrentFrame);
            ModuleName.Text = streamable.Module.Name;
            VirtSize.Text = "Virtual Memory: " + MakeNiceSizeString(streamable.VirtSize);
            PhysSize.Text = "Physical Memory: " + MakeNiceSizeString(streamable.PhysSize);
            FileSize.Text = "File Size: " + MakeNiceSizeString(streamable.FileSize);

            CurrentParent = streamable.ParentFile;

            if (streamable.ParentFile == null)
            {
                ParentLink.Text = "none";
                ParentLink.Enabled = false;
            }
            else
            {
                ParentLink.Text = streamable.ParentFile.GetNameAt(CurrentFrame);
                ParentLink.Enabled = true;
            }

            DependencyGraph.SetStreamable(streamable, CurrentFrame, Scene, ShowMapDeps);

            eventList = streamable.Events;
            EventHistory.DataSource = eventList;

            if (!ForceFrameUpdate)
            {
                SelectEventClosestToFrame(CurrentFrame);
            }

            StrIndex.Text = String.Format("strIndex: 0x{0:X} ({0})", streamable.StrIndex, streamable.StrIndex);

            ChangingSelection = false;
        }

        public void OnSelectEntity(Entity entity) { }

        private void SelectEventClosestToFrame(int frameNumber)
        {
            if (eventList == null || eventList.Count == 0)
            {
                return;
            }

            // Try to find a good frame to focus on.
            // TODO: Use a binary search or something!
            int rowCount = eventList.Count;

            // Note - sometimes the UI takes a while to update the cells.
            rowCount = Math.Min(rowCount, EventHistory.Rows.Count);
            int bestRow = 0;

            for (int x = 0; x < rowCount; x++)
            {
                if (eventList[x].FrameNumber > frameNumber)
                {
                    break;
                }

                bestRow = x;
            }

            EventHistory.ClearSelection();
            EventHistory.CurrentCell = EventHistory.Rows[bestRow].Cells[0];
        }
        /*
        private void EventHistory_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int row = e.RowIndex;

            if (row >= 0 && row < eventList.Count)
            {
                RscEvent rscEvent = eventList[row];
                int frameNumber = rscEvent.FrameNumber;
                Controller.SetToFrame(frameNumber);
            }
        }
        */
        private void EventHistory_SelectionChanged(object sender, EventArgs e)
        {
//            Console.WriteLine("Resource selection changed");
            if (ChangingSelection)
            {
                return;
            }

            if (EventHistory.SelectedCells.Count > 0)
            {
                int index = EventHistory.SelectedCells[0].RowIndex;

                if (index >= 0 && index < eventList.Count)
                {
                    RscEvent rscEvent = eventList[index];
                    float timeToPick = -1.0f;

                    // Now which column did we click?
                    int colIndex = EventHistory.SelectedCells[0].ColumnIndex;
                    int deviceIndex = -1;   // TODO: Need to get this somehow
                    String column = EventHistory.Columns[colIndex].Name;
                    if (column == "Frame")
                    {
                        ForceFrameUpdate = true;
                        Controller.SetToFrame(rscEvent.FrameNumber, true);
                        ForceFrameUpdate = false;
                    }
                    else if (column == "StartTime")
                    {
                        timeToPick = rscEvent.StartTime;
                    }
                    else if (column == "IssueTime")
                    {
                        timeToPick = rscEvent.IssueTime;
                    }
                    else if (column == "ProcessTime")
                    {
                        timeToPick = rscEvent.ProcessTime;
                    }
                    else if (column == "LoadTime")
                    {
                        timeToPick = rscEvent.LoadTime;
                    }

                    if (timeToPick != -1.0f)
                    {
                        Console.WriteLine("Picking device " + deviceIndex + " at " + timeToPick);
                    }

                    //                    Controller.ResourceView.SetToStreamable(unrequest.Streamable);
                }
            }
        }

        private void EventHistory_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            int row = e.RowIndex;

            if (row >= 0 && row < eventList.Count)
            {
                RscEvent rscEvent = eventList[row];
                bool isPriority = ((rscEvent.Flags & Request.PRIORITY_LOAD) != 0);

                if (EventHistory.Columns[e.ColumnIndex].Name == "Action")
                {
                    if (boldFont == null)
                    {
                        boldFont = new Font(e.CellStyle.Font, FontStyle.Bold);
                    }

                    if (isPriority)
                    {
                        e.CellStyle.Font = boldFont;
                    }
                }
                else if (EventHistory.Columns[e.ColumnIndex].Name == "Score")
                {
                    // Don't show negative numbers.
                    String scoreString = e.Value.ToString();
                    if (scoreString.Length > 0 && scoreString[0] == '-')
                    {
                        e.Value = "";
                    }
                }
                else if (EventHistory.Columns[e.ColumnIndex].Name == "StartTime")
                {
                    if (rscEvent.StartTime == 0.0f)
                    {
                        e.Value = "";
                    }
                }
                else if (EventHistory.Columns[e.ColumnIndex].Name == "Flags")
                {
                    e.Value = Request.CreateFlagString(rscEvent.Flags);
                }
                else if (EventHistory.Columns[e.ColumnIndex].Name == "IssueTime")
                {
                    if (rscEvent.IssueTime == 0.0f)
                    {
                        e.Value = "";
                    }
                    else if (ShowRelativeTimes.Value && rscEvent.StartTime != 0.0f)
                    {
                        e.Value = "+" + (rscEvent.IssueTime - rscEvent.StartTime);
                    }
                }
                else if (EventHistory.Columns[e.ColumnIndex].Name == "ProcessTime")
                {
                    if (rscEvent.ProcessTime == 0.0f)
                    {
                        e.Value = "";
                    }
                    else if (ShowRelativeTimes.Value)
                    {
                        e.Value = "+" + (rscEvent.ProcessTime - rscEvent.GetStartTime());
                    }
                }
                else if (EventHistory.Columns[e.ColumnIndex].Name == "LoadTime")
                {
                    if (rscEvent.LoadTime == 0.0f)
                    {
                        e.Value = "";
                    }
                    else if (ShowRelativeTimes.Value)
                    {
                        e.Value = "+" + (rscEvent.LoadTime - rscEvent.GetStartTime());
                    }
                }
            }
        }

        // Switch the display to focus on a certain frame
        public void SetToFrame(int frameNumber)
        {
            CurrentFrame = frameNumber;
            //DependencyGraph.UpdateStatus(frameNumber);

            // Switch to this frame - unless we're already there.
            // We could have several records per frame, so don't switch around there.
            if (!ForceFrameUpdate && EventHistory.SelectedCells.Count > 0)
            {
                int index = EventHistory.SelectedCells[0].RowIndex;

                if (index >= 0 && index < eventList.Count)
                {
                    RscEvent rscEvent = eventList[index];

                    if (rscEvent.FrameNumber == frameNumber)
                    {
                        return;
                    }
                }
            }

            ChangingSelection = true;
            if (CurrentStreamable != null)
            {
                SetToStreamable(CurrentStreamable);
            }
            ChangingSelection = false;
        }

        // A new device was registered, or a device has changed.
        public void OnDeviceChanged(int deviceIndex, Device device) { }

        // A streamable has been selected
        public void OnSelectStreamable(Streamable streamable)
        {
            SetToStreamable(streamable);
        }

        // The client information has been updated
        public void ClientInfoUpdated() { }

        private void UpdateDependencyGraph()
        {
            if (CurrentStreamable != null)
            {
                DependencyGraph.SetStreamable(CurrentStreamable, CurrentFrame, Scene, ShowMapDeps);
            }
        }

        // We're in follow mode, and a new frame has been added.
        public void OnNewFrame(int frameNumber)
        {
            if (CurrentStreamable != null)
            {
                CurrentFrame = frameNumber;
                // TODO: We should technically also change "Frame".

                // Don't refresh, it flickers like ass.
                //BeginInvoke((Action)delegate { UpdateDependencyGraph(); });
            }
        }

        // A new entity type was registered
        public void OnNewEntityType(int entityTypeId, EntityType type) { }

        // The connection has been terminated
        public void OnConnectionLost() { }

        // Register this view with the system
        public void Register(MainController controller, Scene scene)
        {
            this.Controller = controller;
            this.Scene = scene;
            DependencyGraph.Controller = controller;
        }

        public void OnFilterChanged()
        {

        }

        public void OnFrameRangeChanged(int firstFrame, int lastFrame)
        {

        }

        private void ParentLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (CurrentParent != null)
            {
                Controller.OnSelectStreamable(CurrentParent, true);                
            }
        }

        private void DependencyGraph_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.XButton1)
            {
                Controller.Undo();
            }

            if (e.Button == MouseButtons.XButton2)
            {
                Controller.Redo();
            }
        }

        private void ShowMapDependencies_CheckedChanged(object sender, EventArgs e)
        {
            ShowMapDeps = ShowMapDependencies.Checked;
            UpdateDependencyGraph();
            Refresh();
        }

		private void DumpEntityAnalysis()
		{
			//1. Get first request frame from where the current selection is
			//2. Get loaded frame immediately after where the current selection is
			//3. Get Low Reads in flight at first request frame, link the up to their original score...
			//4. Figure out a way of linking up requests with their context 
			//		and dependencies with their contexts

			int LoadedFrame = -1;
			int LoadRequestedFrame = -1;
			int FirstRequestedFrame = -1;

			float StartTime = 0.0f;
			float LoadedTime = 0.0f;

			//1.
			//search forward for the next loaded
			if (EventHistory.SelectedCells.Count <= 0)
			{
				return;
			}
			int row = EventHistory.SelectedCells[0].RowIndex;
			while (row < eventList.Count)
			{
				if (eventList[row].Action == RscEvent.ActionType.Loaded)
				{
					LoadedFrame = eventList[row].FrameNumber;
					break;
				}
				row++;
			}

			//2.
			bool foundRequest = false;
			while (row >= 0)	//row will be ahead of the first request by this point
			{					//go back until we get to the first request for this moment

				//keep track of when we were accepted for the header information
				if (eventList[row].Action == RscEvent.ActionType.LoadRequested && LoadRequestedFrame == -1)
				{
					LoadRequestedFrame = eventList[row].FrameNumber;
				}

				if (!foundRequest)
				{
					if (eventList[row].Action == RscEvent.ActionType.REQUEST)
					{
						foundRequest = true;
						FirstRequestedFrame = eventList[row].FrameNumber;
						LoadedTime = eventList[row].GetStartTime();
						StartTime = eventList[row].GetStartTime();
					}
				}
				else 
				{	//not keep going back until we get a break in frames or a non request
					if (eventList[row].Action == RscEvent.ActionType.Loaded || FirstRequestedFrame - eventList[row].FrameNumber > 1)
					{						
						break;
					}
					else
					{
						if (eventList[row].Action == RscEvent.ActionType.REQUEST)							
						{
							FirstRequestedFrame = eventList[row].FrameNumber;
							StartTime = eventList[row].GetStartTime();
						}
					}
				}
				row--;
			}
			
			if (LoadedFrame == -1 || FirstRequestedFrame == -1) //didn't find anything, 
			{
				MessageBox.Show("Something went wrong with your request, send this strviz, the asset and frame number to Admin");
				return;
			}

			String strHeader = String.Format("\nWhat blocked : {0} 's REQUEST for {1} s, {2} frames\n\n", ResourceName.Text, LoadedTime - StartTime, LoadRequestedFrame - FirstRequestedFrame);

			//3&4
			DeviceView.DumpReadRequests(FirstRequestedFrame, LoadedFrame, Scene, 1, strHeader);
		}
    }
}
