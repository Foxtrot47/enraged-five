﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StreamingVisualize.Model;
using StreamingVisualize.Controller;

namespace StreamingVisualize.View.Components
{
    public partial class StreamableLabel : Control
    {
        public Streamable Streamable { get; set; }

        public Streamable.Status Status { get; set; }

        public MainController Controller;

        public int FrameNumber { get; set; }


        private Pen BoxPen = new Pen(Color.Black, 1.0f);
        private Brush TextBrush = new SolidBrush(Color.Black);

        private Dictionary<Streamable.Status, Brush> Brushes = new Dictionary<Streamable.Status, Brush>()
        {
            { Streamable.Status.NOTLOADED, new SolidBrush(Color.Orange) },
            { Streamable.Status.LOADREQUESTED, new SolidBrush(Color.Lavender) },
            { Streamable.Status.LOADING, new SolidBrush(Color.Cyan) },
            { Streamable.Status.LOADED, new SolidBrush(Color.LawnGreen) },
            { Streamable.Status.UNREGISTERED, new SolidBrush(Color.Gray) },

        };

        public StreamableLabel()
        {
            InitializeComponent();
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);

            if (Streamable != null)
            {
                pe.Graphics.FillRectangle(Brushes[Status], 1, 1, Size.Width - 2, Size.Height - 2);
                pe.Graphics.DrawRectangle(BoxPen, 0, 0, Size.Width - 1, Size.Height - 1);
                pe.Graphics.DrawString(Streamable.IdentityHistory.GetStateAt(FrameNumber).Name, this.Font, TextBrush, 2.0f, 2.0f);
            }
        }

        private void StreamableLabel_Click(object sender, EventArgs e)
        {
            Controller.OnSelectStreamable(Streamable, true);
        }
    }
}
