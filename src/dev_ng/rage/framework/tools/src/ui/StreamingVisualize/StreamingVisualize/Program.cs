﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Threading;

namespace StreamingVisualize
{
	static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
			FileStream ostrm;
			StreamWriter writer;			
			TextWriter oldOut = Console.Out;
			try
			{
				ostrm = new FileStream("c:/strvizdbg.txt", FileMode.OpenOrCreate, FileAccess.Write);
				writer = new StreamWriter(ostrm);
			}
			catch (Exception e)
			{
				Console.WriteLine("Cannot open Redirect.txt for writing");
				Console.WriteLine(e.Message);
				return;
			}
			writer.AutoFlush = true;
			Console.SetOut(writer);
			Console.SetError(writer);
			Console.WriteLine("Piping op to a text file");			

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

			MainFrame mainFrame = new MainFrame(args);

			// Add the event handler for handling UI thread exceptions to the event.
			Application.ThreadException += new ThreadExceptionEventHandler(mainFrame.ThreadException);

            Application.Run(mainFrame);

			Console.SetOut(oldOut);
			writer.Close();
			ostrm.Close();
			Console.WriteLine("Done");
        }
    }
}
