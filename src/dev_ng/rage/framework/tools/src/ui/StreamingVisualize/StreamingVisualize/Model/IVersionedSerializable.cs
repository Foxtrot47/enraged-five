﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StreamingVisualize.Model
{
    public interface IVersionedSerializable
    {
        void Serialize(VersionedWriter writer);

        void Deserialize(VersionedReader reader, int version);
    }
}
