﻿
using StreamingVisualize.Model;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StreamingVisualize.Controller;
using System.IO;

namespace StreamingVisualize
{
    public partial class ClientWindow : Form, IViewController
    {
        private Scene scene;
        private MainController controller = new MainController();
        private Communication communication;

        public ClientWindow(Scene scene, Communication communication)
        {
            this.scene = scene;
            this.communication = communication;

            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            controller.RegisterView(this);

            FrameView.Scene = scene;
            controller.FrameView = FrameView;
            controller.Scene = scene;
            controller.EntityView = EntityView;
            controller.ClientWindow = this;
            scene.controller = controller;
            FrameView.Controller = controller;
            ResourceView.Controller = controller;
            EntityView.Scene = scene;
            EntityView.Keyframe = scene.CurrentFrame;
            EntityView.Controller = controller;
            FrameOverview.Controller = controller;
            FrameOverview.Scene = scene;

            DeviceView1.DeviceIndex = 0;
            DeviceView2.DeviceIndex = 1;
            DeviceView1.Controller = controller;
            DeviceView2.Controller = controller;

            controller.RegisterView(EntityView);
            controller.RegisterView(FrameOverview);
            controller.RegisterView(FrameView);
            controller.RegisterView(ResourceView);
            controller.RegisterView(DeviceView1);
            controller.RegisterView(DeviceView2);

            controller.RegisterDevice(0, scene.GetDevice(0));
            controller.RegisterDevice(1, scene.GetDevice(1));

            ClientInfoUpdated();
        }

        private void FrameView_Load(object sender, EventArgs e)
        {

        }

        public void SetToFrame(int frameNumber)
        {
        }

        public void GoToLastFrame()
        {
            if (scene != null && scene.FrameCount > 0)
            {
                controller.SetToFrame(scene.FrameCount - 1);
                controller.OnNewFrame(scene.FrameCount - 1);
            }
        }

        public void ClientInfoUpdated()
        {
            BeginInvoke((Action)delegate { ClientInfoUpdatedCore(); });
        }

        public void ClientInfoUpdatedCore()
        {
            if (scene.Platform != null && scene.Platform.Length > 0)
            {
                Text = scene.Platform + " @ " + scene.RemoteAddr;
            }
        }

        // If a new frame was added
        public void OnFrameChanged() { }

        // A new device was registered, or a device has changed.
        public void OnDeviceChanged(int deviceIndex, Device device) { }

        // A streamable has been selected
        public void OnSelectStreamable(Streamable streamable) { }

        // We're in follow mode, and a new frame has been added.
        public void OnNewFrame(int frameNumber) { }

        // A new entity type was registered
        public void OnNewEntityType(int entityTypeId, EntityType type) { }

        // The connection has been terminated
        public void OnConnectionLost() { }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();

            //sfd.InitialDirectory = "c:\\";
            sfd.Filter = "StreamingVisualize (*.svz)|*.svz|All files (*.*)|*.*";
            sfd.FilterIndex = 0;
            sfd.RestoreDirectory = true;


            if (sfd.ShowDialog() == DialogResult.OK)
            {
                //try
                {
                    Stream stream = sfd.OpenFile();
                    VersionedWriter writer = new VersionedWriter(stream);
                    writer.WriteHeader();
                    writer.Write(scene);
                    writer.Close();
                    stream.Close();

                    MessageBox.Show("File saved successfully.");
                }
/*                catch (Exception ex)
                {
                    MessageBox.Show("Error saving file: " + ex.Message);
                }*/
            }
        }

        private void quitToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void ClientWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            controller.UnregisterView(EntityView);
            controller.UnregisterView(FrameOverview);
            controller.UnregisterView(FrameView);
            controller.UnregisterView(ResourceView);
            controller.UnregisterView(DeviceView1);
            controller.UnregisterView(DeviceView2);

            if (communication != null)
            {
                communication = null;
            }
        }
    }
}
