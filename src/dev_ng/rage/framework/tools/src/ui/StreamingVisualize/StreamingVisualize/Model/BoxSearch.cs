﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StreamingVisualize.Model
{
    public class BoxSearch : IVersionedSerializable
    {
        public int SearchType { get; set; }
        public Box16 Aabb { get; set; }

        public float BoxX1
        {
            get { return Aabb.BoxMin.X; }
        }

        public float BoxY1
        {
            get { return Aabb.BoxMin.Y; }
        }

        public float BoxZ1
        {
            get { return Aabb.BoxMin.Z; }
        }

        public float BoxX2
        {
            get { return Aabb.BoxMax.X; }
        }

        public float BoxY2
        {
            get { return Aabb.BoxMax.Y; }
        }

        public float BoxZ2
        {
            get { return Aabb.BoxMax.Z; }
        }

        public void Serialize(VersionedWriter writer)
        {
            writer.Write((byte)SearchType);
            writer.Write(Aabb);
        }

        public void Deserialize(VersionedReader reader, int version)
        {
            SearchType = reader.ReadByte();
            Aabb = reader.ReadBox16();
        }
    }
}
