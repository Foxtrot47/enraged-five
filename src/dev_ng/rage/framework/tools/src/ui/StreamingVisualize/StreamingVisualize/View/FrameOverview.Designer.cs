﻿namespace StreamingVisualize.View
{
    partial class FrameOverview
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Scroller = new System.Windows.Forms.HScrollBar();
            this.SuspendLayout();
            // 
            // Scroller
            // 
            this.Scroller.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Scroller.Location = new System.Drawing.Point(0, 119);
            this.Scroller.Name = "Scroller";
            this.Scroller.Size = new System.Drawing.Size(661, 17);
            this.Scroller.TabIndex = 0;
            this.Scroller.Scroll += new System.Windows.Forms.ScrollEventHandler(this.Scroller_Scroll);
            // 
            // FrameOverview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Scroller);
            this.DoubleBuffered = true;
            this.Name = "FrameOverview";
            this.Size = new System.Drawing.Size(661, 136);
            this.Load += new System.EventHandler(this.FrameOverview_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.FrameOverview_Paint);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrameOverview_KeyDown);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FrameOverview_MouseDown);
            this.MouseEnter += new System.EventHandler(this.FrameOverview_MouseEnter);
            this.MouseLeave += new System.EventHandler(this.FrameOverview_MouseLeave);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FrameOverview_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FrameOverview_MouseUp);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.HScrollBar Scroller;
    }
}
