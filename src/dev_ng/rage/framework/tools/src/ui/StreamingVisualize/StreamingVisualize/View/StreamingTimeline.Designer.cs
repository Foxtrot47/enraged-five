﻿namespace StreamingVisualize.View
{
    partial class StreamingTimeline
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Scroller = new System.Windows.Forms.HScrollBar();
            this.SuspendLayout();
            // 
            // Scroller
            // 
            this.Scroller.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Scroller.Location = new System.Drawing.Point(0, 283);
            this.Scroller.Name = "Scroller";
            this.Scroller.Size = new System.Drawing.Size(383, 17);
            this.Scroller.TabIndex = 0;
            this.Scroller.Scroll += new System.Windows.Forms.ScrollEventHandler(this.Scroller_Scroll);
            // 
            // StreamingTimeline
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.Scroller);
            this.DoubleBuffered = true;
            this.Name = "StreamingTimeline";
            this.Size = new System.Drawing.Size(383, 300);
            this.Load += new System.EventHandler(this.StreamingTimeline_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.StreamingTimeline_Paint);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.StreamingTimeline_KeyDown);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.StreamingTimeline_MouseDown);
            this.MouseHover += new System.EventHandler(this.StreamingTimeline_MouseHover);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.StreamingTimeline_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.StreamingTimeline_MouseUp);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.HScrollBar Scroller;
    }
}
