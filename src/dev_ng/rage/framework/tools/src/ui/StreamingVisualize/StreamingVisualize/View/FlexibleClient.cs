﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StreamingVisualize.Model;
using StreamingVisualize.Controller;
using StreamingVisualize.View.Components;
using System.IO;
using System.IO.Compression;
using System.Threading;

namespace StreamingVisualize.View
{
    public partial class FlexibleClient : Form, IViewController
    {
        private Scene scene;
        private MainController controller = new MainController();
        private Communication communication;
        private FilterDialog filterDialog;

        public ViewLayoutManager ViewLayoutManager = new ViewLayoutManager();


        public class SaveInfo
        {
            public Stream stream;
            public VersionedWriter writer;
            public String fileName;
            public ProgressView progressView;
        }


        public FlexibleClient(Scene scene, Communication communication)
        {
            this.scene = scene;
            this.communication = communication;
            controller.Scene = scene;
            controller.ClientWindow = this;
            scene.controller = controller;

            controller.RegisterView(this);

            ViewLayoutManager.CreateDefaultLayout();
            
            InitializeComponent();

            ViewLayoutManager.CreateHierarchy(this, scene, controller);
            PerformLayout();

            filterDialog = new FilterDialog();
            filterDialog.Controller = controller;
            filterDialog.Filter = scene.Filter;
            filterDialog.Scene = scene;
        }

        private void FlexibleClient_Load(object sender, EventArgs e)
        {
        }

        public void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();

            //sfd.InitialDirectory = "c:\\";
            sfd.Filter = "StreamingVisualize (*.svz)|*.svz|All files (*.*)|*.*";
            sfd.FilterIndex = 0;
            sfd.RestoreDirectory = true;


            if (sfd.ShowDialog() == DialogResult.OK)
            {
//                try
                {
                    Stream stream = sfd.OpenFile();
                    VersionedWriter writer = new VersionedWriter(stream);
                    writer.WriteHeader();

                    ProgressView progressView = new ProgressView();
                    progressView.StartUp("Saving " + sfd.FileName, 10, null);
                    progressView.Show();

                    SaveInfo info = new SaveInfo();
                    info.stream = stream;
                    info.writer = writer;
                    info.fileName = sfd.FileName;
                    info.progressView = progressView;

                    Thread thread = new Thread(FileSaver);
                    thread.Name = "File Saver";
                    thread.Start(info);
                }
  /*              catch (Exception ex)
                {
                    MessageBox.Show("Error saving file: " + ex.Message);
                }*/
            }
        }

        private void FileSaver(Object info)
        {
            SaveInfo saveInfo = (SaveInfo)info;
            VersionedWriter writer = saveInfo.writer;
            Stream stream = saveInfo.stream;
            bool success = false;

            lock (scene.LockObj)
            {
                // Let's compress the main data.
                Stream compressedStream = new BufferedStream(new DeflateStream(stream, CompressionMode.Compress), 262144);
                writer.SetStream(compressedStream);

                // Would be a lot cleaner to pass the progress view down the Write() function,
                // but I can't be bothered do that much refactoring.
                scene.ProgressView = saveInfo.progressView;
                writer.Write(scene);
                scene.ProgressView = null;
                success = scene.LastSaveSuccess;
            }
            writer.Close();
            stream.Close();

            if (scene.LastSaveSuccess)
            {
                BeginInvoke((Action)delegate { OnSuccessfulSave(saveInfo.progressView, saveInfo.fileName); });
            }
            else
            {
                BeginInvoke((Action)delegate { OnUnsuccessfulSave(saveInfo.progressView); });
            }
        }

        private void OnSuccessfulSave(ProgressView progressView, String filename)
        {
            scene.Filename = filename;
            ClientInfoUpdatedCore();

            progressView.Dispose();

            MessageBox.Show("File saved successfully.");
        }

        private void OnUnsuccessfulSave(ProgressView progressView)
        {
            progressView.Dispose();
            MessageBox.Show("File not saved.");
        }

        public void GoToLastFrame()
        {
            if (scene != null && scene.FrameCount > 0)
            {
                controller.SetToFrame(scene.FrameCount - 1, false);
                controller.OnNewFrame(scene.FrameCount - 1);

                for (int x = 0; x < scene.EntityTypeCount; x++)
                {
                    EntityType type = scene.GetEntityType(x);

                    if (type != null)
                    {
                        controller.OnNewEntityType(x, type);
                    }
                }
            }
        }

        public void ClientInfoUpdated()
        {
            BeginInvoke((Action)delegate { ClientInfoUpdatedCore(); });
        }

        public void ClientInfoUpdatedCore()
        {
            String title = (scene.Filename == null) ? "" : scene.Filename;
            String versionName = (scene.Version == null) ? "" : scene.Version;
            String assetType = (scene.AssetType == null) ? "" : scene.AssetType;

            if (scene.Platform != null && scene.Platform.Length > 0)
            {
                String connectionStatus = (communication == null) ? " DISCONNECTED" : "";
                title += " - " + assetType + " " + scene.Platform + " " + versionName + " @ " + scene.RemoteAddr + connectionStatus;
            }
            else
            {
                if (communication == null)
                {
                    title += " - DISCONNECTED";
                }
            }

            Text = title;
        }

        // Switch the display to focus on a certain frame
        public void SetToFrame(int frameNumber) { }

        // A new device was registered, or a device has changed.
        public void OnDeviceChanged(int deviceIndex, Device device) { }

        // A streamable has been selected
        public void OnSelectStreamable(Streamable streamable) { }

        // We're in follow mode, and a new frame has been added.
        public void OnNewFrame(int frameNumber) { }

        // A new entity type was registered
        public void OnNewEntityType(int entityTypeId, EntityType type) { }

        public void OnSelectEntity(Entity entity) { }

        // The connection has been terminated
        public void OnConnectionLost()
        {
            // Do we need to terminate? ... Let's just do it.
            if (communication != null)
            {
                communication.Terminate();
                communication = null;
            }

            ClientInfoUpdated();
        }

        // Register this view with the system
        public void Register(MainController controller, Scene scene) { }

        private void FlexibleClient_FormClosed(object sender, FormClosedEventArgs e)
        {
            ViewLayoutManager.UnregisterAll(controller);

            if (scene != null)
            {
                scene.Playback.Terminate();
            }

            if (controller != null)
            {
                controller.UnregisterView(this);
            }

            if (communication != null)
            {
                communication.Terminate();
                communication = null;
            }
        }

        private void ShowFilterDialog()
        {
            filterDialog.Populate();
            filterDialog.Show();
            filterDialog.BringToFront();
        }

        private void filterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowFilterDialog();
        }

        public void OnFilterChanged()
        {

        }

        public void OnFrameRangeChanged(int firstFrame, int lastFrame)
        {

        }

        private void AnalyzeWorker(Object data)
        {
            ProgressView progress = (ProgressView)data;
            String analysis = scene.AnalyzeAllTests(progress);

            BeginInvoke((Action)delegate { OnAnalysisDone(progress, analysis); });
        }

        private void analyzeAllTestsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProgressView progress = new ProgressView();
            progress.StartUp("Analyzing All Tests", scene.FrameCount, null);
            progress.Show();

            Thread thread = new Thread(AnalyzeWorker);
            thread.Name = "Analyzer";
            thread.Start(progress);
        }

        private void OnAnalysisDone(ProgressView progressView, String analysis)
        {
            progressView.Hide();
            Clipboard.SetText(analysis);
            MessageBox.Show("Analysis copied to clipboard - paste it into Excel.");
        }

        private void startPlaybackToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PlaybackView.ShowView(controller, scene);
        }
    }
}
