﻿namespace StreamingVisualize.View
{
    partial class FilterDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.Module = new System.Windows.Forms.ComboBox();
            this.moduleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.NameFilter = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.NameExclusion = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.moduleBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Module";
            // 
            // Module
            // 
            this.Module.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Module.DataSource = this.moduleBindingSource;
            this.Module.DisplayMember = "Name";
            this.Module.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Module.FormattingEnabled = true;
            this.Module.Location = new System.Drawing.Point(90, 13);
            this.Module.Name = "Module";
            this.Module.Size = new System.Drawing.Size(182, 21);
            this.Module.TabIndex = 1;
            this.Module.ValueMember = "Index";
            this.Module.SelectionChangeCommitted += new System.EventHandler(this.Module_SelectionChangeCommitted);
            // 
            // moduleBindingSource
            // 
            this.moduleBindingSource.DataSource = typeof(StreamingVisualize.Model.Module);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Name Filter";
            // 
            // NameFilter
            // 
            this.NameFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.NameFilter.Location = new System.Drawing.Point(94, 47);
            this.NameFilter.Name = "NameFilter";
            this.NameFilter.Size = new System.Drawing.Size(177, 20);
            this.NameFilter.TabIndex = 3;
            this.NameFilter.TextChanged += new System.EventHandler(this.NameFilter_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Name Exclusion";
            // 
            // NameExclusion
            // 
            this.NameExclusion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.NameExclusion.Location = new System.Drawing.Point(94, 80);
            this.NameExclusion.Name = "NameExclusion";
            this.NameExclusion.Size = new System.Drawing.Size(177, 20);
            this.NameExclusion.TabIndex = 5;
            this.NameExclusion.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // FilterDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 109);
            this.Controls.Add(this.NameExclusion);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.NameFilter);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Module);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "FilterDialog";
            this.Text = "Filter";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FilterDialog_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.moduleBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox Module;
        private System.Windows.Forms.BindingSource moduleBindingSource;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox NameFilter;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox NameExclusion;
    }
}