﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StreamingVisualize.View;
using StreamingVisualize.Model;
using System.Windows.Forms;
using StreamingVisualize.View.Components;

namespace StreamingVisualize.Controller
{
    public class MainController
    {
        public List<IViewController> Views = new List<IViewController>();

        public FlexibleClient ClientWindow;

        public Scene Scene { get; set; }

        private ViewHistory ViewHistory = new ViewHistory();

        // If true, we're not going to honor frame/selection switches - this is usually because we know that we're changing something ourselves,
        // like changing a view.
        private bool DisableSwitching = false;


        // If true, this controller is currently in the process of doing things that
        // change selections. Views should not perform actions as a response to a selection
        // change while this is in progress.
        public bool ControllerSelecting { get; set; }


        public void OnSelectStreamable(Streamable streamable, bool recordUndo)
        {
            if (!DisableSwitching)
            {
                foreach (IViewController controller in Views)
                {
                    controller.OnSelectStreamable(streamable);
                }

                if (recordUndo)
                {
                    ViewHistory.RecordSetStreamable(streamable);
                }
            }
        }

        public void OnSelectEntity(Entity entity, bool recordUndo)
        {
            if (!DisableSwitching)
            {
                foreach (IViewController controller in Views)
                {
                    controller.OnSelectEntity(entity);
                }

                if (recordUndo)
                {
                    //                ViewHistory.RecordSetStreamable(streamable);
                }

                // Try to get the necessary information to send to the playback device.
                Entity.NewState state = entity.NewHistory.CurrentState;
                if (state.GUID != 0)
                {
                    if (state.MapDataStreamable != null)
                    {
                        if (state.Streamable != null)
                        {
                            String mapData = state.MapDataStreamable.CurrentName;
                            String modelName = state.Streamable.CurrentName;

                            Scene.Playback.SelectEntity(state.GUID, mapData, modelName);
                        }
                    }
                }
            }
        }

        public void SetPlaybackIP(String ipAddress)
        {
            Scene.Playback.SetIPAddress(ipAddress);

        }

        public void SetToFrame(int frameNumber, bool recordUndo)
        {
            if (!DisableSwitching)
            {
                if (Scene.FrameCount > frameNumber)
                {
                    Scene.CurrentFrameNumber = frameNumber;
                    Scene.SetEntityState(frameNumber);
                    Scene.SetStreamableState(frameNumber);

                    Frame frame = Scene.GetFrame(frameNumber);

                    Scene.Playback.SetCamera(frame.CamPos, frame.CamDir, frame.CamUp, frame.FovV);

                    foreach (IViewController controller in Views)
                    {
                        controller.SetToFrame(frameNumber);
                    }

                    // Turn follow mode off if we're switching to a specific frame
                    if (frameNumber < Scene.FrameCount - 1)
                    {
                        Scene.FollowMode = false;
                    }

                    if (recordUndo)
                    {
                        ViewHistory.RecordSetFrame(frameNumber);
                    }
                }
            }
        }

        public void RegisterDevice(int deviceIndex, Device device)
        {
            foreach (IViewController controller in Views)
            {
                controller.OnDeviceChanged(deviceIndex, device);
            }
        }

        public void ClientInfoUpdated()
        {
            foreach (IViewController controller in Views)
            {
                controller.ClientInfoUpdated();
            }
        }

        public void RegisterView(IViewController controller)
        {
            Views.Add(controller);
        }

        public void UnregisterView(IViewController controller)
        {
            Views.Remove(controller);
        }

        public void OnNewEntityType(int entityType, EntityType type)
        {
            foreach (IViewController controller in Views)
            {
                controller.OnNewEntityType(entityType, type);
            }
        }

        public void OnNewFrame(int frameNumber)
        {
            foreach (IViewController controller in Views)
            {
                controller.OnNewFrame(frameNumber);
            }
        }

        public void MarkConnectionLost()
        {
            foreach (IViewController controller in Views)
            {
                controller.OnConnectionLost();
            }
        }

        public void ChangeViewItem(IViewController controller, int viewType)
        {
            DisableSwitching = true;
            ClientWindow.ViewLayoutManager.ChangeView(this, controller, viewType);
            DisableSwitching = false;
        }

        public void ShowError(String errorMessage)
        {
            const String caption = "StreamingVisualize Error";

            Console.WriteLine(errorMessage);

            if (ClientWindow != null)
            {
                ClientWindow.BeginInvoke((Action)delegate { MessageBox.Show(ClientWindow, errorMessage, caption); });
            }
            else
            {
                MessageBox.Show(errorMessage, caption);
            }
        }

        public void OnFilterChanged()
        {
            Scene.ApplyFilter(Scene.Filter);

            foreach (IViewController controller in Views)
            {
                controller.OnFilterChanged();
            }
        }

        public void OnFrameRangeChanged(int firstFrame, int lastFrame)
        {
            Scene.SelectedFirstFrame = firstFrame;
            Scene.SelectedLastFrame = lastFrame;

            foreach (IViewController controller in Views)
            {
                controller.OnFrameRangeChanged(firstFrame, lastFrame);
            }
        }

        public void Undo()
        {
            ViewHistory.Undo(this);
        }

        public void Redo()
        {
            ViewHistory.Redo(this);
        }
    }
}
