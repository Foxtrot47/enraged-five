﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StreamingVisualize.Controller;
using StreamingVisualize.Model;
using StreamingVisualize.View.Components;

namespace StreamingVisualize.View
{
    public partial class DependentsView : UserControl, IViewController
    {
        private Streamable CurrentStreamable;

        private MainController Controller;
        private Scene Scene;
        private ViewContextMenu viewContextMenu = new ViewContextMenu();

        private List<Streamable> CurrentList;
        private bool ChangingSelection = false;



        public DependentsView()
        {
            InitializeComponent();
        }

        public void SetToFrame(int frameNumber)
        {
        }

        public void OnDeviceChanged(int deviceIndex, Device device)
        {
        }

        public void OnSelectStreamable(Streamable streamable)
        {
            CurrentStreamable = streamable;

            RefreshData();
        }

        public void RefreshData()
        {
            ChangingSelection = true;

            if (Scene.Filter.IsActive())
            {
                // We need to create our own list.
                CurrentList = new List<Streamable>();

                foreach (Streamable streamable in CurrentStreamable.IdentityHistory.CurrentState.Dependents)
                {
                    if (!streamable.Filtered)
                    {
                        CurrentList.Add(streamable);
                    }
                }
            }
            else
            {
                // Just copy it.
                CurrentList = CurrentStreamable.IdentityHistory.CurrentState.Dependents;
            }

            DependentList.DataSource = CurrentList;
            ChangingSelection = false;
        }

        public void ClientInfoUpdated()
        {
        }

        public void OnNewFrame(int frameNumber)
        {
        }

        public void OnNewEntityType(int entityTypeId, EntityType type)
        {
        }

        public void OnConnectionLost()
        {
        }

        public void Register(MainController controller, Scene scene)
        {
            this.Controller = controller;
            this.Scene = scene;
        }

        public void OnFilterChanged()
        {
            RefreshData();
        }

        public void OnFrameRangeChanged(int firstFrame, int lastFrame)
        {
        }

        public void OnSelectEntity(Entity entity) { }

        private void RecreateContextMenu()
        {
            ContextMenu menu = viewContextMenu.CreateContextMenu(Controller, this);
            this.ContextMenu = menu;
        }

        private void DependentsView_Load(object sender, EventArgs e)
        {
            RecreateContextMenu();
        }

        private void DependentList_SelectionChanged(object sender, EventArgs e)
        {
            if (ChangingSelection)
            {
                return;
            }

            if (DependentList.SelectedCells.Count > 0 && DependentList.DataSource != null)
            {
                int index = DependentList.SelectedCells[0].RowIndex;

                if (index >= 0 && index < CurrentList.Count)
                {
                    Controller.OnSelectStreamable(CurrentList[index], true);
                }
            }

        }

        private void DependentList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void DependentList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DependentList_SelectionChanged(sender, null);
        }
    }
}
