﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace StreamingVisualize.Model
{
    public class Request : IVersionedSerializable
    {
        public const int DONTDELETE = (1 << 0);
        public const int FORCE_LOAD = (1 << 1);
        public const int PRIORITY_LOAD = (1 << 2);
        public const int LOADSCENE = (1 << 3);
        public const int MISSION_REQUIRED = (1 << 4);
	    public const int CUTSCENE_REQUIRED = (1<<5);
	    public const int INTERIOR_REQUIRED = (1<<6);
	    public const int ZONEDASSET_REQUIRED = (1<<7);
           
        // SV-Internal flags:
        public const int INTERIOR = (1 << 30);
        public const int EXTERIOR = (1 << 29);

        public const int SV_FLAG_MASK = 0x7fff0000;

        public enum ShortReqFlags
        {
            DONT = DONTDELETE,
            FORCE = FORCE_LOAD,
            PRIO = PRIORITY_LOAD,
            LOAD = LOADSCENE,
            MISS = MISSION_REQUIRED,
            CUT = CUTSCENE_REQUIRED,
            INT = INTERIOR_REQUIRED,
            ZONE = ZONEDASSET_REQUIRED,
        };

        static public Dictionary<int, String> FlagTypes = new Dictionary<int, String>()
        {
            { DONTDELETE, "DONT DELETE" },
            { PRIORITY_LOAD, "PRIORITY" },
            { LOADSCENE, "LOADSCENE" },
            { MISSION_REQUIRED, "MISSION REQUIRED" },
            { CUTSCENE_REQUIRED, "CUTSCENE REQUIRED" },
            { INTERIOR_REQUIRED, "INTERIOR REQUIRED" },
            { ZONEDASSET_REQUIRED, "ZONEDASSET REQUIRED" },
        };


        // DOOMED indicates that this request was unrequested in the same frame
        public const int DOOMED = (1 << 30);

        /*
        	STRFLAG_DONTDELETE 				=	(1<<0),	// Never delete the object, until CanDelete is called
	STRFLAG_FORCE_LOAD				=	(1<<1),	// Keep the request alive until the object has been loaded
	STRFLAG_PRIORITY_LOAD			=	(1<<2),	// Put the request on the priority list, which get loaded before any other objects.
	STRFLAG_LOADSCENE				= 	(1<<3),	// Mark the request as part of a load scene. During a LoadScene call, it doesn't exit until all these have been loaded in a blocking fashion.
	STRFLAG_MISSION_REQUIRED		= 	(1<<4), // Mark the request as being required for a mission - special handling involved
	STRFLAG_CUTSCENE_REQUIRED		=	(1<<5), // Mark the request as being required for a cutscene - special handling involved
	STRFLAG_INTERIOR_REQUIRED		=	(1<<6), // Mark the request as being required for an interior - special handling involved 
};

// state flags
enum {
	STRFLAG_INTERNAL_LIVE			=	(1<<8),
	STRFLAG_INTERNAL_DUMMY			=	(1<<9), 
	STRFLAG_INTERNAL_COMPRESSED		=	(1<<10),
	STRFLAG_INTERNAL_RESOURCE		=	(1<<11),
	STRFLAG_INTERNAL_DEFRAGGING		=	(1<<12),
	STRFLAG_INTERNAL_DONT_DEFRAG	=	(1<<13),
	STRFLAG_INTERNAL_UNUSED_1		=	(1<<14),
	STRFLAG_INTERNAL_UNUSED_2		=	(1<<15)
*/

        public int Frame { get; set; }
        public Streamable Streamable { get; set; }
        public int Flags { get; set; }
        public String Context { get; set; }
        public float Score { get; set; }

        public void Serialize(VersionedWriter writer)
        {
            writer.Write(Frame);
            writer.WriteStreamableRef(Streamable);
            writer.Write(Flags);
            writer.WriteStringRef(Context); // String ref since version 26
            writer.Write(Score);
        }

        public void Deserialize(VersionedReader reader, int version)
        {
            Frame = reader.ReadInt();
            Streamable = reader.ReadStreamableRef();
            Flags = reader.ReadInt();

            if (version >= 26)
            {
                Context = reader.ReadStringRef();
            }
            else
            {
                Context = reader.ReadAndRegisterString();
            }
            Score = reader.ReadFloat();
        }

        public static String CreateFlagString(int flags)
        {
            String flagString = "";

            foreach (Request.ShortReqFlags flag in Enum.GetValues(typeof(Request.ShortReqFlags)))
            {
                if ((flags & Convert.ToInt32(flag)) != 0)
                {
                    flagString += flag.ToString() + " ";
                }
            }

            return flagString;
        }
    }
}
