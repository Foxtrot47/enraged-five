﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StreamingVisualize.Model;
using StreamingVisualize.Controller;
using StreamingVisualize.View.Components;

namespace StreamingVisualize.View
{
    public partial class EntityView : UserControl, IViewController
    {
        private const float FOV = 1.0f;
        private const float FarPlane = 500.0f;

        public MainController Controller { get; set; }

        public Scene Scene { get; set; }
        public Keyframe Keyframe { get; set; }
        public Frame Frame { get; set; }

        float CenterX = 0.0f, CenterY = 0.0f;
        float MapScale = 1.0f;
        uint EntityTypesToShow = 0xffffffff;

        private bool mCenterAroundCamera = true;
        private bool CenterAroundCamera
        {
            get { return mCenterAroundCamera; }
            set { mCenterAroundCamera = value; CenterAroundCameraMenu.Checked = mCenterAroundCamera; }
        }

        private int ControlHalfWidth, ControlHalfHeight;
        private int FullWidth, FullHeight;

        private Pen frustumPen = new Pen(Color.Yellow, 2.0f);
        private Pen streamingPen = new Pen(Color.Red);

        private Pen selectedPen = new Pen(Color.Red, 2.0f);
        private Pen selectedStreamablePen = new Pen(Color.OrangeRed, 2.0f);

        private Streamable streamable;
        private Streamable selectedStreamable;      // Officially selected from the controller

        private SimpleTooltip tooltip = new SimpleTooltip();
        private Entity lastHoveredEntity;

        private MenuItem CenterAroundCameraMenu;


        private bool dragging;
        private int lastDragX, lastDragY;

        


        public EntityView()
        {
            InitializeComponent();
        }

        private void ConvertTo2D(Vector pos3d, out int x, out int y)
        {
            ConvertTo2D(pos3d.X, pos3d.Y, out x, out y);
        }

        private void ConvertTo2D(float xPos, float yPos, out int x, out int y)
        {
            xPos -= CenterX;
            yPos -= CenterY;
            xPos *= MapScale;
            yPos *= MapScale;

            x = (int)xPos + ControlHalfWidth;
            y = (int)yPos + ControlHalfHeight;
        }

        private void ConvertTo3D(int x2d, int y2d, out float x3d, out float y3d)
        {
            x2d -= ControlHalfWidth;
            y2d -= ControlHalfHeight;

            float xPos = (float)x2d;
            float yPos = (float)y2d;
            xPos /= MapScale;
            yPos /= MapScale;
            xPos += CenterX;
            yPos += CenterY;

            x3d = xPos;
            y3d = yPos;
        }

        private void DrawEntity(Graphics g, Entity entity)
        {
            if (entity.CurrentState == null)
            {
                return;
            }

            int x1, y1, x2, y2;

            ConvertTo2D(entity.CurrentState.BoxMin, out x1, out y1);
            ConvertTo2D(entity.CurrentState.BoxMax, out x2, out y2);

            // Guarding band check to eliminate everything outside the frustum.
            if (x1 < 0 && x2 < 0)
                return;

            if (y1 < 0 && y2 < 0)
                return;

            if (x1 >= FullWidth && x2 >= FullWidth)
                return;

            if (y1 >= FullWidth && y2 >= FullHeight)
                return;

            // Also eliminate tiny elements - this will help us when we're zoomed out.
            if (x1 == y1 && x2 == y2)
                return;

            EntityType type = Scene.GetEntityType(entity.Type);
            Pen pen = (entity.CurrentState.LoadedState == Streamable.Status.LOADED) ? type.LoadedPen : type.UnloadedPen;

            if (entity.CurrentState.LoadedState == Streamable.Status.LOADING || entity.CurrentState.LoadedState == Streamable.Status.LOADREQUESTED)
            {
                pen = streamingPen;
            }

            if (entity.Streamable == selectedStreamable)
            {
                pen = selectedStreamablePen;
            }

            if (entity == lastHoveredEntity)
            {
                pen = selectedPen;
            }

            g.DrawRectangle(pen, x1, y1, x2 - x1, y2 - y1);
        }

        private void GoToCamera(bool refresh)
        {
            if (Frame != null)
            {
                CenterX = Frame.CamPos.X;
                CenterY = Frame.CamPos.Y;

                if (refresh)
                {
                    Refresh();
                }
            }
        }

        public void OnFrameChanged()
        {
            if (CenterAroundCamera)
            {
                GoToCamera(false);
            }
        }

        private bool IsHit(Entity entity, int x, int y)
        {
            int x1, y1, x2, y2;

            if (entity.CurrentState == null)
            {
                return false;
            }

            ConvertTo2D(entity.CurrentState.BoxMin, out x1, out y1);
            ConvertTo2D(entity.CurrentState.BoxMax, out x2, out y2);

            if (x1 == y1 && x2 == y2)
                return false;

            return (x >= x1 && x <= x2 && y >= y1 && y <= y2);
        }

        private bool IsToBeDrawn(Entity entity)
        {
            return ((EntityTypesToShow & (1 << entity.Type)) != 0);
        }

        private void EntityView_Paint(object sender, PaintEventArgs e)
        {
            if (Keyframe != null)
            {
                // Cache out some values - who knows how fast the property getters of a control are.
                ControlHalfWidth = Width / 2;
                ControlHalfHeight = Height / 2;
                FullWidth = Width;
                FullHeight = Height;

                Graphics g = e.Graphics;

                // Draw all entities.
                int count = Keyframe.Entities.Count;

                for (int x = 0; x < count; x++)
                {
                    Entity entity = Keyframe.Entities[x];

                    if (IsToBeDrawn(entity))
                    {
                        DrawEntity(g, entity);
                    }
                }

                // Draw the camera frustum and streaming volumes.
                if (Frame != null)
                {
                    DrawFrustum(g, Frame);
                    DrawStreamingVolumes(g, Frame);
                }

                tooltip.Draw(g, DefaultFont);
            }
        }

        private void DrawFrustum(Graphics g, Frame frame)
        {
            int eyeX, eyeY;

            ConvertTo2D(frame.CamPos, out eyeX, out eyeY);

            float dx = frame.CamDir.X * FarPlane;
            float dy = frame.CamDir.Y * FarPlane;

            float dx1, dy1, dx2, dy2;

            dx1 = dx * (float)Math.Cos(FOV) - dy * (float)Math.Sin(FOV);
            dy1 = dx * (float)Math.Sin(FOV) + dy * (float)Math.Cos(FOV);

            dx2 = dx * (float) Math.Cos(-FOV) - dy * (float) Math.Sin(-FOV);
            dy2 = dx * (float) Math.Sin(-FOV) + dy * (float) Math.Cos(-FOV);

            int cdx1, cdy1, cdx2, cdy2;

            ConvertTo2D(dx1 + frame.CamPos.X, dy1 + frame.CamPos.Y, out cdx1, out cdy1);
            ConvertTo2D(dx2 + frame.CamPos.X, dy2 + frame.CamPos.Y, out cdx2, out cdy2);

            g.DrawLine(frustumPen, eyeX, eyeY, cdx1, cdy1);
            g.DrawLine(frustumPen, eyeX, eyeY, cdx2, cdy2);
            g.DrawLine(frustumPen, cdx1, cdy1, cdx2, cdy2);
        }

        private void DrawStreamingVolumes(Graphics g, Frame frame)
        {
        }

        protected override bool IsInputKey(Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Up:
                case Keys.Down:
                case Keys.Left:
                case Keys.Right:
                    return true;
            }
            return base.IsInputKey(keyData);
        }

        private void EntityView_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.PageUp:
                case Keys.Add:
                    MapScale *= 1.1f;
                    break;

                case Keys.PageDown:
                case Keys.Subtract:
                    MapScale *= 0.9f;
                    break;

                case Keys.Left:
                    CenterX -= MapScale * 30.0f;
                    break;

                case Keys.Right:
                    CenterX += MapScale * 30.0f;
                    break;

                case Keys.Up:
                    CenterY -= MapScale * 30.0f;
                    break;

                case Keys.Down:
                    CenterY += MapScale * 30.0f;
                    break;

                case Keys.F:
                    if (e.Modifiers == Keys.Control)
                    {
                        FindStreamable fs = new FindStreamable(Scene);
                        fs.OnItemSelectedCb = StreamableSelectedCb;
                        fs.Show();
                    }
                    break;
            }

            Refresh();
        }

        private Entity FindClosestEntity(Streamable streamable)
        {
            if (Keyframe == null)
                return null;

            // Find the entity with the smallest bounding box.
            float bestDistance = 999999.0f;
            Entity bestEntity = null;

            int count = Keyframe.Entities.Count;

            for (int i = 0; i < count; i++)
            {
                Entity entity = Keyframe.Entities[i];

                if (entity.CurrentState != null && entity.Streamable == streamable && IsToBeDrawn(entity))
                {
                    float dist = Math.Abs(entity.CurrentState.GetCenterX() - CenterX) + Math.Abs(entity.CurrentState.GetCenterY());

                    if (dist < bestDistance)
                    {
                        bestEntity = entity;
                        bestDistance = dist;
                    }
                }
            }

            return bestEntity;
        }

        public void StreamableSelectedCb(Streamable streamable)
        {
            Entity entity = FindClosestEntity(streamable);
            lastHoveredEntity = entity;

            if (entity != null && entity.CurrentState != null)
            {
                CenterX = entity.CurrentState.GetCenterX();
                CenterY = entity.CurrentState.GetCenterY();
            }

            Controller.OnSelectStreamable(streamable);

            Refresh();
        }

        private void EntityView_MouseMove(object sender, MouseEventArgs e)
        {
            if (Keyframe == null)
                return;

            String tooltipText = "";
            streamable = null;

            int x = e.X;
            int y = e.Y;

            int count = Keyframe.Entities.Count;

            // Find the entity with the smallest bounding box.
            float smallestBox = 999999.0f;
            Entity selectedEntity = null;

            for (int i = 0; i < count; i++)
            {
                Entity entity = Keyframe.Entities[i];

                if (IsToBeDrawn(entity) && IsHit(entity, x, y))
                {
                    float bbox = entity.GetCurrentBoundingBoxSize();

                    if (bbox > 0.0f && bbox < smallestBox)
                    {
                        smallestBox = bbox;
                        selectedEntity = entity;
                    }
                }
            }

            // Provide information about this particular entity.
            if (selectedEntity != null)
            {
                streamable = selectedEntity.Streamable;

                if (streamable != null)
                {
                    tooltipText += selectedEntity.Streamable.Name;
                    tooltipText += "\n";
                    tooltipText += streamable.CurrentStatus + "\n";
                    tooltipText += Scene.GetEntityType(selectedEntity.Type).Name;
                }
            }

            lastHoveredEntity = selectedEntity;

            tooltip.MouseMove(e);
            tooltip.SetToolTip(tooltipText);

            if (dragging)
            {
                int dx = e.X - lastDragX;
                int dy = e.Y - lastDragY;

                float dragDiffX, dragDiffY;

                int oldX;
                int oldY;

                ConvertTo2D(CenterX, CenterY, out oldX, out oldY);
                oldX -= dx;
                oldY -= dy;
                ConvertTo3D(oldX, oldY, out dragDiffX, out dragDiffY);

                CenterX = dragDiffX;
                CenterY = dragDiffY;

                lastDragX = e.X;
                lastDragY = e.Y;

                CenterAroundCamera = false;
            }

            Refresh();
        }

        public void RefreshDeferred()
        {
            BeginInvoke((Action)delegate { Refresh(); });
        }

        private void EntityView_MouseDown(object sender, MouseEventArgs e)
        {
            if (streamable != null)
            {
                Controller.OnSelectStreamable(streamable);
            }

            dragging = true;
            lastDragX = e.X;
            lastDragY = e.Y;
        }

        protected override void OnMouseWheel(MouseEventArgs e)
        {
            if (e.Delta != 0)
            {
                MapScale *= (e.Delta > 0) ? 1.1f : 0.9f;
                Refresh();
            }

            base.OnMouseWheel(e);
        }

        private void EntityView_MouseHover(object sender, EventArgs e)
        {
            // Focus so we get mouse wheel events.
            Focus();
        }

        private void EntityView_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;

            if (lastHoveredEntity != null)
            {
                Controller.OnSelectStreamable(lastHoveredEntity.Streamable);
            }
        }

        private void ToggleTypeVisibility(int entityType, MenuItem item)
        {
            EntityTypesToShow ^= (uint) (1 << entityType);
            item.Checked = (EntityTypesToShow & (1 << entityType)) != 0;
            Refresh();
        }

        private void ToggleFollowCamera()
        {
            CenterAroundCamera = !CenterAroundCamera;
        }

        private void PopulateContextMenu(ContextMenu menu)
        {
            if (Scene != null)
            {
                MenuItem[] typeItems = new MenuItem[Scene.EntityTypeCount];
                int count = 0;

                foreach (KeyValuePair<int, EntityType> kvp in Scene.EntityTypes)
                {
                    MenuItem item = new MenuItem(kvp.Value.Name);
                    int entityTypeId = kvp.Key;
                    item.Checked = (EntityTypesToShow & (1 << entityTypeId)) != 0;

                    item.Click += (sender, e) => ToggleTypeVisibility(entityTypeId, item);

                    typeItems[count++] = item;
                }

                MenuItem typesMenu = new MenuItem("&Types", typeItems);

                menu.MenuItems.Add(typesMenu);
            }

            CenterAroundCameraMenu = new MenuItem("&Follow Camera");
            CenterAroundCameraMenu.Click += (sender, e) => ToggleFollowCamera();
            CenterAroundCameraMenu.Checked = CenterAroundCamera;
            menu.MenuItems.Add(CenterAroundCameraMenu);

            MenuItem goToCamera = new MenuItem("&Go To Camera");
            goToCamera.Click += (sender, e) => GoToCamera(true);
            menu.MenuItems.Add(goToCamera);
        }

        private void RecreateContextMenu()
        {
            ContextMenu menu = new ContextMenu();
            PopulateContextMenu(menu);
            this.ContextMenu = menu;
        }

        private void EntityView_Load(object sender, EventArgs e)
        {
            RecreateContextMenu();
        }

        // Switch the display to focus on a certain frame
        public void SetToFrame(int frameNumber)
        {
            OnFrameChanged();
            RefreshDeferred();
        }

        // A new device was registered, or a device has changed.
        public void OnDeviceChanged(int deviceIndex, Device device) { }

        // A streamable has been selected
        public void OnSelectStreamable(Streamable streamable)
        {
            selectedStreamable = streamable;
            RefreshDeferred();
        }

        // The client information has been updated
        public void ClientInfoUpdated() { }

        // We're in follow mode, and a new frame has been added.
        public void OnNewFrame(int frameNumber)
        {
            if (Scene != null)
            {
                Frame = Scene.GetFrame(frameNumber);
            }

            OnFrameChanged();
            RefreshDeferred();
        }

        // A new entity type was registered
        public void OnNewEntityType(int entityTypeId, EntityType type)
        {
            // By default, disable what we think should be disabled.
            if (type.Visible == false)
            {
                EntityTypesToShow &= (uint) ~(1 << entityTypeId);
            }
            RecreateContextMenu();
        }

        // The connection has been terminated
        public void OnConnectionLost() { }
    }
}
