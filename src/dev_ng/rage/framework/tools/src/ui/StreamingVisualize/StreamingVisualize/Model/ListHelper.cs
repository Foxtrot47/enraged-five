﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StreamingVisualize.Model
{
    /* NOTE: This class contains functionality that is probably already part of .NET.
     * If my .NET-fu was better, I would know.
     */
    public class ListHelper
    {
        public static bool IsEqualShallow<T>(IList<T> a, IList<T> b) where T : class
        {
            if (a.Count != b.Count)
            {
                return false;
            }

            for (int x = 0; x < a.Count; x++)
            {
                if (a[x] != b[x])
                {
                    return false;
                }
            }

            return true;
        }
    }
}
