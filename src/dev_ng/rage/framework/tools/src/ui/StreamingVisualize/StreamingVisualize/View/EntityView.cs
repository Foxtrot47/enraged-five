﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StreamingVisualize.Model;
using StreamingVisualize.Controller;
using StreamingVisualize.View.Components;
using System.Diagnostics;

namespace StreamingVisualize.View
{
    public partial class EntityView : UserControl, IViewController
    {
        private const int LINE_VOLUME_RADIUS = 5;

        private ViewContextMenu viewContextMenu = new ViewContextMenu();

        public MainController Controller { get; set; }

        public Scene Scene { get; set; }
        public Frame Frame { get; set; }

        private int FrameNumber;
        private bool ChangingSelection;

        private BackgroundMap.MapOption BackgroundBitmap;
        private float BgMapX1 = -3500.0f;
        private float BgMapY1 = -7000.0f;
        private float BgMapX2 = 7300.0f;
        private float BgMapY2 = 7500.0f;

        // Number of pixels moved during the last mouse down
        private int DraggingDist;
        private double MouseDownTime;

        float CenterX = 0.0f, CenterY = 0.0f;
        float MapScale = 1.0f;
        uint EntityTypesToShow = 0xffffffff;

        private bool mCenterAroundCamera = true;
        private bool CenterAroundCamera
        {
            get { return mCenterAroundCamera; }
            set { mCenterAroundCamera = value; CenterAroundCameraMenu.Checked = mCenterAroundCamera; }
        }

        private ContextMenuHelper.ToggleBool ShowSelectedOnly = new ContextMenuHelper.ToggleBool(false);
        private ContextMenuHelper.ToggleBool ShowVisibleOnly = new ContextMenuHelper.ToggleBool(false);
        private ContextMenuHelper.ToggleBool ShowImaps = new ContextMenuHelper.ToggleBool(false);
        private ContextMenuHelper.ToggleBool SolidImapBoxes = new ContextMenuHelper.ToggleBool(false);
        private ContextMenuHelper.ToggleBool ShowImapPhysicalBoundsOnly = new ContextMenuHelper.ToggleBool(false);
        private ContextMenuHelper.ToggleBool ShowCulledImapsOnly = new ContextMenuHelper.ToggleBool(false);
        private ContextMenuHelper.ToggleBool ShowCullboxes = new ContextMenuHelper.ToggleBool(false);
        private ContextMenuHelper.ToggleBool SolidCullboxBoxes = new ContextMenuHelper.ToggleBool(true);
        private ContextMenuHelper.ToggleBool ImapSelectionMode = new ContextMenuHelper.ToggleBool(false);
        private ContextMenuHelper.ToggleBool CullboxSelectionMode = new ContextMenuHelper.ToggleBool(false);
        private ContextMenuHelper.ToggleBool NewColorScheme = new ContextMenuHelper.ToggleBool(true);
        private ContextMenuHelper.ToggleBool PriorityColorScheme = new ContextMenuHelper.ToggleBool(false);
        private ContextMenuHelper.ToggleBool CenterSelected = new ContextMenuHelper.ToggleBool(false);
        private ContextMenuHelper.ToggleBool ShowBoundSphere = new ContextMenuHelper.ToggleBool(false);
        private ContextMenuHelper.ToggleBool ShowRequestScore = new ContextMenuHelper.ToggleBool(false);

        private ContextMenuHelper.ToggleIntBoolDictionary ShowLodTypes = new ContextMenuHelper.ToggleIntBoolDictionary();
        private ContextMenuHelper.ToggleIntBoolDictionary ShowFlagTypes = new ContextMenuHelper.ToggleIntBoolDictionary();

        private IList<Cullbox> activeCullboxes = new List<Cullbox>();

        private int ControlHalfWidth, ControlHalfHeight;
        private int FullWidth, FullHeight;

        private Pen frustumPen = new Pen(Color.Yellow, 2.0f);
        private Pen streamingPen = new Pen(Color.Red);

        private Pen selectedPen = new Pen(Color.Yellow, 2.0f);
        private Pen selectedStreamablePen = new Pen(Color.LightYellow, 3.0f);

        private Pen imapPen = new Pen(Color.Blue, 3.0f);
        private Pen CullboxPen = new Pen(Color.YellowGreen, 4.0f);
        private Pen SelectedCullboxPen = new Pen(Color.White, 4.0f);

        private Pen lowLodPen = new Pen(Color.Red, 2.0f);
        private Pen missingLodPen = new Pen(Color.IndianRed, 4.0f);

        private Streamable streamable;
        private Streamable selectedStreamable;      // Officially selected from the controller
        private Cullbox selectedCullbox;

        private Entity entity;

        private SimpleTooltip tooltip = new SimpleTooltip();
        private Entity lastHoveredEntity;
        private MapData lastHoveredMapData;
        private Cullbox lastHoveredCullbox;

        private MenuItem CenterAroundCameraMenu;

        private UpdateThrottler throttler = new UpdateThrottler(500);

        private Dictionary<Streamable.Status, Boolean> visibleStatus = new Dictionary<Streamable.Status, Boolean>()
        {
            { Streamable.Status.NOTLOADED, true },
            { Streamable.Status.LOADREQUESTED, true },
            { Streamable.Status.LOADING, true },
            { Streamable.Status.LOADED, true },
            { Streamable.Status.UNREGISTERED, false},
        };

        private Dictionary<Entity.NewState.LodStates, Boolean> visibleLodStates = new Dictionary<Entity.NewState.LodStates, Boolean>()
        {
            { Entity.NewState.LodStates.LOD_OK, true },
            { Entity.NewState.LodStates.LOD_LOW, true },
            { Entity.NewState.LodStates.LOD_MISSING, true },
        };

        private Dictionary<Streamable.Status, Brush> ImapBrushes = new Dictionary<Streamable.Status, Brush>()
        {
            { Streamable.Status.NOTLOADED, new SolidBrush(Color.DarkRed) },
            { Streamable.Status.LOADREQUESTED, new SolidBrush(Color.DarkMagenta) },
            { Streamable.Status.LOADING, new SolidBrush(Color.DarkCyan) },
            { Streamable.Status.LOADED, new SolidBrush(Color.DarkGreen) },
            { Streamable.Status.UNREGISTERED, new SolidBrush(Color.DarkGray) },
        };

        private Brush CullboxBrush = new SolidBrush(Color.DarkMagenta);

        private Dictionary<Streamable.Status, Pen> NewPensVisible = new Dictionary<Streamable.Status, Pen>()
        {
            { Streamable.Status.NOTLOADED, new Pen(Color.Red, 2.0f) },
            { Streamable.Status.LOADREQUESTED, new Pen(Color.Cyan, 2.0f) },
            { Streamable.Status.LOADING, new Pen(Color.Turquoise, 2.0f) },
            { Streamable.Status.LOADED, new Pen(Color.Green, 1.0f) },
            { Streamable.Status.UNREGISTERED, new Pen(Color.DarkGray, 1.0f) },
        };

        private Dictionary<Streamable.Status, Pen> NewPensInvisible = new Dictionary<Streamable.Status, Pen>()
        {
            { Streamable.Status.NOTLOADED, new Pen(Color.Black, 0.0f) },            // Will not even render
            { Streamable.Status.LOADREQUESTED, new Pen(Color.Yellow, 2.0f) },
            { Streamable.Status.LOADING, new Pen(Color.LightGoldenrodYellow, 2.0f) },
            { Streamable.Status.LOADED, new Pen(Color.Orange, 1.0f) },
            { Streamable.Status.UNREGISTERED, new Pen(Color.DarkGray, 1.0f) },
        };

        private List<Pen> ScorePens = new List<Pen>()
        {
            new Pen(Color.Blue, 1.0f),      // ACTIVE_OUTSIDE_PVS
            new Pen(Color.Orange, 1.0f),      // INVISIBLE_FAR
            new Pen(Color.Yellow, 1.0f),      // INVISIBLE_EXTERIOR
            new Pen(Color.MediumPurple, 1.0f),      // VEHICLESANDPEDS_CUTSCENE - not used
            new Pen(Color.Purple, 1.0f),      // INVISIBLE_OTHERROOMS
            new Pen(Color.Cyan, 1.0f),      // INVISIBLE
            new Pen(Color.Green, 2.0f),      // VISIBLE_FAR
            new Pen(Color.Blue, 1.0f),      // STREAMBUCKET_VEHICLESANDPEDS - not used
            new Pen(Color.Red, 2.0f),      // VISIBLE
            new Pen(Color.Blue, 1.0f),      // ACTIVE_PED - not used
        };

        private Pen ScoreLoaded = new Pen(Color.DarkOrchid);
        private Pen ScoreUnloaded = new Pen(Color.DarkBlue);

        private Pen SearchPen = new Pen(Color.DarkRed, 5.0f);

        private Brush OverlayBrush = new SolidBrush(Color.White);

        private bool showStateChangesOnly;


        private bool dragging;
        private int lastDragX, lastDragY;

        // Temporary list for rendering
        private List<Entity> TempSelectedEntities = new List<Entity>();


        


        public EntityView()
        {
            InitializeComponent();

            CullboxPen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            SelectedCullboxPen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
        }

        private bool ConvertTo2DAndCull(Vector boxMin, Vector boxMax, out int x1, out int y1, out int x2, out int y2, bool forceSmallObjects)
        {
            ConvertTo2D(boxMin, out x1, out y1);
            ConvertTo2D(boxMax, out x2, out y2);

            return PerformCull(ref x1, ref y1, ref x2, ref y2, forceSmallObjects);
        }

        private bool PerformCull(ref int x1, ref int y1, ref int x2, ref int y2, bool forceSmallObjects)
        {
            // Guarding band check to eliminate everything outside the frustum.
            if (x1 < 0 && x2 < 0)
                return false;

            if (y1 < 0 && y2 < 0)
                return false;

            if (x1 >= FullWidth && x2 >= FullWidth)
                return false;

            if (y1 >= FullHeight && y2 >= FullHeight)
                return false;

            // Also eliminate tiny elements - this will help us when we're zoomed out.
            if (x1 >= x2 - 1 || y2 >= y1 - 1)
            {
                if (forceSmallObjects)
                {
                    // Let's make it visible.
                    if (x1 >= x2 - 1)
                    {
                        x1 -= 1;
                        x2 += 1;
                    }

                    if (y2 >= y1 - 1)
                    {
                        y2 -= 1;
                        y1 += 1;
                    }
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        private bool ConvertTo2DAndCull(Vector16 boxMin, Vector16 boxMax, out int x1, out int y1, out int x2, out int y2, bool forceSmallObjects)
        {
            ConvertTo2D(boxMin, out x1, out y1);
            ConvertTo2D(boxMax, out x2, out y2);

            return PerformCull(ref x1, ref y1, ref x2, ref y2, forceSmallObjects);
        }

        private void ConvertTo2D(Vector pos3d, out int x, out int y)
        {
            ConvertTo2D(pos3d.X, pos3d.Y, out x, out y);
        }

        private void ConvertTo2D(Vector16 pos3d, out int x, out int y)
        {
            ConvertTo2D(pos3d.X, pos3d.Y, out x, out y);
        }

        private void ConvertTo2D(float xPos, float yPos, out int x, out int y)
        {
            xPos -= CenterX;
            yPos -= CenterY;
            xPos *= MapScale;
            yPos *= -MapScale;

            x = (int)xPos + ControlHalfWidth;
            y = (int)yPos + ControlHalfHeight;
        }

        private void ConvertTo3D(int x2d, int y2d, out float x3d, out float y3d)
        {
            x2d -= ControlHalfWidth;
            y2d -= ControlHalfHeight;

            float xPos = (float)x2d;
            float yPos = (float)y2d;
            xPos /= MapScale;
            yPos /= -MapScale;
            xPos += CenterX;
            yPos += CenterY;

            x3d = xPos;
            y3d = yPos;
        }

        private void DrawBoxSearch(Graphics g, BoxSearch search)
        {
            int x1, y1, x2, y2;

            if (!ConvertTo2DAndCull(search.Aabb.BoxMin, search.Aabb.BoxMax, out x1, out y1, out x2, out y2, true))
            {
                return;
            }

            g.DrawRectangle(SearchPen, x1, y2, x2 - x1, y1 - y2);
        }

        private void DrawEntity(Graphics g, Entity entity)
        {
            if (entity.CurrentState == null)
            {
                return;
            }

            int x1, y1, x2, y2;

            if (!ConvertTo2DAndCull(entity.PositionHistory.CurrentState.BoxMin, entity.PositionHistory.CurrentState.BoxMax, out x1, out y1, out x2, out y2, entity.Streamable == selectedStreamable))
            {
                return;
            }

            // Also eliminate tiny elements - this will help us when we're zoomed out.

            EntityType type = Scene.GetEntityType(entity.CurrentState.EntityType);
            Entity.NewState.LodStates lodState = entity.CurrentState.LodState;
            Pen pen;

            if (NewColorScheme.Value)
            {
                Streamable.Status status = entity.CurrentState.LoadedState;
                bool visible = entity.NewHistory.CurrentState.IsVisible(Scene);

                if (visible)
                {
                    pen = NewPensVisible[status];
                }
                else
                {
                    pen = NewPensInvisible[status];
                }

                if (lodState == Entity.NewState.LodStates.LOD_LOW)
                {
                    pen = lowLodPen;
                }
                else if (lodState == Entity.NewState.LodStates.LOD_MISSING)
                {
                    pen = missingLodPen;
                }

                if (entity.Streamable == selectedStreamable)
                {
                    pen = selectedStreamablePen;
                    TempSelectedEntities.Add(entity);
                }

                if (entity == lastHoveredEntity)
                {
                    pen = selectedPen;
                }
            }
            else
            {
                pen = (entity.CurrentState.LoadedState == Streamable.Status.LOADED) ? type.LoadedPen : type.UnloadedPen;

                if (entity.CurrentState.LoadedState == Streamable.Status.LOADING || entity.CurrentState.LoadedState == Streamable.Status.LOADREQUESTED)
                {
                    pen = streamingPen;
                }

                // LOD state issues?
                if (lodState == Entity.NewState.LodStates.LOD_LOW)
                {
                    pen = lowLodPen;
                }
                else if (lodState == Entity.NewState.LodStates.LOD_MISSING)
                {
                    pen = missingLodPen;
                }

                if (entity.Streamable == selectedStreamable)
                {
                    pen = selectedStreamablePen;
                    TempSelectedEntities.Add(entity);
                }

                if (entity == lastHoveredEntity)
                {
                    pen = selectedPen;
                }
            }

            if (PriorityColorScheme.Value)
            {
                // Is this entity being requested?
                if (entity.CurrentState.LoadedState == Streamable.Status.LOADED)
                {
                    pen = ScoreLoaded;
                }
                else if (entity.CurrentState.LoadedState == Streamable.Status.NOTLOADED)
                {
                    pen = ScoreUnloaded;
                }
                else
                {
                    // Get the current score.
                    NeededList NeededList = Scene.GetFrame(FrameNumber).NeededList;

                    foreach (NeededList.Entry listEntry in NeededList.List)
                    {
                        if (listEntry.entity == entity)
                        {
                            int score = (int)listEntry.score;
                            pen = ScorePens[score];
                            break;
                        }
                    }
                }
            }

            g.DrawRectangle(pen, x1, y2, x2 - x1, y1 - y2);

            if (ShowBoundSphere.Value)
            {
                Vector16 sphereCenter = entity.PositionHistory.CurrentState.SphereCenter;
                ConvertTo2D(sphereCenter, out x1, out y1);

                ConvertTo2D(sphereCenter.X + entity.PositionHistory.CurrentState.SphereRadius, sphereCenter.Y, out x2, out y2);
                float radius = x2 - x1;

                g.DrawEllipse(pen, x1 - radius, y1 - radius, radius + radius, radius + radius);
            }

            if (lodState != Entity.NewState.LodStates.LOD_OK)
            {
                // Make LOD problems obvious.
                g.DrawLine(pen, x1, y1, x2, y2);
                g.DrawLine(pen, x2, y1, x1, y2);
            }
        }

        private void GoToCamera(bool refresh)
        {
            if (Frame != null)
            {
                CenterX = Frame.CamPos.X;
                CenterY = Frame.CamPos.Y;

                if (refresh)
                {
                    Refresh();
                }
            }
        }

        public void OnFrameChanged()
        {
            if (Frame != null)
            {
                activeCullboxes = Scene.GetCullboxesAt(Frame.StreamCamPos);
            }

            if (CenterAroundCamera)
            {
                GoToCamera(false);
            }
        }

        private bool IsHit(Entity entity, int x, int y)
        {
            int x1, y1, x2, y2;

            if (entity.CurrentState == null)
            {
                return false;
            }

            ConvertTo2D(entity.PositionHistory.CurrentState.BoxMin, out x1, out y1);
            ConvertTo2D(entity.PositionHistory.CurrentState.BoxMax, out x2, out y2);

            if (x1 == y1 && x2 == y2)
                return false;

            return (x >= x1 && x <= x2 && y <= y1 && y >= y2);
        }

        private bool IsHit(Cullbox cullbox, int x, int y)
        {
            int x1, y1, x2, y2;

            ConvertTo2D(cullbox.Extents.BoxMin, out x1, out y1);
            ConvertTo2D(cullbox.Extents.BoxMax, out x2, out y2);

            if (x1 == y1 && x2 == y2)
                return false;

            return (x >= x1 && x <= x2 && y <= y1 && y >= y2);
        }

        private bool IsHit(BoxSearch search, int x, int y)
        {
            int x1, y1, x2, y2;

            ConvertTo2D(search.Aabb.BoxMin, out x1, out y1);
            ConvertTo2D(search.Aabb.BoxMax, out x2, out y2);

            if (x1 == y1 && x2 == y2)
                return false;

            return (x >= x1 && x <= x2 && y <= y1 && y >= y2);
        }

        private bool IsHit(MapData mapData, int x, int y)
        {
            int x1, y1, x2, y2;

            ConvertTo2D(mapData.StreamingBox.BoxMin, out x1, out y1);
            ConvertTo2D(mapData.StreamingBox.BoxMax, out x2, out y2);

            if (x1 == y1 && x2 == y2)
                return false;

            return (x >= x1 && x <= x2 && y <= y1 && y >= y2);
        }

        private bool IsToBeDrawn(MapData mapData)
        {
            Streamable streamable = Scene.GetStreamable(mapData.strIndex);

            if (Scene.Filter.IsActive() && streamable.Filtered)
            {
                return false;
            }

            if (showStateChangesOnly)
            {
                Streamable.State currentState = streamable.History.CurrentState;

                // If this is the first frame, there was no change yet.
                if (currentState.Frame == 0)
                {
                    return false;
                }

                // If the current state wasn't create this frame, then it hasn't changed since last frame.
                if (currentState.Frame != FrameNumber)
                {
                    return false;
                }

                Streamable.State prevState = streamable.History.GetStateAt(FrameNumber - 1);

                Debug.Assert(prevState != currentState);    // It must have changed last frame, see check above!

                if (prevState.Status == currentState.Status)
                {
                    // Loaded state hasn't changed.
                    return false;
                }
            }

            if (ShowCulledImapsOnly.Value)
            {
                bool isCulled = false;
                foreach (Cullbox cullbox in activeCullboxes)
                {
                    if (cullbox.Culls(streamable))
                    {
                        isCulled = true;
                        break;
                    }
                }

                if (!isCulled)
                {
                    return false;
                }
            }

            return visibleStatus[streamable.GetCurrentStatus()];
         }

        private bool IsToBeDrawn(Cullbox cullbox)
        {
            if (Scene.Filter.IsActive())
            {
                if (Scene.Filter.IsNameFilteredOut(cullbox.Name))
                {
                    return false;
                }
            }

            return true;
/*            Streamable streamable = Scene.GetStreamable(mapData.strIndex);

            if (Scene.Filter.IsActive() && streamable.Filtered)
            {
                return false;
            }

            if (showStateChangesOnly)
            {
                Streamable.State currentState = streamable.History.CurrentState;

                // If this is the first frame, there was no change yet.
                if (currentState.Frame == 0)
                {
                    return false;
                }

                // If the current state wasn't create this frame, then it hasn't changed since last frame.
                if (currentState.Frame != FrameNumber)
                {
                    return false;
                }

                Streamable.State prevState = streamable.History.GetStateAt(FrameNumber - 1);

                Debug.Assert(prevState != currentState);    // It must have changed last frame, see check above!

                if (prevState.Status == currentState.Status)
                {
                    // Loaded state hasn't changed.
                    return false;
                }
            }

            return visibleStatus[streamable.GetCurrentStatus()];*/
        }

        private bool IsToBeDrawn(Entity entity)
        {
            if ((EntityTypesToShow & (1 << entity.CurrentState.EntityType)) != 0)
            {
                if (ShowSelectedOnly.Value)
                {
                    if (entity.Streamable != selectedStreamable)
                    {
                        return false;
                    }
                }

                if (ShowVisibleOnly.Value)
                {
                    if (!entity.NewHistory.CurrentState.IsVisible(Scene))
                    {
                        return false;
                    }
                }

                if (NewColorScheme.Value && entity.Streamable != null && entity.Streamable.GetCurrentStatus() == Streamable.Status.NOTLOADED && !entity.NewHistory.CurrentState.IsVisible(Scene))
                {
                    return false;
                }
                
                if (entity.Streamable == null)
                {
                    return true;
                }

                /*if (Scene.Filter.IsFilteredOut(entity.Streamable))
                {
                    return false;
                }*/
                if (entity.Streamable.Filtered)
                {
                    return false;
                }

                if (!ShowLodTypes.IsSet(entity.CurrentState.LodType))
                {
                    return false;
                }

                int streamableFlags = entity.Streamable.History.CurrentState.Flags;

                foreach (int flag in ShowFlagTypes.Value.Keys)
                {
                    if (ShowFlagTypes.Value[flag])
                    {
                        if ((streamableFlags & flag) == 0)
                        {
                            return false;
                        }
                    }
                }

                if (showStateChangesOnly)
                {
                    Entity.NewState currentState = entity.NewHistory.CurrentState;

                    // If this is the first frame, there was no change yet.
                    if (currentState.Frame == 0)
                    {
                        return false;
                    }

                    // If the current state wasn't create this frame, then it hasn't changed since last frame.
                    if (currentState.Frame != FrameNumber)
                    {
                        return false;
                    }

                    Entity.NewState prevState = entity.NewHistory.GetStateAt(FrameNumber - 1);

                    Debug.Assert(prevState != currentState);    // It must have changed last frame, see check above!

                    if (prevState.LoadedState == currentState.LoadedState)
                    {
                        // Loaded state hasn't changed.
                        return false;
                    }
                }
                
                return visibleStatus[entity.Streamable.GetCurrentStatus()] && visibleLodStates[entity.CurrentState.LodState];
            }

            return false;
        }

        private void EntityView_Paint(object sender, PaintEventArgs e)
        {
            // The paint function is so complicated that it could contain things called "bugs".
            // As such, we'll provide our own exception handler so we can debug it.
            try
            {
                OnPaint(sender, e);
            }
            catch (Exception ex)
            {
                Debug.Assert(false, ex.Message);
            }
        }

        private void DrawImap(Graphics g, MapData mapData, bool solid)
        {
            int x1, y1, x2, y2;

            if (solid && !SolidImapBoxes.Value)
            {
                return;
            }

            Streamable streamable = Scene.GetStreamable(mapData.strIndex);

            if (ShowImapPhysicalBoundsOnly.Value)
            {
                if (!ConvertTo2DAndCull(mapData.PhysicsBox.BoxMin, mapData.PhysicsBox.BoxMax, out x1, out y1, out x2, out y2, streamable == selectedStreamable))
                {
                    return;
                }
            }
            else
            {
                if (!ConvertTo2DAndCull(mapData.StreamingBox.BoxMin, mapData.StreamingBox.BoxMax, out x1, out y1, out x2, out y2, streamable == selectedStreamable))
                {
                    return;
                }
            }

            if (solid)
            {
                Brush brush = ImapBrushes[streamable.GetCurrentStatus()];

                g.FillRectangle(brush, x1, y2, x2 - x1, y1 - y2);
            }
            else
            {
                Pen borderPen = (streamable == this.streamable) ? selectedPen : imapPen;

                if (streamable == selectedStreamable)
                {
                    borderPen = selectedStreamablePen;
                }

                g.DrawRectangle(borderPen, x1, y2, x2 - x1, y1 - y2);

                if (!ShowImapPhysicalBoundsOnly.Value)
                {
                    // Do the physics bounds as well.
                    ConvertTo2D(mapData.PhysicsBox.BoxMin, out x1, out y1);
                    ConvertTo2D(mapData.PhysicsBox.BoxMax, out x2, out y2);

                    g.DrawRectangle(borderPen, x1, y2, x2 - x1, y1 - y2);
                }
            }
        }

        private void DrawCullbox(Graphics g, Cullbox cullbox, bool solid)
        {
            int x1, y1, x2, y2;

            if (solid && !SolidCullboxBoxes.Value)
            {
                return;
            }

            if (!ConvertTo2DAndCull(cullbox.Extents.BoxMin, cullbox.Extents.BoxMax, out x1, out y1, out x2, out y2, cullbox == selectedCullbox))
            {
                return;
            }

            if (solid)
            {
                g.FillRectangle(CullboxBrush, x1, y2, x2 - x1, y1 - y2);
            }
            else
            {
                Pen borderPen = (cullbox == lastHoveredCullbox) ? SelectedCullboxPen : CullboxPen;

                g.DrawRectangle(borderPen, x1, y2, x2 - x1, y1 - y2);
            }
        }

        private void DrawAllImaps(Graphics g, bool solid)
        {
            // For thread safety, copy the list of imaps out.
            MapData[] mapDataList;

            lock (Scene.MapDatas)
            {
                if (Scene.MapDatas.Count == 0)
                {
                    return;
                }

                mapDataList = new MapData[Scene.MapDatas.Count];
                Scene.MapDatas.Values.CopyTo(mapDataList, 0);
            }

            foreach (MapData mapData in mapDataList)
            {
                if (IsToBeDrawn(mapData))
                {
                    DrawImap(g, mapData, solid);
                }
            }

            // Draw the currently selected one on top of everything.
            if (selectedStreamable != null)
            {
                MapData mapData;
                if (Scene.MapDatas.TryGetValue(selectedStreamable.Index, out mapData))
                {
                    DrawImap(g, mapData, solid);
                }
            }
        }

        private void DrawAllCullboxes(Graphics g, bool solid)
        {
            foreach (Cullbox cullbox in Scene.Cullboxes)
            {
                if (IsToBeDrawn(cullbox))
                {
                    DrawCullbox(g, cullbox, solid);
                }
            }

            // Draw the currently selected one on top of everything.
            if (selectedCullbox != null)
            {
                DrawCullbox(g, selectedCullbox, solid);
            }
        }

        private float ComputeDistToCamera(Vector position)
        {
            Vector camPos = Scene.GetFrame(FrameNumber).CamPos;

            return camPos.Dist(position);
        }

        private void FindCenter(Streamable streamable, out float X, out float Y)
        {
            float minX = float.MaxValue;
            float minY = float.MaxValue;
            float maxX = float.MinValue;
            float maxY = float.MinValue;

            if (Scene.CurrentFrame != null)
            {
                foreach (Entity entity in Scene.CurrentFrame.Entities)
                {
                    if (entity.Streamable == streamable)
                    {
                        Entity.PositionState state = entity.PositionHistory.CurrentState;

                        minX = Math.Min(minX, state.BoxMin.X);
                        minY = Math.Min(minY, state.BoxMin.Y);
                        maxX = Math.Max(maxX, state.BoxMax.X);
                        maxY = Math.Max(maxY, state.BoxMax.Y);
                    }
                }
            }

            if (minX == float.MaxValue)
            {
                X = 0.0f;
                Y = 0.0f;
            }
            else
            {
                X = (minX + maxX) * 0.5f;
                Y = (minY + maxY) * 0.5f;
            }
        }

        private void FindCenter(Entity entity, out float X, out float Y)
        {
            Entity.PositionState state = entity.PositionHistory.CurrentState;

            X = (state.BoxMin.X + state.BoxMax.X) * 0.5f;
            Y = (state.BoxMin.Y + state.BoxMax.Y) * 0.5f;
        }

        private void DrawBackgroundMap(Graphics g, BackgroundMap.MapOption backgroundBitmap)
        {
            if (backgroundBitmap != null && backgroundBitmap.Bitmap != null)
            {
                // Stretch it according to the map dimensions.
                int bgX1, bgX2, bgY1, bgY2;

                ConvertTo2D(BgMapX1, BgMapY1, out bgX1, out bgY1);
                ConvertTo2D(BgMapX2, BgMapY2, out bgX2, out bgY2);

                g.DrawImage(backgroundBitmap.Bitmap, (float)bgX1, (float)bgY2, (float)(bgX2-bgX1), (float)(bgY1-bgY2));
            }
        }

        private void OnPaint(object sender, PaintEventArgs e)
        {
            if (Scene.CurrentFrame != null)
            {
                // Cache out some values - who knows how fast the property getters of a control are.
                ControlHalfWidth = Width / 2;
                ControlHalfHeight = Height / 2;
                FullWidth = Width;
                FullHeight = Height;

                Graphics g = e.Graphics;

                DrawBackgroundMap(g, BackgroundBitmap);

                // Draw all IMAPs.
                if (ShowImaps.Value)
                {
                    DrawAllImaps(g, true);
                }

                // Draw all cullboxes.
                if (ShowCullboxes.Value)
                {
                    DrawAllCullboxes(g, true);
                }

                // Draw all IMAPs.
                if (ShowImaps.Value)
                {
                    DrawAllImaps(g, false);
                }

                // Draw all cullboxes.
                if (ShowCullboxes.Value)
                {
                    DrawAllCullboxes(g, false);
                }


                // Draw all entities.
                int count = Scene.CurrentFrame.Entities.Count;

                TempSelectedEntities.Clear();

                for (int x = 0; x < count; x++)
                {
                    Entity entity = Scene.CurrentFrame.Entities[x];

                    if (IsToBeDrawn(entity))
                    {
                        DrawEntity(g, entity);
                    }
                }

                if (ShowImaps.Value && lastHoveredMapData != null)
                {
                    DrawImap(g, lastHoveredMapData, false);
                }

                if (ShowCullboxes.Value && lastHoveredCullbox != null)
                {
                    DrawCullbox(g, lastHoveredCullbox, false);
                }

                // Draw selected entities again - we want them on top.
                List<Entity> OnTop = new List<Entity>(TempSelectedEntities);

                foreach (Entity entity in OnTop)
                {
                    DrawEntity(g, entity);
                }

                lock(Scene.LockObj)
                {
                    foreach (BoxSearch search in Scene.GetFrame(Scene.CurrentFrameNumber).BoxSearches)
                    {
                        DrawBoxSearch(g, search);
                    }
                }

                // Finally, the overlay text.
                String overlayText;
                overlayText = String.Format("{0}/{1} {2:F1}, {3:F1}", FrameNumber, Scene.FrameCount, CenterX, CenterY);
                g.DrawString(overlayText, Font, OverlayBrush, 10.0f, 10.0f);

                // Draw the camera frustum and streaming volumes.
                if (Frame != null)
                {
                    DrawFrustum(g, Frame);
                    DrawStreamingVolumes(g, Frame);
                }

                tooltip.Draw(g, DefaultFont, Width, Height);
            }
        }

        private void DrawFrustum(Graphics g, Pen pen, Vector camPos, Vector camDir, Vector streamCamPos, Vector streamCamDir, float fov, float farClip)
        {
            int eyeX, eyeY;
            float cosFov = (float)Math.Cos(fov);
            float sinFov = (float)Math.Sin(fov);
            float cosNegFov = (float)Math.Cos(-fov);
            float sinNegFov = (float)Math.Sin(-fov);

            ConvertTo2D(camPos, out eyeX, out eyeY);

            float dx = camDir.X * farClip;
            float dy = camDir.Y * farClip;

            float dx1, dy1, dx2, dy2;

            dx1 = dx * cosFov - dy * sinFov;
            dy1 = dx * sinFov + dy * cosFov;

            dx2 = dx * cosNegFov - dy * sinNegFov;
            dy2 = dx * sinNegFov + dy * cosNegFov;

            int cdx1, cdy1, cdx2, cdy2;

            ConvertTo2D(dx1 + camPos.X, dy1 + camPos.Y, out cdx1, out cdy1);
            ConvertTo2D(dx2 + camPos.X, dy2 + camPos.Y, out cdx2, out cdy2);

            g.DrawLine(pen, eyeX, eyeY, cdx1, cdy1);
            g.DrawLine(pen, eyeX, eyeY, cdx2, cdy2);
            g.DrawLine(pen, cdx1, cdy1, cdx2, cdy2);

            // Now the stream camera.
            int sx1, sy1, sx2, sy2;

            float targetX = streamCamPos.X + streamCamDir.X * 100.0f;
            float targetY = streamCamPos.Y + streamCamDir.Y * 100.0f;

            ConvertTo2D(streamCamPos, out sx1, out sy1);
            ConvertTo2D(targetX, targetY, out sx2, out sy2);

            g.DrawLine(pen, sx1, sy1, sx2, sy2);
        }

        private void DrawFrustum(Graphics g, Frame frame)
        {
            DrawFrustum(g, frustumPen, frame.CamPos, frame.CamDir, frame.StreamCamPos, frame.StreamCamDir, frame.FovH, frame.FarClip);
        }

        private void DrawStreamingVolume(Graphics g, StreamingVolume volume)
        {
            float posX = volume.StreamVolPos.X;
            float posY = volume.StreamVolPos.Y;

            int x, y;
            ConvertTo2D(posX, posY, out x, out y);

            switch (volume.Type)
            {
                case StreamingVolume.VolumeType.SPHERE:
                    {
                        float edgeX = posX - volume.Radius;
                        float edgeY = posY - volume.Radius;

                        int x1, y1;
                        ConvertTo2D(edgeX, edgeY, out x1, out y1);

                        int width = (x - x1) * 2;
                        int height = (y - y1) * 2;

                        g.DrawEllipse(frustumPen, x1, y1, width, height);
                    }
                    break;

                case StreamingVolume.VolumeType.FRUSTUM:
                    DrawFrustum(g, frustumPen, volume.StreamVolPos, volume.StreamVolDir, volume.StreamVolPos, volume.StreamVolDir, 1.0f, 500.0f); // TODO: Get those from the volume
                    break;

                case StreamingVolume.VolumeType.LINE:
                    {
                        int x2, y2;

                        ConvertTo2D(posX + volume.StreamVolDir.X * 100.0f, posY + volume.StreamVolDir.Y * 100.0f, out x2, out y2);

                        g.DrawLine(frustumPen, x, y, x2, y2);

                        // Add circles around the end points - particularly important if the line is going straight down.
                        g.DrawEllipse(frustumPen, x - LINE_VOLUME_RADIUS, y - LINE_VOLUME_RADIUS, LINE_VOLUME_RADIUS * 2, LINE_VOLUME_RADIUS * 2);
                        g.DrawEllipse(frustumPen, x2 - LINE_VOLUME_RADIUS, y2 - LINE_VOLUME_RADIUS, LINE_VOLUME_RADIUS * 2, LINE_VOLUME_RADIUS * 2);
                    }
                    break;
            }
        }

        private void DrawStreamingVolumes(Graphics g, Frame frame)
        {
            if (frame.StreamingVolumes != null)
            {
                foreach (StreamingVolume vol in frame.StreamingVolumes)
                {
                    DrawStreamingVolume(g, vol);
                }
            }
        }

        protected override bool IsInputKey(Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Up:
                case Keys.Down:
                case Keys.Left:
                case Keys.Right:
                    return true;
            }
            return base.IsInputKey(keyData);
        }

        private void EntityView_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.PageUp:
                case Keys.Add:
                    MapScale *= 1.1f;
                    break;

                case Keys.PageDown:
                case Keys.Subtract:
                    MapScale *= 0.9f;
                    break;

                case Keys.Left:
                    CenterX -= MapScale * 30.0f;
                    break;

                case Keys.Right:
                    CenterX += MapScale * 30.0f;
                    break;

                case Keys.Up:
                    CenterY -= MapScale * 30.0f;
                    break;

                case Keys.Down:
                    CenterY += MapScale * 30.0f;
                    break;

                case Keys.F:
                    if (e.Modifiers == Keys.Control)
                    {
                        /*(FindStreamable fs = new FindStreamable(Scene);
                        fs.OnItemSelectedCb = StreamableSelectedCb;
                        fs.Show();*/
                        FindStreamable.ShowDialog(Controller, Scene);
                        FindStreamable.Instance.OnItemSelectedCb = StreamableSelectedCb;
                    }
                    break;
            }

            Refresh();
        }

        private Entity FindClosestEntity(Streamable streamable)
        {
            if (Scene.CurrentFrame == null)
                return null;

            // Find the entity with the smallest bounding box.
            float[] bestDistance = new float[2];
            Entity[] bestEntity = new Entity[2];

            for (int x = 0; x < bestDistance.Length; x++)
            {
                bestDistance[x] = 999999.0f;
            }

            Array.Clear(bestEntity, 0, bestEntity.Length);



            int count = Scene.CurrentFrame.Entities.Count;

            for (int i = 0; i < count; i++)
            {
                Entity entity = Scene.CurrentFrame.Entities[i];

                if (entity.CurrentState != null && entity.Streamable == streamable && IsToBeDrawn(entity))
                {
                    int importance = 1;

                    if (entity.CurrentState.LoadedState != Streamable.Status.LOADED && entity.CurrentState.IsVisible(Scene))
                    {
                        importance = 0;
                    }

                    float dist = Math.Abs(entity.PositionHistory.CurrentState.GetCenterX() - CenterX) + Math.Abs(entity.PositionHistory.CurrentState.GetCenterY());

                    if (dist < bestDistance[importance])
                    {
                        bestEntity[importance] = entity;
                        bestDistance[importance] = dist;
                    }
                }
            }

            for (int x = 0; x < 2; x++)
            {
                if (bestEntity[x] != null)
                {
                    return bestEntity[x];
                }
            }

            return null;
        }

        public void StreamableSelectedCb(Streamable streamable)
        {
            Entity entity = FindClosestEntity(streamable);
            lastHoveredEntity = entity;

            if (entity != null && entity.CurrentState != null)
            {
                CenterX = entity.PositionHistory.CurrentState.GetCenterX();
                CenterY = entity.PositionHistory.CurrentState.GetCenterY();
                Controller.OnSelectEntity(entity, false);
            }

//            Controller.OnSelectStreamable(streamable, true);
            
            Refresh();
        }

        private MapData FindBestImap(int x, int y)
        {
            // Find the imap with the smallest bounding box.
            float smallestBox = 999999.0f;
            MapData selectedMapData = null;
            int selectedMapIndex = 0;

            foreach (int mapIndex in Scene.MapDatas.Keys)
            {
                MapData mapData = Scene.MapDatas[mapIndex];

                if (IsToBeDrawn(mapData) && IsHit(mapData, x, y))
                {
                    float bbox = mapData.GetBoundingBoxSize();

                    if (bbox > 0.0f && bbox < smallestBox)
                    {
                        smallestBox = bbox;
                        selectedMapData = mapData;
                        selectedMapIndex = mapIndex;
                    }
                }
            }

            return selectedMapData;
        }

        private Cullbox FindBestCullbox(int x, int y)
        {
            // Find the imap with the smallest bounding box.
            float smallestBox = 999999.0f;
            Cullbox selectedCullbox = null;

            foreach (Cullbox cullbox in Scene.Cullboxes)
            {
                if (IsToBeDrawn(cullbox) && IsHit(cullbox, x, y))
                {
                    float bbox = cullbox.GetBoundingBoxSize();

                    if (bbox > 0.0f && bbox < smallestBox)
                    {
                        smallestBox = bbox;
                        selectedCullbox = cullbox;
                    }
                }
            }

            return selectedCullbox;
        }

        private BoxSearch FindBestSearch(int x, int y)
        {
            // Find the search with the smallest bounding box.
/*            float smallestBox = 999999.0f;
            MapData selectedMapData = null;
            int selectedMapIndex = 0;*/

            foreach (BoxSearch search in Scene.GetFrame(FrameNumber).BoxSearches)
            {
                if (/*IsToBeDrawn(mapData) &&*/ IsHit(search, x, y))
                {
/*                    float bbox = search.GetBoundingBoxSize();

                    if (bbox > 0.0f && bbox < smallestBox)
                    {
                        smallestBox = bbox;
                        selectedMapData = mapData;
                        selectedMapIndex = mapIndex;
                    }*/

                    return search;
                }
            }

            return null;
        }

        private Entity FindBestEntity(int x, int y)
        {
            int count = Scene.CurrentFrame.Entities.Count;

            // Find the entity with the smallest bounding box.
            float[] smallestBox = new float[2];
            Entity[] bestEntity = new Entity[2];

            for (int i = 0; i < smallestBox.Length; i++)
            {
                smallestBox[i] = 999999.0f;
            }

            Array.Clear(bestEntity, 0, bestEntity.Length);

            for (int i = 0; i < count; i++)
            {
                Entity entity = Scene.CurrentFrame.Entities[i];

                if (IsToBeDrawn(entity) && IsHit(entity, x, y))
                {
                    float bbox = entity.GetCurrentBoundingBoxSize();

                    int importance = 1;

                    if (entity.CurrentState.LodState != Entity.NewState.LodStates.LOD_OK || (entity.CurrentState.LoadedState != Streamable.Status.LOADED && entity.CurrentState.IsVisible(Scene)))
                    {
                        importance = 0;
                    }

                    if (bbox > 0.0f && bbox < smallestBox[importance])
                    {
                        smallestBox[importance] = bbox;
                        bestEntity[importance] = entity;
                    }
                }
            }

            for (int best = 0; best < 2; best++)
            {
                if (bestEntity[best] != null)
                {
                    return bestEntity[best];
                }
            }

            return null;
        }

        private void EntityView_MouseMove(object sender, MouseEventArgs e)
        {
            if (Scene.CurrentFrame == null)
                return;

            String tooltipText = "";
            streamable = null;
            entity = null;

            int x = e.X;
            int y = e.Y;
            Entity selectedEntity = null;

            // Let's do box searches on top of them all.
            List<BoxSearch> boxSearches;

            lock (Scene.LockObj)
            {
                boxSearches = Scene.GetFrame(FrameNumber).BoxSearches;
            }
            
            foreach (BoxSearch search in boxSearches)
            {
                if (IsHit(search, x, y))
                {
                    tooltipText += "Box search " + Scene.SearchTypes.GetFlag(search.SearchType) + "\n\n";
                }
            }

            // Let's try entities first, unless entities are disabled.
            if (!ImapSelectionMode.Value && !CullboxSelectionMode.Value)
            {
                selectedEntity = FindBestEntity(x, y);

                // Provide information about this particular entity.
                if (selectedEntity != null)
                {
                    streamable = selectedEntity.Streamable;

                    if (streamable != null)
                    {
                        tooltipText += selectedEntity.Streamable.GetNameAt(FrameNumber);
                        tooltipText += "\n";
                        tooltipText += streamable.GetCurrentStatus() + "\n";
                        tooltipText += Scene.GetEntityType(selectedEntity.CurrentState.EntityType).Name + "\n";
                        tooltipText += Scene.GetLodType(selectedEntity.CurrentState.LodType).Name;

                        Streamable.State streamableState = selectedEntity.Streamable.History.GetStateAt(FrameNumber);

                        String flagString = Request.CreateFlagString(streamableState.Flags);

                        tooltipText += "\nFlags: " + flagString;

                        if ((selectedEntity.CurrentState.EntityFlags & Entity.NewState.FLAG_LOW_PRIORITY_STREAM) != 0)
                        {
                            tooltipText += "\n*LOW PRIORITY STREAM*";
                        }

                        tooltipText += "\nDist To Camera: " + ComputeDistToCamera(selectedEntity.PositionHistory.CurrentState.GetCenter()) + "\n";
                        tooltipText += "\nVIS FLAGS: " + Scene.VisFlagsDef.CreateString(selectedEntity.CurrentState.PhaseFlags, selectedEntity.CurrentState.OptionFlags);

                        tooltipText += "\nLOD Distance: " + selectedEntity.CurrentState.LodDistance;

                        Entity parentEntity;
                        
                        if (Scene.CurrentFrame.EntityMap.TryGetValue(selectedEntity.CurrentState.ParentLodEntityId, out parentEntity))
                        {
                            if (!parentEntity.CurrentState.IsDeleted())
                            {
                                tooltipText += "\nParent LOD: " + parentEntity.CurrentState.Streamable.CurrentName;
                            }
                        }

                        if (selectedEntity.CurrentState.MapDataStreamable != null)
                        {
                            tooltipText += "\nIMAP: " + selectedEntity.CurrentState.MapDataStreamable.CurrentName;
                        }
                    }

                    if (ShowRequestScore.Value)
                    {
                        int MinFrameTest = Math.Max(FrameNumber - 1000, 0);
                        for (int Frm = FrameNumber; Frm > MinFrameTest; Frm--)
                        {
                            Frame FrameInfo = Scene.GetFrame(Frm);
                            foreach (NeededList.Entry entry in FrameInfo.NeededList.List)
                            {
                                if (entry.entity == selectedEntity)
                                {
                                    tooltipText += String.Format("\nScore: {0:F} (Frame {1:F})", entry.score, Frm);
                                    Frm = -1;
                                    break;
                                }
                            }
                        }
                    }

                    entity = selectedEntity;
                }
            }

            lastHoveredEntity = selectedEntity;
            lastHoveredMapData = null;
            lastHoveredCullbox = null;

            if (lastHoveredEntity == null && ShowCullboxes.Value)
            {
                Cullbox cullbox = FindBestCullbox(x, y);
                lastHoveredCullbox = cullbox;

                if (cullbox != null)
                {
                    tooltipText += "Cullbox: " + cullbox.Name + "\n";
                    tooltipText += "Contents: " + cullbox.Contents.Count + " maps" + "\n";
                }
            }

            // Try mapdata if we couldn't get an entity.
            if (lastHoveredEntity == null && ShowImaps.Value && !CullboxSelectionMode.Value)
            {
                MapData selectedMapData = FindBestImap(x, y);

                // Provide information about this particular entity.
                if (selectedMapData != null)
                {
                    streamable = Scene.GetStreamable(selectedMapData.strIndex);

                    if (streamable != null)
                    {
                        tooltipText += streamable.GetNameAt(FrameNumber);
                        tooltipText += "\n";
                        tooltipText += streamable.GetCurrentStatus() + "\n";

                        // Is it culled?
                        IList<Cullbox> culling = Scene.GetCullboxesCullingMap(activeCullboxes, selectedMapData);

                        if (culling.Count > 0)
                        {
                            tooltipText += "Culled by:\n";

                            foreach (Cullbox cullbox in culling)
                            {
                                tooltipText += cullbox.Name + "\n";
                            }
                        }
                    }
                }

                lastHoveredEntity = null;
                lastHoveredMapData = selectedMapData;
            }

            // Add the current position to the tool tip.
            float posX, posY;
            ConvertTo3D(e.X, e.Y, out posX, out posY);
            tooltipText += String.Format("\n{0:F2}, {1:F2}\n", posX, posY);

            tooltip.MouseMove(e);
            tooltip.SetToolTip(tooltipText);

            if (dragging)
            {
                int dx = e.X - lastDragX;
                int dy = e.Y - lastDragY;

                DraggingDist += Math.Abs(dx);
                DraggingDist += Math.Abs(dy);

                float dragDiffX, dragDiffY;

                int oldX;
                int oldY;

                ConvertTo2D(CenterX, CenterY, out oldX, out oldY);
                oldX -= dx;
                oldY -= dy;
                ConvertTo3D(oldX, oldY, out dragDiffX, out dragDiffY);

                CenterX = dragDiffX;
                CenterY = dragDiffY;

                lastDragX = e.X;
                lastDragY = e.Y;

                CenterAroundCamera = false;
            }

            Refresh();
        }
/*
        public void SelectMap()
        {
            if (selectedEntity != null)
            {
                if (selectedEntity.CurrentState.MapDataStreamable != null)
                {
                    selectedStreamable = 
                    tooltipText += "\nIMAP: " + selectedEntity.CurrentState.MapDataStreamable.CurrentName;
                        }
        */
        public void RefreshDeferred()
        {
            BeginInvoke((Action)delegate { Refresh(); });
        }

        private void EntityView_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            lastDragX = e.X;
            lastDragY = e.Y;

            DraggingDist = 0;
            MouseDownTime = DateTime.Now.TimeOfDay.TotalMilliseconds;
        }

        protected override void OnMouseWheel(MouseEventArgs e)
        {
            if (e.Delta != 0)
            {
                MapScale *= (e.Delta > 0) ? 1.1f : 0.9f;
                Refresh();
            }

            base.OnMouseWheel(e);
        }

        private void EntityView_MouseHover(object sender, EventArgs e)
        {
            // Focus so we get mouse wheel events.
            Focus();
        }

        private void EntityView_MouseUp(object sender, MouseEventArgs e)
        {
            if (streamable != null)
            {
                if (e.Button == MouseButtons.Left)
                {
                    if (DraggingDist < 30)
                    {
                        if (DateTime.Now.TimeOfDay.TotalMilliseconds - MouseDownTime < 300.0)
                        {
                            ChangingSelection = true;
                            Controller.OnSelectStreamable(streamable, true);

                            if (entity != null)
                            {
                                Controller.OnSelectEntity(entity, true);
                            }

                            if (lastHoveredCullbox != null)
                            {
                                selectedCullbox = lastHoveredCullbox;
                            }

                            ChangingSelection = false;
                        }
                    }
                }
            }

            dragging = false;
        }

        private void ToggleTypeVisibility(int entityType, MenuItem item)
        {
            EntityTypesToShow ^= (uint) (1 << entityType);
            item.Checked = (EntityTypesToShow & (1 << entityType)) != 0;
            Refresh();
        }

        private void ToggleStatusVisibility(Streamable.Status status, MenuItem item)
        {
            bool newStatus = !visibleStatus[status];
            visibleStatus[status] = newStatus;

            item.Checked = newStatus;
            Refresh();
        }

        private void ToggleLodStateVisibility(Entity.NewState.LodStates lodState, MenuItem item)
        {
            bool newStatus = !visibleLodStates[lodState];
            visibleLodStates[lodState] = newStatus;

            item.Checked = newStatus;
            Refresh();
        }

        private void ToggleFollowCamera()
        {
            CenterAroundCamera = !CenterAroundCamera;
        }

        private void ToggleShowStateChangesOnly(MenuItem item)
        {
            showStateChangesOnly = !showStateChangesOnly;
            item.Checked = showStateChangesOnly;

            Refresh();
        }

        private void PopulateContextMenu(ContextMenu menu)
        {
            if (Scene != null)
            {
                // Handle visibility flags for each entity type
                MenuItem[] typeItems = new MenuItem[Scene.EntityTypeCount];
                int count = 0;

                foreach (KeyValuePair<int, EntityType> kvp in Scene.EntityTypes)
                {
                    MenuItem item = new MenuItem(kvp.Value.Name);
                    int entityTypeId = kvp.Key;
                    item.Checked = (EntityTypesToShow & (1 << entityTypeId)) != 0;

                    item.Click += (sender, e) => ToggleTypeVisibility(entityTypeId, item);

                    typeItems[count++] = item;
                }

                MenuItem typesMenu = new MenuItem("&Types", typeItems);
                menu.MenuItems.Add(typesMenu);
            }

            // Handle visibility flags for each residency status
            MenuItem[] statusItems = new MenuItem[visibleStatus.Count];
            int statusItemCount = 0;

            foreach (KeyValuePair<Streamable.Status, Boolean> kvp in visibleStatus)
            {
                MenuItem item = new MenuItem(kvp.Key.ToString());
                item.Checked = kvp.Value;
                Streamable.Status status = kvp.Key;

                item.Click += (sender, e) => ToggleStatusVisibility(status, item);
                statusItems[statusItemCount++] = item;
            }

            menu.MenuItems.Add(new MenuItem("&Status", statusItems));

            // Handle visibility flags for each LOD State
            MenuItem[] lodStateItems = new MenuItem[visibleLodStates.Count];
            int lodStateItemCount = 0;

            foreach (KeyValuePair<Entity.NewState.LodStates, Boolean> kvp in visibleLodStates)
            {
                MenuItem item = new MenuItem(kvp.Key.ToString());
                item.Checked = kvp.Value;
                Entity.NewState.LodStates lodState = kvp.Key;

                item.Click += (sender, e) => ToggleLodStateVisibility(lodState, item);
                lodStateItems[lodStateItemCount++] = item;
            }

            // Handle visibility flags for each LOD type
            menu.MenuItems.Add(ContextMenuHelper.CreateCheckboxSubmenu("&LOD Type", ShowLodTypes, Scene.LodTypes, () => Refresh(), true));

            menu.MenuItems.Add(ContextMenuHelper.CreateCheckboxSubmenu("&Flags", ShowFlagTypes, Request.FlagTypes, () => Refresh(), false));

            menu.MenuItems.Add(new MenuItem("&LOD States", lodStateItems));

            menu.MenuItems.Add(ContextMenuHelper.CreateToggleMenu("Show Sele&cted Only", ShowSelectedOnly, () => Refresh()));
            menu.MenuItems.Add(ContextMenuHelper.CreateToggleMenu("Show &Visible Only", ShowVisibleOnly, () => Refresh()));

            MenuItem[] imapSubMenu = new MenuItem[] {
                ContextMenuHelper.CreateToggleMenu("Show &IMAPs", ShowImaps, () => Refresh()),
                ContextMenuHelper.CreateToggleMenu("Show Culled IMAPs Only", ShowCulledImapsOnly, () => Refresh()),
                ContextMenuHelper.CreateToggleMenu("Show Physical IMAPs Bounds Only", ShowImapPhysicalBoundsOnly, () => Refresh()),
                ContextMenuHelper.CreateToggleMenu("I&MAP Selection Mode", ImapSelectionMode, () => Refresh()),
                ContextMenuHelper.CreateToggleMenu("&Solid IMAP Boxes", SolidImapBoxes, () => Refresh()),
            };

            menu.MenuItems.Add(new MenuItem("&IMAPs", imapSubMenu));

            MenuItem[] cullboxSubMenu = new MenuItem[] {
                ContextMenuHelper.CreateToggleMenu("Show &Cullboxes", ShowCullboxes, () => Refresh()),
                ContextMenuHelper.CreateToggleMenu("Cullbox Selection Mode", CullboxSelectionMode, () => Refresh()),
                ContextMenuHelper.CreateToggleMenu("&Solid Cullbox Boxes", SolidCullboxBoxes, () => Refresh()),
            };

            menu.MenuItems.Add(new MenuItem("Cullboxes", cullboxSubMenu));

            menu.MenuItems.Add(ContextMenuHelper.CreateToggleMenu("New &Color Scheme", NewColorScheme, () => Refresh()));
            menu.MenuItems.Add(ContextMenuHelper.CreateToggleMenu("&Priority Color Scheme", PriorityColorScheme, () => Refresh()));

            menu.MenuItems.Add(ContextMenuHelper.CreateToggleMenu("Center Selected", CenterSelected, () => Refresh()));
            menu.MenuItems.Add(ContextMenuHelper.CreateToggleMenu("Show Bound Sphere", ShowBoundSphere, () => Refresh()));

            menu.MenuItems.Add(ContextMenuHelper.CreateToggleMenu("Show Request Score", ShowRequestScore, () => Refresh()));

            //menu.MenuItems.Add(ContextMenuHelper.CreateActionMenu("Select Map", () => SelectMap()));
            
            CenterAroundCameraMenu = new MenuItem("&Follow Camera");
            CenterAroundCameraMenu.Click += (sender, e) => ToggleFollowCamera();
            CenterAroundCameraMenu.Checked = CenterAroundCamera;
            menu.MenuItems.Add(CenterAroundCameraMenu);

            MenuItem goToCamera = new MenuItem("&Go To Camera");
            goToCamera.Click += (sender, e) => GoToCamera(true);
            menu.MenuItems.Add(goToCamera);

            MenuItem stateChangesOnly = new MenuItem("Show State &Changes Only");
            stateChangesOnly.Checked = showStateChangesOnly;
            stateChangesOnly.Click += (sender, e) => ToggleShowStateChangesOnly(stateChangesOnly);
            menu.MenuItems.Add(stateChangesOnly);

            BackgroundMap.GetInstance().ReadMapList(false);
            MenuItem[] bgMapItems = BackgroundMap.GetInstance().CreateMenu(OnChangeMap);
            MenuItem bgMapItem = new MenuItem("Map", bgMapItems);

            menu.MenuItems.Add(bgMapItem);
        }

        private void OnChangeMap(BackgroundMap.MapOption map)
        {
            BackgroundBitmap = map;
            Refresh();
        }

        private void RecreateContextMenu()
        {
            ContextMenu menu = viewContextMenu.CreateContextMenu(Controller, this);
            PopulateContextMenu(menu);
            this.ContextMenu = menu;
        }

        private void EntityView_Load(object sender, EventArgs e)
        {
            BackgroundBitmap = BackgroundMap.GetInstance().GetDefaultMap();

            if (BackgroundBitmap != null)
            {
                BackgroundBitmap.LoadBitmap();
                OnChangeMap(BackgroundBitmap);
            }

            RecreateContextMenu();
        }

        // Switch the display to focus on a certain frame
        public void SetToFrame(int frameNumber)
        {
            if (Scene != null)
            {
                Frame = Scene.GetFrame(frameNumber);
                FrameNumber = frameNumber;
            }

            OnFrameChanged();

            RefreshDeferred();
        }

        // A new device was registered, or a device has changed.
        public void OnDeviceChanged(int deviceIndex, Device device) { }

        // A streamable has been selected
        public void OnSelectStreamable(Streamable streamable)
        {
            selectedStreamable = streamable;

            if (CenterSelected.Value)
            {
                FindCenter(streamable, out CenterX, out CenterY);
            }

            RefreshDeferred();
        }

        // The client information has been updated
        public void ClientInfoUpdated() { }

        public void OnSelectEntity(Entity entity)
        {
            if (!ChangingSelection && CenterSelected.Value)
            {
                // Center around this entity.
                FindCenter(entity, out CenterX, out CenterY);
                selectedStreamable = entity.CurrentState.Streamable;
                Refresh();
            }
        }

        // We're in follow mode, and a new frame has been added.
        public void OnNewFrame(int frameNumber)
        {
            if (Scene != null)
            {
                Frame = Scene.GetFrame(frameNumber);
                FrameNumber = frameNumber;
            }

            OnFrameChanged();

            if (throttler.MayUpdate())
            {
                RefreshDeferred();
            }
        }

        // A new entity type was registered
        public void OnNewEntityType(int entityTypeId, EntityType type)
        {
            // By default, disable what we think should be disabled.
            if (type.Visible == false)
            {
                EntityTypesToShow &= (uint) ~(1 << entityTypeId);
            }
            RecreateContextMenu();
        }

        // The connection has been terminated
        public void OnConnectionLost() { }

        // Register this view with the system
        public void Register(MainController controller, Scene scene)
        {
            this.Controller = controller;
            this.Scene = scene;
        }

        public void OnFilterChanged()
        {
            Refresh();
        }

        public void OnFrameRangeChanged(int firstFrame, int lastFrame)
        {

        }
    }
}
