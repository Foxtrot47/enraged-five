﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StreamingVisualize.Controller;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using StreamingVisualize.View;

namespace StreamingVisualize.Model
{
    public class Scene : IVersionedSerializable
    {
        private class OrphanRequest
        {
            public enum ActionType
            {
                PROCESS,
                FINISH,
            };

            public int Handle { get; set; }
            public ActionType Type { get; set; }
            public int Frame { get; set; }
            public float Time { get; set; }
            public int DeviceIndex { get; set; }
        }

        // Next index to assign to a dummy streamable.
        private int nextDummyStreamableIndex = -2;

        private List<OrphanRequest> orphanRequests = new List<OrphanRequest>();

        public Dictionary<String, Streamable> DummyStreamables = new Dictionary<String, Streamable>();

        public Dictionary<String, Streamable> StreamableLookup = new Dictionary<String, Streamable>();

        public Dictionary<long, String> LowLevelHandleLookup = new Dictionary<long, String>();

        public NamedFlags SearchTypes = new NamedFlags();

        // Mapping from gameside strIndex to index inside streamable array
        public Dictionary<int, int> StrIndexToIndex = new Dictionary<int, int>();

        public StringPool StringPool = new StringPool();

        public VisFlagsDef VisFlagsDef = new VisFlagsDef();

        // Dummy streamables. Streamable -2 would be at index 0, -3 at index 1, etc.
        public List<Streamable> DummyStreamableFixupArray = new List<Streamable>();

        public Module DummyStreamableModule = new Module();

        public List<int> Markers = new List<int>();

        public List<StreamDevice> StreamDevices = new List<StreamDevice>();

        // strIndex -> MapData map
        public Dictionary<int, MapData> MapDatas = new Dictionary<int, MapData>();

        // mapDataId -> MapData map
        public Dictionary<int, MapData> MapDataFromId = new Dictionary<int, MapData>();

        // All entities in this list are currently not in their optimal LOD and need to be watched.
        private List<Entity> CurrentEntityLodProblems = new List<Entity>();

        // All cullboxes.
        public List<Cullbox> Cullboxes = new List<Cullbox>();

        public Keyframe CurrentFrame = new Keyframe();

        public int CurrentFrameNumber = 0;

        // The current frame range selected by the user
        public int SelectedFirstFrame = 0;
        public int SelectedLastFrame = 0;

        // Next index in the streamable array to use
        private int NextStreamableIndex = 0;

        // This is the frame we're currently adding data to.
        public int PresentFrameNumber;

        public MainController controller { get; set; }

        public Playback Playback = new Playback();

        // Highest module number, plus 1.
        public int maxModuleIndex;

        // The highest frame number, plus 1.
        public int MaxFrame { get { return frames.Count; } }

        public String BuildConfiguration { get; set; }
        public String Platform { get; set; }
        public String RemoteAddr { get; set; }
        public String Version { get; set; }
        public String AssetType { get; set; }
        public String RestIP { get; set; }

        public Filter Filter = new Filter();

        public int MapDataModuleIndex = 0;
        public int ArchetypeModuleIndex = 0;

        // If true, we'll follow next frames as they happen
        public bool FollowMode = true;

        public Dictionary<int, EntityType> EntityTypes = new Dictionary<int, EntityType>();

        public Dictionary<int, LodType> LodTypes = new Dictionary<int, LodType>();

        // Dummy lod type for older files
        public LodType DummyLodType = new LodType();

//        List<Streamable> streamables = new List<Streamable>(100000);
//        List<Module> modules = new List<Module>(100);

        // These are all streamables that have been mapped to a strIndex.
        Streamable[] streamables = new Streamable[1000000];

        // All known streamables, including dummies.
        List<Streamable> AllStreamables = new List<Streamable>();

        Module[] modules = new Module[100];
        Device[] devices = new Device[5];

        public int MaxStreamableIndex { get { return streamables.Length; } }

        public int maxDeviceIndex = 0;

        List<Frame> frames = new List<Frame>();

        public int FrameCount { get { return frames.Count; } }

        public int EntityTypeCount { get { return EntityTypes.Count; } }

        // The filename we saved this scene as (if it was saved already).
        public String Filename;

        // Global lock on the entire scene - held by worker thread while processing commands, held by UI
        // thread while serializing.
        public object LockObj = new object();

        // Used while saving. Dirty, but that's what happens when you retrofit stuff.
        public ProgressView ProgressView;
        public bool LastSaveSuccess;


        private const int FLAGTYPE_SEARCH = 0;





        public Scene()
        {
            DummyStreamableModule.Name = "(Not Managed By Module)";

            // Create the first frame.
            Frame frame = new Frame();
            frames.Add(frame);
        }

        public void UnregisterObject(int strIndex)
        {
            int index;

            if (StrIndexToIndex.TryGetValue(strIndex, out index))
            {
                Streamable streamable = streamables[index];
                if (streamable != null)
                {
                    RscEvent reqEvent = new RscEvent();
                    reqEvent.Action = RscEvent.ActionType.UNREGISTER;
                    reqEvent.FrameNumber = PresentFrameNumber;
                    reqEvent.Context = "";
                    streamable.Events.Add(reqEvent);

                    if (streamable.History.PresentState.Status != Streamable.Status.UNREGISTERED)
                    {
                        streamable.History.GetStateForWriting(this).Status = Streamable.Status.UNREGISTERED;
                        streamable.History.ValidateDupe();
                    }

                    if (streamable.Entities.Count > 0)
                    {
                        // Console.WriteLine("Streamable " + streamable.CurrentName + " still has " + streamable.Entities.Count + " entities");
                        streamable.Entities.Clear();
                    }
                }
            }
        }

        public void RegisterObject(int strIndex, Module module, String name, int virtSize, int physSize, int fileSize, int parentIndex)
        {
            int index;

            if (StrIndexToIndex.TryGetValue(strIndex, out index))
            {
                if (streamables[index] != null)
                {
                    // Re-registering.
                    Streamable oldStreamable = streamables[index];
                    oldStreamable.IdentityHistory.GetStateForWriting(PresentFrameNumber, this).Name = name;
                    oldStreamable.IdentityHistory.PresentState.LodTypeId = -1;
                    oldStreamable.IdentityHistory.ValidateDupe();


                    RscEvent reqEvent = new RscEvent();
                    reqEvent.Action = RscEvent.ActionType.REGISTER;
                    reqEvent.FrameNumber = PresentFrameNumber;
                    reqEvent.Context = "";
                    oldStreamable.Events.Add(reqEvent);
                    lock (StreamableLookup)
                    {
                        StreamableLookup[name] = oldStreamable;
                    }

                    // We may have gotten a preliminary registration without size information - let's update that.
                    if (oldStreamable.VirtSize == 0 && oldStreamable.PhysSize == 0 && (virtSize != 0 || physSize != 0))
                    {
                        oldStreamable.VirtSize = virtSize;
                        oldStreamable.PhysSize = physSize;
                        oldStreamable.FileSize = fileSize;
                    }

                    if (parentIndex != -1 && oldStreamable.ParentFile == null)
                    {
                        oldStreamable.ParentFile = (parentIndex == -1) ? null : GetOrCreateStreamable(parentIndex);
                    }

                    if (oldStreamable.Module == DummyStreamableModule)
                    {
                        oldStreamable.Module = module;
                    }

                    return;   
                }
            }

            index = NextStreamableIndex++;

            Streamable streamable = new Streamable();
            streamable.IdentityHistory.GetStateForWriting(0, this).Name = name;
            streamable.IdentityHistory.PresentState.LodTypeId = -1;
            streamable.IdentityHistory.ValidateDupe();
            streamable.Index = index;
            streamable.StrIndex = strIndex;
            streamable.Module = module;
            streamable.VirtSize = virtSize;
            streamable.PhysSize = physSize;
            streamable.FileSize = fileSize;
            streamable.ParentFile = (parentIndex == -1) ? null : GetOrCreateStreamable(parentIndex);

            lock (StrIndexToIndex)
            {
                StrIndexToIndex[strIndex] = index;
            }

            lock (AllStreamables)
            {
                AllStreamables.Add(streamable);
            }

            streamables[index] = streamable;
            lock (StreamableLookup)
            {
                StreamableLookup[name] = streamable;
            }

            Debug.Assert(module != null);
        }

        public void RegisterModule(int moduleIndex, String name)
        {
            Module module = new Module();
            module.Name = name;
            module.Index = moduleIndex;

            modules[moduleIndex] = module;

            maxModuleIndex = Math.Max(maxModuleIndex, moduleIndex + 1);
        }

        public void RegisterVisFlag(int flag, String name)
        {
            bool isOptionFlag = (flag & 0x80000000) != 0;
            flag &= 0x7fffffff;

            if (isOptionFlag)
            {
                VisFlagsDef.OptionFlags[flag] = name;
            }
            else
            {
                VisFlagsDef.PhaseFlags[flag] = name;
            }

            // Handle the known special flags
            if (name.Equals("GBUF"))
            {
                VisFlagsDef.GbufFlag = flag;
            }
        }

        public NamedFlags GetNamedFlagFromType(int flagType)
        {
            switch (flagType)
            {
                case FLAGTYPE_SEARCH:
                    return SearchTypes;
            }

            return null;
        }

        public void RegisterNamedFlag(int flagType, int flagValue, String flagName)
        {
            GetNamedFlagFromType(flagType).RegisterFlag(flagValue, flagName);
        }

        public void SetFlags(int strIndex, int flags, String context)
        {
            Streamable streamable = GetStreamable(strIndex);

            if (streamable != null)
            {
                RscEvent rscEvent = new RscEvent();

                rscEvent.Action = RscEvent.ActionType.FlagsChange;
                rscEvent.Flags = flags;
                rscEvent.FrameNumber = PresentFrameNumber;
                rscEvent.Context = context;
                streamable.Events.Add(rscEvent);

                Streamable.State state = streamable.History.GetStateForWriting(this);
                state.Flags = flags;
            }
        }

        public void AddBoxSearch(int searchTypeId, Box16 box)
        {
            BoxSearch boxSearch = new BoxSearch();

            boxSearch.SearchType = searchTypeId;
            boxSearch.Aabb = box;

            GetPresentFrame().BoxSearches.Add(boxSearch);
        }

        public void RegisterDevice(int deviceIndex, String name)
        {
            Device device = new Device();

            device.Name = name;
            devices[deviceIndex] = device;

            maxDeviceIndex = Math.Max(maxDeviceIndex, deviceIndex + 1);

            // HACK HACK
            if (controller != null)
            {
                controller.RegisterDevice(deviceIndex, device);
            }
        }

        public void RegisterAllDevices()
        {
            for (int x = 0; x < devices.Length; x++)
            {
                if (devices[x] != null)
                {
                    controller.RegisterDevice(x, devices[x]);
                }
            }
        }

        // Return the frame we're adding data to right now.
        public Frame GetPresentFrame()
        {
            return frames[PresentFrameNumber];
        }

        public Frame GetFrame(int frame)
        {
            return frames[frame];
        }

        public Frame CreateFrame(int frame)
        {
            while (frames.Count <= frame)
            {
                Frame newFrame = new Frame();
                newFrame.Reqs = frames[frames.Count - 1].Reqs;
                newFrame.RealReqs = frames[frames.Count - 1].RealReqs;
                frames.Add(newFrame);
            }

            return frames[frame];
        }

        public Streamable GetStreamable(int strIndex)
        {
            int index;

            if (StrIndexToIndex.TryGetValue(strIndex, out index))
            {
                if (index >= 0)
                {
                    return streamables[index];
                }
            }

            return null;
        }

        public void ApplyFilter(Filter filter)
        {
            lock (AllStreamables)
            {
                foreach (Streamable streamable in AllStreamables)
                {
                    streamable.Filtered = filter.IsFilteredOut(streamable);
                }
            }
        }

        public Streamable GetStreamableByIndex(int index)
        {
            return streamables[index];
        }

        public Streamable GetOrCreateStreamableByIndex(int index)
        {
            if (streamables[index] != null)
            {
                return streamables[index];
            }

            Streamable streamable = new Streamable();
            streamable.Module = DummyStreamableModule;
            String Name = "DUMMY - SHOULD HAVE BEEN REPLACED DURING LOADING (index " + index + ")"; // Do not use string pool here

            streamable.IdentityHistory.GetStateForWriting(0, this).Name = Name;
            streamable.IdentityHistory.PresentState.LodTypeId = -1;
            streamable.IdentityHistory.ValidateDupe();

            streamable.Index = index;
            streamable.StrIndex = -1;

            streamables[index] = streamable;

            return streamable;
        }


        public Streamable GetOrCreateStreamable(int strIndex)
        {
         //   Debug.Assert(streamables[strIndex] != null, "Accessing invalid streamable " + strIndex);

            int index = -1;

            if (StrIndexToIndex.TryGetValue(strIndex, out index))
            {
                if (streamables[index] != null)
                {
                    return streamables[index];
                }
            }

            Streamable streamable = new Streamable();
            streamable.Module = DummyStreamableModule;
            String Name = StringPool.GetString("INVALID STREAMABLE " + strIndex);
            streamable.IdentityHistory.GetStateForWriting(0, this).Name = Name;
            streamable.IdentityHistory.PresentState.LodTypeId = -1;
            streamable.IdentityHistory.ValidateDupe();

            index = NextStreamableIndex++;
            streamable.Index = index;
            streamable.StrIndex = strIndex;

            lock (StrIndexToIndex)
            {
                StrIndexToIndex[strIndex] = index;
            }

            streamables[index] = streamable;
            lock (StreamableLookup)
            {
                StreamableLookup[Name] = streamable;
            }

            lock (AllStreamables)
            {
                AllStreamables.Add(streamable);
            }

            return streamable;
        }

        public Module GetModule(int moduleIndex)
        {
            return modules[moduleIndex];
        }

        public Device GetDevice(int deviceIndex)
        {
            return devices[deviceIndex];
        }

        public void AddRequest(int strIndex, int flags, float time, float score, String context)
        {
            Streamable streamable = GetOrCreateStreamable(strIndex);
            Frame frameObj = GetPresentFrame();
            frameObj.AddRequest(streamable, PresentFrameNumber, flags, score, context);


            // Let's get the previous three states of this streamable.
            // If it was a pattern of request -> requested -> unrequest -> request, then we can
            // cull the state a bit.
            int evCount = streamable.Events.Count;
            int prevFrame = PresentFrameNumber - 1;

            if (evCount > 2)
            {
                if (streamable.Events[evCount - 1].Action == RscEvent.ActionType.Unloaded)
                {
                    if (streamable.Events[evCount - 2].Action == RscEvent.ActionType.UNREQUEST)
                    {
                        if (streamable.Events[evCount - 3].Action == RscEvent.ActionType.LoadRequested && streamable.Events[evCount - 3].FrameNumber == prevFrame)
                        {
                            // Yep, that's the pattern.
                            // For now, let's not delete the previous REQUEST - the score and/or circumstances
                            // might be different. But the rest can go.
                            streamable.Events.RemoveRange(evCount - 3, 3);

                            // This ALSO means that we should update the entity states, so they show up as "requested". Otherwise, the prevailing state
                            // for the previous frame would have been "not loaded".
                            foreach (Entity entity in streamable.Entities)
                            {
                                // Update the previous state, if that's the current one. No need to create a new
                                // state, given what we've seen above, it's all but certain that the entity has seen its status change.
                                if (entity.NewHistory.PresentState.LoadedState == Streamable.Status.NOTLOADED)
                                {
                                    entity.NewHistory.PresentState.LoadedState = Streamable.Status.LOADREQUESTED;
                                }
                            }
                        }
                    }
                }
            }

            RscEvent rscEvent = new RscEvent();
            rscEvent.Action = RscEvent.ActionType.REQUEST;
            rscEvent.Reason = RscEvent.ReasonType.NONE;
            rscEvent.FrameNumber = PresentFrameNumber;
            rscEvent.StartTime = time;
            rscEvent.Score = score;
            rscEvent.Context = StringPool.GetString(context);
            rscEvent.Flags = flags;

            Debug.Assert(context != null);

            streamable.Events.Add(rscEvent);

            if (streamable.History.PresentState.Flags != flags)
            {
                streamable.History.GetStateForWriting(this).Flags = flags;
            }
        }

        public void AddUnrequest(int strIndex, String context, int strIndexNeedingMemory, float score)
        {
            // Adjust the context if we know who's trying to get memory.
            if (strIndexNeedingMemory != -1)
            {
                Streamable needsMemory = GetOrCreateStreamable(strIndexNeedingMemory);

                context = StringPool.GetString("For " + needsMemory.IdentityHistory.PresentState.Name);
            }
            Streamable streamable = GetOrCreateStreamable(strIndex);
            Frame frameObj = GetPresentFrame();
            frameObj.AddUnrequest(streamable, PresentFrameNumber, context);

            RscEvent rscEvent = new RscEvent();
            rscEvent.Action = RscEvent.ActionType.UNREQUEST;
//            rscEvent.Reason = RscEvent.ReasonType.NONE;
            rscEvent.FrameNumber = PresentFrameNumber;
            rscEvent.Score = score;
            rscEvent.Context = StringPool.GetString(context);

            Debug.Assert(context != null);

            streamable.Events.Add(rscEvent);

            // Did we just doom an existing request?
            foreach (Request request in frameObj.requests)
            {
                if (request.Streamable == streamable)
                {
                    // Yup.
                    request.Flags |= Request.DOOMED;
                }
            }

            // Check the previous frame too, since the new frame setup makes unrequests lag a frame behind.
            if (CurrentFrameNumber > 0)
            {
                Frame prevFrameObj = GetFrame(CurrentFrameNumber - 1);
                foreach (Request request in prevFrameObj.requests)
                {
                    if (request.Streamable == streamable)
                    {
                        // Yup.
                        request.Flags |= Request.DOOMED;
                    }
                }
            }
        }

        public void AddStatusChange(int strIndex, int newStatus)
        {
            Streamable streamable = GetOrCreateStreamable(strIndex);
/*            Frame frameObj = GetPresentFrame();
            frameObj.AddStatusChange(streamable, PresentFrameNumber, newStatus);*/

            RscEvent rscEvent = new RscEvent();
            rscEvent.FrameNumber = PresentFrameNumber;
            rscEvent.Context = "";

            switch (newStatus)
            {
                case (int) Streamable.Status.NOTLOADED:
                    rscEvent.Action = RscEvent.ActionType.Unloaded;
                    break;

                case (int) Streamable.Status.LOADED:
                    rscEvent.Action = RscEvent.ActionType.Loaded;
                    break;

                case (int) Streamable.Status.LOADREQUESTED:
                    rscEvent.Action = RscEvent.ActionType.LoadRequested;
                    break;

                default:
                    rscEvent.Action = RscEvent.ActionType.Loading;
                    break;
            }


            streamable.Events.Add(rscEvent);
            streamable.SetStatus(PresentFrameNumber, (Streamable.Status) newStatus, this);
        }

        public void AddIssueRequest(float time, int strIndex, int deviceIndex, int handle, int lsn, int flags)
        {
            Streamable streamable = GetOrCreateStreamable(strIndex);
            Device device = GetDevice(deviceIndex);

            DeviceEvent devEvent = new DeviceEvent();
            devEvent.Streamable = streamable;
            devEvent.Time = time;
            devEvent.Frame = PresentFrameNumber;
            devEvent.LSN = lsn;
            streamable.LSN = lsn;
            devEvent.Handle = handle;
            devEvent.Action = DeviceEvent.ActionType.QUEUE;
            device.DeviceEvents.Add(devEvent);
//            device.OpenRequests.Add(handle, streamable);
            device.OpenRequests[handle] = streamable;

            DeviceStreamingState streamingState = device.StreamingState.GetStateForWriting(this);
            streamingState.AddQueued(streamable, (flags & Request.PRIORITY_LOAD) != 0, PresentFrameNumber, time);

            RscEvent lastRequest = streamable.GetLastRequest();
            lastRequest.IssueTime = time;
            //lastRequest.DeviceRequestIndex = device.DeviceEvents.Count - 1;

            ProcessOrphanRequests(deviceIndex);
        }

        public void OpenRawFile(float time, /*long handle,*/ int handle, String name)
        {
            Streamable streamable = GetOrCreateDummyStreamable(name, 0);
            //Streamable hostStreamable = GetOrCreateDummyStreamable(LowLevelHandleLookup[handle], 0);
            Device device = GetDevice(0);   // TODO: Get this somehow

            DeviceEvent devEvent = new DeviceEvent();
            devEvent.Streamable = streamable;
            devEvent.Time = time;
            devEvent.Frame = PresentFrameNumber;
            devEvent.LSN = 0;
            devEvent.Handle = handle;
            devEvent.Action = DeviceEvent.ActionType.QUEUE;
            device.DeviceEvents.Add(devEvent);
            device.OpenRequests[handle] = streamable;

            DeviceStreamingState streamingState = device.StreamingState.GetStateForWriting(this);
            streamingState.AddQueued(streamable, false, PresentFrameNumber, time);

            RscEvent rscEvent = new RscEvent();
            rscEvent.Action = RscEvent.ActionType.REQUEST;
            rscEvent.Reason = RscEvent.ReasonType.NONE;
            rscEvent.FrameNumber = PresentFrameNumber;
            rscEvent.StartTime = time;
            rscEvent.Score = 0.0f;
            rscEvent.Context = "";
            rscEvent.Flags = 0;

            streamable.Events.Add(rscEvent);

            //RscEvent lastRequest = streamable.GetLastRequest();
            //lastRequest.IssueTime = time;
            //lastRequest.DeviceRequestIndex = device.DeviceEvents.Count - 1;
        }

        private void ProcessOrphanRequests(int deviceIndex)
        {
            Device device = GetDevice(deviceIndex);

            for (int x = 0; x < orphanRequests.Count; x++)
            {
				try
				{
					OrphanRequest request = orphanRequests[x];

					// TODO: We should be able to limit it to one device... doesn't work though yet.
					if (request.DeviceIndex == deviceIndex)
					{
						if (device.OpenRequests.ContainsKey(request.Handle))
						{
							Streamable streamable = device.OpenRequests[request.Handle];
							RscEvent rscEvent = streamable.GetLastRequest();
							Debug.Assert(rscEvent != null, "No open request for " + streamable);

							DeviceStreamingState streamingState = device.StreamingState.GetStateForWriting(this);

							switch (request.Type)
							{
								case OrphanRequest.ActionType.PROCESS:
									rscEvent.ProcessTime = request.Time;

									try
									{
										DeviceEvent devEvent = new DeviceEvent();
										devEvent.Streamable = streamable;
										devEvent.Time = request.Time;
										devEvent.Frame = request.Frame;
										devEvent.Handle = request.Handle;
										devEvent.LSN = streamable.LSN;
										devEvent.Action = DeviceEvent.ActionType.READ;
										device.DeviceEvents.Add(devEvent);
									}
									catch (Exception exc)
									{
										Console.WriteLine("Error in OrphanRequest.ActionType.PROCESS");
										Console.WriteLine("Exception : {0}", exc.ToString());
										Console.WriteLine("StackTrace: '{0}'", Environment.StackTrace);
										Console.WriteLine("StackTrace Ex: '{0}'", exc.StackTrace);
									}
									streamingState.AddStreaming(streamable, (rscEvent.Flags & Request.PRIORITY_LOAD) != 0, request.Frame, request.Time);
									break;

								case OrphanRequest.ActionType.FINISH:
									rscEvent.LoadTime = request.Time;
									try
									{

										// Since we're finished now, we can remove the "open request".
										device.OpenRequests.Remove(request.Handle);
										streamingState.FinishStreaming(streamable);
									}
									catch (Exception exc)
									{
										Console.WriteLine("Error in OrphanRequest.ActionType.FINISH");
										Console.WriteLine("Exception : {0}", exc.ToString());
										Console.WriteLine("StackTrace: '{0}'", Environment.StackTrace);
										Console.WriteLine("StackTrace Ex: '{0}'", exc.StackTrace);										
									}
									break;
							}

							orphanRequests.RemoveAt(x);
							x--;
						}
					}

					if (request.Frame < frames.Count - 30)
					{
						Debug.Assert(false, "OLD ORPHAN - handle " + request.Handle + ", device=" + request.DeviceIndex + ", frame=" + request.Frame);
					}
				}
				catch (Exception exc)
				{
					try
					{
						Console.WriteLine("StackTrace: '{0}'", Environment.StackTrace);
						Console.WriteLine("StackTrace Ex: '{0}'", exc.StackTrace);
						
						
						
						Console.WriteLine("Error in ProcessOrphanRequests");

						Console.WriteLine("x {0}", x);
						//Console.WriteLine("request.DeviceIndex {0}", request.DeviceIndex);
						Console.WriteLine("orphanRequests.Count {0}", orphanRequests.Count);
						Console.WriteLine("orphanRequests.Count {0}", deviceIndex);
						
					}
					finally
					{
						
					}
				}
            }
        }

        public void AddProcessRequest(float time, int deviceIndex, int handle)
        {
            Device device = GetDevice(deviceIndex);

            OrphanRequest request = new OrphanRequest();
            request.Time = time;
            request.Frame = PresentFrameNumber;
            request.DeviceIndex = deviceIndex;
            request.Handle = handle;
            request.Type = OrphanRequest.ActionType.PROCESS;
            orphanRequests.Add(request);

            ProcessOrphanRequests(deviceIndex);
        }

        public void AddFinishRequest(float time, int deviceIndex, int handle)
        {
            OrphanRequest request = new OrphanRequest();
            request.Time = time;
            request.Frame = PresentFrameNumber;
            request.DeviceIndex = deviceIndex;
            request.Handle = handle;
            request.Type = OrphanRequest.ActionType.FINISH;
            orphanRequests.Add(request);

            ProcessOrphanRequests(deviceIndex);
        }

        private Streamable GetOrCreateDummyStreamable(String filename, int lsn)
        {
            Streamable streamable;

            if (DummyStreamables.ContainsKey(filename))
            {
                streamable = DummyStreamables[filename];
            }
            else
            {
                Debug.Assert(DummyStreamableFixupArray.Count == -2 - nextDummyStreamableIndex);

                streamable = new Streamable();
                streamable.Module = DummyStreamableModule;
                streamable.IdentityHistory.GetStateForWriting(0, this).Name = filename;
                streamable.IdentityHistory.PresentState.LodTypeId = -1;
                streamable.IdentityHistory.ValidateDupe();
                streamable.Index = nextDummyStreamableIndex--;
                streamable.StrIndex = streamable.Index;

                lock (StrIndexToIndex)
                {
                    StrIndexToIndex[streamable.StrIndex] = streamable.Index;
                }

                streamable.LSN = lsn;
                lock (StreamableLookup)
                {
                    StreamableLookup[filename] = streamable;
                }
                DummyStreamableFixupArray.Add(streamable);
                lock (AllStreamables)
                {
                    AllStreamables.Add(streamable);
                }
            }

            return streamable;
        }

        public void AddStreamerRead(float time, int deviceIndex, int handle, String filename, int lsn)
        {
            Device device = GetDevice(deviceIndex);
            Streamable streamable = GetOrCreateDummyStreamable(filename, lsn);

            DeviceEvent devEvent = new DeviceEvent();
            devEvent.Streamable = streamable;
            devEvent.Time = time;
            devEvent.Frame = PresentFrameNumber;
            devEvent.Handle = handle;
            devEvent.LSN = lsn;
            device.DeviceEvents.Add(devEvent);
            Debug.Assert(device.OpenRequests.ContainsKey(handle) == false, "Request contention - there's already an open request for handle " + handle + " on device " + deviceIndex);
            device.OpenRequests[handle] = streamable;

            DeviceStreamingState streamingState = device.StreamingState.GetStateForWriting(this);

            // TODO: These are low-level requests, so we'll consider them "priority" requests. In real life,
            // they don't really have the concept of priority, but they cockblock the streaming system, so
            // I guess that counts.
            streamingState.AddQueued(streamable, true, PresentFrameNumber, time);

            // Create a dummy request object for this streamable.
            RscEvent lastRequest = new RscEvent();

            lastRequest.Score = -1.0f;
            lastRequest.IssueTime = time;
            //lastRequest.DeviceRequestIndex = device.DeviceEvents.Count - 1;
            lastRequest.Context = "";
            streamable.Events.Add(lastRequest);

            ProcessOrphanRequests(deviceIndex);
        }

        public void AddStreamerDecompress(float time, int deviceIndex, int handle, bool starting)
        {
            Device device = GetDevice(deviceIndex);

            Debug.Assert(device.OpenRequests.ContainsKey(handle));

            DeviceEvent devEvent = new DeviceEvent();
            devEvent.Streamable = device.OpenRequests[handle];
            devEvent.Time = time;
            devEvent.Frame = PresentFrameNumber;
            devEvent.Handle = handle;
            devEvent.Action = (starting) ? DeviceEvent.ActionType.DECOMPRESS : DeviceEvent.ActionType.DECOMPRESSEND;
            device.DeviceEvents.Add(devEvent);
        }

        public void AddCancelRequest(float time, int strIndex)
        {
            Streamable streamable = GetOrCreateStreamable(strIndex);
            //Frame frameObj = GetFrame(frame);
            //frameObj.AddCancel(streamable, frame);

            RscEvent rscEvent = new RscEvent();
            rscEvent.Action = RscEvent.ActionType.CANCEL;
            rscEvent.Reason = RscEvent.ReasonType.NONE;
            rscEvent.FrameNumber = PresentFrameNumber;
            rscEvent.StartTime = time;

            streamable.Events.Add(rscEvent);
        }

        public Frame BeginNewFrame()
        {
            PresentFrameNumber++;
            Frame newFrame = new Frame();
            newFrame.Reqs = frames[frames.Count - 1].Reqs;
            newFrame.RealReqs = frames[frames.Count - 1].RealReqs;

            frames.Add(newFrame);

            // Toss all the "finished" events.
            foreach (Device device in devices)
            {
                if (device != null)
                {
                    device.PurgeFinishedEvents(this);
                }
            }

            // Let's undo the LOD flags from the previous frame.
            // We'll do this for TWO frames ago, for several reasons - #1 that way we don't constantly create new
            // entity state history elements even if the state hasn't changed, and #2 that way we don't immediately clear out
            // the latest state, so it'll be visible in the entity view.
            if (frames.Count > 1)
            {
                Frame prevFrame = frames[frames.Count - 2];

                List<Entity> remainingProblemEntities = new List<Entity>();

                foreach (Entity entity in CurrentEntityLodProblems)
                {
                    Debug.Assert(entity.IsOnWatchList == true);

                    Entity.NewState.LodStates prevState = entity.PrevLodState;
                    entity.PrevLodState = entity.CurrentLodState;

                    entity.CurrentLodState = Entity.NewState.LodStates.LOD_OK;

                    if (entity.PrevLodState == Entity.NewState.LodStates.LOD_OK)
                    {
                        // This entity is no longer a problem entity.
                        entity.IsOnWatchList = false;
                    }
                    else
                    {
                        // This entity still needs to remain on the watch list.
                        remainingProblemEntities.Add(entity);
                    }

                    if (entity.PrevLodState != entity.NewHistory.PresentState.LodState)
                    {
                        StreamingVisualize.Model.Entity.NewState currentState = entity.NewHistory.GetStateForWriting(PresentFrameNumber, this);
                        currentState.LodState = entity.PrevLodState;

                        entity.NewHistory.ValidateDupe();

                        if (FollowMode)
                        {
                            entity.NewHistory.CurrentState = entity.NewHistory.PresentState;
                        }
                    }

                    if (!entity.IsOnWatchList)
                    {
                        Debug.Assert(entity.NewHistory.PresentState.LodState == Entity.NewState.LodStates.LOD_OK);
                    }
                }

                // Update the list of problem entities.
                CurrentEntityLodProblems = remainingProblemEntities;
            }

            return newFrame;
        }

        public void MarkNewFrame(int frame)
        {
            for (int x = 0; x < maxDeviceIndex; x++)
            {
                Device device = devices[x];

                if (device != null)
                {
                    device.MarkFrame(frame);
                }
            }

            if (FollowMode)
            {
                CurrentFrameNumber = frame;

                controller.OnNewFrame(frame);
            }
        }

        public void AddMarker(int flags)
        {
            // Add it to the list of markers if it wasn't already.
            if (Markers.Count == 0 || Markers[Markers.Count - 1] != PresentFrameNumber)
            {
                Markers.Add(PresentFrameNumber);
            }

            Frame frame = GetPresentFrame();
            frame.Flags |= Frame.MARKER | flags;
        }

        public Entity AddEntity(int entityId, Streamable streamable, Vector16 boxMin, Vector16 boxMax, int lodTypeId, Streamable mapDataStreamable, int guid, int entityType, int entityFlags, Vector16 sphereCenter, float sphereRadius, float lodDistance, int parentLodEntityId)
        {
            Entity entity = new Entity();

            entity.EntityId = entityId;

            Entity.PositionState posState = entity.PositionHistory.GetStateForWriting(0, this);
            posState.BoxMin = new Vector16(Vector16.Zero);
            posState.BoxMax = new Vector16(Vector16.Zero);
            posState.SetDeleted();

            entity.PositionHistory.ValidateDupe();

            posState = entity.PositionHistory.GetStateForWriting(this);
            posState.BoxMin = boxMin;
            posState.BoxMax = boxMax;
            posState.AdjustPosition();
            posState.SphereCenter = sphereCenter;
            posState.SphereRadius = sphereRadius;
            entity.PositionHistory.ValidateDupe();

            Entity.NewState state = entity.NewHistory.GetStateForWriting(0, this);
            state.SetDeleted();

            state = entity.NewHistory.GetStateForWriting(this);

            state.LodType = (byte)lodTypeId;
            state.EntityType = (byte)entityType;
            state.EntityFlags = entityFlags;
            state.Streamable = streamable;
            state.MapDataStreamable = mapDataStreamable;
            state.GUID = guid;
            state.LodDistance = lodDistance;
            state.ParentLodEntityId = parentLodEntityId;

            state.LoadedState = (streamable != null) ? streamable.History.PresentState.Status : Streamable.Status.NOTLOADED;

            entity.NewHistory.ValidateDupe();

            if (ArchetypeModuleIndex == 0 && streamable.Module != DummyStreamableModule)
            {
                ArchetypeModuleIndex = streamable.Module.Index;
            }

            // Do not create a new state for the streamable - especially because we'd have to copy dependencies and
            // stuff over. Also because the LOD type could change (HD vs ORPHANHD, for example).
            if (streamable != null)
            {
                streamable.IdentityHistory.PresentState.LodTypeId = lodTypeId;
                streamable.Entities.Add(entity);
            }

            CurrentFrame.EntityMap.Add(entityId, entity);
            CurrentFrame.Entities.Add(entity);
            return entity;
        }

        public void SetLodParent(int entityId, int parentEntityId)
        {
            Entity entity;

            if (CurrentFrame.EntityMap.TryGetValue(entityId, out entity))
            {
                if (entity.NewHistory.PresentState.ParentLodEntityId != parentEntityId)
                {
                    Entity.NewState state = entity.NewHistory.GetStateForWriting(PresentFrameNumber, this);
                    state.ParentLodEntityId = parentEntityId;
                }
            }
            else
            {
                Console.WriteLine("Unknown child entity " + entityId);
            }

        }

        public void UpdateEntity(int entityId, Streamable streamable, int lodType, int entityFlags, Vector16 boxMin, Vector16 boxMax, int entityType, Vector16 sphereCenter, float sphereRadius, float lodDistance, int parentLodEntityId)
        {
            Entity entity;

            if (!CurrentFrame.EntityMap.TryGetValue(entityId, out entity))
            {
                entity = AddEntity(entityId, streamable, boxMin, boxMax, lodType, null, 0, entityType, entityFlags, sphereCenter, sphereRadius, lodDistance, parentLodEntityId);
            }
            else
            {
                // Create a position state if that changed.
                StreamingVisualize.Model.Entity.PositionState presentPosState = entity.PositionHistory.PresentState;

                if (!(presentPosState.SphereRadius == sphereRadius && presentPosState.SphereCenter.Equals(sphereCenter) && presentPosState.BoxMin.Equals(boxMin) && presentPosState.BoxMax.Equals(boxMax)))
                {
                    // Need a new state.
                    presentPosState = entity.PositionHistory.GetStateForWriting(PresentFrameNumber, this);
                    presentPosState.BoxMin = boxMin;
                    presentPosState.BoxMax = boxMax;
                    presentPosState.AdjustPosition();
                    presentPosState.SphereRadius = sphereRadius;
                    presentPosState.SphereCenter = sphereCenter;

                    entity.PositionHistory.ValidateDupe();
                }

                Streamable.Status newLoadedState = (streamable != null) ? streamable.History.PresentState.Status : Streamable.Status.NOTLOADED;


                // Don't add a new state if nothing changed.
                StreamingVisualize.Model.Entity.NewState currentState = entity.NewHistory.PresentState;

                if (currentState != null)
                {
                    if (currentState.Streamable == streamable && currentState.LodType == lodType && currentState.EntityType == entityType && currentState.LoadedState == newLoadedState)
                    {
                        // Nothing changed - don't waste memory!
                        return;
                    }
                }

                // Create a new state for this entity.
                StreamingVisualize.Model.Entity.NewState state = entity.NewHistory.GetStateForWriting(PresentFrameNumber, this);

                state.LodType = (byte)lodType;
                state.EntityType = (byte)entityType;
                state.EntityFlags = entityFlags;
                state.ParentLodEntityId = parentLodEntityId;
                state.LodDistance = lodDistance;

                // Do not create a new state for the streamable - especially because we'd have to copy dependencies and
                // stuff over. Also because the LOD type could change (HD vs ORPHANHD, for example).
                streamable.IdentityHistory.PresentState.LodTypeId = lodType;

                Streamable prevStreamable = entity.NewHistory.PresentState.Streamable;

                if (prevStreamable != streamable)
                {
                    if (prevStreamable != null)
                    {
                        prevStreamable.Entities.Remove(entity);
                    }

                    state.LoadedState = streamable.History.PresentState.Status;
                    streamable.Entities.Add(entity);
                }

                state.Streamable = streamable;

                state.LoadedState = newLoadedState;

                entity.NewHistory.ValidateDupe();
            }
        }

        public void DeleteEntity(int entityId)
        {
            Entity entity;

            if (CurrentFrame.EntityMap.TryGetValue(entityId, out entity))
            {
                Streamable prevStreamable = entity.NewHistory.PresentState.Streamable;
                if (prevStreamable != null)
                {
                    prevStreamable.Entities.Remove(entity);
                }

                StreamingVisualize.Model.Entity.NewState state = entity.NewHistory.GetStateForWriting(PresentFrameNumber, this);
                state.SetDeleted();
                entity.NewHistory.ValidateDupe();

                StreamingVisualize.Model.Entity.PositionState posState = entity.PositionHistory.GetStateForWriting(PresentFrameNumber, this);
                posState.SetDeleted();
                entity.PositionHistory.ValidateDupe();
            }
        }

        public void MissingLod(int entityId, bool parentLodMissing)
        {
            AddMarker((parentLodMissing) ? Frame.MISSING_LOD : Frame.LOW_LOD);

            Entity entity;

            if (CurrentFrame.EntityMap.TryGetValue(entityId, out entity))
            {
                Entity.NewState.LodStates lodState = (parentLodMissing) ? Entity.NewState.LodStates.LOD_MISSING : Entity.NewState.LodStates.LOD_LOW;

                if (entity.NewHistory.PresentState.LodState != lodState)
                {
                    StreamingVisualize.Model.Entity.NewState state = entity.NewHistory.GetStateForWriting(PresentFrameNumber, this);
                    state.LodState = lodState;
                    entity.NewHistory.ValidateDupe();
                }

                entity.CurrentLodState = lodState;

                Frame frame = frames[frames.Count - 1];
                if (parentLodMissing)
                {
                    frame.AddMissingLodEntity(entity);
                }
                else
                {
                    frame.AddLowLodEntity(entity);
                }

                if (!entity.IsOnWatchList)
                {
                    entity.IsOnWatchList = true;
                    CurrentEntityLodProblems.Add(entity);
                }
            }
        }

        public void UpdateVisFlags(int entityId, int phaseFlags, int optionFlags)
        {
            Entity entity;

            if (CurrentFrame.EntityMap.TryGetValue(entityId, out entity))
            {
                if (entity.NewHistory.PresentState.PhaseFlags != phaseFlags || entity.NewHistory.PresentState.OptionFlags != optionFlags)
                {
                    StreamingVisualize.Model.Entity.NewState state = entity.NewHistory.GetStateForWriting(PresentFrameNumber, this);
                    state.PhaseFlags = phaseFlags;
                    state.OptionFlags = optionFlags;
                    entity.NewHistory.ValidateDupe();
                }
            }
        }

        public void SetExtraClientInfo(String version, String assetType)
        {
            Version = version;
            AssetType = assetType;

            controller.ClientInfoUpdated();
        }

        public void SetClientInfo(String config, String platform, String remoteAddr, String restIP)
        {
            BuildConfiguration = config;
            Platform = platform;
            RemoteAddr = remoteAddr;
            RestIP = restIP;

            controller.ClientInfoUpdated();
        }

        public void SetEntityState(int frameNumber)
        {
            foreach (Entity entity in CurrentFrame.Entities)
            {
                entity.SetToState(frameNumber);
            }
        }

        public void SetStreamableState(int frameNumber)
        {
            lock (AllStreamables)
            {
                foreach (Streamable streamable in AllStreamables)
                {
                    streamable.History.SetToState(frameNumber);
                    streamable.IdentityHistory.SetToState(frameNumber);
                }
            }
        }

        public void RegisterMapData(int strIndex, int mapDataId, Box physicsBox, Box streamingBox, int parentStrIndex)
        {
            MapData mapData;

            if (!MapDatas.ContainsKey(strIndex))
            {
                mapData = new MapData();
                mapData.strIndex = strIndex;
                MapDatas.Add(strIndex, mapData);
            }
            else
            {
                mapData = MapDatas[strIndex];
            }

            MapDataFromId[mapDataId] = mapData;
            mapData.PhysicsBox = physicsBox;
            mapData.StreamingBox = streamingBox;

            Streamable streamable = GetStreamable(strIndex);

            if (MapDataModuleIndex == 0 && streamable != null && streamable.Module != DummyStreamableModule)
            {
                MapDataModuleIndex = streamable.Module.Index;
            }

            if (parentStrIndex != -1)
            {
                MapData parentMap;

                if (MapDatas.TryGetValue(parentStrIndex, out parentMap))
                {
                    if (!parentMap.Children.Contains(mapData))
                    {
                        parentMap.Children.Add(mapData);
                    }
                }
                else
                {
                   // Console.WriteLine("Unknown mapdata parent " + parentStrIndex);
                }
            }
        }

        public void AddCullbox(String name, Box aabb, List<int> mapDatas)
        {
            Cullbox cullbox = new Cullbox();

            cullbox.Name = name;
            cullbox.Extents = aabb;

            foreach (int mapDataStrIndex in mapDatas)
            {
                if (mapDataStrIndex != -1)
                {
                    cullbox.Contents.Add(GetStreamable(mapDataStrIndex));
                }
            }

//            cullbox.
            Cullboxes.Add(cullbox);
            FillCullboxChildren(cullbox);
        }

        public void InstantiateMapData(int mapDataId)
        {
            MapData mapData = MapDataFromId[mapDataId];

            // Create a new state.
            mapData.History.GetStateForWriting(this);
        }
/*
        public void AddVisibleEntities(int[] entityIds)
        {
            foreach (int entityId in entityIds)
            {
                Entity entity;

                if (CurrentFrame.EntityMap.TryGetValue(entityId, out entity))
                {
                    if (entity.NewHistory.PresentState.VisibleFlags != visibleFlag)
                    {
                        entity.NewHistory.GetStateForWriting(this).VisibleFlags = (byte)visibleFlag;
                        entity.NewHistory.ValidateDupe();
                    }
                }
            }
        }

        public void RemoveVisibleEntities(int[] entityIds)
        {
            foreach (int entityId in entityIds)
            {
                Entity entity;

                if (CurrentFrame.EntityMap.TryGetValue(entityId, out entity))
                {
                    int visibleFlag = 0;
                    if (entity.NewHistory.PresentState.VisibleFlags != visibleFlag)
                    {
                        entity.NewHistory.GetStateForWriting(this).VisibleFlags = (byte)visibleFlag;
                        entity.NewHistory.ValidateDupe();
                    }
                }
            }
        }
        */
        public void AddMapDataEntity(int strIndex, int entityId, int entityStrIndex, Box16 entityBox, int lodTypeId, int guid, int entityType, int entityFlags, Vector16 sphereCenter, float sphereRadius, float lodDistance, int parentLodEntityId)
        {
            MapData mapData = MapDatas[strIndex];
            Debug.Assert(mapData.History.PresentState != null);
            Streamable mapDataStreamable = GetStreamable(strIndex);

            Entity entity;

            if (!CurrentFrame.EntityMap.TryGetValue(entityId, out entity))
            {
                entity = AddEntity(entityId, GetStreamable(entityStrIndex), entityBox.BoxMin, entityBox.BoxMax, lodTypeId, mapDataStreamable, guid, entityType, entityFlags, sphereCenter, sphereRadius, lodDistance, parentLodEntityId);
            }

            mapData.History.PresentState.Entities.Add(entity);

            // And the streamable get the IMAP as a virtual dependency.
            Streamable streamable = entity.NewHistory.PresentState.Streamable;

            if (streamable != null)
            {
                if (!streamable.IdentityHistory.PresentState.Dependencies.Contains(mapDataStreamable))
                {
                    streamable.IdentityHistory.PresentState.Dependencies.Add(mapDataStreamable);
                    mapDataStreamable.IdentityHistory.PresentState.Dependents.Add(streamable);
                }

                streamable.IdentityHistory.PresentState.LodTypeId = lodTypeId;
            }

            Entity.NewState state = entity.NewHistory.PresentState;
            if (state.Streamable != streamable || state.GUID != guid || state.MapDataStreamable != mapDataStreamable)
            {
                state = entity.NewHistory.GetStateForWriting(this);
                state.Streamable = streamable;
                state.GUID = guid;
                state.MapDataStreamable = mapDataStreamable;
                state.LodType = (byte)lodTypeId;
                state.EntityFlags = entityFlags;
                entity.NewHistory.ValidateDupe();
            }

            if (!(entity.PositionHistory.PresentState.BoxMin.Equals(entityBox.BoxMin) && entity.PositionHistory.PresentState.BoxMax.Equals(entityBox.BoxMax)))
            {
                Entity.PositionState posState = entity.PositionHistory.GetStateForWriting(this);
                posState.BoxMin = entityBox.BoxMin;
                posState.BoxMax = entityBox.BoxMax;
                posState.AdjustPosition();
                entity.PositionHistory.ValidateDupe();
            }
        }

        public void AddToNeededList(int entityId, float score, int flags)
        {
            NeededList.Entry entry = new NeededList.Entry();
            Entity entity;

            if (!CurrentFrame.EntityMap.TryGetValue(entityId, out entity))
            {
                entity = new Entity();
                entity.EntityId = entityId;
                CurrentFrame.EntityMap.Add(entityId, entity);
                CurrentFrame.Entities.Add(entity);

                // Create the base state to indicate we didn't exist yet at this point.
                entity.NewHistory.GetStateForWriting(0, this).SetDeleted();
                entity.PositionHistory.GetStateForWriting(0, this).SetDeleted();
            }

            entry.entity = entity;
            entry.score = score;
            entry.flags = (byte)flags;

            lock (GetPresentFrame().NeededList)
            {
                GetPresentFrame().NeededList.List.Add(entry);
            }
        }

        public void RegisterEntityType(int entityType, String name, int color, bool visible)
        {
            EntityType type = new EntityType();
            type.SetColor(color);
            type.Name = name;
            type.Visible = visible;

            Debug.Assert(EntityTypes.ContainsKey(entityType) == false, "Registered entity type " + entityType + " twice");

            EntityTypes[entityType] = type;

            controller.OnNewEntityType(entityType, type);
        }

        public void RegisterLodType(int lodType, String name)
        {
            LodType type = new LodType();
            type.Name = name;
            type.LodTypeId = lodType;

            Debug.Assert(LodTypes.ContainsKey(lodType) == false, "Registered LOD type " + lodType + " twice");

            LodTypes[lodType] = type;

            // It's a LOD type, but no need to create two separate hooks for that.
            //controller.OnNewEntityType(entityType, type);
            // HACK
            int firstEntityKey = EntityTypes.Keys.GetEnumerator().Current;
            controller.OnNewEntityType(firstEntityKey, EntityTypes[firstEntityKey]);

        }

        public EntityType GetEntityType(int entityTypeId)
        {
            return EntityTypes[entityTypeId];
        }

        public LodType GetLodType(int lodTypeId)
        {
            LodType result = DummyLodType;

            LodTypes.TryGetValue(lodTypeId, out result);
            return result;
        }

        public void SetMarkerText(float time, String markerText)
        {
            if (GetPresentFrame().MarkerText == null)
            {
                GetPresentFrame().MarkerText = markerText;
            }
            else
            {
                GetPresentFrame().MarkerText += " / " + markerText;
            }

            // Also add an event for this.
            for (int x = 0; x < devices.Length; x++)
            {
                Device device = devices[x];

                if (device != null)
                {
                    DeviceEvent devEvent = new DeviceEvent();
                    devEvent.Streamable = null;
                    devEvent.Action = DeviceEvent.ActionType.MARKER;
                    devEvent.Time = time;
                    devEvent.Frame = PresentFrameNumber;
                    devEvent.Size = 0;
                    devEvent.Handle = 0;
                    devEvent.Marker = markerText;
                    devEvent.LSN = 0;
                    device.DeviceEvents.Add(devEvent);
                }
            }
        }

        public void FailedAllocation(Streamable streamable, int reason)
        {
            Frame frame = GetPresentFrame();
            FailedAlloc failedAlloc = new FailedAlloc();

            failedAlloc.Reason = (FailedAlloc.ReasonType)reason;
            failedAlloc.Streamable = streamable;

            frame.FailedAllocs.Add(failedAlloc);

            switch (failedAlloc.Reason)
            {
                case FailedAlloc.ReasonType.OOM:
                    frame.Flags |= Frame.OOM_NO_MEMORY;
                    break;

                case FailedAlloc.ReasonType.FRAGMENTED:
                    frame.Flags |= Frame.OOM_FRAGMENTED;
                    break;

                case FailedAlloc.ReasonType.CHURN_PROTECTION:
                    frame.Flags |= Frame.OOM_CHURN_PROTECTION;
                    break;
            }
        }

        public void Serialize(VersionedWriter writer)
        {
            long marker = 0;

            writer.scene = this;

            LastSaveSuccess = false;
            ProgressView.SetStatus("Writing file header...", 0);

            writer.Write(BuildConfiguration);
            writer.Write(Platform);
            writer.Write(RemoteAddr);

            // Version 30: Additional information
            writer.Write(Version);
            writer.Write(AssetType);

            Console.WriteLine("Header cost: " + marker);
            marker = writer.BytesWritten;

            if (ProgressView.ShouldAbort())
            {
                return;
            }

            // Version 26: String pool.
            ProgressView.SetStatus("Writing string pool...", 0);
            writer.Write(StringPool);

            Console.WriteLine("String pool: {0:N0}", writer.BytesWritten - marker);
            marker = writer.BytesWritten;

            if (ProgressView.ShouldAbort())
            {
                return;
            }

            ProgressView.SetStatus("Writing modules...", 1);

            writer.WriteSparseArray(modules);

            Console.WriteLine("Modules: {0:N0}", writer.BytesWritten - marker);
            marker = writer.BytesWritten;

            if (ProgressView.ShouldAbort())
            {
                return;
            }

            // Version 35: Visflags.
            VisFlagsDef.Serialize(writer);

            // Version 37: Search types.
            SearchTypes.Serialize(writer);

            ProgressView.SetStatus("Writing streamables...", 2);

            writer.WriteSparseArray(streamables);

            // TODO: We need a count for this.
            int count = 0;
            for (int x = 0; x < streamables.Length; x++)
            {
                if (streamables[x] != null)
                {
                    count++;
                }
            }

            Console.WriteLine("Streamables: {0:N0} ({1:N0} objects)", writer.BytesWritten - marker, count);
            marker = writer.BytesWritten;

            if (ProgressView.ShouldAbort())
            {
                return;
            }

            ProgressView.SetStatus("Writing dummy streamables...", 3);

            writer.Write(DummyStreamableFixupArray);

            Console.WriteLine("Dummy streamable fixup: {0:N0}", writer.BytesWritten - marker);
            marker = writer.BytesWritten;

            if (ProgressView.ShouldAbort())
            {
                return;
            }

            // Version 41: Cull boxes.
            writer.Write(Cullboxes);

            ProgressView.SetStatus("Writing devices...", 4);

            writer.WriteSparseArray(devices);

            Console.WriteLine("Devices: {0:N0}", writer.BytesWritten - marker);
            marker = writer.BytesWritten;

            if (ProgressView.ShouldAbort())
            {
                return;
            }

            ProgressView.SetStatus("Writing meta info...", 5);

            writer.Write(EntityTypes);

            Console.WriteLine("Entity Types: {0:N0}", writer.BytesWritten - marker);
            marker = writer.BytesWritten;

            // Version 20: LOD types.
            writer.Write(LodTypes);
            Console.WriteLine("LOD Types: {0:N0}", writer.BytesWritten - marker);
            marker = writer.BytesWritten;

            writer.WriteCheckpoint();

            if (ProgressView.ShouldAbort())
            {
                return;
            }

            ProgressView.SetStatus("Writing entities...", 6);

            writer.Write(CurrentFrame.Entities);
            writer.WriteCheckpoint();

            Console.WriteLine("Entities: {0:N0} ({1:N0} objects)", writer.BytesWritten - marker, CurrentFrame.Entities.Count);
            marker = writer.BytesWritten;

            if (ProgressView.ShouldAbort())
            {
                return;
            }

            ProgressView.SetStatus("Writing frames...", 7);

            writer.Write(frames);
            writer.WriteCheckpoint();

            Console.WriteLine("Frames: {0:N0}", writer.BytesWritten - marker);
            marker = writer.BytesWritten;

            if (ProgressView.ShouldAbort())
            {
                return;
            }

            ProgressView.SetStatus("Writing map data...", 8);

            // Version 14: Map data.
            writer.Write(MapDatas);
            writer.WriteCheckpoint();

            if (ProgressView.ShouldAbort())
            {
                return;
            }

            ProgressView.SetStatus("Writing additional information...", 9);

            Console.WriteLine("MapDatas: {0:N0} bytes ({1:N0} objects)", writer.BytesWritten - marker, MapDatas.Count);
            marker = writer.BytesWritten;

            // Version 17: Scene data.
            writer.Write(MapDataModuleIndex);
            writer.Write(ArchetypeModuleIndex);

            ProgressView.SetStatus("DONE.", 10);

            LastSaveSuccess = true;
            return;
        }

        public void Deserialize(VersionedReader reader, int version)
        {
            BuildConfiguration = reader.ReadString();
            Platform = reader.ReadString();
            RemoteAddr = reader.ReadString();

            if (version >= 30)
            {
                AssetType = reader.ReadString();
                Version = reader.ReadString();
            }

            if (version >= 26)
            {
                reader.Read(StringPool);
            }

            reader.ReadSparseArray(modules);

            // Set the indices so we can fix it up again.
            for (int x = 0; x < modules.Length; x++)
            {
                if (modules[x] != null)
                {
                    modules[x].Index = x;
                    maxModuleIndex = x + 1;
                }
            }

            if (version >= 35)
            {
                VisFlagsDef.Deserialize(reader, version);
            }
            else
            {
                VisFlagsDef.CreateCompatibilitySet();
            }

            if (version >= 37)
            {
                SearchTypes.Deserialize(reader, version);
            }



            reader.ReadSparseArray(streamables);
            reader.HandleDelayedRefs();
            reader.ReadList(DummyStreamableFixupArray);

            if (version >= 41)
            {
                reader.ReadList(Cullboxes);
            }

            reader.ReadSparseArray(devices);
            reader.ReadIntDictionary(EntityTypes);

            if (version >= 20)
            {
                reader.ReadIntDictionary(LodTypes);

                foreach (int key in LodTypes.Keys)
                {
                    LodTypes[key].LodTypeId = key;
                }
            }
            reader.VerifyCheckpoint();

            reader.ReadList(CurrentFrame.Entities);
            reader.VerifyCheckpoint();

            // Populate our entity map.
            if (version >= 19)
            {
                foreach (Entity entity in CurrentFrame.Entities)
                {
                    CurrentFrame.EntityMap[entity.EntityId] = entity;
                }
            }

            reader.ReadList(frames);
            reader.VerifyCheckpoint();

            if (version >= 14)
            {
                reader.ReadIntDictionary(MapDatas);
                reader.VerifyCheckpoint();
            }

            if (version >= 17)
            {
                MapDataModuleIndex = reader.ReadInt();
                ArchetypeModuleIndex = reader.ReadInt();
            }

            // We need to connect the dots still.
            for (int x = 0; x < DummyStreamableFixupArray.Count; x++)
            {
                Streamable streamable = DummyStreamableFixupArray[x];
                for (int y = 0; y < streamable.IdentityHistory.StateCount; y++)
                {
                    String Name = streamable.IdentityHistory.GetStateByIndex(y).Name;

                    DummyStreamables[Name] = streamable;
                    StreamableLookup[Name] = streamable;
                }

                AllStreamables.Add(streamable);
                StrIndexToIndex[streamable.StrIndex] = streamable.Index;
            }

            for (int x = 0; x < streamables.Length; x++)
            {
                if (streamables[x] != null)
                {
                    AllStreamables.Add(streamables[x]);

                    if (StrIndexToIndex.ContainsKey(streamables[x].StrIndex))
                    {
                        Console.WriteLine("ERROR - index " + streamables[x].StrIndex + " used twice");
                    }
                    StrIndexToIndex[streamables[x].StrIndex] = x;
                }
            }

            foreach (Streamable streamable in AllStreamables)
            {
                for (int y = 0; y < streamable.IdentityHistory.StateCount; y++)
                {
                    Streamable.Identity identity = streamable.IdentityHistory.GetStateByIndex(y);
                    String Name = identity.Name;
                    StreamableLookup[Name] = streamable;
                }
            }

            for (int x = 0; x < frames.Count; x++)
            {
                if (GetFrame(x).HasMarker)
                {
                    AddMarker(x);
                }
            }

            maxDeviceIndex = 0;
            for (int x = 0; x < devices.Length; x++)
            {
                if (devices[x] != null)
                {
                    maxDeviceIndex = x + 1;
                }
            }

            // Populate the bad LOD states.
            foreach (Entity entity in CurrentFrame.Entities)
            {
                for (int z = 0; z < entity.NewHistory.StateCount; z++)
                {
                    Entity.NewState state = entity.NewHistory.GetStateByIndex(z);

                    if (state.LodState != Entity.NewState.LodStates.LOD_OK)
                    {
                        // Here's one.
                        int lastFrame = (z < entity.NewHistory.StateCount - 1) ? entity.NewHistory.GetStateByIndex(z + 1).Frame : frames.Count;

                        for (int a = state.Frame; a < lastFrame; a++)
                        {
                            Frame frame = frames[a];

                            if (frame.BadLodEntities == null)
                            {
                                frame.BadLodEntities = new List<Entity>();
                            }

                            frame.BadLodEntities.Add(entity);
                        }
                    }
                }
            }

            // Retrofit with a req counter.
            if (version < 40)
            {
                ComputeReqs();
            }

            CreateMapDataHierarchy();
            FillAllCullboxChildren();

            // Finally, make sure the current states are actually current.
            SetEntityState(0);
            SetStreamableState(0);
            CurrentFrameNumber = 0;
        }

        private void ComputeReqs()
        {
            foreach (Streamable streamable in streamables)
            {
                if (streamable != null)
                {
                    int startReqFrame = 0;
                    bool reqState = false;
                    bool finalReqState = false;
                    bool isDummy = streamable.VirtSize > 0 || streamable.PhysSize > 0;

                    int stateCount = streamable.History.StateCount;

                    for (int x = 0; x < stateCount; x++)
                    {
                        Streamable.State state = streamable.History.GetStateByIndex(x);
                        finalReqState = state.Status == Streamable.Status.LOADREQUESTED || state.Status == Streamable.Status.LOADING;

                        if (finalReqState != reqState)
                        {
                            int stateFrame = state.GetFrameNumber();
                            if (finalReqState)
                            {
                                startReqFrame = stateFrame;
                            }
                            else
                            {
                                for (int y = startReqFrame; y < stateFrame; y++)
                                    frames[y].Reqs++;

                                if (!isDummy)
                                {
                                    for (int y = startReqFrame; y < stateFrame; y++)
                                        frames[y].RealReqs++;
                                }
                            }

                            reqState = finalReqState;
                        }
                    }

                    if (finalReqState)
                    {
                        for (int y = startReqFrame; y < frames.Count - 1; y++)
                        {
                            frames[y].Reqs++;
                        }

                        if (!isDummy)
                        {
                            for (int y = startReqFrame; y < frames.Count - 1; y++)
                            {
                                frames[y].RealReqs++;
                            }
                        }
                    }
                }
            }
        }

        public IList<Module> CreateModuleList()
        {
            List<Module> result = new List<Module>();

            foreach (Module module in modules)
            {
                if (module != null)
                {
                    result.Add(module);
                }
            }

            result.Add(DummyStreamableModule);
            return result;
        }

        public void LowLevelOpen(long handle, float time, String filename)
        {
            // TODO: The map might point to the streamable instead
            Debug.Assert(LowLevelHandleLookup.ContainsKey(handle) == false, "Handle " + handle + " used twice");
            LowLevelHandleLookup.Add(handle, filename);

            Device device = GetDevice(0);   // TODO: Need device
            Streamable streamable = GetOrCreateDummyStreamable(LowLevelHandleLookup[handle], 0);

            DeviceEvent devEvent = new DeviceEvent();
            devEvent.Streamable = streamable;
            devEvent.Time = time;
            devEvent.Frame = PresentFrameNumber;
            devEvent.LSN = 0;
            devEvent.Action = DeviceEvent.ActionType.OPEN;
            device.DeviceEvents.Add(devEvent);
        }

        private int ComputeLsn(long offset, Streamable streamable)
        {
            return streamable.LSN + (int)(offset / 2048U);  // TODO: We need a per-device block size
        }

        public void LowLevelRead(long handle, float time, int size, int streamerHandle, int queue, long offset)
        {
            if (LowLevelHandleLookup.ContainsKey(handle) == false)
            {
                // TODO: Don't disregard
                return;
            }

            Debug.Assert(LowLevelHandleLookup.ContainsKey(handle) == true, "Handle " + handle + " does not exist");

            Device device = GetDevice(queue);
            Streamable streamable = GetOrCreateDummyStreamable(LowLevelHandleLookup[handle], 0);

            DeviceEvent devEvent = new DeviceEvent();
            devEvent.Streamable = streamable;
            devEvent.Time = time;
            devEvent.Frame = PresentFrameNumber;
            devEvent.LSN = ComputeLsn(offset, streamable);
            devEvent.Size = size;
            devEvent.Handle = streamerHandle;
            devEvent.Action = DeviceEvent.ActionType.LOWREAD;
            device.DeviceEvents.Add(devEvent);
        }

        public void LowLevelReadEnd(long handle, float time, int streamerHandle, int queue)
        {
            if (LowLevelHandleLookup.ContainsKey(handle) == false)
            {
                // TODO: Don't disregard
                return;
            }

            Debug.Assert(LowLevelHandleLookup.ContainsKey(handle) == true, "Handle " + handle + " does not exist");

            Device device = GetDevice(queue);
            Streamable streamable = GetOrCreateDummyStreamable(LowLevelHandleLookup[handle], 0);

            DeviceEvent devEvent = new DeviceEvent();
            devEvent.Streamable = streamable;
            devEvent.Time = time;
            devEvent.Frame = PresentFrameNumber;
            devEvent.Handle = streamerHandle;
            devEvent.Action = DeviceEvent.ActionType.LOWREADEND;
            device.DeviceEvents.Add(devEvent);
        }

        public void LowLevelSeek(long handle, float time, long offset, int streamerHandle, int queue)
        {
            if (LowLevelHandleLookup.ContainsKey(handle) == false)
            {
                // TODO: Don't disregard
                return;
            }

            Debug.Assert(LowLevelHandleLookup.ContainsKey(handle) == true, "Handle " + handle + " does not exist");

            Device device = GetDevice(queue);
            Streamable streamable = GetOrCreateDummyStreamable(LowLevelHandleLookup[handle], 0);

            DeviceEvent devEvent = new DeviceEvent();
            devEvent.Streamable = streamable;
            devEvent.Time = time;
            devEvent.Frame = PresentFrameNumber;
            devEvent.LSN = (int)offset;
            devEvent.Size = 0;
            devEvent.Handle = streamerHandle;
            devEvent.Action = DeviceEvent.ActionType.LOWSEEK;
            device.DeviceEvents.Add(devEvent);
        }

        public void LowLevelWrite(long handle, float time, int size, int streamerHandle, int queue)
        {
            if (LowLevelHandleLookup.ContainsKey(handle) == false)
            {
                // TODO: Don't disregard
                return;
            }

            Debug.Assert(LowLevelHandleLookup.ContainsKey(handle) == true, "Handle " + handle + " does not exist");

            Device device = GetDevice(queue);
            Streamable streamable = GetOrCreateDummyStreamable(LowLevelHandleLookup[handle], 0);

            DeviceEvent devEvent = new DeviceEvent();
            devEvent.Streamable = streamable;
            devEvent.Action = DeviceEvent.ActionType.LOWWRITE;
            devEvent.Time = time;
            devEvent.Frame = PresentFrameNumber;
            devEvent.Size = size;
            devEvent.Handle = streamerHandle;
            devEvent.LSN = 0;
            device.DeviceEvents.Add(devEvent);
        }

        public void DeviceIdle(float time, int queue)
        {
            Device device = GetDevice(queue);

            DeviceEvent devEvent = new DeviceEvent();
            devEvent.Streamable = null;
            devEvent.Action = DeviceEvent.ActionType.IDLE;
            devEvent.Time = time;
            devEvent.Frame = PresentFrameNumber;
            devEvent.Size = 0;
            devEvent.Handle = 0;
            devEvent.LSN = 0;
            device.DeviceEvents.Add(devEvent);
        }

        public void LowLevelClose(long handle, float time)
        {
            if (LowLevelHandleLookup.ContainsKey(handle) == false)
            {
                // TODO: Don't disregard
                return;
            }

            Debug.Assert(LowLevelHandleLookup.ContainsKey(handle) != false, "Handle " + handle + " does not exist");

            Device device = GetDevice(0);   // TODO: Need device
            Streamable streamable = GetOrCreateDummyStreamable(LowLevelHandleLookup[handle], 0);

            DeviceEvent devEvent = new DeviceEvent();
            devEvent.Streamable = streamable;
            devEvent.Action = DeviceEvent.ActionType.CLOSE;
            devEvent.Time = time;
            devEvent.Frame = PresentFrameNumber;
            devEvent.LSN = 0;
            device.DeviceEvents.Add(devEvent);

            LowLevelHandleLookup.Remove(handle);
        }

        private String[] CreateEmptyColumn(String[] src)
        {
            String[] result = new String[src.Length];

            for (int x = 0; x < result.Length; x++)
            {
                result[x] = "";
            }

            return result;
        }

        private String[] CreateDiffs(String[] baseline, String[] current)
        {
            String[] result = new String[baseline.Length];

            for (int x = 0; x < result.Length; x++)
            {
                float baseValue, currentValue;
                bool hasValue = false;

                if (Single.TryParse(baseline[x], out baseValue))
                {
                    if (Single.TryParse(current[x], out currentValue))
                    {
                        result[x] = String.Format("{0:F2}", currentValue - baseValue);
                        hasValue = true;
                    }
                }

                if (!hasValue)
                {
                    if (x == 0)
                    {
                        result[x] = "DIFF: " + current[x];
                    }
                    else
                    {
                        result[x] = current[x];
                    }
                }
            }

            return result;
        }

        private String[] CreateAverages(List<String[]> src)
        {
            int rows = src[0].Length;
            int cols = src.Count;

            String[] result = new String[rows];
            float Countf = (float) cols;

            for (int x = 0; x < rows; x++)
            {
                // Can we get the average here?
                float avg = 0.0f;
                bool isValue = true;

                for (int y = 0; y < cols; y++)
                {
                    float val;
                        
                    if (Single.TryParse(src[y][x], out val))
                    {
                        avg += val;
                    }
                    else
                    {
                        // This is a header or something.
                        isValue = false;
                        break;
                    }
                }

                if (isValue)
                {
                    result[x] = String.Format("{0:F2}", avg / Countf);
                }
                else
                {
                    if (x == 0)
                    {
                        result[x] = "AVG: " + src[0][x];
                    }
                    else
                    {
                        result[x] = src[0][x];
                    }
                }
            }

            return result;
        }

        public String AnalyzeAllTests(ProgressView progress)
        {
            // Find every test instance.
            // Skip the first frame, there's too much going on there.
            int FirstFrame = 1;

            List<String[]> MasterResults = new List<String[]>();

            List<String[]> Results = new List<String[]>();

            List<String[]> ConfigurationResults = new List<String[]>();

            Analysis DummyAnalysis = new Analysis();
            DummyAnalysis.Scene = this;

            Results.Add(DummyAnalysis.CreateHeaderColumn());
            MasterResults.Add(DummyAnalysis.CreateHeaderColumn());

            while (FirstFrame < FrameCount)
            {
                if ((GetFrame(FirstFrame).Flags & Frame.BEGIN_TEST) != 0)
                {
                    if (progress.ShouldAbort())
                    {
                        break;
                    }

                    // We found the beginning of a test. Let's find the end.
                    int EndFrame = FirstFrame;

                    while (EndFrame < FrameCount)
                    {
                        if ((GetFrame(EndFrame).Flags & Frame.BEGIN_TEST) != 0)
                        {
                            // Another BEGIN_TEST which supersedes the previous one.
                            FirstFrame = EndFrame;
                        }

                        if ((GetFrame(EndFrame).Flags & (Frame.END_TEST | Frame.TEST_NEW_CONFIG)) != 0)
                        {
                            if (FirstFrame != EndFrame)
                            {
                                // Found it. Now let's get the test name.
                                Analysis Analysis = new Analysis();

                                Analysis.FirstFrame = FirstFrame;
                                Analysis.LastFrame = EndFrame;
                                Analysis.Scene = this;

                                String MarkerText = GetFrame(EndFrame).MarkerText;

                                if (MarkerText != null)
                                {
                                    Analysis.TestName = MarkerText;
                                }

                                MarkerText = GetFrame(FirstFrame).MarkerText;

                                if (MarkerText != null)
                                {
                                    Analysis.TestName = MarkerText;
                                }

                                progress.SetStatus(Analysis.TestName, FirstFrame);

                                Analysis.Analyze();
                                String[] column = Analysis.CreateCsvColumn();
                                Results.Add(column);
                                ConfigurationResults.Add(column);
                            }

                            if ((GetFrame(EndFrame).Flags & Frame.TEST_NEW_CONFIG) != 0)
                            {
                                if (ConfigurationResults.Count != 0)
                                {
                                    // We're done with one setup. Create averages, add them to the master table.
                                    String[] averages = CreateAverages(ConfigurationResults);
                                    Results.Add(CreateEmptyColumn(averages));
                                    Results.Add(averages);
                                    Results.Add(CreateEmptyColumn(averages));
                                    MasterResults.Add(averages);

                                    if (MasterResults.Count > 2)
                                    {
                                        MasterResults.Add(CreateDiffs(MasterResults[1], MasterResults[MasterResults.Count - 1]));
                                    }

                                    ConfigurationResults.Clear();
                                }
                            }


                            FirstFrame = EndFrame + 1;
                            break;
                        }

                        EndFrame++;
                    }
                }

                FirstFrame++;
            }

            progress.SetStatus("Creating CSV Data...", FrameCount - 2);

            if (ConfigurationResults.Count != 0)
            {
                // We're done with one setup. Create averages, add them to the master table.
                String[] averages = CreateAverages(ConfigurationResults);
                Results.Add(CreateEmptyColumn(averages));
                Results.Add(averages);
                MasterResults.Add(averages);

                if (MasterResults.Count > 2)
                {
                    MasterResults.Add(CreateDiffs(MasterResults[1], MasterResults[MasterResults.Count - 1]));
                }

                ConfigurationResults.Clear();
            }

            // Collate the results.
            if (Results.Count == 1)
            {
                return "No tests found";
            }

            StringBuilder FinalCsv = new StringBuilder(65536);

            for (int line = 0; line < Results[0].Length; line++)
            {
                for (int col = 0; col < Results.Count; col++)
                {
                    FinalCsv.Append(Results[col][line]);
                    FinalCsv.Append('\t');
                }

                FinalCsv.Append('\n');
            }

            // Now the master summary, if there is one.
            if (MasterResults.Count > 0)
            {
                FinalCsv.Append("\nAVERAGES\n");
            }

            for (int line = 0; line < MasterResults[0].Length; line++)
            {
                for (int col = 0; col < MasterResults.Count; col++)
                {
                    FinalCsv.Append(MasterResults[col][line]);
                    FinalCsv.Append('\t');
                }

                FinalCsv.Append('\n');
            }

            progress.SetStatus("Done.", FrameCount);

            return FinalCsv.ToString();
        }

        public IList<Cullbox> GetCullboxesAt(Vector point)
        {
            List<Cullbox> result = new List<Cullbox>();

            foreach (Cullbox cullbox in Cullboxes)
            {
                if (cullbox.Contains(point))
                {
                    result.Add(cullbox);
                }
            }

            return result;
        }

        public IList<Cullbox> GetCullboxesCullingMap(IList<Cullbox> cullboxes, MapData mapData)
        {
            List<Cullbox> result = new List<Cullbox>();
            Streamable mapStreamable = GetStreamable(mapData.strIndex);

            foreach (Cullbox cullbox in cullboxes)
            {
                if (cullbox.Culls(mapStreamable))
                {
                    result.Add(cullbox);
                }
            }

            return result;
        }

        public void DeletePriorHistory(int frame)
        {
            foreach (Entity entity in CurrentFrame.Entities)
            {
                entity.DeletePriorHistory(frame);
            }

            for (int x = 0; x < frame; x++)
            {
                frames[x].DeleteData();
            }

            if (frames[frame].MarkerText == null)
            {
                frames[frame].MarkerText = "PRIOR HISTORY DELETED";
            }
            else
            {
                frames[frame].MarkerText = "PRIOR HISTORY DELETED / " + frames[frame].MarkerText;
            }
        }

        public void CreateMapDataHierarchy()
        {
            foreach (MapData mapdata in MapDatas.Values)
            {
                if (mapdata.ParentStrIndex != -1)
                {
                    MapDatas[mapdata.ParentStrIndex].Children.Add(mapdata);
                }
            }
        }

        public void AddMapDataChildrenRecursively(List<Streamable> mapDataList, MapData mapData)
        {
            Streamable mapStreamable = GetStreamable(mapData.strIndex);

            if (!mapDataList.Contains(mapStreamable))
            {
                mapDataList.Add(mapStreamable);

                foreach (MapData child in mapData.Children)
                {
                    AddMapDataChildrenRecursively(mapDataList, child);
                }
            }
        }

        public void FillCullboxChildren(Cullbox cullbox)
        {
            List<Streamable> fullList = new List<Streamable>();

            foreach (Streamable streamable in cullbox.Contents)
            {
                AddMapDataChildrenRecursively(fullList, MapDatas[streamable.StrIndex]);
            }


            cullbox.Contents = fullList;
        }

        public void FillAllCullboxChildren()
        {
            foreach (Cullbox cullbox in Cullboxes)
            {
                FillCullboxChildren(cullbox);
            }
        }
    }
}
