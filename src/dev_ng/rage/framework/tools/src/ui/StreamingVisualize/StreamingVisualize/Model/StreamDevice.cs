﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StreamingVisualize.Model
{
    public class StreamDevice : IVersionedSerializable
    {
        public const int DEFAULT_BLOCK_SIZE = 2048;

        public String Name { get; set; }

        public int BlockSize { get; set; }

//        public PerFrameState<DeviceStreamingState> StreamingState = new PerFrameState<DeviceStreamingState>();


        public void Serialize(VersionedWriter writer)
        {
            writer.Write(Name);
            writer.Write(BlockSize);

            // File version 2: Streaming state
  //          writer.Write(StreamingState);
        }

        public void Deserialize(VersionedReader reader, int version)
        {
            Name = reader.ReadString();

            // File version 2: Streaming state
            if (version > 1)
            {
//                reader.ReadPerFrameState(StreamingState);
            }

            BlockSize = reader.ReadInt();
        }
    }
}
