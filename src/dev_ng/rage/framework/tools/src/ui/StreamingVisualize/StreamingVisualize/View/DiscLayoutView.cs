﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StreamingVisualize.Controller;
using StreamingVisualize.Model;
using StreamingVisualize.View.Components;
using System.Diagnostics;

namespace StreamingVisualize.View
{
    public partial class DiscLayoutView : UserControl, IViewController
    {
        private MainController Controller;
        private Scene Scene;
        private ViewContextMenu viewContextMenu = new ViewContextMenu();
        private SimpleTooltip tooltip = new SimpleTooltip();

        // Height of a bar in pixels
        private const int BarHeight = 12;

        // Number of pixels spacing between each bar
        private const int Spacing = 2;

        // Minimum visible size of bar elements that are considered important, others on top of it might
        // be pushed back.
        private const int MinVisibleWidth = 3;

        private const int NO_MOUSE = 0x7fffffff;

        private int LastMouseX = NO_MOUSE;
        private int LastMouseY;

        private bool IsDragging = false;
        private Stopwatch DragTimer = new Stopwatch();

        public const float TimeSpan = 0.3f;
        public const float Lines = 10.0f;

        public const float TimeSeparator = TimeSpan / Lines;


        private int MouseFrameNumber;

        private int CurrentFrameNumber;

        private int DragStartX;
        private float DragStartOffset;

        private float ZoomFactor = 2.0f;

        private Brush LabelBrush = new SolidBrush(Color.Black);
        private Pen RectangleBorder = new Pen(Color.Black, 1.0f);
        private StringFormat barTextFormat = new StringFormat();

        private Streamable HoverStreamable = null;
        private bool ChangingSelection = false;

        private Brush[] OperationBrushes = new Brush[] {
            new SolidBrush(Color.LightGreen),
            new SolidBrush(Color.LightSalmon),
            new SolidBrush(Color.LightYellow),
            new SolidBrush(Color.LightBlue),
            new SolidBrush(Color.LightCoral),
            new SolidBrush(Color.White),
            new SolidBrush(Color.LightSkyBlue),
            new SolidBrush(Color.LightCyan),
            new SolidBrush(Color.LightPink),
            new SolidBrush(Color.LightGray),
            new SolidBrush(Color.LightSlateGray),
            new SolidBrush(Color.Orange),
            new SolidBrush(Color.Lime),
        };

        private Brush PrioReqBrush = new SolidBrush(Color.Red);

        private Brush FrameBg = new SolidBrush(Color.LightGray);

        private Font TrackLabel = new Font("Tahoma", 12.0f, FontStyle.Bold);
        private Font FrameLabel = new Font("Tahoma", 36.0f, FontStyle.Bold);

        private Brush FrameLabelBrush = new SolidBrush(Color.DarkSlateGray);
        private Brush TrackLabelBrush = new SolidBrush(Color.Black);

        private Brush SeekBrush = new SolidBrush(Color.Purple);
        private Brush BackSeekBrush = new SolidBrush(Color.Red);
        private Brush ReadBrush = new SolidBrush(Color.Green);

        class LayoutView
        {
            public int DeviceEventIndex = 0;

            public int DeviceIndex;

            private int Offset = 0;



            public void UpdateFirstDeviceEventIndex(Scene scene, float time)
            {
                Device device = scene.GetDevice(DeviceIndex);
                int startIndex = DeviceEventIndex;

                if (device.DeviceEvents.Count == 0)
                {
                    DeviceEventIndex = -1;
                    return;
                }

                if (startIndex >= device.DeviceEvents.Count)
                {
                    startIndex = device.DeviceEvents.Count - 1;
                }

                while (startIndex > 0)
                {
                    if (device.DeviceEvents[startIndex - 1].Time > time)
                    {
                        startIndex--;
                    }
                    else
                    {
                        break;
                    }
                }

                while (startIndex + 1 < device.DeviceEvents.Count && device.DeviceEvents[startIndex].Time < time)
                {
                    startIndex++;
                }

                DeviceEventIndex = startIndex;
            }

            private void RenderFileList(Graphics g, StringBuilder fileList, Scene scene, int yPos, DiscLayoutView view, StringBuilder tooltipText, float startTime, int frame)
            {
                if (view.LastMouseY >= yPos && view.LastMouseY < yPos + DiscLayoutView.BarHeight)
                {
                    tooltipText.Append(String.Format("{0}\n", scene.GetDevice(DeviceIndex).Name));
                    tooltipText.Append(String.Format("{0:F2}s-{1:F2}s\n", startTime, startTime + DiscLayoutView.TimeSeparator));
                    tooltipText.Append(String.Format("LSN: {0:X}\n", GetLsn(view, view.LastMouseX)));
                }

                if (fileList.Length < 1)
                {
                    return;
                }

                g.DrawString(String.Format("{0}: {1}", frame, fileList.ToString()), view.Font, view.LabelBrush, 10, yPos);
            }

            private long GetXPos(DiscLayoutView view, int lsn)
            {
//                return (int)(((lsn & 0x3fffffff) - Offset) / view.ZoomFactor);

                float localLsn = (float)((lsn & 0x3fffffff) - Offset);
                return (long) Math.Pow(Math.Abs(localLsn), 1.0f / view.ZoomFactor) * Math.Sign(localLsn) + view.Width / 2;
            }

            private int GetLsn(DiscLayoutView view, int xPos)
            {
//                return (int)(((float)xPos * view.ZoomFactor) + Offset);
                float localPos = (float)(xPos - view.Width / 2);
                return (int)((Math.Pow(Math.Abs(localPos), view.ZoomFactor) * Math.Sign(localPos)) + Offset);
            }

            private Streamable GetRealStreamable(Device device, int devEventIndex)
            {
                // Find out which actual file that is.
                int handle = device.DeviceEvents[devEventIndex].Handle;

                while (--devEventIndex >= 0)
                {
                    DeviceEvent queueEvent = device.DeviceEvents[devEventIndex];
                    if (queueEvent.Action == DeviceEvent.ActionType.QUEUE && queueEvent.Handle == handle)
                    {
                        return queueEvent.Streamable;
                    }
                }

                return null;
            }

            private void RenderBar(Graphics g, DiscLayoutView view, Brush brush, int yPos, int lsnStart, int lsnCount, StringBuilder tooltipText, DeviceEvent devEvent, Scene scene, int devEventIndex, bool seek)
            {
                long x1 = GetXPos(view, lsnStart);
                long x2 = GetXPos(view, lsnStart + lsnCount);

                if (x1 < 0 && x2 < 0)
                {
                    return;
                }

                if (x1 >= view.Width && x2 >= view.Width)
                {
                    return;
                }

                x1 = Math.Max(x1, 0);
                x2 = Math.Min(x2, view.Width);

                if (/*view.LastMouseX >= x1 && view.LastMouseX <= x2 &&*/ view.LastMouseY >= yPos && view.LastMouseY <= yPos + DiscLayoutView.BarHeight / 2)
                {
                    if (seek)
                    {
                        tooltipText.Append("SEEKING TO: ");
                    }

                    Streamable streamable = GetRealStreamable(scene.GetDevice(DeviceIndex), devEventIndex);
                    tooltipText.Append(String.Format("{0}, LSN {1:X}-{2:X} at {3:F2}s\n",streamable.GetNameAt(devEvent.Frame),
                        lsnStart & 0x3fffffff, (lsnStart & 0x3fffffff) + lsnCount, devEvent.Time));
                }

                g.FillRectangle(brush, x1, yPos, x2 - x1 + 1, DiscLayoutView.BarHeight / 2);
            }

            public int RenderDevice(Graphics g, Scene scene, int yPos, float time, DiscLayoutView view, StringBuilder tooltipText)
            {
                Device device = scene.GetDevice(DeviceIndex);

                UpdateFirstDeviceEventIndex(scene, time);

                // Center around the LSN at this point.
                int testIndex = DeviceEventIndex;

                while (testIndex >= 0)
                {
                    if (device.DeviceEvents[testIndex].Action == DeviceEvent.ActionType.LOWREAD)
                    {
                        int lsn = device.DeviceEvents[testIndex].LSN;
                        lsn &= 0x3fffffff;

//                        Offset = lsn - ((float)view.Width * 0.5f * view.ZoomFactor);
                        Offset = lsn;
                        break;
                    }

                    testIndex--;
                }

                // Draw the "grid". Man, this display sucks.
                int color = 1;
                int height = (BarHeight + Spacing) * (((int) DiscLayoutView.Lines) + 3);


                for (int gridLsn = 0; gridLsn < 0x100000; gridLsn += 0x10000)
                {
                    int red = ((color & 4) != 0) ? 255 : 0;
                    int green = ((color & 2) != 0) ? 255 : 0;
                    int blue = ((color & 1) != 0) ? 255 : 0;

                    SolidBrush brush = new SolidBrush(Color.FromArgb(255, red, green, blue));

                    long xPos = GetXPos(view, gridLsn);
                    g.FillRectangle(brush, xPos, yPos, 1, height - BarHeight);

                    color++;
                }


                float curTime = time - DiscLayoutView.TimeSpan;
                float nextLineTime = time - (DiscLayoutView.TimeSpan - DiscLayoutView.TimeSeparator);

                UpdateFirstDeviceEventIndex(scene, curTime);
                StringBuilder fileList = new StringBuilder(256);
                int lastLSN = 0;
                Streamable lastStreamable = null;
                int lastFrame = 0;

                if (DeviceEventIndex != -1)
                {
                    int barY = yPos + ((((int) DiscLayoutView.Lines) + 1) * BarHeight + Spacing);
                    int eventIndex = DeviceEventIndex;

                    while (eventIndex < device.DeviceEvents.Count)
                    {
                        DeviceEvent devEvent = device.DeviceEvents[eventIndex];

                        if (devEvent.Time > time)
                        {
                            break;
                        }

                        while (devEvent.Time > nextLineTime)
                        {
                            RenderFileList(g, fileList, scene, barY, view, tooltipText, nextLineTime - DiscLayoutView.TimeSeparator, lastFrame);
                            fileList.Clear();
                            lastStreamable = null;
                            lastFrame = 0;
                            nextLineTime += DiscLayoutView.TimeSeparator;
                            barY -= BarHeight + Spacing - 2;
                        }

                        if (devEvent.Action == DeviceEvent.ActionType.LOWREAD)
                        {
                            if (lastLSN != 0 && lastLSN != devEvent.LSN)
                            {
                                int lsn1 = lastLSN;
                                int lsn2 = devEvent.LSN;
                                int lsnDist = Math.Abs(lsn2 - lsn1);
                                bool backwardSeek = (lastLSN > devEvent.LSN);

                                RenderBar(g, view, backwardSeek ? view.BackSeekBrush : view.SeekBrush, barY + ((backwardSeek) ? 0 : 1), Math.Min(lsn1, lsn2), lsnDist, tooltipText, devEvent, scene, eventIndex, true);
                            }

                            lastLSN = devEvent.LSN;

                            if (lastFrame == 0)
                            {
                                lastFrame = devEvent.Frame;
                            }

                            Streamable curStreamable = GetRealStreamable(device, eventIndex);
                            if (lastStreamable != curStreamable)
                            {
                                fileList.Append(curStreamable.GetNameAt(devEvent.Frame));
                                fileList.Append("  ");

                                lastStreamable = curStreamable;
                            }

                            RenderBar(g, view, view.ReadBrush, barY + DiscLayoutView.BarHeight / 2, lastLSN, (devEvent.Size + 2047) / 2048, tooltipText, devEvent, scene, eventIndex, false);
                            lastLSN += (devEvent.Size + 2047) / 2048;
                        }

                        eventIndex++;
                    }

                    RenderFileList(g, fileList, scene, barY, view, tooltipText, nextLineTime - DiscLayoutView.TimeSeparator, lastFrame);
                }


                return height;
            }



        }


        LayoutView[] LayoutViews = new LayoutView[16];




        public DiscLayoutView()
        {
            barTextFormat.Trimming = StringTrimming.EllipsisCharacter;
            InitializeComponent();
        }

        // Switch the display to focus on a certain frame
        public void SetToFrame(int frameNumber)
        {
            if (!ChangingSelection)
            {
                // Get the beginning of this frame.
                float frameStart = Scene.GetFrame(frameNumber).Time;

                CurrentFrameNumber = frameNumber;

//                Offset = frameStart;
                UpdateScrollbar();
                UpdateScrollbarPos();
                Refresh();
            }
        }

        // A new device was registered, or a device has changed.
        public void OnDeviceChanged(int deviceIndex, Device device) { }

        // A streamable has been selected
        public void OnSelectStreamable(Streamable streamable)
        {
        }

        // The client information has been updated
        public void ClientInfoUpdated() { }

        // We're in follow mode, and a new frame has been added.
        public void OnNewFrame(int frameNumber) { }

        // A new entity type was registered
        public void OnNewEntityType(int entityTypeId, EntityType type) { }

        // The connection has been terminated
        public void OnConnectionLost() { }

        // Register this view with the system
        public void Register(MainController controller, Scene scene)
        {
            this.Controller = controller;
            this.Scene = scene;

  //          PopulateList();
            UpdateScrollbar();
        }

        public void OnFilterChanged()
        {
//            PopulateList();
            Refresh();
        }

        public void OnFrameRangeChanged(int firstFrame, int lastFrame)
        {
            Refresh();
        }

        public void OnSelectEntity(Entity entity) { }

        private void RecreateContextMenu()
        {
            ContextMenu menu = viewContextMenu.CreateContextMenu(Controller, this);

            menu.MenuItems.Add(ContextMenuHelper.CreateActionMenu("Mark First", MarkFirst));
            menu.MenuItems.Add(ContextMenuHelper.CreateActionMenu("Mark Last", MarkLast));

//            PopulateContextMenu(menu);
            this.ContextMenu = menu;
        }

        private void MarkFirst()
        {
            if (MouseFrameNumber != -1)
            {
                Scene.SelectedFirstFrame = MouseFrameNumber;
                Scene.SelectedLastFrame = Math.Max(Scene.SelectedLastFrame, Scene.SelectedFirstFrame);
                Controller.OnFrameRangeChanged(Scene.SelectedFirstFrame, Scene.SelectedLastFrame);
            }
        }

        private void MarkLast()
        {
            if (MouseFrameNumber != -1)
            {
                Scene.SelectedLastFrame = MouseFrameNumber;
                Scene.SelectedFirstFrame = Math.Min(Scene.SelectedFirstFrame, Scene.SelectedLastFrame);
                Controller.OnFrameRangeChanged(Scene.SelectedFirstFrame, Scene.SelectedLastFrame);
            }
        }

        private void DiscLayoutView_Load(object sender, EventArgs e)
        {
            RecreateContextMenu();
        }

        private static int GetFirstEvent(Device device, int frame, DeviceEvent.LayerType layerType)
        {
            int result = frame;

            // Search backwards first.
            while (frame > 0)
            {
                frame--;

                if (device.DeviceEvents[frame].Layer == layerType)
                {
                    return frame;
                }
            }

            // Forward then.
            while (frame < device.DeviceEvents.Count)
            {
                if (device.DeviceEvents[frame].Layer == layerType)
                {
                    return frame;
                }

                frame++;
            }

            return -1;
        }

        private Brush GetBrushFromHandle(int handle)
        {
            return OperationBrushes[(handle ^ (handle >> 6)) % OperationBrushes.Length];
        }

        private void DiscLayoutView_Paint(object sender, PaintEventArgs e)
        {
            if (Scene != null)
            {
                StringBuilder tooltipText = new StringBuilder(1024);

                int yPos = 0;

                for (int deviceIndex = 0; deviceIndex < Scene.maxDeviceIndex; deviceIndex++)
                {
                    if (LayoutViews[deviceIndex] == null)
                    {
                        LayoutViews[deviceIndex] = new LayoutView();
                        LayoutViews[deviceIndex].DeviceIndex = deviceIndex;
                    }

                    float time = Scene.GetFrame(CurrentFrameNumber).Time;
                    if (CurrentFrameNumber < Scene.MaxFrame - 1)
                    {
                        time = Scene.GetFrame(CurrentFrameNumber + 1).Time;
                    }

                    yPos += LayoutViews[deviceIndex].RenderDevice(e.Graphics, Scene, yPos, time, this, tooltipText);
                }

                tooltip.SetToolTip(tooltipText.ToString());
                tooltip.Draw(e.Graphics, DefaultFont, Width, Height);
            }
        }

        private void UpdateScrollbar()
        {
            if (Scene != null)
            {
                float lastTime = Scene.GetFrame(Scene.FrameCount - 1).Time;
                Scroller.Maximum = (int)(lastTime * 1000);
            }
        }

        private void UpdateScrollbarPos()
        {
            if (Scene != null)
            {
//                Scroller.Value = Math.Min(Math.Max((int)(Offset * 1000.0f), 0), Scroller.Maximum);
            }
        }

        private void Scroller_Scroll(object sender, ScrollEventArgs e)
        {

        }

        protected override void OnMouseWheel(MouseEventArgs e)
        {
            if (e.Delta != 0)
            {
                /*
                if (e.Delta < 0)
                {
                    ZoomFactor += 1.0f;
                }
                else
                {
                    if (ZoomFactor > 1.0f)
                    {
                        ZoomFactor -= 1.0f;
                    }
                }
                 */
//                ZoomFactor *= (e.Delta < 0) ? 1.1f : 0.9f;

                ZoomFactor *= (e.Delta < 0) ? 1.05f : 0.95f;
                UpdateScrollbar();
                Refresh();
            }

            base.OnMouseWheel(e);
        }

        private void DiscLayoutView_MouseMove(object sender, MouseEventArgs e)
        {
            LastMouseX = e.X;
            LastMouseY = e.Y;
/*
            MouseFrameNumber = GetFrameAtTime(GetSeconds(LastMouseX));

            if (IsDragging)
            {
                int delta = e.X - DragStartX;
                float deltaSeconds = delta * ZoomFactor;

                Offset = DragStartOffset - deltaSeconds;
                UpdateScrollbarPos();
            }
            */
            tooltip.MouseMove(e);
            
            //HoverStreamable = null;
            Refresh();
        }

        private void DiscLayoutView_MouseHover(object sender, EventArgs e)
        {
            Focus();
        }

        private void DiscLayoutView_MouseUp(object sender, MouseEventArgs e)
        {
/*            ChangingSelection = true;
            IsDragging = false;

            // Select the streamable and the frame only if we didn't drag or use the context menu.
            if (e.Button == MouseButtons.Left)
            {
                // Filter out drag movements
                if (DragTimer.ElapsedMilliseconds < 1000 && Math.Abs(e.X - DragStartX) < 5)
                {
                    if (HoverStreamable != null)
                    {
                        Controller.OnSelectStreamable(HoverStreamable, true);
                    }

                    // Also select this frame, and this time.
                    float seconds = GetSeconds(e.X);
                    int frame = GetFrameAtTime(seconds);

                    if (frame != -1)
                    {
                        if (frame > 0)
                        {
                            frame--;
                        }
                        Controller.SetToFrame(frame, true);
                    }
                }
            }

            ChangingSelection = false;*/
        }

        private void DiscLayoutView_MouseDown(object sender, MouseEventArgs e)
        {
            if (HoverStreamable == null)
            {
//                DragStartOffset = Offset;
                IsDragging = true;
            }

            DragStartX = e.X;
            DragTimer.Restart();
        }

        protected override bool IsInputKey(Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Left:
                case Keys.Right:
                    return true;
            }
            return base.IsInputKey(keyData);
        }

        private void DiscLayoutView_KeyDown(object sender, KeyEventArgs e)
        {
        }
    }
}
