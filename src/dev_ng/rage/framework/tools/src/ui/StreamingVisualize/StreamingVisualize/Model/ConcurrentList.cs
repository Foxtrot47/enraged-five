﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StreamingVisualize.Model
{
    public class ConcurrentList<T> : IList<T>
    {
        List<T> m_List;
        object m_Lock = new object();

        public ConcurrentList()
        {
            m_List = new List<T>();
        }

        public ConcurrentList(IEnumerable<T> enm)
        {
            m_List = new List<T>(enm);
        }

        public int IndexOf(T item)
        {
            lock (m_Lock) return m_List.IndexOf(item);
        }

        public void Insert(int index, T item)
        {
            lock (m_Lock) m_List.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            lock (m_Lock) m_List.RemoveAt(index);
        }

        public T this[int index]
        {
            get
            {
                lock (m_Lock) return m_List[index];
            }
            set
            {
                lock (m_Lock) m_List[index] = value;
            }
        }

        public void Add(T item)
        {
            lock (m_Lock) m_List.Add(item);
        }

        public void Clear()
        {
            lock (m_Lock) m_List.Clear();
        }

        public bool Contains(T item)
        {
            lock (m_Lock) return m_List.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            lock (m_Lock) m_List.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { lock (m_Lock) return m_List.Count; }
        }

        public bool IsReadOnly
        {
            get { lock (m_Lock) return false; }
        }

        public bool Remove(T item)
        {
            lock (m_Lock) return m_List.Remove(item);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return new Enumerator(this);
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return new Enumerator(this);
        }

        class Enumerator : IEnumerator<T>
        {
            private ConcurrentList<T> m_Owner;
            private int m_Index;
            private T m_Current;

            public Enumerator(ConcurrentList<T> lst)
            {
                m_Owner = lst;
                Reset();
            }

            public T Current
            {
                get { return m_Current; }
            }

            public void Dispose()
            {
                m_Owner = null;
                Reset();
            }

            object System.Collections.IEnumerator.Current
            {
                get { return m_Current; }
            }

            public bool MoveNext()
            {
                lock (m_Owner.m_Lock)
                {
                    m_Index++;
                    if (m_Index < m_Owner.m_List.Count)
                    {
                        m_Current = m_Owner.m_List[m_Index];
                        return true;
                    }
                    else
                    {
                        m_Current = default(T);
                        return false;
                    }
                }
            }

            public void Reset()
            {
                m_Index = -1;
                m_Current = default(T);
            }
        }
    }
}
