﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StreamingVisualize.Model
{
    public class StatusChange
    {
        public int Frame { get; set; }
        public Streamable Streamable { get; set; }
        public int NewStatus { get; set; }
    }
}
