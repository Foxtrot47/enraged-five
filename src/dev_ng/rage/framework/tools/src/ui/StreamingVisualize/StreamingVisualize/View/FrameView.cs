﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StreamingVisualize.Model;
using StreamingVisualize.Controller;
using StreamingVisualize.View.Components;

namespace StreamingVisualize.View
{
    public partial class FrameView : UserControl, IViewController
    {
        private ViewContextMenu viewContextMenu = new ViewContextMenu();

        public Scene Scene { get; set; }
        private List<Request> RequestList;
        private List<Unrequest> UnrequestList;
        private IList<FailedAlloc> FailedAllocList;
        public MainController Controller;

        private ToolTip MarkerToolTip = new ToolTip();

        private int frameNumber;
        private Font boldFont;
        private Font doomedFont, doomedBoldFont;

        private bool ShowDoomedRequests = true;

        // If true, we're changing lists around. Don't respond to selection changes.
        private bool ChangingSelection;

        public FrameView()
        {
            InitializeComponent();
        }

        public void UpdateFrameNumber()
        {
            if (Scene != null)
            {
                FrameNumber.Text = String.Format("Frame: {0}/{1}, {2:F2}", frameNumber, Scene.FrameCount, Scene.GetFrame(frameNumber).Time);
            }
        }

        public void UpdateFrameNumberDeferred()
        {
            BeginInvoke((Action)delegate { UpdateFrameNumber(); });
        }

        private void Requests_SelectionChanged(object sender, EventArgs e)
        {
            if (ChangingSelection)
            {
                return;
            }

            if (Requests.SelectedCells.Count > 0)
            {
                int index = Requests.SelectedCells[0].RowIndex;

                if (index >= 0 && index < RequestList.Count)
                {
                    Request request = RequestList[index];
                    Controller.OnSelectStreamable(request.Streamable, true);
                }
            }
        }

        private void Unrequests_SelectionChanged(object sender, EventArgs e)
        {
            if (ChangingSelection)
            {
                return;
            }

            if (Unrequests.SelectedCells.Count > 0)
            {
                int index = Unrequests.SelectedCells[0].RowIndex;

                if (index >= 0 && index < UnrequestList.Count)
                {
                    Unrequest unrequest = UnrequestList[index];
                    Controller.OnSelectStreamable(unrequest.Streamable, true);
                }
            }
        }

        private void Requests_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Requests_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            int row = e.RowIndex;

            if (row >= 0 && row < RequestList.Count)
            {
                Request request = RequestList[row];
                int flags = request.Flags;
                bool isPriority = ((flags & Request.PRIORITY_LOAD) != 0);

                if (Requests.Columns[e.ColumnIndex].Name == "Flags")
                {
                    String flagString = "";

                    foreach (Request.ShortReqFlags flag in Enum.GetValues(typeof(Request.ShortReqFlags)))
                    {
                        if ((flags & Convert.ToInt32(flag)) != 0)
                        {
                            flagString += flag.ToString() + " ";
                        }
                    }

                    e.Value = flagString;
                }
                else if (Requests.Columns[e.ColumnIndex].Name == "Streamable")
                {
                    if (boldFont == null)
                    {
                        boldFont = new Font(e.CellStyle.Font, FontStyle.Bold);
                        doomedFont = new Font(e.CellStyle.Font, FontStyle.Strikeout);
                        doomedBoldFont = new Font(e.CellStyle.Font, FontStyle.Bold | FontStyle.Strikeout);
                    }

                    if (isPriority)
                    {
                        e.CellStyle.Font = boldFont;
                    }

                    if ((request.Flags & Request.DOOMED) != 0)
                    {
                        e.CellStyle.Font = (isPriority) ? doomedBoldFont : doomedFont;
                    }
                }
                else if (Requests.Columns[e.ColumnIndex].Name == "Score")
                {
                    // Don't show negative numbers.
                    String scoreString = e.Value.ToString();
                    if (scoreString.Length > 0 && scoreString[0] == '-')
                    {
                        e.Value = "";
                    }
                }
            }
        }

        private void Unrequests_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        public void OnSelectEntity(Entity entity) { }

        // Switch the display to focus on a certain frame
        public void SetToFrame(int frameNumber)
        {
            if (Scene == null)
            {
                return;
            }

            this.frameNumber = frameNumber;
            BeginInvoke((Action)delegate { UpdateFrame(); });
        }

        private void UpdateFrame()
        {
            ChangingSelection = true;

            UpdateFrameNumber();

            Frame frame = Scene.GetFrame(frameNumber);

            lock (frame)
            {
                float fovH = (float)(Math.Tanh(frame.FovH) * 2 * 180 / Math.PI);
                float fovV = (float)(Math.Tanh(frame.FovV) * 2 * 180 / Math.PI);
                CamPos.Text = String.Format("Camera Pos: {0}, Dir: {1}, FOV: {2:F1}h/{3:F1}v, Far Clip: {4:F0}", frame.CamPos, frame.CamDir, fovH, fovV, frame.FarClip);

                if (frame.StreamingVolumes == null)
                {
                    StreamingVolume1.Text = "NONE";
                    StreamingVolume2.Text = "NONE";
                }
                else
                {
                    StreamingVolume1.Text = frame.StreamingVolumes[0].ToString();

                    if (frame.StreamingVolumes.Count > 1)
                    {
                        StreamingVolume2.Text = frame.StreamingVolumes[1].ToString();
                    }
                    else
                    {
                        StreamingVolume2.Text = "NONE";
                    }
                }
                MasterCutoff.Text = String.Format("{0}", frame.MasterCutoff);
                MarkerText.Text = frame.MarkerText;

                if (frameNumber > 0)
                {
                    Frame prevFrame = Scene.GetFrame(frameNumber - 1);
                    Vector prevPos = prevFrame.CamPos;

                    float dist = frame.CamPos.Dist(prevPos);
                    float timePassed = frame.Time - prevFrame.Time;

                    if (timePassed > 0)
                    {
                        CameraSpeed.Text = String.Format("Speed: {0:F2}m/s", dist / timePassed);
                    }
                    else
                    {
                        CameraSpeed.Text = "Speed: N/A";
                    }
                }
                else
                {
                    CameraSpeed.Text = " ";
                }

                if (frame.MarkerText != null && frame.MarkerText.Length > 0)
                {
                    String multilineMarker = frame.GetMultilineMarkerText();

                    // Set up the delays for the ToolTip.
                    MarkerToolTip.AutoPopDelay = 10000;
                    MarkerToolTip.InitialDelay = 100;
                    MarkerToolTip.ReshowDelay = 500;
                    MarkerToolTip.ShowAlways = true;

                    MarkerToolTip.SetToolTip(MarkerText, multilineMarker);
                }

                if (ShowDoomedRequests && !Scene.Filter.IsActive())
                {
                    RequestList = frame.requests;
                }
                else
                {
                    List<Request> filteredRequests = new List<Request>();

                    foreach (Request req in frame.requests)
                    {
                        if (ShowDoomedRequests || (req.Flags & Request.DOOMED) == 0)
                        {
                            //if (!Scene.Filter.IsFilteredOut(req.Streamable))
                            if (!req.Streamable.Filtered)
                            {
                                filteredRequests.Add(req);
                            }
                        }
                    }

                    RequestList = filteredRequests;
                }

                Requests.DataSource = RequestList;

                if (!Scene.Filter.IsActive())
                {
                    UnrequestList = frame.unrequests;
                }
                else
                {
                    List<Unrequest> filteredUnrequests = new List<Unrequest>();

                    lock (frame.unrequests)
                    {
                        foreach (Unrequest req in frame.unrequests)
                        {
                            //if (!Scene.Filter.IsFilteredOut(req.Streamable))
                            if (!req.Streamable.Filtered)
                            {
                                filteredUnrequests.Add(req);
                            }
                        }
                    }

                    UnrequestList = filteredUnrequests;
                }

                Unrequests.DataSource = UnrequestList;

                FailedAllocList = frame.FailedAllocs;
                FailedAllocs.DataSource = FailedAllocList;
            }

            BoxSearches.DataSource = frame.BoxSearches;

            ChangingSelection = false;
        }

        // A new device was registered, or a device has changed.
        public void OnDeviceChanged(int deviceIndex, Device device) { }

        // A streamable has been selected
        public void OnSelectStreamable(Streamable streamable) { }

        // The client information has been updated
        public void ClientInfoUpdated() { }

        // We're in follow mode, and a new frame has been added.
        public void OnNewFrame(int frameNumber)
        {
            SetToFrame(frameNumber);
        }

        // A new entity type was registered
        public void OnNewEntityType(int entityTypeId, EntityType type) { }

        // The connection has been terminated
        public void OnConnectionLost() { }

        // Register this view with the system
        public void Register(MainController controller, Scene scene)
        {
            this.Controller = controller;
            this.Scene = scene;
        }

        private void ToggleShowDoomedRequests(MenuItem item)
        {
            ShowDoomedRequests = !ShowDoomedRequests;
            item.Checked = ShowDoomedRequests;

            UpdateFrame();
            Refresh();
        }

        private void DumpFrameInfoToClipboard()
        {
            Frame frame = Scene.GetFrame(frameNumber);
            StringBuilder result = new StringBuilder(16384);

            result.Append("Frame\tCam Pos X\tCam Pos Y\tCam Pos Z\tCam Dir X\tCam Dir Y\tCam Dir Z\tFOV h\tFOV v\tFar Clip\tMaster Cutoff\n");

            result.Append(String.Format("{0:F}\t{1:F}\t{2:F}\t{3:F}\t{4:F}\t{5:F}\t{6:F}\t{7:F}\t{8:F}\t{9:F}\t{10:F}\n",
                frameNumber,
                frame.CamPos.X, frame.CamPos.Y, frame.CamPos.Z,
                frame.CamDir.X, frame.CamDir.Y, frame.CamDir.Z,
                frame.FovH, frame.FovV, frame.FarClip,
                frame.MasterCutoff
                ));

            result.Append("\nMarkers:\n");

			String[] markerLines = {"NO MARKER"};
			if (frame.MarkerText != null){
				markerLines = frame.MarkerText.Split(new string[] { " / " }, StringSplitOptions.None);
			}


            foreach (String line in markerLines)
            {
                result.Append(String.Format("\"{0}\"\n", line));
            }

            result.Append("\nBox Searches:\n");

            foreach (BoxSearch search in frame.BoxSearches)
            {
                result.Append(String.Format("{0:F}\t{1:F}\t{2:F}\t{3:F}\t{4:F}\t{5:F}\t{6}\n",
                    search.Aabb.BoxMin.X, search.Aabb.BoxMin.Y, search.Aabb.BoxMin.Z,
                    search.Aabb.BoxMax.X, search.Aabb.BoxMax.Y, search.Aabb.BoxMax.Z,
                    search.SearchType
                    ));
            }

            result.Append("\n\nRequests\tFlags\tContext\tScore\tUnrequests\t\tContext\t\tDoomed Requests\tFlags\tContext\tScore\n");

            List<String>[] columns = new List<String>[12];

            for (int x=0; x<columns.Length; x++)
            {
                columns[x] = new List<String>();
            }

            for (int x=0; x<frame.requests.Count; x++)
            {
                Request req = frame.requests[x];

                if ((req.Flags & Request.DOOMED) == 0)
                {
                    if (!req.Streamable.Filtered)
                    {
                        columns[0].Add(req.Streamable.CurrentName);
                        columns[1].Add(Request.CreateFlagString(req.Flags));
                        columns[2].Add(req.Context);
                        columns[3].Add(String.Format("{0:F}", req.Score));
                    }
                }
                else
                {
                    if (!req.Streamable.Filtered)
                    {
                        columns[8].Add(req.Streamable.CurrentName);
                        columns[9].Add(Request.CreateFlagString(req.Flags));
                        columns[10].Add(req.Context);
                        columns[11].Add(String.Format("{0:F}", req.Score));
                    }
                }
            }

            for (int x=0; x<frame.unrequests.Count; x++)
            {
                Unrequest unreq = frame.unrequests[x];

                columns[4].Add(unreq.Streamable.CurrentName);
                columns[5].Add("");
                columns[6].Add(unreq.Context);
                columns[7].Add("");
            }

            int highestLine = -1;
            for (int x=0; x<columns.Length; x++)
            {
                highestLine = Math.Max(highestLine, columns[x].Count);
            }

            for (int x=0; x<highestLine; x++)
            {
                for (int y=0; y<columns.Length; y++)
                {
                    if (columns[y].Count > x)
                    {
                        result.Append(columns[y][x]);
                    }
                    else
                    {
                        result.Append(" ");
                    }

                    result.Append("\t");
                }

                result.Append("\n");
            }

            Clipboard.SetText(result.ToString());
            MessageBox.Show("Frame data copied to clipboard. Paste it into Excel.");
        }

        private void FrameView_Load(object sender, EventArgs e)
        {
            ContextMenu menu = viewContextMenu.CreateContextMenu(Controller, this);

            MenuItem doomedReqItem = new MenuItem("Show &Doomed Requests");
            doomedReqItem.Checked = ShowDoomedRequests;
            doomedReqItem.Click += (menuSender, menuE) => ToggleShowDoomedRequests(doomedReqItem);
            menu.MenuItems.Add(doomedReqItem);

            menu.MenuItems.Add(ContextMenuHelper.CreateActionMenu("Dump Frame Info To Clipboard", DumpFrameInfoToClipboard));


            this.ContextMenu = menu;

            if (Scene != null)
            {
                if (Scene.CurrentFrameNumber < Scene.MaxFrame)
                {
                    SetToFrame(Scene.CurrentFrameNumber);
                }
            }
        }

        public void OnFilterChanged()
        {
            UpdateFrame();
        }

        public void OnFrameRangeChanged(int firstFrame, int lastFrame)
        {

        }

        private void Unrequests_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Unrequests_SelectionChanged(sender, null);
        }

        private void Requests_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Requests_SelectionChanged(sender, null);
        }

        private void FailedAllocs_CellClick(object sender, DataGridViewCellEventArgs e)
        {
        }
    }
}
