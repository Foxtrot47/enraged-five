﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace StreamingVisualize.Model
{
    public class Device : IVersionedSerializable
    {
        public String Name { get; set; }

        public List<DeviceEvent> DeviceEvents = new List<DeviceEvent>();

        // Maps a streaming handle to a request
        public Dictionary<int, Streamable> OpenRequests = new Dictionary<int, Streamable>();

        // Maps a frame number to an index in the events list
        public Dictionary<int, int> FrameToRow = new Dictionary<int, int>();

        public PerFrameState<DeviceStreamingState> StreamingState = new PerFrameState<DeviceStreamingState>();


        public void MarkFrame(int frameNumber)
        {
            int index = DeviceEvents.Count;

            if (index > 0)
            {
                index--;
            }

            FrameToRow[frameNumber] = index;
        }

        private void RecreateFrameToRow()
        {
            int lastFrame = -1;

            for (int x = 0; x < DeviceEvents.Count; x++)
            {
                int frame = DeviceEvents[x].Frame;

                if (frame != lastFrame)
                {
                    for (int y = lastFrame + 1; y <= frame; y++)
                    {
                        FrameToRow[y] = x;
                    }
                    lastFrame = frame;
                }
            }
        }

        public float GetEarliestWallclockTime(int firstFrame)
        {
            // TODO: Binary search would be awesome.
            for (int x = 0; x < DeviceEvents.Count; x++)
            {
                if (DeviceEvents[x].Frame >= firstFrame)
                {
                    if (x > 0 && DeviceEvents[x].Frame > firstFrame)
                    {
                        x--;
                    }

                    return DeviceEvents[x].Time;
                }
            }

            // Nothing happened after (or at) that frame.
            return float.MaxValue;
        }

        public float GetLatestWallclockTime(int lastFrame)
        {
            // TODO: Binary search would be awesome.
            for (int x = 1; x < DeviceEvents.Count; x++)
            {
                if (DeviceEvents[x].Frame > lastFrame)
                {
                    return DeviceEvents[x - 1].Time;
                }
            }

            // Nothing happened after (or at) that frame.
            if (DeviceEvents.Count > 0)
            {
                return DeviceEvents[DeviceEvents.Count - 1].Time;
            }

            return 0;
        }

        public void Serialize(VersionedWriter writer)
        {
            writer.Write(Name);
            writer.Write(DeviceEvents);

            // File version 2: Streaming state
            writer.Write(StreamingState);
        }

        public void Deserialize(VersionedReader reader, int version)
        {
            Name = reader.ReadString();
            reader.ReadList(DeviceEvents);

            // File version 2: Streaming state
            if (version > 1)
            {
                reader.ReadPerFrameState(StreamingState);
            }

            RecreateFrameToRow();
        }

        public void PurgeFinishedEvents(Scene scene)
        {
            // Do we have any finished events?
            bool hasFinished = false;

            DeviceStreamingState currentState = StreamingState.PresentState;

            // TODO: We could simply add a flag to DeviceStreamingState to indicate
            // that there are finished events.
            foreach (DeviceStreamingState.StreamableEntry entry in currentState.Queued)
            {
                if (entry.Finished)
                {
                    hasFinished = true;
                    break;
                }
            }

            if (!hasFinished)
            {
                foreach (DeviceStreamingState.StreamableEntry entry in currentState.Streaming)
                {
                    if (entry.Finished)
                    {
                        hasFinished = true;
                        break;
                    }
                }
            }

            if (hasFinished)
            {
                // Yep - we need to clean up here.
                // Cloning the list will automatically purge old requests.
                DeviceStreamingState newState = StreamingState.GetStateForWriting(scene);
            }
        }
    }
}
