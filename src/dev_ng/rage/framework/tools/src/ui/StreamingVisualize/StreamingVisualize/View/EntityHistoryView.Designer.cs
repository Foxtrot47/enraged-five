﻿namespace StreamingVisualize.View
{
    partial class EntityHistoryView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.EntityName = new System.Windows.Forms.Label();
            this.Archetype = new System.Windows.Forms.Label();
            this.MapData = new System.Windows.Forms.Label();
            this.LodType = new System.Windows.Forms.Label();
            this.EntityType = new System.Windows.Forms.Label();
            this.Visibility = new System.Windows.Forms.Label();
            this.StreamableStatus = new System.Windows.Forms.Label();
            this.History = new System.Windows.Forms.DataGridView();
            this.FrameNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EventCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Context = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Score = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StreamableCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GuidCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LodTypeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EntityTypeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.entityHistoryItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.History)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.entityHistoryItemBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // EntityName
            // 
            this.EntityName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.EntityName.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.EntityName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EntityName.Location = new System.Drawing.Point(3, 0);
            this.EntityName.Name = "EntityName";
            this.EntityName.Size = new System.Drawing.Size(417, 13);
            this.EntityName.TabIndex = 1;
            this.EntityName.Text = "NO RESOURCE SELECTED";
            // 
            // Archetype
            // 
            this.Archetype.AutoSize = true;
            this.Archetype.Location = new System.Drawing.Point(3, 20);
            this.Archetype.Name = "Archetype";
            this.Archetype.Size = new System.Drawing.Size(55, 13);
            this.Archetype.TabIndex = 2;
            this.Archetype.Text = "Archetype";
            // 
            // MapData
            // 
            this.MapData.AutoSize = true;
            this.MapData.Location = new System.Drawing.Point(3, 42);
            this.MapData.Name = "MapData";
            this.MapData.Size = new System.Drawing.Size(51, 13);
            this.MapData.TabIndex = 3;
            this.MapData.Text = "MapData";
            // 
            // LodType
            // 
            this.LodType.AutoSize = true;
            this.LodType.Location = new System.Drawing.Point(275, 21);
            this.LodType.Name = "LodType";
            this.LodType.Size = new System.Drawing.Size(49, 13);
            this.LodType.TabIndex = 4;
            this.LodType.Text = "LodType";
            // 
            // EntityType
            // 
            this.EntityType.AutoSize = true;
            this.EntityType.Location = new System.Drawing.Point(273, 38);
            this.EntityType.Name = "EntityType";
            this.EntityType.Size = new System.Drawing.Size(57, 13);
            this.EntityType.TabIndex = 5;
            this.EntityType.Text = "EntityType";
            // 
            // Visibility
            // 
            this.Visibility.AutoSize = true;
            this.Visibility.Location = new System.Drawing.Point(7, 60);
            this.Visibility.Name = "Visibility";
            this.Visibility.Size = new System.Drawing.Size(43, 13);
            this.Visibility.TabIndex = 6;
            this.Visibility.Text = "Visibility";
            // 
            // StreamableStatus
            // 
            this.StreamableStatus.AutoSize = true;
            this.StreamableStatus.Location = new System.Drawing.Point(277, 55);
            this.StreamableStatus.Name = "StreamableStatus";
            this.StreamableStatus.Size = new System.Drawing.Size(90, 13);
            this.StreamableStatus.TabIndex = 7;
            this.StreamableStatus.Text = "StreamableStatus";
            // 
            // History
            // 
            this.History.AllowUserToAddRows = false;
            this.History.AllowUserToDeleteRows = false;
            this.History.AllowUserToResizeRows = false;
            this.History.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.History.AutoGenerateColumns = false;
            this.History.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.History.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.FrameNumberCol,
            this.EventCol,
            this.Context,
            this.Score,
            this.StreamableCol,
            this.GuidCol,
            this.LodTypeCol,
            this.EntityTypeCol});
            this.History.DataSource = this.entityHistoryItemBindingSource;
            this.History.Location = new System.Drawing.Point(0, 88);
            this.History.Name = "History";
            this.History.ReadOnly = true;
            this.History.RowHeadersVisible = false;
            this.History.Size = new System.Drawing.Size(420, 268);
            this.History.TabIndex = 8;
            this.History.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.History_CellContentClick);
            this.History.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.History_CellFormatting);
            this.History.CellValueNeeded += new System.Windows.Forms.DataGridViewCellValueEventHandler(this.History_CellValueNeeded);
            this.History.SelectionChanged += new System.EventHandler(this.History_SelectionChanged);
            // 
            // FrameNumberCol
            // 
            this.FrameNumberCol.DataPropertyName = "FrameNumber";
            this.FrameNumberCol.HeaderText = "FrameNumber";
            this.FrameNumberCol.Name = "FrameNumberCol";
            this.FrameNumberCol.ReadOnly = true;
            // 
            // EventCol
            // 
            this.EventCol.DataPropertyName = "Event";
            this.EventCol.HeaderText = "Event";
            this.EventCol.Name = "EventCol";
            this.EventCol.ReadOnly = true;
            // 
            // Context
            // 
            this.Context.DataPropertyName = "Context";
            this.Context.HeaderText = "Context";
            this.Context.Name = "Context";
            this.Context.ReadOnly = true;
            // 
            // Score
            // 
            this.Score.DataPropertyName = "Score";
            this.Score.HeaderText = "Score";
            this.Score.Name = "Score";
            this.Score.ReadOnly = true;
            // 
            // StreamableCol
            // 
            this.StreamableCol.DataPropertyName = "Streamable";
            this.StreamableCol.HeaderText = "Streamable";
            this.StreamableCol.Name = "StreamableCol";
            this.StreamableCol.ReadOnly = true;
            // 
            // GuidCol
            // 
            this.GuidCol.DataPropertyName = "GUID";
            this.GuidCol.HeaderText = "GUID";
            this.GuidCol.Name = "GuidCol";
            this.GuidCol.ReadOnly = true;
            // 
            // LodTypeCol
            // 
            this.LodTypeCol.DataPropertyName = "LodType";
            this.LodTypeCol.HeaderText = "LodType";
            this.LodTypeCol.Name = "LodTypeCol";
            this.LodTypeCol.ReadOnly = true;
            // 
            // EntityTypeCol
            // 
            this.EntityTypeCol.DataPropertyName = "EntityType";
            this.EntityTypeCol.HeaderText = "EntityType";
            this.EntityTypeCol.Name = "EntityTypeCol";
            this.EntityTypeCol.ReadOnly = true;
            // 
            // entityHistoryItemBindingSource
            // 
            this.entityHistoryItemBindingSource.DataSource = typeof(StreamingVisualize.View.EntityHistoryView.EntityHistoryItem);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Streamable";
            this.dataGridViewTextBoxColumn1.HeaderText = "Streamable";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "LodType";
            this.dataGridViewTextBoxColumn2.HeaderText = "LodType";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "EntityType";
            this.dataGridViewTextBoxColumn3.HeaderText = "EntityType";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Streamable";
            this.dataGridViewTextBoxColumn4.HeaderText = "Streamable";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "LodType";
            this.dataGridViewTextBoxColumn5.HeaderText = "LodType";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "EntityType";
            this.dataGridViewTextBoxColumn6.HeaderText = "EntityType";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "Streamable";
            this.dataGridViewTextBoxColumn7.HeaderText = "Streamable";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "LodType";
            this.dataGridViewTextBoxColumn8.HeaderText = "LodType";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "EntityType";
            this.dataGridViewTextBoxColumn9.HeaderText = "EntityType";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "Streamable";
            this.dataGridViewTextBoxColumn10.HeaderText = "Streamable";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "LodType";
            this.dataGridViewTextBoxColumn11.HeaderText = "LodType";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "EntityType";
            this.dataGridViewTextBoxColumn12.HeaderText = "EntityType";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "Streamable";
            this.dataGridViewTextBoxColumn13.HeaderText = "Streamable";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "LodType";
            this.dataGridViewTextBoxColumn14.HeaderText = "LodType";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "EntityType";
            this.dataGridViewTextBoxColumn15.HeaderText = "EntityType";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.DataPropertyName = "Streamable";
            this.dataGridViewTextBoxColumn16.HeaderText = "Streamable";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.DataPropertyName = "LodType";
            this.dataGridViewTextBoxColumn17.HeaderText = "LodType";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.DataPropertyName = "EntityType";
            this.dataGridViewTextBoxColumn18.HeaderText = "EntityType";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.DataPropertyName = "Streamable";
            this.dataGridViewTextBoxColumn19.HeaderText = "Streamable";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.DataPropertyName = "LodType";
            this.dataGridViewTextBoxColumn20.HeaderText = "LodType";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.DataPropertyName = "EntityType";
            this.dataGridViewTextBoxColumn21.HeaderText = "EntityType";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.ReadOnly = true;
            // 
            // EntityHistoryView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.History);
            this.Controls.Add(this.StreamableStatus);
            this.Controls.Add(this.Visibility);
            this.Controls.Add(this.EntityType);
            this.Controls.Add(this.LodType);
            this.Controls.Add(this.MapData);
            this.Controls.Add(this.Archetype);
            this.Controls.Add(this.EntityName);
            this.Name = "EntityHistoryView";
            this.Size = new System.Drawing.Size(423, 359);
            this.Load += new System.EventHandler(this.EntityHistoryView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.History)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.entityHistoryItemBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label EntityName;
        private System.Windows.Forms.Label Archetype;
        private System.Windows.Forms.Label MapData;
        private System.Windows.Forms.Label LodType;
        private System.Windows.Forms.Label EntityType;
        private System.Windows.Forms.Label Visibility;
        private System.Windows.Forms.Label StreamableStatus;
        private System.Windows.Forms.DataGridView History;
        private System.Windows.Forms.BindingSource entityHistoryItemBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn FrameNumberCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn EventCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn Context;
        private System.Windows.Forms.DataGridViewTextBoxColumn Score;
        private System.Windows.Forms.DataGridViewTextBoxColumn StreamableCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn GuidCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn LodTypeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn EntityTypeCol;
    }
}
