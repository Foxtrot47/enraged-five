﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StreamingVisualize.Model;
using StreamingVisualize.Controller;

namespace StreamingVisualize.View.Components
{
    public partial class StreamableDependencyControl : UserControl
    {
        private class Node
        {
            public List<Node> Children = new List<Node>();
            public StreamableLabel Label;
            public int MaxWidth;
            public int X, Y;
            public Node Parent, PrevSibling;
        }

        public MainController Controller;

        private Streamable Streamable;
        private int Frame;
        private Node Root;
        private int BoxHeight = 16;
        private int VertMargin = 8;
        private int HorizMargin = 16;
        private int BoxWidth = 192;


        public StreamableDependencyControl()
        {
            InitializeComponent();
        }

        private Node CreateNodeTree(Streamable Streamable, Scene scene, bool showMapDeps)
        {
            Node node = new Node();

            node.Label = new StreamableLabel();
            node.Label.Streamable = Streamable;
            node.Label.Controller = Controller;
            node.PrevSibling = null;
            List<Streamable> Dependencies = Streamable.IdentityHistory.GetStateAt(Frame).Dependencies;
            int maxWidth = Dependencies.Count;
            int nextIndex = 0;

            for (int x = 0; x < Dependencies.Count; x++)
            {
                if (showMapDeps || Dependencies[x].Module != scene.GetModule(scene.MapDataModuleIndex))
                {
                    Node child = CreateNodeTree(Dependencies[x], scene, showMapDeps);
                    child.Parent = node;
                    if (nextIndex > 0)
                    {
                        child.PrevSibling = node.Children[nextIndex - 1];
                    }
                    node.Children.Add(child);
                    maxWidth = Math.Max(maxWidth, node.Children[nextIndex++].MaxWidth);
                }
            }

            node.MaxWidth = maxWidth;

            return node;
        }

        private void AddNodes(Node Node)
        {
            Controls.Add(Node.Label);

            for (int x = 0; x < Node.Children.Count; x++)
            {
                AddNodes(Node.Children[x]);
            }
        }

        private void UpdateStatus(Node Node, int frame)
        {
            Node.Label.Status = Node.Label.Streamable.GetCurrentStatus();
            Node.Label.FrameNumber = frame;

            for (int x = 0; x < Node.Children.Count; x++)
            {
                UpdateStatus(Node.Children[x], frame);
            }
        }

        // This code has been borrowed from http://www.codeproject.com/Articles/20508/Tree-Chart-Generator
        private void PositionNode(Node Node, int xPos, int yPos, int width, int height, int siblingCount)
        {
            for (int x = 0; x < Node.Children.Count; x++)
            {
                PositionNode(Node.Children[x], xPos, yPos + BoxHeight + VertMargin, width, height, 0);
            }

            int[] results = new int[] { XPosByOwnChildren(Node), XPosByParentPreviousSibling(Node),
                                XPosByPreviousSibling(Node), HorizMargin };

            Array.Sort(results);

            xPos = results[3];

            Node.X = xPos;
            Node.Y = yPos;

            Node.Label.Size = new Size(BoxWidth, BoxHeight);
            Node.Label.Location = new Point(results[3], yPos);
        }

        private int XPosByOwnChildren(Node Node)
        {
            int result = -1;

            int childCount = Node.Children.Count;
            if (childCount > 0)
            {
                int leftX = Node.Children[0].X;
                int rightX = Node.Children[childCount-1].X;

                result = (((rightX + BoxWidth) - leftX) / 2) - (BoxWidth / 2) + leftX;
            }

            return result;
        }

        private int XPosByPreviousSibling(Node Node)
        {
            int result = -1;
            int x = -1;

            if (Node.PrevSibling != null)
            {
                int prevChildCount = Node.PrevSibling.Children.Count;
                if (prevChildCount > 0)
                {
                    x = MaxOfChildren(Node.PrevSibling.Children[prevChildCount - 1]);
                    result = x + BoxWidth + HorizMargin;
                }
                else
                {
                    result = Node.PrevSibling.X + BoxWidth + HorizMargin;
                }
            }

            return result;
        }

        private int XPosByParentPreviousSibling(Node Node)
        {
            int result = -1;
            int x = -1;

            if (Node.Parent != null)
            {
                Node parentPrev = null;

                if (Node.Parent.Parent != null)
                {
                    parentPrev = Node.Parent.Parent.PrevSibling;
                }

                if (parentPrev != null)
                {
                    if (parentPrev.Children.Count > 0)
                    {
                        x = MaxOfChildren(parentPrev.Children[parentPrev.Children.Count - 1]);
                    }
                    else
                    {
                        x = parentPrev.X;
                    }

                    result = x + BoxWidth + HorizMargin;
                }
                else
                {
                    result = XPosByParentPreviousSibling(Node.Parent);
                }
            }

            return result;
        }

        private int MaxOfChildren(Node Node)
        {
            int result = -1;

            while (Node.Children.Count > 0)
            {
                Node = Node.Children[Node.Children.Count - 1];
            }

            result = Node.X;
            return result;
        }

        private void RepositionNodes()
        {
            int width = this.Width;
            int height = this.Height;

            BoxHeight = Font.Height + 10;

            PositionNode(Root, 0, 0, width, height, 1);
        }

        public void UpdateStatus(int Frame)
        {
            this.Frame = Frame;

            if (Root == null)
            {
                return;
            }

            UpdateStatus(Root, Frame);
            Invalidate(true);
        }

        public void SetStreamable(Streamable Streamable, int frame, Scene scene, bool showMapDeps)
        {
            SuspendLayout();

            this.Streamable = Streamable;
            this.Frame = frame;

            Controls.Clear();

            // Create the tree.
            Root = CreateNodeTree(Streamable, scene, showMapDeps);

            AddNodes(Root);
            UpdateStatus(Root, Frame);

            RepositionNodes();
            ResumeLayout();
        }

        private void progressBar1_Resize(object sender, EventArgs e)
        {
            RepositionNodes();
        }

        private void progressBar1_Layout(object sender, LayoutEventArgs e)
        {

        }


    }
}
