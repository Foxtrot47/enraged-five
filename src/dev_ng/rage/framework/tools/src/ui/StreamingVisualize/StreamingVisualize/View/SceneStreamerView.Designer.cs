﻿namespace StreamingVisualize.View
{
    partial class SceneStreamerView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Header = new System.Windows.Forms.Label();
            this.SceneStreamerList = new System.Windows.Forms.DataGridView();
            this.streamerEntryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.entityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.requestDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VisFlags = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EntityFlags = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.scoreDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sizeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lodTypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mapDataDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.distance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fDot = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fCosof = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fSubtent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.SceneStreamerList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.streamerEntryBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // Header
            // 
            this.Header.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Header.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.Header.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Header.Location = new System.Drawing.Point(3, 0);
            this.Header.Name = "Header";
            this.Header.Size = new System.Drawing.Size(527, 13);
            this.Header.TabIndex = 2;
            this.Header.Text = "SCENE STREAMER";
            // 
            // SceneStreamerList
            // 
            this.SceneStreamerList.AllowUserToAddRows = false;
            this.SceneStreamerList.AllowUserToDeleteRows = false;
            this.SceneStreamerList.AllowUserToResizeRows = false;
            this.SceneStreamerList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SceneStreamerList.AutoGenerateColumns = false;
            this.SceneStreamerList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SceneStreamerList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.entityDataGridViewTextBoxColumn,
            this.requestDataGridViewTextBoxColumn,
            this.VisFlags,
            this.EntityFlags,
            this.scoreDataGridViewTextBoxColumn,
            this.sizeDataGridViewTextBoxColumn,
            this.lodTypeDataGridViewTextBoxColumn,
            this.mapDataDataGridViewTextBoxColumn,
            this.distance,
            this.fDot,
            this.fCosof,
            this.fSubtent});
            this.SceneStreamerList.DataSource = this.streamerEntryBindingSource;
            this.SceneStreamerList.Location = new System.Drawing.Point(0, 17);
            this.SceneStreamerList.Name = "SceneStreamerList";
            this.SceneStreamerList.ReadOnly = true;
            this.SceneStreamerList.RowHeadersVisible = false;
            this.SceneStreamerList.Size = new System.Drawing.Size(533, 407);
            this.SceneStreamerList.TabIndex = 3;
            this.SceneStreamerList.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.SceneStreamerList_CellFormatting);
            this.SceneStreamerList.SelectionChanged += new System.EventHandler(this.SceneStreamerList_SelectionChanged);
            // 
            // streamerEntryBindingSource
            // 
            this.streamerEntryBindingSource.DataSource = typeof(StreamingVisualize.View.SceneStreamerView.StreamerEntry);
            // 
            // entityDataGridViewTextBoxColumn
            // 
            this.entityDataGridViewTextBoxColumn.DataPropertyName = "Streamable";
            this.entityDataGridViewTextBoxColumn.HeaderText = "Streamable";
            this.entityDataGridViewTextBoxColumn.Name = "entityDataGridViewTextBoxColumn";
            this.entityDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // requestDataGridViewTextBoxColumn
            // 
            this.requestDataGridViewTextBoxColumn.DataPropertyName = "Request";
            this.requestDataGridViewTextBoxColumn.HeaderText = "Request";
            this.requestDataGridViewTextBoxColumn.Name = "requestDataGridViewTextBoxColumn";
            this.requestDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // VisFlags
            // 
            this.VisFlags.DataPropertyName = "VisFlags";
            this.VisFlags.HeaderText = "VisFlags";
            this.VisFlags.Name = "VisFlags";
            this.VisFlags.ReadOnly = true;
            // 
            // EntityFlags
            // 
            this.EntityFlags.DataPropertyName = "EntityFlags";
            this.EntityFlags.HeaderText = "EntityFlags";
            this.EntityFlags.Name = "EntityFlags";
            this.EntityFlags.ReadOnly = true;
            // 
            // scoreDataGridViewTextBoxColumn
            // 
            this.scoreDataGridViewTextBoxColumn.DataPropertyName = "Score";
            this.scoreDataGridViewTextBoxColumn.HeaderText = "Score";
            this.scoreDataGridViewTextBoxColumn.Name = "scoreDataGridViewTextBoxColumn";
            this.scoreDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // sizeDataGridViewTextBoxColumn
            // 
            this.sizeDataGridViewTextBoxColumn.DataPropertyName = "Size";
            this.sizeDataGridViewTextBoxColumn.HeaderText = "Size";
            this.sizeDataGridViewTextBoxColumn.Name = "sizeDataGridViewTextBoxColumn";
            this.sizeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // lodTypeDataGridViewTextBoxColumn
            // 
            this.lodTypeDataGridViewTextBoxColumn.DataPropertyName = "LodType";
            this.lodTypeDataGridViewTextBoxColumn.HeaderText = "LodType";
            this.lodTypeDataGridViewTextBoxColumn.Name = "lodTypeDataGridViewTextBoxColumn";
            this.lodTypeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // mapDataDataGridViewTextBoxColumn
            // 
            this.mapDataDataGridViewTextBoxColumn.DataPropertyName = "MapData";
            this.mapDataDataGridViewTextBoxColumn.HeaderText = "MapData";
            this.mapDataDataGridViewTextBoxColumn.Name = "mapDataDataGridViewTextBoxColumn";
            this.mapDataDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // distance
            // 
            this.distance.DataPropertyName = "distance";
            this.distance.HeaderText = "distance";
            this.distance.Name = "distance";
            this.distance.ReadOnly = true;
            // 
            // fDot
            // 
            this.fDot.DataPropertyName = "fDot";
            this.fDot.HeaderText = "fDot";
            this.fDot.Name = "fDot";
            this.fDot.ReadOnly = true;
            // 
            // fCosof
            // 
            this.fCosof.DataPropertyName = "fCosof";
            this.fCosof.HeaderText = "fCosof";
            this.fCosof.Name = "fCosof";
            this.fCosof.ReadOnly = true;
            // 
            // fSubtent
            // 
            this.fSubtent.DataPropertyName = "fSubtent";
            this.fSubtent.HeaderText = "fSubtent";
            this.fSubtent.Name = "fSubtent";
            this.fSubtent.ReadOnly = true;
            // 
            // SceneStreamerView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.SceneStreamerList);
            this.Controls.Add(this.Header);
            this.Name = "SceneStreamerView";
            this.Size = new System.Drawing.Size(533, 424);
            this.Load += new System.EventHandler(this.SceneStreamerView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.SceneStreamerList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.streamerEntryBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label Header;
        private System.Windows.Forms.DataGridView SceneStreamerList;
        private System.Windows.Forms.BindingSource streamerEntryBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn entityDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn requestDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn VisFlags;
        private System.Windows.Forms.DataGridViewTextBoxColumn EntityFlags;
        private System.Windows.Forms.DataGridViewTextBoxColumn scoreDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sizeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lodTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mapDataDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn distance;
        private System.Windows.Forms.DataGridViewTextBoxColumn fDot;
        private System.Windows.Forms.DataGridViewTextBoxColumn fCosof;
        private System.Windows.Forms.DataGridViewTextBoxColumn fSubtent;
    }
}
