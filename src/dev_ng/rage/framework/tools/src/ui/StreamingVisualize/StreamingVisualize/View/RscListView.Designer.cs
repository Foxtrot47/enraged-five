﻿namespace StreamingVisualize.View
{
    partial class RscListView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.RscList = new System.Windows.Forms.DataGridView();
            this.streamableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrentName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrentStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrentFlags = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Module = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VirtMem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PhysMem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClosestEntity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EntityDistance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.RscList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.streamableBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // RscList
            // 
            this.RscList.AllowUserToAddRows = false;
            this.RscList.AllowUserToDeleteRows = false;
            this.RscList.AutoGenerateColumns = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.RscList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.RscList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.RscList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CurrentName,
            this.CurrentStatus,
            this.CurrentFlags,
            this.Module,
            this.VirtMem,
            this.PhysMem,
            this.ClosestEntity,
            this.EntityDistance});
            this.RscList.DataSource = this.streamableBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.RscList.DefaultCellStyle = dataGridViewCellStyle2;
            this.RscList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RscList.EnableHeadersVisualStyles = false;
            this.RscList.Location = new System.Drawing.Point(0, 0);
            this.RscList.Name = "RscList";
            this.RscList.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.RscList.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.RscList.RowHeadersVisible = false;
            this.RscList.Size = new System.Drawing.Size(150, 150);
            this.RscList.TabIndex = 0;
            this.RscList.VirtualMode = true;
            this.RscList.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.RscList_CellFormatting);
            this.RscList.CellValueNeeded += new System.Windows.Forms.DataGridViewCellValueEventHandler(this.RscList_CellValueNeeded);
            this.RscList.SelectionChanged += new System.EventHandler(this.RscList_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Module";
            this.dataGridViewTextBoxColumn1.HeaderText = "Module";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // CurrentName
            // 
            this.CurrentName.DataPropertyName = "CurrentName";
            this.CurrentName.HeaderText = "CurrentName";
            this.CurrentName.Name = "CurrentName";
            this.CurrentName.ReadOnly = true;
            // 
            // CurrentStatus
            // 
            this.CurrentStatus.DataPropertyName = "CurrentStatus";
            this.CurrentStatus.HeaderText = "CurrentStatus";
            this.CurrentStatus.Name = "CurrentStatus";
            this.CurrentStatus.ReadOnly = true;
            // 
            // CurrentFlags
            // 
            this.CurrentFlags.DataPropertyName = "CurrentFlags";
            this.CurrentFlags.HeaderText = "CurrentFlags";
            this.CurrentFlags.Name = "CurrentFlags";
            this.CurrentFlags.ReadOnly = true;
            // 
            // Module
            // 
            this.Module.DataPropertyName = "Module";
            this.Module.HeaderText = "Module";
            this.Module.Name = "Module";
            this.Module.ReadOnly = true;
            // 
            // VirtMem
            // 
            this.VirtMem.HeaderText = "Virt Mem";
            this.VirtMem.Name = "VirtMem";
            this.VirtMem.ReadOnly = true;
            // 
            // PhysMem
            // 
            this.PhysMem.HeaderText = "PhysMem";
            this.PhysMem.Name = "PhysMem";
            this.PhysMem.ReadOnly = true;
            // 
            // ClosestEntity
            // 
            this.ClosestEntity.HeaderText = "Closest Entity";
            this.ClosestEntity.Name = "ClosestEntity";
            this.ClosestEntity.ReadOnly = true;
            // 
            // EntityDistance
            // 
            this.EntityDistance.HeaderText = "Entity Distance";
            this.EntityDistance.Name = "EntityDistance";
            this.EntityDistance.ReadOnly = true;
            // 
            // RscListView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.RscList);
            this.Name = "RscListView";
            this.Load += new System.EventHandler(this.RscListView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RscList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.streamableBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView RscList;
        private System.Windows.Forms.BindingSource streamableBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn indexDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn virtSizeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn physSizeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fileSizeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lSNDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrentName;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrentStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrentFlags;
        private System.Windows.Forms.DataGridViewTextBoxColumn Module;
        private System.Windows.Forms.DataGridViewTextBoxColumn VirtMem;
        private System.Windows.Forms.DataGridViewTextBoxColumn PhysMem;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClosestEntity;
        private System.Windows.Forms.DataGridViewTextBoxColumn EntityDistance;
//        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        //private System.Windows.Forms.DataGridViewTextBoxColumn currentStatusDataGridViewTextBoxColumn;
    }
}
