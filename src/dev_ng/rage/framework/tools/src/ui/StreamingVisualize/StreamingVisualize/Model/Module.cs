﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StreamingVisualize.Model
{
    public class Module : IVersionedSerializable
    {
        public String Name { get; set; }

        // For serialization purposes only
        public int Index { get; set; }

        public void Serialize(VersionedWriter writer)
        {
            writer.Write(Name);
        }

        public void Deserialize(VersionedReader reader, int version)
        {
            Name = reader.ReadString();
        }
    }
}
