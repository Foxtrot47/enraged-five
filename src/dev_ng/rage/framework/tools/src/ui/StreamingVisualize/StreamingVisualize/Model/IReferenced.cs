﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StreamingVisualize.Model
{
    public interface IReferenced
    {
        int GetIndex();
    }
}
