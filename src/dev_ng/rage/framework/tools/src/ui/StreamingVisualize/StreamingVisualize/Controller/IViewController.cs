﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StreamingVisualize.Model;

namespace StreamingVisualize.Controller
{
    public interface IViewController
    {
        // Switch the display to focus on a certain frame
        void SetToFrame(int frameNumber);

        // A new device was registered, or a device has changed.
        void OnDeviceChanged(int deviceIndex, Device device);

        // A streamable has been selected
        void OnSelectStreamable(Streamable streamable);

        // The client information has been updated
        void ClientInfoUpdated();

        // We're in follow mode, and a new frame has been added.
        void OnNewFrame(int frameNumber);

        // A new entity type was registered
        void OnNewEntityType(int entityTypeId, EntityType type);

        // The connection has been terminated
        void OnConnectionLost();

        // Register this view with the system
        void Register(MainController controller, Scene scene);

        // The scene's filter has changed
        void OnFilterChanged();

        // The user has picked a new frame range
        void OnFrameRangeChanged(int firstFrame, int lastFrame);

        // A new playback device has been selected
//        void OnNewPlaybackDevice(String ipAddress);

        // A new entity has been selected
        void OnSelectEntity(Entity entity);
    }
}
