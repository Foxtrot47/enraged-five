﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using StreamingVisualize.Properties;

namespace StreamingVisualize.View.Components
{
    public class BackgroundMap
    {
        public delegate void OnChangeBackground(MapOption mapOption);

        private String DefaultMapFile = "gta5_Normal";


        public class MapOption
        {
            public String MapName { get; set; }

            public String MapFile { get; set; }

            public Bitmap Bitmap;


            public void LoadBitmap()
            {
                if (Bitmap == null && MapFile.Length > 0)
                {
                    Bitmap = new Bitmap(MapFile);
                }
            }
        }

        private List<MapOption> Maps = new List<MapOption>();

        private MapOption DefaultMap;

        static private BackgroundMap Instance = new BackgroundMap();



        public void ReadSetting()
        {
            if (Settings.Default.DefaultMapBg != null)
            {
                DefaultMapFile = Settings.Default.DefaultMapBg;
            }
        }

        public void SaveSetting()
        {
            Settings.Default["DefaultMapBg"] = DefaultMapFile;
            Settings.Default.Save();
        }

        public void ReadMapList(bool forceReload)
        {
            if (!forceReload && Maps.Count > 0)
            {
                // We already have the list.
                return;
            }

            ReadSetting();
            
            // Try current path first.
            String[] files = null;

            try
            {
                files = Directory.GetFiles("maps");
            }
            catch (Exception )
            {
                // Nothing - then try the exe path.
                try
                {
                    String exePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                    files = Directory.GetFiles(String.Format("{0}\\maps", exePath));
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Can't find maps directory: " + ex.Message);
                }
            }

            if (files != null)
            {
                foreach (String file in files)
                {
                    if (file.Length > 3)
                    {
                        String lcTest = file.ToLower();
                        if (lcTest.EndsWith(".jpg") || lcTest.EndsWith(".bmp") || lcTest.EndsWith(".jpeg") || lcTest.EndsWith(".png") || lcTest.EndsWith(".gif"))
                        {
                            MapOption option = new MapOption();

                            option.MapFile = file;
                            option.MapName = file.Substring(0, file.LastIndexOf('.'));

                            int lastPath = option.MapName.LastIndexOfAny(new char[] {'\\', '/'});
                            if (lastPath != -1)
                            {
                                option.MapName = option.MapName.Substring(lastPath+1);
                            }

                            Maps.Add(option);

                            if (DefaultMap == null && DefaultMapFile.Equals(option.MapName, StringComparison.CurrentCultureIgnoreCase))
                            {
                                DefaultMap = option;
                            }
                        }
                    }
                }
            }

            // Finally, add the "none" option.
            MapOption noneOption = new MapOption();
            noneOption.MapFile = "";
            noneOption.MapName = "None";
            Maps.Add(noneOption);
        }

        public MapOption GetDefaultMap()
        {
            ReadMapList(false);

            return DefaultMap;
        }

        private void ChangeBackground(OnChangeBackground changeBgCb, MapOption mapOption, MenuItem item)
        {
            DefaultMapFile = mapOption.MapName;
            SaveSetting();

            // Make sure it's loaded.
            mapOption.LoadBitmap();
            changeBgCb(mapOption);

            // Update the checked item
            Menu parent = item.Parent;

            foreach (MenuItem child in parent.MenuItems)
            {
                child.Checked = (child == item);
            }
        }

        public MenuItem[] CreateMenu(OnChangeBackground changeBgCb)
        {
            MenuItem[] result = new MenuItem[Maps.Count];
            int index = 0;

            foreach (MapOption option in Maps)
            {
                // Gotta cache those out, the lambda function won't work right otherwise.
                MapOption mapOption = option;
                int indexNo = index;

                result[index] = new MenuItem(option.MapName);
                result[index].Click += (sender, e) => ChangeBackground(changeBgCb, mapOption, result[indexNo]);
                result[index].Checked = (mapOption == DefaultMap);

                index++;
            }

            return result;
        }

        static public BackgroundMap GetInstance()
        {
            return Instance;
        }
    }
}
