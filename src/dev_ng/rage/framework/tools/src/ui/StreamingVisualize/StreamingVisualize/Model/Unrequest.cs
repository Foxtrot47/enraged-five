﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StreamingVisualize.Model
{
    public class Unrequest : IVersionedSerializable
    {
/*        public enum ReasonType {
            PURGEREQUEST,
            FREEMEMORY,
            OTHER,
        }*/
        public int Frame { get; set; }
        public Streamable Streamable { get; set; }
//        public ReasonType Reason { get; set; }
        public String Context { get; set; }

        public void Serialize(VersionedWriter writer)
        {
            writer.Write(Frame);
            writer.WriteStreamableRef(Streamable);
            writer.WriteStringRef(Context);         // String ref since version 26
        }

        public void Deserialize(VersionedReader reader, int version)
        {
            Frame = reader.ReadInt();
            Streamable = reader.ReadStreamableRef();

            if (version >= 26)
            {
                Context = reader.ReadStringRef();
            }
            else
            {
                Context = reader.ReadAndRegisterString();
            }
        }
    }
}
