﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace StreamingVisualize.View.Components
{
    public class SimpleTooltip
    {
        const int Y_OFFSET = 16;        // Stay away from the mouse
        private String tooltip = "";

        private float tooltipX, tooltipY;
        private Brush tooltipBrush = new SolidBrush(Color.Yellow);
        private Pen tooltipBorderPen = new Pen(Color.Black);
        private Brush tooltipTextBrush = new SolidBrush(Color.Black);


        public void Draw(Graphics g, Font font, int controlWidth, int controlHeight)
        {
            if (tooltip.Length > 0)
            {
                String tooltipString = tooltip;
                // MeasureString crashes with strings >6535 characters with the most useless exception ever.

                if (tooltipString.Length > 65535)
                {
                    tooltipString = tooltipString.Substring(0, 65535);
                }

                SizeF textSize = g.MeasureString(tooltipString, font);

                float yPos = tooltipY + Y_OFFSET;

                if (yPos + textSize.Height > controlHeight && textSize.Height < tooltipY)
                {
                    yPos = tooltipY - textSize.Height - 1;
                }

                if (tooltipX + textSize.Width > controlWidth && textSize.Width < tooltipX)
                {
                    tooltipX = tooltipX - textSize.Width - 1;
                }



                g.FillRectangle(tooltipBrush, tooltipX, yPos, textSize.Width, textSize.Height);
                g.DrawRectangle(tooltipBorderPen, tooltipX, yPos, textSize.Width, textSize.Height);
                g.DrawString(tooltipString, font, tooltipTextBrush, tooltipX, yPos);
            }
        }

        public void SetToolTip(String text)
        {
            tooltip = text;
        }

        public void ResetToolTip()
        {
            tooltip = "";
        }

        public void MouseMove(MouseEventArgs e)
        {
            tooltipX = (float)e.X;
            tooltipY = (float)e.Y;
        }
    }
}
