﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace StreamingVisualize.Model
{
    public class Filter
    {
        public Module Module { get; set; }

        public String ResourceNameSubstring = null;

        public String ResourceNameExcludeSubstring = null;

        Regex InclusionRegex = null;

        Regex ExclusionRegex = null;


        public bool IsNameFilteredOut(String name)
        {
            if (InclusionRegex != null && !InclusionRegex.IsMatch(name))
            {
                return true;
            }

            if (ExclusionRegex != null && ExclusionRegex.IsMatch(name))
            {
                return true;
            }

            return false;
        }

        public bool IsFilteredOut(Streamable streamable)
        {
            if (Module != null && streamable.Module != Module)
            {
                return true;
            }

            String streamableName = streamable.CurrentName;

            if (IsNameFilteredOut(streamableName))
            {
                return true;
            }

            return false;
        }

        public void Update()
        {
            if (ResourceNameSubstring != null && ResourceNameSubstring.Length > 0)
            {
                InclusionRegex = new Regex(ResourceNameSubstring, RegexOptions.Compiled | RegexOptions.IgnoreCase);
            }
            else
            {
                InclusionRegex = null;
            }

            if (ResourceNameExcludeSubstring != null && ResourceNameExcludeSubstring.Length > 0)
            {
                ExclusionRegex = new Regex(ResourceNameExcludeSubstring, RegexOptions.Compiled | RegexOptions.IgnoreCase);
            }
            else
            {
                ExclusionRegex = null;
            }
        }

        public bool IsActive()
        {
            return (Module != null || ResourceNameSubstring != null || ResourceNameExcludeSubstring != null);
        }
    }
}
