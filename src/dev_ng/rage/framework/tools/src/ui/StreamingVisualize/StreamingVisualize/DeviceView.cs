﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StreamingVisualize.Model;
using StreamingVisualize.Controller;

namespace StreamingVisualize.View
{
    public partial class DeviceView : UserControl, IViewController
    {
        public MainController Controller { get; set; }

        private List<DeviceEvent> deviceEvents;

        public Device Device { get; set; }
        public int DeviceIndex { get; set; }

        // If true, we're changing lists around. Don't respond to selection changes.
        private bool ChangingSelection;



        public DeviceView()
        {
            InitializeComponent();
        }

        public void SetDevice(Device device)
        {
            if (device != null)
            {
                ChangingSelection = true;

                Device = device;
                DeviceName.Text = device.Name;

                deviceEvents = device.DeviceEvents;
                DeviceEvents.RowCount = deviceEvents.Count;

                ChangingSelection = false;
            }
        }

        private void RefreshData()
        {
            SetDevice(Device);
        }

        private void SetToFrameCore(int frameNumber)
        {
            if (Device != null)
            {
                // Switch to this frame - unless we're already there.
                // We could have several records per frame, so don't switch around there.
                if (DeviceEvents.SelectedCells.Count > 0)
                {
                    int index = DeviceEvents.SelectedCells[0].RowIndex;

                    if (index >= 0 && index < deviceEvents.Count)
                    {
                        DeviceEvent deviceEvent = deviceEvents[index];

                        if (deviceEvent.Frame == frameNumber)
                        {
                            return;
                        }
                    }
                }

                ChangingSelection = true;

                int row = 0;
                
                Device.FrameToRow.TryGetValue(frameNumber, out row);

                if (DeviceEvents.RowCount > row)
                {
//                    DeviceEvents.Rows[row].Selected = true;
                    DeviceEvents.CurrentCell = DeviceEvents[0, row];
                    DeviceEvents.FirstDisplayedCell = DeviceEvents.CurrentCell;
                }

                ChangingSelection = false;
            }
        }

        public void RefreshDeferred()
        {
            BeginInvoke((Action)delegate { RefreshData(); });
        }

        private void Refresh_Click(object sender, EventArgs e)
        {
//            DeviceEvents.DataSource = null;
//            DeviceEvents.DataSource = deviceEvents;

            DeviceEvents.RowCount = deviceEvents.Count;
        }

        private void DeviceEvents_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void DeviceEvents_CellValueNeeded(object sender, DataGridViewCellValueEventArgs e)
        {
            int row = e.RowIndex;

            if (deviceEvents == null)
            {
                return;
            }

            if (row < 0 || row >= deviceEvents.Count)
            {
                return;
            }

            DeviceEvent devEvent = deviceEvents[row];

            switch (e.ColumnIndex)
            {
                case 0:
                    e.Value = devEvent.Frame;
                    break;

                case 1:
                    e.Value = devEvent.Time;
                    break;

                case 2:
                    e.Value = devEvent.LSN;
                    break;

                case 3:
                    e.Value = devEvent.Streamable;
                    break;
            }
        }

        private void DeviceEvents_SelectionChanged(object sender, EventArgs e)
        {
            if (ChangingSelection)
            {
                return;
            }

            if (DeviceEvents.SelectedCells.Count > 0)
            {
                int index = DeviceEvents.SelectedCells[0].RowIndex;

                if (index >= 0 && index < deviceEvents.Count)
                {
                    DeviceEvent devEvent = deviceEvents[index];

                    Controller.SetToFrame(devEvent.Frame);
                    Controller.OnSelectStreamable(devEvent.Streamable);
                }
            }
        }

        // Switch the display to focus on a certain frame
        public void SetToFrame(int frameNumber)
        {
            BeginInvoke((Action)delegate { SetToFrameCore(frameNumber); });
        }

        // If a new frame was added
        public void OnFrameChanged()
        {

        }

        // A new device was registered, or a device has changed.
        public void OnDeviceChanged(int deviceIndex, Device device)
        {
            if (deviceIndex == DeviceIndex)
            {
                BeginInvoke((Action)delegate { SetDevice(device); });
            }
        }

        // A streamable has been selected
        public void OnSelectStreamable(Streamable streamable)
        {

        }

        // The client information has been updated
        public void ClientInfoUpdated()
        {

        }

        // We're in follow mode, and a new frame has been added.
        public void OnNewFrame(int frameNumber)
        {
            RefreshDeferred();
        }

        // A new entity type was registered
        public void OnNewEntityType(int entityTypeId, EntityType type)
        {

        }

        // The connection has been terminated
        public void OnConnectionLost() { }
    }
}
