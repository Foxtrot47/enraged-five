﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace StreamingVisualize.View.Components
{
    public class ViewDefinition
    {
        public String Name;

        public delegate UserControl CreateView();

        public CreateView CreateViewCb;

        public const int TYPE_FRAME_OVERVIEW = 0;
        public const int TYPE_FRAME_DETAILS = 1;
        public const int TYPE_DEVICE_0 = 2;
        public const int TYPE_DEVICE_1 = 3;
        public const int TYPE_DEVICE_FRAME_0 = 4;
        public const int TYPE_DEVICE_FRAME_1 = 5;
        public const int TYPE_ENTITY = 6;
        public const int TYPE_RESOURCE = 7;


        static ViewDefinition[] ViewTypes = new ViewDefinition[] {
            new ViewDefinition("Frame Overview", () => { return new FrameOverview(); } ),
            new ViewDefinition("Frame Details", () => { return new FrameView(); } ),
            new ViewDefinition("Device View (Disc)", () => { return new DeviceView(0); } ),
            new ViewDefinition("Device View (HDD)", () => { return new DeviceView(1); } ),
            new ViewDefinition("Device Frame State (Disc)", () => { return new DeviceFrameState(0); } ),
            new ViewDefinition("Device Frame State (HDD)", () => { return new DeviceFrameState(1); } ),
            new ViewDefinition("Entity View", () => { return new EntityView(); } ),
            new ViewDefinition("Resource View", () => { return new ResourceView(); } ),
            new ViewDefinition("Stats View", () => { return new StatsView(); } ),
            new ViewDefinition("Resource List View", () => { return new RscListView(); } ),
            new ViewDefinition("Analysis View", () => { return new AnalysisView(); } ),
            new ViewDefinition("Dependents View", () => { return new DependentsView(); } ),
            new ViewDefinition("Entity History View", () => { return new EntityHistoryView(); } ),
            new ViewDefinition("Scene Streamer View", () => { return new SceneStreamerView(); } ),
            new ViewDefinition("Streaming Timeline", () => { return new StreamingTimeline(); } ),
            new ViewDefinition("Disc Layout View", () => { return new DiscLayoutView(); } ),
            new ViewDefinition("Memory View", () => { return new MemoryView(); } ),
        };

        public static int ViewTypeCount { get { return ViewTypes.Length; } }

        public static ViewDefinition GetDefinition(int viewType)
        {
            return ViewTypes[viewType];
        }

        


        public ViewDefinition(String name, CreateView createViewCb)
        {
            this.Name = name;
            this.CreateViewCb = createViewCb;
        }

    
    }

}
