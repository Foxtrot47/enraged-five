﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace StreamingVisualize.Model
{
    public class Entity : IVersionedSerializable
    {
        public static int TotalCount;
        public static int TotalStateCount;
        public static int TotalPosStateCount;


        public class NewState : IFrameState, ICloneable, IVersionedSerializable, IComparable<NewState>
        {
            public enum LodStates
            {
                LOD_OK,             // Desired LOD available
                LOD_LOW,            // Had to force lower LOD
                LOD_MISSING,        // Entity not rendering due to missing parent LOD
            }
/*
            public const int VISIBLE_PVS = (1 << 0);
            public const int VISIBLE_RENDERPHASE = (1 << 1);
            public const int VISIBLE_GBUF = (1 << 2);
*/
            public const int FLAG_LOW_PRIORITY_STREAM = (1 << 22);

            public Streamable Streamable { get; set; }
            public Streamable MapDataStreamable { get; set; }
            public int Frame { get; set; }
            public int GUID { get; set; }
            public int EntityFlags { get; set; }
            public int PhaseFlags { get; set; }
            public int OptionFlags { get; set; }
            public float LodDistance { get; set; }
//            public Entity ParentLod { get; set; }
            public int ParentLodEntityId { get; set; }
            public byte LoadedStateStorage;
            public byte EntityType { get; set; }
            public byte LodType { get; set; }
            //public byte VisibleFlags { get; set; }
            
            public byte LodStateStorage = (byte) LodStates.LOD_OK;

            public Streamable.Status LoadedState
            {
                get { return (Streamable.Status)LoadedStateStorage; }
                set { LoadedStateStorage = (byte)value; }
            }

            public LodStates LodState 
            {
                get { return (LodStates) LodStateStorage; }
                set { LodStateStorage = (byte) value; }
            }

            public NewState()
            {
                ++TotalStateCount;
            }

            public bool IsDeleted()
            {
                return Streamable == null;
            }

            public void SetDeleted()
            {
                Streamable = null;
            }

            public bool IsVisible(Scene scene)
            {
                return (PhaseFlags & scene.VisFlagsDef.GbufFlag) != 0;
            }

            public void FromOldState(OldState state)
            {
                Streamable = state.Streamable;
                LoadedStateStorage = (byte) state.LoadedState;
                //VisibleFlags = (byte)((state.Visible) ? VISIBLE_PVS | VISIBLE_RENDERPHASE | VISIBLE_GBUF : 0);
                OptionFlags = 0;
                PhaseFlags = (state.Visible) ? -1 : 0;
                LodType = (byte)state.LodType;
                LodState = state.LodState;
            }

            public void Serialize(VersionedWriter writer)
            {
                writer.WriteStreamableRef(Streamable);
                writer.WriteStreamableRef(MapDataStreamable);
                writer.Write(GUID);
                writer.Write(LoadedStateStorage);
                writer.Write(LodStateStorage);

                //writer.Write(VisibleFlags);   // Obsolete since version 35

                writer.Write(LodType);

                // Version 24: Entity type.
                writer.Write((byte)EntityType);

                // Version 34: Entity flags.
                writer.Write(EntityFlags);

                // Version 35: VisFlags.
                writer.Write(PhaseFlags);
                writer.Write(OptionFlags);

                // Version 36: LOD info.
                writer.Write(LodDistance);
//                writer.WriteEntityRef(ParentLod);
                writer.Write(ParentLodEntityId);
            }

            public void Deserialize(VersionedReader reader, int version)
            {
                Streamable = reader.ReadStreamableRef();
                MapDataStreamable = reader.ReadStreamableRef();
                GUID = reader.ReadInt();
                LoadedStateStorage = (byte)reader.ReadByte();
                LodStateStorage = (byte)reader.ReadByte();

                if (version < 35)
                {
                    byte visFlags = (byte)reader.ReadByte();
                    PhaseFlags = (visFlags != 0) ? -1 : 0;
                    OptionFlags = 0;
                }

                LodType = (byte)reader.ReadByte();

                if (version >= 24)
                {
                    EntityType = reader.ReadByte();
                }

                if (version >= 34)
                {
                    EntityFlags = reader.ReadInt();
                }

                if (version >= 35)
                {
                    PhaseFlags = reader.ReadInt();
                   OptionFlags = reader.ReadInt();
                }

                if (version >= 36)
                {
                    LodDistance = reader.ReadFloat();
//                    ParentLod = reader.ReadEntityDef();
                    ParentLodEntityId = reader.ReadInt();
                }
            }

            public Object Clone()
            {
                NewState obj = new NewState();

                // When cloning, only keep the entries that are still ongoing.
                obj.Streamable = Streamable;
                obj.MapDataStreamable = MapDataStreamable;
                obj.LoadedState = LoadedState;
                obj.Frame = Frame;
                obj.LodState = LodState;
//                obj.VisibleFlags = VisibleFlags;
                obj.LodType = LodType;
                obj.EntityType = EntityType;
                obj.EntityFlags = EntityFlags;
                obj.PhaseFlags = PhaseFlags;
                obj.OptionFlags = OptionFlags;
                obj.LodDistance = LodDistance;
                obj.ParentLodEntityId = ParentLodEntityId;

                return obj;
            }

            public int GetFrameNumber()
            {
                return Frame;
            }

            public void SetFrameNumber(int frameNumber)
            {
                Frame = frameNumber;
            }

            public int CompareTo(NewState s)
            {
                if (s.Streamable == Streamable && s.MapDataStreamable == MapDataStreamable && s.LoadedState == LoadedState &&
                    s.LodState == LodState && s.LodType == LodType && s.PhaseFlags == PhaseFlags && s.OptionFlags == OptionFlags &&
                    s.LodDistance == LodDistance && s.ParentLodEntityId == ParentLodEntityId)
                {
                    return 0;
                }

                return 1;
            }
        }

        // Describes the state of an entity at any given frame.
        public class PositionState : IFrameState, ICloneable, IVersionedSerializable, IComparable<PositionState>
        {
            // If BoxMin.X (and BoxMax.X) is DELETED_X, this entity currently doesn't exist.
            const float DELETED_X_OLD = -99999.0f;
            const short DELETED_X = -32768;

            public Vector16 BoxMin { get; set; }
            public Vector16 BoxMax { get; set; }
            public Vector16 SphereCenter { get; set; }
            public float SphereRadius { get; set; }
            public int Frame { get; set; }

            public PositionState()
            {
                ++TotalPosStateCount;
            }

            // Make it slightly larger if it's really tiny, just so it's visible in the entity view.
            public void AdjustPosition()
            {
                if (BoxMax.X - BoxMin.X < 0.5f)
                {
                    BoxMin.X -= 0.25f;
                    BoxMax.X += 0.25f;
                }

                if (BoxMax.Y - BoxMin.Y < 0.5f)
                {
                    BoxMin.Y -= 0.25f;
                    BoxMax.Y += 0.25f;
                }
            }

            public void FromOldState(OldState state)
            {
                BoxMin = state.BoxMin.ToVector16();
                BoxMax = state.BoxMax.ToVector16();
                CreateSphereFromBox();
            }

            private void CreateSphereFromBox()
            {
                SphereCenter = BoxMax.Add(BoxMin).Scale(0.5f);
                SphereRadius = BoxMax.Sub(BoxMin).GetHighestComponent() * 0.5f;
            }

            public bool IsDeleted()
            {
                return BoxMin.iX == DELETED_X;
            }

            public void SetDeleted()
            {
                SphereCenter = BoxMin = BoxMax = new Vector16(DELETED_X, DELETED_X, DELETED_X);

            }

            public float GetCenterX()
            {
                return (BoxMax.X + BoxMin.X) * 0.5f;
            }

            public float GetCenterY()
            {
                return (BoxMax.Y + BoxMin.Y) * 0.5f;
            }

            public Vector GetCenter()
            {
                return new Vector((BoxMax.X + BoxMin.X) * 0.5f, (BoxMax.Y + BoxMin.Y) * 0.5f, (BoxMax.Z + BoxMin.Z) * 0.5f);
            }

            public float GetRadius()
            {
                return Math.Max(Math.Max(BoxMax.X - BoxMin.X, BoxMax.Y - BoxMin.Y), BoxMax.Z - BoxMin.Z);
            }

            public void Serialize(VersionedWriter writer)
            {
                writer.Write(BoxMin);
                writer.Write(BoxMax);

                // Version 36: Bounding sphere
                writer.Write(SphereCenter);
                writer.Write(SphereRadius);
            }

            public void Deserialize(VersionedReader reader, int version)
            {
                if (version >= 25)
                {
                    BoxMin = reader.ReadVector16();
                    BoxMax = reader.ReadVector16();
                }
                else
                {
                    BoxMin = reader.ReadVector().ToVector16();
                    BoxMax = reader.ReadVector().ToVector16();

                    if (BoxMin.X == DELETED_X_OLD)
                    {
                        BoxMin.iX = DELETED_X;
                    }
                }

                if (version >= 36)
                {
                    SphereCenter = reader.ReadVector16();
                    SphereRadius = reader.ReadFloat();
                }
                else
                {
                    CreateSphereFromBox();
                }
            }

            public Object Clone()
            {
                PositionState obj = new PositionState();

                // When cloning, only keep the entries that are still ongoing.
                obj.BoxMin = BoxMin;
                obj.BoxMax = BoxMax;

                obj.SphereCenter = SphereCenter;
                obj.SphereRadius = SphereRadius;

                return obj;
            }

            public int GetFrameNumber()
            {
                return Frame;
            }

            public void SetFrameNumber(int frameNumber)
            {
                Frame = frameNumber;
            }

            public int CompareTo(PositionState s)
            {
                if (s.BoxMin.CompareTo(BoxMin) == 0 && s.BoxMax.CompareTo(BoxMax) == 0 && s.SphereCenter.CompareTo(SphereCenter) == 0 && s.SphereRadius == SphereRadius)
                {
                    return 0;
                }

                return 1;
            }
        }


        // Describes the state of an entity at any given frame.
        public class OldState : IFrameState, ICloneable, IVersionedSerializable, IComparable<OldState>
        {
            // If BoxMin.X (and BoxMax.X) is DELETED_X, this entity currently doesn't exist.
            const float DELETED_X = -99999.0f;

            public Vector BoxMin { get; set; }
            public Vector BoxMax { get; set; }

            public Streamable Streamable { get; set; }
            public int Frame { get; set; }
            public Streamable.Status LoadedState { get; set; }
            public int LodType { get; set; }

            public NewState.LodStates LodState = NewState.LodStates.LOD_OK;

            public bool Visible { get; set; }

            public bool IsDeleted()
            {
                return BoxMin.X == DELETED_X;
            }

            public void SetDeleted()
            {
                BoxMin = BoxMax = new Vector(DELETED_X, DELETED_X, DELETED_X);
            }

            public float GetCenterX()
            {
                return (BoxMax.X + BoxMin.X) * 0.5f;
            }

            public float GetCenterY()
            {
                return (BoxMax.Y + BoxMin.Y) * 0.5f;
            }

            public Vector GetCenter()
            {
                return new Vector((BoxMax.X + BoxMin.X) * 0.5f, (BoxMax.Y + BoxMin.Y) * 0.5f, (BoxMax.Z + BoxMin.Z) * 0.5f);
            }

            public void Serialize(VersionedWriter writer)
            {
                writer.Write(BoxMin);
                writer.Write(BoxMax);
                writer.WriteStreamableRef(Streamable);
                writer.Write(Frame);
                writer.Write((byte)LoadedState);

                // Version 10: LOD state.
                writer.Write((byte)LodState);

                // Version 16: Visibility state.
                writer.Write((byte) ((Visible) ? 1 : 0));

                // Version 20: Lod type.
                writer.Write((byte)LodType);
            }

            public void Deserialize(VersionedReader reader, int version)
            {
                BoxMin = reader.ReadVector();
                BoxMax = reader.ReadVector();
                Streamable = reader.ReadStreamableRef();
                Frame = reader.ReadInt();
                LoadedState = (Streamable.Status)reader.ReadByte();

                if (version > 9)
                {
                    LodState = (NewState.LodStates)reader.ReadByte();
                }

                if (version >= 16)
                {
                    Visible = reader.ReadByte() != 0;
                }
                else
                {
                    Visible = true;
                }

                if (version >= 20)
                {
                    LodType = reader.ReadByte();
                }
                else
                {
                    LodType = 0;
                }
            }

            public Object Clone()
            {
                OldState obj = new OldState();

                // When cloning, only keep the entries that are still ongoing.
                obj.BoxMin = BoxMin;
                obj.BoxMax = BoxMax;
                obj.Streamable = Streamable;
                obj.LoadedState = LoadedState;
                obj.Frame = Frame;
                obj.LodState = LodState;
                obj.Visible = Visible;
                obj.LodType = LodType;

                return obj;
            }

            public int CompareTo(OldState o)
            {
                bool equal =
                    (BoxMin.Equals(o.BoxMin)) &&
                    (BoxMax.Equals(o.BoxMax)) &&
                    (Streamable == o.Streamable) &&
                    (LoadedState == o.LoadedState) &&
                    (Frame == o.Frame) &&
                    (LodState == o.LodState) &&
                    (Visible == o.Visible) &&
                    (LodType == o.LodType);

                return (equal) ? 0 : 1;
            }

            public int GetFrameNumber()
            {
                return Frame;
            }

            public void SetFrameNumber(int frameNumber)
            {
                Frame = frameNumber;
            }
        }

        // Full history of this entity, sorted by frame number.
        //public PerFrameState<State> History = new PerFrameState<State>();

        public PerFrameState<NewState> NewHistory = new PerFrameState<NewState>();
        public PerFrameState<PositionState> PositionHistory = new PerFrameState<PositionState>();

        public NewState CurrentState { get { return NewHistory.CurrentState; } }

        public static int NextAutoIndex;

        public int EntityId { get; set; }

        //public Streamable.Status LoadedState { get; set; }
        public Streamable Streamable { get { return (CurrentState != null) ? CurrentState.Streamable : null; } }

        // LOD state in the present frame - this will be reset to LOD_OK every frame, and used to see whether or not
        // we need to update our entity state.
        public NewState.LodStates CurrentLodState = NewState.LodStates.LOD_OK;
        public NewState.LodStates PrevLodState = NewState.LodStates.LOD_OK;

        // True if this entity is on the global problem child list, i.e. its LOD state is not LOD_OK.
        public bool IsOnWatchList = false;



        public Entity()
        {
            ++TotalCount;
        }

        /* Returns the size of the current bounding box as the length of X or Y, whichever is higher.
         * 0 if the bounding box doesn't have a current state.
         */       
        public float GetCurrentBoundingBoxSize()
        {
            PositionState currentState = PositionHistory.CurrentState;

            if (currentState == null)
            {
                return 0.0f;
            }

            return Math.Max(currentState.BoxMax.X - currentState.BoxMin.X, currentState.BoxMax.Y - currentState.BoxMin.Y);
        }

        public void ChangeLoadedState(int frameNumber, Streamable.Status status, Scene scene)
        {
            if (NewHistory.PresentState.LoadedState != status)
            {
                NewState state = NewHistory.GetStateForWriting(frameNumber, scene);
                state.LoadedState = status;
                NewHistory.ValidateDupe();
            }
        }

        public void SetToState(int frameNumber)
        {
            NewHistory.SetToState(frameNumber);
            PositionHistory.SetToState(frameNumber);
        }

        public void Serialize(VersionedWriter writer)
        {
   //         writer.Write((byte)0 /*LoadedState*/);
 //           writer.Write((byte)Type);

            // Version 19: Entity ID.
            writer.Write(EntityId);

            // Version 23: Separate states.
            writer.Write(NewHistory);
            writer.Write(PositionHistory);
        }

        public void Deserialize(VersionedReader reader, int version)
        {
            if (version < 18)
            {
                // Unused index
                reader.ReadInt();
            }

            if (version < 24)
            {
                /*LoadedState = (Streamable.Status)*/
                reader.ReadByte();
            }

            int globalType = 0;

            if (version < 24)
            {
                globalType = reader.ReadByte();
            }

            if (version > 9)
            {
                if (version < 23)
                {
                    PerFrameState<OldState> oldHistory = new PerFrameState<OldState>();
                    reader.ReadPerFrameState(oldHistory);

                    for (int x = 0; x < oldHistory.StateCount; x++)
                    {
                        OldState oldState = oldHistory.GetStateByIndex(x);
                        int frame = oldState.GetFrameNumber();
                        NewHistory.GetStateForWriting(frame, null).FromOldState(oldState);

                        int newStates = NewHistory.StateCount;
                        if (newStates > 1 && NewHistory.GetStateByIndex(newStates-1).CompareTo(NewHistory.GetStateByIndex(newStates-2)) == 0)
                        {
                            NewHistory.PopLast();
                        }

                        PositionHistory.GetStateForWriting(frame, null).FromOldState(oldState);

                        newStates = PositionHistory.StateCount;
                        if (newStates > 1 && PositionHistory.GetStateByIndex(newStates-1).CompareTo(PositionHistory.GetStateByIndex(newStates-2)) == 0)
                        {
                            PositionHistory.PopLast();
                        }
                    }
                }
            }
            else
            {
                // Screw that, I'm not supporting v9 anymore.
            }

            if (version < 24)
            {
                for (int x = 0; x < NewHistory.StateCount; x++)
                {
                    NewHistory.GetStateByIndex(x).EntityType = (byte)globalType;
                }
            }
            /*
            if (NewHistory.CurrentState != null)
            {
                if (Streamable != null)
                {
                    Streamable.Entities.Add(this);
                }
            }
            */
            if (version >= 19)
            {
                EntityId = reader.ReadInt();
            }

            if (version >= 23)
            {
                reader.ReadPerFrameState(NewHistory);
                reader.ReadPerFrameState(PositionHistory);

                if (version == 24)
                {
                    // Shit, I fucked up again.
                    for (int x = 1; x < NewHistory.StateCount; x++)
                    {
                        if (NewHistory.GetStateByIndex(x).CompareTo(NewHistory.GetStateByIndex(x - 1)) == 0)
                        {
                            NewHistory.DeleteStateByIndexUnsafe(x);
                            x--;
                        }
                    }

                    for (int x = 1; x < PositionHistory.StateCount; x++)
                    {
                        if (PositionHistory.GetStateByIndex(x).CompareTo(PositionHistory.GetStateByIndex(x - 1)) == 0)
                        {
                            PositionHistory.DeleteStateByIndexUnsafe(x);
                            x--;
                        }
                    }
                }
            }
        }

        public void DeletePriorHistory(int frame)
        {
            int stateIndex = PositionHistory.GetStateIndexAt(frame);

            if (stateIndex > 0)
            {
                PositionHistory.DeleteAllBeforeIndex(stateIndex);
            }
        }
    }
}
