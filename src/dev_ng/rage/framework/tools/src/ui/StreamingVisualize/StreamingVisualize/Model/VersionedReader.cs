﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace StreamingVisualize.Model
{
    public class VersionedReader
    {
        private BinaryReader Reader;
        private int Version;
        private Scene scene;


        public class DelayedStrRef
        {
            public IList<Streamable> List;
            public int Index;
        }

        List<DelayedStrRef> DelayedRefs = new List<DelayedStrRef>();


        public VersionedReader(BinaryReader reader)
        {
            this.Reader = reader;
        }

        public VersionedReader(Scene scene, Stream stream)
        {
            this.scene = scene;
            this.Reader = new BinaryReader(stream);
        }

        public bool IsCompressed()
        {
            return Version > 5;
        }

        public void SetStream(Stream stream)
        {
            Reader = new BinaryReader(stream);
        }

        public void Close()
        {
            Reader.Close();
        }

        private void VerifyCount(int count)
        {
            if (count < 0 || count > 0x1fffffff)
            {
                throw new Exception("Count value seems invalid (" + count + ") - is the file corrupted?");
            }
        }

        public bool ReadHeader()
        {
            int Magic = ReadInt();

            if (Magic != VersionedWriter.MAGIC_HEADER)
            {
                throw new Exception("Bad file header - is this really a StreamingVisualize(TM) file?");
            }

            Version = ReadInt();

            if (Version < 1 || Version > VersionedWriter.FILE_VERSION)
            {
                throw new Exception("Unknown file version (" + Version + ") - you may need to update your StreamingVisualize(TM) client.");
            }

            return true;
        }

        public int ReadInt()
        {
            return Reader.ReadInt32();
        }

        public int ReadBytes(byte[] result, int size)
        {
            return Reader.Read(result, 0, size);
        }

        public uint ReadUint()
        {
            return Reader.ReadUInt32();
        }

        public float ReadFloat()
        {
            return Reader.ReadSingle();
        }

        public short ReadShort()
        {
            return Reader.ReadInt16();
        }

        public byte ReadByte()
        {
            return Reader.ReadByte();
        }

        public bool ReadBool()
        {
            return Reader.ReadByte() != 0;
        }

        public void Read<T>(T value) where T : IVersionedSerializable
        {
            value.Deserialize(this, Version);
        }

        public String ReadStringRef()
        {
            int stringId = ReadInt();
            return scene.StringPool.GetStringFromId(stringId);
        }

        public void ReadList<T>(IList<T> value) where T : IVersionedSerializable, new() 
        {
            int count = ReadInt();
            VerifyCount(count);

            value.Clear();

            for (int x = 0; x < count; x++)
            {
                T obj = new T();
                obj.Deserialize(this, Version);

                value.Add(obj);
            }
        }

        public void ReadIntDictionary<T>(Dictionary<int, T> value) where T : IVersionedSerializable, new()
        {
            int count = ReadInt();
            VerifyCount(count);

            for (int x = 0; x < count; x++)
            {
                int key = ReadInt();
                T obj = new T();
                Read(obj);

                value[key] = obj;
            }
        }


        public void ReadSparseArray<T>(T[] value) where T : IVersionedSerializable, new() 
        {
            // Write capacity...
            int capacity = ReadInt();
            VerifyCount(capacity);            

            if (capacity > value.Length)
            {
                throw new Exception("Capacity of type " + value.GetType().FullName + " array is " + capacity + ", exceeds " + value.Length);
            }

            int length = ReadInt();
            VerifyCount(length);

            for (int x=0; x<length; x++)
            {
                int index = ReadInt();

                if (index < 0 || index >= capacity)
                {
                    throw new Exception("Invalid index in sparse array - " + index + " out of " + capacity + ", at " + x);
                }

                T obj = new T();
                obj.Deserialize(this, Version);

                value[index] = obj;
            }
        }

        public Vector ReadVector()
        {
            Vector v = new Vector();

            v.X = ReadFloat();
            v.Y = ReadFloat();
            v.Z = ReadFloat();

            return v;
        }

        public Vector16 ReadVector16()
        {
            Vector16 v = new Vector16();

            v.iX = ReadShort();
            v.iY = ReadShort();
            v.iZ = ReadShort();

            return v;
        }

        public Box ReadBox()
        {
            Box b = new Box();

            b.BoxMin = ReadVector();
            b.BoxMax = ReadVector();

            return b;
        }

        public Box16 ReadBox16()
        {
            Box16 b = new Box16();

            b.BoxMin = ReadVector16();
            b.BoxMax = ReadVector16();

            return b;
        }

        public void AddStreamableRefNowOrLater(IList<Streamable> StrList)
        {
            int refValue = ReadInt();

            if (refValue != -1)
            {
                if (refValue < 0)
                {
                    StrList.Add(scene.DummyStreamableFixupArray[-2 - refValue]);
                }
                else
                {
                    Streamable streamable = scene.GetStreamableByIndex(refValue);

                    if (streamable != null)
                    {
                        StrList.Add(streamable);
                    }
                    else
                    {
                        DelayedStrRef delRef = new DelayedStrRef();
                        delRef.Index = refValue;
                        delRef.List = StrList;

                        DelayedRefs.Add(delRef);
                    }
                }
            }
        }

        public void HandleDelayedRefs()
        {
            foreach (DelayedStrRef delRef in DelayedRefs)
            {
                delRef.List.Add(scene.GetOrCreateStreamableByIndex(delRef.Index));
            }

            DelayedRefs.Clear();
        }

        public Streamable ReadStreamableRef()
        {
            int refValue = ReadInt();

            if (refValue == -1)
            {
                return null;
            }

            if (refValue < 0)
            {
                return scene.DummyStreamableFixupArray[-2 - refValue];
            }

            return scene.GetOrCreateStreamableByIndex(refValue);
        }

        public Entity ReadEntityDef()
        {
            int refValue = ReadInt();

            if (refValue == -1 || refValue == 0)
            {
                return null;
            }

            return scene.CurrentFrame.EntityMap[refValue];
        }

        public void ReadPerFrameState<T>(PerFrameState<T> state) where T : class, IFrameState, ICloneable, IComparable<T>, IVersionedSerializable, new()
        {
            state.Deserialize(this, Version);
        }



        public Module ReadModuleRef()
        {
            int refValue = ReadShort();

            if (refValue == -1)
            {
                return null;
            }
            else
            {
                return scene.GetModule(refValue);
            }
        }

        public String ReadString()
        {
            return Reader.ReadString();
        }

        // For compatibility: Read an old-style string and add it to the string pool
        public String ReadAndRegisterString()
        {
            return scene.StringPool.GetString(Reader.ReadString());
        }

        public void VerifyCheckpoint()
        {
            if (ReadInt() != VersionedWriter.MAGIC_CHECKPOINT)
            {
                throw new Exception("Checkpoint verification failed - is the file corrupted?");
            }
        }
    }
}
