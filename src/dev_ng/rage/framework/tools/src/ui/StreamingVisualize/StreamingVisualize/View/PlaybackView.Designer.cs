﻿namespace StreamingVisualize.View
{
    partial class PlaybackView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.IPAddress = new System.Windows.Forms.TextBox();
            this.EnablePlayback = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Console IP";
            // 
            // IPAddress
            // 
            this.IPAddress.Location = new System.Drawing.Point(96, 5);
            this.IPAddress.Name = "IPAddress";
            this.IPAddress.Size = new System.Drawing.Size(178, 20);
            this.IPAddress.TabIndex = 1;
            // 
            // EnablePlayback
            // 
            this.EnablePlayback.Location = new System.Drawing.Point(10, 51);
            this.EnablePlayback.Name = "EnablePlayback";
            this.EnablePlayback.Size = new System.Drawing.Size(140, 27);
            this.EnablePlayback.TabIndex = 2;
            this.EnablePlayback.Text = "Enable Playback";
            this.EnablePlayback.UseVisualStyleBackColor = true;
            this.EnablePlayback.Click += new System.EventHandler(this.EnablePlayback_Click);
            // 
            // PlaybackView
            // 
            this.AcceptButton = this.EnablePlayback;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(282, 85);
            this.Controls.Add(this.EnablePlayback);
            this.Controls.Add(this.IPAddress);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "PlaybackView";
            this.Text = "Playback";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PlaybackView_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox IPAddress;
        private System.Windows.Forms.Button EnablePlayback;
    }
}