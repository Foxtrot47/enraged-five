﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StreamingVisualize.Model;
using StreamingVisualize.Controller;
using StreamingVisualize.View.Components;

namespace StreamingVisualize.View
{
    public partial class DeviceView : UserControl, IViewController
    {
        private ViewContextMenu viewContextMenu = new ViewContextMenu();

        public MainController Controller { get; set; }
		private Scene Scene;
        private List<DeviceEvent> deviceEvents;
		private List<DeviceEvent> filteredDeviceEvents;
		public Dictionary<int, int> filteredFrameToRow = new Dictionary<int, int>();

        public Device Device { get; set; }
        public int DeviceIndex { get; set; }

        // If true, we're changing lists around. Don't respond to selection changes.
        private bool ChangingSelection;

        private bool ShowSeeks = true;

		private uint EventTypesToShow = 0x00000000;
		private int firstFrame = 0;
		private int lastFrame = 0;

        public DeviceView(int deviceIndex)
        {
            InitializeComponent();

            DeviceIndex = deviceIndex;
        }

        public void SetDevice(Device device)
        {
            if (device != null)
            {
                ChangingSelection = true;

                Device = device;
                DeviceName.Text = device.Name;

				deviceEvents = device.DeviceEvents.OrderBy(o => o.Frame).ThenBy(o => o.Time).ToList();
                DeviceEvents.RowCount = deviceEvents.Count;

                ChangingSelection = false;
            }
        }

        private void RefreshData()
        {
            SetDevice(Device);
        }

		private List<DeviceEvent> GetDisplayedList()
		{
			if(EventTypesToShow != 0)
				return filteredDeviceEvents;
			return deviceEvents;
		}

        private void SetToFrameCore(int frameNumber)
        {
			List<DeviceEvent> displayedDeviceEvents = GetDisplayedList();

            if (Device != null)
            {
                // Switch to this frame - unless we're already there.
                // We could have several records per frame, so don't switch around there.
                if (DeviceEvents.SelectedCells.Count > 0)
                {
                    int index = DeviceEvents.SelectedCells[0].RowIndex;

                    if (index >= 0 && index < displayedDeviceEvents.Count)
                    {
                        DeviceEvent deviceEvent = displayedDeviceEvents[index];

                        if (deviceEvent.Frame == frameNumber)
                        {
                            return;
                        }
                    }
                }

                ChangingSelection = true;

                int row = 0;
                
				if(EventTypesToShow == 0)
				{
					Device.FrameToRow.TryGetValue(frameNumber, out row);
				}
				else
				{
					if (!filteredFrameToRow.TryGetValue(frameNumber, out row))
					{
						//we don't have a frame, so return, 
						ChangingSelection = false;						
						return;
					}
				}


                if (DeviceEvents.RowCount > row)
                {
//                    DeviceEvents.Rows[row].Selected = true;
                    DeviceEvents.CurrentCell = DeviceEvents[0, row];
                    DeviceEvents.FirstDisplayedCell = DeviceEvents.CurrentCell;
                }

                ChangingSelection = false;
            }
        }

        public void RefreshDeferred()
        {
            BeginInvoke((Action)delegate { RefreshData(); });
        }

        private void Refresh_Click(object sender, EventArgs e)
        {
//            DeviceEvents.DataSource = null;
//            DeviceEvents.DataSource = deviceEvents;

            DeviceEvents.RowCount = deviceEvents.Count;
        }

        private static int ComputeSeek(List<DeviceEvent> deviceEvents, DeviceEvent devEvent, int row)
        {
            // We only care about LOWREADs at this point (TODO: Look for other events, like seeks and stuff too)
            if (devEvent.Action != DeviceEvent.ActionType.LOWREAD)
            {
                return 0;

            }

            int thisLsn = devEvent.LSN;

            // Find the previous load 
            while (--row >= 0)
            {
                DeviceEvent prevDevEvent = deviceEvents[row];

                if (prevDevEvent.Action == DeviceEvent.ActionType.LOWREAD)
                {
                    int prevLsn = prevDevEvent.LSN + prevDevEvent.Size / 2048;          // TODO: We need a per-device block size!
                    return thisLsn - prevLsn;
                }
            }

            // This is the very first event.
            return 0;
        }

        private void DeviceEvents_CellValueNeeded(object sender, DataGridViewCellValueEventArgs e)
        {
            int row = e.RowIndex;
			List<DeviceEvent> displayedDeviceEvents = GetDisplayedList();

			if (displayedDeviceEvents == null)
            {
                return;
            }

			if (row < 0 || row >= displayedDeviceEvents.Count)
            {
                return;
            }

			DeviceEvent devEvent = displayedDeviceEvents[row];			
			
            switch (e.ColumnIndex)
            {
                case 0:
                    e.Value = devEvent.Frame;
                    break;

                case 1:
                    e.Value = devEvent.Time;
                    break;

                case 2:
                    e.Value = devEvent.Action;
                    break;

                case 3:
                    {
                        if (ShowSeeks)
                        {
							e.Value = ComputeSeek(deviceEvents, devEvent, row);
                        }
                        else
                        {
                            e.Value = String.Format("{0:X}", devEvent.LSN);
                        }
                    }
                    break;

                case 4:
                    e.Value = devEvent.Size;
                    break;

                case 5:
                    e.Value = devEvent.Handle;
                    break;

                case 6:
                    {
                        if (devEvent.Action == DeviceEvent.ActionType.MARKER)
                        {
                            e.Value = devEvent.Marker;
                        }
						else if (devEvent.Action == DeviceEvent.ActionType.LOWREAD)
						{
							//incase we have a filter applied recalc the frame to row
							//as we need the full data to calculate this
							int original_row = 0;
							Device.FrameToRow.TryGetValue(devEvent.Frame, out original_row);
							while (deviceEvents[original_row].Frame == devEvent.Frame)
							{
								if (deviceEvents[original_row].Action == devEvent.Action && deviceEvents[original_row].Handle == devEvent.Handle)
								{
									break;
								}
								original_row++;
							}
							e.Value = GetReadFileName(original_row, devEvent, deviceEvents);
						}
                        else
                        {
							e.Value = devEvent.Streamable;
                        }
                    }
                    break;
				case 7:
					{
						if (devEvent.Action == DeviceEvent.ActionType.LOWREAD)
						{
							e.Value = devEvent.Streamable;
						}
					}
					break;
            }
        }

        private void DeviceEvents_SelectionChanged(object sender, EventArgs e)
        {
            if (ChangingSelection)
            {
                return;
            }

			List<DeviceEvent> displayedDeviceEvents = GetDisplayedList();

            if (DeviceEvents.SelectedCells.Count > 0)
            {
                int index = DeviceEvents.SelectedCells[0].RowIndex;

				if (index >= 0 && index < displayedDeviceEvents.Count)
                {
					DeviceEvent devEvent = displayedDeviceEvents[index];

                    Controller.SetToFrame(devEvent.Frame, true);

                    if (devEvent.Streamable != null)
                    {
                        Controller.OnSelectStreamable(devEvent.Streamable, true);
                    }
                }
            }
        }

        // Switch the display to focus on a certain frame
        public void SetToFrame(int frameNumber)
        {
            BeginInvoke((Action)delegate { SetToFrameCore(frameNumber); });
        }

        // A new device was registered, or a device has changed.
        public void OnDeviceChanged(int deviceIndex, Device device)
        {
            if (deviceIndex == DeviceIndex)
            {
                BeginInvoke((Action)delegate { SetDevice(device); });
            }
        }

        // A streamable has been selected
        public void OnSelectStreamable(Streamable streamable)
        {

        }

        // The client information has been updated
        public void ClientInfoUpdated()
        {

        }

        // We're in follow mode, and a new frame has been added.
        public void OnNewFrame(int frameNumber)
        {
            RefreshDeferred();
        }

        // A new entity type was registered
        public void OnNewEntityType(int entityTypeId, EntityType type)
        {

        }

        public void OnSelectEntity(Entity entity) { }

        // The connection has been terminated
        public void OnConnectionLost() { }


        // Register this view with the system
        public void Register(MainController controller, Scene scene)
        {
            this.Controller = controller;
			this.Scene = scene;
            if (scene.GetDevice(DeviceIndex) != null)
            {
                SetDevice(scene.GetDevice(DeviceIndex));
            }
        }

        private void ToggleShowSeeks()
        {
            ShowSeeks = !ShowSeeks;
            RecreateContextMenu();
            RefreshData();
            Refresh();
        }

		private void ToggleTypeVisibility(DeviceEvent.ActionType eventType, MenuItem item)
		{
			EventTypesToShow ^= (uint)(1 << (int)eventType);
			item.Checked = (EventTypesToShow & (1 << (int)eventType)) != 0;
						
			filteredDeviceEvents = (deviceEvents.Where(d => (EventTypesToShow & 1 << (int)(d.Action)) != 0)).ToList();
			List<DeviceEvent> displayedDeviceEvents = GetDisplayedList();
			filteredFrameToRow.Clear();
			var index = 0;
			foreach (var devEvent in filteredDeviceEvents)
			{
				filteredFrameToRow[devEvent.Frame] = index;					
				index++;
			}
			Refresh();
		}

        private void PopulateContextMenu(ContextMenu menu)
        {
            MenuItem showSeeksItem = new MenuItem("Show Seeks");
            showSeeksItem.Checked = ShowSeeks;
            showSeeksItem.Click += (sender, e) => ToggleShowSeeks();

			MenuItem filter = new MenuItem("Filter Event");
			foreach (DeviceEvent.ActionType value in (DeviceEvent.ActionType[]) Enum.GetValues(typeof(DeviceEvent.ActionType)))
			{
				MenuItem item = new MenuItem(value.ToString());
				int eventTypeId = (int)value;
				item.Checked = (EventTypesToShow & (1 << (int)value)) != 0;
				item.Click += (sender, e) => ToggleTypeVisibility(value, item);
				filter.MenuItems.Add(item);
			}

			menu.MenuItems.Add(filter);
            menu.MenuItems.Add(showSeeksItem);
			menu.MenuItems.Add(ContextMenuHelper.CreateActionMenu("Dump Read Requests In Selected Range", InternalDumpReadRequests));
        }

        private void RecreateContextMenu()
        {
            ContextMenu menu = viewContextMenu.CreateContextMenu(Controller, this);
            PopulateContextMenu(menu);
            this.ContextMenu = menu;
        }

        private void DeviceView_Load(object sender, EventArgs e)
        {
            RecreateContextMenu();
        }

        public void OnFilterChanged()
        {

        }

        public void OnFrameRangeChanged(int firstFrame, int lastFrame)
        {
			this.firstFrame = firstFrame;
			this.lastFrame = lastFrame;
		}

        private void DeviceEvents_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            int row = e.RowIndex;
			List<DeviceEvent> displayedDeviceEvents = GetDisplayedList();

			if (displayedDeviceEvents == null)
            {
                return;
            }

			if (row < 0 || row >= displayedDeviceEvents.Count)
            {
                return;
            }

			DeviceEvent devEvent = displayedDeviceEvents[row];

            if (devEvent.Action == DeviceEvent.ActionType.IDLE)
            {
                e.CellStyle.ForeColor = Color.Red;
            }
            else if (devEvent.Action == DeviceEvent.ActionType.MARKER)
            {
                e.CellStyle.ForeColor = Color.Purple;
            }
        }

		private static String GetReadFileName(int index, DeviceEvent devEvent, List<DeviceEvent> deviceEvents)
		{
			// Find out which actual file that is.
			int prevEvent = index;
			while (--prevEvent >= 0)
			{
				DeviceEvent queueEvent = deviceEvents[prevEvent];
				if (queueEvent.Action == DeviceEvent.ActionType.QUEUE && queueEvent.Handle == devEvent.Handle)
				{
					return queueEvent.Streamable.GetNameAt(devEvent.Frame);
				}
			}
			return "???";
		}

		private static bool HasQueueEvent(int index, DeviceEvent devEvent, List<DeviceEvent> deviceEvents)
		{
			// Find out which actual file that is.
			int prevEvent = index;
			while (--prevEvent >= 0)
			{
				DeviceEvent queueEvent = deviceEvents[prevEvent];
				if (queueEvent.Action == DeviceEvent.ActionType.QUEUE && queueEvent.Handle == devEvent.Handle)
				{
					return true;
				}
			}
			return false;
		}
		// bridge the gap between device events and rscevents
		private static DeviceEvent GetQueueEvent(int index, DeviceEvent devEvent, List<DeviceEvent> deviceEvents)
		{
			// Find out which actual file that is.
			int prevEvent = index;
			while (--prevEvent >= 0)
			{
				DeviceEvent queueEvent = deviceEvents[prevEvent];
				if (queueEvent.Action == DeviceEvent.ActionType.QUEUE && queueEvent.Handle == devEvent.Handle)
				{
					return queueEvent;
				}
			}
			return deviceEvents[0];
		}
		
		private void InternalDumpReadRequests()
		{
			int start = 0, end = deviceEvents.Count;
			if (firstFrame != lastFrame)
			{
				start = firstFrame; end = lastFrame;

			}
			DumpReadRequests(start, end, Scene, DeviceIndex);
		}

		//TODO:
		// Make this AboutBox bit more general so that we can dump information based on
		//  the currently applied filter.
		public static void DumpReadRequests(int start, int end, Scene scene, int deviceIdx, String strHeader = null)
		{
			Device Device = scene.GetDevice(deviceIdx);
			List<DeviceEvent> deviceEvents = Device.DeviceEvents.OrderBy(o => o.Frame).ThenBy(o => o.Time).ToList();

			int current_frame = start;

			StringBuilder result = new StringBuilder(65536);
			if (strHeader != null)
			{
				result.Append(strHeader);
			}

			result.Append(
				"Frame"		+
				"\tTime"	+
				"\tTaken"	+
				"\tSize"	+
				"\tLSN"		+
				"\tSeek"	+
				"\tMB/s"	+
				"\tName"	+
				"\tRPF"		+
				"\tType"	+
				"\tScore"	+
				"\tOriginal Context" +
				"\tFlags"	+
				"\tIn Flight" +
				"\n"
				);

			//find the starting point
			int x = 0;
			for (; x < deviceEvents.Count; x++)
			{
				if (deviceEvents[x].Frame == current_frame)
					break;
			}

			//get a list of the streamable in flight during the first frame
			DeviceStreamingState state = Device.StreamingState.GetStateAt(current_frame);
			IList<DeviceStreamingState.StreamableEntry> streamingList = state.Streaming;
			Hashtable inFlight = new Hashtable();
			for (int i=0; i < streamingList.Count; i++)
			{
				if (!inFlight.ContainsKey(streamingList[i].Streamable.CurrentName))
				{
					inFlight.Add(streamingList[i].Streamable.CurrentName, true);
				}
			}

			for (; x < deviceEvents.Count; x++)
			{
				DeviceEvent devEvent = deviceEvents[x];				

				if (devEvent.Frame > end)
					continue;

				if (devEvent.Action == DeviceEvent.ActionType.LOWREAD)
				{
					int eventEnd = x;
					// find end of this event
					while (++eventEnd < deviceEvents.Count)
					{
						if (deviceEvents[eventEnd].Layer == DeviceEvent.LayerType.PHYSICAL)
						{
							break;
						}
					}
					float endTime = deviceEvents[eventEnd].Time;
					String streamableName = GetReadFileName(x, devEvent, deviceEvents);
					
					//scan through to get the score of this item at the time it's request was accepted to the queue
					int eventIdx = 0, frameRequestIdx = 0;
					int loadRequestedFrame = -1;
					float score = -1.0f;	
					String context = "NONE";
					String flags = "";
					if (HasQueueEvent(x, devEvent, deviceEvents))
					{
						DeviceEvent queueEnv = GetQueueEvent(x, devEvent, deviceEvents);
						Streamable queuedStreamable = queueEnv.Streamable;
						List<RscEvent> eventList = queuedStreamable.Events;

						//find the correct event the work back to the load requested frame
						while(eventIdx < eventList.Count)
						{
							if (eventList[eventIdx].FrameNumber == queueEnv.Frame)
							{
								if (eventList[eventIdx].Action != RscEvent.ActionType.Loading)
								{
									Console.WriteLine(String.Format("Event {0} at frame {1} is not Loading, it's {2}", queuedStreamable.GetNameAt(eventList[eventIdx].FrameNumber), eventList[eventIdx].FrameNumber, eventList[eventIdx].Action));
								}
								else
								{
									if(eventIdx > 0)
									{
										if (eventList[eventIdx-1].Action != RscEvent.ActionType.LoadRequested)
										{
											Console.WriteLine(String.Format("Event {0} at frame {1} is not Load Requested, it's {2}", queuedStreamable.GetNameAt(eventList[eventIdx].FrameNumber), eventList[eventIdx].FrameNumber, eventList[eventIdx].Action));
										}
										loadRequestedFrame = eventList[eventIdx - 1].FrameNumber;
										//work our way back to the request
										while (eventIdx >= 0 && eventList[eventIdx].Action != RscEvent.ActionType.REQUEST)
										{
											eventIdx--;
										}
										break;
									}
								}
							}
							eventIdx++;
						}

						//here we get the actual score during the load requested frame
						if (loadRequestedFrame > -1)
						{
							Frame frame = scene.GetFrame(loadRequestedFrame);
							lock (frame)
							{
								List<Request> requests = frame.requests;
								//loop through requests and find the right one
								while (frameRequestIdx < requests.Count && frameRequestIdx > -1)
								{
									if (requests[frameRequestIdx].Streamable.CurrentName == queuedStreamable.CurrentName)
									{
										score = requests[frameRequestIdx].Score;
										int findroot = frameRequestIdx;
										while (findroot > -1 && requests[findroot].Context == "DEPENDENCY")
										{
											findroot--;
										}
										if (findroot > -1)
										{
											flags = Request.CreateFlagString(requests[findroot].Flags);
											context = requests[findroot].Context;
										}
									}
									frameRequestIdx++;
								}
							}
						}
					}					

					result.Append(
							String.Format("{0}\t{1:F9}\t{2:F9}\t{3:F9}\t{4}\t{5}\t{6:F9}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\n", //there has got to be a better way than this...
							devEvent.Frame,
							devEvent.Time,
							(endTime-devEvent.Time) * 1000.0f,
							(float)devEvent.Size / 1024.0f,
							devEvent.LSN,
							ComputeSeek(deviceEvents, devEvent, x),
							(float)devEvent.Size / (1024.0f * 1024.0f) / (endTime - devEvent.Time),							
							streamableName,							
							devEvent.Streamable.GetNameAt(devEvent.Frame), 
							devEvent.Action.ToString(),
							score.ToString("00.00000000000000"),
							context, 
							flags,
							inFlight.Contains(streamableName) ? "True" : "False")
						);
				}					
			}


			Clipboard.SetText(result.ToString());
			MessageBox.Show("Requests copied to clipboard. You can paste it into Excel.");
		}
    }
}
