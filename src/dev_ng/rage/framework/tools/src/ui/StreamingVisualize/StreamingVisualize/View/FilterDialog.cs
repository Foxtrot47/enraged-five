﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StreamingVisualize.Controller;
using StreamingVisualize.Model;
using System.Text.RegularExpressions;

namespace StreamingVisualize.View
{
    public partial class FilterDialog : Form
    {
        public MainController Controller;
        public Scene Scene;
        public Filter Filter;

        private IList<Module> modules;

        private Module allModules = new Module();

        public FilterDialog()
        {
            allModules.Name = "(All Modules)";
            InitializeComponent();
        }

        public void Populate()
        {
            modules = Scene.CreateModuleList();
            modules.Add(allModules);

            Module.DataSource = modules;
            Module.SelectedIndex = modules.Count - 1;
        }

        private void Module_SelectionChangeCommitted(object sender, EventArgs e)
        {
            int selectedIndex = Module.SelectedIndex;

            if (selectedIndex >= 0)
            {
                Module module = modules[selectedIndex];

                if (module == allModules)
                {
                    module = null;
                }

                Filter.Module = module;
                Controller.OnFilterChanged();
            }
        }

        private String FilterString(String input)
        {
            return Regex.Replace(input, @"[\(\)\[\]\\]", "", RegexOptions.None);
        }

        private void NameFilter_TextChanged(object sender, EventArgs e)
        {
            if (NameFilter.Text.Length > 0)
            {
                String filtered = FilterString(NameFilter.Text);

                // Object comparison, not string comparison. That's cool.
                if (filtered != NameFilter.Text)
                {
                    NameFilter.Text = filtered;
                }

                Filter.ResourceNameSubstring = NameFilter.Text;
            }
            else
            {
                Filter.ResourceNameSubstring = "";
            }

            Filter.Update();

            Controller.OnFilterChanged();
        }

        private void FilterDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                Hide();
                e.Cancel = true;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (NameExclusion.Text.Length > 0)
            {
                String filtered = FilterString(NameExclusion.Text);

                // Object comparison, not string comparison. That's cool.
                if (filtered != NameFilter.Text)
                {
                    NameExclusion.Text = filtered;
                }

                Filter.ResourceNameExcludeSubstring = NameExclusion.Text;
            }
            else
            {
                Filter.ResourceNameExcludeSubstring = "";
            }

            Filter.Update();

            Controller.OnFilterChanged();

        }
    }
}
