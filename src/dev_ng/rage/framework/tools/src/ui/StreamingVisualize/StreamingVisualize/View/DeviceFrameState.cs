﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StreamingVisualize.Controller;
using StreamingVisualize.Model;
using StreamingVisualize.View.Components;

namespace StreamingVisualize.View
{
    public partial class DeviceFrameState : UserControl, IViewController
    {
        private ViewContextMenu viewContextMenu = new ViewContextMenu();

        public MainController Controller;

        public Device Device;

        public int CurrentFrame;

        public int DeviceIndex;

        private IList<DeviceStreamingState.StreamableEntry> queuedList;

        private IList<DeviceStreamingState.StreamableEntry> streamingList;

        private Font finishedFont;
        private Font finishedPriorityFont;
        private Font priorityFont;


        // If true, we're changing lists around. Don't respond to selection changes.
        private bool ChangingSelection;


        public DeviceFrameState(int deviceIndex)
        {
            InitializeComponent();

            this.DeviceIndex = deviceIndex;
        }

        private void UpdateTitle()
        {
            if (Device != null)
            {
                Title.Text = Device.Name + " at frame " + CurrentFrame;
            }
        }

        public void RefreshState()
        {
            if (Device != null)
            {
                ChangingSelection = true;

                DeviceStreamingState state = Device.StreamingState.GetStateAt(CurrentFrame);

                queuedList = state.Queued;
                streamingList = state.Streaming;

                Queued.DataSource = state.Queued;
                Streaming.DataSource = state.Streaming;

                ChangingSelection = false;

                UpdateTitle();
            }
        }

        public void SetDevice(Device device)
        {
            this.Device = device;
            BeginInvoke((Action)delegate { RefreshState(); });
        }

        // Switch the display to focus on a certain frame
        public void SetToFrame(int frameNumber)
        {
            CurrentFrame = frameNumber;
            BeginInvoke((Action)delegate { RefreshState(); });
        }

        // A new device was registered, or a device has changed.
        public void OnDeviceChanged(int deviceIndex, Device device)
        {
            if (deviceIndex == this.DeviceIndex)
            {
                SetDevice(device);
            }
        }

        // A streamable has been selected
        public void OnSelectStreamable(Streamable streamable) { }

        // The client information has been updated
        public void ClientInfoUpdated() { }

        // We're in follow mode, and a new frame has been added.
        public void OnNewFrame(int frameNumber)
        {
            CurrentFrame = frameNumber;
            BeginInvoke((Action)delegate { RefreshState(); });
        }

        // A new entity type was registered
        public void OnNewEntityType(int entityTypeId, EntityType type) { }

        // The connection has been terminated
        public void OnConnectionLost() { }

        public void OnSelectEntity(Entity entity) { }

        private void FormatCell(DataGridViewCellFormattingEventArgs e, IList<DeviceStreamingState.StreamableEntry> list)
        {
            int index = e.RowIndex;

            if (streamingList != null && index >= 0 && index < list.Count)
            {
                if (finishedFont == null)
                {
                    finishedFont = new Font(e.CellStyle.Font, FontStyle.Italic);
                    priorityFont = new Font(e.CellStyle.Font, FontStyle.Bold);
                    finishedPriorityFont = new Font(e.CellStyle.Font, FontStyle.Italic | FontStyle.Bold);
                }

                Font font;

                if (list[index].Finished)
                {
                    font = (list[index].Priority) ? finishedPriorityFont : finishedFont;
                }
                else
                {
                    font = (list[index].Priority) ? priorityFont : e.CellStyle.Font;
                }

                e.CellStyle.Font = font;
            }
        }

        private void Queued_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            FormatCell(e, queuedList);
        }

        private void Streaming_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            FormatCell(e, streamingList);
        }

        private void Queued_SelectionChanged(object sender, EventArgs e)
        {
            if (ChangingSelection)
            {
                return;
            }

            if (Queued.SelectedCells.Count > 0 && queuedList != null)
            {
                int index = Queued.SelectedCells[0].RowIndex;

                if (index >= 0 && index < queuedList.Count)
                {
                    Controller.OnSelectStreamable(queuedList[index].Streamable, true);
                }
            }
        }

        private void Streaming_SelectionChanged(object sender, EventArgs e)
        {
            if (ChangingSelection)
            {
                return;
            }

            if (Streaming.SelectedCells.Count > 0 && streamingList != null)
            {
                int index = Streaming.SelectedCells[0].RowIndex;

                if (index >= 0 && index < streamingList.Count)
                {
                    Controller.OnSelectStreamable(streamingList[index].Streamable, true);
                }
            }
        }

        // Register this view with the system
        public void Register(MainController controller, Scene scene)
        {
            this.Controller = controller;

            if (Device == null)
            {
                Device = scene.GetDevice(DeviceIndex);
                RefreshState();
            }
        }

        private void DeviceFrameState_Load(object sender, EventArgs e)
        {
            ContextMenu = viewContextMenu.CreateContextMenu(Controller, this);
        }

        public void OnFilterChanged()
        {

        }

        public void OnFrameRangeChanged(int firstFrame, int lastFrame)
        {

        }

        private void Streaming_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Streaming_SelectionChanged(sender, null);
        }

        private void Queued_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Queued_SelectionChanged(sender, null);
        }
    }
}
