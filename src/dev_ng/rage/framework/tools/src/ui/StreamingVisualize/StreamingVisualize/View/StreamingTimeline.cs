﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StreamingVisualize.Controller;
using StreamingVisualize.Model;
using StreamingVisualize.View.Components;
using System.Diagnostics;

namespace StreamingVisualize.View
{
    public partial class StreamingTimeline : UserControl, IViewController
    {
        private MainController Controller;
        private Scene Scene;
        private ViewContextMenu viewContextMenu = new ViewContextMenu();
        private SimpleTooltip tooltip = new SimpleTooltip();

        // Height of a bar in pixels
        private const int BarHeight = 24;

        // Number of pixels spacing between each bar
        private const int Spacing = 6;

        // Minimum visible size of bar elements that are considered important, others on top of it might
        // be pushed back.
        private const int MinVisibleWidth = 3;

        private const int NO_MOUSE = 0x7fffffff;

        private int LastMouseX = NO_MOUSE;
        private int LastMouseY;

        private bool IsDragging = false;
        private Stopwatch DragTimer = new Stopwatch();

        private int MouseFrameNumber;

        private int DragStartX;
        private float DragStartOffset;

        private float ZoomFactor = 0.0002f;
        private float Offset = 0.0f;

        private Brush LabelBrush = new SolidBrush(Color.Black);
        private Pen RectangleBorder = new Pen(Color.Black, 1.0f);
        private StringFormat barTextFormat = new StringFormat();

        private Streamable HoverStreamable = null;
        private bool ChangingSelection = false;

        private Brush[] OperationBrushes = new Brush[] {
            new SolidBrush(Color.LightGreen),
            new SolidBrush(Color.LightSalmon),
            new SolidBrush(Color.LightYellow),
            new SolidBrush(Color.LightBlue),
            new SolidBrush(Color.LightCoral),
            new SolidBrush(Color.White),
            new SolidBrush(Color.LightSkyBlue),
            new SolidBrush(Color.LightCyan),
            new SolidBrush(Color.LightPink),
            new SolidBrush(Color.LightGray),
            new SolidBrush(Color.LightSlateGray),
            new SolidBrush(Color.Orange),
            new SolidBrush(Color.Lime),
        };

        private Brush PrioReqBrush = new SolidBrush(Color.Red);

        private Brush FrameBg = new SolidBrush(Color.LightGray);

        private Font TrackLabel = new Font("Tahoma", 12.0f, FontStyle.Bold);
        private Font FrameLabel = new Font("Tahoma", 36.0f, FontStyle.Bold);

        private Brush FrameLabelBrush = new SolidBrush(Color.DarkSlateGray);
        private Brush TrackLabelBrush = new SolidBrush(Color.Black);

        
        public StreamingTimeline()
        {
            barTextFormat.Trimming = StringTrimming.EllipsisCharacter;
            InitializeComponent();
        }

        // Switch the display to focus on a certain frame
        public void SetToFrame(int frameNumber)
        {
            if (!ChangingSelection)
            {
                // Get the beginning of this frame.
                float frameStart = Scene.GetFrame(frameNumber).Time;

                Offset = frameStart;
                UpdateScrollbar();
                UpdateScrollbarPos();
                Refresh();
            }
        }

        // A new device was registered, or a device has changed.
        public void OnDeviceChanged(int deviceIndex, Device device) { }

        // A streamable has been selected
        public void OnSelectStreamable(Streamable streamable)
        {
        }

        // The client information has been updated
        public void ClientInfoUpdated() { }

        // We're in follow mode, and a new frame has been added.
        public void OnNewFrame(int frameNumber) { }

        // A new entity type was registered
        public void OnNewEntityType(int entityTypeId, EntityType type) { }

        // The connection has been terminated
        public void OnConnectionLost() { }

        // Register this view with the system
        public void Register(MainController controller, Scene scene)
        {
            this.Controller = controller;
            this.Scene = scene;

  //          PopulateList();
            UpdateScrollbar();
        }

        public void OnFilterChanged()
        {
//            PopulateList();
            Refresh();
        }

        public void OnFrameRangeChanged(int firstFrame, int lastFrame)
        {
            Refresh();
        }

        public void OnSelectEntity(Entity entity) { }

        private void DumpRequests()
        {
            int firstFrame = Scene.SelectedFirstFrame;
            int lastFrame = Scene.SelectedLastFrame;

            StringBuilder result = new StringBuilder(65536);

            result.Append("Frame\tTime\tDevice\tFile\tEvent\n");

            for (int x=0; x<Scene.maxDeviceIndex; x++)
            {
                Device device = Scene.GetDevice(x);

                if (device != null)
                {
                    int eventCount = device.DeviceEvents.Count;

                    for (int y=0; y<eventCount; y++)
                    {
                        if (device.DeviceEvents[y].Frame >= firstFrame)
                        {
                            for (; y < eventCount && device.DeviceEvents[y].Frame < lastFrame; y++)
                            {
                                DeviceEvent ev = device.DeviceEvents[y];
                                if (ev.Action == DeviceEvent.ActionType.MARKER)
                                {
                                    result.Append(String.Format("{0}\t{1:F3}\t\t\t{2}\n",
                                        ev.Frame, ev.Time, ev.Marker));

                                }
                                else if (ev.Action == DeviceEvent.ActionType.READ)
                                {
                                    result.Append(String.Format("{0}\t{1:F3}\t{2}\t{3}\n",
                                        ev.Frame, ev.Time, device.Name, ev.Streamable.GetNameAt(ev.Frame)));
                                }
                            }

                            break;
                        }
                    }
                }
            }

            Clipboard.SetText(result.ToString());
            MessageBox.Show("Requests copied to clipboard. You can paste it into Excel.");
        }

        private void DumpRequestsByModule()
        {
            int firstFrame = Scene.SelectedFirstFrame;
            int lastFrame = Scene.SelectedLastFrame;

            StringBuilder result = new StringBuilder(65536);

            result.Append("Frame\tLength\t");

            for (int x = 0; x < Scene.maxModuleIndex; x++)
            {
                String devName = Scene.GetModule(x) != null ? Scene.GetModule(x).Name : "";
                result.Append(devName);
                result.Append("\t");
            }

            result.Append("\r\n");

            int[] perModuleCount = new int[Scene.maxModuleIndex];
            int[] perModuleSum = new int[Scene.maxModuleIndex];
            List<String>[] perModuleRequests = new List<String>[Scene.maxModuleIndex];
            Dictionary<String, int> requestCounts = new Dictionary<String, int>();
            int maxReqNum = 0;

            for (int x = 0; x < perModuleRequests.Length; x++)
            {
                perModuleRequests[x] = new List<String>();
            }

            Array.Clear(perModuleSum, 0, perModuleSum.Length);

            for (int frame = firstFrame; frame < lastFrame; frame++)
            {
                float frameLength = 0.0f;

                Frame frameInfo = Scene.GetFrame(frame);
                Array.Clear(perModuleCount, 0, perModuleCount.Length);

                if (frame < Scene.MaxFrame)
                {
                    frameLength = Scene.GetFrame(frame + 1).Time - frameInfo.Time;
                }


                foreach (Request req in frameInfo.requests)
                {
                    // Skip doomed requests
                    if ((req.Flags & Request.DOOMED) == 0)
                    {
                        String strName = req.Streamable.GetNameAt(frame);

                        // Skip dummies.
                        if (req.Streamable.VirtSize > 0 || req.Streamable.PhysSize > 0)
                        {
                            int moduleIndex = req.Streamable.Module.Index;

                            perModuleCount[moduleIndex]++;
                            perModuleSum[moduleIndex]++;
                            perModuleRequests[moduleIndex].Add(strName);
                            maxReqNum = Math.Max(maxReqNum, perModuleRequests[moduleIndex].Count);

                            int prevCount = 0;
                            if (requestCounts.ContainsKey(strName))
                            {
                                prevCount = requestCounts[strName];
                            }

                            requestCounts[strName] = prevCount + 1;
                        }
                    }
                }

                result.Append(String.Format("{0}\t{1:F}\t", frame, frameLength));

                foreach (int count in perModuleCount)
                {
                    result.Append(String.Format("{0}\t", count));
                }
                result.Append("\r\n");
            }

            result.Append("SUM\t \t");
            int totalReqSum = 0;

            foreach (int count in perModuleSum)
            {
                result.Append(String.Format("{0}\t", count));
                totalReqSum += count;
            }

            if (totalReqSum > 0)
            {
                result.Append("\r\nPCT %\t \t");
                float totalReq = (float)totalReqSum;

                foreach (int count in perModuleSum)
                {
                    result.Append(String.Format("{0:F2}%\t", (float)count * 100.0f / totalReq));
                }
            }


            result.Append("\r\n");
            result.Append("\r\n");
            result.Append("\r\n");
            result.Append("\r\n");
            result.Append("\r\n");
            result.Append("\r\n");
            result.Append("\r\n");

            for (int x = 0; x < maxReqNum; x++)
            {
                result.Append(" \t \t");
                for (int y=0; y<Scene.maxModuleIndex; y++)
                {
                    if (perModuleRequests[y].Count > x)
                    {
                        result.Append(perModuleRequests[y][x]);
                    }

                    result.Append("\t");
                }

                result.Append("\r\n");
            }

            result.Append("\r\n");
            result.Append("\r\n");
            result.Append("\r\n");
            result.Append("\r\n");
            result.Append("\r\n");
            result.Append("\r\n");
            result.Append("\r\n");
            result.Append("\r\nMuliple loads:\r\n");

            List<int> reqCounts = new List<int>(requestCounts.Values);
            reqCounts.Sort();

            for (int x = reqCounts.Count - 1; x >= 0; x--)
            {
                if (reqCounts[x] <= 1)
                {
                    break;
                }

                foreach (String strName in requestCounts.Keys)
                {
                    if (requestCounts[strName] == reqCounts[x])
                    {
                        result.Append(String.Format("{0}\t{1}\r\n", reqCounts[x], strName));
                        requestCounts.Remove(strName);
                        break;
                    }
                }
            }

            Clipboard.SetText(result.ToString());
            MessageBox.Show("Requests copied to clipboard. You can paste it into Excel.");
        }

        private void RecreateContextMenu()
        {
            ContextMenu menu = viewContextMenu.CreateContextMenu(Controller, this);

            menu.MenuItems.Add(ContextMenuHelper.CreateActionMenu("Dump Requests In Selected Range", DumpRequests));
            menu.MenuItems.Add(ContextMenuHelper.CreateActionMenu("Dump Requests By Module", DumpRequestsByModule));
            menu.MenuItems.Add(ContextMenuHelper.CreateActionMenu("Mark First", MarkFirst));
            menu.MenuItems.Add(ContextMenuHelper.CreateActionMenu("Mark Last", MarkLast));

//            PopulateContextMenu(menu);
            this.ContextMenu = menu;
        }

        private void MarkFirst()
        {
            if (MouseFrameNumber != -1)
            {
                Scene.SelectedFirstFrame = MouseFrameNumber;
                Scene.SelectedLastFrame = Math.Max(Scene.SelectedLastFrame, Scene.SelectedFirstFrame);
                Controller.OnFrameRangeChanged(Scene.SelectedFirstFrame, Scene.SelectedLastFrame);
            }
        }

        private void MarkLast()
        {
            if (MouseFrameNumber != -1)
            {
                Scene.SelectedLastFrame = MouseFrameNumber;
                Scene.SelectedFirstFrame = Math.Min(Scene.SelectedFirstFrame, Scene.SelectedLastFrame);
                Controller.OnFrameRangeChanged(Scene.SelectedFirstFrame, Scene.SelectedLastFrame);
            }
        }

        private void StreamingTimeline_Load(object sender, EventArgs e)
        {
            RecreateContextMenu();
        }

        private float GetSeconds(int xPos)
        {
            return (float)xPos * ZoomFactor + Offset;
        }

        private int GetXPos(float seconds)
        {
            return (int)((seconds - Offset) / ZoomFactor);
        }

        private static int GetFirstEvent(Device device, int frame, DeviceEvent.LayerType layerType)
        {
            int result = frame;

            // Search backwards first.
            while (frame > 0)
            {
                frame--;

                if (device.DeviceEvents[frame].Layer == layerType)
                {
                    return frame;
                }
            }

            // Forward then.
            while (frame < device.DeviceEvents.Count)
            {
                if (device.DeviceEvents[frame].Layer == layerType)
                {
                    return frame;
                }

                frame++;
            }

            return -1;
        }

        private Brush GetBrushFromHandle(int handle)
        {
            return OperationBrushes[(handle ^ (handle >> 6)) % OperationBrushes.Length];
        }

        private void RenderBar(Graphics g, float startSecond, float endSecond, int yPos, Brush brush, String label, bool prio)
        {
            int x = GetXPos(startSecond);
            int width = GetXPos(endSecond) - x;

            if (prio)
            {
                g.FillRectangle(PrioReqBrush, x, yPos - 2, width, BarHeight + 4);
            }

            g.FillRectangle(brush, x, yPos, width, BarHeight);
            g.DrawRectangle(RectangleBorder, x, yPos, width, BarHeight);

            if (width > 30)
            {
                Rectangle rect = new Rectangle(x + 2, yPos + 2, width - 4, BarHeight - 4);
                g.DrawString(label, Font, LabelBrush, rect, barTextFormat);
            }
        }

        private void RenderDevice(Graphics g, Device device, int yPos, float height, StringBuilder toolTipText)
        {
            // Get the basics straight - where are we?
            float StartSeconds = GetSeconds(0);
            float EndSeconds = GetSeconds(Width);

            float MouseTime = GetSeconds(LastMouseX);

            // Find the first event that falls into this area.
            int Events = device.DeviceEvents.Count;

            if (Events == 0)
            {
                return;
            }

            g.DrawString(String.Format("{0} Low-level I/O", device.Name), TrackLabel, TrackLabelBrush, 10.0f, (float) yPos + 2.0f);
            g.DrawString(String.Format("{0} Streamer Thread", device.Name), TrackLabel, TrackLabelBrush, 10.0f, (float)(yPos + BarHeight + Spacing) + 2.0f);
            g.DrawString(String.Format("{0} Main Thread", device.Name), TrackLabel, TrackLabelBrush, 10.0f, (float)(yPos + 2 * (BarHeight + Spacing)) + 2.0f);

            int FirstEvent = -1;

            // TODO: Use binary search, and cache value off.
            for (int x = 0; x < Events; x++)
            {
                if (device.DeviceEvents[x].Time >= StartSeconds)
                {
                    FirstEvent = x;
                    break;
                }
            }

            if (FirstEvent == -1)
            {
                return;
            }

            // First, the reader, i.e. the physical layer.
            // Find the first physical event before this.
            int index = GetFirstEvent(device, FirstEvent, DeviceEvent.LayerType.PHYSICAL);

            if (index != -1)
            {
                while (index < Events)
                {
                    DeviceEvent ev = device.DeviceEvents[index];

                    // We only care about reads for now.
                    if (ev.Action == DeviceEvent.ActionType.LOWREAD)
                    {
                        // Here we go.
                        if (GetXPos(ev.Time) > Width)
                        {
                            // Out of frame - we're done rendering.
                            break;
                        }

                        // Let's find the end of it.
                        while (++index < Events)
                        {
                            if (device.DeviceEvents[index].Layer == DeviceEvent.LayerType.PHYSICAL)
                            {
                                break;
                            }
                        }

                        if (index == Events)
                        {
                            break;
                        }

                        Streamable packfileStreamable = ev.Streamable;
                        Streamable streamable = packfileStreamable;

                        // Find out which actual file that is.
                        int prevEvent = index;
                        int handle = ev.Handle;

                        while (--prevEvent >= 0)
                        {
                            DeviceEvent queueEvent = device.DeviceEvents[prevEvent];
                            if (queueEvent.Action == DeviceEvent.ActionType.QUEUE && queueEvent.Handle == handle)
                            {
                                streamable = queueEvent.Streamable;
                                break;
                            }
                        }

                        String streamableName = streamable.GetNameAt(ev.Frame);

                        float startTime = ev.Time;
                        float endTime = device.DeviceEvents[index].Time;

                        if (!streamable.Filtered)
                        {
                            RenderBar(g, startTime, endTime, yPos, GetBrushFromHandle(ev.Handle), streamableName, false);

                            if (startTime <= MouseTime && MouseTime <= endTime)
                            {
                                if (LastMouseY >= yPos && LastMouseY < yPos + BarHeight)
                                {
                                    HoverStreamable = streamable;
                                    toolTipText.Append(String.Format("{0}: {1}\n", ev.Action, streamableName));
                                    toolTipText.Append(String.Format("File: {1}\n", ev.Action, packfileStreamable.GetNameAt(ev.Frame)));
                                    toolTipText.Append(String.Format("{0:F2}s-{1:F2}s ({2:F1}ms)\n", startTime, endTime, (endTime - startTime) * 1000.0f));

                                    if (ev.Size > 0 && startTime != endTime)
                                    {
                                        toolTipText.Append(String.Format("{0:F1}KB ({1:F1}MB/s)\n", (float)ev.Size / 1024.0f, (float)ev.Size / (1024.0f * 1024.0f) / (endTime - startTime)));
                                    }

                                    toolTipText.Append(String.Format("Handle {0}\n", ev.Handle));
                                }
                            }
                        }

                        continue;
                    }

                    index++;
                }
            }

            // Next up - the streamer thread. Queuing, compression, etc.
            yPos += BarHeight + Spacing;

            int lastEventX = -1000;

            index = GetFirstEvent(device, FirstEvent, DeviceEvent.LayerType.STREAMER);

            if (index != -1)
            {
                while (index < Events)
                {
                    DeviceEvent ev = device.DeviceEvents[index];

                    // We only care about reads and decompression starts for now.
                    if (ev.Action == DeviceEvent.ActionType.READ || ev.Action == DeviceEvent.ActionType.DECOMPRESS)
                    {
                        // Here we go.
                        if (GetXPos(ev.Time) > Width)
                        {
                            // Out of frame - we're done rendering.
                            break;
                        }

                        // Let's find the end of it.
                        while (++index < Events)
                        {
                            if (device.DeviceEvents[index].Layer == DeviceEvent.LayerType.STREAMER)
                            {
                                break;
                            }
                        }

                        if (index == Events)
                        {
                            break;
                        }

                        float startTime = ev.Time;
                        float endTime = device.DeviceEvents[index].Time;

                        if (ev.Action == DeviceEvent.ActionType.QUEUE)
                        {
                            // Queues are immediate, so let's give them a minimum size.
                            endTime = Math.Max(endTime, startTime + 0.005f);
                        }

                        // Cheat a little - make sure that queue events are visible, even if that means pushing the event back a little.
                        int startXPos = GetXPos(startTime);
                        if (startXPos < lastEventX + MinVisibleWidth)
                        {
                            // But only if the previous event actually was a queue event.
                            if (index > 0 && device.DeviceEvents[index].Action == DeviceEvent.ActionType.QUEUE)
                            {
                                startXPos = lastEventX + MinVisibleWidth;
                                startTime = GetSeconds(lastEventX + MinVisibleWidth);
                                endTime = Math.Max(endTime, GetSeconds(startXPos + MinVisibleWidth));
                            }
                        }

                        lastEventX = startXPos;

                        if (!ev.Streamable.Filtered)
                        {
                            RenderBar(g, startTime, endTime, yPos, GetBrushFromHandle(ev.Handle), ev.Streamable.GetNameAt(ev.Frame), false);

                            if (startTime <= MouseTime && MouseTime <= endTime)
                            {
                                if (LastMouseY >= yPos && LastMouseY < yPos + BarHeight)
                                {
                                    HoverStreamable = ev.Streamable;
                                    toolTipText.Append(String.Format("{0}: {1}\n", ev.Action, ev.Streamable.GetNameAt(ev.Frame)));
                                    toolTipText.Append(String.Format("{0:F2}s-{1:F2}s ({2:F1}ms)\n", startTime, endTime, (endTime - startTime) * 1000.0f));
                                    toolTipText.Append(String.Format("File Size: {0}KB\n", ev.Streamable.FileSize / 1024));
                                    toolTipText.Append(String.Format("Handle {0}\n", ev.Handle));
                                }
                            }
                        }

                        continue;
                    }

                    index++;
                }
            }

            // Next up - the main thread. Assert, requesting, etc.
            for (int pass = 0; pass < 2; pass++)
            {
                yPos += BarHeight + Spacing;

                index = GetFirstEvent(device, FirstEvent, DeviceEvent.LayerType.MAIN);
                lastEventX = -1000;

                if (index != -1)
                {
                    while (index < Events)
                    {
                        DeviceEvent ev = device.DeviceEvents[index];

                        // Here we go.
                        if (GetXPos(ev.Time) > Width)
                        {
                            // Out of frame - we're done rendering.
                            break;
                        }

                        if (ev.Layer == DeviceEvent.LayerType.MAIN)
                        {
                            int passType = (ev.Action == DeviceEvent.ActionType.MARKER) ? 1 : 0;
                            if (pass == passType)
                            {
                                float startTime = ev.Time;

                                // All events on the main thread are immediate and have no duration.
                                float endTime = startTime + 0.005f;

                                String infoText = "";

                                if (ev.Action == DeviceEvent.ActionType.MARKER)
                                {
                                    infoText = ev.Marker;
                                }
                                else
                                {
                                    if (ev.Streamable != null)
                                    {
                                        infoText = ev.Streamable.GetNameAt(ev.Frame);
                                    }
                                }

                                // Cheat a little - make sure that queue events are visible, even if that means pushing the event back a little.
                                int startXPos = GetXPos(startTime);
                                if (startXPos < lastEventX + MinVisibleWidth)
                                {
                                    startXPos = lastEventX + MinVisibleWidth;
                                    startTime = GetSeconds(lastEventX + MinVisibleWidth);
                                    endTime = Math.Max(endTime, GetSeconds(startXPos + MinVisibleWidth));
                                }

                                lastEventX = startXPos;

                                if (ev.Streamable == null || !ev.Streamable.Filtered)
                                {
                                    RenderBar(g, startTime, endTime, yPos, GetBrushFromHandle(ev.Handle), infoText, false);

                                    if (startTime <= MouseTime && MouseTime <= endTime)
                                    {
                                        if (LastMouseY >= yPos && LastMouseY < yPos + BarHeight)
                                        {
                                            if (ev.Streamable != null)
                                            {
                                                HoverStreamable = ev.Streamable;
                                                toolTipText.Append(String.Format("{0}: {1}\n", ev.Action, ev.Streamable.GetNameAt(ev.Frame)));
                                            }
                                            else
                                            {
                                                toolTipText.Append(infoText);
                                                toolTipText.Append('\n');
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        index++;
                    }
                }
            }
        }

        // TODO: Move this to scene.
        private int GetFrameAtTime(float seconds)
        {
            // TODO: Use binary search.
            for (int x = 0; x < Scene.FrameCount; x++)
            {
                if (Scene.GetFrame(x).Time > seconds)
                {
                    return (x > 0) ? x-1 : x;
                }
            }

            return -1;
        }

        private void StreamingTimeline_Paint(object sender, PaintEventArgs e)
        {
            if (Scene != null)
            {
                int localHeight = Scroller.Top;
                int yPos = 0;

                StringBuilder tooltipText = new StringBuilder(2048);

                // Find the frame number.
                float firstSec = GetSeconds(0);

                int frame = GetFrameAtTime(firstSec);
                float mouseTime = GetSeconds(LastMouseX);
                int mouseFrame = GetFrameAtTime(mouseTime);

                int lastFrameLabel = -1000;

                if (frame != -1)
                {
                    // Draw the frame indicators.
                    if (frame > 0)
                    {
                        frame--;
                    }

                    int leftFrame = frame;

                    float timeStart = Scene.GetFrame(frame).Time;
                    int startPos = GetXPos(timeStart);

                    while (startPos < Width && ++frame < Scene.FrameCount)
                    {
                        float nextTime = Scene.GetFrame(frame).Time;
                        int endPos = GetXPos(nextTime);

                        if ((frame & 1) != 0)
                        {
                            e.Graphics.FillRectangle(FrameBg, startPos, 0, endPos - startPos, Height);
                        }

                        startPos = endPos;
                    }

                    // One more time, this time for the frame labels.
                    frame = leftFrame;
                    startPos = GetXPos(timeStart);

                    float frameLabelPos = (float)localHeight - 70.0f;

                    while (startPos < Width && ++frame < Scene.FrameCount)
                    {
                        float nextTime = Scene.GetFrame(frame).Time;
                        int endPos = GetXPos(nextTime);

                        if (startPos - lastFrameLabel > 100)
                        {
                            e.Graphics.DrawString(String.Format("{0}", frame-1), FrameLabel, FrameLabelBrush, (float)startPos + 5.0f, frameLabelPos);
                            lastFrameLabel = startPos;
                        }

                        startPos = endPos;
                    }
                }



                if (LastMouseX != NO_MOUSE && mouseFrame >= 0)
                {
                    float length = 0.0f;
                    if (Scene.FrameCount > mouseFrame + 1)
                    {
                        length = (Scene.GetFrame(mouseFrame + 1).Time - Scene.GetFrame(mouseFrame).Time) * 1000.0f;
                    }

                    tooltipText.Append(String.Format("Time: {0:F3}s, Frame {1}, Length: {2}ms\n\n", GetSeconds(LastMouseX), mouseFrame, length));
                }

                for (int x = 0; x < Scene.maxDeviceIndex; x++)
                {
                    Device device = Scene.GetDevice(x);

                    if (device != null)
                    {
                        RenderDevice(e.Graphics, device, yPos, localHeight, tooltipText);

                        yPos += (BarHeight + Spacing) * 4;
                    }
                }

                tooltip.SetToolTip(tooltipText.ToString());

                tooltip.Draw(e.Graphics, DefaultFont, Width, Height);
            }
        }

        private void UpdateScrollbar()
        {
            if (Scene != null)
            {
                float lastTime = Scene.GetFrame(Scene.FrameCount - 1).Time;
                Scroller.Maximum = (int)(lastTime * 1000);
            }
        }

        private void UpdateScrollbarPos()
        {
            if (Scene != null)
            {
                Scroller.Value = Math.Min(Math.Max((int)(Offset * 1000.0f), 0), Scroller.Maximum);
            }
        }

        private void Scroller_Scroll(object sender, ScrollEventArgs e)
        {
            Offset = ((float)Scroller.Value) / 1000.0f;
            Refresh();
        }

        protected override void OnMouseWheel(MouseEventArgs e)
        {
            if (e.Delta != 0)
            {
                ZoomFactor *= (e.Delta < 0) ? 1.1f : 0.9f;
                UpdateScrollbar();
                Refresh();
            }

            base.OnMouseWheel(e);
        }

        private void StreamingTimeline_MouseMove(object sender, MouseEventArgs e)
        {
            LastMouseX = e.X;
            LastMouseY = e.Y;

            MouseFrameNumber = GetFrameAtTime(GetSeconds(LastMouseX));

            if (IsDragging)
            {
                int delta = e.X - DragStartX;
                float deltaSeconds = delta * ZoomFactor;

                Offset = DragStartOffset - deltaSeconds;
                UpdateScrollbarPos();
            }

            tooltip.MouseMove(e);

            HoverStreamable = null;
            Refresh();
        }

        private void StreamingTimeline_MouseHover(object sender, EventArgs e)
        {
            Focus();
        }

        private void StreamingTimeline_MouseUp(object sender, MouseEventArgs e)
        {
            ChangingSelection = true;
            IsDragging = false;

            // Select the streamable and the frame only if we didn't drag or use the context menu.
            if (e.Button == MouseButtons.Left)
            {
                // Filter out drag movements
                if (DragTimer.ElapsedMilliseconds < 1000 && Math.Abs(e.X - DragStartX) < 5)
                {
                    if (HoverStreamable != null)
                    {
                        Controller.OnSelectStreamable(HoverStreamable, true);
                    }

                    // Also select this frame, and this time.
                    float seconds = GetSeconds(e.X);
                    int frame = GetFrameAtTime(seconds);

                    if (frame != -1)
                    {
                        if (frame > 0)
                        {
                            frame--;
                        }
                        Controller.SetToFrame(frame, true);
                    }
                }
            }

            ChangingSelection = false;
        }

        private void StreamingTimeline_MouseDown(object sender, MouseEventArgs e)
        {
            if (HoverStreamable == null)
            {
                DragStartOffset = Offset;
                IsDragging = true;
            }

            DragStartX = e.X;
            DragTimer.Restart();
        }

        private void GoToNextMarker(int stride)
        {
            // TODO: We should cache the left-most marker?
            Device device = Scene.GetDevice(0); // TODO: Do we need to find a usable device here first? Do we support per-device markers at some point?
            int evCount = device.DeviceEvents.Count;
            int lastIndex = -1;

            for (int index = 0; index < evCount; index++)
            {
                if (device.DeviceEvents[index].Action == DeviceEvent.ActionType.MARKER)
                {
                    lastIndex = index;
                    if (device.DeviceEvents[index].Time >= Offset)
                    {
                        break;
                    }
                }
            }

            if (lastIndex != -1)
            {
                // We found the current marker - find the next one.
                int limit = (stride > 0) ? evCount - 1 : 0;

                while (lastIndex != limit)
                {
                    lastIndex += stride;

                    if (device.DeviceEvents[lastIndex].Action == DeviceEvent.ActionType.MARKER)
                    {
                        Offset = device.DeviceEvents[lastIndex].Time;
                        UpdateScrollbarPos();
                        Refresh();
                        break;
                    }
                }
                return;
            }
        }

        protected override bool IsInputKey(Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Left:
                case Keys.Right:
                    return true;
            }
            return base.IsInputKey(keyData);
        }

        private void StreamingTimeline_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Left:
                    if (e.Control)
                    {
                        GoToNextMarker(-1);
                    }
                    break;

                case Keys.Right:
                    if (e.Control)
                    {
                        GoToNextMarker(1);
                    }
                    break;
            }
        }
    }
}
