﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StreamingVisualize.Model;
using System.IO;
using StreamingVisualize.View;
using System.IO.Compression;
using System.Threading;

namespace StreamingVisualize
{
    public partial class MainFrame : Form
    {
        Listener listener;
		FlexibleClient client;

        private String[] filesToLoad;

        public MainFrame(String[] filesToLoad)
        {
            this.filesToLoad = filesToLoad;

            InitializeComponent();

            listener = new Listener();
            listener.OnConnectionCb = new Listener.OnConnection(OnConnection);
            listener.OnErrorCb = new Listener.OnError(OnError);

            listener.Start();

			//enable drag and drop
			this.AllowDrop = true;
			this.DragEnter += new DragEventHandler(MainFrameDrag);
			this.DragDrop += new DragEventHandler(MainFrameDrop);
        }

        // Called from the listener thread when a new connection starts.
        private void OnConnection(Scene scene, Communication communication)
        {
            Invoke(new Listener.OnConnection(NewConnectionHandler), new object[] { scene, communication });
        }

        // Called from the listener thread when an error occurs.
        private void OnError(String errorMsg)
        {
            Invoke(new Listener.OnError(ErrorHandler), new object[] { errorMsg });
        }

        // Called on UI thread when a new connection starts.
        private void NewConnectionHandler(Scene scene, Communication communication)
        {
			client = new FlexibleClient(scene, communication);
			client.MdiParent = this;
			client.Dock = DockStyle.Fill;
			client.Show();
        }

        // Called on UI thread when a fatal error occurs.
        private void ErrorHandler(String errorMsg)
        {
			Console.WriteLine("StackTrace: '{0}'", Environment.StackTrace);
            MessageBox.Show(this, errorMsg, "Error");
            //Dispose();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();

            //ofd.InitialDirectory = "c:\\";
            ofd.Filter = "StreamingVisualize (*.svz)|*.svz|All files (*.*)|*.*";
            ofd.FilterIndex = 0;
            ofd.RestoreDirectory = true;


            if (ofd.ShowDialog() == DialogResult.OK)
            {
          //      try
                {
                    Stream stream = ofd.OpenFile();
                    LoadFile(stream, ofd.FileName);
                    stream.Close();
                }
            /*    catch (Exception ex)
                {
                    MessageBox.Show("Error loading file: " + ex.Message);
                }*/
            }
        }

        public void LoadFile(String filename)
        {
           // try
            {
                Stream stream = new FileStream(filename, FileMode.Open);
                LoadFile(stream, filename);
                stream.Close();
            }
            /*catch (System.Exception ex)
            {
                MessageBox.Show("Error loading file: " + ex.Message);
            }*/
        }

        public void LoadFile(Stream stream, String filename)
        {
        //    try
            {
                Stream decompressStream = null;
                Scene scene = new Scene();
                scene.Filename = filename;

                VersionedReader reader = new VersionedReader(scene, stream);
                reader.ReadHeader();

                if (reader.IsCompressed())
                {
                    // We need to switch to a decompression stream.
                    decompressStream = new BufferedStream(new DeflateStream(stream, CompressionMode.Decompress), 262144);
                    reader.SetStream(decompressStream);
                }

                reader.Read(scene);
                decompressStream.Close();
                reader.Close();

                FlexibleClient newClient = new FlexibleClient(scene, null);
                newClient.MdiParent = this;
                newClient.Dock = DockStyle.Fill;
                newClient.Show();

                // Make sure all views know about all devices.
                scene.RegisterAllDevices();

                // When we're done, switch to the last frame to set everything in motion.
                newClient.GoToLastFrame();
                newClient.ClientInfoUpdatedCore();
            }
           /* catch (Exception ex)
            {
                MessageBox.Show("Error loading file: " + ex.Message);
            }*/
        }

        private void MainFrame_FormClosed(object sender, FormClosedEventArgs e)
        {
            listener.OnConnectionCb = null;
            listener.OnErrorCb = null;

            listener.RequestStop();
            Application.Exit();
        }

        private void quitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listener.RequestStop();
            Application.Exit();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox aboutBox = new AboutBox();
            aboutBox.Show();
        }

        private void MainFrame_Load(object sender, EventArgs e)
        {
            foreach (String file in filesToLoad)
            {
                LoadFile(file);
            }
        }

		public void MainFrameDrag(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent(DataFormats.FileDrop)) e.Effect = DragDropEffects.Copy;
		}

		public void MainFrameDrop(object sender, DragEventArgs e)
		{
			string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
			foreach (string file in files) LoadFile(file);
		}

		// Creates the error message and displays it. 
		private static DialogResult ShowThreadExceptionDialog(string title, Exception e)
		{
			string errorMsg = "An application error occurred. Please contact the adminstrator " +
				"with the following information:\n\n";
			errorMsg = errorMsg + e.Message + "\n\nStack Trace:\n" + e.StackTrace;
			Console.WriteLine("StackTrace: '{0}'", Environment.StackTrace);
			Console.WriteLine("StackTrace e: '{0}'", e.StackTrace);
			return MessageBox.Show(errorMsg, title, MessageBoxButtons.AbortRetryIgnore,
				MessageBoxIcon.Stop);			
		}
		// Handle the UI exceptions by showing a dialog box, and asking the user whether 
		// or not they wish to abort execution. 
		public void ThreadException(object sender, ThreadExceptionEventArgs t)
		{
			DialogResult result = DialogResult.Cancel;
			try
			{
				Console.WriteLine("Thread Exception", Environment.StackTrace);
				client.saveAsToolStripMenuItem_Click(this, t);
				Console.WriteLine("StackTrace: '{0}'", Environment.StackTrace);
				result = ShowThreadExceptionDialog("Windows Forms Error", t.Exception);
			}
			catch
			{
				try
				{
					Console.WriteLine("StackTrace: '{0}'", Environment.StackTrace);
					MessageBox.Show("Fatal Windows Forms Error",
						"Fatal Windows Forms Error", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Stop);
				}
				finally
				{
					Application.Exit();
				}
			}

			// Exits the program when the user clicks Abort. 
			if (result == DialogResult.Abort)
				Application.Exit();
		}
    }
}
