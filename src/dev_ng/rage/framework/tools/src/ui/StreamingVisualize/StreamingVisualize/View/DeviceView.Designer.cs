﻿namespace StreamingVisualize.View
{
    partial class DeviceView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
			this.DeviceName = new System.Windows.Forms.Label();
			this.DeviceEvents = new System.Windows.Forms.DataGridView();
			this.deviceEventBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.Frame = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Time = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Action = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.LSN = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.OperationSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.StreamerHandle = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Streamable = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.RPF = new System.Windows.Forms.DataGridViewTextBoxColumn();
			((System.ComponentModel.ISupportInitialize)(this.DeviceEvents)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.deviceEventBindingSource)).BeginInit();
			this.SuspendLayout();
			// 
			// DeviceName
			// 
			this.DeviceName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.DeviceName.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.DeviceName.Location = new System.Drawing.Point(-3, 0);
			this.DeviceName.Name = "DeviceName";
			this.DeviceName.Size = new System.Drawing.Size(426, 13);
			this.DeviceName.TabIndex = 0;
			this.DeviceName.Text = "Device Name";
			// 
			// DeviceEvents
			// 
			this.DeviceEvents.AllowUserToAddRows = false;
			this.DeviceEvents.AllowUserToDeleteRows = false;
			this.DeviceEvents.AllowUserToResizeRows = false;
			this.DeviceEvents.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.DeviceEvents.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.DeviceEvents.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.DeviceEvents.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Frame,
            this.Time,
            this.Action,
            this.LSN,
            this.OperationSize,
            this.StreamerHandle,
            this.Streamable,
            this.RPF});
			dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
			dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
			this.DeviceEvents.DefaultCellStyle = dataGridViewCellStyle2;
			this.DeviceEvents.Location = new System.Drawing.Point(0, 16);
			this.DeviceEvents.Name = "DeviceEvents";
			this.DeviceEvents.ReadOnly = true;
			dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.DeviceEvents.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.DeviceEvents.RowHeadersVisible = false;
			this.DeviceEvents.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.DeviceEvents.Size = new System.Drawing.Size(426, 260);
			this.DeviceEvents.TabIndex = 1;
			this.DeviceEvents.VirtualMode = true;
			this.DeviceEvents.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.DeviceEvents_CellFormatting);
			this.DeviceEvents.CellValueNeeded += new System.Windows.Forms.DataGridViewCellValueEventHandler(this.DeviceEvents_CellValueNeeded);
			this.DeviceEvents.SelectionChanged += new System.EventHandler(this.DeviceEvents_SelectionChanged);
			// 
			// deviceEventBindingSource
			// 
			this.deviceEventBindingSource.DataSource = typeof(StreamingVisualize.Model.DeviceEvent);
			// 
			// Frame
			// 
			this.Frame.HeaderText = "Frame";
			this.Frame.Name = "Frame";
			this.Frame.ReadOnly = true;
			this.Frame.Width = 60;
			// 
			// Time
			// 
			this.Time.HeaderText = "Time";
			this.Time.Name = "Time";
			this.Time.ReadOnly = true;
			this.Time.Width = 70;
			// 
			// Action
			// 
			this.Action.HeaderText = "Action";
			this.Action.Name = "Action";
			this.Action.ReadOnly = true;
			this.Action.Width = 60;
			// 
			// LSN
			// 
			this.LSN.HeaderText = "LSN";
			this.LSN.Name = "LSN";
			this.LSN.ReadOnly = true;
			this.LSN.Width = 70;
			// 
			// OperationSize
			// 
			this.OperationSize.HeaderText = "Size";
			this.OperationSize.Name = "OperationSize";
			this.OperationSize.ReadOnly = true;
			// 
			// StreamerHandle
			// 
			this.StreamerHandle.HeaderText = "Handle";
			this.StreamerHandle.Name = "StreamerHandle";
			this.StreamerHandle.ReadOnly = true;
			// 
			// Streamable
			// 
			this.Streamable.HeaderText = "Streamable";
			this.Streamable.Name = "Streamable";
			this.Streamable.ReadOnly = true;
			// 
			// RPF
			// 
			this.RPF.HeaderText = "RPF";
			this.RPF.Name = "RPF";
			this.RPF.ReadOnly = true;
			// 
			// DeviceView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.DeviceEvents);
			this.Controls.Add(this.DeviceName);
			this.Name = "DeviceView";
			this.Size = new System.Drawing.Size(426, 289);
			this.Load += new System.EventHandler(this.DeviceView_Load);
			((System.ComponentModel.ISupportInitialize)(this.DeviceEvents)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.deviceEventBindingSource)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label DeviceName;
        private System.Windows.Forms.DataGridView DeviceEvents;
		private System.Windows.Forms.BindingSource deviceEventBindingSource;
		private System.Windows.Forms.DataGridViewTextBoxColumn Frame;
		private System.Windows.Forms.DataGridViewTextBoxColumn Time;
		private System.Windows.Forms.DataGridViewTextBoxColumn Action;
		private System.Windows.Forms.DataGridViewTextBoxColumn LSN;
		private System.Windows.Forms.DataGridViewTextBoxColumn OperationSize;
		private System.Windows.Forms.DataGridViewTextBoxColumn StreamerHandle;
		private System.Windows.Forms.DataGridViewTextBoxColumn Streamable;
		private System.Windows.Forms.DataGridViewTextBoxColumn RPF;
    }
}
