﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StreamingVisualize.Model
{
    public class StringPool : IVersionedSerializable
    {
        private List<String> StringList = new List<String>();

        // Map a string to an index in the StringList.
        private Dictionary<String, int> StringHashMap = new Dictionary<String, int>();



        public int GetOrCreateStringId(String text)
        {
            if (text == null)
            {
                return -1;

            }
            lock (this)
            {
                int result;

                // Is this string in our pool already?
                if (!StringHashMap.TryGetValue(text, out result))
                {
                    // No - add it.
                    StringList.Add(text);
                    result = StringList.Count-1;
                    StringHashMap.Add(text, result);
                }

                return result;
            }
        }

        public int GetStringId(String text)
        {
            if (text == null)
            {
                return -1;
            }

            lock (this)
            {
                return StringHashMap[text];
            }
        }

        public String GetString(String text)
        {
            if (text == null)
            {
                return null;
            }

            int index = GetOrCreateStringId(text);
            return StringList[index];
        }

        public String GetStringFromId(int id)
        {
            if (id == -1)
            {
                return null;
            }

            return StringList[id];
        }

        public void Serialize(VersionedWriter writer)
        {
            writer.Write(StringList.Count);
            
            for (int x=0; x<StringList.Count; x++)
            {
                byte[] stringBytes = Encoding.UTF8.GetBytes(StringList[x]);
                writer.Write((short) stringBytes.Length);
                writer.Write(stringBytes);
            }
        }

        public void Deserialize(VersionedReader reader, int version)
        {
            int count = reader.ReadInt();
            StringList.Capacity = count;

            for (int x = 0; x < count; x++)
            {
                int size = reader.ReadShort();
                byte[] buffer = new byte[size];         // TODO: Keep a temp read buffer?
                reader.ReadBytes(buffer, size);
                String text = System.Text.Encoding.UTF8.GetString(buffer);
                StringList.Add(text);

                StringHashMap.Add(text, x);
            }
        }
    }
}
