﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace StreamingVisualize.Model
{
    public class Analysis
    {
        public class Churn
        {
            public const int HISTORY_SIZE = 5;
            public const int REPORTING_THRESHOLD = 2;       // Needs this many bad events in the frame range to be considered
            public const int TIMESPAN_THRESHOLD = 1000;     // A state change is only considered bad if it's within this many frames

            public Streamable Streamable { get; set; }

            //public Streamable.Status StartState { get; set; }

            public int LoadFrame { get; set; }

            public int UnloadFrame { get; set; }

            public String LoadContext { get; set; }

            public String UnloadContext { get; set; }

            // Frames at which a state change occurred
            //public int[] Frames;

            //public int FrameCount { get { return Frames.Length; } }

        }

        public class BadLod
        {
            public Streamable Streamable { get; set; }

            // Frame in which the problem started
            public int FirstFrame { get; set; }

            // Last frame in which the problem occurred
            public int LastFrame { get; set; }

            // Most recent frame this object was requested
            public int FirstRequest { get; set; }

            // LOD type of the archetype
            public LodType LodType { get; set; }

            // Bad LOD type
//            public Entity.NewState.LodStates LodState { get; set; }

            // Last mapdata dependency to come resident
            public Streamable LastResidentMapData { get; set; }

            // Frame when LastResidentMapData became resident
            public int LastMapDataFrameNumber { get; set; }
        }

        public class UnusedArchetype
        {
            public Streamable Streamable { get; set; }

            // Frame in which this archetype came in
            public int StreamedFrameNumber { get; set; }

            // Frame in which this archetype was actually used, -1 for never
            public int UsedFrameNumber { get; set; }

            // LOD type of the archetype
            public LodType LodType { get; set; }

            // Context under which the archetype was streamed in
            public String Context { get; set; }
        }

        public class MissingImaps
        {
            public Streamable EntityStreamable { get; set; }

            public Streamable MapDataStreamable { get; set; }

            public int ImapStreamData { get; set; }
        }

        public class StreamableState
        {
            public Streamable Streamable { get; set; }

            public int FrameRequested { get; set; }

            public int FrameLoaded { get; set; }

            public int FrameUnloaded { get; set; }

            public int FrameVisibleSinceLoaded { get; set; }

            public int FrameNeeded { get; set; }
        }

        public class NumberOverview
        {
            public float Average
            {
                get
                {
                    if (Count == 0.0f)
                    {
                        return 0.0f;
                    }

                    return Sum / Count;
                }
            }

            public float Sum = 0.0f;

            public float Count = 0.0f;

            public float Peak = 0.0f;



            public void AddNumber(float number)
            {
                Sum += number;
                Count += 1.0f;

                Peak = Math.Max(Peak, number);
            }

            public void Reset()
            {
                Sum = Count = Peak = 0.0f;
            }
        }

        private class StreamableEntityInfo
        {
            public bool[] WasVisible;
        }

        public class FrameState
        {
            public List<StreamableState> Streamables;
        }

        private class FrameRange
        {
            public int FrameCount;
            public String Name;

            public FrameRange(int frameCount, String name)
            {
                FrameCount = frameCount;
                Name = name;
            }
        }

        private static FrameRange[] BadLodFrameRanges = new FrameRange[] {
            new FrameRange(  3, "Very Short" ),
            new FrameRange(  8, "Short" ),
            new FrameRange( 15, "Medium" ),
            new FrameRange( 25, "Long" ),
            new FrameRange( 40, "Very Long" ),
            new FrameRange( 40, "Well, Fuck" ),
        };

        private static FrameRange[] ChurnFrameRanges = new FrameRange[] {
            new FrameRange(  3, "Super Short" ),
            new FrameRange( 12, "Short" ),
            new FrameRange( 25, "Medium" ),
            new FrameRange( 50, "Long" ),
            new FrameRange( 80, "Very Long" ),
            new FrameRange( 80, "Borderline Churn" ),
        };

        private static FrameRange[] DoubleLoadFrameRanges = new FrameRange[] {
            new FrameRange(  3, "Super Short" ),
            new FrameRange( 20, "Short" ),
            new FrameRange( 100, "Medium" ),
            new FrameRange( 500, "Long" ),
            new FrameRange( 1000, "Very Long" ),
            new FrameRange( 2000, "Borderline Churn" ),
        };



        // Map to the current state, used while creating the final map. Valid for the currently processed frame only.
        private Dictionary<Streamable, StreamableState> CurrentStates = new Dictionary<Streamable, StreamableState>();
        private Dictionary<Streamable, BadLod> CurrentLodStates = new Dictionary<Streamable, BadLod>();

        private Dictionary<Streamable, StreamableEntityInfo> StreamableEntityInfos = new Dictionary<Streamable, StreamableEntityInfo>();

        public List<FrameState> FrameStates = new List<FrameState>();


        public Scene Scene;

        public int FirstFrame;
        public int LastFrame;

        public float WallclockStart;
        public float WallclockEnd;

        public int RequestCount;
        public int DoomedRequestCount;

        public int MissingLod;
        public int LowLod;

        public NumberOverview DeletedScores = new NumberOverview();
        public int GarbageCollected;

        public int[] MissingEntityByLod = new int[16];

        public int[] MissingLodByLod = new int[16];
        public int[] LowLodByLod = new int[16];

        public List<Churn> Churns = new List<Churn>();
        public List<Churn> DoubleLoads = new List<Churn>();
        public List<UnusedArchetype> UnusedArchetypes = new List<UnusedArchetype>();

        public float[] AvgReqToLoaded = new float[2];

        public String TestName { get; set; }

        public BadLod[] FrameCountBadLods;

        // Number of times a bad LOD with each frame count occurred.
        // BadLodCount[2] == how many archetypes weren't visible for 2 frames
        public int[] BadLodFrameCount = new int[128];
        public int[] ChurnFrameCount = new int[128];

        public int[,] DoubleLoadByLod = new int[16,16];
        public int[,] ChurnByLod = new int[16, 16];
        public int[,] BadLodByLod = new int[16, 16];

        public int[] RequestByModule = new int[128];

        // Number of seconds spent per module, per device, index 0 = all devices
        public float[,] StreamTimePerModule;

        // Number of seconds spent per LOD, per device, index 0 = all devices
        public float[,] StreamTimePerLod;

        // Number of seconds idle, per device
        public float[] IdleTime;

        // All bad lods.
        public List<BadLod> BadLods;



        public Analysis()
        {
            TestName = "Custom Test";
        }

        private int GetFrameRangeIndex(FrameRange[] frameRange, int frameCount)
        {
            for (int x = 0; x < frameRange.Length - 1; x++)
            {
                if (frameCount < frameRange[x].FrameCount)
                {
                    return x;
                }
            }

            return frameRange.Length - 1;
        }

        public void Reset()
        {
            Churns = new List<Churn>();
            DoubleLoads = new List<Churn>();

            RequestCount = 0;
            DoomedRequestCount = 0;
            MissingLod = 0;
            LowLod = 0;
            GarbageCollected = 0;

            CurrentStates = new Dictionary<Streamable, StreamableState>();
            CurrentLodStates = new Dictionary<Streamable, BadLod>();
            UnusedArchetypes = new List<UnusedArchetype>();
            StreamableEntityInfos = new Dictionary<Streamable, StreamableEntityInfo>();

            BadLods = new List<BadLod>();

            Array.Clear(LowLodByLod, 0, LowLodByLod.Length);
            Array.Clear(MissingLodByLod, 0, MissingLodByLod.Length);
            Array.Clear(RequestByModule, 0, RequestByModule.Length);

            DeletedScores.Reset();
        }

        public void Analyze()
        {
            Reset();

            Console.WriteLine("Creating Analysis...");
            List<Streamable> tempList = new List<Streamable>();

            int sumReqToLoad = 0;
            int reqCount = 0;

            Console.WriteLine("Getting wallclock times...");
            // We currently only store the wallclock time in device events, so let's harvest those.

            WallclockStart = float.MaxValue;
            WallclockEnd = float.MinValue;

            for (int x = 0; x < Scene.maxDeviceIndex; x++)
            {
                Device device = Scene.GetDevice(x);
                if (device != null)
                {
                    WallclockStart = Math.Min(WallclockStart, device.GetEarliestWallclockTime(FirstFrame));
                    WallclockEnd = Math.Max(WallclockEnd, device.GetLatestWallclockTime(LastFrame));
                }
            }

            StreamTimePerModule = new float[Scene.maxModuleIndex, Scene.maxDeviceIndex + 1];
            StreamTimePerLod = new float[Scene.LodTypes.Count, Scene.maxDeviceIndex + 1];
            IdleTime = new float[Scene.maxDeviceIndex + 1];

            Array.Clear(StreamTimePerModule, 0, Scene.maxModuleIndex * (Scene.maxDeviceIndex + 1));
            Array.Clear(StreamTimePerLod, 0, Scene.LodTypes.Count * (Scene.maxDeviceIndex + 1));
            Array.Clear(IdleTime, 0, Scene.maxDeviceIndex + 1);

            Console.WriteLine("Creating Frame Map...");
            // Let's get the vital stats
            for (int frameNumber = FirstFrame; frameNumber <= LastFrame; frameNumber++)
            {
                FrameState frameState = new FrameState();

                Frame frame = Scene.GetFrame(frameNumber);

                // Count the number of requests, separate by doomed vs. fulfilled
                for (int x = 0; x < frame.requests.Count; x++)
                {
                    if ((frame.requests[x].Flags & Request.DOOMED) == 0)
                    {
                        RequestCount++;

                        Module module = frame.requests[x].Streamable.Module;

                        if (module != Scene.DummyStreamableModule)
                        {
                            RequestByModule[module.Index]++;
                        }
                    }
                    else
                    {
                        DoomedRequestCount++;
                    }
                }

                if (frame.BadLodEntities != null)
                {
                    for (int x = 0; x < frame.BadLodEntities.Count; x++)
                    {
                        Entity entity = frame.BadLodEntities[x];
                        Entity.NewState state = entity.NewHistory.GetStateAt(frameNumber);

                        Debug.Assert(state.LodState != Entity.NewState.LodStates.LOD_OK);

                        int lodType = state.LodType;

                        /*if (state.LodState == Entity.NewState.LodStates.LOD_LOW)
                        {
                            LowLod++;
                            LowLodByLod[lodType]++;
                        }
                        else*/
                        {
                            MissingLod++;
                            MissingLodByLod[lodType]++;
                        }
                    }
                }

                // Look for churn - streamables that are streamed in and out over a short period of time.
/*                int streamableCount = Scene.MaxStreamableIndex;
                int[] changeFrames = new int[Churn.HISTORY_SIZE];

                for (int x=0; x<streamableCount; x++)
                {
                    Streamable streamable = Scene.GetStreamable(x);

                    if (streamable != null)
                    {
                        int eventCount = streamable.Events.Count;

                        // Only look for events within this range.
                        Streamable.Status initialState = Streamable.Status.NOTLOADED;
                        int eventIndex = 0;
                        changeFrames[0] = 0;

                        while (eventIndex < eventCount && streamable.Events[eventIndex].FrameNumber < FirstFrame)
                        {
                            int eventFrame = streamable.Events[eventIndex].FrameNumber;
                            RscEvent.ActionType action = streamable.Events[eventIndex].Action;

                            if (action == RscEvent.ActionType.Loaded)
                            {
                                initialState = Streamable.Status.LOADED;
                                changeFrames[0] = eventFrame;
                            }
                            else if (action == RscEvent.ActionType.Unloaded)
                            {
                                initialState = Streamable.Status.NOTLOADED;
                                changeFrames[0] = eventFrame;
                            }

                            eventIndex++;
                        }

                        int nextIndex = 1;
                        int badEvents = 0;

                        // We found the first event that's inside the given frame range. Let's check for all
                        // events until we leave the range.
                        while (eventIndex < eventCount && streamable.Events[eventIndex].FrameNumber <= LastFrame)
                        {
                            int eventFrame = streamable.Events[eventIndex].FrameNumber;
                            RscEvent.ActionType action = streamable.Events[eventIndex].Action;

                            if (action == RscEvent.ActionType.Loaded || action == RscEvent.ActionType.Unloaded)
                            {
                                changeFrames[nextIndex++] = eventFrame;

                                // Was this event very close to the previous one?
                                if (eventFrame - changeFrames[nextIndex-2] < Churn.TIMESPAN_THRESHOLD)
                                {
                                    // Yep. This shouldn't happen, at least not twice.
                                    badEvents++;
                                }

                                if (nextIndex >= changeFrames.Length)
                                {
                                    // We have enough evidence that this is churning like stupid.
                                    break;
                                }
                            }

                            eventIndex++;
                        }

                        if (badEvents >= Churn.REPORTING_THRESHOLD)
                        {
                            // This is bad churn. Worth reporting.
                            Churn churn = new Churn();
                            churn.Streamable = streamable;
                            churn.StartState = initialState;
                            churn.Frames = new int[changeFrames.Length];

                            Array.Copy(changeFrames, churn.Frames, changeFrames.Length);

                            Churns.Add(churn);
                        }
                    }
                }
                */
                // Let's check on all streamables and get some stats.
                // What's being requested this frame?
                foreach (Request request in frame.requests)
                {
                    if ((request.Flags & Request.DOOMED) == 0)
                    {
                        Streamable streamable = request.Streamable;
                        StreamableState state;

                        if (!CurrentStates.TryGetValue(streamable, out state))
                        {
                            // Create a new state.
                            state = new StreamableState();
                            state.Streamable = streamable;
                            state.FrameRequested = frameNumber;
                        }
                    }
                }

                // What about unrequests?

                // Let's go through our favorites - the bad LOD states.
                if (frame.BadLodEntities != null)
                {
                    foreach (Entity entity in frame.BadLodEntities)
                    {
                        Entity.NewState entityState = entity.NewHistory.GetStateAt(frameNumber);
                        Streamable streamable = entityState.Streamable;

                        Debug.Assert(entityState.LodState != Entity.NewState.LodStates.LOD_OK);

                        BadLod lodState;

                        if (!CurrentLodStates.TryGetValue(streamable, out lodState))
                        {
                            lodState = new BadLod();
                            lodState.Streamable = streamable;
                            //lodState.LodState = entityState.LodState;
                            lodState.FirstFrame = frameNumber;
                            lodState.LodType = Scene.GetLodType(streamable.IdentityHistory.GetStateAt(frameNumber).LodTypeId);

                            BadLods.Add(lodState);
                            CurrentLodStates[streamable] = lodState;
                        }

                        lodState.LastFrame = frameNumber;
                    }
                }

                List<BadLod> badLods = new List<BadLod>(CurrentLodStates.Values);

                // Did we resolve any of the bad LOD states?
                foreach (BadLod badLod in badLods)
                {
                    if (badLod.LastFrame != frameNumber)
                    {
                        // This one is resolved.
                        CurrentLodStates.Remove(badLod.Streamable);
                    }
                }
            }

            Console.WriteLine("Creating Stream Time Map...");

            // Let's do that per device...
            for (int x = 0; x < Scene.maxDeviceIndex; x++)
            {
                Device device = Scene.GetDevice(x);

                if (device != null)
                {
                    // Find the first event that applies to us.
                    int eventCount = device.DeviceEvents.Count;

                    int eventIndex = 0;
                    float CurrentTime = 0.0f;

                    while (eventIndex < eventCount && device.DeviceEvents[eventIndex].Frame < FirstFrame)
                    {
                        eventIndex++;
                    }

                    if (eventIndex == eventCount)
                    {
                        // Nothing relevant here. We were completely idle.
                        IdleTime[0] += WallclockEnd - WallclockStart;
                        IdleTime[x+1] += WallclockEnd - WallclockStart;
                    }
                    else
                    {
                        CurrentTime = device.DeviceEvents[eventIndex].Time;
                        Streamable lastStreamable = null;

                        while (eventIndex < eventCount)
                        {
                            // Find the first new stream request.
                            while (eventIndex < eventCount && device.DeviceEvents[eventIndex].Action != DeviceEvent.ActionType.READ)
                            {
                                eventIndex++;
                            }

                            if (eventIndex == eventCount || device.DeviceEvents[eventIndex].Time > WallclockEnd)
                            {
                                // Nothing else past this point.
                                IdleTime[0] += WallclockEnd - CurrentTime;
                                IdleTime[x + 1] += WallclockEnd - CurrentTime;
                                break;
                            }

                            float LoadStartTime = device.DeviceEvents[eventIndex].Time;
                            IdleTime[0] += LoadStartTime - CurrentTime;
                            IdleTime[x + 1] += LoadStartTime - CurrentTime;

                            Streamable streamable = device.DeviceEvents[eventIndex].Streamable;

                            if (LoadStartTime != CurrentTime && lastStreamable != null)
                            {
                                int frameNumber = device.DeviceEvents[eventIndex].Frame;
                                Console.WriteLine("{0:F} Idle time between {1:F} and {2:F}, cumulative {3:F}, between {4} and {5}", LoadStartTime - CurrentTime, CurrentTime,
                                    LoadStartTime, IdleTime[x + 1], lastStreamable.GetNameAt(frameNumber), streamable.GetNameAt(frameNumber));
                            }

                            lastStreamable = streamable;

                            // Beginning of an event. When does it end?
                            // We don't store FINISH in the events yet, which sucks, so we have to search the streamable, ffs.
                            IList<RscEvent> strEvents = streamable.Events;
                            int rscEventCount = strEvents.Count;

                            float FinishTime = -1.0f;

                            // Find the matching event.
                            for (int y = 0; y < rscEventCount; y++)
                            {
                                // Only REQUEST items contain all the timing information, including request, process, finish.
                                if (strEvents[y].Action == RscEvent.ActionType.REQUEST)
                                {
                                    if (strEvents[y].ProcessTime == LoadStartTime)
                                    {
                                        // Found it. Now what's the finish time?
                                        FinishTime = strEvents[y].LoadTime;
                                        break;
                                    }
                                }
                            }

                            Debug.Assert(FinishTime != -1.0f);          // We couldn't find a matching resource event?

                            // What came first - the next request, or the  finish time?
                            eventIndex++;

                            int nextIndex = eventIndex;

                            while (nextIndex < eventCount && device.DeviceEvents[nextIndex].Time < WallclockEnd && device.DeviceEvents[nextIndex].Action != DeviceEvent.ActionType.READ)
                            {
                                nextIndex++;
                            }

                            if (nextIndex < eventCount && device.DeviceEvents[nextIndex].Time < WallclockEnd && device.DeviceEvents[nextIndex].Action == DeviceEvent.ActionType.READ)
                            {
                                // Here's the next one.
                                if (device.DeviceEvents[nextIndex].Time < FinishTime)
                                {
                                    //Console.WriteLine("Next request earlier than finish time, {0:F} vs {1:F}", FinishTime, device.DeviceEvents[nextIndex].Time);
                                    FinishTime = device.DeviceEvents[nextIndex].Time;
                                }
                            }

                            // How long did we stream?
                            float StreamTime = FinishTime - LoadStartTime;

                            StreamTimePerModule[streamable.Module.Index, 0] += StreamTime;
                            StreamTimePerModule[streamable.Module.Index, x + 1] += StreamTime;

                            int lodTypeId = streamable.IdentityHistory.GetStateAt(device.DeviceEvents[eventIndex].Frame).LodTypeId;

                            if (lodTypeId != -1)
                            {
                                StreamTimePerLod[lodTypeId, 0] += StreamTime;
                                StreamTimePerLod[lodTypeId, x + 1] += StreamTime;
                            }

                            CurrentTime = FinishTime;
                        }
                    }
                }
            }

            // Let's look at entities.
            Console.WriteLine("Looking up entities...");

            for (int x = 0; x < Scene.CurrentFrame.Entities.Count; x++)
            {
                Entity entity = Scene.CurrentFrame.Entities[x];

                // Go through all the entity states in this frame - when did the visibility change, when did the loading
                // state change?
                int stateIndex = entity.NewHistory.GetStateIndexAt(FirstFrame);

                if (stateIndex != -1)
                {
                    Entity.NewState curState = entity.NewHistory.GetStateAt(stateIndex);
                    Streamable curStreamable = curState.Streamable;
                    
                    StreamableEntityInfo entityInfo = null;

                    if (curStreamable != null)
                    {
                        if (!StreamableEntityInfos.TryGetValue(curStreamable, out entityInfo))
                        {
                            // First time this streamable is associated with an entity.
                            entityInfo = new StreamableEntityInfo();
                            entityInfo.WasVisible = new bool[LastFrame - FirstFrame + 1];
                            Array.Clear(entityInfo.WasVisible, 0, entityInfo.WasVisible.Length);
                            StreamableEntityInfos[curStreamable] = entityInfo;
                        }
                    }

                    // Let's update our status while we're visible.
                    int stateCount = entity.NewHistory.StateCount;
                    int curFrame = Math.Max(curState.Frame, FirstFrame);

                    while (++stateIndex < stateCount)
                    {
                        Entity.NewState nextState = entity.NewHistory.GetStateAt(stateIndex);

                        if (curState.IsVisible(Scene) && entityInfo != null)
                        {
                            int LastStateFrame = Math.Min(nextState.Frame, LastFrame);
                            for (int y = curFrame; y < LastStateFrame; y++)
                            {
                                entityInfo.WasVisible[y - FirstFrame] = true;
                            }
                        }

                        if (nextState.Streamable != curStreamable)
                        {
                            // Well fuck, a streamable change.
                            curStreamable = nextState.Streamable;

                            if (curStreamable != null)
                            {
                                if (!StreamableEntityInfos.TryGetValue(curStreamable, out entityInfo))
                                {
                                    // First time this streamable is associated with an entity.
                                    entityInfo = new StreamableEntityInfo();
                                    entityInfo.WasVisible = new bool[LastFrame - FirstFrame + 1];
                                    Array.Clear(entityInfo.WasVisible, 0, entityInfo.WasVisible.Length);
                                    StreamableEntityInfos[curStreamable] = entityInfo;
                                }
                            }
                            else
                            {
                                entityInfo = null;
                            }
                        }

                        curState = nextState;
                    }

                    if (curState.IsVisible(Scene) && entityInfo != null)
                    {
                        for (int y = curFrame; y < LastFrame; y++)
                        {
                            entityInfo.WasVisible[y - FirstFrame] = true;
                        }
                    }
                }
            }

            Console.WriteLine("Looking up Streamables...");
            Array.Clear(ChurnFrameCount, 0, ChurnFrameCount.Length);
            Array.Clear(ChurnByLod, 0, ChurnByLod.Length);
            Array.Clear(DoubleLoadByLod, 0, DoubleLoadByLod.Length);

            // Now let's go through all streamables and look at how they've been doing in between the frame range here.
            foreach (int strIndex in Scene.StrIndexToIndex.Keys)
            {
                Streamable streamable = Scene.GetStreamable(strIndex);

                if (streamable != null)
                {
                    // Let's use events rather than states - states could swallow states if there are multiple changes
                    // in one frame.
                    int eventCount = streamable.Events.Count;
                    int lastLoad = -1;
                    int firstRequest = -1;
                    int lastUnload = -1;
                    String lastUnloadContext = "";
                    Streamable.Status lastState = Streamable.Status.NOTLOADED;

                    // Keep track of the state until the start frame.
                    for (int y = 0; y < eventCount; y++)
                    {
                        RscEvent rscEvent = streamable.Events[y];

                        if (firstRequest == -1 && rscEvent.Action == RscEvent.ActionType.LoadRequested)
                        {
                            firstRequest = rscEvent.FrameNumber;
                        }
                        else if (firstRequest != -1 && rscEvent.Action == RscEvent.ActionType.Unloaded)
                        {
                            firstRequest = -1;
                        }

                        if (rscEvent.Score > 0.0f && rscEvent.Action == RscEvent.ActionType.UNREQUEST)
                        {
                            DeletedScores.AddNumber(rscEvent.Score);
                        }

                        if (rscEvent.Action == RscEvent.ActionType.UNREQUEST && rscEvent.Context.StartsWith("GARBAGE"))
                        {
                            GarbageCollected++;
                        }

                        if (rscEvent.FrameNumber >= FirstFrame)
                        {
                            if (rscEvent.FrameNumber >= LastFrame)
                            {
                                break;
                            }

                            Streamable.Status newState = rscEvent.GetStatusChange();

                            if (newState == Streamable.Status.LOADED)
                            {
                                // We just streamed something in - first of all, is any of its entities even visible?
                                // If not, this was a bit pointless.
                                lastLoad = rscEvent.FrameNumber;

                                sumReqToLoad += lastLoad - firstRequest;
                                reqCount++;
                                firstRequest = -1;

                                // Did we just double-load?
                                if (lastUnload != -1 && lastLoad - lastUnload < 5000)
                                {
                                    // That's worth mentioning.
                                    Churn churn = new Churn();
                                    churn.Streamable = streamable;
                                    churn.LoadFrame = lastLoad;
                                    churn.UnloadFrame = lastUnload;
                                    churn.LoadContext = "";
                                    churn.UnloadContext = lastUnloadContext;
                                    // Get the context for both.
                                    for (int z = y - 1; z >= 0; z--)
                                    {
                                        RscEvent prevEvent = streamable.Events[z];

                                        if (prevEvent.Action == RscEvent.ActionType.REQUEST && !prevEvent.Context.StartsWith("DEPENDENCY"))
                                        {
                                            churn.LoadContext = prevEvent.Context;
                                            break;
                                        }
                                    }

                                    int duration = rscEvent.FrameNumber - lastLoad;
//                                        DoubleLoadFrameCount[Math.Min(duration, ChurnFrameCount.Length)]++;
                                    int frameRangeIndex = GetFrameRangeIndex(ChurnFrameRanges, duration);
                                    int lodTypeId = streamable.IdentityHistory.GetStateAt(lastLoad).LodTypeId;

                                    if (lodTypeId != -1)
                                    {
                                        DoubleLoadByLod[lodTypeId, frameRangeIndex]++;
                                    }
                                    DoubleLoads.Add(churn);
                                }

                                lastUnload = -1;

                                StreamableEntityInfo entityInfo;

                                // We'll only have something here if we're an archetype. And had an entity.
                                if (StreamableEntityInfos.TryGetValue(streamable, out entityInfo))
                                {
                                    bool[] wasVisible = entityInfo.WasVisible;

                                    if (!wasVisible[rscEvent.FrameNumber - FirstFrame])
                                    {
                                        // Well great, there wasn't even a visible entity for this streamable.
                                        UnusedArchetype unused = new UnusedArchetype();

                                        unused.Streamable = streamable;
                                        unused.StreamedFrameNumber = rscEvent.FrameNumber;

                                        int visibleFrame = -1;

                                        // So when DID we get used?
                                        for (int z=rscEvent.FrameNumber - FirstFrame; z<LastFrame - FirstFrame; z++)
                                        {
                                            if (wasVisible[z])
                                            {
                                                // Finally - we found it.
                                                visibleFrame = z + FirstFrame;
                                                break;
                                            }
                                        }

                                        // Did we get streamed out before that? That would be embarrassing.
                                        if (visibleFrame != -1)
                                        {
                                            for (int z=y+1; z<eventCount; z++)
                                            {
                                                RscEvent nextEvent = streamable.Events[z];
                                                if (nextEvent.FrameNumber >= visibleFrame)
                                                {
                                                    // Okay, we were still resident when the entity became visible.
                                                    break;
                                                }

                                                Streamable.Status status = nextEvent.GetStatusChange();

                                                if (status == Streamable.Status.UNREGISTERED || status == Streamable.Status.NOTLOADED)
                                                {
                                                    // Sigh, we really got unloaded.
                                                    visibleFrame = -1;
                                                    break;
                                                }
                                            }
                                        }

                                        // While we're here - what was the context for the request?
                                        String context = "";
                                        for (int z = y - 1; z >= 0; z--)
                                        {
                                            RscEvent prevEvent = streamable.Events[z];

                                            if (prevEvent.Action == RscEvent.ActionType.REQUEST)
                                            {
                                                context = prevEvent.Context;
                                                break;
                                            }
                                        }

                                        unused.LodType = Scene.GetLodType(streamable.IdentityHistory.GetStateAt(lastLoad).LodTypeId);

                                        unused.Context = context;
                                        unused.UsedFrameNumber = visibleFrame;
                                        UnusedArchetypes.Add(unused);
                                    }
                                }
                            }
                            else if (rscEvent.Action == RscEvent.ActionType.UNREQUEST && lastState == Streamable.Status.LOADED)
                            {
                                lastUnloadContext = rscEvent.Context;
                            }
                            else if (newState == Streamable.Status.NOTLOADED && (lastState == Streamable.Status.LOADED || lastState == Streamable.Status.LOADING))
                            {
                                lastUnload = rscEvent.FrameNumber;

                                // Did we just churn?
                                if (lastLoad != -1)
                                {
                                    int timeFrame = rscEvent.FrameNumber - lastLoad;

                                    if (timeFrame < 100)
                                    {
                                        // That's worth mentioning.
                                        Churn churn = new Churn();
                                        churn.Streamable = streamable;
                                        churn.LoadFrame = lastLoad;
                                        churn.UnloadFrame = rscEvent.FrameNumber;
                                        churn.LoadContext = "";
                                        churn.UnloadContext = "";
                                        // Get the context for both.
                                        for (int z = y - 1; z >= 0; z--)
                                        {
                                            RscEvent prevEvent = streamable.Events[z];

                                            if (prevEvent.Action == RscEvent.ActionType.REQUEST)
                                            {
                                                churn.LoadContext = prevEvent.Context;
                                                break;
                                            }

                                            if (prevEvent.Action == RscEvent.ActionType.UNREQUEST)
                                            {
                                                churn.UnloadContext = prevEvent.Context;
                                            }
                                        }

                                        int duration = rscEvent.FrameNumber - lastLoad;
                                        ChurnFrameCount[Math.Min(duration, ChurnFrameCount.Length)]++;
                                        int frameRangeIndex = GetFrameRangeIndex(ChurnFrameRanges, duration);
                                        int lodTypeId = streamable.IdentityHistory.GetStateAt(lastLoad).LodTypeId;

                                        if (lodTypeId != -1)
                                        {
                                            ChurnByLod[lodTypeId, frameRangeIndex]++;
                                        }
                                        Churns.Add(churn);
                                    }

                                    lastLoad = -1;
                                }
                            }

                            if (newState != Streamable.Status.INVALID)
                            {
                                lastState = newState;
                            }
                        }
                    }
/*
                    // Get the first applicable state.
                    int stateIndex = streamable.History.GetStateIndexAt(FirstFrame);

                    if (stateIndex == -1)
                    {
                        continue;
                    }
                    */
/*                    int sumReqToLoad = 0;
                    int reqCount = 0;*/

                    // Find the first state where we're load-requesting.
                    //int requestFrame = -1;
                    /*
                    if (streamable.History.GetStateByIndex(stateIndex).Status == Streamable.Status.LOADREQUESTED)
                    {
                        while (stateIndex > 0)
                        {
                            if (streamable.History.GetStateByIndex(stateIndex - 1).Status == Streamable.Status.LOADREQUESTED)
                            {
                                stateIndex--;
                            }
                            else
                            {
                                break;
                            }
                        }

                    //    requestFrame = streamable.History.GetStateByIndex(stateIndex).Frame;
                    }

                    int stateCount = streamable.History.StateCount;

                    while (stateIndex < stateCount)
                    {
                        if (streamable.History.GetStateByIndex(stateIndex).Status == Streamable.Status.LOADREQUESTED)
                        {
                            // Found a load request. Is it still in the range?
                            int requestFrame = streamable.History.GetStateByIndex(stateIndex).Frame;
                            if (requestFrame < LastFrame)
                            {
                                while (++stateIndex < stateCount)
                                {
                                    Streamable.Status state = streamable.History.GetStateByIndex(stateIndex).Status;
                                    if (state == Streamable.Status.LOADED)
                                    {
                                        int loadedFrame = streamable.History.GetStateByIndex(stateIndex).Frame;

                                        if (requestFrame != -1)
                                        {
                                            sumReqToLoad += loadedFrame - requestFrame;
                                            Debug.Assert(loadedFrame - requestFrame < 1000);
                                            reqCount++;
                                        }
                                        break;
                                    }

                                    if (state == Streamable.Status.NOTLOADED)
                                    {
                                        // Well, we gave up on it.
                                        break;
                                    }
                                }
                            }
                        }

                        stateIndex++;
                    }*/
                }
            }

            if (reqCount > 0)
            {
                AvgReqToLoaded[0] = (float) sumReqToLoad / (float) reqCount;
            }
            else
            {
                AvgReqToLoaded[0] = 0.0f;
            }

            Array.Clear(BadLodFrameCount, 0, BadLodFrameCount.Length);
            Array.Clear(BadLodByLod, 0, BadLodByLod.Length);

            Module mapDataModule = Scene.GetModule(Scene.MapDataModuleIndex);

            Console.WriteLine("Creating MapData dependency table...");

            // Let's go through the frame map and look at our findings.
            foreach (BadLod badLod in BadLods)
            {
                Streamable streamable = badLod.Streamable;

                // First of all - when did the archetype become resident?
                int archResidentFrame = streamable.GetFrameBecameOrWillBeResident(badLod.FirstFrame);

                if (archResidentFrame == -1)
                {
                    archResidentFrame = badLod.FirstFrame;
                }

                // What was the last mapdata to become resident, and when?
                Streamable.Identity strIdentity = streamable.IdentityHistory.GetStateAt(badLod.FirstFrame);
                Streamable worstMapData = null;
                int worstMapDataFrame = 0;

                foreach (Streamable dep in strIdentity.Dependencies)
                {
                    if (dep.Module == mapDataModule)
                    {
                        // When did this guy become resident?
                        int mapDataResidentFrame = dep.GetFrameBecameOrWasResident(archResidentFrame);

                        if (mapDataResidentFrame > worstMapDataFrame || mapDataResidentFrame == -1)
                        {
                            worstMapDataFrame = mapDataResidentFrame;
                            worstMapData = dep;
                        } 
                    }
                }

                badLod.LastMapDataFrameNumber = worstMapDataFrame;
                badLod.LastResidentMapData = worstMapData;

                badLod.FirstRequest = streamable.GetFirstRequestBefore(badLod.FirstFrame);

                // How many frames was it bad for?
                int badFrameCount = badLod.LastFrame - badLod.FirstFrame;
                badFrameCount = Math.Min(badFrameCount, BadLodFrameCount.Length - 1);
                BadLodFrameCount[badFrameCount]++;

                int lodTypeId = streamable.IdentityHistory.GetStateAt(badLod.FirstFrame).LodTypeId;

                if (lodTypeId != -1)
                {
                    int frameRangeIndex = GetFrameRangeIndex(ChurnFrameRanges, badFrameCount);
                    BadLodByLod[lodTypeId, frameRangeIndex]++;
                }
            }

            Console.WriteLine("Done.");
        }

        private void CreateLodHeaderList(String prefix, List<String> result)
        {
            foreach (LodType lodType in Scene.LodTypes.Values)
            {
                result.Add(String.Format("{0} - {1}", prefix, lodType));
            }
        }

        private void CreateLodResultList(List<String> result, int[] data)
        {
            foreach (LodType lodType in Scene.LodTypes.Values)
            {
                result.Add(String.Format("{0}", data[lodType.LodTypeId]));
            }
        }

        private void CreateModuleHeaderList(String prefix, List<String> result)
        {
            for (int x = 0; x < Scene.maxModuleIndex; x++)
            {
                Module module = Scene.GetModule(x);

                if (module != null)
                {
                    result.Add(String.Format("{0} - {1}", prefix, module.Name));
                }
            }
        }

        private void CreateModuleResultList(List<String> result, int[] data)
        {
            for (int x = 0; x < Scene.maxModuleIndex; x++)
            {
                Module module = Scene.GetModule(x);

                if (module != null)
                {
                    result.Add(String.Format("{0}", data[module.Index]));
                }
            }
        }

        private void CreateModuleAverageResultList(List<String> result, int[] data, float frameCountf)
        {
            for (int x = 0; x < Scene.maxModuleIndex; x++)
            {
                Module module = Scene.GetModule(x);

                if (module != null)
                {
                    result.Add(String.Format("{0:F2}", (float) data[module.Index] / frameCountf));
                }
            }
        }

        private void CreateFrameRangeLodHeaderList(String prefix, FrameRange[] frameRanges, List<String> result)
        {
            foreach (LodType lodType in Scene.LodTypes.Values)
            {
                int count = frameRanges.Length;
                for (int x=0; x<count-1; x++)
                {
                    result.Add(String.Format("{0} - {1} {2} (<{3} frames)",
                        prefix, lodType, frameRanges[x].Name, frameRanges[x].FrameCount));
                }

                result.Add(String.Format("{0} - {1} {2} ({3}+ frames)",
                    prefix, lodType, frameRanges[count-1].Name, frameRanges[count-1].FrameCount));
            }
        }

        private void CreateFrameRangeLodResultList(FrameRange[] frameRanges, List<String> result, int[,] data)
        {
            foreach (LodType lodType in Scene.LodTypes.Values)
            {
                int count = frameRanges.Length;
                for (int x = 0; x < count; x++)
                {
                    result.Add(String.Format("{0}", data[lodType.LodTypeId, x]));
                }
            }
        }

        /** Creates a single column that can be pasted into an Excel sheet.
         */
        public String[] CreateCsvColumn()
        {
            List<String> Result = new List<String>();

            Result.Add(TestName);
            Result.Add(String.Format("{0} - {1}", FirstFrame, LastFrame));

            float FrameCountf = (float)LastFrame - (float)FirstFrame + 1.0f;
            float TimeDeltaf = WallclockEnd - WallclockStart;

            Result.Add(String.Format("{0:F2}", TimeDeltaf));

            Result.Add(String.Format("{0}", MissingLod));
            Result.Add(String.Format("{0:F}", (float)MissingLod / FrameCountf));

            CreateLodResultList(Result, MissingLodByLod);

            Result.Add(String.Format("{0}", RequestCount));
            Result.Add(String.Format("{0:F2}", (float)RequestCount / FrameCountf));

            Result.Add(String.Format("{0:F}", AvgReqToLoaded[0]));

            // Per-device stats
            for (int x = 0; x <= Scene.maxDeviceIndex; x++)
            {
                if (x == 0 || Scene.GetDevice(x - 1) != null)
                {
                    Result.Add(String.Format("{0:F}", IdleTime[x]));

                    for (int y = 0; y < Scene.LodTypes.Count; y++)
                    {
                        Result.Add(String.Format("{0:F}", StreamTimePerLod[y, x]));
                    }

                    for (int y = 0; y < Scene.maxModuleIndex; y++)
                    {
                        Result.Add(String.Format("{0:F}", StreamTimePerModule[y, x]));
                    }
                }
            }

            Result.Add(String.Format("{0}", Churns.Count));
            Result.Add(String.Format("{0}", (float)Churns.Count / FrameCountf));

            Result.Add(String.Format("{0}", DoubleLoads.Count));
            Result.Add(String.Format("{0}", (float)DoubleLoads.Count / FrameCountf));

            Result.Add(String.Format("{0}", UnusedArchetypes.Count));
            Result.Add(String.Format("{0:F}", (float)UnusedArchetypes.Count / FrameCountf));

            CreateModuleResultList(Result, RequestByModule);
            CreateModuleAverageResultList(Result, RequestByModule, FrameCountf);
            
            Result.Add(String.Format("{0}", DoomedRequestCount));
            Result.Add(String.Format("{0:F2}", (float)DoomedRequestCount / FrameCountf));

            Result.Add(String.Format("{0:F}", DeletedScores.Average));
            Result.Add(String.Format("{0:F}", DeletedScores.Peak));
            Result.Add(String.Format("{0}", (int)DeletedScores.Count));

            Result.Add(String.Format("{0}", GarbageCollected));


    //        Result.Add(String.Format("{0}", LowLod));
      //      Result.Add(String.Format("{0:F2}", (float)LowLod / FrameCountf));

        //    Result.Add(String.Format("{0}", MissingLod));
          //  Result.Add(String.Format("{0:F2}", (float)MissingLod / FrameCountf));

  //          CreateLodResultList(Result, LowLodByLod);

            CreateFrameRangeLodResultList(ChurnFrameRanges, Result, ChurnByLod);
            CreateFrameRangeLodResultList(DoubleLoadFrameRanges, Result, DoubleLoadByLod);
            CreateFrameRangeLodResultList(BadLodFrameRanges, Result, BadLodByLod);
/*
            for (int x = 0; x < BadLodFrameCount.Length; x++)
            {
                Result.Add(String.Format("{0}", BadLodFrameCount[x]));
            }

            for (int x = 0; x < ChurnFrameCount.Length; x++)
            {
                Result.Add(String.Format("{0}", ChurnFrameCount[x]));
            }
*/

            return Result.ToArray();
        }

        /** Create a single column with all the headers. This should be the first column of the final
         *  Excel sheet.
         */
        public String[] CreateHeaderColumn()
        {
            List<String> Result = new List<String>();

            Result.Add("Test");
            Result.Add("Frame Range");

            Result.Add("Seconds");

            Result.Add("Total Missing Entities");
            Result.Add("Missing Entities (Avg per frame)");

            CreateLodHeaderList("Missing Entities", Result);

            Result.Add("Total Requests");
            Result.Add("Requests (Avg per frame)");

            Result.Add("Avg req-to-load");

            for (int x = 0; x <= Scene.maxDeviceIndex; x++)
            {
                if (x == 0 || Scene.GetDevice(x - 1) != null)
                {
                    String deviceName = (x == 0) ? "All devices" : Scene.GetDevice(x-1).Name;
                    Result.Add(String.Format("{0} Idle Time", deviceName));

                    for (int y = 0; y < Scene.LodTypes.Count; y++)
                    {
                        Result.Add(String.Format("{0} {1} Stream Time", deviceName, Scene.LodTypes[y].Name));
                    }

                    for (int y = 0; y < Scene.maxModuleIndex; y++)
                    {
                        Result.Add(String.Format("{0} {1} Stream Time", deviceName, Scene.GetModule(y).Name));
                    }
                }
            }

            Result.Add("Total Churns");
            Result.Add("Churns (Avg per frame)");

            Result.Add("Total Double Loads");
            Result.Add("Double Loads (Avg per frame)");

            Result.Add("Total Unused Archetypes");
            Result.Add("Unused Archetypes (Avg per frame)");

            CreateModuleHeaderList("Total Requests", Result);
            CreateModuleHeaderList("Total Requests (Avg/frame)", Result);

            Result.Add("Total Doomed Requests");
            Result.Add("Doomed Requests (Avg per frame)");

            Result.Add("Average Deleted Scores");
            Result.Add("Peak Deleted Scores");
            Result.Add("Deleted Objects With Score");

            Result.Add("Garbage Collected");

/*            Result.Add("Total Low LOD Entities");
            Result.Add("Low LOD (Avg per frame)");

            Result.Add("Total Missing LOD Entities");
            Result.Add("Missing LOD (Avg per frame)");*/

//            CreateLodHeaderList("Low LOD", Result);

            CreateFrameRangeLodHeaderList("Churn", ChurnFrameRanges, Result);
            CreateFrameRangeLodHeaderList("Double Load", DoubleLoadFrameRanges, Result);
            CreateFrameRangeLodHeaderList("Missing Entities", BadLodFrameRanges, Result);

            /*
            for (int x = 0; x < BadLodFrameCount.Length; x++)
            {
                Result.Add(String.Format("Bad LOD {0}", x));
            }

            for (int x = 0; x < ChurnFrameCount.Length; x++)
            {
                Result.Add(String.Format("Churn {0}", x));
            }
            */
            return Result.ToArray();
        }
    }
}
