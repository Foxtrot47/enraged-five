﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StreamingVisualize.Model
{
    public class LodType : IVersionedSerializable
    {
        public String Name { get; set; }
        public bool Visible { get; set; }

        public int LodTypeId { get; set; }
/*
        public Pen UnloadedPen { get; set; }
        public Pen LoadedPen { get; set; }

        public void SetColor(int color)
        {
            // Get the color at half brightness.
            int halfColor = (color >> 1) & 0x7f7f7f;

            UnloadedPen = new Pen(Color.FromArgb((int)(0xff000000 | (uint)halfColor)));
            LoadedPen = new Pen(Color.FromArgb((int)(0xff000000 | (uint)color)));
        }*/

        public void Serialize(VersionedWriter writer)
        {
            writer.Write(Name);
            writer.Write(Visible);
//            writer.Write(UnloadedPen.Color.ToArgb());
//            writer.Write(LoadedPen.Color.ToArgb());
        }

        public void Deserialize(VersionedReader reader, int version)
        {
            Name = reader.ReadString();
            Visible = reader.ReadBool();
//            UnloadedPen = new Pen(Color.FromArgb(reader.ReadInt()));
//            LoadedPen = new Pen(Color.FromArgb(reader.ReadInt()));
        }

        override public String ToString()
        {
            return Name;
        }
    }
}
