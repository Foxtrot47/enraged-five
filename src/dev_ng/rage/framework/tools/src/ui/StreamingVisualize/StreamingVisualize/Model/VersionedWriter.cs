﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace StreamingVisualize.Model
{
    public class VersionedWriter
    {
        public const int MAGIC_HEADER = 0x5354565a;
        public const int MAGIC_CHECKPOINT = 0xa110ca1;
        public const int FILE_VERSION = 42;


        private BinaryWriter Writer;
        public long BytesWritten;
        public Scene scene;


        public VersionedWriter(BinaryWriter writer)
        {
            this.Writer = writer;
            BytesWritten = 0;
        }

        public VersionedWriter(Stream stream)
        {
            Writer = new BinaryWriter(stream);
            BytesWritten++;
        }

        public void SetStream(Stream stream)
        {
            if (Writer != null)
            {
                Writer.Flush();
            }

            Writer = new BinaryWriter(stream);
            BytesWritten++;
        }

        public void Close()
        {
            Writer.Close();
        }

        public void Write(int value)
        {
            Writer.Write(value);
            BytesWritten += sizeof(int);
        }

        public void Write(byte[] bytes)
        {
            Writer.Write(bytes);

            BytesWritten += bytes.Length;
        }

        public void Write(uint value)
        {
            Writer.Write(value);
            BytesWritten += sizeof(uint);
        }

        public void Write(float value)
        {
            Writer.Write(value);
            BytesWritten += sizeof(float);
        }

        public void Write(short value)
        {
            Writer.Write(value);
            BytesWritten += sizeof(short);
        }

        public void Write(byte value)
        {
            Writer.Write(value);
            BytesWritten += sizeof(byte);
        }

        public void Write(bool value)
        {
            Writer.Write((byte)((value) ? 1 : 0));
            BytesWritten += sizeof(byte);
        }

        public void WriteStringRef(String text)
        {
            Write(scene.StringPool.GetStringId(text));
        }

        public void Write(IVersionedSerializable value)
        {
            value.Serialize(this);
        }

        public void Write<T>(IList<T> value) where T : IVersionedSerializable
        {
            Writer.Write(value.Count);

            for (int x = 0; x < value.Count; x++)
            {
                value[x].Serialize(this);
            }
        }

        public void Write<T>(Dictionary<int, T> value) where T : IVersionedSerializable
        {
            Writer.Write(value.Count);

            foreach (KeyValuePair<int, T> kvp in value)
            {
                Writer.Write(kvp.Key);
                Write(kvp.Value);
                BytesWritten += sizeof(int);
            }
        }

        public void Write<T>(PerFrameState<T> state) where T : class, IFrameState, ICloneable, IComparable<T>, IVersionedSerializable, new()
        {
            state.Serialize(this);
        }

        public void WriteValuesOnly<U, T>(Dictionary<U, T> value) where T : IVersionedSerializable
        {
            Writer.Write(value.Count);
            BytesWritten += sizeof(int);

            foreach (KeyValuePair<U, T> kvp in value)
            {
                Write(kvp.Value);
            }
        }

        public void WriteRef(IReferenced value)
        {
            Writer.Write(value.GetIndex());
            BytesWritten += sizeof(int);
        }

        public void WriteSparseArray(IVersionedSerializable[] value)
        {
            // Write capacity...
            Writer.Write(value.Length);
            BytesWritten += sizeof(int);

            int inUse = 0;
            for (int x = 0; x < value.Length; x++)
            {
                if (value[x] != null)
                {
                    inUse++;
                }
            }

            // ...then size.
            Writer.Write(inUse);
            BytesWritten += sizeof(uint);

            for (int x = 0; x < value.Length; x++)
            {
                if (value[x] != null)
                {
                    Writer.Write(x);
                    BytesWritten += sizeof(int);
                    value[x].Serialize(this);
                }
            }
        }

        public void WriteStreamableRef(Streamable streamable)
        {
            if (streamable == null)
            {
                Writer.Write(-1);
            }
            else
            {
                Writer.Write(streamable.Index);
            }
            BytesWritten += sizeof(int);
        }

        public void WriteEntityRef(Entity entity)
        {
            if (entity == null)
            {
                Writer.Write(-1);
            }
            else
            {
                Writer.Write(entity.EntityId);
            }
            BytesWritten += sizeof(int);
        }

        public void WriteModuleRef(Module module)
        {
            if (module == null)
            {
                Writer.Write((short)-1);
            }
            else
            {
                Writer.Write((short)module.Index);
            }
            BytesWritten += sizeof(short);
        }

        public void Write(String value)
        {
            Writer.Write(value);

            // Eh, what's the size of that? 
            BytesWritten += sizeof(int);
            BytesWritten += sizeof(short) * sizeof(char);
        }

        public void Write(Vector value)
        {
            Write(value.X);
            Write(value.Y);
            Write(value.Z);

            BytesWritten += sizeof(float) * 3;
        }

        public void Write(Vector16 value)
        {
            Write(value.iX);
            Write(value.iY);
            Write(value.iZ);

            BytesWritten += sizeof(short) * 3;
        }

        public void Write(Box value)
        {
            Write(value.BoxMin);
            Write(value.BoxMax);
        }

        public void Write(Box16 value)
        {
            Write(value.BoxMin);
            Write(value.BoxMax);
        }

        public void WriteHeader()
        {
            Writer.Write(MAGIC_HEADER);
            Writer.Write(FILE_VERSION);

            BytesWritten += sizeof(int) * 2;
        }

        public void WriteCheckpoint()
        {
            Writer.Write(MAGIC_CHECKPOINT);

            BytesWritten += sizeof(int);
        }
    }
}
