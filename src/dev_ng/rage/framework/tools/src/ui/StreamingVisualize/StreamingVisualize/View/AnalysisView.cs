﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StreamingVisualize.Controller;
using StreamingVisualize.Model;
using StreamingVisualize.View.Components;

namespace StreamingVisualize.View
{
    public partial class AnalysisView : UserControl, IViewController
    {
        private MainController Controller;
        private Scene Scene;
        private int FirstFrame, LastFrame;

        private ViewContextMenu viewContextMenu = new ViewContextMenu();
        private IList<Analysis.Churn> ActiveChurnList;

        private Analysis Analysis = new Analysis();

        private bool ChangingSelection = false;

        private int StreamableColumn;
        private int MapDataColumn;

        private int ChurnLoadFrameColumn;
        private int ChurnUnloadFrameColumn;
        private int ChurnStreamableColumn;

        private int UnusedStreamedFrameColumn;
        private int UnusedUsedFrameColumn;
        private int UnusedStreamableColumn;


        private interface SubView
        {
            void MakeVisible(AnalysisView view);
        }

        private class ChurnListView : SubView
        {
            public override string ToString()
            {
                return "Churn List";
            }

            public void MakeVisible(AnalysisView view)
            {
                view.ActiveChurnList = view.Analysis.Churns;
                view.ChurnList.DataSource = view.Analysis.Churns;

                view.ChurnList.Show();
                view.BadLodList.Hide();
                view.UnusedArchetypeList.Hide();
                view.MainInfo.Hide();
            }
        }

        private class DoubleLoadListView : SubView
        {
            public override string ToString()
            {
                return "Double Load List";
            }

            public void MakeVisible(AnalysisView view)
            {
                view.ActiveChurnList = view.Analysis.DoubleLoads;
                view.ChurnList.DataSource = view.Analysis.DoubleLoads;

                view.ChurnList.Show();
                view.BadLodList.Hide();
                view.UnusedArchetypeList.Hide();
                view.MainInfo.Hide();
            }
        }

        private class BadLodView : SubView
        {
            public override string ToString()
            {
                return "Bad LODs";
            }

            public void MakeVisible(AnalysisView view)
            {
                view.ChurnList.Hide();
                view.BadLodList.Show();
                view.UnusedArchetypeList.Hide();
                view.MainInfo.Hide();
            }
        }

        private class UnusedView : SubView
        {
            public override string ToString()
            {
                return "Unused Archetypes";
            }

            public void MakeVisible(AnalysisView view)
            {
                view.ChurnList.Hide();
                view.BadLodList.Hide();
                view.UnusedArchetypeList.Show();
                view.MainInfo.Hide();
            }
        }

        private class MainInfoView : SubView
        {
            public override string ToString()
            {
                return "Main Info";
            }

            public void MakeVisible(AnalysisView view)
            {
                view.ChurnList.Hide();
                view.BadLodList.Hide();
                view.UnusedArchetypeList.Hide();
                view.MainInfo.Show();
            }
        }



        public AnalysisView()
        {
            InitializeComponent();
        }


        public void OnFilterChanged()
        {

        }


        // Switch the display to focus on a certain frame
        public void SetToFrame(int frameNumber)
        {
        }

        // A new device was registered, or a device has changed.
        public void OnDeviceChanged(int deviceIndex, Device device) { }

        // A streamable has been selected
        public void OnSelectStreamable(Streamable streamable)
        {
        }

        // The client information has been updated
        public void ClientInfoUpdated() { }

        // We're in follow mode, and a new frame has been added.
        public void OnNewFrame(int frameNumber) { }

        // A new entity type was registered
        public void OnNewEntityType(int entityTypeId, EntityType type) { }

        // The connection has been terminated
        public void OnConnectionLost() { }

        // Register this view with the system
        public void Register(MainController controller, Scene scene)
        {
            this.Controller = controller;
            this.Scene = scene;

            FirstFrame = scene.SelectedFirstFrame;
            LastFrame = scene.SelectedLastFrame;

            RefreshData();
        }

        public void RefreshData()
        {
            Analysis.FirstFrame = FirstFrame;
            Analysis.LastFrame = LastFrame;
            Analysis.Scene = Scene;

            Analysis.Analyze();

            // Create the narrative.
            /*StringBuilder mainText = new StringBuilder(2048);

            float FrameCountf = (float)LastFrame - (float)FirstFrame + 1.0f;

            mainText.AppendFormat("Requests: {0} ({1:F2} per frame)\n", Analysis.RequestCount, (float)Analysis.RequestCount / FrameCountf);
            mainText.AppendFormat("Doomed Requests: {0} ({1:F2} per frame)\n", Analysis.DoomedRequestCount, (float)Analysis.DoomedRequestCount / FrameCountf);

            mainText.AppendFormat("Low LOD entities: {0} ({1:F2} per frame)\n", Analysis.LowLod, (float)Analysis.LowLod / FrameCountf);
            mainText.AppendFormat("Missing LOD entities: {0} ({1:F2} per frame)\n", Analysis.MissingLod, (float)Analysis.MissingLod / FrameCountf);

            for (int x = 0; x < Analysis.BadLodFrameCount.Length; x++)
            {
                if (Analysis.BadLodFrameCount[x] > 0)
                {
                    mainText.AppendFormat("Bad Lod for {0} frames: {1}\n", x, Analysis.BadLodFrameCount[x]);
                }
            }

            mainText.AppendFormat("Avg req-to-load: {0:F} frames\n", Analysis.AvgReqToLoaded[0]);*/

            StringBuilder mainText = new StringBuilder(126384);

            String[] headers = Analysis.CreateHeaderColumn();
            String[] data = Analysis.CreateCsvColumn();

            for (int x = 0; x < headers.Length; x++)
            {
                mainText.Append(headers[x]);
                mainText.Append(": ");
                mainText.Append(data[x]);
                mainText.Append('\n');
            }

            MainInfo.Text = mainText.ToString();
            Clipboard.SetText(MainInfo.Text);

            ChangingSelection = true;
            BadLodList.DataSource = Analysis.BadLods;
            ChurnList.DataSource = Analysis.Churns;
            UnusedArchetypeList.DataSource = Analysis.UnusedArchetypes;
            ChangingSelection = false;

            Refresh();
        }

        public void OnFrameRangeChanged(int firstFrame, int lastFrame)
        {
            FirstFrame = firstFrame;
            LastFrame = lastFrame;

            RefreshData();
        }

        private void PopulateContextMenu(ContextMenu menu)
        {
        }

        private void RecreateContextMenu()
        {
            ContextMenu menu = viewContextMenu.CreateContextMenu(Controller, this);
            PopulateContextMenu(menu);
            this.ContextMenu = menu;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
/*            if (ChurnReport.Columns[e.ColumnIndex].Name == "Frames" && e.RowIndex < Analysis.Churns.Count)
            {
                StringBuilder result = new StringBuilder(256);

                Analysis.Churn churn = Analysis.Churns[e.RowIndex];

                for (int x = 0; x < churn.Frames.Length; x++)
                {
                    result.AppendFormat("{0} ", churn.Frames[x]);
                }

                e.Value = result.ToString();
            }*/
        }

        private void BadLodList_SelectionChanged(object sender, EventArgs e)
        {
            if (!ChangingSelection && Analysis.BadLods != null)
            {
                if (BadLodList.SelectedCells.Count > 0)
                {
                    int row = BadLodList.SelectedCells[0].RowIndex;
                    int col = BadLodList.SelectedCells[0].ColumnIndex;

                    if (row >= 0 && row < Analysis.BadLods.Count)
                    {
                        Analysis.BadLod badLod = Analysis.BadLods[row];

                        String column = BadLodList.Columns[col].Name;

                        if (column == "FirstFrameCol")
                        {
                            Controller.SetToFrame(badLod.FirstFrame, true);
                        }
                        else if (column == "LastFrameCol")
                        {
                            Controller.SetToFrame(badLod.LastFrame, true);
                        }
                        else if (column == "FirstRequestCol")
                        {
                            if (badLod.FirstRequest >= 0)
                            {
                                Controller.SetToFrame(badLod.FirstRequest, true);
                            }
                        }
                        else if (column == "StreamableCol")
                        {
                            Controller.OnSelectStreamable(badLod.Streamable, true);
                        }
                        else if (column == "LastResidentMapDataCol")
                        {
                            if (badLod.LastResidentMapData != null)
                            {
                                Controller.OnSelectStreamable(badLod.LastResidentMapData, true);
                            }
                        }
                        else if (column == "LastMapDataFrameNumberCol")
                        {
                            if (badLod.LastMapDataFrameNumber >= 0)
                            {
                                Controller.SetToFrame(badLod.LastMapDataFrameNumber, true);
                            }
                        }
                    }
                }
            }
        }

        private void BadLodList_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {

        }

        private void BadLodList_CellValueNeeded(object sender, DataGridViewCellValueEventArgs e)
        {
            // We need to get the name at the time, not "CurrentName".
            if (e.ColumnIndex == StreamableColumn || e.ColumnIndex == MapDataColumn)
            {
                int row = e.RowIndex;

                if (Analysis.BadLods != null && row >= 0 && row < Analysis.BadLods.Count)
                {
                    Analysis.BadLod badLod = Analysis.BadLods[row];
                    int frameNumber = badLod.FirstFrame;

                    if (e.ColumnIndex == StreamableColumn)
                    {
                        e.Value = badLod.Streamable.GetNameAt(frameNumber);
                    }
                    else
                    {
                        // This is technically not necessary, we don't have streamable mapdata.
                        if (badLod.LastResidentMapData != null)
                        {
                            e.Value = badLod.LastResidentMapData.GetNameAt(frameNumber);
                        }
                    }
                }
            }
        }

        private void AnalysisView_Load(object sender, EventArgs e)
        {
            StreamableColumn = BadLodList.Columns["StreamableCol"].Index;
            MapDataColumn = BadLodList.Columns["LastResidentMapDataCol"].Index;

            ChurnLoadFrameColumn = ChurnList.Columns["ChurnLoadFrame"].Index;
            ChurnUnloadFrameColumn = ChurnList.Columns["ChurnUnloadFrame"].Index;
            ChurnStreamableColumn = ChurnList.Columns["ChurnStreamable"].Index;

            UnusedStreamedFrameColumn = UnusedArchetypeList.Columns["UnusedStreamedFrameNumber"].Index;
            UnusedUsedFrameColumn = UnusedArchetypeList.Columns["UnusedUsedFrameNumber"].Index;
            UnusedStreamableColumn = UnusedArchetypeList.Columns["UnusedStreamable"].Index;

            RecreateContextMenu();

            InfoSelector.Items.Add(new BadLodView());
            InfoSelector.Items.Add(new ChurnListView());
            InfoSelector.Items.Add(new DoubleLoadListView());
            InfoSelector.Items.Add(new UnusedView());
            InfoSelector.Items.Add(new MainInfoView());
        }

        private void InfoSelector_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (InfoSelector.SelectedIndex >= 0)
            {
                ((SubView)InfoSelector.Items[InfoSelector.SelectedIndex]).MakeVisible(this);
            }

        }

        private void Copy_Click(object sender, EventArgs e)
        {

        }

        private void ChurnList_CellValueNeeded(object sender, DataGridViewCellValueEventArgs e)
        {
            // We need to get the name at the time, not "CurrentName".
            if (e.ColumnIndex == ChurnStreamableColumn)
            {
                int row = e.RowIndex;

                if (ActiveChurnList != null && row >= 0 && row < ActiveChurnList.Count)
                {
                    Analysis.Churn churn = ActiveChurnList[row];
                    int frameNumber = churn.LoadFrame;

                    e.Value = churn.Streamable.GetNameAt(frameNumber);
                }
            }
        }

        private void ChurnList_SelectionChanged(object sender, EventArgs e)
        {
            if (!ChangingSelection && ActiveChurnList != null)
            {
                if (ChurnList.SelectedCells.Count > 0)
                {
                    int row = ChurnList.SelectedCells[0].RowIndex;
                    int col = ChurnList.SelectedCells[0].ColumnIndex;

                    if (row >= 0 && row < ActiveChurnList.Count)
                    {
                        Analysis.Churn churn = ActiveChurnList[row];

                        if (col == ChurnLoadFrameColumn)
                        {
                            Controller.SetToFrame(churn.LoadFrame, true);
                        }
                        else if (col == ChurnUnloadFrameColumn)
                        {
                            Controller.SetToFrame(churn.UnloadFrame, true);
                        }
                        else if (col == ChurnStreamableColumn)
                        {
                            Controller.OnSelectStreamable(churn.Streamable, true);
                        }
                    }
                }
            }
        }

        private void UnusedArchetypeList_SelectionChanged(object sender, EventArgs e)
        {
            if (!ChangingSelection && Analysis.UnusedArchetypes != null)
            {
                if (UnusedArchetypeList.SelectedCells.Count > 0)
                {
                    int row = UnusedArchetypeList.SelectedCells[0].RowIndex;
                    int col = UnusedArchetypeList.SelectedCells[0].ColumnIndex;

                    if (row >= 0 && row < Analysis.UnusedArchetypes.Count)
                    {
                        Analysis.UnusedArchetype UnusedArchetype = Analysis.UnusedArchetypes[row];

                        if (col == UnusedStreamedFrameColumn)
                        {
                            Controller.SetToFrame(UnusedArchetype.StreamedFrameNumber, true);
                        }
                        else if (col == UnusedUsedFrameColumn)
                        {
                            Controller.SetToFrame(UnusedArchetype.UsedFrameNumber, true);
                        }
                        else if (col == UnusedStreamableColumn)
                        {
                            Controller.OnSelectStreamable(UnusedArchetype.Streamable, true);
                        }
                    }
                }
            }
        }

        private void UnusedArchetypeList_CellValueNeeded(object sender, DataGridViewCellValueEventArgs e)
        {
            // We need to get the name at the time, not "CurrentName".
            if (e.ColumnIndex == UnusedStreamableColumn)
            {
                int row = e.RowIndex;

                if (Analysis.UnusedArchetypes != null && row >= 0 && row < Analysis.UnusedArchetypes.Count)
                {
                    Analysis.UnusedArchetype unused = Analysis.UnusedArchetypes[row];
                    int frameNumber = unused.StreamedFrameNumber;

                    e.Value = unused.Streamable.GetNameAt(frameNumber);
                }
            }
        }

        public void OnSelectEntity(Entity entity) { }
    }
}
