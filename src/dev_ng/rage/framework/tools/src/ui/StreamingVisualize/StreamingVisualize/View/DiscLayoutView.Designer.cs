﻿namespace StreamingVisualize.View
{
    partial class DiscLayoutView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Scroller = new System.Windows.Forms.HScrollBar();
            this.SuspendLayout();
            // 
            // Scroller
            // 
            this.Scroller.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Scroller.Location = new System.Drawing.Point(0, 220);
            this.Scroller.Name = "Scroller";
            this.Scroller.Size = new System.Drawing.Size(384, 17);
            this.Scroller.TabIndex = 1;
            this.Scroller.Scroll += new System.Windows.Forms.ScrollEventHandler(this.Scroller_Scroll);
            // 
            // DiscLayoutView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Scroller);
            this.DoubleBuffered = true;
            this.Name = "DiscLayoutView";
            this.Size = new System.Drawing.Size(384, 237);
            this.Load += new System.EventHandler(this.DiscLayoutView_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.DiscLayoutView_Paint);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DiscLayoutView_KeyDown);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.DiscLayoutView_MouseDown);
            this.MouseHover += new System.EventHandler(this.DiscLayoutView_MouseHover);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.DiscLayoutView_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.DiscLayoutView_MouseUp);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.HScrollBar Scroller;
    }
}
