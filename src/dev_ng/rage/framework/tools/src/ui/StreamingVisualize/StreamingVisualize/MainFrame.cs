﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StreamingVisualize.Model;
using System.IO;

namespace StreamingVisualize
{
    public partial class MainFrame : Form
    {
        Listener listener;

        private delegate void OnConnectionHandler(Scene scene, Communication communication);

        public MainFrame()
        {
            InitializeComponent();

            listener = new Listener();
            listener.OnConnectionCb = new Listener.OnConnection(OnConnection);

            listener.Start();
        }

        // Called from the listener thread when a new connection starts.
        private void OnConnection(Scene scene, Communication communication)
        {
            Invoke(new OnConnectionHandler(NewConnectionHandler), new object[] { scene, communication });
        }

        // Called on UI thread when a new connection starts.
        private void NewConnectionHandler(Scene scene, Communication communication)
        {
            ClientWindow newClient = new ClientWindow(scene, communication);
            newClient.MdiParent = this;
            newClient.Dock = DockStyle.Fill;
            newClient.Show();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();

            //ofd.InitialDirectory = "c:\\";
            ofd.Filter = "StreamingVisualize (*.svz)|*.svz|All files (*.*)|*.*";
            ofd.FilterIndex = 0;
            ofd.RestoreDirectory = true;


            if (ofd.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    Stream stream = ofd.OpenFile();
                    Scene scene = new Scene();

                    VersionedReader reader = new VersionedReader(scene, stream);
                    reader.ReadHeader();
                    reader.Read(scene);
                    reader.Close();
                    stream.Close();

                    ClientWindow newClient = new ClientWindow(scene, null);
                    newClient.MdiParent = this;
                    newClient.Dock = DockStyle.Fill;
                    newClient.Show();

                    // When we're done, switch to the last frame to set everything in motion.
                    newClient.GoToLastFrame();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error loading file: " + ex.Message);
                }
            }
        }

        private void MainFrame_FormClosed(object sender, FormClosedEventArgs e)
        {
            Dispose();
        }
    }
}
