﻿namespace StreamingVisualize.View
{
    partial class ProgressView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MainAction = new System.Windows.Forms.Label();
            this.SubAction = new System.Windows.Forms.Label();
            this.Abort = new System.Windows.Forms.Button();
            this.Progress = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // MainAction
            // 
            this.MainAction.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MainAction.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainAction.Location = new System.Drawing.Point(1, 13);
            this.MainAction.Name = "MainAction";
            this.MainAction.Size = new System.Drawing.Size(337, 23);
            this.MainAction.TabIndex = 0;
            this.MainAction.Text = "label1";
            this.MainAction.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // SubAction
            // 
            this.SubAction.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SubAction.Location = new System.Drawing.Point(1, 36);
            this.SubAction.Name = "SubAction";
            this.SubAction.Size = new System.Drawing.Size(337, 18);
            this.SubAction.TabIndex = 1;
            this.SubAction.Text = "label1";
            this.SubAction.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Abort
            // 
            this.Abort.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Abort.Location = new System.Drawing.Point(120, 89);
            this.Abort.Name = "Abort";
            this.Abort.Size = new System.Drawing.Size(100, 25);
            this.Abort.TabIndex = 2;
            this.Abort.Text = "Abort";
            this.Abort.UseVisualStyleBackColor = true;
            this.Abort.Click += new System.EventHandler(this.Abort_Click);
            // 
            // Progress
            // 
            this.Progress.Location = new System.Drawing.Point(45, 57);
            this.Progress.Name = "Progress";
            this.Progress.Size = new System.Drawing.Size(248, 16);
            this.Progress.TabIndex = 3;
            this.Progress.Click += new System.EventHandler(this.progressBar1_Click);
            // 
            // ProgressView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.Abort;
            this.ClientSize = new System.Drawing.Size(340, 126);
            this.Controls.Add(this.Progress);
            this.Controls.Add(this.Abort);
            this.Controls.Add(this.SubAction);
            this.Controls.Add(this.MainAction);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ProgressView";
            this.Text = "ProgressView";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ProgressView_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label MainAction;
        private System.Windows.Forms.Label SubAction;
        private System.Windows.Forms.Button Abort;
        private System.Windows.Forms.ProgressBar Progress;
    }
}