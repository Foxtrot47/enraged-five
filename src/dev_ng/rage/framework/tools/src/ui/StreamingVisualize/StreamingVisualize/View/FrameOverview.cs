﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StreamingVisualize.View.Components;
using StreamingVisualize.Model;
using StreamingVisualize.Controller;
using System.Diagnostics;
using System.Drawing.Drawing2D;

namespace StreamingVisualize.View
{
    public partial class FrameOverview : UserControl, IViewController
    {
        /** PURPOSE: A BarGraph can get an integer value out of any frame (like the number of requests, number of
         *  unrequests, etc), which can then be displayed in the frame overview.
         */
        private interface BarGraph
        {
            String GetName();

            int GetValue(Scene scene, Frame frame, int frameNumber);
        }

        /** PURPOSE: A MarkerVisualizer can get a boolean value out of any frame (like "had priority requests",
         * "was out of memory", etc), which can then be displayed in the frame overview.
         */
        private interface MarkerVisualizer
        {
            String GetName();

            bool GetValue(Frame frame, int frameNumber);
        }


        private class RealRequestBarGraph : BarGraph
        {
            public String GetName() { return "Real Requests"; }

            public int GetValue(Scene scene, Frame frame, int frameNumber)
            {
                /*
                if (scene.Filter.IsActive())
                {
                    // Manually count requests.
                    int result = 0;

                    foreach (Request req in frame.requests)
                    {
                        if (!req.Streamable.Filtered)
                        {
                            result++;
                        }
                    }

                    return result;
                }
                */
                return frame.RealReqs;
            }
        }

        private class RequestBarGraph : BarGraph
        {
            public String GetName() { return "Requests"; }

            public int GetValue(Scene scene, Frame frame, int frameNumber)
            {
                /*
                if (scene.Filter.IsActive())
                {
                    // Manually count requests.
                    int result = 0;

                    foreach (Request req in frame.requests)
                    {
                        if (!req.Streamable.Filtered)
                        {
                            result++;
                        }
                    }

                    return result;
                }
                */
                return frame.Reqs;
            }
        }

        private class RequestThisFrameBarGraph : BarGraph
        {
            public String GetName() { return "Requests this frame"; }

            public int GetValue(Scene scene, Frame frame, int frameNumber)
            {
                if (scene.Filter.IsActive())
                {
                    // Manually count requests.
                    int result = 0;

                    foreach (Request req in frame.requests)
                    {
                        if (!req.Streamable.Filtered)
                        {
                            result++;
                        }
                    }

                    return result;
                }
                return frame.requests.Count;
            }
        }

        private class CulledMapsBarGraph : BarGraph
        {
            public String GetName() { return "Culled Maps / 10"; }

            public int GetValue(Scene scene, Frame frame, int frameNumber)
            {
                IList<Cullbox> cullboxes = scene.GetCullboxesAt(frame.StreamCamPos);
                Dictionary<Streamable, bool> culled = new Dictionary<Streamable, bool>();

                foreach (Cullbox cullbox in cullboxes)
                {
                    foreach (Streamable streamable in cullbox.Contents)
                    {
                        culled[streamable] = true;
                    }
                }

                int count = 0;

                if (!scene.Filter.IsActive())
                {
                    count = culled.Keys.Count;
                }
                else
                {
                    foreach (Streamable streamable in culled.Keys)
                    {
                        if (!scene.Filter.IsFilteredOut(streamable))
                        {
                            count++;
                        }
                    }
                }

                return (count + 9) / 10;
            }
        }


        private class CullboxesBarGraph : BarGraph
        {
            public String GetName() { return "Active Cullboxes"; }

            public int GetValue(Scene scene, Frame frame, int frameNumber)
            {
                IList<Cullbox> cullboxes = scene.GetCullboxesAt(frame.StreamCamPos);
                return cullboxes.Count;
            }
        }

        private class CameraSpeedBarGraph : BarGraph
        {
            public String GetName() { return "Camera Speed (m/s)"; }

            public int GetValue(Scene scene, Frame frame, int frameNumber)
            {
                if (frameNumber > 0)
                {
                    Frame prevFrame = scene.GetFrame(frameNumber - 1);
                    Vector prevPos = prevFrame.CamPos;

                    float dist = frame.CamPos.Dist(prevPos);
                    float timePassed = frame.Time - prevFrame.Time;

                    if (timePassed > 0)
                    {
                        return (int) (dist / timePassed);
                    }
                }

                return 0;
            }
        }


        private class UnrequestBarGraph : BarGraph
        {
            public String GetName() { return "Unrequests"; }

            public int GetValue(Scene scene, Frame frame, int frameNumber)
            {
                if (scene.Filter.IsActive())
                {
                    // Manually count requests.
                    int result = 0;

                    foreach (Unrequest req in frame.unrequests)
                    {
                        if (!req.Streamable.Filtered)
                        {
                            result++;
                        }
                    }

                    return result;
                }

                return frame.unrequests.Count;
            }
        }

        private class CutoffScore : BarGraph
        {
            public String GetName() { return "Cutoff Score * 30"; }

            public int GetValue(Scene scene, Frame frame, int frameNumber)
            {
                return (int)(frame.MasterCutoff * 30.0f);
            }
        }

        private class BestScoringRequest : BarGraph
        {
            public String GetName() { return "Best Scoring Request * 30"; }

            public int GetValue(Scene scene, Frame frame, int frameNumber)
            {
                int bestValue = 0;

                foreach (Request request in frame.requests)
                {
                    if (((request.Flags & Request.DOOMED) == 0) && request.Score > 0.0f)
                    {
                        bestValue = Math.Max(bestValue, (int)(request.Score * 30.0f));
                    }
                }

                return bestValue;
            }
        }

        private class MissingEntities : BarGraph
        {
            public String GetName() { return "Missing Entities"; }

            public int GetValue(Scene scene, Frame frame, int frameNumber)
            {
                if (frame.BadLodEntities == null)
                {
                    return 0;
                }

                return frame.BadLodEntities.Count;
            }
        }


        private class HasPriorityRequestsMarker : MarkerVisualizer
        {
            public String GetName() { return "Priority Requests"; }

            public bool GetValue(Frame frame, int frameNumber)
            {
                return (frame.Flags & Frame.HAS_PRIORITY) != 0;
            }
        }

        private class WasFragmentedMarker : MarkerVisualizer
        {
            public String GetName() { return "Fragmented"; }

            public bool GetValue(Frame frame, int frameNumber)
            {
                return (frame.Flags & Frame.OOM_FRAGMENTED) != 0;
            }
        }

        private class ChurnProtectionMarker : MarkerVisualizer
        {
            public String GetName() { return "Churn Protection"; }

            public bool GetValue(Frame frame, int frameNumber)
            {
                return (frame.Flags & Frame.OOM_CHURN_PROTECTION) != 0;
            }
        }

        private class AssertMarker : MarkerVisualizer
        {
            public String GetName() { return "Assert"; }

            public bool GetValue(Frame frame, int frameNumber)
            {
                return (frame.Flags & Frame.ASSERT_OCCURRED) != 0;
            }
        }

        private class BlockedForStreamingMarker : MarkerVisualizer
        {
            public String GetName() { return "Blocked For Streaming"; }

            public bool GetValue(Frame frame, int frameNumber)
            {
                return (frame.Flags & Frame.BLOCKED_FOR_STREAMING) != 0;
            }
        }

        private class BugReportedMarker : MarkerVisualizer
        {
            public String GetName() { return "Bug Reported"; }

            public bool GetValue(Frame frame, int frameNumber)
            {
                return (frame.Flags & Frame.BUG_REPORTED) != 0;
            }
        }

        private class LowLodMarker : MarkerVisualizer
        {
            public String GetName() { return "Low LOD"; }

            public bool GetValue(Frame frame, int frameNumber)
            {
                return (frame.Flags & Frame.LOW_LOD) != 0;
            }
        }

        private class MissingEntityMarker : MarkerVisualizer
        {
            public String GetName() { return "Missing Entity"; }

            public bool GetValue(Frame frame, int frameNumber)
            {
                return (frame.Flags & Frame.MISSING_LOD) != 0;
            }
        }

        private class NotableEventMarker : MarkerVisualizer
        {
            public String GetName() { return "Notable Event"; }

            public bool GetValue(Frame frame, int frameNumber)
            {
                return (frame.Flags & (Frame.ASSERT_OCCURRED | Frame.MARKER | Frame.BLOCKED_FOR_STREAMING | Frame.BUG_REPORTED)) != 0;
            }
        }

        private class FreeResourceForMemoryMarker : MarkerVisualizer
        {
            public String GetName() { return "Unrequest Resource For Memory"; }

            public bool GetValue(Frame frame, int frameNumber)
            {
                // HACK HACK HACK
                foreach (Unrequest unrequest in frame.unrequests)
                {
                    if (unrequest.Context != null && unrequest.Context.StartsWith("For"))
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        private class StreamableLoadedRequestedMarker : MarkerVisualizer
        {
            public String GetName() { return "Streamable Loaded/Requested"; }

            public Streamable SelectedStreamable;
 


            public bool GetValue(Frame frame, int frameNumber)
            {
                if (SelectedStreamable == null)
                {
                    return false;
                }

                Streamable.Status status = SelectedStreamable.History.GetStateAt(frameNumber).Status;
                return (status == Streamable.Status.LOADED || status == Streamable.Status.LOADING || status == Streamable.Status.LOADREQUESTED);
            }
        }

        private class StreamableLoadedMarker : MarkerVisualizer
        {
            public String GetName() { return "Streamable Loaded/Loading"; }

            public Streamable SelectedStreamable;



            public bool GetValue(Frame frame, int frameNumber)
            {
                if (SelectedStreamable == null)
                {
                    return false;
                }

                Streamable.Status status = SelectedStreamable.History.GetStateAt(frameNumber).Status;
                return (status == Streamable.Status.LOADED || status == Streamable.Status.LOADING);
            }
        }



        private class FlagChecker
        {
            public FlagChecker(int flagmask, Color color)
            {
                this.FlagMask = flagmask;
                this.Brush = new SolidBrush(color);
            }

            public int FlagMask;
            public Brush Brush;
        };

        private BarGraph[] barGraphList = new BarGraph[] {
            new RealRequestBarGraph(),
            new RequestBarGraph(),
            new RequestThisFrameBarGraph(),
            new UnrequestBarGraph(),
            new CulledMapsBarGraph(),
            new CullboxesBarGraph(),
            new CutoffScore(),
            new BestScoringRequest(),
            new MissingEntities(),
            new CameraSpeedBarGraph(),
        };

        private StreamableLoadedRequestedMarker streamableLoadedRequestedMarker = new StreamableLoadedRequestedMarker();
        private StreamableLoadedMarker streamableLoadedMarker = new StreamableLoadedMarker();

        private List<MarkerVisualizer> markerVisualizerList = new List<MarkerVisualizer>() {
            new HasPriorityRequestsMarker(),
            new FreeResourceForMemoryMarker(),
            new NotableEventMarker(),
            new WasFragmentedMarker(),
            new ChurnProtectionMarker(),
            new AssertMarker(),
            new BlockedForStreamingMarker(),
            new BugReportedMarker(),
            new LowLodMarker(),
            new MissingEntityMarker(),
        };

        private BarGraph[] barGraphs = new BarGraph[2];

        private MarkerVisualizer[] markerVisualizers = new MarkerVisualizer[3];

        private MarkerVisualizer highlighter;

        private FlagChecker[] FlagCheckers = new FlagChecker[] {
            new FlagChecker(Frame.OOM_NO_MEMORY, Color.Red),
            new FlagChecker(Frame.OOM_FRAGMENTED, Color.Yellow),
            new FlagChecker(Frame.OOM_CHURN_PROTECTION, Color.Turquoise),
            new FlagChecker(Frame.MARKER, Color.Purple),
        };

        private ViewContextMenu viewContextMenu = new ViewContextMenu();

        public Scene Scene { get; set; }
        public MainController Controller { get; set; }

        public int CurrentFrameNumber { get; set; }

        private Streamable SelectedStreamable;

        // The frame that the mouse is currently hovering over
        private int MouseFrameNumber;

        private SimpleTooltip tooltip = new SimpleTooltip();

        private UpdateThrottler throttler = new UpdateThrottler(500);

        // Width of each frame in pixels
        private int frameWidth = 8;

        // Height of a flag indicator in pixels
        private float flagHeight = 4.0f;

        // Total offset in X in pixels
        private int offsetX = 0;

        //private bool follow = false;
        private bool isDragging;
        private int lastDragX;
        private int dragStartX;
        private bool TooltipOnCurrentFrame;
        private bool DisableOffBorder = false;

        private int lastMouseX;

        // Try to stay this many pixels off the edge when making the current frame visible
        private const int PreferredMargin = 64;

        private Brush[] barGraphBrush = new Brush[] {
            new SolidBrush(Color.Green),
            new SolidBrush(Color.Red),
        };

        private Brush[] markerBrush = new Brush[] {
            new SolidBrush(Color.Green),
            new SolidBrush(Color.Red),
            new SolidBrush(Color.Blue),
        };

        private Brush frameRangeBrush = new HatchBrush(HatchStyle.ForwardDiagonal, Color.IndianRed, Color.Transparent);

        private Brush selectedFrameBrush = new SolidBrush(Color.White);

        private Font frameNumberFont = new Font("Arial", 30.0f, FontStyle.Bold);
        private Brush frameNumberBrush = new SolidBrush(Color.DarkGray);

        private Brush highlighterBrush = new SolidBrush(Color.Cyan);


        public FrameOverview()
        {
            markerVisualizerList.Add(streamableLoadedRequestedMarker);
            markerVisualizerList.Add(streamableLoadedMarker);

            barGraphs[0] = barGraphList[0];     // The first bar graph shows real requests
            barGraphs[1] = barGraphList[3];     // The second bar graph shows unrequests

            markerVisualizers[0] = markerVisualizerList[0]; // First marker: Has any priority requests
            markerVisualizers[1] = markerVisualizerList[1]; // Second marker: Had to free memory
            markerVisualizers[2] = markerVisualizerList[2]; // Third marker: Notable event


            InitializeComponent();
        }

        private void UpdateTooltip()
        {
            // What frame are we over?
            int frameNumber = (TooltipOnCurrentFrame) ? CurrentFrameNumber : ((lastMouseX + offsetX) / frameWidth);

            if (frameNumber >= Scene.MaxFrame)
            {
                MouseFrameNumber = -1;
                tooltip.ResetToolTip();
            }
            else
            {
                MouseFrameNumber = frameNumber;
                String tooltipText = "Frame " + frameNumber + "/" + Scene.MaxFrame + "\n\n";

                Frame frame = Scene.GetFrame(frameNumber);

                // Show the marker text, if there is one.
                if (frame.MarkerText != null)
                {
					bool first = true;
					tooltipText += "MARKER: ";
					String[] markerLines = frame.MarkerText.Split(new string[] { " / " }, StringSplitOptions.None);
					foreach (String line in markerLines)
					{
						if (first)
						{
							tooltipText += String.Format("\"{0}\"\n", line);
							first = false;
						}
						else
						{
							tooltipText += String.Format("\t\"{0}\"\n", line);
						}

					}
                }

                // Show ALL bar graphs.
                foreach (BarGraph graph in barGraphList)
                {
                    tooltipText += graph.GetName() + ": " + graph.GetValue(Scene, frame, frameNumber) + "\n";
                }

                // And ALL markers.
                foreach (MarkerVisualizer visualizer in markerVisualizerList)
                {
                    if (visualizer.GetValue(frame, frameNumber))
                    {
                        tooltipText += visualizer.GetName() + "\n";
                    }
                }

                tooltip.SetToolTip(tooltipText);
            }
        }

        private void UpdateScrollerPos()
        {
            if (offsetX > Scroller.Maximum)
            {
                SetScrollerRange();
            }

            Scroller.Value = Math.Min(offsetX, Scroller.Maximum);
        }

        private void FrameOverview_MouseMove(object sender, MouseEventArgs e)
        {
            // Drag, if possible.
            if (isDragging)
            {
                int delta = e.X - lastDragX;
                offsetX = Math.Max(offsetX - delta, 0);

                // Limit it so we don't go past the last frame.
                int maxX = Math.Max(Scene.MaxFrame * frameWidth - Width, 0);
                offsetX = Math.Min(offsetX, maxX);

                lastDragX = e.X;
                UpdateScrollerPos();
            }

            lastMouseX = e.X;
            TooltipOnCurrentFrame = false;
            UpdateTooltip();

            tooltip.MouseMove(e);
            Refresh();
        }

        private void SetScrollerRange()
        {
            // Total number of pixels worth of graph goodness
            int maxFrameCount = Scene.MaxFrame;
            int totalWidth = maxFrameCount * frameWidth;

            int scrollRange = Math.Max(totalWidth - Width, 0);

            if (Scroller.Maximum != scrollRange)
            {
                Scroller.Maximum = scrollRange;
            }
        }

        private void FrameOverview_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isDragging = true;
                lastDragX = e.X;
                dragStartX = e.X;
            }
        }

        private void FrameOverview_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            if (Scene == null)
            {
                return;
            }

            UpdateScrollerPos();
            SetScrollerRange();

            // Draw all the frames.
            // Get the left-most one.
            int frameNumber = offsetX / frameWidth;
            float xpos = (float)(frameNumber * frameWidth - offsetX);

            float barWidthf = (float)Math.Max(frameWidth - 1, 1);
            float barWidthPerGraphf = (float)Math.Max(barWidthf / (float)barGraphs.Length, 1.0f);

            int localHeight = Scroller.Top;

            int maxFrameCount = Scene.MaxFrame;
            int baseYPos = localHeight - 20;

            // First, the highlighter.
            // Highlighter first.
            if (highlighter != null)
            {
                int highFrame = frameNumber;
                float highXpos = xpos;

                while (highXpos < Width && highFrame < maxFrameCount - 1)
                {
                    Frame frame = Scene.GetFrame(highFrame);
                    if (highlighter.GetValue(frame, highFrame))
                    {
                        g.FillRectangle(highlighterBrush, highXpos, 0, (float)frameWidth, (float)Height);
                    }

                    highFrame++;
                    highXpos += (float)frameWidth;
                }
            }



            // Do the background frame markers.
            int markerNumber = frameNumber;
            markerNumber += 99;
            markerNumber -= markerNumber % 100;

            float markerPos = (float)(markerNumber * frameWidth - offsetX);

            while (markerPos < (float)Width)
            {
                g.DrawString(String.Format("{0}", markerNumber), frameNumberFont, frameNumberBrush, markerPos, 10.0f);
                markerNumber += 100;
                markerPos += (frameWidth) * 100.0f;
            }

            // Highlight the frame range.
            float frXpos = (float)(Scene.SelectedFirstFrame * frameWidth - offsetX);
            float frWidth = (float)((Scene.SelectedLastFrame + 1 - Scene.SelectedFirstFrame) * frameWidth);
            g.FillRectangle(frameRangeBrush, (float)frXpos, 0.0f, (float)frWidth, (float)localHeight);

            // NOTE that we do not draw the last frame - its data could change, and some of the visualizers currently
            // iterate through the frame to retrieve information, which would result in a crash.
            while (xpos < Width && frameNumber < maxFrameCount - 1)
            {
                // Highlight the currently selected frame.
                if (CurrentFrameNumber == frameNumber)
                {
                    g.FillRectangle(selectedFrameBrush, (float)xpos, 0.0f, (float)frameWidth, (float)localHeight);
                }

                Frame frame = Scene.GetFrame(frameNumber);

                // Do the bar graphs.
                for (int graphIndex = 0; graphIndex < barGraphs.Length; graphIndex++)
                {
                    BarGraph graph = barGraphs[graphIndex];

                    if (graph != null)
                    {
                        float graphOffsetX = barWidthf * (float)graphIndex / (float)barGraphs.Length;
                        float leftX = (float)xpos + graphOffsetX;
                        float requestCountf = (float)graph.GetValue(Scene, frame, frameNumber);
                        g.FillRectangle(barGraphBrush[graphIndex], leftX, baseYPos - requestCountf, barWidthPerGraphf, requestCountf);
                    }
                }

                // Now the flags.
                float yPos = (float)localHeight - 2.0f - flagHeight;

                for (int markerIndex = 0; markerIndex < markerVisualizers.Length; markerIndex++)
                {
                    MarkerVisualizer visualizer = markerVisualizers[markerIndex];

                    if (visualizer != null)
                    {
                        if (visualizer.GetValue(frame, frameNumber))
                        {
                            g.FillRectangle(markerBrush[markerIndex], (float)xpos, yPos, barWidthf, flagHeight);
                        }
                    }

                    yPos -= flagHeight;
                }


                /*                int flags = frame.Flags;
                                float yPos = (float)Height - 2.0f - flagHeight;

                                if (flags != 0)
                                {
                                    foreach (FlagChecker flagChecker in FlagCheckers)
                                    {
                                        if ((flags & flagChecker.FlagMask) != 0)
                                        {
                                            g.FillRectangle(flagChecker.Brush, (float)xpos, yPos, barWidthf, flagHeight);
                                        }

                                        yPos -= flagHeight;
                                    }
                                }
                */
                frameNumber++;
                xpos += (float)frameWidth;
            }

            tooltip.Draw(g, DefaultFont, Width, localHeight);
        }

        private void FrameOverview_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isDragging = false;

                // Don't update the current frame if we had been dragging.
                if (Math.Abs(e.X - dragStartX) < 4)
                {
                    // What frame are we over?
                    int frameNumber = (e.X + offsetX) / frameWidth;

                    if (frameNumber < Scene.MaxFrame && frameNumber >= 0)
                    {
                        DisableOffBorder = true;
                        Controller.SetToFrame(frameNumber, true);
                        DisableOffBorder = false;
                    }
                }
            }
        }

        public void RefreshDeferred()
        {
            BeginInvoke((Action)delegate { Refresh(); });
        }

        // Switch the display to focus on a certain frame
        public void SetToFrame(int frameNumber)
        {
            CurrentFrameNumber = frameNumber;
            EnsureFrameVisible(frameNumber, true);
            RefreshDeferred();
        }

        protected override void OnMouseWheel(MouseEventArgs e)
        {
            if (e.Delta != 0)
            {
                // Keep the selected item at the same place.
                int selectedX = CurrentFrameNumber * frameWidth - offsetX;

                frameWidth += (e.Delta > 0) ? 1 : -1;

                frameWidth = Math.Max(frameWidth, 1);

                offsetX = CurrentFrameNumber * frameWidth - selectedX;
                offsetX = Math.Max(offsetX, 0);
                UpdateScrollerPos();

                Refresh();
            }

            base.OnMouseWheel(e);
        }

        // A new device was registered, or a device has changed.
        public void OnDeviceChanged(int deviceIndex, Device device) { }

        // A streamable has been selected
        public void OnSelectStreamable(Streamable streamable)
        {
            SelectedStreamable = streamable;
            streamableLoadedRequestedMarker.SelectedStreamable = streamable;
            streamableLoadedMarker.SelectedStreamable = streamable;
            Refresh();
        }

        // The client information has been updated
        public void ClientInfoUpdated() { }

        // We're in follow mode, and a new frame has been added.
        public void OnNewFrame(int frameNumber)
        {
            EnsureFrameVisible(frameNumber, false);

            UpdateTooltip();

            if (throttler.MayUpdate())
            {
                RefreshDeferred();
            }
        }

        public void EnsureFrameVisible(int frameNumber, bool offBorder)
        {
            // Make sure the new frame is still visible.
            int mustBeVisibleX = frameNumber * frameWidth;

            if (!offBorder || DisableOffBorder)
            {
                // If we're just meant to clip it, just clip it.
                offsetX = Math.Max(offsetX, Math.Max(mustBeVisibleX - Width - frameWidth, 0));
                offsetX = Math.Min(offsetX, mustBeVisibleX);
            }
            else
            {
                // Let's see - is it even on the screen?
                if (offsetX > mustBeVisibleX || offsetX < mustBeVisibleX - Width - frameWidth)
                {
                    // Entirely off screen - let's center it first, and then clip it.
                    offsetX = mustBeVisibleX - (Width / 2);
                }
                else
                {
                    // Try to keep it off the border.
                    offsetX = Math.Min(offsetX, mustBeVisibleX - PreferredMargin);
                    offsetX = Math.Max(offsetX, mustBeVisibleX - Width - frameWidth + PreferredMargin);
                }

                // A final clip to keep it within the bounds.
                offsetX = Math.Min(offsetX, Scene.FrameCount * frameWidth - Width);
                offsetX = Math.Max(offsetX, 0);
            }
        }

        // A new entity type was registered
        public void OnNewEntityType(int entityTypeId, EntityType type) { }

        private void FrameOverview_MouseEnter(object sender, EventArgs e)
        {
            this.Focus();
        }

        protected override bool IsInputKey(Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Up:
                case Keys.Down:
                case Keys.Left:
                case Keys.Right:
                    return true;
            }
            return base.IsInputKey(keyData);
        }

        // Switch to a certain frame number, but clip it first.
        private void SetToClippedFrame(int frameNumber)
        {
            frameNumber = Math.Min(Scene.MaxFrame - 2, frameNumber);
            frameNumber = Math.Max(frameNumber, 0);
            Controller.SetToFrame(frameNumber, true);
        }

        private void SelectNewFrame()
        {
            TooltipOnCurrentFrame = false;
            EnsureFrameVisible(CurrentFrameNumber, true);
            UpdateTooltip();
            Controller.SetToFrame(CurrentFrameNumber, true);
        }

        private void FrameOverview_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Left:
                    if (e.Control)
                    {
                        // Go to the previous marker.
                        int TestFrame = CurrentFrameNumber;
                        while (--TestFrame > 0)
                        {
                            if (Scene.GetFrame(TestFrame).MarkerText != null)
                            {
                                CurrentFrameNumber = TestFrame;
                                SelectNewFrame();
                                break;
                            }
                        }
                    }
                    else
                    {
                        if (CurrentFrameNumber > 0)
                        {
                            CurrentFrameNumber--;
                            SelectNewFrame();
                        }
                    }
                    break;

                case Keys.Right:
                    if (e.Control)
                    {
                        // Go to the previous marker.
                        int TestFrame = CurrentFrameNumber;
                        while (++TestFrame < Scene.MaxFrame - 1)
                        {
                            if (Scene.GetFrame(TestFrame).MarkerText != null)
                            {
                                CurrentFrameNumber = TestFrame;
                                SelectNewFrame();
                                break;
                            }
                        }
                    }
                    else
                    {
                        if (Scene != null && CurrentFrameNumber < Scene.MaxFrame - 1)
                        {
                            CurrentFrameNumber++;
                            SelectNewFrame();
                        }
                    }
                    break;

                case Keys.F:
                case Keys.End:
                    if (Scene != null)
                    {
                        Scene.FollowMode = true;

                        if (Scene.MaxFrame > 1)
                        {
                            Controller.SetToFrame(Scene.MaxFrame - 2, true);
                        }

                        Scene.FollowMode = true;
                    }
                    break;

                case Keys.PageDown:
                    {
                        if (Scene != null)
                        {
                            Scene.FollowMode = false;

                            int framesPerScreen = Width / frameWidth;
                            SetToClippedFrame(CurrentFrameNumber + framesPerScreen * 2 / 3);
                        }
                    }
                    break;

                case Keys.PageUp:
                    {
                        if (Scene != null)
                        {
                            Scene.FollowMode = false;

                            int framesPerScreen = Width / frameWidth;
                            SetToClippedFrame(CurrentFrameNumber - framesPerScreen * 2 / 3);
                        }
                    }
                    break;

                case Keys.G:
                    if (e.Modifiers == Keys.Control)
                    {
                        GoToFrame gf = new GoToFrame();
                        gf.SelectFrameCb = SelectFrame;
                        gf.Show();
                    }
                    break;

                case Keys.Home:
                    if (Scene != null && Scene.MaxFrame > 0)
                    {
                        Controller.SetToFrame(0, true);
                    }
                    break;
            }
        }

        // The connection has been terminated
        public void OnConnectionLost() { }

        public void OnSelectEntity(Entity entity) { }

        // Register this view with the system
        public void Register(MainController controller, Scene scene)
        {
            this.Controller = controller;
            this.Scene = scene;
        }

        private void UncheckAllSiblings(MenuItem item)
        {
            foreach (MenuItem sibling in item.Parent.MenuItems)
            {
                sibling.Checked = false;
            }
        }

        private void SetBarGraph(int barGraphIndex, BarGraph graph, MenuItem item)
        {
            UncheckAllSiblings(item);

            barGraphs[barGraphIndex] = graph;
            item.Checked = true;
            Refresh();
        }

        private void SetMarkerVisualizer(int markerVisualizerIndex, MarkerVisualizer visualizer, MenuItem item)
        {
            UncheckAllSiblings(item);

            markerVisualizers[markerVisualizerIndex] = visualizer;
            item.Checked = true;
            Refresh();
        }


        private void SetHighlighter(MarkerVisualizer visualizer, MenuItem item)
        {
            UncheckAllSiblings(item);

            highlighter = visualizer;
            item.Checked = true;
            Refresh();
        }

        private void MarkFirst()
        {
            if (MouseFrameNumber != -1)
            {
                Scene.SelectedFirstFrame = MouseFrameNumber;
                Scene.SelectedLastFrame = Math.Max(Scene.SelectedLastFrame, Scene.SelectedFirstFrame);
                Controller.OnFrameRangeChanged(Scene.SelectedFirstFrame, Scene.SelectedLastFrame);
            }
        }

        private void MarkLast()
        {
            if (MouseFrameNumber != -1)
            {
                Scene.SelectedLastFrame = MouseFrameNumber;
                Scene.SelectedFirstFrame = Math.Min(Scene.SelectedFirstFrame, Scene.SelectedLastFrame);
                Controller.OnFrameRangeChanged(Scene.SelectedFirstFrame, Scene.SelectedLastFrame);
            }
        }

        private void DeletePriorHistory()
        {
            int frame = MouseFrameNumber;

            DialogResult result = MessageBox.Show("You're about to delete most of the log data prior to frame " + frame + ". Are you sure that's what you want?",
                "Confirm Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);

            if (result == DialogResult.Yes)
            {
                // Okay, go to town.
                Scene.DeletePriorHistory(frame);
            }
        }

        private void SelectTestRange()
        {
            if (MouseFrameNumber != -1)
            {
                // Find the closest test range.
                int startFrame = 0;
                int endFrame = 0;

                for (startFrame = MouseFrameNumber; startFrame >= 0; startFrame--)
                {
                    if ((Scene.GetFrame(startFrame).Flags & Frame.BEGIN_TEST) != 0)
                    {
                        break;
                    }
                }

                for (endFrame = MouseFrameNumber; endFrame < Scene.MaxFrame - 1; endFrame++)
                {
                    if ((Scene.GetFrame(endFrame).Flags & Frame.END_TEST) != 0)
                    {
                        break;
                    }
                }

                if (startFrame == -1 || endFrame >= Scene.MaxFrame - 1)
                {
                    return;
                }

                Scene.SelectedFirstFrame = startFrame;
                Scene.SelectedLastFrame = endFrame;
                Controller.OnFrameRangeChanged(Scene.SelectedFirstFrame, Scene.SelectedLastFrame);
            }
        }

        private void FrameOverview_Load(object sender, EventArgs e)
        {
            ContextMenu menu = viewContextMenu.CreateContextMenu(Controller, this);

            // Add our own options.
            for (int x = 0; x < barGraphs.Length; x++)
            {
                MenuItem[] barGraphItems = new MenuItem[barGraphList.Length];

                for (int y = 0; y < barGraphList.Length; y++)
                {
                    int itemIndex = x;
                    int graphIndex = y;
                    barGraphItems[y] = new MenuItem(barGraphList[y].GetName());
                    barGraphItems[y].RadioCheck = true;
                    barGraphItems[y].Click += (menuSender, menuE) => SetBarGraph(itemIndex, barGraphList[graphIndex], barGraphItems[graphIndex]);
                    barGraphItems[y].Checked = barGraphs[x] == barGraphList[y];
                }

                MenuItem barGraphSelector = new MenuItem("Bar Graph " + (x + 1), barGraphItems);
                menu.MenuItems.Add(barGraphSelector);
            }

            for (int x = 0; x < markerVisualizers.Length; x++)
            {
                MenuItem[] markerVisualizerItems = new MenuItem[markerVisualizerList.Count];

                for (int y = 0; y < markerVisualizerList.Count; y++)
                {
                    int itemIndex = x;
                    int visualizerIndex = y;
                    markerVisualizerItems[y] = new MenuItem(markerVisualizerList[y].GetName());
                    markerVisualizerItems[y].RadioCheck = true;
                    markerVisualizerItems[y].Click += (menuSender, menuE) => SetMarkerVisualizer(itemIndex, markerVisualizerList[visualizerIndex], markerVisualizerItems[visualizerIndex]);
                    markerVisualizerItems[y].Checked = markerVisualizers[x] == markerVisualizerList[y];
                }

                MenuItem markerVisualizerSelector = new MenuItem("Marker " + (x + 1), markerVisualizerItems);
                menu.MenuItems.Add(markerVisualizerSelector);
            }

            MenuItem[] highlighterItems = new MenuItem[markerVisualizerList.Count + 1];

            for (int y = 0; y < markerVisualizerList.Count; y++)
            {
                int visualizerIndex = y;
                highlighterItems[y] = new MenuItem(markerVisualizerList[y].GetName());
                highlighterItems[y].RadioCheck = true;
                highlighterItems[y].Click += (menuSender, menuE) => SetHighlighter(markerVisualizerList[visualizerIndex], highlighterItems[visualizerIndex]);
                highlighterItems[y].Checked = highlighter == markerVisualizerList[y];
            }

            highlighterItems[markerVisualizerList.Count] = new MenuItem("NONE");
            highlighterItems[markerVisualizerList.Count].RadioCheck = true;
            highlighterItems[markerVisualizerList.Count].Click += (menuSender, menuE) => SetHighlighter(null, highlighterItems[markerVisualizerList.Count]);
            highlighterItems[markerVisualizerList.Count].Checked = highlighter == null;

            MenuItem highlighterSelector = new MenuItem("Highlighter", highlighterItems);
            menu.MenuItems.Add(highlighterSelector);


            MenuItem markFirst = new MenuItem("Mark First");
            markFirst.Click += (menuSender, menuE) => MarkFirst();
            menu.MenuItems.Add(markFirst);

            MenuItem markLast = new MenuItem("Mark Last");
            markLast.Click += (menuSender, menuE) => MarkLast();
            menu.MenuItems.Add(markLast);

            menu.MenuItems.Add(ContextMenuHelper.CreateActionMenu("Select Test Range", SelectTestRange));
            menu.MenuItems.Add(ContextMenuHelper.CreateActionMenu("Delete Prior History", DeletePriorHistory));

            this.ContextMenu = menu;
        }

        private void SelectFrame(int frame)
        {
            Controller.SetToFrame(frame, true);
        }

        public void OnFilterChanged()
        {
            Refresh();
        }

        public void OnFrameRangeChanged(int firstFrame, int lastFrame)
        {
            Refresh();
        }

        private void FrameOverview_MouseLeave(object sender, EventArgs e)
        {
            //this.InvokeLostFocus();
        }

        private void Scroller_Scroll(object sender, ScrollEventArgs e)
        {
            int value = e.NewValue;

            if (value != offsetX)
            {
                offsetX = value;
                Refresh();
            }
        }
    }
}
