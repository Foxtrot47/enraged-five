﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StreamingVisualize.Model
{
    public class Keyframe : IVersionedSerializable
    {
        public ConcurrentList<Entity> Entities = new ConcurrentList<Entity>();
        public Dictionary<int, Entity> EntityMap = new Dictionary<int, Entity>();
        public int FrameNumber { get; set; }

        public void Serialize(VersionedWriter writer)
        {
            writer.Write(FrameNumber);
        }

        public void Deserialize(VersionedReader reader, int version)
        {
            FrameNumber = reader.ReadInt();
        }
    }
}
