﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using StreamingVisualize.Model;
using System.Diagnostics;
using System.IO;

namespace StreamingVisualize
{
    public class Communication
    {
        private const int PROTOCOL_VERSION = 22;

        private const int SV_INIT = 1;
        private const int SV_REGISTEROBJECT = 2;
        private const int SV_REGISTERMODULE = 3;
        private const int SV_REQUEST = 4;
        private const int SV_UNREQUEST = 5;
        private const int SV_SET_STATUS = 6;
        private const int SV_FRAME = 7;
        private const int SV_REGISTERDEVICE = 8;
        private const int SV_ISSUEREQUEST = 9;
        private const int SV_PROCESSREQUEST = 10;
        private const int SV_FINISHSTREAMING = 11;
        private const int SV_SETCONTEXT = 12;
        private const int SV_CANCEL = 13;
        private const int SV_ADD_MARKER = 14;
        private const int SV_ADD_ENTITY = 15;
        private const int SV_DELETE_ENTITY = 16;
        private const int SV_UPDATE_ENTITY = 17;
        private const int SV_REGISTER_ENTITY_TYPE = 18;
        private const int SV_STREAMER_READ = 19;
        private const int SV_FAILED_ALLOCATION = 20;
        private const int SV_ADD_STREAMING_VOLUME = 21;
        private const int SV_LOWLEVEL_OPEN = 22;
        private const int SV_LOWLEVEL_CLOSE = 23;
        private const int SV_LOWLEVEL_READ = 24;
        private const int SV_LOWLEVEL_WRITE = 25;
        private const int SV_LOWLEVEL_SEEK = 26;
        private const int SV_MISSING_LOD = 27;
        private const int SV_DEPENDENCY_INFO = 28;
        private const int SV_UNREGISTER_OBJECT = 29;
        private const int SV_REGISTER_MAPDATA = 30;
        private const int SV_INSTANTIATE_MAPDATA = 31;
        private const int SV_ADD_MAPDATA_ENTITY = 32;
        private const int SV_SET_MARKER_TEXT = 33;
        private const int SV_ADD_VISIBLE_ENTITIES = 34;
        private const int SV_REMOVE_VISIBLE_ENTITIES = 35;
        private const int SV_REGISTER_LOD_TYPE = 36;
        private const int SV_ADD_TO_NEEDED_LIST = 37;
        private const int SV_SET_EXTRA_CLIENT_INFO = 38;
        private const int SV_DEVICE_IDLE = 39;
        private const int SV_LOWLEVEL_READEND = 40;
		private const int SV_STREAMER_DECOMPRESS = 41;
        private const int SV_UPDATE_VISFLAGS = 42;
        private const int SV_REGISTER_VISFLAG = 43;
        private const int SV_SET_LOD_PARENT = 44;
        private const int SV_ADD_BOX_SEARCH = 45;
        private const int SV_REGISTER_NAMED_FLAG = 46;
        private const int SV_SET_FLAGS = 47;
        private const int SV_OPEN_RAW_FILE = 48;
        private const int SV_CHANGE_GAME_STATE = 49;
        private const int SV_REGISTER_STATE_FLAG = 50;
        private const int SV_ADD_CULLBOX = 51;




        // Networking metrics to see how much data we're sending
        private int[] commandCount = new int[256];
        private long[] commandSize = new long[256];
        private long dataReceived = 0;




        private delegate void CommandHandler();

        private Dictionary<int, CommandHandler> commandHandlers = new Dictionary<int, CommandHandler>();

        // If true, we're supposed to bail.
        private volatile bool terminateRequest;

        private bool initCalled = false;

        static private List<Communication> Sockets = new List<Communication>();


        private Scene scene;
        
        Socket socket;
        byte[] commandBuffer = new byte[65536];
        UTF8Encoding enc = new UTF8Encoding();


        public Communication(Socket socket, Scene scene)
        {
            this.socket = socket;
            this.scene = scene;

            lock(Sockets)
            {
                Sockets.Add(this);
            }
        }

        public void Start()
        {
            Thread thread = new Thread(new ThreadStart(Worker));
            thread.Name = "Connection Handler";
            thread.Start();
        }

        // Called from another thread, in an attempt to terminate the connection.
        public void Kill()
        {
            terminateRequest = true;

            try
            {
                socket.Close();
            }
            catch (Exception e)
            {
                // Many reasons why this can go wrong. Well, we tried.
				Console.WriteLine(e.ToString());
				Console.WriteLine("StackTrace: '{0}'", Environment.StackTrace);
				//throw;
            }
        }

        static public void KillAllSockets()
        {
            lock (Sockets)
            {
                foreach (Communication comm in Sockets)
                {
                    comm.Kill();
                }
            }
        }

        private bool Read(byte[] buffer, int len)
        {
            int offset = 0;
            dataReceived += (long)len;

            while (len > 0)
            {
                int read = socket.Receive(commandBuffer, offset, len, SocketFlags.None);

                if (read == 0)
                {
                    Console.WriteLine("Didn't receive any data");
                    throw new IOException("No data received - connection assumed dead");
                }

                offset += read;
                len -= read;
            }

            return true;
        }

        private long ReadLong()
        {
            ulong v1 = (ulong) (uint) ReadInt();
            ulong v2 = (ulong) (uint) ReadInt();

            return (long)((v2 << 32) | v1);
        }

        private int ReadInt()
        {
            Read(commandBuffer, 4);

            return commandBuffer[3] << 24 | commandBuffer[2] << 16 | commandBuffer[1] << 8 | commandBuffer[0];
        }

        private float ReadFloat()
        {
            int intValue = ReadInt();

            unsafe
            {
                int* floatPtr = &intValue;
                return *((float*)floatPtr);
            }
        }

        private Vector ReadVector()
        {
            Vector v = new Vector();
            v.X = ReadFloat();
            v.Y = ReadFloat();
            v.Z = ReadFloat();

            return v;
        }

        private Vector ReadVectorFrom16()
        {
            Vector v = new Vector();
            v.X = Vector16.Unpack((short)ReadShort());
            v.Y = Vector16.Unpack((short)ReadShort());
            v.Z = Vector16.Unpack((short)ReadShort());

            return v;
        }

        private Vector16 ReadVector16()
        {
            Vector16 v = new Vector16((short) ReadShort(), (short) ReadShort(), (short) ReadShort());

            return v;
        }

        private Box ReadBox()
        {
            Box b = new Box();
            b.BoxMin = ReadVector();
            b.BoxMax = ReadVector();

            return b;
        }

        private Box ReadBoxFrom16()
        {
            Box b = new Box();
            b.BoxMin = ReadVectorFrom16();
            b.BoxMax = ReadVectorFrom16();

            return b;
        }

        private Box16 ReadBox16()
        {
            Box16 b = new Box16();
            b.BoxMin = ReadVector16();
            b.BoxMax = ReadVector16();

            return b;
        }

        private int ReadShort()
        {
            Read(commandBuffer, 2);

            return commandBuffer[1] << 8 | commandBuffer[0];
        }

        private int ReadChar()
        {
            Read(commandBuffer, 1);

            return commandBuffer[0];
        }

        private String ReadString()
        {
            int len = ReadShort();

            if (len > 65535)
            {
                Console.WriteLine("String too long");
            }

            Read(commandBuffer, len);
            return enc.GetString(commandBuffer, 0, len - 1);
        }

        private String ReadFixedString(int len)
        {
            Read(commandBuffer, len);
            return stringFromFixedBuffer(commandBuffer);
        }

        private void Worker()
        {
            Console.WriteLine("New thread started");

            int lastCommand = 0;

            dataReceived = 0;
            Array.Clear(commandCount, 0, commandCount.Length);

            commandHandlers[SV_INIT] = HandleInit;
            commandHandlers[SV_REGISTEROBJECT] = RegisterObject;
            commandHandlers[SV_REGISTERMODULE] = RegisterModule;
            commandHandlers[SV_REGISTERDEVICE] = RegisterDevice;
            commandHandlers[SV_REQUEST] = Request;
            commandHandlers[SV_UNREQUEST] = Unrequest;
            commandHandlers[SV_SET_STATUS] = SetStatus;
            commandHandlers[SV_ISSUEREQUEST] = IssueRequest;
            commandHandlers[SV_PROCESSREQUEST] = ProcessRequest;
            commandHandlers[SV_FINISHSTREAMING] = FinishRequest;
            commandHandlers[SV_CANCEL] = CancelRequest;
            commandHandlers[SV_ADD_MARKER] = AddMarker;
            commandHandlers[SV_ADD_ENTITY] = AddEntity;
            commandHandlers[SV_UPDATE_ENTITY] = UpdateEntity;
            commandHandlers[SV_DELETE_ENTITY] = DeleteEntity;
            commandHandlers[SV_FRAME] = BeginNewFrame;
            commandHandlers[SV_REGISTER_ENTITY_TYPE] = RegisterEntityType;
            commandHandlers[SV_STREAMER_READ] = StreamerRead;
            commandHandlers[SV_FAILED_ALLOCATION] = FailedAllocation;
            commandHandlers[SV_ADD_STREAMING_VOLUME] = AddStreamingVolume;
            commandHandlers[SV_LOWLEVEL_OPEN] = LowLevelOpen;
            commandHandlers[SV_LOWLEVEL_CLOSE] = LowLevelClose;
            commandHandlers[SV_LOWLEVEL_READ] = LowLevelRead;
            commandHandlers[SV_LOWLEVEL_WRITE] = LowLevelWrite;
            commandHandlers[SV_LOWLEVEL_SEEK] = LowLevelSeek;
            commandHandlers[SV_MISSING_LOD] = MissingLod;
            commandHandlers[SV_DEPENDENCY_INFO] = DependencyInfo;
            commandHandlers[SV_UNREGISTER_OBJECT] = UnregisterObject;
            commandHandlers[SV_REGISTER_MAPDATA] = RegisterMapData;
            commandHandlers[SV_INSTANTIATE_MAPDATA] = InstantiateMapData;
            commandHandlers[SV_ADD_MAPDATA_ENTITY] = AddMapdataEntity;
            commandHandlers[SV_SET_MARKER_TEXT] = SetMarkerText;
            commandHandlers[SV_ADD_VISIBLE_ENTITIES] = AddVisibleEntities;
            commandHandlers[SV_REMOVE_VISIBLE_ENTITIES] = RemoveVisibleEntities;
            commandHandlers[SV_REGISTER_LOD_TYPE] = RegisterLodType;
            commandHandlers[SV_ADD_TO_NEEDED_LIST] = AddToNeededList;
            commandHandlers[SV_SET_EXTRA_CLIENT_INFO] = SetExtraClientInfo;
            commandHandlers[SV_DEVICE_IDLE] = DeviceIdle;
            commandHandlers[SV_LOWLEVEL_READEND] = LowLevelReadEnd;
            commandHandlers[SV_STREAMER_DECOMPRESS] = StreamerDecompress;
            commandHandlers[SV_UPDATE_VISFLAGS] = UpdateVisFlags;
            commandHandlers[SV_REGISTER_VISFLAG] = RegisterVisFlags;
            commandHandlers[SV_SET_LOD_PARENT] = SetLodParent;
            commandHandlers[SV_REGISTER_NAMED_FLAG] = RegisterNamedFlag;
            commandHandlers[SV_ADD_BOX_SEARCH] = AddBoxSearch;
            commandHandlers[SV_SET_FLAGS] = SetFlags;
            commandHandlers[SV_OPEN_RAW_FILE] = OpenRawFile;
            commandHandlers[SV_CHANGE_GAME_STATE] = ChangeGameState;
            commandHandlers[SV_REGISTER_STATE_FLAG] = RegisterStateFlag;
            commandHandlers[SV_ADD_CULLBOX] = AddCullbox;

            try
            {
                while (!terminateRequest)
                {
                    int header = ReadInt();
                    int command = header & 255;

                    //Console.WriteLine("Command " + command);

                    if (!commandHandlers.ContainsKey(command))
                    {
                        scene.controller.ShowError("Unknown command " + command + ", last known command was " + lastCommand + " - there was a communication error. This is a bug in StreamingVisualize, or the game.");
                        Terminate();
                        break;
                    }

                    commandCount[command]++;

                    CommandHandler handler = commandHandlers[command];
                    long dataBefore = dataReceived;
                    handler();
                    commandSize[command] += dataReceived - dataBefore;
                    lastCommand = command;
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine("IO Exception during communication - closing comms thread - " + ex.Message);				
				Console.WriteLine("StackTrace: '{0}'", Environment.StackTrace);
				//throw;
            }
            catch (SocketException ex)
            {
                Console.WriteLine("Socket Exception during communication - closing comms thread - " + ex.Message);
				Console.WriteLine("StackTrace: '{0}'", Environment.StackTrace);
				//throw;
            }

            scene.controller.MarkConnectionLost();
            DumpNetStats();

            // Kill the socket, just to make sure the title won't try to talk to us anymore.
            try
            {
                socket.Close();
            }
            catch (System.Exception e)
            {
				Console.WriteLine(e.ToString());
				Console.WriteLine("StackTrace: '{0}'", Environment.StackTrace);
				//throw;
            }

            lock (Sockets)
            {
                Sockets.Remove(this);
            }

            Console.WriteLine("Communication handler thread terminated");
        }

        public void Terminate()
        {
            Console.WriteLine("Requesting communication handler termination");
            terminateRequest = true;
        }

        private String stringFromFixedBuffer(byte[] buffer)
        {
            for (int x = 0; x < buffer.Length; x++)
            {
                if (buffer[x] == 0)
                {
                    return enc.GetString(buffer, 0, x);
                }
            }

            return "";
        }



        private void HandleInit()
        {
            if (initCalled)
            {
                scene.controller.ShowError("Received two initialization packets - something in the communication must have gone wrong.");
                Terminate();
            }

            initCalled = true;

            int protocol = ReadInt();

            if (protocol != PROTOCOL_VERSION)
            {
                String action = (protocol > PROTOCOL_VERSION) ? "Please update your StreamingVisualize(TM) executable, either by updating your tools or by getting it directly from Perforce."
                    : "Your game executable is too old. Either update your executable, or grab an older version of StreamingVisualize(TM) from Perforce.";
                scene.controller.ShowError(String.Format("Unknown protocol version {0}, expected {1}. {2}",
                    protocol, PROTOCOL_VERSION, action));
                Terminate();
            }

            Read(commandBuffer, 256);
            String symbolFile = stringFromFixedBuffer(commandBuffer);
            Console.WriteLine("Symbol file: " + symbolFile);

            Read(commandBuffer, 32);
            String platform = stringFromFixedBuffer(commandBuffer);
            Console.WriteLine("Platform: " + platform);

            Read(commandBuffer, 32);
            String configuration = stringFromFixedBuffer(commandBuffer);
            Console.WriteLine("Configuration: " + configuration);

            Read(commandBuffer, 32);
            String restIP = stringFromFixedBuffer(commandBuffer);

            int pointerSize = ReadShort();
            Console.WriteLine("Pointer size: " + pointerSize);

            int platformId = ReadChar();
            Console.WriteLine("Platform ID: " + platformId);

            int flags = ReadChar();

            scene.SetClientInfo(configuration, platform, socket.RemoteEndPoint.ToString(), restIP);
        }

        private void RegisterObject()
        {
            int size = ReadInt() << 2;
            int strIndex = ReadInt();
            int moduleIndex = ReadInt();
            int virtSize = ReadInt();
            int physSize = ReadInt();
            int fileSize = ReadInt();
            int parentIndex = ReadInt();
            String rawString = ReadString();
            String name = scene.StringPool.GetString(rawString);
            Module module = scene.GetModule(moduleIndex);

            Debug.Assert(module != null, "Streamable " + name + " is using an invalid module " + moduleIndex);

            lock (scene.LockObj)
            {
                scene.RegisterObject(strIndex, module, name, virtSize, physSize, fileSize, parentIndex);
            }

            int pad = size - 4 - 4 - 4 - 4 - 4 - 4 - 2 - rawString.Length - 1;
            if (pad > 0)
            {
                Read(commandBuffer, pad);
            }
        }

        private void RegisterModule()
        {
            int size = ReadInt() << 2;
            int moduleIndex = ReadInt();
            String rawString = ReadString();
            String name = scene.StringPool.GetString(rawString);

            lock (scene.LockObj)
            {
                scene.RegisterModule(moduleIndex, name);
            }

            int pad = size - 4 - 2 - rawString.Length - 1;
            if (pad > 0)
            {
                Read(commandBuffer, pad);
            }
        }

        private void RegisterDevice()
        {
            int size = ReadInt() << 2;
            int deviceIndex = ReadInt();
            String name = ReadString();

            lock (scene.LockObj)
            {
                scene.RegisterDevice(deviceIndex, name);
            }

            int pad = size - 4 - 2 - name.Length - 1;
            if (pad > 0)
            {
                Read(commandBuffer, pad);
            }
        }

        private void Request()
        {
            int strIndex = ReadInt();
            int flags = ReadInt();
            float time = ReadFloat();
            float score = ReadFloat();
            String context = scene.StringPool.GetString(ReadFixedString(16));

            lock (scene.LockObj)
            {
                scene.AddRequest(strIndex, flags, time, score, context);
            }
        }

        private void Unrequest()
        {
            int strIndex = ReadInt();
            int strIndexNeedingMemory = ReadInt();
            float score = ReadFloat();
            String context = scene.StringPool.GetString(ReadFixedString(16));

            lock (scene.LockObj)
            {
                scene.AddUnrequest(strIndex, context, strIndexNeedingMemory, score);
            }
        }

        private void SetStatus()
        {
            int strIndex = ReadInt();
            int status = ReadInt();

            lock (scene.LockObj)
            {
                scene.AddStatusChange(strIndex, status);
            }
        }

        private void IssueRequest()
        {
            int strIndex = ReadInt();
            int deviceIndex = ReadInt();
            int handle = ReadInt();
            float time = ReadFloat();
            int lsn = ReadInt();
            int flags = ReadInt();

            lock (scene.LockObj)
            {
                scene.AddIssueRequest(time, strIndex, deviceIndex, handle, lsn, flags);
            }
        }

        private void OpenRawFile()
        {
            int handle = ReadInt();
            float time = ReadFloat();
            String name = scene.StringPool.GetString(ReadFixedString(256));

            lock (scene.LockObj)
            {
                scene.OpenRawFile(time, handle, name);
            }
        }

        private void ChangeGameState()
        {
            int addFlags = ReadInt();
            int remFlags = ReadInt();
        }

        private void RegisterStateFlag()
        {
            int flagMask = ReadInt();
            String name = ReadFixedString(32);
        }

        private void AddCullbox()
        {
            ReadInt();      // Length
            String name = ReadFixedString(32);
            Box box = ReadBox();
            int count = ReadInt();

            List<int> containers = new List<int>();

            for (int x = 0; x < count; x++)
            {
                containers.Add(ReadInt());
            }

            scene.AddCullbox(name, box, containers);
        }


        private void ProcessRequest()
        {
            int deviceIndex = ReadInt();
            int handle = ReadInt();
            float time = ReadFloat();

            lock (scene.LockObj)
            {
                scene.AddProcessRequest(time, deviceIndex, handle);
            }
        }

        private void FinishRequest()
        {
            int deviceIndex = ReadInt();
            int handle = ReadInt();
            float time = ReadFloat();

            lock (scene.LockObj)
            {
                scene.AddFinishRequest(time, deviceIndex, handle);
            }
        }

        private void CancelRequest()
        {
            int strIndex = ReadInt();
            float time = ReadFloat();

            lock (scene.LockObj)
            {
                scene.AddCancelRequest(time, strIndex);
            }
        }

        private void BeginNewFrame()
        {
            float time = ReadFloat();
            Vector camPos = ReadVector();
            Vector camDir = ReadVector();
            Vector camUp = ReadVector();
            Vector streamCamPos = ReadVector();
            Vector streamCamDir = ReadVector();
            float masterCutoff = ReadFloat();
            int flags = ReadInt();
            float fovH = ReadFloat();
            float fovV = ReadFloat();
            float farClip = ReadFloat();

            lock (scene.LockObj)
            {
                Frame frame = scene.BeginNewFrame();
                frame.CamPos = camPos;
                frame.CamDir = camDir;
                frame.CamUp = camUp;
                frame.StreamCamPos = streamCamPos;
                frame.StreamCamDir = streamCamDir;
                frame.MasterCutoff = masterCutoff;
                frame.Flags = flags;
                frame.FovH = fovH;
                frame.FovV = fovV;
                frame.FarClip = farClip;
                frame.Time = time;

                scene.MarkNewFrame(scene.FrameCount - 1);
            }
        }

        private void AddStreamingVolume()
        {
            StreamingVolume volume = new StreamingVolume();

            int slot = ReadInt();
            int type = ReadInt();
            volume.StreamVolPos = ReadVector();
            volume.StreamVolDir = ReadVector();
            volume.Radius = ReadFloat();
            volume.AssetFlags = (uint)ReadInt();
            volume.Type = (StreamingVolume.VolumeType)type;

            lock (scene.LockObj)
            {
                Frame frame = scene.GetPresentFrame();

                lock (frame)
                {
                    if (frame.StreamingVolumes == null)
                    {
                        frame.StreamingVolumes = new List<StreamingVolume>();
                    }

                    frame.StreamingVolumes.Add(volume);
                }
            }
        }

        private void LowLevelOpen()
        {
            int size = ReadInt() << 2;
            long handle = ReadLong();
            float time = ReadFloat();
            ReadInt();
            String rawString = ReadString(); 
            String filename = scene.StringPool.GetString(rawString);

            int pad = 258 - (rawString.Length + 1);
            pad &= 3;
            if (pad > 0)
            {
                Read(commandBuffer, pad);
            }

            lock (scene.LockObj)
            {
                scene.LowLevelOpen(handle, time, filename);
            }
        }

        private void LowLevelClose()
        {
            long handle = ReadLong();
            float time = ReadFloat();
            ReadInt();

            lock (scene.LockObj)
            {
                scene.LowLevelClose(handle, time);
            }
        }

        private void LowLevelRead()
        {
            long handle = ReadLong();
            float time = ReadFloat();
            int size = ReadInt();
            long offset = ReadLong();
            int streamerHandle = ReadInt();
            int queue = ReadInt();

            lock (scene.LockObj)
            {
                scene.LowLevelRead(handle, time, size, streamerHandle, queue, offset);
            }
        }

        private void LowLevelReadEnd()
        {
            long handle = ReadLong();
            float time = ReadFloat();
            int streamerHandle = ReadInt();
            int queue = ReadInt();
            ReadInt();

            lock (scene.LockObj)
            {
                scene.LowLevelReadEnd(handle, time, streamerHandle, queue);
            }
        }

        private void LowLevelSeek()
        {
            long handle = ReadLong();
            float time = ReadFloat();
            int streamerHandle = ReadInt();
            long offset = ReadLong();
            int queue = ReadInt();
            ReadInt();

            lock (scene.LockObj)
            {
                scene.LowLevelSeek(handle, time, offset, streamerHandle, queue);
            }
        }

        private void LowLevelWrite()
        {
            long handle = ReadLong();
            float time = ReadFloat();
            int size = ReadInt();
            int streamerHandle = ReadInt();
            int queue = ReadInt();

            lock (scene.LockObj)
            {
                scene.LowLevelWrite(handle, time, size, streamerHandle, queue);
            }
        }

        private void DeviceIdle()
        {
            int queue = ReadInt();
            float time = ReadFloat();

            lock (scene.LockObj)
            {
                scene.DeviceIdle(time, queue);
            }
        }

        private void AddMarker()
        {
            int flags = ReadInt();

            lock (scene.LockObj)
            {
                scene.AddMarker(flags);
            }
//            int frameNumber = ReadInt();
//            scene.AddMarker(frameNumber);


//            scene.controller.SetToFrame(frameNumber);
        }

        private void AddEntity()
        {
            int entityId = ReadInt();
            int strIndex = ReadInt();
            //            Vector pos = ReadVector();
            Vector boxMin = ReadVector();
            Vector boxMax = ReadVector();

            lock (scene.LockObj)
            {
//                Streamable streamable = scene.GetOrCreateStreamable(strIndex);

                // TODO: Add lod type, or remove this function.
                //scene.AddEntity(entityId, streamable, boxMin, boxMax, 0);
            }
        }

        private void UpdateEntity()
        {
            int entityId = ReadInt();
            uint indexLodEntityType = (uint) ReadInt();
            int entityFlags = ReadInt();
            Vector16 boxMin = ReadVector16();
            Vector16 boxMax = ReadVector16();

            int strIndex = (int)indexLodEntityType & 0xffffff;
            int lodType = (int)(indexLodEntityType >> 28) & 0xf;
            int entityType = (int)(indexLodEntityType >> 24) & 0xf;

            Vector16 sphereCenter = ReadVector16();
            ReadShort();
            float sphereRadius = ReadFloat();

            float lodDistance = ReadFloat();
            int parentLodEntityId = ReadInt();

            lock (scene.LockObj)
            {
                Streamable streamable = scene.GetOrCreateStreamable(strIndex);

                scene.UpdateEntity(entityId, streamable, lodType, entityFlags, boxMin, boxMax, entityType, sphereCenter, sphereRadius, lodDistance, parentLodEntityId);
            }
        }

        private void DeleteEntity()
        {
            int entityId = ReadInt();

            lock (scene.LockObj)
            {
                scene.DeleteEntity(entityId);
            }
        }

        private void MissingLod()
        {
            int entityId = ReadInt();
            bool parentLodMissing = ReadInt() != 0;

            lock (scene.LockObj)
            {
                scene.MissingLod(entityId, parentLodMissing);
            }
        }

        private void DependencyInfo()
        {
            int size = ReadInt() << 2;
            int strIndex = ReadInt();
            int depCount = ReadInt();

            Streamable streamable = scene.GetOrCreateStreamable(strIndex);

//            streamable.IdentityHistory.PresentState.Dependencies.Clear();
            Streamable.Identity identity = streamable.IdentityHistory.PresentState;

            for (int x = 0; x<depCount; x++)
            {
                Streamable dep = scene.GetOrCreateStreamable(ReadInt());

                if (!identity.Dependencies.Contains(dep))
                {
                    identity.Dependencies.Add(dep);
                    dep.IdentityHistory.PresentState.Dependents.Add(streamable);
                }
            }
        }

        private void SetMarkerText()
        {
            int size = ReadInt() << 2;
            float time = ReadFloat();
            ReadShort();
            String message = ReadString();


            int pad = 4 - ((message.Length + 1) & 3);

            if (pad < 4)
            {
                Read(commandBuffer, pad);
            }

            scene.SetMarkerText(time, message);
        }

        private void AddToNeededList()
        {
            int size = ReadInt() << 2;
            int count = ReadInt();

            for (int x = 0; x < count; x++)
            {
                int entityId = ReadInt();
                float score = ReadFloat();
                //int flags = ReadChar();
                int flags = 0;
                scene.AddToNeededList(entityId, score, flags);
            }
        }

        private void SetExtraClientInfo()
        {
            String version = ReadFixedString(32);
            String assetType = ReadFixedString(32);

            scene.SetExtraClientInfo(version, assetType);
        }

        private void AddVisibleEntities()
        {
            int size = ReadInt() << 2;
            int entityCount = ReadInt();
            int[] entityList = new int[entityCount];

            for (int x = 0; x < entityCount; x++)
            {
                entityList[x] = ReadInt();
            }

//            scene.AddVisibleEntities(entityList);
        }

        private void RemoveVisibleEntities()
        {
            int size = ReadInt() << 2;
            int entityCount = ReadInt();
            int[] entityList = new int[entityCount];

            for (int x = 0; x < entityCount; x++)
            {
                entityList[x] = ReadInt();
            }

          //  scene.RemoveVisibleEntities(entityList);
        }

        private void UnregisterObject()
        {
            int index = ReadInt();
            scene.UnregisterObject(index);
        }

        private void RegisterMapData()
        {
            int index = ReadInt();
            int mapDataId = ReadInt();
            int parentStrIndex = ReadInt();
            Box physicsBox = ReadBox();
            Box streamingBox = ReadBox();

            scene.RegisterMapData(index, mapDataId, physicsBox, streamingBox, parentStrIndex);
        }

        private void InstantiateMapData()
        {
            //int index = ReadInt();
            int mapDataId = ReadInt();
            scene.InstantiateMapData(mapDataId);
        }

        private void AddMapdataEntity()
        {
            int index = ReadInt();
            int guid = ReadInt();
            int entity = ReadInt();
            int entityStrIndexLodType = ReadInt();
            int entityStrIndex = entityStrIndexLodType & 0xffffff;
            int lodTypeId = (entityStrIndexLodType >> 28) & 0xf;
            int entityTypeId = (entityStrIndexLodType >> 24) & 0xf;
            int entityFlags = ReadInt();
            Box16 entityBox = ReadBox16();

            Vector16 sphereCenter = ReadVector16();
            ReadShort();
            float sphereRadius = ReadFloat();

            float lodDistance = ReadFloat();
            int parentLodEntityId = ReadInt();

            scene.AddMapDataEntity(index, entity, entityStrIndex, entityBox, lodTypeId, guid, entityTypeId, entityFlags, sphereCenter, sphereRadius, lodDistance, parentLodEntityId);
        }


        private void RegisterEntityType()
        {
            int entityType = ReadInt();
            int color = ReadInt();
            String name = scene.StringPool.GetString(ReadFixedString(16));
            int visible = ReadInt();

            lock (scene.LockObj)
            {
                scene.RegisterEntityType(entityType, name, color, visible != 0);
            }
        }

        private void RegisterLodType()
        {
            int lodType = ReadInt();
            String name = ReadFixedString(16);

            lock (scene.LockObj)
            {
                scene.RegisterLodType(lodType, name);
            }
        }

        private void StreamerRead()
        {
            int size = ReadInt() << 2;
            int deviceIndex = ReadInt();
            int handle = ReadInt();
            float time = ReadFloat();
            int lsn = ReadInt();
            String rawString = ReadString();
            String filename = scene.StringPool.GetString(rawString);

            int pad = 258 - (rawString.Length + 1);
            pad &= 3;
            if (pad > 0)
            {
                Read(commandBuffer, pad);
            }

            lock (scene.LockObj)
            {
                scene.AddStreamerRead(time, deviceIndex, handle, filename, lsn);
            }
        }

        private void StreamerDecompress()
        {
            int deviceIndex = ReadInt();
            int handle = ReadInt();
            float time = ReadFloat();
            int starting = ReadInt();

            lock (scene.LockObj)
            {
                scene.AddStreamerDecompress(time, deviceIndex, handle, starting != 0);
            }
        }

        private void UpdateVisFlags()
        {
            int size = ReadInt() << 2;
            int count = ReadInt();

            lock (scene.LockObj)
            {
                for (int x = 0; x < count; x++)
                {
                    int entityId = ReadInt();
                    int phaseFlags = ReadInt();
                    int optionFlags = ReadInt();
                    scene.UpdateVisFlags(entityId, phaseFlags, optionFlags);
                }
            }
        }

        private void RegisterVisFlags()
        {
            int flag = ReadInt();
            string name = ReadFixedString(16);

            lock (scene.LockObj)
            {
                scene.RegisterVisFlag(flag, name);
            }
        }

        private void SetLodParent()
        {
            int entity = ReadInt();
            int parentLod = ReadInt();

            lock (scene.LockObj)
            {
                scene.SetLodParent(entity, parentLod);
            }
        }

        private void RegisterNamedFlag()
        {
            int flagType = ReadInt();
            int flagValue = ReadInt();
            string name = ReadFixedString(16);

            lock (scene.LockObj)
            {
                scene.RegisterNamedFlag(flagType, flagValue, name);
            }
        }

        private void AddBoxSearch()
        {
            int searchTypeId = ReadInt();
            Box16 box = ReadBox16();

            lock (scene.LockObj)
            {
                scene.AddBoxSearch(searchTypeId, box);
            }
        }

        private void SetFlags()
        {
            int strIndex = ReadInt();
            int flags = ReadInt();
            String context = scene.StringPool.GetString(ReadFixedString(16));

            lock (scene.LockObj)
            {
                scene.SetFlags(strIndex, flags, context);
            }
        }

        private void FailedAllocation()
        {
            int strIndex = ReadInt();
            int reason = ReadInt();

            lock (scene.LockObj)
            {
                Streamable streamable = scene.GetOrCreateStreamable(strIndex);

                scene.FailedAllocation(streamable, reason);
            }
        }

        private void DumpNetStats()
        {
            Console.WriteLine("Total data received: " + dataReceived);

            float dataReceivedf = (float)dataReceived;

            if (scene != null && scene.MaxFrame > 0)
            {
                float dataPerFrame = dataReceivedf / (float)scene.MaxFrame;
                Console.WriteLine("Avg data per frame: " + dataPerFrame);
            }

            for (int x = 0; x < commandCount.Length; x++)
            {
                if (commandCount[x] != 0)
                {
                    float dataPct = (float)commandSize[x] / dataReceivedf;
                    float avgPayload = (float)commandSize[x] / (float)commandCount[x];

                    Console.WriteLine("Command {0,3}: {1,5} calls, {2,8} bytes, avg payload {3,6:F1} ({4:P})", x, commandCount[x], commandSize[x], avgPayload, dataPct);
                }
            }
        }
    }
}
