﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Diagnostics;

namespace StreamingVisualize.Model
{
    // An instance of this class contains all the events that happened in one specific frame.
    public class Frame : IVersionedSerializable
    {
        // This frame has a user marker.
        public const int MARKER = (1 << 0);

        // There were OOMs due to fragmented memory.
        public const int OOM_FRAGMENTED = (1 << 1);

        // There were OOMs due to insufficient memory.
        public const int OOM_NO_MEMORY = (1 << 2);

        // There were OOMs due to churn protection.
        public const int OOM_CHURN_PROTECTION = (1 << 3);

        // There was at least one priority request.
        public const int HAS_PRIORITY = (1 << 4);

        // An assert occurred.
        public const int ASSERT_OCCURRED = (1 << 5);

        // The game blocked because crucial assets were missing
        public const int BLOCKED_FOR_STREAMING = (1 << 6);

		// A bug has been reported
        public const int BUG_REPORTED = (1 << 7);

        // An entity is using a lower LOD than it should.
        public const int LOW_LOD = (1 << 8);

        // An entity is not rendered entirely because both its LOD and its parent LOD are not resident.
        public const int MISSING_LOD = (1 << 9);

        // An iteration in an automated test has begun.
        public const int BEGIN_TEST = (1 << 10);

		// An iteration in an automated test has ended.
		public const int END_TEST = (1 << 11);

		// The configuration in an automated test has changed.
        public const int TEST_NEW_CONFIG = (1 << 12);

        // Used in serialization only: If set, there's a marker text for this frame.
        public const int HAS_MARKER_TEXT = (1 << 30);


        // Max # of streaming volumes
        public const int STREAMING_VOLUMES = 2;

        public List<Request> requests = new List<Request>();
        public List<Unrequest> unrequests = new List<Unrequest>();
        //public List<StatusChange> statusChanges = new List<StatusChange>();
        public List<FailedAlloc> FailedAllocs = new List<FailedAlloc>();

        public List<Entity> BadLodEntities;

        public NeededList NeededList = new NeededList();


        public Vector CamPos = new Vector();
        public Vector CamDir = new Vector();
        public Vector CamUp = new Vector();
        public float FovV = 1.0f;
        public float FovH = 1.0f;
        public float FarClip = 500.0f;

        public Vector StreamCamPos = new Vector();
        public Vector StreamCamDir = new Vector();

        public List<StreamingVolume> StreamingVolumes;
        public List<BoxSearch> BoxSearches = new List<BoxSearch>();

        public float MasterCutoff { get; set; }
        public int FreeVirtMemKB { get; set; }
        public int FreePhysMemKB { get; set; }
        public int Flags { get; set; }
        public float Time { get; set; }             // Wallclock time

        public short Reqs { get; set; }             // Number of resources currently in LOADREQUESTED or LOADING state
        public short RealReqs { get; set; }             // Number of non-dummy resources currently in LOADREQUESTED or LOADING state

        public String MarkerText { get; set; }

        public bool OOMNomemoryThisFrame { get { return (Flags & OOM_NO_MEMORY) != 0; } }
        public bool HasMarker { get { return (Flags & MARKER) != 0; } }

//        public bool MemFreeThisFrame { get; set; }
//        public bool HasMarker { get; set; }

        public void AddRequest(Streamable streamable, int frame, int flags, float score, String context)
        {
            Request request = new Request();
            request.Streamable = streamable;
            request.Frame = frame;
            request.Flags = flags;
            request.Score = score;
            request.Context = context;

            if ((flags & Request.PRIORITY_LOAD) != 0)
            {
                Flags |= HAS_PRIORITY;
            }

            lock (this)
            {
                requests.Add(request);
            }
        }

        public void AddUnrequest(Streamable streamable, int frame, String context)
        {
            Unrequest unrequest = new Unrequest();
            unrequest.Streamable = streamable;
            unrequest.Frame = frame;
            unrequest.Context = context;

            unrequests.Add(unrequest);
        }

        public void AddLowLodEntity(Entity entity)
        {
            if (BadLodEntities == null)
            {
                BadLodEntities = new List<Entity>();
            }

            BadLodEntities.Add(entity);
        }

        public void AddMissingLodEntity(Entity entity)
        {
            if (BadLodEntities == null)
            {
                BadLodEntities = new List<Entity>();
            }

            BadLodEntities.Add(entity);
        }

        public String GetMultilineMarkerText()
        {
            String[] lines = MarkerText.Split(new string[] { " / "}, StringSplitOptions.None );
            StringBuilder result = new StringBuilder(2048);

            foreach (String line in lines)
            {
                result.Append(line);
                result.Append("\n");
            }

            return result.ToString();
        }



        /*
        public void AddStatusChange(Streamable streamable, int frame, int newStatus)
        {
            StatusChange change = new StatusChange();
            change.Streamable = streamable;
            change.Frame = frame;
            change.NewStatus = newStatus;

            statusChanges.Add(change);
        }
        */
        public void Serialize(VersionedWriter writer)
        {
            writer.Write(CamPos);
            writer.Write(CamDir);

            // Version 32: Wallclock time.
            writer.Write(Time);

            // Version 22: CamUp
            writer.Write(CamUp);

            // Version 29: Stream camera pos/dir
            writer.Write(StreamCamPos);
            writer.Write(StreamCamDir);

            writer.Write(requests);
            writer.Write(unrequests);
            writer.Write(FailedAllocs);

            writer.Write(MasterCutoff);
            writer.Write(FreeVirtMemKB);
            writer.Write(FreePhysMemKB);
            writer.Write(Flags | ((MarkerText != null) ? HAS_MARKER_TEXT : 0));

            // Version 15: Marker text.
            if (MarkerText != null)
            {
                writer.Write(MarkerText);
            }

            // Version 17: FOV/far clip.
            writer.Write(FovH);
            writer.Write(FovV);
            writer.Write(FarClip);

            // File version 2:
            // Any streaming volumes?
            int volCount = 0;

            if (StreamingVolumes != null)
            {
                foreach (StreamingVolume vol in StreamingVolumes)
                {
                    if (vol != null)
                    {
                        volCount++;
                    }
                }
            }

            writer.Write((byte)volCount);

            if (StreamingVolumes != null)
            {
                foreach (StreamingVolume vol in StreamingVolumes)
                {
                    //writer.Write((byte) x);
                    vol.Serialize(writer);
                }
            }

            // Version 27: Needed list.
            writer.Write(NeededList);

            // Version 37: Box searches.
            writer.Write(BoxSearches);

            // Version 39: Requests.
            writer.Write(Reqs);

            // Version 40: Real requests.
            writer.Write(RealReqs);
        }

        public void Deserialize(VersionedReader reader, int version)
        {
            CamPos = reader.ReadVector();
            CamDir = reader.ReadVector();

            if (version >= 32)
            {
                Time = reader.ReadFloat();
            }

            if (version >= 22)
            {
                CamUp = reader.ReadVector();
            }

            if (version >= 29)
            {
                StreamCamPos = reader.ReadVector();
                StreamCamDir = reader.ReadVector();
            }

            reader.ReadList(requests);
            reader.ReadList(unrequests);
            reader.ReadList(FailedAllocs);

            MasterCutoff = reader.ReadFloat();
            FreeVirtMemKB = reader.ReadInt();
            FreePhysMemKB = reader.ReadInt();
            Flags = reader.ReadInt();

            if ((Flags & HAS_MARKER_TEXT) != 0)
            {
                Flags &= ~HAS_MARKER_TEXT;
                MarkerText = reader.ReadString();
            }

            if (version >= 17)
            {
                FovH = reader.ReadFloat();
                FovV = reader.ReadFloat();
                FarClip = reader.ReadFloat();
            }


            // File format 2 and up supports streaming volumes.
            if (version > 1)
            {
                int volCount = reader.ReadByte();

                if (volCount > 0)
                {
                    StreamingVolumes = new List<StreamingVolume>();

                    for (int x = 0; x < volCount; x++)
                    {
                        if (version < 21)
                        {
                            reader.ReadByte();
                        }

                        StreamingVolume volume = new StreamingVolume();

                        volume.Deserialize(reader, version);
                        StreamingVolumes.Add(volume);
                    }
                }
            }

            // Older file formats don't store the priority flag.
            if (version < 4)
            {
                foreach (Request req in requests)
                {
                    if ((req.Flags & Request.PRIORITY_LOAD) != 0)
                    {
                        Flags |= HAS_PRIORITY;
                        break;
                    }
                }
            }

            if (version >= 27)
            {
                reader.Read(NeededList);
            }

            if (version >= 37)
            {
                reader.ReadList<BoxSearch>(BoxSearches);
            }

            if (version >= 39)
            {
                Reqs = reader.ReadShort();

                if (version == 39)
                {
                    Reqs = 0;           // Reset - we'll recalculate them.
                }
            }

            if (version >= 40)
            {
                RealReqs = reader.ReadShort();
            }
        }

        public void DeleteData()
        {
            requests.Clear();
            unrequests.Clear();
            FailedAllocs.Clear();
            BadLodEntities = null;
            NeededList = new NeededList();
            StreamingVolumes = null;
            BoxSearches.Clear();
        }
    }
}
