﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StreamingVisualize.View.Components;
using StreamingVisualize.Model;
using StreamingVisualize.Controller;
using System.Diagnostics;

namespace StreamingVisualize.View
{
    public partial class FrameOverview : UserControl, IViewController
    {
        private class FlagChecker
        {
            public FlagChecker(int flagmask, Color color)
            {
                this.FlagMask = flagmask;
                this.Brush = new SolidBrush(color);
            }

            public int FlagMask;
            public Brush Brush;
        };

        private FlagChecker[] FlagCheckers = new FlagChecker[] {
            new FlagChecker(Frame.OOM_NO_MEMORY, Color.Red),
            new FlagChecker(Frame.OOM_FRAGMENTED, Color.Yellow),
            new FlagChecker(Frame.OOM_CHURN_PROTECTION, Color.Turquoise),
            new FlagChecker(Frame.MARKER, Color.Purple),
        };


        public Scene Scene { get; set; }
        public MainController Controller { get; set; }

        public int CurrentFrameNumber { get; set; }

        private SimpleTooltip tooltip = new SimpleTooltip();

        // Width of each frame in pixels
        private int frameWidth = 8;

        // Height of a flag indicator in pixels
        private float flagHeight = 4.0f;

        // Total offset in X in pixels
        private int offsetX = 0;

        //private bool follow = false;
        private bool isDragging;
        private int lastDragX;
        private int dragStartX;
        private bool TooltipOnCurrentFrame;

        private int lastMouseX;

        private Brush requestsBrush = new SolidBrush(Color.Green);
        private Brush selectedFrameBrush = new SolidBrush(Color.White);


        public FrameOverview()
        {
            InitializeComponent();
        }

        private void UpdateTooltip()
        {
            // What frame are we over?
            int frameNumber = (TooltipOnCurrentFrame) ? CurrentFrameNumber : ((lastMouseX + offsetX) / frameWidth);

            if (frameNumber >= Scene.MaxFrame)
            {
                tooltip.ResetToolTip();
            }
            else
            {
                String tooltipText = "Frame " + frameNumber + "/" + Scene.MaxFrame;
                tooltip.SetToolTip(tooltipText);
            }
        }

        private void FrameOverview_MouseMove(object sender, MouseEventArgs e)
        {
            // Drag, if possible.
            if (isDragging)
            {
                int delta = e.X - lastDragX;
                offsetX = Math.Max(offsetX - delta, 0);

                // Limit it so we don't go past the last frame.
                int maxX = Math.Max(Scene.MaxFrame * frameWidth - Width, 0);
                offsetX = Math.Min(offsetX, maxX);

                lastDragX = e.X;
            }

            lastMouseX = e.X;
            TooltipOnCurrentFrame = false;
            UpdateTooltip();

            tooltip.MouseMove(e);
            Refresh();
        }

        private void FrameOverview_MouseDown(object sender, MouseEventArgs e)
        {
            isDragging = true;
            lastDragX = e.X;
            dragStartX = e.X;
        }

        private void FrameOverview_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            if (Scene == null)
            {
                return;
            }

            // Draw all the frames.
            // Get the left-most one.
            int frameNumber = offsetX / frameWidth;
            float xpos = (float)(frameNumber * frameWidth - offsetX);

            float barWidthf = (float)Math.Max(frameWidth - 1, 1);

            int maxFrameCount = Scene.MaxFrame;
            int baseYPos = Height - 20;

            while (xpos < Width && frameNumber < maxFrameCount)
            {
                // Highlight the currently selected frame.
                if (CurrentFrameNumber == frameNumber)
                {
                    g.FillRectangle(selectedFrameBrush, (float)xpos, 0.0f, (float) frameWidth, (float)Height);
                }

                Frame frame = Scene.GetFrame(frameNumber);
                // Number of requests.

                float requestCountf = (float)frame.requests.Count;
                g.FillRectangle(requestsBrush, (float)xpos, baseYPos - requestCountf, barWidthf, requestCountf);

                // Now the flags. Let's do an early out first.
                int flags = frame.Flags;
                float yPos = (float)Height - 2.0f - flagHeight;

                if (flags != 0)
                {
                    foreach (FlagChecker flagChecker in FlagCheckers)
                    {
                        if ((flags & flagChecker.FlagMask) != 0)
                        {
                            g.FillRectangle(flagChecker.Brush, (float)xpos, yPos, barWidthf, flagHeight);
                        }

                        yPos -= flagHeight;
                    }
                }

                frameNumber++;
                xpos += (float)frameWidth;
           }

            tooltip.Draw(g, DefaultFont);
        }

        private void FrameOverview_MouseUp(object sender, MouseEventArgs e)
        {
            isDragging = false;

            // What frame are we over?
            int frameNumber = (e.X + offsetX) / frameWidth;

            if (frameNumber < Scene.MaxFrame && frameNumber >= 0)
            {
                Controller.SetToFrame(frameNumber);
            }
        }

        public void RefreshDeferred()
        {
            BeginInvoke((Action)delegate { Refresh(); });
        }

        // Switch the display to focus on a certain frame
        public void SetToFrame(int frameNumber)
        {
            CurrentFrameNumber = frameNumber;
            RefreshDeferred();
        }

        protected override void OnMouseWheel(MouseEventArgs e)
        {
            if (e.Delta != 0)
            {
                // Keep the selected item at the same place.
                int selectedX = CurrentFrameNumber * frameWidth - offsetX;

                frameWidth += (e.Delta > 0) ? 1 : -1;

                frameWidth = Math.Max(frameWidth, 1);

                offsetX = CurrentFrameNumber * frameWidth - selectedX;
                offsetX = Math.Max(offsetX, 0);

                Refresh();
            }

            base.OnMouseWheel(e);
        }

        // If a new frame was added
        public void OnFrameChanged()
        {
            RefreshDeferred();
        }

        // A new device was registered, or a device has changed.
        public void OnDeviceChanged(int deviceIndex, Device device) { }

        // A streamable has been selected
        public void OnSelectStreamable(Streamable streamable) { }

        // The client information has been updated
        public void ClientInfoUpdated() { }

        // We're in follow mode, and a new frame has been added.
        public void OnNewFrame(int frameNumber)
        {
            EnsureFrameVisible(frameNumber);

            UpdateTooltip();

            RefreshDeferred();
        }

        public void EnsureFrameVisible(int frameNumber)
        {
            // Make sure the new frame is still visible.
            int mustBeVisibleX = (frameNumber + 1) * frameWidth;

            offsetX = Math.Max(offsetX, mustBeVisibleX - Width);
            offsetX = Math.Min(offsetX, mustBeVisibleX);
        }

        // A new entity type was registered
        public void OnNewEntityType(int entityTypeId, EntityType type) { }

        private void FrameOverview_MouseEnter(object sender, EventArgs e)
        {
            this.Focus();
        }

        protected override bool IsInputKey(Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Up:
                case Keys.Down:
                case Keys.Left:
                case Keys.Right:
                    return true;
            }
            return base.IsInputKey(keyData);
        }

        private void SelectNewFrame()
        {
            TooltipOnCurrentFrame = false;
            EnsureFrameVisible(CurrentFrameNumber);
            UpdateTooltip();
            Controller.SetToFrame(CurrentFrameNumber);
        }

        private void FrameOverview_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Left:
                    if (CurrentFrameNumber > 0)
                    {
                        CurrentFrameNumber--;
                        SelectNewFrame();
                    }
                    break;

                case Keys.Right:
                    if (Scene != null && CurrentFrameNumber < Scene.MaxFrame - 1)
                    {
                        CurrentFrameNumber++;
                        SelectNewFrame();
                    }
                    break;

                case Keys.F:
                    if (Scene != null)
                    {
                        Scene.FollowMode = true;
                    }
                    break;
            }
        }

        // The connection has been terminated
        public void OnConnectionLost() { }
    }
}
