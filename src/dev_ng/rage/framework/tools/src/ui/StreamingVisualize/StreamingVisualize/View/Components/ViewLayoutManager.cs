﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StreamingVisualize.Controller;
using System.Xml.Serialization;
using System.IO;
using System.Diagnostics;
using StreamingVisualize.Model;

namespace StreamingVisualize.View.Components
{
    public class ViewLayoutManager
    {
        [Serializable]
        [System.Xml.Serialization.XmlInclude(typeof(ViewInstance))]
        [System.Xml.Serialization.XmlInclude(typeof(Splitter))]
        public abstract class LayoutComponent
        {
            public LayoutComponent() { }

            public List<LayoutComponent> Children = new List<LayoutComponent>();

            [XmlIgnoreAttribute]
            public Control control;

            [XmlIgnoreAttribute]
            public LayoutComponent Parent;



            abstract protected Control CreateView(ViewLayoutManager manager, MainController controller, Scene scene);
            abstract public void EndCreateView(ViewLayoutManager manager, MainController controller, Scene scene);

            public void DoHierarchical(ViewLayoutManager manager, Action<LayoutComponent> action)
            {
                action(this);

                foreach (LayoutComponent child in Children)
                {
                    child.DoHierarchical(manager, action);
                }
            }

            public void AddChild(LayoutComponent child)
            {
                child.Parent = this;
                Children.Add(child);
            }

            public void DoHierarchicalTail(ViewLayoutManager manager, Action<LayoutComponent> action)
            {
                foreach (LayoutComponent child in Children)
                {
                    child.DoHierarchicalTail(manager, action);
                }

                action(this);
            }

            protected virtual void AddChild(int index, Control child)
            {
                control.Controls.Add(child);
            }

            public void AddChildren()
            {
                for (int x = 0; x < Children.Count; x++)
                {
                    AddChild(x, Children[x].control);
                    Children[x].AddChildren();
                }
            }

            public void AddSingleChildControl(LayoutComponent component)
            {
                for (int x = 0; x < Children.Count; x++)
                {
                    if (Children[x] == component)
                    {
                        AddChild(x, Children[x].control);
                        break;
                    }
                }
            }

            public void Instantiate(ViewLayoutManager manager, MainController controller, Scene scene)
            {
                control = CreateView(manager, controller, scene);
            }
        }

        [Serializable]
        public class ViewInstance : LayoutComponent
        {
            public ViewInstance() { }

            public int ViewType;

            protected override Control CreateView(ViewLayoutManager manager, MainController controller, Scene scene)
            {
                ViewDefinition definition = ViewDefinition.GetDefinition(ViewType);
                Control control = definition.CreateViewCb();

                control.Dock = System.Windows.Forms.DockStyle.Fill;
                control.Location = new System.Drawing.Point(0, 0);
                //control.AutoScroll = true;
                control.AutoSize = true;
                //control.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;

                IViewController viewController = (IViewController)control;

                viewController.Register(controller, scene);
                controller.RegisterView(viewController);

                manager.ControlToComponentMap[control] = this;
                manager.ControllerToComponentMap[viewController] = this;

                return control;
            }

            public override void EndCreateView(ViewLayoutManager manager, MainController controller, Scene scene) { }
        }

        [Serializable]
        public class Splitter : LayoutComponent
        {
            public enum OrientationType
            {
                HORIZONTAL,
                VERTICAL,
            }

            // 0.0 to 1.0 of the width or height of the main frame
            public float SplitFactor;

            public OrientationType Orientation;

            protected override Control CreateView(ViewLayoutManager manager, MainController controller, Scene scene)
            {
                SplitContainer splitter = new SplitContainer();
                /*splitter.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                | System.Windows.Forms.AnchorStyles.Left)
                | System.Windows.Forms.AnchorStyles.Right)));*/
                splitter.Dock = System.Windows.Forms.DockStyle.Fill;
                splitter.Location = new System.Drawing.Point(-2, 0);
                splitter.Orientation = (Orientation == OrientationType.HORIZONTAL) ? System.Windows.Forms.Orientation.Horizontal : System.Windows.Forms.Orientation.Vertical;
                splitter.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;

                float length = (float) ((Orientation == OrientationType.HORIZONTAL) ? splitter.Height : splitter.Width);
                splitter.SplitterDistance = (int)(length * SplitFactor);

                splitter.BeginInit();
                splitter.SuspendLayout();

                return splitter;
            }

            public override void EndCreateView(ViewLayoutManager manager, MainController controller, Scene scene)
            {
                SplitContainer splitter = (SplitContainer) control;
                splitter.EndInit();
                splitter.ResumeLayout(false);
                splitter.PerformLayout();
            }

            protected override void AddChild(int index, Control child)
            {
                SplitContainer container = (SplitContainer)control;

                if (index == 0)
                {
                    container.Panel1.Controls.Add(child);
                }
                else
                {
                    container.Panel2.Controls.Add(child);
                }
            }
        }


        LayoutComponent Root;

        Dictionary<Control, LayoutComponent> ControlToComponentMap = new Dictionary<Control, LayoutComponent>();
        Dictionary<IViewController, LayoutComponent> ControllerToComponentMap = new Dictionary<IViewController, LayoutComponent>();


        public void UnregisterAll(MainController controller)
        {
            foreach (IViewController view in ControllerToComponentMap.Keys)
            {
                controller.UnregisterView(view);
            }
        }

        public void CreateDefaultLayout()
        {
            Root = new Splitter();
            ((Splitter) Root).Orientation = Splitter.OrientationType.HORIZONTAL;
            ((Splitter) Root).SplitFactor = 0.2f;

            ViewInstance frameOverview = new ViewInstance();
            frameOverview.ViewType = ViewDefinition.TYPE_FRAME_OVERVIEW;

            Splitter splitterMain = new Splitter();
            splitterMain.Orientation = Splitter.OrientationType.VERTICAL;
            splitterMain.SplitFactor = 0.5f;

            Splitter splitterLeft = new Splitter();
            splitterLeft.Orientation = Splitter.OrientationType.HORIZONTAL;
            splitterLeft.SplitFactor = 0.5f;

            Splitter splitterRight = new Splitter();
            splitterRight.Orientation = Splitter.OrientationType.HORIZONTAL;
            splitterRight.SplitFactor = 0.5f;

            Splitter splitterDeviceState = new Splitter();
            splitterDeviceState.Orientation = Splitter.OrientationType.VERTICAL;
            splitterDeviceState.SplitFactor = 0.5f;


            ViewInstance frameView = new ViewInstance();
            frameView.ViewType = ViewDefinition.TYPE_FRAME_DETAILS;

            ViewInstance entityView = new ViewInstance();
            entityView.ViewType = ViewDefinition.TYPE_ENTITY;

            ViewInstance resourceView = new ViewInstance();
            resourceView.ViewType = ViewDefinition.TYPE_RESOURCE;

            ViewInstance deviceStateView0 = new ViewInstance();
            deviceStateView0.ViewType = ViewDefinition.TYPE_DEVICE_FRAME_0;

            ViewInstance deviceStateView1 = new ViewInstance();
            deviceStateView1.ViewType = ViewDefinition.TYPE_DEVICE_FRAME_1;

            Root.AddChild(frameOverview);
            Root.AddChild(splitterMain);

            splitterMain.AddChild(splitterLeft);
            splitterMain.AddChild(splitterRight);

            splitterLeft.AddChild(frameView);
            splitterLeft.AddChild(entityView);

            splitterRight.AddChild(resourceView);
            splitterRight.AddChild(splitterDeviceState);

            splitterDeviceState.AddChild(deviceStateView0);
            splitterDeviceState.AddChild(deviceStateView1);

            TextWriter writer = new StreamWriter("c:\\svlayout.xml");
            XmlSerializer serializer = new XmlSerializer(typeof(LayoutComponent));
            serializer.Serialize(writer, Root);
            writer.Close();
        }

        public void CreateHierarchy(Control container, Scene scene, MainController controller)
        {
            //Root.CreateViewHierarchical(this, container);
            Root.DoHierarchical(this, (LayoutComponent c) => { c.Instantiate(this, controller, scene); });
            Root.AddChildren();
            container.Controls.Add(Root.control);
            Root.DoHierarchical(this, (LayoutComponent c) => { c.EndCreateView(this, controller, scene); });
        }

        public void ChangeView(MainController controller, IViewController viewController, int viewType)
        {
            LayoutComponent comp = ControllerToComponentMap[viewController];

            controller.UnregisterView(viewController);

            Control parent = comp.control.Parent;
            parent.Controls.Remove(comp.control);
            comp.control.Dispose();

            ControlToComponentMap.Remove(comp.control);
            ControllerToComponentMap.Remove(viewController);

            ((ViewInstance) comp).ViewType = viewType;
            comp.Instantiate(this, controller, controller.Scene);
            comp.Parent.AddSingleChildControl(comp);
            comp.EndCreateView(this, controller, controller.Scene);

            ControlToComponentMap[comp.control] = comp;

            viewController = (IViewController)comp.control;   // TODO: We could add an accessor in IViewController for the control instead
            ControllerToComponentMap[viewController] = comp;
        }
    }
}
