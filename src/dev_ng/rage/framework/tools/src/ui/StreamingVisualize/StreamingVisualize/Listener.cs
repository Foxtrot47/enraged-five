﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using StreamingVisualize.Model;

namespace StreamingVisualize
{
    class Listener
    {
        public delegate void OnConnection(Scene scene, Communication communication);
        public delegate void OnError(String errorMsg);

        public OnConnection OnConnectionCb { get; set; }
        public OnError OnErrorCb { get; set; }

        private Socket listener;


        volatile bool shouldQuit = false;

        // Spawns a new thread that will begin listening for connections and respond to them.
        public void Start()
        {
            Thread thread = new Thread(new ThreadStart(Worker));
            thread.Name = "Connection Listener";
            thread.Start();
        }

        private void Worker()
        {
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            IPAddress ipAddress = null;

            for (int x = 0; x < ipHostInfo.AddressList.Length; x++)
            {
                // Pick the IPv4 one, 360 can't handle IPv6.
                if (ipHostInfo.AddressList[x].AddressFamily == AddressFamily.InterNetwork)
                {
                    ipAddress = ipHostInfo.AddressList[x];
                    break;
                }
            }

            if (ipAddress == null)
            {
                Console.WriteLine("Could not get IPv4 address of the local workstation - consoles won't be able to connect.");
                ipAddress = ipHostInfo.AddressList[0];
            }

            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, 8799);
            listener = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            try
            {
                Console.WriteLine("Waiting for incoming connection...");
                listener.Bind(localEndPoint);
                listener.Listen(10);

                while (!shouldQuit)
                {
                    Socket handler = listener.Accept();
                    Console.WriteLine("Received connection attempt - beginning handshake");

                    Scene scene = new Scene();
                    Communication comm = new Communication(handler, scene);

                    if (OnConnectionCb != null)
                    {
                        OnConnectionCb(scene, comm);
                    }

                    comm.Start();
                }
            }
            catch(Exception e)
            {
				Console.WriteLine(e.ToString());
				Console.WriteLine(e.StackTrace);
				//Console.WriteLine("StackTrace: '{0}'", Environment.StackTrace);

                if (OnErrorCb != null)
                {
                    OnErrorCb("Error trying to listen for incoming connections. Is another instance of StreamingVisualize already running? Use the task manager to check for zombie processes.Error: \n\n" + e.ToString());
                }
				//throw;
            }
        }

        public void RequestStop()
        {
            shouldQuit = true;

            // Try to kill the socket.
            try
            {
                listener.Close();
            }
            catch (System.Exception e)
            {
				Console.WriteLine("StackTrace: '{0}'", Environment.StackTrace);
				Console.WriteLine(e.ToString());
				//throw;
            }

            Communication.KillAllSockets();
        }
    }
}
