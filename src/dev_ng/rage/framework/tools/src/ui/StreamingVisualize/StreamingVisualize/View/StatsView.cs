﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StreamingVisualize.Controller;
using StreamingVisualize.Model;
using StreamingVisualize.View.Components;

namespace StreamingVisualize.View
{
    public partial class StatsView : UserControl, IViewController
    {
        const int FILE_SIZE_BUCKETS = 20;
        const int MAX_MODULES = 30;
        const int CONCURRENT_MODULES = 3;
        const int MIN_BUCKET_POWER = 8;         // 2^MIN_BUCKET_POWER is the smallest file size bucket we carry

        private Font boldFont;
        private Brush textBrush;

        private int[] selectedModules = new int[CONCURRENT_MODULES];
        private String[] cachedToolTips = new String[FILE_SIZE_BUCKETS];

        private Brush[] selectedModuleBrushes = new Brush[CONCURRENT_MODULES];
        private Brush combinedModuleBrush;

        private Pen foregroundPen;

        private IList<StreamableEntry> StreamableList;

        private ViewContextMenu viewContextMenu = new ViewContextMenu();
        private SimpleTooltip tooltip = new SimpleTooltip();

        private float barsYTop;
        private float barsHeight;

        // Bucket that the mouse is currently over, -1 if none
        private int CurrentBucket = -1;

        // Number of seconds that we're spanning
        private float TimeRange;

        // If true, we're changing lists around. Don't respond to selection changes.
        private bool ChangingSelection;

        public class StreamableEntry
        {
            public String Name { get; set; }
            public Int64 FileSize { get; set; }
            public float ThroughPut { get; set; }

            public Streamable Streamable;
            public int Frame;
        }



        private class FileStats
        {
            public int filesLoaded;
            public Int64 bytesLoaded;
            public float throughPutSum;     // Sum of all throughputs in MB/s, divide by filesLoaded to get number
            public float timeSpentSum;

            public FileStats()
            {
                filesLoaded = 0;
                bytesLoaded = 0;
                throughPutSum = 0.0f;
                timeSpentSum = 0.0f;
            }

            public void AddFile(int fileSize, float throughPut, float timeSpent)
            {
                filesLoaded++;
                bytesLoaded += (Int64)fileSize;
                throughPutSum += throughPut;
                timeSpentSum += timeSpent;
            }

            public String GetThruputString(float time)
            {
                float mbPerSec = ((float) (bytesLoaded / 1024)) / (time * 1024.0f);
                float filesPerSec = ((float)filesLoaded) / time;
                return String.Format("{0:F3}MB/s, {0:F2} files/s", mbPerSec, filesPerSec);
            }
        }

        private interface ValueInterpreter
        {
            String GetName();

            float GetValue(FileStats stats, float timeRange);
        }

        private class FilesLoaded : ValueInterpreter
        {
            public String GetName() { return "Files Loaded"; }

            public float GetValue(FileStats stats, float timeRange) { return (float)stats.filesLoaded; }
        }

        private class TotalDataRead : ValueInterpreter
        {
            public String GetName() { return "Total Data Read (MB)"; }

            public float GetValue(FileStats stats, float timeRange) { return (float)(stats.bytesLoaded / (1024 * 1024)); }
        }

        private class Throughput : ValueInterpreter
        {
            public String GetName() { return "Throughput (MB/s)"; }

            public float GetValue(FileStats stats, float timeRange) { return (stats.filesLoaded > 0) ? stats.throughPutSum / (float)stats.filesLoaded : 0.0f; }
        }

        private class TotalTimeSpent : ValueInterpreter
        {
            public String GetName() { return "Total Time Spent (s)"; }

            public float GetValue(FileStats stats, float timeRange) { return stats.timeSpentSum; }
        }

        private class TotalTimeSpentPerFile : ValueInterpreter
        {
            public String GetName() { return "Total Time Spent Per File (s)"; }

            public float GetValue(FileStats stats, float timeRange) { return (stats.filesLoaded > 0) ? stats.timeSpentSum / (float)stats.filesLoaded : 0.0f; }
        }

        private ValueInterpreter[] knownInterpreters = new ValueInterpreter[] {
            new FilesLoaded(),
            new TotalDataRead(),
            new Throughput(),
            new TotalTimeSpent(),
            new TotalTimeSpentPerFile(),
        };


        MainController Controller;
        Scene Scene;

        // Bitmask of devices to consider
        int DeviceMask = 0xffff;

        // First frame to consider
        int FirstFrame;

        // Last frame to consider
        int LastFrame;

        // Zoom for the bar graph
        float zoomFactor = 1.0f;


        // Buckets are stored for files whose size is up to 2^(bucket + MIN_BUCKET_POWER).
        FileStats totalStats;
        FileStats[] totalStatsPerBucket = new FileStats[FILE_SIZE_BUCKETS];
        FileStats[,] totalStatsPerBucketPerModule = new FileStats[MAX_MODULES,FILE_SIZE_BUCKETS];

        ValueInterpreter CurrentInterpreter = new FilesLoaded();





        public StatsView()
        {
            InitializeComponent();

            selectedModules[0] = 1;
            selectedModules[1] = 2;
            selectedModules[2] = 3;
        }

        private void PopulateStreamableList(int barIndex)
        {
            ChangingSelection = true;
            StreamableList = new List<StreamableEntry>();

            ProcessData((streamable, timeTaken, moduleIndex, throughPut, fileSize, fileBucket, rscEvent, devEvent) =>
            {
                StreamableEntry entry = new StreamableEntry();
                entry.Name = streamable.CurrentName;    // TODO: Use the name at that time?
                entry.FileSize = fileSize;
                entry.ThroughPut = throughPut;
                entry.Streamable = streamable;
                entry.Frame = devEvent.Frame;

                StreamableList.Add(entry);
            });

            Streamables.DataSource = StreamableList;
            ChangingSelection = false;
        }

        private int GetFileSizeBucket(Int64 fileSize)
        {
            int fileBucket = -MIN_BUCKET_POWER;

            fileSize--;
            while (fileSize > 0)
            {
                fileBucket++;
                fileSize >>= 1;
            }

            fileBucket = Math.Min(fileBucket, FILE_SIZE_BUCKETS - 1);
            fileBucket = Math.Max(fileBucket, 0);

            return fileBucket;
        }

        private delegate void FileEvent(Streamable streamable, float timeTaken, int moduleIndex, float throughPut, int fileSize, int fileBucket, RscEvent rscEvent, DeviceEvent devEvent);

        private void ProcessData(FileEvent fileEvent)
        {
            // We're doing it by device, since devices contain the information we need.
            for (int deviceNo = 0; deviceNo < Scene.maxDeviceIndex; deviceNo++)
            {
                if (((1 << deviceNo) & DeviceMask) != 0)
                {
                    Device device = Scene.GetDevice(deviceNo);

                    if (device != null)
                    {
                        // Skip everything until we get to the frame we want.
                        int eventCount = device.DeviceEvents.Count;
                        int eventIndex = 0;

                        while (eventIndex < eventCount && device.DeviceEvents[eventIndex].Frame < FirstFrame)
                        {
                            eventIndex++;
                        }

                        // Now keep going until we get to the end of the list.
                        while (eventIndex < eventCount && device.DeviceEvents[eventIndex].Frame <= LastFrame)
                        {
                            DeviceEvent devEvent = device.DeviceEvents[eventIndex++];

                            // We only care about reads.
                            if (devEvent.Action == DeviceEvent.ActionType.READ)
                            {
                                Streamable streamable = devEvent.Streamable;

                                int fileSize = streamable.FileSize;

                                // Skip files that we don't know the file size about.
                                if (fileSize == 0)
                                {
                                    continue;
                                }

                                // Get the bucket by performing a binary logarithm.
                                int fileBucket = GetFileSizeBucket(fileSize);

                                // Let's get the finish time through the streamable.
                                int rscEventCount = streamable.Events.Count;
                                float throughPut = 0.0f;
                                float timeTaken = 0.0f;
                                RscEvent rscEvent = null;

                                for (int x = 0; x < rscEventCount; x++)
                                {
                                    rscEvent = streamable.Events[x];

                                    if (rscEvent.ProcessTime == devEvent.Time)
                                    {
                                        timeTaken = rscEvent.LoadTime - rscEvent.ProcessTime;

                                        if (timeTaken > 0.0f)
                                        {
                                            // We're dealing with floats, so let's try to keep the accuracy reasonable.
                                            if (timeTaken < 0.0001f)
                                            {
                                                throughPut = ((float)(fileSize)) / (rscEvent.LoadTime - rscEvent.ProcessTime);
                                            }
                                            else
                                            {
                                                throughPut = ((float)(fileSize / 1024)) / ((rscEvent.LoadTime - rscEvent.ProcessTime) * 1024.0f);
                                            }
                                        }

                                        break;
                                    }
                                }

                                int moduleIndex = streamable.Module.Index;

                                fileEvent(streamable, timeTaken, moduleIndex, throughPut, fileSize, fileBucket, rscEvent, devEvent);
                            }
                        }
                    }
                }
            }         
        }


        public void CollectData()
        {
            totalStats = new FileStats();

            for (int x = 0; x < FILE_SIZE_BUCKETS; x++)
            {
                totalStatsPerBucket[x] = new FileStats();

                for (int y = 0; y < MAX_MODULES; y++)
                {
                    totalStatsPerBucketPerModule[y, x] = new FileStats();
                }
            }

            ProcessData((streamable, timeTaken, moduleIndex, throughPut, fileSize, fileBucket, rscEvent, devEvent) => {

                totalStats.AddFile(fileSize, throughPut, timeTaken);
                totalStatsPerBucket[fileBucket].AddFile(fileSize, throughPut, timeTaken);
                totalStatsPerBucketPerModule[moduleIndex, fileBucket].AddFile(fileSize, throughPut, timeTaken);
            });
        }

        // Switch the display to focus on a certain frame
        public void SetToFrame(int frameNumber)
        {
        }

        // A new device was registered, or a device has changed.
        public void OnDeviceChanged(int deviceIndex, Device device) { }

        // A streamable has been selected
        public void OnSelectStreamable(Streamable streamable)
        {
        }

        // The client information has been updated
        public void ClientInfoUpdated() { }

        // We're in follow mode, and a new frame has been added.
        public void OnNewFrame(int frameNumber) { }

        // A new entity type was registered
        public void OnNewEntityType(int entityTypeId, EntityType type) { }

        // The connection has been terminated
        public void OnConnectionLost() { }

        // Register this view with the system
        public void Register(MainController controller, Scene scene)
        {
            this.Controller = controller;
            this.Scene = scene;

            FirstFrame = scene.SelectedFirstFrame;
            LastFrame = scene.SelectedLastFrame;
        }

        public void OnFilterChanged()
        {

        }

        private String MakeNiceSizeString(int bytes)
        {
            if (bytes < 1024)
            {
                return String.Format("{0}b", bytes);
            }
            else if (bytes < 1024 * 1024)
            {
                return String.Format("{0}K", bytes / 1024);
            }
            else if (bytes < 1024 * 1024 * 1024)
            {
                return String.Format("{0}M", bytes / (1024 * 1024));
            }
            return String.Format("{0}G", bytes / (1024 * 1024 * 1024));
        }

        // Extract whatever value we're supposed to display (as per the user's selection) from a given FileStats object.
        private float GetValue(FileStats stats, float timeRange)
        {
            return CurrentInterpreter.GetValue(stats, timeRange);
        }

        private String GetValueString(FileStats stats, float timeRange)
        {
            return String.Format("{0:F6}", GetValue(stats, timeRange));
        }

        private void StatsView_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            if (totalStats == null)
            {
                // No stats collected yet.
                // This will block the UI thread for quite a bit, and this sucks, but doing this in an MT fashion
                // would be a lot of headache.
                CollectData();
            }

            if (boldFont == null)
            {
                boldFont = new Font(Font, FontStyle.Bold);
                textBrush = new SolidBrush(ForeColor);

                combinedModuleBrush = new SolidBrush(Color.Black);
                selectedModuleBrushes[0] = new SolidBrush(Color.Red);
                selectedModuleBrushes[1] = new SolidBrush(Color.Green);
                selectedModuleBrushes[2] = new SolidBrush(Color.Blue);

                foregroundPen = new Pen(ForeColor);
            }

            // TODO: We could consider getting the actual wall clock from each Frame object.
            float time = ((float) (LastFrame - FirstFrame + 1)) / 30.0f;
            TimeRange = time;

            float fontHeight = (float) Font.Height;
            float yPos = fontHeight * 2.0f;
            float xPos = 10.0f;

            g.DrawString("TOTAL THRUPUT: " + totalStats.GetThruputString(time) + " between frame " + FirstFrame + " and " + LastFrame, boldFont, textBrush, xPos, yPos);
            yPos += fontHeight;

            String subSummary = "Showing " + CurrentInterpreter.GetName() + " for ";

            int devices = 0;
            for (int x = 0; x < Scene.maxDeviceIndex; x++)
            {
                if (((1 << x) & DeviceMask) != 0)
                {
                    if (++devices != 1)
                    {
                        subSummary += ", ";
                    }

                    subSummary += Scene.GetDevice(x).Name;
                }
            }

            if (devices == 0)
            {
                subSummary += "no devices. Enable some in the context menu.";
            }

            g.DrawString(subSummary, Font, textBrush, xPos, yPos);
            yPos += fontHeight;

            // Make it "nice".
            g.DrawLine(foregroundPen, xPos + 5.0f, yPos + 5.0f, (float)Width - 5.0f, yPos + 5.0f);
            yPos += 10.0f;

            float barCenter = 100.0f;

            // Legend: Show the three modules we selected.
            float cursorX = xPos;

            for (int mod = 0; mod < CONCURRENT_MODULES; mod++)
            {
                int module = selectedModules[mod];
                if (module != -1)
                {
                    String modName = Scene.GetModule(module).Name;

                    float textLen = g.MeasureString(modName, boldFont).Width;
                    g.DrawString(modName, boldFont, selectedModuleBrushes[mod], cursorX, yPos);
                    cursorX += textLen + 10.0f;
                }
            }

            yPos += fontHeight + 5.0f;

            barsYTop = yPos;
            barsHeight = fontHeight;

            // Create a graph with the distribution per bucket.
            for (int x=0; x<FILE_SIZE_BUCKETS; x++)
            {
                String bucketName = MakeNiceSizeString(1 << (x+MIN_BUCKET_POWER));
                SizeF textSize = g.MeasureString(bucketName, boldFont);

                String toolTipString = "Files around " + bucketName + "\n\n";

                g.DrawString(bucketName, boldFont, textBrush, barCenter - textSize.Width - 5.0f, yPos);

                // Let's draw the total size first.
                FileStats stats = totalStatsPerBucket[x];
                float value = GetValue(stats, time) * zoomFactor;
                g.FillRectangle(combinedModuleBrush, barCenter + 5.0f, yPos + 2.0f, value, fontHeight - 4.0f);
                g.DrawString(GetValueString(stats, time), Font, textBrush, barCenter + 10.0f + value, yPos);

                // Now the selected modules.
                float offset = 2.0f;
                for (int mod = 0; mod < CONCURRENT_MODULES; mod++)
                {
                    int module = selectedModules[mod];
                    if (module != -1)
                    {
                        FileStats modStats = totalStatsPerBucketPerModule[module, x];
                        float modValue = GetValue(modStats, time) * zoomFactor;
                        g.FillRectangle(selectedModuleBrushes[mod], barCenter + 5.0f, yPos + offset, modValue, 2.0f);
                        //g.DrawString(GetValueString(modStats), Font, textBrush, barCenter + 10.0f + modValue, yPos);

                        offset += (fontHeight - 4.0f) / 3.0f;

                        toolTipString += Scene.GetModule(module).Name + ": " + GetValueString(modStats, time) + "\n";
                    }
                }

                toolTipString += "Total: " + GetValueString(stats, time);

                yPos += fontHeight;
                cachedToolTips[x] = toolTipString;
            }

            tooltip.Draw(g, DefaultFont, Width, Height);
        }

        private void RefreshData()
        {
            totalStats = null;
            Refresh();
        }

        private void SelectModule(int selectedModuleIndex, int moduleIndex)
        {
            selectedModules[selectedModuleIndex] = moduleIndex;
            RefreshData();
            RecreateContextMenu();
        }

        private MenuItem[] CreateModuleSubMenu(Scene scene, int selectedModuleIndex)
        {
            int count = 0;

            for (int x = 0; x < scene.maxModuleIndex; x++)
            {
                if (scene.GetModule(x) != null)
                {
                    count++;
                }
            }

            MenuItem[] result = new MenuItem[count];

            count = 0;
            for (int x = 0; x < scene.maxModuleIndex; x++)
            {
                if (scene.GetModule(x) != null)
                {
                    MenuItem item = new MenuItem(scene.GetModule(x).Name);

                    int moduleIndex = x;
                    item.Checked = selectedModules[selectedModuleIndex] == x;
                    item.Click += (sender, e) => SelectModule(selectedModuleIndex, moduleIndex);

                    result[count++] = item;
                }
            }

            return result;
        }

        private void PickWorstOffenders(bool reverse)
        {
            if (CurrentBucket != -1)
            {
                // Find the top offenders by putting them all in a list and sorting them.
                List<Tuple<int, float> > list = new List<Tuple<int, float> >();

                for (int mod = 0; mod < Scene.maxModuleIndex; mod++)
                {
                    FileStats modStats = totalStatsPerBucketPerModule[mod, CurrentBucket];
                    float modValue = GetValue(modStats, TimeRange);

                    list.Add(new Tuple<int, float>(mod, modValue));
                }

                list.Sort((a, b) => a.Item2.CompareTo(b.Item2));

                // Now just pick off the worst offenders.
                if (reverse)
                {
                    // Skip the 0.0f ones.
                    int index = 0;

                    while (index < Scene.maxModuleIndex && list[index].Item2 == 0.0f)
                    {
                        index++;
                    }

                    // Now go for it.
                    for (int x = 0; x < CONCURRENT_MODULES; x++)
                    {
                        selectedModules[x] = (index < Scene.maxModuleIndex) ? list[index++].Item1 : -1;
                    }
                }
                else
                {
                    for (int x = 0; x < CONCURRENT_MODULES; x++)
                    {
                        selectedModules[x] = list[Scene.maxModuleIndex - x - 1].Item1;
                    }
                }

                Refresh();
            }
        }

        public void OnFrameRangeChanged(int firstFrame, int lastFrame)
        {
            FirstFrame = firstFrame;
            LastFrame = lastFrame;

            RefreshData();
        }

        private void SelectInterpreter(ValueInterpreter interpreter)
        {
            CurrentInterpreter = interpreter;
            RecreateContextMenu();
            Refresh();
        }

        private void ToggleDevice(int deviceIndex)
        {
            DeviceMask ^= (1 << deviceIndex);
            RecreateContextMenu();
            RefreshData();
        }

        private MenuItem[] CreateDeviceSubMenu(Scene scene, int deviceMask)
        {
            // Count 'em.
            int deviceCount = 0;

            for (int x = 0; x < Scene.maxDeviceIndex; x++)
            {
                if (Scene.GetDevice(x) != null)
                {
                    deviceCount++;
                }
            }

            MenuItem[] result = new MenuItem[deviceCount];

            int index = 0;
            for (int x = 0; x < Scene.maxDeviceIndex; x++)
            {
                if (Scene.GetDevice(x) != null)
                {
                    int deviceIndex = x;
                    MenuItem devItem = new MenuItem(Scene.GetDevice(x).Name);
                    devItem.Checked = ((1 << x) & deviceMask) != 0;
                    devItem.Click += (sender, e) => ToggleDevice(deviceIndex);
                    result[index++] = devItem;
                }
            }

            return result;
        }

        private void PopulateContextMenu(ContextMenu menu)
        {
            if (Scene != null)
            {
                // Create module selection list
                for (int x = 0; x < CONCURRENT_MODULES; x++)
                {
                    MenuItem[] subMenu = CreateModuleSubMenu(Scene, x);
                    MenuItem moduleItem = new MenuItem("Module " + (x + 1), subMenu);

                    menu.MenuItems.Add(moduleItem);
                }

                MenuItem worstOffenders = new MenuItem("Pick &Worst Offenders For Bucket");
                worstOffenders.Click += (sender, e) => PickWorstOffenders(false);
                menu.MenuItems.Add(worstOffenders);

                MenuItem bestOffenders = new MenuItem("Pick &Best Non-Zero For Bucket");
                bestOffenders.Click += (sender, e) => PickWorstOffenders(true);
                menu.MenuItems.Add(bestOffenders);

                MenuItem[] interpreterList = new MenuItem[knownInterpreters.Length];

                // Create interpreter list
                for (int x = 0; x < knownInterpreters.Length; x++)
                {
                    ValueInterpreter interpreter = knownInterpreters[x];
                    MenuItem item = new MenuItem(interpreter.GetName());
                    item.Checked = CurrentInterpreter == interpreter;
                    item.Click += (sender, e) => SelectInterpreter(interpreter);
                    interpreterList[x] = item;
                }

                menu.MenuItems.Add(new MenuItem("Value To Show", interpreterList));

                // Create device list
                MenuItem[] deviceSubMenu = CreateDeviceSubMenu(Scene, DeviceMask);
                MenuItem deviceSelection = new MenuItem("Devices", deviceSubMenu);
                menu.MenuItems.Add(deviceSelection);
            }
        }

        private void RecreateContextMenu()
        {
            ContextMenu menu = viewContextMenu.CreateContextMenu(Controller, this);
            PopulateContextMenu(menu);
            this.ContextMenu = menu;
        }

        private void StatsView_Load(object sender, EventArgs e)
        {
            RecreateContextMenu();
        }

        private int GetBarFromMousePos(int mouseYPos)
        {
            float index = (((float)mouseYPos) - barsYTop) / barsHeight;
            return (int)index;
        }

        private void StatsView_MouseMove(object sender, MouseEventArgs e)
        {
            String toolTipText = "";

            // Let's see which bar we're hovering over.
            int barIndex = GetBarFromMousePos(e.Y);

            if (barIndex >= 0 && barIndex < cachedToolTips.Length && cachedToolTips[barIndex] != null)
            {
                toolTipText = cachedToolTips[barIndex];
                CurrentBucket = barIndex;
            }
            else
            {
                CurrentBucket = -1;
            }

            tooltip.MouseMove(e);
            tooltip.SetToolTip(toolTipText);
            Refresh();
        }

        private void StatsView_Resize(object sender, EventArgs e)
        {

        }

        private void StatsView_MouseDown(object sender, MouseEventArgs e)
        {
            // Let's see which bar we're hovering over.
            int barIndex = GetBarFromMousePos(e.Y);

            PopulateStreamableList(barIndex);
        }

        protected override void OnMouseWheel(MouseEventArgs e)
        {
            if (e.Delta != 0)
            {
                zoomFactor *= (e.Delta > 0) ? 1.1f : 0.9f;
                Refresh();
            }

            base.OnMouseWheel(e);
        }

        private void StatsView_MouseEnter(object sender, EventArgs e)
        {
            this.Focus();
        }

        private void Streamables_SelectionChanged(object sender, EventArgs e)
        {
            if (ChangingSelection)
            {
                return;
            }

            if (Streamables.SelectedCells.Count > 0)
            {
                int index = Streamables.SelectedCells[0].RowIndex;

                if (index >= 0 && index < StreamableList.Count)
                {
                    ChangingSelection = true;
                    StreamableEntry entry = StreamableList[index];
                    Controller.OnSelectStreamable(entry.Streamable, true);
                    Controller.SetToFrame(entry.Frame, true);
                    ChangingSelection = false;
                }
            }

        }

        public void OnSelectEntity(Entity entity) { }

        private void CopyToClipboard_Click(object sender, EventArgs e)
        {
            String result = "";

            // The header.
            result += "Bucket";

            for (int x = 0; x < knownInterpreters.Length; x++)
            {
                result += "\t";
                result += knownInterpreters[x].GetName();
            }

            result += "\n";

            // First, the global list.
            for (int x = 0; x < FILE_SIZE_BUCKETS; x++)
            {
                String bucketName = MakeNiceSizeString(1 << (x + MIN_BUCKET_POWER));
                FileStats stats = totalStatsPerBucket[x];

                result += bucketName;

                for (int y = 0; y < knownInterpreters.Length; y++)
                {
                    float value = knownInterpreters[y].GetValue(stats, TimeRange) * zoomFactor;
                    result += "\t";
                    result += value;
                }

                result += "\n";
            }

            // Now, the current bucket.
            result += "\nCurrent bucket:\n";
            result += "File\tSize\tThroughput\tLoad Time\tProcess Time\tTime Taken\n";

            int barIndex = CurrentBucket;

            ProcessData((streamable, timeTaken, moduleIndex, throughPut, fileSize, fileBucket, rscEvent, devEvent) =>
            {
                result += streamable.CurrentName;       // TODO: Use the name at that time?2
                result += "\t";
                result += fileSize;
                result += "\t";
                result += throughPut;
                result += "\t";
                result += rscEvent.LoadTime;
                result += "\t";
                result += rscEvent.ProcessTime;
                result += "\t";
                result += rscEvent.ProcessTime - rscEvent.LoadTime;
                result += "\t";
                result += "\n";
            });

            Clipboard.SetText(result);
        }
    }
}
