﻿namespace StreamingVisualize.View
{
    partial class FindStreamable
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.StreamableName = new System.Windows.Forms.TextBox();
            this.Results = new System.Windows.Forms.ListBox();
            this.streamableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.streamableBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // StreamableName
            // 
            this.StreamableName.Location = new System.Drawing.Point(13, 13);
            this.StreamableName.Name = "StreamableName";
            this.StreamableName.Size = new System.Drawing.Size(259, 20);
            this.StreamableName.TabIndex = 0;
            this.StreamableName.TextChanged += new System.EventHandler(this.StreamableName_TextChanged);
            this.StreamableName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.StreamableName_KeyDown);
            // 
            // Results
            // 
            this.Results.DataSource = this.streamableBindingSource;
            this.Results.DisplayMember = "CurrentName";
            this.Results.FormattingEnabled = true;
            this.Results.Location = new System.Drawing.Point(13, 40);
            this.Results.Name = "Results";
            this.Results.Size = new System.Drawing.Size(258, 212);
            this.Results.TabIndex = 1;
            this.Results.ValueMember = "Index";
            this.Results.SelectedIndexChanged += new System.EventHandler(this.Results_SelectedIndexChanged);
            this.Results.DoubleClick += new System.EventHandler(this.Results_DoubleClick);
            this.Results.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Results_MouseDoubleClick);
            // 
            // streamableBindingSource
            // 
            this.streamableBindingSource.DataSource = typeof(StreamingVisualize.Model.Streamable);
            // 
            // FindStreamable
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.Results);
            this.Controls.Add(this.StreamableName);
            this.Name = "FindStreamable";
            this.Text = "Find Streamable";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FindStreamable_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.streamableBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox StreamableName;
        private System.Windows.Forms.ListBox Results;
        private System.Windows.Forms.BindingSource streamableBindingSource;
    }
}