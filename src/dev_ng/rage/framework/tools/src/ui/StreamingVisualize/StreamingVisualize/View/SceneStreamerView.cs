﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StreamingVisualize.Model;
using StreamingVisualize.Controller;
using StreamingVisualize.View.Components;

namespace StreamingVisualize.View
{
    public partial class SceneStreamerView : SVView
    {
        public enum RequestStatus
        {
            NOT_REQUESTED,
            REQUESTED_PURGED,
            REQUESTED,
        }

        public class StreamerEntry
        {
            //Streamable Entity;
            public Entity Entity { get; set; }

            public Streamable Streamable { get; set; }

            public float Score { get; set; }

            public float Size { get; set; }

            public LodType LodType { get; set; }

            public String VisFlags { get; set; }

            public Streamable MapData { get; set; }

            public RequestStatus Request { get; set; }

            public int EntityFlags { get; set; }

            public float distance { get; set; }
            public float fDot { get; set; }
            public float fCosof { get; set; }
            public float fSubtent { get; set; }

            public int PhaseFlags;
        }

        private float ms_fCosOfScreenYawForPriorityConsideration = (float) Math.Cos(5.0f / 2.0f * Math.PI / 180.0f);
        private const float ms_fHighPriorityYaw = 10.0f;	//in degrees
        private const float ms_fHighPriorityYawCompare = (ms_fHighPriorityYaw/180.0f)*(float) Math.PI;


        private List<StreamerEntry> StreamerStatus = new List<StreamerEntry>();
        private int FrameNumber;
        private bool ChangingSelection;

        private ViewContextMenu ViewContextMenu = new ViewContextMenu();
        private Scene Scene;
        private MainController Controller;

        private Streamable CurrentStreamble;

        private Font CurrentStreamableFont;


        public SceneStreamerView()
        {
            InitializeComponent();

            CurrentStreamableFont = new Font(this.Font, FontStyle.Bold);
        }

        public void CreateStreamerStatus()
        {
            List<StreamerEntry> Result = new List<StreamerEntry>();

            Frame frame = Scene.GetFrame(FrameNumber);
            NeededList neededList = frame.NeededList;

            Vector CamPos = Scene.GetFrame(FrameNumber).StreamCamPos;
            Vector CamDir = Scene.GetFrame(FrameNumber).StreamCamDir;

            lock (neededList)
            {
                foreach (NeededList.Entry listEntry in neededList.List)
                {
                    StreamerEntry streamerEntry = new StreamerEntry();
                    Entity entity = listEntry.entity;
                    Entity.NewState state = entity.NewHistory.GetStateAt(FrameNumber);

                    Entity.PositionState posState = entity.PositionHistory.GetStateAt(FrameNumber);

                    // Apply filter.
                    if (!Scene.Filter.IsFilteredOut(state.Streamable))
                    {
                        streamerEntry.Entity = entity;
                        streamerEntry.Streamable = state.Streamable;
                        streamerEntry.Score = listEntry.score;
                        streamerEntry.Size = posState.SphereRadius;
                        streamerEntry.LodType = Scene.GetLodType(state.LodType);
                        streamerEntry.MapData = state.MapDataStreamable;
                        streamerEntry.PhaseFlags = state.PhaseFlags;
                        streamerEntry.EntityFlags = state.EntityFlags;
                        streamerEntry.VisFlags = Scene.VisFlagsDef.CreateString(state.PhaseFlags, state.OptionFlags);

                        if ((state.PhaseFlags & Scene.VisFlagsDef.GbufFlag) == 0)
                        {
                            // Be loud and clear when it's not visible.
                            streamerEntry.VisFlags += "NO_GBUF";
                        }

                        // The following code is taken semi-verbatim from the gameside SceneStreamerList.cpp.
//                        Vector EntityCenter = posState.GetCenter();
                        Vector EntityCenter = new Vector(posState.SphereCenter);
                        Vector vDir = EntityCenter.Sub(CamPos);
                        float distance = vDir.Mag();

                        vDir = vDir.Scale(1.0f / (distance + 0.000001f));
                        float fDot = vDir.Dot(CamDir);
                        float fRadius = streamerEntry.Size;

                        streamerEntry.distance = distance;
                        streamerEntry.fDot = fDot;
                        streamerEntry.fCosof = 0.0f;
                        streamerEntry.fSubtent = 0.0f;


                        if (fDot >= 0.0f)
                        {
                            // we want the cos of the angle subtended by the sphere, which is adjacent/hypot. Adjacent is dist to sphere
                            float fHypot = (float) Math.Sqrt(distance * distance + fRadius * fRadius);
                            float fCosofApproxAngleSubtendedBySphere = (distance / fHypot);

                            streamerEntry.fCosof = fCosofApproxAngleSubtendedBySphere;

                            // if it subtends a small angle then ignore it (is it small on the screen?)
                            if (fCosofApproxAngleSubtendedBySphere < ms_fCosOfScreenYawForPriorityConsideration)
                            {
                                float fSubtendedAngle = (float) Math.Acos(fCosofApproxAngleSubtendedBySphere);
                                streamerEntry.fSubtent = (float) Math.Acos(fDot) - fSubtendedAngle;
                            }
                        }

                        RequestStatus status = RequestStatus.NOT_REQUESTED;

                        for (int x = 0; x < frame.requests.Count; x++)
                        {
                            if (frame.requests[x].Streamable == state.Streamable)
                            {
                                status = ((frame.requests[x].Flags & Request.DOOMED) != 0) ? RequestStatus.REQUESTED_PURGED : RequestStatus.REQUESTED;
                            }
                        }

                        streamerEntry.Request = status;
                        Result.Add(streamerEntry);
                    }
                }
            }

            // Reverse list, we want highest priority first.
            Result.Reverse();

            StreamerStatus = Result;
        }

        private void RecreateContextMenu()
        {
            ContextMenu menu = ViewContextMenu.CreateContextMenu(Controller, this);

            this.ContextMenu = menu;
        }


        private void SceneStreamerView_Load(object sender, EventArgs e)
        {
            RecreateContextMenu();

/*            StreamableColIdx = History.Columns["StreamableCol"].Index;
            FrameNumberColIdx = History.Columns["FrameNumberCol"].Index;
            EventColIdx = History.Columns["EventCol"].Index;
            GuidColIdx = History.Columns["GuidCol"].Index;*/
        }

        public override void OnFilterChanged()
        {
            RefreshData();
        }

        public override void Register(MainController controller, Scene scene)
        {
            this.Controller = controller;
            this.Scene = scene;

            if (scene != null)
            {
                SetToFrame(scene.CurrentFrameNumber);
            }
        }

        public override void OnNewFrame(int frameNumber)
        {
            FrameNumber = frameNumber;
            BeginInvoke((Action)delegate { RefreshData(); });
        }

        public override void SetToFrame(int frameNumber)
        {
            if (!ChangingSelection)
            {
                ChangingSelection = true;
                FrameNumber = frameNumber;
                RefreshData();
                ChangingSelection = false;
            }
        }

        public void RefreshData()
        {
            ChangingSelection = true;

            Header.Text = "SCENE STREAMER AT " + FrameNumber;

            CreateStreamerStatus();
            SceneStreamerList.DataSource = StreamerStatus;

            ChangingSelection = false;
        }

        private void SceneStreamerList_SelectionChanged(object sender, EventArgs e)
        {
            if (!ChangingSelection)
            {
                if (SceneStreamerList.SelectedCells.Count > 0)
                {
                    int index = SceneStreamerList.SelectedCells[0].RowIndex;

                    if (index >= 0 && index < StreamerStatus.Count)
                    {
                        ChangingSelection = true;

                        StreamerEntry entry = StreamerStatus[index];
                        Controller.OnSelectEntity(entry.Entity, false);
                        Controller.OnSelectStreamable(entry.Streamable, false);

                        CurrentStreamble = entry.Streamable;

                        ChangingSelection = false;
                        Refresh();
                    }
                }
            }
        }

        private void SceneStreamerList_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            String column = SceneStreamerList.Columns[e.ColumnIndex].Name;

            if (e.RowIndex < StreamerStatus.Count)
            {
                StreamerEntry entry = StreamerStatus[e.RowIndex];

                if (entry.Streamable == CurrentStreamble)
                {
                    e.CellStyle.Font = CurrentStreamableFont;
                }

                if (column == "fSubtent")
                {
                    e.CellStyle.ForeColor = (entry.fSubtent >= ms_fHighPriorityYawCompare) ? Color.Red : Color.Black;
                }
                else if (column == "distance")
                {
                    e.CellStyle.ForeColor = (entry.fDot < 0 && entry.distance > entry.Size) ? Color.Red : Color.Black;
                }
                else if (column == "fCosof")
                {
                    e.CellStyle.ForeColor = (entry.fCosof >= ms_fCosOfScreenYawForPriorityConsideration) ? Color.Red : Color.Black;
                }
                else if (column == "VisFlags")
                {
                    e.CellStyle.ForeColor = ((entry.PhaseFlags & Scene.VisFlagsDef.GbufFlag) == 0) ? Color.Red : Color.Black;
                }
                else if (column == "EntityFlags")
                {
                    bool isLowPrio = (entry.EntityFlags & Entity.NewState.FLAG_LOW_PRIORITY_STREAM) != 0;
                    e.Value = (isLowPrio) ? "LOW_PRIO" : "";
                    e.CellStyle.ForeColor = (isLowPrio) ? Color.Red : Color.Black;
                }
            }
        }
    }
}
