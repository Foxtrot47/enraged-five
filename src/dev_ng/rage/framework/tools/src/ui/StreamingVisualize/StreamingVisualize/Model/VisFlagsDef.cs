﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StreamingVisualize.Model
{
    public class VisFlagsDef
    {
        public Dictionary<int, String> PhaseFlags = new Dictionary<int, String>();
        public Dictionary<int, String> OptionFlags = new Dictionary<int, String>();

        public int GbufFlag = 1;


        public String CreateString(int phaseFlags, int optionFlags)
        {
            StringBuilder result = new StringBuilder(512);

            foreach (int phaseFlag in PhaseFlags.Keys)
            {
                if ((phaseFlags & phaseFlag) != 0)
                {
                    result.Append(PhaseFlags[phaseFlag]);
                    result.Append(' ');
                }
            }

            foreach (int optionFlag in OptionFlags.Keys)
            {
                if ((optionFlags & optionFlag) != 0)
                {
                    result.Append(OptionFlags[optionFlag]);
                    result.Append(' ');
                }
            }

            return result.ToString();
        }

        private void SerializeFlags(VersionedWriter writer, Dictionary<int, String> flags)
        {
            writer.Write((byte)flags.Count);

            foreach (int flag in flags.Keys)
            {
                writer.Write(flag);
                writer.Write(flags[flag]);
            }
        }

        private void DeserializeFlags(VersionedReader reader, Dictionary<int, String> flags)
        {
            int count = reader.ReadByte();

            for (int x = 0; x < count; x++)
            {
                int flag = reader.ReadInt();
                String name = reader.ReadString();

                flags.Add(flag, name);
            }
        }

        public void Serialize(VersionedWriter writer)
        {
            SerializeFlags(writer, PhaseFlags);
            SerializeFlags(writer, OptionFlags);
            writer.Write(GbufFlag);
        }

        public void Deserialize(VersionedReader reader, int version)
        {
            DeserializeFlags(reader, PhaseFlags);
            DeserializeFlags(reader, OptionFlags);
            GbufFlag = reader.ReadInt();
        }

        public void CreateCompatibilitySet()
        {
            PhaseFlags.Add(1, "(Visible)");
            GbufFlag = 1;
        }
    }
}
