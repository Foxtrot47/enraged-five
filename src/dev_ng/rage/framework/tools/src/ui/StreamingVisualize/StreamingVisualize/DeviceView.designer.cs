﻿namespace StreamingVisualize.View
{
    partial class DeviceView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.DeviceName = new System.Windows.Forms.Label();
            this.DeviceEvents = new System.Windows.Forms.DataGridView();
            this.Frame = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LSN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Streamable = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.deviceEventBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.DeviceEvents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceEventBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // DeviceName
            // 
            this.DeviceName.AutoSize = true;
            this.DeviceName.Location = new System.Drawing.Point(-3, 0);
            this.DeviceName.Name = "DeviceName";
            this.DeviceName.Size = new System.Drawing.Size(72, 13);
            this.DeviceName.TabIndex = 0;
            this.DeviceName.Text = "Device Name";
            // 
            // DeviceEvents
            // 
            this.DeviceEvents.AllowUserToAddRows = false;
            this.DeviceEvents.AllowUserToDeleteRows = false;
            this.DeviceEvents.AllowUserToResizeRows = false;
            this.DeviceEvents.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DeviceEvents.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DeviceEvents.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DeviceEvents.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Frame,
            this.Time,
            this.LSN,
            this.Streamable});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DeviceEvents.DefaultCellStyle = dataGridViewCellStyle2;
            this.DeviceEvents.Location = new System.Drawing.Point(0, 29);
            this.DeviceEvents.Name = "DeviceEvents";
            this.DeviceEvents.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DeviceEvents.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.DeviceEvents.RowHeadersVisible = false;
            this.DeviceEvents.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DeviceEvents.Size = new System.Drawing.Size(426, 260);
            this.DeviceEvents.TabIndex = 1;
            this.DeviceEvents.VirtualMode = true;
            this.DeviceEvents.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DeviceEvents_CellContentClick);
            this.DeviceEvents.CellValueNeeded += new System.Windows.Forms.DataGridViewCellValueEventHandler(this.DeviceEvents_CellValueNeeded);
            this.DeviceEvents.SelectionChanged += new System.EventHandler(this.DeviceEvents_SelectionChanged);
            // 
            // Frame
            // 
            this.Frame.HeaderText = "Frame";
            this.Frame.Name = "Frame";
            this.Frame.ReadOnly = true;
            this.Frame.Width = 60;
            // 
            // Time
            // 
            this.Time.HeaderText = "Time";
            this.Time.Name = "Time";
            this.Time.ReadOnly = true;
            this.Time.Width = 70;
            // 
            // LSN
            // 
            this.LSN.HeaderText = "LSN";
            this.LSN.Name = "LSN";
            this.LSN.ReadOnly = true;
            this.LSN.Width = 70;
            // 
            // Streamable
            // 
            this.Streamable.HeaderText = "Streamable";
            this.Streamable.Name = "Streamable";
            this.Streamable.ReadOnly = true;
            // 
            // deviceEventBindingSource
            // 
            this.deviceEventBindingSource.DataSource = typeof(StreamingVisualize.Model.DeviceEvent);
            // 
            // DeviceView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.DeviceEvents);
            this.Controls.Add(this.DeviceName);
            this.Name = "DeviceView";
            this.Size = new System.Drawing.Size(426, 289);
            ((System.ComponentModel.ISupportInitialize)(this.DeviceEvents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceEventBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label DeviceName;
        private System.Windows.Forms.DataGridView DeviceEvents;
        private System.Windows.Forms.BindingSource deviceEventBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn Frame;
        private System.Windows.Forms.DataGridViewTextBoxColumn Time;
        private System.Windows.Forms.DataGridViewTextBoxColumn LSN;
        private System.Windows.Forms.DataGridViewTextBoxColumn Streamable;
    }
}
