﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Collections.Concurrent;
using StreamingVisualize.Model;
using System.Net;
using System.IO;
using System.Xml.Linq;

namespace StreamingVisualize.Controller
{
    public class Playback
    {
        private String IPAddress;

        private const int PUT_TIMEOUT = 5000;

        private Thread Thread;

        private Vector LastCamPos;
        private Vector LastCamDir;
        private Vector LastCamUp;
        private float LastFovY;
        private bool CameraChanged;

        private int LastEntityGuid;
        private String LastEntityMapSection;
        private String LastEntityModelName;
        private bool SelectedEntityChanged;

        private static readonly string streamingImapSuffix_ = "_strm_";
        private static readonly string streamingLongImapSuffix_ = "_long_";



        private static string GetMapSectionNameFromIMAPName(string imapName)
        {
            if (imapName.Contains(streamingImapSuffix_))
            {
                return imapName.Substring(0, imapName.IndexOf(streamingImapSuffix_));
            }
            else if (imapName.Contains(streamingLongImapSuffix_))
            {
                return imapName.Substring(0, imapName.IndexOf(streamingLongImapSuffix_));
            }

            return imapName;
        }

        private interface Job
        {
            bool Execute(Playback playback);
        }

        private class JobQuit : Job 
        {
            public bool Execute(Playback playback)
            {
                return false;
            }
        }

        private class ChangeCameraJob : Job
        {
            public bool Execute(Playback playback)
            {
                if (playback.CameraChanged)
                {
                    XElement element;
                    lock (playback)
                    {
                        element = new XElement("CFreeCamAdjust",
                            new XElement("vPos", new XAttribute("x", playback.LastCamPos.X), new XAttribute("y", playback.LastCamPos.Y), new XAttribute("z", playback.LastCamPos.Z)),
                            new XElement("vFront", new XAttribute("x", playback.LastCamDir.X), new XAttribute("y", playback.LastCamDir.Y), new XAttribute("z", playback.LastCamDir.Z)),
                            new XElement("vUp", new XAttribute("x", playback.LastCamUp.X), new XAttribute("y", playback.LastCamUp.Y), new XAttribute("z", playback.LastCamUp.Z)));

                        playback.CameraChanged = false;
                    }

                    playback.SendRequest(CreateCameraRequest(element));
                }

                return true;
            }
        }

        private class SelectEntityJob : Job
        {
            public bool Execute(Playback playback)
            {
                if (playback.SelectedEntityChanged)
                {
                    XElement element;

                    String mapSection = playback.LastEntityMapSection;
                    // Strip out the extension.
                    int ext = mapSection.LastIndexOf('.');
                    if (ext != -1)
                    {
                        mapSection = mapSection.Substring(0, ext);
                    }
                    mapSection = GetMapSectionNameFromIMAPName(mapSection);

                    lock (playback)
                    {
                        element = new XElement("CEntityPickEvent",
                            new XElement("guid", new XAttribute("value", playback.LastEntityGuid)),
                            new XElement("mapSectionName", mapSection),
                            new XElement("modelName", playback.LastEntityModelName));

                        playback.SelectedEntityChanged = false;
                    }

                    playback.SendRequest(CreateSelectionRequest(element));
                }

                return true;
            }
        }

        private static String CreateCameraRequest(XElement element)
        {
            XDocument document = new XDocument(new XElement("MapLiveEditRestInterface"));

            document.Root.Add(new XElement("incomingCameraChanges"));
            document.Root.Element("incomingCameraChanges").Add(element);

            return document.ToString();
        }

        private static String CreateSelectionRequest(XElement element)
        {
            XDocument document = new XDocument(new XElement("MapLiveEditRestInterface"));

            document.Root.Add(new XElement("incomingPickerSelect"));
            document.Root.Element("incomingPickerSelect").Add(element);

            return document.ToString();
        }

        private BlockingCollection<Job> Jobs = new BlockingCollection<Job>();


        public Playback()
        {
            Thread = new Thread(Worker);
            Thread.Name = "Playback Interface";
            Thread.Start();
        }

        public void SetIPAddress(String ipAddress)
        {
            IPAddress = ipAddress;
        }

        public void Terminate()
        {
            Jobs.Add(new JobQuit());
        }

        public void SetCamera(Vector camPos, Vector camDir, Vector camUp, float fovY)
        {
            lock (this)
            {
                LastCamPos = camPos;
                LastCamDir = camDir;
                LastCamUp = camUp;
                LastFovY = fovY;
                CameraChanged = true;
            }

            ChangeCameraJob job = new ChangeCameraJob();

            Jobs.Add(job);
        }

        public void SelectEntity(int guid, String mapData, String modelName)
        {
            lock (this)
            {
                LastEntityGuid = guid;
                LastEntityMapSection = mapData;
                LastEntityModelName = modelName;
                SelectedEntityChanged = true;
            }

            SelectEntityJob job = new SelectEntityJob();

            Jobs.Add(job);
        }

        private void Worker()
        {
            bool stayAlive = true;
            do
            {
                Job job = Jobs.Take();

                stayAlive = job.Execute(this);
            }
            while (stayAlive);
        }

        private void SendRequest(String text)
        {
            if (IPAddress == null || IPAddress.Length == 0)
            {
                return;
            }

            try
            {
                WebRequest request = HttpWebRequest.Create("http://" + IPAddress + "/Scene/MapLiveEdit");
                request.Timeout = PUT_TIMEOUT;
                request.Method = "PUT";
                byte[] postData = Encoding.UTF8.GetBytes(text);
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = postData.Length;
                Stream dataStream = request.GetRequestStream();
                dataStream.Write(postData, 0, postData.Length);
                dataStream.Close();
                using (WebResponse response = request.GetResponse()) { }// There's translatable response to a PUT
            }
            catch (Exception e)
            {
                Console.WriteLine("Bad REST: " + e.Message);
            }
        }
    }
}
