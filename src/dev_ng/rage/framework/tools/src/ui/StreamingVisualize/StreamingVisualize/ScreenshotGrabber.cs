﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Diagnostics;
using System.IO;

namespace StreamingVisualize
{
    class ScreenshotGrabber
    {
        private bool terminationRequest;

        private const String PS3_SHOT_CMD = "C:\\Program Files (x86)\\SN Systems\\PS3\\bin\\ps3ctrl.exe";
        private const String PS3_SHOT_PARAM = "capture {0}";

        private String shotPathPrefix;
        private eint shotNumber;


        private Semaphore ThrottleSemaphore;


        public ScreenshotGrabber()
        {
            shotPathPrefix = Path.Combine(Path.GetTempPath(), "strmvizshots", "shot");
            shotNumber = 1;
        }

        public void Start()
        {
            Thread thread = new Thread(new ThreadStart(Worker));
            thread.Name = "Connection Handler";
            thread.Start();
        }

        private void Worker()
        {
            while (!terminationRequest)
            {
                // Try to get a screenshot.
                TakeScreenshot();

            }
        }

        private void TakeScreenshot()
        {
            int ExitCode;
            ProcessStartInfo ProcessInfo;
            Process Process;

            String file = shotPathPrefix + shotNumber + ".jpg";
            shotNumber++;

            ProcessInfo = new ProcessStartInfo(PS3_SHOT_CMD, String.Format(PS3_SHOT_PARAM, file);
            ProcessInfo.CreateNoWindow = true;
            ProcessInfo.UseShellExecute = false;
            Process = Process.Start(ProcessInfo);
            Process.WaitForExit();
            ExitCode = Process.ExitCode;
            Process.Close();
        }

        public void Terminate()
        {
            terminationRequest = true;
        }

        
    }
}
