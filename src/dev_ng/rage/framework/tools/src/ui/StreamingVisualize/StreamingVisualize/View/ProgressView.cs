﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace StreamingVisualize.View
{
    public partial class ProgressView : Form
    {
        public delegate void AbortDelegate();

        private String requestedSubAction;
        private int requestedProgress;
        private AbortDelegate abortDelegate;
        private bool abort;


        public ProgressView()
        {
            InitializeComponent();
        }

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }

        // Called from the main thread only - perform initial setup.
        public void StartUp(String mainAction, int maxItems, AbortDelegate abortDelegate)
        {
            MainAction.Text = mainAction;
            Progress.Maximum = maxItems;

            requestedProgress = 0;
            requestedSubAction = "";
            this.abortDelegate = abortDelegate;

            UpdateStatus();
        }

        private void UpdateStatus()
        {
            lock (this)
            {
                SubAction.Text = requestedSubAction;
                Progress.Value = requestedProgress;
            }
        }

        // Called from a worker thread.
        public void SetStatus(String subAction, int progress)
        {
            lock (this)
            {
                requestedSubAction = subAction;
                requestedProgress = progress;
            }

            BeginInvoke((Action)delegate { UpdateStatus(); });
        }

        private void ProgressView_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                AbortProgress();
            }
        }

        public bool ShouldAbort()
        {
            return abort;
        }

        // Called from main thread only.
        private void AbortProgress()
        {
            abort = true;
            SubAction.Text = "Aborting...";

            if (abortDelegate != null)
            {
                abortDelegate();
            }
        }

        private void Abort_Click(object sender, EventArgs e)
        {
            AbortProgress();
        }
    }
}
