﻿namespace StreamingVisualize.View
{
    partial class EntityView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // EntityView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.Black;
            this.DoubleBuffered = true;
            this.MinimumSize = new System.Drawing.Size(100, 100);
            this.Name = "EntityView";
            this.Size = new System.Drawing.Size(100, 100);
            this.Load += new System.EventHandler(this.EntityView_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.EntityView_Paint);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.EntityView_KeyDown);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.EntityView_MouseDown);
            this.MouseHover += new System.EventHandler(this.EntityView_MouseHover);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.EntityView_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.EntityView_MouseUp);
            this.ResumeLayout(false);

        }

        #endregion
    }
}
