﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StreamingVisualize.Model
{
    public class Vector16
    {
        public short iX;
        public short iY;
        public short iZ;

        public float X
        {
            get { return (float)iX / FACTOR; }
            set { iX = (short)(value * FACTOR); }
        }
        public float Y
        {
            get { return (float)iY / FACTOR; }
            set { iY = (short)(value * FACTOR); }
        }
        public float Z
        {
            get { return (float)iZ / FACTOR; }
            set { iZ = (short)(value * FACTOR); }
        }

        public static Vector16 Zero = new Vector16(0, 0, 0);

        public const float FACTOR = 4.0f;

        public override String ToString()
        {
            return String.Format("{0:F2}, {1:F2}, {2:F2}", X, Y, Z);
        }

        public Vector16()
        {

        }

        public static float Unpack(short value)
        {
            return (float)value / FACTOR;
        }

        public Vector16(Vector16 v)
        {
            iX = v.iX;
            iY = v.iY;
            iZ = v.iZ;
        }

        public Vector16(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public Vector16(short x, short y, short z)
        {
            iX = x;
            iY = y;
            iZ = z;
        }

        public Vector16 Add(Vector16 o)
        {
            Vector16 r = new Vector16();

            r.iX = (short)(iX + o.iX);
            r.iY = (short)(iY + o.iY);
            r.iZ = (short)(iZ + o.iZ);

            return r;
        }

        public Vector16 Sub(Vector16 o)
        {
            Vector16 r = new Vector16();

            r.iX = (short)(iX - o.iX);
            r.iY = (short)(iY - o.iY);
            r.iZ = (short)(iZ - o.iZ);

            return r;
        }

        public Vector16 Scale(float factor)
        {
            Vector16 r = new Vector16();

            r.X = X * factor;
            r.Y = Y * factor;
            r.Z = Z * factor;

            return r;
        }

        public float GetHighestComponent()
        {
            float r = Math.Max(X, Y);
            return Math.Max(r, Z);
        }

        public Vector16 Min(Vector16 o)
        {
            Vector16 r = new Vector16();

            r.iX = Math.Min(iX, o.iX);
            r.iY = Math.Min(iY, o.iY);
            r.iZ = Math.Min(iZ, o.iZ);

            return r;
        }

        public Vector16 Max(Vector16 o)
        {
            Vector16 r = new Vector16();

            r.iX = Math.Max(iX, o.iX);
            r.iY = Math.Max(iY, o.iY);
            r.iZ = Math.Max(iZ, o.iZ);

            return r;
        }

        public float Dist(Vector16 o)
        {
            return (float)Math.Sqrt((X - o.X) * (X - o.X) + (Y - o.Y) * (Y - o.Y) + (Z - o.Z) * (Z - o.Z));
        }

        public int CompareTo(Vector16 s)
        {
            return (iX == s.iX && iY == s.iY && iZ == s.iZ) ? 0 : 1;
        }

        public bool Equals(Vector16 s)
        {
            return iX == s.iX && iY == s.iY && iZ == s.iZ;
        }
    }
}
