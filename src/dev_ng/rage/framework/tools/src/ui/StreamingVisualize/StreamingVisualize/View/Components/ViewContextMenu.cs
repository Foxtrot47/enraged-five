﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StreamingVisualize.Controller;
using System.Windows.Forms;

namespace StreamingVisualize.View.Components
{
    public class ViewContextMenu
    {
        MenuItem closeItem;
        MenuItem[] changeViewItems;


        public ContextMenu CreateContextMenu(MainController controller, IViewController view)
        {
            ContextMenu menu = new ContextMenu();

            // Create the core components: An open to close this window...
            closeItem = new MenuItem("&Close");
            menu.MenuItems.Add(closeItem);

            // Add the option to change this view.
            changeViewItems = new MenuItem[ViewDefinition.ViewTypeCount];

            for (int x = 0; x < ViewDefinition.ViewTypeCount; x++)
            {
                int viewType = x;
                ViewDefinition definition = ViewDefinition.GetDefinition(x);
                changeViewItems[x] = new MenuItem(definition.Name);
                changeViewItems[x].Click += (sender, e) => { controller.ChangeViewItem(view, viewType); };
            }

            menu.MenuItems.Add(new MenuItem("Change View", changeViewItems));

            return menu;
       }
    }
}
