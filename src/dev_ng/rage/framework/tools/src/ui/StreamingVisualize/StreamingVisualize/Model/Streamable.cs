﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace StreamingVisualize.Model
{
    public class Streamable : IVersionedSerializable
    {
        public enum Status
        {
            NOTLOADED = 0,
            LOADED = 1,
            LOADREQUESTED = 2,
            LOADING = 3,
            UNREGISTERED = 4,
            INVALID = 5,            // Not really used as state, but could be a return value in some functions
        };

        public class State : IFrameState, ICloneable, IVersionedSerializable, IComparable<State>
        {
            public int Frame { get; set; }
            public Streamable.Status Status { get; set; }
            public int Flags { get; set; }

            public void Serialize(VersionedWriter writer)
            {
                writer.Write((byte)Status);

                // Version 38: Flags
                writer.Write(Flags);
            }

            public void Deserialize(VersionedReader reader, int version)
            {
                Status = (Streamable.Status)reader.ReadByte();

                if (version >= 38)
                {
                    Flags = reader.ReadInt();
                }
            }

            public int GetFrameNumber()
            {
                return Frame;
            }

            public void SetFrameNumber(int frameNumber)
            {
                Frame = frameNumber;
            }

            public Object Clone()
            {
                State obj = new State();

                obj.Status = Status;
                obj.Flags = Flags;
                return obj;
            }

            public int CompareTo(State o)
            {
                if (Status == o.Status)
                {
                    return 0;
                }

                return 1;
            }
        }

        public class Identity : IFrameState, ICloneable, IVersionedSerializable, IComparable<Identity>
        {
            public int Frame { get; set; }
            public String Name { get; set; }
            public List<Streamable> Dependencies = new List<Streamable>();  // Other files this one depends on
            public List<Streamable> Dependents = new List<Streamable>();
            public int LodTypeId { get; set; }

            public void Serialize(VersionedWriter writer)
            {
                writer.WriteStringRef(Name);        // Ref since version 26

                int depCount = Dependencies.Count;
                writer.Write((short)depCount);

                for (int x = 0; x < depCount; x++)
                {
                    writer.WriteStreamableRef(Dependencies[x]);
                }

                // Version 15: Dependents.
                depCount = Dependents.Count;
                writer.Write((short)depCount);

                for (int x = 0; x < depCount; x++)
                {
                    writer.WriteStreamableRef(Dependents[x]);
                }

                // Version 21: Lod type.
                writer.Write((byte) LodTypeId);
            }

            public void Deserialize(VersionedReader reader, int version)
            {
                if (version >= 26)
                {
                    Name = reader.ReadStringRef();
                }
                else
                {
                    Name = reader.ReadAndRegisterString();
                }

                int size = reader.ReadShort();

                for (int x = 0; x < size; x++)
                {
                    reader.AddStreamableRefNowOrLater(Dependencies);
                }

                if (version >= 15)
                {
                    size = reader.ReadShort();

                    for (int x = 0; x < size; x++)
                    {
                        reader.AddStreamableRefNowOrLater(Dependents);
                    }
                }

                if (version >= 21)
                {
                    LodTypeId = reader.ReadByte();

                    if (LodTypeId == 255)
                    {
                        LodTypeId = -1;
                    }
                }
            }

            // Add all our dependencies and their dependencies to the provided list, but
            // avoid duplicates.
            public void AddDependencies(int frameNumber, List<Streamable> result)
            {
                foreach (Streamable dep in Dependencies)
                {
                    if (!result.Contains(dep))
                    {
                        result.Add(dep);
                        dep.IdentityHistory.GetStateAt(frameNumber).AddDependencies(frameNumber, result);
                    }
                }
            }

            // Get all list of all the dependencies of this resource's dependencies (and theirs), but not
            // the direct dependencies of this streamable themselves.
            public List<Streamable> GetIndirectDependencies()
            {
                int frameNumber = GetFrameNumber();
                List<Streamable> result = new List<Streamable>();

                foreach (Streamable dep in Dependencies)
                {
                    dep.IdentityHistory.GetStateAt(frameNumber).AddDependencies(frameNumber, result);
                }

                return result;
            }

            public int GetFrameNumber()
            {
                return Frame;
            }

            public void SetFrameNumber(int frameNumber)
            {
                Frame = frameNumber;
            }

            public Object Clone()
            {
                Identity obj = new Identity();

                obj.Name = Name;
                // The dependencies are very likely to change, so let's not even copy them.
                
                return obj;
            }

            public int CompareTo(Identity o)
            {
                if (Dependencies.Count == o.Dependencies.Count && Name.Equals(o.Name))
                {
                    for (int x = 0; x < Dependencies.Count; x++)
                    {
                        if (Dependencies[x] != o.Dependencies[x])
                        {
                            return 1;
                        }
                    }

                    return 0;
                }

                return 1;
            }
        }


        public PerFrameState<State> History = new PerFrameState<State>();
        public PerFrameState<Identity> IdentityHistory = new PerFrameState<Identity>();

        // Streamable index. If positive, this is the strIndex that was used at run-time (and is stored
        // in the scene's streamable array, at that particular index). If negative,
        // it's a dummy streamable that doesn't correspond to a real streamable in the title.
        public int Index { get; set; }
        
        // This is the strIndex, as used in the game. (We may need to bump this to a 64-bit int if
        // we go ahead and make strIndices pointers.)
        public int StrIndex { get; set; }
        public Module Module { get; set; }
        public int VirtSize { get; set; }
        public int PhysSize { get; set; }
        public int FileSize { get; set; }

        public StreamDevice StreamDevice { get; set; }
        public Streamable ParentFile { get; set; }       // The RPF file this streamable is in.

        // NOTE that the LSN is only known if there was at least one request for this file.
        public int LSN { get; set; }

        public List<RscEvent> Events = new List<RscEvent>();

        // These entities are associated with this streamable at the present frame.
        public List<Entity> Entities = new List<Entity>();

        public String CurrentName { get { return (IdentityHistory.CurrentState != null) ? IdentityHistory.CurrentState.Name : ""; } }

        public String CurrentFlags
        {
            get
            {
                if (History.CurrentState == null)
                {
                    return "";
                }

                return Request.CreateFlagString(History.CurrentState.Flags);
            }
        }

        public Streamable.Status CurrentStatus { get { return (History.CurrentState != null) ? History.CurrentState.Status : Streamable.Status.NOTLOADED; } }

        // If true, the current main filter is hiding this streamable.
        public bool Filtered = false;

        public Streamable()
        {
            History.GetStateForWriting(0, null).Status = Streamable.Status.NOTLOADED;
        }

        public override string ToString()
        {
            return CurrentName;
        }

        public String GetNameAt(int frameNumber)
        {
            return IdentityHistory.GetStateAt(frameNumber).Name;
        }

        /* Gets the current state, i.e. the one at the currently selected frame - the one presented
         * to the user.
         */
        public Status GetCurrentStatus()
        {
            if (History.CurrentState != null)
            {
                return History.CurrentState.Status;
            }

            return Status.NOTLOADED;
        }

        public RscEvent GetLastRequest()
        {
            for (int x = Events.Count - 1; x >= 0; x--)
            {
                RscEvent rscEvent = Events[x];

                if (rscEvent.Action == RscEvent.ActionType.REQUEST)
                {
                    return rscEvent;
                }
            }

            return null;
        }

        public void SetStatus(int frameNumber, Status status, Scene scene)
        {
            Status oldStatus = History.PresentState.Status;
            if (oldStatus != status)
            {
                bool wasReq = (oldStatus == Status.LOADREQUESTED) || (oldStatus == Status.LOADING);
                bool isReq = (status == Status.LOADREQUESTED) || (status == Status.LOADING);
                bool isDummy = (VirtSize > 0 || PhysSize > 0);

                if (wasReq != isReq)
                {
                    if (isReq)
                    {
                        scene.GetPresentFrame().Reqs++;

                        if (!isDummy)
                        {
                            scene.GetPresentFrame().RealReqs++;
                        }
                    }
                    else
                    {
                        scene.GetPresentFrame().Reqs--;

                        if (!isDummy)
                        {
                            scene.GetPresentFrame().RealReqs--;
                        }
                    }
                }

                History.GetStateForWriting(frameNumber, scene).Status = status;
                History.ValidateDupe();
            }

            for (int x = 0; x < Entities.Count; x++)
            {
                Entities[x].ChangeLoadedState(frameNumber, status, scene);
            }
        }

        /** Starting backward from the given frame, this function returns the frame at which this streamable
         *  became resident. -1 if this cannot be computed (for example because the streamable actually was never
         *  resident at or before the specified frame).
         */
        public int GetFrameBecameResident(int currentFrame)
        {
            int stateIndex = History.GetStateIndexAt(currentFrame);

            if (stateIndex == -1)
            {
                return -1;
            }

            Debug.Assert(History.GetStateByIndex(stateIndex).Status == Streamable.Status.LOADED);

            while (--stateIndex >= 0)
            {
                if (History.GetStateByIndex(stateIndex).Status != Streamable.Status.LOADED)
                {
                    return History.GetStateByIndex(stateIndex + 1).Frame;
                }
            }

            // Could be that it's been resident from the start during the initial load, and we overwrote the previous
            // NOTLOADED/LOADREQUESTED/etc states.
            return 0;
        }

        /** Gets the frame at which this object became resident last. This will be on or before
         *  currentFrame.
         */
        public int GetFrameBecameOrWasResident(int currentFrame)
        {
            int stateIndex = History.GetStateIndexAt(currentFrame);

            if (stateIndex == -1)
            {
                return -1;
            }

            while (--stateIndex >= 0)
            {
                if (History.GetStateByIndex(stateIndex).Status != Streamable.Status.LOADED)
                {
                    return History.GetStateByIndex(stateIndex + 1).Frame;
                }
            }

            // Could be that it's been resident from the start during the initial load, and we overwrote the previous
            // NOTLOADED/LOADREQUESTED/etc states.
            return 0;
        }

        public int GetFrameBecameOrWillBeResident(int currentFrame)
        {
            int stateIndex = History.GetStateIndexAt(currentFrame);

            if (stateIndex == -1)
            {
                return -1;
            }

            // If we're already resident, see when we became resident.
            if (History.GetStateByIndex(stateIndex).Status == Streamable.Status.LOADED)
            {
                return GetFrameBecameResident(currentFrame);
            }

            // Before that, anything?
/*            while (--stateIndex >= 0)
            {
                if (History.GetStateByIndex(stateIndex).Status == Streamable.Status.UNREGISTERED)
                {
                    // Nothing this way - try different direction.
                    break;

                if (History.GetStateByIndex(stateIndex).Status != Streamable.Status.LOADED)
                {
                    // Here we are - when did this start?
                    return History.GetStateByIndex(stateIndex + 1).Frame;
                }
            }*/


            // See when we will be resident, if at all.
            int stateCount = History.StateCount;

            while (stateIndex < stateCount)
            {
                if (History.GetStateByIndex(stateIndex).Status == Streamable.Status.UNREGISTERED)
                {
                    // NEVER.
                    return -1;
                }

                if (History.GetStateByIndex(stateIndex).Status == Streamable.Status.LOADED)
                {
                    return History.GetStateByIndex(stateIndex).Frame;
                }

                stateIndex++;
            }

            return -1;
        }

        /* Find out when the first request was before currentFrame, or if there was none, the first
         * one after.
         */
        public int GetFirstRequestBefore(int currentFrame)
        {
            // TODO: Maybe a binary search can help here.
            int firstRequest = -1;
            bool lastWasRequest = false;

            int eventIndex = 0;
            int eventCount = Events.Count;

            while (eventIndex < eventCount)
            {
                int frameNumber = Events[eventIndex].FrameNumber;
                if (frameNumber > currentFrame && firstRequest != -1)
                {
                    return firstRequest;
                }

                RscEvent.ActionType action = Events[eventIndex].Action;

                if (action == RscEvent.ActionType.UNREGISTER)
                {
                    // If we already overshot, we never requested this streamable within the given timeframe, ever.
                    if (frameNumber > currentFrame)
                    {
                        return -1;
                    }

                    // Forget everything.
                    lastWasRequest = false;
                    firstRequest = -1;
                }
                else if (action == RscEvent.ActionType.REQUEST)
                {
                    if (!lastWasRequest)
                    {
                        firstRequest = frameNumber;
                    }

                    lastWasRequest = true;
                }
                else if (action == RscEvent.ActionType.Loading || action == RscEvent.ActionType.Unloaded || action == RscEvent.ActionType.Loaded || action == RscEvent.ActionType.UNREQUEST)
                {
                    lastWasRequest = false;
                }

                eventIndex++;
            }

            return firstRequest;

            /*
            int stateIndex = History.GetStateIndexAt(currentFrame);

            if (stateIndex == -1)
            {
                return -1;
            }

            // If we're already resident, see when we became resident.
            if (History.GetStateByIndex(stateIndex).Status == Streamable.Status.LOADREQUESTED)
            {
                // Keep going until we find the beginning of this state.
                while (stateIndex > 0)
                {
                    if (History.GetStateByIndex(stateIndex).Status != Streamable.Status.LOADREQUESTED)
                    {
                        break;
                    }

                    stateIndex--;
                }

                return History.GetStateByIndex(stateIndex).Frame;
            }

            // Find out when we did become resident.
            int stateTest = stateIndex;

            while (stateTest >= 0)
            {
                if (History.GetStateByIndex(stateTest).Status == Streamable.Status.LOADREQUESTED)
                {
                    return History.GetStateByIndex(stateTest).Frame;
                }

                stateTest--;
            }


            // Not requested yet - maybe later.
            int stateCount = History.StateCount;

            while (stateIndex < stateCount)
            {
                if (History.GetStateByIndex(stateIndex).Status == Streamable.Status.LOADREQUESTED)
                {
                    return History.GetStateByIndex(stateIndex).Frame;
                }

                stateIndex++;
            }

            return -1;*/
        }

        public void Serialize(VersionedWriter writer)
        {
            writer.Write(Index);
            //writer.Write(Module.Index);
            writer.Write(VirtSize);
            writer.Write(PhysSize);
            writer.Write((int) History.PresentState.Status);    // Now obsolete
            writer.WriteModuleRef(Module);

            // Version 31: strIndex.
            writer.Write(StrIndex);

            // Version 7: File size.
            writer.Write(FileSize);

            // Version 8: Parent file.
            writer.WriteStreamableRef(ParentFile);

            writer.Write(Events);
  //          writer.Write(Entities);

            // Version 13: Identity history.
            IdentityHistory.Serialize(writer);

            // Version 12: History.
            History.Serialize(writer);
        }

        public void Deserialize(VersionedReader reader, int version)
        {
            String Name = null;

            if (version < 13)
            {
                Name = reader.ReadString();
            }

            Index = reader.ReadInt();
            VirtSize = reader.ReadInt();
            PhysSize = reader.ReadInt();
            Streamable.Status status = (Status)reader.ReadInt();

            // We didn't save the history prior to version 12
            if (version < 12)
            {
                History.GetStateForWriting(0, null).Status = status;
            }

            Module = reader.ReadModuleRef();

            if (version >= 31)
            {
                StrIndex = reader.ReadInt();
            }
            else
            {
                StrIndex = Index;
            }

            if (version >= 7)
            {
                FileSize = reader.ReadInt();
            }
            else
            {
                FileSize = VirtSize + PhysSize;
            }

            if (version >= 8)
            {
                ParentFile = reader.ReadStreamableRef();
            }

            reader.ReadList(Events);

            if (version >= 13)
            {
                IdentityHistory.Deserialize(reader, version);
            }
            else
            {
                IdentityHistory.GetStateForWriting(0, null).Name = Name;
            }

            if (version >= 11)
            {
                if (version < 13)
                {
                    int size = reader.ReadShort();

                    for (int x = 0; x < size; x++)
                    {
                        IdentityHistory.PresentState.Dependencies.Add(reader.ReadStreamableRef());
                    }
                }
            }

            if (version >= 12)
            {
                History.Deserialize(reader, version);

                if (version <= 25)
                {
                    // Clean up.
                    History.RemoveAllDupes();
                }
            }
        }

        public void DeletePriorHistory(int frame)
        {
            int historyIndex = History.GetStateIndexAt(frame);

            if (historyIndex > 0)
            {
                History.DeleteAllBeforeIndex(frame);
            }

            // Find the newest event prior to the frame.
            for (int x = 0; x < Events.Count; x++)
            {
                if (Events[x].FrameNumber >= frame)
                {
                    if (x > 0)
                    {
                        // Let's go back and delete everything. We COULD be a bit more selective here.
                        Events.RemoveRange(0, x);
                    }

                    break;
                }
            }
        }
    }
}
