﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StreamingVisualize.Model
{
    public class NeededList : IVersionedSerializable
    {
        public class Entry : IVersionedSerializable
        {
            public const int ON_ODD = (1 << 0);



            public Entity entity;
            public float score;
            public int flags;

            public void Serialize(VersionedWriter writer)
            {
                writer.WriteEntityRef(entity);
                writer.Write(score);

                // Version 28: Flags
                writer.Write((byte)flags);
            }

            public void Deserialize(VersionedReader reader, int version)
            {
                entity = reader.ReadEntityDef();
                score = reader.ReadFloat();

                if (version >= 28)
                {
                    flags = (int)reader.ReadByte();
                }
                else
                {
                    flags = 0;
                }
            }
        }

        public List<Entry> List = new List<Entry>();



        public void Serialize(VersionedWriter writer)
        {
            writer.Write(List);
        }

        public void Deserialize(VersionedReader reader, int version)
        {
            reader.ReadList(List);
        }
    }
}
