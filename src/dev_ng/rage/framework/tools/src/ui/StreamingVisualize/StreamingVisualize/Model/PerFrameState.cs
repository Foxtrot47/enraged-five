﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace StreamingVisualize.Model
{
    public class PerFrameState<T> : IVersionedSerializable where T : class, IFrameState, ICloneable, IComparable<T>, IVersionedSerializable, new()
    {
        // List of all states, sorted by frame
        List<T> States = new List<T>();

        // The current state (as displayed to the user), can be null.
        public T CurrentState;

        // The state at the present. This will always point to something inside the state list.
        // It represents the state at the frame denoted in "LastChangeFrame".
        public T PresentState;

        // The current frame number, i.e the frame number of what's being shown in CurrentState.
        int CurrentFrame;

        // This is the number of the frame at which we last made a change to this state.
        private int LastChangeFrame { get; set; }

        public int StateCount { get { return States.Count; } }

        public class Record
        {
            public int count;
            public Object holder;  // This is PerFrameState<T>
        }

        public static Dictionary<Type, Record> RecordHolder = new Dictionary<Type, Record>();


        public PerFrameState()
        {
            CurrentState = PresentState = new T();
            PresentState.SetFrameNumber(0);
            States.Add(PresentState);
        }

        public void SetToState(int frameNumber)
        {
            if (CurrentFrame != frameNumber || CurrentState == null)
            {
                CurrentState = GetStateAt(frameNumber);
                CurrentFrame = frameNumber;
            }
        }

        public T GetStateByIndex(int index)
        {
            return States[index];
        }

        public int GetStateIndexAt(int frameNumber)
        {
            int count = States.Count;

            if (count == 0)
            {
                // No history.
                return -1;
            }

            int minVal = 0;
            int maxVal = count - 1;

            int curVal = maxVal / 2;

            while (true)
            {
                if (States[curVal].GetFrameNumber() <= frameNumber)
                {
                    // We're not there yet. Look to the right.
                    if (maxVal == curVal)
                    {
                        // Actually, we found it.
                        return curVal;
                    }

                    minVal = curVal;
                    curVal = (minVal + maxVal) / 2;

                    if (curVal == minVal)
                    {
                        curVal++;
                    }
                }
                else
                {
                    // We're too far. Let's go back.
                    // Can we even go back?
                    if (minVal == curVal)
                    {
                        // TODO: We could almost assert on this - curVal should be >0 at this point.
                        if (curVal > 0)
                        {
                            curVal--;
                        }

                        return curVal;
                    }

                    maxVal = curVal - 1;
                    curVal = (minVal + maxVal) / 2;
                }
            }

        }

        public T GetStateAt(int frameNumber)
        {
            int index = GetStateIndexAt(frameNumber);

            if (index == -1)
            {
                return null;
            }

            return States[index];
        }

        public void Serialize(VersionedWriter writer)
        {
            writer.Write(States.Count);

            foreach (T state in States)
            {
                writer.Write(state.GetFrameNumber());
                state.Serialize(writer);
            }
        }

        public void Deserialize(VersionedReader reader, int version)
        {
            int stateCount = reader.ReadInt();
            States.Clear();

            Debug.Assert(stateCount < 0xffffff);

            for (int x = 0; x < stateCount; x++ )
            {
                int frameNumber = reader.ReadInt();
                T state = new T();
                state.SetFrameNumber(frameNumber);
                state.Deserialize(reader, version);
                States.Add(state);
            }

            if (stateCount > 0)
            {
                PresentState = CurrentState = States[stateCount - 1];
                CurrentFrame = LastChangeFrame = CurrentState.GetFrameNumber();

                Record oldRecord;
                if (RecordHolder.TryGetValue(States[0].GetType(), out oldRecord))
                {
                    if (stateCount > oldRecord.count)
                    {
                        oldRecord.count = stateCount;
                        oldRecord.holder = this;
                    }
                }
                else
                {
                    oldRecord = new Record();
                    oldRecord.count = stateCount;
                    oldRecord.holder = this;

                    RecordHolder.Add(States[0].GetType(), oldRecord);
                }
            }
        }

        // This will get the present state with the intent of making a change to it.
        // If this is the first time this state is being changed this frame, it will be cloned.
        // If not, this function will return the existing present state.
        public T GetStateForWriting(Scene scene)
        {
            return GetStateForWriting(scene.PresentFrameNumber, scene);
        }

        // See the other variant of GetStateForWriting().
        // presentFrameNumber must be equal or higher than the highest
        // frame number used in this object - it's not possible to insert data before the last frame.
        public T GetStateForWriting(int presentFrameNumber, Scene scene)
        {
            T result = PresentState;

            if (LastChangeFrame != presentFrameNumber)
            {
                // We can't insert data from previous frame, only append new ones.
                Debug.Assert(States.Count == 0 || States[States.Count - 1].GetFrameNumber() < presentFrameNumber);

                // We need to make a copy.
                result = (T) PresentState.Clone();
                result.SetFrameNumber(presentFrameNumber);
                States.Add(result);

                PresentState = result;
                LastChangeFrame = presentFrameNumber;

                if (CurrentState == null || scene == null || scene.FollowMode)
                {
                    CurrentState = result;
                    CurrentFrame = presentFrameNumber;
                }
            }

            return result;
        }

        public void ValidateDupe()
        {
            if (States.Count > 1)
            {
             //   Debug.Assert(States[States.Count - 1].CompareTo(States[States.Count - 2]) != 0);
                if (States[States.Count - 1].CompareTo(States[States.Count - 2]) == 0)
                {
                    PopLast();
                }
            }
        }

        public void RemoveAllDupes()
        {
            for (int x = 1; x < States.Count; x++)
            {
                if (States[x - 1].CompareTo(States[x]) == 0)
                {
                    DeleteStateByIndexUnsafe(x);
                    x--;
                }
            }
        }

        // Remove the last state from the list. Typically only used during serialization, it's not really
        // that safe.
        public void PopLast()
        {
            if (CurrentState == PresentState)
            {
                CurrentState = States[StateCount - 2];
                CurrentFrame = CurrentState.GetFrameNumber();
            }

            PresentState = States[StateCount - 2];

            States.RemoveAt(States.Count - 1);
        }

        public void DeleteStateByIndexUnsafe(int index)
        {
            T state = States[index];

            if (CurrentState == state)
            {
                CurrentState = States[index - 1];
            }

            if (PresentState == state)
            {
                PresentState = States[index - 1];
            }

            States.RemoveAt(index);
        }

        // Populate this object from a list that is expected to have a sorted list of states.
        // It will use that list as its actual storage, so it takes ownership at this point.
        public void ImportFromList(List<T> src)
        {
            States = src;

            if (StateCount > 0)
            {
                PresentState = CurrentState = States[StateCount - 1];
                CurrentFrame = LastChangeFrame = CurrentState.GetFrameNumber();
            }
        }
/*
        public void AddNewState(Scene scene, int frameNumber, T state)
        {
            Debug.Assert(States.Count == 0 || States[States.Count - 1].GetFrameNumber() < frameNumber);

            States.Add(state);

            if (scene.FollowMode)
            {
                CurrentState = state;
                CurrentFrame = frameNumber;
            }
        }*/

        public void DeleteAllBeforeIndex(int index)
        {
            States.RemoveRange(0, index);

            // TODO: What if the current state points to one of them? Need to handle that.
        }
    }
}
