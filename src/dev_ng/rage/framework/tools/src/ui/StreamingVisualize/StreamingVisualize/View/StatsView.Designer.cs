﻿namespace StreamingVisualize.View
{
    partial class StatsView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Header = new System.Windows.Forms.Label();
            this.Streamables = new System.Windows.Forms.DataGridView();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fileSizeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ThroughPut = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.streamableEntryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.CopyToClipboard = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Streamables)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.streamableEntryBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // Header
            // 
            this.Header.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Header.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.Header.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Header.Location = new System.Drawing.Point(0, 0);
            this.Header.Name = "Header";
            this.Header.Size = new System.Drawing.Size(411, 13);
            this.Header.TabIndex = 1;
            this.Header.Text = "STATS";
            // 
            // Streamables
            // 
            this.Streamables.AllowUserToAddRows = false;
            this.Streamables.AllowUserToDeleteRows = false;
            this.Streamables.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Streamables.AutoGenerateColumns = false;
            this.Streamables.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Streamables.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameDataGridViewTextBoxColumn,
            this.fileSizeDataGridViewTextBoxColumn,
            this.ThroughPut});
            this.Streamables.DataSource = this.streamableEntryBindingSource;
            this.Streamables.Location = new System.Drawing.Point(193, 73);
            this.Streamables.Name = "Streamables";
            this.Streamables.ReadOnly = true;
            this.Streamables.RowHeadersVisible = false;
            this.Streamables.Size = new System.Drawing.Size(215, 158);
            this.Streamables.TabIndex = 2;
            this.Streamables.SelectionChanged += new System.EventHandler(this.Streamables_SelectionChanged);
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Name";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fileSizeDataGridViewTextBoxColumn
            // 
            this.fileSizeDataGridViewTextBoxColumn.DataPropertyName = "FileSize";
            this.fileSizeDataGridViewTextBoxColumn.HeaderText = "FileSize";
            this.fileSizeDataGridViewTextBoxColumn.Name = "fileSizeDataGridViewTextBoxColumn";
            this.fileSizeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // ThroughPut
            // 
            this.ThroughPut.DataPropertyName = "ThroughPut";
            this.ThroughPut.HeaderText = "ThroughPut";
            this.ThroughPut.Name = "ThroughPut";
            this.ThroughPut.ReadOnly = true;
            // 
            // streamableEntryBindingSource
            // 
            this.streamableEntryBindingSource.DataSource = typeof(StreamingVisualize.View.StatsView.StreamableEntry);
            // 
            // CopyToClipboard
            // 
            this.CopyToClipboard.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CopyToClipboard.Location = new System.Drawing.Point(368, 17);
            this.CopyToClipboard.Name = "CopyToClipboard";
            this.CopyToClipboard.Size = new System.Drawing.Size(40, 23);
            this.CopyToClipboard.TabIndex = 3;
            this.CopyToClipboard.Text = "Copy";
            this.CopyToClipboard.UseVisualStyleBackColor = true;
            this.CopyToClipboard.Click += new System.EventHandler(this.CopyToClipboard_Click);
            // 
            // StatsView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.CopyToClipboard);
            this.Controls.Add(this.Streamables);
            this.Controls.Add(this.Header);
            this.DoubleBuffered = true;
            this.Name = "StatsView";
            this.Size = new System.Drawing.Size(411, 234);
            this.Load += new System.EventHandler(this.StatsView_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.StatsView_Paint);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.StatsView_MouseDown);
            this.MouseEnter += new System.EventHandler(this.StatsView_MouseEnter);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.StatsView_MouseMove);
            this.Resize += new System.EventHandler(this.StatsView_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.Streamables)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.streamableEntryBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label Header;
        private System.Windows.Forms.DataGridView Streamables;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fileSizeDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource streamableEntryBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn ThroughPut;
        private System.Windows.Forms.Button CopyToClipboard;
    }
}
