﻿namespace StreamingVisualize.View
{
    partial class ResourceView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.ResourceName = new System.Windows.Forms.Label();
            this.ModuleName = new System.Windows.Forms.Label();
            this.VirtSize = new System.Windows.Forms.Label();
            this.PhysSize = new System.Windows.Forms.Label();
            this.EventHistory = new System.Windows.Forms.DataGridView();
            this.Frame = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Action = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Score = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Flags = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Reason = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Context = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StartTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IssueTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProcessTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LoadTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rscEventBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.FileSize = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ParentLink = new System.Windows.Forms.LinkLabel();
            this.StrIndex = new System.Windows.Forms.Label();
            this.DependencyGraph = new StreamingVisualize.View.Components.StreamableDependencyControl();
            this.ShowMapDependencies = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.EventHistory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rscEventBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // ResourceName
            // 
            this.ResourceName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ResourceName.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ResourceName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ResourceName.Location = new System.Drawing.Point(0, 0);
            this.ResourceName.Name = "ResourceName";
            this.ResourceName.Size = new System.Drawing.Size(443, 13);
            this.ResourceName.TabIndex = 0;
            this.ResourceName.Text = "NO RESOURCE SELECTED";
            // 
            // ModuleName
            // 
            this.ModuleName.AutoSize = true;
            this.ModuleName.Location = new System.Drawing.Point(0, 18);
            this.ModuleName.Name = "ModuleName";
            this.ModuleName.Size = new System.Drawing.Size(73, 13);
            this.ModuleName.TabIndex = 1;
            this.ModuleName.Text = "Module Name";
            // 
            // VirtSize
            // 
            this.VirtSize.AutoSize = true;
            this.VirtSize.Location = new System.Drawing.Point(0, 36);
            this.VirtSize.Name = "VirtSize";
            this.VirtSize.Size = new System.Drawing.Size(42, 13);
            this.VirtSize.TabIndex = 2;
            this.VirtSize.Text = "VirtSize";
            // 
            // PhysSize
            // 
            this.PhysSize.AutoSize = true;
            this.PhysSize.Location = new System.Drawing.Point(354, 36);
            this.PhysSize.Name = "PhysSize";
            this.PhysSize.Size = new System.Drawing.Size(50, 13);
            this.PhysSize.TabIndex = 3;
            this.PhysSize.Text = "PhysSize";
            // 
            // EventHistory
            // 
            this.EventHistory.AllowUserToAddRows = false;
            this.EventHistory.AllowUserToDeleteRows = false;
            this.EventHistory.AllowUserToResizeRows = false;
            this.EventHistory.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.EventHistory.AutoGenerateColumns = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.EventHistory.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.EventHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.EventHistory.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Frame,
            this.Action,
            this.Score,
            this.Flags,
            this.Reason,
            this.Context,
            this.StartTime,
            this.IssueTime,
            this.ProcessTime,
            this.LoadTime});
            this.EventHistory.DataSource = this.rscEventBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.EventHistory.DefaultCellStyle = dataGridViewCellStyle2;
            this.EventHistory.Location = new System.Drawing.Point(0, 193);
            this.EventHistory.Name = "EventHistory";
            this.EventHistory.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.EventHistory.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.EventHistory.RowHeadersVisible = false;
            this.EventHistory.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.EventHistory.Size = new System.Drawing.Size(446, 129);
            this.EventHistory.TabIndex = 4;
            this.EventHistory.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.EventHistory_CellFormatting);
            this.EventHistory.SelectionChanged += new System.EventHandler(this.EventHistory_SelectionChanged);
            // 
            // Frame
            // 
            this.Frame.DataPropertyName = "FrameNumber";
            this.Frame.HeaderText = "Frame";
            this.Frame.Name = "Frame";
            this.Frame.ReadOnly = true;
            // 
            // Action
            // 
            this.Action.DataPropertyName = "Action";
            this.Action.HeaderText = "Action";
            this.Action.Name = "Action";
            this.Action.ReadOnly = true;
            // 
            // Score
            // 
            this.Score.DataPropertyName = "Score";
            this.Score.HeaderText = "Score";
            this.Score.Name = "Score";
            this.Score.ReadOnly = true;
            // 
            // Flags
            // 
            this.Flags.DataPropertyName = "Flags";
            this.Flags.HeaderText = "Flags";
            this.Flags.Name = "Flags";
            this.Flags.ReadOnly = true;
            // 
            // Reason
            // 
            this.Reason.DataPropertyName = "Reason";
            this.Reason.HeaderText = "Reason";
            this.Reason.Name = "Reason";
            this.Reason.ReadOnly = true;
            this.Reason.Visible = false;
            // 
            // Context
            // 
            this.Context.DataPropertyName = "Context";
            this.Context.HeaderText = "Context";
            this.Context.Name = "Context";
            this.Context.ReadOnly = true;
            // 
            // StartTime
            // 
            this.StartTime.DataPropertyName = "StartTime";
            this.StartTime.HeaderText = "StartTime";
            this.StartTime.Name = "StartTime";
            this.StartTime.ReadOnly = true;
            // 
            // IssueTime
            // 
            this.IssueTime.DataPropertyName = "IssueTime";
            this.IssueTime.HeaderText = "IssueTime";
            this.IssueTime.Name = "IssueTime";
            this.IssueTime.ReadOnly = true;
            // 
            // ProcessTime
            // 
            this.ProcessTime.DataPropertyName = "ProcessTime";
            this.ProcessTime.HeaderText = "ProcessTime";
            this.ProcessTime.Name = "ProcessTime";
            this.ProcessTime.ReadOnly = true;
            // 
            // LoadTime
            // 
            this.LoadTime.DataPropertyName = "LoadTime";
            this.LoadTime.HeaderText = "LoadTime";
            this.LoadTime.Name = "LoadTime";
            this.LoadTime.ReadOnly = true;
            // 
            // rscEventBindingSource
            // 
            this.rscEventBindingSource.DataSource = typeof(StreamingVisualize.Model.RscEvent);
            // 
            // FileSize
            // 
            this.FileSize.AutoSize = true;
            this.FileSize.Location = new System.Drawing.Point(354, 17);
            this.FileSize.Name = "FileSize";
            this.FileSize.Size = new System.Drawing.Size(43, 13);
            this.FileSize.TabIndex = 5;
            this.FileSize.Text = "FileSize";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(0, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Parent:";
            // 
            // ParentLink
            // 
            this.ParentLink.AutoSize = true;
            this.ParentLink.Location = new System.Drawing.Point(48, 54);
            this.ParentLink.Name = "ParentLink";
            this.ParentLink.Size = new System.Drawing.Size(0, 13);
            this.ParentLink.TabIndex = 7;
            this.ParentLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.ParentLink_LinkClicked);
            // 
            // StrIndex
            // 
            this.StrIndex.AutoSize = true;
            this.StrIndex.Location = new System.Drawing.Point(354, 54);
            this.StrIndex.Name = "StrIndex";
            this.StrIndex.Size = new System.Drawing.Size(44, 13);
            this.StrIndex.TabIndex = 11;
            this.StrIndex.Text = "strIndex";
            // 
            // DependencyGraph
            // 
            this.DependencyGraph.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DependencyGraph.AutoScroll = true;
            this.DependencyGraph.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.DependencyGraph.Location = new System.Drawing.Point(3, 94);
            this.DependencyGraph.Name = "DependencyGraph";
            this.DependencyGraph.Size = new System.Drawing.Size(440, 93);
            this.DependencyGraph.TabIndex = 10;
            this.DependencyGraph.MouseDown += new System.Windows.Forms.MouseEventHandler(this.DependencyGraph_MouseDown);
            // 
            // ShowMapDependencies
            // 
            this.ShowMapDependencies.AutoSize = true;
            this.ShowMapDependencies.Location = new System.Drawing.Point(3, 71);
            this.ShowMapDependencies.Name = "ShowMapDependencies";
            this.ShowMapDependencies.Size = new System.Drawing.Size(146, 17);
            this.ShowMapDependencies.TabIndex = 12;
            this.ShowMapDependencies.Text = "Show map dependencies";
            this.ShowMapDependencies.UseVisualStyleBackColor = true;
            this.ShowMapDependencies.CheckedChanged += new System.EventHandler(this.ShowMapDependencies_CheckedChanged);
            // 
            // ResourceView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.ShowMapDependencies);
            this.Controls.Add(this.StrIndex);
            this.Controls.Add(this.DependencyGraph);
            this.Controls.Add(this.ParentLink);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.FileSize);
            this.Controls.Add(this.EventHistory);
            this.Controls.Add(this.PhysSize);
            this.Controls.Add(this.VirtSize);
            this.Controls.Add(this.ModuleName);
            this.Controls.Add(this.ResourceName);
            this.Name = "ResourceView";
            this.Size = new System.Drawing.Size(446, 322);
            this.Load += new System.EventHandler(this.ResourceView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.EventHistory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rscEventBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label ResourceName;
        public System.Windows.Forms.Label ModuleName;
        private System.Windows.Forms.Label VirtSize;
        private System.Windows.Forms.Label PhysSize;
        private System.Windows.Forms.DataGridView EventHistory;
        private System.Windows.Forms.BindingSource rscEventBindingSource;
        //private System.Windows.Forms.DataGridViewTextBoxColumn streamableDataGridViewTextBoxColumn;
        private System.Windows.Forms.Label FileSize;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.LinkLabel ParentLink;
        private Components.StreamableDependencyControl DependencyGraph;
        private System.Windows.Forms.Label StrIndex;
        private System.Windows.Forms.DataGridViewTextBoxColumn Frame;
        private System.Windows.Forms.DataGridViewTextBoxColumn Action;
        private System.Windows.Forms.DataGridViewTextBoxColumn Score;
        private System.Windows.Forms.DataGridViewTextBoxColumn Flags;
        private System.Windows.Forms.DataGridViewTextBoxColumn Reason;
        private System.Windows.Forms.DataGridViewTextBoxColumn Context;
        private System.Windows.Forms.DataGridViewTextBoxColumn StartTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn IssueTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProcessTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn LoadTime;
        private System.Windows.Forms.CheckBox ShowMapDependencies;
    }
}
