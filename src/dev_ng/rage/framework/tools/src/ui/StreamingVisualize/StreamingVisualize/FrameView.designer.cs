﻿namespace StreamingVisualize.View
{
    partial class FrameView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.Requests = new System.Windows.Forms.DataGridView();
            this.Context = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Score = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FrameNumber = new System.Windows.Forms.Label();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Unrequests = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.CamPos = new System.Windows.Forms.Label();
            this.StreamingVolume = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.MasterCutoff = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.FailedAllocs = new System.Windows.Forms.DataGridView();
            this.streamableDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reasonDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.failedAllocBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.Streamable = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.frameDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.streamableDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flagsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.scoreDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.requestBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.frameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.streamableDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unrequestBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.Requests)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Unrequests)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FailedAllocs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.failedAllocBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.requestBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.unrequestBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // Requests
            // 
            this.Requests.AllowUserToAddRows = false;
            this.Requests.AllowUserToDeleteRows = false;
            this.Requests.AllowUserToResizeRows = false;
            this.Requests.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Requests.AutoGenerateColumns = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Requests.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.Requests.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Requests.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Streamable,
            this.Context,
            this.Score,
            this.frameDataGridViewTextBoxColumn1,
            this.streamableDataGridViewTextBoxColumn1,
            this.flagsDataGridViewTextBoxColumn,
            this.contextDataGridViewTextBoxColumn1,
            this.scoreDataGridViewTextBoxColumn});
            this.Requests.DataSource = this.requestBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Requests.DefaultCellStyle = dataGridViewCellStyle2;
            this.Requests.Location = new System.Drawing.Point(0, 16);
            this.Requests.Name = "Requests";
            this.Requests.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Requests.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.Requests.RowHeadersVisible = false;
            this.Requests.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Requests.Size = new System.Drawing.Size(290, 273);
            this.Requests.TabIndex = 0;
            this.Requests.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Requests_CellContentClick);
            this.Requests.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.Requests_CellFormatting);
            this.Requests.SelectionChanged += new System.EventHandler(this.Requests_SelectionChanged);
            // 
            // Context
            // 
            this.Context.DataPropertyName = "Context";
            this.Context.HeaderText = "Context";
            this.Context.Name = "Context";
            this.Context.ReadOnly = true;
            // 
            // Score
            // 
            this.Score.DataPropertyName = "Score";
            this.Score.HeaderText = "Score";
            this.Score.Name = "Score";
            this.Score.ReadOnly = true;
            // 
            // FrameNumber
            // 
            this.FrameNumber.AutoSize = true;
            this.FrameNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FrameNumber.Location = new System.Drawing.Point(0, 3);
            this.FrameNumber.Name = "FrameNumber";
            this.FrameNumber.Size = new System.Drawing.Size(106, 13);
            this.FrameNumber.TabIndex = 1;
            this.FrameNumber.Text = "FRAME NUMBER";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Streamable";
            this.dataGridViewTextBoxColumn1.HeaderText = "Streamable";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Streamable";
            this.dataGridViewTextBoxColumn2.HeaderText = "Streamable";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // Unrequests
            // 
            this.Unrequests.AllowUserToAddRows = false;
            this.Unrequests.AllowUserToDeleteRows = false;
            this.Unrequests.AllowUserToResizeRows = false;
            this.Unrequests.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Unrequests.AutoGenerateColumns = false;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Unrequests.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.Unrequests.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Unrequests.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.frameDataGridViewTextBoxColumn,
            this.streamableDataGridViewTextBoxColumn,
            this.contextDataGridViewTextBoxColumn});
            this.Unrequests.DataSource = this.unrequestBindingSource;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Unrequests.DefaultCellStyle = dataGridViewCellStyle5;
            this.Unrequests.Location = new System.Drawing.Point(2, 16);
            this.Unrequests.Name = "Unrequests";
            this.Unrequests.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Unrequests.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.Unrequests.RowHeadersVisible = false;
            this.Unrequests.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Unrequests.Size = new System.Drawing.Size(249, 273);
            this.Unrequests.TabIndex = 3;
            this.Unrequests.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Unrequests_CellContentClick);
            this.Unrequests.SelectionChanged += new System.EventHandler(this.Unrequests_SelectionChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(-3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "REQUESTS:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "UNREQUESTS:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Camera Pos:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(3, 37);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(112, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Streaming Volume:";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Streamable";
            this.dataGridViewTextBoxColumn3.HeaderText = "Streamable";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Location = new System.Drawing.Point(0, 53);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.Requests);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.Unrequests);
            this.splitContainer1.Panel2.Controls.Add(this.label2);
            this.splitContainer1.Size = new System.Drawing.Size(560, 294);
            this.splitContainer1.SplitterDistance = 298;
            this.splitContainer1.TabIndex = 8;
            // 
            // CamPos
            // 
            this.CamPos.AutoSize = true;
            this.CamPos.Location = new System.Drawing.Point(121, 20);
            this.CamPos.Name = "CamPos";
            this.CamPos.Size = new System.Drawing.Size(67, 13);
            this.CamPos.TabIndex = 9;
            this.CamPos.Text = "Camera Pos:";
            // 
            // StreamingVolume
            // 
            this.StreamingVolume.AutoSize = true;
            this.StreamingVolume.Location = new System.Drawing.Point(121, 37);
            this.StreamingVolume.Name = "StreamingVolume";
            this.StreamingVolume.Size = new System.Drawing.Size(35, 13);
            this.StreamingVolume.TabIndex = 10;
            this.StreamingVolume.Text = "label3";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(403, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Master Cutoff:";
            // 
            // MasterCutoff
            // 
            this.MasterCutoff.AutoSize = true;
            this.MasterCutoff.Location = new System.Drawing.Point(497, 18);
            this.MasterCutoff.Name = "MasterCutoff";
            this.MasterCutoff.Size = new System.Drawing.Size(40, 13);
            this.MasterCutoff.TabIndex = 12;
            this.MasterCutoff.Text = "1.2345";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(-3, 350);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Failed Allocations:";
            // 
            // FailedAllocs
            // 
            this.FailedAllocs.AllowUserToAddRows = false;
            this.FailedAllocs.AllowUserToDeleteRows = false;
            this.FailedAllocs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FailedAllocs.AutoGenerateColumns = false;
            this.FailedAllocs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.FailedAllocs.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.streamableDataGridViewTextBoxColumn2,
            this.reasonDataGridViewTextBoxColumn1});
            this.FailedAllocs.DataSource = this.failedAllocBindingSource;
            this.FailedAllocs.Location = new System.Drawing.Point(0, 366);
            this.FailedAllocs.Name = "FailedAllocs";
            this.FailedAllocs.ReadOnly = true;
            this.FailedAllocs.RowHeadersVisible = false;
            this.FailedAllocs.Size = new System.Drawing.Size(554, 91);
            this.FailedAllocs.TabIndex = 14;
            // 
            // streamableDataGridViewTextBoxColumn2
            // 
            this.streamableDataGridViewTextBoxColumn2.DataPropertyName = "Streamable";
            this.streamableDataGridViewTextBoxColumn2.FillWeight = 200F;
            this.streamableDataGridViewTextBoxColumn2.HeaderText = "Streamable";
            this.streamableDataGridViewTextBoxColumn2.Name = "streamableDataGridViewTextBoxColumn2";
            this.streamableDataGridViewTextBoxColumn2.ReadOnly = true;
            this.streamableDataGridViewTextBoxColumn2.Width = 200;
            // 
            // reasonDataGridViewTextBoxColumn1
            // 
            this.reasonDataGridViewTextBoxColumn1.DataPropertyName = "Reason";
            this.reasonDataGridViewTextBoxColumn1.FillWeight = 300F;
            this.reasonDataGridViewTextBoxColumn1.HeaderText = "Reason";
            this.reasonDataGridViewTextBoxColumn1.Name = "reasonDataGridViewTextBoxColumn1";
            this.reasonDataGridViewTextBoxColumn1.ReadOnly = true;
            this.reasonDataGridViewTextBoxColumn1.Width = 300;
            // 
            // failedAllocBindingSource
            // 
            this.failedAllocBindingSource.DataSource = typeof(StreamingVisualize.Model.FailedAlloc);
            // 
            // Streamable
            // 
            this.Streamable.DataPropertyName = "Streamable";
            this.Streamable.HeaderText = "Streamable";
            this.Streamable.Name = "Streamable";
            this.Streamable.ReadOnly = true;
            // 
            // frameDataGridViewTextBoxColumn1
            // 
            this.frameDataGridViewTextBoxColumn1.DataPropertyName = "Frame";
            this.frameDataGridViewTextBoxColumn1.HeaderText = "Frame";
            this.frameDataGridViewTextBoxColumn1.Name = "frameDataGridViewTextBoxColumn1";
            this.frameDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // streamableDataGridViewTextBoxColumn1
            // 
            this.streamableDataGridViewTextBoxColumn1.DataPropertyName = "Streamable";
            this.streamableDataGridViewTextBoxColumn1.HeaderText = "Streamable";
            this.streamableDataGridViewTextBoxColumn1.Name = "streamableDataGridViewTextBoxColumn1";
            this.streamableDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // flagsDataGridViewTextBoxColumn
            // 
            this.flagsDataGridViewTextBoxColumn.DataPropertyName = "Flags";
            this.flagsDataGridViewTextBoxColumn.HeaderText = "Flags";
            this.flagsDataGridViewTextBoxColumn.Name = "flagsDataGridViewTextBoxColumn";
            this.flagsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // contextDataGridViewTextBoxColumn1
            // 
            this.contextDataGridViewTextBoxColumn1.DataPropertyName = "Context";
            this.contextDataGridViewTextBoxColumn1.HeaderText = "Context";
            this.contextDataGridViewTextBoxColumn1.Name = "contextDataGridViewTextBoxColumn1";
            this.contextDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // scoreDataGridViewTextBoxColumn
            // 
            this.scoreDataGridViewTextBoxColumn.DataPropertyName = "Score";
            this.scoreDataGridViewTextBoxColumn.HeaderText = "Score";
            this.scoreDataGridViewTextBoxColumn.Name = "scoreDataGridViewTextBoxColumn";
            this.scoreDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // requestBindingSource
            // 
            this.requestBindingSource.DataSource = typeof(StreamingVisualize.Model.Request);
            // 
            // frameDataGridViewTextBoxColumn
            // 
            this.frameDataGridViewTextBoxColumn.DataPropertyName = "Frame";
            this.frameDataGridViewTextBoxColumn.HeaderText = "Frame";
            this.frameDataGridViewTextBoxColumn.Name = "frameDataGridViewTextBoxColumn";
            this.frameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // streamableDataGridViewTextBoxColumn
            // 
            this.streamableDataGridViewTextBoxColumn.DataPropertyName = "Streamable";
            this.streamableDataGridViewTextBoxColumn.HeaderText = "Streamable";
            this.streamableDataGridViewTextBoxColumn.Name = "streamableDataGridViewTextBoxColumn";
            this.streamableDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // contextDataGridViewTextBoxColumn
            // 
            this.contextDataGridViewTextBoxColumn.DataPropertyName = "Context";
            this.contextDataGridViewTextBoxColumn.HeaderText = "Context";
            this.contextDataGridViewTextBoxColumn.Name = "contextDataGridViewTextBoxColumn";
            this.contextDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // unrequestBindingSource
            // 
            this.unrequestBindingSource.DataSource = typeof(StreamingVisualize.Model.Unrequest);
            // 
            // FrameView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.FailedAllocs);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.MasterCutoff);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.StreamingVolume);
            this.Controls.Add(this.CamPos);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.FrameNumber);
            this.Name = "FrameView";
            this.Size = new System.Drawing.Size(563, 461);
            ((System.ComponentModel.ISupportInitialize)(this.Requests)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Unrequests)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.FailedAllocs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.failedAllocBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.requestBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.unrequestBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.DataGridView Requests;
        public System.Windows.Forms.BindingSource requestBindingSource;
        private System.Windows.Forms.Label FrameNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridView Unrequests;
        private System.Windows.Forms.BindingSource unrequestBindingSource;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Streamable;
        private System.Windows.Forms.DataGridViewTextBoxColumn Context;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Score;
        private System.Windows.Forms.Label CamPos;
        private System.Windows.Forms.Label StreamingVolume;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label MasterCutoff;
        private System.Windows.Forms.DataGridViewTextBoxColumn frameDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn streamableDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn flagsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn contextDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn scoreDataGridViewTextBoxColumn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView FailedAllocs;
        private System.Windows.Forms.BindingSource failedAllocBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn frameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn streamableDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn contextDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn streamableDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn reasonDataGridViewTextBoxColumn1;
    }
}
