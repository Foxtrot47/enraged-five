﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StreamingVisualize.Model;
using StreamingVisualize.Controller;
using StreamingVisualize.View.Components;

namespace StreamingVisualize.View
{
    public partial class EntityHistoryView : UserControl, IViewController
    {
        public class EntityHistoryItem
        {
            public enum EventType
            {
                CREATE,
                DESTROY,
                NEW_ARCHETYPE,
                CREATED_IN_MAP,
                DELETED_IN_MAP,
                REQUESTED,
                LOADED,
                UNLOADED,
                VISIBLE,
                INVISIBLE,
                VISIBLE_MISSING,
                DEP_REQ,
                DEP_LOADING,
                DEP_LOADED,
                DEP_UNLOADED,
                INDIR_DEP_REQ,
                INDIR_DEP_LOADING,
                INDIR_DEP_LOADED,
                INDIR_DEP_UNLOADED,
                FUTURE_DEP_LOADING,
                FUTURE_DEP_REQUESTED,
                FUTURE_DEP_LOADED,
                WTF,            // LOADING on an archetype
            }

            public EventType Event { get; set; }

            public int FrameNumber { get; set; }

            public Streamable Streamable { get; set; }

            public LodType LodType { get; set; }

            public EntityType EntityType { get; set; }

            public int GUID { get; set; }

            public String Context { get; set; }

            public float Score { get; set; }

            public int CompareFrame(EntityHistoryItem o)
            {
                if (o.FrameNumber == FrameNumber)
                {
                    return 0;
                }

                return (o.FrameNumber > FrameNumber) ? -1 : 1;
            }
        }

        private enum Relationship
        {
            DEP,
            INDIR_DEP,
            SELF,
        }

        public MainController Controller;

        public Scene Scene;

        private ViewContextMenu ViewContextMenu = new ViewContextMenu();

        private Entity Entity;

        private int FrameNumber;

        private List<EntityHistoryItem> EntityHistory = new List<EntityHistoryItem>();

        private bool ChangingSelection = false;

        private int StreamableColIdx;
        private int FrameNumberColIdx;
        private int EventColIdx;
        private int GuidColIdx;

        private ContextMenuHelper.ToggleBool ShowIndirDeps = new ContextMenuHelper.ToggleBool(true);



        private void AddStreamableEvents(List<Streamable> deps, int firstFrame, int lastFrame, Relationship relationship, List<EntityHistoryItem> result)
        {
            foreach (Streamable streamable in deps)
            {
                AddStreamableEvents(streamable, firstFrame, lastFrame, relationship, result);
            }
        }

        private void AddStreamableEvents(Streamable streamable, int firstFrame, int lastFrame, Relationship relationship, List<EntityHistoryItem> result)
        {
            int eventCount = streamable.Events.Count;
            int index = 0;
            Streamable.Status lastState = Streamable.Status.NOTLOADED;

            while (index < eventCount)
            {
                RscEvent rscEvent = streamable.Events[index];

                // Let's first find the state at the time when this became a relevant frame.
                if (rscEvent.FrameNumber < firstFrame)
                {
                    Streamable.Status newState = rscEvent.GetStatusChangeWithContext();

                    if (newState != Streamable.Status.INVALID)
                    {
                        lastState = newState;
                    }
                }
                else
                {
                    break;
                }

                index++;
            }

            // If resident already, go back in time and find out when it became resident.
            if (lastState != Streamable.Status.UNREGISTERED && lastState != Streamable.Status.NOTLOADED && relationship != Relationship.SELF)
            {
                int retroTestIndex = index - 1;

                while (retroTestIndex >= 0)
                {
                    RscEvent rscEvent = streamable.Events[retroTestIndex];
                    Streamable.Status newState = rscEvent.GetStatusChangeWithContext();

                    if (newState != Streamable.Status.INVALID)
                    {
                        if (newState == Streamable.Status.NOTLOADED || newState == Streamable.Status.UNREGISTERED)
                        {
                            // Okay, we know the full history now.
                            break;
                        }

                        EntityHistoryItem item = new EntityHistoryItem();
                        item.FrameNumber = rscEvent.FrameNumber;
                        item.Streamable = streamable;

                        switch (newState)
                        {
                            case Streamable.Status.LOADED:
                                item.Event = EntityHistoryItem.EventType.FUTURE_DEP_LOADED;
                                break;

                            case Streamable.Status.LOADING:
                                item.Event = EntityHistoryItem.EventType.FUTURE_DEP_LOADING;
                                break;

                            case Streamable.Status.LOADREQUESTED:
                                item.Event = EntityHistoryItem.EventType.FUTURE_DEP_REQUESTED;
                                break;
                        }

                        item.Context = rscEvent.Context;
                        item.Score = rscEvent.Score;
                        result.Add(item);
                    }

                    retroTestIndex--;
                }
            }

            // Now get the history until we don't care anymore.
            while (index < eventCount)
            {
                RscEvent rscEvent = streamable.Events[index];

                // Let's first find the state at the time when this became a relevant frame.
                if (rscEvent.FrameNumber <= lastFrame)
                {
                    Streamable.Status newState = rscEvent.GetStatusChangeWithContext();

                    if (newState != Streamable.Status.INVALID)
                    {
                        EntityHistoryItem item = new EntityHistoryItem();
                        item.FrameNumber = rscEvent.FrameNumber;
                        item.Streamable = streamable;

                        switch (relationship)
                        {
                            case Relationship.INDIR_DEP:
                                switch (newState)
                                {
                                    case Streamable.Status.NOTLOADED:
                                        item.Event = EntityHistoryItem.EventType.INDIR_DEP_UNLOADED;
                                        break;

                                    case Streamable.Status.LOADED:
                                        item.Event = EntityHistoryItem.EventType.INDIR_DEP_LOADED;
                                        break;

                                    case Streamable.Status.LOADING:
                                        item.Event = EntityHistoryItem.EventType.INDIR_DEP_LOADING;
                                        break;

                                    case Streamable.Status.LOADREQUESTED:
                                        item.Event = EntityHistoryItem.EventType.INDIR_DEP_REQ;
                                        break;
                                }
                                break;

                            case Relationship.DEP:
                                switch (newState)
                                {
                                    case Streamable.Status.NOTLOADED:
                                        item.Event = EntityHistoryItem.EventType.DEP_UNLOADED;
                                        break;

                                    case Streamable.Status.LOADED:
                                        item.Event = EntityHistoryItem.EventType.DEP_LOADED;
                                        break;

                                    case Streamable.Status.LOADING:
                                        item.Event = EntityHistoryItem.EventType.DEP_LOADING;
                                        break;

                                    case Streamable.Status.LOADREQUESTED:
                                        item.Event = EntityHistoryItem.EventType.DEP_REQ;
                                        break;
                                }
                                break;

                            case Relationship.SELF:
                                switch (newState)
                                {
                                    case Streamable.Status.NOTLOADED:
                                        item.Event = EntityHistoryItem.EventType.UNLOADED;
                                        break;

                                    case Streamable.Status.LOADED:
                                        item.Event = EntityHistoryItem.EventType.LOADED;
                                        break;

                                    case Streamable.Status.LOADING:
                                        // Shouldn't be getting this on an archetype!
                                        item.Event = EntityHistoryItem.EventType.WTF;
                                        break;

                                    case Streamable.Status.LOADREQUESTED:
                                        item.Event = EntityHistoryItem.EventType.REQUESTED;
                                        break;
                                }
                                break;
                        }

                        item.Context = rscEvent.Context;
                        item.Score = rscEvent.Score;
                        result.Add(item);
                    }
                }
                else
                {
                    break;
                }

                index++;
            }
        }

        private List<EntityHistoryItem> CreateHistory(Entity entity)
        {
            Streamable lastArchetype = null;
            Streamable lastMapData = null;

            List<Streamable> lastDeps = new List<Streamable>();
            List<Streamable> lastIndirDeps = new List<Streamable>();

            List<EntityHistoryItem> result = new List<EntityHistoryItem>();

            LodType lastLodType = null;
            EntityType lastEntityType = null;
            //Streamable.Status lastLoadedState = Streamable.Status.NOTLOADED;

            int stateIndex = 0;
            int stateCount = entity.NewHistory.StateCount;

            // Frame when we last got a new streamable.
            int lastStreamableStartFrame = 0;

            //int lastVisible = 0;
            //Entity.NewState.LodStates lastLodState = Entity.NewState.LodStates.LOD_OK;
            EntityHistoryItem.EventType lastVisible = EntityHistoryItem.EventType.INVISIBLE;  // We're just using the visiblity enums here

            // Ignore everything until the last REGISTER_ARCHETYPE before the current frame.
            int lastRegister = 0;
            Streamable lastStreamable = null;

            while (stateIndex < stateCount)
            {
                Entity.NewState state = entity.NewHistory.GetStateByIndex(stateIndex);
                int frameNumber = state.GetFrameNumber();

                if (frameNumber >= FrameNumber)
                {
                    break;
                }

                if (state.Streamable != lastStreamable)
                {
                    lastStreamable = state.Streamable;
                    lastRegister = stateIndex;
                }

                stateIndex++;
            }

            stateIndex = lastRegister;

            while (stateIndex < stateCount)
            {
                Entity.NewState state = entity.NewHistory.GetStateByIndex(stateIndex);
                int frameNumber = state.GetFrameNumber();

                // Keep track of all changes to our dependencies.


                if (Scene.GetLodType(state.LodType) != lastLodType)
                {
                    lastLodType = Scene.GetLodType(state.LodType);
                }

                if (Scene.GetEntityType(state.EntityType) != lastEntityType)
                {
                    lastEntityType = Scene.GetEntityType(state.EntityType);
                }

                if (state.Streamable != lastArchetype)
                {
                    if (state.Streamable == null)
                    {
                        EntityHistoryItem item = new EntityHistoryItem();
                        item.FrameNumber = frameNumber;
                        item.Event = EntityHistoryItem.EventType.DESTROY;
                        result.Add(item);

                        // Add streamable events for all our kids and grandkids.
                        AddStreamableEvents(lastDeps, lastStreamableStartFrame, frameNumber, Relationship.DEP, result);
                        AddStreamableEvents(lastIndirDeps, lastStreamableStartFrame, frameNumber, Relationship.INDIR_DEP, result);
                        AddStreamableEvents(lastArchetype, lastStreamableStartFrame, frameNumber, Relationship.SELF, result);

                        lastMapData = null;
                        lastDeps = new List<Streamable>();
                        lastIndirDeps = new List<Streamable>();
                    }
                    else
                    {
                        if (lastArchetype != null)
                        {
                            // Add streamable events for all our kids and grandkids.
                            AddStreamableEvents(lastDeps, lastStreamableStartFrame, frameNumber, Relationship.DEP, result);
                            AddStreamableEvents(lastIndirDeps, lastStreamableStartFrame, frameNumber, Relationship.INDIR_DEP, result);
                            AddStreamableEvents(lastArchetype, lastStreamableStartFrame, frameNumber, Relationship.SELF, result);
                        }

                        EntityHistoryItem item = new EntityHistoryItem();
                        item.FrameNumber = frameNumber;
                        item.Event = EntityHistoryItem.EventType.NEW_ARCHETYPE;
                        item.Streamable = state.Streamable;
                        item.GUID = state.GUID;
                        result.Add(item);

                        if (item.FrameNumber > FrameNumber)
                        {
                            break;
                        }

                        lastDeps = state.Streamable.IdentityHistory.GetStateAt(frameNumber).Dependencies;
                        lastStreamableStartFrame = frameNumber;

                        if (ShowIndirDeps.Value)
                        {
                            lastIndirDeps = state.Streamable.IdentityHistory.GetStateAt(frameNumber).GetIndirectDependencies();
                        }
                    }

                    lastArchetype = state.Streamable;
                }

                if (state.MapDataStreamable != lastMapData)
                {
                    if (state.MapDataStreamable != null && state.Streamable != null)
                    {
                        EntityHistoryItem item = new EntityHistoryItem();
                        item.FrameNumber = frameNumber;
                        item.Event = EntityHistoryItem.EventType.CREATED_IN_MAP;
                        item.Streamable = state.MapDataStreamable;
                        item.LodType = lastLodType;
                        item.EntityType = lastEntityType;
                        item.GUID = state.GUID;
                        result.Add(item);
                    }

                    lastMapData = state.MapDataStreamable;  // TODO: Maybe don't update it if state.Streamable == null?
                }

                EntityHistoryItem.EventType visibleState;

                if (state.LodState != Entity.NewState.LodStates.LOD_OK)
                {
                    visibleState = EntityHistoryItem.EventType.VISIBLE_MISSING;
                }
                else
                {
                    visibleState = (state.IsVisible(Scene)) ? EntityHistoryItem.EventType.VISIBLE : EntityHistoryItem.EventType.INVISIBLE;
                }

                if (visibleState != lastVisible)
                {
                    EntityHistoryItem item = new EntityHistoryItem();
                    item.FrameNumber = frameNumber;
                    item.Event = visibleState;
                    item.Streamable = state.Streamable;
                    item.LodType = lastLodType;
                    item.EntityType = lastEntityType;
                    item.GUID = state.GUID;
                    result.Add(item);

                    lastVisible = visibleState;
                }
                /*
                if (state.LoadedState != lastLoadedState)
                {
                    EntityHistoryItem item = new EntityHistoryItem();
                    item.FrameNumber = frameNumber;

                    // We shouldn't get any of these.
                    if (state.LoadedState != Streamable.Status.INVALID && state.LoadedState != Streamable.Status.LOADING && state.LoadedState != Streamable.Status.UNREGISTERED)
                    {
                        switch (state.LoadedState)
                        {
                            case Streamable.Status.NOTLOADED:
                                item.Event = EntityHistoryItem.EventType.UNLOADED;
                                break;

                            case Streamable.Status.LOADREQUESTED:
                                item.Event = EntityHistoryItem.EventType.REQUESTED;
                                break;

                            case Streamable.Status.LOADED:
                                item.Event = EntityHistoryItem.EventType.LOADED;
                                break;
                        }

                        item.Streamable = lastArchetype;
                        item.LodType = lastLodType;
                        item.EntityType = lastEntityType;
                        result.Add(item);
                    }

                    lastLoadedState = state.LoadedState;
                }
                */

                stateIndex++;
            }

            // Add streamable events for all our kids and grandkids.
            AddStreamableEvents(lastDeps, lastStreamableStartFrame, Scene.MaxFrame - 1, Relationship.DEP, result);
            AddStreamableEvents(lastIndirDeps, lastStreamableStartFrame, Scene.MaxFrame - 1, Relationship.INDIR_DEP, result);

            if (lastArchetype != null)
            {
                AddStreamableEvents(lastArchetype, lastStreamableStartFrame, Scene.MaxFrame - 1, Relationship.SELF, result);
            }

            result.Sort((a, b) => a.CompareFrame(b));

            return result;
        }


        public EntityHistoryView()
        {
            InitializeComponent();
        }

        public void SetToFrame(int frameNumber)
        {
            FrameNumber = frameNumber;
            RefreshFrame();
        }

        public void OnDeviceChanged(int deviceIndex, Device device)
        {
        }

        public void OnSelectStreamable(Streamable streamable)
        {
        }

        public void RefreshData()
        {
            if (Entity != null)
            {
                ChangingSelection = true;

                EntityHistory = CreateHistory(Entity);

                Streamable streamable = Entity.NewHistory.GetStateAt(FrameNumber).Streamable;

                if (streamable == null)
                {
                    EntityName.Name = "DELETED ENTITY";
                }
                else
                {
                    EntityName.Name = streamable.IdentityHistory.GetStateAt(FrameNumber).Name;
                }

                History.DataSource = EntityHistory;
                ChangingSelection = false;
            }

            RefreshFrame();
        }

        public void RefreshFrame()
        {
            if (Entity != null)
            {
                Entity.NewState state = Entity.NewHistory.GetStateAt(FrameNumber);

                if (state.Streamable == null)
                {
                    EntityName.Text = "NO ENTITY";
                    LodType.Text = "";
                    EntityType.Text = "";
                    Archetype.Text = "";
                    MapData.Text = "";
                    StreamableStatus.Text = "";
                    Visibility.Text = "";
                }
                else
                {
                    EntityName.Text = state.Streamable.IdentityHistory.GetStateAt(FrameNumber).Name;
                    EntityType.Text = Scene.GetEntityType(state.EntityType).ToString();
                    LodType.Text = Scene.GetLodType(state.LodType).ToString();
                    Archetype.Text = state.Streamable.IdentityHistory.GetStateAt(FrameNumber).Name;
                    MapData.Text = (state.MapDataStreamable != null) ? state.MapDataStreamable.IdentityHistory.GetStateAt(FrameNumber).Name : "";
                    StreamableStatus.Text = state.LoadedState.ToString();
                    Visibility.Text = (state.IsVisible(Scene)) ? "VISIBLE" : "outside PVS";
                }
            }
        }

        public void ClientInfoUpdated()
        {
        }

        public void OnNewFrame(int frameNumber)
        {
            FrameNumber = frameNumber;
            BeginInvoke((Action)delegate { RefreshFrame(); });
        }

        public void OnNewEntityType(int entityTypeId, EntityType type)
        {
        }

        public void OnConnectionLost()
        {
        }

        public void Register(MainController controller, Scene scene)
        {
            this.Controller = controller;
            this.Scene = scene;

            if (scene != null)
            {
                FrameNumber = scene.CurrentFrameNumber;
            }
        }

        public void OnFilterChanged()
        {
            RefreshData();
        }

        public void OnFrameRangeChanged(int firstFrame, int lastFrame)
        {
        }

        public void OnSelectEntity(Entity entity)
        {
            Entity = entity;
            RefreshData();
        }

        private void RecreateContextMenu()
        {
            ContextMenu menu = ViewContextMenu.CreateContextMenu(Controller, this);

            menu.MenuItems.Add(ContextMenuHelper.CreateToggleMenu("Show &Indirect Dependencies", ShowIndirDeps, () => RefreshData()));
            
            this.ContextMenu = menu;
        }

        private void History_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void EntityHistoryView_Load(object sender, EventArgs e)
        {
            RecreateContextMenu();

            StreamableColIdx = History.Columns["StreamableCol"].Index;
            FrameNumberColIdx = History.Columns["FrameNumberCol"].Index;
            EventColIdx = History.Columns["EventCol"].Index;
            GuidColIdx = History.Columns["GuidCol"].Index;
        }

        private void History_SelectionChanged(object sender, EventArgs e)
        {
            if (!ChangingSelection)
            {
                if (History.SelectedCells.Count > 0)
                {
                    int row = History.SelectedCells[0].RowIndex;
                    int col = History.SelectedCells[0].ColumnIndex;

                    if (row >= 0 && row < EntityHistory.Count)
                    {
                        EntityHistoryItem item = EntityHistory[row];

                        if (col == StreamableColIdx)
                        {

                            if (item.Streamable != null)
                            {
                                Controller.OnSelectStreamable(item.Streamable, true);
                            }
                        }

                        if (col == FrameNumberColIdx)
                        {
                            Controller.SetToFrame(item.FrameNumber, true);
                        }
                    }
                }
            }
        }

        private void History_CellValueNeeded(object sender, DataGridViewCellValueEventArgs e)
        {
        }

        private void History_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == EventColIdx)
            {
                if (EntityHistory != null && EntityHistory.Count > e.RowIndex)
                {
                    if (EntityHistory[e.RowIndex].Event == EntityHistoryItem.EventType.VISIBLE_MISSING)
                    {
                        e.CellStyle.ForeColor = Color.Red;
                    }
                }
            }

            if (e.ColumnIndex == GuidColIdx)
            {
                if (EntityHistory != null && EntityHistory.Count > e.RowIndex)
                {
                    e.Value = String.Format("{0:X}", EntityHistory[e.RowIndex].GUID);
                }
            }
        }
    }
}
