﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.Diagnostics;

namespace StreamingVisualize.View
{
    public partial class AboutBox : Form
    {
        public AboutBox()
        {
            InitializeComponent();

            Assembly assembly = Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            Version.Text = "Version " + fvi.ProductVersion;
        }

        private void OK_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
