﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StreamingVisualize.Model
{
    public class Box16
    {
        public Vector16 BoxMin { get; set; }
        public Vector16 BoxMax { get; set; }

        public Box16()
        {

        }

        public Box16(Vector16 boxMin, Vector16 boxMax)
        {
            BoxMin = boxMin;
            BoxMax = boxMax;
        }

        public void Normalize()
        {
            Vector16 min = BoxMin.Min(BoxMax);
            Vector16 max = BoxMax.Max(BoxMax);

            BoxMin = min;
            BoxMax = max;
        }

        /* Returns the size of the bounding box as the length of X or Y, whichever is higher.
         * 0 if the bounding box doesn't have a current state.
         */
        public float GetSize()
        {
            return Math.Max(BoxMax.X - BoxMin.X, BoxMax.Y - BoxMax.Y);
        }
    }
}
