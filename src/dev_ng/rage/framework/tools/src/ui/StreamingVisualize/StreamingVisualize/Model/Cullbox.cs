﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StreamingVisualize.Model
{
    public class Cullbox : IVersionedSerializable
    {
        public Box Extents { get; set; }

        public String Name { get; set; }

        public List<Streamable> Contents = new List<Streamable>();

        public void Serialize(VersionedWriter writer)
        {
            writer.Write(Name);         // They don't repeat, no need to use a ref
            writer.Write(Extents);

            int streamableCount = Contents.Count;

            writer.Write((short)streamableCount);

            for (int x = 0; x < streamableCount; x++)
            {
                writer.WriteStreamableRef(Contents[x]);
            }
        }

        public void Deserialize(VersionedReader reader, int version)
        {
            Name = reader.ReadString();
            Extents = reader.ReadBox();
            int count = reader.ReadShort();

            for (int x = 0; x < count; x++)
            {
                Contents.Add(reader.ReadStreamableRef());
            }
        }

        /* Returns the size of the bounding box as the length of X or Y, whichever is higher.
         * 0 if the bounding box doesn't have a current state.
         */
        public float GetBoundingBoxSize()
        {
            return Extents.GetSize();
        }

        public bool Contains(Vector point)
        {
            return Extents.Contains(point);
        }

        public bool Culls(Streamable mapStreamable)
        {
            return Contents.Contains(mapStreamable);
        }
    }
}
