﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StreamingVisualize.Model
{
    public class Vector : IComparable<Vector>
    {
        public float X { get; set; }
        public float Y { get; set; }
        public float Z { get; set; }

        public static Vector Zero = new Vector(0.0f, 0.0f, 0.0f);

        public override String ToString()
        {
            return String.Format("{0:F2}, {1:F2}, {2:F2}", X, Y, Z);
        }

        public Vector()
        {

        }

        public Vector(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public Vector(Vector v)
        {
            X = v.X;
            Y = v.Y;
            Z = v.Z;
        }

        public Vector(Vector16 v)
        {
            X = v.X;
            Y = v.Y;
            Z = v.Z;
        }

        public Vector Min(Vector o)
        {
            Vector r = new Vector();

            r.X = Math.Min(X, o.X);
            r.Y = Math.Min(Y, o.Y);
            r.Z = Math.Min(Z, o.Z);

            return r;
        }

        public Vector Max(Vector o)
        {
            Vector r = new Vector();

            r.X = Math.Max(X, o.X);
            r.Y = Math.Max(Y, o.Y);
            r.Z = Math.Max(Z, o.Z);

            return r;
        }

        public Vector Sub(Vector o)
        {
            Vector r = new Vector();

            r.X = X - o.X;
            r.Y = Y - o.Y;
            r.Z = Z - o.Z;

            return r;
        }

        public Vector Scale(float m)
        {
            Vector r = new Vector();

            r.X = X * m;
            r.Y = Y * m;
            r.Z = Z * m;

            return r;
        }

        public float Dot(Vector o)
        {
            return X * o.X + Y * o.Y + Z * o.Z;
        }

        public float Mag()
        {
            return (float) Math.Sqrt(Dot(this));
        }

        public Vector16 ToVector16()
        {
            return new Vector16(X, Y, Z);
        }

        public float Dist(Vector o)
        {
            return (float) Math.Sqrt((X - o.X) * (X - o.X) + (Y - o.Y) * (Y - o.Y) + (Z - o.Z) * (Z - o.Z));
        }

        public int CompareTo(Vector s)
        {
            return (X == s.X && Y == s.Y && Z == s.Z) ? 0 : 1;
        }

        public bool Equals(Vector s)
        {
            return X == s.X && Y == s.Y && Z == s.Z;
        }
    }
}
