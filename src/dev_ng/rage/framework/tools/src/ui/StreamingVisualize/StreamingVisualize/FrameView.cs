﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StreamingVisualize.Model;
using StreamingVisualize.Controller;

namespace StreamingVisualize.View
{
    public partial class FrameView : UserControl, IViewController
    {
        public Scene Scene { get; set; }
        private List<Request> RequestList;
        private List<Unrequest> UnrequestList;
        private IList<FailedAlloc> FailedAllocList;
        public MainController Controller;

        private int frameNumber;
        private Font boldFont;

        // If true, we're changing lists around. Don't respond to selection changes.
        private bool ChangingSelection;

        public FrameView()
        {
            InitializeComponent();
        }

        public void UpdateFrameNumber()
        {
            if (Scene != null)
            {
                FrameNumber.Text = "Frame: " + frameNumber + "/" + Scene.FrameCount;
            }
        }

        public void UpdateFrameNumberDeferred()
        {
            BeginInvoke((Action)delegate { UpdateFrameNumber(); });
        }

        private void Requests_SelectionChanged(object sender, EventArgs e)
        {
            if (ChangingSelection)
            {
                return;
            }

            if (Requests.SelectedCells.Count > 0)
            {
                int index = Requests.SelectedCells[0].RowIndex;

                if (index >= 0 && index < RequestList.Count)
                {
                    Request request = RequestList[index];
                    Controller.OnSelectStreamable(request.Streamable);
                }
            }
        }

        private void Unrequests_SelectionChanged(object sender, EventArgs e)
        {
            if (ChangingSelection)
            {
                return;
            }

            if (Unrequests.SelectedCells.Count > 0)
            {
                int index = Unrequests.SelectedCells[0].RowIndex;

                if (index >= 0 && index < UnrequestList.Count)
                {
                    Unrequest unrequest = UnrequestList[index];
                    Controller.OnSelectStreamable(unrequest.Streamable);
                }
            }
        }

        private void Requests_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Requests_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            int row = e.RowIndex;

            if (row >= 0 && row < RequestList.Count)
            {
                Request request = RequestList[row];
                bool isPriority = ((request.Flags & Request.PRIORITY_LOAD) != 0);

                if (Requests.Columns[e.ColumnIndex].Name == "Flags")
                {
                    e.Value = (isPriority) ? "PRIORITY" : "";
                }
                else if (Requests.Columns[e.ColumnIndex].Name == "Streamable")
                {
                    if (boldFont == null)
                    {
                        boldFont = new Font(e.CellStyle.Font, FontStyle.Bold);
                    }

                    if (isPriority)
                    {
                        e.CellStyle.Font = boldFont;
                    }
                }
                else if (Requests.Columns[e.ColumnIndex].Name == "Score")
                {
                    // Don't show negative numbers.
                    String scoreString = e.Value.ToString();
                    if (scoreString.Length > 0 && scoreString[0] == '-')
                    {
                        e.Value = "";
                    }
                }
            }
        }

        private void Unrequests_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        // Switch the display to focus on a certain frame
        public void SetToFrame(int frameNumber)
        {
            if (Scene == null)
            {
                return;
            }

            this.frameNumber = frameNumber;
            BeginInvoke((Action)delegate { UpdateFrame(); });
        }

        private void UpdateFrame()
        {
            ChangingSelection = true;

            UpdateFrameNumber();

            Frame frame = Scene.GetFrame(frameNumber);

            CamPos.Text = String.Format("Camera Pos: {0}, Dir: {1}", frame.CamPos, frame.CamDir);
            StreamingVolume.Text = frame.StreamingVolume.ToString();
            MasterCutoff.Text = String.Format("{0}", frame.MasterCutoff);

            RequestList = frame.requests;
            Requests.DataSource = RequestList;

            UnrequestList = frame.unrequests;
            Unrequests.DataSource = UnrequestList;

            FailedAllocList = frame.FailedAllocs;
            FailedAllocs.DataSource = FailedAllocList;

            ChangingSelection = false;
        }

        // If a new frame was added
        public void OnFrameChanged() { }

        // A new device was registered, or a device has changed.
        public void OnDeviceChanged(int deviceIndex, Device device) { }

        // A streamable has been selected
        public void OnSelectStreamable(Streamable streamable) { }

        // The client information has been updated
        public void ClientInfoUpdated() { }

        // We're in follow mode, and a new frame has been added.
        public void OnNewFrame(int frameNumber)
        {
            SetToFrame(frameNumber);
        }

        // A new entity type was registered
        public void OnNewEntityType(int entityTypeId, EntityType type) { }

        // The connection has been terminated
        public void OnConnectionLost() { }
    }
}
