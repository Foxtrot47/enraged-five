﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace StreamingVisualize.View.Components
{
    public class ContextMenuHelper
    {
        public delegate void RefreshFunc();

        public delegate void ActionFunc();


        public class ToggleBool
        {
            public bool Value { get; set; }

            public ToggleBool()
            {
                Value = false;
            }

            public ToggleBool(bool initialValue)
            {
                Value = initialValue;
            }
        }

        public class ToggleIntBoolDictionary
        {
            public Dictionary<int, bool> Value = new Dictionary<int, bool>();

            public bool IsSet(int key)
            {
                bool result = true;

                Value.TryGetValue(key, out result);
                return result;
            }
        }

        static public MenuItem CreateToggleMenu(String text, ToggleBool toggleBool, RefreshFunc func)
        {
            //boolGetter(theBool);
            MenuItem item = new MenuItem(text);
            item.Checked = toggleBool.Value;
            item.Click += (sender, e) =>
            {
                toggleBool.Value = !toggleBool.Value;
                item.Checked = toggleBool.Value;
                func();
            };

            return item;
        }

        static public MenuItem CreateCheckboxSubmenu<T>(String text, ToggleIntBoolDictionary toggleDictionary, Dictionary<int, T> dictionary, RefreshFunc func, bool defaultState)
        {
            MenuItem[] items = new MenuItem[dictionary.Count];
            int index = 0;

            foreach (int key in dictionary.Keys)
            {
                T value = dictionary[key];
                MenuItem subItem = new MenuItem(value.ToString());

                if (toggleDictionary.Value.ContainsKey(key))
                {
                    subItem.Checked = toggleDictionary.Value[key];
                }
                else
                {
                    subItem.Checked = defaultState;
                    toggleDictionary.Value.Add(key, defaultState);
                }

                int keyValue = key;

                subItem.Click += (sender, e) =>
                {
                    toggleDictionary.Value[keyValue] = !toggleDictionary.Value[keyValue];
                    subItem.Checked = toggleDictionary.Value[keyValue];
                    func();
                };

                items[index++] = subItem;
            }

            MenuItem item = new MenuItem(text, items);
            return item;
        }


        static public MenuItem CreateActionMenu(String text, ActionFunc func)
        {
            MenuItem item = new MenuItem(text);
            item.Click += (sender, e) => { func(); };
            return item;
        }
    }
}
