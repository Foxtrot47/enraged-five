﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace StreamingVisualize.View
{
    public partial class GoToFrame : Form
    {
        public delegate void OnSelectFrame(int frame);

        public OnSelectFrame SelectFrameCb;



        public GoToFrame()
        {
            InitializeComponent();

            Frame.SelectAll();
        }

        private void OnReturn()
        {
            if (SelectFrameCb != null)
            {
                try {
                    int frame = Int32.Parse(Frame.Text);
                    SelectFrameCb(frame);
                }
                catch (Exception )
                {

                }

                Dispose();
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar)
                && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
            
            if (e.KeyChar == (char)Keys.Enter || e.KeyChar == (char)Keys.Return)
            {
                OnReturn();
                e.Handled = true;
            }
        }
    }
}
