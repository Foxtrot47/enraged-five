﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StreamingVisualize.Model
{
    public class RscEvent : IVersionedSerializable
    {
        public enum ActionType
        {
            REQUEST,
            UNREQUEST,
            Unloaded,
            Loaded,
            LoadRequested,
            Loading,
            CANCEL,
            REGISTER,
            UNREGISTER,
            FlagsChange,
        }

        public enum ReasonType
        {
            NONE,
            REQUEST_PURGE,
            CHURN_PROTECTION,
            DELETE_NEXT_USED,
            STATUS_NOTLOADED,
            STATUS_LOADED,
            STATUS_LOADREQUESTED,
            STATUS_LOADING,
        }


       // public Streamable Streamable { get; set; }
        public ActionType Action { get; set; }
        public ReasonType Reason { get; set; }
        public int Flags { get; set; }
        public int FrameNumber { get; set; }
        public float StartTime { get; set; }
        public float IssueTime { get; set; }
        public float ProcessTime { get; set; }
        public float LoadTime { get; set; }
        public float Score { get; set; }
        public String Context { get; set; }

        // Get the time at which the request first started. Which is either StartTime (if we used
        // the high-level streaming system), or IssueTime (for the low-level streaming system).
        public float GetStartTime()
        {
            return (StartTime != 0.0f) ? StartTime : IssueTime;
        }

        // Returns the status change, if this is one.
        public Streamable.Status GetStatusChange()
        {
            switch (Action)
            {
                case ActionType.Unloaded:
                case ActionType.REGISTER:
                    return Streamable.Status.NOTLOADED;

                case ActionType.Loaded:
                    return Streamable.Status.LOADED;

                case ActionType.LoadRequested:
                    return Streamable.Status.LOADREQUESTED;

                case ActionType.Loading:
                    return Streamable.Status.LOADING;

                case ActionType.UNREGISTER:
                    return Streamable.Status.UNREGISTERED;
            }

            return Streamable.Status.INVALID;
        }

        public Streamable.Status GetStatusChangeWithContext()
        {
            switch (Action)
            {
                case ActionType.UNREQUEST:
                case ActionType.REGISTER:
                    return Streamable.Status.NOTLOADED;

                case ActionType.Loaded:
                    return Streamable.Status.LOADED;

                case ActionType.REQUEST:
                    return Streamable.Status.LOADREQUESTED;

                case ActionType.Loading:
                    return Streamable.Status.LOADING;

                case ActionType.UNREGISTER:
                    return Streamable.Status.UNREGISTERED;
            }

            return Streamable.Status.INVALID;
        }

        public void Serialize(VersionedWriter writer)
        {
            writer.Write((byte)Action);
            writer.Write((byte)Reason);
            writer.Write(Flags);
            writer.Write(FrameNumber);
            writer.Write(StartTime);
            writer.Write(IssueTime);
            writer.Write(ProcessTime);
            writer.Write(LoadTime);
            writer.Write(Score);

            // Version 26: String ref
            writer.WriteStringRef(Context);
        }

        public void Deserialize(VersionedReader reader, int version)
        {
            Action = (ActionType)reader.ReadByte();
            Reason = (ReasonType)reader.ReadByte();
            Flags = reader.ReadInt();
            FrameNumber = reader.ReadInt();
            StartTime = reader.ReadFloat();
            IssueTime = reader.ReadFloat();
            ProcessTime = reader.ReadFloat();
            LoadTime = reader.ReadFloat();
            Score = reader.ReadFloat();

            if (version >= 26)
            {
                Context = reader.ReadStringRef();
            }
            else
            {
                Context = reader.ReadAndRegisterString();
            }
        }
    
    }
}
