﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StreamingVisualize.Model
{
    public class NamedFlags
    {
        public Dictionary<int, String> Flags = new Dictionary<int, String>();

        public void Serialize(VersionedWriter writer)
        {
            writer.Write(Flags.Count);

            foreach (int key in Flags.Keys)
            {
                writer.Write(key);
                writer.Write(Flags[key]);
            }
        }

        public void Deserialize(VersionedReader reader, int version)
        {
            int count = reader.ReadInt();

            for (int x = 0; x < count; x++)
            {
                int key = reader.ReadInt();
                Flags.Add(key, reader.ReadString());
            }
        }

        public void RegisterFlag(int flagValue, String flagName)
        {
            Flags.Add(flagValue, flagName);
        }

        public String GetFlag(int flagValue)
        {
            return Flags[flagValue];
        }
    }
}
