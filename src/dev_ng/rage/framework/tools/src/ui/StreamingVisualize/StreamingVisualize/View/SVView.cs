﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StreamingVisualize.Controller;
using StreamingVisualize.Model;

namespace StreamingVisualize.View
{
    public class SVView : UserControl, IViewController
    {
        public virtual void SetToFrame(int frameNumber)
        {
        }

        public virtual void OnDeviceChanged(int deviceIndex, Device device)
        {
        }

        public virtual void OnSelectStreamable(Streamable streamable)
        {
        }

        public virtual void ClientInfoUpdated()
        {
        }

        public virtual void OnNewFrame(int frameNumber)
        {
        }

        public virtual void OnNewEntityType(int entityTypeId, EntityType type)
        {
        }

        public virtual void OnConnectionLost()
        {
        }

        public virtual void Register(MainController controller, Scene scene)
        {
        }

        public virtual void OnFilterChanged()
        {
        }

        public virtual void OnFrameRangeChanged(int firstFrame, int lastFrame)
        {
        }

        public virtual void OnSelectEntity(Entity entity)
        {
        }
    }
}
