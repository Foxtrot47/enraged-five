﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace StreamingVisualize.View
{
    class UpdateThrottler
    {
        private int minMsBetweenUpdates;

        private Stopwatch stopwatch = new Stopwatch();


        public UpdateThrottler(int minMsBetweenUpdates)
        {
            this.minMsBetweenUpdates = minMsBetweenUpdates;
            stopwatch.Start();
        }

        public bool MayUpdate()
        {
            if (stopwatch.ElapsedMilliseconds > (long)minMsBetweenUpdates)
            {
                stopwatch.Restart();
                return true;
            }

            return false;
        }
    }
}
