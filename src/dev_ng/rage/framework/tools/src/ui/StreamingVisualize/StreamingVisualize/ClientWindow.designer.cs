﻿namespace StreamingVisualize
{
    partial class ClientWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.FrameOverview = new StreamingVisualize.View.FrameOverview();
            this.FrameView = new StreamingVisualize.View.FrameView();
            this.EntityView = new StreamingVisualize.View.EntityView();
            this.ResourceView = new StreamingVisualize.View.ResourceView();
            this.DeviceView1 = new StreamingVisualize.View.DeviceView();
            this.DeviceView2 = new StreamingVisualize.View.DeviceView();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer1.Location = new System.Drawing.Point(12, 115);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer4);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(786, 416);
            this.splitContainer1.SplitterDistance = 387;
            this.splitContainer1.TabIndex = 2;
            // 
            // splitContainer4
            // 
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.Location = new System.Drawing.Point(0, 0);
            this.splitContainer4.Name = "splitContainer4";
            this.splitContainer4.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.FrameView);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this.EntityView);
            this.splitContainer4.Size = new System.Drawing.Size(383, 412);
            this.splitContainer4.SplitterDistance = 262;
            this.splitContainer4.TabIndex = 1;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer2.Location = new System.Drawing.Point(-2, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.ResourceView);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer2.Size = new System.Drawing.Size(395, 414);
            this.splitContainer2.SplitterDistance = 203;
            this.splitContainer2.TabIndex = 2;
            // 
            // splitContainer3
            // 
            this.splitContainer3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.DeviceView1);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.DeviceView2);
            this.splitContainer3.Size = new System.Drawing.Size(395, 207);
            this.splitContainer3.SplitterDistance = 217;
            this.splitContainer3.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(810, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.Visible = false;
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveAsToolStripMenuItem});
            this.fileToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.MatchOnly;
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
            this.saveAsToolStripMenuItem.MergeIndex = 1;
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
            this.saveAsToolStripMenuItem.Text = "&Save As...";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // FrameOverview
            // 
            this.FrameOverview.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FrameOverview.Controller = null;
            this.FrameOverview.CurrentFrameNumber = 0;
            this.FrameOverview.Location = new System.Drawing.Point(12, 27);
            this.FrameOverview.Name = "FrameOverview";
            this.FrameOverview.Scene = null;
            this.FrameOverview.Size = new System.Drawing.Size(784, 82);
            this.FrameOverview.TabIndex = 4;
            // 
            // FrameView
            // 
            this.FrameView.AutoScroll = true;
            this.FrameView.AutoSize = true;
            this.FrameView.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.FrameView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FrameView.Location = new System.Drawing.Point(0, 0);
            this.FrameView.Name = "FrameView";
            this.FrameView.Scene = null;
            this.FrameView.Size = new System.Drawing.Size(383, 262);
            this.FrameView.TabIndex = 0;
            this.FrameView.Load += new System.EventHandler(this.FrameView_Load);
            // 
            // EntityView
            // 
            this.EntityView.AutoSize = true;
            this.EntityView.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.EntityView.BackColor = System.Drawing.Color.Black;
            this.EntityView.Controller = null;
            this.EntityView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.EntityView.Frame = null;
            this.EntityView.Keyframe = null;
            this.EntityView.Location = new System.Drawing.Point(0, 0);
            this.EntityView.MinimumSize = new System.Drawing.Size(100, 100);
            this.EntityView.Name = "EntityView";
            this.EntityView.Scene = null;
            this.EntityView.Size = new System.Drawing.Size(383, 146);
            this.EntityView.TabIndex = 0;
            // 
            // ResourceView
            // 
            this.ResourceView.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ResourceView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ResourceView.Location = new System.Drawing.Point(0, 0);
            this.ResourceView.Name = "ResourceView";
            this.ResourceView.Size = new System.Drawing.Size(391, 199);
            this.ResourceView.TabIndex = 0;
            // 
            // DeviceView1
            // 
            this.DeviceView1.Controller = null;
            this.DeviceView1.Device = null;
            this.DeviceView1.DeviceIndex = 0;
            this.DeviceView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DeviceView1.Location = new System.Drawing.Point(0, 0);
            this.DeviceView1.Name = "DeviceView1";
            this.DeviceView1.Size = new System.Drawing.Size(213, 203);
            this.DeviceView1.TabIndex = 0;
            // 
            // DeviceView2
            // 
            this.DeviceView2.Controller = null;
            this.DeviceView2.Device = null;
            this.DeviceView2.DeviceIndex = 0;
            this.DeviceView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DeviceView2.Location = new System.Drawing.Point(0, 0);
            this.DeviceView2.Name = "DeviceView2";
            this.DeviceView2.Size = new System.Drawing.Size(170, 203);
            this.DeviceView2.TabIndex = 0;
            // 
            // ClientWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(810, 543);
            this.Controls.Add(this.FrameOverview);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.menuStrip1);
            this.Name = "ClientWindow";
            this.Text = "StreamingVisualize";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ClientWindow_FormClosed);
            this.Load += new System.EventHandler(this.Form2_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel1.PerformLayout();
            this.splitContainer4.Panel2.ResumeLayout(false);
            this.splitContainer4.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private View.FrameView FrameView;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private View.ResourceView ResourceView;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private View.DeviceView DeviceView1;
        private View.DeviceView DeviceView2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private View.EntityView EntityView;
        private View.FrameOverview FrameOverview;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;

    }
}