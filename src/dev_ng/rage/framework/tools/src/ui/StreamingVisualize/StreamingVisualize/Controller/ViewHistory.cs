﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StreamingVisualize.Model;
using System.Diagnostics;

namespace StreamingVisualize.Controller
{
    class ViewHistory
    {
        private List<IViewEvent> Events = new List<IViewEvent>();

        private int UndoPosition = -1;


        private interface IViewEvent
        {
            void Execute(MainController Controller);

            // Compare to another event - types are guaranteed to be the same
            bool Compare(IViewEvent other);
        }

        private class StreamableEvent : IViewEvent
        {
            public Streamable Streamable;

            public StreamableEvent(Streamable Streamable)
            {
                this.Streamable = Streamable;
            }

            public void Execute(MainController Controller)
            {
                Controller.OnSelectStreamable(Streamable, false);
            }

            public bool Compare(IViewEvent other)
            {
                return ((StreamableEvent)other).Streamable == Streamable;
            }
        }

        private class FrameEvent : IViewEvent
        {
            public int Frame;

            public FrameEvent(int Frame)
            {
                this.Frame = Frame;
            }

            public void Execute(MainController Controller)
            {
                Controller.SetToFrame(Frame, false);
            }

            public bool Compare(IViewEvent other)
            {
                return ((FrameEvent)other).Frame == Frame;
            }
        }


        private void AddEvent(IViewEvent Event)
        {
            if (UndoPosition + 1 < Events.Count)
            {
                Events.RemoveRange(UndoPosition + 1, Events.Count - UndoPosition - 1);
            }

            // Don't add the same event twice.
            if (Events.Count > 0)
            {
                IViewEvent prevEvent = Events[Events.Count - 1];

                if (prevEvent.GetType() == Event.GetType())
                {
                    if (prevEvent.Compare(Event))
                    {
                        return;
                    }
                }
            }

            Events.Add(Event);
            UndoPosition++;

            Debug.Assert(UndoPosition == Events.Count - 1);
        }

        public void Undo(MainController Controller)
        {
            if (UndoPosition >= 0)
            {
                Events[UndoPosition--].Execute(Controller);
            }
        }

        public void Redo(MainController Controller)
        {
            if (UndoPosition + 1 < Events.Count)
            {
                Events[++UndoPosition].Execute(Controller);
            }
        }

        public void RecordSetFrame(int frame)
        {
            AddEvent(new FrameEvent(frame));
        }

        public void RecordSetStreamable(Streamable streamable)
        {
            AddEvent(new StreamableEvent(streamable));
        }
    }
}
