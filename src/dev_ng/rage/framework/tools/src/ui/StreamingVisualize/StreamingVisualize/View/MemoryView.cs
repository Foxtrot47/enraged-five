﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StreamingVisualize.Controller;
using StreamingVisualize.Model;
using StreamingVisualize.View.Components;

namespace StreamingVisualize.View
{
    public partial class MemoryView : UserControl, IViewController
    {
        private class ModuleInfo
        {
            public long VirtUsage;
            public long PhysUsage;

            public ModuleInfo Clone()
            {
                ModuleInfo c = new ModuleInfo();

                c.VirtUsage = VirtUsage;
                c.PhysUsage = PhysUsage;
                return c;
            }
        }

        private ViewContextMenu viewContextMenu = new ViewContextMenu();

        private Dictionary<Module, ModuleInfo> ModuleInfos;
        private Dictionary<Module, ModuleInfo> BaseLine;

        private Brush ModuleBrush = new SolidBrush(Color.Black);
        private Font ModuleFont;

        private ContextMenuHelper.ToggleIntBoolDictionary ShowFlagTypes = new ContextMenuHelper.ToggleIntBoolDictionary();

//        List<ModuleUsage> Modules;

        int FrameNumber;
        Scene Scene;
        MainController Controller;


        private bool IsFlagFilteredOut(int streamableFlags)
        {
            foreach (int flag in ShowFlagTypes.Value.Keys)
            {
                if (ShowFlagTypes.Value[flag])
                {
                    if ((streamableFlags & flag) == 0)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public void CreateMemoryUsage()
        {
            ModuleInfos = new Dictionary<Module, ModuleInfo>();

            for (int x=0; x<Scene.maxModuleIndex; x++)
            {
                Module module = Scene.GetModule(x);

                if (module != null)
                {
                    ModuleInfos.Add(module, new ModuleInfo());
                }
            }

            foreach (int strIndex in Scene.StrIndexToIndex.Keys)
            {
                Streamable streamable = Scene.GetStreamable(strIndex);

                if (streamable != null)
                {
                    // TODO: Oh fuck, the sizes need to go into the identity history.
                    if (streamable.VirtSize != 0 || streamable.PhysSize != 0)
                    {
                        if (!Scene.Filter.IsFilteredOut(streamable))
                        {
                            Streamable.State state = streamable.History.GetStateAt(FrameNumber);

                            if (state.Status == Streamable.Status.LOADED || state.Status == Streamable.Status.LOADING)
                            {
                                if (!IsFlagFilteredOut(state.Flags))
                                {
                                    ModuleInfo info = ModuleInfos[streamable.Module];

                                    info.VirtUsage += streamable.VirtSize;
                                    info.PhysUsage += streamable.PhysSize;
                                }
                            }
                        }
                    }
                }
            }
        }

        public MemoryView()
        {
            InitializeComponent();
            ModuleFont = new Font(Font, FontStyle.Bold);
        }

        public void UpdateList()
        {
            CreateMemoryUsage();
            Refresh();
        }

        public void SetToFrame(int frameNumber)
        {
            FrameNumber = frameNumber;
            UpdateList();
        }

        public void OnDeviceChanged(int deviceIndex, Device device)
        {
        }

        public void OnSelectStreamable(Streamable streamable)
        {
        }

        public void ClientInfoUpdated()
        {
        }

        public void OnNewFrame(int frameNumber)
        {
        }

        public void OnNewEntityType(int entityTypeId, EntityType type)
        {
        }

        public void OnConnectionLost()
        {
        }

        public void Register(MainController controller, Scene scene)
        {
            Scene = scene;
            Controller = controller;

            FrameNumber = Scene.CurrentFrameNumber;

            CreateMemoryUsage();
        }

        public void OnFilterChanged()
        {
        }

        public void OnFrameRangeChanged(int firstFrame, int lastFrame)
        {
        }

        public void OnSelectEntity(Entity entity)
        {
        }

        private void MemoryView_Paint(object sender, PaintEventArgs e)
        {
            if (ModuleInfos != null)
            {
                Graphics g = e.Graphics;
                float yPos = 10.0f;
                float modNameRight = 200.0f;
                float memCenter = 300.0f;
                float baselineCenter = 500.0f;
                float height = (float) Font.Height;

                g.DrawString("RscPhys", ModuleFont, ModuleBrush, memCenter + 5.0f, yPos);

                String virtHeading = "RscVirt";
                SizeF headingSize = g.MeasureString(virtHeading, ModuleFont);
                g.DrawString(virtHeading, ModuleFont, ModuleBrush, memCenter - headingSize.Width - 5.0f, yPos);

                if (BaseLine != null)
                {
                    String baselineHeading = "Baseline";
                    headingSize = g.MeasureString(baselineHeading, ModuleFont);
                    g.DrawString(baselineHeading, ModuleFont, ModuleBrush, baselineCenter - headingSize.Width * 0.5f, yPos);
                }

                yPos += height * 1.5f;
   
                foreach (Module module in ModuleInfos.Keys)
                {
                    ModuleInfo moduleInfo = ModuleInfos[module];

                    SizeF size = g.MeasureString(module.Name, ModuleFont);
                    g.DrawString(module.Name, ModuleFont, ModuleBrush, modNameRight - size.Width, yPos);

                    long virtSize = moduleInfo.VirtUsage;
                    long physSize = moduleInfo.PhysUsage;

                    if (BaseLine != null)
                    {
                        virtSize -= BaseLine[module].VirtUsage;
                        physSize -= BaseLine[module].PhysUsage;
                    }

                    String virtString = String.Format("{0}KB", virtSize / 1024);
                    size = g.MeasureString(virtString, Font);
                    g.DrawString(virtString, Font, ModuleBrush, memCenter - size.Width - 5.0f, yPos);

                    String physString = String.Format("{0}KB", physSize / 1024);
                    g.DrawString(physString, Font, ModuleBrush, memCenter + 5.0f, yPos);

                    if (BaseLine != null)
                    {
                        virtString = String.Format("{0}KB", BaseLine[module].VirtUsage / 1024);
                        size = g.MeasureString(virtString, Font);
                        g.DrawString(virtString, Font, ModuleBrush, baselineCenter - size.Width - 5.0f, yPos);

                        physString = String.Format("{0}KB", BaseLine[module].PhysUsage / 1024);
                        g.DrawString(physString, Font, ModuleBrush, baselineCenter + 5.0f, yPos);
                    }

                    yPos += height;
                }
            }
        }

        private void MakeBaseline()
        {
            BaseLine = new Dictionary<Module, ModuleInfo>();

            foreach (Module module in ModuleInfos.Keys)
            {
                BaseLine.Add(module, ModuleInfos[module].Clone());
            }

            Refresh();
        }

        private void ClearBaseline()
        {
            BaseLine = null;
            Refresh();
        }

        private void PopulateContextMenu(ContextMenu menu)
        {
            menu.MenuItems.Add(ContextMenuHelper.CreateActionMenu("Make Baseline", MakeBaseline));
            menu.MenuItems.Add(ContextMenuHelper.CreateActionMenu("Clear Baseline", ClearBaseline));
            menu.MenuItems.Add(ContextMenuHelper.CreateCheckboxSubmenu("&Flags", ShowFlagTypes, Request.FlagTypes, () => Refresh(), false));
        }

        private void RecreateContextMenu()
        {
            ContextMenu menu = viewContextMenu.CreateContextMenu(Controller, this);
            PopulateContextMenu(menu);
            this.ContextMenu = menu;
        }

        private void MemoryView_Load(object sender, EventArgs e)
        {
            RecreateContextMenu();
        }
    }
}
