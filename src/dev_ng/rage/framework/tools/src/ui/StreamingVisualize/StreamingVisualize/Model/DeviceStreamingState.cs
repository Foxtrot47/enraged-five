﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace StreamingVisualize.Model
{
    public class DeviceStreamingState : IFrameState, IVersionedSerializable, ICloneable, IComparable<DeviceStreamingState>
    {
        public class StreamableEntry
        {
            public Streamable Streamable { get; set; }

            public bool Finished { get; set; }

            public bool Priority { get; set; }

            public int RequestedAtFrame { get; set; }

            public float RequestedAtTime { get; set; }
        }

        // Streamables that are being streamed right now
        public List<StreamableEntry> Streaming = new List<StreamableEntry>();

        // Streamables currently queued up
        public List<StreamableEntry> Queued = new List<StreamableEntry>();

        public int FrameNumber { get; set; }



        public List<StreamableEntry> CloneStreamableList(List<StreamableEntry> src)
        {
            List<StreamableEntry> result = new List<StreamableEntry>();

            foreach (StreamableEntry entry in src)
            {
                if (!entry.Finished)
                {
                    StreamableEntry newEntry = new StreamableEntry();
                    newEntry.Streamable = entry.Streamable;
                    newEntry.Priority = entry.Priority;
                    newEntry.Finished = false;
                    result.Add(newEntry);
                }
            }

            return result;
        }

        public int GetFrameNumber()
        {
            return FrameNumber;
        }

        public void SetFrameNumber(int frameNumber)
        {
            this.FrameNumber = frameNumber;
        }

        public void AddQueued(Streamable streamable, bool priority, int requestFrame, float requestTime)
        {
            // We COULD be queuing something twice in one frame. That is particularly likely in the first
            // frame. So if we have a finished queued event, just un-finish it.
 
            StreamableEntry entry = Queued.Find((StreamableEntry e) => { return e.Streamable == streamable; });

            if (entry != null)
            {
                // Told you.
                Debug.Assert(entry.Finished == true, "Two queued requests for " + streamable.CurrentName + " in a row in the same frame - why didn't the first one finish?");
                entry.Finished = false;
                entry.Priority |= priority;
                return;
            }

            entry = new StreamableEntry();
            entry.Streamable = streamable;
            entry.Finished = false;
            entry.Priority = priority;
            entry.RequestedAtFrame = requestFrame;
            entry.RequestedAtTime = requestTime;

            Queued.Add(entry);
        }

        public void AddStreaming(Streamable streamable, bool priority, int requestFrame, float requestTime)
        {
            // We COULD be streaming something twice in one frame. That is particularly likely in the first
            // frame. So if we have a finished streaming event, just un-finish it.
            StreamableEntry entry = Streaming.Find((StreamableEntry e) => { return e.Streamable == streamable; });

            if (entry != null)
            {
                // Told you.
                Debug.Assert(entry.Finished == true, "Two streaming requests for " + streamable.CurrentName + " in a row in the same frame - why didn't the first one finish?");
                entry.Finished = false;
                entry.Priority |= priority;
            }
            else
            {
                entry = new StreamableEntry();
                entry.Streamable = streamable;
                entry.Finished = false;
                entry.Priority = priority;
                entry.RequestedAtFrame = requestFrame;
                entry.RequestedAtTime = requestTime;
                Streaming.Add(entry);
            }

            entry = Queued.Find((StreamableEntry e) => { return e.Streamable == streamable; });

            Debug.Assert(entry != null);
            entry.Finished = true;
        }

        public void FinishStreaming(Streamable streamable)
        {
            StreamableEntry entry = Streaming.Find((StreamableEntry e) => { return e.Streamable == streamable; });

            Debug.Assert(entry != null);
            entry.Finished = true;
        }

        public Object Clone()
        {
            DeviceStreamingState obj = new DeviceStreamingState();

            // When cloning, only keep the entries that are still ongoing.
            obj.Streaming = CloneStreamableList(Streaming);
            obj.Queued = CloneStreamableList(Queued);
            obj.FrameNumber = FrameNumber;

            return obj;
        }

        public int CompareTo(DeviceStreamingState o)
        {
            if (ListHelper.IsEqualShallow(Streaming, o.Streaming))
            {
                return 0;
            }

            return 1;
        }

        public void WriteStreamableList(VersionedWriter writer, List<StreamableEntry> list)
        {
            writer.Write((short)list.Count);

            foreach (StreamableEntry entry in list)
            {
                writer.WriteStreamableRef(entry.Streamable);
                writer.Write(entry.Finished);

                // File version 3:
                writer.Write(entry.Priority);
            }
        }

        public void ReadStreamableList(VersionedReader reader, int version, List<StreamableEntry> list)
        {
            int count = reader.ReadShort();

            for (int x = 0; x < count; x++)
            {
                StreamableEntry entry = new StreamableEntry();
                entry.Streamable = reader.ReadStreamableRef();
                entry.Finished = reader.ReadBool();

                // File version 3:
                if (version > 1 /*2*/)  // Bug in 1.3.0
                {
                    entry.Priority = reader.ReadBool();
                }

                list.Add(entry);
            }
        }

        public void Serialize(VersionedWriter writer)
        {
            WriteStreamableList(writer, Queued);
            WriteStreamableList(writer, Streaming);
        }

        public void Deserialize(VersionedReader reader, int version)
        {
            ReadStreamableList(reader, version, Queued);
            ReadStreamableList(reader, version, Streaming);
        }
    }
}
