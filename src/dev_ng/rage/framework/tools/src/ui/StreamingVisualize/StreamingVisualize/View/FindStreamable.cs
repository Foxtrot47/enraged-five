﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StreamingVisualize.Model;
using StreamingVisualize.Controller;

namespace StreamingVisualize.View
{
    public partial class FindStreamable : Form
    {
        public MainController Controller;

        public Scene Scene { get; set; }

        public IList<Streamable> FilteredList;

        public delegate void OnItemSelected(Streamable streamable);
        public OnItemSelected OnItemSelectedCb;

        public static FindStreamable Instance;


        public FindStreamable(MainController controller, Scene scene)
        {
            Controller = controller;
            this.Scene = scene;
            InitializeComponent();
        }

        private void StreamableName_TextChanged(object sender, EventArgs e)
        {
            String searchString = StreamableName.Text;

            if (searchString.Length < 3)
            {
                Results.DataSource = null;
                return;
            }

            IList<Streamable> it;

            lock (Scene.StreamableLookup)
            {
                it = Scene.StreamableLookup.Where(kvp => kvp.Key.StartsWith(searchString, StringComparison.CurrentCultureIgnoreCase)).Select(kvp => kvp.Value).ToList();
            }

            Results.DataSource = it;
            FilteredList = it;
        }

        private void Results_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void StreamableName_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyData)
            {
                case Keys.Up:
                    if (Results.SelectedIndex > 0)
                    {
                        Results.SelectedIndex--;
                    }
                    break;

                case Keys.Down:
                    if (FilteredList != null && Results.SelectedIndex < FilteredList.Count - 1)
                    {
                        Results.SelectedIndex++;
                    }
                    break;

                case Keys.Return:
                    int row = Results.SelectedIndex;
                    if (FilteredList != null && row >= 0 && row < FilteredList.Count)
                    {
                        Streamable streamable = FilteredList[row];

                        if (OnItemSelectedCb != null)
                        {
                            OnItemSelectedCb(streamable);
                        }

                        Hide();
                    }
                    break;
            }
        }

        protected override bool IsInputKey(Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Up:
                case Keys.Down:
                case Keys.Return:
                    return true;
            }
            return base.IsInputKey(keyData);
        }

        private void Results_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int row = Results.SelectedIndex;
            if (FilteredList != null && row >= 0 && row < FilteredList.Count)
            {
                Streamable streamable = FilteredList[row];

                if (OnItemSelectedCb != null)
                {
                    OnItemSelectedCb(streamable);
                }

                Controller.OnSelectStreamable(streamable, true);

                Hide();

                //Dispose();
            }
        }

        private void FindStreamable_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                Hide();
                e.Cancel = true;
            }
        }

        public static void ShowDialog(MainController controller, Scene scene)
        {
            if (Instance == null)
            {
                Instance = new FindStreamable(controller, scene);
            }
            else
            {
                Instance.Controller = controller;
                Instance.Scene = scene;
            }

            Instance.StreamableName_TextChanged(null, null);
            Instance.Show();
        }

        private void Results_DoubleClick(object sender, EventArgs e)
        {
            Results_MouseDoubleClick(sender, null);
        }
    }
}
