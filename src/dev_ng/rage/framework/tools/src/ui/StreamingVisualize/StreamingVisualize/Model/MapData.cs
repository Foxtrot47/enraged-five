﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StreamingVisualize.Model
{
    public class MapData : IVersionedSerializable
    {
        public Box PhysicsBox { get; set; }
        public Box StreamingBox { get; set; }
        public int strIndex { get; set; }
        public int ParentStrIndex { get; set; }
        public List<MapData> Children = new List<MapData>();

        public MapData()
        {
            ParentStrIndex = -1;
        }

        public class Contents : IFrameState, ICloneable, IVersionedSerializable, IComparable<Contents>
        {
            public int Frame { get; set; }
            public List<Entity> Entities = new List<Entity>();

            public void Serialize(VersionedWriter writer)
            {
                int entityCount = Entities.Count;

                writer.Write((short)entityCount);

                for (int x=0; x<entityCount; x++)
                {
                    writer.WriteEntityRef(Entities[x]);
                }
            }

            public void Deserialize(VersionedReader reader, int version)
            {
                if (version < 18)
                {
                    // Garbage.
                    List<Entity> garbage = new List<Entity>();
                    reader.ReadList<Entity>(garbage);
                }
                else
                {
                    int count = reader.ReadShort();

                    for (int x = 0; x < count; x++)
                    {
                        Entities.Add(reader.ReadEntityDef());
                    }
                }
            }

            public int GetFrameNumber()
            {
                return Frame;
            }

            public void SetFrameNumber(int frameNumber)
            {
                Frame = frameNumber;
            }

            public Object Clone()
            {
                Contents obj = new Contents();

                // The entity list will change, so we ain't copying it.
                return obj;
            }

            public int CompareTo(Contents o)
            {
                if (Entities.Count == o.Entities.Count)
                {
                    for (int x = 0; x < Entities.Count; x++)
                    {
                        if (Entities[x] != o.Entities[x])
                        {
                            return 1;
                        }
                    }
                    return 0;
                }

                return 1;
            }
        }


        public PerFrameState<Contents> History = new PerFrameState<Contents>();

        /* Returns the size of the bounding box as the length of X or Y, whichever is higher.
         * 0 if the bounding box doesn't have a current state.
         */
        public float GetBoundingBoxSize()
        {
            return StreamingBox.GetSize();
        }

        public void Serialize(VersionedWriter writer)
        {
            writer.Write(PhysicsBox);
            writer.Write(StreamingBox);
            writer.Write(strIndex);
            History.Serialize(writer);

            // Version 42: Hierarchy.
            writer.Write(ParentStrIndex);
        }

        public void Deserialize(VersionedReader reader, int version)
        {
            PhysicsBox = reader.ReadBox();
            StreamingBox = reader.ReadBox();
            strIndex = reader.ReadInt();
            reader.ReadPerFrameState(History);

            if (version >= 42)
            {
                ParentStrIndex = reader.ReadInt();
            }
        }
    }
}
