﻿//---------------------------------------------------------------------------------------------
// <copyright file="MainWindowDataContext.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RpfViewer
{
    using System;
    using System.IO;
    using System.Windows;
    using RSG.Editor;
    using RSG.Editor.Controls;
    using RSG.Rpf.ViewModel;

    /// <summary>
    /// Represents the data context that should be attached to the main window.
    /// </summary>
    internal sealed class MainWindowDataContext : NotifyPropertyChangedBase
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Title"/> property.
        /// </summary>
        private string _title;

        /// <summary>
        /// The private field used for the <see cref="RpfViewerDataContext"/> property.
        /// </summary>
        private RpfViewerDataContext _rpfViewerDataContext;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MainWindowDataContext"/> class.
        /// </summary>
        public MainWindowDataContext()
        {
            this._title = String.Empty;
            this._rpfViewerDataContext = new RpfViewerDataContext();
            this._rpfViewerDataContext.LoadedFile += this.OnLoad;
            this._rpfViewerDataContext.UnloadedFile += this.OnUnload;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the title that should be set on the main window.
        /// </summary>
        public string Title
        {
            get { return this._title; }
            private set { this.SetProperty(ref this._title, value); }
        }

        /// <summary>
        /// Gets the data context that will be attached to the RPF viewer control.
        /// </summary>
        public RpfViewerDataContext RpfViewerDataContext
        {
            get { return this._rpfViewerDataContext; }
            private set { this.SetProperty(ref this._rpfViewerDataContext, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Called whenever a RPF file has been loaded, successfully or unsuccessfully.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The data used for this event.
        /// </param>
        private void OnLoad(object sender, RpfLoadedEventArgs e)
        {
            if (e.Successful)
            {
                this.Title = Path.GetFileName(e.Path);
                RsApplication app = Application.Current as RsApplication;
                if (app != null)
                {
                    app.AddToMruList("Files", e.Path, new string[0]);
                }
            }
            else
            {
                this.Title = String.Empty;

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.AppendFormat("Failed to load RPF file '{0}'.", e.Path);
                sb.AppendLine();
                sb.AppendLine();
                sb.AppendLine("This normally occurs because RPF decryption has failed.");
                sb.AppendLine();
                sb.AppendLine("Has the RPF been renamed from it's original filename?");
                DispatcherHelper.InvokeOnUI(() =>
                    RsMessageBox.Show(Application.Current.MainWindow, sb.ToString(), "Load Failed", MessageBoxButton.OK, MessageBoxImage.Error));
            }
        }

        /// <summary>
        /// Called whenever a RPF file has been unloaded.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The data used for this event.
        /// </param>
        private void OnUnload(object sender, EventArgs e)
        {
            this.Title = String.Empty;
        }
        #endregion Methods
    } // RpfViewer.MainWindowDataContext {Class}
} // RpfViewer {Namespace}
