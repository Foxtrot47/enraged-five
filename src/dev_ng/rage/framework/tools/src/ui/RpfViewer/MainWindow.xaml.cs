﻿//---------------------------------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RpfViewer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Input;
    using RSG.Editor;
    using RSG.Editor.Controls;
    using RSG.Editor.SharedCommands;
    using RSG.Rpf.Commands;
    using RSG.Rpf.View;
    using RSG.Rpf.View.Commands;
    using RSG.Rpf.ViewModel;

    /// <summary>
    /// Interaction logic for the main window of the application.
    /// </summary>
    public partial class MainWindow : RsMainWindow
    {
        #region Fields
        /// <summary>
        /// The global identifier used for the actions menu.
        /// </summary>
        private static Guid _actionsMenu;

        /// <summary>
        /// The global identifier used for the categories menu.
        /// </summary>
        private static Guid _categoriesMenu;

        /// <summary>
        /// The global identifier used for the filter tool bar.
        /// </summary>
        private static Guid _filterToolBar;

        /// <summary>
        /// The global identifier used for the standard tool bar.
        /// </summary>
        private static Guid _standardToolBar;

        /// <summary>
        /// The global identifier used for the view menu.
        /// </summary>
        private static Guid _viewMenu;

        /// <summary>
        /// A private reference to the data context as an instance of the
        /// <see cref="RpfViewerDataContext"/> class.
        /// </summary>
        private RpfViewerDataContext _rpfViewerDataContext;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="MainWindow" /> class.
        /// </summary>
        static MainWindow()
        {
            _actionsMenu = new Guid("85F5529F-7014-4222-9BFB-F543B7B301C4");
            _categoriesMenu = new Guid("603AC61C-7B60-455A-8C11-03C25FD936E9");
            _filterToolBar = new Guid("D74AF8CB-7688-43A8-9785-277DD7B082E4");
            _standardToolBar = new Guid("E1B8D025-4D8A-40A7-A77D-6264B811A30E");
            _viewMenu = new Guid("FC28EF6B-6906-4732-9966-1CD772444706");
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        /// <param name="application">
        /// The instance to the application this is going to be the main window for.
        /// </param>
        public MainWindow(RsApplication application)
        {
            if (application == null)
            {
                throw new ArgumentNullException(
                    "The current application needs to be of type RsApplication.");
            }

            this.InitializeComponent();
            MainWindowDataContext dataContext = new MainWindowDataContext();
            this._rpfViewerDataContext = dataContext.RpfViewerDataContext;
            this.DataContext = dataContext;
            this.AddCommandInstances(application);
            this.AttachCommandBindings();
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Adds the commands instances and menu instances to the command manager.
        /// </summary>
        /// <param name="application">
        /// The current application instance.
        /// </param>
        private void AddCommandInstances(RsApplication application)
        {
            application.RegisterMruList(
                "Files",
                true,
                RockstarCommands.OpenFileFromMru,
                new Guid("A52BEB37-7C7A-40EC-8F98-1F5349E30255"),
                new Guid("DB95D4C7-1783-4291-9CC1-BC2D2C4625BB"),
                new Guid("7C06AFE4-2E03-4761-8136-EA8D87A5863D"));

            RockstarCommandManager.AddViewMenu(_viewMenu);
            RockstarCommandManager.AddMenu(0, "Categories", _categoriesMenu, _viewMenu);
            RockstarCommandManager.AddActionsMenu(_actionsMenu);
            RockstarCommandManager.AddToolbar(0, 0, "Standard", _standardToolBar);
            RockstarCommandManager.AddToolbar(0, 1, "Filter", _filterToolBar);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.OpenFile,
                0,
                false,
                "Open",
                new Guid("231F1E2D-4577-460B-8112-7ECFFFE5749C"),
                RockstarCommandManager.FileMenuId);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Close,
                1,
                false,
                "Close",
                 new Guid("C1019B3D-FCF2-40F1-B7AB-36F6E8B8D5D5"),
                RockstarCommandManager.FileMenuId);

            RockstarCommandManager.AddCommandInstance(
                RpfViewerCommands.ExtractRpfItems,
                0,
                false,
                "Extract...",
                new Guid("EF691894-B9BE-4A70-8800-73087BE8B9FC"),
                _actionsMenu);

            RockstarCommandManager.AddCommandInstance(
                RpfViewerCommands.ExtractEntireRpf,
                1,
                false,
                "Extract All...",
                new Guid("53F88A56-6A12-4321-ACA6-969F74287D53"),
                _actionsMenu);

            RockstarCommandManager.AddCommandInstance(
                RpfViewerCommands.ViewRpfItem,
                2,
                true,
                "View File",
                new Guid("0A30A8EC-CDF6-4649-A99A-979185FE8055"),
                _actionsMenu);

            RockstarCommandManager.AddCommandInstance(
                RpfViewerCommands.CopyFileName,
                3,
                true,
                "Copy File Name",
                new Guid("EF13F15F-72AB-4D9B-84C6-A16633C5FFFB"),
                _actionsMenu);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.SelectAll,
                ushort.MaxValue - 1,
                true,
                "Select All",
                new Guid("BB219002-A352-4341-963D-5B2C94B8FBE4"),
                _actionsMenu);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.InvertSelection,
                ushort.MaxValue,
                false,
                "Invert Selection",
                new Guid("7BFB9703-3654-4A3C-B3CB-8A0A5243D1C1"),
                _actionsMenu);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.OpenFile,
                0,
                false,
                "Open",
                new Guid("AC7478D2-480F-4259-B255-CECE2B0083AE"),
                _standardToolBar,
                new CommandInstanceCreationArgs()
                {
                    DisplayStyle = CommandItemDisplayStyle.ImageAndText
                });

            RockstarCommandManager.AddCommandInstance(
                RpfViewerCommands.ExtractRpfItems,
                1,
                true,
                "Extract...",
                new Guid("4D6822B0-D8CA-423C-974B-28AF39091217"),
                _standardToolBar,
                new CommandInstanceCreationArgs()
                {
                    DisplayStyle = CommandItemDisplayStyle.ImageAndText
                });

            RockstarCommandManager.AddCommandInstance(
                RpfViewerCommands.ExtractEntireRpf,
                2,
                false,
                "Extract All...",
                new Guid("E4A8C00A-C275-4284-9A09-645B1C6A1AA7"),
                _standardToolBar,
                new CommandInstanceCreationArgs()
                {
                    DisplayStyle = CommandItemDisplayStyle.ImageAndText
                });

            RockstarCommandManager.AddCommandInstance(
                RpfViewerCommands.FilterRpfByCategory,
                0,
                true,
                "Filter Options...",
                new Guid("5C269619-D58B-490E-A424-FCDA86CD7213"),
                _filterToolBar,
                new CommandInstanceCreationArgs()
                {
                    AreMultiItemsShown = true,
                    EachItemStartsGroup = true,
                });

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.Find,
                1,
                true,
                "Find RPF Item...",
                new Guid("DF4C2C05-2095-47D2-8EBD-9A6995343F8C"),
                _filterToolBar);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.FindPrevious,
                2,
                false,
                "Find Previous",
                new Guid("9EF76BAD-72B8-49CA-A5CC-9745260690A6"),
                _filterToolBar);

            RockstarCommandManager.AddCommandInstance(
                RockstarCommands.FindNext,
                3,
                false,
                "Find Next",
                new Guid("3A40916E-09D3-4124-9394-C4AD06EF89B0"),
                _filterToolBar);

            RockstarCommandManager.AddCommandInstance(
                RpfViewerCommands.FilterRpfByCategory,
                0,
                true,
                "Filter Options...",
                new Guid("ED3B44D7-8E1C-461C-B0CA-D8F329F0AA9B"),
                _categoriesMenu,
                new CommandInstanceCreationArgs()
                {
                    AreMultiItemsShown = true
                });

            RockstarCommandManager.AddCommandInstance(
                RpfViewerCommands.ExtractRpfItems,
                0,
                false,
                "Extract...",
                new Guid("B86D3B42-B453-47B4-AB79-9D8E35FC9AA5"),
                RpfViewerCommandIds.ContextMenu);

            RockstarCommandManager.AddCommandInstance(
                RpfViewerCommands.ViewRpfItem,
                1,
                false,
                "View File",
                new Guid("D120AE24-A2A3-489D-A9F8-D16C77B7718A"),
                RpfViewerCommandIds.ContextMenu);

            RockstarCommandManager.AddCommandInstance(
                RpfViewerCommands.CopyFileName,
                2,
                false,
                "Copy File Name",
                new Guid("72AD6D37-8E08-4FF0-8692-514CCD7B3FCF"),
                RpfViewerCommandIds.ContextMenu);
        }

        /// <summary>
        /// Attaches all of the necessary command bindings to this instance.
        /// </summary>
        private void AttachCommandBindings()
        {
            ICommandAction action = new ExtractItemsAction(this.EntriesResolver);
            action.AddBinding(RpfViewerCommands.ExtractEntireRpf, this);

            action = new SelectAllAction(this.ControlsResolver);
            action.AddBinding(RockstarCommands.SelectAll, this);

            action = new InvertSelectionAction(this.ControlsResolver);
            action.AddBinding(RockstarCommands.InvertSelection, this);

            action = new OpenFileAction(this.ViewModelsResolver);
            action.AddBinding(RockstarCommands.OpenFile, this);

            action = new CloseFileAction(this.ViewModelsResolver);
            action.AddBinding(RockstarCommands.Close, this);

            action = new OpenFromMruAction(this.ViewModelsResolver);
            action.AddBinding(RockstarCommands.OpenFileFromMru, this);

            action = new FilterByCategoryAction(this.ControlsResolver);
            action.AddBinding(RpfViewerCommands.FilterRpfByCategory, this);

            SearchAction searchAction = new SearchAction(this.ControlsResolver);
            searchAction.AddBinding(this);
        }

        /// <summary>
        /// Represents the method that is used to retrieve a command parameter object for the
        /// view commands.
        /// </summary>
        /// <param name="commandData">
        /// The command data that has been sent.
        /// </param>
        /// <returns>
        /// The command parameter object for the specified command.
        /// </returns>
        private IEnumerable<RpfViewControl> ControlsResolver(CommandData commandData)
        {
            return Enumerable.Repeat(this.RpfViewerControl, 1);
        }

        /// <summary>
        /// Represents the method that is used to retrieve a command parameter object for the
        /// view commands.
        /// </summary>
        /// <param name="commandData">
        /// The command data that has been sent.
        /// </param>
        /// <returns>
        /// The command parameter object for the specified command.
        /// </returns>
        private IEnumerable<RpfViewerDataContext> ViewModelsResolver(CommandData commandData)
        {
            return Enumerable.Repeat(this._rpfViewerDataContext, 1);
        }

        /// <summary>
        /// Represents the method that is used to retrieve a command parameter object for the
        /// view commands.
        /// </summary>
        /// <param name="commandData">
        /// The command data that has been sent.
        /// </param>
        /// <returns>
        /// The command parameter object for the specified command.
        /// </returns>
        private IEnumerable<PackEntryViewModel> EntriesResolver(CommandData commandData)
        {
            return this._rpfViewerDataContext.AllEntries;
        }
        #endregion Methods
    } // RpfViewer.MainWindow {Class}
} // RpfViewer {Namespace}
