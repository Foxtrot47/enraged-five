

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 7.00.0555 */
/* at Wed Apr 09 12:06:18 2014
 */
/* Compiler settings for ShellExtension.idl:
    Oicf, W1, Zp8, env=Win64 (32b run), target_arch=AMD64 7.00.0555 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __ShellExtension_i_h__
#define __ShellExtension_i_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IRockstarMenu_FWD_DEFINED__
#define __IRockstarMenu_FWD_DEFINED__
typedef interface IRockstarMenu IRockstarMenu;
#endif 	/* __IRockstarMenu_FWD_DEFINED__ */


#ifndef __IRockstarShellFolder_FWD_DEFINED__
#define __IRockstarShellFolder_FWD_DEFINED__
typedef interface IRockstarShellFolder IRockstarShellFolder;
#endif 	/* __IRockstarShellFolder_FWD_DEFINED__ */


#ifndef __RockstarMenu_FWD_DEFINED__
#define __RockstarMenu_FWD_DEFINED__

#ifdef __cplusplus
typedef class RockstarMenu RockstarMenu;
#else
typedef struct RockstarMenu RockstarMenu;
#endif /* __cplusplus */

#endif 	/* __RockstarMenu_FWD_DEFINED__ */


#ifndef __RockstarShellFolder_FWD_DEFINED__
#define __RockstarShellFolder_FWD_DEFINED__

#ifdef __cplusplus
typedef class RockstarShellFolder RockstarShellFolder;
#else
typedef struct RockstarShellFolder RockstarShellFolder;
#endif /* __cplusplus */

#endif 	/* __RockstarShellFolder_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __IRockstarMenu_INTERFACE_DEFINED__
#define __IRockstarMenu_INTERFACE_DEFINED__

/* interface IRockstarMenu */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IRockstarMenu;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("20603B4A-C767-49C9-BC4A-B850A9C2EDAB")
    IRockstarMenu : public IUnknown
    {
    public:
    };
    
#else 	/* C style interface */

    typedef struct IRockstarMenuVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IRockstarMenu * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IRockstarMenu * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IRockstarMenu * This);
        
        END_INTERFACE
    } IRockstarMenuVtbl;

    interface IRockstarMenu
    {
        CONST_VTBL struct IRockstarMenuVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IRockstarMenu_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IRockstarMenu_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IRockstarMenu_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IRockstarMenu_INTERFACE_DEFINED__ */


#ifndef __IRockstarShellFolder_INTERFACE_DEFINED__
#define __IRockstarShellFolder_INTERFACE_DEFINED__

/* interface IRockstarShellFolder */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IRockstarShellFolder;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("1EAAB775-1199-4370-AB20-D690982B59C0")
    IRockstarShellFolder : public IUnknown
    {
    public:
    };
    
#else 	/* C style interface */

    typedef struct IRockstarShellFolderVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IRockstarShellFolder * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IRockstarShellFolder * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IRockstarShellFolder * This);
        
        END_INTERFACE
    } IRockstarShellFolderVtbl;

    interface IRockstarShellFolder
    {
        CONST_VTBL struct IRockstarShellFolderVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IRockstarShellFolder_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IRockstarShellFolder_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IRockstarShellFolder_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IRockstarShellFolder_INTERFACE_DEFINED__ */



#ifndef __ShellExtensionLib_LIBRARY_DEFINED__
#define __ShellExtensionLib_LIBRARY_DEFINED__

/* library ShellExtensionLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_ShellExtensionLib;

EXTERN_C const CLSID CLSID_RockstarMenu;

#ifdef __cplusplus

class DECLSPEC_UUID("F818B71E-A877-4FEA-B4A8-321216C4433B")
RockstarMenu;
#endif

EXTERN_C const CLSID CLSID_RockstarShellFolder;

#ifdef __cplusplus

class DECLSPEC_UUID("03AA3F77-3ECB-49E7-B451-8B0167D2DC0E")
RockstarShellFolder;
#endif
#endif /* __ShellExtensionLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


