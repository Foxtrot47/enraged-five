#ifndef PIDLMGR_H
#define PIDLMGR_H

//
// File:: PidlMgr.h
// Description:: PIDL Manager Class.
//
// Author:: David Muir <david.muir@rockstarnorth.com>
// Date:: 26 February 2014
//

#if defined _MSC_VER && _MSC_VER >= 1300
#pragma once
#endif

// Local headers
#include "RockstarShellConfig.h"

// Windows headers
#include "shtypes.h"

typedef enum tagITEM_TYPE
{
	RSG_ALIAS = 0x00000001,
	RSG_WEBPAGE = 0x00000002,
} ITEM_TYPE;

typedef struct tagPIDLDATA
{
	USHORT		cb;			// Size of structure.
	ITEM_TYPE	type;		// Folder or file.
	TCHAR		szName[1];
} PIDLDATA, FAR *LPPIDLDATA;

/**
 * PIDL Manager class; every item in the shell namespace must have a unique PIDL.
 * This class is used to manage these PIDLs for the Shell Folder.
 */
class CPidlMgr
{
public:
	LPITEMIDLIST	Create( ITEM_TYPE iItemType, LPTSTR szName );
	void			Delete( LPITEMIDLIST pidl );
	LPITEMIDLIST	GetNextItem( LPCITEMIDLIST pidl );
	LPITEMIDLIST	GetLastItem( LPCITEMIDLIST pidl );
	UINT			GetByteSize( LPCITEMIDLIST pidl );
	bool			IsSingle( LPCITEMIDLIST pidl );
	LPITEMIDLIST	Concatenate( LPCITEMIDLIST pidl1, LPCITEMIDLIST pidl2 );
	LPITEMIDLIST	Copy( LPCITEMIDLIST pidlSrc );

	// Retrieve encrypted file name (without the path)
	// The pidl MUST remain valid until the caller has finished with the returned string.
	HRESULT			GetName( LPCITEMIDLIST, LPTSTR );

	// Retrieve the item type (see above)
	ITEM_TYPE		GetItemType( LPCITEMIDLIST pidl );

	HRESULT			GetFullName( LPCITEMIDLIST pidl, LPTSTR szFullName, DWORD *pdwLen ); 
	BOOL			HasSubFolder( LPCITEMIDLIST pidl); 
	HRESULT			GetItemAttributes( LPCITEMIDLIST pidl, USHORT iAttrNum, LPTSTR pszAttrOut );

private:
	LPPIDLDATA		GetDataPointer( LPCITEMIDLIST pidl );
};

#endif // PIDLMGR_H
