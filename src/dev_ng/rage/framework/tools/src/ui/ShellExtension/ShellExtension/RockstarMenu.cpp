//
// File:: RockstarMenu.cpp
// Description:: "Rockstar Games" menu implementation
//
// Author:: David Muir <david.muir@rockstarnorth.com>
// Date:: 4 June 2010
//

//---------------------------------------------------------------------------
// Preprocessor Includes
//---------------------------------------------------------------------------

// Local headers
#include "RockstarMenu.h"
#include "RockstarShellConfig.h"
#include "dllmain.h"

// configParser headers
#include "configParser/configGameView.h"

// configUtil headers
#include "configUtil/configUtil.h"
#include "configUtil/environment.h"
#include "configUtil/path.h"
#include "configUtil/VersionInfo.h"
using namespace configUtil;

// ConfigParser and ConfigUtil headers
#include "configParser/configCoreView.h"

// Boost headers
#include "boost/regex.hpp"

// Windows headers
#include <Windows.h>
#include <winver.h>

//---------------------------------------------------------------------------
// Preprocessor Defines
//---------------------------------------------------------------------------
#define ROOT_MENU_NAME		(_T("Rockstar Games"))
#define ROOT_MENU_NAME_LEN	(strlen("Rockstar Games"))
#define ROOT_MENU_XML		(_T("shell.xml"))
#define ROOT_MENU_META		(_T("shell.meta"))
#define TITLE				(_T("Rockstar Games Shell Extension"))
#define COPYRIGHT			(_T("Copyright � Rockstar Games 2010-2014"))
#define HELP_HREF			(_T("https://devstar.rockstargames.com/wiki/index.php/Shell_Extension"))
#define MAX_ARGS_LEN		(2048) // http://blogs.msdn.com/b/oldnewthing/archive/2003/12/10/56028.aspx

//---------------------------------------------------------------------------
// Public Methods
//---------------------------------------------------------------------------

//construction and destruction:
CRockstarMenu::CRockstarMenu()
	: m_eShellConfigType( Unknown )
{
	// LibXML2 Initialisation.
	xmlInitParser( );
	LIBXML_TEST_VERSION;
	
	memset( m_sShellFilename, 0, sizeof( TCHAR ) * MAX_PATH );
	memset( m_sDirectory, 0, sizeof( TCHAR ) * MAX_PATH );
	m_hRsBitmap16 = LoadBitmap( _pModule->GetResourceInstance(), MAKEINTRESOURCE( IDB_ROCKSTAR16 ) );
	m_hRsBitmap32 = LoadBitmap( _pModule->GetResourceInstance(), MAKEINTRESOURCE( IDB_ROCKSTAR32 ) );
}

CRockstarMenu::~CRockstarMenu()
{
	DeleteObject( m_hRsBitmap32 );
	DeleteObject( m_hRsBitmap16 );

	xmlCleanupParser( );
}

//we're called here before the menu is displayed with information about what files were selected upon a right click.
STDMETHODIMP 
CRockstarMenu::Initialize( PCIDLIST_ABSOLUTE pidlFolder, LPDATAOBJECT pDataObj, HKEY hProgID )
{
	m_nMaxID = 0;
	if ( pidlFolder )
	{
		m_bIsFolder = true;
		TCHAR tmpFile[MAX_PATH];
		if ( SHGetPathFromIDList( pidlFolder, tmpFile ) )
		{
			memset( m_sDirectory, 0, sizeof( TCHAR ) * MAX_PATH );
			_tcscpy_s( m_sDirectory, MAX_PATH, tmpFile );
			return ( S_OK );
		}
		else
		{
			return ( E_INVALIDARG );
		}
	}
	else
	{
		m_bIsFolder = false;
		FORMATETC fmt = {CF_HDROP, NULL, DVASPECT_CONTENT, -1, TYMED_HGLOBAL};
		STGMEDIUM stg = {TYMED_HGLOBAL};
		HDROP hDrop;

		if ( !pDataObj )
			return ( E_INVALIDARG );
		if( FAILED(pDataObj->GetData(&fmt,&stg)))
			return ( E_INVALIDARG );
	 
		//Get a pointer to the actual data.
		hDrop = (HDROP)GlobalLock(stg.hGlobal);

		//Make sure it worked.
		if ( 0 == hDrop )
			return E_INVALIDARG;

		UINT uNumFiles = DragQueryFile( hDrop, 0xFFFFFFFF, NULL, 0 );
		HRESULT hr = S_OK;

		if ( 0 == uNumFiles )
		{
			GlobalUnlock(stg.hGlobal);
			ReleaseStgMedium(&stg);
			return ( E_INVALIDARG );
		}

		// Process our list of selected file(s)/directories...
		unsigned long i;
		TCHAR tmpFile[MAX_PATH];
		for ( i=0; i < uNumFiles; ++i )
		{
			if ( 0 == DragQueryFile( hDrop, i, tmpFile, MAX_PATH ) )
			{
				hr = E_INVALIDARG;
			}
			else if ( Path::is_directory( tmpFile ) )
			{
				m_bIsFolder = true;
				this->m_lFiles.push_front( tmpFile );
				if ( 0 == i )
					_tcscpy_s( m_sDirectory, MAX_PATH, tmpFile );
			}
			else
			{
				this->m_lFiles.push_front( tmpFile );
				if ( 0 == i )
					Path::get_directory( m_sDirectory, MAX_PATH, tmpFile );
			}
		}

		GlobalUnlock(stg.hGlobal);
		ReleaseStgMedium(&stg);
	}
	return ( S_OK );
}

//here we are asked to add our menu items to hmenu, the shell's context menu
HRESULT 
CRockstarMenu::QueryContextMenu( HMENU hmenu, UINT uMenuIndex, UINT uidFirstCmd, UINT uidLastCmd, UINT uFlags )
{
	//Note: the CMF_DEFAULTONLY flag means (I think) that the user has double clicked the item, and
	//hence is performing the default action.  So, for a menu extension like ours, we should
	//not add any items and just return.
	if(uFlags & CMF_DEFAULTONLY)
		return MAKE_HRESULT(SEVERITY_SUCCESS,FACILITY_NULL,0);
	//Otherwise, we'll add our menu items.  You can also get a hold of some other flags (like whether or not
	//the shift key is pressed for extended verbs) but I don't know how many end users even know that
	//functionality exists, so I'm not going to mess with it.  Cool, though.
	unsigned long originalFirst = uidFirstCmd; //we'll compute how many items we added from this.

	//
	HMENU hm = CreatePopupMenu();
	MENUITEMINFO mii;
	mii.cbSize = sizeof( MENUITEMINFO );
	mii.fMask = MIIM_TYPE | MIIM_SUBMENU;
	mii.fType = MFT_STRING;
	mii.hSubMenu = hm;
	mii.dwTypeData = ROOT_MENU_NAME;
	mii.cch = (unsigned int)ROOT_MENU_NAME_LEN;
	InsertMenuItem( hmenu, uMenuIndex++,MF_STRING | MF_BYPOSITION,&mii );
	if ( NULL != m_hRsBitmap16 )
		SetMenuItemBitmaps( hmenu, uMenuIndex-1, MF_BYPOSITION, m_hRsBitmap16, NULL );

	bool shellXmlOk = ParseShellConfig( hm, uidFirstCmd );
	if ( !shellXmlOk )
	{
		// Default Menu when ROOT_MENU_XML is not found.
		AppendMenu( hm, MF_STRING | MF_DISABLED, uidFirstCmd, 
			L"No XML data found or invalid config file version.  Menu uninitialised." );
	}
	TCommandHandler* pHelpHandler = 
		new TCommandHandler( _T("Help Topics..."), _T(":help"), _T("Display help information.") );
	TCommandHandler* pAboutHandler = 
		new TCommandHandler( _T("About..."), _T(":about"), _T("Display extension about information.") );

	CreateSeparatorItem( hm );
	CreateMenuItem( pHelpHandler, hm, uidFirstCmd );
	CreateMenuItem( pAboutHandler, hm, uidFirstCmd );
	m_nMaxID = uidFirstCmd;

	// The return value seems to tell the shell how many items with commands 
	// we've added (leaf items).
	return ( MAKE_HRESULT( SEVERITY_SUCCESS, FACILITY_NULL, uidFirstCmd-originalFirst ) );
}

//if explorer has a status bar displayed, then we can provide some help text in it via this function
HRESULT 
CRockstarMenu::GetCommandString( UINT_PTR idCmd, UINT uFlags, UINT *pwReserved, LPSTR pszName, UINT cchMax )
{
	//If Explorer is asking for a help string, copy our string into the supplied buffer:
	TCHAR hlpText[MAX_PATH]; //stores our help message for the user.
	bool idMatch = false; //if the id sent is one of ours, then we'll set this to true and copy the strings
	//since the recent list is always small (bounded by a constant) I will keep the list representation, since
	//the lack of random access doesn't really hurt.

	if(uFlags & GCS_HELPTEXT)
	{
		Assertf( idCmd < m_nMaxID, "Menu ID out of range." );
		if ( idCmd >= m_nMaxID )
			return ( E_INVALIDARG );
		if ( idCmd >= m_mCmdHandlers.size() )
			return ( E_INVALIDARG );

		TCommandHandler* pHandler = m_mCmdHandlers[idCmd];
		_tcscpy_s( hlpText, pHandler->tooltip.c_str() );

		if ( uFlags & GCS_UNICODE ) //then pszName is actually wchars, not chars.
			::wcsncpy(reinterpret_cast<wchar_t*>(pszName),hlpText,cchMax);
		else
			::wcstombs(pszName, hlpText, cchMax);

		return ( S_OK );
	}
	return ( E_INVALIDARG );
}

//this function is called when the users makes a selection that belongs to us.
HRESULT 
CRockstarMenu::InvokeCommand( LPCMINVOKECOMMANDINFO pCmdInfo )
{
	//If lpVerb really points to a string, ignore this function call and bail out.
	//Probably would have been taken care of by our switch statement anyway, but why
	//not just get this case out of the way now.
	if ( 0 != HIWORD(pCmdInfo->lpVerb ) )
		return E_INVALIDARG;

	//Note that the command index appears to be relative to items we added rather than absolute positions.
	unsigned long idCmd = LOWORD(pCmdInfo->lpVerb);
	Assertf( idCmd < m_nMaxID, "Menu ID out of range." );
	if ( idCmd >= m_nMaxID )
		return ( E_INVALIDARG );

	// Menu item is valid, determine if its a built-in handler or a custom
	// ShellExecute action!
	TCommandHandler* handler = m_mCmdHandlers[idCmd];
	if ( L":help" == handler->cmd )
	{
		ShellExecute( 0, L"open", HELP_HREF, L"", L"", SW_NORMAL );
	}
	else if ( L":about" == handler->cmd )
	{
		ShowAboutBox( );
	}
	else
	{
		// No built-in handler found so lets try to execute the handler.
		// Assuming its a compatible line for environment path substitution
		// and then passing into ShellExecute.
		STL_STRING fileList;
		for ( string_list::iterator it = m_lFiles.begin(); 
			  it != m_lFiles.end();
			  ++it )
		{
			fileList += *it;
			fileList += _T(" ");
		}

		Environment env;
		env.import_core_globals( );
		env.import_secondary_globals( );
		env.add( _T("dir"), m_sDirectory );
		env.add( _T("files"), fileList.c_str() );
		
		TCHAR cmd[MAX_PATH], args[MAX_ARGS_LEN];
		env.subst( cmd, MAX_PATH, handler->cmd.c_str() );
		env.subst( args, MAX_ARGS_LEN, handler->args.c_str() );

		TCHAR cmd2[MAX_PATH];
		TCHAR args2[MAX_ARGS_LEN];
		memset( cmd2, 0, MAX_PATH * sizeof( TCHAR ) );
		memset( args2, 0, MAX_ARGS_LEN * sizeof( TCHAR ) );
		ExpandEnvironmentStrings( cmd, cmd2, MAX_PATH );
		ExpandEnvironmentStrings( args, args2, MAX_ARGS_LEN );
		_tcscpy_s( cmd, MAX_PATH, cmd2 );
		_tcscpy_s( args, MAX_ARGS_LEN, args2 );

		int result = (int)ShellExecute( NULL, NULL, 
			cmd, args, _T(""), SW_SHOWDEFAULT );
#ifndef _DEBUG
		if ( result < 32 )
#endif // !_DEBUG
		{
			TCHAR buffer[4096] = { 0 };
			DWORD dwError = GetLastError( );

			// Default handler.
			wchar_t digits[32];
			::_ltow(idCmd+1,digits,10);
			std::wstring msg = L"You selected item ";
			msg += digits;
			msg += L" on the following files:\n";
			string_list::iterator i;
			for(i=this->m_lFiles.begin(); i!=this->m_lFiles.end(); i++)
				msg += (*i) + L"\n";
			
			msg += L"\nResolved handler:  " + STL_STRING(cmd);
			msg += L"\nHandler arguments: " + STL_STRING(args);
			msg += L"\n";

			::FormatMessage( FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
				NULL, dwError, 0, buffer, 4096, NULL );
			msg += L"\nLast Win32 Error:  ";
			msg += buffer;		

			::MessageBox( 0, msg.c_str(), TITLE, 0 );
		}
	}

	return ( S_OK );
}

//---------------------------------------------------------------------------
// Public Methods : IContextMenu2
//---------------------------------------------------------------------------

HRESULT
CRockstarMenu::HandleMenuMsg( UINT uMsg, WPARAM wParam, LPARAM lParam )
{
	return ( S_OK );
}

//---------------------------------------------------------------------------
// Public Methods : IContextMenu3
//---------------------------------------------------------------------------

HRESULT
CRockstarMenu::HandleMenuMsg2( UINT uMsg, WPARAM wParam, LPARAM lParam, LRESULT* plResult )
{
	return ( S_OK );
}

//---------------------------------------------------------------------------
// Protected Methods
//---------------------------------------------------------------------------

void
CRockstarMenu::ClearHandlers( )
{
	for ( TCommandHandlerMapIter it = m_mCmdHandlers.begin();
		  it != m_mCmdHandlers.end();
		  ++it )
	{
		TCommandHandler* pHandler = (*it);
		if ( pHandler )
			delete ( pHandler );
	}
	m_mCmdHandlers.clear();
}

CRockstarMenu::TCommandHandler*	
CRockstarMenu::CreateHandler( configUtil::Environment* pEnv, shell::MenuCommand* mi )
{	
	TCHAR name[MAX_PATH] = {0};
	TCHAR handler[MAX_PATH] = {0};
	TCHAR args[MAX_ARGS_LEN] = {0};
	TCHAR filter[MAX_PATH] = {0};
	TCHAR tooltip[MAX_ARGS_LEN] = {0};
	
	AnsiToTChar( name, MAX_PATH, mi->Name );
	pEnv->subst( name, MAX_PATH );
	AnsiToTChar( handler, MAX_PATH, mi->Handler );
	pEnv->subst( handler, MAX_PATH );
	AnsiToTChar( args, MAX_PATH, mi->Arguments );
	pEnv->subst( args, MAX_PATH );
	AnsiToTChar( filter, MAX_PATH, mi->Filter );
	pEnv->subst( filter, MAX_PATH );
	AnsiToTChar( tooltip, MAX_PATH, mi->ToolTip );
	pEnv->subst( tooltip, MAX_PATH );

	return ( new TCommandHandler( name, handler, args, filter, tooltip ) ) ;
}

CRockstarMenu::TCommandHandler*
CRockstarMenu::CreateHandler( configUtil::Environment* pEnv, xmlNodePtr pNode )
{
	xmlElementPtr pElem = reinterpret_cast<xmlElementPtr>( pNode );
	TCHAR name[MAX_PATH] = {0};
	TCHAR handler[MAX_PATH] = {0};
	TCHAR args[MAX_ARGS_LEN] = {0};
	TCHAR filter[MAX_PATH] = {0};
	TCHAR tooltip[MAX_ARGS_LEN] = {0};

	xmlAttributePtr pAttr = pElem->attributes;
	while ( pAttr )
	{
		if ( xmlStrEqual( BAD_CAST "name", pAttr->name ) )
		{
			AnsiToTChar( name, MAX_PATH, (const char*)pAttr->children->content );
			pEnv->subst( name, MAX_PATH );
		}
		else if ( xmlStrEqual( BAD_CAST "handler", pAttr->name ) )
		{
			AnsiToTChar( handler, MAX_PATH, (const char*)pAttr->children->content );
			pEnv->subst( handler, MAX_PATH );
		}
		else if ( xmlStrEqual( BAD_CAST "arguments", pAttr->name ) )
		{
			AnsiToTChar( args, MAX_ARGS_LEN, (const char*)pAttr->children->content );
			pEnv->subst( args, MAX_PATH );
		}
		else if ( xmlStrEqual( BAD_CAST "filter", pAttr->name ) )
		{
			AnsiToTChar( filter, MAX_PATH, (const char*)pAttr->children->content );
			pEnv->subst( filter, MAX_PATH );
		}
		else if ( xmlStrEqual( BAD_CAST "tooltip", pAttr->name ) )
		{	
			AnsiToTChar( tooltip, MAX_PATH, (const char*)pAttr->children->content );
			pEnv->subst( tooltip, MAX_PATH );
		}
		pAttr = (xmlAttributePtr)( pAttr->next );
	}

	return ( new TCommandHandler( name, handler, args, filter, tooltip ) ) ;
}

bool
CRockstarMenu::IsHandlerValid( TCommandHandler* pHandler )
{
	// Validate that this command has $(dir) if its a directory or $(files)
	// if files are selected.  If arguments contains neither then it will
	// always be enabled.
	if ( pHandler->filter.size() > 0 )
	{
		if ( 0 == _tcscmp( pHandler->filter.c_str(), _T("Directory") ) )
		{
			return ( m_bIsFolder );
		}
		else
		{
			boost::wregex re( pHandler->filter );
			boost::wcmatch what;
			bool match = true;

			for ( std::list<STL_STRING>::iterator it = m_lFiles.begin();
				 it != m_lFiles.end();
				 ++it )
			{
				const TCHAR* s = (*it).c_str();
				match &= boost::regex_match( (*it).c_str(), what, re );
			}
			return ( match );
		}
	}
	else
	{
		return ( true );
	}

	// Disable command by default.
	return ( false );
}

bool
CRockstarMenu::ParseShellConfig( HMENU& hm, UINT& uidCmd )
{
	// We try finding and using the XML file defined in "RSG_SHELL".  If this
	// is not set then we default back to using the old ConfigParser library.
	// This is useful for testing and should allow us to break free from projects
	// in the future.
	configParser::ConfigGameView gv( false );
	if ( 0 == GetEnvironmentVariable( _T("RSG_SHELL"), m_sShellFilename, MAX_PATH ) )
	{
		TCHAR toolsConfigDir[MAX_PATH] = {0};
		TCHAR configFilename[MAX_PATH] = {0};
		gv.GetToolsConfigDir( toolsConfigDir, MAX_PATH );
		Path::combine( configFilename, MAX_PATH, 2L, (TCHAR*)toolsConfigDir, (TCHAR*)ROOT_MENU_META );
		m_eShellConfigType = Meta;

		// If the metadata file doesn't exist then we fallback to old XML.
		if ( !FileExists( configFilename ) )
		{
			_tcsset_s( configFilename, MAX_PATH, 0 );
			Path::combine( configFilename, MAX_PATH, 2L, (TCHAR*)toolsConfigDir, (TCHAR*)ROOT_MENU_XML );
			m_eShellConfigType = XML;
		}
		_tcscpy_s( m_sShellFilename, configFilename );
	}
	else
	{
		// Determine which type the RSG_SHELL is set to.
		if ( NULL != _tcsstr( m_sShellFilename, _T(".meta") ) )
			m_eShellConfigType = Meta;
		else
			m_eShellConfigType = XML;
	}
	char ansiFilename[MAX_PATH] = {0};
	TCharToAnsi( ansiFilename, MAX_PATH, m_sShellFilename );

	ClearHandlers();
	if ( !FileExists( m_sShellFilename ) )
		return ( false );

	bool result = true;
	switch ( m_eShellConfigType )
	{
	case XML:
		{
			xmlDocPtr pXmlDoc = xmlParseFile( ansiFilename );
			if ( !pXmlDoc )
				return ( false );

			xmlNode* pNode = xmlDocGetRootElement( pXmlDoc )->children;
			while ( NULL != pNode )
			{
				BuildMenuItem( &gv, gv.GetEnvironment(), hm, uidCmd, pNode );
				pNode = pNode->next;
			}
			xmlFreeDoc( pXmlDoc );
		}
		break;
	case Meta:
		{
			if ( !ReloadConfig( ansiFilename ) )
				return ( false );

			// Verify configuration version.
			if ( CONFIG_FILE_VERSION != s_pShellConfig->Version )
				return ( false );

			// Walk our DOM.
			for ( int n = 0; n < s_pShellConfig->Menu.Items.GetCount(); ++n )
			{
				shell::MenuItem* pMenuItem = s_pShellConfig->Menu.Items[n];
				result &= BuildMenuItem( &gv, gv.GetEnvironment(), hm, uidCmd, pMenuItem );
			}		
		}
		break;
	default:
		result = false;
		break;
	}

	return ( result );
}

bool
CRockstarMenu::BuildMenuItem( const configParser::ConfigGameView* gv, 
	configUtil::Environment* pEnv, HMENU& parentMenu, UINT& uidCmd, shell::MenuItem* pMenuItem )
{
	bool result = true;
	if ( dynamic_cast<shell::MenuInclude*>( pMenuItem ) )
	{
		// DHM TODO
	}
	else if ( dynamic_cast<shell::Menu*>( pMenuItem ) )
	{
		// Build menu-item.
		shell::Menu* pMenu = static_cast<shell::Menu*>( pMenuItem );
		if ( !pMenu->Visible )
			return ( true ); // Skip.

		TCHAR name[MAX_PATH] = {0};
		AnsiToTChar( name, MAX_PATH, pMenu->Name );
		pEnv->subst( name, MAX_PATH );

		HMENU subhm = CreatePopupMenu();
		MENUITEMINFO mii;
		mii.cbSize = sizeof( MENUITEMINFO );
		mii.fMask = MIIM_TYPE | MIIM_SUBMENU;
		mii.fType = MFT_STRING;
		mii.hSubMenu = subhm;
		mii.dwTypeData = name;
		mii.cch = (unsigned int)( _tcslen( name ) );
		InsertMenuItem( parentMenu, uidCmd, MF_STRING | MF_BYPOSITION, &mii );

		for ( int n = 0; n < pMenu->Items.GetCount(); ++n )
		{
			shell::MenuItem* pSubMenuItem = pMenu->Items[n];
			result &= BuildMenuItem( gv, pEnv, subhm, uidCmd, pSubMenuItem );
		}
	}
	else if ( dynamic_cast<shell::MenuCommand*>( pMenuItem ) )
	{
		shell::MenuCommand* pCommand = static_cast<shell::MenuCommand*>( pMenuItem );
		TCommandHandler* pHandler = CreateHandler( pEnv, pCommand );
		bool enabled = IsHandlerValid( pHandler );
		CreateMenuItem( pHandler, parentMenu, uidCmd, NULL, enabled );
	}
	else if ( dynamic_cast<shell::AutoMenu*>( pMenuItem ) )
	{
		// Read name and variable attributes if available. 
		shell::AutoMenu* pAutoMenu = static_cast<shell::AutoMenu*>( pMenuItem );
		TCHAR name[MAX_PATH] = {0};
		TCHAR variable[MAX_PATH] = {0};
		AnsiToTChar( name, MAX_PATH, pAutoMenu->Name );
		AnsiToTChar( variable, MAX_PATH, pAutoMenu->Variable );
		if ( 0 == _tcslen( name ) )
			_tcscpy_s( name, MAX_PATH, ROOT_MENU_NAME );

		// Per-branch menu.
		if ( 0 == _tcsicmp( _T("$(branch)"), variable ) )
		{
			int numBranches = gv->GetNumBranches( );
			for ( int n = 0; n < numBranches; ++n )
			{
				TCHAR branchkey[MAX_PATH] = {0};
				gv->GetBranchKey( n, branchkey, MAX_PATH );
				pEnv->push( );
				pEnv->add( _T("branch"), branchkey );

				TCHAR menuname[MAX_PATH] = {0};
				_tcscpy_s( menuname, MAX_PATH, name );
				pEnv->subst( menuname, MAX_PATH );

				HMENU subhm = CreatePopupMenu();
				MENUITEMINFO mii;
				mii.cbSize = sizeof( MENUITEMINFO );
				mii.fMask = MIIM_TYPE | MIIM_SUBMENU;
				mii.fType = MFT_STRING;
				mii.hSubMenu = subhm;
				mii.dwTypeData = menuname;
				mii.cch = (unsigned int)( _tcslen( menuname ) );
				InsertMenuItem( parentMenu, uidCmd, MF_STRING | MF_BYPOSITION, &mii );

				// Parse children (recurse)
				for ( int n = 0; n < pAutoMenu->Items.GetCount(); ++n )
				{
					shell::MenuItem* pSubMenuItem = pAutoMenu->Items[n];
					result &= BuildMenuItem( gv, pEnv, subhm, uidCmd, pSubMenuItem );
				}
				pEnv->pop( );
			}
		}
		// Per-target menu.
		else if ( 0 == _tcsicmp( _T("$(target)"), variable ) )
		{
			int numTargets = gv->GetNumTargets( );
			for ( int n = 0; n < numTargets; ++n )
			{
				TCHAR targetkey[MAX_PATH] = {0};
				gv->GetTargetKey( n, targetkey, MAX_PATH );
				pEnv->push( );
				pEnv->add( _T("target"), targetkey );

				TCHAR menuname[MAX_PATH] = {0};
				_tcscpy_s( menuname, MAX_PATH, name );
				pEnv->subst(menuname, MAX_PATH );

				HMENU subhm = CreatePopupMenu();
				MENUITEMINFO mii;
				mii.cbSize = sizeof( MENUITEMINFO );
				mii.fMask = MIIM_TYPE | MIIM_SUBMENU;
				mii.fType = MFT_STRING;
				mii.hSubMenu = subhm;
				mii.dwTypeData = menuname;
				mii.cch = (unsigned int)( _tcslen( menuname ) );
				InsertMenuItem( parentMenu, uidCmd, MF_STRING | MF_BYPOSITION, &mii );

				// Parse children (recurse)
				for ( int n = 0; n < pAutoMenu->Items.GetCount(); ++n )
				{
					shell::MenuItem* pSubMenuItem = pAutoMenu->Items[n];
					result &= BuildMenuItem( gv, pEnv, subhm, uidCmd, pSubMenuItem );
				}
				pEnv->pop( );
			}
		}
	}
	else if ( dynamic_cast<shell::Separator*>( pMenuItem ) )
	{
		CreateSeparatorItem( parentMenu );
	}
	else
	{
		Assertf( false, "Unsupported MenuItem." );
	}

	return (result);
}

bool
CRockstarMenu::BuildMenuItem( const configParser::ConfigGameView* gv, 
	configUtil::Environment* pEnv, HMENU& parentMenu, UINT& uidCmd, xmlNodePtr pNode )
{
	assert( pNode );
	if ( !pNode )
		return ( false );

	if ( xmlStrEqual( BAD_CAST "menuitem", BAD_CAST pNode->name ) ) 
	{
		TCommandHandler* pHandler = CreateHandler( pEnv, pNode );
		bool enabled = IsHandlerValid( pHandler );
		CreateMenuItem( pHandler, parentMenu, uidCmd, NULL, enabled );
	}
	else if ( xmlStrEqual( BAD_CAST "menu", BAD_CAST pNode->name ) )
	{
		// Read name attribute if available.  Otherwise default to ROOT_MENU_NAME.
		TCHAR name[MAX_PATH] = {0};
		xmlElementPtr pElem = reinterpret_cast<xmlElementPtr>( pNode );
		xmlAttributePtr pAttr = pElem->attributes;
		while ( pAttr )
		{
			if ( xmlStrEqual( BAD_CAST "name", pAttr->name ) )
			{
				AnsiToTChar( name, MAX_PATH, (const char*)pAttr->children->content );
			}
			pAttr = (xmlAttributePtr)( pAttr->next );
		}
		if ( 0 == _tcslen( name ) )
			_tcscpy_s( name, MAX_PATH, ROOT_MENU_NAME );

		HMENU subhm = CreatePopupMenu();
		MENUITEMINFO mii;
		mii.cbSize = sizeof( MENUITEMINFO );
		mii.fMask = MIIM_TYPE | MIIM_SUBMENU;
		mii.fType = MFT_STRING;
		mii.hSubMenu = subhm;
		mii.dwTypeData = name;
		mii.cch = (unsigned int)( _tcslen( name ) );
		InsertMenuItem( parentMenu, uidCmd, MF_STRING | MF_BYPOSITION, &mii );

		// Parse children (recurse)
		xmlNodePtr pChildNode = pNode->children;
		while ( NULL != pChildNode )
		{
			BuildMenuItem( gv, pEnv, subhm, uidCmd, pChildNode );
			pChildNode = pChildNode->next;
		}
	}
	else if ( xmlStrEqual( BAD_CAST "automenu", BAD_CAST pNode->name ) )
	{
		// Read name and variable attributes if available. 
		TCHAR name[MAX_PATH] = {0};
		TCHAR variable[MAX_PATH] = {0};
		xmlElementPtr pElem = reinterpret_cast<xmlElementPtr>( pNode );
		xmlAttributePtr pAttr = pElem->attributes;
		while ( pAttr )
		{
			if ( xmlStrEqual( BAD_CAST "name", BAD_CAST pAttr->name ) )
			{
				AnsiToTChar( name, MAX_PATH, (const char*)pAttr->children->content );
			}
			else if ( xmlStrEqual( BAD_CAST "variable", BAD_CAST pAttr->name ) )
			{
				AnsiToTChar( variable, MAX_PATH, (const char*)pAttr->children->content );
			}
			pAttr = (xmlAttributePtr)( pAttr->next );
		}
		if ( 0 == _tcslen( name ) )
			_tcscpy_s( name, MAX_PATH, ROOT_MENU_NAME );

		// Per-branch menu.
		if ( 0 == _tcsicmp( _T("$(branch)"), variable ) )
		{
			int numBranches = gv->GetNumBranches( );
			for ( int n = 0; n < numBranches; ++n )
			{
				TCHAR branchkey[MAX_PATH] = {0};
				gv->GetBranchKey( n, branchkey, MAX_PATH );
				pEnv->push( );
				pEnv->add( _T("branch"), branchkey );

				TCHAR menuname[MAX_PATH] = {0};
				_tcscpy_s( menuname, MAX_PATH, name );
				pEnv->subst(menuname, MAX_PATH );

				HMENU subhm = CreatePopupMenu();
				MENUITEMINFO mii;
				mii.cbSize = sizeof( MENUITEMINFO );
				mii.fMask = MIIM_TYPE | MIIM_SUBMENU;
				mii.fType = MFT_STRING;
				mii.hSubMenu = subhm;
				mii.dwTypeData = menuname;
				mii.cch = (unsigned int)( _tcslen( menuname ) );
				InsertMenuItem( parentMenu, uidCmd, MF_STRING | MF_BYPOSITION, &mii );

				// Parse children (recurse)
				xmlNodePtr pChildNode = pNode->children;
				while ( NULL != pChildNode )
				{
					BuildMenuItem( gv, pEnv,subhm, uidCmd, pChildNode );
					pChildNode = pChildNode->next;
				}
				pEnv->pop( );
			}
		}
		// Per-target menu.
		else if ( 0 == _tcsicmp( _T("$(target)"), variable ) )
		{
			int numTargets = gv->GetNumTargets( );
			for ( int n = 0; n < numTargets; ++n )
			{
				TCHAR targetkey[MAX_PATH] = {0};
				gv->GetTargetKey( n, targetkey, MAX_PATH );
				pEnv->push( );
				pEnv->add( _T("target"), targetkey );

				TCHAR menuname[MAX_PATH] = {0};
				_tcscpy_s( menuname, MAX_PATH, name );
				pEnv->subst(menuname, MAX_PATH );

				HMENU subhm = CreatePopupMenu();
				MENUITEMINFO mii;
				mii.cbSize = sizeof( MENUITEMINFO );
				mii.fMask = MIIM_TYPE | MIIM_SUBMENU;
				mii.fType = MFT_STRING;
				mii.hSubMenu = subhm;
				mii.dwTypeData = menuname;
				mii.cch = (unsigned int)( _tcslen( menuname ) );
				InsertMenuItem( parentMenu, uidCmd, MF_STRING | MF_BYPOSITION, &mii );

				// Parse children (recurse)
				xmlNodePtr pChildNode = pNode->children;
				while ( NULL != pChildNode )
				{
					BuildMenuItem( gv, pEnv,subhm, uidCmd, pChildNode );
					pChildNode = pChildNode->next;
				}
				pEnv->pop( );
			}
		}
	}
	else if ( xmlStrEqual( BAD_CAST "separator", BAD_CAST pNode->name ) )
	{
		CreateSeparatorItem( parentMenu );
	}

	return ( true );
}

void
CRockstarMenu::CreateMenuItem( TCommandHandler* pHandler,
							HMENU menu, 
							UINT& uidCmd, 
							HBITMAP hBitmap,
							bool enabled,
							bool checked )
{
	UINT flags = MF_STRING;
	flags |= ( enabled ? MF_ENABLED : MF_DISABLED );
	flags |= ( checked ? MF_CHECKED : 0x0 );

	assert( m_mCmdHandlers.size() < uidCmd );
	m_mCmdHandlers.push_back( pHandler );
	AppendMenu( menu, flags, uidCmd++, pHandler->name.c_str() );
}

void
CRockstarMenu::CreateSeparatorItem( HMENU menu )
{
	AppendMenu( menu, MF_SEPARATOR, NULL, NULL );
}

void
CRockstarMenu::ShowAboutBox( ) const
{
	TCHAR moduleName[MAX_PATH];
	DWORD moduleLen = GetModuleFileName( _pModule->GetResourceInstance(), moduleName, MAX_PATH ); 
	VersionInfo vs( moduleName );	
	TCHAR copyright[MAX_PATH] = {0};
	TCHAR product[MAX_PATH] = {0};
	TCHAR rsgshell[MAX_PATH] = _T("<undefined>");
	const VS_FIXEDFILEINFO& ffi = vs.QueryFixedFileInfo( );
	vs.QueryString( product, MAX_PATH, _T("ProductName") );
	vs.QueryString( copyright, MAX_PATH, _T("LegalCopyright") );
	GetEnvironmentVariable( _T("RSG_SHELL"), rsgshell, MAX_PATH );

	wchar_t buffer[MAX_PATH];
	wsprintf( buffer, L"%s\n%s\n\nVersion: %d.%d.%d.%d\n\nRSG_SHELL: %s\nShell XML: %s\n\nby David Muir <david.muir@rockstarnorth.com>\n\n",
		product,
		copyright,
		HIWORD( ffi.dwProductVersionMS ), LOWORD( ffi.dwProductVersionMS ),
		HIWORD( ffi.dwProductVersionLS ), LOWORD( ffi.dwProductVersionLS ),
		rsgshell,
		m_sShellFilename);

	// Display message box.
	MessageBox( 0, buffer, TITLE, 0 );
}

//---------------------------------------------------------------------------
// Private Methods
//---------------------------------------------------------------------------
// None

// RockstarMenu.cpp
