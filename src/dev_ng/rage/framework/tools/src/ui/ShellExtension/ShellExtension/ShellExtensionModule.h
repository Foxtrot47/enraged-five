#ifndef SHELLEXTENSIONMODULE_H
#define SHELLEXTENSIONMODULE_H

//
// File:: ShellExtensionModule.h
// Description:: Shell extension core module.
//
// Author:: David Muir <david.muir@rockstarnorth.com>
// Date:: 10 April 2014
//

#if defined _MSC_VER && _MSC_VER >= 1300
#pragma once
#endif

#endif // SHELLEXTENSIONMODULE_H