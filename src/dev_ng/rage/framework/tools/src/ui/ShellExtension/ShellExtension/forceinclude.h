#ifndef FORCEINCLUDE_H
#define FORCEINCLUDE_H

//
// File:: forceinclude.h
// Description:: Shell extension force-include.
//
// Author:: David Muir <david.muir@rockstarnorth.com>
// Date:: 4 June 2010
//

#if defined _MSC_VER && _MSC_VER >= 1300
#pragma once
#endif

#include <Windows.h>
#include "ShellExtensionModule.h"

#if defined(_DEBUG) && defined(_M_X64)
	#include "forceinclude/win64_tooldebug.h"
#elif defined(_DEBUG)
	#include "forceinclude/win32_tooldebug.h"
#elif defined(NDEBUG) && defined(_M_X64)
	#include "forceinclude/win64_toolrelease.h"
#elif defined(NDEBUG)
	#include "forceinclude/win32_toolrelease.h"
#elif defined(_M_X64)
	#include "forceinclude/win64_toolbeta.h"
#else
	#include "forceinclude/win32_toolbeta.h"
#endif

#pragma warning( disable: 4800 )
#pragma warning( disable: 4265 )
#pragma warning( disable: 4263 )

// External module reference.
// extern class CShellExtensionModule _ShellModule;

#endif //FORCEINCLUDE_H
