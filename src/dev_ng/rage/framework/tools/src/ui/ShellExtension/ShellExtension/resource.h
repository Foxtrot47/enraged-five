//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ShellExtension.rc
//
#define IDS_PROJNAME                    100
#define IDR_SHELLEXTENSION              101
#define IDS_RPFFS_FILE_EXTENSION        101
#define IDR_ROCKSTARMENU                102
#define IDS_RPFFS_FILE_EXTRACT          102
#define IDR_RPFFILESYSTEM               103
#define IDS_RPFFS_DESCRIPTION           103
#define IDS_RPFFS_DISPLAYNAME           104
#define IDS_COLUMN_NAME_FILENAME        105
#define IDS_COLUMN_NAME_SIZE            106
#define IDS_COLUMN_NAME_PACKED          107
#define IDS_COLUMN_NAME_RATIO           108
#define IDS_COLUMN_NAME_OFFSET          109
#define IDS_COLUMN_NAME_INDEX           110
#define IDS_COLUMN_NAME_VERSION         111
#define IDS_COLUMN_NAME_VIRTUAL_SIZE    112
#define IDS_COLUMN_NAME_PHYSICAL_SIZE   113
#define IDB_ROCKSTAR16                  201
#define IDB_ROCKSTAR32                  202
#define IDI_ROCKSTAR                    203
#define IDS_VENDOR                      204
#define IDS_RPFFS_URLPROTOCOL           205

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        210
#define _APS_NEXT_COMMAND_VALUE         32768
#define _APS_NEXT_CONTROL_VALUE         201
#define _APS_NEXT_SYMED_VALUE           104
#endif
#endif
