REM 
REM File:: %RS_TOOLSSRC%/ui/ShellExtension/unregister.bat
REM Description:: Force unregistration of the Shell Extension
REM
REM Author:: David Muir <david.muir@rockstarnorth.com>
REM Date:: 4 June 2010
REM

CALL setenv.bat
regsvr32 /u ShellExtension_x64.dll

REM %RS_TOOLSSRC%/ui/ShellExtension/unregister.bat
