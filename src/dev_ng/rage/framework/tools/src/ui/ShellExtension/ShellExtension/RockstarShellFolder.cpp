//
// File:: RockstarShellFolder.cpp
// Description:: Shell extension Virtual-Folder class.
//
// Author:: David Muir <david.muir@rockstarnorth.com>
// Date:: 26 February 2014
//

#include "RockstarShellFolder.h"
#include "dllmain.h"
#include "PidlEnum.h"

// Registry update method.
HRESULT WINAPI 
CRockstarShellFolder::UpdateRegistry( BOOL bRegister )
{
	// Get the OS information.

	OSVERSIONINFO OsVersionInfo;
	BOOL      bWindowsNT = FALSE;

	// Initialize the size of OsVersionInfo
	OsVersionInfo.dwOSVersionInfoSize = sizeof( OSVERSIONINFO );

	// Get the OS Version
	if ( ::GetVersionEx( &OsVersionInfo ))
	{
		// Are we running NT ?
		if ( OsVersionInfo.dwPlatformId == VER_PLATFORM_WIN32_NT )
			bWindowsNT = TRUE;
	}
	else
	{
		return E_FAIL;
	}

	// Perform the default registry functions.
	HRESULT hResult = _AtlModule.UpdateRegistryFromResource(
		IDR_ROCKSTARSHELLFOLDER,
		bRegister );

	// Note : ATL Registry Scripts/Classes do not support the 
	//        writing of Binary Values so, we must write the 
	//        Registry Value using Win32.

	if ( bRegister )
	{
		if ( hResult == NOERROR )
		{
			DWORD     dwDisposition = NULL;
			HKEY      hSectionKey   = NULL;

			if ( bWindowsNT )
			{
				// Add the Extension to the Shell Extensions Approved List...
				// The RISC Version of VC++ 5.x does not support HKLM in its 
				// ATL scripts...

				if ( ERROR_SUCCESS == ::RegCreateKeyEx( HKEY_LOCAL_MACHINE, 
					(LPCTSTR) STRING_APPROVED_SECTION, 
					NULL, 
					REG_NONE, 
					REG_OPTION_NON_VOLATILE, 
					KEY_WRITE | KEY_READ, 
					NULL, 
					&hSectionKey, 
					&dwDisposition ))
				{
					// If the Section Key was created or opened, then write the 
					// 'ShellFolder' Value
					if ( hSectionKey )
					{
						if ( ERROR_SUCCESS != ::RegSetValueEx( hSectionKey,
							(LPCTSTR)STRING_CLSID,
							NULL,
							REG_SZ,
							( CONST BYTE * )
							(LPCTSTR)
							STRING_ADSIView,
							_tcslen(
							STRING_ADSIView )))
						{
							// If we fail to write the Value, then Fail the regstration.
							hResult = E_FAIL;
						}
					}
					else
					{
						hResult = E_FAIL;
					}
				}

				// Close the key
				::RegCloseKey( hSectionKey );

				// Reset the Key Value
				hSectionKey = NULL;
			}

			// Build the ADSIView Root Key ( This is done 
			// here for Alpha Support.  The RISC Version of VC++ 5.x 
			// does not support HKLM in its ATL scripts...

			if ( ERROR_SUCCESS != ::RegCreateKeyEx( HKEY_LOCAL_MACHINE, 
				(LPCTSTR) STRING_DESKTOP_SECTION, 
				NULL, 
				REG_NONE, 
				REG_OPTION_NON_VOLATILE, 
				KEY_WRITE | KEY_READ, 
				NULL, 
				&hSectionKey, 
				&dwDisposition ))
			{
				return E_FAIL;
			}

			// Close the Key
			::RegCloseKey( hSectionKey );

			// Reset the hSectionKey Value
			hSectionKey = NULL;

			// Generate the 'ShellFolder\Attributes' Value here.
			// This is a Binary Registry Value

			if ( ERROR_SUCCESS == ::RegCreateKeyEx( HKEY_CLASSES_ROOT, 
				(LPCTSTR) STRING_SHELLFOLDER_SECTION, 
				NULL, 
				REG_NONE, 
				REG_OPTION_NON_VOLATILE, 
				KEY_WRITE | KEY_READ, 
				NULL, 
				&hSectionKey, 
				&dwDisposition ))
			{
				BYTE  btValue[] = { 0x40, 0x01, 0x00, 0xA0 };
				DWORD dwValueSize = 4;

				// If the Section Key was created or opened, 
				// then write the 'ShellFolder' Value
				if ( hSectionKey )
				{
					if ( ERROR_SUCCESS != ::RegSetValueEx(
						hSectionKey,
						(LPCTSTR) STRING_ATTRIBUTES_ENTRY,
						NULL,
						REG_BINARY,
						( CONST BYTE * )&btValue,
						dwValueSize ))
					{
						// If we fail to write the Value, 
						// then Fail the regstration.
						hResult = E_FAIL;
					}
				}
				else
					hResult = E_FAIL;
			}

			// Close the Key
			::RegCloseKey( hSectionKey );
		}
	}
	else
	{
		// If this is Windows NT...
		if ( bWindowsNT )
		{
			DWORD     dwDisposition = NULL;
			HKEY      hSectionKey   = NULL;

			// Remove the Extension to the Shell Extensions Approved List...
			// The RISC Version of VC++ 5.x does not
			// support HKLM in its ATL scripts...

			if ( ERROR_SUCCESS == ::RegCreateKeyEx( HKEY_LOCAL_MACHINE, 
				(LPCTSTR) STRING_APPROVED_SECTION, 
				NULL, 
				REG_NONE, 
				REG_OPTION_NON_VOLATILE, 
				KEY_WRITE | KEY_READ, 
				NULL, 
				&hSectionKey, 
				&dwDisposition ))
			{
				// If the Section Key was created or opened,
				// then write the 'ShellFolder' Value
				if ( hSectionKey )
				{
					if ( ERROR_SUCCESS != ::RegDeleteValue(
						hSectionKey, (LPCTSTR) STRING_CLSID ))
					{
						// If we fail to write the Value, then Fail the regstration.
						hResult = E_FAIL;
					}
					else
					{
						hResult = E_FAIL;
					}
				}

				// Close the key
				::RegCloseKey( hSectionKey );

				// Reset the Key Value
				hSectionKey = NULL;
			}
		}

		// Unregister the ADSIView Root Key
		if ( ERROR_SUCCESS != ::RegDeleteKey( HKEY_LOCAL_MACHINE, 
			(LPCTSTR) STRING_DESKTOP_SECTION ))
		{
			hResult = E_FAIL;
		}

	}

	return hResult;
}

//////////////////////////////////////////////////////////////////////////
// IShellFolder methods 

//////////////////////////////////////////////////////////////////////////
// CRockstarShellFolder ParseDisplayName

STDMETHODIMP CRockstarShellFolder::ParseDisplayName(HWND /*hwndOwner*/,
	LPBC /*pbcReserved*/,
	LPOLESTR /*lpszName*/,
	ULONG * /*pchEaten*/,
	LPITEMIDLIST * /*ppidl*/,
	ULONG * /*pdwAttributes*/)
{
	return E_NOTIMPL;
}

//////////////////////////////////////////////////////////////////////////
// CRockstarShellFolder EnumObjects

STDMETHODIMP CRockstarShellFolder::EnumObjects( HWND /*hwndOwner*/,
	DWORD dwFlags,
	LPENUMIDLIST * ppEnumIDList )
{
	// setup the out params
	*ppEnumIDList = NULL; 

	CComObject<CPidlEnum>* pEnum = NULL;
	HRESULT hr = CComObject<CPidlEnum>::CreateInstance( &pEnum );
	if ( SUCCEEDED(hr) )
	{
		// AddRef() the object while we're using it.
		pEnum->AddRef();

		// Init the enumerator.  Init() will AddRef() our IUnknown (obtained with
		// GetUnknown()) so this object will stay alive as long as the enumerator 
		// needs access to the collection m_NEFileArray.
		hr = pEnum->_Init(m_pidlPath, dwFlags);

		// Return an IEnumIDList interface to the caller.
		if (SUCCEEDED(hr))
			hr = pEnum->QueryInterface(IID_IEnumIDList, (void**)ppEnumIDList);

		pEnum->Release();
	}

	return (hr);
}

//////////////////////////////////////////////////////////////////////////
// CRockstarShellFolder BindToObject

STDMETHODIMP CRockstarShellFolder::BindToObject( LPCITEMIDLIST pidl,
	LPBC /*pbcReserved*/,
	REFIID riid,
	LPVOID * ppvOut)
{
	*ppvOut = NULL;

	// make sure that this is the interface we want
	if ( IID_IShellFolder != riid )
		return (E_NOINTERFACE);

	CComObject<CRockstarShellFolder>* pFolderObj = NULL;
	HRESULT hr = CComObject<CRockstarShellFolder>::CreateInstance( &pFolderObj );
	if ( FAILED(hr) )
		return (hr);

	pFolderObj->AddRef( );
	hr = QueryInterface( riid, ppvOut );

	ATLASSERT( pFolderObj );
	pFolderObj->Release( );

	return (hr);
}

//////////////////////////////////////////////////////////////////////////
// CRockstarShellFolder BindToStorage

STDMETHODIMP CRockstarShellFolder::BindToStorage( LPCITEMIDLIST /*pidl*/,
	LPBC /*pbcReserved*/,
	REFIID /*riid*/,
	LPVOID * ppvObj )
{
	*ppvObj = NULL;
	return (E_NOTIMPL);
}

//////////////////////////////////////////////////////////////////////////
// CRockstarShellFolder CompareIDs

STDMETHODIMP CRockstarShellFolder::CompareIDs( LPARAM /*lParam*/,
	LPCITEMIDLIST pidl1,
	LPCITEMIDLIST pidl2 )
{
	return S_FALSE;
}

//////////////////////////////////////////////////////////////////////////
// CRockstarShellFolder CreateViewObject

STDMETHODIMP CRockstarShellFolder::CreateViewObject( HWND /*hwndOwner*/,
	REFIID riid,
	LPVOID * ppvOut)
{
	ATLTRACE( "CRockstarShellFolder::CreateViewObject()" );

	// Initialize the return
	*ppvOut = NULL;

	// Do we support the interface
	if (IID_IShellView != riid)
		return E_NOINTERFACE;

	return (E_NOTIMPL);
}

//////////////////////////////////////////////////////////////////////////
// CRockstarShellFolder GetAttributesOf

STDMETHODIMP CRockstarShellFolder::GetAttributesOf( UINT cidl,
	LPCITEMIDLIST * apidl,
	ULONG * rgfInOut)
{
	return (S_OK);
}

//////////////////////////////////////////////////////////////////////////
// CRockstarShellFolder GetUIObjectOf

STDMETHODIMP CRockstarShellFolder::GetUIObjectOf( HWND hwndOwner,
	UINT cidl,
	LPCITEMIDLIST * apidl,
	REFIID riid,
	UINT * prgfInOut,
	LPVOID * ppvOut)
{
	// if this is NOT the extract Icon
	// we have not implemented it yet
	if ( IID_IExtractIconA != riid && IID_IExtractIconW != riid )
		return (E_NOTIMPL);

	// setup the out params
	*ppvOut = NULL;

	// make sure that it is only asking for 1
	if ( 1 != cidl )
		return (E_NOTIMPL);

	return (S_OK);
}

//////////////////////////////////////////////////////////////////////////
// CRockstarShellFolder GetDisplayNameOf

STDMETHODIMP CRockstarShellFolder::GetDisplayNameOf( LPCITEMIDLIST pIDL,
	DWORD uFlags,
	LPSTRRET lpName)
{
	return (S_OK);
}

//////////////////////////////////////////////////////////////////////////
// CRockstarShellFolder SetNameOf

STDMETHODIMP CRockstarShellFolder::SetNameOf( HWND /*hwndOwner*/,
	LPCITEMIDLIST /*pidl*/,
	LPCOLESTR /*lpszName*/,
	DWORD /*uFlags*/,
	LPITEMIDLIST * /*ppidlOut*/)
{
	return E_NOTIMPL;
}

//////////////////////////////////////////////////////////////////////////
// IPersist methods 

//////////////////////////////////////////////////////////////////////////
// CRockstarShellFolder GetClassID

STDMETHODIMP CRockstarShellFolder::GetClassID( LPCLSID /*lpClassID*/ )
{
	ATLTRACE( _T("CRockstarShellFolder::GetClassID( )\n") );

	return E_NOTIMPL;
}

//////////////////////////////////////////////////////////////////////////
// IPersistFolder methods 

//////////////////////////////////////////////////////////////////////////
// CRockstarShellFolder Initialize

STDMETHODIMP CRockstarShellFolder::Initialize( LPCITEMIDLIST pidl )
{
	ATLTRACE( _T("CRockstarShellFolder::Initialize( )\n") );

	if ( pidl && ( !_AtlModule.GetPidlRoot( ) ) )
	{
		_AtlModule.SetPidlRoot( m_pidlMgr.Copy(pidl) );
	}
	return (S_OK);
}
