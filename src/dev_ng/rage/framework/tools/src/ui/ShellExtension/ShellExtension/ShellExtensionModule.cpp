//
// File:: ShellExtensionModule.cpp
// Description:: Shell extension core module.
//
// Author:: David Muir <david.muir@rockstarnorth.com>
// Date:: 10 April 2014
//

#include "ShellExtensionModule.h"
#include "RockstarNamespace.h"

BOOL 
CShellExtensionModule::GetConfigBool(VFS_CONFIG Item)
{
	switch( Item ) {

	case VFS_CAN_SLOW_COPY:
	case VFS_CAN_SLOW_ENUM:
	case VFS_CAN_PROGRESSUI:
	case VFS_CAN_ROOT_SENDTO:
	case VFS_CAN_ROOT_PREVIEW:
	case VFS_CAN_ROOT_PROPSHEET:
	case VFS_CAN_ROOT_DROPTARGET:
	case VFS_CAN_ROOT_CONTEXTMENU:
	case VFS_CAN_ROOT_CUSTOMSCRIPT:
	case VFS_CAN_ROOT_STARTMENU_LINK:
	case VFS_CAN_ATTACHMENTSERVICES:
		return TRUE;

	case VFS_HAVE_UNIQUE_NAMES:
	case VFS_HAVE_VIRTUAL_FILES:
		return TRUE;

	}
	return FALSE;
}

LONG 
CShellExtensionModule::GetConfigInt(VFS_CONFIG Item)
{
	switch( Item ) {

	case VFS_INT_LOCATION:
		return VFS_LOCATION_MYCOMPUTER;

	case VFS_INT_MAX_FILENAME_LENGTH:
		return MAX_PATH;

	case VFS_INT_MAX_PATHNAME_LENGTH:
		return MAX_PATH;

	case VFS_INT_SHELLROOT_SFGAO:
		return SFGAO_CANCOPY 
			| SFGAO_CANMOVE 
			| SFGAO_CANRENAME 
			| SFGAO_DROPTARGET
			| SFGAO_STREAM
			| SFGAO_FOLDER 
			| SFGAO_BROWSABLE 
			| SFGAO_HASSUBFOLDER 
			| SFGAO_HASPROPSHEET 
			| SFGAO_FILESYSANCESTOR
			| SFGAO_STORAGEANCESTOR;

	}
	return 0;
}

LPCWSTR 
CShellExtensionModule::GetConfigStr(VFS_CONFIG Item)
{
	switch( Item ) {

	case VFS_STR_FILENAME_CHARS_NOTALLOWED:
		return L":<>\\/|\"'*?[]";

	}
	return NULL;
}

HRESULT 
CShellExtensionModule::CreateFileSystem( PCIDLIST_ABSOLUTE pidlRoot, CNseFileSystem** ppFS )
{
	CRockstarNamespace* pNS = new CRockstarNamespace();
	HRESULT Hr = pNS->Init();
	if( SUCCEEDED(Hr) ) *ppFS = pNS;
	else delete pNS; 
	return Hr;
}

HRESULT 
CShellExtensionModule::DllInstall( )
{
	return ( S_OK );
}

HRESULT 
CShellExtensionModule::DllUninstall( )
{
	return ( S_OK );
}

HRESULT
CShellExtensionModule::ShellAction(LPCWSTR pstrType, LPCWSTR pstrCmdLine)
{
	return ( S_OK );
}

BOOL 
CShellExtensionModule::DllMain( DWORD dwReason, LPVOID lpReserved )
{
	return ( TRUE );
}

