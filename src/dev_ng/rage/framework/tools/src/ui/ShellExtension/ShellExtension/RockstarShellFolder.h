#ifndef ROCKSTARSHELLFOLDER_H
#define ROCKSTARSHELLFOLDER_H

//
// File:: RockstarShellFolder.h
// Description:: Shell extension Virtual-Folder class.
//
// Author:: David Muir <david.muir@rockstarnorth.com>
// Date:: 26 February 2014
//

#if defined _MSC_VER && _MSC_VER >= 1300
#pragma once
#endif

// Local headers
#include "resource.h"       // main symbols
#include "ShellExtension_i.h"
#include "PidlMgr.h"

// Windows headers
#include <atlbase.h>
#include <atlcom.h>
#include <ShlObj.h>
#include <comdef.h>
#include <Windows.h>

#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif

#define STRING_APPROVED_SECTION    _T("\
SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Shell Extensions\\Approved")

#define STRING_DESKTOP_SECTION     _T("\
SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Desktop\\\
Namespace\\{03AA3F77-3ECB-49E7-B451-8B0167D2DC0E}")

#define STRING_SHELLFOLDER_SECTION _T("\
CLSID\\{03AA3F77-3ECB-49E7-B451-8B0167D2DC0E}\\ShellFolder")

#define STRING_ATTRIBUTES_ENTRY    _T("Attributes")

#define STRING_CLSID               _T("\
{03AA3F77-3ECB-49E7-B451-8B0167D2DC0E}")

#define STRING_ADSIView            _T("Rockstar Games")

// CRockstarShellFolder

class ATL_NO_VTABLE CRockstarShellFolder :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CRockstarShellFolder, &CLSID_RockstarShellFolder>,
	public IRockstarShellFolder,
	public IShellFolder,
	public IPersistFolder
{
public:
	DECLARE_PROTECT_FINAL_CONSTRUCT()
	
	BEGIN_COM_MAP(CRockstarShellFolder)
		COM_INTERFACE_ENTRY(IRockstarShellFolder)
		COM_INTERFACE_ENTRY(IShellFolder)
		COM_INTERFACE_ENTRY(IPersistFolder)
	END_COM_MAP()
		
	static HRESULT WINAPI UpdateRegistry( BOOL bRegister );
public:

	// *** IShellFolder methods ***
	STDMETHOD(ParseDisplayName) ( HWND hwndOwner,
		LPBC pbcReserved,
		LPOLESTR lpszDisplayName,
		ULONG * pchEaten,
		LPITEMIDLIST * ppidl,
		ULONG *pdwAttributes);
	STDMETHOD(EnumObjects)   ( HWND hwndOwner,
		DWORD grfFlags,
		LPENUMIDLIST * ppenumIDList);
	STDMETHOD(BindToObject)     ( LPCITEMIDLIST pidl,
		LPBC pbcReserved,
		REFIID riid,
		LPVOID * ppvOut);
	STDMETHOD(BindToStorage)    ( LPCITEMIDLIST pidl,
		LPBC pbcReserved,
		REFIID riid,
		LPVOID * ppvObj);
	STDMETHOD(CompareIDs)       ( LPARAM lParam,
		LPCITEMIDLIST pidl1,
		LPCITEMIDLIST pidl2 );
	STDMETHOD(CreateViewObject) ( HWND hwndOwner,
		REFIID riid,
		LPVOID * ppvOut);
	STDMETHOD(GetAttributesOf)  ( UINT cidl,
		LPCITEMIDLIST * apidl,
		ULONG * rgfInOut);
	STDMETHOD(GetUIObjectOf)    ( HWND hwndOwner,
		UINT cidl,
		LPCITEMIDLIST * apidl,
		REFIID riid,
		UINT * prgfInOut,
		LPVOID * ppvOut);
	STDMETHOD(GetDisplayNameOf) ( LPCITEMIDLIST pidl,
		DWORD uFlags,
		LPSTRRET lpName);
	STDMETHOD(SetNameOf)        ( HWND hwndOwner,
		LPCITEMIDLIST pidl,
		LPCOLESTR lpszName,
		DWORD uFlags,
		LPITEMIDLIST * ppidlOut);

	// *** IPersist methods ***
	STDMETHOD(GetClassID)( LPCLSID lpClassID );

	// *** IPersistFolder methods ***
	STDMETHOD(Initialize)( LPCITEMIDLIST pidl );

private:
	CPidlMgr		m_pidlMgr;
	LPITEMIDLIST	m_pidlPath;
};

OBJECT_ENTRY_AUTO(__uuidof(RockstarShellFolder), CRockstarShellFolder)

#endif // ROCKSTARSHELLFOLDER_H
