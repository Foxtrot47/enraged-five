//
// File:: dllmain.cpp
// Description:: "Rockstar Games" Shell Extension DLL Main
//
// Author:: David Muir <david.muir@rockstarnorth.com>
// Date:: 4 June 2010
//

#include "resource.h"
#include "ShellExtension_i.h"
#include "dllmain.h"
#include "RockstarMenu.h"
#include "RpfFileSystem.h"

// Windows headers
#include <atlbase.h>
#include <atlcom.h>
#include <commctrl.h>

// Rage headers
#include "parser/manager.h"

CComModule _AtlModule;

BEGIN_OBJECT_MAP(ObjectMap)
OBJECT_ENTRY(CLSID_RockstarMenu, CRockstarMenu)
//OBJECT_ENTRY(CLSID_RpfFileSystem, CRpfFileSystem)
END_OBJECT_MAP()

// DLL Entry Point
extern "C" BOOL WINAPI 
DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	switch ( dwReason )
	{
	case DLL_PROCESS_ATTACH:
		{
			_AtlModule.Init( ObjectMap, hInstance, &LIBID_ShellExtensionLib );
			DisableThreadLibraryCalls( hInstance );
			
			if ( !::rage::sysParam::IsInitialized( ) )
				::rage::sysParam::Init( 0, NULL );
			INIT_PARSER;
		}
		break;
	case DLL_PROCESS_DETACH:
		{
			SHUTDOWN_PARSER;
			_AtlModule.Term( );
		}
		break;
	}
	return ( TRUE );
}

#if 0
HRESULT
CShellExtensionModule::Initialise( _ATL_OBJMAP_ENTRY* p, HINSTANCE hInst, 
	const GUID* plibid )
{
	m_hInstance = hInst;
	::OleInitialize( NULL );
	::InitCommonControls( );

	// Initialise shell allocator helper object.
	HRESULT hr;
	hr = this->m_shAllocator.Initialise( );
	if ( FAILED(hr) )
		return (hr);

	// Get Windows Version Information.
	OSVERSIONINFO ovi;
	ZeroMemory( &ovi, sizeof( OSVERSIONINFO ) );
	ovi.dwOSVersionInfoSize =	sizeof(ovi);
	::GetVersionEx(&ovi);
	m_wWindowsMajor = static_cast<WORD>( ovi.dwMajorVersion );
	m_wWindowsMinor = static_cast<WORD>( ovi.dwMinorVersion );

	return (hr);
}

void
CShellExtensionModule::Terminate( )
{
	this->m_shAllocator.Terminate( );
	CAtlDllModuleT< CShellExtensionModule >::Term( );
}
#endif
