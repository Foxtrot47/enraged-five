#ifndef DLLMAIN_H
#define DLLMAIN_H

#if defined _MSC_VER && _MSC_VER >= 1300
#pragma once
#endif

// ShellExtensionFramework Headers
#include "ShellAlloctor.h"

// Local headers
#include "ShellExtension_i.h"
#include "resource.h"

// Windows headers
#include <atlbase.h>
#include <Windows.h>

#if 0
class CShellExtensionModule : public CAtlDllModuleT< CShellExtensionModule >
{
public :
	DECLARE_LIBID(LIBID_ShellExtensionLib)
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_SHELLEXTENSION, "{FFD4CC26-079F-451B-80F8-156BB7A09FDE}")

	HRESULT		Initialise( _ATL_OBJMAP_ENTRY* p, HINSTANCE hInst, const GUID* plibid = NULL );
	void		Terminate( );

	HINSTANCE	GetInstance( ) const { return ( m_hInstance ); }
	CShellAllocator& GetAllocator( ) { return m_shAllocator; }

	LPITEMIDLIST GetPidlRoot( ) const { return ( m_pidlRoot ); }
	void		SetPidlRoot( LPITEMIDLIST pidl ) { m_pidlRoot = pidl; }
private:
	HINSTANCE		m_hInstance;		// Module instance handle
	CShellAllocator	m_shAllocator;		// Shell allocator.

	WORD			m_wWindowsMajor;	// Windows Major Version
	WORD			m_wWindowsMinor;	// Windows Minor Version

	LPITEMIDLIST	m_pidlRoot;			// Root PIDL.
};
#endif

// External module reference.
extern class CComModule _AtlModule;

#endif // DLLMAIN_H