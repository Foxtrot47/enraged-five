//
// File:: RockstarShellConfig.cpp
// Description:: Shell extension configuration data functions.
//
// Author:: David Muir <david.muir@rockstarnorth.com>
// Date:: 26 February 2014
//

#include "RockstarShellConfig.h"

// RAGE Headers
#include "parser/manager.h"
#include "system/param.h"
using namespace rage;

// Global shell configuration data.
shell::ShellConfig* s_pShellConfig = NULL;
static FILETIME s_ftLoadedModifiedTime;
static FILETIME s_ftModifiedTime;

bool 
ReloadConfig( const char* filename )
{
	if ( NULL == s_pShellConfig )
	{
		memset( &s_ftLoadedModifiedTime, 0, sizeof( FILETIME ) );
		memset( &s_ftModifiedTime, 0, sizeof( FILETIME ) );
	}

	HANDLE hFile = CreateFileA( filename, GENERIC_READ, FILE_SHARE_READ, NULL,
		OPEN_EXISTING, 0, NULL );
	if ( INVALID_HANDLE_VALUE == hFile )
		return ( false );

	// Read file modified-time.
	if ( !GetFileTime(  hFile, NULL, NULL, &s_ftModifiedTime ) )
		return ( false );

	// Close file handle.
	CloseHandle( hFile );

	// Only re-load data if the metadata has been modified.
	bool result = true;
	if ( 1 == CompareFileTime( &s_ftModifiedTime, &s_ftLoadedModifiedTime ) )
	{
		// Initialise our parser system and config object.
		if ( !::rage::sysParam::IsInitialized( ) )
			::rage::sysParam::Init( 0, NULL );
		//if ( !::rage::parManager::sm_Instance )
		//	INIT_PARSER;

		// Load the metadata file.
		s_ftLoadedModifiedTime = s_ftModifiedTime;

		if ( NULL != s_pShellConfig )
		{
			delete s_pShellConfig;
			s_pShellConfig = NULL;
		}

		result = ( PARSER.LoadObjectPtr<shell::ShellConfig>( filename, "", s_pShellConfig ) );
		// SHUTDOWN_PARSER;
	}

	return ( result );
}
