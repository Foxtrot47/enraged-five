

/* this ALWAYS GENERATED file contains the IIDs and CLSIDs */

/* link this file in with the server and any clients */


 /* File created by MIDL compiler version 7.00.0555 */
/* at Wed Apr 09 12:06:18 2014
 */
/* Compiler settings for ShellExtension.idl:
    Oicf, W1, Zp8, env=Win64 (32b run), target_arch=AMD64 7.00.0555 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


#ifdef __cplusplus
extern "C"{
#endif 


#include <rpc.h>
#include <rpcndr.h>

#ifdef _MIDL_USE_GUIDDEF_

#ifndef INITGUID
#define INITGUID
#include <guiddef.h>
#undef INITGUID
#else
#include <guiddef.h>
#endif

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        DEFINE_GUID(name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8)

#else // !_MIDL_USE_GUIDDEF_

#ifndef __IID_DEFINED__
#define __IID_DEFINED__

typedef struct _IID
{
    unsigned long x;
    unsigned short s1;
    unsigned short s2;
    unsigned char  c[8];
} IID;

#endif // __IID_DEFINED__

#ifndef CLSID_DEFINED
#define CLSID_DEFINED
typedef IID CLSID;
#endif // CLSID_DEFINED

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        const type name = {l,w1,w2,{b1,b2,b3,b4,b5,b6,b7,b8}}

#endif !_MIDL_USE_GUIDDEF_

MIDL_DEFINE_GUID(IID, IID_IRockstarMenu,0x20603B4A,0xC767,0x49C9,0xBC,0x4A,0xB8,0x50,0xA9,0xC2,0xED,0xAB);


MIDL_DEFINE_GUID(IID, IID_IRockstarShellFolder,0x1EAAB775,0x1199,0x4370,0xAB,0x20,0xD6,0x90,0x98,0x2B,0x59,0xC0);


MIDL_DEFINE_GUID(IID, LIBID_ShellExtensionLib,0x77613898,0x4E2F,0x457B,0xAC,0x05,0xBA,0x84,0x67,0x8B,0x55,0xA1);


MIDL_DEFINE_GUID(CLSID, CLSID_RockstarMenu,0xF818B71E,0xA877,0x4FEA,0xB4,0xA8,0x32,0x12,0x16,0xC4,0x43,0x3B);


MIDL_DEFINE_GUID(CLSID, CLSID_RockstarShellFolder,0x03AA3F77,0x3ECB,0x49E7,0xB4,0x51,0x8B,0x01,0x67,0xD2,0xDC,0x0E);

#undef MIDL_DEFINE_GUID

#ifdef __cplusplus
}
#endif



