REM 
REM File:: %RS_TOOLSSRC%/ui/ShellExtension/unregister.bat
REM Description:: Force unregistration of the Shell Extension
REM
REM Author:: David Muir <david.muir@rockstarnorth.com>
REM Date:: 4 June 2010
REM

CALL setenv.bat
regsvr32 /u ShellExtension\x64\Debug\ShellExtension_x64.dll
regsvr32 /u ShellExtension\x64\Release\ShellExtension_x64.dll
regsvr32 /u ShellExtension\win32\Debug\ShellExtension_Win32.dll
regsvr32 /u ShellExtension\win32\Release\ShellExtension_Win32.dll

REM %RS_TOOLSSRC%/ui/ShellExtension/unregister.bat
