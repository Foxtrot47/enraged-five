#ifndef ROCKSTARSHELLCONFIG_H
#define ROCKSTARSHELLCONFIG_H

//
// File:: RockstarShellConfig.h
// Description:: Shell extension configuration data functions.
//
// Author:: David Muir <david.muir@rockstarnorth.com>
// Date:: 26 February 2014
//

#if defined _MSC_VER && _MSC_VER >= 1300
#pragma once
#endif

#include "shell.h"

#define CONFIG_FILE_VERSION (2)

extern shell::ShellConfig* s_pShellConfig;

bool ReloadConfig( const char* filename );

#endif // ROCKSTARSHELLCONFIG_H
