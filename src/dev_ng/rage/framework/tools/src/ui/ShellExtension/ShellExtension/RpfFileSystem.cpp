//
// File:: RpfFileSystem.cpp
// Description:: RPF File System extension.
//
// Author:: David Muir <david.muir@rockstarnorth.com>
// Date:: 14 April 2014
//

#include "RpfFileSystem.h"
#include "dllmain.h"
#include "resource.h"

// Shell Extension Framework headers
#include "BasicShellView.h"

//const CLSID CLSID_RpfFileSystem = // {3E1537C6-A2AF-46C3-8570-72CC57047224}
//	{ 0x3e1537c6, 0xa2af, 0x46c3, { 0x85, 0x70, 0x72, 0xcc, 0x57, 0x4, 0x72, 0x24 } };

LPTSTR PidlToString( LPCITEMIDLIST pidl );

CRpfFileSystem::CRpfFileSystem( )
	: m_pidlRoot( NULL )
{
	m_allocator.Initialise( );
}

CRpfFileSystem::~CRpfFileSystem( )
{
	m_allocator.Terminate( );
}

ULONG 
CRpfFileSystem::AddRef( void )
{
	InterlockedIncrement( &m_refCount );
	return ( m_refCount );
}

ULONG 
CRpfFileSystem::Release( void )
{
	// Decrement the object's internal counter.
	ULONG ulRefCount = InterlockedDecrement( &m_refCount );
	if (0 == m_refCount)
	{
		delete this;
	}
	return ulRefCount;
}

HRESULT 
CRpfFileSystem::QueryInterface( REFIID riid, LPVOID* ppvObj )
{
	// Always set out parameter to NULL, validating it first.
	if (!ppvObj)
		return E_INVALIDARG;
	*ppvObj = NULL;
	if (riid == IID_IUnknown || riid == IID_IRpfFileSystem)
	{
		// Increment the reference count and return the pointer.
		*ppvObj = (LPVOID)this;
		AddRef();
		return NOERROR;
	}
	return E_NOINTERFACE;
}

STDMETHODIMP 
CRpfFileSystem::GetClassID( CLSID* pclsid )
{
	ATLTRACE( _T("CRpfFileSystem::GetClassID") );
	*pclsid = GetObjectCLSID( );
	return ( S_OK );
}

STDMETHODIMP 
CRpfFileSystem::Initialize( PCIDLIST_ABSOLUTE pidl )
{
	ATLTRACE( _T("CRpfFileSystem::Initialize") );

	m_pidlRoot = m_PidlManager.Copy( pidl );
	return ( S_OK );
}

STDMETHODIMP 
CRpfFileSystem::GetCurFolder( PIDLIST_ABSOLUTE* ppidl )
{
	ATLTRACE( _T("CRpfFileSystem::GetCurFolder" ) );

	if ( NULL == ppidl )
		return ( E_POINTER );

	*ppidl = m_PidlManager.Copy( m_pidlRoot );
	return ( S_OK );
}

STDMETHODIMP 
CRpfFileSystem::ParseDisplayName( HWND hwnd, IBindCtx* pbc, PWSTR pszDisplayName, ULONG* pchEaten, PIDLIST_RELATIVE* ppidl, ULONG* pdwAttributes )
{
   ATLTRACE( _T( "CRpfFileSystem::ParseDisplayName  name='%ws' pbc=%p\n" ), pszDisplayName, pbc);
   
   return S_OK;
}

STDMETHODIMP 
CRpfFileSystem::EnumObjects(HWND hwnd, SHCONTF grfFlags, IEnumIDList** ppEnumIDList)
{
	ATLTRACENOTIMPL( _T("CRpfFileSystem::EnumObjects\n" ) );
}

// BindToObject is invoked when a folder in our part of the namespace is being browsed.
STDMETHODIMP 
CRpfFileSystem::BindToObject( PCUIDLIST_RELATIVE pidl, IBindCtx* pbc, REFIID riid, LPVOID* ppRetVal)
{
   ATLTRACE( _T( "CRpfFileSystem::BindToObject  riid=%s\n" ), PidlToString(pidl));
   
   if ( !CRpfFileSystemPidl::IsOwn( pidl ) )
	   return ( E_INVALIDARG );

   CComPtr<IShellFolder> DesktopPtr;

   if ( !m_PidlManager.IsSingle(pidl ) )
   {		
	   HRESULT hr;
	   hr = SHGetDesktopFolder(&DesktopPtr);
	   if (FAILED(hr))
		   return hr;

	   PIDLIST_RELATIVE pidlLocal;
	   hr = DesktopPtr->ParseDisplayName(NULL, pbc, CRpfFileSystemPidl::GetFilename(pidl), NULL, &pidlLocal, NULL);
	   if (FAILED(hr))
		   return hr;

	   // Bind to the root folder of the favorite folder
	   CComPtr<IShellFolder> RootFolderPtr;
	   hr = DesktopPtr->BindToObject(pidlLocal, NULL, IID_IShellFolder, (void**)&RootFolderPtr);
	   ILFree(pidlLocal);
	   if (FAILED(hr))
		   return hr;

	   // And now bind to the sub-item of it
	   LPITEMIDLIST nextItem = m_PidlManager.GetNextItem(pidl);
	   return RootFolderPtr->BindToObject((PCUIDLIST_RELATIVE)nextItem, pbc, riid, ppRetVal);
   }

   ATLTRACE(L"CRpfFileSystem::BindToObject - failed\n");
   return E_NOINTERFACE;
}

STDMETHODIMP 
CRpfFileSystem::BindToStorage( PCUIDLIST_RELATIVE pidl, IBindCtx* pbc, REFIID riid, LPVOID* ppRetVal)
{
   ATLTRACENOTIMPL( _T("CRpfFileSystem::BindToStorage\n") ); 
}

STDMETHODIMP 
CRpfFileSystem::CompareIDs(LPARAM lParam, PCUIDLIST_RELATIVE pidl1, PCUIDLIST_RELATIVE pidl2)
{	
	ATLTRACE(_T("CRpfFileSystem(0x%08x)::CompareIDs(lParam=%d) pidl1=[%s], pidl2=[%s]\n"), 
		this, lParam, PidlToString(pidl1), PidlToString(pidl2));

	// First check if the pidl are ours
	if (!CRpfFileSystemPidl::IsOwn(pidl1) || !CRpfFileSystemPidl::IsOwn(pidl2))
		return ( E_INVALIDARG );

	// Now check if the pidl are one or multi level, in case they are multi-level, return non-equality
	if (!m_PidlManager.IsSingle(pidl1) || !m_PidlManager.IsSingle(pidl2))
		return MAKE_HRESULT(SEVERITY_SUCCESS, 0, 1);

	USHORT result = 0;	// see note below (MAKE_HRESULT)
	switch (lParam & SHCIDS_COLUMNMASK)
	{
		case DETAILS_COLUMN_FILENAME:		
			result = wcscmp( CRpfFileSystemPidl::GetFilename(pidl1), CRpfFileSystemPidl::GetFilename(pidl2) );	
			break;
		case DETAILS_COLUMN_SIZE:
			result = CRpfFileSystemPidl::GetEntrySize(pidl1) - CRpfFileSystemPidl::GetEntrySize(pidl2);
			break;
		case DETAILS_COLUMN_PACKED_SIZE:
			result = CRpfFileSystemPidl::GetEntryPackedSize(pidl1) - CRpfFileSystemPidl::GetEntryPackedSize(pidl2);
			break;
		case DETAILS_COLUMN_RATIO:
			break;
		case DETAILS_COLUMN_OFFSET:
			break;
		case DETAILS_COLUMN_INDEX:
			break;
		case DETAILS_COLUMN_VERSION:
			break;
		case DETAILS_COLUMN_VIRTUAL_SIZE:
			break;
		case DETAILS_COLUMN_PHYSICAL_SIZE:
			break;
		default:
			return ( E_INVALIDARG );
	}

	// Warning: the last param MUST be unsigned, if not (ie: short) a negative 
	// value will trash the high order word of the HRESULT!
	return MAKE_HRESULT(SEVERITY_SUCCESS, 0, /*-1,0,1*/ result );
}

STDMETHODIMP 
CRpfFileSystem::CreateViewObject(HWND hwndOwner, REFIID riid, LPVOID* ppRetVal)
{
   ATLTRACE( _T("CRpfFileSystem::CreateViewObject\n" ) );
   if ( NULL == ppRetVal )
	   return ( E_POINTER );

	HRESULT hr;
	*ppRetVal = NULL;

	// We handle on the IShellView.
	if ( IID_IShellView == riid )
	{
		// Create a view object
		CComObject<CBasicShellView>* pViewObject;
		hr = CComObject<CBasicShellView>::CreateInstance(&pViewObject);
		if (FAILED(hr))
			return hr;

		// AddRef the object while we are using it
		pViewObject->AddRef();

		// Tight the view object lifetime with the current IShellFolder.
		pViewObject->Init(GetUnknown());

		// Create the view
		hr = pViewObject->Create((IShellView**)ppRetVal, hwndOwner, (IShellFolder*)this);

		// We are finished with our own use of the view object (AddRef()'ed above by us, AddRef()'ed by Create)
		pViewObject->Release();

		return hr;

	}

	// We do not handle other objects.
	return ( E_NOINTERFACE );
}

STDMETHODIMP 
CRpfFileSystem::GetAttributesOf(UINT cidl, PCUITEMID_CHILD_ARRAY rgpidl, SFGAOF* rgfInOut)
{
	ATLTRACENOTIMPL( _T("CRpfFileSystem::GetAttributesOf\n") );
}

STDMETHODIMP 
CRpfFileSystem::GetUIObjectOf(HWND hwndOwner, UINT cidl, PCUITEMID_CHILD_ARRAY rgpidl, REFIID riid, UINT* rgfReserved, LPVOID* ppRetVal)
{
	ATLTRACENOTIMPL( _T("CRpfFileSystem::GetUIObjectOf\n") );
}

STDMETHODIMP 
CRpfFileSystem::GetDisplayNameOf( PCUITEMID_CHILD pidl, SHGDNF uFlags, STRRET* psrName)
{
	ATLTRACENOTIMPL( _T("CRpfFileSystem::GetDisplayNameOf\n") );
}

STDMETHODIMP 
CRpfFileSystem::SetNameOf(HWND hwnd, PCUITEMID_CHILD pidl, LPCWSTR pszName, SHGDNF uFlags, PITEMID_CHILD* ppidlOut)
{
	ATLTRACENOTIMPL( _T("CRpfFileSystem::SetNameOf\n") );
}

// IShellFolder2

STDMETHODIMP 
CRpfFileSystem::GetDefaultSearchGUID(GUID* pguid)
{
   ATLTRACENOTIMPL( _T("CRpfFileSystem::GetDefaultSearchGUID\n") );
}

STDMETHODIMP 
CRpfFileSystem::EnumSearches(IEnumExtraSearch** ppenum)
{
   ATLTRACENOTIMPL( _T("CRpfFileSystem::EnumSearches\n") );
}

STDMETHODIMP 
CRpfFileSystem::GetDefaultColumn(DWORD /*dwRes*/, ULONG* pSort, ULONG* pDisplay)
{
	ATLTRACE("CFavosRootShellFolder(0x%08x)::GetDefaultColumn()\n", this);

	if (!pSort || !pDisplay)
		return E_POINTER;

	*pSort = DETAILS_COLUMN_FILENAME;
	*pDisplay = DETAILS_COLUMN_FILENAME;

	return S_OK;

}

STDMETHODIMP 
CRpfFileSystem::GetDefaultColumnState(UINT iColumn, SHCOLSTATEF* pcsFlags)
{
	ATLTRACE( _T("CRpfFileSystem(0x%08x)::GetDefaultColumnState(iColumn=%d)\n"), this, iColumn );

	if (!pcsFlags)
		return E_POINTER;

	// Seems that SHCOLSTATE_PREFER_VARCMP doesn't have any noticeable effect (if supplied or not) for Win2K, but don't
	// set it for WinXP, since it will not sort the column. (not setting it means that our CompareIDs() will be called)
	switch (iColumn)
	{
	case DETAILS_COLUMN_FILENAME:
		*pcsFlags = SHCOLSTATE_TYPE_STR | SHCOLSTATE_ONBYDEFAULT;
		break;
	case DETAILS_COLUMN_PACKED_SIZE:
		*pcsFlags = SHCOLSTATE_TYPE_INT | SHCOLSTATE_ONBYDEFAULT;	
		break;
	case DETAILS_COLUMN_SIZE:		
		*pcsFlags = SHCOLSTATE_TYPE_INT | SHCOLSTATE_ONBYDEFAULT;	
		break;
	case DETAILS_COLUMN_RATIO:
		*pcsFlags = SHCOLSTATE_TYPE_STR | SHCOLSTATE_ONBYDEFAULT;	
		break;
	case DETAILS_COLUMN_OFFSET:
		*pcsFlags = SHCOLSTATE_TYPE_INT | SHCOLSTATE_ONBYDEFAULT;	
		break;
	case DETAILS_COLUMN_INDEX:
		*pcsFlags = SHCOLSTATE_TYPE_INT | SHCOLSTATE_ONBYDEFAULT;	
		break;
	case DETAILS_COLUMN_VERSION:
		*pcsFlags = SHCOLSTATE_TYPE_INT | SHCOLSTATE_ONBYDEFAULT;	
		break;
	case DETAILS_COLUMN_VIRTUAL_SIZE:
		*pcsFlags = SHCOLSTATE_TYPE_INT | SHCOLSTATE_ONBYDEFAULT;	
		break;
	case DETAILS_COLUMN_PHYSICAL_SIZE:
		*pcsFlags = SHCOLSTATE_TYPE_INT | SHCOLSTATE_ONBYDEFAULT;	
		break;
	default:
		return ( E_INVALIDARG );
	}

	return ( S_OK );
}

STDMETHODIMP 
CRpfFileSystem::GetDetailsEx(PCUITEMID_CHILD pidl, const SHCOLUMNID* pscid, VARIANT* pv)
{
	ATLTRACENOTIMPL( _T("CRpfFileSystem::GetDetailsEx\n") );
}

STDMETHODIMP 
CRpfFileSystem::GetDetailsOf(PCUITEMID_CHILD pidl, UINT iColumn, SHELLDETAILS* psd)
{	
	ATLTRACE( _T("CRpfFileSystem(0x%08x)::GetDetailsOf(iColumn=%d) pidl=[%s]\n"), 
		this, iColumn, PidlToString(pidl) );

	if (iColumn >= DETAILS_COLUMN_MAX)
		return ( E_FAIL );

	// Shell asks for the column headers
	if (pidl == NULL)
	{
		// Load the iColumn based string from the resource
		CString ColumnName(MAKEINTRESOURCE(IDS_COLUMN_NAME_FILENAME+iColumn));
		psd->fmt = LVCFMT_LEFT;
		psd->cxChar = 32;
		return SetReturnString(ColumnName, psd->str) ? S_OK : E_OUTOFMEMORY;
	}

	// Okay, this time it's for a real item
	TCHAR tmpStr[16];
	switch (iColumn)
	{
	case DETAILS_COLUMN_FILENAME:
		psd->fmt = LVCFMT_LEFT;
		psd->cxChar = wcslen(CRpfFileSystemPidl::GetFilename(pidl));
		return SetReturnStringW(CRpfFileSystemPidl::GetFilename(pidl), psd->str) ? S_OK : E_OUTOFMEMORY;

	case DETAILS_COLUMN_PACKED_SIZE:
		{
			ZeroMemory( tmpStr, sizeof(TCHAR) * 16 );
			LONG packedSize = CRpfFileSystemPidl::GetEntryPackedSize( pidl );
			
			wsprintf( tmpStr, _T("%d"), packedSize );
			psd->fmt = LVCFMT_LEFT;
			psd->cxChar = _tcslen( tmpStr );
			return SetReturnStringW( tmpStr, psd->str) ? S_OK : E_OUTOFMEMORY;
		}
	case DETAILS_COLUMN_SIZE:
		{
			ZeroMemory( tmpStr, sizeof(TCHAR) * 16 );
			LONG size = CRpfFileSystemPidl::GetEntrySize( pidl );

			wsprintf( tmpStr, _T("%d"), size );
			psd->fmt = LVCFMT_LEFT;
			psd->cxChar = _tcslen( tmpStr );
			return SetReturnString(tmpStr, psd->str) ? S_OK : E_OUTOFMEMORY;
		}
	case DETAILS_COLUMN_RATIO:
		{
			ZeroMemory( tmpStr, sizeof(TCHAR) * 16 );
			LONG packedSize = CRpfFileSystemPidl::GetEntryPackedSize( pidl );
			LONG size = CRpfFileSystemPidl::GetEntrySize( pidl );
			float ratio = size / (float)packedSize;

			wsprintf( tmpStr, _T("%0.2f"), size );
			psd->fmt = LVCFMT_LEFT;
			psd->cxChar = _tcslen( tmpStr );
			return SetReturnString(tmpStr, psd->str) ? S_OK : E_OUTOFMEMORY;
		}
	case DETAILS_COLUMN_OFFSET:
		break;
	case DETAILS_COLUMN_INDEX:
		break;
	case DETAILS_COLUMN_VERSION:
		break;
	case DETAILS_COLUMN_VIRTUAL_SIZE:
		break;
	case DETAILS_COLUMN_PHYSICAL_SIZE:
		break;
	}

	return E_INVALIDARG;
}

STDMETHODIMP 
CRpfFileSystem::MapColumnToSCID(UINT iColumn, SHCOLUMNID* pscid)
{
	ATLTRACENOTIMPL( _T("CRpfFileSystem::MapColumnToSCID\n") );
}       

bool 
CRpfFileSystem::SetReturnStringA(LPCSTR Source, STRRET &str)
{
	ULONG StringLen = strlen(Source)+1;
	str.uType = STRRET_WSTR;
	str.pOleStr = (LPOLESTR)m_allocator.Alloc(StringLen*sizeof(OLECHAR));
	if (!str.pOleStr)
		return false;

	mbstowcs(str.pOleStr, Source, StringLen);
	return true;
}

bool 
CRpfFileSystem::SetReturnStringW(LPCWSTR Source, STRRET &str)
{
	ULONG StringLen = wcslen(Source)+1;
	str.uType = STRRET_WSTR;
	str.pOleStr = (LPOLESTR)m_allocator.Alloc(StringLen*sizeof(OLECHAR));
	if (!str.pOleStr)
		return false;

	wcsncpy(str.pOleStr, Source, StringLen);
	return true;
}

/************************************************************************/
/* CRpfFileSystemPidl : PIDL Methods                                    */
/* Structure:                                                           */
/*	DWORD  Magic                                                        */
/*  USHORT Type                                                         */
/*  LONG   Compressed Size                                              */
/*  LONG   Uncompressed Size                                            */
/*  USHORT[] Filename                                                   */
/* Structure:                                                           */
/************************************************************************/

LONG
CRpfFileSystemPidl::GetSize( )
{
	return ( 4 + 
		2 + 
		4 +
		4 +
		(_tcslen( m_sFilename ) + 1) * sizeof( OLECHAR ) );
}

void
CRpfFileSystemPidl::CopyTo( LPVOID pTarget )
{	
	*(DWORD*)pTarget = MAGIC;
	*((USHORT*)pTarget+2) = m_ItemType;
	*((LONG*)pTarget+3) = m_lSize;
	*((LONG*)pTarget+4) = m_lUncompressedSize;
#ifdef _UNICODE
	wcscpy((OLECHAR*)pTarget+5, m_sFilename);
#else
	mbstowcs((OLECHAR*)pTarget+5, m_sFilename, strlen(m_sFilename)+1);
#endif
}

bool
CRpfFileSystemPidl::IsOwn( LPCITEMIDLIST pidl )
{
	if ( ( NULL == pidl ) || ( pidl->mkid.cb < 4 ) )
		return ( false );

	return *((DWORD*)(pidl->mkid.abID)) == MAGIC;
}

LPOLESTR
CRpfFileSystemPidl::GetFilename( LPCITEMIDLIST pidl )
{
	return (OLECHAR*)pidl + 5;
}

CRpfFileSystemPidl::RpfEntryType 
CRpfFileSystemPidl::GetItemType( LPCITEMIDLIST pidl )
{
	// DHM TODO
	return (RPF_PIDL_FILE);
}

LONG
CRpfFileSystemPidl::GetEntryPackedSize( LPCITEMIDLIST pidl )
{
	// DHM TODO
	return (0);
}

LONG
CRpfFileSystemPidl::GetEntrySize( LPCITEMIDLIST pidl )
{
	// DHM TODO
	return (0);
}

LONG
CRpfFileSystemPidl::GetEntryOffset( LPCITEMIDLIST pidl )
{
	// DHM TODO
	return (0);
}

USHORT
CRpfFileSystemPidl::GetEntryIndex( LPCITEMIDLIST pidl )
{
	// DHM TODO
	return (0);
}

USHORT
CRpfFileSystemPidl::GetEntryVersion( LPCITEMIDLIST pidl )
{
	// DHM TODO
	return (0);
}

LONG
CRpfFileSystemPidl::GetEntryVirtualSize( LPCITEMIDLIST pidl )
{
	// DHM TODO
	return (0);
}

LONG
CRpfFileSystemPidl::GetEntryPhysicalSize( LPCITEMIDLIST pidl )
{
	// DHM TODO
	return (0);
}

/************************************************************************/
/* Utility Methods                                                       */
/************************************************************************/

LPTSTR 
PidlToString(LPCITEMIDLIST pidl)
{
	static int which = 0;
	static TCHAR str1[200];
	static TCHAR str2[200];
	TCHAR *str;
	which ^= 1;
	str = which ? str1 : str2;

	str[0] = '\0';

	if (pidl == NULL)
	{
		_tcscpy(str, _T("<null>"));
		return str;
	}

	while (pidl->mkid.cb != 0)
	{
		if (*((DWORD*)(pidl->mkid.abID)) == CRpfFileSystemPidl::MAGIC)
		{
#ifndef _UNICODE
			char tmp[128];
			mbstowcs((USHORT*)(pidl->mkid.abID)+4, tmp, 128);
			_tcscat(str, tmp);
#else
			_tcscat(str, (const wchar_t*)(pidl->mkid.abID)+4);
#endif
			_tcscat(str, _T("::"));
		}
		else
		{
			TCHAR tmp[16];
			_stprintf(tmp, _T("<unk-%02d>::"), pidl->mkid.cb);
			_tcscat(str, tmp);
		}

		pidl = LPITEMIDLIST(LPBYTE(pidl) + pidl->mkid.cb);
	}
	return str;
}
