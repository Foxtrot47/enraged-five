// RockstarMenu.h : Declaration of the CRockstarMenu
#ifndef ROCKSTARMENU_H
#define ROCKSTARMENU_H

#if defined _MSC_VER && _MSC_VER >= 1300
#pragma once
#endif

#include "resource.h"       // main symbols
#include "shell.h"
#include "dllmain.h"

#include "ShellExtension_i.h"

//Here's what we need for the shell interfaces:
#include <shlobj.h>
#include <comdef.h>

// Windows headers
#include <atlbase.h>
#include <atlcom.h>
#include <Windows.h>

//We'll use a list of strings to store the filenames selected:
#include <string>
#include <list>
#include <vector>

#include "ContextMenu.h"
#include "RegistryMap.h"
USE_SHELL_EXTENSION_FRAMEWORK_NS

#ifdef _UNICODE
typedef std::wstring STL_STRING;
#else
#error _UNICODE must be defined.
#endif // _UNICODE
typedef std::list<STL_STRING> string_list;

#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif

// LibXML2 headers
#include "libxml/xpath.h"

// Forward-declarations
namespace configParser
{
	class ConfigGameView;
}

namespace configUtil
{
	class Environment;
}

// CRockstarMenu

class ATL_NO_VTABLE CRockstarMenu :
	public CContextMenu<CRockstarMenu, &CLSID_RockstarMenu>
	/*,
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CRockstarMenu, &CLSID_RockstarMenu>,
	public IShellExtInit,
	public IContextMenu3,
	public IObjectWithSiteImpl<CRockstarMenu>*/
{
public:
	CRockstarMenu();
	~CRockstarMenu();

	DECLARE_REGISTRY_RESOURCEID_EX(IDR_ROCKSTARMENU)
	BEGIN_REGISTRY_MAP(CRockstarMenu)
		REGMAP_UUID( "CLSID", CLSID_RockstarMenu )
		REGMAP_ENTRY( "NAME", "Rockstar Games Menu" )
		REGMAP_ENTRY( "DESCRIPTION", "Rockstar Games Context Menu" )
	END_REGISTRY_MAP()

DECLARE_NOT_AGGREGATABLE(CRockstarMenu)

BEGIN_COM_MAP(CRockstarMenu)
	COM_INTERFACE_ENTRY(IShellExtInit)
	COM_INTERFACE_ENTRY(IContextMenu)
	COM_INTERFACE_ENTRY(IContextMenu2)
	COM_INTERFACE_ENTRY(IContextMenu3)
	COM_INTERFACE_ENTRY(IObjectWithSite)
END_COM_MAP()

	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:

	// IShellExtInit
	STDMETHODIMP Initialize(PCIDLIST_ABSOLUTE, LPDATAOBJECT, HKEY);

	// IContextMenu
	STDMETHODIMP GetCommandString(UINT_PTR, UINT, UINT*, LPSTR, UINT cchMax);
	STDMETHODIMP InvokeCommand(LPCMINVOKECOMMANDINFO);
	STDMETHODIMP QueryContextMenu(HMENU, UINT, UINT, UINT, UINT);

	// IContextMenu2
	STDMETHODIMP HandleMenuMsg(	UINT uMsg, WPARAM wParam, LPARAM lParam );

	// IContextMenu3
	STDMETHODIMP HandleMenuMsg2( UINT uMsg, WPARAM wParam, LPARAM lParam, LRESULT* plResult );

protected:

	// Command Handler structure
	typedef struct _TCommandHandler
	{
		std::wstring	name;	// Menu name string
		std::wstring	cmd;	// Command string
		std::wstring	args;	// Arguments string
		std::wstring	filter;	// File filter to enable command
		std::wstring	tooltip; // Tooltip to display on menu item hover
		bool			dummy;	// Dummy flag.

		// Constructor.
		_TCommandHandler( const std::wstring& name, const std::wstring& cmd, 
			const std::wstring& args, const std::wstring& filter,
			const std::wstring& tooltip )
			: name( name )
			, cmd( cmd )
			, args( args )
			, filter( filter )
			, tooltip( tooltip )
			, dummy( false )
		{
		}

		// Pre-defined dummy handler constructor.
		_TCommandHandler( const std::wstring& name, const std::wstring& cmd, 
			const std::wstring& tooltip )
			: name( name )
			, cmd( cmd )
			, tooltip( tooltip )
			, dummy( true )
		{
		}

		// Copy constructor.
		_TCommandHandler( const _TCommandHandler& h )
			: name( h.name )
			, cmd( h.cmd )
			, args( h.args )
			, filter( h.filter )
			, tooltip( h.tooltip )
			, dummy( h.dummy )
		{
		}
	} TCommandHandler;

	typedef std::vector<TCommandHandler*> TCommandHandlerMap;
	typedef TCommandHandlerMap::iterator TCommandHandlerMapIter;

	// Configuration file type.
	enum ConfigFileType
	{
		Unknown,
		XML,	// Old XML-style.
		Meta	// New parCodeGen-style.
	};

	ConfigFileType		m_eShellConfigType;
	TCHAR				m_sShellFilename[MAX_PATH]; // Top-level loaded Shell XML.
	HBITMAP				m_hRsBitmap32;
	HBITMAP				m_hRsBitmap16;
	bool				m_bIsFolder;		// Selected directory flag.
	string_list			m_lFiles;			// Selected files, $(files)
	TCHAR				m_sDirectory[MAX_PATH];	// Selected directory, $(dir)
	UINT				m_nMaxID;
	TCommandHandlerMap	m_mCmdHandlers;		// Command handlers

	// Handler Methods
	void				ClearHandlers( );
	TCommandHandler*	CreateHandler( configUtil::Environment* pEnv, shell::MenuCommand* pMenuItem );
	bool				IsHandlerValid( TCommandHandler* pHandler );

	// Config Data Parsing Methods
	bool	ParseShellConfig( HMENU& hm, UINT& uidCmd );
	bool	BuildMenuItem( const configParser::ConfigGameView* gv, 
		configUtil::Environment* pEnv, HMENU& parentMenu, UINT& uidCmd, 
		shell::MenuItem* pMenuItem );
	bool	BuildMenuItem( const configParser::ConfigGameView* gv, 
		configUtil::Environment* pEnv, HMENU& parentMenu, UINT& uidCmd, 
		xmlNodePtr pNode );
	TCommandHandler*	CreateHandler( configUtil::Environment* pEnv, xmlNodePtr pNode );

	// Menu Construction Methods
	void	CreateMenuItem( TCommandHandler* pHandler, HMENU menu, UINT& uidCmd, 
		HBITMAP hBitmap = NULL, bool enabled = true, 
		bool checked = false );
	void	CreateSeparatorItem( HMENU menu );

	// Common Item Handlers
	void	ShowAboutBox( ) const;

private:
	inline void 
	VersionDwordLong2String( DWORDLONG Version, std::wstring& verStr ) const
	{
		WORD a, b, c, d;

		a = (WORD)(Version >> 48);
		b = (WORD)((Version >> 32) & 0xffff);
		c = (WORD)((Version >> 16) & 0xffff);
		d = (WORD)(Version & 0xffff);

		TCHAR buffer[MAX_PATH] = {0};
		wsprintf( buffer, L"%d.%d.%d.%d", a, b, c, d );
		verStr = std::wstring( buffer );
	}

	inline bool FileExists( TCHAR* filename )
	{
		DWORD dwAttrib = GetFileAttributes( filename );

		return (dwAttrib != INVALID_FILE_ATTRIBUTES && 
			!(dwAttrib & FILE_ATTRIBUTE_DIRECTORY));
	}
};

OBJECT_ENTRY_AUTO(__uuidof(RockstarMenu), CRockstarMenu)

#endif // ROCKSTARMENU_H
