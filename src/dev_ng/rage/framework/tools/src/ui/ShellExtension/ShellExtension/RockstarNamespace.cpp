//
// File:: RockstarNamespace.cpp
// Description:: Shell extension namespace.
//
// Author:: David Muir <david.muir@rockstarnorth.com>
// Date:: 10 April 2014
//

#include "RockstarNamespace.h"

const CLSID CLSID_RockstarNamespace = // {FB08B1EA-D30E-4DBD-8BA3-9F5B550BD7B8}
	{0xfb08b1ea, 0xd30e, 0x4dbd, { 0x8b, 0xa3, 0x9f, 0x5b, 0x55, 0xb, 0xd7, 0xb8 }};


