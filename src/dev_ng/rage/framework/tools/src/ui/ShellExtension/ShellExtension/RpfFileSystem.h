#ifndef RPFFILESYSTEM_H
#define RPFFILESYSTEM_H

//
// File:: RpfFileSystem.h
// Description:: RPF File System extension.
//
// Author:: David Muir <david.muir@rockstarnorth.com>
// Date:: 14 April 2014
//

#include "ShellExtensionFramework.h"
#include "RegistryMap.h"
#include "Namespace.h"
#include "NamespaceItem.h"
#include "Pidl.h"
#include "PidlManager.h"

#include "dllmain.h"
#include "atlstr.h"

// RageCore headers
#include "file/packfile.h"
#include "system/magicnumber.h"

extern const CLSID CLSID_RpfFileSystem;

USE_SHELL_EXTENSION_FRAMEWORK_NS

// Forward-declaration.
class CRpfFileSystemPidl;

/**
	\brief Details view column constants.
 */
enum
{
	DETAILS_COLUMN_FILENAME,
	DETAILS_COLUMN_SIZE,
	DETAILS_COLUMN_PACKED_SIZE,
	DETAILS_COLUMN_RATIO,
	DETAILS_COLUMN_OFFSET,
	DETAILS_COLUMN_INDEX,
	DETAILS_COLUMN_VERSION,
	DETAILS_COLUMN_VIRTUAL_SIZE,
	DETAILS_COLUMN_PHYSICAL_SIZE,

	DETAILS_COLUMN_MAX
};

#ifdef _UNICODE
#define SetReturnString SetReturnStringW
#else
#define SetReturnString SetReturnStringA
#endif

/**
	\brief RPF File System.
	This presents a flattened list of files contained in an RPF in Windows Explorer.
 */
class ATL_NO_VTABLE CRpfFileSystem : 
	public CNamespace,
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CRpfFileSystem, &CLSID_RpfFileSystem>,
	public IShellFolder2,
	public IPersistFolder2
{
public:
	CRpfFileSystem( );
	virtual ~CRpfFileSystem( ); 

	DECLARE_REGISTRY_RESOURCEID_EX( IDR_RPFFILESYSTEM )
	BEGIN_REGISTRY_MAP(CRpfFileSystem)
		REGMAP_UUID( "CLSID", CLSID_RpfFileSystem )
		REGMAP_ENTRY( "DESCRIPTION", "Rockstar Games RPF FileSystem" )
		REGMAP_ENTRY( "ATTRIBUTES", "28000000" ) // HEX SFGAO_BROWSABLE | SFGAO_FOLDER
	END_REGISTRY_MAP()
	
	DECLARE_PROTECT_FINAL_CONSTRUCT()

	BEGIN_COM_MAP(CRpfFileSystem)
		COM_INTERFACE_ENTRY(IShellFolder)
		COM_INTERFACE_ENTRY(IShellFolder2)
		COM_INTERFACE_ENTRY(IPersistFolder)
		COM_INTERFACE_ENTRY(IPersistFolder2)
		COM_INTERFACE_ENTRY(IPersist)
	END_COM_MAP()
	
	// IPersist
	STDMETHOD(GetClassID)( CLSID* pclsid );

	// IPersistFolder
	STDMETHOD(Initialize)( PCIDLIST_ABSOLUTE pidl );

	// IPersistFolder2
	STDMETHOD(GetCurFolder)( PIDLIST_ABSOLUTE* ppidl );
	
	// IShellFolder
	STDMETHOD(ParseDisplayName)( HWND hwnd, IBindCtx* pbc, PWSTR pszDisplayName, ULONG* pchEaten, PIDLIST_RELATIVE* ppidl, ULONG* pdwAttributes);
	STDMETHOD(EnumObjects)( HWND hwnd, SHCONTF grfFlags, IEnumIDList** ppEnumIDList);
	STDMETHOD(BindToObject)( PCUIDLIST_RELATIVE pidl, IBindCtx* pbc, REFIID riid, LPVOID* ppRetVal);
	STDMETHOD(BindToStorage)( PCUIDLIST_RELATIVE pidl, IBindCtx* pbc, REFIID riid, LPVOID* ppRetVal);
	STDMETHOD(CompareIDs)( LPARAM lParam, PCUIDLIST_RELATIVE pidl1, PCUIDLIST_RELATIVE pidl2);
	STDMETHOD(CreateViewObject)( HWND hwndOwner, REFIID riid, LPVOID* ppRetVal);
	STDMETHOD(GetAttributesOf)( UINT cidl, PCUITEMID_CHILD_ARRAY rgpidl, SFGAOF* rgfInOut);
	STDMETHOD(GetUIObjectOf)( HWND hwndOwner, UINT cidl, PCUITEMID_CHILD_ARRAY rgpidl, REFIID riid, UINT* rgfReserved, LPVOID* ppRetVal);
	STDMETHOD(GetDisplayNameOf)( PCUITEMID_CHILD pidl, SHGDNF uFlags, STRRET* psrName);
	STDMETHOD(SetNameOf)( HWND hwnd, PCUITEMID_CHILD pidl, LPCWSTR pszName, SHGDNF uFlags, PITEMID_CHILD* ppidlOut);

	// IShellFolder2
	STDMETHOD(GetDefaultSearchGUID)(GUID* pguid);
	STDMETHOD(EnumSearches)(IEnumExtraSearch** ppenum);
	STDMETHOD(GetDefaultColumn)(DWORD dwRes, ULONG* pSort, ULONG* pDisplay);
	STDMETHOD(GetDefaultColumnState)(UINT iColumn, SHCOLSTATEF* pcsFlags);
	STDMETHOD(GetDetailsEx)(PCUITEMID_CHILD pidl, const SHCOLUMNID* pscid, VARIANT* pv);
	STDMETHOD(GetDetailsOf)(PCUITEMID_CHILD pidl, UINT iColumn, SHELLDETAILS* psd);
	STDMETHOD(MapColumnToSCID)(UINT iColumn, SHCOLUMNID* pscid);

private:
	unsigned int		m_refCount;
	CPidlManager		m_PidlManager;
	LPITEMIDLIST		m_pidlRoot;

	CShellAllocator		m_allocator;
	::rage::fiPackfile	m_pArchive;	// Rage RPF file handle.
	
	bool SetReturnStringA( LPCSTR Source, STRRET& str );
	bool SetReturnStringW( LPCWSTR Source, STRRET& str );
};

/**
	\brief RpfFileSystem PIDL class.
 */
class CRpfFileSystemPidl : public CPidl
{
public:
	enum { MAGIC = MAKE_MAGIC_NUMBER( 'R', 'P', 'F', 0 ) };
	typedef enum tagRpfEntryType
	{
		RPF_PIDL_FILE,
		RPF_PIDL_RESOURCE
	} RpfEntryType;

	// CPidl Methods
	LONG				GetSize( );
	void				CopyTo( LPVOID pTarget );

	// Define set of static methods used to get data from a given PIDL.

	/** \brief Is this PIDL one of the RpfFileSystem? */
	static bool			IsOwn( LPCITEMIDLIST pidl );

	/** \brief Return target path */
	static LPOLESTR		GetFilename( LPCITEMIDLIST pidl );

	/** \brief Return type */
	static RpfEntryType GetItemType( LPCITEMIDLIST pidl );

	/** \brief Return compressed size */
	static LONG			GetEntryPackedSize( LPCITEMIDLIST pidl );

	/** \brief Return uncompressed size */
	static LONG			GetEntrySize( LPCITEMIDLIST pidl );

	/** \brief Return offset */
	static LONG			GetEntryOffset( LPCITEMIDLIST pidl );

	/** \brief Return index */
	static USHORT		GetEntryIndex( LPCITEMIDLIST pidl );
	
	/** \brief Return entry resource version (if RPF_PIDL_RESOURCE) */
	static USHORT		GetEntryVersion( LPCITEMIDLIST pidl );
	
	/** \brief Return Virtual Size */
	static LONG			GetEntryVirtualSize( LPCITEMIDLIST pidl );

	/** \brief Return Physical Size */
	static LONG			GetEntryPhysicalSize( LPCITEMIDLIST pidl );

protected:
	unsigned short	m_ItemType;
	LONG			m_lSize;
	LONG			m_lUncompressedSize;
	unsigned int	m_lOffset;
	USHORT			m_uIndex;
	USHORT			m_uVersion;
	LONG			m_lVirtualSize;
	LONG			m_lPhysicalSize;
	CString			m_sFilename;
};

/**
	\brief RPF File System Root item.
 */
class CRpfItemRoot : public CNamespaceItem
{

};

#endif // RPFFILESYSTEM_H
