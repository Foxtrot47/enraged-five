#ifndef ROCKSTARNAMESPACE_H
#define ROCKSTARNAMESPACE_H

//
// File:: RockstarNamespace.h
// Description:: Shell extension namespace.
//
// Author:: David Muir <david.muir@rockstarnorth.com>
// Date:: 10 April 2014
//

#include "ShellExtensionFramework.h"
#include "Namespace.h"
#include "NamespaceItem.h"

extern const CLSID CLSID_RockstarNamespace;

USE_SHELL_EXTENSION_FRAMEWORK_NS
	
#if 0
/**
	\brief Rockstar Namespace extension.
 */
class CRockstarNamespace : 
	public CNamespace<CRockstarNamespace, &CLSID_RockstarNamespace>
{
public:
	CRockstarNamespace( );
	virtual ~CRockstarNamespace( );
};

class CRockstarNamespaceItemRoot : public CNamespaceItem
{

};
#endif

#endif // ROCKSTARNAMESPACE_H
