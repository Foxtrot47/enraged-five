REM 
REM File:: %RS_TOOLSSRC%/ui/ShellExtension/register.bat
REM Description:: Force unregistration of the Shell Extension
REM
REM Author:: David Muir <david.muir@rockstarnorth.com>
REM Date:: 4 June 2010
REM

CALL setenv.bat
regsvr32 ShellExtension_Win32.dll

REM %RS_TOOLSSRC%/ui/ShellExtension/register.bat
