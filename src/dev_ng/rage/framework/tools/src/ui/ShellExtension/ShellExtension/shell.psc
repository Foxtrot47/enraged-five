<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"

							generate="class">
	<!--
		File:: shell.psc
		Description: Tools Windows Shell Extension Configuration Files Parser Schema
		Version: 1.0
		
		Author: David Muir <david.muir@rockstarnorth.com>
		Date: 6 January 2014
	-->
	
	<!-- MenuItem Definition -->
	<structdef type="::shell::MenuItem">
	</structdef>

	<!-- MenuCommand Definition -->
	<structdef autoregister="true" type="::shell::MenuCommand" base="::shell::MenuItem">
		<string name="Name" type="atString" description="Friendly-name for command." />
		<string name="Handler" type="atString" description="Command-line executable or batch script." />
		<string name="Arguments" type="atString" description="Command-line arguments (e.g. $(files), $(directory) or hard-coded.)." />
		<string name="Filter" type="atString" description="File/directory filter to enable/disable command." />
		<string name="ToolTip" type="atString" description="Tooltip for command." />
	</structdef>

	<!-- Menu Definition -->
	<structdef type="::shell::Menu" base="::shell::MenuItem" preserveNames="true">
		<bool name="Visible" init="true" description="Whether the menu is visible." />
		<string name="Name" type="pointer" description="Friendly-name for menu." />
		<array name="Items" type="atArray" description="Array of menu items." >
			<pointer type="::shell::MenuItem" policy="owner" />
		</array>
	</structdef>

	<!-- Separator Definition (just has special meaning) -->
	<structdef type="::shell::Separator" base="::shell::MenuItem" preserveNames="true" />

	<!-- AutoMenu Definition -->
	<structdef type="::shell::AutoMenu" base="::shell::MenuItem" preserveNames="true">
		<string name="Name" type="pointer" description="Friendly-name for menu." />
		<string name="Variable" type="pointer" description="Variable to trigger the auto-build of this menu (only $(branch) and $(target) supported)." />
		<array name="Items" type="atArray" description="Array of menu items.">
			<pointer type="::shell::MenuItem" policy="owner" />
		</array>
	</structdef>

	<!-- MenuInclude Definition -->
	<structdef type="::shell::MenuInclude" base="::shell::MenuItem" preserveNames="true">
		<string name="Name" type="pointer" description="Friendly-name for menu." />
		<string name="Filename" type="pointer" description="Filename to load menu definition from." />
	</structdef>

	<!--
		NamespaceItem Definition
		This defines a shell folder namespace item.
	-->
	<structdef type="::shell::NamespaceItem">
		<string name="Name" type="pointer" description="Friendly-name for Shell Folder item." />
	</structdef>

	<!--
		NamespaceItemWebView Definition
		This defines a shell folder item with a web-page view control.
	-->
	<structdef type="::shell::NamespaceItemWebView" base="::shell::NamespaceItem">
		<string name="URL" type="pointer" description="URL for webpage to embed." />
	</structdef>

	<!--
		NamespaceItemAlias Definition
		This defines a shell folder item that aliases a project directory.
	-->
	<structdef type="::shell::NamespaceItemAlias" base="::shell::NamespaceItem">
		<string name="Path" type="pointer" description="Absolute path to folder." />
	</structdef>
	
	<!-- 
		Namespace Definition 
		This is the top-level configuration object for the root Explorer folder.
	-->
	<structdef type="::shell::Namespace" preserveNames="true">
		<bool name="Enabled" init="true" description="Whether the shell namespace is enabled." />
		<array name="Items" type="atArray" description="Array of Shell Folder items.">
			<pointer type="::shell::NamespaceItem" policy="owner" />
		</array>
	</structdef>

	<!-- Shell Definition -->
	<structdef type="::shell::ShellConfig" preserveNames="true">
		<u32 name="Version" description="Configuration version identifier." />
		<struct type="::shell::Menu" name="Menu" description="Top-level menu object." />
		<struct type="::shell::Namespace" name="Namespace" description="Top-level namespace object." />
	</structdef>
</ParserSchema>