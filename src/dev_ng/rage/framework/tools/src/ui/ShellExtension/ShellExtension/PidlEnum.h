#ifndef PIDLENUM_H
#define PIDLENUM_H

//
// File:: PidlEnum.h
// Description:: PIDL enumeration list.
//
// Author:: David Muir <david.muir@rockstarnorth.com>
// Date:: 26 February 2014
//

#if defined _MSC_VER && _MSC_VER >= 1300
#pragma once
#endif

// Windows headers
#include <atlbase.h>
#include <atlcom.h>
#include <ShlObj.h>

// Local headers
#include "PidlMgr.h"

typedef struct tagENUMLIST
{
	struct tagENUMLIST   *pNext;
	LPITEMIDLIST         pidl;
} ENUMLIST, FAR *LPENUMLIST;

/////////////////////////////////////////////////////////////////////////////
// CPidlEnum

class ATL_NO_VTABLE CPidlEnum : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public IEnumIDList
{
public:
	DECLARE_PROTECT_FINAL_CONSTRUCT()

	BEGIN_COM_MAP(CPidlEnum)
		COM_INTERFACE_ENTRY_IID(IID_IEnumIDList,IEnumIDList)
	END_COM_MAP()

public:
	LPENUMLIST m_pFirst;
	LPENUMLIST m_pLast;
	LPENUMLIST m_pCurrent;
	CPidlMgr   m_PidlMgr;

public:
	HRESULT FinalConstruct()
	{
		m_pFirst = m_pLast = m_pCurrent = NULL;
		return S_OK;
	}

	void FinalRelease()
	{
		DeleteList();
		m_pFirst = m_pLast = m_pCurrent = NULL;
	}

public:
	//IEnumIDList
	STDMETHOD (Next) (DWORD, LPITEMIDLIST*, LPDWORD);
	STDMETHOD (Skip) (DWORD);
	STDMETHOD (Reset) (void);
	STDMETHOD (Clone) (LPENUMIDLIST*);

	//User Defined Funcs
	BOOL DeleteList(void);
	BOOL AddToEnumList(LPITEMIDLIST pidl);
	HRESULT _Init(LPCITEMIDLIST pidlRoot,DWORD dwFlags);
	HRESULT _AddPidls(ITEM_TYPE iItemType, LPTSTR pszPath);

};

#endif // PIDLENUM_H