#pragma once

#include <Windows.h>
#include <Mmsystem.h>

class audMidiDriver
{
public:
	audMidiDriver(void);
	~audMidiDriver(void);

	static bool InitClass();

	static void ShutdownClass();
	static bool OutputBytes(unsigned char *buf, unsigned long numBytes);

private:

	 static void CALLBACK MidiOutProc(HMIDIOUT MidiOut, UINT Msg,
                                         DWORD Instance, DWORD Param1, 
                                         DWORD Param2);
	static HMIDIOUT sm_DeviceHandle;
};
