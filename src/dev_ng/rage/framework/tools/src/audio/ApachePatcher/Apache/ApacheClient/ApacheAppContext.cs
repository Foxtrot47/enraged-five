using System;
using System.Windows.Forms;
using System.Drawing;
using System.Collections;
using System.Net;
using System.Net.Sockets;

namespace ApacheClient
{
	/// <summary>
	/// This class has several jobs:
	///		- Create the NotifyIcon UI
	///		- Determines when the Application should exit
	/// </summary>
	public class ApacheClientApplicationContext : ApplicationContext 
	{
		private System.ComponentModel.IContainer	components;						// a list of components to dispose when the context is disposed
		private System.Windows.Forms.NotifyIcon		m_NotifyIcon;				// the icon that sits in the system tray
		private System.Windows.Forms.ContextMenu	m_NotifyIconContextMenu;	// the context menu for the notify icon
		private System.Windows.Forms.MenuItem		exitContextMenuItem;			// exit menu command for context menu 

		private PatchSettings m_PatchSettings;
		private Hashtable m_MenuHashTable;

		private UdpClient m_UdpClient;
		
		/// <summary>
		/// This class should be created and passed into Application.Run( ... )
		/// </summary>
		public ApacheClientApplicationContext() 
		{
			// create the notify icon and it's associated context menu
			InitializeContext();

			m_MenuHashTable = new Hashtable();
			m_PatchSettings = new PatchSettings("patchSettings.xml");

			m_UdpClient = new UdpClient(m_PatchSettings.PatchServerHost, m_PatchSettings.PatchServerPort);

			// create context menu
			OutputHostEntry host = m_PatchSettings.OutputMap.FindOutputHost(Environment.MachineName.ToLower());
            if (host == null)
            {
                MessageBox.Show(string.Format("Couldn't find machine name {0} in patchSettings.xml", Environment.MachineName));
            }
            else
            {
                foreach (PatchEntry patch in host.HostPatches)
                {
                    MenuItem patchMenuItem = new MenuItem(patch.Name, new System.EventHandler(mnuClickHandler));

                    m_MenuHashTable.Add(patchMenuItem, new PatchSetting(m_PatchSettings.PortMap.FindInputPort(patch.Patch).PortNumber,
                        m_PatchSettings.PortMap.FindOutputPort(host.OutputHostPorts[0].PortName).PortNumber));

                    m_NotifyIconContextMenu.MenuItems.Add(patchMenuItem);
                }


                foreach (OutputHostPortEntry outputPort in host.OutputHostPorts)
                {
                    m_NotifyIconContextMenu.MenuItems.Add("-");

                    MenuItem outputPortMenu = new MenuItem(outputPort.DisplayName);

                    foreach (PatchGroupEntry patchGroup in m_PatchSettings.PatchGroups)
                    {
                        MenuItem patchGroupMenu = new MenuItem(patchGroup.Name);
                        foreach (PatchEntry patch in patchGroup.Patches)
                        {
                            MenuItem patchMenuItem = new MenuItem(patch.Name, new System.EventHandler(mnuClickHandler));
                            m_MenuHashTable.Add(patchMenuItem, new PatchSetting(m_PatchSettings.PortMap.FindInputPort(patch.Patch).PortNumber,
                                    m_PatchSettings.PortMap.FindOutputPort(outputPort.PortName).PortNumber));
                            patchGroupMenu.MenuItems.Add(patchMenuItem);
                        }
                        outputPortMenu.MenuItems.Add(patchGroupMenu);
                    }

                    m_NotifyIconContextMenu.MenuItems.Add(outputPortMenu);
                }

            }
			m_NotifyIconContextMenu.MenuItems.Add("-");

			m_NotifyIconContextMenu.MenuItems.AddRange(new MenuItem[] { exitContextMenuItem });
			exitContextMenuItem.Text = "&Exit";
			exitContextMenuItem.Click += new System.EventHandler(this.exitContextMenuItem_Click);
		}

		/// <summary>
		/// Create the NotifyIcon UI, the ContextMenu for the NotifyIcon and an Exit menu item. 
		/// </summary>
		private void InitializeContext() 
		{
			this.components = new System.ComponentModel.Container();
			this.m_NotifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
			this.m_NotifyIconContextMenu = new System.Windows.Forms.ContextMenu();

			this.exitContextMenuItem = new System.Windows.Forms.MenuItem();
		
			// 
			// m_NotifyIcon
			// 
			this.m_NotifyIcon.ContextMenu = this.m_NotifyIconContextMenu;
			this.m_NotifyIcon.DoubleClick += new System.EventHandler(this.m_NotifyIcon_DoubleClick);
			this.m_NotifyIcon.Icon = new Icon(GetType(), "App.ico");
			this.m_NotifyIcon.Text = "ApachePatcher";
			this.m_NotifyIcon.Visible = true;
		}


		private void mnuClickHandler(object sender, System.EventArgs e)
		{
			if(m_MenuHashTable.ContainsKey(sender))
			{
				PatchSetting patch = (PatchSetting)m_MenuHashTable[sender];
				System.Diagnostics.Debug.WriteLine(string.Format("Patching {0} to {1} - {2}\n", patch.From, patch.To, patch.ToString()));
				byte[] buf = System.Text.Encoding.ASCII.GetBytes(patch.ToString());
				m_UdpClient.Send(buf,buf.Length);
			}
		}
		/// <summary>
		/// When the application context is disposed, dispose things like the notify icon.
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}		
		}		


		/// <summary>
		/// When the exit menu item is clicked, make a call to terminate the ApplicationContext.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void exitContextMenuItem_Click(object sender, EventArgs e) 
		{
			ExitThread();
		}
		
		/// <summary>
		/// When the notify icon is double clicked in the system tray, bring up a form with a calendar on it.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void m_NotifyIcon_DoubleClick(object sender,System.EventArgs e)
		{
	
		}


		/// <summary>
		/// If we are presently showing a mainForm, clean it up.
		/// </summary>
		protected override void ExitThreadCore()
		{
			base.ExitThreadCore ();
		}
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			ApacheClientApplicationContext applicationContext = new ApacheClientApplicationContext();
			Application.Run(applicationContext);
		}
		
	}


}
