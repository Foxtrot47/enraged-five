#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Configuration::Install;


namespace ApacheServer
{
	[RunInstaller(true)]	

	/// <summary> 
	/// Summary for ProjectInstaller
	/// </summary>
	public __gc class ProjectInstaller : public System::Configuration::Install::Installer
	{
	public: 
		ProjectInstaller(void)
		{
			InitializeComponent();
		}
        
	protected: 
		void Dispose(Boolean disposing)
		{
			if (disposing && components)
			{
				components->Dispose();
			}
			__super::Dispose(disposing);
		}
	private: System::ServiceProcess::ServiceProcessInstaller *  ApacheProcessInstaller;
	private: System::ServiceProcess::ServiceInstaller *  ApacheServiceInstaller;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container* components;
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>		
		void InitializeComponent(void)
		{
			this->ApacheProcessInstaller = new System::ServiceProcess::ServiceProcessInstaller();
			this->ApacheServiceInstaller = new System::ServiceProcess::ServiceInstaller();
			// 
			// ApacheProcessInstaller
			// 
			this->ApacheProcessInstaller->Account = System::ServiceProcess::ServiceAccount::LocalSystem;
			this->ApacheProcessInstaller->Password = S"0";
			this->ApacheProcessInstaller->Username = S"0";
			// 
			// ApacheServiceInstaller
			// 
			this->ApacheServiceInstaller->DisplayName = S"Apache Patch Server";
			this->ApacheServiceInstaller->ServiceName = S"ApacheServerWinService";
			// 
			// ProjectInstaller
			// 
			System::Configuration::Install::Installer* __mcTemp__1[] = new System::Configuration::Install::Installer*[2];
			__mcTemp__1[0] = this->ApacheProcessInstaller;
			__mcTemp__1[1] = this->ApacheServiceInstaller;
			this->Installers->AddRange(__mcTemp__1);

		}
	};
}