using System;
using System.Collections;
using System.Xml;
using System.Net;
using System.Net.Sockets;

/*
 * <PatchServer host="colinaudioxp" port="22050"/>
	
	<PatchGroup name="Ali">
		<Patch name="Xenon"		patch="ALI_XENON"/>
		<Patch name="PS3"		patch="ALI_PS3"/>
		<Patch name="PC"		patch="ALI_PC"/>	
	</PatchGroup>
	<PatchGroup name="Colin">
		<Patch name="Xenon"		patch="COLIN_XENON"/>
		<Patch name="PS3"		patch="COLIN_PS3"/>
		<Patch name="PC"		patch="COLIN_PC"/>	
	</PatchGroup>
	<PatchGroup name="Matt">
		<Patch name="Xenon"		patch="MATT_XENON"/>
		<Patch name="PS3"		patch="MATT_PS3"/>
		<Patch name="PC"		patch="MATT_PC"/>	
	</PatchGroup>
	<PatchGroup name="George">
		<Patch name="Xenon"		patch="GEORGE_XENON"/>
		<Patch name="PS3"		patch="GEORGE_PS3"/>
		<Patch name="PC"		patch="GEORGE_PC"/>	
	</PatchGroup>

	<PortMap>
		<InputPort name="ALI_XENON"		port="1"/>
		<InputPort name="ALI_PS3"		port="2"/>
		<InputPort name="ALI_PC"		port="3"/>
		<InputPort name="COLIN_XENON"	port="4"/>
		<InputPort name="COLIN_PS3"		port="5"/>
		<InputPort name="COLIN_PC"		port="6"/>
		<InputPort name="MATT_XENON"	port="7"/>
		<InputPort name="MATT_PS3"		port="8"/>
		<InputPort name="MATT_PC"		port="9"/>
		<InputPort name="GEORGE_XENON"	port="10"/>
		<InputPort name="GEORGE_PS3"	port="11"/>
		<InputPort name="GEORGE_PC"		port="12"/>

		<OutputPort name="ALI_AMP"		port="1"/>
		<OutputPort name="COLIN_AMP"	port="2"/>
		<OutputPort name="MATT_AMP"		port="3"/>
		<OutputPort name="GEORGE_AMP"	port="4"/>
		<OutputPort name="GUEST_AMP"	port="5"/>
	</PortMap>
	
	<OutputMap>
		<Output host="alastair-magrg">
			<OutputPort name="ALI_AMP"/>
			<OutputPort name="GUEST_AMP"/>
		</Output>
		<Output host="colin_entwistle">
			<OutputPort name="COLIN_AMP"/>
			<OutputPort name="GUEST_AMP"/>
		</Output>
		<Output host="matthew_smith">
			<OutputPort name="MATT_AMP"/>
			<OutputPort name="GUEST_AMP"/>
		</Output>
		<Output host="audio_pc">
			<OutputPort name="GEORGE_AMP"/>
			<OutputPort name="GUEST_AMP"/>
		</Output>
	</OutputMap>
	*/
namespace ApacheClient
{

	public class PatchSetting
	{
		public readonly int From,To;

		public PatchSetting(int from, int to)
		{
			From = from;
			To = to;
		}

		public override string ToString()
		{
			return "PATCH:" + From.ToString() + ":" + To.ToString();
		}

	}

	public class OutputHostPortEntry
	{
		public readonly string PortName;
		public readonly string DisplayName;

		public OutputHostPortEntry(XmlNode node)
		{
			PortName = node.Attributes["name"].Value;
			DisplayName = node.Attributes["displayName"].Value;
		}
	}

	public class OutputHostEntry
	{
		public readonly OutputHostPortEntry[] OutputHostPorts;
		public readonly PatchEntry[] HostPatches;

		public readonly string HostName;

		public OutputHostEntry(XmlNode node)
		{
			ArrayList al = new ArrayList();
			ArrayList patches = new ArrayList();
			HostName = node.Attributes["host"].Value;
			foreach(XmlNode child in node.ChildNodes)
			{
				if(child.Name == "OutputPort")
				{
					al.Add(new OutputHostPortEntry(child));
				}
				else if(child.Name == "Patch")
				{
					patches.Add(new PatchEntry(child));
				}
			}
			HostPatches = (PatchEntry[])patches.ToArray(typeof(PatchEntry));
			OutputHostPorts = (OutputHostPortEntry[])al.ToArray(typeof(OutputHostPortEntry));
		}
	}

	public class OutputMap
	{
		public readonly OutputHostEntry[] Hosts;

		public OutputMap(XmlNode node)
		{
			ArrayList al = new ArrayList();
			foreach(XmlNode child in node.ChildNodes)
			{
				if(child.Name == "Output")
				{
					al.Add(new OutputHostEntry(child));
				}
			}

			Hosts = (OutputHostEntry[])al.ToArray(typeof(OutputHostEntry));
		}

		public OutputHostEntry FindOutputHost(string host)
		{
			foreach(OutputHostEntry output in Hosts)
			{
				if(output.HostName.ToLower() == host)
				{
					return output;
				}
			}
			return null;
		}
	}

	public abstract class PortEntry
	{
		public readonly string Name;
		public readonly int PortNumber;

		public PortEntry(XmlNode node)
		{
			Name = node.Attributes["name"].Value;
			PortNumber = Int32.Parse(node.Attributes["port"].Value);
		}
	}

	public class InputPortEntry : PortEntry
	{

		public InputPortEntry(XmlNode node) : base(node)
		{

		}
	}

	public class OutputPortEntry : PortEntry
	{
		public OutputPortEntry(XmlNode node) : base(node)
		{

		}
	}

	public class PortMap
	{
		InputPortEntry[] InputPorts;
		OutputPortEntry[] OutputPorts;

		public PortMap(XmlNode node)
		{
			ArrayList inputs = new ArrayList();
			ArrayList outputs = new ArrayList();

			foreach(XmlNode child in node.ChildNodes)
			{
				if(child.Name == "InputPort")
				{
					inputs.Add(new InputPortEntry(child));
				}
				else if(child.Name == "OutputPort")
				{
					outputs.Add(new OutputPortEntry(child));
				}
			}

			InputPorts = (InputPortEntry[])inputs.ToArray(typeof(InputPortEntry));
			OutputPorts = (OutputPortEntry[])outputs.ToArray(typeof(OutputPortEntry));
		}

		public InputPortEntry FindInputPort(string name)
		{
			foreach(InputPortEntry port in InputPorts)
			{
				if(port.Name == name)
				{
					return port;
				}
			}
			return null;
		}

		public OutputPortEntry FindOutputPort(string name)
		{
			foreach(OutputPortEntry port in OutputPorts)
			{
				if(port.Name == name)
				{
					return port;
				}
			}
			return null;
		}
	}

	public class PatchEntry
	{
		public readonly string Name;
		public readonly string Patch;

		public PatchEntry(XmlNode node)
		{
			Name = node.Attributes["name"].Value;
			Patch = node.Attributes["patch"].Value;
		}
	}

	public class PatchGroupEntry
	{
		public readonly string Name;
		public readonly PatchEntry[] Patches;

		public PatchGroupEntry(XmlNode node)
		{
			Name = node.Attributes["name"].Value;
			ArrayList al = new ArrayList();
			foreach(XmlNode child in node.ChildNodes)
			{
				if(child.Name == "Patch")
				{
					al.Add(new PatchEntry(child));
				}
			}
			Patches = (PatchEntry[])al.ToArray(typeof(PatchEntry));
		}
	}

	/// <summary>
	/// Summary description for PatchSettings.
	/// </summary>
	public class PatchSettings
	{
		public readonly string PatchServerHost;
		public readonly int PatchServerPort;
		public readonly PatchGroupEntry[] PatchGroups;
		public readonly PortMap PortMap;
		public readonly OutputMap OutputMap;


		public PatchSettings(string patchSettings)
		{
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.Load(patchSettings);
			ArrayList patchGroups = new ArrayList();
			
			foreach(XmlNode node in xmlDoc.DocumentElement.ChildNodes)
			{
				switch(node.Name)
				{
					case "PatchServer":
						PatchServerHost = node.Attributes["host"].Value;
						PatchServerPort = Int32.Parse(node.Attributes["port"].Value);
						break;
					case "PatchGroup":
						patchGroups.Add(new PatchGroupEntry(node));
						break;
					case "PortMap":
						PortMap = new PortMap(node);
						break;
					case "OutputMap":
						OutputMap = new OutputMap(node);
						break;
				}
			}

			PatchGroups = (PatchGroupEntry[])patchGroups.ToArray(typeof(PatchGroupEntry));
		}		
	}
}
