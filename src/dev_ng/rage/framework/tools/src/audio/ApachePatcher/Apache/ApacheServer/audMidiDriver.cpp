#include "StdAfx.h"
#include ".\audmididriver.h"
#using <mscorlib.dll>

#include <Windows.h>
#include <Mmsystem.h>

#pragma comment(lib, "winmm.lib")

HMIDIOUT audMidiDriver::sm_DeviceHandle;

audMidiDriver::audMidiDriver(void)
{
}

audMidiDriver::~audMidiDriver(void)
{
}

bool audMidiDriver::InitClass()
{
	//UINT deviceId;
	//MIDIOUTCAPS caps;

	//for(deviceId = 0; deviceId < midiOutGetNumDevs(); deviceId++)
	//{
	//	MMRESULT res = midiOutGetDevCaps((UINT_PTR)deviceId, &caps, sizeof(caps));
	//	if(res != MMSYSERR_NOERROR)
	//	{
	//		return false;
	//	}
	//}

	if(midiOutOpen(&sm_DeviceHandle, MIDI_MAPPER,(DWORD_PTR)&MidiOutProc, NULL, CALLBACK_FUNCTION) != MMSYSERR_NOERROR)
	{
		return false;
	}

	return true;
}

void audMidiDriver::ShutdownClass()
{
	midiOutClose(sm_DeviceHandle);
}

bool audMidiDriver::OutputBytes(unsigned char *buf, unsigned long bufSize)
{


	unsigned char *headerBuf = (unsigned char*)GlobalAlloc(GPTR, sizeof(MIDIHDR));
	MIDIHDR *hdr = (MIDIHDR*)headerBuf;
	MMRESULT res;

	unsigned char *globalBuf = (unsigned char*)GlobalAlloc(GMEM_FIXED,bufSize);
	memcpy(globalBuf, buf, bufSize);
	hdr->lpData = (LPSTR)globalBuf;
	hdr->dwBufferLength = bufSize;
	hdr->dwFlags = 0;

	if((res=midiOutPrepareHeader(sm_DeviceHandle, hdr, sizeof(MIDIHDR) != MMSYSERR_NOERROR)))
	{
		return false;
	}
	if((res=midiOutLongMsg(sm_DeviceHandle, hdr, sizeof(MIDIHDR)) != MMSYSERR_NOERROR))
	{
		return false;
	}

	if((res=midiOutUnprepareHeader(sm_DeviceHandle, hdr, sizeof(MIDIHDR)) != MMSYSERR_NOERROR))
	{
		return false;
	}

	return true;
}


void WINAPI audMidiDriver::MidiOutProc(HMIDIOUT MidiOut, UINT Msg,
                                         DWORD Instance, DWORD Param1, 
                                         DWORD Param2)
{

}