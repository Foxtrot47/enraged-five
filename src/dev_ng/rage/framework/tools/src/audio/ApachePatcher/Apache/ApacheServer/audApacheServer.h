#pragma once

__gc class audApacheServer
{
public:
	audApacheServer(void);
	~audApacheServer(void);

	void OnStart();
	void OnStop();

private:

	static void SetPatch(int from, int to);
	static void SendRemoteCommand(int command);

	System::Threading::Thread *m_Thread;
	static void ThreadProc();

	static bool sm_ShouldStop;

	static int sm_PatchTable __nogc[12];
	static Interop::MIDIOXLib::MoxScript *sm_MidiOx;
};
