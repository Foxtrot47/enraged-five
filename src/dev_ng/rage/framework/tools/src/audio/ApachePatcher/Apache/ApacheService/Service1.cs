using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Net;
using System.Net.Sockets;

namespace ApacheService
{
	public class ApachePatchService : System.ServiceProcess.ServiceBase
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		private TcpListener m_Listener;
		
		// this array stores the current patch config
		private byte[] m_OutputPatches;

		
		MidiInterop.MidiDeviceHandle m_MidiHandle;
		

		public ApachePatchService()
		{
			// This call is required by the Windows.Forms Component Designer.
			InitializeComponent();


			m_OutputPatches = new byte[12];
			for(byte i = 0 ; i < 12; i++)
			{
				m_OutputPatches[i] = i;
			}

			// initialise MIDI
			
			m_MidiHandle = MidiInterop.OpenMidiOut();
		}

		// The main entry point for the process
		static void Main()
		{
			System.ServiceProcess.ServiceBase[] ServicesToRun;
	
			// More than one user Service may run within the same process. To add
			// another service to this process, change the following line to
			// create a second service object. For example,
			//
			//   ServicesToRun = new System.ServiceProcess.ServiceBase[] {new Service1(), new MySecondUserService()};
			//
			ServicesToRun = new System.ServiceProcess.ServiceBase[] { new ApachePatchService() };

			System.ServiceProcess.ServiceBase.Run(ServicesToRun);
		}

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			this.ServiceName = "ApachePatchService";
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		/// <summary>
		/// Set things in motion so your service can do its work.
		/// </summary>
		protected override void OnStart(string[] args)
		{
			m_Listener = new TcpListener(System.Net.IPAddress.Any, 22050);
		}

		private byte[] GetCommandBytes(byte command)
		{
			byte[] buf = new byte[7];
			buf[0] = 0xf0;
			buf[1] = 0x00;
			buf[2] = 0x01;
			buf[3] = 0x40;
			buf[4] = 0x00;
			buf[5] = command;
			buf[6] = 0xf7;
			return buf;
		}

		private byte[][] GetPatchCommands(byte from, byte to)
		{
			const byte PatchCommand = 0x43,/* StatusCommand = 0x44,*/ InputBaseCommand = 0x20, OutputBaseCommand = 0x10;
			// PATCH
			// INPUT<x>
			// OUTPUT<x>
			// maybe then need status? STATUS
			byte[][] bufList = new byte[4][];

			bufList[0] = GetCommandBytes(PatchCommand);
			bufList[1] = GetCommandBytes((byte)(InputBaseCommand + (from - 1)));
			bufList[2] = GetCommandBytes((byte)(OutputBaseCommand + (to - 1)));
		//	bufList[3] = GetCommandBytes(StatusCommand);
			return bufList;
		}

		// PURPOSE
		// initialises all output patches to their corresponding input patch
		// eg 1->1, 2->2 etc
		private byte[] GetInitPatchCommand()
		{
			return new byte[] {0xf0,
								  0x00,0x01,0x40,
								  0x00,0x02,0x31,
			0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,
			0xf7};

		}
 
		/// <summary>
		/// Stop this service.
		/// </summary>
		protected override void OnStop()
		{
			// TODO: Add code here to perform any tear-down necessary to stop your service.
		}
	}
}
