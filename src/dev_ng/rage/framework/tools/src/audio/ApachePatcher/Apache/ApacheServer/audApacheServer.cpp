#include "StdAfx.h"

#include "audapacheserver.h"
#include "audmididriver.h"


#using <mscorlib.dll>

using namespace Interop::MIDIOXLib;
using namespace System;
using namespace System::Threading;
using namespace System::Net;
using namespace System::Net::Sockets;

audApacheServer::audApacheServer(void)
{
	for(int i = 0 ; i < 12; i++)
	{
		sm_PatchTable[i] = i;
	}
}

audApacheServer::~audApacheServer(void)
{
}

void audApacheServer::OnStart()
{
	m_Thread = new Thread(new ThreadStart(this, &audApacheServer::ThreadProc));
	sm_ShouldStop = false;
	m_Thread->Start();
}

void audApacheServer::OnStop()
{
	sm_ShouldStop = true;
	m_Thread->Join();
}
//
//byte *GetCommandBytes(byte command)
//{
//	byte *buf = new byte[7];
//	buf[0] = 0xf0;
//	buf[1] = 0x00;
//	buf[2] = 0x01;
//	buf[3] = 0x40;
//	buf[4] = 0x00;
//	buf[5] = command;
//	buf[6] = 0xf7;
//	return buf;
//}
//
//ArrayList GetPatchCommands(byte from, byte to)
//{
//	const byte PatchCommand = 0x43,/* StatusCommand = 0x44,*/ InputBaseCommand = 0x20, OutputBaseCommand = 0x10;
//	// PATCH
//	// INPUT<x>
//	// OUTPUT<x>
//	ArrayList *al = new ArrayList();
//
//
//	al->Add(__box(GetCommandBytes(PatchCommand)));
//	/*bufList[1] = GetCommandBytes((byte)(InputBaseCommand + (from - 1)));
//	bufList[2] = GetCommandBytes((byte)(OutputBaseCommand + (to - 1)));*/
////	bufList[3] = GetCommandBytes(StatusCommand);
//	return al;
//}

void audApacheServer::SetPatch(int from, int to)
{
	int fromInternal = from - 1;
	int toInternal = to - 1;

	System::Diagnostics::Debug::WriteLine(String::Format("Request patch {0} to {1}", __box(from), __box(to)));
	if(sm_PatchTable[toInternal] == fromInternal)
	{
		System::Diagnostics::Debug::WriteLine("already patched ...");
		return;
	}

	// patch
	SendRemoteCommand(0x43);
	// input 
	SendRemoteCommand(0x20 + fromInternal);
	// output
	SendRemoteCommand(0x10 + toInternal);

	sm_PatchTable[toInternal] = fromInternal;
}

void audApacheServer::SendRemoteCommand(int command)
{
	sm_MidiOx->SendSysExString(System::String::Concat("F0 00 01 40 00 ",__box(command)->ToString("X")," F7"));
}

void audApacheServer::ThreadProc()
{

	sm_MidiOx = new MoxScriptClass();

	// init apache with default patches (1->1,2->2 etc)
	// recall
	sm_MidiOx->SendSysExString("F0 00 01 40 00 41 F7");
	// input 1
	sm_MidiOx->SendSysExString("F0 00 01 40 00 20 F7");

	UdpClient *client = new UdpClient(new IPEndPoint(IPAddress::Any, 22050));
	

	while(!sm_ShouldStop)
	{
		try
		{
			IPEndPoint *endPoint;
			String *str = System::Text::Encoding::ASCII->GetString(client->Receive(&endPoint));
			System::Diagnostics::Debug::WriteLine(str);
			String *delims = ":";
			String *params __gc[] = str->Split(delims->ToCharArray());
			if(params->Length == 3)
			{
				if(params[0]->Equals("PATCH"))
				{
					int from = Convert::ToInt32(params[1]);
					int to = Convert::ToInt32(params[2]);
					SetPatch(from, to);
				}
			}
			else if(params[0]->Trim()->Equals("SHUTDOWN"))
			{
				sm_ShouldStop = true;
			}
		}
		catch(Exception *ex)
		{
			System::Diagnostics::Debug::WriteLine(ex->ToString());
		}
	}

	
}