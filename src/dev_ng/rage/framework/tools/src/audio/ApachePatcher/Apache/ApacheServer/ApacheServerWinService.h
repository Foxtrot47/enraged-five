#pragma once

using namespace System;
using namespace System::Collections;
using namespace System::ServiceProcess;
using namespace System::ComponentModel;

#include "audApacheServer.h"

namespace ApacheServer
{
	/// <summary> 
	/// Summary for ApacheServerWinService
	/// </summary>
	///
	/// WARNING: If you change the name of this class, you will need to change the 
	///          'Resource File Name' property for the managed resource compiler tool 
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	public __gc class ApacheServerWinService : public System::ServiceProcess::ServiceBase 
	{
	public:
		ApacheServerWinService()
		{
			InitializeComponent();    
		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		void Dispose(bool disposing)
		{
			if (disposing && components)
			{
				components->Dispose();
			}
			__super::Dispose(disposing);
		}
		
	protected:
		/// <summary>
		/// Set things in motion so your service can do its work.
		/// </summary>
		void OnStart(String* args[])
		{
			m_Server = new audApacheServer();
			m_Server->OnStart();
		}
		
		/// <summary>
		/// Stop this service.
		/// </summary>
		void OnStop()
		{     
			m_Server->OnStop();
		}
    		
	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container *components;

		audApacheServer *m_Server;
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>	
		void InitializeComponent(void)
		{
			this->components = new System::ComponentModel::Container();
			this->CanStop = true;
			this->CanPauseAndContinue = true;
			this->AutoLog = true;
			this->ServiceName = S"ApacheServerWinService";
		}		
	};
}