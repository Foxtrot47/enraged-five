1. Extract sample_drawable.zip to c:\soft\rage, preserving folders

2. Extract sample_drawable_assets.zip to t:\, preserving folders

3. to relink the exe, go to c:\soft\rage\ and run

	ppu-lv2-gcc -@top\sample_drawable_link_resp.txt
	make_fself sample_drawable.elf sample_drawable.self

4. run it with the command line:

	-file t:/rage/assets/rage_male/rage_male.type -many=10 -perobjectlighting -frametime

5. to enable gcm hud, pass -gcmhud on the command line

6. to control the size of the test case, vary the number after the -many object
