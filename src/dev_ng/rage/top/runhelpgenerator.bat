
setlocal

if "%1" == "-xge" (
set XGSUB=xgsubmit.exe /group=helpbuild /command
set XGWAIT=xgwait.exe /group=helpbuild
) else (
set XGSUB=
set XGWAIT=
)

%XGSUB% HelpGenerator.exe rage_default_settings.xml -rebuild
%XGSUB% HelpGenerator.exe rage_framework_settings.xml -rebuild
%XGSUB% HelpGenerator.exe rage_helpgeneratorui_settings.xml -rebuild
%XGSUB% HelpGenerator.exe rage_memorytracker_settings.xml -rebuild
%XGSUB% HelpGenerator.exe rage_script_settings.xml -rebuild
%XGSUB% HelpGenerator.exe rage_rag_settings.xml -rebuild
%XGSUB% HelpGenerator.exe rage_rexMBRage1_settings.xml -rebuild
%XGSUB% HelpGenerator.exe rage_rexMBRage2_settings.xml -rebuild
%XGSUB% HelpGenerator.exe rage_cutscene_settings.xml -rebuild
%XGWAIT%
