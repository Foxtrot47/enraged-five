// create the top-level samples object

samples = new SectionContainer();

// create each section object below

// ******************************************************************
// ******************************************************************
// ******************************************************************
// ******************************************************************
// ******************************************************************
// ******************************************************************
// ******************************************************************
// ******************************************************************
// ******************************************************************

// ******************************************************************
// PHYSICS section
// id, name
section_PHYSICS = new Section("PHYSICS", "Physics");
samples.appendSection(section_PHYSICS);
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_PHYSICS.appendSample(new Sample(
	"viewfrag",
	"Fragment type viewer",
	"..\\suite\\tools\\ui\\viewfrag\\viewfrag_2005.sln",
	"viewfrag\\viewfrag_2005.sln",
	"viewfrag_viewfrag.jpg",
	"viewfrag_viewfrag.txt",
	"suite\\tools\\ui\\viewfrag viewfrag -file=$/fragments/piranha/entity.type",
	"suite\\tools\\ui\\viewfrag viewfrag /xenon -file=$/fragments/piranha/entity.type -rag",
	"suite\\tools\\ui\\viewfrag viewfrag /ps3 -file=$/fragments/piranha/entity.type -rag",
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_PHYSICS.appendSample(new Sample(
	"viewfragnm",
	"NaturalMotion character working through the fragment system interface",
	"..\\naturalmotion\\tools\\viewfragnm\\viewfragnm_2005.sln",
	"viewfragnm\\viewfragnm_2005.sln",
	"viewfragnm_viewfraganim.jpg",
	"viewfragnm_viewfragnm.txt",
	"naturalmotion\\tools\\viewfragnm viewfragnm",
	"naturalmotion\\tools\\viewfragnm viewfragnm /xenon -rag",
	"naturalmotion\\tools\\viewfragnm viewfragnm /ps3 -rag",
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_PHYSICS.appendSample(new Sample(
	"sample_articulated",
	"Articulated body physics examples.",
	"..\\base\\samples\\sample_physics\\sample_articulated_2005.sln",
	"sample_physics\\sample_articulated_2005.sln",
	"sample_physics_sample_articulated.jpg",
	"sample_physics_sample_articulated.txt",
	"base\\samples\\sample_physics sample_articulated",
	"base\\samples\\sample_physics sample_articulated /xenon -rag",
	"base\\samples\\sample_physics sample_articulated /ps3 -rag",
	null));	
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_PHYSICS.appendSample(new Sample(
	"sample_articonstraint",
	"Articulated constraint body physics sample.",
	"..\\base\\samples\\sample_physics\\sample_articonstraint_2005.sln",
	"sample_physics\\sample_articonstraint_2005.sln",
	"sample_physics_sample_articonstraint.jpg",
	"sample_physics_sample_articonstraint.txt",
	"base\\samples\\sample_physics sample_articonstraint",
	"base\\samples\\sample_physics sample_articonstraint /xenon -rag",
	"base\\samples\\sample_physics sample_articonstraint /ps3 -rag",
	null));	
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_PHYSICS.appendSample(new Sample(
	"sample_fragcar",
	"Fragmenting vehicle physics.",
	"..\\suite\\samples\\sample_vehicle\\sample_fragcar_2005.sln",
	"sample_vehicle\\sample_fragcar_2005.sln",
	"sample_vehicle_sample_fragcar.jpg",
	"sample_vehicle_sample_fragcar.txt",
	"suite\\samples\\sample_vehicle sample_fragcar",
	"suite\\samples\\sample_vehicle sample_fragcar /xenon -rag",
	"suite\\samples\\sample_vehicle sample_fragcar /ps3 -rag",
	null));	
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_PHYSICS.appendSample(new Sample(
	"sample_basic",
	"Shows the bare minimum steps required to load and instantiate a fragment type.",
	"..\\suite\\samples\\sample_fragment\\sample_basic_2005.sln",
	"sample_fragment\\sample_basic_2005.sln",
	"sample_fragment_sample_basic.jpg",
	"sample_fragment_sample_basic.txt",
	"suite\\samples\\sample_fragment sample_basic",
	"suite\\samples\\sample_fragment sample_basic /xenon -rag",
	"suite\\samples\\sample_fragment sample_basic /ps3 -rag",
	null));	
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_PHYSICS.appendSample(new Sample(
	"sample_smallcache",
	"Demonstrates how the fragment system behaves with a very small fragment.",
	"..\\suite\\samples\\sample_fragment\\sample_smallcache_2005.sln",
	"sample_fragment\\sample_smallcache_2005.sln",
	"sample_fragment_sample_smallcache.jpg",
	"sample_trigger_sample_smallcache.txt",
	"suite\\samples\\sample_fragment sample_smallcache",
	"suite\\samples\\sample_fragment sample_smallcache /xenon -rag",
	"suite\\samples\\sample_fragment sample_smallcache /ps3 -rag",
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_PHYSICS.appendSample(new Sample(
	"sample_basic",
	"Demonstrates how to create a basic physics level with interacting objects.",
	"..\\base\\samples\\sample_physics\\sample_basic_2005.sln",
	"sample_physics\\sample_basic_2005.sln",
	"sample_physics_sample_basic.jpg",
	"sample_physics_sample_basic.txt",
	"base\\samples\\sample_physics sample_basic",
	"base\\samples\\sample_physics sample_basic /xenon -rag",
	"base\\samples\\sample_physics sample_basic /ps3 -rag",
	null));	
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_PHYSICS.appendSample(new Sample(
	"sample_constraint",
	"Demo and test constraint forces on physical objects.",
	"..\\base\\samples\\sample_physics\\sample_constraint_2005.sln",
	"sample_physics\\sample_constraint_2005.sln",
	"sample_physics_sample_constraint.jpg",
	"sample_physics_sample_constraint.txt",
	"base\\samples\\sample_physics sample_constraint",
	"base\\samples\\sample_physics sample_constraint /xenon -rag",
	"base\\samples\\sample_physics sample_constraint /ps3 -rag",
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_PHYSICS.appendSample(new Sample(
	"sample_contacts",
	"Demonstrates stacks of physical objects with contact forces.",
	"..\\base\\samples\\sample_physics\\sample_contacts_2005.sln",
	"sample_physics\\sample_contacts_2005.sln",
	"sample_physics_sample_contacts.jpg",
	"sample_physics_sample_contacts.txt",
	"base\\samples\\sample_physics sample_contacts",
	"base\\samples\\sample_physics sample_contacts /xenon -rag",
	"base\\samples\\sample_physics sample_contacts /ps3 -rag",
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_PHYSICS.appendSample(new Sample(
	"sample_explosion",
	"Explosion demo. Press <u>o</u> for an explosion at the origin or <u>SPACE</u> for a randomly placed explosion.",
	"..\\base\\samples\\sample_physics\\sample_explosion_2005.sln",
	"sample_physics\\sample_explosion_2005.sln",
	"sample_physics_sample_explosion.jpg",
	"sample_physics_sample_explosion.txt",
	"base\\samples\\sample_physics sample_explosion",
	"base\\samples\\sample_physics sample_explosion /xenon -rag",
	"base\\samples\\sample_physics sample_explosion /ps3 -rag",
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_PHYSICS.appendSample(new Sample(
	"sample_rope",
	"Demonstrates rope physics in a variety of situations.",
	"..\\suite\\samples\\sample_rope\\sample_rope_2005.sln",
	"sample_rope\\sample_rope_2005.sln",
	"sample_rope_sample_rope.jpg",
	"sample_physics_sample_rope.txt",
	"suite\\samples\\sample_rope sample_rope",
	"suite\\samples\\sample_rope sample_rope /xenon -rag",
	"suite\\samples\\sample_rope sample_rope /ps3 -rag",
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_PHYSICS.appendSample(new Sample(
	"sample_water",
	"Water demo.",
	"..\\base\\samples\\sample_physics\\sample_water_2005.sln",
	"sample_physics\\sample_water_2005.sln",
	"sample_physics_sample_water.jpg",
	"sample_physics_sample_water.txt",
	"base\\samples\\sample_physics sample_water",
	"base\\samples\\sample_physics sample_water /xenon -rag",
	"base\\samples\\sample_physics sample_water /ps3 -rag",
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_PHYSICS.appendSample(new Sample(
	"sample_conveyor",
	"Demonstrates animated kinematic (keyframed) bodies interacting with dynamic bodies as well as objects having a surface velocity which is controlled by application logic rather than physics simulation.  The later is done with the phInst::GetExternallyControlledxxxVelocity() functions.",
	"..\\base\\samples\\sample_physics\\sample_conveyor_2005.sln",
	"sample_physics\\sample_conveyor_2005.sln",
	"sample_physics_sample_conveyor.jpg",
	"output.txt",
	"base\\samples\\sample_physics sample_conveyor",
	"base\\samples\\sample_physics sample_conveyor /xenon -rag",
	"base\\samples\\sample_physics sample_conveyor /ps3 -rag",
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_PHYSICS.appendSample(new Sample(
	"sample_cloth",
	"Demonstrates physically simulated cloth.",
	"..\\suite\\samples\\sample_cloth\\sample_cloth_2005.sln",
	"sample_cloth\\sample_cloth_2005.sln",
	"sample_cloth_sample_cloth.jpg",
	"output.txt",
	"suite\\samples\\sample_cloth sample_cloth",
	"suite\\samples\\sample_cloth sample_cloth /xenon -rag",
	"suite\\samples\\sample_cloth sample_cloth /ps3 -rag",
	null));	

		
// ******************************************************************
// AI section
// id, name
section_AI = new Section("AI", "AI");
samples.appendSection(section_AI);
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_AI.appendSample(new Sample(
	"sample_ai",
	"Tester for the ainav module.  Tests search portion of nav code.",
	"..\\suite\\samples\\sample_ai\\sample_navquery_2005.sln",
	"sample_ai\\sample_ai sample_navquery_2005.sln",
	"sample_ai_sample_navquery.JPG",
	"sample_ai_sample_ainavquery.txt",
	"suite\\samples\\sample_ai sample_navquery",
	"suite\\samples\\sample_ai sample_navquery /xenon",
	"suite\\samples\\sample_ai sample_navquery /ps3",
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_AI.appendSample(new Sample(
	"sample_coverfinder",
	"Shows how to use the Combat Cover Finder class",
	"..\\suite\\samples\\sample_aicover\\sample_coverfinder_2005.sln",
	"sample_aicover\\sample_coverfinder_2005.sln",
	"sample_aicover_sample_coverfinder.JPG",
	"sample_aicover_samplecoverfinder.txt",
	"suite\\samples\\sample_aicover sample_coverfinder",
	"suite\\samples\\sample_aicover sample_coverfinder /xenon",
	"suite\\samples\\sample_aicover sample_coverfinder /ps3",
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_AI.appendSample(new Sample(
	"sample_covergrid",
	"Shows how to load data in the Combat Cover Grid and Grid Instance classes",
	"..\\suite\\samples\\sample_aicover\\sample_covergrid_2005.sln",
	"sample_aicover\\sample_covergrid_2005.sln",
	"sample_aicover_sample_covergrid.JPG",
	"sample_aicover_samplecovergrid.txt",
	"suite\\samples\\sample_aicover sample_covergrid",
	"suite\\samples\\sample_aicover sample_covergrid /xenon",
	"suite\\samples\\sample_aicover sample_covergrid /ps3",
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_AI.appendSample(new Sample(
	"buildmesh",
	"Builds a navigation mesh for AI",
	"..\\suite\\tools\\cli\\ainavtools\\buildmesh_2005.sln",
	"ainavtools\\buildmesh_2005.sln",
	"ainavtools_buildmesh.jpg",
	"ainavtools_buildmesh.txt",
	"suite\\tools\\cli\\ainavtools buildmesh",
	"suite\\tools\\cli\\ainavtools buildmesh /xenon",
	"suite\\tools\\cli\\ainavtools buildmesh /ps3",
	null));
	
// ******************************************************************
// GRAPHICS section
// id, name
section_GRAPHICS = new Section("GRAPHICS", "Graphics");
samples.appendSection(section_GRAPHICS);
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_GRAPHICS.appendSample(new Sample(
	"sample_immediatemode",
	"Example of drawing in immediate mode.",
	"..\\base\\samples\\sample_grcore\\sample_immediatemode_2005.sln",
	"sample_grcore\\sample_immediatemode_2005.sln",
	"sample_grcore_sample_immediatemode.jpg",
	"sample_grcore_sample_immediatemode.txt",
	"base\\samples\\sample_grcore sample_immediatemode",
	"base\\samples\\sample_grcore sample_immediatemode /xenon",
	"base\\samples\\sample_grcore sample_immediatemode /ps3",
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_GRAPHICS.appendSample(new Sample(
	"sample_drawable",
	"Loads a rmcDrawable from a .type file and draws it to the screen. Also contains code demonstrating resourcing.",
	"..\\base\\samples\\sample_rmcore\\sample_drawable_2005.sln",
	"sample_rmcore\\sample_drawable_2005.sln",
	"sample_rmcore_sample_drawable.jpg",
	"sample_rmcore_sample_drawable.txt",
	"base\\samples\\sample_rmcore sample_drawable",
	"base\\samples\\sample_rmcore sample_drawable /xenon",
	"base\\samples\\sample_rmcore sample_drawable /ps3",
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_GRAPHICS.appendSample(new Sample(
	"sample_texdict",
	"Loading a texture out of a texture dictionary.",
	"..\\base\\samples\\sample_grcore\\sample_texdict_2005.sln",
	"sample_grcore\\sample_texdict_2005.sln",
	"sample_grcore_sample_texdict.jpg",
	"sample_grcore_sample_texdict.txt",
	"base\\samples\\sample_grcore sample_texdict",
	"base\\samples\\sample_grcore sample_texdict /xenon",
	"base\\samples\\sample_grcore sample_texdict /ps3",
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_GRAPHICS.appendSample(new Sample(
	"sample_texture",
	"Texturing example.",
	"..\\base\\samples\\sample_grcore\\sample_texture_2005.sln",
	"sample_grcore\\sample_texture_2005.sln",
	"sample_grcore_sample_texture.jpg",
	"sample_grcore_sample_texture.txt",
	"base\\samples\\sample_grcore sample_texture",
	"base\\samples\\sample_grcore sample_texture /xenon",
	"base\\samples\\sample_grcore sample_texture /ps3",
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_GRAPHICS.appendSample(new Sample(
	"sample_shaderperf",
	"Demonstrates use of command buffers on Xenon to massively reduce CPU draw time.",
	"..\\base\\samples\\sample_rmcore\\sample_shaderperf_2005.sln",
	"sample_rmcore\\sample_shaderperf_2005.sln",
	"sample_rmcore_sample_shaderperf.jpg",
	"sample_rmcore_sample_shaderperf.txt",
	"base\\samples\\sample_rmcore sample_shaderperf",
	"base\\samples\\sample_rmcore sample_shaderperf /xenon",
	"base\\samples\\sample_rmcore sample_shaderperf /ps3",
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_GRAPHICS.appendSample(new Sample(
	"sample_rendertarget",
	"How to create and use a rendertarget, also how to use a rendertarget as a texture.",
	"..\\base\\samples\\sample_rmcore\\sample_rendertarget_2005.sln",
	"sample_rmcore\\sample_rendertarget_2005.sln",
	"sample_rmcore_sample_rendertarget.jpg",
	"sample_rmcore_sample_rendertarget.txt",
	"base\\samples\\sample_rmcore sample_rendertarget",
	"base\\samples\\sample_rmcore sample_rendertarget /xenon -rag",
	"base\\samples\\sample_rmcore sample_rendertarget /ps3 -rag",
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_GRAPHICS.appendSample(new Sample(
	"sample_shadervariables",
	"Demonstrates blending between 2 textures using a shader and variables connected to the game engine.",
	"..\\base\\samples\\sample_rmcore\\sample_shadervariables_2005.sln",
	"sample_rmcore\\sample_shadervariables_2005.sln",
	"sample_rmcore_sample_shadervariables.jpg",
	"sample_rmcore_sample_shadervariable.txt",
	"base\\samples\\sample_rmcore sample_shadervariables",
	"base\\samples\\sample_rmcore sample_shadervariables /xenon -rag",
	"base\\samples\\sample_rmcore sample_shadervariables /ps3 -rag",
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_GRAPHICS.appendSample(new Sample(
	"sample_occlusion",
	"Demonstrates occlusion between randomly placed cubes.",
	"..\\base\\samples\\sample_rmocclude\\sample_occlusion_2005.sln",
	"sample_rmocclude\\sample_occlusion_2005.sln",
	"sample_rmocclude_sample_occlusion.jpg",
	"sample_rmocclude_sample_occlusion.txt",
	"base\\samples\\sample_rmocclude sample_occlusion",
	"base\\samples\\sample_rmocclude sample_occlusion /xenon",
	"base\\samples\\sample_rmocclude sample_occlusion /ps3",
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_GRAPHICS.appendSample(new Sample(
	"rorc",
	"Rorc is a tool used to generate offline resource.",
	"..\\base\\tools\\rorc\\rorc_2005.sln",
	"rorc\\rorc_2005.sln",
	null,
	null,
	null,
	null,
	null,
	null));
	
// ******************************************************************
// PARSER section
// id, name
section_PARSER = new Section("PARSER", "Parser");
samples.appendSection(section_PARSER);
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_PARSER.appendSample(new Sample(
	"sample_numericarray",
	"Shows how to use the parser to save/load arrays of numerical data.",
	"..\\base\\samples\\sample_parser\\sample_numericarray_2005.sln",
	"sample_parser\\sample_numericarray_2005.sln",
	null,
	"numericarrays.xml",
	"base\\samples\\sample_parser sample_numericarray",
	null,
	null,
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_PARSER.appendSample(new Sample(
	"sample_pointerarray",
	"Shows how to load pointer arrays.",
	"..\\base\\samples\\sample_parser\\sample_pointerarray_2005.sln",
	"sample_parser\\sample_pointerarray_2005.sln",
	null,
	"pointerout.xml",
	"base\\samples\\sample_parser sample_pointerarray",
	null,
	null,
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_PARSER.appendSample(new Sample(
	"sample_prealloc",
	"Shows how to use the parser with clases and derived classes.",
	"..\\base\\samples\\sample_parser\\sample_prealloc_2005.sln",
	"sample_parser\\sample_prealloc_2005.sln",
	null,
	"sample_parser_sample_prealloc.txt",
	"base\\samples\\sample_parser sample_prealloc",
	null,
	null,
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_PARSER.appendSample(new Sample(
	"sample_pull",
	"Demonstrates pull style of reading data from a file.",
	"..\\base\\samples\\sample_parser\\sample_pull_2005.sln",
	"sample_parser\\sample_pull_2005.sln",
	null,
	"sample_parser_sample_pull.txt",
	"base\\samples\\sample_parser sample_pull",
	null,
	null,
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_PARSER.appendSample(new Sample(
	"sample_push",
	"Demonstrates push style of reading data from a file.",
	"..\\base\\samples\\sample_parser\\sample_push_2005.sln",
	"sample_parser\\sample_push_2005.sln",
	null,
	"sample_parser_sample_push.txt",
	"base\\samples\\sample_parser sample_push",
	null,
	null,
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_PARSER.appendSample(new Sample(
	"sample_rtstruct",
	"Shows how to create parsable structures at run-time (memory addresses).",
	"..\\base\\samples\\sample_parser\\sample_rtstruct_2005.sln",
	"sample_parser\\sample_rtstruct_2005.sln",
	null,
	"rtstructout.xml",
	"base\\samples\\sample_parser sample_rtstruct",
	null,
	null,
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_PARSER.appendSample(new Sample(
	"sample_struct",
	"Shows how to read/write data structures to/from a file.",
	"..\\base\\samples\\sample_parser\\sample_struct_2005.sln",
	"sample_parser\\sample_struct_2005.sln",
	null,
	"structout.xml",
	"base\\samples\\sample_parser sample_struct",
	null,
	null,
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_PARSER.appendSample(new Sample(
	"sample_tree",
	"Demonstrates tree style of reading data from a file, and how to access that data from the tree.",
	"..\\base\\samples\\sample_parser\\sample_tree_2005.sln",
	"sample_parser\\sample_tree_2005.sln",
	null,
	"sample_parser_sample_tree.txt",
	"base\\samples\\sample_parser sample_tree",
	null,
	null,
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_PARSER.appendSample(new Sample(
	"sample_widgets",
	"Shows how to use the parser to create widgetsStarts up a graphics window with nothing in it. Opening the <u>Parser Widgets</u> bank, modifying widgets and pressing Save will save any changes to T:\rage\assets\sample_parser\widgetdata.xml.",
	"..\\base\\samples\\sample_parser\\sample_widgets_2005.sln",
	"sample_parser\\sample_widgets_2005.sln",
	null,
	"sample_parser_sample_widgets.txt",
	"base\\samples\\sample_parser sample_widgets",
	null,
	null,
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_PARSER.appendSample(new Sample(
	"sample_write",
	"Shows how to write data to a file, similar to the pull method of reading.",
	"..\\base\\samples\\sample_parser\\sample_write_2005.sln",
	"sample_parser\\sample_write_2005.sln",
	null,
	"writedata.xml",
	"base\\samples\\sample_parser sample_write",
	null,
	null,
	null));




// ******************************************************************
// RAG section
// id, name
section_RAG = new Section("RAG", "RAG");
samples.appendSection(section_RAG);
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_RAG.appendSample(new Sample(
	"rag",
	"Tool for accessing and manipulating run-time game data through the Widget system.",
	"..\\base\\tools\\rag\\rag.sln",
	"rag\\rag.sln",
	null,
	null,
	null,
	null,
	null,
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_RAG.appendSample(new Sample(
	"ragEventEditor",
	"Tool for editing and exporting events.",
	"..\\base\\tools\\ragEventEditor\\ragEventEditor.sln",
	"ragEventEditor\\ragEventEditor.sln",
	null,
	null,
	null,
	null,
	null,
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_RAG.appendSample(new Sample(
	"sample_scr_generic",
	"Sample demonstrating a script running in the game engine. Used by the Script Editor sample below. Requires //rageassets/dev/rage/assets/sample_scr_generic/ and //rageassets/dev/rage/assets/tune/scripts/. Run Rag before launching Win32. Is supposed to show a rotating line segment, but is currently broken.",
	"..\\script\\samples\\sample_scr_grcore\\sample_scr_generic_2005.sln",
	"sample_scr_grcore\\sample_scr_generic_2005.sln",
	null,
	null,
	"script\\samples\\sample_scr_grcore sample_scr_generic -rag",
	null,
	null,
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_RAG.appendSample(new Sample(
	"ragScriptEditor",
	"Tool for editing and compiling scripts.  Running the editor from here will load a sample project (see sample_scr_grcore\\sample_scr_generic_2005.sln above) with instructions on how to compile and run a script (open demo.sc).  Required: 'sample_scr_grcore\\sample_scr_generic_2005.sln' as stated above, and an environment variable named RAGE_ASSET_DIR set to your //rageassets path, such as T:\\rage\\assets.",
	"..\\script\\tools\\ragScriptEditor2\\ragScriptEditor.sln",
	"ragScriptEditor2\\ragScriptEditor.sln",
	null,
	null,
	null,
	null,
	null,
	"tools\\base\\exes\\ScriptEditor ragScriptEditor.exe %RAGE_ASSET_DIR%\\sample_scr_generic\\sample_scr_generic.scproj"));

// ******************************************************************
// NETWORKING section
// id, name
section_NETWORKING = new Section("NETWORKING", "Networking");
samples.appendSection(section_NETWORKING);
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_NETWORKING.appendSample(new Sample(
	"sample_connect",
	"Demonstrates basic usage of the net library.",
	"..\\base\\samples\\sample_net\\sample_connect_2005.sln",
	"sample_net\\sample_connect_2005.sln",
	"sample_net_sample_connect.jpg",
	"sample_net_sample_connect.txt",
	"base\\samples\\sample_net sample_connect",
	null,
	null,
	null));	
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_NETWORKING.appendSample(new Sample(
	"sample_message",
	"Demonstrates usage of the network serialization system.",
	"..\\base\\samples\\sample_net\\sample_message_2005.sln",
	"sample_net\\sample_message_2005.sln",
	"rockstar_symbol.jpg",
	"output.txt",
	"base\\samples\\sample_net sample_message",
	null,
	null,
	null));	

// ******************************************************************
// MISC section
// id, name
section_MISC = new Section("MISC", "Miscellaneous");
samples.appendSection(section_MISC);
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_MISC.appendSample(new Sample(
	"sample_wilderness",
	"Using Sample Simple World as a framework, this sample demonstrates grass, water rendering, atmospheric scattering, time-of-day lighting, cammach, and dynamic shadows running in a game sized environment.  <a href=\"Using_the_Wilderness_Demo.htm\">Using the Wilderness Demo</a>",
	"..\\suite\\samples\\sample_wilderness\\sample_wilderness_2005.sln",
	"sample_wilderness\\sample_wilderness_2005.sln",
	"sample_wilderness_sample_wilderness.jpg",
	null,
	"suite\\samples\\sample_wilderness sample_wilderness @T:\\rageland\\assets\\scripts_data\\ragewood_commandLineRag.txt",
	"suite\\samples\\sample_wilderness sample_wilderness /xenon @T:\\rageland\\assets\\scripts_data\\ragewood_commandLineRag.txt",
    "suite\\samples\\sample_wilderness sample_wilderness /ps3 @T:\\rageland\\assets\\scripts_data\\ragewood_commandLineRag.txt",
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_MISC.appendSample(new Sample(
	"sample_skydome",
	"This demostrates a procedurally created skydome with day and night, multiple weather settings and blending between the weather settings.",
	"..\\suite\\samples\\sample_skydome\\sample_skydome_test_2005.sln",
	"sample_skydome\\sample_skydome_test_2005.sln",
	"sample_skydome_test.jpg",
	null,
	"suite\\samples\\sample_skydome sample_skydome_test",
	"suite\\samples\\sample_skydome sample_skydome_test /xenon -rag",
	"suite\\samples\\sample_skydome sample_skydome_test /ps3 -rag",
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_MISC.appendSample(new Sample(
	"sample_grass",
	"How to load grass from a xml file, generate it from textures or load from a resourced grass file. Also it shows how to add different static particle types and set their parameters.",
	"..\\suite\\samples\\sample_grass\\sample_grass_2005.sln",
	"sample_grass\\sample_grass_2005.sln",
	"sample_grass.jpg",
	null,
	"suite\\samples\\sample_grass sample_grass -zup -particlelist",
	"suite\\samples\\sample_grass sample_grass /xenon -rag -zup -particlelist",
	"suite\\samples\\sample_grass sample_grass /ps3 -zup -particlelist",
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_MISC.appendSample(new Sample(
	"sample_lighting",
	"An example use of the rage manylights system for rendering scenes with lots of lights with a forward renderer.",
	"..\\suite\\samples\\sample_lighting\\sample_lighting_2005.sln",
	"sample_lighting\\sample_lighting_2005.sln",
	"sample_lighting_sample_lighting.jpg",
	null,
	"suite\\samples\\sample_lighting sample_lighting",
	"suite\\samples\\sample_lighting sample_lighting /xenon -rag",
	null,
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_MISC.appendSample(new Sample(
	"sample_playswf",
	"Rage flash movie player - Allows for artists to test their flash movies separately before adding to the game.",
	"..\\base\\samples\\sample_flash\\sample_playswf_2005.sln",
	"sample_flash\\sample_playswf_2005.sln",
	"sample_flash_sample_playswf.jpg",
	null,
	"base\\samples\\sample_flash sample_playswf",
	"base\\samples\\sample_flash sample_playswf /xenon",
	"base\\samples\\sample_flash sample_playswf /ps3",
	null));
	
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_MISC.appendSample(new Sample(
	"sample_sfviewer",
	"Rage scaleform player.",
	"..\\base\\samples\\sample_scaleform\\sample_sfviewer_2005.sln",
	"sample_scaleform\\sample_sfviewer_2005.sln",
	"sample_scaleform_sample_sfviewer.jpg",
	null,
	"base\\samples\\sample_scaleform sample_sfviewer -file=$/sample_flash/demo.sfproj -rag",
	"base\\samples\\sample_scaleform sample_sfviewer /xenon -rag -file=$/sample_flash/demo.sfproj ",
	"base\\samples\\sample_scaleform sample_sfviewer /ps3 -rag -file=$/sample_flash/demo.sfproj ",
	null));
	
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_MISC.appendSample(new Sample(
	"sample_multidrawable",
	"The SimpleWorld viewer is intended to be the primary content preview tool for artists. This tool supports previewing a wide range of content including models, animations, fragments, particle systems, and the post processing pipeline.",
	"..\\suite\\samples\\sample_simpleworld\\sample_multidrawable_2005.sln",
	"sample_simpleworld\\sample_multidrawable_2005.sln",
	"sample_simpleworld_sample_multidrawable.jpg",
	null,
	"suite\\samples\\sample_simpleworld sample_multidrawable -rag",
	"suite\\samples\\sample_simpleworld sample_multidrawable /xenon -rag",
	"suite\\samples\\sample_simpleworld sample_multidrawable /ps3 -rag",
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_MISC.appendSample(new Sample(
	"rageScriptEditorWindow",
	"Easy registration of a command in Maya",
	"..\\base\\tools\\rageScriptEditorWindow\\rageScriptEditorWindow.sln",
	"rageScriptEditorWindow\\rageScriptEditorWindow.sln",
	null,
	null,
	null,
	null,
	null,
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_MISC.appendSample(new Sample(
	"sample_camclient",
	"Demonstrates how to use the cammach system for game/follow cameras",
	"..\\suite\\samples\\sample_camclient\\sample_basic_2005.sln",
	"sample_camclient\\sample_basic_2005.sln",
	"sample_camclient_sample_basic.JPG",
	"sample_camclient_sample_basic.txt",
	"suite\\samples\\sample_camclient sample_basic",
	null,
	null,
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_MISC.appendSample(new Sample(
	"sample_profiledraw",
	"Shows how to use the profile drawing library",
	"..\\base\\samples\\sample_profile\\sample_profiledraw_2005.sln",
	"sample_profile\\sample_profiledraw_2005.sln",
	"sample_profile_sample_profiledraw.JPG",
	"sample_profile_sample_profiledraw.txt",
	"base\\samples\\sample_profile sample_profiledraw",
	null,
	null,
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_MISC.appendSample(new Sample(
	"sample_testmanager",
	"Demonstrates point particle effects. Should see smiling faces.  Launch ParticleInterface.exe once sample is running.",
	"..\\suite\\samples\\sample_rmptfx\\sample_viewer_2005.sln",
	"sample_rmptfx\\sample_viewer_2005.sln",
	"sample_rmptfx_sample_viewer.JPG",
	"sample_rmptfx_sample_viewer.txt",
	"suite\\samples\\sample_rmptfx sample_viewer -demo",
	"suite\\samples\\sample_rmptfx sample_viewer /xenon -demo",
	"suite\\samples\\sample_rmptfx sample_viewer /ps3 -demo",
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_MISC.appendSample(new Sample(
	"sample_actor",
	"Builds on sample_basic by adding communication with actors.  The main visible change is that the colors of the spheres should change. One should go from yellow to green and the other from magneta to blue. What's going on is that as the moving sphere rotates it passes through a trigger volume that reduces it's \"health\" (shown by the color). Once its health hits 0 it should reduce the health of the other sphere.",
	"..\\suite\\samples\\sample_trigger\\sample_actor_2005.sln",
	"sample_trigger\\sample_actor_2005.sln",
	"sample_trigger_sample_actor.JPG",
	"sample_trigger_sample_actor.txt",
	"suite\\samples\\sample_trigger sample_actor",
	"suite\\samples\\sample_trigger sample_actor /xenon",
	"suite\\samples\\sample_trigger sample_actor /ps3",
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_MISC.appendSample(new Sample(
	"sample_basic",
	"Demonstrates how to connect conditions and actions with the trigger system. On Xenon Beta, if you press \"A\" on the controller it should print something to the console output.",
	"..\\suite\\samples\\sample_trigger\\sample_basic_2005.sln",
	"sample_trigger\\sample_basic_2005.sln",
	"sample_trigger_sample_basic.JPG",
	"sample_trigger_sample_basic.txt",
	null,
	null,
	null,
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_MISC.appendSample(new Sample(
	"sample_linked",
	"Demonstrate the creation and use of gizmos. Shows how rotation and translation gizmos can be linked together by pointing them at the same matrix.",
	"..\\base\\samples\\sample_gizmo\\sample_linked_2005.sln",
	"sample_gizmo\\sample_linked_2005.sln",
	"sample_gizmo_sample_linked.jpg",
	"sample_gizmo_sample_linked.txt",
	"base\\samples\\sample_gizmo sample_linked",
	"base\\samples\\sample_gizmo sample_linked /xenon",
	"base\\samples\\sample_gizmo sample_linked /ps3",
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_MISC.appendSample(new Sample(
	"testquantize",
	"Runs through a series of quantize tests automatically",
	"..\\base\\src\\vector\\testquantize_2005.sln",
	"vector\\testquantize_2005.sln",
	"vector_testquantize.JPG",
	"vector_testquantize.txt",
	"base\\src\\vector testquantize",
	null,
	null,
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_MISC.appendSample(new Sample(
	"sample_bintree",
	"Simple test sequence demonstrating and testing the atBinTree.",
	"..\\base\\samples\\sample_atl\\sample_bintree_2005.sln",
	"sample_atl\\sample_bintree_2005.sln",
	"vector_testquantize.JPG",
	"vector_testquantize.txt",
	"base\\samples\\sample_atl sample_bintree",
	null,
	null,
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_MISC.appendSample(new Sample(
	"sample_cinematic",
	"This is the Rage Cut Scene tester.",
	"..\\suite\\samples\\sample_cutscene\\sample_cinematic_2005.sln",
	"sample_cutscene\\sample_cinematic_2005.sln",
	"sample_cutscene_sample_cinematic.jpg",
	"sample_cutscene_sample_cinematic.txt",
	null,
	"suite\\samples\\sample_cutscene sample_cinematic /xenon -rag -cutfile t:/rage/assets/cuts/rdr2Test",
	"suite\\samples\\sample_cutscene sample_cinematic /ps3 -rag -cutfile t:/rage/assets/cuts/rdr2Test",
	null));

// ******************************************************************
// ANIMATION section
// id, name
section_ANIMATION = new Section("ANIMATION", "Animation");
samples.appendSection(section_ANIMATION);
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_ANIMATION.appendSample(new Sample(
	"sample_mt_creature",
	"Demonstrates creature, with bones, blend shapes and animated normal maps, playing through a motion tree.",
	"..\\suite\\samples\\sample_motiontree\\sample_mt_creature_2005.sln",
	"sample_motiontree\\sample_mt_creature_2005.sln",
	"sample_motiontree_sample_mt_creature.jpg",
	"sample_motiontree_sample_mt_creature.txt",
	"suite\\samples\\sample_motiontree sample_mt_creature -rag",
	"suite\\samples\\sample_motiontree sample_mt_creature /xenon -rag",
	"suite\\samples\\sample_motiontree sample_mt_creature /ps3 -rag",
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_ANIMATION.appendSample(new Sample(
	"sample_anim",
	"Load anim, pose a skeleton, and render a skinned model.",
	"..\\base\\samples\\sample_cranimation\\sample_anim_2005.sln",
	"sample_cranimation\\sample_anim_2005.sln",
	"sample_cranimation_sample_anim.jpg",
	"sample_cranimation_sample_anim.txt",
	"base\\samples\\sample_cranimation sample_anim",
	"base\\samples\\sample_cranimation sample_anim /xenon",
	"base\\samples\\sample_cranimation sample_anim /ps3",
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_ANIMATION.appendSample(new Sample(
	"sample_blend",
	"Demonstrates pair blending of animations.",
	"..\\base\\samples\\sample_cranimation\\sample_blend_2005.sln",
	"sample_cranimation\\sample_blend_2005.sln",
	"sample_cranimation_sample_blend.jpg",
	"sample_cranimation_sample_blend.txt",
	"base\\samples\\sample_cranimation sample_blend",
	"base\\samples\\sample_cranimation sample_blend /xenon",
	"base\\samples\\sample_cranimation sample_blend /ps3",
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_ANIMATION.appendSample(new Sample(
	"sample_blendn",
	"Demonstrates n-way blending of animations.",
	"..\\base\\samples\\sample_cranimation\\sample_blendn_2005.sln",
	"sample_cranimation\\sample_blendn_2005.sln",
	"sample_cranimation_sample_blendn.jpg",
	"sample_cranimation_sample_blendn.txt",
	"base\\samples\\sample_cranimation sample_blendn",
	"base\\samples\\sample_cranimation sample_blendn /xenon",
	"base\\samples\\sample_cranimation sample_blendn /ps3",
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_ANIMATION.appendSample(new Sample(
	"sample_player",
	"Demonstrates playback of an animation using simple player.",
	"..\\base\\samples\\sample_cranimation\\sample_player_2005.sln",
	"sample_cranimation\\sample_player_2005.sln",
	"sample_cranimation_sample_player.jpg",
	"sample_cranimation_sample_player.txt",
	"base\\samples\\sample_cranimation sample_player",
	"base\\samples\\sample_cranimation sample_player /xenon",
	"base\\samples\\sample_cranimation sample_player /ps3",
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_ANIMATION.appendSample(new Sample(
	"sample_filter",
	"Demonstrates use of frame filters.",
	"..\\base\\samples\\sample_cranimation\\sample_filter_2005.sln",
	"sample_cranimation\\sample_filter_2005.sln",
	"sample_cranimation_sample_filter.jpg",
	"sample_cranimation_sample_filter.txt",
	"base\\samples\\sample_cranimation sample_filter",
	"base\\samples\\sample_cranimation sample_filter /xenon",
	"base\\samples\\sample_cranimation sample_filter /ps3",
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_ANIMATION.appendSample(new Sample(
	"sample_creature",
	"Example of blend shapes supported by using new creature class.",
	"..\\base\\samples\\sample_creature\\sample_creature_2005.sln",
	"sample_creature\\sample_creature_2005.sln",
	"sample_creature_sample_creature.jpg",
	"sample_creature_sample_creature.txt",
	"base\\samples\\sample_creature sample_creature",
	"base\\samples\\sample_creature sample_creature /xenon",
	"base\\samples\\sample_creature sample_creature /ps3",
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_ANIMATION.appendSample(new Sample(
	"sample_mt_simple",
	"Demonstrates simple motion tree functionality.",
	"..\\suite\\samples\\sample_motiontree\\sample_mt_simple_2005.sln",
	"sample_motiontree\\sample_mt_simple_2005.sln",
	"sample_motiontree_sample_motiontree_simple.jpg",
	"sample_motiontree_sample_motiontree_simple.txt",
	"suite\\samples\\sample_motiontree sample_mt_simple",
	"suite\\samples\\sample_motiontree sample_mt_simple /xenon",
	"suite\\samples\\sample_motiontree sample_mt_simple /ps3",
	null));
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_ANIMATION.appendSample(new Sample(
	"sample_ponytail",
	"Demonstrates physically simulated pony tail.",
	"..\\suite\\samples\\sample_cloth\\sample_ponytail_2005.sln",
	"sample_cloth\\sample_ponytail_2005.sln",
	"sample_ponytail_sample_ponytail.jpg",
	"sample_ponytail_sample_ponytail.txt",
	"suite\\samples\\sample_cloth sample_ponytail",
	"suite\\samples\\sample_cloth sample_ponytail /xenon -rag",
	"suite\\samples\\sample_cloth sample_ponytail /ps3 -rag",
	null));	
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_ANIMATION.appendSample(new Sample(
	"sample_pm",
	"Tests parameterized motion - Locomotion",
	"..\\suite\\samples\\sample_parameterizedmotion\\sample_pm_2005.sln",
	"sample_parameterizedmotion\\sample_pm_2005.sln",
	"sample_pr_demo_0.jpg",
	"output.txt",
	"suite\\samples\\sample_parameterizedmotion sample_pm -rag -demo 0",
	"suite\\samples\\sample_parameterizedmotion sample_pm /xenon -rag -demo 0",
	"suite\\samples\\sample_parameterizedmotion sample_pm /ps3 -rag -demo 0",
	null));	
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_ANIMATION.appendSample(new Sample(
	"sample_pm",
	"Tests parameterized motion - Punching",
	"..\\suite\\samples\\sample_parameterizedmotion\\sample_pm_2005.sln",
	"sample_parameterizedmotion\\sample_pm_2005.sln",
	"sample_pr_demo_1.jpg",
	"output.txt",
	"suite\\samples\\sample_parameterizedmotion sample_pm -rag -demo 1",
	"suite\\samples\\sample_parameterizedmotion sample_pm /xenon -rag -demo 1",
	"suite\\samples\\sample_parameterizedmotion sample_pm /ps3 -rag -demo 1",
	null));	
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_ANIMATION.appendSample(new Sample(
	"sample_pm",
	"Tests parameterized motion - One-Handed Aiming",
	"..\\suite\\samples\\sample_parameterizedmotion\\sample_pm_2005.sln",
	"sample_parameterizedmotion\\sample_pm_2005.sln",
	"sample_pr_demo_2.jpg",
	"output.txt",
	"suite\\samples\\sample_parameterizedmotion sample_pm -rag -demo 2",
	"suite\\samples\\sample_parameterizedmotion sample_pm /xenon -rag -demo 2",
	"suite\\samples\\sample_parameterizedmotion sample_pm /ps3 -rag -demo 2",
	null));	
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_ANIMATION.appendSample(new Sample(
	"sample_pm",
	"Tests parameterized motion - Object Pick up",
	"..\\suite\\samples\\sample_parameterizedmotion\\sample_pm_2005.sln",
	"sample_parameterizedmotion\\sample_pm_2005.sln",
	"sample_pr_demo_3.jpg",
	"output.txt",
	"suite\\samples\\sample_parameterizedmotion sample_pm -rag -demo 3",
	"suite\\samples\\sample_parameterizedmotion sample_pm /xenon -rag -demo 3",
	"suite\\samples\\sample_parameterizedmotion sample_pm /ps3 -rag -demo 3",
	null));	
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_ANIMATION.appendSample(new Sample(
	"sample_pm",
	"Tests parameterized motion - Two-Handed Punching",
	"..\\suite\\samples\\sample_parameterizedmotion\\sample_pm_2005.sln",
	"sample_parameterizedmotion\\sample_pm_2005.sln",
	"sample_pr_demo_4.jpg",
	"output.txt",
	"suite\\samples\\sample_parameterizedmotion sample_pm -rag -demo 4",
	"suite\\samples\\sample_parameterizedmotion sample_pm /xenon -rag -demo 4",
	"suite\\samples\\sample_parameterizedmotion sample_pm /ps3 -rag -demo 4",
	null));	
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_ANIMATION.appendSample(new Sample(
	"sample_pm",
	"Tests parameterized motion - Two-Handed Aiming",
	"..\\suite\\samples\\sample_parameterizedmotion\\sample_pm_2005.sln",
	"sample_parameterizedmotion\\sample_pm_2005.sln",
	"sample_pr_demo_5.jpg",
	"output.txt",
	"suite\\samples\\sample_parameterizedmotion sample_pm -rag -demo 5",
	"suite\\samples\\sample_parameterizedmotion sample_pm /xenon -rag -demo 5",
	"suite\\samples\\sample_parameterizedmotion sample_pm /ps3 -rag -demo 5",
	null));	
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_ANIMATION.appendSample(new Sample(
	"sample_pm",
	"Tests parameterized motion - Fast Walk",
	"..\\suite\\samples\\sample_parameterizedmotion\\sample_pm_2005.sln",
	"sample_parameterizedmotion\\sample_pm_2005.sln",
	"sample_pr_demo_6.jpg",
	"output.txt",
	"suite\\samples\\sample_parameterizedmotion sample_pm -rag -demo 6",
	"suite\\samples\\sample_parameterizedmotion sample_pm /xenon -rag -demo 6",
	"suite\\samples\\sample_parameterizedmotion sample_pm /ps3 -rag -demo 6",
	null));	
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_ANIMATION.appendSample(new Sample(
	"sample_pm",
	"Tests parameterized motion - Head Look at",
	"..\\suite\\samples\\sample_parameterizedmotion\\sample_pm_2005.sln",
	"sample_parameterizedmotion\\sample_pm_2005.sln",
	"sample_pr_demo_7.jpg",
	"output.txt",
	"suite\\samples\\sample_parameterizedmotion sample_pm -rag -demo 7",
	"suite\\samples\\sample_parameterizedmotion sample_pm /xenon -rag -demo 7",
	"suite\\samples\\sample_parameterizedmotion sample_pm /ps3 -rag -demo 7",
	null));	
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_ANIMATION.appendSample(new Sample(
	"sample_pm",
	"Tests parameterized motion - Run",
	"..\\suite\\samples\\sample_parameterizedmotion\\sample_pm_2005.sln",
	"sample_parameterizedmotion\\sample_pm_2005.sln",
	"sample_pr_demo_8.jpg",
	"output.txt",
	"suite\\samples\\sample_parameterizedmotion sample_pm -rag -demo 8",
	"suite\\samples\\sample_parameterizedmotion sample_pm /xenon -rag -demo 8",
	"suite\\samples\\sample_parameterizedmotion sample_pm /ps3 -rag -demo 8",
	null));	
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_ANIMATION.appendSample(new Sample(
	"sample_pm",
	"Tests parameterized motion - Walk to target ",
	"..\\suite\\samples\\sample_parameterizedmotion\\sample_pm_2005.sln",
	"sample_parameterizedmotion\\sample_pm_2005.sln",
	"sample_pr_demo_9.jpg",
	"output.txt",
	"suite\\samples\\sample_parameterizedmotion sample_pm -rag -demo 9",
	"suite\\samples\\sample_parameterizedmotion sample_pm /xenon -rag -demo 9",
	"suite\\samples\\sample_parameterizedmotion sample_pm /ps3 -rag -demo 9",
	null));	
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_ANIMATION.appendSample(new Sample(
	"sample_pm",
	"Tests parameterized motion - Step up",
	"..\\suite\\samples\\sample_parameterizedmotion\\sample_pm_2005.sln",
	"sample_parameterizedmotion\\sample_pm_2005.sln",
	"sample_pr_demo_10.jpg",
	"output.txt",
	"suite\\samples\\sample_parameterizedmotion sample_pm -rag -demo 10",
	"suite\\samples\\sample_parameterizedmotion sample_pm /xenon -rag -demo 10",
	"suite\\samples\\sample_parameterizedmotion sample_pm /ps3 -rag -demo 10",
	null));	
// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
section_ANIMATION.appendSample(new Sample(
	"sample_pm",
	"Tests parameterized motion - Hit reaction",
	"..\\suite\\samples\\sample_parameterizedmotion\\sample_pm_2005.sln",
	"sample_parameterizedmotion\\sample_pm_2005.sln",
	"sample_pr_demo_11.jpg",
	"output.txt",
	"suite\\samples\\sample_parameterizedmotion sample_pm -rag -demo 11",
	"suite\\samples\\sample_parameterizedmotion sample_pm /xenon -rag -demo 11",
	"suite\\samples\\sample_parameterizedmotion sample_pm /ps3 -rag -demo 11",
	null));	
//// ******************************************************************
////  section
//// id, name
//section_THISISADIFFERENTNAMEONPURPOSE = new Section("", "");
//samples.appendSection(section_RENAMETHIS);
//// id, desc, slnpath, slndesc, screenshot, output, win32, xenon, ps3, generic
//section_RENAMETHIS.appendSample(new Sample(
//	"",
//	"",
//	"",
//	"",
//	"",
//	"",
//	null,
//	null,
//	null,
//	null));
