@echo off

CD ..\..

for /R %%D in (.) do (
	PUSHD %%D
	IF EXIST *.cpp (
		for %%F in (*.cpp) do (
			findstr SCR_REGISTER %%F
			IF NOT ERRORLEVEL 1 (
				echo %%D\%%F . . .
				echo on
				p4 edit %%F
				copy %%F %%F.bak
				%~dp0\..\script\src\script\register_secure %%F.bak %%F
				@echo off
			)
		)
	)
	POPD
)
