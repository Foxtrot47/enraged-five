@echo off
REM Invoke this batch file from the highest part of the tree
REM you want to recursively process (rage, rage\base, etc)

@echo -------------------------------------------------------------------------------------------------
@echo --- Project Generator 3 (PG3) 
@echo --- https://devstar.rockstargames.com/wiki/index.php/Dev:Project_builder3 
@echo.

@echo ***********************************************************************
@echo *** PLEASE DO NOT USE THIS SCRIPT 				  ***
@echo ***********************************************************************
@echo instead call %RS_TOOLSROOT%/script/util/projGen.bat 
@echo this will recurse in a directory 
@echo it uses any local projgen.config files to associate 
@echo the appropriate settings for a collection of projects & solutions.
@echo ***********************************************************************
@echo *** This script will now exit without having done anything          ***
@echo ***********************************************************************

goto  END

@echo --- Get all makefiles of all flavours
dir/b/s makefile.txt > rage.makefiles 2>nul
dir/b/s *.makefile >> rage.makefiles 2>nul
dir/b/s *.slndef >> rage.makefiles 2>nul

egrep -i .*rage\\base\\src.* rage.makefiles > 2.makefiles

type nul > blank
fc 2.makefiles blank > nul
if %ERRORLEVEL%==1 (
  @echo
  @echo --- Build any rage\base\src ( sample dependencies do not require unity )
  echo build %RS_TOOLSROOT%\etc\projgen\rage_base_src.build >> 2.makefiles
  %RS_TOOLSROOT%\bin\projectgenerator\projectgenerator.exe --changelist --verify 2.makefiles
)


egrep -i -v .*sample^|rage\\base\\src.* rage.makefiles > 3.makefiles

type nul > blank
fc 3.makefiles blank > nul
if %ERRORLEVEL%==1 (
 @echo
 @echo --- Build any NON SAMPLES or NON RAGE BASE SRC
 echo build %RS_TOOLSROOT%\etc\projgen\game.build >> 3.makefiles
 %RS_TOOLSROOT%\bin\projectgenerator\projectgenerator.exe --changelist --verify 3.makefiles
)


egrep -i .*sample.* rage.makefiles > 4.makefiles

type nul > blank
fc 4.makefiles blank > nul
if %ERRORLEVEL%==1 (
 @echo
 @echo --- Build any SAMPLES
 echo build %RS_TOOLSROOT%\etc\projgen\sample.build >> 4.makefiles
 %RS_TOOLSROOT%\bin\projectgenerator\projectgenerator.exe --changelist --verify 4.makefiles
)




@echo
@echo --- Tidy up.
erase *.makefiles 2>nul
erase blank 2>nul

:END
@echo.
@echo --- PG3 Complete.
@echo -------------------------------------------------------------------------------------------------
