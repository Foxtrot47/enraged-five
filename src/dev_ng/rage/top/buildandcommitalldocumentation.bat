REM @echo off

setlocal

pushd ..
p4 edit .../*.dtx
p4 edit .../*.chm
p4 edit .../*.HxS
p4 edit .../*.HxI
popd

pushd doc
pushd bin

ECHO Building Help Files:

if "%1" == "-xge" (
xgconsole.exe /COMMAND="..\..\runhelpgenerator.bat -xge"
) else (
call ..\..\runhelpgenerator.bat
)

setlocal enabledelayedexpansion

REM Give DEBUG a non-empty value to enter debug mode
SET DEBUG=
IF "%DEBUG%"=="" (
	SET NOCHANGES=
	SET DEBUGECHO=
) ELSE (
	SET NOCHANGES=-n
	SET DEBUGECHO=ECHO
)

echo submitting all changes:

p4 revert -a ../../../...

SET DCA_TEMPFILE=%TEMP%\DCA_TEMPFILE_%RANDOM%.txt

REM NOTE: redirecting to a temp file because delayed expansion
REM       doesn't work properly with piped output
CALL ..\..\dca_recurse.bat ..\..\doc_commit_template.txt > %DCA_TEMPFILE%

REM if "Files:" was never found in the changespec,
REM   there were no files to commit
IF "%DCA_NEXTISFILES%"=="" (
	echo no files found to commit, aborting
) ELSE (
	IF "%DEBUG%"=="" (
		type "%DCA_TEMPFILE%" | p4 submit -i 
	) ELSE (
		echo changelist spec:
		echo ==================
		type "%DCA_TEMPFILE%"
		echo ==================
	)
)

IF EXIST "%DCA_TEMPFILE%" DEL /F %DCA_TEMPFILE%

popd
popd
