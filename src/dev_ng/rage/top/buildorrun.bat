@echo off
setlocal

if "%3"=="/xenon" goto XENON
if "%3"=="/ps3" goto PS3

REM see if the sample is already built, in which case just run it
if EXIST ../%1/win32_beta/%2_win32_beta.exe goto RUN

REM we have to build the sample
echo -=-=-=- building sample (PC), please wait... -=-=-=-
echo %1,%2,"Beta Win32" > ..\qa\buildorrun.txt
cd ..
call qa\qabuild /build qa\buildorrun.txt
del qa\buildorrun.txt
cd top

:RUN

echo -=-=-=- running sample (PC)... -=-=-=-

REM shift the args out of the way that we aren't passing through
set EXECUTABLE=win32_beta\%2_win32_beta.exe
cd ..\%1
shift
shift

if NOT "%REDIRECT%" == "" goto REDIRECT
%EXECUTABLE% %1 %2 %3 %4 %5 %6 %7 %8 %9
goto END

:REDIRECT
echo Sending output to %REDIRECT%
%EXECUTABLE% %1 %2 %3 %4 %5 %6 %7 %8 %9 > %REDIRECT%
goto END

:END
pause
exit/b

:XENON

REM see if the sample is already built, in which case just run it
if EXIST ../%1/xenon_beta/%2_xenon_beta.xex goto RUN_XENON

REM we have to build the sample
echo -=-=-=- building sample (Xenon), please wait... -=-=-=-
echo %1,%2,"Beta Xenon" > ..\qa\buildorrun.txt
cd ..
call qa\qabuild /build qa\buildorrun.txt
del qa\buildorrun.txt
cd top

:RUN_XENON

echo -=-=-=- running sample (Xenon)... -=-=-=-
"%XEDK%\bin\win32\xbreboot"
..\framework\tools\bin\systrayrfs -exit
..\framework\tools\bin\rag\rag -exit
REM this also bootstraps rfs.dat in %TEMP% for us.
start ..\framework\tools\bin\systrayrfs -trusted
start ..\framework\tools\bin\rag\rag

"%XEDK%\bin\win32\xbdel" /R xe:\samples
"%XEDK%\bin\win32\xbmkdir" xe:\samples
"%XEDK%\bin\win32\xbcp" ..\%1\xenon_beta\%2_xenon_beta.xex xe:\samples
"%XEDK%\bin\win32\xbcp" %TEMP%\rfs.dat xe:\samples

REM shift the args out of the way that we aren't passing through
set EXECUTABLE=xe:\samples\%2_xenon_beta.xex
shift
shift
shift

"%XEDK%\bin\win32\xbreboot" %EXECUTABLE% %1 %2 %3 %4 %5 %6 %7 %8 %9

goto END

:PS3

REM see if the sample is already built, in which case just run it
if EXIST ../%1/psn_beta_snc/%2_psn_beta_snc.ppu.self goto RUN_PS3

REM we have to build the sample
echo -=-=-=- building sample (PS3), please wait... -=-=-=-
echo %1,%2,"SN PS3SNC Beta Win32" > ..\qa\buildorrun.txt
cd ..
call qa\qabuild /build qa\buildorrun.txt
del qa\buildorrun.txt
cd top

:RUN_PS3

echo -=-=-=- running sample (ps3)... -=-=-=-
ps3run -r
..\framework\tools\bin\systrayrfs -exit
..\framework\tools\bin\rag\rag -exit
REM this also bootstraps rfs.dat in %TEMP% for us.
start ..\framework\tools\bin\systrayrfs -trusted
start ..\framework\tools\bin\rag\rag

REM shift the args out of the way that we aren't passing through
set EXECUTABLE=../%1/psn_beta_snc/%2_psn_beta_snc.ppu.self
shift
shift
shift

echo *** HIT ESCAPE TO CLEAR THIS WINDOW ONCE YOU ARE DONE WITH THE APP ***
ps3run -p %EXECUTABLE% %1 %2 %3 %4 %5 %6 %7 %8 %9

goto END

