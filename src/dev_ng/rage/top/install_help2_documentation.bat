@echo off

setlocal

pushd doc
pushd bin

ECHO Installing Help2 Files:
HelpGenerator.exe rage_default_settings.xml -install

popd
popd