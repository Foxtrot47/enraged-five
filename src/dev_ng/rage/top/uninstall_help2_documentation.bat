@echo off

setlocal

pushd doc
pushd bin

ECHO Uninstalling Help2 Files:
HelpGenerator.exe rage_default_settings.xml -uninstall

popd
popd