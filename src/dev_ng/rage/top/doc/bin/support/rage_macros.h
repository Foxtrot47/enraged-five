////////////////////////////////////
// Define macros here to help out the documentation parser
// (empty macros can be used to make sure we don't get bogus symbols in the output

// Note that because of the way comments get combined - we should still be able to document the macros in the rage code
// (I hope)

#define CompileTimeAssert (x)

#define DECLARE_FRAG_INTERFACE (x)

#define EXT_PFD_DECLARE_ITEM (x)
#define EXT_PFD_DECLARE_GROUP (x)

#define IMPLEMENT_PLACE (x)

#define RAGE_DEFINE_CHANNEL (x)

#define PARAM (x, y)
#define REQPARAM (x, y)
#define FPARAM (x, y, z)
#define XPARAM (x)

