=================================
	RAGE DOCUMENTATION
=================================

Rage documentation is a combination of code, comments, and library help files.  The source code is parsed for function declarations, enumerations, function comments and the like.  Each Rage library has its own help file that describes general usage, gotches, and presents other important information that a programmer working with the Rage API may need to know.  In additional, many of our standalone tools and applications may have their own help files that are accessible from the applications themselves.

Rage Documentation may be accessed in one way (currently - more may be added in the future):

1) Compiled Help (.chm)

	The easiest way to view this help is to execute "top\doc\Rage Documentation (Chm).bat".

	This help format places each Rage library and application in its own .chm file, located in "top\doc\chm", that is referenced by a
	master file.  You may open up the help files individually should you so choose.
