@echo off

SETLOCAL ENABLEEXTENSIONS ENABLEDELAYEDEXPANSION

SET TR_USEP4=1
ECHO TR_USEP4=%TR_USEP4%
SET P4CLIENT=ragemonkey-everything

if "%1"=="" (
	echo you need to include the RAGE release number
	echo For example, "WR100" specifies the release tag
	echo for the Weekly Release, version 1.00.
	goto exit
)

cd ..

goto skipCurrentRelease
rem all of the current release stuff is outdated

echo copying qa\current-release.html to qa\notes\release-%1.html...
copy qa\current-release.html qa\notes\release-%1.html
echo deleting current contents of current-release.html...
echo on > qa\current-release.html
echo adding qa\notes\release-%1.html...
if "%TR_USEP4%"=="1" (
	p4 add qa/notes/release-%1.html
) ELSE (
	cvs add qa/notes/release-%1.html
)

echo copying qa\current-release-ab.html to qa\notes\release-ab-%1.html...
copy qa\current-release-ab.html qa\notes\release-ab-%1.html
echo deleting current contents of current-release-ab.html...
echo on > qa\current-release-ab.html
echo adding qa\notes\release-ab-%1.html...
if "%TR_USEP4%"=="1" (
	p4 add qa/notes/release-ab-%1.html
) ELSE (
	cvs add qa/notes/release-ab-%1.html
)

echo copying qa\current-release-api.html to qa\notes\release-api-%1.html...
copy qa\current-release-api.html qa\notes\release-api-%1.html
echo deleting current contents of current-release-api.html...
echo on > qa\current-release-api.html
echo adding qa\notes\release-api-%1.html...
if "%TR_USEP4%"=="1" (
	p4 add qa/notes/release-api-%1.html
) ELSE (
	cvs add qa/notes/release-api-%1.html
)

:skipCurrentRelease

echo committing qa directory...
if "%TR_USEP4%"=="1" (
	p4 revert -a
	p4 submit
) ELSE (
	cvs commit qa
)
echo off

for %%T IN (%1) DO IF "%TR_USEP4%"=="1" (
	echo === TAG '%%T' ===
	p4 tag -l %%T //rage/dev/...
) ELSE (
	echo === TAG '%%T' ===
	REM do loose files here
	cvs -Q tag -l %%T

	REM then do each subtree
	for /D %%D IN (*) DO (
		IF NOT %%D==CVS (
			ECHO tagging in %%D...
			pushd %%D
			cvs -Q tag %%T
			popd
		)
	)
)

:exit

ENDLOCAL
