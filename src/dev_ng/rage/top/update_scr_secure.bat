@echo off

CD ..\..

FOR %%F in (rage\suite rage\contrib) do (
	echo Processing subtree under %%F . . .
	PUSHD %%F

	for /R %%D in (.) do (
		PUSHD %%D
		IF EXIST *.cpp (
			for %%F in (*.cpp) do (
				findstr SCR_REGISTER_SECURE %%F
				IF NOT ERRORLEVEL 1 (
					echo %%D\%%F . . .
					echo on
					p4 edit %%F
					copy %%F %%F.bak
					%~dp0\..\script\src\script\update_secure %%F.bak %%F
					@echo off
				)
			)
		)
		POPD
	)
	POPD
)
