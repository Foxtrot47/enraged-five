@echo off

SET DCA_NEXTISDESCRIPTION=
SET DCA_NEXTISFILES=

for /F "eol=# tokens=1* usebackq" %%A in (`p4 change -o`) do (
	IF "!DCA_NEXTISFILES!"=="1" (
		ECHO		%%A %%B
	) ELSE (
		IF "%%A"=="Files:" (
			ECHO %%A
			SET DCA_NEXTISFILES=1
		) ELSE IF "!DCA_NEXTISDESCRIPTION!"=="1" (
			IF EXIST %1 (
				type %1
			) else (
				ECHO 	%*
			)
			ECHO.
			SET DCA_NEXTISDESCRIPTION=
		) ELSE IF "%%A"=="Description:" (
			ECHO %%A
			SET DCA_NEXTISDESCRIPTION=1
		) ELSE (
			ECHO %%A	%%B
			ECHO.
		)
	)
)

