@echo off

setlocal

pushd doc
pushd bin

ECHO Extracting QA Warnings:
HelpGenerator.exe rage_default_settings.xml -qa

setlocal enabledelayedexpansion

REM Give DEBUG a non-empty value to enter debug mode
SET DEBUG=
IF "%DEBUG%"=="" (
	SET NOCHANGES=
	SET DEBUGECHO=
) ELSE (
	SET NOCHANGES=-n
	SET DEBUGECHO=ECHO
)

REM add qa files:
pushd ..\..\..\base\doc\qa
	echo adding base qaignore files:
	p4 add %NOCHANGES% -t text qaignore_*.txt
popd
pushd ..\..\..\script\doc\qa
	echo adding script qaignore files:
	p4 add %NOCHANGES% -t text qaignore_*.txt
popd
pushd ..\..\..\suite\doc\qa
	echo adding suite qaignore files:
	p4 add %NOCHANGES% -t text qaignore_*.txt
popd

echo submitting all changes:

SET DCA_TEMPFILE=%TEMP%\DCA_TEMPFILE_%RANDOM%.txt

REM NOTE: redirecting to a temp file because delayed expansion
REM       doesn't work properly with piped output
CALL ..\..\dca_recurse.bat ..\..\doc_check_commit_template.txt > %DCA_TEMPFILE%

REM if "Files:" was never found in the changespec,
REM   there were no files to commit
IF "%DCA_NEXTISFILES%"=="" (
	echo no files found to commit, aborting
) ELSE (
	IF "%DEBUG%"=="" (
		type "%DCA_TEMPFILE%" | p4 submit -i
	) ELSE (
		echo changelist spec:
		echo ==================
		type "%DCA_TEMPFILE%"
		echo ==================
	)
)

IF EXIST "%DCA_TEMPFILE%" DEL /F %DCA_TEMPFILE%

popd
popd
