@ECHO ON
SETLOCAL

CALL :alienbrain_grab art/characters/rage_male/scene
CALL :maya_export T:/rage/art/characters/rage_male/scene/rage_male001.mb

GOTO endeverything












:maya_export
CALL T:\rage\tools\Modules\base\rage\exes\SetupEnvironmentForRageAndMaya7.0.bat
"c:\Program Files\Alias\Maya7.0\bin\mayabatch.exe" -batch -command "rageBatchExport(\"%1"\",\"\")"
GOTO endeverything

:alienbrain_grab

IF [%1]==[] (
	ECHO ERROR: alienbrain_grab routine called with no parameters
	GOTO endeverything
)
	
ab logon -u readonly -p "" -d rage -s SANRAGE 
ab getlatest %1 -forcefileupdate -localpath "T:/rage/"%1
ab logoff

GOTO endeverything

:endeverything