import sys
import os
import shutil
import subprocess

if len(sys.argv) < 4:
    print "Usage: %s sourceDir destDir [files...]" % (sys.argv[0])

sourcedir = sys.argv[1]
destdir = sys.argv[2]
files = sys.argv[3:]

def runCommand(cmd):
    cmdFile = os.popen(cmd)
    print cmdFile.read()
    status = cmdFile.close()
    if status:
        sys.exit(status)

os.chdir(destdir)
chmodfiles = [x for x in files if os.path.exists(x)]
#runCommand("p4 edit " + " ".join(files))
#if len(chmodfiles):
#    print "attrib -r " + " ".join(chmodfiles)
#    runCommand("attrib -r " + " ".join(chmodfiles))
for i in files:
    runCommand("attrib -r " + i)
    srcfile = os.path.join(sourcedir, i)
    shutil.copy(srcfile, i)
    print "Copied %s -> %s" % (srcfile, i)
#runCommand("p4 revert -a " + " ".join(files))

