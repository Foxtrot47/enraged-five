# whatsnew.py
#
# Usage: whatsnew.py OLDTAGNAME NEWTAGNAME
# searches for any files on disk whose head revision is newer than the tagged revision
# oldtagname can be CURRENT
# newtagname can be HEAD
# whatsnew.py CURRENT HEAD should be the same as running 'status'

import os
import popen2
import re
import sys
import types
import optparse

class Log:
    def __init__(self, lines):
        self.rev = lines[0].strip().split()[1] # "revision 1.5.2.5"
        self.revParts = [int(x) for x in self.rev.split(".")]
        properties = lines[1].strip().split(";")
        self.date = properties[0].split(":", 1)[1]
        self.author = properties[1].split(":", 1)[1]
        self.description = "".join(lines[2:])

class Diff:
    def __init__(self, lines):
        self.fileSpec = None
        self.filename = lines[0][len("Index: "):].strip()
        self.cvsPath = lines[2].split()[1].strip() # RCS file: /Angel/soft/path
        if lines[3].startswith("retrieving revision"):
            self.oldRevision = lines[3][len("retrieving revision "):].strip()
        else:
            self.oldRevision = "0"
            
        if lines[4].startswith("retrieving revision"):
            self.newRevision = lines[4][len("retrieving revision "):].strip()
        else:
            self.newRevision = "0"

    def FindFileSpec(self):
        self.fileSpec = GetFilesInCvs(self.filename)[0];

    def FindDiffLogs(self):
        if self.oldRevision != "0" and self.newRevision != "0":
            # make a list of all the revisions to retrieve
            newRev = self.newRevision
            oldRev = self.oldRevision
            if CompareRevisions(newRev, oldRev) == -1:
                (newRev, oldRev) = (oldRev, newRev)
            rev = newRev
            revList = []
            while(rev != "0" and rev != oldRev):
                revList.append(rev)
                rev = FindParentRevision(rev)

            logArgs = ["-r"+x for x in revList]
            self.logInfo = FindLogInfo("cvs log -N " + " ".join(logArgs) + " " + self.filename)
        else:
            # just get them all
            self.logInfo = FindLogInfo("cvs log -N " + self.filename)

class LogInfo:
    def __init__(self, head, tags, logs):
        self.head = head
        self.tags = tags
        self.logs = logs

def FindLogInfo(cvsCommand):
    (stdout, stdin) = popen2.popen2(cvsCommand)

    head = ""
    tags = {}
    logs = {}
    
    readingTags = False
    readingLogs = False
    lines = stdout.readlines()
    logLines = []
    for i in lines:
        if i.startswith("symbolic names:"):
            readingTags = True
        elif i.startswith("keyword"):
            readingTags = False
        elif i.startswith("head:"):
            head = i.split(":")[1].strip()
        elif readingTags:
            tag = i.split(":")
            tags[tag[0].strip()] = tag[1].strip()
        elif i.startswith("--------------"):
            if readingLogs:
                newLog = Log(logLines)
                logs[newLog.rev] = newLog
            readingLogs = True
            logLines = []
        elif i.startswith("=============="):
            readingLogs = False
            if len(logLines) > 0:
                newLog = Log(logLines)
                logs[newLog.rev] = newLog
        elif readingLogs:
            logLines.append(i)
    return LogInfo(head, tags, logs)

class FileSpec:
    def __init__(self, filename, status, workingRev, reposRev, stickyTag, branch, cvsPath):
        self.filename = filename
        self.status = status
        self.workingRev = workingRev
        self.reposRev = reposRev
        self.stickyTag = stickyTag
        self.branch = branch
        self.cvsPath = cvsPath
        self.logInfo = None
        
    def FindTagsAndLogs(self):
        self.logInfo = FindLogInfo("cvs log " + self.filename)

    def GetRevisionFromTag(self, tagname):
        if tagname == "HEAD":
            # see if we're on a branch, if so find the latest
            # tag on that branch
            if self.branch != "":
                latestIdx = -1
                latestRev = "-1"
                branchParts = self.branch.split(".")
                for i in self.logs.values():
                    print branchParts, i.revParts[:-1]
                    if branchParts == i.revParts[:-1]:
                        if i.revParts[-1] > latestIdx:
                            latestIdx = i.revParts[-1]
                            latestRev = i.rev;
                return latestRev
            return self.head
        elif tagname == "CURRENT":
            return self.workingRev
        else:
            if self.tags.has_key(tagname):
                return self.tags[tagname]
            else:
                return "-1"

    def CompareTags(self, tagA, tagB):
        return CompareRevisions(self.GetRevisionFromTag(tagA), self.GetRevisionFromTag(tagB))

FileLineRE = re.compile(r"File: (?P<FileName>.*?)\s+Status:\s*(?P<Status>.*)$")
WorkingRevLineRE = re.compile(r"Working revision:\s+(?P<WorkingRev>.*)$")
ReposRevLineRE = re.compile(r"Repository revision:\s+(?P<ReposRev>\S+)?\s+(?P<CvsPathName>.*)$")
StickyTagRE = re.compile(r"Sticky Tag:\s+(?P<StickyTagName>\S+)\s*(\(branch: (?P<BranchRev>\S+)\))?")


def CreateFileSpec(lines, dirname):
    filename = ""
    status = ""
    workingRev = ""
    reposRev = ""
    stickyTag = ""
    branch = ""
    for i in lines:
        if i.startswith("File"):
            match = FileLineRE.match(i)
            if match:
                filename = match.group('FileName')
                status = match.group('Status')
        elif i.startswith("Working"):
            match = WorkingRevLineRE.match(i)
            if match:
                workingRev = match.group('WorkingRev')
        elif i.startswith("Repository"):
            match = ReposRevLineRE.match(i)
            if match:
                reposRev = match.group('ReposRev')
                cvsPath = match.group('CvsPathName')                
        elif i.startswith("Sticky Tag"):
            match = StickyTagRE.match(i)
            if match:
                stickyTag = match.group('StickyTagName')
                branch = match.group('BranchRev')
    return FileSpec(filename=os.path.join(dirname,filename), status=status, workingRev=workingRev, reposRev=reposRev, stickyTag=stickyTag, branch=branch, cvsPath=cvsPath)

def GetFilesInCvs(dirname):
    realDirName = dirname
    # if we actually have a file instead of a directory,
    # get the directory name
    if os.path.isfile(dirname):
        realDirName = os.path.dirname(dirname)
    (stdout, stdin) = popen2.popen2("cvs status " + dirname)
    stdoutlines = [x.strip() for x in stdout.readlines()]
    section = []
    addingToSection = False
    fileSpecs = []
    for i in stdoutlines:
        if addingToSection:
            if i.startswith("=================="):
                fileSpecs.append(CreateFileSpec(section, realDirName))
                section = []
            else:
                section.append(i)
        else:
            if i.startswith("=================="):
                addingToSection = True
    if len(section) > 0:
        fileSpecs.append(CreateFileSpec(section, realDirName))
    return fileSpecs

def FindAllTags(fileSpecs):
    for i in fileSpecs:
        i.FindTagsAndLogs()

# returns: -1 if revA is older, 1 if revB is older
# throws value error if revs are on different branches
def CompareRevisions(revA, revB):
    if type(revA) is types.StringType:
        return CompareRevisions([int(x) for x in revA.split(".")], revB)
    if type(revB) is types.StringType:
        return CompareRevisions(revA, [int(x) for x in revB.split(".")])

        
    # some cases:
    # 1) lengths the same: can compare if difference is only in last elt
    # 2) lengths differ: can compare if one is the prefix of another

    if len(revA) == len(revB):
        # make sure prefixes are the same
        matchingPrefix = True
        for i in range(0, len(revA)-1):
            if revA[i] != revB[i]:
                matchingPrefix = False;
                break
        if matchingPrefix:
            return cmp(revA[-1], revB[-1])
        else:
            raise ValueError
    else:
        # truncate the long one and compare. If still the same
        # then the long one is newer
        if len(revA) > len(revB):
            c = CompareRevisions(revA[0:len(revB)], revB)
            if c == -1:
                raise ValueError
            else:
                return 1
        else:
            c = CompareRevisions(revA, revB[0:len(revA)])
            if c == 1:
                raise ValueError
            else:
                return -1

def FindParentRevision(revList):
    if type(revList) is types.StringType:
        return FindParentRevision([int(x) for x in revList.split(".")])
        
    if (revList == [1,1]):
        return "0"
    elif (revList[-1] == 1):
        parent = revList[:-2]
        return ".".join([str(x) for x in parent])
    else:
        parent = revList[:]
        parent[-1] -= 1
        return ".".join([str(x) for x in parent])

def FindAllParents(revList):
    parentRev = FindParentRevision(revList)
    print parentRev
    if parentRev != "0":
        return FindAllParents(parentRev)

def FindChangedFiles(oldTag, newTag, dirname):
    oldTagQuery = "-r " + oldTag
    newTagQuery = "-r " + newTag
    if oldTag == "CURRENT":
        oldTagQuery = ""
    if newTag == "CURRENT":
        newTagQuery = ""
    (stdout, stderr) = popen2.popen2("cvs diff -RN %s %s --brief %s" % (oldTagQuery, newTagQuery, dirname))
    readDiff = False
    diffLines = []
    diffs = []
    for i in stdout:
        if (i.startswith("Index:")):
            if len(diffLines) > 0:
                diffs.append(Diff(diffLines))
            diffLines = [i]
            readDiff = True
        elif readDiff:
            diffLines.append(i)
    if len(diffLines) > 0:
        diffs.append(Diff(diffLines))

    return diffs

def main(args):
    if type(args) is types.StringType:
        return main(args.split())

    opt = optparse.OptionParser(usage="Usage: %prog [options] OLDTAG NEWTAG [C:/root/directory/name]")
    opt.add_option("-s", "--simple", dest="simpleOutput", action="store_true", default=False, help="Just lists the files and revisions, doesn't list each change")
    opt.add_option("--nochangelog", dest="changeLog", action="store_false", default=True, help="Don't print out the change logs")
    opt.add_option("-r", '--reversed', dest="justReversed", action="store_true", default=False, help="Just print files where the OLDTAG file is newer than NEWTAG")

    (options, outargs) = opt.parse_args(args[1:])

    if len(outargs) < 2 or len(outargs) > 3:
        opt.print_help()
        sys.exit(1)

    oldTag = outargs[0]
    newTag = outargs[1]
    dirName = os.getcwd()
    if len(outargs) == 3:
        dirName = outargs[2]

    descriptions = {}

    diffs = FindChangedFiles(oldTag, newTag, dirName)

    for i in diffs:
        if i.oldRevision != "0" and i.newRevision != "0":

            newRev = i.newRevision
            oldRev = i.oldRevision

            revDiff = 0
            try:
                revDiff = CompareRevisions(newRev, oldRev)
            except ValueError:
                print "Revisions can't be compared: File %s: %s -> %s" % (i.filename, i.oldRevision, i.newRevision)
                continue            

            if options.justReversed and revDiff == 1:
                continue

            if revDiff == -1:
                (oldRev, newRev) = (newRev, oldRev)

            print "File %s: %s -> %s" % (i.filename, i.oldRevision, i.newRevision)

            if options.simpleOutput:
                continue
            
            i.FindDiffLogs()

            rev = newRev
            while(rev != "0" and rev != oldRev):
                log = i.logInfo.logs[rev]
                print "     %-12s author: %-15s date:%s" % (log.rev, log.author, log.date)
                rev = FindParentRevision(rev)

                # add description string to map
                descriptionString = "author: %s\tdate: %s\n%s" % (log.author, log.date, log.description)
                descriptions[descriptionString] = 0;
            print
        elif i.newRevision == "0" and i.oldRevision != "0":
            print "File %s: Has local changes since %s" % (i.filename, i.oldRevision)
            print

    if options.changeLog and not options.simpleOutput:
        if len(descriptions.keys()) > 0:
            print "=========================================================="
            print "Change logs: "
            print
            for i in descriptions.keys():
                print "-------------------------"
                print i
        else:
            print "No changes"
            

if __name__ == '__main__':
    main(sys.argv)