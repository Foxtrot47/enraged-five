@echo off

set XPROJ = $(RAGE_DIR)\base\samples
set XPROJ = %XPROJ% $(RAGE_DIR)\base\src\ $(RAGE_DIR)\base\qa $(RAGE_DIR)\base\tools
set XPROJ = %XPROJ% $(RAGE_DIR)\suite\samples $(RAGE_DIR)\suite\src $(RAGE_DIR)\suite\qa $(RAGE_DIR)\suite\tools
set XPROJ = %XPROJ% $(RAGE_DIR)\script\src
set XPROJ = %XPROJ% $(RAGE_DIR)\speedtree\src
set XPROJ = %XPROJ% $(RAGE_DIR)\stlport\STLport-5.0RC5\src

set LIBS = sample_grcore init sample_file stlport

set XINCLUDE = $(RAGE_DIR)\base\tools\cli $(RAGE_DIR)\base\tools\dcc\libs $(RAGE_DIR)\base\tools\libs $(RAGE_DIR)\base\tools\ui

set DIRECTORY = $(RAGE_DIR)\base\src
set DIRECTORY = %DIRECTORY% $(RAGE_DIR)\base\qa
set DIRECTORY = %DIRECTORY% $(RAGE_DIR)\suite\src
set DIRECTORY = %DIRECTORY% $(RAGE_DIR)\suite\qa
set DIRECTORY = %DIRECTORY% $(RAGE_DIR)\script\src
set DIRECTORY = %DIRECTORY% $(RAGE_DIR)\speedtree\src