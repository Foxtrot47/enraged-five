//
// programmerquiz/writeme/main.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#define _CRT_SECURE_NO_WARNINGS
#include <tchar.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>

void ReverseWords(char* chars);

void DoublePrint(FILE* outputFile, char* string)
{
	fprintf(outputFile, string);
	printf(string);
}

void ProcessInput(char* inputFileName, FILE* outputFile)
{
	FILE* inputFile = fopen(inputFileName, "rb");
	assert(inputFile);

	// Get the file length
	fseek(inputFile, 0L, SEEK_END);
	int fileLen = ftell(inputFile);
	rewind(inputFile);

	// Make space and read the file
	char* inputChars = (char*) malloc (sizeof(char)*(fileLen + 1));
	fread(inputChars, 1, fileLen, inputFile);
	fclose(inputFile);
	inputChars[fileLen] = '\0';

	DoublePrint(outputFile, "original text:\n\n");
	DoublePrint(outputFile, inputChars);

	ReverseWords(inputChars);

	DoublePrint(outputFile, "\n\nafter reverse_words:\n\n");
	DoublePrint(outputFile, inputChars);

	free(inputChars);
}

int _tmain(int argc, _TCHAR* argv[])
{
	FILE* outputFile = fopen("output.txt", "wb");

	DoublePrint(outputFile, "\n\n*** input1.txt ***\n\n");
	ProcessInput("input1.txt", outputFile);
	DoublePrint(outputFile, "\n\n*** input2.txt ***\n\n");
	ProcessInput("input2.txt", outputFile);
	DoublePrint(outputFile, "\n\n*** input3.txt ***\n\n");
	ProcessInput("input3.txt", outputFile);

	fclose(outputFile);

	// Pause and then clean up
	printf("\n\nPress ENTER to continue");
	getchar();

	return 0;
}

