//
// programmerquiz/writeme/writeme.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

// Instructions to candidate:

// - Implement the ReverseWords function as efficiently as possible
// - You may not modify main.cpp
// - Points are given for correctness, speed, and style
// - There are three input files for you to test with: input1.txt, input2.txt, and input3.txt
// - ReverseWords should reverse the words within the input string without reversing the words themselves,
//   for example:
//
//  one two three four five
//
//  should become:
//
//  five four three two one
//

#include <string.h>
#include <ctype.h>

// Reverse the order of the words in a string without reversing the letters within each word
void ReverseWords(char* chars)
{

}
