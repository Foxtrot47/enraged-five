//
// programmerquiz/fixme/main.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

// Instructions to candidate:

// - Find as many things wrong with the example as you can and fix them
// - You may not modify main.cpp
// - SortIntsByCount is *supposed* to sort an array by the number of 1 bits in the binary representation of each number
// - It doesn't work correctly as provided
// - Your task is to make it work correctly and as fast as possible
// - Points are given for correctness, speed, and style

#include <stdio.h>
#include <tchar.h>
#include <time.h>
#include <windows.h>
#include <winbase.h>

void SortIntsByBitCount(unsigned int* intArray, int count);
unsigned int BitCount(unsigned int);

/// Create a Timer, which will immediately begin counting
/// up from 0.0 seconds.
/// You can call reset() to make it start over.
class Timer {
public:
	Timer() {
		reset();
	}
	/// reset() makes the timer start over counting from 0.0 seconds.
	void reset() {
		unsigned __int64 pf;
		QueryPerformanceFrequency( (LARGE_INTEGER *)&pf );
		freq_ = 1.0 / (double)pf;
		QueryPerformanceCounter( (LARGE_INTEGER *)&baseTime_ );
	}
	/// seconds() returns the number of seconds (to very high resolution)
	/// elapsed since the timer was last created or reset().
	double seconds() {
		unsigned __int64 val;
		QueryPerformanceCounter( (LARGE_INTEGER *)&val );
		return (val - baseTime_) * freq_;
	}
	/// seconds() returns the number of milliseconds (to very high resolution)
	/// elapsed since the timer was last created or reset().
	double milliseconds() {
		return seconds() * 1000.0;
	}
private:
	double freq_;
	unsigned __int64 baseTime_;
};

int _tmain(int argc, _TCHAR* argv[])
{
	const int PROBLEM_SIZE = 65536;
	unsigned int intArray[PROBLEM_SIZE];

	for (int i = 0; i < PROBLEM_SIZE; ++i)
	{
		intArray[i] = rand();
	}

	Timer timer;

	SortIntsByBitCount(intArray, PROBLEM_SIZE);

	double elapsed = timer.milliseconds();

	for (int i = 0; i < PROBLEM_SIZE; ++i)
	{
		printf("%d, %d\n", intArray[i], BitCount(intArray[i]));
	}

	printf ("\n\nElapsed CPU time test:   %.9f  milliseconds", elapsed);

	getchar();

	return 0;
}

