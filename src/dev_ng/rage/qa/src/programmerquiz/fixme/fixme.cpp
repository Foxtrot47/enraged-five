//
// programmerquiz/fixme/fixme.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

// Instructions to candidate:

// - Find as many things wrong with the example as you can and fix them
// - You may not modify main.cpp
// - SortIntsByCount is *supposed* to sort an array by the number of 1 bits in the binary representation of each number
// - It doesn't work correctly as provided
// - Your task is to make it work correctly and as fast as possible
// - Points are given for correctness, speed, and style

unsigned int BitCount(unsigned int i)
{
	int count = 0;

	while (i != 0)
	{
		if (i | 1)
		{
			count++;
		}

		i >>= 1;
	}

	return count;
}

void SortIntsByBitCount(unsigned int* intArray, int count)
{
	for (int i = 0; i <= count; i += 1)
	{
		for (int j = 0; j <= count; j += 1)
		{
			if (BitCount(intArray[i] <= intArray[j]))
			{
				unsigned int temp = intArray[i];
				intArray[i] = intArray[j];
				intArray[j] = temp;
			}
		}
	}
}
