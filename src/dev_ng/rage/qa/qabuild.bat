@ECHO OFF

SETLOCAL
setlocal enableextensions

IF "%1"=="" %0 /build

IF "%2"=="" %0 %1 qa\testers.txt

IF NOT EXIST "%2" (
	ECHO %2 not found.  Try running as qa\qabuild from toplevel rage directory.
	EXIT /B
)


SET BUILDTYPE=%1
SET LISTFILE=%2
SET SKIPTO=%3
SET BUILDLOG=%TEMP%\qa.txt
SET TIMELOG=C:\qabuild_times.txt
SET XBECOPY_SUPPRESS_COPY=1

ECHO --------------------------------------------------------------------------------------------------------- >> %TIMELOG%
ECHO -----------------  Build started at %TIME%  --------------------------------------------------------      >> %TIMELOG%
ECHO --------------------------------------------------------------------------------------------------------- >> %TIMELOG%

REM Enable SN DBS, requires VSI.Net 1.5.2 or higher.
SET VSI_USE_DBS=1

IF "%BUILDTYPE%"=="/deploy" (
	ECHO *** Xenon deployment enabled.
	SET BUILDTYPE=/build
	SET XBECOPY_SUPPRESS_COPY=
)

IF "%BUILDTYPE%"=="/check" (
	ECHO *** Verifying .sln files exist.
	FOR /F "delims=, tokens=1,2,3" %%I IN (%LISTFILE%) DO (
		IF NOT EXIST %%I\%%J.sln ECHO %%I\%%J.sln does not exist.
	)
	EXIT/B
)

IF EXIST %BUILDLOG% DEL %BUILDLOG%

SET DEFAULT_BUILDS="Debug Win32" "Release Win32" "Debug Xenon" "SN PS3SNC Debug Win32"

SET USE_IB=1
REM Stole this trick from minimonkey, thanks Wil!
FOR %%F IN (BuildConsole.exe) DO IF "%%~$PATH:F"=="" (
	ECHO Incredibuild not found!
	SET USE_IB=0
)

REM Make sure command-line compiler and devenv are available
IF "%VS80COMNTOOLS%"=="" (
	ECHO VS.Net 2005 NOT installed or is being ignored.
	CALL "%VS71COMNTOOLS%\vsvars32.bat" > NUL
	SET USE_2005=NO
) ELSE (
	ECHO VS.Net 2005 installed.
	CALL "%VS80COMNTOOLS%\vsvars32.bat" > NUL
	SET USE_2005=YES
)

SET VSIBUILD=YES
IF NOT EXIST "%SN_COMMON_PATH%\VSI\BIN\VSIBUILD.EXE" (
	ECHO VsiBuild not found, you need VSI 1.7.3 or higher!  Using old method.
	SET VSIBUILD=NO
)


SET TOTAL=0
SET FAILED=0
SET DISABLED=0

REM Add ourselves to the path so we can find local copies
REM of special tools after we've switched to the project directories.
PATH %CD%\qa;%PATH%

setlocal ENABLEDELAYEDEXPANSION 

FOR /F "delims=, tokens=1,2,3" %%I IN (%LISTFILE%) DO (
  REM see if builds are overridden:
  if "%%K"=="" ( 
	set BUILDS=%DEFAULT_BUILDS% 
  ) else (
	set BUILDS=%%K
  )

  IF NOT "!SKIPTO!"=="" (
	IF !SKIPTO!==%%J (
		ECHO Skipping to...
		SET SKIPTO=
	)
  )
  IF "!SKIPTO!"=="" (
	SET PROJ_NAME=%%J
	SET NET_VERSION=2003
	IF %USE_2005%==YES IF EXIST %%I\%%J_2005.vcproj (
		SET PROJ_NAME=%%J_2005
		SET NET_VERSION=2005
	)
	SET SLN_NAME=!PROJ_NAME!.sln

	ECHO *** %%I\!PROJ_NAME!:

	IF !BUILDS!==DISABLED (
			ECHO ***     DISABLED
			SET /A DISABLED=DISABLED+1
	) ELSE (
	  FOR %%B IN (!BUILDS!) DO (
		ECHO ***     %%B
		PUSHD %%I
		REM LNK4204 is because /IGNORE:4204 doesn't work correctly!

		SET USE_IB_THIS_TIME=%USE_IB%
		IF %%B=="SN PS3 Debug Win32" SET USE_IB_THIS_TIME=0
		IF %%B=="SN PS3 Beta Win32" SET USE_IB_THIS_TIME=0
		IF %%B=="SN PS3 Release Win32" SET USE_IB_THIS_TIME=0
		IF %%B=="SN PS3 PreRelease Win32" SET USE_IB_THIS_TIME=0
		IF %%B=="SN PS3 Final Win32" SET USE_IB_THIS_TIME=0

		IF %%B=="SN PS3SNC Debug Win32" SET USE_IB_THIS_TIME=0
		IF %%B=="SN PS3SNC Beta Win32" SET USE_IB_THIS_TIME=0
		IF %%B=="SN PS3SNC Release Win32" SET USE_IB_THIS_TIME=0
		IF %%B=="SN PS3SNC PreRelease Win32" SET USE_IB_THIS_TIME=0
		IF %%B=="SN PS3SNC Final Win32" SET USE_IB_THIS_TIME=0

		SET CFG=%%B
		REM Translate build configs between 2003 and 2005.
		IF !NET_VERSION!==2005 (
			IF !CFG!=="Debug Win32" SET CFG="Debug|Win32"
			IF !CFG!=="Beta Win32" SET CFG="Beta|Win32"
			IF !CFG!=="Release Win32" SET CFG="Release|Win32"
			IF !CFG!=="Final Win32" SET CFG="Final|Win32"

			IF !CFG!=="Debug Xenon" SET CFG="Debug|Xbox 360"
			IF !CFG!=="Beta Xenon" SET CFG="Beta|Xbox 360"
			IF !CFG!=="Release Xenon" SET CFG="Release|Xbox 360"
			IF !CFG!=="Final Xenon" SET CFG="Final|Xbox 360"

			IF !CFG!=="ToolDebug Win32" SET CFG="Debug|Tool"
			IF !CFG!=="ToolBeta Win32" SET CFG="Beta|Tool"
			IF !CFG!=="ToolRelease Win32" SET CFG="Release|Tool"

			IF !CFG!=="SN PS3 Debug Win32" SET CFG="Debug|PS3SNC"
			IF !CFG!=="SN PS3 Beta Win32" SET CFG="Beta|PS3SNC"
			IF !CFG!=="SN PS3 Release Win32" SET CFG="Release|PS3SNC"
			IF !CFG!=="SN PS3 Final Win32" SET CFG="Final|PS3SNC"

			IF !CFG!=="SN PS3SNC Debug Win32" SET CFG="Debug|PS3SNC"
			IF !CFG!=="SN PS3SNC Beta Win32" SET CFG="Beta|PS3SNC"
			IF !CFG!=="SN PS3SNC Release Win32" SET CFG="Release|PS3SNC"
			IF !CFG!=="SN PS3SNC Final Win32" SET CFG="Final|PS3SNC"
		)

		REM DOS sucks so much for not having a 'system' command. But you can trick the 'for' command into doing something similar.
		for /f "tokens=1-4" %%w in (
		'C:\usr\local\bin\date.exe "+ %%j %%k %%M %%S"'
		) do (
		set /a STARTTIME= %%w * 86400 + %%x * 3600 + %%y * 60 + %%z
		)
		

		IF !USE_IB_THIS_TIME!==1 (
			ECHO Building %%I\!SLN_NAME! !CFG! with Incredibuild.
			BuildConsole.exe !SLN_NAME! %BUILDTYPE% /prj="!PROJ_NAME!" /cfg=!CFG! /all | qa-tee -a %BUILDLOG% | qa-egrep -v "(LNK4204|X1002|LNK4099)" | qa-egrep "(: error|: fatal error|: warning|:  error|:  fatal error|:  warning)"
		) ELSE IF !VSIBUILD!==YES (
			ECHO Building %%I\!SLN_NAME! !CFG! with vsibuild using Incredibuild.
			"%SN_COMMON_PATH%\vsi\bin\vsibuild" %CD%\%%I\!SLN_NAME! !CFG! %BUILDTYPE% /incredi | qa-tee -a %BUILDLOG% | qa-egrep -v "(LNK4204|X1002|LNK4099|L0134)" | qa-egrep "(: error|: fatal error|: warning|:  error|:  fatal error|:  warning|: undefined reference)"
		) ELSE (
			ECHO Building %%I\!SLN_NAME! !CFG! with devenv.
			devenv !SLN_NAME! %BUILDTYPE% !CFG! | qa-tee -a %BUILDLOG% | qa-egrep -v "(LNK4204|X1002|LNK4099)" | qa-egrep "(: error|: fatal error|: warning|:  error|:  fatal error|:  warning|: undefined reference)"
		)
		for /f "tokens=1-4" %%w in (
		'C:\usr\local\bin\date.exe "+ %%j %%k %%M %%S"'
		) do (
		set /a ENDTIME= %%w * 86400 + %%x * 3600 + %%y * 60 + %%z
		)

		set /a TOTALTIME=!ENDTIME! - !STARTTIME!
		echo !SLN_NAME! !CFG! : !TOTALTIME! seconds >> %TIMELOG%


		REM ERRORLEVEL test looks backward here because we're actually
		REM testing the result of the egrep, not the devenv.
		IF ERRORLEVEL 1 (
			REM ECHO Success: !SLN_NAME! - !CFG!
		) ELSE (
			REM ECHO Failed: !SLN_NAME! - !CFG!
			SET /A FAILED=FAILED+1
			REM Print out a separator line so that errors are
			REM easier to see.
			ECHO *** %%I\!SLN_NAME! !CFG! FAILED.
			ECHO *** file://c:\soft\rage\%%I\!SLN_NAME!
			ECHO ***
		)
		POPD
		SET /A TOTAL=TOTAL+1
	  )
	)
  )
)

ECHO "----------------------------- Build finished at %TIME% ------------------" >> %TIMELOG%


ECHO *** 
ECHO *** %FAILED% of %TOTAL% builds failed.  %DISABLED% temporarily disabled.
ECHO *** See build log at %BUILDLOG%
ECHO ***
