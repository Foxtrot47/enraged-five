// ======
// glow.h
// ======

#ifndef _GLOW_H_
#define _GLOW_H_

namespace Proto1 {

float *Glow(float *dst, const float *src, int w, int h, int numPasses);//, int bits = 16, int numThreads = 2, bool noSIMD = false);

} // namespace Proto1

#endif // _GLOW_H_
