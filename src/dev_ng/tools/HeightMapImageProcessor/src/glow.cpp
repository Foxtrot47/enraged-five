// ========
// glow.cpp
// ========

#include "../../common/common.h"

namespace Proto1 {

// format conversion (required if filter is implemented with non-float types)
static __forceinline void Quantise(float &dst, float f, int bits) { dst = f; }
static __forceinline void Unquantise(float &dst, float x, int bits) { dst = x; }

template <typename T> static __forceinline T     AvgBias_       () { return T(1); }
template <>           static __forceinline float AvgBias_<float>() { return 0.0f; }

template <typename T> static __forceinline T Avg_(T a, T b) { return (a + b + AvgBias_<T>())/T(2); }

template <typename T> static __forceinline void Transpose(T *dst, const T *src, int w, int h)
{
    for (int y = 0; y < h; ++y)
    {
        T *col = &dst[y];

        for (int x = 0; x < w; ++x)
        {
            *col = *(src++);
            col += h;
        }
    }
}

template <typename T> static void Glow1DFIR(T *dst, const T *ref, int w)
{
	T v0 = T(0);
	T v1 = dst[0];

	for (int x = 1; x < w; ++x)
	{
		const T r = ref[x - 1];
		T v2 = dst[x];

	//	dst[x - 1] = Max<T>(Avg_<T>(v0, v2), r);
		dst[x - 1] = ((r == -1.0f) ? Avg_<T>(v0, v2) : r);

		v0 = v1;
		v1 = v2;
	}

	// last element
	{
		const T r = ref[w - 1];
		T v2 = T(0);

	//	dst[w - 1] = Max<T>(Avg_<T>(v0, v2), r);
		dst[w - 1] = ((r == -1.0f) ? Avg_<T>(v0, v2) : r);
	}
}

template <typename T> static void Glow1DIIR(T *dst, const T *ref, int w)
{
	T v0 = dst[0];

	for (int x = 1; x < w; ++x)
	{
		const T r = ref[x - 1];
		T v2 = dst[x];

	//	dst[x - 1] = v0 = Max<T>(Avg_<T>(v0, v2), r);
		dst[x - 1] = v0 = ((r == -1.0f) ? Avg_<T>(v0, v2) : r);
	}

	// last element
	{
		const T r = ref[w - 1];
		T v2 = T(0);

	//	dst[w - 1] = v0 = Max<T>(Avg_<T>(v0, v2), r);
		dst[w - 1] = v0 = ((r == -1.0f) ? Avg_<T>(v0, v2) : r);
	}
}

template <typename T> static void Glow2DFIR(T *dst, const T *ref, int w, int h)
{
	for (int y = 0; y < h; ++y, dst += w, ref += w)
	{
		Glow1DFIR<T>(dst, ref, w);
	}
}

template <typename T> static void Glow2DIIR(T *dst, const T *ref, int w, int h)
{
	for (int y = 0; y < h; ++y, dst += w, ref += w)
	{
		Glow1DIIR<T>(dst, ref, w);
	}
}

template <typename T> static float *Glow2D(float *dst, const float *src, int w, int h, int numPasses, int bits = 32, bool useFIR = false, bool horizOnly = false)
{
	int area = w*h;
	T *tmp1 = new T[area];
	T *ref1 = new T[area];
	T *tmp2 = NULL;
	T *ref2 = NULL;

	for (int i = 0; i < area; ++i)
	{
		Quantise(tmp1[i], src[i], bits);
		Quantise(ref1[i], src[i], bits);
	}

	if (!horizOnly)
	{
		tmp2 = new T[area];
		ref2 = new T[area];

		Transpose<T>(ref2, ref1, w, h);
	}

	if (useFIR)
	{
		for (int pass = 1; pass <= numPasses; ++pass)
		{
			Glow2DFIR<T>(tmp1, ref1, w, h);

			if (!horizOnly)
			{
				Transpose<T>(tmp2, tmp1, w, h);
				Glow2DFIR<T>(tmp2, ref2, h, w);
				Transpose<T>(tmp1, tmp2, h, w);
			}
		}
	}
	else // IIR
	{
		for (int pass = 1; pass <= numPasses; ++pass)
		{
			Glow2DIIR<T>(tmp1, ref1, w, h);

			if (!horizOnly)
			{
				Transpose<T>(tmp2, tmp1, w, h);
				Glow2DIIR<T>(tmp2, ref2, h, w);
				Transpose<T>(tmp1, tmp2, h, w);
			}
		}
	}

	for (int i = 0; i < area; ++i)
	{
		Unquantise(dst[i], tmp1[i], bits);
	}

	delete[] tmp1;
	delete[] ref1;

	if (!horizOnly)
	{
		delete[] tmp2;
		delete[] ref2;
	}

	return dst;
}

float *Glow(float *dst, const float *src, int w, int h, int numPasses)//, int bits, int numThreads, bool noSIMD)
{
	return Glow2D<float>(dst, src, w, h, numPasses);
}

} // namespace Proto1
