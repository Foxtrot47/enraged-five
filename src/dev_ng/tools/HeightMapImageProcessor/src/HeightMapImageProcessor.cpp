// ===========================
// HeightMapImageProcessor.cpp
// ===========================

#include "../../common/common.h"
#include "../../common/dds.h"
#include "../../common/fileutil.h"
//#include "glow.h"

#include "../../../rage/framework/src/fwgeovis/geovis.h"

#include <math.h>
#include <float.h>
#include <time.h>

#define BIT(n) (1<<(n))
#define BIT64(n) (1ULL<<(n))

// http://paulbourke.net/miscellaneous/interpolation/
class BestFitLine
{
public:
	BestFitLine()
	{
		memset(this, 0, sizeof(*this));
	}

	void AddPoint(float x, float y)
	{
		m_sum_x  += x;
		m_sum_y  += y;
		m_sum_xx += x*x;
		m_sum_yy += y*y;
		m_sum_xy += x*y;
		m_sum_1  += 1.0f;
	}

	float GetLine(float& a, float& b) const
	{
		a = 0.0f;
		b = 0.0f;

		if (m_sum_1 >= 2.0f)
		{
			const float q = 1.0f/m_sum_1;

			const float sxx = m_sum_xx - m_sum_x*m_sum_x*q;
			const float syy = m_sum_yy - m_sum_y*m_sum_y*q;
			const float sxy = m_sum_xy - m_sum_x*m_sum_y*q;

			if (Abs<float>(sxx) != 0.0f)
			{
				b = sxy/sxx;
				a = (m_sum_y - b*m_sum_x)*q;

				if (Abs<float>(syy) != 0.0f)
				{
					return 1.0f;
				}
				else
				{
					return sxy/sqrtf(sxx*syy);
				}
			}
		}

		return 0.0f;
	}

private:
	float m_sum_x;
	float m_sum_y;
	float m_sum_xx;
	float m_sum_yy;
	float m_sum_xy;
	float m_sum_1;
};

class varString : public std::string
{
public:
	inline varString(const char* format, ...)
	{
		char temp[8192] = "";
		va_list args;
		va_start(args, format);
		vsnprintf(temp, sizeof(temp), format, args);
		va_end(args);

		std::string::operator=(temp);
	}

	inline operator const char*() const
	{
		return std::string::c_str();
	}
};

static void ByteSwapData(void* data, int dataSize, int swapSize)
{
	u8* data0 = (u8*)data;
	u8* data1 = data0 + dataSize;

	if (swapSize == 2)
	{
		for (; data0 < data1; data0 += 2)
		{
			const u8 swap[2] = {data0[0], data0[1]};

			data0[0] = swap[1];
			data0[1] = swap[0];
		}
	}
	else if (swapSize == 4)
	{
		for (; data0 < data1; data0 += 4)
		{
			const u8 swap[4] = {data0[0], data0[1], data0[2], data0[3]};

			data0[0] = swap[3];
			data0[1] = swap[2];
			data0[2] = swap[1];
			data0[3] = swap[0];
		}
	}
}

/*template <typename T> static void ByteSwap(T& data)
{
	ByteSwapData(&data, sizeof(data), sizeof(data));
}*/

static bool ReadFileLine(char* line, int lineMax, FILE* fp)
{
	if (fgets(line, lineMax, fp))
	{
		char temp[4096] = "";
		strcpy(temp, line);
		const char* start = temp;
		while (*start == ' ' || *start == '\t')
		{
			start++; // skip leading whitespace
		}
		strcpy(line, start);
		if (strrchr(line, '\r')) { strrchr(line, '\r')[0] = '\0'; } // remove trailing newline
		if (strrchr(line, '\n')) { strrchr(line, '\n')[0] = '\0'; }
		return true;
	}

	return false;
}

// %RS_CODEBRANCH%\rage\base\src\file\stream.cpp
static int rage_fgetline(char* dest, int maxSize, FILE* S)
{
	int stored;
	do {
		if (fgets(dest,maxSize,S))
			stored = (int)strlen(dest);
		else
			stored = 0;
		if (stored == 0)
			break; // real EOF
		while (stored && dest[stored-1] <= 32)
			--stored;
		dest[stored] = 0;
		// consume empty lines:
	} while (!stored);
	return stored;
}

static void __system(const char* format, ...)
{
	char cmd[8192] = "";
	va_list args;
	va_start(args, format);
	vsnprintf(cmd, sizeof(cmd), format, args);
	va_end(args);

	for (char* s = cmd; *s; s++)
	{
		if (*s == '/') { *s = '\\'; }
		if (*s == '$') { *s = '/'; }
	}

	system(cmd);
}

static void __mkdir(const char* dir, bool bDeleteAllFiles = false)
{
	if (bDeleteAllFiles)
	{
		__system("if exist %s del   $S $Q %s/*.* > nul", dir, dir);
		__system("if exist %s rmdir $S $Q %s     > nul", dir, dir);
	}

	__system("if not exist %s mkdir %s > nul", dir, dir);
}

class ProgressDisplay
{
public:
	ProgressDisplay() : m_progress(0.0f), m_numChars(0), m_clock(0) {}
	ProgressDisplay(const char* format, ...) : m_progress(0.0f), m_numChars(0), m_clock(0)
	{
		char temp[1024] = "";
		va_list args;
		va_start(args, format);
		vsnprintf(temp, sizeof(temp), format, args);
		va_end(args);

		fprintf(stdout, "%s .. ", temp);
		fflush(stdout);

		Update(-1, 1);

		m_clock = clock();

		m_remainingTimeEnabled = true; // TODO -- fix this, it's not quite working right
		m_remainingTime = 0;
		m_remainingTimeStr[0] = '\0';
	}

	void Update(float f)
	{
		m_progress = f;
		char temp[64] = "";
		sprintf(temp, "%.3f%%", 100.0f*m_progress);
		Erase(m_numChars);
		fprintf(stdout, "%s", temp);
		fflush(stdout);
		m_numChars = (int)strlen(temp);

		if (m_remainingTimeEnabled)
		{
			const float elapsed = (float)(clock() - m_clock)/(float)CLOCKS_PER_SEC;

			if (elapsed > 5.0f && m_progress > 0.0f)
			{
				if (clock() - m_remainingTime > CLOCKS_PER_SEC)
				{
					const float secs = elapsed*(1.0f - m_progress)/m_progress;
					const float mins = secs/60.0f;
					const float hrs  = mins/60.0f;

					if      (secs <= 100.0f) { sprintf(m_remainingTimeStr, " (%.2f secs remaining)", secs); }
					else if (mins <= 100.0f) { sprintf(m_remainingTimeStr, " (%.2f mins remaining)", mins); }
					else                     { sprintf(m_remainingTimeStr, " (%.2f hours remaining)", hrs); }

					m_remainingTime = clock();
				}

				fprintf(stdout, m_remainingTimeStr);
				fflush(stdout);
				m_numChars += (int)strlen(m_remainingTimeStr);
			}
		}
	}

	void Update(int i, int n)
	{
		Update((float)(i + 1)/(float)n);
	}

	template <typename T> void Update(int i, const T& v)
	{
		Update(i, (int)v.size());
	}

	void End(const char* msg = "done", bool bShowTime = true)
	{
		Erase(m_numChars);

		fprintf(stdout, "%s.", msg);

		if (bShowTime)
		{
			char totalStr[64] = "";

			if (1) // show total
			{
				static clock_t total = 0;

				total += clock() - m_clock;

				const float totalSecs = (float)total/(float)CLOCKS_PER_SEC;
				const float totalMins = totalSecs/60.0f;
				const float totalHrs  = totalMins/60.0f;

				if      (totalSecs <=   1.0f) { sprintf(totalStr, ""); }
				else if (totalSecs <= 100.0f) { sprintf(totalStr, ", total %.2f secs", totalSecs); }
				else if (totalMins <= 100.0f) { sprintf(totalStr, ", total %.2f mins", totalMins); }
				else                          { sprintf(totalStr, ", total %.2f hours", totalHrs); }
			}

			const float secs = (float)(clock() - m_clock)/(float)CLOCKS_PER_SEC;
			const float mins = secs/60.0f;
			const float hrs  = mins/60.0f;

			if      (secs <=   1.0f) { fprintf(stdout, ""); }
			else if (secs <= 100.0f) { fprintf(stdout, " (%.2f secs%s)", secs, totalStr); }
			else if (mins <= 100.0f) { fprintf(stdout, " (%.2f mins%s)", mins, totalStr); }
			else                     { fprintf(stdout, " (%.2f hours%s)", hrs, totalStr); }
		}

		if (m_remainingTimeEnabled)
		{
			fprintf(stdout, "                             \n"); // extra space to clear leftover chars
		}
		else
		{
			fprintf(stdout, "    \n"); // extra space to clear leftover chars
		}
	}

protected:
	static void Erase(int numChars)
	{
		for (int k = 0; k < numChars; k++)
		{
			fprintf(stdout, "%c", 0x08);
		}

		fflush(stdout);
	}

	float   m_progress;
	int     m_numChars;
	clock_t m_clock;

	bool    m_remainingTimeEnabled;
	clock_t m_remainingTime;
	char    m_remainingTimeStr[64];
};

class ProgressDisplayAuto : public ProgressDisplay
{
public:
	ProgressDisplayAuto(const char* format, ...)
	{
		char temp[1024] = "";
		va_list args;
		va_start(args, format);
		vsnprintf(temp, sizeof(temp), format, args);
		va_end(args);

		fprintf(stdout, "%s .. ", temp);
		fflush(stdout);

		//Update(-1, 1);

		m_clock = clock();
	}

	~ProgressDisplayAuto()
	{
		End();
	}
};

class Vec3
{
public:
	Vec3() {}
	Vec3(float x_, float y_, float z_) : x(x_), y(y_), z(z_) {}
	Vec3(float f_) : x(f_), y(f_), z(f_) {}

	float x, y, z;
};

class Pixel32
{
public:
	Pixel32() {}
	Pixel32(int r_, int g_, int b_, int a_) : r(r_), g(g_), b(b_), a(a_) {}

	bool operator ==(const Pixel32& rhs) const { return r == rhs.r && g == rhs.g && b == rhs.b && a == rhs.a; }
	bool operator !=(const Pixel32& rhs) const { return r != rhs.r || g != rhs.g || b != rhs.b || a != rhs.a; }

	u32 b:8, g:8, r:8, a:8;
};

static bool GetImageDimensionsDDS(const char* path, int& w, int& h)
{
	if (strstr(path, ".dds"))
	{
		FILE* ddsFile = fopen(path, "rb");

		if (ddsFile)
		{
			DDSURFACEDESC2 ddsHeader;

			fseek(ddsFile, 4, SEEK_SET); // skip past tag
			fread(&ddsHeader, sizeof(DDSURFACEDESC2), 1, ddsFile);
			fclose(ddsFile);

			if (ddsHeader.dwSize == sizeof(DDSURFACEDESC2))
			{
				w = (int)ddsHeader.dwWidth;
				h = (int)ddsHeader.dwHeight;

				return true;
			}
		}
	}

	return false;
}

static float* LoadImageDDS(const char* path, int& w, int& h)
{
	float* image = NULL;

	if (strstr(path, ".dds"))
	{
		FILE* ddsFile = fopen(path, "rb");

		if (ddsFile)
		{
			fseek(ddsFile, 0, SEEK_END);
			const u32 ddsSize = (u32)ftell(ddsFile);
			fseek(ddsFile, 0, SEEK_SET);

			if (ddsSize > sizeof(DDSURFACEDESC2))
			{
				u8* ddsData = new u8[ddsSize];
				fread(ddsData, ddsSize, 1, ddsFile);

				const DDSURFACEDESC2* ddsHeader = (DDSURFACEDESC2*)(ddsData + 4);
				const void*           ddsPixels = (const u8*)ddsHeader + ddsHeader->dwSize;

				assert(ddsHeader->dwSize == sizeof(DDSURFACEDESC2));

				if ((w == 0 && h == 0) || (w == (int)ddsHeader->dwWidth && h == (int)ddsHeader->dwHeight))
				{
					w = (int)ddsHeader->dwWidth;
					h = (int)ddsHeader->dwHeight;

					if (GetDDSFormat(ddsHeader->ddpfPixelFormat) == DDS_D3DFMT_A8R8G8B8 && ddsSize >= ddsHeader->dwSize + w*h*sizeof(Pixel32))
					{
						const Pixel32* pixels = (const Pixel32*)ddsPixels;
						image = new float[w*h];

						for (int i = 0; i < w*h; i++)
						{
							image[i] = (float)pixels[i].r/255.0f;
						}
					}

					if (GetDDSFormat(ddsHeader->ddpfPixelFormat) == DDS_D3DFMT_L8 && ddsSize >= ddsHeader->dwSize + w*h*sizeof(u8))
					{
						const u8* pixels = (const u8*)ddsPixels;
						image = new float[w*h];

						for (int i = 0; i < w*h; i++)
						{
							image[i] = (float)pixels[i]/255.0f;
						}
					}

					if (GetDDSFormat(ddsHeader->ddpfPixelFormat) == DDS_D3DFMT_L16 && ddsSize >= ddsHeader->dwSize + w*h*sizeof(u16))
					{
						const u16* pixels = (const u16*)ddsPixels;
						image = new float[w*h];

						for (int i = 0; i < w*h; i++)
						{
							image[i] = (float)pixels[i]/65535.0f;
						}
					}

					if (GetDDSFormat(ddsHeader->ddpfPixelFormat) == DDS_D3DFMT_R32F && ddsSize >= ddsHeader->dwSize + w*h*sizeof(float))
					{
						const float* pixels = (const float*)ddsPixels;
						image = new float[w*h];

						for (int i = 0; i < w*h; i++)
						{
							image[i] = pixels[i];
						}
					}
				}

				delete[] ddsData;
			}

			fclose(ddsFile);
		}
	}

	return image;
}

static float* LoadImageDDSSpan(const char* path, int w, int y0, int h)
{
	float* image = NULL;

	if (strstr(path, ".dds"))
	{
		FILE* ddsFile = fopen(path, "rb");

		if (ddsFile)
		{
			fseek(ddsFile, 0, SEEK_END);
			const u32 ddsSize = (u32)ftell(ddsFile);
			fseek(ddsFile, 0, SEEK_SET);

			if (ddsSize > sizeof(DDSURFACEDESC2))
			{
				u8* ddsData = new u8[ddsSize];
				fread(ddsData, ddsSize, 1, ddsFile);

				const DDSURFACEDESC2* ddsHeader = (DDSURFACEDESC2*)(ddsData + 4);
				const void*           ddsPixels = (const u8*)ddsHeader + ddsHeader->dwSize;

				assert(ddsHeader->dwSize == sizeof(DDSURFACEDESC2));

				if (w == (int)ddsHeader->dwWidth && y0 >= 0 && y0 + h <= (int)ddsHeader->dwHeight)
				{
					if (GetDDSFormat(ddsHeader->ddpfPixelFormat) == DDS_D3DFMT_A8R8G8B8 && ddsSize >= ddsHeader->dwSize + w*h*sizeof(Pixel32))
					{
						const Pixel32* pixels = (const Pixel32*)ddsPixels + w*y0;
						image = new float[w*h];

						for (int i = 0; i < w*h; i++)
						{
							image[i] = (float)pixels[i].r/255.0f;
						}
					}

					if (GetDDSFormat(ddsHeader->ddpfPixelFormat) == DDS_D3DFMT_L8 && ddsSize >= ddsHeader->dwSize + w*h*sizeof(u8))
					{
						const u8* pixels = (const u8*)ddsPixels + w*y0;
						image = new float[w*h];

						for (int i = 0; i < w*h; i++)
						{
							image[i] = (float)pixels[i]/255.0f;
						}
					}

					if (GetDDSFormat(ddsHeader->ddpfPixelFormat) == DDS_D3DFMT_L16 && ddsSize >= ddsHeader->dwSize + w*h*sizeof(u16))
					{
						const u16* pixels = (const u16*)ddsPixels + w*y0;
						image = new float[w*h];

						for (int i = 0; i < w*h; i++)
						{
							image[i] = (float)pixels[i]/65535.0f;
						}
					}

					if (GetDDSFormat(ddsHeader->ddpfPixelFormat) == DDS_D3DFMT_R32F && ddsSize >= ddsHeader->dwSize + w*h*sizeof(float))
					{
						const float* pixels = (const float*)ddsPixels + w*y0;
						image = new float[w*h];

						for (int i = 0; i < w*h; i++)
						{
							image[i] = pixels[i];
						}
					}
				}

				delete[] ddsData;
			}

			fclose(ddsFile);
		}
	}

	return image;
}

static u8* LoadImageU8DDS(const char* path, int& w, int& h)
{
	u8* image = NULL;

	if (strstr(path, ".dds"))
	{
		FILE* ddsFile = fopen(path, "rb");

		if (ddsFile)
		{
			fseek(ddsFile, 0, SEEK_END);
			const u32 ddsSize = (u32)ftell(ddsFile);
			fseek(ddsFile, 0, SEEK_SET);

			if (ddsSize > sizeof(DDSURFACEDESC2))
			{
				u8* ddsData = new u8[ddsSize];
				fread(ddsData, ddsSize, 1, ddsFile);

				const DDSURFACEDESC2* ddsHeader = (DDSURFACEDESC2*)(ddsData + 4);
				const void*           ddsPixels = (const u8*)ddsHeader + ddsHeader->dwSize;

				assert(ddsHeader->dwSize == sizeof(DDSURFACEDESC2));

				if ((w == 0 && h == 0) || (w == (int)ddsHeader->dwWidth && h == (int)ddsHeader->dwHeight))
				{
					w = (int)ddsHeader->dwWidth;
					h = (int)ddsHeader->dwHeight;

					if (GetDDSFormat(ddsHeader->ddpfPixelFormat) == DDS_D3DFMT_A8R8G8B8 && ddsSize >= ddsHeader->dwSize + w*h*sizeof(Pixel32))
					{
						const Pixel32* pixels = (const Pixel32*)ddsPixels;
						image = new u8[w*h];

						for (int i = 0; i < w*h; i++)
						{
							image[i] = pixels[i].r;
						}
					}

					if (GetDDSFormat(ddsHeader->ddpfPixelFormat) == DDS_D3DFMT_L8 && ddsSize >= ddsHeader->dwSize + w*h*sizeof(u8))
					{
						const u8* pixels = (const u8*)ddsPixels;
						image = new u8[w*h];

						for (int i = 0; i < w*h; i++)
						{
							image[i] = pixels[i];
						}
					}

					if (GetDDSFormat(ddsHeader->ddpfPixelFormat) == DDS_D3DFMT_L16 && ddsSize >= ddsHeader->dwSize + w*h*sizeof(u16))
					{
						const u16* pixels = (const u16*)ddsPixels;
						image = new u8[w*h];

						for (int i = 0; i < w*h; i++)
						{
							image[i] = pixels[i]>>8;
						}
					}

					if (GetDDSFormat(ddsHeader->ddpfPixelFormat) == DDS_D3DFMT_R32F && ddsSize >= ddsHeader->dwSize + w*h*sizeof(float))
					{
						const float* pixels = (const float*)ddsPixels;
						image = new u8[w*h];

						for (int i = 0; i < w*h; i++)
						{
							image[i] = (u8)Clamp<float>(0.5f + 255.0f*pixels[i], 0.0f, 255.0f);
						}
					}
				}

				delete[] ddsData;
			}

			fclose(ddsFile);
		}
	}

	return image;
}

static u16* LoadImageU16DDS(const char* path, int& w, int& h)
{
	u16* image = NULL;

	if (strstr(path, ".dds"))
	{
		FILE* ddsFile = fopen(path, "rb");

		if (ddsFile)
		{
			fseek(ddsFile, 0, SEEK_END);
			const u32 ddsSize = (u32)ftell(ddsFile);
			fseek(ddsFile, 0, SEEK_SET);

			if (ddsSize > sizeof(DDSURFACEDESC2))
			{
				u8* ddsData = new u8[ddsSize];
				fread(ddsData, ddsSize, 1, ddsFile);

				const DDSURFACEDESC2* ddsHeader = (DDSURFACEDESC2*)(ddsData + 4);
				const void*           ddsPixels = (const u8*)ddsHeader + ddsHeader->dwSize;

				assert(ddsHeader->dwSize == sizeof(DDSURFACEDESC2));

				if ((w == 0 && h == 0) || (w == (int)ddsHeader->dwWidth && h == (int)ddsHeader->dwHeight))
				{
					w = (int)ddsHeader->dwWidth;
					h = (int)ddsHeader->dwHeight;

					if (GetDDSFormat(ddsHeader->ddpfPixelFormat) == DDS_D3DFMT_A8R8G8B8 && ddsSize >= ddsHeader->dwSize + w*h*sizeof(Pixel32))
					{
						const Pixel32* pixels = (const Pixel32*)ddsPixels;
						image = new u16[w*h];

						for (int i = 0; i < w*h; i++)
						{
							image[i] = pixels[i].r;
							image[i] |= (image[i]<<8);
						}
					}

					if (GetDDSFormat(ddsHeader->ddpfPixelFormat) == DDS_D3DFMT_L8 && ddsSize >= ddsHeader->dwSize + w*h*sizeof(u8))
					{
						const u8* pixels = (const u8*)ddsPixels;
						image = new u16[w*h];

						for (int i = 0; i < w*h; i++)
						{
							image[i] = pixels[i];
							image[i] |= (image[i]<<8);
						}
					}

					if (GetDDSFormat(ddsHeader->ddpfPixelFormat) == DDS_D3DFMT_L16 && ddsSize >= ddsHeader->dwSize + w*h*sizeof(u16))
					{
						const u16* pixels = (const u16*)ddsPixels;
						image = new u16[w*h];

						for (int i = 0; i < w*h; i++)
						{
							image[i] = pixels[i];
						}
					}

					if (GetDDSFormat(ddsHeader->ddpfPixelFormat) == DDS_D3DFMT_R32F && ddsSize >= ddsHeader->dwSize + w*h*sizeof(float))
					{
						const float* pixels = (const float*)ddsPixels;
						image = new u16[w*h];

						for (int i = 0; i < w*h; i++)
						{
							image[i] = (u16)Clamp<float>(0.5f + 65535.0f*pixels[i], 0.0f, 65535.0f);
						}
					}
				}

				delete[] ddsData;
			}

			fclose(ddsFile);
		}
	}

	return image;
}

static Pixel32* LoadImagePixel32DDS(const char* path, int& w, int& h)
{
	Pixel32* image = NULL;

	if (strstr(path, ".dds"))
	{
		FILE* ddsFile = fopen(path, "rb");

		if (ddsFile)
		{
			fseek(ddsFile, 0, SEEK_END);
			const u32 ddsSize = (u32)ftell(ddsFile);
			fseek(ddsFile, 0, SEEK_SET);

			if (ddsSize > sizeof(DDSURFACEDESC2))
			{
				u8* ddsData = new u8[ddsSize];
				fread(ddsData, ddsSize, 1, ddsFile);

				const DDSURFACEDESC2* ddsHeader = (DDSURFACEDESC2*)(ddsData + 4);
				const void*           ddsPixels = (const u8*)ddsHeader + ddsHeader->dwSize;

				assert(ddsHeader->dwSize == sizeof(DDSURFACEDESC2));

				if ((w == 0 && h == 0) || (w == (int)ddsHeader->dwWidth && h == (int)ddsHeader->dwHeight))
				{
					w = (int)ddsHeader->dwWidth;
					h = (int)ddsHeader->dwHeight;

					if (GetDDSFormat(ddsHeader->ddpfPixelFormat) == DDS_D3DFMT_A8R8G8B8 && ddsSize >= ddsHeader->dwSize + w*h*sizeof(Pixel32))
					{
						const Pixel32* pixels = (const Pixel32*)ddsPixels;
						image = new Pixel32[w*h];

						for (int i = 0; i < w*h; i++)
						{
							image[i] = pixels[i];
						}
					}
					else
					{
						assert(0); // we don't support other formats when loading Pixel32 image
					}
				}

				delete[] ddsData;
			}

			fclose(ddsFile);
		}
	}

	return image;
}

static u64* LoadImageU64DDS(const char* path, int& w, int& h)
{
	u64* image = NULL;

	if (strstr(path, ".dds"))
	{
		FILE* ddsFile = fopen(path, "rb");

		if (ddsFile)
		{
			fseek(ddsFile, 0, SEEK_END);
			const u32 ddsSize = (u32)ftell(ddsFile);
			fseek(ddsFile, 0, SEEK_SET);

			if (ddsSize > sizeof(DDSURFACEDESC2))
			{
				u8* ddsData = new u8[ddsSize];
				fread(ddsData, ddsSize, 1, ddsFile);

				const DDSURFACEDESC2* ddsHeader = (DDSURFACEDESC2*)(ddsData + 4);
				const void*           ddsPixels = (const u8*)ddsHeader + ddsHeader->dwSize;

				assert(ddsHeader->dwSize == sizeof(DDSURFACEDESC2));

				if ((w == 0 && h == 0) || (w == (int)ddsHeader->dwWidth && h == (int)ddsHeader->dwHeight))
				{
					w = (int)ddsHeader->dwWidth;
					h = (int)ddsHeader->dwHeight;

					// expect 64-bit DDS to be A16B16G16R16 - we could support other 64-bit formats i suppose .. but i'm only using this for material masks at the moment
					if (GetDDSFormat(ddsHeader->ddpfPixelFormat) == DDS_D3DFMT_A16B16G16R16 && ddsSize >= ddsHeader->dwSize + w*h*sizeof(Pixel32))
					{
						const u64* pixels = (const u64*)ddsPixels;
						image = new u64[w*h];

						for (int i = 0; i < w*h; i++)
						{
							image[i] = pixels[i];
						}
					}
				}

				delete[] ddsData;
			}

			fclose(ddsFile);
		}
	}

	return image;
}

static Vec3* LoadImageRGBDDS(const char* path, int& w, int& h)
{
	return NULL; // TODO
}

static float* LoadImage(const char* path, int& w, int& h)
{
	float* image = LoadImageDDS(path, w, h);

	if (image)
	{
		return image;
	}

	FREE_IMAGE_FORMAT fif = FreeImage_GetFileType(path);
	FIBITMAP *dib = NULL;

	if (fif == FIF_UNKNOWN)
	{
		fif = FreeImage_GetFIFFromFilename(path);
	}

	if (fif != FIF_UNKNOWN && FreeImage_FIFSupportsReading(fif))
	{
		dib = FreeImage_Load(fif, path);
	}

	if (dib && FreeImage_GetBPP(dib) != 32)
	{
		FIBITMAP *dib2 = FreeImage_ConvertTo32Bits(dib);
		FreeImage_Unload(dib);
		dib = dib2;
	}

	if (dib)
	{
		if ((w == 0 && h == 0) || (w == FreeImage_GetWidth(dib) && h == FreeImage_GetHeight(dib)))
		{
			w = FreeImage_GetWidth(dib);
			h = FreeImage_GetHeight(dib);

			image = new float[w*h];

			for (int j = 0; j < h; j++)
			{
				const Pixel32* pixels = reinterpret_cast<const Pixel32*>(FreeImage_GetScanLine(dib, h - j - 1));

				for (int i = 0; i < w; i++)
				{
					image[i + j*w] = (float)pixels[i].r/255.0f;
				}
			}
		}

		FreeImage_Unload(dib);
	}

	return image;
}

static u8* LoadImageU8(const char* path, int& w, int& h)
{
	u8* image = LoadImageU8DDS(path, w, h);

	if (image)
	{
		return image;
	}

	FREE_IMAGE_FORMAT fif = FreeImage_GetFileType(path);
	FIBITMAP *dib = NULL;

	if (fif == FIF_UNKNOWN)
	{
		fif = FreeImage_GetFIFFromFilename(path);
	}

	if (fif != FIF_UNKNOWN && FreeImage_FIFSupportsReading(fif))
	{
		dib = FreeImage_Load(fif, path);
	}

	if (dib && FreeImage_GetBPP(dib) != 32)
	{
		FIBITMAP *dib2 = FreeImage_ConvertTo32Bits(dib);
		FreeImage_Unload(dib);
		dib = dib2;
	}

	if (dib)
	{
		if ((w == 0 && h == 0) || (w == FreeImage_GetWidth(dib) && h == FreeImage_GetHeight(dib)))
		{
			w = FreeImage_GetWidth(dib);
			h = FreeImage_GetHeight(dib);

			image = new u8[w*h];

			for (int j = 0; j < h; j++)
			{
				const Pixel32* pixels = reinterpret_cast<const Pixel32*>(FreeImage_GetScanLine(dib, h - j - 1));

				for (int i = 0; i < w; i++)
				{
					image[i + j*w] = pixels[i].r;
				}
			}
		}

		FreeImage_Unload(dib);
	}

	return image;
}

static Vec3* LoadImageRGB(const char* path, int& w, int& h)
{
	Vec3* image = LoadImageRGBDDS(path, w, h);

	if (image)
	{
		return image;
	}

	FREE_IMAGE_FORMAT fif = FreeImage_GetFileType(path);
	FIBITMAP *dib = NULL;

	if (fif == FIF_UNKNOWN)
	{
		fif = FreeImage_GetFIFFromFilename(path);
	}

	if (fif != FIF_UNKNOWN && FreeImage_FIFSupportsReading(fif))
	{
		dib = FreeImage_Load(fif, path);
	}

	if (dib && FreeImage_GetBPP(dib) != 32)
	{
		FIBITMAP *dib2 = FreeImage_ConvertTo32Bits(dib);
		FreeImage_Unload(dib);
		dib = dib2;
	}

	if (dib)
	{
		if ((w == 0 && h == 0) || (w == FreeImage_GetWidth(dib) && h == FreeImage_GetHeight(dib)))
		{
			w = FreeImage_GetWidth(dib);
			h = FreeImage_GetHeight(dib);

			image = new Vec3[w*h];

			for (int j = 0; j < h; j++)
			{
				const Pixel32* pixels = reinterpret_cast<const Pixel32*>(FreeImage_GetScanLine(dib, h - j - 1));

				for (int i = 0; i < w; i++)
				{
					image[i + j*w].x = (float)pixels[i].r/255.0f;
					image[i + j*w].y = (float)pixels[i].g/255.0f;
					image[i + j*w].z = (float)pixels[i].b/255.0f;
				}
			}
		}

		FreeImage_Unload(dib);
	}

	return image;
}

static Pixel32* LoadImagePixel32(const char* path, int& w, int& h)
{
	Pixel32* image = LoadImagePixel32DDS(path, w, h);

	if (image)
	{
		return image;
	}

	FREE_IMAGE_FORMAT fif = FreeImage_GetFileType(path);
	FIBITMAP *dib = NULL;

	if (fif == FIF_UNKNOWN)
	{
		fif = FreeImage_GetFIFFromFilename(path);
	}

	if (fif != FIF_UNKNOWN && FreeImage_FIFSupportsReading(fif))
	{
		dib = FreeImage_Load(fif, path);
	}

	if (dib && FreeImage_GetBPP(dib) != 32)
	{
		FIBITMAP *dib2 = FreeImage_ConvertTo32Bits(dib);
		FreeImage_Unload(dib);
		dib = dib2;
	}

	if (dib)
	{
		if ((w == 0 && h == 0) || (w == FreeImage_GetWidth(dib) && h == FreeImage_GetHeight(dib)))
		{
			w = FreeImage_GetWidth(dib);
			h = FreeImage_GetHeight(dib);

			image = new Pixel32[w*h];

			for (int j = 0; j < h; j++)
			{
				const Pixel32* pixels = reinterpret_cast<const Pixel32*>(FreeImage_GetScanLine(dib, h - j - 1));

				for (int i = 0; i < w; i++)
				{
					image[i + j*w] = pixels[i];
				}
			}
		}

		FreeImage_Unload(dib);
	}

	return image;
}

static bool SaveImageDDS(const char* path, const float* image, int w, int h, bool bAutoOpen)
{
	if (strstr(path, ".dds"))
	{
		ProgressDisplayAuto progress("saving \"%s\"", path);
		FILE* dds = fopen(path, "wb");

		if (dds)
		{
			const u32 ddsMagic = MAKE_MAGIC_NUMBER('D','D','S',' ');
			DDSURFACEDESC2 ddsHeader;
			memset(&ddsHeader, 0, sizeof(ddsHeader));

			ddsHeader.dwSize = sizeof(ddsHeader);
			ddsHeader.dwFlags = 0x00001007; // DDSD_CAPS | DDSD_HEIGHT | DDSD_WIDTH | DDSD_PIXELFORMAT
			ddsHeader.dwWidth = w;
			ddsHeader.dwHeight = h;
			ddsHeader.ddpfPixelFormat.dwSize = sizeof(ddsHeader.ddpfPixelFormat);
			ddsHeader.ddpfPixelFormat.dwFlags = DDPF_FOURCC;
			ddsHeader.ddpfPixelFormat.dwFourCC = DDS_D3DFMT_R32F;
			ddsHeader.ddsCaps.dwCaps1 = 0x00001002; // DDSCAPS_ALPHA | DDSCAPS_TEXTURE

			fwrite(&ddsMagic, sizeof(ddsMagic), 1, dds);
			fwrite(&ddsHeader, sizeof(ddsHeader), 1, dds);
			fwrite(image, sizeof(float), w*h, dds);
			fclose(dds);

			if (bAutoOpen)
			{
				system(path);
			}

			return true;
		}
		else
		{
			progress.End("failed");
		}
	}

	return false;
}

static bool SaveImageDDS(const char* path, const u8* image, int w, int h, bool bAutoOpen)
{
	if (strstr(path, ".dds"))
	{
		ProgressDisplayAuto progress("saving \"%s\"", path);
		FILE* dds = fopen(path, "wb");

		if (dds)
		{
			const u32 ddsMagic = MAKE_MAGIC_NUMBER('D','D','S',' ');
			DDSURFACEDESC2 ddsHeader;
			memset(&ddsHeader, 0, sizeof(ddsHeader));

			ddsHeader.dwSize = sizeof(ddsHeader);
			ddsHeader.dwFlags = 0x00001007; // DDSD_CAPS | DDSD_HEIGHT | DDSD_WIDTH | DDSD_PIXELFORMAT
			ddsHeader.dwWidth = w;
			ddsHeader.dwHeight = h;
			ddsHeader.ddpfPixelFormat.dwSize = sizeof(ddsHeader.ddpfPixelFormat);
			ddsHeader.ddpfPixelFormat.dwFlags = DDPF_LUMINANCE;
			ddsHeader.ddpfPixelFormat.dwRGBBitCount = 8;
			ddsHeader.ddpfPixelFormat.dwRBitMask = 0x000000ff;
			ddsHeader.ddsCaps.dwCaps1 = 0x00001002; // DDSCAPS_ALPHA | DDSCAPS_TEXTURE

			fwrite(&ddsMagic, sizeof(ddsMagic), 1, dds);
			fwrite(&ddsHeader, sizeof(ddsHeader), 1, dds);
			fwrite(image, sizeof(u8), w*h, dds);
			fclose(dds);

			if (bAutoOpen)
			{
				system(path);
			}

			return true;
		}
		else
		{
			progress.End("failed");
		}
	}

	return false;
}

static bool SaveImageDDS(const char* path, const u16* image, int w, int h, bool bAutoOpen)
{
	if (strstr(path, ".dds"))
	{
		ProgressDisplayAuto progress("saving \"%s\"", path);
		FILE* dds = fopen(path, "wb");

		if (dds)
		{
			const u32 ddsMagic = MAKE_MAGIC_NUMBER('D','D','S',' ');
			DDSURFACEDESC2 ddsHeader;
			memset(&ddsHeader, 0, sizeof(ddsHeader));

			ddsHeader.dwSize = sizeof(ddsHeader);
			ddsHeader.dwFlags = 0x00001007; // DDSD_CAPS | DDSD_HEIGHT | DDSD_WIDTH | DDSD_PIXELFORMAT
			ddsHeader.dwWidth = w;
			ddsHeader.dwHeight = h;
			ddsHeader.ddpfPixelFormat.dwSize = sizeof(ddsHeader.ddpfPixelFormat);
			ddsHeader.ddpfPixelFormat.dwFlags = DDPF_LUMINANCE;
			ddsHeader.ddpfPixelFormat.dwRGBBitCount = 16;
			ddsHeader.ddpfPixelFormat.dwRBitMask = 0x0000ffff;
			ddsHeader.ddsCaps.dwCaps1 = 0x00001002; // DDSCAPS_ALPHA | DDSCAPS_TEXTURE

			fwrite(&ddsMagic, sizeof(ddsMagic), 1, dds);
			fwrite(&ddsHeader, sizeof(ddsHeader), 1, dds);
			fwrite(image, sizeof(u16), w*h, dds);
			fclose(dds);

			if (bAutoOpen)
			{
				system(path);
			}

			return true;
		}
		else
		{
			progress.End("failed");
		}
	}

	return false;
}

static bool SaveImageRGBDDS(const char* path, const Vec3* image, int w, int h, bool bAutoOpen)
{
	if (strstr(path, ".dds"))
	{
		if (0) // TODO
		{
			if (bAutoOpen)
			{
				system(path);
			}
		}
	}

	return false;
}

static bool SaveImagePixel32DDS(const char* path, const Pixel32* image, int w, int h, bool bAutoOpen)
{
	if (strstr(path, ".dds"))
	{
		FILE* dds = fopen(path, "wb");

		if (dds)
		{
			const u32 ddsMagic = MAKE_MAGIC_NUMBER('D','D','S',' ');
			DDSURFACEDESC2 ddsHeader;
			memset(&ddsHeader, 0, sizeof(ddsHeader));

			ddsHeader.dwSize = sizeof(ddsHeader);
			ddsHeader.dwFlags = 0x00001007; // DDSD_CAPS | DDSD_HEIGHT | DDSD_WIDTH | DDSD_PIXELFORMAT
			ddsHeader.dwWidth = w;
			ddsHeader.dwHeight = h;
			ddsHeader.ddpfPixelFormat.dwSize = sizeof(ddsHeader.ddpfPixelFormat);
			ddsHeader.ddpfPixelFormat.dwFlags = DDPF_RGB | DDPF_ALPHAPIXELS;
			ddsHeader.ddpfPixelFormat.dwRGBBitCount = 32;
			ddsHeader.ddpfPixelFormat.dwRBitMask = 0x00ff0000;
			ddsHeader.ddpfPixelFormat.dwGBitMask = 0x0000ff00;
			ddsHeader.ddpfPixelFormat.dwBBitMask = 0x000000ff;
			ddsHeader.ddpfPixelFormat.dwABitMask = 0xff000000;
			ddsHeader.ddsCaps.dwCaps1 = 0x00001002; // DDSCAPS_ALPHA | DDSCAPS_TEXTURE

			fwrite(&ddsMagic, sizeof(ddsMagic), 1, dds);
			fwrite(&ddsHeader, sizeof(ddsHeader), 1, dds);
			fwrite(image, sizeof(Pixel32), w*h, dds);
			fclose(dds);

			if (bAutoOpen)
			{
				system(path);
			}

			return true;
		}
	}

	return false;
}

static bool SaveImage(const char* path, const float* image, int w, int h, bool bAutoOpen)
{
	if (SaveImageDDS(path, image, w, h, bAutoOpen))
	{
		return true;
	}

	ProgressDisplayAuto progress("saving \"%s\"", path);

	FREE_IMAGE_FORMAT fif = FreeImage_GetFIFFromFilename(path);

	if (fif == FIF_UNKNOWN)
	{
		return false; // unknown filetype
	}

	if (!FreeImage_FIFSupportsWriting(fif))
	{
		return false; // cannot save to this filetype
	}

	FIBITMAP *dib = FreeImage_Allocate(w, h, 32);

	if (dib == NULL)
	{
		return false; // failed to allocate DIB
	}

	for (int j = 0; j < h; j++)
	{
		Pixel32* pixels = reinterpret_cast<Pixel32*>(FreeImage_GetScanLine(dib, h - j - 1));

		for (int i = 0; i < w; i++)
		{
			Pixel32& c = pixels[i];

			c.r = (u32)Clamp<float>(0.5f + 255.0f*image[i + j*w], 0.0f, 255.0f);
			c.g = c.r;
			c.b = c.r;
			c.a = 255;
		}
	}

	const bool success = FreeImage_Save(fif, dib, path) != 0;

	FreeImage_Unload(dib);

	if (success && bAutoOpen)
	{
		system(path);
	}

	return success;
}

// save as greyscale image (don't convert to 32-bit)
static bool SaveImageU8(const char* path, const u8* image, int w, int h, bool bAutoOpen)
{
	if (SaveImageDDS(path, image, w, h, bAutoOpen))
	{
		return true;
	}

	ProgressDisplayAuto progress("saving \"%s\"", path);

	FREE_IMAGE_FORMAT fif = FreeImage_GetFIFFromFilename(path);

	if (fif == FIF_UNKNOWN)
	{
		return false; // unknown filetype
	}

	if (!FreeImage_FIFSupportsWriting(fif))
	{
		return false; // cannot save to this filetype
	}

	FIBITMAP *dib = FreeImage_Allocate(w, h, 8);

	if (dib == NULL)
	{
		return false; // failed to allocate DIB
	}

	for (int j = 0; j < h; j++)
	{
		u8* pixels = reinterpret_cast<u8*>(FreeImage_GetScanLine(dib, h - j - 1));

		for (int i = 0; i < w; i++)
		{
			pixels[i] = image[i + j*w];
		}
	}

	const bool success = FreeImage_Save(fif, dib, path) != 0;

	FreeImage_Unload(dib);

	if (success && bAutoOpen)
	{
		system(path);
	}

	return success;
}

static bool SaveImage(const char* path, const u8* image, int w, int h, bool bAutoOpen, bool bSaveU8 = false)
{
	if (SaveImageDDS(path, image, w, h, bAutoOpen))
	{
		return true;
	}
	else if (bSaveU8)
	{
		return SaveImageU8(path, image, w, h, bAutoOpen);
	}

	ProgressDisplayAuto progress("saving \"%s\"", path);

	FREE_IMAGE_FORMAT fif = FreeImage_GetFIFFromFilename(path);

	if (fif == FIF_UNKNOWN)
	{
		return false; // unknown filetype
	}

	if (!FreeImage_FIFSupportsWriting(fif))
	{
		return false; // cannot save to this filetype
	}

	FIBITMAP *dib = FreeImage_Allocate(w, h, 32);

	if (dib == NULL)
	{
		return false; // failed to allocate DIB
	}

	for (int j = 0; j < h; j++)
	{
		Pixel32* pixels = reinterpret_cast<Pixel32*>(FreeImage_GetScanLine(dib, h - j - 1));

		for (int i = 0; i < w; i++)
		{
			Pixel32& c = pixels[i];

			c.r = image[i + j*w];
			c.g = c.r;
			c.b = c.r;
			c.a = 255;
		}
	}

	const bool success = FreeImage_Save(fif, dib, path) != 0;

	FreeImage_Unload(dib);

	if (success && bAutoOpen)
	{
		system(path);
	}

	return success;
}

static bool SaveImage(const char* path, const u16* image, int w, int h, bool bAutoOpen, bool bSaveU16 = false)
{
	if (SaveImageDDS(path, image, w, h, bAutoOpen))
	{
		return true;
	}

	ProgressDisplayAuto progress("saving \"%s\"", path);

	FREE_IMAGE_FORMAT fif = FreeImage_GetFIFFromFilename(path);

	if (fif == FIF_UNKNOWN)
	{
		return false; // unknown filetype
	}

	if (!FreeImage_FIFSupportsWriting(fif))
	{
		return false; // cannot save to this filetype
	}

	FIBITMAP *dib = NULL;

	if (bSaveU16)
	{
		dib = FreeImage_AllocateT(FIT_UINT16, w, h);

		if (dib == NULL)
		{
			return false; // failed to allocate DIB
		}

		for (int j = 0; j < h; j++)
		{
			memcpy(reinterpret_cast<u16*>(FreeImage_GetScanLine(dib, h - j - 1)), &image[j*w], w*sizeof(u16));
		}
	}
	else
	{
		dib = FreeImage_Allocate(w, h, 32);

		if (dib == NULL)
		{
			return false; // failed to allocate DIB
		}

		for (int j = 0; j < h; j++)
		{
			Pixel32* pixels = reinterpret_cast<Pixel32*>(FreeImage_GetScanLine(dib, h - j - 1));

			for (int i = 0; i < w; i++)
			{
				Pixel32& c = pixels[i];

				c.r = image[i + j*w]>>8;
				c.g = c.r;
				c.b = c.r;
				c.a = 255;
			}
		}
	}

	const bool success = FreeImage_Save(fif, dib, path) != 0;

	FreeImage_Unload(dib);

	if (success && bAutoOpen)
	{
		system(path);
	}

	return success;
}

static bool SaveImageRGB(const char* path, const Vec3* image, int w, int h, bool bAutoOpen)
{
	if (SaveImageRGBDDS(path, image, w, h, bAutoOpen))
	{
		return true;
	}

	ProgressDisplayAuto progress("saving \"%s\"", path);

	FREE_IMAGE_FORMAT fif = FreeImage_GetFIFFromFilename(path);

	if (fif == FIF_UNKNOWN)
	{
		return false; // unknown filetype
	}

	if (!FreeImage_FIFSupportsWriting(fif))
	{
		return false; // cannot save to this filetype
	}

	FIBITMAP *dib = FreeImage_Allocate(w, h, 32);

	if (dib == NULL)
	{
		return false; // failed to allocate DIB
	}

	for (int j = 0; j < h; j++)
	{
		Pixel32* pixels = reinterpret_cast<Pixel32*>(FreeImage_GetScanLine(dib, h - j - 1));

		for (int i = 0; i < w; i++)
		{
			Pixel32& c = pixels[i];

			c.r = (u32)Clamp<float>(0.5f + 255.0f*image[i + j*w].x, 0.0f, 255.0f);
			c.g = (u32)Clamp<float>(0.5f + 255.0f*image[i + j*w].y, 0.0f, 255.0f);
			c.b = (u32)Clamp<float>(0.5f + 255.0f*image[i + j*w].z, 0.0f, 255.0f);
			c.a = 255;
		}
	}

	const bool success = FreeImage_Save(fif, dib, path) != 0;

	FreeImage_Unload(dib);

	if (success && bAutoOpen)
	{
		system(path);
	}

	return success;
}

static bool SaveImagePixel32(const char* path, const Pixel32* image, int w, int h, bool bAutoOpen, const char* extraMsg = "")
{
	if (SaveImagePixel32DDS(path, image, w, h, bAutoOpen))
	{
		return true;
	}

	ProgressDisplayAuto progress("saving \"%s\"%s", path, extraMsg);

	FREE_IMAGE_FORMAT fif = FreeImage_GetFIFFromFilename(path);

	if (fif == FIF_UNKNOWN)
	{
		return false; // unknown filetype
	}

	if (!FreeImage_FIFSupportsWriting(fif))
	{
		return false; // cannot save to this filetype
	}

	FIBITMAP *dib = FreeImage_Allocate(w, h, 32);

	if (dib == NULL)
	{
		return false; // failed to allocate DIB
	}

	for (int j = 0; j < h; j++)
	{
		Pixel32* pixels = reinterpret_cast<Pixel32*>(FreeImage_GetScanLine(dib, h - j - 1));

		for (int i = 0; i < w; i++)
		{
			pixels[i] = image[i + j*w];
		}
	}

	const bool success = FreeImage_Save(fif, dib, path) != 0;

	FreeImage_Unload(dib);

	if (success && bAutoOpen)
	{
		system(path);
	}

	return success;
}

static bool SaveImagePNGSilent(const char* path, const Pixel32* image, int w, int h)
{
	FREE_IMAGE_FORMAT fif = FreeImage_GetFIFFromFilename(path);

	if (fif == FIF_UNKNOWN)
	{
		return false; // unknown filetype
	}

	if (!FreeImage_FIFSupportsWriting(fif))
	{
		return false; // cannot save to this filetype
	}

	FIBITMAP *dib = FreeImage_Allocate(w, h, 32);

	if (dib == NULL)
	{
		return false; // failed to allocate DIB
	}

	for (int j = 0; j < h; j++)
	{
		Pixel32* pixels = reinterpret_cast<Pixel32*>(FreeImage_GetScanLine(dib, h - j - 1));

		for (int i = 0; i < w; i++)
		{
			pixels[i] = image[i + j*w];
		}
	}

	const bool success = FreeImage_Save(fif, dib, path) != 0;

	FreeImage_Unload(dib);

	return success;
}

// given an array of w floats in 0-1 range, build an antialiased "graph" image of dimensions (w,h)
static float* Graph(float* dst, const float* src, int w, int h)
{
	if (h == 0)
	{
		h = w/2;
	}

	for (int x = 0; x < w; x++)
	{
		float v0 = Clamp<float>(1.0f - src[Max<int>(0, x - 1)], 0.0f, 1.0f - 0.00001f)*(float)h; // [0..h]
		float v1 = Clamp<float>(1.0f - src[Min<int>(x, w - 1)], 0.0f, 1.0f - 0.00001f)*(float)h; // [0..h]
		float h0 = Min<float>(v0, v1);
		float h1 = Max<float>(v0, v1);
		float h0_floor = floorf(h0);

		int ymin = Clamp<int>((int)floorf(h0), 0, h - 1);
		int ymax = Clamp<int>((int)floorf(h1), 0, h - 1) + 1;

		float* p = &dst[x]; // pointer to column data (dst)

		for (int y = 0; y < ymin; y++) // over
		{
			*p = 1.0f, p += w;
		}

		if (ymax - ymin == 1)
		{
			const float coverage = (h1 + h0)/2.0f - h0_floor;

			*p = coverage, p += w;
		}
		else
		{
			float y0 = h0_floor + 0.0f;
			float y1 = h0_floor + 1.0f;
			float dx = 1.0f/(h1 - h0);
			float x0 = (y0 - h0)*dx;
			float x1 = (y1 - h0)*dx;

			for (int y = ymin; y < ymax; y++)
			{
				// compute coverage for pixel (x,y) - "bounds" of pixel is [0..1],[y..y+1]
				// intersect with line from (0,h0) to (1,h1)

				float coverage = 1.0f - (x1 + x0)/2.0f;

				coverage -= Max<float>(0.0f, -x0       )*(h0 - y0)/2.0f;
				coverage -= Max<float>(0.0f,  x1 - 1.0f)*(h1 - y1)/2.0f;

				*p = coverage, p += w;

				y0 = y1; y1 += 1.0f;
				x0 = x1; x1 += dx;
			}
		}

		for (int y = ymax; y < h; y++) // under
		{
			*p = 0.0f, p += w;
		}
	}

	return dst;
}

#define TILE_MAP_EMPTY '.'
#define TILE_MAP_HOLES 'o'
#define TILE_MAP_OK    '_'

class TileMap
{
public:
	static int ReadInt(const char* name, FILE* fp)
	{
		char line[1024] = "";

		if (fp && ReadFileLine(line, sizeof(line), fp))
		{
			if (memcmp(line, name, strlen(name)) == 0)
			{
				return atoi(line + strlen(name));
			}
		}

		assert(0); // failed!
		exit(-1);
		return 0;
	}

	bool Load(const char* path)
	{
		FILE* fp = fopen(varString("%s/tiles.txt", path).c_str(), "r");

		if (fp)
		{
			m_tileMapX = ReadInt("tiles_x0=", fp);
			m_tileMapY = ReadInt("tiles_y0=", fp);
			m_tileMapW = ReadInt("tiles_x1=", fp) - m_tileMapX;
			m_tileMapH = ReadInt("tiles_y1=", fp) - m_tileMapY;
			m_downsamp = ReadInt("downsamp=", fp);

			const int tileMaskSize = (m_tileMapW*m_tileMapH + 7)/8;

			m_tileMap = new char[m_tileMapW*m_tileMapH];

			for (int j = 0; j < m_tileMapH; j++)
			{
				char line[1024] = "";

				ReadFileLine(line, sizeof(line), fp);

				const char* lineStart = strchr(line, ':');

				if (lineStart)
				{
					lineStart++;

					for (int i = 0; i < m_tileMapW; i++)
					{
						m_tileMap[i + (m_tileMapH - j - 1)*m_tileMapW] = lineStart[i];
					}
				}
			}

			m_materialCount = ReadInt("materials=", fp);

			if (m_materialCount > 0)
			{
				m_materials = new u16[m_materialCount];

				for (int i = 0; i < m_materialCount; i++)
				{
					m_materials[i] = (u16)ReadInt("", fp);
				}
			}
			else
			{
				m_materials = NULL;
			}

			fclose(fp);
			return true;
		}
		else
		{
			fprintf(stdout, "failed to load tile map %s!\n", path);
		}

		return false;
	}

	char* m_tileMap;
	int   m_tileMapX;
	int   m_tileMapY;
	int   m_tileMapW;
	int   m_tileMapH;
	int   m_downsamp;
	int   m_materialCount;
	u16*  m_materials;
};

template <typename T> static float GetMaxValue();
template <> static float GetMaxValue<u8>() { return 255.0f; }
template <> static float GetMaxValue<u16>() { return 65535.0f; }
template <> static float GetMaxValue<float>() { return 1.0f; }

template <typename T> static T Invert(T value);
template <> static u8 Invert<u8>(u8 value) { return ~value; }
template <> static u16 Invert<u16>(u16 value) { return ~value; }
template <> static float Invert<float>(float value) { return 1.0f - value; }

template <typename T> static T* LoadImageTypeDDS(const char* path, int& w, int& h);

template <> static u8*      LoadImageTypeDDS<u8>     (const char* path, int& w, int& h) { return       LoadImageU8DDS     (path, w, h); }
template <> static u16*     LoadImageTypeDDS<u16>    (const char* path, int& w, int& h) { return       LoadImageU16DDS    (path, w, h); }
template <> static u32*     LoadImageTypeDDS<u32>    (const char* path, int& w, int& h) { return (u32*)LoadImagePixel32DDS(path, w, h); }
template <> static u64*     LoadImageTypeDDS<u64>    (const char* path, int& w, int& h) { return       LoadImageU64DDS    (path, w, h); }
template <> static float*   LoadImageTypeDDS<float>  (const char* path, int& w, int& h) { return       LoadImageDDS       (path, w, h); }
template <> static Pixel32* LoadImageTypeDDS<Pixel32>(const char* path, int& w, int& h) { return       LoadImagePixel32DDS(path, w, h); }

template <typename T> static void AssembleImageFromTiles(const TileMap& tm, int resolution, const char* path, const char* suffix, bool bInvert = false, bool bClearToWhite = false)
{
	ProgressDisplay progress("assembling image '%s'", suffix + 1);

	const int pixelsPerCell = resolution*gv::WORLD_CELL_RESOLUTION;
	const int tileResX = pixelsPerCell*gv::WORLD_CELLS_PER_TILE;
	const int tileResY = pixelsPerCell*gv::WORLD_CELLS_PER_TILE;

	const int worldW = tileResX*tm.m_tileMapW;
	const int worldH = tileResX*tm.m_tileMapH;
	T* image = new T[worldW*worldH];
	bool bValidImage = false;

	const float maxValue = GetMaxValue<T>();
	const T clearValue = bClearToWhite ? T(maxValue) : 0;

	for (int i = 0; i < worldW*worldH; i++)
	{
		image[i] = clearValue;
	}

	for (int tileY = 0; tileY < tm.m_tileMapH; tileY++)
	{
		for (int tileX = 0; tileX < tm.m_tileMapW; tileX++)
		{
			if (tm.m_tileMap[tileX + tileY*tm.m_tileMapW] == TILE_MAP_EMPTY)
			{
				continue;
			}

			const int sectorX = (tm.m_tileMapX + tileX)*gv::WORLD_CELLS_PER_TILE;
			const int sectorY = (tm.m_tileMapY + tileY)*gv::WORLD_CELLS_PER_TILE;

			int w = tileResX;
			int h = tileResY;
			T* tile = LoadImageTypeDDS<T>(varString("%s/heightmap_tile_%s%s.dds", path, gv::GetTileNameFromCoords(sectorX, sectorY), suffix).c_str(), w, h);

			if (tile)
			{
				for (int j = 0; j < tileResY; j++)
				{
					for (int i = 0; i < tileResX; i++)
					{
						const int ii = i + tileX*tileResX;
						const int jj = j + tileY*tileResY;

						image[ii + (worldH - jj - 1)*worldW] = tile[i + (tileResY - j - 1)*tileResX];
					}
				}

				delete[] tile;
				bValidImage = true;
			}
			else
			{
				for (int j = 0; j < tileResY; j++)
				{
					for (int i = 0; i < tileResX; i++)
					{
						const int ii = i + tileX*tileResX;
						const int jj = j + tileY*tileResY;

						image[ii + (worldH - jj - 1)*worldW] = 0;
					}
				}
			}
		}

		progress.Update(tileY, tm.m_tileMapH);
	}

	if (bInvert)
	{
		for (int i = 0; i < worldW*worldH; i++)
		{
			image[i] = Invert<T>(image[i]);
		}
	}

	progress.End(bValidImage ? "done" : "no images found");

	if (bValidImage)
	{
		// save png
		{
			char pngPath[1024] = "";
			strcpy(pngPath, varString("%s/assembled%s.png", path, suffix).c_str());

			if (sizeof(T) == sizeof(u16))
			{
				// save as 16-bit png if we can
				SaveImage(pngPath, (const u16*)image, worldW, worldH, false, true);
			}
			else
			{
				SaveImage(pngPath, image, worldW, worldH, false);
			}
		}

		// save dds
		{
			char ddsPath[1024] = "";
			strcpy(ddsPath, varString("%s/assembled%s.dds", path, suffix).c_str());
			SaveImageDDS(ddsPath, image, worldW, worldH, false);
		}
	}

	delete[] image;
}

template <> void AssembleImageFromTiles<Pixel32>(const TileMap& tm, int resolution, const char* path, const char* suffix, bool bInvert, bool bClearToWhite)
{
	ProgressDisplay progress("assembling image '%s'", suffix + 1);

	const int pixelsPerCell = resolution*gv::WORLD_CELL_RESOLUTION;
	const int tileResX = pixelsPerCell*gv::WORLD_CELLS_PER_TILE;
	const int tileResY = pixelsPerCell*gv::WORLD_CELLS_PER_TILE;

	const int worldW = tileResX*tm.m_tileMapW;
	const int worldH = tileResX*tm.m_tileMapH;

	Pixel32* image = new Pixel32[worldW*worldH];
	bool bValidImage = false;

	const Pixel32 clearValue = bClearToWhite ? Pixel32(255,255,255,255) : Pixel32(0,0,0,0);

	for (int i = 0; i < worldW*worldH; i++)
	{
		image[i] = clearValue;
	}

	for (int tileY = 0; tileY < tm.m_tileMapH; tileY++)
	{
		for (int tileX = 0; tileX < tm.m_tileMapW; tileX++)
		{
			if (tm.m_tileMap[tileX + tileY*tm.m_tileMapW] == TILE_MAP_EMPTY)
			{
				continue;
			}

			const int sectorX = (tm.m_tileMapX + tileX)*gv::WORLD_CELLS_PER_TILE;
			const int sectorY = (tm.m_tileMapY + tileY)*gv::WORLD_CELLS_PER_TILE;

			int w = tileResX;
			int h = tileResY;
			Pixel32* tile = LoadImageTypeDDS<Pixel32>(varString("%s/heightmap_tile_%s%s.dds", path, gv::GetTileNameFromCoords(sectorX, sectorY), suffix).c_str(), w, h);

			if (tile)
			{
				for (int j = 0; j < tileResY; j++)
				{
					for (int i = 0; i < tileResX; i++)
					{
						const int ii = i + tileX*tileResX;
						const int jj = j + tileY*tileResY;

						image[ii + (worldH - jj - 1)*worldW] = tile[i + (tileResY - j - 1)*tileResX];
					}
				}

				delete[] tile;
				bValidImage = true;
			}
		}

		progress.Update(tileY, tm.m_tileMapH);
	}

	if (bInvert)
	{
		for (int i = 0; i < worldW*worldH; i++)
		{
			image[i].r ^= 0xff;
			image[i].g ^= 0xff;
			image[i].b ^= 0xff;
		}
	}

	progress.End();

	if (bValidImage)
	{
		// save png
		{
			char pngPath[1024] = "";
			strcpy(pngPath, varString("%s/assembled%s.png", path, suffix).c_str());
			SaveImagePixel32(pngPath, image, worldW, worldH, false);
		}

		// save dds
		{
			char ddsPath[1024] = "";
			strcpy(ddsPath, varString("%s/assembled%s.dds", path, suffix).c_str());
			SaveImagePixel32DDS(ddsPath, image, worldW, worldH, false);
		}
	}

	delete[] image;
}

// see BS#1156382
// NOTE -- this list must be kept in sync with:
//   %RS_CODEBRANCH%\game\scene\world\GameWorldHeightMap.cpp
//   %RS_CODEBRANCH%\tools\HeightMapImageProcessor\src\HeightMapImageProcessor.cpp
static const char* g_materialMaskList[] =
{
	"BRICK", // added for rdr3
	"BUSHES",
	"CLAY_HARD",
	"CLAY_SOFT",
	"DIRT_TRACK",
	"GRASS",
	"GRASS_LONG",
	"GRASS_MOWN",
	"GRASS_SHORT",
	"GRAVEL_LARGE",
	"GRAVEL_SMALL",
	"HAY",
	"LEAVES",
	"MUD_DEEP",
	"MUD_HARD",
	"MUD_POTHOLE",
	"MUD_SOFT",
	"MUD_UNDERWATER",
	"MARSH",
	"MARSH_DEEP",
	"ROCK",
	"ROCK_MOSSY",
	"SAND_COMPACT",
	"SAND_DRY_DEEP",
	"SAND_LOOSE",
	"SAND_TRACK",
	"SAND_UNDERWATER",
	"SAND_WET",
	"SAND_WET_DEEP",
	"SNOW_COMPACT", // added for rdr3
	"SOIL",
	"STONE",
	// =========
	"TARMAC",
	"TREE_BARK",
	"TWIGS",
	"WOODCHIPS",
	"unused_36",
	"unused_37",
	"unused_38",
	"unused_39",
	"unused_40",
	"unused_41",
	"unused_42",
	"unused_43",
	"unused_44",
	"unused_45",
	"unused_46",
	"unused_47",
	"unused_48",
	"unused_49",
	"unused_50",
	"unused_51",
	"unused_52",
	"unused_53",
	"unused_54",
	"unused_55",
	"unused_56",
	"unused_57",
	"unused_58",
	"unused_59",
	"unused_60",
	"unused_61",
	"OTHER",
	"ALL", // must be at index 0x003f
};
CompileTimeAssert(NELEM(g_materialMaskList) == 64); // we're going to store these masks in a 64-bit image

static const char** GetProceduralList()
{
	static char** s_proceduralList = NULL;
	static bool   s_proceduralListLoaded = false;
	static int    s_proceduralListCount = 0;

	if (!s_proceduralListLoaded)
	{
		FILE* proceduralMetaFile = fopen(RS_COMMON"/data/materials/procedural.meta", "r");

		if (proceduralMetaFile)
		{
			std::vector<std::string> names;
			bool bParsingProcNames = false;
			char line[1024] = "";

			while (rage_fgetline(line, sizeof(line), proceduralMetaFile))
			{
				if (strstr(line, "<procTagTable>"))
				{
					bParsingProcNames = true;
				}
				else if (strstr(line, "</procTagTable>"))
				{
					break;
				}
				else if (bParsingProcNames)
				{
					char* s = strstr(line, "<name>");

					if (s)
					{
						char* name = s + strlen("<name>");

						s = strstr(name, "</name>");

						if (s)
						{
							*s = '\0';
							names.push_back(name);
						}
					}
				}
			}

			fclose(proceduralMetaFile);

			if (names.size() > 0)
			{
				s_proceduralList = new char*[names.size() + 1];

				for (int i = 0; i < (int)names.size(); i++)
				{
					s_proceduralList[i] = new char[names[i].length() + 1];
					strcpy(s_proceduralList[i], names[i].c_str());
				}

				s_proceduralList[names.size()] = NULL;
				s_proceduralListCount = (int)names.size();
			}
		}

		s_proceduralListLoaded = true;
	}

	return const_cast<const char**>(s_proceduralList);
}

template <typename T> static void AssembleMaterialMaskImagesFromTiles(const TileMap& tm, int resolution, const char* path, const char* suffix, const char* dstDir, const char** materialNames)
{
	ProgressDisplay progress("assembling image '%s'", suffix + 1);

	const int pixelsPerCell = resolution*gv::WORLD_CELL_RESOLUTION;
	const int tileResX = pixelsPerCell*gv::WORLD_CELLS_PER_TILE;
	const int tileResY = pixelsPerCell*gv::WORLD_CELLS_PER_TILE;

	const int worldW = tileResX*tm.m_tileMapW;
	const int worldH = tileResX*tm.m_tileMapH;

	T* image = new T[worldW*worldH];

	T allMasks = 0;

	memset(image, 0, worldW*worldH*sizeof(T));

	for (int tileY = 0; tileY < tm.m_tileMapH; tileY++)
	{
		for (int tileX = 0; tileX < tm.m_tileMapW; tileX++)
		{
			if (tm.m_tileMap[tileX + tileY*tm.m_tileMapW] == TILE_MAP_EMPTY)
			{
				continue;
			}

			const int sectorX = (tm.m_tileMapX + tileX)*gv::WORLD_CELLS_PER_TILE;
			const int sectorY = (tm.m_tileMapY + tileY)*gv::WORLD_CELLS_PER_TILE;

			int w = tileResX;
			int h = tileResY;
			T* tile = LoadImageTypeDDS<T>(varString("%s/heightmap_tile_%s%s.dds", path, gv::GetTileNameFromCoords(sectorX, sectorY), suffix).c_str(), w, h);

			if (tile)
			{
				for (int j = 0; j < tileResY; j++)
				{
					for (int i = 0; i < tileResX; i++)
					{
						const int ii = i + tileX*tileResX;
						const int jj = j + tileY*tileResY;

						image[ii + (worldH - jj - 1)*worldW] = tile[i + (tileResY - j - 1)*tileResX];
					}
				}

				for (int i = 0; i < tileResX*tileResY; i++)
				{
					allMasks |= ((const T*)tile)[i];
				}

				delete[] tile;
			}
		}

		progress.Update(tileY, tm.m_tileMapH);
	}

	progress.End();

	if (allMasks)
	{
		u8* maskImage = new u8[worldW*worldH];

		for (int k = 0; k < sizeof(T)*8; k++)
		{
			if (allMasks & BIT64(k))
			{
				for (int i = 0; i < worldW*worldH; i++)
				{
					maskImage[i] = (((const T*)image)[i] & BIT64(k)) ? 255 : 0;
				}

				// save png
				{
					char pngPath[1024] = "";
					strcpy(pngPath, varString("%s/%s/%s.png", path, dstDir, materialNames[k]).c_str());
					SaveImage(pngPath, maskImage, worldW, worldH, false, true);
				}
			}
		}

		delete[] maskImage;
	}

	delete[] image;
}

static void AssembleMaterialProcImagesFromTiles(const TileMap& tm, int resolution, const char* path, const char* suffix, int index)
{
	ProgressDisplay progress("assembling image '%s_%04d'", suffix + 1, index);

	const int pixelsPerCell = resolution*gv::WORLD_CELL_RESOLUTION;
	const int tileResX = pixelsPerCell*gv::WORLD_CELLS_PER_TILE;
	const int tileResY = pixelsPerCell*gv::WORLD_CELLS_PER_TILE;

	const int worldW = tileResX*tm.m_tileMapW;
	const int worldH = tileResX*tm.m_tileMapH;

	u64* image = new u64[worldW*worldH];

	const char** proceduralList = GetProceduralList();

	// clear
	memset(image, 0, worldW*worldH*sizeof(u64));

	for (int tileY = 0; tileY < tm.m_tileMapH; tileY++)
	{
		for (int tileX = 0; tileX < tm.m_tileMapW; tileX++)
		{
			if (tm.m_tileMap[tileX + tileY*tm.m_tileMapW] == TILE_MAP_EMPTY)
			{
				continue;
			}

			const int sectorX = (tm.m_tileMapX + tileX)*gv::WORLD_CELLS_PER_TILE;
			const int sectorY = (tm.m_tileMapY + tileY)*gv::WORLD_CELLS_PER_TILE;

			int w = tileResX;
			int h = tileResY;
			u64* tile = LoadImageTypeDDS<u64>(varString("%s/heightmap_tile_%s%s_%04d.dds", path, gv::GetTileNameFromCoords(sectorX, sectorY), suffix, index).c_str(), w, h);

			if (tile)
			{
				for (int j = 0; j < tileResY; j++)
				{
					for (int i = 0; i < tileResX; i++)
					{
						const int ii = i + tileX*tileResX;
						const int jj = j + tileY*tileResY;

						image[ii + (worldH - jj - 1)*worldW] = tile[i + (tileResY - j - 1)*tileResX];
					}
				}

				delete[] tile;
			}
		}

		progress.Update(tileY, tm.m_tileMapH);
	}

	progress.End();

	Pixel32* maskImage = new Pixel32[worldW*worldH];

	for (int k = 0; k < 64 && index*64 + k < tm.m_materialCount; k++)
	{
		const int matId  = tm.m_materials[index*64 + k] & 0x3f;
		const int procId = tm.m_materials[index*64 + k] >> 8;

		int numPixels = 0;

		for (int i = 0; i < worldW*worldH; i++)
		{
			if (((const u64*)image)[i] & BIT64(k))
			{
				maskImage[i] = Pixel32(255,255,255,255);
				numPixels++;
			}
			else
			{
				maskImage[i] = Pixel32(0,0,0,255);
			}
		}

		char procName[256] = "";
		strcpy(procName, proceduralList[procId]);

		if (strcmp(procName, "null") == 0)
		{
			strcat(procName, varString("__%d", procId).c_str());
		}

		if (numPixels > 0) // save png
		{
			char pngPath[1024] = "";
			strcpy(pngPath, varString("%s/materialmasks/%s_[%s].png", path, g_materialMaskList[matId], procName).c_str());
			SaveImagePixel32(pngPath, maskImage, worldW, worldH, false, varString(" (%d pixels, matId=%d, procId=%d)", numPixels, matId, procId).c_str());
		}
		else
		{
			fprintf(stdout, "no pixels for %s_[%s] (matId=%d, procId=%d)\n", g_materialMaskList[matId], procName, matId, procId);
		}
	}

	delete[] image;
	delete[] maskImage;
}

static void OverlayMaskImage(Pixel32* image, int w, int h, const float* maskImage, const Vec3& colour, float amount, bool bInvert)
{
	if (maskImage == NULL || amount <= 0.0f)
	{
		return;
	}

	for (int i = 0; i < w*h; i++)
	{
		float r = (float)image[i].r/255.0f;
		float g = (float)image[i].g/255.0f;
		float b = (float)image[i].b/255.0f;

		float maskAmount = maskImage[i];

		if (bInvert)
		{
			maskAmount = 1.0f - maskAmount;
		}

		r += (colour.x - r)*maskAmount*amount;
		g += (colour.y - g)*maskAmount*amount;
		b += (colour.z - b)*maskAmount*amount;

		image[i].r = (u8)(0.5f + 255.0f*r);
		image[i].g = (u8)(0.5f + 255.0f*g);
		image[i].b = (u8)(0.5f + 255.0f*b);
	}
}

static void OverlayHeightMap(Pixel32* image, int w, int h, const float* heightmap, const Vec3& colour, float amount, int radius = 1, int marginY0 = 0, int marginY1 = 0)
{
	if (heightmap == NULL || amount <= 0.0f)
	{
		return;
	}

	for (int j = 0; j < h; j++)
	{
		for (int i = 0; i < w; i++)
		{
			float grad = 0.0f;

			if (radius == 0)
			{
				float z[3][3];

				for (int dj = -1; dj <= 1; dj++)
				{
					for (int di = -1; di <= 1; di++)
					{
						const int ii = Clamp<int>(i + di, 0, w - 1);
						const int jj = Clamp<int>(j + dj, -marginY0, h - 1 + marginY1);

						z[di + 1][dj + 1] = heightmap[ii + jj*w];
					}
				}

				const float tmp0x = (z[0][0] + 2.0f*z[0][1] + z[0][2])/4.0f;
				const float tmp2x = (z[2][0] + 2.0f*z[2][1] + z[2][2])/4.0f;
				const float tmp0y = (z[0][0] + 2.0f*z[1][0] + z[2][0])/4.0f;
				const float tmp2y = (z[0][2] + 2.0f*z[1][2] + z[2][2])/4.0f;

				const float gx = tmp0x - tmp2x;
				const float gy = tmp0y - tmp2y;

				grad = Clamp<float>(Max<float>(Abs<float>(gx), Abs<float>(gy))*128.0f, 0.0f, 1.0f);
			}
			else
			{
				for (int r = 1; r <= radius; r++)
				{
					float gx = 0.0f;
					float gy = 0.0f;

					for (int di = -r; di <= r; di++)
					{
						for (int dj = 1; dj <= r; dj++)
						{
							const int ii = Clamp<int>(i + di, 0, w - 1);
							const int jp = Clamp<int>(j + dj, -marginY0, h - 1 + marginY1);
							const int jn = Clamp<int>(j - dj, -marginY0, h - 1 + marginY1);

							gx += heightmap[ii + jp*w];
							gx -= heightmap[ii + jn*w];
						}
					}

					for (int dj = -r; dj <= r; dj++)
					{
						for (int di = 1; di <= r; di++)
						{
							const int ip = Clamp<int>(i + di, 0, w - 1);
							const int in = Clamp<int>(i - di, 0, w - 1);
							const int jj = Clamp<int>(j + dj, -marginY0, h - 1 + marginY1);

							gy += heightmap[ip + jj*w];
							gy -= heightmap[in + jj*w];
						}
					}

					const float temp1 = Max<float>(Abs<float>(gx), Abs<float>(gy))/(float)(r*r*2 + r);
					const float temp2 = Clamp<float>(128.0f*temp1, 0.0f, 1.0f/sqrtf((float)r));

					grad = Max<float>(temp2, grad);
				}
			}

			const float edge = powf(grad, 2.0f);

			float r = (float)image[i + j*w].r/255.0f;
			float g = (float)image[i + j*w].g/255.0f;
			float b = (float)image[i + j*w].b/255.0f;

			r += (colour.x - r)*edge*amount;
			g += (colour.y - g)*edge*amount;
			b += (colour.z - b)*edge*amount;

			image[i + j*w].r = (u8)(0.5f + 255.0f*r);
			image[i + j*w].g = (u8)(0.5f + 255.0f*g);
			image[i + j*w].b = (u8)(0.5f + 255.0f*b);
		}
	}
}

static void OverlayPolyDensityMap(Pixel32* image, int w, int h, const float* densityMap, float amount, bool bIsPedDensity, bool bIsColourKey)
{
	if (densityMap == NULL || amount <= 0.0f)
	{
		return;
	}

	const float midpt = 0.08f; // PFD_ColorMidPoint

	const Vec3 colourAtZero(0.4f, 0.0f, 0.6f);
	const Vec3 colourAtMid(1.0f - midpt, midpt, 0.0f);
	const Vec3 colourAtOne = bIsPedDensity ? Vec3(0.0f, 1.0f, 0.0f) : Vec3(1.0f, 1.0f, 1.0f);

	for (int i = 0; i < w*h; i++)
	{
		const float density = densityMap[i];
		Vec3 colour;

		if (density < midpt)
		{
			const float t = density/midpt; // [0..1]

			colour.x = Clamp<float>(colourAtZero.x + (colourAtMid.x - colourAtZero.x)*t, 0.0f, 1.0f);
			colour.y = Clamp<float>(colourAtZero.y + (colourAtMid.y - colourAtZero.y)*t, 0.0f, 1.0f);
			colour.z = Clamp<float>(colourAtZero.z + (colourAtMid.z - colourAtZero.z)*t, 0.0f, 1.0f);
		}
		else
		{
			const float t = (density - midpt)/(1.0f - midpt); // [0..1]

			colour.x = Clamp<float>(colourAtMid.x + (colourAtOne.x - colourAtMid.x)*t, 0.0f, 1.0f);
			colour.y = Clamp<float>(colourAtMid.y + (colourAtOne.y - colourAtMid.y)*t, 0.0f, 1.0f);
			colour.z = Clamp<float>(colourAtMid.z + (colourAtOne.z - colourAtMid.z)*t, 0.0f, 1.0f);
		}

		float r = (float)image[i].r/255.0f;
		float g = (float)image[i].g/255.0f;
		float b = (float)image[i].b/255.0f;

		if (bIsPedDensity)
		{
			if (density >= 0.0f && (density < 1.0f || bIsColourKey))
			{
				r += (colour.x - r)*amount;
				g += (colour.y - g)*amount;
				b += (colour.z - b)*amount;
			}
		}
		else
		{
			r += (colour.x - r)*amount*(1.0f - density);
			g += (colour.y - g)*amount*(1.0f - density);
			b += (colour.z - b)*amount*(1.0f - density);
		}

		image[i].r = (u8)(0.5f + 255.0f*r);
		image[i].g = (u8)(0.5f + 255.0f*g);
		image[i].b = (u8)(0.5f + 255.0f*b);
	}
}

class WorldMask
{
public:
	WorldMask(const char* path)
	{
		memset(this, 0, sizeof(*this));

		const std::string worldMaskPath = varString("%s/assembled_wld.dds", path);

		int w = 0;
		int h = 0;

		if (GetImageDimensionsDDS(worldMaskPath.c_str(), w, h))
		{
			m_w = w;
			m_h = h;
		}
		else
		{
			return;
		}

		// find an appropriate span height
		{
			const int spanHeightsToTry[] = {512, 500, 256, 250, 128, 100, 75, 64, 50, 32, 25, 16, 10, 8, 5, 4, 2, 1, 0};

			for (int i = 0; spanHeightsToTry[i] > 0; i++)
			{
				if (h%spanHeightsToTry[i] == 0)
				{
					m_spanHeight = spanHeightsToTry[i];
					break;
				}
			}

			if (m_spanHeight == 0)
			{
				m_spanHeight = h;
				printf("failed to find span height for %dx%d image!\n", w, h);
				system("pause");
			}
		}

		// load world mask
		{
			ProgressDisplay progress("loading world mask (span height = %d)", m_spanHeight);

			const int worldMaskSize = (w*h + 7)/8;
			m_worldMask = new u8[worldMaskSize];
			memset(m_worldMask, 0, worldMaskSize*sizeof(u8));

			for (int span = 0; span < h; span += m_spanHeight)
			{
				float* temp = LoadImageDDSSpan(worldMaskPath.c_str(), w, span, m_spanHeight);

				if (temp)
				{
					for (int j = span; j < span + m_spanHeight; j++)
					{
						for (int i = 0; i < w; i++)
						{
							if (temp[i + (j - span)*w] == 1.0f)
							{
								const int index = i + j*w;

								m_worldMask[index/8] |= BIT(index%8);
							}
						}
					}

					delete[] temp;
				}
				else
				{
					break;
				}

				progress.Update(span/m_spanHeight, h/m_spanHeight);
			}

			progress.End();
		}

		// expand world mask
		{
			ProgressDisplay progress("expanding world mask");

			m_worldMinX = new int[h];
			m_worldMinY = new int[w];
			m_worldMaxX = new int[h];
			m_worldMaxY = new int[w];

			for (int j = 0; j < h; j++)
			{
				m_worldMinX[j] = +999999;
				m_worldMaxX[j] = -999999;
			}

			for (int i = 0; i < w; i++)
			{
				m_worldMinY[i] = +999999;
				m_worldMaxY[i] = -999999;
			}

			for (int j = 0; j < h; j++)
			{
				for (int i = 0; i < w; i++)
				{
					const int index = i + j*w;

					if (m_worldMask[index/8] & BIT(index%8))
					{
						m_worldMinX[j] = Min<int>(i, m_worldMinX[j]);
						m_worldMinY[i] = Min<int>(j, m_worldMinY[i]);
						m_worldMaxX[j] = Max<int>(i, m_worldMaxX[j]);
						m_worldMaxY[i] = Max<int>(j, m_worldMaxY[i]);
					}
				}
			}

			if (1) // dilate world worldMask by 1 pixel
			{
				int* tempMinX = new int[h];
				int* tempMinY = new int[w];
				int* tempMaxX = new int[h];
				int* tempMaxY = new int[w];

				memcpy(tempMinX, m_worldMinX, h*sizeof(int));
				memcpy(tempMinY, m_worldMinY, w*sizeof(int));
				memcpy(tempMaxX, m_worldMaxX, h*sizeof(int));
				memcpy(tempMaxY, m_worldMaxY, w*sizeof(int));

				for (int j = 0; j < h; j++)
				{
					int minX = tempMinX[j];
					int maxX = tempMaxX[j];

					if (j > 0)
					{
						minX = Max<int>(tempMinX[j - 1], minX);
						maxX = Min<int>(tempMaxX[j - 1], maxX);
					}

					if (j < h - 1)
					{
						minX = Max<int>(tempMinX[j + 1], minX);
						maxX = Min<int>(tempMaxX[j + 1], maxX);
					}

					m_worldMinX[j] = minX;
					m_worldMaxX[j] = maxX;
				}

				for (int i = 0; i < w; i++)
				{
					int minY = tempMinY[i];
					int maxY = tempMaxY[i];

					if (i > 0)
					{
						minY = Max<int>(tempMinY[i - 1], minY);
						maxY = Min<int>(tempMaxY[i - 1], maxY);
					}

					if (i < w - 1)
					{
						minY = Max<int>(tempMinY[i + 1], minY);
						maxY = Min<int>(tempMaxY[i + 1], maxY);
					}

					m_worldMinY[i] = minY;
					m_worldMaxY[i] = maxY;
				}

				delete[] tempMinX;
				delete[] tempMinY;
				delete[] tempMaxX;
				delete[] tempMaxY;
			}

			for (int j = 0; j < h; j++)
			{
				for (int i = 0; i < w; i++)
				{
					if (i < m_worldMinX[j] || i > m_worldMaxX[j] ||
						j < m_worldMinY[i] || j > m_worldMaxY[i])
					{
						const int index = i + j*w;

						m_worldMask[index/8] |= BIT(index%8);
					}
				}
			}

			progress.End();
		}
	}

	~WorldMask()
	{
		if (m_worldMask) { delete[] m_worldMask; }
		if (m_worldMinX) { delete[] m_worldMinX; }
		if (m_worldMinY) { delete[] m_worldMinY; }
		if (m_worldMaxX) { delete[] m_worldMaxX; }
		if (m_worldMaxY) { delete[] m_worldMaxY; }
	}

	int  m_w;
	int  m_h;
	int  m_spanHeight;
	u8*  m_worldMask;
	int* m_worldMinX;
	int* m_worldMinY;
	int* m_worldMaxX;
	int* m_worldMaxY;
};

static void CompositePolyDensityImage(const WorldMask& wm, int resolution, const char* type, const char* suffix, const char* outputName, const char* path, bool bAutoOpen)
{
	fprintf(stdout, "compositing %s density image ...\n", type);

	const int w = wm.m_w;
	const int h = wm.m_h;

	Pixel32* image = NULL;

	// create image
	{
		ProgressDisplay progress("creating image");

		image = new Pixel32[w*h];

		for (int i = 0; i < w*h; i++)
		{
			image[i].r = 255;
			image[i].g = 255;
			image[i].b = 255;
			image[i].a = 255;
		}

		progress.End();
	}

	// process overlays (heightmap)
	{
		ProgressDisplay progress("processing overlays (heightmap)");

		for (int span = 0; span < h; span += wm.m_spanHeight)
		{
			const int margin = resolution;
			const int spanY0 = Max<int>(0, span - margin);
			const int spanY1 = Min<int>(span + wm.m_spanHeight + margin, h);

			float* heightmap = LoadImageDDSSpan(varString("%s/assembled_max.dds", path).c_str(), w, spanY0, spanY1 - spanY0);

			if (heightmap)
			{
				const int marginY0 = span - spanY0;
				const int marginY1 = spanY1 - (span + wm.m_spanHeight);

				OverlayHeightMap(image + span*w, w, wm.m_spanHeight, heightmap + marginY0*w, Vec3(0.0f, 0.0f, 0.0f), 0.125f, margin, marginY0, marginY1);
				delete[] heightmap;
			}
			else
			{
				break;
			}

			progress.Update(span/wm.m_spanHeight, h/wm.m_spanHeight);
		}

		progress.End();
	}

	// process overlays (roads mask)
	{
		ProgressDisplay progress("processing overlays (roads mask)");

		char roadsPath[1024] = "";
		sprintf(roadsPath, RS_ASSETS "/non_final/heightmap/masks/roads_%d.dds", resolution);

		for (int span = 0; span < h; span += wm.m_spanHeight)
		{
		//	float* roadsMask = LoadImageDDSSpan(varString("%s/assembled_roads.dds", path).c_str(), w, span, wm.m_spanHeight);
			float* roadsMask = LoadImageDDSSpan(roadsPath, w, span, wm.m_spanHeight);

			if (roadsMask)
			{
				OverlayMaskImage(image + span*w, w, wm.m_spanHeight, roadsMask, Vec3(0.37f, 0.25f, 0.20f), 0.5f, true);
				delete[] roadsMask;
			}
			else
			{
				break;
			}

			progress.Update(span/wm.m_spanHeight, h/wm.m_spanHeight);
		}

		progress.End();
	}

	// process overlays (poly density)
	{
		ProgressDisplay progress("processing overlays (%s density)", type);

		for (int span = 0; span < h; span += wm.m_spanHeight)
		{
			float* polyDensityMap = LoadImageDDSSpan(varString("%s/assembled%s.dds", path, suffix).c_str(), w, span, wm.m_spanHeight);

			if (polyDensityMap)
			{
				for (int j = span; j < span + wm.m_spanHeight; j++) // mask pixels that are outside the world
				{
					for (int i = 0; i < w; i++)
					{
						float& density = polyDensityMap[i + (j - span)*w];

						if (i < wm.m_worldMinX[j] || i > wm.m_worldMaxX[j] ||
							j < wm.m_worldMinY[i] || j > wm.m_worldMaxY[i])
						{
							density = 1.0f;
						}
					}
				}

				OverlayPolyDensityMap(image + span*w, w, wm.m_spanHeight, polyDensityMap, 1.0f, false, false);
				delete[] polyDensityMap;
			}
			else
			{
				break;
			}

			progress.Update(span/wm.m_spanHeight, h/wm.m_spanHeight);
		}

		progress.End();
	}

	// process overlays (water mask)
	{
		ProgressDisplay progress("processing overlays (water mask)");

		for (int span = 0; span < h; span += wm.m_spanHeight)
		{
			float* waterMask = LoadImageDDSSpan(varString("%s/assembled_water.dds", path).c_str(), w, span, wm.m_spanHeight);

			if (waterMask)
			{
				for (int j = span; j < span + wm.m_spanHeight; j++) // mask pixels that are outside the world
				{
					for (int i = 0; i < w; i++)
					{
						if (i < wm.m_worldMinX[j] || i > wm.m_worldMaxX[j] ||
							j < wm.m_worldMinY[i] || j > wm.m_worldMaxY[i])
						{
							waterMask[i + (j - span)*w] = 1.0f;
						}
					}
				}

				OverlayMaskImage(image + span*w, w, wm.m_spanHeight, waterMask, Vec3(0.0f, 0.0f, 1.0f), 0.125f, false);
				delete[] waterMask;
			}
			else
			{
				break;
			}

			progress.Update(span/wm.m_spanHeight, h/wm.m_spanHeight);
		}

		progress.End();
	}

	// process overlays (container mask)
	{
		ProgressDisplay progress("processing overlays (container mask)");

		char containerBoundsPath[1024] = "";
		sprintf(containerBoundsPath, RS_ASSETS "/non_final/heightmap/masks/container_mask_%d.dds", resolution);

		for (int span = 0; span < h; span += wm.m_spanHeight)
		{
			float* containerBounds = LoadImageDDSSpan(containerBoundsPath, w, span, wm.m_spanHeight);

			if (containerBounds)
			{
				OverlayMaskImage(image + span*w, w, wm.m_spanHeight, containerBounds, Vec3(1.0f, 1.0f, 1.0f), 0.75f, true);
				delete[] containerBounds;
			}
			else
			{
				break;
			}

			progress.Update(span/wm.m_spanHeight, h/wm.m_spanHeight);
		}

		progress.End();
	}

	// process overlays (container bounds)
	{
		ProgressDisplay progress("processing overlays (container bounds)");

		char containerBoundsPath[1024] = "";
		sprintf(containerBoundsPath, RS_ASSETS "/non_final/heightmap/masks/container_bounds_%d.dds", resolution);

		for (int span = 0; span < h; span += wm.m_spanHeight)
		{
			float* containerBounds = LoadImageDDSSpan(containerBoundsPath, w, span, wm.m_spanHeight);

			if (containerBounds)
			{
				OverlayMaskImage(image + span*w, w, wm.m_spanHeight, containerBounds, Vec3(0.0f, 0.0f, 0.0f), 1.0f, true);
				delete[] containerBounds;
			}
			else
			{
				break;
			}

			progress.Update(span/wm.m_spanHeight, h/wm.m_spanHeight);
		}

		progress.End();
	}

	char pngPath[1024] = "";
	strcpy(pngPath, varString("%s/composite%s.png", path, outputName).c_str());
	SaveImagePixel32(pngPath, image, w, h, bAutoOpen);

	delete[] image;
}

static void CompositePedDensityImage(const WorldMask& wm, int resolution, const char* path, bool bAutoOpen)
{
	fprintf(stdout, "compositing ped density image ...\n");

	const int w = wm.m_w;
	const int h = wm.m_h;

	Pixel32* image = NULL;

	// create image
	{
		ProgressDisplay progress("creating image");

		image = new Pixel32[w*h];

		for (int i = 0; i < w*h; i++)
		{
			image[i].r = 255;
			image[i].g = 255;
			image[i].b = 255;
			image[i].a = 255;
		}

		progress.End();
	}

	// process overlays (heightmap)
	{
		ProgressDisplay progress("processing overlays (heightmap)");

		for (int span = 0; span < h; span += wm.m_spanHeight)
		{
			const int margin = resolution;
			const int spanY0 = Max<int>(0, span - margin);
			const int spanY1 = Min<int>(span + wm.m_spanHeight + margin, h);

			float* heightmap = LoadImageDDSSpan(varString("%s/assembled_max.dds", path).c_str(), w, spanY0, spanY1 - spanY0);

			if (heightmap)
			{
				const int marginY0 = span - spanY0;
				const int marginY1 = spanY1 - (span + wm.m_spanHeight);

				OverlayHeightMap(image + span*w, w, wm.m_spanHeight, heightmap + marginY0*w, Vec3(0.0f, 0.0f, 0.0f), 0.125f, margin, marginY0, marginY1);
				delete[] heightmap;
			}
			else
			{
				break;
			}

			progress.Update(span/wm.m_spanHeight, h/wm.m_spanHeight);
		}

		progress.End();
	}

	// process overlays (roads mask)
	{
		ProgressDisplay progress("processing overlays (roads mask)");

		char roadsPath[1024] = "";
		sprintf(roadsPath, RS_ASSETS "/non_final/heightmap/masks/roads_%d.dds", resolution);

		for (int span = 0; span < h; span += wm.m_spanHeight)
		{
		//	float* roadsMask = LoadImageDDSSpan(varString("%s/assembled_roads.dds", path).c_str(), w, span, wm.m_spanHeight);
			float* roadsMask = LoadImageDDSSpan(roadsPath, w, span, wm.m_spanHeight);

			if (roadsMask)
			{
				OverlayMaskImage(image + span*w, w, wm.m_spanHeight, roadsMask, Vec3(0.37f, 0.25f, 0.20f), 0.1f, true);
				delete[] roadsMask;
			}
			else
			{
				break;
			}

			progress.Update(span/wm.m_spanHeight, h/wm.m_spanHeight);
		}

		progress.End();
	}

	// process overlays (ped density)
	{
		ProgressDisplay progress("processing overlays (ped density)");

		for (int span = 0; span < h; span += wm.m_spanHeight)
		{
			float* pedDensityMap = LoadImageDDSSpan(varString("%s/assembled_den_ped.dds", path).c_str(), w, span, wm.m_spanHeight);

			if (pedDensityMap)
			{
				for (int j = span; j < span + wm.m_spanHeight; j++) // mask pixels that are outside the world
				{
					for (int i = 0; i < w; i++)
					{
						if (i < wm.m_worldMinX[j] || i > wm.m_worldMaxX[j] ||
							j < wm.m_worldMinY[i] || j > wm.m_worldMaxY[i])
						{
							pedDensityMap[i + (j - span)*w] = 1.0f;
						}
					}
				}

				OverlayPolyDensityMap(image + span*w, w, wm.m_spanHeight, pedDensityMap, 1.0f, true, false);
				delete[] pedDensityMap;
			}
			else
			{
				break;
			}

			progress.Update(span/wm.m_spanHeight, h/wm.m_spanHeight);
		}

		progress.End();
	}

	// process overlays (water mask)
	{
		ProgressDisplay progress("processing overlays (water mask)");

		for (int span = 0; span < h; span += wm.m_spanHeight)
		{
			float* waterMask = LoadImageDDSSpan(varString("%s/assembled_water.dds", path).c_str(), w, span, wm.m_spanHeight);

			if (waterMask)
			{
				for (int j = span; j < span + wm.m_spanHeight; j++) // mask pixels that are outside the world
				{
					for (int i = 0; i < w; i++)
					{
						if (i < wm.m_worldMinX[j] || i > wm.m_worldMaxX[j] ||
							j < wm.m_worldMinY[i] || j > wm.m_worldMaxY[i])
						{
							waterMask[i + (j - span)*w] = 1.0f;
						}
					}
				}

				OverlayMaskImage(image + span*w, w, wm.m_spanHeight, waterMask, Vec3(0.0f, 0.0f, 1.0f), 0.125f, false);
				delete[] waterMask;
			}
			else
			{
				break;
			}

			progress.Update(span/wm.m_spanHeight, h/wm.m_spanHeight);
		}

		progress.End();
	}

	// process overlays (density colour key)
	{
		ProgressDisplay progress("processing overlays (density colour key)");

		const int numKeys = 8;
		const int c = ((int)(0.20f*(float)w) + numKeys - 1)/numKeys;
		const int i0 = (int)(0.05f*(float)w);
		const int j0 = (int)(0.05f*(float)h);
		const int j1 = j0 + c;

		float* colourKeyMask = new float[w*(j1 - j0)];

		for (int i = 0; i < w*(j1 - j0); i++)
		{
			colourKeyMask[i] = -1.0f;
		}

		for (int n = 0; n < numKeys - 1; n++)
		{
			for (int j = j0; j < j1 - resolution*8; j++)
			{
				for (int i = i0 + c*n; i < i0 + c*(n + 1) - resolution*8; i++)
				{
					colourKeyMask[i + (j - j0)*w] = (float)n/(float)(numKeys - 1);
				}
			}
		}

		OverlayPolyDensityMap(image + j0*w, w, j1 - j0, colourKeyMask, 1.0f, true, true);

		progress.End();
	}

	// process overlays (container mask)
	{
		ProgressDisplay progress("processing overlays (container mask)");

		char containerBoundsPath[1024] = "";
		sprintf(containerBoundsPath, RS_ASSETS "/non_final/heightmap/masks/container_mask_%d.dds", resolution);

		for (int span = 0; span < h; span += wm.m_spanHeight)
		{
			float* containerBounds = LoadImageDDSSpan(containerBoundsPath, w, span, wm.m_spanHeight);

			if (containerBounds)
			{
				OverlayMaskImage(image + span*w, w, wm.m_spanHeight, containerBounds, Vec3(1.0f, 1.0f, 1.0f), 0.75f, true);
				delete[] containerBounds;
			}
			else
			{
				break;
			}

			progress.Update(span/wm.m_spanHeight, h/wm.m_spanHeight);
		}

		progress.End();
	}

	// process overlays (container bounds)
	{
		ProgressDisplay progress("processing overlays (container bounds)");

		char containerBoundsPath[1024] = "";
		sprintf(containerBoundsPath, RS_ASSETS "/non_final/heightmap/masks/container_bounds_%d.dds", resolution);

		for (int span = 0; span < h; span += wm.m_spanHeight)
		{
			float* containerBounds = LoadImageDDSSpan(containerBoundsPath, w, span, wm.m_spanHeight);

			if (containerBounds)
			{
				OverlayMaskImage(image + span*w, w, wm.m_spanHeight, containerBounds, Vec3(0.0f, 0.0f, 0.0f), 0.75f, true);
				delete[] containerBounds;
			}
			else
			{
				break;
			}

			progress.Update(span/wm.m_spanHeight, h/wm.m_spanHeight);
		}

		progress.End();
	}

	char pngPath[1024] = "";
	strcpy(pngPath, varString("%s/composite_ped_density.png", path).c_str());
	SaveImagePixel32(pngPath, image, w, h, bAutoOpen);

	delete[] image;
}

static void CompositeLightDensityImage(const WorldMask& wm, int resolution, const char* type, const char* suffix, const char* outputName, const char* path, bool bAutoOpen, float exponent = 1.0f, float rescale = 1.0f)
{
	fprintf(stdout, "compositing %s density image ...\n", type);

	const int w = wm.m_w;
	const int h = wm.m_h;

	Pixel32* image = NULL;

	// create image
	{
		ProgressDisplay progress("creating image");

		image = new Pixel32[w*h];

		for (int i = 0; i < w*h; i++)
		{
			image[i].r = 255;
			image[i].g = 255;
			image[i].b = 255;
			image[i].a = 255;
		}

		progress.End();
	}

	// process overlays (heightmap)
	{
		ProgressDisplay progress("processing overlays (heightmap)");

		for (int span = 0; span < h; span += wm.m_spanHeight)
		{
			const int margin = resolution;
			const int spanY0 = Max<int>(0, span - margin);
			const int spanY1 = Min<int>(span + wm.m_spanHeight + margin, h);

			float* heightmap = LoadImageDDSSpan(varString("%s/assembled_max.dds", path).c_str(), w, spanY0, spanY1 - spanY0);

			if (heightmap)
			{
				const int marginY0 = span - spanY0;
				const int marginY1 = spanY1 - (span + wm.m_spanHeight);

				OverlayHeightMap(image + span*w, w, wm.m_spanHeight, heightmap + marginY0*w, Vec3(0.0f, 0.0f, 0.0f), 0.125f, margin, marginY0, marginY1);
				delete[] heightmap;
			}
			else
			{
				break;
			}

			progress.Update(span/wm.m_spanHeight, h/wm.m_spanHeight);
		}

		progress.End();
	}

	// process overlays (roads mask)
	{
		ProgressDisplay progress("processing overlays (roads mask)");

		char roadsPath[1024] = "";
		sprintf(roadsPath, RS_ASSETS "/non_final/heightmap/masks/roads_%d.dds", resolution);

		for (int span = 0; span < h; span += wm.m_spanHeight)
		{
		//	float* roadsMask = LoadImageDDSSpan(varString("%s/assembled_roads.dds", path).c_str(), w, span, wm.m_spanHeight);
			float* roadsMask = LoadImageDDSSpan(roadsPath, w, span, wm.m_spanHeight);

			if (roadsMask)
			{
				OverlayMaskImage(image + span*w, w, wm.m_spanHeight, roadsMask, Vec3(0.37f, 0.25f, 0.20f), 0.15f, true);
				delete[] roadsMask;
			}
			else
			{
				break;
			}

			progress.Update(span/wm.m_spanHeight, h/wm.m_spanHeight);
		}

		progress.End();
	}

	// process overlays (poly density)
	{
		ProgressDisplay progress("processing overlays (%s density)", type);

		for (int span = 0; span < h; span += wm.m_spanHeight)
		{
			float* polyDensityMap = LoadImageDDSSpan(varString("%s/assembled%s.dds", path, suffix).c_str(), w, span, wm.m_spanHeight);

			if (polyDensityMap)
			{
				for (int j = span; j < span + wm.m_spanHeight; j++) // mask pixels that are outside the world
				{
					for (int i = 0; i < w; i++)
					{
						float& density = polyDensityMap[i + (j - span)*w];

						density = (density - 192.0f/255.0f)/(1.0f - 192.0f/255.0f);

						if (density < 1.0f)
						{
							if (exponent != 1.0f)
							{
								density = powf(density, exponent);
							}

							density *= rescale;
						}

						density = Clamp<float>(density, 0.0f, 1.0f);
					}
				}

				OverlayPolyDensityMap(image + span*w, w, wm.m_spanHeight, polyDensityMap, 1.0f, false, false);
				delete[] polyDensityMap;
			}
			else
			{
				break;
			}

			progress.Update(span/wm.m_spanHeight, h/wm.m_spanHeight);
		}

		progress.End();
	}

	// process overlays (water mask)
	{
		ProgressDisplay progress("processing overlays (water mask)");

		for (int span = 0; span < h; span += wm.m_spanHeight)
		{
			float* waterMask = LoadImageDDSSpan(varString("%s/assembled_water.dds", path).c_str(), w, span, wm.m_spanHeight);

			if (waterMask)
			{
				for (int j = span; j < span + wm.m_spanHeight; j++) // mask pixels that are outside the world
				{
					for (int i = 0; i < w; i++)
					{
						if (i < wm.m_worldMinX[j] || i > wm.m_worldMaxX[j] ||
							j < wm.m_worldMinY[i] || j > wm.m_worldMaxY[i])
						{
							waterMask[i + (j - span)*w] = 1.0f;
						}
					}
				}

				OverlayMaskImage(image + span*w, w, wm.m_spanHeight, waterMask, Vec3(0.0f, 0.0f, 1.0f), 0.125f, false);
				delete[] waterMask;
			}
			else
			{
				break;
			}

			progress.Update(span/wm.m_spanHeight, h/wm.m_spanHeight);
		}

		progress.End();
	}

	// process overlays (container mask)
	{
		ProgressDisplay progress("processing overlays (container mask)");

		char containerBoundsPath[1024] = "";
		sprintf(containerBoundsPath, RS_ASSETS "/non_final/heightmap/masks/container_mask_%d.dds", resolution);

		for (int span = 0; span < h; span += wm.m_spanHeight)
		{
			float* containerBounds = LoadImageDDSSpan(containerBoundsPath, w, span, wm.m_spanHeight);

			if (containerBounds)
			{
				OverlayMaskImage(image + span*w, w, wm.m_spanHeight, containerBounds, Vec3(1.0f, 1.0f, 1.0f), 0.25f, true);
				delete[] containerBounds;
			}
			else
			{
				break;
			}

			progress.Update(span/wm.m_spanHeight, h/wm.m_spanHeight);
		}

		progress.End();
	}

	// process overlays (container bounds)
	{
		ProgressDisplay progress("processing overlays (container bounds)");

		char containerBoundsPath[1024] = "";
		sprintf(containerBoundsPath, RS_ASSETS "/non_final/heightmap/masks/container_bounds_%d.dds", resolution);

		for (int span = 0; span < h; span += wm.m_spanHeight)
		{
			float* containerBounds = LoadImageDDSSpan(containerBoundsPath, w, span, wm.m_spanHeight);

			if (containerBounds)
			{
				OverlayMaskImage(image + span*w, w, wm.m_spanHeight, containerBounds, Vec3(0.0f, 0.0f, 0.0f), 0.5f, true);
				delete[] containerBounds;
			}
			else
			{
				break;
			}

			progress.Update(span/wm.m_spanHeight, h/wm.m_spanHeight);
		}

		progress.End();
	}

	char pngPath[1024] = "";
	strcpy(pngPath, varString("%s/composite%s.png", path, outputName).c_str());
	SaveImagePixel32(pngPath, image, w, h, bAutoOpen);

	delete[] image;
}

static void CompositeGenericGlowImage(const WorldMask& wm, int resolution, const char* path, const char* outName, int glow, const char** glowTypeStrings, const char** glowTypeSuffix, const Vec3* glowTypeColours, int glowTypeCount, bool bInvert, bool bAutoOpen)
{
	fprintf(stdout, "compositing %s image ...\n", outName + 1);

	const int w = wm.m_w;
	const int h = wm.m_h;

	Pixel32* image = NULL;

	// create image
	{
		ProgressDisplay progress("creating image");

		image = new Pixel32[w*h];

		for (int i = 0; i < w*h; i++)
		{
			image[i].r = 255;
			image[i].g = 255;
			image[i].b = 255;
			image[i].a = 255;
		}

		progress.End();
	}

	// process overlays (heightmap)
	{
		ProgressDisplay progress("processing overlays (heightmap)");

		for (int span = 0; span < h; span += wm.m_spanHeight)
		{
			const int margin = resolution;
			const int spanY0 = Max<int>(0, span - margin);
			const int spanY1 = Min<int>(span + wm.m_spanHeight + margin, h);

			float* heightmap = LoadImageDDSSpan(varString("%s/assembled_max.dds", path).c_str(), w, spanY0, spanY1 - spanY0);

			if (heightmap)
			{
				const int marginY0 = span - spanY0;
				const int marginY1 = spanY1 - (span + wm.m_spanHeight);

				OverlayHeightMap(image + span*w, w, wm.m_spanHeight, heightmap + marginY0*w, Vec3(0.0f, 0.0f, 0.0f), 0.125f, margin, marginY0, marginY1);
				delete[] heightmap;
			}
			else
			{
				break;
			}

			progress.Update(span/wm.m_spanHeight, h/wm.m_spanHeight);
		}

		progress.End();
	}

	// process overlays (roads mask)
	{
		ProgressDisplay progress("processing overlays (roads mask)");

		char roadsPath[1024] = "";
		sprintf(roadsPath, RS_ASSETS "/non_final/heightmap/masks/roads_%d.dds", resolution);

		for (int span = 0; span < h; span += wm.m_spanHeight)
		{
		//	float* roadsMask = LoadImageDDSSpan(varString("%s/assembled_roads.dds", path).c_str(), w, span, wm.m_spanHeight);
			float* roadsMask = LoadImageDDSSpan(roadsPath, w, span, wm.m_spanHeight);

			if (roadsMask)
			{
				OverlayMaskImage(image + span*w, w, wm.m_spanHeight, roadsMask, Vec3(0.37f, 0.25f, 0.20f), 0.125f, true);
				delete[] roadsMask;
			}
			else
			{
				break;
			}

			progress.Update(span/wm.m_spanHeight, h/wm.m_spanHeight);
		}

		progress.End();
	}

	// process overlays (water mask)
	{
		ProgressDisplay progress("processing overlays (water mask)");

		for (int span = 0; span < h; span += wm.m_spanHeight)
		{
			float* waterMask = LoadImageDDSSpan(varString("%s/assembled_water.dds", path).c_str(), w, span, wm.m_spanHeight);

			if (waterMask)
			{
				for (int j = span; j < span + wm.m_spanHeight; j++) // mask pixels that are outside the world
				{
					for (int i = 0; i < w; i++)
					{
						if (i < wm.m_worldMinX[j] || i > wm.m_worldMaxX[j] ||
							j < wm.m_worldMinY[i] || j > wm.m_worldMaxY[i])
						{
							waterMask[i + (j - span)*w] = 1.0f;
						}
					}
				}

				OverlayMaskImage(image + span*w, w, wm.m_spanHeight, waterMask, Vec3(0.0f, 0.0f, 1.0f), 0.125f, false);
				delete[] waterMask;
			}
			else
			{
				break;
			}

			progress.Update(span/wm.m_spanHeight, h/wm.m_spanHeight);
		}

		progress.End();
	}

	// process overlays (container bounds)
	{
		ProgressDisplay progress("processing overlays (container bounds)");

		char containerBoundsPath[1024] = "";
		sprintf(containerBoundsPath, RS_ASSETS "/non_final/heightmap/masks/container_bounds_%d.dds", resolution);

		for (int span = 0; span < h; span += wm.m_spanHeight)
		{
			float* containerBounds = LoadImageDDSSpan(containerBoundsPath, w, span, wm.m_spanHeight);

			if (containerBounds)
			{
				OverlayMaskImage(image + span*w, w, wm.m_spanHeight, containerBounds, Vec3(0.0f, 0.0f, 0.0f), 0.33f, true);
				delete[] containerBounds;
			}
			else
			{
				break;
			}

			progress.Update(span/wm.m_spanHeight, h/wm.m_spanHeight);
		}

		progress.End();
	}

	for (int glowType = glowTypeCount - 1; glowType >= 0; glowType--)
	{
		const char* glowTypeStr = glowTypeStrings[glowType];
		const Vec3& glowTypeCol = glowTypeColours[glowType];

		u16* glowImage        = NULL;
		u8*  glowTileMask     = NULL;
		int  glowTileResX     = 0;
		int  glowTileResY     = 0;
		int  glowTileNumCols  = 0;
		int  glowTileNumRows  = 0;
		int  glowTileCoverage = 0;

		u8* worldMask = NULL;

		if (strcmp(glowTypeSuffix[glowType], "wld") == 0)
		{
			if (!bInvert)
			{
				const int worldMaskSize = (w*h + 7)/8;
				worldMask = new u8[worldMaskSize];

				for (int i = 0; i < worldMaskSize; i++)
				{
					worldMask[i] = ~wm.m_worldMask[i];
				}
			}
			else
			{
				worldMask = wm.m_worldMask;
			}
		}
		else // load glow mask
		{
			ProgressDisplay progress("loading %s glow mask", glowTypeStr);

			const int worldMaskSize = (w*h + 7)/8;
			worldMask = new u8[worldMaskSize];
			memset(worldMask, 0, worldMaskSize*sizeof(u8));

			for (int span = 0; span < h; span += wm.m_spanHeight)
			{
				float* temp = LoadImageDDSSpan(varString("%s/assembled_%s.dds", path, glowTypeSuffix[glowType]).c_str(), w, span, wm.m_spanHeight);

				if (temp)
				{
					for (int j = span; j < span + wm.m_spanHeight; j++)
					{
						for (int i = 0; i < w; i++)
						{
							if (temp[i + (j - span)*w] > 0.0f)
							{
								const int index = i + j*w;

								worldMask[index/8] |= BIT(index%8);
							}
						}
					}

					delete[] temp;
				}
				else
				{
					delete[] worldMask;
					worldMask = NULL;
					progress.End("mask not found");
					break;
				}

				progress.Update(span/wm.m_spanHeight, h/wm.m_spanHeight);
			}

			if (worldMask)
			{
				if (!bInvert)
				{
					for (int i = 0; i < worldMaskSize; i++)
					{
						worldMask[i] ^= 0xff;
					}
				}
			}
			else
			{
				continue;
			}

			progress.End();
		}

		// set up glow tile mask
		{
			ProgressDisplay progress("setting up %s glow tile mask", glowTypeStr);
#if 1
			const int circleRadius = 12;
			const u8 circle[circleRadius*2 + 1][circleRadius*2 + 1] =
			{
				{0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0},
				{0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0},
				{0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0},
				{0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0},
				{0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0},
				{0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0},
				{0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0},
				{0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0},
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
				{1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1},
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
				{0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0},
				{0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0},
				{0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0},
				{0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0},
				{0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0},
				{0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0},
				{0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0},
				{0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0},
			};
#elif 1
			const int circleRadius = 6;
			const u8 circle[circleRadius*2 + 1][circleRadius*2 + 1] =
			{
				{0,0,0,0,1,1,1,1,1,0,0,0,0},
				{0,0,1,1,1,1,1,1,1,1,1,0,0},
				{0,1,1,1,1,1,1,1,1,1,1,1,0},
				{0,1,1,1,1,1,1,1,1,1,1,1,0},
				{1,1,1,1,1,1,1,1,1,1,1,1,1},
				{1,1,1,1,1,1,1,1,1,1,1,1,1},
				{1,1,1,1,1,1,0,1,1,1,1,1,1},
				{1,1,1,1,1,1,1,1,1,1,1,1,1},
				{1,1,1,1,1,1,1,1,1,1,1,1,1},
				{0,1,1,1,1,1,1,1,1,1,1,1,0},
				{0,1,1,1,1,1,1,1,1,1,1,1,0},
				{0,0,1,1,1,1,1,1,1,1,1,0,0},
				{0,0,0,0,1,1,1,1,1,0,0,0,0},
			};
#else
			const int circleRadius = 0;
			const u8 circle[circleRadius*2 + 1][circleRadius*2 + 1] = {0};
#endif
			if (w%5 == 0 && h%5 == 0)
			{
				glowTileResX = Max<int>(50, glow + circleRadius);
				glowTileResY = Max<int>(50, glow + circleRadius);

				while (w%glowTileResX) { glowTileResX += 5; } // find evenly divisible sizes .. could use GCD or something
				while (h%glowTileResY) { glowTileResY += 5; }
			}
			else // just one big tile
			{
				glowTileResX = w;
				glowTileResY = h;
			}

			glowTileNumCols = w/glowTileResX;
			glowTileNumRows = h/glowTileResY;

			const int glowTileMaskSize = (glowTileNumRows*glowTileNumCols + 7)/8;

			glowTileMask = new u8[glowTileMaskSize];
			memset(glowTileMask, 0x00, glowTileMaskSize*sizeof(u8));

			glowImage = new u16[w*h];
			memset(glowImage, 0xff, w*h*sizeof(u16));

			for (int tileY = 0; tileY < glowTileNumRows; tileY++)
			{
				for (int tileX = 0; tileX < glowTileNumCols; tileX++)
				{
					bool bTileHasEmptyWorldCells = false;

					for (int y = 0; y < glowTileResY; y++)
					{
						for (int x = 0; x < glowTileResX; x++)
						{
							const int index = (x + tileX*glowTileResX) + (y + tileY*glowTileResY)*w;

							if ((worldMask[index/8] & BIT(index%8)) == 0)
							{
								glowImage[index] = 0;
								bTileHasEmptyWorldCells = true;

								if (circleRadius > 0)
								{
									for (int dy = -circleRadius; dy <= circleRadius; dy++)
									{
										for (int dx = -circleRadius; dx <= circleRadius; dx++)
										{
											if (dx|dy)
											{
												const int x2 = x + tileX*glowTileResX + dx;
												const int y2 = y + tileY*glowTileResY + dy;

												if (x2 >= 0 && x2 < w &&
													y2 >= 0 && y2 < h)
												{
													if (circle[dx + circleRadius][dy + circleRadius])
													{
														const int index2 = x2 + y2*w;

														glowImage[index2] = Min<u16>(0xc000, glowImage[index2]);
													}
												}
											}
										}
									}
								}
							}
						}
					}

					if (bTileHasEmptyWorldCells)
					{
						for (int dy = -1; dy <= 1; dy++)
						{
							for (int dx = -1; dx <= 1; dx++)
							{
								const int tileXX = tileX + dx;
								const int tileYY = tileY + dy;

								if (tileXX >= 0 && tileXX < glowTileNumCols &&
									tileYY >= 0 && tileYY < glowTileNumRows)
								{
									const int tileIndex = tileXX + tileYY*glowTileNumCols;

									if ((glowTileMask[tileIndex/8] & BIT(tileIndex%8)) == 0)
									{
										glowTileMask[tileIndex/8] |= BIT(tileIndex%8);
										glowTileCoverage++;
									}
								}
							}
						}
					}
				}

				progress.Update(tileY, glowTileNumRows);
			}

			if (worldMask != wm.m_worldMask)
			{
				delete[] worldMask;
			}

			progress.End();
		}

		// process glow
		{
			ProgressDisplay progress("processing %s glow (%d passes, %d %dx%d tiles, %.2f%% coverage)", glowTypeStr, glow, glowTileCoverage, glowTileResX, glowTileResY, 100.0f*(float)glowTileCoverage/(float)(glowTileNumRows*glowTileNumCols));

			u16* tempSrc = glowImage;
			u16* tempDst = new u16[w*h];

			memcpy(tempDst, tempSrc, w*h*sizeof(u16));

			for (int pass = 0; pass < glow; pass++)
			{
				for (int tileY = 0; tileY < glowTileNumRows; tileY++)
				{
					for (int tileX = 0; tileX < glowTileNumCols; tileX++)
					{
						const int tileIndex = tileX + tileY*glowTileNumCols;

						if (glowTileMask[tileIndex/8] & BIT(tileIndex%8))
						{
							for (int y = 0; y < glowTileResY; y++)
							{
								for (int x = 0; x < glowTileResX; x++)
								{
									const int xx = x + tileX*glowTileResX;
									const int yy = y + tileY*glowTileResY;
									const int index = xx + yy*w;

									float sum = 0.0f;

									for (int dy = -1; dy <= 1; dy++)
									{
										for (int dx = -1; dx <= 1; dx++)
										{
											const int xxx = Clamp<int>(xx + dx, 0, w - 1);
											const int yyy = Clamp<int>(yy + dy, 0, h - 1);

											sum += (float)tempSrc[xxx + yyy*w];
										}
									}

									tempDst[index] = Min<u16>((u16)(0.5f + sum/9.0f), tempSrc[index]);
								}
							}
						}
					}
				}

				std::swap(tempSrc, tempDst);

				progress.Update(pass, glow);
			}

			glowImage = tempSrc;

			delete[] tempDst;
			delete[] glowTileMask;

			progress.End();
		}

		// apply glow
		{
			ProgressDisplay progress("applying %s glow", glowTypeStr);

			for (int i = 0; i < w*h; i++)
			{
				float r = (float)image[i].r/255.0f;
				float g = (float)image[i].g/255.0f;
				float b = (float)image[i].b/255.0f;

				const float a = (float)glowImage[i]/65535.0f;

				r += (glowTypeCol.x - r)*(1.0f - a);
				g += (glowTypeCol.y - g)*(1.0f - a);
				b += (glowTypeCol.z - b)*(1.0f - a);

				image[i].r = (u8)(0.5f + 255.0f*r);
				image[i].g = (u8)(0.5f + 255.0f*g);
				image[i].b = (u8)(0.5f + 255.0f*b);
				image[i].a = 255;
			}

			delete[] glowImage;

			progress.End();
		}
	}

	char pngPath[1024] = "";
	strcpy(pngPath, varString("%s/composite%s.png", path, outName).c_str());
	SaveImagePixel32(pngPath, image, w, h, bAutoOpen);

	delete[] image;
}

static void CompositeCollisionHolesImage(const WorldMask& wm, int resolution, const char* path, int glow, bool bAutoOpen)
{
	const char* glowTypeStrings[] =
	{
		"world",
		"mover bound",
		"weapon bound",
		"vehicle bound",
	};

	const char* glowTypeSuffix[] =
	{
		"wld",
		"mov",
		"wpn",
		"veh",
	};

	const Vec3 glowTypeColours[] =
	{
		Vec3(1.0f, 0.0f, 0.0f),
		Vec3(1.0f, 0.5f, 0.0f),
		Vec3(0.5f, 0.0f, 1.0f),
		Vec3(0.0f, 1.0f, 0.0f),
	};

	CompositeGenericGlowImage(wm, resolution, path, "_collision_holes", glow, glowTypeStrings, glowTypeSuffix, glowTypeColours, NELEM(glowTypeColours), true, bAutoOpen);
}

static void CompositeStairsGlowImage(const WorldMask& wm, int resolution, const char* path, int glow, bool bAutoOpen)
{
	const char* glowTypeStrings[] =
	{
		"stairs",
		"stairslope",
	};

	const char* glowTypeSuffix[] =
	{
		"stairs",
		"stairslope",
	};

	const Vec3 glowTypeColours[] =
	{
		Vec3(1.0f, 0.0f, 0.0f),
		Vec3(0.0f, 0.0f, 1.0f),
	};

	CompositeGenericGlowImage(wm, resolution, path, "_stairs", glow, glowTypeStrings, glowTypeSuffix, glowTypeColours, NELEM(glowTypeColours), false, bAutoOpen);
}

static void CompositeScriptGlowImage(const WorldMask& wm, int resolution, const char* path, int glow, bool bAutoOpen)
{
	const char* glowTypeStrings[] =
	{
		"script",
	};

	const char* glowTypeSuffix[] =
	{
		"script",
	};

	const Vec3 glowTypeColours[] =
	{
		Vec3(1.0f, 0.0f, 0.0f),
	};

	CompositeGenericGlowImage(wm, resolution, path, "_script", glow, glowTypeStrings, glowTypeSuffix, glowTypeColours, NELEM(glowTypeColours), false, bAutoOpen);
}

static void CompositeCablesGlowImage(const WorldMask& wm, int resolution, const char* path, int glow, bool bAutoOpen)
{
	const char* glowTypeStrings[] =
	{
		"cables",
	};

	const char* glowTypeSuffix[] =
	{
		"cables",
	};

	const Vec3 glowTypeColours[] =
	{
		Vec3(1.0f, 0.0f, 0.0f),
	};

	CompositeGenericGlowImage(wm, resolution, path, "_cables", glow, glowTypeStrings, glowTypeSuffix, glowTypeColours, NELEM(glowTypeColours), false, bAutoOpen);
}

static void CompositeCoronasGlowImage(const WorldMask& wm, int resolution, const char* path, int glow, bool bAutoOpen)
{
	const char* glowTypeStrings[] =
	{
		"coronas",
	};

	const char* glowTypeSuffix[] =
	{
		"coronas",
	};

	const Vec3 glowTypeColours[] =
	{
		Vec3(1.0f, 0.0f, 0.0f),
	};

	CompositeGenericGlowImage(wm, resolution, path, "_coronas", glow, glowTypeStrings, glowTypeSuffix, glowTypeColours, NELEM(glowTypeColours), false, bAutoOpen);
}

static void CompositeAnimBldgGlowImage(const WorldMask& wm, int resolution, const char* path, int glow, bool bAutoOpen)
{
	const char* glowTypeStrings[] =
	{
		"animbldg",
	};

	const char* glowTypeSuffix[] =
	{
		"animbldg",
	};

	const Vec3 glowTypeColours[] =
	{
		Vec3(1.0f, 0.0f, 0.0f),
	};

	CompositeGenericGlowImage(wm, resolution, path, "_animbldg", glow, glowTypeStrings, glowTypeSuffix, glowTypeColours, NELEM(glowTypeColours), false, bAutoOpen);
}

static void CompositeVehicleHoleGlowImage(const WorldMask& wm, int resolution, const char* path, int glow, bool bAutoOpen)
{
	const char* glowTypeStrings[] =
	{
		"veh",
	};

	const char* glowTypeSuffix[] =
	{
		"veh",
	};

	const Vec3 glowTypeColours[] =
	{
		Vec3(1.0f, 0.0f, 0.0f),
	};

	CompositeGenericGlowImage(wm, resolution, path, "_vehicle_holes", glow, glowTypeStrings, glowTypeSuffix, glowTypeColours, NELEM(glowTypeColours), true, bAutoOpen);
}

static void CompositeMoverNoVehGlowImage(const WorldMask& wm, int resolution, const char* path, int glow, bool bAutoOpen)
{
	const char* glowTypeStrings[] =
	{
		"movnoveh_m",
		"movnoveh_p",
	};

	const char* glowTypeSuffix[] =
	{
		"movnoveh_m",
		"movnoveh_p",
	};

	const Vec3 glowTypeColours[] =
	{
		Vec3(1.0f, 0.0f, 0.0f),
		Vec3(0.0f, 0.0f, 1.0f),
	};

	CompositeGenericGlowImage(wm, resolution, path, "_movnoveh", glow, glowTypeStrings, glowTypeSuffix, glowTypeColours, NELEM(glowTypeColours), false, bAutoOpen);
}

static void CompositeGenericNoOutliersImage(const float* image, int w, int h, const char* outputName, const char* path, float outliersLower, float outliersUpper, int numBuckets, float histoScale, float imageScale, bool bInvert, bool bAutoOpen)
{
	float* temp = new float[w*h];
	memcpy(temp, image, w*h*sizeof(float));

	int index0 = 0;
	int index1 = 0;

	float valueMin = 0.0f;
	float valueMax = 0.0f;

	if (outliersLower > 0.0f ||
		outliersUpper > 0.0f)
	{
		// sort
		{
			ProgressDisplay progress("sorting");

			class CompareFloats { public: static int func(const void* pVoidA, const void* pVoidB)
			{
				const float* pA = (const float*)pVoidA;
				const float* pB = (const float*)pVoidB;
				if ((*pA) > (*pB)) { return +1; }
				if ((*pA) < (*pB)) { return -1; }
				return 0;
			}};
			qsort(temp, w*h, sizeof(float), CompareFloats::func);

			progress.End();
		}

		index0 = Clamp<int>((int)ceilf(outliersLower*(float)(w*h)), 0, w*h - 1);
		index1 = Clamp<int>((int)floorf((1.0f - outliersUpper)*(float)(w*h)), 0, w*h - 1);

		valueMin = temp[index0];
		valueMax = temp[index1];

		fprintf(stdout, "range = [%f..%f], excluding outliers = [%f..%f]\n", temp[0], temp[w*h - 1], valueMin, valueMax); 
	}
	else
	{
		memcpy(temp, image, w*h*sizeof(float)); // just copy it

		index0 = 0;
		index1 = w*h - 1;

		valueMin = image[0];
		valueMax = image[0];

		for (int i = 1; i < w*h; i++)
		{
			valueMin = Min<float>(image[i], valueMin);
			valueMax = Max<float>(image[i], valueMax);
		}

		fprintf(stdout, "range = [%f..%f]\n", valueMin, valueMax);
	}

	if (numBuckets > 0 && valueMin < valueMax)
	{
		int* histogram = new int[numBuckets];
		memset(histogram, 0, numBuckets*sizeof(int));

		for (int i = index0; i <= index1; i++)
		{
			const float t = (temp[i] - valueMin)/(valueMax - valueMin); // [0..1]
			const int bucket = Min<int>((int)(t*(float)numBuckets), numBuckets - 1);

			histogram[bucket]++;
		}

		int histogramMax = 0;

		for (int bucket = 0; bucket < numBuckets; bucket++)
		{
			histogramMax = Max<int>(histogram[bucket], histogramMax);
		}

		const int graphW = numBuckets;
		const int graphH = graphW/4;
		float* graph = new float[graphW];
		float* graphImage = new float[graphW*graphH];

		for (int bucket = 0; bucket < numBuckets; bucket++)
		{
			graph[bucket] = (float)histogram[bucket]/(float)histogramMax; // [0..1]
		}

		if (histoScale != 1.0f)
		{
			for (int bucket = 0; bucket < numBuckets; bucket++)
			{
				graph[bucket] = Clamp<float>(graph[bucket]*histoScale, 0.0f, 1.0f);
			}
		}

		Graph(graphImage, graph, graphW, graphH);

		SaveImage(varString("%s/assembled%s_histogram.png", path, outputName).c_str(), graphImage, graphW, graphH, false);

		delete[] histogram;
		delete[] graph;
		delete[] graphImage;
	}

	for (int i = 0; i < w*h; i++)
	{
		temp[i] = Clamp<float>((image[i]*imageScale - valueMin)/(valueMax - valueMin), 0.0f, 1.0f);

		if (bInvert)
		{
			temp[i] = 1.0f - temp[i];
		}
	}

	SaveImage(varString("%s/assembled%s.png", path, outputName).c_str(), temp, w, h, bAutoOpen);

	delete[] temp;
}

static void CompositeGenericNoOutliersImage2(const char* suffix, const char* outputName, const char* path, float outliersLower, float outliersUpper, int numBuckets, float histoScale, float imageScale, float exp, bool bInvert, bool bAutoOpen)
{
	int w = 0;
	int h = 0;
	float* image = LoadImageDDS(varString("%s/assembled%s.dds", path, suffix).c_str(), w, h);

	if (image)
	{
		if (exp != 1.0f)
		{
			for (int i = 0; i < w*h; i++)
			{
				image[i] = powf(image[i], exp);
			}
		}

		CompositeGenericNoOutliersImage(image, w, h, outputName, path, outliersLower, outliersUpper, numBuckets, histoScale, imageScale, bInvert, bAutoOpen);
		delete[] image;
	}
}

static void CompositeMinMaxHeightDiffImage(const char* path, float exp, bool bAutoOpen)
{
	int w = 0;
	int h = 0;
	float* heightMax = LoadImageDDS(varString("%s/assembled_max.dds", path).c_str(), w, h);

	if (heightMax)
	{
		float* heightMin = LoadImageDDS(varString("%s/assembled_min.dds", path).c_str(), w, h);

		if (heightMin)
		{
			float maxDiff = 0.0f;

			for (int i = 0; i < w*h; i++)
			{
				maxDiff = Max<float>(heightMax[i] - heightMin[i], maxDiff);
			}

			for (int i = 0; i < w*h; i++)
			{
				heightMax[i] = powf((heightMax[i] - heightMin[i])/maxDiff, exp);
			}

			CompositeGenericNoOutliersImage(heightMax, w, h, "_height_diff", path, 0.2f, 0.005f, 1024, 10.0f, 1.0f, true, bAutoOpen);
			//SaveImage(varString("%s/assembled_height_diff.png", path).c_str(), heightMax, w, h, bAutoOpen);

			delete[] heightMin;
		}

		delete[] heightMax;
	}
}

static void CompositeUnderwaterHeightImage(const char* path)
{
	int w = 0;
	int h = 0;
	float* heightMax = LoadImageDDS(varString("%s/assembled_max.dds", path).c_str(), w, h);

	if (heightMax)
	{
		float* heightMaxNeg = LoadImageDDS(varString("%s/assembled_maxneg.dds", path).c_str(), w, h);

		if (heightMaxNeg)
		{
			u16* temp = new u16[w*h];

			for (int i = 0; i < w*h; i++)
			{
				if (heightMax[i] > 0.0f)
				{
					temp[i] = (u16)Clamp<float>(0.5f + 65535.0f*(0.5f + 0.5f*heightMax[i]), 0.0f, 65535.0f);
				}
				else
				{
					temp[i] = (u16)Clamp<float>(0.5f + 65535.0f*(0.5f - 0.5f*heightMaxNeg[i]), 0.0f, 65535.0f);
				}
			}

			SaveImage(varString("%s/assembled_underwater.png", path).c_str(), temp, w, h, false, true);

			delete[] temp;
			delete[] heightMaxNeg;
		}

		delete[] heightMax;
	}
}

static void CompositeBoundIDEdgesImage(const char* path, const char* ext, int downsample)
{
	int w = 0;
	int h = 0;
	Pixel32* boundIDImage = LoadImagePixel32(varString("%s/assembled%s.png", path, ext).c_str(), w, h);

	if (boundIDImage)
	{
		ProgressDisplay progress("processing bound ID edges");

		const int w2 = w/downsample;
		const int h2 = h/downsample;

		float* edgeImage = new float[w2*h2];

		for (int j = 0; j <= h - downsample; j += downsample)
		{
			for (int i = 0; i <= w - downsample; i += downsample)
			{
				int sum = 0;

				for (int jj = 0; jj < downsample; jj++)
				{
					for (int ii = 0; ii < downsample; ii++)
					{
						Pixel32 c00 = boundIDImage[(i + ii) + (j + jj)*w];
						bool bIsEdge = false;

						for (int dj = -1; dj <= 1; dj++)
						{
							for (int di = -1; di <= 1; di++)
							{
								const int i2 = Clamp<int>(i + ii + di, 0, w - 1);
								const int j2 = Clamp<int>(j + jj + dj, 0, h - 1);

								if (boundIDImage[i2 + j2*w] != c00)
								{
									bIsEdge = true;
									break;
								}
							}

							if (bIsEdge)
							{
								break;
							}
						}

						if (bIsEdge)
						{
							sum++;
						}
					}
				}

				edgeImage[(i/downsample) + (j/downsample)*w2] = 1.0f - (float)sum/(float)(downsample*downsample);
			}

			progress.Update(j, h);
		}

		progress.End();

		SaveImage(varString("%s/assembled%s_edges.png", path, ext).c_str(), edgeImage, w2, h2, false);
		delete[] edgeImage;
		delete[] boundIDImage;
	}
}

static void DoubleResolutionRGB(const char* dstPath, const char* srcPath)
{
	int w = 0;
	int h = 0;
	Vec3* image = LoadImageRGB(srcPath, w, h);

	if (image)
	{
		Vec3* temp = new Vec3[w*h*4];

		for (int j = 0; j < h; j++)
		{
			for (int i = 0; i < w; i++)
			{
				temp[(i*2 + 0) + (j*2 + 0)*(w*2)] = image[i + j*w];
				temp[(i*2 + 1) + (j*2 + 0)*(w*2)] = image[i + j*w];
				temp[(i*2 + 0) + (j*2 + 1)*(w*2)] = image[i + j*w];
				temp[(i*2 + 1) + (j*2 + 1)*(w*2)] = image[i + j*w];
			}
		}

		SaveImageRGB(dstPath, temp, w*2, h*2, false);

		delete[] temp;
		delete[] image;
	}
}

static void QuarterResolution(const char* dstPath, const char* srcPath, bool bNormalise, bool bSaveNormalisedSourceAsPNG = false)
{
	int w = 0;
	int h = 0;
	u8* image = LoadImageU8(srcPath, w, h);

	if (image)
	{
		if (w >= 4 && h >= 4)
		{
			if (bNormalise)
			{
				u8 valueMin = image[0];
				u8 valueMax = image[0];

				for (int i = 1; i < w*h; i++)
				{
					valueMin = Min<u8>(image[i], valueMin);
					valueMax = Max<u8>(image[i], valueMax);
				}

				if (valueMin < valueMax)
				{
					for (int i = 0; i < w*h; i++)
					{
						image[i] = (u8)(0.5f + 255.0f*(float)(image[i] - valueMin)/(float)(valueMax - valueMin));
					}
				}

				if (bSaveNormalisedSourceAsPNG)
				{
					char pngPath[1024] = "";
					strcpy(pngPath, srcPath);
					strcpy(strrchr(pngPath, '.'), ".png");
					SaveImage(pngPath, image, w, h, false);
				}
			}

			u8* temp = new u8[(w/4)*(h/4)];

			for (int j = 0; j/4 < h/4; j += 4)
			{
				for (int i = 0; i/4 < w/4; i += 4)
				{
					float sum = 0.0f;

					for (int jj = 0; jj < 4; jj++)
					{
						for (int ii = 0; ii < 4; ii++)
						{
							sum += (float)image[(i + ii) + (j + jj)*w];
						}
					}

					temp[(i/4) + (j/4)*(w/4)] = (u8)(0.5f + sum/16.0f);
				}
			}

			SaveImage(dstPath, temp, w/4, h/4, false);

			delete[] temp;
		}

		delete[] image;
	}
}

// this was just a one-off function to help an artist track down a bug ..
static void ShowNonMonochromePixels(const char* path)
{
	int w = 0;
	int h = 0;
	Vec3* image = LoadImageRGB(path, w, h);

	if (image)
	{
		for (int i = 0; i < w*h; i++)
		{
			if (image[i].x != image[i].y ||
				image[i].y != image[i].z ||
				image[i].z != image[i].x)
			{
				image[i].x = 1.0f;
				image[i].y = 0.0f;
				image[i].z = 0.0f;
			}
		}

		char path2[1024] = "";
		strcpy(path2, path);
		strcpy(strrchr(path2, '.'), "_colours.png");

		SaveImageRGB(path2, image, w, h, true);
	}
}

// this is a test for floating-point density maps (floating point so it's easier to control the range, etc.)
static void TestDensityMap()
{
	int w = 0;
	int h = 0;
	float* densityMap = LoadImage  (RS_ASSETS "/non_final/heightmap/heightmap_highres_denA.dds", w, h);
	u8*    worldMask  = LoadImageU8(RS_ASSETS "/non_final/heightmap/heightmap_highres_wld.dds", w, h);

	if (densityMap == NULL || worldMask == NULL)
	{
		return;
	}

	float densityMin = +FLT_MAX;
	float densityMax = -FLT_MAX;

	// ================================
	// A: max(1.0f/max(0.01f, area))
	// B: max(area)
	// C: max(abs(normal.z))
	// D: max(1 - abs(normal.z))
	// E: sum area_2D
	// F: sum area_2D/max(0.01f, area))
	// ================================

	for (int i = 0; i < w*h; i++)
	{
		if (worldMask[i] == 255 && densityMap[i] > 0.001f)
		{
			const float density = logf(sqrtf(densityMap[i]));

			densityMin = Min<float>(density, densityMin);
			densityMax = Max<float>(density, densityMax);
		}
	}

	const int numBuckets = 2048;
	int* histogram = new int[numBuckets];

	memset(histogram, 0, numBuckets*sizeof(int));

	for (int i = 0; i < w*h; i++)
	{
		if (worldMask[i] == 255 && densityMap[i] > 0.001f)
		{
			const float f = (densityMap[i] - densityMin)/(densityMax - densityMin); // [0..1]
			const int bucket = Clamp<int>((int)floorf(f*(float)numBuckets), 0, numBuckets - 1);

			histogram[bucket]++;
		}
	}

	for (int i = 0; i < w*h; i++)
	{
		if (worldMask[i] == 255 && densityMap[i] > 0.001f)
		{
			const float density = logf(densityMap[i]);

			densityMap[i] = (density - densityMin)/(densityMax - densityMin);
		}
		else
		{
			densityMap[i] = 0.0f;
		}
	}

	SaveImage("density_test.png", densityMap, w, h, true);
}

// this is a test for making the heightmap gradient more visible .. i'm not sure it's going to work though
static void TestHeightMapGradient()
{
	const int r = 8;

	int w = 0;
	int h = 0;
	float* heightmap = LoadImage(RS_ASSETS "/non_final/heightmap/heightmap_highres_max.dds", w, h);

	if (heightmap == NULL)
	{
		return;
	}

	float* temp = new float[w*h];

	for (int j = 0; j < h; j++)
	{
		for (int i = 0; i < w; i++)
		{
			float z[3][3];

			for (int dj = -1; dj <= 1; dj++)
			{
				for (int di = -1; di <= 1; di++)
				{
					const int ii = Clamp<int>(i + di, 0, w - 1);
					const int jj = Clamp<int>(j + dj, 0, h - 1);

					z[di + 1][dj + 1] = heightmap[ii + jj*w];
				}
			}

			const float tmp0x = (z[0][0] + 2.0f*z[0][1] + z[0][2])/4.0f;
			const float tmp2x = (z[2][0] + 2.0f*z[2][1] + z[2][2])/4.0f;
			const float tmp0y = (z[0][0] + 2.0f*z[1][0] + z[2][0])/4.0f;
			const float tmp2y = (z[0][2] + 2.0f*z[1][2] + z[2][2])/4.0f;

			const float cx = tmp0x - tmp2x;
			const float cy = tmp0y - tmp2y;

			temp[i + j*w] = Max<float>(Abs<float>(cx), Abs<float>(cy));
		}
	}

	for (int j = 0; j < h; j++)
	{
		for (int i = 0; i < w; i++)
		{
			float zmin = +FLT_MAX;
			float zmax = -FLT_MAX;

			for (int dj = -r; dj <= r; dj++)
			{
				for (int di = -r; di <= r; di++)
				{
					const int ii = i + di;
					const int jj = j + dj;

					if (ii >= 0 && ii < w &&
						jj >= 0 && jj < h)
					{
						zmin = Min<float>(temp[ii + jj*w], zmin);
						zmax = Max<float>(temp[ii + jj*w], zmax);
					}
				}
			}

			if (zmin < zmax)
			{
				heightmap[i + j*w] = 1.0f - powf((temp[i + j*w] - zmin)/(zmax - zmin), 2.0f);
			}
			else
			{
				heightmap[i + j*w] = 1.0f;
			}
		}
	}

	SaveImage(varString("test_r%d.png", r).c_str(), heightmap, w, h, true);
}

static void BuildSearchList(std::vector<std::string>& files, const char* path, const char* ext1)
{
	__system("dir $S $B %s > %s", path, RS_TEMPFILE);

	FILE* fp = fopen(RS_TEMPFILE, "r");

	if (fp)
	{
		char line[1024] = "";

		while (ReadFileLine(line, sizeof(line), fp))
		{
			for (char* s = line; *s; s++) { if (*s == '\\') { *s = '/'; } }
			_strlwr(line);

			const char* ext = strrchr(line, '.');

			if (ext)
			{
				if (ext1 && strcmp(ext, ext1) != 0)
				{
					continue; // doesn't match ext1 or ext2
				}

				files.push_back(line);
			}
		}

		//CloseAndDeleteTempFile(fp);
	}
}

class CPropGroupEntry
{
public:
	CPropGroupEntry() {}
	CPropGroupEntry(const char* name, float fRadius) : m_name(name), m_fRadius(fRadius) {}

	std::string m_name;
	float m_fRadius;
};

class CPropGroup
{
public:
	CPropGroup() {}
	CPropGroup(const char* name) : m_name(name) {}

	std::string m_name;
	std::vector<CPropGroupEntry> m_entries;
};

static std::vector<CPropGroup> g_propGroups;
static bool                    g_propGroupsLoaded = false;

// NOTE -- this function must be kept in sync between 3 files:
// %RS_CODEBRANCH%\game\debug\AssetAnalysis\GeometryCollector.cpp
// %RS_CODEBRANCH%\tools\GeometryCollectorTool\src\GeometryCollectorTool.cpp
// %RS_CODEBRANCH%\tools\HeightMapImageProcessor\src\HeightMapImageProcessor.cpp
static void LoadPropGroups(bool bSeparateGeomGroups = true, bool bVerbose = true)
{
	if (!g_propGroupsLoaded)
	{
		FILE* pGroupList = fopen(varString("%s/non_final/propgroups/list.txt", RS_ASSETS).c_str(), "r");

		if (pGroupList)
		{
			char line[128] = "";

			while (rage_fgetline(line, sizeof(line), pGroupList))
			{
				const varString propGroupPath("%s/non_final/propgroups/%s.txt", RS_ASSETS, line);
				FILE* pGroup = fopen(propGroupPath.c_str(), "r");

				if (pGroup)
				{
					char* pExt = strrchr(line, '.');

					if (pExt)
					{
						*pExt = '\0';
					}

					g_propGroups.push_back(CPropGroup(line));

					float fPropRadiusMax = 0.0f;

					while (rage_fgetline(line, sizeof(line), pGroup))
					{
						const char* pPropName = strtok(line, ",\t ");

						if (pPropName)
						{
							const char* pPropRadiusStr = strtok(NULL, ",\t ");
							const float fPropRadius = pPropRadiusStr ? (float)atof(pPropRadiusStr) : 0.0f;

							g_propGroups.back().m_entries.push_back(CPropGroupEntry(pPropName, fPropRadius));
							fPropRadiusMax = Max<float>(fPropRadius, fPropRadiusMax);
						}
					}

					if (bVerbose)
					{
						fprintf(stdout, "loaded prop group \"%s\"\n", g_propGroups.back().m_name.c_str());

						for (int i = 0; i < (int)g_propGroups.back().m_entries.size(); i++)
						{
							fprintf(stdout, "  %s (radius=%f)\n", g_propGroups.back().m_entries[i].m_name.c_str(), g_propGroups.back().m_entries[i].m_fRadius);
						}
					}

					if (bSeparateGeomGroups && fPropRadiusMax > 0.0f)
					{
						rewind(pGroup);

						sprintf(line, "%s_geom", g_propGroups.back().m_name.c_str());
						g_propGroups.push_back(CPropGroup(line));

						while (rage_fgetline(line, sizeof(line), pGroup))
						{
							const char* pPropName = strtok(line, ",\t ");

							if (pPropName)
							{
								g_propGroups.back().m_entries.push_back(CPropGroupEntry(pPropName, 0.0f));
							}
						}

						if (bVerbose)
						{
							fprintf(stdout, "loaded prop group \"%s\"\n", g_propGroups.back().m_name.c_str());
							fprintf(stdout, "  all radii set to zero\n");
						}
					}

					fclose(pGroup);
				}
				else
				{
					fprintf(stderr, "failed to open \"%s\"\n", propGroupPath.c_str());
				}
			}

			fclose(pGroupList);
		}

		g_propGroupsLoaded = true;
	}
}

int main(int argc, const char* argv[])
{
	PrintDirectoryPaths();
	FreeImage_Initialise(); // TODO -- remove this when we move all this to the new image library

	if (1)
	{
		const clock_t startTime = clock();
		const int resolutionDefault = 1;
		const int resolution = (argc > 1 ? atoi(argv[1]) : resolutionDefault);
		const int glow = 12*resolution;

		char path[1024] = "";
		sprintf(path, RS_ASSETS "/non_final/heightmap/tiles_%d", resolution);

		// ==============================================================================================
		if (argc > 2 && strcmp(argv[2], "-wateronly") == 0)
		{
			TileMap tm;

			if (tm.Load(path))
			{
				AssembleImageFromTiles<u8>   (tm, resolution, path, "_wld");
				AssembleImageFromTiles<u8>   (tm, resolution, path, "_water", false, true);
				AssembleImageFromTiles<u8>   (tm, resolution, path, "_watercoll");
				AssembleImageFromTiles<u8>   (tm, resolution, path, "_wateroccl");
				AssembleImageFromTiles<u8>   (tm, resolution, path, "_waterdraw");
				AssembleImageFromTiles<u8>   (tm, resolution, path, "_waterocean", false, true);
				AssembleImageFromTiles<u8>   (tm, resolution, path, "_waternoreflect");
				AssembleImageFromTiles<float>(tm, resolution, path, "_waterheight");
				AssembleImageFromTiles<u8>   (tm, resolution, path, "_waterportal");
				AssembleImageFromTiles<u16>  (tm, resolution, path, "_max");
				AssembleImageFromTiles<u16>  (tm, resolution, path, "_maxneg");
			}
		}
		else if (1) // standard images
		{
			if (1)
			{
				TileMap tm;

				if (tm.Load(path))
				{
					__mkdir(varString("%s/materialmasks", path).c_str(), false);
					__mkdir(varString("%s/propgroupmasks", path).c_str(), false);

					AssembleImageFromTiles<u8>     (tm, resolution, path, "_wld");
					AssembleImageFromTiles<u8>     (tm, resolution, path, "_water", false, true);
					AssembleImageFromTiles<u8>     (tm, resolution, path, "_stairs");
					AssembleImageFromTiles<u8>     (tm, resolution, path, "_stairslope");
					AssembleImageFromTiles<u8>     (tm, resolution, path, "_script");
					AssembleImageFromTiles<u8>     (tm, resolution, path, "_cables");
					AssembleImageFromTiles<u8>     (tm, resolution, path, "_coronas");
					AssembleImageFromTiles<u8>     (tm, resolution, path, "_animbldg");
				//	AssembleImageFromTiles<u8>     (tm, resolution, path, "_rds");
					AssembleImageFromTiles<u8>     (tm, resolution, path, "_mov", false, true);
					AssembleImageFromTiles<u8>     (tm, resolution, path, "_wpn", false, true);
					AssembleImageFromTiles<u8>     (tm, resolution, path, "_veh", false, true);
					AssembleImageFromTiles<u8>     (tm, resolution, path, "_movnoveh_m");
					AssembleImageFromTiles<u8>     (tm, resolution, path, "_movnoveh_p");
					AssembleImageFromTiles<u8>     (tm, resolution, path, "_interiors");
					AssembleImageFromTiles<u8>     (tm, resolution, path, "_watercoll");
					AssembleImageFromTiles<u8>     (tm, resolution, path, "_wateroccl");
					AssembleImageFromTiles<u8>     (tm, resolution, path, "_waterdraw");
					AssembleImageFromTiles<u8>     (tm, resolution, path, "_waterocean", false, true);
					AssembleImageFromTiles<u8>     (tm, resolution, path, "_waternoreflect");
					AssembleImageFromTiles<float>  (tm, resolution, path, "_waterheight");
					AssembleImageFromTiles<u8>     (tm, resolution, path, "_waterportal");
					AssembleImageFromTiles<u8>     (tm, resolution, path, "_mirrordraw");
					AssembleImageFromTiles<u8>     (tm, resolution, path, "_mirrorportal");
					AssembleImageFromTiles<u8>     (tm, resolution, path, "_type_box");
					AssembleImageFromTiles<u8>     (tm, resolution, path, "_type_cyl");
					AssembleImageFromTiles<u8>     (tm, resolution, path, "_type_cap");
					AssembleImageFromTiles<u8>     (tm, resolution, path, "_type_sph");
					AssembleImageFromTiles<u8>     (tm, resolution, path, "_type_nonbvhprim");
					AssembleImageFromTiles<u16>    (tm, resolution, path, "_min");
					AssembleImageFromTiles<u16>    (tm, resolution, path, "_max");
					AssembleImageFromTiles<u16>    (tm, resolution, path, "_maxneg");
					AssembleImageFromTiles<u8>     (tm, resolution, path, "_den_mov_poly", false, true);
					AssembleImageFromTiles<u8>     (tm, resolution, path, "_den_mov_prim", false, true);
					AssembleImageFromTiles<u8>     (tm, resolution, path, "_den_wpn_poly", false, true);
					AssembleImageFromTiles<u8>     (tm, resolution, path, "_den_wpn_prim", false, true);
					AssembleImageFromTiles<u8>     (tm, resolution, path, "_den_ped", false, true);
					AssembleImageFromTiles<u8>     (tm, resolution, path, "_den_light", false, true);
					AssembleImageFromTiles<u8>     (tm, resolution, path, "_den", false, true);
					AssembleImageFromTiles<Pixel32>(tm, resolution, path, "_regionID", false, true);
					AssembleImageFromTiles<Pixel32>(tm, resolution, path, "_boundID", false, true);

					AssembleMaterialMaskImagesFromTiles<u64>(tm, resolution, path, "_materials", "materialmasks", g_materialMaskList);

					for (int i = 0; i < (tm.m_materialCount + 63)/64; i++)
					{
						AssembleMaterialProcImagesFromTiles(tm, resolution, path, "_materials", i);
					}

					LoadPropGroups();

					if (g_propGroups.size() > 0)
					{
						char** propGroupNames = new char*[g_propGroups.size()];

						for (int i = 0; i < (int)g_propGroups.size(); i++)
						{
							propGroupNames[i] = new char[g_propGroups[i].m_name.length() + 1];
							strcpy(propGroupNames[i], g_propGroups[i].m_name.c_str());
						}

						AssembleMaterialMaskImagesFromTiles<u16>(tm, resolution, path, "_propgroupmask", "propgroupmasks", (const char**)propGroupNames);

						for (int i = 0; i < (int)g_propGroups.size(); i++)
						{
							delete[] propGroupNames[i];
						}

						delete[] propGroupNames;
					}

					// zip it
					{
						char _7zaPath[1024] = "";
						strcpy(_7zaPath, argv[0]); // exe path
						for (char* s = _7zaPath; *s; s++) { if (*s == '\\') { *s = '/'; } }
						strcpy(strrchr(_7zaPath, '/'), "/7za920/7za.exe");

						const char* aux = "start \"unzip\" $WAIT "; // run unzip in a separate console window (change this string to "" to run in the same window)
						char cmd[1024] = "";

						const char* dirs[] =
						{
							"materialmasks",
							"propgroupmasks",
						};

						for (int i = 0; i < NELEM(dirs); i++)
						{
							sprintf(cmd, "%s%s a %s/%s.zip %s/%s/", aux, _7zaPath, path, dirs[i], path, dirs[i]);
							fprintf(stdout, "> %s .. ", cmd);
							__system(cmd);
							fprintf(stdout, "ok.\n");

							sprintf(cmd, "rmdir $Q $S %s/%s", path, dirs[i]);
							fprintf(stdout, "> %s .. ", cmd);
							__system(cmd);
							fprintf(stdout, "ok.\n");
						}
					}
				}
			}

			if (1)
			{
				WorldMask wm(path);

				if (wm.m_worldMask)
				{
					CompositePolyDensityImage    (wm, resolution, "mover polygon bounds",    "_den_mov_poly", "_mover_density",             path, false);
					CompositePolyDensityImage    (wm, resolution, "mover primitive bounds",  "_den_mov_prim", "_mover_density_primitives",  path, false);
					CompositePolyDensityImage    (wm, resolution, "weapon polygon bounds",   "_den_wpn_poly", "_weapon_density",            path, false);
					CompositePolyDensityImage    (wm, resolution, "weapon primitive bounds", "_den_wpn_prim", "_weapon_density_primitives", path, false);
					CompositeLightDensityImage   (wm, resolution, "light density",           "_den_light",    "_light_density",             path, false, 16.0f);
					CompositePedDensityImage     (wm, resolution, path, false);
					CompositeCollisionHolesImage (wm, resolution, path, glow, false);
					CompositeStairsGlowImage     (wm, resolution, path, glow, false);
					CompositeScriptGlowImage     (wm, resolution, path, glow, false);
					CompositeCablesGlowImage     (wm, resolution, path, glow, false);
					CompositeCoronasGlowImage    (wm, resolution, path, glow, false);
					CompositeAnimBldgGlowImage   (wm, resolution, path, glow, false);
					CompositeVehicleHoleGlowImage(wm, resolution, path, glow, false);
					CompositeMoverNoVehGlowImage (wm, resolution, path, glow, false);
				}
			}

			if (1)
			{
				CompositeMinMaxHeightDiffImage(path, 0.4f, false);
				CompositeUnderwaterHeightImage(path);
				CompositeBoundIDEdgesImage    (path, "_regionID", 2);
				CompositeBoundIDEdgesImage    (path, "_boundID", 2);
			}
		}

		// ==============================================================================================
		if (0 && argc == 1) // experimental density maps
		{
			if (1)
			{
				TileMap tm;

				if (tm.Load(path))
				{
					AssembleImageFromTiles<float>(tm, resolution, path, "_denA");
					AssembleImageFromTiles<float>(tm, resolution, path, "_denB");
					AssembleImageFromTiles<float>(tm, resolution, path, "_denC");
					AssembleImageFromTiles<float>(tm, resolution, path, "_denD");
					AssembleImageFromTiles<float>(tm, resolution, path, "_denE");
					AssembleImageFromTiles<float>(tm, resolution, path, "_denF");
					AssembleImageFromTiles<float>(tm, resolution, path, "_denG");
					AssembleImageFromTiles<float>(tm, resolution, path, "_denH");
					AssembleImageFromTiles<float>(tm, resolution, path, "_denI");
				}
			}

			if (1)
			{
				/*
				const float densityA = 1.0f/Max<float>(0.001f, area);
				const float densityB = area;
				const float densityC = Abs<float>(normal_z);
				const float densityD = 1.0f - Abs<float>(normal_z);
				const float densityE = clippedArea2D;
				const float densityF = clippedArea2D*densityA;
				const float densityG = 1.0f;
				const float densityH = bIsEdgeCell ? 1.0f : 0.0f;
				const float densityI = bIsVertCell ? 1.0f : 0.0f;
				*/
			//	CompositeGenericNoOutliersImage2("_denA", "_densityA",    path, 0.1f, 0.1f, 2048, 1.0f, 1.0f, 1.0f, true, false);
			//	CompositeGenericNoOutliersImage2("_denB", "_densityB",    path, 0.1f, 0.1f, 2048, 1.0f, 1.0f, 1.0f, false, false);
			//	CompositeGenericNoOutliersImage2("_denC", "_densityC",    path, 0.1f, 0.1f, 2048, 1.0f, 1.0f, 1.0f, false, false);
			//	CompositeGenericNoOutliersImage2("_denD", "_densityD",    path, 0.1f, 0.1f, 2048, 1.0f, 1.0f, 1.0f, true, false);
			//	CompositeGenericNoOutliersImage2("_denE", "_densityE",    path, 0.1f, 0.1f, 2048, 1.0f, 1.0f, 1.0f, true, false);
			//	CompositeGenericNoOutliersImage2("_denE", "_densityE_08", path, 0.1f, 0.8f, 2048, 1.0f, 1.0f, 1.0f, true, false);
			//	CompositeGenericNoOutliersImage2("_denF", "_densityF",    path, 0.1f, 0.1f, 2048, 1.0f, 1.0f, 1.0f, true, false);
			//	CompositeGenericNoOutliersImage2("_denG", "_densityG",    path, 0.0f, 0.0f, 2048, 1.0f, 100.0f, 1.0f, true, false);
			//	CompositeGenericNoOutliersImage2("_denH", "_densityH",    path, 0.0f, 0.0f, 2048, 1.0f, 100.0f, 1.0f, true, false);
				CompositeGenericNoOutliersImage2("_denI", "_densityI",    path, 0.0f, 0.0f, 2048, 1.0f, 100.0f, 1.0f, true, false);
			}
		}

		const float secs = (float)(clock() - startTime)/(float)CLOCKS_PER_SEC;
		const float mins = secs/60.0f;
		const float hrs  = mins/60.0f;

		if (!IsDebuggerPresent())
		{
			fprintf(stdout, "%s ", argv[0]);
		}

		if      (secs <= 100.0f) { fprintf(stdout, "done. (%.2f secs)\n", secs); }
		else if (mins <= 100.0f) { fprintf(stdout, "done. (%.2f mins)\n", mins); }
		else                     { fprintf(stdout, "done. (%.2f hours)\n", hrs); }

		if (IsDebuggerPresent())
		{
			system("pause");
		}

		exit(0);
	}

	return 0;
}
