@echo off
pushd %~dp0

set started=%time% 
@echo STARTED : %started%

echo Building DongleTool...
pushd DongleTool
call rebuild_vcproj.bat
popd

echo Building Sln...
pushd VS_Project
call rebuild_sln.bat
popd

@echo STARTED  : %started% 
@echo FINISHED : %time% 