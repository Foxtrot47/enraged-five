top: bottom

include ..\..\..\..\rage\build\Makefile.template

.PHONY: FORCE
TARGET = dongletool
ELF = $(INTDIR)/$(TARGET)_$(INTDIR).ppu.elf
SELF = $(INTDIR)/$(TARGET)_$(INTDIR).ppu.self

bottom: $(SELF)

$(SELF): $(ELF)
	make_fself $(ELF) $(SELF)

LIBS += ..\DongleTool\$(INTDIR)\dongletool.lib

..\DongleTool\$(INTDIR)\dongletool.lib: FORCE
	$(MAKE) -C ..\DongleTool -f dongletool.mk

LIBS += ..\..\$(INTDIR)\rage/base/src/vcproj/RageCore/RageCore.lib

..\..\$(INTDIR)\rage/base/src/vcproj/RageCore/RageCore.lib: FORCE
	$(MAKE) -C ..\.. -f rage/base/src/vcproj/RageCore/RageCore.mk

LIBS += ..\..\rage\stlport\STLport-5.0RC5\src/$(INTDIR)/stlport.lib

..\..\rage\stlport\STLport-5.0RC5\src/$(INTDIR)/stlport.lib: FORCE
	$(MAKE) -C ..\..\rage\stlport\STLport-5.0RC5\src -f stlport.mk

LIBS += ..\..\$(INTDIR)\rage/base/src/vcproj/RageGraphics/RageGraphics.lib

..\..\$(INTDIR)\rage/base/src/vcproj/RageGraphics/RageGraphics.lib: FORCE
	$(MAKE) -C ..\.. -f rage/base/src/vcproj/RageGraphics/RageGraphics.mk

LIBS += ..\..\$(INTDIR)\rage/base/src/vcproj/RageNet/RageNet.lib

..\..\$(INTDIR)\rage/base/src/vcproj/RageNet/RageNet.lib: FORCE
	$(MAKE) -C ..\.. -f rage/base/src/vcproj/RageNet/RageNet.mk

INCLUDES = -I ..\..\..\..\rage\stlport\STLport-5.0RC5\stlport -I ..\..\rage\base\src -I ..\..\rage\suite\src -I ..\..\rage\script\src

$(INTDIR)\dongletool.obj: dongletool.cpp
	echo Make C++ compiling $(abspath $<)
	$(CXX) $(CXXFLAGS) -c $(abspath $<) -o $@

$(ELF): $(LIBS) $(INTDIR)\dongletool.obj
	ppu-lv2-gcc -o $(ELF) $(INTDIR)\dongletool.obj -Wl,--start-group $(LIBS) -Wl,--end-group $(LLIBS)
