DONGLETOOL

-----------------------------
Description
-----------------------------

  DongleTool was created to add a bit of more security to the game. Permits to add a verification 
to execute based on a password and the machine MacAddress.


-----------------------------
Verification In Game
-----------------------------

  Activate the verification in app.cpp by setting USE_DONGLE (  #define USE_DONGLE	(ENABLE_DONGLE & 1) )


-----------------------------
How to Use Dongletool
-----------------------------

1. Get depot //depot/jimmy/src/dev/tools/DongleTool
2. exes are loacated here with the following names:
   - dongletool_xenon_beta.xex
   - dongletool_psn_beta.self
3. On XENON you only need to execute bat file MakeDongles.bat to install the dongle. More elaborated options are available.
4. On both platforms the respective exes can be loaded in a console to create the Dongles.
   
___ XENON ___

Command Arguments:
... -rag                         --> Activate RAG.
... -rootdir       <Directory>   --> Specifies the root directory.
... -encodestring  <Password>    --> Specifies the encode string to use.
... -macaddrstring <MacAddress>  --> Specifies the maccAddress to use.
... -writeCode or -verifyCode    --> Write/Verify Dongle and exit.

1. Dongle is installed directly in the xbox. Default xbox set in Xbox Neighborhood will be used for this.


___ PSN ___

Command Arguments:
... -rag                         --> Activate RAG.
... -rootdir       <Directory>   --> Specifies the root directory.
... -encodestring  <Password>    --> Specifies the encode string to use.
... -macaddrstring <MacAddress>  --> Specifies the maccAddress to use.
... -writeCode or -verifyCode    --> Write/Verify Dongle and exit.
... -addressesFile <Filename>    --> Specify which file should be used to read macAddress.

NOTE: THERE MUST BE NO SPACES AFTER ANY MAC ADDRESSES OR SUBSEQUENT ADDRESSES ARE IGNORED!! (ps3)

1. Dongle Creation:
 - Mac Addresses are read from a file addresses.dat located in the assets directory. An encripted file will be created with all 
 macAddresses that are permitted to run the game "root.sys". This file has to be added to platform:/Data/ directory in the game.
 - Because of this any console can be used to create the dongles.

 2. Check Rag for more options.
 

