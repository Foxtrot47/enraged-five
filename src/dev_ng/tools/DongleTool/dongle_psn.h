//
// dongletool_psn.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef PSNDONGLE_H
#define PSNDONGLE_H

#if __PPU

class CPSNDongle
{
public:
	static bool ValidateCodeFile(const char* encodeString, char* debugString);

private:
	//Some very cheesy encoding to avoid that the string that is written
	//to the memory card directly shows up in the executeable.
	static void Encode(char *buffer, int sizeInBytes);
};

#endif // __PPU

#endif // PSNDONGLE_H
