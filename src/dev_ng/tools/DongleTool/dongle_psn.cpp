//
// input/dongle_psn.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

// #if __PPU	// avoid LNK4221
#if !__WIN32PC

#if __PPU
#include <netex/libnetctl.h>
#include <cell/rtc.h>
#include <sys/return_code.h>
#include <stdio.h>

// Rage
#include "data/aes.h"
#include "rline/rl.h"
#include "string/string.h"
#include "system/memops.h"

// GTAIV
#include "dongle_psn.h"
#include "system/FileMgr.h"

bool 
CPSNDongle::ValidateCodeFile(const char* encodeString, char* debugString)
{
	bool ret = false;

	int err;
	CellNetCtlInfo cell_info;
	u8 mac[6];
	char macAddrString [18];

	// Look up current MAC address
	if(0 <= (err = cellNetCtlGetInfo(CELL_NET_CTL_INFO_ETHER_ADDR, &cell_info)))
	{
		CompileTimeAssert(sizeof(mac) == CELL_NET_CTL_ETHER_ADDR_LEN);
		sysMemCpy(mac, cell_info.ether_addr.data, CELL_NET_CTL_ETHER_ADDR_LEN);

		// Get the Hexa String of the Mac Address
		sprintf(macAddrString, "%02X:%02X:%02X:%02X:%02X:%02X", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
		macAddrString[17] = '\0';
	}
	else
	{
		rlNpError("[CPSNDongle::ValidateCodeFile] Error calling cellNetCtlGetInfo()", err);
	}

	// Note: This gets a bit messy since key and mac address are encoded one way (irreversibly) but 
	// date stamp is encoded using AES (reversibly). This is done to maintain backwards compatibility with
	// previous dongles

	/*-------------------------------*/
	int32 bufferMacAddressLen = StringLength(macAddrString)+1;
	int32 bufferKeyStringLen  = StringLength(encodeString)+1;
	int32 bufferDateStringLen = 32;
	int32 bufferLen = bufferMacAddressLen+bufferKeyStringLen+bufferDateStringLen;

	char* origBuffer = Alloca(char, bufferLen);
	char* encodedBuffer = Alloca(char, bufferLen);
	// code to ensure release and beta versions of this work
	memset(origBuffer, 0xcd, bufferLen);

	// Encode current key and mac address for comparison with read one
	// No need to do for date
	char* origBufferMacAddress = Alloca(char, bufferMacAddressLen);
	char* origBufferKeyString  = Alloca(char, bufferKeyStringLen);
	// code to ensure release and beta versions of this work
	memset(origBufferMacAddress, 0xcd, bufferMacAddressLen);
	memset(origBufferKeyString, 0xcd, bufferKeyStringLen);

	// Encode Mac Address
	memcpy(origBufferMacAddress, macAddrString, bufferMacAddressLen);
	Encode(origBufferMacAddress, bufferMacAddressLen);

	// Encode Key String
	memcpy(origBufferKeyString, encodeString,bufferKeyStringLen);
	Encode(origBufferKeyString, bufferKeyStringLen);

	// Mac Address + Key String
	memcpy(origBuffer, origBufferMacAddress, bufferMacAddressLen); 
	memcpy(origBuffer+bufferMacAddressLen, origBufferKeyString, bufferKeyStringLen);

	// Look up current date and time
	CellRtcTick currentTick;
	cellRtcGetCurrentTick(&currentTick); // Get current time (local time)

	FileHandle fid;
	fid = CFileMgr::OpenFile("platform:/DATA/root.sys"); // open for reading
	if (!fid)
	{
		Displayf("\n");
		Displayf("\n");
		Displayf("/*--------------------------*/");
		Displayf("UNAUTHORIZED demo copy");
		Displayf("/*--------------------------*/");
		Displayf("\n");
		strcpy(debugString, " UNAUTHORIZED demo copy - KEY FILE does not exist");
		return ret;
	}

	bool bDateReadOK = false;
	bool bDateOK = false;
	bool bKeyOK = false;
	bool bMacOK = false;

	while(true)
	{
		if(CFileMgr::Read(fid, encodedBuffer, bufferLen) != bufferLen)
			break;
		//if(*pLine == '#' || *pLine == '\0')
		//	break;

		// Check if current date is past expiry
		//int bytesRead = 0;
		//bytesRead = StringLength(pLine);

		/*--------------------------*/
		// Compare with read date
		char* readDateStr = encodedBuffer+bufferMacAddressLen+bufferKeyStringLen;
		AES aes;
		aes.Decrypt(readDateStr, bufferLen-bufferMacAddressLen-bufferKeyStringLen);

		CellRtcTick readTick;
		if(cellRtcParseDateTime(&readTick, readDateStr) == CELL_OK)
		{
			bDateReadOK = true;
			if(readTick.tick > currentTick.tick)
			{
				bDateOK = true;
			}
		}
		else
		{
#if __DEV
			Displayf("[CPSNDongle::ValidateCodeFile] Notice: No expiry found in dongle");
#endif
		}
		/*--------------------------*/
		// Check MAC
		if(memcmp(origBufferMacAddress, encodedBuffer, bufferMacAddressLen) == 0)
		{
			bMacOK = true;
		}
		else
		{
			char zeroMac [bufferMacAddressLen];
			// Check for read MAC address being 0
			sprintf(zeroMac,"00:00:00:00:00:00");
			Encode(zeroMac,bufferMacAddressLen);
			if(memcmp(encodedBuffer, zeroMac, bufferMacAddressLen) == 0)
			{
				// Mac address on memory stick was set to 0 so we can ignore this check
				bMacOK = true;
			}
		}
		/*--------------------------*/
		// Check key
		if(memcmp(origBufferKeyString, encodedBuffer + bufferMacAddressLen, bufferKeyStringLen) == 0)
		{
			bKeyOK = true;
		}

		// Note if no date could be read then we assume an expiry date was not set and continue loading
		if(bKeyOK && bMacOK && (bDateOK || !bDateReadOK))
		{
			ret = true;

			Displayf("\n");
			Displayf("\n");
			Displayf("/*--------------------------*/");
			Displayf("AUTHORIZATION OK");
			Displayf("/*--------------------------*/");
			Displayf("\n");
			break;
		}
		else
		{
			if( !bMacOK )
				strcpy(debugString, " UNAUTHORIZED demo copy - The dongle is for a different machine.!");
			if( !bKeyOK )
				strcpy(debugString, " UNAUTHORIZED demo copy - The dongle key does not match the game.! ");
			if( !bDateOK && bDateReadOK)
				strcpy(debugString, " UNAUTHORIZED demo copy - The dongle has expired.! ");
		}
	}
	CFileMgr::CloseFile(fid);

	if (!ret && !(bKeyOK && bMacOK && (bDateOK || !bDateReadOK)))
	{
		Displayf("\n");
		Displayf("\n");
		Displayf("/*--------------------------*/");
		Displayf("UNAUTHORIZED demo copy");
		Displayf("/*--------------------------*/");
		Displayf("\n");
	}

	return ret;
}

void 
CPSNDongle::Encode(char *buffer, int sizeInBytes)
{
	int						value;
	int						i;

	for(i=0; i<sizeInBytes; i++)
	{
		value= buffer[i];
		value += 61 - (77 * i)%13 + i/5;

		if(i>0)
		{
			value += buffer[i-1];
		}

		if(i<sizeInBytes-1)
		{
			value -= buffer[i+1] / 2;
		}

		value= value % 255;
		buffer[i]= (char)value;
	}
}

#endif // __PPU

#endif // !__WIN32PC

// EOF

