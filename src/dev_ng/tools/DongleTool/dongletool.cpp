// 
// /dongletool.cpp 
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved. 
//
#include <system/main.h>
#include <system/param.h>
#include <system/bootmgr.h>
#include <grcore/setup.h>
#include <grcore/device.h>
#include <grcore/font.h>
#include <input/dongle.h>
#include <input/input.h>
#include <bank/bkmgr.h>
#include <bank/bank.h>
#include <system/timer.h>
#include <file/asset.h>

#if __PPU
#include "dongletool_psn.h"
#elif __XENON
#include "dongletool_xenon.h"
#endif

using namespace rage;

//#if __DEV && __XENON
//#pragma optimize("", off)
//#endif

// --- Defines ------------------------------------------------------------------

#define LOCAL_ASSET_PATH "X://gta5//src//dev//tools//DongleTool//assets//"
#define CONSOLE_ASSET_PATH "X://gta5//src//dev//tools//DongleTool//assets//"

PARAM(encodestring, "dongle string to encode");
PARAM(macaddrstring, " mac address to encode ");
PARAM(writeCode, " Indicate That we want to write the code and exit ");
PARAM(verifyCode, " Indicate That we want to verify the code and exit ");
PARAM(rootdir, " Indicate where root folder is ");

enum eCurrentOperation
{
	OP_IDLE,
	OP_READ,
	OP_READING,
	OP_WRITE,
	OP_WRITING,
	OP_REMOVEDONGLE,
	OP_QUIT,
	OP_EXIT
};

#if __PPU
CPSNDongleTool DONGLETOOL;
#elif __XENON
CXenonDongleTool DONGLETOOL;
#endif


// --- Defines ------------------------------------------------------------------

static eCurrentOperation sCurrentOperation = OP_IDLE;
//static const char* sZeroMacString = "00:00:00:00:00:00";
static fiDevice::SystemTime sExpiryTime;
static bool sbExpires = false;
//static bool sbRestrictMac = false;
static char sMessage[500];
static char sEncodeString[64];
static char sMacAddrString[64];

static const u32 MAX_KEY_LENGTH = 32;
static const u32 MAC_ADDR_LENGTH = 18;

static u32 sExitTime = 0;

static sysTimer sSysTimer;
static const u32 EXIT_TIMER = 2000;

static const char* sRootFolder = NULL;

///////////////////////////////////////////////////////////////////////////////////////////////////

// PURPOSE
//   Message displayed on screen
void DisplayFunc(const char *message)
{
	Displayf(message);
	safecpy(sMessage, message, sizeof(sMessage));
}


#if __BANK && (__SPU || __PPU || __XENON)

// PURPOSE
//   Start operation that encodes the password and the macAddress.
static void sWriteCodeFile()
{
	if(sCurrentOperation == OP_IDLE)
	{
		safecpy(sMessage, "Wrinting code file", sizeof(sMessage));
		sCurrentOperation = OP_WRITE;
	}
}

// PURPOSE
//   Start operation that verifies the password and the macAddress.
static void sReadCodeFile()
{
	if( sCurrentOperation == OP_IDLE )
	{
		safecpy(sMessage, "Reading code file", sizeof(sMessage));
		sCurrentOperation = OP_READ;
	}
}

// PURPOSE
//   Start operation that exits the aplication.
static void sQuit()
{
	safecpy(sMessage, "Exiting Dongle", sizeof(sMessage));
	sExitTime = static_cast<u32>(sSysTimer.GetMsTime()) + EXIT_TIMER;
	sCurrentOperation = OP_QUIT;
}

// PURPOSE
//   Remove Dongle.
void sRemoveDongle()
{
	if( sCurrentOperation == OP_IDLE )
	{
		safecpy(sMessage, "Removing code file", sizeof(sMessage));
		sCurrentOperation = OP_REMOVEDONGLE;
	}
}

// PURPOSE
//   Initialize rag widgets - rag is used to start operations.
static void InitWidgets()
{
	sExpiryTime.wDay = 1;
	sExpiryTime.wDayOfWeek = 1;
	sExpiryTime.wHour = 0;
	sExpiryTime.wMilliseconds = 0;
	sExpiryTime.wMinute = 0;
	sExpiryTime.wMonth = 1;
	sExpiryTime.wSecond = 0;
	sExpiryTime.wYear = 1900;

	rage::bkBank& bank = BANKMGR.CreateBank("DONGLE");

	bank.PushGroup("Expiry Date");
	{
		bank.AddToggle("Dongle expires", &sbExpires);
		bank.AddSlider("Day",  &sExpiryTime.wDay, 1, 31, 1.0f);
		bank.AddSlider("Month (1: Jan, 2: Feb, etc)",&sExpiryTime.wMonth, 1, 12, 1.0f);
		bank.AddSlider("Year", &sExpiryTime.wYear, 1900, 2100, 1.0f);
	}
	bank.PopGroup();

	bank.PushGroup("Operations");
	{
		//bank.AddToggle("Restrict Mac Address", &sbRestrictMac);

		bank.AddButton("Write Code File",  datCallback(CFA(sWriteCodeFile)));
		bank.AddButton("Verify Code File", datCallback(CFA(sReadCodeFile)));
		bank.AddButton("Remove Code File", datCallback(CFA(sRemoveDongle)));

		bank.AddText("MAC address ", &sMacAddrString[0], MAC_ADDR_LENGTH);
		bank.AddText("Encode key", &sEncodeString[0], MAX_KEY_LENGTH);

		bank.AddButton("Exit Dongle Tool", datCallback(CFA(sQuit)));
	}
	bank.PopGroup();
}

// PURPOSE
//   Shutdown rag widgets.
static void ShutdownWidgets()
{
	rage::bkBank* bank = BANKMGR.FindBank("DONGLE");
	if (bank)
	{
		BANKMGR.DestroyBank(*bank);
	}
}

#endif // __BANK && (__PPU || __XENON)


///////////////////////////////////////////////////////////////////////////////////////////////////

// PURPOSE
//   Update 
void Update()
{
#if ENABLE_DONGLE && (__XENON || __PPU)

	// Check Input
	u32 uReleasedButton = ioPad::GetPad(0).GetReleasedButtons();
	// X button
	if (((uReleasedButton & ioPad::RLEFT) == ioPad::RLEFT))
	{
		sWriteCodeFile();
	}
	// A button
	else if (((uReleasedButton & ioPad::RDOWN) == ioPad::RDOWN))
	{
		sReadCodeFile();
	}
	// B button
	else if (((uReleasedButton & ioPad::RRIGHT) == ioPad::RRIGHT))
	{
		sRemoveDongle();
	}
	// Y button
	else if (((uReleasedButton & ioPad::RUP) == ioPad::RUP))
	{
		sQuit();
	}

	switch(sCurrentOperation)
	{
	case OP_READING:
		{
			if (PARAM_verifyCode.Get())
			{
				safecpy(sMessage, "Exiting Dongle", sizeof(sMessage));
				sExitTime = static_cast<u32>(sSysTimer.GetMsTime()) + EXIT_TIMER;
				sCurrentOperation = OP_QUIT;
			}
			else
			{
				sCurrentOperation = OP_IDLE;
			}
		}
		break;

	case OP_READ:
		{
			DONGLETOOL.ReadCodeFile(sEncodeString, sMacAddrString /*sbRestrictMac ? sMacAddrString : sZeroMacString*/, sMessage, sRootFolder);
			sCurrentOperation = OP_READING;
		}
		break;

	case OP_WRITING:
		{
			if (!DONGLETOOL.Pending())
			{
				if (PARAM_writeCode.Get())
				{
					safecpy(sMessage, "Exiting Dongle", sizeof(sMessage));
					sExitTime = static_cast<u32>(sSysTimer.GetMsTime()) + EXIT_TIMER;
					sCurrentOperation = OP_QUIT;
				}
				else
				{
					sCurrentOperation = OP_IDLE;
				}
			}
		}
		break;

	case OP_WRITE:
		{
			DONGLETOOL.WriteCodeFile(sEncodeString, sMacAddrString /*sbRestrictMac ? sMacAddrString : sZeroMacString*/, sExpiryTime, sbExpires, sMessage, sRootFolder);
			sCurrentOperation = OP_WRITING;
		}
		break;

	case OP_REMOVEDONGLE:
		{
			DONGLETOOL.RemoveCodeFile(sMessage);
			sCurrentOperation = OP_IDLE;
		}
		break;

	case OP_QUIT:
		{
			if (sExitTime < static_cast<u32>(sSysTimer.GetMsTime()))
			{
				sCurrentOperation = OP_EXIT;
			}			
		}
		break;

	default:
		break;
	}
#endif // ENABLE_DONGLE

	switch(sCurrentOperation)
	{
	case OP_READ:
	case OP_WRITE:
	case OP_REMOVEDONGLE:
	case OP_QUIT:
	default:
		break;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////

// PURPOSE
//   Renders text on screen
void Render()
{

#if __XENON
	grcFont::GetCurrent().DrawScaled(60.0, 120.0, 0.0, Color32(255, 255, 255, 255), 1.2f, 2.0f, "Dongle Menu:");
	grcFont::GetCurrent().DrawScaled(60.0, 140.0, 0.0, Color32(0,     0, 255, 255), 1.2f, 2.0f, "............ X --> Write Code File");
	grcFont::GetCurrent().DrawScaled(60.0, 160.0, 0.0, Color32(0,   255,   0, 255), 1.2f, 2.0f, "............ A --> Verify Code File");
	grcFont::GetCurrent().DrawScaled(60.0, 180.0, 0.0, Color32(255,   0,   0, 255), 1.2f, 2.0f, "............ B --> Remove Code File");
	grcFont::GetCurrent().DrawScaled(60.0, 200.0, 0.0, Color32(255, 255,   0, 255), 1.2f, 2.0f, "............ Y --> Exit Dongle Tool");
	grcFont::GetCurrent().DrawScaled(60.0, 250.0, 0.0, Color32(255, 255, 255, 255), 1.0f, 1.0f, "Arguments: [ -rag || -rootdir || -encodestring <password> || -macaddrstring <maccAddress> || -writeCode or -verifyCode ]");
#elif __PPU
	grcFont::GetCurrent().DrawScaled(60.0, 120.0, 0.0, Color32(255, 255, 255, 255), 1.2f, 2.0f, "Dongle Menu:");
	grcFont::GetCurrent().DrawScaled(60.0, 140.0, 0.0, Color32(0,     0, 255, 255), 1.2f, 2.0f, "............ X --> Write Code File");
	grcFont::GetCurrent().DrawScaled(60.0, 160.0, 0.0, Color32(0,   255,   0, 255), 1.2f, 2.0f, "............ A --> Verify Code File");
	grcFont::GetCurrent().DrawScaled(60.0, 180.0, 0.0, Color32(255,   0,   0, 255), 1.2f, 2.0f, "............ B --> Remove Code File");
	grcFont::GetCurrent().DrawScaled(60.0, 200.0, 0.0, Color32(255, 255,   0, 255), 1.2f, 2.0f, "............ Y --> Exit Dongle Tool");
	grcFont::GetCurrent().DrawScaled(60.0, 250.0, 0.0, Color32(255, 255, 255, 255), 1.0f, 1.0f, "Arguments: [ -rag || -rootdir || -encodestring <password> || -macaddrstring <maccAddress> || -writeCode or -verifyCode || addressesFile <filename> ]");
#endif

	grcFont::GetCurrent().Draw(60, 350, "----------------------------------------------------");
	grcFont::GetCurrent().DrawScaled(60.0, 375.0, 0.0, Color32(255, 255, 255,255), 1.0f, 1.0f, "   Message Buffer   ");
	grcFont::GetCurrent().Draw(60, 400, "----------------------------------------------------");
	grcFont::GetCurrent().DrawScaled(60.0, 500.0, 0.0, Color32(255, 255, 255,255), 1.0f, 1.0f, sMessage);
	grcFont::GetCurrent().Draw(60, 600, "----------------------------------------------------");
}

///////////////////////////////////////////////////////////////////////////////////////////////////

int Main()
{
	if (!PARAM_rootdir.Get(sRootFolder))
	{
		sRootFolder = sysBootManager::IsBootedFromDisc() ? CONSOLE_ASSET_PATH : LOCAL_ASSET_PATH;
	}
	ASSET.SetPath(sRootFolder);

	sMessage[0] = '\0';
	sEncodeString[0] = '\0';
	sMacAddrString[0] = '\0';

	sCurrentOperation = OP_IDLE;

	const char* paramMacAddr = NULL;
	const char* paramKeyStr = NULL;

	if(PARAM_encodestring.Get(paramKeyStr))
	{
		safecpy(&sEncodeString[0], paramKeyStr, sizeof(sEncodeString));
	}

	if(PARAM_macaddrstring.Get(paramMacAddr))
	{
		safecpy(&sMacAddrString[0], paramMacAddr, sizeof(sMacAddrString));
	}

	if (PARAM_writeCode.Get())
	{
		if(!PARAM_encodestring.Get() || !PARAM_macaddrstring.Get() )
		{
#if !__BANK
			// Only quit if there is no bank, since we can use widgets instead of command line
			Quitf("usage: -encodestring <encode-string> -macaddrstring <macaddress-string> -writeCode");
#endif
		}
		else
		{
			sCurrentOperation = OP_WRITE;
		}
	}
	else if (PARAM_verifyCode.Get())
	{
		if( !PARAM_encodestring.Get() || !PARAM_macaddrstring.Get() )
		{
#if !__BANK
			// Only quit if there is no bank, since we can use widgets instead of command line
			Quitf("usage: -encodestring <encode-string> -macaddrstring <macaddress-string> -verifyCode");
#endif
		}
		else
		{
			sCurrentOperation = OP_READ;
		}
	}

	//GRCDEVICE.SetDefaultEffectName("X://gta5//src//dev//tools//DongleTool//assets//shaders//im");

	const bool inWindow = true;
	grcSetup setup;
	setup.Init(sRootFolder, "dongle");
	setup.BeginGfx(inWindow);
	setup.CreateDefaultFactories();
	setup.SetClearColor(Color32(0,0,0,255));
	rage::INPUT.Begin(inWindow);

#if __DEBUGBUTTONS
	ioPad::GetPad(0).SetDebugButton(ioPad::SELECT);
#endif

#if __BANK
	InitWidgets();
#endif // __BANK

	sSysTimer.Reset();

	do
	{
		setup.BeginUpdate();
		INPUT.Update();
		Update();
		setup.EndUpdate();

		/*
		setup.BeginDraw(true);
		Render();
		setup.EndDraw();
		*/

		sysIpcSleep(15);
	} while( !setup.WantExit() && sCurrentOperation != OP_EXIT );

#if __BANK
	ShutdownWidgets();
#endif // __BANK

	//setup.EndGfx();
	setup.DestroyFactories();
	setup.Shutdown();

	//exit(0);
	return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////


// EOF

