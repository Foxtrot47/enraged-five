//
// dongletool_xenon.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#if __XENON

// Rage
#include <input/dongle.h>

// Dongle
#include "dongletool_xenon.h"

//#if __DEV
//#pragma optimize("", off)
//#endif

static ioDongle DONGLE;
static const char* sMCFileName="root.sys";

bool 
CXenonDongleTool::ReadCodeFile(const char* encodeString, const char* UNUSED_PARAM(macAddrString), char* errorString, const char* UNUSED_PARAM(rootFolder))
{
	if (!DONGLE.ReadCodeFile(encodeString, sMCFileName, errorString))
	{
		Displayf(errorString);
		return false;
	}
	else
	{
		safecpy(errorString, "Autorization OK", 64);
	}

	return true;
}

void 
CXenonDongleTool::WriteCodeFile(const char* encodeString, const char* macaddrString, const fiDevice::SystemTime expiryDate, bool bExpires, char* errorString, const char* UNUSED_PARAM(rootFolder))
{
	DONGLE.WriteCodeFile(encodeString, macaddrString, expiryDate, bExpires, sMCFileName);

	char sMessage[64];
	sprintf(sMessage, "Write Code File - %s", sMCFileName);
	safecpy(errorString, sMessage, 128);
}

void 
CXenonDongleTool::RemoveCodeFile(char* errorString)
{
	// DONGLE.RemoveDongle()
	safecpy(errorString, "Remove Code File - FAILED - Not Implemented yet. Delete file root.sys", 128);
}

bool  
CXenonDongleTool::Pending()
{
	return !DONGLE.CheckWriteCodeFile();
}

#endif // __XENON

// EOF

