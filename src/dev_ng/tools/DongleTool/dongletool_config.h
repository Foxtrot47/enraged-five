//
// name:		dongletool_config.h
// description: Place your preprocessor defines in here. For the Jimmy project. This file is used in both
//				core code and jimmy project
//
#define ONLINE_PLAY
#define NO_SPEEDTREE
#define BIGWORLD_HACK		(1)

// what project am I working on
#define __GTANY		0
#define __JIMMY		1

#if __GTANY
#define GTANY_ONLY(x)	x
#else
#define GTANY_ONLY(x)
#endif

#if __JIMMY
#define JIMMY_ONLY(x)	x
#else
#define JIMMY_ONLY(x)
#endif

// if master build defines are not defined then define them. default to the european build
#ifndef __MASTER
#define __MASTER			0
#define __GERMAN_BUILD		0
#define __AUSSIE_BUILD		0
#define __EUROPEAN_BUILD	1
#define __AMERICAN_BUILD	0
#endif

#ifndef __AUSSIE_BUILD
#define __AUSSIE_BUILD		0
#endif
