@ECHO OFF

SETLOCAL ENABLEEXTENSIONS ENABLEDELAYEDEXPANSION

SET SCRIPT_DIR=X:\gta5\src\dev\tools\DongleTool\

ECHO %SCRIPT_DIR%

SET EXENAME=DongleTool_xenon_beta.xex
SET DONGLEDIR=xe:\DongleTool\
SET XEBIN=%XEDK%\bin\win32\
SET ENCODESTRING=
SET MACADDRSTRING=
SET MCFILENAME=root.sys

:getEncodeString
SET /P ENCODESTRING=Please enter the encode string:
IF /I "!ENCODESTRING!"=="" GOTO getEncodeString

:getMacAddressString
SET /P MACADDRSTRING=Please enter the mac address string( Example - 00:18:8B:07:37:06 ):
IF /I "!MACADDRSTRING!"=="" GOTO getMacAddressString

REM :getMCFilename
REM SET /P MCFILENAME=Please enter the memory card filename:
REM IF /I "!MCFILENAME!"=="" GOTO getMCFilename

ECHO Copying dongle tool to xbox 360...

"%XEBIN%xbcp.exe" /R /F /T /Y "%SCRIPT_DIR%%EXENAME%" %DONGLEDIR%

ECHO Launching dongle tool...

REM "%XEBIN%xbreboot.exe" %DONGLEDIR%%EXENAME% -encodestring %ENCODESTRING% -macaddrstring %MACADDRSTRING% -mcfilename %MCFILENAME%
"%XEBIN%xbreboot.exe" %DONGLEDIR%%EXENAME% -encodestring %ENCODESTRING% -macaddrstring %MACADDRSTRING% -writeCode

ENDLOCAL

pause