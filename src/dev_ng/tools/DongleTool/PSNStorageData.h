//
// name:        PSNStorageData.h
// description: Implements file exchanges in PS3 between the internal hard disk drive and a recording medium such as Memory Stick.
// notes:       - It only accepts files in the PLAYSTATION®3 format or PSP™ (PlayStation®Portable) format.
//              - Regardless of the original filename, the filename of the exported file will be EXPORT.BIN. 
//                Even when EXPORT.BIN already exists, the file will be overwritten without a separate name being assigned 
//                for the save or any confirmation being provided. Thus, when exporting two (or more) files consecutively, 
//                the file(s) exported earlier will be cleared.
//              - Regardless of the original filename, the filename of the imported file will be IMPORT.BIN. Even when 
//                IMPORT.BIN already exists, the file will be overwritten without a separate name being assigned for the save 
//                or any confirmation being provided. Thus, when Importing two (or more) files consecutively, the file(s) imported 
//                earlier will be cleared. Before this happens, the application must use IMPORT.BIN for processing, and save it 
//                under a different name if necessary. 
// author:		Miguel Freitas

#ifndef PSNSTORAGEDATA_H
#define PSNSTORAGEDATA_H

#if __PPU

// Forward Definitions

namespace rage 
{

//PURPOSE
//  Implements file exchanges in PS3 between the internal hard disk drive and a recording medium such as Memory Stick.
//
// NOTES
//      - It only accepts files in the PLAYSTATION®3 format or PSP™ (PlayStation®Portable) format.
//      - Regardless of the original filename, the filename of the exported file will be EXPORT.BIN. 
//        Even when EXPORT.BIN already exists, the file will be overwritten without a separate name being assigned 
//        for the save or any confirmation being provided. Thus, when exporting two (or more) files consecutively, 
//        the file(s) exported earlier will be cleared.
//      - Regardless of the original filename, the filename of the imported file will be IMPORT.BIN. Even when 
//        IMPORT.BIN already exists, the file will be overwritten without a separate name being assigned for the save 
//        or any confirmation being provided. Thus, when Importing two (or more) files consecutively, the file(s) imported 
//        earlier will be cleared. Before this happens, the application must use IMPORT.BIN for processing, and save it 
//        under a different name if necessary.
class CPSNStorageData
{
public:

#define MEMORY_CONTAINER_SIZE_SAVEDATA_COPY	(4 * 1024 * 1024)

	enum eStorageDataStatus
	{
		STATUS_NONE = 0,
		STATUS_RUNNING
	};

	enum eStorageDataTransferType
	{
		FILE_IMPORT = 0,
		FILE_EXPORT,
		FILE_MOVE
	};

private:
	static int  m_iTransferStatus;
	static bool m_bSuccess;
	static u32  m_MemoryContainer;

public:

	CPSNStorageData() { };
	~CPSNStorageData() { };

	//PURPOSE
	//   - Imports a file from the recording medium to the game data folder of the internal disk drive.
	//   - Exports a file from the game folder of the internal disk drive  to the game data folder of the recording medium.
	//   - Move a file from the recording medium to the game folder of the internal disk drive.
	//PARAMS
	//   transferType    - Type of file transfer - Import || Export || Move
	//   sourcePath      - Path of the source file to be imported (includes filename).
	//   destinationPath - Path to the destination game data folder (excludes filename).
	//   filesize        - FileSize being imported.
	//NOTES
	//   This method initiates the transfer process and returns immediately.
	//   When the transfer completes transfer status will be put in STATUS_NONE and 
	//   you can check if the transfer succeeded by calling GetErrorReport().
	void TransferFile(const eStorageDataTransferType transferType,
					  const char* sourcePath, 
					  const char* destinationPath, 
					  const int fileSize);

	//PURPOSE
	//   Check event and call callback. Must be called every frame.
	//NOTES:
	//   MUST BE CALLED EVERY FRAME By the main thread.
	void Update();

	//PURPOSE
	//   Return the operation status.
	bool GetIsPending() { return (m_iTransferStatus!=STATUS_NONE); };

	//PURPOSE
	//   Return the error report.
	bool GetSucess() { return m_bSuccess; };

private:

	//PURPOSE
	//     Completion callback function. When the transfer finishes this function is called.
	static void CB_TransferFile(int result, 
								void* userdata);

};

}	// namespace rage

#endif // __PPU

#endif // PSNSTORAGEDATA_H

// eof

