@echo off

setlocal
pushd "%~dp0"

CALL setenv.bat

pushd ..\..\rage
set RAGE_DIR=%CD%
popd

REM Detect if current SDK is either unset or older than 360 and switch.
IF NOT EXIST %SCE_PS3_ROOT%\info\old\400.001\Bugfix_SDK_e.txt set SCE_PS3_ROOT=X:/ps3sdk/dev/usr/local/430_001/cell

ECHO LOAD_SLN ENVIRONMENT
ECHO RAGE_DIR:               %RAGE_DIR%
ECHO SCE_PS3_ROOT:           %SCE_PS3_ROOT%
ECHO END LOAD_SLN ENVIRONMENT

start "" %cd%\DongleTool_2008.sln

popd
