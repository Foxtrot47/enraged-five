//
// input/dongle_psn.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

// #if __PPU	// avoid LNK4221
#if !__WIN32PC

#if __PPU
#include <netex/libnetctl.h>
#include <cell/rtc.h>
#include <grcore/texturegcm.h>
#include <sys/fs_external.h>

// Rage
#include "data/aes.h"
#include "system/param.h"
#include "rline/rl.h"
#include "file/stream.h"
#include "file/asset.h"
#include "bank/bkmgr.h"
#include "bank/bank.h"

// Dongle
#include "dongletool_psn.h"


// --- Defines ------------------------------------------------------------------

PARAM(addressesFile, "addresses file where macAddreses are stored");

bool
CPSNDongleTool::ReadCodeFile(const char* encodeString, const char* macAddrString, char* errorString, const char* rootFolder)
{
	bool ret = false;

	// Note: This gets a bit messy since key and mac address are encoded one way (irreversibly) but 
	// date stamp is encoded using AES (reversibly). This is done to maintain backwards compatibility with
	// previous dongles

	/*-------------------------------*/
	int bufferMacAddressLen = StringLength(macAddrString)+1;
	int bufferKeyStringLen  = StringLength(encodeString)+1;
	int bufferDateStringLen = 32;
	int bufferLen = bufferMacAddressLen+bufferKeyStringLen+bufferDateStringLen;

	char* origBuffer = Alloca(char, bufferLen);
	char* encodedBuffer = Alloca(char, bufferLen);
	// code to ensure release and beta versions of this work
	memset(origBuffer, 0xcd, bufferLen);

	// Encode current key and mac address for comparison with read one
	// No need to do for date
	char* origBufferMacAddress = Alloca(char, bufferMacAddressLen);
	char* origBufferKeyString  = Alloca(char, bufferKeyStringLen);
	// code to ensure release and beta versions of this work
	memset(origBufferMacAddress, 0xcd, bufferMacAddressLen);
	memset(origBufferKeyString, 0xcd, bufferKeyStringLen);

	// Encode Mac Address
	memcpy(origBufferMacAddress, macAddrString, bufferMacAddressLen);
	Encode(origBufferMacAddress, bufferMacAddressLen);

	// Encode Key String
	memcpy(origBufferKeyString, encodeString,bufferKeyStringLen);
	Encode(origBufferKeyString, bufferKeyStringLen);

	// Mac Address + Key String
	memcpy(origBuffer, origBufferMacAddress, bufferMacAddressLen); 
	memcpy(origBuffer+bufferMacAddressLen, origBufferKeyString, bufferKeyStringLen);

	// Look up current date and time
	CellRtcTick currentTick;
	cellRtcGetCurrentTick(&currentTick); // Get current time (local time)


	if (rootFolder)
	{
		Displayf("Set Path to Root Folder '%s'...", rootFolder);
		ASSET.SetPath(rootFolder);
	}

	fiStream* pStream = ASSET.Open("root.sys", "", false, true); // open for reading
	if (!pStream)
	{
		if (errorString)
		{
			char* pMessage = "File root.sys does not exist";
			safecpy(errorString, pMessage, 128);
		}

		return false;
	}

	bool bDateReadOK = false;
	bool bDateOK = false;
	bool bMacOK = false;
	bool bKeyOK = false;

	while(true)// ((pLine = CFileMgr::ReadLine(fid)) != NULL) )
	{
		if(pStream->Read(encodedBuffer, bufferLen) != bufferLen)
			break;

		//if(*pLine == '#' || *pLine == '\0')
		//	break;

		// Check if current date is past expiry
		//int bytesRead = 0;
		//bytesRead = StringLength(pLine);

		/*--------------------------*/
		// Compare with read date
		char* readDateStr = encodedBuffer+bufferMacAddressLen+bufferKeyStringLen;
		AES aes;
		aes.Decrypt(readDateStr, bufferLen-bufferMacAddressLen-bufferKeyStringLen);
		CellRtcTick readTick;
		if(cellRtcParseDateTime(&readTick, readDateStr) == CELL_OK)
		{
			bDateReadOK = true;
			if(readTick.tick > currentTick.tick)
			{
				bDateOK = true;
			}
		}
		else
		{
#if __DEV
			// Displayf("[ioDongle] Notice: No expiry found in dongle");
#endif
		}

		/*--------------------------*/
		// Check MAC
		if(memcmp(origBufferMacAddress, encodedBuffer, bufferMacAddressLen) == 0)
		{
			bMacOK = true;
		}
		else
		{
			char zeroMac [bufferMacAddressLen];
			// Check for read MAC address being 0
			sprintf(zeroMac,"00:00:00:00:00:00");
			Encode(zeroMac,bufferMacAddressLen);
			if(memcmp(encodedBuffer, zeroMac, bufferMacAddressLen) == 0)
			{
				// Mac address on memory stick was set to 0 so we can ignore this check
				bMacOK = true;
			}
		}

		/*--------------------------*/
		// Check key
		if(memcmp(origBufferKeyString, encodedBuffer + bufferMacAddressLen, bufferKeyStringLen) == 0)
		{
			bKeyOK = true;
		}
	}

	// Note if no date could be read then we assume an expiry date was not set and continue loading
	if(bKeyOK && bMacOK && (bDateOK || !bDateReadOK))
	{
		ret = true;

		Displayf("AUTHORIZATION OK %s", macAddrString);
		char sMessage[128];
		sprintf(sMessage, "AUTHORIZATION OK %s", macAddrString);
		safecpy(errorString, sMessage, 128);
	}
	else
	{
		if (!bMacOK && !bKeyOK)
		{
			Displayf("UNAUTHORIZED demo copy");
			if (errorString)
			{
				char* pMessage = "UNAUTHORIZED demo copy - The dongle is for a different machine And The dongle key does not match the game";
				safecpy(errorString, pMessage, 128);
			}
		}
		else if( !bMacOK )
		{
			Displayf(" UNAUTHORIZED demo copy - The dongle is for a different machine.! %s", macAddrString);
			if (errorString)
			{
				char sMessage[128];
				sprintf(sMessage, " UNAUTHORIZED demo copy - The dongle is for a different machine.! %s", macAddrString);
				safecpy(errorString, sMessage, 128);
			}
		}
		else if( !bKeyOK )
		{
			Displayf(" UNAUTHORIZED demo copy - The dongle key does not match the game.! ");
			if (errorString)
			{
				char* pMessage = "UNAUTHORIZED demo copy - The dongle key does not match the game.";
				safecpy(errorString, pMessage, 128);
			}
		}

		if( !bDateOK && bDateReadOK)
		{
			Displayf(" UNAUTHORIZED demo copy - The dongle has expired.! ");
			if (errorString)
			{
				char* pMessage = "UNAUTHORIZED demo copy - The dongle has expired.!";
				safecpy(errorString, pMessage, 128);
			}
		}
	}
	pStream->Close();

	return ret;
}

void 
CPSNDongleTool::WriteCodeFile(const char* encodeString, const char* UNUSED_PARAM(macaddrString), const fiDevice::SystemTime expiryDate, bool bExpires, char* errorString, const char* rootFolder)
{
	int iNumAddresses = 0;

	char* pLine;

	if (rootFolder)
	{
		Displayf("Set Path to Root Folder '%s'...", rootFolder);
		ASSET.SetPath(rootFolder);
	}

	//pStream = ASSET.Create("addresses.dat", "", false);
	const char* addressesFile = NULL;
	if(PARAM_addressesFile.Get(addressesFile))
	{
	}
	else
	{
		addressesFile = "addresses.dat";
	}

	Displayf("Opening '%s'...", addressesFile);
	fiStream* pStream = ASSET.Open(addressesFile, "", false, true); // open for reading
	if (!pStream)
	{
		Displayf("Write Code File - DONGLE FAILED - Could not open %s file to read mac addresses!", addressesFile);

		if (errorString)
		{
			char sMessage[128];
			sprintf(sMessage, "Write Code File - DONGLE FAILED - Could not open %s file to read mac addresses", addressesFile);
			safecpy(errorString, sMessage, 128);
		}

		return;
	}

	if (pStream)
	{
		char filePathName[CELL_FS_MAX_FS_PATH_LENGTH];
		if (rootFolder)
		{
			snprintf(filePathName, CELL_FS_MAX_FS_PATH_LENGTH, "%s%s", rootFolder, "root.sys");
		}
		else
		{
			snprintf(filePathName, CELL_FS_MAX_FS_PATH_LENGTH, "%s", "root.sys");
		}

		Displayf("Creating '%s'...", filePathName);
		fiStream *fileStream = fiStream::Create(filePathName);

		if(!fileStream)
		{
			pStream->Close();
			Displayf("Write Code File - DONGLE FAILED - Cant create file %s", filePathName);

			if (errorString)
			{
				char sMessage[128];
				sprintf(sMessage, "Write Code File - DONGLE FAILED - Cant create file %s", filePathName);
				safecpy(errorString, sMessage, 128);
			}

			return;
		}

		while( ((pLine = ReadLine(pStream)) != NULL) )
		{
			if(*pLine == '#' || *pLine == '\0')
				continue;

			int length;
			char* origString = CreateEncodedMacAddress(encodeString, pLine, expiryDate, bExpires, length);
			if (origString)
			{
				fileStream->Write(origString, length);
			}

			iNumAddresses++;
		}

		int ret = 0;
		ret = fileStream->Close();
		if (0!=ret)
		{
			Assertf(0, "ERROR CLOSING FILE");
			Assertf(0, "DONGLE NOT WRITTEN");

			if (errorString)
			{
				char* pMessage = "Write Code File - ERROR CLOSING FILE, DONGLE NOT WRITTEN";
				safecpy(errorString, pMessage, 128);
			}

			return;
		}

		Displayf("Write Code File - FOUND - %d MAC ADDRESSES", iNumAddresses);
		if (errorString)
		{
			char sMessage[128];
			sprintf(sMessage, "Write Code File - Success - Found \"%d\" Mac Addresses", iNumAddresses);
			safecpy(errorString, sMessage, 128);
		}

		pStream->Close();
	}

	if(iNumAddresses<0)
	{
		Assertf(iNumAddresses>0, "Write Code File - 0 ADDRESSES TO ENCODE!" );
		return;
	}

	return;
}

//
//bool 
//CPSNDongleTool::ValidateCodeFile(const char* encodeString)
//{
//
///*	int err;
//	CellNetCtlInfo cell_info;
//	u8 mac[6];
//	char macAddrString [18];
//
//	// Look up current MAC address
//	if(0 <= (err = cellNetCtlGetInfo(CELL_NET_CTL_INFO_ETHER_ADDR, &cell_info)))
//	{
//		CompileTimeAssert(sizeof(mac) == CELL_NET_CTL_ETHER_ADDR_LEN);
//		sysMemCpy(mac, cell_info.ether_addr.data, CELL_NET_CTL_ETHER_ADDR_LEN);
//
//		// Get the Hexa String of the Mac Address
//		sprintf(macAddrString, "%02X:%02X:%02X:%02X:%02X:%02X", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
//		macAddrString[17] = '\0';
//	}
//	else
//	{
//		rlCellError("[ioDongle] ReadCodeFile() - Error calling cellNetCtlGetInfo()", err);
//	}
//
//	return ReadCodeFile(encodeString, macAddrString);*/
//
//	char* pLine;
//	CFileMgr::SetDir("");
//	FileHandle fid = CFileMgr::OpenFile("common:/DATA/ADDRESSES.DAT"); // open for reading
//
//	if (!CFileMgr::IsValidFileHandle(fid))
//	{
//		Assertf(0, "[CPSNDongleTool::WriteCodeFile] - Could not open common:/DATA/ADDRESSES.DAT file to read mac addresses!" );
//		return false;
//	}
//
//	if (fid)
//	{
//		while( ((pLine = CFileMgr::ReadLine(fid)) != NULL) )
//		{
//			if(*pLine == '#' || *pLine == '\0')
//				continue;
//
//			ReadCodeFile(encodeString, pLine);
//		}
//		CFileMgr::CloseFile(fid);
//	}
//	return true;
//}


void
CPSNDongleTool::RemoveCodeFile(char* errorString)
{
	safecpy(errorString, "Remove Code File - FAILED - Not Implemented yet. Delete file root.sys", 128);
}

bool 
CPSNDongleTool::Pending()
{
	return false;
}

char* 
CPSNDongleTool::CreateEncodedMacAddress(const char* encodeString, const char* macaddrString, const fiDevice::SystemTime expiryDate, bool bExpires, int& encodeStringLen)
{
	size_t bufferMacAddressLen = StringLength(macaddrString)+1;
	size_t bufferKeyStringLen  = StringLength(encodeString)+1;
	size_t bufferExpiryStringLen = 32;

	// Generate a system time string
	CellRtcDateTime cellDate;
	cellDate.year = expiryDate.wYear;
	cellDate.month = expiryDate.wMonth;
	cellDate.day = expiryDate.wDay;
	cellDate.hour = expiryDate.wHour;
	cellDate.minute = expiryDate.wMinute;
	cellDate.second = expiryDate.wSecond;
	cellDate.microsecond = 0;

	CellRtcTick cellExpiryTick;

	if(bExpires && cellRtcGetTick(&cellDate,&cellExpiryTick) != CELL_OK)
	{
#if __BANK
		Displayf("[ioDongle] Error converting expiry time! Dongle not written");
#endif // __BANK
		return NULL;
	}

	char* origBufferExpiryString = Alloca(char, bufferExpiryStringLen);

	if(bExpires)
		cellRtcFormatRfc2822LocalTime(origBufferExpiryString,&cellExpiryTick);
	else
		memset(origBufferExpiryString, 0x00, bufferExpiryStringLen);

#if __BANK
	Displayf("[ioDongle] Key : %s || %d", encodeString, bufferKeyStringLen);
	Displayf("[ioDongle] Mac Address : %s || %d", macaddrString, bufferMacAddressLen);
	Displayf("[ioDongle] Expiry Date : %s || %d", origBufferExpiryString, bufferExpiryStringLen);
#endif // __BANK

	// We will use AES to encrypt the date, its length must be a multiple of 16
	if(bufferExpiryStringLen%16 >0)
		bufferExpiryStringLen += 16-(bufferExpiryStringLen % 16);

	size_t bufferLen = bufferMacAddressLen + bufferKeyStringLen + bufferExpiryStringLen;
	char* outputString = Alloca(char, bufferLen);
	// code to ensure release and beta versions of this work
	memset(outputString, 0xcd, bufferLen);

	char* origBufferMacAddress = Alloca(char, bufferMacAddressLen);
	char* origBufferKeyString  = Alloca(char, bufferKeyStringLen);

	// Encode Mac Address
	memcpy(origBufferMacAddress, macaddrString, bufferMacAddressLen);
	CPSNDongleTool::Encode(origBufferMacAddress, bufferMacAddressLen);

	// Encode Key String
	memcpy(origBufferKeyString, encodeString, bufferKeyStringLen);
	CPSNDongleTool::Encode(origBufferKeyString, bufferKeyStringLen);

	AES aes;
	aes.Encrypt(origBufferExpiryString, bufferExpiryStringLen);

	// Copy Mac Address + Key String + Date string into origBuffer (for writing)
	memcpy(outputString, origBufferMacAddress, bufferMacAddressLen);
	memcpy(outputString+bufferMacAddressLen, origBufferKeyString, bufferKeyStringLen);
	memcpy(outputString+bufferMacAddressLen+bufferKeyStringLen, origBufferExpiryString, bufferExpiryStringLen);

	encodeStringLen = bufferLen;
	return outputString;
}

void 
CPSNDongleTool::Encode(char *buffer, int sizeInBytes)
{
	int value;
	int i;

	for(i=0; i<sizeInBytes; i++)
	{
		value= buffer[i];
		value += 61 - (77 * i)%13 + i/5;

		if(i>0)
		{
			value += buffer[i-1];
		}

		if(i<sizeInBytes-1)
		{
			value -= buffer[i+1] / 2;
		}

		value= value % 255;
		buffer[i]= (char)value;
	}
}

bool 
CPSNDongleTool::ReadLine(fiStream* pStream, char* pLine, int maxLen)
{
	int read = fgets(pLine,maxLen-1,pStream);
	if (read == 0  && pStream->Tell() >= pStream->Size())
		return false;

	int i;
	for(i=0; i<read; i++)
	{
		switch(pLine[i])
		{
		case '\r':
			pLine[i] = '\0';
			break;
		case '\n':
			pLine[i] = '\0';
		case '\0' :
			return true;				
		}
	}

	pLine[i] = '\0';

	return true;
}

char* 
CPSNDongleTool::ReadLine(fiStream* pStream, bool bRemoveUnwantedChars)
{
#define READ_SIZE_FOR_A_LINE 1024 //increased from 512 because time cycle was failing

	static char line[READ_SIZE_FOR_A_LINE];
	char* pLineStart = &line[0];

	if(ReadLine(pStream, &line[0], READ_SIZE_FOR_A_LINE))
	{
		char* pC = &line[0];
		while(*pC != '\0')
		{
			if(bRemoveUnwantedChars && ((*pC < ' ' && *pC >= 0) || *pC == ','))
				*pC = ' ';
			pC++;
		}

		// get beginning of line first non-space/non-control character
		while(*pLineStart == ' ' && *pLineStart != '\0') 
			pLineStart++;

		return pLineStart;
	}

	return NULL;
}

#endif // __PPU

#endif // !__WIN32PC

// EOF

