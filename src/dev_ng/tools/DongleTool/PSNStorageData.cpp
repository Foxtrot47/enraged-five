//
// name:        SPNStorageData.cpp
// description: Implements file exchanges in PS3 between the internal hard disk drive and a recording medium such as Memory Stick.
// notes:       - It only accepts files in the PLAYSTATION®3 format or PSP™ (PlayStation®Portable) format.
//              - Regardless of the original filename, the filename of the exported file will be EXPORT.BIN. 
//                Even when EXPORT.BIN already exists, the file will be overwritten without a separate name being assigned 
//                for the save or any confirmation being provided. Thus, when exporting two (or more) files consecutively, 
//                the file(s) exported earlier will be cleared.
//              - Regardless of the original filename, the filename of the imported file will be IMPORT.BIN. Even when 
//                IMPORT.BIN already exists, the file will be overwritten without a separate name being assigned for the save 
//                or any confirmation being provided. Thus, when Importing two (or more) files consecutively, the file(s) imported 
//                earlier will be cleared. Before this happens, the application must use IMPORT.BIN for processing, and save it 
//                under a different name if necessary. 
// author:		Miguel Freitas
//
#if __PPU

#define PRINT_DEBUG 0

#include <sysutil/sysutil_storagedata.h>
#include "PSNStorageData.h"
#include "system/sysmemcontainer_psn.h"
#include "string/string.h"

/*-----------------------------------------*/

namespace rage 
{

int  CPSNStorageData::m_iTransferStatus = CPSNStorageData::STATUS_NONE;
bool CPSNStorageData::m_bSuccess  = true;
u32  CPSNStorageData::m_MemoryContainer = SYS_MEMORY_CONTAINER_ID_INVALID;

void 
CPSNStorageData::TransferFile(const eStorageDataTransferType transferType, 
							  const char* sourcePath, 
							  const char* destinationPath, 
							  const int fileSize)
{
	Assert(SYS_MEMORY_CONTAINER_ID_INVALID == m_MemoryContainer);

	int ret = 0;
	char title[] = "TransferFile";
	CellStorageDataSetParam setParam;

	setParam.fileSizeMax = fileSize;
	setParam.title = title;
	setParam.reserved = NULL;

	m_MemoryContainer = SYSMEMCONTAINER.Allocate(MEMORY_CONTAINER_SIZE_SAVEDATA_COPY);

	Assert(m_MemoryContainer != SYS_MEMORY_CONTAINER_ID_INVALID);

	size_t bufferLen = StringLength(sourcePath)+1;
	char *srcMediaFile = Alloca(char, bufferLen);
	strcpy(srcMediaFile, sourcePath);

	bufferLen  = StringLength(sourcePath)+1;
	char *dstHddDir = Alloca(char, bufferLen);
	strcpy(dstHddDir, destinationPath);

	switch(transferType)
	{
	case FILE_IMPORT:
		{
			ret = cellStorageDataImport( CELL_STORAGEDATA_VERSION_CURRENT,
										 srcMediaFile,
										 dstHddDir,
										 &setParam,
										 &CPSNStorageData::CB_TransferFile,
										 m_MemoryContainer,
										 (void*)FILE_IMPORT );

			m_iTransferStatus = STATUS_RUNNING;
		}
		break;
	case FILE_EXPORT:
		{
			ret = cellStorageDataExport( CELL_STORAGEDATA_VERSION_CURRENT,
										 srcMediaFile,
										 dstHddDir,
										 &setParam,
										 &CPSNStorageData::CB_TransferFile,
										 m_MemoryContainer,
										 (void*)FILE_EXPORT );

			m_iTransferStatus = STATUS_RUNNING;
		}
		break;
	case FILE_MOVE:
		{
			ret = cellStorageDataImportMove( CELL_STORAGEDATA_VERSION_CURRENT,
											 srcMediaFile,
											 dstHddDir,
											 &setParam,
											 &CPSNStorageData::CB_TransferFile,
											 m_MemoryContainer,
											 (void*)FILE_MOVE );

			m_iTransferStatus = STATUS_RUNNING;
		}
		break;
	default: 
		{
			Displayf("UNKNOWN Transfer type.\n"); 
			ret = -1;
		}
		break;
	}

	if( ret != CELL_STORAGEDATA_RET_OK ) 
	{
		m_bSuccess = false;

#if PRINT_DEBUG
		Displayf(" [CPSNStorageData] ERROR - failed : 0x%x\n", ret);
		switch(ret)
		{
		case CELL_STORAGEDATA_ERROR_INTERNAL:  Displayf("Fatal internal error.\n"); break;
		case CELL_STORAGEDATA_ERROR_PARAM:     Displayf("Error in the parameter to be set to the utility(application error).\n"); break;
		case CELL_STORAGEDATA_ERROR_BUSY:      Displayf("Multiple functions of the storage data utility were called at the same time.\n"); break;
		default: Displayf("UNKNOWN ERROR occured.\n"); break;
		}
#endif

		SYSMEMCONTAINER.Destroy(m_MemoryContainer);
		m_MemoryContainer = SYS_MEMORY_CONTAINER_ID_INVALID;
		m_iTransferStatus = STATUS_NONE;
	}
	else
	{
		m_bSuccess = true;

#if PRINT_DEBUG
		switch(transferType)
		{
		case FILE_IMPORT: Displayf("Calling cellStorageDataImport() succeeded.\n"); break;
		case FILE_EXPORT: Displayf("Calling cellStorageDataExport() succeeded.\n"); break;
		case FILE_MOVE:   Displayf("Calling cellStorageDataImportMove() succeeded.\n"); break;
		default:          Displayf("UNKNOWN Transfer type.\n"); break;
		}
#endif
	}
}

void 
CPSNStorageData::Update()
{
	int ret = 0;
	ret = cellSysutilCheckCallback();

	if ( ret ) 
	{
		Displayf( "cellSysutilCheckCallback() = 0x%x\n", ret );
	}
}

void 
CPSNStorageData::CB_TransferFile(int result, 
								 void* userdata)
{
#if PRINT_DEBUG
	(void)userdata;

	int mode = (int)userdata;
	switch(mode)
	{
	case FILE_IMPORT: Displayf(" [CPSNStorageData] CPSNStorageData::CB_TransferFile() : FILE_IMPORT \n"); break;
	case FILE_EXPORT: Displayf(" [CPSNStorageData] CPSNStorageData::CB_TransferFile() : FILE_EXPORT \n"); break;
	case FILE_MOVE: Displayf(" [CPSNStorageData] CPSNStorageData::CB_TransferFile() : FILE_MOVE \n"); break;
	default: Displayf(" [CPSNStorageData] CPSNStorageData::CB_TransferFile() : UNKNOWN TRANSFER \n"); break;
	}

	Displayf(" [CPSNStorageData] CPSNStorageData::CB_TransferFile() : 0x%x \n", result);
#endif
	m_iTransferStatus = STATUS_NONE;

	if (result!=CELL_STORAGEDATA_RET_OK)
	{
		m_bSuccess  = false;

#if PRINT_DEBUG
		switch(result)
		{
		case CELL_STORAGEDATA_RET_CANCEL:         Displayf("CELL_STORAGEDATA_RET_CANCEL : User canceled and closed import dialog.\n"); break;
		case CELL_STORAGEDATA_ERROR_INTERNAL:     Displayf("CELL_STORAGEDATA_ERROR_INTERNAL : System error occured.\n"); break;
		case CELL_STORAGEDATA_ERROR_ACCESS_ERROR: Displayf("CELL_STORAGEDATA_ERROR_ACCESS_ERROR : HDD access error occured.\n"); break;
		case CELL_STORAGEDATA_ERROR_FAILURE:      Displayf("CELL_STORAGEDATA_ERROR_FAILURE : Copy process has failed.\n"); break;
		default:                                  Displayf("UNKNOWN ERROR occured.\n");	break;
		}
#endif
	}
	else
	{
		m_bSuccess  = true;
	}

	Assert(SYS_MEMORY_CONTAINER_ID_INVALID != m_MemoryContainer);

	SYSMEMCONTAINER.Destroy(m_MemoryContainer);
	m_MemoryContainer = SYS_MEMORY_CONTAINER_ID_INVALID;
}


} // namespace rage

#endif // __PPU

// eof

