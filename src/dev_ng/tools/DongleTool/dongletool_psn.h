//
// dongletool_psn.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef PSNDONGLETOOL_H
#define PSNDONGLETOOL_H

#if __PPU

#include "file/device.h"

// --- Forward Definitions ------------------------------------------------------
namespace rage
{
	class fiStream;
}

// --- Defines ------------------------------------------------------------------


// --- Class --------------------------------------------------------------------

class CPSNDongleTool
{
public:
	bool  ReadCodeFile(const char* encodeString, const char* macAddrString, char* errorString, const char* rootFolder);
	void  WriteCodeFile(const char* encodeString, const char* macaddrString, const fiDevice::SystemTime expiryDate, bool bExpires, char* errorString, const char* rootFolder);
	void  RemoveCodeFile(char* errorString);
	bool  Pending();

private:
	static char* CreateEncodedMacAddress(const char* encodeString, const char* macaddrString, const fiDevice::SystemTime expiryDate, bool bExpires, int& encodeStringLen);
	static bool ValidateCodeFile(const char* encodeString, const char* macAddress);

	//Some very cheesy encoding to avoid that the string that is written
	//to the memory card directly shows up in the executeable.
	static void Encode(char *buffer, int sizeInBytes);

	//Read a single line from the file
	bool  ReadLine(fiStream* pStream, char* pLine, int maxLen);

	//Read a single line from the file and possibly remove crap (tabs and commas)
	char* ReadLine(fiStream* pStream, bool bRemoveUnwantedChars = true);

};

#endif // __PPU

#endif // PSNDONGLETOOL_H
