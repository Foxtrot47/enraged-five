//
// dongletool_xenon.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef XENONDONGLETOOL_H
#define XENONDONGLETOOL_H

#if __XENON

#include "file/device.h"


// --- Forward Definitions ------------------------------------------------------


// --- Defines ------------------------------------------------------------------


// --- Class --------------------------------------------------------------------

class CXenonDongleTool
{
public:
	bool  ReadCodeFile(const char* encodeString, const char* macAddrString, char* errorString, const char* rootFolder);
	void  WriteCodeFile(const char* encodeString, const char* macaddrString, const fiDevice::SystemTime expiryDate, bool bExpires, char* errorString, const char*  rootFolder);
	void  RemoveCodeFile(char* errorString);
	bool  Pending();

private:

};

#endif // __XENON

#endif // XENONDONGLETOOL_H
