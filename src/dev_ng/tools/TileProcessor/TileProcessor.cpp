// =================
// TileProcessor.cpp
// =================

#include "../common/common.h"
#include "../common/dds.h"
#include "../common/fileutil.h"
#include "../common/imageutil.h"
#include "../common/progressdisplay.h"
#include "../common/stringutil.h"
#include "../common/vector.h"

#include "../../rage/framework/src/fwgeovis/geovis.h"

static void ConvertToDDS(const char* path)
{
	int w = 0;
	int h = 0;
	u8* image = LoadImage_u8(path, w, h);

	if (image)
	{
		char path2[1024] = "";
		strcpy(path2, path);
		strcpy(strrchr(path2, '.'), ".dds");

		SaveImage(path2, image, w, h, false);
	}
}

static void ConvertToPNG(const char* path, bool bNormalise, bool bSaveAsU16)
{
	int w = 0;
	int h = 0;
	float* image = LoadImage_float(path, w, h);

	if (image)
	{
		char path2[1024] = "";
		strcpy(path2, path);
		strcpy(strrchr(path2, '.'), bSaveAsU16 ? ".png" : ".png");

		if (bNormalise)
		{
			float valueMin = image[0];
			float valueMax = image[0];

			for (int i = 1; i < w*h; i++)
			{
				valueMin = Min<float>(image[i], valueMin);
				valueMax = Max<float>(image[i], valueMax);
			}

			if (valueMin < valueMax)
			{
				for (int i = 0; i < w*h; i++)
				{
					image[i] = (image[i] - valueMin)/(valueMax - valueMin);
				}
			}
		}

		if (bSaveAsU16)
		{
			u16* temp = new u16[w*h];

			for (int i = 0; i < w*h; i++)
			{
				temp[i] = (u16)Clamp<float>(0.5f + 65535.0f*image[i], 0.0f, 65535.0f);
			}

			SaveImage(path2, temp, w, h, false, true);
			delete[] temp;
		}
		else
		{
			SaveImage(path2, image, w, h, false);
		}
	}
}

static void ConvertToPNG_RGB(const char* path)
{
	int w = 0;
	int h = 0;
	Pixel32* image = LoadImage_Pixel32(path, w, h);

	if (image)
	{
		char path2[1024] = "";
		strcpy(path2, path);
		strcpy(strrchr(path2, '.'), ".png");

		for (int i = 0; i < w*h; i++)
		{
			image[i].a = 255;
		}

		SaveImage(path2, image, w, h, false);
	}
}

static void ConvertScreenCaptureTiles(const char* path)
{
	int coordStep = 1;//gv::WORLD_CELLS_PER_TILE;

	if (stristr(path, "gta5")) // TODO -- once we've migrated the new tile indexing to V, we can remove this hack
	{
		coordStep = gv::WORLD_CELLS_PER_TILE;
	}

	u64 layerFlags = 0;
	int layerMax = 0;

	int resX = 0;
	int resY = 0;

	int i0 = +99999;
	int j0 = +99999;
	int i1 = -99999;
	int j1 = -99999;

	// first scan the directory to find the tile bounds ..
	{
		std::vector<std::string> files;
		BuildSearchList(files, path, ".dds");

		for (int i = 0; i < (int)files.size(); i++)
		{
			const char* filepath = files[i].c_str();
			const char* s = strchr(strrchr(filepath, '/') + 1, '_');

			if (s)
			{
				const char* layerStr = strstr(filepath, "layer_");
				int layerIndex = -1;

				if (layerStr)
				{
					layerIndex = atoi(strchr(layerStr, '_') + 1);
				}

				printf("filepath = %s, layerStr = %s, layerIndex=%d\n", filepath, layerStr ? layerStr : "NULL", layerIndex);
				if (layerIndex >= 0 && layerIndex < 64)
				{
					layerMax = Max<int>(layerIndex, layerMax);
					layerFlags |= BIT64(layerIndex);
				}

				const int coordI = atoi(s + 1)/coordStep;

				s = strchr(s + 1, '_');

				if (s)
				{
					const int coordJ = atoi(s + 1)/coordStep;

					const int coordIMin = ((gv::WORLD_BOUNDS_MIN_X - gv::NAVMESH_BOUNDS_MIN_X)/gv::WORLD_CELL_SIZE)/coordStep;
					const int coordJMin = ((gv::WORLD_BOUNDS_MIN_Y - gv::NAVMESH_BOUNDS_MIN_Y)/gv::WORLD_CELL_SIZE)/coordStep;
					const int coordIMax = ((gv::WORLD_BOUNDS_MAX_X - gv::NAVMESH_BOUNDS_MIN_X)/gv::WORLD_CELL_SIZE - gv::WORLD_CELLS_PER_TILE)/coordStep;
					const int coordJMax = ((gv::WORLD_BOUNDS_MAX_Y - gv::NAVMESH_BOUNDS_MIN_Y)/gv::WORLD_CELL_SIZE - gv::WORLD_CELLS_PER_TILE)/coordStep;

					if (coordI >= coordIMin && coordI <= coordIMax &&
						coordJ >= coordJMin && coordJ <= coordJMax)
					{
						i0 = Min<int>(coordI, i0);
						j0 = Min<int>(coordJ, j0);
						i1 = Max<int>(coordI, i1);
						j1 = Max<int>(coordJ, j1);

						if (resX == 0 && resY == 0)
						{
							delete[] LoadImage_Pixel32(files[i].c_str(), resX, resY); // get resolution ..
						}
					}
					else
					{
						//printf("skipping %s\n", filepath);
					}
				}
			}
		}
	}

	fprintf(stdout, "i=[%d..%d], j=[%d..%d], layerMax=%d, layerFlags=0x%08x%08x, res=%dx%d, fullres=%dx%d\n", i0*coordStep, i1*coordStep, j0*coordStep, j1*coordStep, layerMax, (u32)(layerFlags>>32), (u32)layerFlags, resX, resY, resX*(i1 - i0 + 1), resY*(j1 - j0 + 1));

	if (resX > 0 && resY > 0 && i0 <= i1 && j0 <= j1)
	{
		const int w = (i1 - i0 + 1)*resX;
		const int h = (j1 - j0 + 1)*resY;

		Pixel32* image = new Pixel32[w*h];

		for (int layerIndex = layerMax; layerIndex >= 0; layerIndex--)
		{
			if ((layerFlags & BIT64(layerIndex)) == 0)
			{
				continue;
			}

			int numNotFound = 0;

			for (int j = j0; j <= j1; j++)
			{
				for (int i = i0; i <= i1; i++)
				{
					char tileName[512] = "";
					sprintf(tileName, "%s\\tile_%s_layer_%04d", path, gv::GetTileNameFromCoords(i*gv::WORLD_CELLS_PER_TILE, j*gv::WORLD_CELLS_PER_TILE), layerIndex);

					int w1 = resX;
					int h1 = resY;
					Pixel32* tile = LoadImage_Pixel32(varString("%s.png", tileName).c_str(), w1, h1);

					if (tile == NULL)
					{
						tile = LoadImage_Pixel32(varString("%s.dds", tileName).c_str(), w1, h1);
					}

					if (tile)
					{
						for (int k = 0; k < resX*resY; k++) // for now - force alpha to 255, because it's annoying to deal with in photoshop
						{
							tile[k].a = 255;
						}

						for (int jj = 0; jj < resY; jj++)
						{
							for (int ii = 0; ii < resX; ii++)
							{
								image[(ii + (i - i0)*resX) + (h - (jj + (j - j0)*resY) - 1)*w] = tile[ii + (resY - jj - 1)*resX];
							}
						}

						delete[] tile;
					}
					else if (numNotFound < 20)
					{
						printf("%s not found%s\n", tileName, ++numNotFound == 20 ? " ..." : "");
					}
				}
			}

			char compositePath[1024] = "";
			strcpy(compositePath, path);
			sprintf(strrchr(compositePath, '\\'), "\\composite_%d.png", layerIndex);
			SaveImage(compositePath, image, w, h, layerIndex == 0);
		}

		delete[] image;
	}
}

static void ConvertAmbientScannerTiles(const char* path)
{
	const int w = gv::WORLD_BOUNDS_MAX_X - gv::WORLD_BOUNDS_MIN_X;
	const int h = gv::WORLD_BOUNDS_MAX_Y - gv::WORLD_BOUNDS_MIN_Y;
	Pixel32* image = new Pixel32[w*h];

	// height
	{
		std::vector<std::string> files;
		BuildSearchList(files, path, ".png");

		memset(image, 0, w*h*sizeof(Pixel32));

		ProgressDisplay progress("assembling height images");

		for (int k = 0; k < (int)files.size(); k++)
		{
			const char* filepath = files[k].c_str();
			int tile_w = 0;
			int tile_h = 0;
			Pixel32* tile = LoadImage_Pixel32(filepath, tile_w, tile_h);

			if (tile)
			{
				const int coords_x = atoi(strstr(filepath, "x=") + strlen("x=")); // world coords of tile
				const int coords_y = atoi(strstr(filepath, "y=") + strlen("y="));
				const int coords_w = atoi(strstr(filepath, "w=") + strlen("w="));
				const int coords_h = atoi(strstr(filepath, "h=") + strlen("h="));

				const int scale_x = coords_w/tile_w; // should be 3
				const int scale_y = coords_h/tile_h;

				for (int tile_j = 0; tile_j < tile_h; tile_j++)
				{
					for (int tile_i = 0; tile_i < tile_w; tile_i++)
					{
						for (int j2 = 0; j2 < scale_y; j2++)
						{
							for (int i2 = 0; i2 < scale_x; i2++)
							{
								const int i3 = coords_x + i2 + tile_i*scale_x;
								const int j3 = coords_y + j2 + tile_j*scale_y;

								if (i3 >= 0 && i3 < w &&
									j3 >= 0 && j3 < h)
								{
									Pixel32& p = image[i3 + (h - j3 - 1)*w];

									p.r = tile[tile_i + tile_j*tile_w].r;
									p.g = tile[tile_i + tile_j*tile_w].g;
									p.b = tile[tile_i + tile_j*tile_w].b;

									if (p.a < 255)
									{
										p.a++;
									}
								}
							}
						}
					}
				}

				delete[] tile;
			}

			progress.Update(k, files);
		}

		float maxDiff = 0.0f;

		for (int i = 0; i < w*h; i++)
		{
			maxDiff = Max<float>((float)(image[i].r - image[i].g), maxDiff);
		}

		if (maxDiff == 0.0f)
		{
			maxDiff = 1.0f;
		}

		u8* heightDiff = new u8[w*h];

		for (int i = 0; i < w*h; i++)
		{
			heightDiff[i] = (u8)(0.5f + 255.0f*sqrtf((float)(image[i].r - image[i].g)/maxDiff));

			if (image[i].a == 0)
			{
				image[i] = Pixel32(0,0,255,255);
			}
			else if (image[i].a > 1)
			{
				image[i].r = (image[i].r + 2*255)/3; // blend towards red
				image[i].g = (image[i].g)/3;
				image[i].b = (image[i].b)/3;
			}

			image[i].a = 255;
		}

		char path2[512] = "";
		sprintf(path2, "ambientscanner_height.png");
		SaveImage(path2, image, w, h);

		sprintf(path2, "ambientscanner_heightdiff.png");
		SaveImage(path2, heightDiff, w, h);

		delete[] heightDiff;

		progress.End();
	}

	// search
	{
		std::vector<std::string> files;
		BuildSearchList(files, path, ".dds");

		ProgressDisplay progress("assembling search images");
		const int n = 7;

		for (int bit = 0; bit < n; bit++)
		{
			memset(image, 0, w*h*sizeof(Pixel32));

			for (int k = 0; k < (int)files.size(); k++)
			{
				const char* filepath = files[k].c_str();
				int tile_w = 0;
				int tile_h = 0;
				u8* tile = LoadImage_u8(filepath, tile_w, tile_h);

				if (tile)
				{
					const int coords_x = atoi(strstr(filepath, "x=") + strlen("x=")); // world coords of tile
					const int coords_y = atoi(strstr(filepath, "y=") + strlen("y="));
					const int coords_w = atoi(strstr(filepath, "w=") + strlen("w="));
					const int coords_h = atoi(strstr(filepath, "h=") + strlen("h="));

					const int scale_x = coords_w/tile_w; // should be 3
					const int scale_y = coords_h/tile_h;

					for (int tile_j = 0; tile_j < tile_h; tile_j++)
					{
						for (int tile_i = 0; tile_i < tile_w; tile_i++)
						{
							for (int j2 = 0; j2 < scale_y; j2++)
							{
								for (int i2 = 0; i2 < scale_x; i2++)
								{
									const int i3 = coords_x + i2 + tile_i*scale_x;
									const int j3 = coords_y + j2 + tile_j*scale_y;

									if (i3 >= 0 && i3 < w &&
										j3 >= 0 && j3 < h)
									{
										Pixel32& p = image[i3 + (h - j3 - 1)*w];

										if (tile[tile_i + tile_j*tile_w] & BIT(bit))
										{
											p.r = 255;
											p.g = 255;
											p.b = 255;
										}

										if (p.a < 255)
										{
											p.a++;
										}
									}
								}
							}
						}
					}

					delete[] tile;
				}
			}

			for (int i = 0; i < w*h; i++)
			{
				if (image[i].a == 0)
				{
					image[i] = Pixel32(0,0,255,255);
				}
				else if (image[i].a > 1)
				{
					image[i].r = (image[i].r + 2*255)/3; // blend towards red
					image[i].g = (image[i].g)/3;
					image[i].b = (image[i].b)/3;
				}

				image[i].a = 255;
			}

			char path2[512] = "";
			sprintf(path2, "ambientscanner_search_%02d.png", bit);
			SaveImage(path2, image, w, h);

			progress.Update(bit, n);
		}

		progress.End();
	}

	delete[] image;
}

void BuildSearchList_NoRecursion(std::vector<std::string>& files, const char* path, const char* ext1 = NULL, const char* ext2 = NULL)
{
	char cmd[1024] = "";
	sprintf(cmd, "dir $B %s > %s", path, RS_TEMPFILE);

	for (char* s = cmd; *s; s++)
	{
		if (*s == '/') { *s = '\\'; }
		else if (*s == '$') { *s = '/'; }
	}

	system(cmd);

	FILE* fp = fopen(RS_TEMPFILE, "r");

	if (fp)
	{
		char line[1024] = "";

		while (ReadFileLine(line, sizeof(line), fp))
		{
			for (char* s = line; *s; s++) { if (*s == '\\') { *s = '/'; } }
			_strlwr(line);

			const char* ext = strrchr(line, '.');

			if (ext)
			{
				if ((ext1 && strcmp(ext, ext1) == 0) ||
					(ext2 && strcmp(ext, ext2) == 0) ||
					(ext1 == NULL && ext2 == NULL))
				{
					files.push_back(line);
				}
			}
		}
	}
}

static void ConvertTerrainTiles(const char* path)
{
	std::vector<std::string> files;
	BuildSearchList_NoRecursion(files, path, ".tif");

	if (strstr(path, "\\debris"))
	{
		// X:\rdr3\assets\metadata\terrain\debris
		// terrain_xx_yy.tif, where xx and yy range from 00 through 07 - this corresponds to 2x2 terrain tiles, skipping the far left column

		int tw0 = 0;
		int th0 = 0;
		if (GetImageDimensions(varString("%s\\terrain_00_00.tif", path).c_str(), tw0, th0))
		{
			int w = tw0/2 + tw0*8;
			int h = th0/2 + th0*8;
			Pixel32* image = new Pixel32[w*h];

			for (int i = 0; i < w*h; i++)
			{
				image[i] = Pixel32(0,0,0,255);
			}
			
			ProgressDisplay progress("assembling image");
			int index = 0;

			for (int row = 0; row < 8; row++)
			{
				for (int col = 0; col < 8; col++)
				{
					int tw = 0;
					int th = 0;
					Pixel32* tile = LoadImage_Pixel32(varString("%s\\terrain_%02d_%02d.tif", path, row, col).c_str(), tw, th);

					if (tile)
					{
						if (tw == tw0 && th == th0)
						{
							for (int j = 0; j < th; j++)
							{
								for (int i = 0; i < tw; i++)
								{
									image[(tw/2 + col*tw + i) + ((7 - row)*th + j)*w] = tile[i + j*tw];
								}
							}
						}

						delete[] tile;
					}

					progress.Update(index++, 8*8);
				}
			}

			progress.End();

			SaveImage(varString("%s\\__terrain.png", path).c_str(), image, w, h);
			delete[] image;
		}
	}
	else // grass or trees
	{
		// X:\rdr3\assets\metadata\terrain\grass
		// X:\rdr3\assets\metadata\terrain\trees
		// f_02_ through u_17_ with extensions .tif, _data.tif, and _tint.tif

		int tw0 = 0;
		int th0 = 0;
		if (GetImageDimensions(varString("%s\\f_02_.tif", path).c_str(), tw0, th0))
		{
			const int rowMin = 'f';
			const int rowMax = 'u';
			const int colMin = 2;
			const int colMax = 17;

			int w = tw0*(colMax - colMin + 1);
			int h = th0*(rowMax - rowMin + 1);
			Pixel32* image = new Pixel32[w*h];

			const char* suffixes[] = {"", "_data", "_tint"};

			for (int n = 0; n < NELEM(suffixes); n++)
			{
				for (int i = 0; i < w*h; i++)
				{
					image[i] = Pixel32(0,0,0,255);
				}

				ProgressDisplay progress(varString("assembling image *%s", suffixes[n]).c_str());
				int index = 0;

				for (int row = rowMin; row <= rowMax; row++)
				{
					for (int col = colMin; col <= colMax; col++)
					{
						int tw = 0;
						int th = 0;
						Pixel32* tile = LoadImage_Pixel32(varString("%s\\%c_%02d_%s.tif", path, row, col, suffixes[n]).c_str(), tw, th);

						if (tile)
						{
							if (tw == tw0 && th == th0)
							{
								for (int j = 0; j < th; j++)
								{
									for (int i = 0; i < tw; i++)
									{
										image[((col - colMin)*tw + i) + ((row - rowMin)*th + j)*w] = tile[i + j*tw];
									}
								}
							}

							delete[] tile;
						}

						progress.Update(index++, (rowMax - rowMin + 1)*(colMax - colMin + 1));
					}
				}

				progress.End();

				SaveImage(varString("%s\\__terrain%s.png", path, suffixes[n]).c_str(), image, w, h);
			}

			delete[] image;
		}
	}
}

int main(int argc, const char* argv[])
{
	if (argc == 2) // convert tiles
	{
		const char* path = argv[1];

		if (strstr(path, "\\screencapture\\tiles") != NULL && strchr(path, '.') == NULL)
		{
			ConvertScreenCaptureTiles(path);
			system("pause");
			return 0;
		}

		if (strstr(path, "\\ambientscanner\\tiles") != NULL && strchr(path, '.') == NULL)
		{
			ConvertAmbientScannerTiles(path);
			system("pause");
			return 0;
		}

		if (strstr(path, "\\assets\\metadata\\terrain") != NULL && strchr(path, '.') == NULL)
		{
			ConvertTerrainTiles(path);
			system("pause");
			return 0;
		}
	}

	if (0 && argc == 2) // convert tif -> dds .. (hack)
	{
		const char* path = argv[1];

		if (strstr(path, "\\assets") != NULL && strchr(path, '.') == NULL)
		{
			fprintf(stdout, "searching for tif files .. ");
			fflush(stdout);

			std::vector<std::string> files;
			BuildSearchList(files, path, ".tif");

			fprintf(stdout, "ok.\n");

			ProgressDisplay progress("converting %d tif files to dds", (int)files.size());

			for (int i = 0; i < (int)files.size(); i++)
			{
				int w = 0;
				int h = 0;
				Pixel32* image = LoadImage_Pixel32(files[i].c_str(), w, h);

				if (image)
				{
					char path[512] = "";
					strcpy(path, files[i].c_str());
					strcpy(strrchr(path, '.'), ".dds");
					SaveImage(path, image, w, h, false);
					delete[] image;
				}

				progress.Update(i, files);
			}

			progress.End();
			system("pause");
			exit(0);
		}
	}

	// convert DDS -> PNG
	{
		const char* lastDDSPath = NULL;

		for (int i = 1; i < argc; i++)
		{
			const char* path = argv[i];

			if (strstr(path, ".dds"))
			{
				if (strstr(path, "screencapture"))
				{
					ConvertToPNG_RGB(path);
				}
				else
				{
					bool bIsL16Format = false;

					// first check if it's L16, if so, then we want to output a TIF
					{
						FILE* ddsFile = fopen(path, "rb");

						if (ddsFile)
						{
							DDSURFACEDESC2 ddsHeader;

							fseek(ddsFile, 4, SEEK_SET);
							fread(&ddsHeader, sizeof(ddsHeader), 1, ddsFile);
							fclose(ddsFile);

							if (ddsHeader.ddpfPixelFormat.dwRBitMask == 0x0000ffff &&
								ddsHeader.ddpfPixelFormat.dwRGBBitCount == 16)
							{
								bIsL16Format = true;
							}
						}
					}

					ConvertToPNG(path, true, bIsL16Format);
				}

				lastDDSPath = path;
			}
		}

		if (lastDDSPath) // auto-open last file
		{
			char lastPNGPath[1024] = "";
			strcpy(lastPNGPath, lastDDSPath);
			strcpy(strrchr(lastPNGPath, '.'), ".png");
			system(lastPNGPath);
			return 0;
		}
	}

	// convert PNG -> DDS
	{
		const char* lastPNGPath = NULL;

		for (int i = 1; i < argc; i++)
		{
			const char* path = argv[i];

			if (strstr(path, ".png"))
			{
				ConvertToDDS(path);
				lastPNGPath = path;
			}
		}

		if (lastPNGPath) // auto-open last file
		{
			char lastDDSPath[1024] = "";
			strcpy(lastDDSPath, lastPNGPath);
			strcpy(strrchr(lastDDSPath, '.'), ".dds");
			system(lastDDSPath);
			return 0;
		}
	}

	// convert array -> PNG
	{
		if (argc > 1)
		{
			const char* path = argv[1];

			if (strstr(path, ".array"))
			{
				FILE* fp = fopen(path, "rb");

				if (fp)
				{
					float valueMin = 0.0f;
					float valueMax = 0.0f;
					int numBuckets = 0;

					fread(&valueMin, sizeof(float), 1, fp);
					fread(&valueMax, sizeof(float), 1, fp);
					fread(&numBuckets, sizeof(int), 1, fp);

					const int w = numBuckets;
					const int h = w/2;

					int* histogram = new int[numBuckets];
					float* graph = new float[numBuckets];
					float* image = new float[w*h];

					fread(histogram, sizeof(int), numBuckets, fp);
					fclose(fp);

					int histogramMin = histogram[0];
					int histogramMax = histogram[0];

					for (int i = 1; i < numBuckets; i++)
					{
						histogramMin = Min<int>(histogram[i], histogramMin);
						histogramMax = Max<int>(histogram[i], histogramMax);
					}

					for (int i = 0; i < numBuckets; i++)
					{
						graph[i] = (float)(histogram[i] - histogramMin)/(float)(histogramMax - histogramMin); // [0..1]
					}

					delete[] histogram;

					Graph(image, graph, w, h);

					char pngPath[1024] = "";
					strcpy(pngPath, path);
					strcpy(strrchr(pngPath, '.'), "_graph.png");

					delete[] graph;

					fprintf(stdout, "valueMin     = %f\n", valueMin);
					fprintf(stdout, "valueMax     = %f\n", valueMax);
					fprintf(stdout, "histogramMin = %d\n", histogramMin);
					fprintf(stdout, "histogramMax = %d\n", histogramMax);
					fprintf(stdout, "numBuckets   = %d\n", numBuckets);

					SaveImage(pngPath, image, w, h, false);

					delete[] image;

					system(pngPath);
				}

				if (IsDebuggerPresent())
				{
					system("pause");
				}

				return 0;
			}
		}
	}

	return 0;
}
