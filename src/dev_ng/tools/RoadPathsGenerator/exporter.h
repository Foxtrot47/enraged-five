#ifndef __EXPORTER_H
#define __EXPORTER_H

#include "fwnavgen/basetool.h"
#include "config.h"

//-----------------------------------------------------------------------------

class CRoadPathsExporterTool : public fwLevelProcessToolExporterInterface
{
public:

	CRoadPathsExporterTool(fwLevelProcessTool * pTool);
	virtual ~CRoadPathsExporterTool();

	// -- fwLevelProcessToolExporterInterface interface --

	virtual void PreInit();
	virtual void Init();
	virtual void Shutdown();

	virtual bool UpdateExport();

protected:
	// Opportunity to set the default of IPL groups across the map
	void LoadDefaultIplGroupList();
	void RequestDefaultIplGroups();

	CNavMeshDataDefaultIplGroups*		m_DefaultIplGroups;			// Owned
};

#endif	//__EXPORTER_H
