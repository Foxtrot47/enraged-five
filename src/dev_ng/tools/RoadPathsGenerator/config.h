//
// RoadPathsGenerator/config.h
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef ROADPATHSGENERATOR_CONFIG_H
#define ROADPATHSGENERATOR_CONFIG_H

#include "atl/array.h"
#include "string/string.h"
#include "parser/macros.h"		// PAR_PARSABLE

//-----------------------------------------------------------------------------

//stole this from the navgen tool -JM
/*
PURPOSE
	Simple XML-loadable list of names of IPL groups that should be loaded by default
	when exporting level geometry to build navmesh data from.
*/
struct CNavMeshDataDefaultIplGroups
{
public:
	atArray<ConstString> m_DefaultIplGroups;

	PAR_SIMPLE_PARSABLE;
};

#endif //ROADPATHSGENERATOR_CONFIG_H

// End of file RoadPathsGenerator/config.h