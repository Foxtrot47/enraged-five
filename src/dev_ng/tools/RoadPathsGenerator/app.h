#ifndef ROADPATHSGEN_APP_H
#define ROADPATHSGEN_APP_H

#include "fwnavgen/basetool.h"
#include "fwsys/gameskeleton.h"

//-----------------------------------------------------------------------------

class CLevelProcessToolGameInterfaceGta : public fwLevelProcessToolGameInterface
{
public:
	CLevelProcessToolGameInterfaceGta(fwLevelProcessTool * pTool) : fwLevelProcessToolGameInterface(pTool) { }
	virtual bool InitGame();
	virtual void ShutdownGame();

	virtual void Update1();
	virtual void Update2();

protected:
	void InitForExport();
	void InitLevelForExport(int level);

	void RegisterGameSkeletonFunctions();
	void RegisterCoreInitFunctions();
	void RegisterBeforeMapLoadedInitFunctions();
	void RegisterAfterMapLoadedInitFunctions();
	void RegisterSessionInitFunctions();

	gameSkeleton	m_GameSkeleton;
};

//-----------------------------------------------------------------------------

#endif	// ROADPATHSGEN_APP_H
