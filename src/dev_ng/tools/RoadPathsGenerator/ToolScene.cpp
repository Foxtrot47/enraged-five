
#include "pathserver/PathServer.h"
#include "fwscene/stores/mapdatastore.h"
#include "scene/loader/mapFileMgr.h"
#include "scene/world/GameWorld.h"
#include "scene/portals/portal.h"
#include "scene/scene.h"
#include "vehicles/Trailer.h"
#include "vehicles/VehicleFactory.h"
#include "physics/physics.h"
#include "peds/ped.h"
#include "peds/popcycle.h"

#include "ToolScene.h"

//-----------------------------------------------------------------------------

// PURPOSE: This interface is used for some game-specific extensions to CPathServer.
static CPathServerGameInterfaceGta s_PathServerGameInterface;

void CToolScene::Init(unsigned initMode)
{
	// The code here was originally adapted from CScene::Init(). /FF

	fwScene::InitClass(rage_new CGtaSceneInterface());
	fwScene::Init();

	INSTANCE_STORE.Init(initMode);
	// Maybe necessary for Water::Init():
	//	CLights::Init(); 
	CModelInfo::Init(initMode); 
	ThePaths.Init(initMode);
	CPathServer::Init(s_PathServerGameInterface, CPathServer::EMultiThreaded);
	CVehicle::InitSystem();

	CGameWorld::Init(initMode); // depends on InitPools()
	CPhysics::Init(initMode); // depends on InitPools() 
	// Technically, it would be more correct to call this (since we need water data),
	// but currently Init(initMode) only seems to do graphics stuff, and may have
	// some issues on PC. /FF
	//	Water::Init(initMode); // dependent on CLights::Init()

	CMapFileMgr::Init(initMode);
}


void CToolScene::Update()
{
	CVehicleModelInfo::UpdateHDRequests();

	CPortal::Update();

	// Disabled, this was using the camera position. Instead,
	// we make sure in CNavMeshDataExporterTool::ProcessCollisionExport()
	// that the CCompEntity objects get streamed in and updated.
	//	CCompEntity::Update();

	CPopCycle::Update();

	CPhysics::UpdateRequests();		// Needed for streaming in buildings (interiors?).

	ThePaths.Update();

	// This doesn't do much except failing an assert if the player doesn't exist
	// (which is true in our case).
	//	CPathServer::Process();
	// though we may still want this to be set:
	CPathServer::m_bGameRunning = true;

	CScene::TidyReferences();
}
