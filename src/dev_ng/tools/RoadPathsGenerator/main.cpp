#define LARGE_BUDDY_HEAP

#if __WIN32PC

#define SIMPLE_HEAP_SIZE		(200*1024)
#define SIMPLE_PHYSICAL_SIZE	(352*1024)

#else

#error "Not supported for this platform.")

#endif	// __WIN32PC

#include "fwnavgen/config.h"
#include "parser/manager.h"
#include "system/FileMgr.h"
#include "system/Param.h"
#include "app.h"
#include "exporter.h"

#define PGKNOWNREFPOOLSIZE 65536	// Must be a multiple of 32. This overrides the size of pgBaseKnownReferencePool, which seems to be needed for this tool to work.

#include "fwnavgen/main.h"

PARAM(noptfx, "disables particle effects");

namespace rage
{
	XPARAM(dontwarnonmissingmodule);
	//XPARAM(noHangDetectThread);
}

int Main()
{
	PARAM_noptfx.Set("");
	PARAM_dontwarnonmissingmodule.Set("");	// url:bugstar:498557
	//PARAM_noHangDetectThread.Set("");

//JAY	PARAM_buildpaths.Set("1");	// Set buildpaths here, so the loading of the level builds the paths
	fwLevelProcessToolImpl<CLevelProcessToolGameInterfaceGta, CRoadPathsExporterTool> myApp;
	myApp.Run();

#if __WIN32PC

	// Hack to work around various shutdown problems, such as systems that assert or crash
	// just from static objects being created and then destroyed again.
	ExitProcess(0);

#else

	return 0;

#endif

}

