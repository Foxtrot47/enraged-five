#include "exporter.h"
#include "config_parser.h"

// Rage headers
#include "diag/output.h"
#include "system/param.h"
#include "system/xtl.h"		// For MEMORYSTATUS, etc.

// Framework headers
#include "fwscene/stores/boxstreamer.h"
#include "fwscene/stores/mapdatastore.h"
#include "fwscene/stores/staticboundsstore.h"
#include "fwscene/world/WorldLimits.h"
#include "spatialdata\sphere.h"

// Game headers
#include "camera/viewports/ViewportManager.h"
#include "Objects/DummyObject.h"
#include "Objects/Object.h"
#include "Objects/ObjectPopulation.h"
#include "PathServer/PathServer.h"
#include "pathserver/ExportCollision.h"
#include "Physics/Physics.h"
#include "Renderer/PlantsMgr.h"
#include "Renderer/Water.h"
#include "scene/scene.h"
#include "scene/entities/compEntity.h"
#include "scene/FocusEntity.h"
#include "scene/portals/Portal.h"
#include "scene/streamer/streamvolume.h"
#include "scene/world/GameWorld.h"
#include "script/script.h"			// CTheScripts
#include "streaming/streaming.h"
#include "Task/Movement/Climbing/TaskGoToAndClimbLadder.h"
#include "vehicleAI/PathFind_Build.h"
#include "control/gamelogic.h"

//-----------------------------------------------------------------

PARAM(exportiplgroups, "Path and file name for an XML file containing a list of IPL groups to load, using a CNavMeshDataDefaultIplGroups object.");
PARAM(pathsIPL, "");

//-----------------------------------------------------------------

XPARAM(nopeds);
XPARAM(nocars);
XPARAM(noambient);
XPARAM(nonetwork);
XPARAM(level);

#if __WIN32PC && __BANK
XPARAM(hidewidgets);			// Currently defined in 'bank/pane.cpp'.
#endif

namespace rage
{
	XPARAM(noaudio);
	XPARAM(noaudiothread);
	XPARAM(nowaveslots);

#if __WIN32PC && !__FINAL
	XPARAM(hidewindow);			// Currently defined in 'grcore/device_win32.cpp'.
#endif
}	// namespace rage

CRoadPathsExporterTool::CRoadPathsExporterTool(fwLevelProcessTool * pTool) : fwLevelProcessToolExporterInterface(pTool)
{
	m_DefaultIplGroups = NULL;
}

CRoadPathsExporterTool::~CRoadPathsExporterTool()
{
	delete m_DefaultIplGroups;
	m_DefaultIplGroups = NULL;
}

void CRoadPathsExporterTool::PreInit()
{
	// This is needed for now to prevent the pools from becoming too small when the values
	// from the configuration file override the larger values in code. We may want to
	// solve this in a different way, perhaps either loading a secondary configuration
	// file containing the larger pool values, or adding a conditional section within
	// the single configuration file which is somehow only used from this tool.
	fwConfigManager::SetUseLargerPoolFromCodeOrData(true);

	//hack: the physics system uses this var to determine
	//if it should use larger pools during the export process
	CNavMeshDataExporter::SetExportMode(CNavMeshDataExporter::eWaitingToStartExport);

	PARAM_noaudio.Set("");
	PARAM_noaudiothread.Set("");
	PARAM_nowaveslots.Set("");
	PARAM_nopeds.Set("");
	PARAM_nocars.Set("");
	PARAM_noambient.Set("");
	PARAM_nonetwork.Set("");

#if __WIN32PC && !__FINAL
	PARAM_hidewindow.Set("");
#endif

#if __WIN32PC && __BANK
	PARAM_hidewidgets.Set("");
#endif
}


void CRoadPathsExporterTool::Init()
{
	LoadDefaultIplGroupList();
	RequestDefaultIplGroups();
}

void CRoadPathsExporterTool::Shutdown()
{

}

bool CRoadPathsExporterTool::UpdateExport()
{
	//this needs to be called before every request
	RequestDefaultIplGroups();

	CStreaming::LoadAllRequestedObjects();

	if(CStreaming::GetNumberObjectsRequested() <= 0)
	{
		bool	error = true;

		// Everything should be loaded by this time.
		CPathFindBuild *pPathBuild = rage_new CPathFindBuild();

		// Get the level
		const char *pLevelName = NULL;
		PARAM_level.Get(pLevelName);
		printf("Level Name == %s\n", pLevelName);
		if(pLevelName)
		{
			char	levelPathIPLFile[256];

			const char *iplLoc = NULL;
			PARAM_pathsIPL.Get(iplLoc);
			if(iplLoc)
			{
				sprintf(levelPathIPLFile, "%s", iplLoc);
			}
			else
			{
				sprintf(levelPathIPLFile, "common:/data/levels/%s/paths.IPL", pLevelName);
			}

			// Read in the stuff from the IPL
			if(pPathBuild->ReadPathInfoFromIPL(levelPathIPLFile))
			{
				// Make the data
				pPathBuild->PreparePathData();
				error = false;			
			}

		}

		delete pPathBuild;

		// Exit the process and return any errors
		ExitProcess((int)error);
	}
	return false;
}

void CRoadPathsExporterTool::LoadDefaultIplGroupList()
{
	// Clear out any old data, if this is not the first time it's called.
	delete m_DefaultIplGroups;
	m_DefaultIplGroups = NULL;

	const char* filename = NULL;
	if(PARAM_exportiplgroups.Get(filename))
	{
		if(PARSER.LoadObjectPtr(filename, "xml", m_DefaultIplGroups))
		{
			Displayf("List of IPL groups to load successfully loaded from '%s'.", filename);
		}
		else
		{
			Errorf("List of IPL groups to couldn't be loaded from '%s'.", filename);
		}
	}
	else
	{
		Displayf("No list of IPL groups to load was specified (-exportiplgroups).");
	}
}


void CRoadPathsExporterTool::RequestDefaultIplGroups()
{
	if(!m_DefaultIplGroups)
	{
		return;
	}

	for(int c = 0; c < m_DefaultIplGroups->m_DefaultIplGroups.GetCount(); c++)
	{
		const char* pName = m_DefaultIplGroups->m_DefaultIplGroups[c];
		strLocalIndex index = INSTANCE_STORE.FindSlot(pName);
		if(index != -1)
		{
			INSTANCE_STORE.RequestGroup(index, atStringHash(pName), true);
		}
	}
}