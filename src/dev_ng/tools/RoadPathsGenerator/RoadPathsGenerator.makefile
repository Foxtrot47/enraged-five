Project roadpathsgenerator

ConfigurationType exe
TitleId 0x54540840
AdditionalSections 54540840=..\..\..\..\xlast\Fuzzy.spa

RootDirectory .

IncludePath $(RAGE_DIR)\framework\tools\src\cli\
IncludePath $(RAGE_DIR)\base\src
IncludePath $(RAGE_DIR)\suite\src
IncludePath $(RAGE_DIR)\script\src
IncludePath $(RAGE_DIR)\framework\src
IncludePath $(RAGE_DIR)\3rdParty
IncludePath ..\..\game

ForceInclude ..\..\game\basetypes.h
ForceInclude ..\..\game\game_config.h

Files {
	main.cpp
	app.h
	app.cpp
	exporter.h
	exporter.cpp
	ToolScene.h
	ToolScene.cpp
}

Libraries {

	..\..\game\VS_Project1_lib\game1_lib
	..\..\game\VS_Project2_lib\game2_lib
	..\..\game\VS_Project3_lib\game3_lib
	..\..\game\VS_Project4_lib\game4_lib
	..\..\game\VS_Project_network\network	
	..\..\game\VS_Project\RageMisc\RageMisc
	
	%RAGE_DIR%\base\src\vcproj\RageAudio\RageAudio
	%RAGE_DIR%\base\src\vcproj\RageCore\RageCore
	%RAGE_DIR%\base\src\vcproj\RageCreature\RageCreature
	%RAGE_DIR%\base\src\vcproj\RageGraphics\RageGraphics
	%RAGE_DIR%\base\src\vcproj\RageNet\RageNet
	%RAGE_DIR%\base\src\vcproj\RagePhysics\RagePhysics

	%RAGE_DIR%\suite\src\vcproj\RageSuiteCreature\RageSuiteCreature
	%RAGE_DIR%\naturalmotion\vcproj\NaturalMotion\NaturalMotion
	%RAGE_DIR%\script\src\vcproj\RageScript\RageScript
	%RAGE_DIR%\framework\src\vcproj\RageFramework\RageFramework
	%RAGE_DIR%\framework\tools\src\cli\fwnavgen\fwnavgen
	%RAGE_DIR%\scaleform\src\vcproj\ScaleformGfx\ScaleformGfx
	
	# Please note PS� Unity projects are not listed here, since these are conditional they are specified in the rules files.
}