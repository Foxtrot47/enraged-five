#ifndef TOOL_SCENE_H
#define TOOL_SCENE_H

class CToolScene
{
public:
	static void		Init					(unsigned initMode);
	//static void		Shutdown				(unsigned shutdownMode);
	static void		Update					();
};

#endif	//TOOL_SCENE_H
