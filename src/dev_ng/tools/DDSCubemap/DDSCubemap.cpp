// ==============
// DDSCubemap.cpp
// ==============
// single 4x3 2D image -> cubemap
// cubemap dds -> 4x3 2D image
// cubemap array dds -> multiple 4x3 2D images

#include "../common/common.h"
#include "../common/dds.h"
#include "../common/fileutil.h"
#include "../common/imagedxt.h"
#include "../common/imageutil.h"
#include "../common/stringutil.h"
#include "../common/vector.h"
#include <signal.h>

CRITICAL_SECTION g_critSec;
bool g_ctrlC = false;

static void signalHandler(int)
{
	g_ctrlC = true;
}

static const char* GetLayerMipString(int layerIndex, int arrayCount, int mipIndex, int mipCount)
{
	static char str[64];
	str[0] = '\0';
	if (arrayCount > 1) { strcat(str, varString("[layer=%d]", layerIndex).c_str()); }
	if (mipCount > 1) { strcat(str, varString("[mip=%d]", mipIndex).c_str()); }
	return str;
}

template <typename T> static bool IsTypeVec4()       { return false; }
template <>           static bool IsTypeVec4<Vec4>() { return true; }

template <typename T> static void SaveImageInAllRelevantFormats(const char* path, const T* image, int w, int h, DXGI_FORMAT sourceFormat)
{
	// save png or dds
	{
		char path2[512] = "";
		strcpy(path2, path);
		strcpy(strrchr(path2, '.'), sourceFormat == DXGI_FORMAT_B8G8R8A8_UNORM ? ".png" : ".dds");

		if (SaveImage(path2, image, w, h, false, true))
		{
			EnterCriticalSection(&g_critSec);
			printf("%s saved ok.\n", path2);
			LeaveCriticalSection(&g_critSec);
		}
		else
		{
			EnterCriticalSection(&g_critSec);
			printf("Failed to save %s!\n", path2);
			LeaveCriticalSection(&g_critSec);
		}
	}

	if (sourceFormat == DXGI_FORMAT_R32G32B32A32_FLOAT || // save exr
		sourceFormat == DXGI_FORMAT_R16G16B16A16_FLOAT)
	{
		if (IsTypeVec4<T>())
		{
			char path2[512] = "";
			strcpy(path2, path);
			strcpy(strrchr(path2, '.'), ".exr");

			if (SaveImage(path2, image, w, h, false, true))
			{
				EnterCriticalSection(&g_critSec);
				printf("%s saved ok.\n", path2);
				LeaveCriticalSection(&g_critSec);
			}
			else
			{
				EnterCriticalSection(&g_critSec);
				printf("Failed to save %s!\n", path2);
				LeaveCriticalSection(&g_critSec);
			}
		}
	}
}

static void CopyCubemapFaceSetup(int& ox, int& oy, int& dxx, int& dxy, int& dyx, int& dyy, int faceSize, int faceIndex)
{
	//         +-------+
	//         |face:5 |
	//         |dir=-z |
	//         |rot=0  |
	// +-------+-------+-------+-------+
	// |face:0 |face:3 |face:1 |face:2 |
	// |dir=+x |dir=-y |dir=-x |dir=+y |
	// |rot=270|rot=180|rot=90 |rot=0  |
	// +-------+-------+-------+-------+
	//         |face:4 |
	//         |dir=+z |
	//         |rot=180|
	//         +-------+

	switch (faceIndex)
	{
	case 0: ox = 0*faceSize,     oy = 2*faceSize - 1, dxx = +0, dxy = -1, dyx = +1, dyy = +0; return;
	case 1: ox = 3*faceSize - 1, oy = 1*faceSize,     dxx =  0, dxy = +1, dyx = -1, dyy =  0; return;
	case 2: ox = 3*faceSize,     oy = 1*faceSize,     dxx = +1, dxy =  0, dyx =  0, dyy = +1; return;
	case 3: ox = 2*faceSize - 1, oy = 2*faceSize - 1, dxx = -1, dxy =  0, dyx =  0, dyy = -1; return;
	case 4: ox = 2*faceSize - 1, oy = 3*faceSize - 1, dxx = -1, dxy =  0, dyx =  0, dyy = -1; return;
	case 5: ox = 1*faceSize,     oy = 0*faceSize,     dxx = +1, dxy =  0, dyx =  0, dyy = +1; return;
	}

	assert(false);
}

template <typename T> static void CopyFromCubemapFaceTo4x3Cross(T* crossImage, const T* faceImage, int faceSize, int faceIndex)
{
	const int w = faceSize*4;
	int ox, oy, dxx, dxy, dyx, dyy;
	CopyCubemapFaceSetup(ox, oy, dxx, dxy, dyx, dyy, faceSize, faceIndex);

	for (int y = 0; y < faceSize; y++)
	{
		for (int x = 0; x < faceSize; x++)
		{
			const int xx = ox + x*dxx + y*dyx;
			const int yy = oy + x*dxy + y*dyy;

			crossImage[xx + yy*w] = faceImage[x + y*faceSize];
		}
	}
}

template <typename T> static void CopyToCubemapFaceFrom4x3Cross(const T* crossImage, T* faceImage, int faceSize, int faceIndex)
{
	const int w = faceSize*4;
	int ox, oy, dxx, dxy, dyx, dyy;
	CopyCubemapFaceSetup(ox, oy, dxx, dxy, dyx, dyy, faceSize, faceIndex);

	for (int y = 0; y < faceSize; y++)
	{
		for (int x = 0; x < faceSize; x++)
		{
			const int xx = ox + x*dxx + y*dyx;
			const int yy = oy + x*dxy + y*dyy;

			faceImage[x + y*faceSize] = crossImage[xx + yy*w];
		}
	}
}

// http://guru.multimedia.cx/avoiding-branchesifconditionals
static __forceinline u32 GetNonZeroMask(u32 x) // returns (x ? 0xffffffff : 0) without branching
{
	return (u32)( ( (s32)(x) | -(s32)(x) ) >> 31 );
}

static float GetFloat32_FromFloat16_NoIntrinsics(u16 data) // [seeeeemm.mmmmmmmm] -> [seeeeeee.emmmmmmm.mmm00000.00000000]
{
	u32 e,z,s;

	e = (u32)data << 16;   // [seeeeemm.mmmmmmmm.00000000.00000000]
	s = 0x80000000u & e;   // [s0000000.00000000.00000000.00000000]
	e = 0x7fff0000u & e;   // [0eeeeemm.mmmmmmmm.00000000.00000000]
	z = GetNonZeroMask(e); // all 1's if e!=0
	z = 0x38000000u & z;
	e = ((e >> 3) + z)|s;

	return *(const float*)&e; // [LHS]
}

static void SaveDDSCubemapFaceFrom4x3Cross(const char* pathBase, const char* pathSuffix, const u8* crossImage, int mw, int faceIndex, DXGI_FORMAT dxgiFormat)
{
	const int bpp = GetBitsPerPixel(dxgiFormat);
	const char* faceNames[] = {"LF", "RT", "FR", "BK", "DN", "UP"}; // these correspond to cubemap face names in max
	u8* faceImage = new u8[mw*mw*bpp/8];

	for (int j = 0; j < mw; j++) // re-copy face directly from cross (no rotation)
	{
		const int facePos[6][2] = {{0,1},{2,1},{3,1},{1,1},{1,2},{1,0}};
		const int sx = mw*facePos[faceIndex][0];
		const int sy = mw*facePos[faceIndex][1];

		memcpy(faceImage + j*mw*bpp/8, crossImage + (sx + (sy + j)*mw*4)*bpp/8, mw*bpp/8);
	}

	// TODO -- use SaveImageInAllRelevantFormats
	if (dxgiFormat == DXGI_FORMAT_R32G32B32A32_FLOAT)
	{
		// save dds
		{
			char path[512] = "";
			strcpy(path, pathBase);
			strcpy(strrchr(path, '.'), varString("_face_%s%s.dds", faceNames[faceIndex], pathSuffix).c_str());

			if (SaveImage(path, (const Vec4*)faceImage, mw, mw, false, true))
			{
				EnterCriticalSection(&g_critSec);
				printf("%s saved ok.\n", path);
				LeaveCriticalSection(&g_critSec);
			}
			else
			{
				EnterCriticalSection(&g_critSec);
				printf("Failed to save %s!\n", path);
				LeaveCriticalSection(&g_critSec);
			}
		}

		// save exr
		{
			char path[512] = "";
			strcpy(path, pathBase);
			strcpy(strrchr(path, '.'), varString("_face_%s%s.exr", faceNames[faceIndex], pathSuffix).c_str());

			if (SaveImage(path, (const Vec4*)faceImage, mw, mw, false, true))
			{
				EnterCriticalSection(&g_critSec);
				printf("%s saved ok.\n", path);
				LeaveCriticalSection(&g_critSec);
			}
			else
			{
				EnterCriticalSection(&g_critSec);
				printf("Failed to save %s!\n", path);
				LeaveCriticalSection(&g_critSec);
			}
		}
	}
	else // some other format - save DDS
	{
		char path[512] = "";
		strcpy(path, pathBase);
		strcpy(strrchr(path, '.'), varString("_face_%s%s.dds", faceNames[faceIndex], pathSuffix).c_str());
		FILE* dds2 = fopen(path, "wb");

		if (dds2)
		{
			const int faceSizeInBytes = mw*mw*bpp/8;

			WriteDDSHeader(dds2, DDS_IMAGE_TYPE_2D, mw, mw, 1, 1, 1, DDPIXELFORMAT(0), dxgiFormat);
			fwrite(faceImage, faceSizeInBytes, 1, dds2);
			fclose(dds2);

			EnterCriticalSection(&g_critSec);
			printf("%s saved ok.\n", path);
			LeaveCriticalSection(&g_critSec);
		}
		else
		{
			EnterCriticalSection(&g_critSec);
			printf("Failed to save %s!\n", path);
			LeaveCriticalSection(&g_critSec);
		}
	}

	delete[] faceImage;
}

static bool ConvertCubemapTo4x3Cross(const char* path, bool bSaveAsDDS = false)
{
	FILE* dds = fopen(path, "rb");

	if (dds)
	{
		fseek(dds, sizeof(u32), SEEK_SET); // skip magic
		DDSURFACEDESC2 header;
		fread(&header, sizeof(header), 1, dds);

		DXGI_FORMAT dxgiFormat = DXGI_FORMAT_UNKNOWN;
		int arrayCount = 0;

		DDS_HEADER_DXT10 dx10header;

		if (header.ddpfPixelFormat.dwFourCC == MAKE_MAGIC_NUMBER('D','X','1','0'))
		{
			fread(&dx10header, sizeof(dx10header), 1, dds);
			dxgiFormat = (DXGI_FORMAT)dx10header.dxgiFormat;
			arrayCount = (int)dx10header.arraySize;
		}
		else
		{
			dxgiFormat = GetDX10Format(header.ddpfPixelFormat);
			arrayCount = 1;
		}

		if (IsDX10FormatCompressedOrPacked(dxgiFormat))
		{
			EnterCriticalSection(&g_critSec);
			printf("compressed format %d not supported (requires rotation)\n", dxgiFormat);
			LeaveCriticalSection(&g_critSec);

			system("pause");
			exit(-1);
		}

		const int pos = ftell(dds);
		const int bpp = GetBitsPerPixel(dxgiFormat);
		const int w = (int)header.dwWidth;
		const int mipCount = Max<int>(1, header.dwMipMapCount);

		const int arrayStride = GetImageSizeInBytes(w, w, 1, mipCount, dxgiFormat, DDS_IMAGE_TYPE_CUBE, 1);
		const int faceStride = GetImageSizeInBytes(w, w, 1, mipCount, dxgiFormat, DDS_IMAGE_TYPE_2D, 1);

		// TODO -- use SaveImageInAllRelevantFormats
		if (dxgiFormat == DXGI_FORMAT_B8G8R8A8_UNORM && !bSaveAsDDS) // save each mip as a PNG image
		{
			Pixel32* temp = new Pixel32[w*w];

			for (int layerIndex = 0; layerIndex < arrayCount; layerIndex++)
			{
				for (int mipIndex = 0; mipIndex < mipCount; mipIndex++)
				{
					const int mw = Max<int>(1, w>>mipIndex); // src mip size
					const int iw = mw*4; // dst image size
					const int ih = mw*3;

					Pixel32* crossImage = new Pixel32[iw*ih];
					memset(crossImage, 0, iw*ih*sizeof(Pixel32));

					for (int faceIndex = 0; faceIndex < 6; faceIndex++)
					{
						const int mipOffset = GetImageSizeInBytes(w, w, 1, mipIndex, dxgiFormat, DDS_IMAGE_TYPE_2D, 1); // offset to mip within face
						fseek(dds, pos + arrayStride*layerIndex + faceStride*faceIndex + mipOffset, SEEK_SET);
						fread(temp, sizeof(Pixel32), mw*mw, dds);
						CopyFromCubemapFaceTo4x3Cross(crossImage, temp, mw, faceIndex);

						SaveDDSCubemapFaceFrom4x3Cross(path, GetLayerMipString(layerIndex, arrayCount, mipIndex, mipCount), (const u8*)crossImage, mw, faceIndex, dxgiFormat);
					}

					for (int i = 0; i < iw*ih; i++)
					{
						crossImage[i].a = 255;
					}

					char path2[512] = "";
					strcpy(path2, path);
					strcpy(strrchr(path2, '.'), varString("%s_4x3cross.png", GetLayerMipString(layerIndex, arrayCount, mipIndex, mipCount)).c_str());

					if (SaveImage(path2, crossImage, iw, ih))
					{
						EnterCriticalSection(&g_critSec);
						printf("%s saved ok.\n", path2);
						LeaveCriticalSection(&g_critSec);
					}
					else
					{
						EnterCriticalSection(&g_critSec);
						printf("Failed to save %s!\n", path2);
						LeaveCriticalSection(&g_critSec);
					}

					delete[] crossImage;
				}
			}

			delete[] temp;
		}
		else // some other format - just write out a dds for each layer using the same format (all mips included)
		{
			u8* temp = new u8[w*w*bpp/8];

			for (int layerIndex = 0; layerIndex < arrayCount; layerIndex++)
			{
				char path2[512] = "";
				strcpy(path2, path);
				strcpy(strrchr(path2, '.'), varString("%s_4x3cross.dds", GetLayerMipString(layerIndex, arrayCount, 0, 0)).c_str());

				FILE* dds2 = fopen(path2, "wb");

				if (dds2)
				{
					EnterCriticalSection(&g_critSec);
					printf("%s saved ok.\n", path2);
					LeaveCriticalSection(&g_critSec);

					WriteDDSHeader(dds2, DDS_IMAGE_TYPE_2D, header.dwWidth*4, header.dwHeight*3, 1, header.dwMipMapCount, 1, header.ddpfPixelFormat);

					for (int mipIndex = 0; mipIndex < mipCount; mipIndex++)
					{
						const int mw = Max<int>(1, w>>mipIndex); // src mip size
						const int iw = mw*4; // dst image size
						const int ih = mw*3;

						u8* crossImage = new u8[iw*ih*bpp/8];
						memset(crossImage, 0, iw*ih*bpp/8);

						for (int faceIndex = 0; faceIndex < 6; faceIndex++)
						{
							const int mipOffset = GetImageSizeInBytes(w, w, 1, mipIndex, dxgiFormat, DDS_IMAGE_TYPE_2D, 1); // offset to mip within face
							fseek(dds, pos + arrayStride*layerIndex + faceStride*faceIndex + mipOffset, SEEK_SET);
							fread(temp, mw*mw*bpp/8, 1, dds);

							switch (bpp)
							{
							case   8: CopyFromCubemapFaceTo4x3Cross((u8   *)crossImage, (const u8   *)temp, mw, faceIndex); break;
							case  16: CopyFromCubemapFaceTo4x3Cross((u16  *)crossImage, (const u16  *)temp, mw, faceIndex); break;
							case  32: CopyFromCubemapFaceTo4x3Cross((u32  *)crossImage, (const u32  *)temp, mw, faceIndex); break;
							case  64: CopyFromCubemapFaceTo4x3Cross((u64  *)crossImage, (const u64  *)temp, mw, faceIndex); break;
							case 128: CopyFromCubemapFaceTo4x3Cross((Vec4V*)crossImage, (const Vec4V*)temp, mw, faceIndex); break;
							}

							SaveDDSCubemapFaceFrom4x3Cross(path, GetLayerMipString(layerIndex, arrayCount, mipIndex, mipCount), (const u8*)crossImage, mw, faceIndex, dxgiFormat);
						}

						fwrite(crossImage, iw*ih*bpp/8, 1, dds2);

						if (mipIndex == 0) // save exr
						{
							Vec4* exr = NULL;

							if (dxgiFormat == DXGI_FORMAT_R32G32B32A32_FLOAT)
							{
								exr = (Vec4*)crossImage;
							}
							else if (dxgiFormat == DXGI_FORMAT_R16G16B16A16_FLOAT)
							{
								exr = new Vec4[iw*ih];

								for (int i = 0; i < iw*ih; i++)
								{
									exr[i].x = GetFloat32_FromFloat16_NoIntrinsics(((const u16*)crossImage)[i*4 + 0]);
									exr[i].y = GetFloat32_FromFloat16_NoIntrinsics(((const u16*)crossImage)[i*4 + 1]);
									exr[i].z = GetFloat32_FromFloat16_NoIntrinsics(((const u16*)crossImage)[i*4 + 2]);
									exr[i].w = 1.0f;//GetFloat32_FromFloat16_NoIntrinsics(((const u16*)crossImage)[i*4 + 3]);
								}
							}

							if (exr)
							{
								char path3[512] = "";
								strcpy(path3, path);
								strcpy(strrchr(path3, '.'), varString("%s_4x3cross.exr", GetLayerMipString(layerIndex, arrayCount, 0, 0)).c_str());

								if (SaveImage(path3, exr, iw, ih, false, true))
								{
									EnterCriticalSection(&g_critSec);
									printf("%s saved ok.\n", path3);
									LeaveCriticalSection(&g_critSec);
								}
								else
								{
									EnterCriticalSection(&g_critSec);
									printf("Failed to save %s!\n", path3);
									LeaveCriticalSection(&g_critSec);
								}

								if (exr != (const void*)crossImage)
								{
									delete[] exr;
								}
							}
						}

						delete[] crossImage;
					}

					fclose(dds2);
				}
				else
				{
					EnterCriticalSection(&g_critSec);
					printf("Failed to save %s!\n", path2);
					LeaveCriticalSection(&g_critSec);
				}
			}

			delete[] temp;
		}

		fclose(dds);
		return true;
	}

	return false;
}

static int GetMaxMipCountForProjection(int w, int h, int mips, int ratio_w, int ratio_h)
{
	for (int i = 0; i < mips; i++)
	{
		const int mw = Max<int>(1, w>>i);
		const int mh = Max<int>(1, h>>i);

		if (mh*ratio_w != mw*ratio_h)
		{
			return i; // this mip is not valid, but the last one was
		}
	}

	return mips;
}

static void SaveDDSCubemapFrom4x3Cross(const char* path, const std::vector<void*>& crossImageMips, int w, int h, DXGI_FORMAT dxgiFormat, int ratio_w, int ratio_h)
{
	if (w*3 != h*4)
	{
		EnterCriticalSection(&g_critSec);
		printf("image is %dx%d, expected 4x3 cross\n", w, h);
		LeaveCriticalSection(&g_critSec);

		system("pause");
		exit(-1);
	}

	FILE* dds = fopen(path, "wb");

	if (dds)
	{
		EnterCriticalSection(&g_critSec);
		printf("%s saved ok.\n", path);
		LeaveCriticalSection(&g_critSec);

		const int cubeSize = w/4;
		const int bpp = GetBitsPerPixel(dxgiFormat);
		const int mipCount = GetMaxMipCountForProjection(w, h, (int)crossImageMips.size(), ratio_w, ratio_h);

		WriteDDSHeader(dds, DDS_IMAGE_TYPE_CUBE, cubeSize, cubeSize, 1, mipCount, 1, DDPIXELFORMAT(0), dxgiFormat);

		for (int mipIndex = 0; mipIndex < mipCount; mipIndex++)
		{
			const u8* crossImage = (const u8*)crossImageMips[mipIndex];
			const int mw = cubeSize>>mipIndex;

			const int faceSizeInBytes = mw*mw*bpp/8;
			u8* faceImage = new u8[faceSizeInBytes];

			for (int faceIndex = 0; faceIndex < 6; faceIndex++)
			{
				switch (bpp)
				{
				case   8: CopyToCubemapFaceFrom4x3Cross((const u8   *)crossImage, (u8   *)faceImage, mw, faceIndex); break;
				case  16: CopyToCubemapFaceFrom4x3Cross((const u16  *)crossImage, (u16  *)faceImage, mw, faceIndex); break;
				case  32: CopyToCubemapFaceFrom4x3Cross((const u32  *)crossImage, (u32  *)faceImage, mw, faceIndex); break;
				case  64: CopyToCubemapFaceFrom4x3Cross((const u64  *)crossImage, (u64  *)faceImage, mw, faceIndex); break;
				case 128: CopyToCubemapFaceFrom4x3Cross((const Vec4V*)crossImage, (Vec4V*)faceImage, mw, faceIndex); break;
				}

				fwrite(faceImage, faceSizeInBytes, 1, dds);

				SaveDDSCubemapFaceFrom4x3Cross(path, GetLayerMipString(0, 0, mipIndex, mipCount), (const u8*)crossImage, mw, faceIndex, dxgiFormat);
			}

			delete[] faceImage;
		}

		fclose(dds);
	}
	else
	{
		EnterCriticalSection(&g_critSec);
		printf("Failed to save %s!\n", path);
		LeaveCriticalSection(&g_critSec);
	}
}

static bool Convert4x3CrossToCubemap(const char* path, const char* ext, int ratio_w, int ratio_h)
{
	if (_stricmp(ext, ".dds") == 0)
	{
		FILE* dds = fopen(path, "rb");

		if (dds)
		{
			EnterCriticalSection(&g_critSec);
			printf("%s saved ok.\n", path);
			LeaveCriticalSection(&g_critSec);

			fseek(dds, sizeof(u32), SEEK_SET); // skip magic

			DDSURFACEDESC2 header;
			fread(&header, sizeof(header), 1, dds);

			DXGI_FORMAT dxgiFormat = DXGI_FORMAT_UNKNOWN;
			int arrayCount = 0;

			DDS_HEADER_DXT10 dx10header;

			if (header.ddpfPixelFormat.dwFourCC == MAKE_MAGIC_NUMBER('D','X','1','0'))
			{
				fread(&dx10header, sizeof(dx10header), 1, dds);
				dxgiFormat = (DXGI_FORMAT)dx10header.dxgiFormat;
				arrayCount = (int)dx10header.arraySize;
			}
			else
			{
				dxgiFormat = GetDX10Format(header.ddpfPixelFormat);
				arrayCount = 1;
			}

			if (IsDX10FormatCompressedOrPacked(dxgiFormat))
			{
				EnterCriticalSection(&g_critSec);
				printf("compressed format %d not supported (requires rotation)\n", dxgiFormat);
				LeaveCriticalSection(&g_critSec);
				system("pause");
				exit(-1);
			}

			const int pos = ftell(dds);
			const int bpp = GetBitsPerPixel(dxgiFormat);
			const int w = (int)header.dwWidth;
			const int h = (int)header.dwHeight;
			const int mips = Max<int>(1, header.dwMipMapCount);

			const int cubeSize = w/ratio_w;
			const int cubeMips = GetMaxMipCountForProjection(w, h, mips, ratio_w, ratio_h);

			const int arrayStride = GetImageSizeInBytes(w, h, 1, mips, dxgiFormat, DDS_IMAGE_TYPE_2D, 1);

			for (int layerIndex = 0; layerIndex < arrayCount; layerIndex++)
			{
				std::vector<void*> crossImageMips;

				for (int mipIndex = 0; mipIndex < cubeMips; mipIndex++)
				{
					const int iw = Max<int>(1, w>>mipIndex);
					const int ih = Max<int>(1, h>>mipIndex);

					const int crossSizeInBytes = iw*ih*bpp/8;
					u8* crossImage = new u8[crossSizeInBytes];
					
					const int mipOffset = GetImageSizeInBytes(w, h, 1, mipIndex, dxgiFormat, DDS_IMAGE_TYPE_2D, 1); // offset to mip
					fseek(dds, pos + arrayStride*layerIndex + mipOffset, SEEK_SET);
					fread(crossImage, crossSizeInBytes, 1, dds);

					crossImageMips.push_back(crossImage);
				}

				char path2[512] = "";
				strcpy(path2, path);
				strcpy(strrchr(path2, '.'), varString("_cubemap%s.dds", GetLayerMipString(layerIndex, arrayCount, 0, 0)).c_str());

				SaveDDSCubemapFrom4x3Cross(path2, crossImageMips, w, h, dxgiFormat, ratio_w, ratio_h);

				for (int mipIndex = 0; mipIndex < cubeMips; mipIndex++)
				{
					delete[] crossImageMips[mipIndex];
				}
			}

			fclose(dds);
			return true;
		}
		else
		{
			EnterCriticalSection(&g_critSec);
			printf("Failed to save %s!\n", path);
			LeaveCriticalSection(&g_critSec);
		}
	}
	else if (_stricmp(ext, ".exr") == 0) // load exr as 32-bit float RGBA
	{
		int w = 0;
		int h = 0;
		Vec4* image = LoadImage_Vec4(path, w, h);

		if (image)
		{
			std::vector<void*> crossImageMips;
			crossImageMips.push_back(image);

			char path2[512] = "";
			strcpy(path2, path);
			strcpy(strrchr(path2, '.'), "_cubemap.dds");

			SaveDDSCubemapFrom4x3Cross(path2, crossImageMips, w, h, DXGI_FORMAT_R32G32B32A32_FLOAT, ratio_w, ratio_h);

			delete[] image;
			return true;
		}
	}
	else // some other image type, load as 8888
	{
		int w = 0;
		int h = 0;
		Pixel32* image = LoadImage_Pixel32(path, w, h);

		if (image)
		{
			std::vector<void*> crossImageMips;
			crossImageMips.push_back(image);

			char path2[512] = "";
			strcpy(path2, path);
			strcpy(strrchr(path2, '.'), "_cubemap.dds");

			SaveDDSCubemapFrom4x3Cross(path2, crossImageMips, w, h, DXGI_FORMAT_B8G8R8A8_UNORM, ratio_w, ratio_h);

			delete[] image;
			return true;
		}
	}

	return false;
}

static Vec3V_Out GetCubeDir(int faceIndex, int i, int j, int w)
{
	const float u = -1.0f + 2.0f*((float)i + 0.5f)/(float)w; // [-1..1]
	const float v = -1.0f + 2.0f*((float)j + 0.5f)/(float)w; // [-1..1]

	switch (faceIndex)
	{
	case 0: return Vec3V(+1.0f,-v,-u); // +X
	case 1: return Vec3V(-1.0f,-v,+u); // -X
	case 2: return Vec3V(+u,+1.0f,+v); // +Y
	case 3: return Vec3V(+u,-1.0f,-v); // -Y
	case 4: return Vec3V(+u,-v,+1.0f); // +Z
	case 5: return Vec3V(-u,-v,-1.0f); // -Z
	}

	return Vec3V(0,0,0);
}

static Vec2 GetCubeFaceUV(int& faceIndex, Vec3V_In dir) // returns uv in [-1..1]
{
	const float a = Max<float>(Abs<float>(dir.x), Abs<float>(dir.y), Abs<float>(dir.z));

	if      (a == +dir.x) { faceIndex = 0; return Vec2(-dir.z/dir.x, -dir.y/dir.x); }
	else if (a == -dir.x) { faceIndex = 1; return Vec2(-dir.z/dir.x, +dir.y/dir.x); }
	else if (a == +dir.y) { faceIndex = 2; return Vec2(+dir.x/dir.y, +dir.z/dir.y); }
	else if (a == -dir.y) { faceIndex = 3; return Vec2(-dir.x/dir.y, +dir.z/dir.y); }
	else if (a == +dir.z) { faceIndex = 4; return Vec2(+dir.x/dir.z, -dir.y/dir.z); }
	else if (a == -dir.z) { faceIndex = 5; return Vec2(+dir.x/dir.z, +dir.y/dir.z); }
	else                  { faceIndex = 0; return Vec2(0.0f, 0.0f); }
}

/*static std::vector<Vec4*> LoadCubemapFaces(const char* path, int layerIndex, int mipIndex, int& mw)
{
	std::vector<Vec4*> faces;

	int w = 0;
	int h = 0;
	int mipCount = 0;
	DXGI_FORMAT dxgiFormat = DXGI_FORMAT_UNKNOWN;
	DDS_IMAGE_TYPE type = DDS_IMAGE_TYPE_UNKNOWN;
	int layerCount = 0;

	if (GetDDSInfo(path, w, h, &mipCount, &dxgiFormat, &type, &layerCount))
	{
		if (type == DDS_IMAGE_TYPE_CUBE && layerIndex < layerCount && mipIndex < mipCount)
		{
			if (dxgiFormat == DXGI_FORMAT_R32G32B32A32_FLOAT || // TODO -- support other formats if we need them
				dxgiFormat == DXGI_FORMAT_R16G16B16A16_FLOAT ||
				dxgiFormat == DXGI_FORMAT_B8G8R8A8_UNORM)
			{
				FILE* dds = fopen(path, "rb");

				if (dds)
				{
					fseek(dds, sizeof(u32), SEEK_SET); // skip magic
					DDSURFACEDESC2 header;
					fread(&header, sizeof(header), 1, dds);

					if (header.ddpfPixelFormat.dwFourCC == MAKE_MAGIC_NUMBER('D','X','1','0'))
					{
						fseek(dds, sizeof(DDS_HEADER_DXT10), SEEK_CUR); // skip dx10 header
					}

					const int bpp = GetBitsPerPixel(dxgiFormat);
					const int pos = ftell(dds);

					const int arrayStride = GetImageSizeInBytes(w, w, 1, mipCount, dxgiFormat, DDS_IMAGE_TYPE_CUBE, 1);
					const int faceStride = GetImageSizeInBytes(w, w, 1, mipCount, dxgiFormat, DDS_IMAGE_TYPE_2D, 1);

					mw = w >> mipIndex;

					u8* temp = new u8[mw*mw*bpp/8];

					for (int faceIndex = 0; faceIndex < 6; faceIndex++)
					{
						Vec4* face = new Vec4[mw*mw];

						const int mipOffset = GetImageSizeInBytes(w, w, 1, mipIndex, dxgiFormat, DDS_IMAGE_TYPE_2D, 1); // offset to mip within face
						fseek(dds, pos + arrayStride*layerIndex + faceStride*faceIndex + mipOffset, SEEK_SET);
						fread(temp, mw*mw*bpp/8, 1, dds);

						if (dxgiFormat == DXGI_FORMAT_R32G32B32A32_FLOAT)
						{
							memcpy(face, temp, mw*mw*sizeof(Vec4));
						}
						else if (dxgiFormat == DXGI_FORMAT_R16G16B16A16_FLOAT)
						{
							for (int i = 0; i < mw*mw*4; i++)
							{
								((float*)face)[i] = GetFloat32_FromFloat16_NoIntrinsics(((const u16*)temp)[i]);
							}
						}
						else if (dxgiFormat == DXGI_FORMAT_B8G8R8A8_UNORM)
						{
							for (int i = 0; i < mw*mw; i++)
							{
								((Vec4*)face)[i] = ConvertPixel<Vec4,Pixel32>(((const Pixel32*)temp)[i]);
							}
						}

						faces.push_back(face);
					}

					delete[] temp;
					fclose(dds);
				}
			}
			else
			{
				assert(0); // format not supported
			}
		}
	}

	return faces;
}

static Vec4 SampleCubemap(const std::vector<Vec4*>& faces, int mw, Vec3V_In dir)
{
	int faceIndex = 0;
	const Vec2 uv = GetCubeFaceUV(faceIndex, dir);

	const int i = (int)Clamp<float>((float)mw*(uv.x + 1.0f)/2.0f, 0.0f, (float)(mw - 1));
	const int j = (int)Clamp<float>((float)mw*(uv.y + 1.0f)/2.0f, 0.0f, (float)(mw - 1));

	return faces[faceIndex][i + j*mw];
}*/

static std::vector<Vec4*> LoadCubemapFacesWithBorder(const char* path, int layerIndex, int mipIndex, int& mw) // faces are (mw+2)x(mw+2) pixels
{
	std::vector<Vec4*> faces;

	int w = 0;
	int h = 0;
	int mipCount = 0;
	DXGI_FORMAT dxgiFormat = DXGI_FORMAT_UNKNOWN;
	DDS_IMAGE_TYPE type = DDS_IMAGE_TYPE_UNKNOWN;
	int layerCount = 0;

	if (GetDDSInfo(path, w, h, &mipCount, &dxgiFormat, &type, &layerCount))
	{
		if (type == DDS_IMAGE_TYPE_CUBE && layerIndex < layerCount && mipIndex < mipCount)
		{
			if (dxgiFormat == DXGI_FORMAT_R32G32B32A32_FLOAT || // TODO -- support other formats if we need them
				dxgiFormat == DXGI_FORMAT_R16G16B16A16_FLOAT ||
				dxgiFormat == DXGI_FORMAT_B8G8R8A8_UNORM)
			{
				FILE* dds = fopen(path, "rb");

				if (dds)
				{
					fseek(dds, sizeof(u32), SEEK_SET); // skip magic
					DDSURFACEDESC2 header;
					fread(&header, sizeof(header), 1, dds);

					if (header.ddpfPixelFormat.dwFourCC == MAKE_MAGIC_NUMBER('D','X','1','0'))
					{
						fseek(dds, sizeof(DDS_HEADER_DXT10), SEEK_CUR); // skip dx10 header
					}

					const int bpp = GetBitsPerPixel(dxgiFormat);
					const int pos = ftell(dds);

					const int arrayStride = GetImageSizeInBytes(w, w, 1, mipCount, dxgiFormat, DDS_IMAGE_TYPE_CUBE, 1);
					const int faceStride = GetImageSizeInBytes(w, w, 1, mipCount, dxgiFormat, DDS_IMAGE_TYPE_2D, 1);

					mw = w >> mipIndex;

					u8* temp = new u8[mw*mw*bpp/8];

					for (int faceIndex = 0; faceIndex < 6; faceIndex++)
					{
						Vec4* face = new Vec4[(mw + 2)*(mw + 2)];
						memset(face, 0, (mw + 2)*(mw + 2)*sizeof(Vec4));

						const int mipOffset = GetImageSizeInBytes(w, w, 1, mipIndex, dxgiFormat, DDS_IMAGE_TYPE_2D, 1); // offset to mip within face
						fseek(dds, pos + arrayStride*layerIndex + faceStride*faceIndex + mipOffset, SEEK_SET);
						fread(temp, mw*mw*bpp/8, 1, dds);

						if (dxgiFormat == DXGI_FORMAT_R32G32B32A32_FLOAT)
						{
							for (int j = 0; j < mw; j++)
							{
								memcpy(&face[1 + (j + 1)*(mw + 2)], temp + j*mw*bpp/8, mw*sizeof(Vec4));
							}
						}
						else if (dxgiFormat == DXGI_FORMAT_R16G16B16A16_FLOAT)
						{
							for (int j = 0; j < mw; j++)
							{
								for (int i = 0; i < mw; i++)
								{
									face[(i + 1) + (j + 1)*(mw + 2)].x = GetFloat32_FromFloat16_NoIntrinsics(((const u16*)temp)[(i + j*mw)*4 + 0]);
									face[(i + 1) + (j + 1)*(mw + 2)].y = GetFloat32_FromFloat16_NoIntrinsics(((const u16*)temp)[(i + j*mw)*4 + 1]);
									face[(i + 1) + (j + 1)*(mw + 2)].z = GetFloat32_FromFloat16_NoIntrinsics(((const u16*)temp)[(i + j*mw)*4 + 2]);
									face[(i + 1) + (j + 1)*(mw + 2)].w = GetFloat32_FromFloat16_NoIntrinsics(((const u16*)temp)[(i + j*mw)*4 + 3]);
								}
							}
						}
						else if (dxgiFormat == DXGI_FORMAT_B8G8R8A8_UNORM)
						{
							for (int j = 0; j < mw; j++)
							{
								for (int i = 0; i < mw; i++)
								{
									face[(i + 1) + (j + 1)*(mw + 2)] = ConvertPixel<Vec4,Pixel32>(((const Pixel32*)temp)[i + j*mw]);
								}
							}
						}

						faces.push_back(face);
					}

					delete[] temp;
					fclose(dds);

					const int M = mw + 1;

					for (int i = 1; i <= mw; i++)
					{
						const int j = M - i;

						faces[0][i + 0*(mw + 2)] = faces[2][(M - 1) + j*(mw + 2)];
						faces[0][i + M*(mw + 2)] = faces[3][(M - 1) + i*(mw + 2)];
						faces[0][0 + i*(mw + 2)] = faces[4][(M - 1) + i*(mw + 2)];
						faces[0][M + i*(mw + 2)] = faces[5][(0 + 1) + i*(mw + 2)];

						faces[1][i + 0*(mw + 2)] = faces[2][(0 + 1) + i*(mw + 2)];
						faces[1][i + M*(mw + 2)] = faces[3][(0 + 1) + j*(mw + 2)];
						faces[1][0 + i*(mw + 2)] = faces[5][(M - 1) + i*(mw + 2)];
						faces[1][M + i*(mw + 2)] = faces[4][(0 + 1) + i*(mw + 2)];

						faces[2][i + 0*(mw + 2)] = faces[5][j + (0 + 1)*(mw + 2)];
						faces[2][i + M*(mw + 2)] = faces[4][i + (0 + 1)*(mw + 2)];
						faces[2][0 + i*(mw + 2)] = faces[1][i + (0 + 1)*(mw + 2)];
						faces[2][M + i*(mw + 2)] = faces[0][j + (0 + 1)*(mw + 2)];

						faces[3][i + 0*(mw + 2)] = faces[4][i + (M - 1)*(mw + 2)];
						faces[3][i + M*(mw + 2)] = faces[5][j + (M - 1)*(mw + 2)];
						faces[3][0 + i*(mw + 2)] = faces[1][j + (M - 1)*(mw + 2)];
						faces[3][M + i*(mw + 2)] = faces[0][i + (M - 1)*(mw + 2)];

						faces[4][i + 0*(mw + 2)] = faces[2][i + (M - 1)*(mw + 2)];
						faces[4][i + M*(mw + 2)] = faces[3][i + (0 + 1)*(mw + 2)];
						faces[4][0 + i*(mw + 2)] = faces[1][(M - 1) + i*(mw + 2)];
						faces[4][M + i*(mw + 2)] = faces[0][(0 + 1) + i*(mw + 2)];

						faces[5][i + 0*(mw + 2)] = faces[2][j + (0 + 1)*(mw + 2)];
						faces[5][i + M*(mw + 2)] = faces[3][j + (M - 1)*(mw + 2)];
						faces[5][0 + i*(mw + 2)] = faces[0][(M - 1) + i*(mw + 2)];
						faces[5][M + i*(mw + 2)] = faces[1][(0 + 1) + i*(mw + 2)];
					}

					for (int faceIndex = 0; faceIndex < 6; faceIndex++)
					{
						/*
						// ARB recommendation:
						http://www.opengl.org/registry/specs/ARB/seamless_cube_map.txt
						If a texture sample location would lie in the texture border in
						both u and v (in one of the corners of the cube), there is no
						unique neighboring face from which to extract one texel. The
						recommended method is to average the values of the three
						available samples. However, implementations are free to
						construct this fourth texel in another way, so long as, when the
						three available samples have the same value, this texel also has
						that value.
						*/
						
						Vec4* face = faces[faceIndex];
						
						face[0 + 0*(mw + 2)] = (face[(0 + 1) + (0 + 0)*(mw + 2)] + face[(0 + 0) + (0 + 1)*(mw + 2)] + face[0 + 0*(mw + 2)])/3.0f;
						face[M + 0*(mw + 2)] = (face[(M - 1) + (0 + 0)*(mw + 2)] + face[(M - 0) + (0 + 1)*(mw + 2)] + face[M + 0*(mw + 2)])/3.0f;
						face[0 + M*(mw + 2)] = (face[(0 + 1) + (M - 0)*(mw + 2)] + face[(0 + 0) + (M - 1)*(mw + 2)] + face[0 + M*(mw + 2)])/3.0f;
						face[M + M*(mw + 2)] = (face[(M - 1) + (M - 0)*(mw + 2)] + face[(M - 0) + (M - 1)*(mw + 2)] + face[M + M*(mw + 2)])/3.0f;
					}
				}
			}
			else
			{
				assert(0); // format not supported
			}
		}
	}

	return faces;
}

static Vec4 SampleCubemap(const std::vector<Vec4*>& facesWithBorder, int mw, Vec3V_In dir, bool filtered)
{
	int faceIndex = 0;
	const Vec2 uv = GetCubeFaceUV(faceIndex, dir);

	if (filtered)
	{
		const float x = (float)mw*(uv.x + 1.0f)/2.0f + 0.5f;
		const float y = (float)mw*(uv.y + 1.0f)/2.0f + 0.5f;

		const int i0 = (int)Clamp<float>(x, 0.0f, (float)(mw + 1));
		const int j0 = (int)Clamp<float>(y, 0.0f, (float)(mw + 1));
		const int i1 = Min<int>(i0 + 1, mw + 1);
		const int j1 = Min<int>(j0 + 1, mw + 1);

		const float fx1 = x - floorf(x);
		const float fy1 = y - floorf(y);
		const float fx0 = 1.0f - fx1;
		const float fy0 = 1.0f - fy1;

		float weights[] =
		{
			fx0*fy0,
			fx1*fy0,
			fx0*fy1,
			fx1*fy1,
		};

		// don't sample corners at all
		if ((i0 == 0 || i0 == mw + 1) && (j0 == 0 || j0 == mw + 1)) { weights[0] = 0.0f; }
		if ((i1 == 0 || i1 == mw + 1) && (j0 == 0 || j0 == mw + 1)) { weights[1] = 0.0f; }
		if ((i0 == 0 || i0 == mw + 1) && (j1 == 0 || j1 == mw + 1)) { weights[2] = 0.0f; }
		if ((i1 == 0 || i1 == mw + 1) && (j1 == 0 || j1 == mw + 1)) { weights[3] = 0.0f; }

		Vec4 result(0,0,0,0);

		result += facesWithBorder[faceIndex][i0 + j0*(mw + 2)]*weights[0];
		result += facesWithBorder[faceIndex][i1 + j0*(mw + 2)]*weights[1];
		result += facesWithBorder[faceIndex][i0 + j1*(mw + 2)]*weights[2];
		result += facesWithBorder[faceIndex][i1 + j1*(mw + 2)]*weights[3];

		return result/(weights[0] + weights[1] + weights[2] + weights[3]);
	}
	else
	{
		const int i = (int)Clamp<float>((float)mw*(uv.x + 1.0f)/2.0f, 0.0f, (float)(mw - 1));
		const int j = (int)Clamp<float>((float)mw*(uv.y + 1.0f)/2.0f, 0.0f, (float)(mw - 1));

		return facesWithBorder[faceIndex][(i + 1) + (j + 1)*(mw + 2)];
	}
}

enum CubeProjection
{
	CUBE_PROJECTION_Faces,
	CUBE_PROJECTION_4x3Cross,
	CUBE_PROJECTION_Panorama,
	CUBE_PROJECTION_Cylinder,
	CUBE_PROJECTION_MirrorBall,
	CUBE_PROJECTION_Spherical,
	CUBE_PROJECTION_SphericalSquare,
	CUBE_PROJECTION_Paraboloid,
	CUBE_PROJECTION_ParaboloidSquare,
	CUBE_PROJECTION_Count
};

static const char* g_cubeProjectionStrings[] =
{
	"faces",
	"4x3cross",
	"panorama",
	"cylinder",
	"mirrorball",
	"spherical",
	"sphericalsquare",
	"paraboloid",
	"paraboloidsquare",
};
CompileTimeAssert(NELEM(g_cubeProjectionStrings) == CUBE_PROJECTION_Count);

static __forceinline float frac(float x)
{
	return x - floorf(x);
}

// code is from %RS_CODEBRANCH%\game\shader_source\Debug\debug_DTQ.fx
Vec3V_Out DTQCubemapProject(const Vec2& texcoord, int cubeProjection, bool cubeProjClip, float faceZoom, float cylinderRadius, float cylinderHeight)
{
	Vec3V dir(0,0,0);

	if (cubeProjection == CUBE_PROJECTION_Faces)
	{
		dir.x = -(texcoord.x*2.0f - 1.0f)*faceZoom;
		dir.x = +(texcoord.y*2.0f - 1.0f)*faceZoom;
		dir.y = -1.0f;
	}
	else if (cubeProjection == CUBE_PROJECTION_4x3Cross) // show 4x3 cross
	{
		const Vec2 tc = texcoord*Vec2(4.0f, 3.0f); // [0..4],[0..3]
		const float u = frac(tc.x)*2.0f - 1.0f; // [-1..1]
		const float v = frac(tc.y)*2.0f - 1.0f; // [-1..1]
		const Vec2 face(floorf(tc.x), floorf(tc.y));

		if (face.x == 1.0f)
		{
			if (face.y == 0.0f) { dir = Vec3V(-u,-v,-1); } // -Z
			if (face.y == 1.0f) { dir = Vec3V(-u,-1,+v); } // -Y
			if (face.y == 2.0f) { dir = Vec3V(-u,+v,+1); } // +Z
		}
		else if (face.y == 1.0f)
		{
			if (face.x == 0.0f) { dir = Vec3V(+1,-u,+v); } // +X
			if (face.x == 2.0f) { dir = Vec3V(-1,+u,+v); } // -X
			if (face.x == 3.0f) { dir = Vec3V(+u,+1,+v); } // +Y
		}
	}
	else if (cubeProjection == CUBE_PROJECTION_Panorama)
	{
		const float sinTheta = sinf(-2.0f*PI*(texcoord.x - 0.25f));
		const float cosTheta = cosf(-2.0f*PI*(texcoord.x - 0.25f));
		const float sinPhi   = sinf(0.5f*PI*(texcoord.y*2.0f - 1.0f));
		const float cosPhi   = cosf(0.5f*PI*(texcoord.y*2.0f - 1.0f));

		dir.x = cosPhi*cosTheta;
		dir.y = cosPhi*sinTheta;
		dir.z = sinPhi;
	}
	else if (cubeProjection == CUBE_PROJECTION_Cylinder)
	{
		const float sinTheta = sinf(-2.0f*PI*(texcoord.x - 0.25f));
		const float cosTheta = cosf(-2.0f*PI*(texcoord.x - 0.25f));

		dir.x = cylinderRadius*cosTheta;
		dir.y = cylinderRadius*sinTheta;
		dir.z = cylinderHeight*(texcoord.y - 0.5f);
	}
	else if (cubeProjection == CUBE_PROJECTION_MirrorBall || cubeProjection == CUBE_PROJECTION_Spherical || cubeProjection == CUBE_PROJECTION_SphericalSquare)
	{
		const float u = texcoord.x*2.0f - 1.0f; // [-1..1]
		const float v = texcoord.y*2.0f - 1.0f; // [-1..1]

		const float d2 = (cubeProjection == CUBE_PROJECTION_SphericalSquare) ? Max<float>(u*u, v*v) : (u*u + v*v);

		if (cubeProjClip && d2 > 1.0f)
		{
			return Vec3V(0,0,0); // transparent
		}

		const float z = (cubeProjection == CUBE_PROJECTION_MirrorBall) ? (1.0f - 2.0f*d2) : cosf(PI*sqrtf(d2));

		dir.y = -z;
		dir.x = -u*sqrtf(abs(1.0f - z*z)/d2);
		dir.z = +v*sqrtf(abs(1.0f - z*z)/d2);
	}
	else if (cubeProjection == CUBE_PROJECTION_Paraboloid || cubeProjection == CUBE_PROJECTION_ParaboloidSquare)
	{
		const float u = frac(texcoord.x*2.0f)*2.0f - 1.0f; // [-1..1,-1..1]
		const float v = texcoord.y*2.0f - 1.0f; // [-1..1]

		const float d = (cubeProjection == CUBE_PROJECTION_ParaboloidSquare) ? Max<float>(Abs<float>(u), Abs<float>(v)) : sqrtf(u*u + v*v);

		if (cubeProjClip && d > 1.0f)
		{
			return Vec3V(0,0,0); // transparent
		}

		dir.y = 1.0f - d;
		dir.x = u*2.0f;
		dir.z = v*2.0f;

		if (texcoord.x < 0.5f)
		{
			dir.x = -dir.x;
			dir.y = -dir.y;
		}
	}

	return dir;
}

static void ConvertCubemapToProjection(const char* path, DXGI_FORMAT sourceFormat, int arrayCount, int mipCount, int cubeProjection, int scale = 4)
{
	const float cylinderAspect = 4.0f;
	const float cylinderRadius = 1.0f;
	const float cylinderHeight = 2.0f*PI/cylinderAspect;

	for (int layerIndex = 0; layerIndex < arrayCount; layerIndex++)
	{
		for (int mipIndex = 0; mipIndex < mipCount; mipIndex++)
		{
			int mw = 0;
			std::vector<Vec4*> facesWithBorder = LoadCubemapFacesWithBorder(path, layerIndex, mipIndex, mw);

			if (facesWithBorder.size() > 0)
			{
				const int minProjectionWidth = 512;
				const int pw = Max<int>(minProjectionWidth, mw*scale);
				int ph = pw;
				
				if (cubeProjection == CUBE_PROJECTION_Panorama ||
					cubeProjection == CUBE_PROJECTION_Paraboloid ||
					cubeProjection == CUBE_PROJECTION_ParaboloidSquare)
				{
					ph = pw/2;
				}
				else if (cubeProjection == CUBE_PROJECTION_4x3Cross)
				{
					ph = pw*3/4;
				}
				else if (cubeProjection == CUBE_PROJECTION_Cylinder)
				{
					ph = (int)((float)pw/cylinderAspect);
				}

				Vec4* panoramaImage = new Vec4[pw*ph];

				const float r2 = powf(1.0f + 1.0f/(float)ph, 2.0f);

				for (int j = 0; j < ph; j++)
				{
					for (int i = 0; i < pw; i++)
					{
						const float u = ((float)i + 0.5f)/(float)pw; // [0..1]
						const float v = ((float)j + 0.5f)/(float)ph; // [0..1]

						const Vec3V dir = DTQCubemapProject(Vec2(u, v), cubeProjection, true, 1.0f, cylinderRadius, cylinderHeight);

						if (dir.x != 0.0f || dir.y != 0.0f || dir.z != 0.0f)
						{
							panoramaImage[i + j*pw] = SampleCubemap(facesWithBorder, mw, dir, true);
							panoramaImage[i + j*pw].w = 1.0f;
						}
					}
				}

				char path2[512] = "";
				strcpy(path2, path);
				strcpy(strrchr(path2, '.'), varString("%s_", GetLayerMipString(layerIndex, arrayCount, mipIndex, mipCount)).c_str());
				strcat(path2, g_cubeProjectionStrings[cubeProjection]);
				strcat(path2, ".");

				SaveImageInAllRelevantFormats(path2, panoramaImage, pw, ph, sourceFormat);

				delete[] panoramaImage;
			}
		}
	}
}

static bool ConvertPanoramaToCubemap(const char* path, bool filtered = true, int scale = 2)
{
	int w = 0;
	int h = 0;
	Pixel32* image = LoadImage_Pixel32(path, w, h);

	if (image)
	{
		char path2[1024] = "";
		strcpy(path2, path);
		strcpy(strrchr(path2, '.'), "_cubemap.dds");

		FILE* dds = fopen(path2, "wb");

		if (dds)
		{
			const DDPIXELFORMAT ddpf = GetDDSPixelFormatFromDX10Format(DXGI_FORMAT_B8G8R8A8_UNORM);
			const int cubeSize = w/scale;

			WriteDDSHeader(dds, DDS_IMAGE_TYPE_CUBE, cubeSize, cubeSize, 1, 1, 1, ddpf);

			for (int faceIndex = 0; faceIndex < 6; faceIndex++)
			{
				for (int j = 0; j < cubeSize; j++)
				{
					for (int i = 0; i < cubeSize; i++)
					{
						const Vec3V dir = Normalize(GetCubeDir(faceIndex, i, j, cubeSize));
						const float dx = dir.GetXf();
						const float dy = dir.GetYf();
						const float dz = dir.GetZf();

						const float theta = atan2f(dy, dx);
						const float phi = asinf(dz);

						float x = frac(0.25f - 0.5f*theta/PI)*(float)w;
						float y = (0.5f + phi/PI)*(float)h;
						Pixel32 p;

						if (filtered)
						{
							x -= 0.5f;
							y -= 0.5f;

							const int i0 = Max<int>(0, w + (int)floorf(x))%w; // wrapped
							const int j0 = Clamp<int>((int)y, 0, h - 1); // clamped
							const int i1 = (i0 + 1)%w; // wrapped
							const int j1 = Min<int>(j0 + 1, h - 1); // clamped

							const float fx1 = x - floorf(x);
							const float fy1 = y - floorf(y);
							const float fx0 = 1.0f - fx1;
							const float fy0 = 1.0f - fy1;

							Vec4 sum(0.0f);
							sum += ConvertPixel<Vec4,Pixel32>(image[i0 + j0*w])*fx0*fy0;
							sum += ConvertPixel<Vec4,Pixel32>(image[i1 + j0*w])*fx1*fy0;
							sum += ConvertPixel<Vec4,Pixel32>(image[i0 + j1*w])*fx0*fy1;
							sum += ConvertPixel<Vec4,Pixel32>(image[i1 + j1*w])*fx1*fy1;

							p = ConvertPixel<Pixel32,Vec4>(sum);
						}
						else
						{
							const int i0 = Max<int>(0, w + (int)floorf(x))%w; // wrapped
							const int j0 = Clamp<int>((int)y, 0, h - 1); // clamped

							p = image[i0 + j0*w];
						}
						
						fwrite(&p, sizeof(Pixel32), 1, dds);
					}
				}
			}

			fclose(dds);

			if (0) // testing
			{
				ConvertCubemapToProjection(path2, DXGI_FORMAT_B8G8R8A8_UNORM, 1, 1, CUBE_PROJECTION_4x3Cross, 2);
				ConvertCubemapToProjection(path2, DXGI_FORMAT_B8G8R8A8_UNORM, 1, 1, CUBE_PROJECTION_Panorama, 2);
				ConvertCubemapToProjection(path2, DXGI_FORMAT_B8G8R8A8_UNORM, 1, 1, CUBE_PROJECTION_Spherical, 2);
			}
		}

		delete[] image;
		return true;
	}

	return false;
}

static bool ConvertSphericalToCubemap(const char* path, bool filtered = true, int scale = 2)
{
	int w = 0;
	int h = 0;
	Pixel32* image = LoadImage_Pixel32(path, w, h);

	if (image)
	{
		char path2[1024] = "";
		strcpy(path2, path);
		strcpy(strrchr(path2, '.'), "_cubemap.dds");

		FILE* dds = fopen(path2, "wb");

		if (dds)
		{
			const DDPIXELFORMAT ddpf = GetDDSPixelFormatFromDX10Format(DXGI_FORMAT_B8G8R8A8_UNORM);
			const int cubeSize = w/scale;

			WriteDDSHeader(dds, DDS_IMAGE_TYPE_CUBE, cubeSize, cubeSize, 1, 1, 1, ddpf);

			for (int faceIndex = 0; faceIndex < 6; faceIndex++)
			{
				for (int j = 0; j < cubeSize; j++)
				{
					for (int i = 0; i < cubeSize; i++)
					{
						const Vec3V dir = Normalize(GetCubeDir(faceIndex, i, j, cubeSize));
						const float dx = dir.GetXf();
						const float dy = dir.GetYf();
						const float dz = dir.GetZf();

						const float q = acosf(-dy)/(PI*sqrtf(Abs<float>(1.0f - dy*dy)));

						float x = (0.5f - 0.5f*dx*q)*(float)w;
						float y = (0.5f + 0.5f*dz*q)*(float)h;
						Pixel32 p;

						if (filtered)
						{
							x -= 0.5f;
							y -= 0.5f;

							const int i0 = Clamp<int>((int)x, 0, w - 1);
							const int j0 = Clamp<int>((int)y, 0, h - 1);
							const int i1 = Min<int>(i0 + 1, w - 1);
							const int j1 = Min<int>(j0 + 1, h - 1);

							const float fx1 = x - floorf(x);
							const float fy1 = y - floorf(y);
							const float fx0 = 1.0f - fx1;
							const float fy0 = 1.0f - fy1;

							Vec4 sum(0.0f);
							sum += ConvertPixel<Vec4,Pixel32>(image[i0 + j0*w])*fx0*fy0;
							sum += ConvertPixel<Vec4,Pixel32>(image[i1 + j0*w])*fx1*fy0;
							sum += ConvertPixel<Vec4,Pixel32>(image[i0 + j1*w])*fx0*fy1;
							sum += ConvertPixel<Vec4,Pixel32>(image[i1 + j1*w])*fx1*fy1;

							p = ConvertPixel<Pixel32,Vec4>(sum);
						}
						else
						{
							const int i0 = Clamp<int>((int)x, 0, w - 1);
							const int j0 = Clamp<int>((int)y, 0, h - 1);

							p = image[i0 + j0*w];
						}

						fwrite(&p, sizeof(Pixel32), 1, dds);
					}
				}
			}

			fclose(dds);

			if (0) // testing
			{
				ConvertCubemapToProjection(path2, DXGI_FORMAT_B8G8R8A8_UNORM, 1, 1, CUBE_PROJECTION_4x3Cross, 2);
				ConvertCubemapToProjection(path2, DXGI_FORMAT_B8G8R8A8_UNORM, 1, 1, CUBE_PROJECTION_Panorama, 2);
				ConvertCubemapToProjection(path2, DXGI_FORMAT_B8G8R8A8_UNORM, 1, 1, CUBE_PROJECTION_Spherical, 2);
			}
		}

		delete[] image;
		return true;
	}

	return false;
}

static bool ConvertParaboloidToCubemap(const char* path, bool filtered = true, int scale = 2)
{
	int w = 0;
	int h = 0;
	Pixel32* image = LoadImage_Pixel32(path, w, h);

	if (image)
	{
		char path2[1024] = "";
		strcpy(path2, path);
		strcpy(strrchr(path2, '.'), "_cubemap.dds");

		FILE* dds = fopen(path2, "wb");

		if (dds)
		{
			const DDPIXELFORMAT ddpf = GetDDSPixelFormatFromDX10Format(DXGI_FORMAT_B8G8R8A8_UNORM);
			const int cubeSize = w/scale;

			WriteDDSHeader(dds, DDS_IMAGE_TYPE_CUBE, cubeSize, cubeSize, 1, 1, 1, ddpf);

			for (int faceIndex = 0; faceIndex < 6; faceIndex++)
			{
				for (int j = 0; j < cubeSize; j++)
				{
					for (int i = 0; i < cubeSize; i++)
					{
						const Vec3V dir = Normalize(GetCubeDir(faceIndex, i, j, cubeSize));
						const float dx = dir.GetXf();
						const float dy = dir.GetYf();
						const float dz = dir.GetZf();

						const float d = (dy*dy - 1.0f + 2.0f*sqrtf(dy*dy*abs(1.0f - dy*dy)))/(5.0f*dy*dy - 1.0f);
						const float q = sqrtf(abs(1.0f - 2.0f*d + 5.0f*d*d));
						const float u = dx*q;
						const float v = dz*q;

						float x = 0.5f - 0.25f*u;
						float y = 0.5f + 0.25f*v;

						if (dy > 0.0f)
						{
							x = 2.0f - x;
						}

						x *= 0.5f*(float)w;
						y *= 1.0f*(float)h;

						Pixel32 p;

						if (filtered)
						{
							x -= 0.5f;
							y -= 0.5f;

							const int i0 = Clamp<int>((int)x, 0, w - 1);
							const int j0 = Clamp<int>((int)y, 0, h - 1);
							const int i1 = Min<int>(i0 + 1, w - 1);
							const int j1 = Min<int>(j0 + 1, h - 1);

							const float fx1 = x - floorf(x);
							const float fy1 = y - floorf(y);
							const float fx0 = 1.0f - fx1;
							const float fy0 = 1.0f - fy1;

							Vec4 sum(0.0f);
							sum += ConvertPixel<Vec4,Pixel32>(image[i0 + j0*w])*fx0*fy0;
							sum += ConvertPixel<Vec4,Pixel32>(image[i1 + j0*w])*fx1*fy0;
							sum += ConvertPixel<Vec4,Pixel32>(image[i0 + j1*w])*fx0*fy1;
							sum += ConvertPixel<Vec4,Pixel32>(image[i1 + j1*w])*fx1*fy1;

							p = ConvertPixel<Pixel32,Vec4>(sum);
						}
						else
						{
							const int i0 = Clamp<int>((int)x, 0, w - 1);
							const int j0 = Clamp<int>((int)y, 0, h - 1);

							p = image[i0 + j0*w];
						}

						fwrite(&p, sizeof(Pixel32), 1, dds);
					}
				}
			}

			fclose(dds);

			if (0) // testing
			{
			//	ConvertCubemapToProjection(path2, DXGI_FORMAT_B8G8R8A8_UNORM, 1, 1, CUBE_PROJECTION_4x3Cross, 2);
			//	ConvertCubemapToProjection(path2, DXGI_FORMAT_B8G8R8A8_UNORM, 1, 1, CUBE_PROJECTION_Panorama, 2);
			//	ConvertCubemapToProjection(path2, DXGI_FORMAT_B8G8R8A8_UNORM, 1, 1, CUBE_PROJECTION_Spherical, 2);
				ConvertCubemapToProjection(path2, DXGI_FORMAT_B8G8R8A8_UNORM, 1, 1, CUBE_PROJECTION_Paraboloid, 2);
			}
		}

		delete[] image;
		return true;
	}

	return false;
}

static int g_forceProjection = -1;

static void processFile(const char* path)
{
	const char* ext = strrchr(path, '.');
	if(!ext)
	{
		return;
	}

	int w = 0;
	int h = 0;
	int mips = 0;
	DXGI_FORMAT format = DXGI_FORMAT_UNKNOWN;
	DDS_IMAGE_TYPE type = DDS_IMAGE_TYPE_UNKNOWN;
	int arrayCount = 0;

	if (_stricmp(ext, ".dds") == 0)
	{
		if (GetDDSInfo(path, w, h, &mips, &format, &type, &arrayCount))
		{
			if (type == DDS_IMAGE_TYPE_CUBE)
			{
				if(g_forceProjection == -1)
				{
					ConvertCubemapTo4x3Cross(path);
				}

			//	ConvertCubemapToProjection(path, format, arrayCount, mips, CUBE_PROJECTION_4x3Cross);
				if(g_forceProjection == -1 || g_forceProjection == CUBE_PROJECTION_Panorama)
				{
					ConvertCubemapToProjection(path, format, arrayCount, mips, CUBE_PROJECTION_Panorama);
				}
				if(g_forceProjection == -1 || g_forceProjection == CUBE_PROJECTION_Cylinder)
				{
					ConvertCubemapToProjection(path, format, arrayCount, mips, CUBE_PROJECTION_Cylinder);
				}
				if(g_forceProjection == -1 || g_forceProjection == CUBE_PROJECTION_MirrorBall)
				{
					ConvertCubemapToProjection(path, format, arrayCount, mips, CUBE_PROJECTION_MirrorBall);
				}
				if(g_forceProjection == -1 || g_forceProjection == CUBE_PROJECTION_Spherical)
				{
					ConvertCubemapToProjection(path, format, arrayCount, mips, CUBE_PROJECTION_Spherical);
				}
				if(g_forceProjection == -1 || g_forceProjection == CUBE_PROJECTION_SphericalSquare)
				{
					ConvertCubemapToProjection(path, format, arrayCount, mips, CUBE_PROJECTION_SphericalSquare);
				}
				if(g_forceProjection == -1 || g_forceProjection == CUBE_PROJECTION_Paraboloid)
				{
					ConvertCubemapToProjection(path, format, arrayCount, mips, CUBE_PROJECTION_Paraboloid);
				}
				if(g_forceProjection == -1 || g_forceProjection == CUBE_PROJECTION_ParaboloidSquare)
				{
					ConvertCubemapToProjection(path, format, arrayCount, mips, CUBE_PROJECTION_ParaboloidSquare);
				}
			}
			else if (type == DDS_IMAGE_TYPE_2D)
			{
				if (h*4 == w*3) // 4:3
				{
					Convert4x3CrossToCubemap(path, ext, 4,3);
				}
			//	else if (h*3 == w*4) // 3:4
			//	{
			//		Convert3x4CrossToCubemap(path, ext, 3,4);
			//	}
				else if (h*2 == w) // 2:1
				{
					ConvertPanoramaToCubemap(path);
					//ConvertParaboloidToCubemap(path);
				}
				else if (h == w) // 1:1
				{
					ConvertSphericalToCubemap(path);
				}
				else
				{
					EnterCriticalSection(&g_critSec);
					printf("unsupported dimensions for %s (w=%d,h=%d,type=%d)\n", path, w, h, type);
					LeaveCriticalSection(&g_critSec);
				}
			}
		}
	}
	else // maybe it's not a dds
	{
		w = 0;
		h = 0;

		if (GetImageDimensions(path, w, h))
		{
			if (h*4 == w*3) // 4:3
			{
				Convert4x3CrossToCubemap(path, ext, 4,3);
			}
		//	else if (h*3 == w*4) // 3:4
		//	{
		//		Convert3x4CrossToCubemap(path, ext, 3,4);
		//	}
			else if (h*2 == w) // 2:1
			{
				ConvertPanoramaToCubemap(path);
				//ConvertParaboloidToCubemap(path);
			}
			else if (h == w) // 1:1
			{
				ConvertSphericalToCubemap(path);
			}
			else
			{
				EnterCriticalSection(&g_critSec);
				printf("unsupported dimensions for %s (w=%d,h=%d)\n", path, w, h);
				LeaveCriticalSection(&g_critSec);
			}
		}
	}
}

static void enumerateDirectory(const char* path, std::vector<std::string>& r_files)
{
	char buff[MAX_PATH];
	sprintf_s(buff, sizeof(buff), "%s\\*", path);

	WIN32_FIND_DATAA findData;
	HANDLE hFind = FindFirstFileA(buff, &findData);
	if(hFind == INVALID_HANDLE_VALUE)
	{
		return;
	}

	do {
		// Recurse into directories
		char fullPath[MAX_PATH];
		sprintf_s(fullPath, sizeof(fullPath), "%s\\%s", path, findData.cFileName);
		if(findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			if(strcmp(findData.cFileName, ".") == 0 || strcmp(findData.cFileName, "..") == 0)
			{
				// Special case for "." and ".."
				continue;
			}

			// Actually, don't recurse. Someone is bound to dump the X: drive onto the tool at
			// some point, and hilarity would ensue...
//			processDirectory(fullPath, r_files);
		}
		else
		{
			// Only process .dds files in directories
			const char* ext = strrchr(fullPath, '.');
			if (_stricmp(ext, ".dds") == 0)
			{
				r_files.push_back(fullPath);
			}
		}
	} while(FindNextFileA(hFind, &findData));
	FindClose(hFind);
}

static DWORD __stdcall threadProc(void* pParam)
{
	processFile((const char*)pParam);
	return 0;
}

static void processDirectory(const char* path, int maxThreads)
{
	printf("Processing directory %s\nPress Ctrl+C to abort.\n", path);

	// Gather all files in the directory
	std::vector<std::string> files;
	enumerateDirectory(path, files);
	if(files.empty())
		return;

	// Spawn one thread per CPU if maxThreads wasn't specified
	if(maxThreads <= 0)
	{
		SYSTEM_INFO si;
		GetSystemInfo(&si);
		maxThreads = (int)si.dwNumberOfProcessors;
	}
	if(maxThreads <= 0)
		maxThreads = 1;

	// Process all files
	std::vector<HANDLE> threads;
	threads.reserve(maxThreads);
	size_t idx = 0;
	while(idx < files.size())
	{
		// Spawn a new thread
		HANDLE hThread = CreateThread(NULL, 0, threadProc, (void*)files[idx].c_str(), 0, NULL);
		if(!hThread)
		{
			processFile(files[idx].c_str());
		}
		else
		{
			threads.push_back(hThread);
		}
		++idx;

		// If we reach the thread limit, wait for a thread to exit
		if(threads.size() == maxThreads)
		{
			DWORD ret = WaitForMultipleObjects((DWORD)threads.size(), &threads[0], FALSE, INFINITE);
			ret -= WAIT_OBJECT_0;
			CloseHandle(threads[ret]);
			threads.erase(threads.begin()+ret);
		}

		// Ctrl+C requested?
		if(g_ctrlC)
		{
			EnterCriticalSection(&g_critSec);
			printf("Ctrl+C received; waiting on work already started to complete...\n");
			LeaveCriticalSection(&g_critSec);
			break;
		}
	}

	// Wait for all threads
	WaitForMultipleObjects((DWORD)threads.size(), &threads[0], TRUE, INFINITE);
	for(size_t i=0; i<threads.size(); ++i)
		CloseHandle(threads[i]);
}

int main(int argc, const char* argv[])
{
	InitializeCriticalSection(&g_critSec);

	signal(SIGINT, signalHandler);
	signal(SIGTERM, signalHandler);

	bool pauseAtEnd = true;
	int maxThreads = 0;
	if (argc > 1)
	{
		for (int i = 1; i < argc; i++)
		{
			// Force conversion type?
			if(argv[i][0] == '-')
			{
				if(_stricmp(argv[i], "-NoPause") == 0)
					pauseAtEnd = false;
				else if(_stricmp(argv[i], "-Panorama") == 0)
					g_forceProjection = CUBE_PROJECTION_Panorama;
				else if(_stricmp(argv[i], "-Cylinder") == 0)
					g_forceProjection = CUBE_PROJECTION_Cylinder;
				else if(_stricmp(argv[i], "-MirrorBall") == 0)
					g_forceProjection = CUBE_PROJECTION_MirrorBall;
				else if(_stricmp(argv[i], "-Spherical") == 0)
					g_forceProjection = CUBE_PROJECTION_Spherical;
				else if(_stricmp(argv[i], "-SphericalSquare") == 0)
					g_forceProjection = CUBE_PROJECTION_SphericalSquare;
				else if(_stricmp(argv[i], "-Paraboloid") == 0)
					g_forceProjection = CUBE_PROJECTION_Paraboloid;
				else if(_stricmp(argv[i], "-ParaboloidSquare") == 0)
					g_forceProjection = CUBE_PROJECTION_ParaboloidSquare;
				else if(_strnicmp(argv[i], "-MaxThreads=", 12) == 0)
				{
					maxThreads = atoi(argv[i]+12);
					if(maxThreads < 0 || maxThreads > 100)
					{
						printf("Invalid value for max threads (%d), using default.\n", maxThreads);
						maxThreads = 0;
					}
				}
				else
					g_forceProjection = -1;
				continue;
			}
			const char* path = argv[i];
			DWORD fileAttribs = GetFileAttributesA(path);

			// Directory?
			if(fileAttribs != INVALID_FILE_ATTRIBUTES && (fileAttribs & FILE_ATTRIBUTE_DIRECTORY))
			{
				processDirectory(path, maxThreads);
			}
			else
			{
				processFile(path);
			}
		}
	}
	else
	{
		printf("USAGE: drag image files or directories onto the exe.\n"
			"Optional arguments:\n"
			"   -NoPause           Don't pause after conversion\n"
			"   -Panorama          Generate only Panorama output\n"
			"   -Cylinder          Generate only Cylinder output\n"
			"   -MirrorBall        Generate only MirrorBall output\n"
			"   -Spherical         Generate only Spherical output\n"
			"   -SphericalSquare   Generate only SphericalSquare output\n"
			"   -Paraboloid        Generate only Paraboloid output\n"
			"   -ParaboloidSquare  Generate only ParaboloidSquare output\n"
			"   -MaxThreads=X      Use X processing threads\n");
	}

	if(pauseAtEnd)
	{
		system("pause");
	}
}
