/*
NOISE GENERATOR TOOL
Saturday December 23, 2000 3:36PM
*/

#define _CRT_SECURE_NO_WARNINGS
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "gl/glut.h"
#include "gl/glui.h"
#include "gl/glext.h"

/*
sphere noise params:
"sphere noise2" uses
	seed= 1
	res= 128
	rough= .75
	low_skip= 3
	high_skip= 2
	marbling= 0
noise2a
	rough= .75
	low_skip= 3
	high_skip= 0
noise2b
	rough= .6
	low_skip= 3
	high_skip= 3
*/

#define PIN(z,a,b) ( ((z)<(a)) ? (a) : ( ((z)>(b)) ? (b) : (z) ))

typedef unsigned long pixel_t;

/* ---------- errors */

static void fatal_error(const char *msg);
static void fatal_error(
	const char *msg)
{
	char wait[200];
	fprintf(stdout, "\n\n%s\n", msg);
	fprintf(stdout, "PRESS RETURN TO EXIT\n");
	fgets(wait, 199, stdin);
	exit(-1);
}

static void fatal_error(const char *msg, const char *param);
static void fatal_error(
	const char *msg,
	const char *param)
{
	while(*param == '\t' || *param == ' ') param++;
	char wait[200];
	fprintf(stdout, "\n\n%s: \"%s\"\n", msg, param);
	fprintf(stdout, "PRESS RETURN TO EXIT\n");
	fgets(wait, 199, stdin);
	exit(-1);
}

/* ---------- math */

#define PI      3.14159265358979323846264338327950f
#define PI2     6.28318530717958647692528676655901f
#define EPSILON 0.00000001000000000000000000000000f

class vec3_t
{
	public: float x, y, z;
	
	inline vec3_t(void)
	{
	}
	inline vec3_t(const float _x, const float _y, const float _z)
	{
		x = _x, y = _y, z = _z;
	}
	inline int operator ==(const vec3_t& a) const
	{
		return x == a.x && y == a.y && z == a.z;
	}
	inline int operator !=(const vec3_t& a) const
	{
		return x != a.x || y != a.y || z != a.z;
	}
	inline vec3_t operator +(const vec3_t& a) const
	{
		return vec3_t(x + a.x, y + a.y, z + a.z);
	}
	inline vec3_t operator -(const vec3_t& a) const
	{
		return vec3_t(x - a.x, y - a.y, z - a.z);
	}
	inline vec3_t operator +(void) const
	{
		return vec3_t(+x, +y, +z);
	}
	inline vec3_t operator -(void) const
	{
		return vec3_t(-x, -y, -z);
	}
	inline float operator *(const vec3_t& a) const
	{
		return x*a.x + y*a.y + z*a.z;
	}
	inline vec3_t operator *(const float a) const
	{
		return vec3_t(x*a, y*a, z*a);
	}
	inline vec3_t operator /(const float a) const
	{
		return (*this)*(1.0f/a);
	}
	inline vec3_t operator ^(const vec3_t& a) const
	{
		return vec3_t(y*a.z - z*a.y,
		              z*a.x - x*a.z,
		              x*a.y - y*a.x);
	}
	inline const vec3_t& operator +=(const vec3_t& a)
	{
		(*this) = (*this) + a; return (*this);
	}
	inline const vec3_t& operator -=(const vec3_t& a)
	{
		(*this) = (*this) - a; return (*this);
	}
	inline const vec3_t& operator *=(const float a)
	{
		(*this) = (*this)*a; return (*this);
	}
	inline const vec3_t& operator /=(const float a)
	{
		(*this) = (*this)/a; return (*this);
	}
	inline float operator [](const int i) const
	{
		return ((float*)this)[i];
	}
	inline float& operator [](const int i)
	{
		return ((float*)this)[i];
	}
};

class vec4_t
{
	public: float x, y, z, w;
	
	inline vec4_t(void)
	{
	}
	inline vec4_t(const float _x, const float _y, const float _z, const float _w)
	{
		x = _x, y = _y, z = _z, w = _w;
	}
	inline int operator ==(const vec4_t& a) const
	{
		return x == a.x && y == a.y && z == a.z && w == a.w;
	}
	inline int operator !=(const vec4_t& a) const
	{
		return x != a.x || y != a.y || z != a.z || w != a.w;
	}
	inline vec4_t operator +(const vec4_t& a) const
	{
		return vec4_t(x + a.x, y + a.y, z + a.z, w + a.w);
	}
	inline vec4_t operator -(const vec4_t& a) const
	{
		return vec4_t(x - a.x, y - a.y, z - a.z, w - a.w);
	}
	inline vec4_t operator +(void) const
	{
		return vec4_t(+x, +y, +z, +w);
	}
	inline vec4_t operator -(void) const
	{
		return vec4_t(-x, -y, -z, -w);
	}
	inline float operator *(const vec4_t& a) const
	{
		return x*a.x + y*a.y + z*a.z + w*a.w;
	}
	inline vec4_t operator *(const float a) const
	{
		return vec4_t(x*a, y*a, z*a, w*a);
	}
	inline vec4_t operator /(const float a) const
	{
		return (*this)*(1.0f/a);
	}
	inline const vec4_t& operator +=(const vec4_t& a)
	{
		(*this) = (*this) + a; return (*this);
	}
	inline const vec4_t& operator -=(const vec4_t& a)
	{
		(*this) = (*this) - a; return (*this);
	}
	inline const vec4_t& operator *=(const float a)
	{
		(*this) = (*this)*a; return (*this);
	}
	inline const vec4_t& operator /=(const float a)
	{
		(*this) = (*this)/a; return (*this);
	}
	inline float operator [](const int i) const
	{
		return ((float*)this)[i];
	}
	inline float& operator [](const int i)
	{
		return ((float*)this)[i];
	}
};

/* ---------- noise code */

struct noise_properties_t
{
	long  seed;
	int   res;
	float roughness;
	int   low_freq_skip;
	int   high_freq_skip;
	float marbling;
	float turbulence;
	int   turbulence_res;
	float turbulence_roughness;
	int   turbulence_low_freq_skip;
	int   turbulence_high_freq_skip;
	float turbulence_marbling;
	float turbulence_expshift;
	float turbulence_offset_x;
	float turbulence_offset_y;

	float *preview_data;
	int preview_res;
	int preview_tiles;
	
	int quadratic_sequence_count;
    GLUI_EditText *filename_GLUI;
};

static int CalcIndex1D(int x, int res);
static int CalcIndex2D(int x, int y, int res);
static int CalcIndex3D(int x, int y, int z, int res);

static float Interpolate1D(const float *data, float xf);
static float Interpolate2D(const float *data, float xf, float yf);
static float Interpolate3D(const float *data, float xf, float yf, float zf);

static float Interpolate1D_Linear(const float *data, float xf);
static float Interpolate2D_Linear(const float *data, float xf, float yf);
static float Interpolate3D_Linear(const float *data, float xf, float yf, float zf);

static float *StupidNoise1D(int res, int freq, float *data2, float scale);
static float *StupidNoise2D(int res, int freq, float *data2, float scale);
static float *StupidNoise3D(int res, int freq, float *data2, float scale);

static float *PerlinNoise1D(int res, const noise_properties_t *noise_properties);
static float *PerlinNoise2D(int res, const noise_properties_t *noise_properties);
static float *PerlinNoise3D(int res, const noise_properties_t *noise_properties);

static float *SphereNoise(const float *input_noise, int input_res, float *output_noise, int output_res, int face_index);

static int CalcIndex1D(int x, int res)
{
	x%= res; if (x<0) x+= res;
	
	return x;
}

static int CalcIndex2D(int x, int y, int res)
{
	x%= res; if (x<0) x+= res;
	y%= res; if (y<0) y+= res;
	
	return x + y*res;
}

static int CalcIndex3D(int x, int y, int z, int res)
{
	x%= res; if (x<0) x+= res;
	y%= res; if (y<0) y+= res;
	z%= res; if (z<0) z+= res;
	
	return x + y*res + z*res*res;
}

static float Interpolate1D(const float *data, float xf)
{
	const float x2 = xf*xf;
	const float x3 = xf*x2;

	return
	(
		data[0]*(1.0f - 3.0f*xf + 3.0f*x2 - 1.0f*x3) +
		data[1]*(4.0f           - 6.0f*x2 + 3.0f*x3) +
		data[2]*(1.0f + 3.0f*xf + 3.0f*x2 - 3.0f*x3) +
		data[3]*(                           1.0f*x3)
	)/6.0f;

	// WARNING: result may be out of range
	/*
	float p= (data[3] - data[2]) - (data[0] - data[1]);
	float q= (data[0] - data[1]) - p;
	float r= (data[2] - data[0]);
	float s= (data[1]);

	// d0*(-x^3 + 2*x^2 - x) +
	// d1*( x^3 - 2*x^2 + 1) +
	// d2*(-x^3 +   x^2 + x) +
	// d3*( x^3 -   x^2    )

	return s + xf*(r + xf*(q + xf*p));
	*/
}

static float Interpolate2D(const float *data, float xf, float yf)
{
	float c[4];
	
	for (int i= 0; i<4; i++)
	{
		c[i]= Interpolate1D(data + i*4, xf);
	}
	
	return Interpolate1D(c, yf);
}

static float Interpolate3D(const float *data, float xf, float yf, float zf)
{
	float c[4];
	
	for (int i= 0; i<4; i++)
	{
		c[i]= Interpolate2D(data + i*16, xf, yf);
	}
	
	return Interpolate1D(c, zf);
}

static float Interpolate1D_Linear(const float *data, float xf)
{
	return data[1] + xf*(data[2] - data[1]);
}

static float Interpolate2D_Linear(const float *data, float xf, float yf)
{
	float c[4];
	
	for (int i= 0; i<4; i++)
	{
		c[i]= Interpolate1D_Linear(data + i*4, xf);
	}
	
	return Interpolate1D_Linear(c, yf);
}

static float Interpolate3D_Linear(const float *data, float xf, float yf, float zf)
{
	float c[4];
	
	for (int i= 0; i<4; i++)
	{
		c[i]= Interpolate2D_Linear(data + i*16, xf, yf);
	}
	
	return Interpolate1D_Linear(c, zf);
}

static float Sample2D(
	const float *data,
	short width,
	short height,
	float x,
	float y)
{
	short xi= (short)floorf(x*(float)width);
	short yi= (short)floorf(y*(float)height);
	float xf= x*width  - floorf(x*width);
	float yf= y*height - floorf(y*height);
	
	float d1= data[((xi + 0 + width)%width) + ((yi + 0 + height)%height)*width]*(1.0f - xf) +
	          data[((xi + 1 + width)%width) + ((yi + 0 + height)%height)*width]*xf;
	float d2= data[((xi + 0 + width)%width) + ((yi + 1 + height)%height)*width]*(1.0f - xf) +
	          data[((xi + 1 + width)%width) + ((yi + 1 + height)%height)*width]*xf;
	
	return d1*(1.0f - yf) + d2*yf;
}

static float *StupidNoise1D(int res, int freq, float *data2, float scale)
{
	float *data1= NULL;
	if (!data1) data1= (float*)calloc(sizeof(float), freq);
	if (!data2) data2= (float*)calloc(sizeof(float), res);
	if (!data1 || !data2) fatal_error("out of memory");
	
	for (int i= 0; i<freq; i++)
	{
		data1[i]= (float)rand()/32767.0f;
	}
	
	for (int x= 0; x<res; x++)
	{
		int x3= (x*freq)/res - 1;
		float tmp[64];
		
		for (int x2= 0; x2<4; x2++)
		{
			tmp[x2]= data1[CalcIndex1D(x2 + x3, freq)];
		}
		
		float xf= (float)(x*freq)/(float)(res);
		xf-= floorf(xf);
		data2[CalcIndex1D(x, res)]+= Interpolate1D(tmp, xf)*scale;
	}
	
	free(data1);
	return data2;
}

static float *StupidNoise2D(int res, int freq, float *data2, float scale)
{
	float *data1= NULL;
	if (!data1) data1= (float*)calloc(sizeof(float), freq*freq);
	if (!data2) data2= (float*)calloc(sizeof(float), res*res);
	if (!data1 || !data2) fatal_error("out of memory");
	
	for (int i= 0; i<freq*freq; i++)
	{
		data1[i]= (float)rand()/32767.0f;
	}
	
	for (int y= 0; y<res; y++)
	for (int x= 0; x<res; x++)
	{
		int x3= (x*freq)/res - 1;
		int y3= (y*freq)/res - 1;
		float tmp[64];
		
		for (int y2= 0; y2<4; y2++)
		for (int x2= 0; x2<4; x2++)
		{
			tmp[x2 + y2*4]= data1[CalcIndex2D(x2 + x3, y2 + y3, freq)];
		}
		
		float xf= (float)(x*freq)/(float)(res);
		float yf= (float)(y*freq)/(float)(res);
		xf-= floorf(xf);
		yf-= floorf(yf);
		data2[CalcIndex2D(x, y, res)]+= Interpolate2D(tmp, xf, yf)*scale;
	}
	
	free(data1);
	return data2;
}

static float *StupidNoise3D(int res, int freq, float *data2, float scale)
{
	float *data1= NULL;
	if (!data1) data1= (float*)calloc(sizeof(float), freq*freq*freq);
	if (!data2) data2= (float*)calloc(sizeof(float), res*res*res);
	if (!data1 || !data2) fatal_error("out of memory");
	
	for (int i= 0; i<freq*freq*freq; i++)
	{
		data1[i]= (float)rand()/32767.0f;
	}
	
	for (int z= 0; z<res; z++)
	for (int y= 0; y<res; y++)
	for (int x= 0; x<res; x++)
	{
		int x3= (x*freq)/res - 1;
		int y3= (y*freq)/res - 1;
		int z3= (z*freq)/res - 1;
		float tmp[64];
		
		for (int z2= 0; z2<4; z2++)
		for (int y2= 0; y2<4; y2++)
		for (int x2= 0; x2<4; x2++)
		{
			tmp[x2 + y2*4 + z2*16]= data1[CalcIndex3D(x2 + x3, y2 + y3, z2 + z3, freq)];
		}
		
		float xf= (float)(x*freq)/(float)(res);
		float yf= (float)(y*freq)/(float)(res);
		float zf= (float)(z*freq)/(float)(res);
		xf-= floorf(xf);
		yf-= floorf(yf);
		zf-= floorf(zf);
		data2[CalcIndex3D(x, y, z, res)]+= Interpolate3D(tmp, xf, yf, zf)*scale;
	}
	
	free(data1);
	return data2;
}

static float *PerlinNoise1D(int res, const noise_properties_t *noise_properties)
{
	float *data=  NULL;
	float  scale= 1.0f;
	int    freq=  2;
	int    octaves= 0; do { octaves++; } while ((1<<octaves)<(res));
	
	for (int level= 0; level<octaves; level++)
	{
		if (level>=noise_properties->low_freq_skip &&
		    level<=octaves - noise_properties->high_freq_skip)
		{
			data= StupidNoise1D(res, freq, data, scale);
		}
		else if (level<=octaves - noise_properties->high_freq_skip)
		{
			for (int i= 0; i<freq; i++) rand();
		}
		
		freq*= 2;
		scale*= noise_properties->roughness;
	}
	
	if (data)
	{
		float min_v= data[0];
		float max_v= data[0];
		int i;
		
		for (i= 1; i<res; i++)
		{
			if (min_v>data[i]) min_v= data[i];
			if (max_v<data[i]) max_v= data[i];
		}
		
		for (i= 0; i<res; i++)
		{
			data[i]= (data[i] - min_v)/(max_v - min_v);
		}
		
		if (noise_properties->marbling!=0.0f)
		{
			for (i= 0; i<res; i++)
			{
				data[i]= sinf(PI2*data[i]*noise_properties->marbling)*0.5f + 0.5f;
			}
		}
	}
	else
	{
		data= (float*)calloc(sizeof(float), res);
		if (!data) fatal_error("out of memory");
	}
	
	return data;
}

static float *PerlinNoise2D(int res, const noise_properties_t *noise_properties)
{
	float *data=  NULL;
	float  scale= 1.0f;
	int    freq=  2;
	int    octaves= 0; do { octaves++; } while ((1<<octaves)<(res));
	
	for (int level= 0; level<octaves; level++)
	{
		if (level>=noise_properties->low_freq_skip &&
		    level<=octaves - noise_properties->high_freq_skip)
		{
			data= StupidNoise2D(res, freq, data, scale);
		}
		else if (level<=octaves - noise_properties->high_freq_skip)
		{
			for (int i= 0; i<freq*freq; i++) rand();
		}
		
		freq*= 2;
		scale*= noise_properties->roughness;
	}
	
	if (data)
	{
		if (noise_properties->turbulence!=0.0f)
		{
			noise_properties_t prop = *noise_properties;

			int turbulence_res= 8<<noise_properties->turbulence_res;
			float turbulence_exp= powf(2.0f, noise_properties->turbulence_expshift);

			prop.turbulence     = 0.0f;
			prop.roughness      = noise_properties->turbulence_roughness;
			prop.low_freq_skip  = noise_properties->turbulence_low_freq_skip;
			prop.high_freq_skip = noise_properties->turbulence_high_freq_skip;
			prop.marbling       = noise_properties->turbulence_marbling;

			srand(noise_properties->seed + 1);
			float* dx = PerlinNoise2D(turbulence_res, &prop);
			srand(noise_properties->seed + 2);
			float* dy = PerlinNoise2D(turbulence_res, &prop);
			float* temp = new float[res*res];
			memcpy(temp, data, res*res*sizeof(float));

			for (int j = 0; j < res; j++)
			for (int i = 0; i < res; i++)
			{
				float x = Sample2D(dx, turbulence_res, turbulence_res, (float)i/(float)res, (float)j/(float)res)*2.0f - 1.0f;
				float y = Sample2D(dy, turbulence_res, turbulence_res, (float)i/(float)res, (float)j/(float)res)*2.0f - 1.0f;

				if (turbulence_exp != 1.0f)
				{
					x = powf(x > 0.0f ? x : -x, turbulence_exp)*(x > 0.0f ? 1.0f : -1.0f);
					y = powf(y > 0.0f ? y : -y, turbulence_exp)*(y > 0.0f ? 1.0f : -1.0f);
				}

				x += noise_properties->turbulence_offset_x;
				y += noise_properties->turbulence_offset_y;

				x = x*noise_properties->turbulence/64.0f + (float)i/(float)res;
				y = y*noise_properties->turbulence/64.0f + (float)j/(float)res;

				data[i + j*res] = Sample2D(temp, res, res, x, y);
			}

			delete[] temp;
			free(dx);
			free(dy);
		}

		float min_v= data[0];
		float max_v= data[0];
		int i;
		
		for (i= 1; i<res*res; i++)
		{
			if (min_v>data[i]) min_v= data[i];
			if (max_v<data[i]) max_v= data[i];
		}
		
		for (i= 0; i<res*res; i++)
		{
			data[i]= (data[i] - min_v)/(max_v - min_v);
		}
		
		if (noise_properties->marbling!=0.0f)
		{
			for (i= 0; i<res*res; i++)
			{
				data[i]= sinf(PI2*data[i]*noise_properties->marbling)*0.5f + 0.5f;
			}
		}
	}
	else
	{
		data= (float*)calloc(sizeof(float), res*res);
		if (!data) fatal_error("out of memory");
	}
	
	return data;
}

static float *PerlinNoise3D(int res, const noise_properties_t *noise_properties)
{
	float *data=  NULL;
	float  scale= 1.0f;
	int    freq=  1;
	int    octaves= 0; do { octaves++; } while ((1<<octaves)<(res));
	
	for (int level= 0; level<octaves; level++)
	{
		if (level>=noise_properties->low_freq_skip &&
		    level<=octaves - noise_properties->high_freq_skip)
		{
			data= StupidNoise3D(res, freq, data, scale);
			
			fprintf(stdout, "octave %d computed.\n", level + 1);
			fflush(stdout);
		}
		
		freq*= 2;
		scale*= noise_properties->roughness;
	}
	
	if (data)
	{
		float min_v= data[0];
		float max_v= data[0];
		int i;
		
		for(i= 1; i<res*res*res; i++)
		{
			if (min_v>data[i]) min_v= data[i];
			if (max_v<data[i]) max_v= data[i];
		}
		
		for(i= 0; i<res*res*res; i++)
		{
			data[i]= (data[i] - min_v)/(max_v - min_v);
		}
		
		if (noise_properties->marbling!=0.0f)
		{
			for(i= 0; i<res*res*res; i++)
			{
				data[i]= sinf(PI2*data[i]*noise_properties->marbling)*0.5f + 0.5f;
			}
		}
	}
	else
	{
		data= (float*)calloc(sizeof(float), res*res*res);
		if (!data) fatal_error("out of memory");
	}
	
	return data;
}

static float *SphereNoise(const float *input_noise, int input_res, float *output_noise, int output_res, int face_index)
{
	if (!output_noise)
	{
		output_noise= (float*)calloc(sizeof(float), output_res*output_res);
		if (!output_noise) fatal_error("out of memory");
	}
	
	for (int t= 0; t<output_res; t++)
	for (int s= 0; s<output_res; s++)
	{
		float xf= (float)(2*s - output_res + 1)/(float)output_res;
		float yf= (float)(2*t - output_res + 1)/(float)output_res;
		vec3_t v1;
		
		switch(face_index)
		{
			case 0: v1= vec3_t(+1.0f, +yf, -xf); break; // px
			case 1: v1= vec3_t(+xf, +1.0f, -yf); break; // py
			case 2: v1= vec3_t(-1.0f, +yf, +xf); break; // nx
			case 3: v1= vec3_t(+xf, -1.0f, +yf); break; // ny
			case 4: v1= vec3_t(+xf, +yf, +1.0f); break; // pz
			case 5: v1= vec3_t(-xf, +yf, -1.0f); break; // nz
		}
		
		v1*= 0.5f/sqrtf(v1*v1);
		v1+= vec3_t(0.5f, 0.5f, 0.5f);
		v1*= (float)input_res;
		
		int xi= (int)floorf(v1.x) - 1;
		int yi= (int)floorf(v1.y) - 1;
		int zi= (int)floorf(v1.z) - 1;
		
		v1.x-= floorf(v1.x);
		v1.y-= floorf(v1.y);
		v1.z-= floorf(v1.z);
		
		float tmp[64];
		
		for (int z= 0; z<4; z++)
		for (int y= 0; y<4; y++)
		for (int x= 0; x<4; x++)
		{
			tmp[x + y*4 + z*16]= input_noise[CalcIndex3D(x + xi, y + yi, z + zi, input_res)];
		}
		
		// really stupid - sample 4x4x4 voxels just to do trilinear interpolation
		output_noise[s + t*output_res]= Interpolate3D_Linear(tmp, v1.x, v1.y, v1.z);
	}
	
	return output_noise;
}

/* ---------- prototypes */

static void Initialize(void);

static void WriteTGA(const char *filename, const pixel_t *pixels, short width, short height);
static void WriteTGAf(const char *filename, const float *pixels, short width, short height);
static void WriteTGA_Plate(const char *filename, const pixel_t *pixels, short width, short height, short depth);
static void WriteTGAf_Plate(const char *filename, const float *pixels, short width, short height, short depth);

static void RenderNoiseTexture(int id);

static void GLUT__Display(void);
static void GLUT__Reshape(int width, int height);
static void GLUT__Keyboard(unsigned char key, int x, int y);

static struct noise_properties_t g_noise_properties=
{
	1, // seed
	3, // res=64x64
	0.5f, // roughness
	0, // low_freq_skip
	0, // high_freq_skip
	0.0f, // marbling
	0.0f, // turbulence
	3, // turbulence_res
	0.5f, // turbulence_roughness
	0, // turbulence_low_freq_skip
	0, // turbulence_high_freq_skip
	0.0f, // turbulence_marbling
	0.0f, // turbulence_expshift
	0.0f, // turbulence_offset_x
	0.0f, // turbulence_offset_y
	NULL, // preview_data
	3, // preview_res=64x64
	1, // preview_tiles
	4, // quadratic_sequence_count
    NULL, // filename_GLUI
};

/* ---------- code */

static void Initialize(void)
{
	return;
}

static void WriteTGA_Float(const float *data, int w, int h, const char *path)
{
	FILE *tga= fopen(path, "wb");
	unsigned char *data2= (unsigned char*)malloc(w*h*4);
	
	short num_channels= 3;
	short offset=  0;
	short width=   (short)w;
	short height=  (short)h;
	short unknown= num_channels==3 ? 0x0018/*3-channel*/ : 0x0820/*4-channel*/;
	long  zero[]=  {0x02, 0x00, 0x00, 0x00, 0x00, 0x00};
	
	if (!data2) fatal_error("out of memory");
	if (!tga)   fatal_error("can't open file for writing");
	
	fwrite(&offset,  sizeof(short), 1, tga);
	fwrite(&zero[0], 10, 1, tga);
	fwrite(&width,   sizeof(short), 1, tga);
	fwrite(&height,  sizeof(short), 1, tga);
	fwrite(&unknown, sizeof(short), 1, tga);
	
	for (int i= 0; i<width*height; i++)
	{
		float d= data[i];
		if (d<0.0f) d= 0.0f;
		if (d>1.0f) d= 1.0f;
		
		for (int j= 0; j<num_channels; j++)
		{
			data2[i*num_channels + j]= (unsigned char)floorf(d*255.0f + 0.5f);
		}
	}
	
	fwrite(data2, num_channels, width*height, tga);
	fwrite(&zero[1], 18, 1, tga);
	fwrite("TRUEVISION-XFILE.\0", 18, 1, tga);
	fclose(tga);
	
	return;
}

static void WriteTGA(
	const char *filename,
	const pixel_t *pixels,
	short width,
	short height)
{return;
	FILE *tga= fopen(filename, "wb");
	if (!tga) fatal_error("can't open file for writing", filename);
	pixel_t *line_buffer= (pixel_t*)malloc(sizeof(pixel_t)*width);
	if (!line_buffer) fatal_error("out of memory");
	
	short num_channels= sizeof(pixel_t);
	short offset= 0;
	short unknown= num_channels==3 ? 0x0018/*3-channel*/ : 0x0820/*4-channel*/;
	long  zero[]= {0x02, 0x00, 0x00, 0x00, 0x00, 0x00};
	
	fwrite(&offset,  sizeof(short), 1, tga);
	fwrite(&zero[0], 10, 1, tga);
	fwrite(&width,   sizeof(short), 1, tga);
	fwrite(&height,  sizeof(short), 1, tga);
	fwrite(&unknown, sizeof(short), 1, tga);
	
	for (short y= 0; y<height; y++)
	{
		memcpy(line_buffer, &pixels[y*width], sizeof(pixel_t)*width);
		
		for (short x= 0; x<width; x++)
		{
			pixel_t pixel= line_buffer[x];
			pixel_t a= (pixel>>24)&255;
			pixel_t r= (pixel>>16)&255;
			pixel_t g= (pixel>> 8)&255;
			pixel_t b= (pixel>> 0)&255;
			line_buffer[x]= (a<<24) | (b<<16) | (g<<8) | r;
		}
		
		fwrite(line_buffer, num_channels, width, tga);
	}
	
	fwrite(&zero[1], 18, 1, tga);
	fwrite("TRUEVISION-XFILE.\0", 18, 1, tga);
	fclose(tga);
	free(line_buffer);
	
	return;
}

static void WriteTGAf(
	const char *filename,
	const float *pixels,
	short width,
	short height)
{
	long area= width*height;
	pixel_t *i_pixels= (pixel_t*)malloc(sizeof(pixel_t)*area);
	if (!i_pixels) fatal_error("out of memory");
	
	for (long i= 0; i<area; i++)
	{
		pixel_t a= (pixel_t)floorf(pixels[i]*255.0f + 0.5f);
		pixel_t r= a;
		pixel_t g= a;
		pixel_t b= a;
		
		i_pixels[i]= (a<<24) | (b<<16) | (g<<8) | r;
	}
	
	WriteTGA(filename, i_pixels, width, height);
	free(i_pixels);
	
	return;
}

static void WriteTGA_Plate(
	const char *filename,
	const pixel_t *pixels,
	short width,
	short height,
	short depth)
{
	short plate_width= (width + 3)*depth + 3;
	short plate_height= height + 8;
	short x, y;
	
	pixel_t *plate= (pixel_t*)malloc(sizeof(pixel_t)*plate_width*plate_height);
	if (!plate) fatal_error("out of memory");
	
	// clear plate
	for (y= 0; y<plate_height; y++)
	for (x= 0; x<plate_width; x++)
	{
		if ((y==(height/2 + 4) && (x-1)%(width + 3)==0) || (y==plate_height - 2) || (y==plate_height - 1 && x==1))
		{
			//                           a b g r
			plate[x + y*plate_width]= 0x00ff00ff; // magenta dots (border)
		}
		else if (y==plate_height - 1 && x==2)
		{
			//                           a b g r
			plate[x + y*plate_width]= 0x00ffff00; // cyan dot (transparent)
		}
		else
		{
			//                           a b g r
			plate[x + y*plate_width]= 0x00ff0000; // blue (background)
		}
	}
	
	for (short slice_index= 0; slice_index<depth; slice_index++)
	{
		for (y= 0; y<height; y++)
		{
			memcpy(
				&plate[(y + 4)*plate_width + slice_index*(width + 3) + 3],
				&pixels[width*(y + height*slice_index)],
				sizeof(pixel_t)*width);
		}
	}
	
	WriteTGA(filename, plate, plate_width, plate_height);
	free(plate);
	
	return;
}

static void WriteTGAf_Plate(
	const char *filename,
	const float *pixels,
	short width,
	short height,
	short depth)
{
	long area= width*height*depth;
	pixel_t *i_pixels= (pixel_t*)malloc(sizeof(pixel_t)*area);
	if (!i_pixels) fatal_error("out of memory");
	
	for (long i= 0; i<area; i++)
	{
		pixel_t a= (pixel_t)floorf(pixels[i]*255.0f + 0.5f);
		pixel_t r= a;
		pixel_t g= a;
		pixel_t b= a;
		
		i_pixels[i]= (a<<24) | (b<<16) | (g<<8) | r;
	}
	
	WriteTGA_Plate(filename, i_pixels, width, height, depth);
	free(i_pixels);
	
	return;
}

// i've added this function so that i can debug the damn noise maps..
static pixel_t *CreateRasterizerTexture_VectorNormalization(short resolution);
static pixel_t *CreateRasterizerTexture_VectorNormalization(
	short resolution)
{
	pixel_t *pixels= (pixel_t*)malloc(sizeof(pixel_t)*resolution*resolution*6);
	if (!pixels) fatal_error("out of memory");
	
	for (short face_index= 0; face_index<6; face_index++)
	{
		for (short y= 0; y<resolution; y++)
		{
			for (short x= 0; x<resolution; x++)
			{
				float xf= (float)(2*x - resolution + 1)/(float)resolution;
				float yf= (float)(2*y - resolution + 1)/(float)resolution;
				vec3_t v1;
				
				switch (face_index)
				{
					// negated "yf"...
					case 0: v1= vec3_t(+1.0f, +yf, -xf); break; // px
					case 1: v1= vec3_t(+xf, +1.0f, -yf); break; // py
					case 2: v1= vec3_t(-1.0f, +yf, +xf); break; // nx
					case 3: v1= vec3_t(+xf, -1.0f, +yf); break; // ny
					case 4: v1= vec3_t(+xf, +yf, +1.0f); break; // pz
					case 5: v1= vec3_t(-xf, +yf, -1.0f); break; // nz
				}
				
				pixel_t r= (pixel_t)floorf(0.5f + 255.0f*(v1.x*0.5f + 0.5f));
				pixel_t g= (pixel_t)floorf(0.5f + 255.0f*(v1.y*0.5f + 0.5f));
				pixel_t b= (pixel_t)floorf(0.5f + 255.0f*(v1.z*0.5f + 0.5f));
				
				pixels[x + (y + face_index*resolution)*resolution]= (b<<16) | (g<<8) | r;
			}
		}
	}
	
	return pixels;
}

static float test= 0.0f;

static void RenderNoiseTexture(int id)
{
	short res= 8<<g_noise_properties.res;
	
	srand(g_noise_properties.seed);
	
	switch (id)
	{
		case 1: // 2d
		{
            /*for (int i= 1; i<=4; i++)
            for (int j= 1; j<=10; j++)
            {
                g_noise_properties.roughness= (float)j/10.0f;
                res= 8<<g_noise_properties.res;
                srand(g_noise_properties.seed + i);
                char filename[256];
                sprintf(filename, "%s-%02d-%02d.tga", g_noise_properties.filename_GLUI->get_text(), i, j);
                */

            const char *filename= g_noise_properties.filename_GLUI->get_text();
			    float *noise_2d= PerlinNoise2D(res, &g_noise_properties);
			    WriteTGA_Float(noise_2d, res, res, filename);
			    free(noise_2d);
			    fprintf(stdout, "\"%s\" --> done.\n\n", filename);
			    fflush(stdout);
            //}
			break;
		}
		
		case 2: // 3d
		{
			float *noise_3d= PerlinNoise3D(res, &g_noise_properties);
			WriteTGAf_Plate(g_noise_properties.filename_GLUI->get_text(), noise_3d, res, res, res);
			free(noise_3d);
			fprintf(stdout, "\"%s\" --> done.\n\n", g_noise_properties.filename_GLUI->get_text());
			fflush(stdout);
			break;
		}
		
		case 3: // cube map
		{
			float *noise_3d= PerlinNoise3D(res, &g_noise_properties);
			float *noise_cm= (float*)malloc(sizeof(float)*res*res*6);
			short face_index;
			
			for (face_index= 0; face_index<6; face_index++)
			{
				SphereNoise(noise_3d, res, &noise_cm[face_index*res*res], res, face_index);
			}
			
			WriteTGAf_Plate(g_noise_properties.filename_GLUI->get_text(), noise_cm, res, res, 6);
			
			free(noise_3d);
			free(noise_cm);
			fprintf(stdout, "\"%s\" --> done.\n\n", g_noise_properties.filename_GLUI->get_text());
			fflush(stdout);
			break;
		}
		
		case 4: // preview
		{
			short preview_res= 8<<g_noise_properties.preview_res;
			
			if (g_noise_properties.preview_data)
			{
				free(g_noise_properties.preview_data);
			}
			
			g_noise_properties.preview_data= PerlinNoise2D(preview_res, &g_noise_properties);
		}
		
		case 5: // radial mapping
		{
			// mess with the data
			{
				short size= 8<<g_noise_properties.preview_res;
				
				float *data2= g_noise_properties.preview_data;
			/*
			short x, y;
				float *data1= g_noise_properties.preview_data;
				float *data2= (float*)malloc(sizeof(float)*size*size);
				
				for (y= 0; y<size; y++)
				{
					for (x= 0; x<size; x++)
					{
						float xf= -1.0f + 2.0f*(float)x/(float)size;
						float yf= -1.0f + 2.0f*(float)y/(float)size;
						float r= sqrtf(xf*xf + yf*yf);
						float theta= atan2f(xf, yf);
						
						xf= fmodf(test, 1.0f);
						yf= theta/PI2 + 0.5f;
						
						float q= min(1.0f,
							powf(Sample2D(data1, size, size, xf, yf), 8.0f*r + 1.0f)*
							powf(max(0.0f, 1.0f - r), 2.0f) +
						min(powf(max(0.0f, 1.0f - r), 8.0f)*2.0f, 1.0f));
						
						data2[x + y*size]= q;
					}
				}
			*/	
				glTexImage2D(
					GL_TEXTURE_2D, // GLenum target,
					0,             // GLint level,
					GL_LUMINANCE8, // GLint internalformat,
					(GLsizei)size, // GLsizei width,
					(GLsizei)size, // GLsizei height,
					0,             // GLint border,
					GL_LUMINANCE,  // GLenum format,
					GL_FLOAT,      // GLenum type,
					(GLvoid*)data2); // const GLvoid *pixels
				
			//	free(data2);
			}
			
			glutPostRedisplay();
			break;
		}
		
		case 6: // quadratic sequence
		{
			float *noise_2d[16];
			short count= g_noise_properties.quadratic_sequence_count;
			long resolution= 8<<g_noise_properties.res;
			pixel_t *pixels= (pixel_t*)malloc(sizeof(pixel_t)*resolution*resolution*count);
			short frame;
			
			long seed= g_noise_properties.seed;
			
			// generate a sequence of noise maps
			// (currently unrelated - eventually we might allow the option for correlated sequences)
			for (frame= 0; frame<count; frame++)
			{
				g_noise_properties.seed= seed + frame;
				noise_2d[frame]= PerlinNoise2D(res, &g_noise_properties);
			}
			
			// restore random seed
			g_noise_properties.seed= seed;
			
			// compute RGB maps: R= ([n-1]+[n])/2, G= n, B= ([n]+[n+1])/2
			for (frame= 0; frame<count; frame++)
			{
				long x, y;
				for (y= 0; y<resolution; y++)
				for (x= 0; x<resolution; x++)
				{
					float *noise_A= noise_2d[(frame+count-1)%count];
					float *noise_B= noise_2d[frame];
					float *noise_C= noise_2d[(frame+1)%count];
					long offset= x + y*resolution;
					
					pixel_t r= (pixel_t)PIN(floorf((noise_A[offset] + noise_B[offset])*128.0f), 0, 255);
					pixel_t g= (pixel_t)PIN(floorf((          noise_B[offset]        )*256.0f), 0, 255);
					pixel_t b= (pixel_t)PIN(floorf((noise_B[offset] + noise_C[offset])*128.0f), 0, 255);
					
					pixels[offset + frame*resolution*resolution]= (b<<16) | (g<<8) | r;
				}	
			}
			
			WriteTGA_Plate(g_noise_properties.filename_GLUI->get_text(), pixels, resolution, resolution, count);
			
			free(pixels);
			
			for (frame= 0; frame<count; frame++)
			{
				free(noise_2d[frame]);
			}
			
			fprintf(stdout, "\"%s\" --> done.\n\n", g_noise_properties.filename_GLUI->get_text());
			fflush(stdout);
			break;
		}
	}
	
	return;
}

/* ---------- GLUT */

static void GLUT__Display(void)
{
	if (g_noise_properties.preview_data)
	{
		short preview_res= 8<<g_noise_properties.preview_res;
		float preview_tiles= (float)g_noise_properties.preview_tiles;
		float oores= 0.5f/(float)preview_res;
		
		glEnable(GL_TEXTURE_2D);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		
		//glClear(GL_COLOR_BUFFER_BIT);
		glBegin(GL_QUADS);
		glTexCoord2f(oores, oores); glVertex2f(-1.0f, -1.0f);
		glTexCoord2f(preview_tiles - oores, oores); glVertex2f(+1.0f, -1.0f);
		glTexCoord2f(preview_tiles - oores, preview_tiles - oores); glVertex2f(+1.0f, +1.0f);
		glTexCoord2f(oores, preview_tiles - oores); glVertex2f(-1.0f, +1.0f);
		glEnd();
		glFlush();
		glutSwapBuffers();
	}
	
	return;
}

static void GLUT__Reshape(int width, int height)
{
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	
	return;
}

static void GLUT__Keyboard(unsigned char key, int x, int y)
{
	if (key==27)
	{
		exit(0);
	}
	
	return;
}

static int main_window_id;
static void GLUT__Idle(void)
{
	glutSetWindow(main_window_id);
	//glutPostRedisplay();
}

/* ---------- main */

int main(int argc, char *argv[])
{
	// initialize GLUT/OpenGL
	glutInit(&argc, argv);
	
	// create a window
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
	glutInitWindowSize(512, 512);
	glutInitWindowPosition(600, 450);
	main_window_id= glutCreateWindow(argv[0]);
	
	// load textures and resources
	Initialize();
	
	// set up GLUT callbacks
	glutDisplayFunc(GLUT__Display);
	glutReshapeFunc(GLUT__Reshape);
	glutKeyboardFunc(GLUT__Keyboard);
	
	// set up GLUI interface
	GLUI_Master.set_glutIdleFunc(GLUT__Idle);
	{
		GLUI *glui= GLUI_Master.create_glui("Noise Properties", 0, 100, 450);
		glui->set_main_gfx_window(main_window_id);
		
		GLUI_Panel *panel1= glui->add_panel("");
		{
			panel1->set_alignment(GLUI_ALIGN_LEFT);
			
			glui->add_spinner_to_panel(panel1, "random seed:", GLUI_SPINNER_INT, &g_noise_properties.seed, 4, RenderNoiseTexture)->set_int_limits(1, 0x7fffffff);
			
			GLUI_Listbox *listbox= glui->add_listbox_to_panel(panel1, "resolution:", &g_noise_properties.res, 4, RenderNoiseTexture);
			{
				listbox->add_item(0, "8");
				listbox->add_item(1, "16");
				listbox->add_item(2, "32");
				listbox->add_item(3, "64");
				listbox->add_item(4, "128");
				listbox->add_item(5, "256");
				listbox->add_item(6, "512");
				listbox->add_item(7, "1024");
				listbox->add_item(8, "2048");
				listbox->add_item(9, "4096");
			}
			
			glui->add_spinner_to_panel(panel1, "roughness:", GLUI_SPINNER_FLOAT, &g_noise_properties.roughness, 4, RenderNoiseTexture)->set_float_limits(0.001f, 1.0f);
			glui->add_spinner_to_panel(panel1, "low frequency skip:", GLUI_SPINNER_INT, &g_noise_properties.low_freq_skip, 4, RenderNoiseTexture)->set_int_limits(0, 12);
			glui->add_spinner_to_panel(panel1, "high frequency skip:", GLUI_SPINNER_INT, &g_noise_properties.high_freq_skip, 4, RenderNoiseTexture)->set_int_limits(0, 12);
			glui->add_spinner_to_panel(panel1, "marbling:", GLUI_SPINNER_FLOAT, &g_noise_properties.marbling, 4, RenderNoiseTexture)->set_float_limits(0.0f, 50.0f);
			glui->add_spinner_to_panel(panel1, "turbulence:", GLUI_SPINNER_FLOAT, &g_noise_properties.turbulence, 4, RenderNoiseTexture)->set_float_limits(0.0f, 64.0f);
			GLUI_Listbox *turbulence_res_listbox= glui->add_listbox_to_panel(panel1, "turbulence res:", &g_noise_properties.turbulence_res, 4, RenderNoiseTexture);
			{
				turbulence_res_listbox->add_item(0, "8");
				turbulence_res_listbox->add_item(1, "16");
				turbulence_res_listbox->add_item(2, "32");
				turbulence_res_listbox->add_item(3, "64");
				turbulence_res_listbox->add_item(4, "128");
				turbulence_res_listbox->add_item(5, "256");
				turbulence_res_listbox->add_item(6, "512");
			}

			glui->add_spinner_to_panel(panel1, "turbulence roughness:", GLUI_SPINNER_FLOAT, &g_noise_properties.turbulence_roughness, 4, RenderNoiseTexture)->set_float_limits(0.001f, 1.0f);
			glui->add_spinner_to_panel(panel1, "turbulence low frequency skip:", GLUI_SPINNER_INT, &g_noise_properties.turbulence_low_freq_skip, 4, RenderNoiseTexture)->set_int_limits(0, 12);
			glui->add_spinner_to_panel(panel1, "turbulence high frequency skip:", GLUI_SPINNER_INT, &g_noise_properties.turbulence_high_freq_skip, 4, RenderNoiseTexture)->set_int_limits(0, 12);
			glui->add_spinner_to_panel(panel1, "turbulence marbling:", GLUI_SPINNER_FLOAT, &g_noise_properties.turbulence_marbling, 4, RenderNoiseTexture)->set_float_limits(0.0f, 50.0f);
			glui->add_spinner_to_panel(panel1, "turbulence expshift:", GLUI_SPINNER_FLOAT, &g_noise_properties.turbulence_expshift, 4, RenderNoiseTexture)->set_float_limits(-4.0f, 4.0f);
			glui->add_spinner_to_panel(panel1, "turbulence offset x:", GLUI_SPINNER_FLOAT, &g_noise_properties.turbulence_offset_x, 4, RenderNoiseTexture)->set_float_limits(-1.0f, 1.0f);
			glui->add_spinner_to_panel(panel1, "turbulence offset y:", GLUI_SPINNER_FLOAT, &g_noise_properties.turbulence_offset_y, 4, RenderNoiseTexture)->set_float_limits(-1.0f, 1.0f);
			glui->add_spinner_to_panel(panel1, "quadratic sequence count:", GLUI_SPINNER_INT, &g_noise_properties.quadratic_sequence_count)->set_int_limits(2, 16);
		}
		
		glui->add_column(FALSE);
		
		GLUI_Panel *panel2= glui->add_panel("");
		{
			panel2->set_alignment(GLUI_ALIGN_LEFT);
			
			g_noise_properties.filename_GLUI= glui->add_edittext_to_panel(panel2, "filename:", GLUI_EDITTEXT_TEXT);
            g_noise_properties.filename_GLUI->set_text("noise.tga");
			glui->add_button_to_panel(panel2, "RENDER 2D TEXTURE", 1, RenderNoiseTexture)->set_alignment(GLUI_ALIGN_LEFT);
			glui->add_button_to_panel(panel2, "RENDER 3D TEXTURE", 2, RenderNoiseTexture)->set_alignment(GLUI_ALIGN_LEFT);
			glui->add_button_to_panel(panel2, "RENDER CUBE MAP",   3, RenderNoiseTexture)->set_alignment(GLUI_ALIGN_LEFT);
			glui->add_button_to_panel(panel2, "RENDER QUADRATIC SEQUENCE", 6, RenderNoiseTexture)->set_alignment(GLUI_ALIGN_LEFT);
			
			glui->add_separator_to_panel(panel2);
			
			GLUI_Listbox *listbox= glui->add_listbox_to_panel(panel2, "preview res:", &g_noise_properties.preview_res, 4, RenderNoiseTexture);
			{
				listbox->add_item(0, "8");
				listbox->add_item(1, "16");
				listbox->add_item(2, "32");
				listbox->add_item(3, "64");
				listbox->add_item(4, "128");
				listbox->add_item(5, "256");
				listbox->add_item(6, "512");
			}
			
			glui->add_spinner_to_panel(panel2, "preview tiles:", GLUI_SPINNER_INT, &g_noise_properties.preview_tiles, 4, RenderNoiseTexture)->set_int_limits(1, 4);
			glui->add_button_to_panel(panel2, "RENDER PREVIEW", 4, RenderNoiseTexture)->set_alignment(GLUI_ALIGN_LEFT);
			
			glui->add_spinner_to_panel(panel2, "test:", GLUI_SPINNER_FLOAT, &test, 5, RenderNoiseTexture)->set_float_limits(0.0f, 1.0f);
		}
	}
	
	// start
	GLUI_Master.sync_live_all();
	RenderNoiseTexture(4); // render preview
	glutMainLoop();
	return 0;
}
