set EXEC=X:\gta5\src\dev_ng\tools\StatsToProfileStats\StatsToProfileStats\StatsToProfileStats.exe

set IN1=X:\gta5\titleupdate\dev\common\data\spStatsSetup.xml
set IN2=X:\gta5\titleupdate\dev\common\data\mpStatsSetup.xml

set BEACH1=X:\gta5\titleupdate\dev\dlc_patch\mpBeach\common\data\spStatsSetup.xml
set BUSINESS1=X:\gta5_dlc\mpPacks\mpBusiness\build\dev\common\data\spStatsSetup.xml
set BUSINESS2=X:\gta5_dlc\mpPacks\mpBusiness\build\dev\common\data\mpStatsSetup.xml
set BUSINESS1_2=X:\gta5_dlc\mpPacks\mpBusiness2\build\dev\common\data\spStatsSetup.xml
set BUSINESS2_2=X:\gta5_dlc\mpPacks\mpBusiness2\build\dev\common\data\mpStatsSetup.xml
set HIPSTER1=X:\gta5_dlc\mpPacks\mpHipster\build\dev\common\data\spStatsSetup.xml
set HIPSTER2=X:\gta5_dlc\mpPacks\mpHipster\build\dev\common\data\mpStatsSetup.xml
set VALENTINE1=X:\gta5_dlc\mpPacks\mpValentines\build\dev\common\data\spStatsSetup.xml
set VALENTINE2=X:\gta5_dlc\mpPacks\mpValentines\build\dev\common\data\mpStatsSetup.xml
set PILOT2=X:\gta5_dlc\mpPacks\mpPilot\build\dev\common\data\mpStatsSetup.xml
set INDEPENDENCE1=X:\gta5_dlc\mpPacks\mpIndependence\build\dev\common\data\spStatsSetup.xml
set INDEPENDENCE2=X:\gta5_dlc\mpPacks\mpIndependence\build\dev\common\data\mpStatsSetup.xml
set LTS1=X:\gta5_dlc\mpPacks\mpLTS\build\dev\common\data\spStatsSetup.xml
set LTS2=X:\gta5_dlc\mpPacks\mpLTS\build\dev\common\data\mpStatsSetup.xml
set HEIST1=X:\gta5_dlc\mpPacks\mpHeist\build\dev\common\data\spStatsSetup.xml
set HEIST2=X:\gta5_dlc\mpPacks\mpHeist\build\dev\common\data\mpStatsSetup.xml
set CNC2=X:\gta5_dlc\mpPacks\mpCNC\build\dev\common\data\mpStatsSetup.xml
set CHRISTMAS1=X:\gta5_dlc\mpPacks\mpChristmas2\build\dev\common\data\spStatsSetup.xml
set CHRISTMAS2=X:\gta5_dlc\mpPacks\mpChristmas2\build\dev\common\data\mpStatsSetup.xml
set LUX1=X:\gta5_dlc\mpPacks\mpLuxe\build\dev\common\data\spStatsSetup.xml
set LUX2=X:\gta5_dlc\mpPacks\mpLuxe\build\dev\common\data\mpStatsSetup.xml
set LUX21=X:\gta5_dlc\mpPacks\mpLuxe2\build\dev\common\data\spStatsSetup.xml
set LUX22=X:\gta5_dlc\mpPacks\mpLuxe2\build\dev\common\data\mpStatsSetup.xml

set OUTXBL=X:\gta5\xlast\profilestats_xbl.dev.csv
set OUTPSN=X:\gta5\xlast\profilestats_psn.dev.csv

p4 edit X:\user\miguel.freitas\C#\StatsToProfileStats\TitleUpdate_generate_profile_stats.bat
p4 edit %OUTXBL%
p4 edit %OUTPSN%

%EXEC% -out %OUTXBL% -platform xbl -in %IN1% -in %IN2% -in %LTS1% -in %LTS2% -in %INDEPENDENCE1% -in %INDEPENDENCE2% -in %BEACH1% -in %HIPSTER1% -in %HIPSTER2% -in %BUSINESS1% -in %BUSINESS2% -in %BUSINESS1_2% -in %BUSINESS2_2% -in %VALENTINE1% -in %VALENTINE2% -in %CHRISTMAS1%  -in %CHRISTMAS2% -in %HEIST1% -in %HEIST2% -in %PILOT2%  -in %LUX1% -in %LUX2% -in %LUX21% -in %LUX22%
%EXEC% -out %OUTPSN% -platform psn -in %IN1% -in %IN2% -in %LTS1% -in %LTS2% -in %INDEPENDENCE1% -in %INDEPENDENCE2% -in %BEACH1% -in %HIPSTER1% -in %HIPSTER2% -in %BUSINESS1% -in %BUSINESS2% -in %BUSINESS1_2% -in %BUSINESS2_2% -in %VALENTINE1% -in %VALENTINE2% -in %CHRISTMAS1%  -in %CHRISTMAS2% -in %HEIST1% -in %HEIST2% -in %PILOT2%  -in %LUX1% -in %LUX2% -in %LUX21% -in %LUX22%

pause
