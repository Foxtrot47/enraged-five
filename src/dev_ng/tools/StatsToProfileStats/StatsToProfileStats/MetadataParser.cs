﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

namespace Rockstar.Ros
{
    //PURPOSE
    //  Describes all the profile stats.

    [XmlRoot("ProfileStats")]
    public class MetadataParser
    {
        //Describes a single stat in the set.
        public struct Stat
        {
            [XmlAttribute("Id")]
            public Int64 Id;

            [XmlAttribute("Name")]
            public string Name;

            [XmlAttribute("Type")]
            public string Type;

            [XmlAttribute("Group")]
            public string Group;

            [XmlAttribute("Default")]
            public string DefaultValue;

            [XmlAttribute("Min")]
            public string MinValue;

            [XmlAttribute("Max")]
            public string MaxValue;

            [XmlAttribute("MaxVelocitySeconds")]
            public string MaxVelocitySeconds;

            [XmlAttribute("MaxVelocityDelta")]
            public string MaxVelocityDelta;

            [XmlAttribute("UserData")]
            public string UserData;

            [XmlAttribute("Comment")]
            public string Comment;
        }

        [XmlElement("Stat")]
        public List<Stat> Stats = new List<Stat>();

        //PURPOSE
        //  Converts our uint hash value of the name to the int64 version.
        private static Int64 ConvertToInt64(uint v)
        {
            if (v > int.MaxValue)
            {
                Int64 ret = 0xFFFFFFFF;
                ret = (ret << 32);
                ret = ret | v;
                return ret;
            }

            return v;
        }

        //PURPOSE
        //  Reads a xml file and looks for all elements that should be uploaded to the profile stats 
        //    server and adds then to out list containing all profile stats.
        public bool ReadInXml(string xmlFile, string platform)
        {
            try
            {
                bool isForPsn = platform.ToUpper().Equals("PSN", StringComparison.InvariantCultureIgnoreCase);
                bool isForXbl = platform.ToUpper().Equals("XBL", StringComparison.InvariantCultureIgnoreCase);
                bool isForPc  = platform.ToUpper().Equals("PC", StringComparison.InvariantCultureIgnoreCase);

                bool isForProspero = platform.ToUpper().Equals("PR", StringComparison.InvariantCultureIgnoreCase);
                bool isForScarllet = platform.ToUpper().Equals("SC", StringComparison.InvariantCultureIgnoreCase);

                bool isForGen9 = isForProspero || isForProspero;

                // Load the config file into a document
                XmlDocument doc = new XmlDocument();
                XmlTextReader xmlReader = new XmlTextReader(xmlFile);

                // load the data into the document
                doc.Load(xmlReader);

                // Configure using the 'log4net' element
                XmlNodeList configNodeList = doc.GetElementsByTagName("stat");
                if (configNodeList.Count == 0)
                {
                    Console.Write("\nXML file [" + xmlFile + "] does not contain <stat> element. Load Aborted.");
                }
                else if (configNodeList.Count > 1)
                {
                    Console.Write("\n");
                    Console.Write("\nXML file [" + xmlFile + "] contains [" + configNodeList.Count + "] <stat> elements.");
                    Console.Write("\n");

                    for (int i = 0; i < configNodeList.Count; i++)
                    {
                        XmlElement element = configNodeList[i] as XmlElement;

                        string multihash = element.GetAttribute("Multihash");
                        string name = element.GetAttribute("Name");
                        string type = element.GetAttribute("Type");
                        string group = element.GetAttribute("Group");
                        string profilestat = element.HasAttribute("profile") ? element.GetAttribute("profile") : "false";
                        string def = element.HasAttribute("Default") ? element.GetAttribute("Default") : "0";
                        string owner = element.HasAttribute("Owner") ? element.GetAttribute("Owner") : "script";
                        string maxVelocitySeconds = element.GetAttribute("MaxVelocitySeconds");
                        string maxVelocityDelta = element.GetAttribute("MaxVelocityDelta");
                        string userData = element.HasAttribute("UserData") ? element.GetAttribute("UserData") : "STAT_FLAG_NEVER_SHOW";
                        string comment = element.HasAttribute("Comment") ? element.GetAttribute("Comment") : "None";
                        string characterStat =  element.HasAttribute("characterStat") ? element.GetAttribute("characterStat") : "false";
                        string online =  element.HasAttribute("online") ? element.GetAttribute("online") : "false";
                        string gen9Stat =  element.HasAttribute("Flag") ? element.GetAttribute("Flag") : "false";

                        if (name == null || name.Equals(""))
                            throw new ArgumentException("Name argument is NULL");
                        else if (type == null || type.Equals(""))
                            throw new ArgumentException("Type argument is NULL");
                        else if (profilestat == null || profilestat.Equals(""))
                            throw new ArgumentException("profile argument is NULL");
                        else if (def == null || def.Equals(""))
                            throw new ArgumentException("Default argument is NULL");

                        if (gen9Stat.ToUpper().Equals("FEATURE_GEN9_EXCLUSIVE", StringComparison.InvariantCultureIgnoreCase))
                        {
                            if(!isForGen9)
                            {
                                continue;
                            }
                        }

                        if (profilestat.ToUpper().Equals("TRUE", StringComparison.InvariantCultureIgnoreCase))
                        {
                            Stat s = new Stat();

                            if (multihash.Length == 0)
                            {
                                s.Id = MetadataParser.ConvertToInt64(atString.atStringHash(name, 0));
                                s.Name = name;
                            }
                            else
                            {
                                s.Id = MetadataParser.ConvertToInt64(atString.atStringHash(multihash, atString.atStringHash(name, 0)));
                                s.Name = name + multihash;
                            }
                            s.Type = StatDefinition.GetRosTypeString(type);
                            s.DefaultValue = def;
                            if (type.ToUpper().Equals("STRING", StringComparison.InvariantCultureIgnoreCase) && !def.Equals("0"))
                            {
                                Int64 defValue = MetadataParser.ConvertToInt64(atString.atStringHash(s.DefaultValue, 0));
                                s.DefaultValue = Convert.ToString(defValue);
                            }
                            else if (type.ToUpper().Equals("BOOL", StringComparison.InvariantCultureIgnoreCase))
                            {
                                s.DefaultValue = (def.ToUpper().Equals("FALSE") || def.ToUpper().Equals("TRUE")) ? "0" : def;
                            }
                            s.MaxVelocitySeconds = maxVelocitySeconds;
                            s.MaxVelocityDelta = maxVelocityDelta;
                            s.UserData = userData;
                            s.Comment = comment;
                            s.MinValue = "";
                            s.MaxValue = "";

                            bool grpIsSet = (group != null && !group.Equals(""));
                            if (grpIsSet)
                            {
                                s.Group = group;
                            }
                            else
                            {
                                if (!characterStat.ToUpper().Equals("TRUE", StringComparison.InvariantCultureIgnoreCase))
                                {
                                    if (online.ToUpper().Equals("TRUE", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        s.Group = ((int)CsvGroups.GRP_MP_0).ToString();

			                             //MPPLY
                                        if ('M' == s.Name[0] && 'P' == s.Name[1] && 'P' == s.Name[2] && 'L' == s.Name[3] && 'Y' == s.Name[4] && '_' == s.Name[5])
			                            {
				                            s.Group = ((int)CsvGroups.GRP_MP_6).ToString();
			                            }
			                            //MPGEN
                                        else if ('M' == s.Name[0] && 'P' == s.Name[1] && 'G' == s.Name[2] && 'E' == s.Name[3] && 'N' == s.Name[4] && '_' == s.Name[5])
			                            {
				                            s.Group = ((int)CsvGroups.GRP_MP_7).ToString();
			                            }
                                    }
                                    else
                                    {
                                        s.Group = ((int)CsvGroups.GRP_SP_0).ToString();
                                    }
                                }
                            }

                            //If this is a user id stat then we need 2 u64 to accomodate the stat value
                            bool isUserId = type.ToUpper().Equals("USERID", StringComparison.InvariantCultureIgnoreCase);

                            if (characterStat.ToUpper().Equals("TRUE", StringComparison.InvariantCultureIgnoreCase))
                            {
                                if (online.ToUpper().Equals("TRUE", StringComparison.InvariantCultureIgnoreCase))
                                {
                                    if (!grpIsSet)
                                    {
                                        s.Group = ((int)CsvGroups.GRP_MP_1).ToString();
                                    }
                                    s.Name = "MP0_" + name;
                                    s.Id = MetadataParser.ConvertToInt64(atString.atStringHash(s.Name, 0));
                                    Stats.Add(s);
                                    if (isUserId)
                                    {
                                        s.Name = "MP0_" + name + "_NEXT";
                                        s.Id = MetadataParser.ConvertToInt64(atString.atStringHash(s.Name, 0));
                                        Stats.Add(s);
                                    }

                                    if (!grpIsSet)
                                    {
                                        s.Group = ((int)CsvGroups.GRP_MP_2).ToString();
                                    }
                                    s.Name = "MP1_" + name;
                                    s.Id = MetadataParser.ConvertToInt64(atString.atStringHash(s.Name, 0)); 
                                    Stats.Add(s);
                                    if (isUserId)
                                    {
                                        s.Name = "MP1_" + name + "_NEXT";
                                        s.Id = MetadataParser.ConvertToInt64(atString.atStringHash(s.Name, 0));
                                        Stats.Add(s);
                                    }

                                }
                                else
                                {
                                    if (!grpIsSet)
                                    {
                                        s.Group = ((int)CsvGroups.GRP_SP_1).ToString();
                                    }
                                    s.Name = "SP0_" + name;
                                    s.Id = MetadataParser.ConvertToInt64(atString.atStringHash(s.Name, 0));
                                    Stats.Add(s);
                                    if (isUserId)
                                    {
                                        s.Name = "SP0_" + name + "_NEXT";
                                        s.Id = MetadataParser.ConvertToInt64(atString.atStringHash(s.Name, 0));
                                        Stats.Add(s);
                                    }

                                    if (!grpIsSet)
                                    {
                                        s.Group = ((int)CsvGroups.GRP_SP_2).ToString();
                                    }
                                    s.Name = "SP1_" + name;
                                    s.Id = MetadataParser.ConvertToInt64(atString.atStringHash(s.Name, 0));
                                    Stats.Add(s);
                                    if (isUserId)
                                    {
                                        s.Name = "SP1_" + name + "_NEXT";
                                        s.Id = MetadataParser.ConvertToInt64(atString.atStringHash(s.Name, 0));
                                        Stats.Add(s);
                                    }

                                    if (!grpIsSet)
                                    {
                                        s.Group = ((int)CsvGroups.GRP_SP_3).ToString();
                                    }
                                    s.Name = "SP2_" + name;
                                    s.Id = MetadataParser.ConvertToInt64(atString.atStringHash(s.Name, 0));
                                    Stats.Add(s);
                                    if (isUserId)
                                    {
                                        s.Name = "SP2_" + name + "_NEXT";
                                        s.Id = MetadataParser.ConvertToInt64(atString.atStringHash(s.Name, 0));
                                        Stats.Add(s);
                                    }
                                }
                            }
                            else
                            {
                                Stats.Add(s);
                            }
                        }
                    }
                }

                return true;
            }
            catch (System.Exception ex)
            {
                Console.Write("\n Error - " + ex.ToString());
                return false;
            }
        }

        //PURPOSE
        //  Creates a csv file with all profile stats stored in the list Stats.
        public bool WriteToCsv(StreamWriter writer)
        {
            try
            {
                List<string[]> output = new List<string[]>();

                // Add the headers
                string[] line = new string[(int)CsvColumn.COLUMN_COUNT];

                line[(int)CsvColumn.Id] = CsvColumn.Id.ToString();
                line[(int)CsvColumn.Name] = CsvColumn.Name.ToString();
                line[(int)CsvColumn.Type] = CsvColumn.Type.ToString();
                line[(int)CsvColumn.Group] = CsvColumn.Group.ToString();
                line[(int)CsvColumn.Default] = CsvColumn.Default.ToString();
                line[(int)CsvColumn.Min] = CsvColumn.Min.ToString();
                line[(int)CsvColumn.Max] = CsvColumn.Max.ToString();
                line[(int)CsvColumn.MaxVelocitySeconds] = CsvColumn.MaxVelocitySeconds.ToString();
                line[(int)CsvColumn.MaxVelocityDelta] = CsvColumn.MaxVelocityDelta.ToString();
                line[(int)CsvColumn.UserData] = CsvColumn.UserData.ToString();
                line[(int)CsvColumn.Comment] = CsvColumn.Comment.ToString();

                output.Add(line);

                for (int i = 0; i < Stats.Count; i++)
                {
                    MetadataParser.Stat stat = Stats[i];

                    line = new string[(int)CsvColumn.COLUMN_COUNT];

                    stat.UserData = stat.UserData.Replace(',', ';');
                    stat.Comment = stat.Comment.Replace(',', ';');

                    line[(int)CsvColumn.Id] = stat.Id.ToString();
                    line[(int)CsvColumn.Name] = stat.Name;
                    line[(int)CsvColumn.Type] = stat.Type;
                    line[(int)CsvColumn.Group] = stat.Group;
                    line[(int)CsvColumn.Default] = stat.DefaultValue.ToString();
                    line[(int)CsvColumn.Min] = (stat.MinValue == null) ? "" : stat.MinValue.ToString();
                    line[(int)CsvColumn.Max] = (stat.MaxValue == null) ? "" : stat.MaxValue.ToString();
                    line[(int)CsvColumn.MaxVelocitySeconds] = stat.MaxVelocitySeconds;
                    line[(int)CsvColumn.MaxVelocityDelta] = stat.MaxVelocityDelta;
                    line[(int)CsvColumn.UserData] = stat.UserData;
                    line[(int)CsvColumn.Comment] = stat.Comment;

                    output.Add(line);
                }

                // Assuming no exception occurred, output the file.
                foreach (string[] aLine in output)
                {
                    writer.WriteLine(string.Join(",", aLine));
                }

                return true;
            }
            catch (System.Exception ex)
            {
                Console.Write("\n Error - " + ex.ToString());
                return false;
            }
        }

        #region private

        //PURPOSE
        //  Enum that describes each column in the csv file.
        enum CsvColumn
        {
            Id = 0
            ,Name
            ,Type
            ,Group
            ,Default
            ,Min
            ,Max
            ,MaxVelocitySeconds
            ,MaxVelocityDelta
            ,UserData
            ,Comment
            ,COLUMN_COUNT
        };

        //PURPOSE
        //  Enum that describes all the groups set to the stats.
        enum CsvGroups
        {
            GRP_MP_START = 0
            ,GRP_MP_0 = GRP_MP_START
            ,GRP_MP_1
            ,GRP_MP_2
            ,GRP_MP_3
            ,GRP_MP_4
            ,GRP_MP_5
            ,GRP_MP_6
            ,GRP_MP_7
            ,GRP_MP_END = GRP_MP_7
            ,GRP_SP_START
            ,GRP_SP_0 = GRP_SP_START
            ,GRP_SP_1
            ,GRP_SP_2
            ,GRP_SP_3
            ,GRP_SP_END = GRP_SP_3
        };
        
        #endregion
    }
}
