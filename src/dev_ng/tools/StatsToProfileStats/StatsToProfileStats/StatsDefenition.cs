﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Rockstar.Ros
{
    //PURPOSE
    //  Defines a single stat.
    public class StatDefinition
    {
        // Data types supported by RockStar Online Services.
        public enum RosDataType
        {
            Invalid = -1,
            Int,
            Float,
            Int64
        };

        // Stats Fields
        public readonly int Id;
        public readonly string Name;
        public readonly RosDataType Type;
        public readonly int Group;
        public readonly object DefaultValue;
        public readonly int VersionId; // ???????????
        public readonly string MaxVelocitySeconds;
        public readonly string MaxVelocityDelta;
        // Can be null.
        public readonly string UserData;
        public readonly string Comment;

        //Constructor
        public StatDefinition(int statId
                                , string statName
                                , int versionId
                                , string type
                                , int group
                                , string defaultValue
                                , string maxVelocitySeconds
                                , string maxVelocityDelta
                                , string userData
                                , string comment)
        {
            Id = statId;
            Name = statName;
            VersionId = versionId;
            Type = GetRosType(type);
            DefaultValue = ParseRosValue(defaultValue);
            MaxVelocitySeconds = maxVelocitySeconds;
            MaxVelocityDelta = maxVelocityDelta;
            UserData = userData;
            Comment = comment;
            Group = group;

            if (string.IsNullOrEmpty(Name)) throw new ArgumentException("statName");
            if (!IsAllowedType(Type)) throw new ArgumentException("type");
            if (null == DefaultValue) throw new ArgumentException("defaultValue");
        }

        // Returns true if the type is allowed for a profile stat.
        public static bool IsAllowedType(RosDataType type)
        {
            return type == RosDataType.Int64
                || type == RosDataType.Float
                || type == RosDataType.Int;
        }

        // Converts a value string to an object of the stat's type.
        // Null or invalid strings will result in a null return value.
        public object ParseRosValue(string val)
        {
            if (string.IsNullOrEmpty(val)) return null;

            switch (Type)
            {
                case RosDataType.Int64: return Int64.Parse(val);
                case RosDataType.Float: return float.Parse(val);
                case RosDataType.Int: return int.Parse(val);

                default: throw new Exception("Unknown data type: " + Type.ToString());
            }
        }

        // Returns the allowed RockStar Online Service Type for a given data-type string.
        public static RosDataType GetRosType(string typeString)
        {
            RosDataType rostype = RosDataType.Invalid;

            if (typeString.ToUpper().Equals("FLOAT", StringComparison.InvariantCultureIgnoreCase))
                rostype = RosDataType.Float;
            else if (typeString.ToUpper().Equals("DOUBLE", StringComparison.InvariantCultureIgnoreCase))
                rostype = RosDataType.Float;
            else if (typeString.ToUpper().Equals("INT", StringComparison.InvariantCultureIgnoreCase))
                rostype = RosDataType.Int;
            else if (typeString.ToUpper().Equals("SHORT", StringComparison.InvariantCultureIgnoreCase))
                rostype = RosDataType.Int;
            else if (typeString.ToUpper().Equals("BOOL", StringComparison.InvariantCultureIgnoreCase))
                rostype = RosDataType.Int;
            //else if (typeString.ToUpper().Equals("STRING", StringComparison.InvariantCultureIgnoreCase))
            //    rostype = RosDataType.Int;
            else if (typeString.ToUpper().Equals("LABEL", StringComparison.InvariantCultureIgnoreCase))
                rostype = RosDataType.Int;
            else if (typeString.ToUpper().Equals("U8", StringComparison.InvariantCultureIgnoreCase))
                rostype = RosDataType.Int;
            else if (typeString.ToUpper().Equals("U16", StringComparison.InvariantCultureIgnoreCase))
                rostype = RosDataType.Int;
            else if (typeString.ToUpper().Equals("U32", StringComparison.InvariantCultureIgnoreCase))
                rostype = RosDataType.Int64;
            else if (typeString.ToUpper().Equals("U64", StringComparison.InvariantCultureIgnoreCase))
                rostype = RosDataType.Int64;
            else if (typeString.ToUpper().Equals("X64", StringComparison.InvariantCultureIgnoreCase))
                rostype = RosDataType.Int64;
            else if (typeString.ToUpper().Equals("DATE", StringComparison.InvariantCultureIgnoreCase))
                rostype = RosDataType.Int64;
            else if (typeString.ToUpper().Equals("POS", StringComparison.InvariantCultureIgnoreCase))
                rostype = RosDataType.Int64;
            else if (typeString.ToUpper().Equals("PACKED", StringComparison.InvariantCultureIgnoreCase))
                rostype = RosDataType.Int64;
            else if (typeString.ToUpper().Equals("USERID", StringComparison.InvariantCultureIgnoreCase))
                rostype = RosDataType.Int64;
            else if (typeString.ToUpper().Equals("PROFILESETTING", StringComparison.InvariantCultureIgnoreCase))
                rostype = RosDataType.Int;
            else if (typeString.ToUpper().Equals("S64", StringComparison.InvariantCultureIgnoreCase))
                rostype = RosDataType.Int64;
            else if (typeString.ToUpper().Equals("LONG", StringComparison.InvariantCultureIgnoreCase))
                rostype = RosDataType.Int64;

            return rostype;
        }

        // Returns the allowed RockStar Online Service Type for a given data-type string.
        public static string GetRosTypeString(string typeString)
        {
            if (typeString.ToUpper().Equals("FLOAT", StringComparison.InvariantCultureIgnoreCase))
                return "float";
            else if (typeString.ToUpper().Equals("DOUBLE", StringComparison.InvariantCultureIgnoreCase))
                return "float";
            else if (typeString.ToUpper().Equals("INT", StringComparison.InvariantCultureIgnoreCase))
                return "int";
            else if (typeString.ToUpper().Equals("SHORT", StringComparison.InvariantCultureIgnoreCase))
                return "int";
            else if (typeString.ToUpper().Equals("BOOL", StringComparison.InvariantCultureIgnoreCase))
                return "int";
            //else if (typeString.ToUpper().Equals("STRING", StringComparison.InvariantCultureIgnoreCase))
            //    return "int";
            else if (typeString.ToUpper().Equals("LABEL", StringComparison.InvariantCultureIgnoreCase))
                return "int";
            else if (typeString.ToUpper().Equals("U8", StringComparison.InvariantCultureIgnoreCase))
                return "int";
            else if (typeString.ToUpper().Equals("U16", StringComparison.InvariantCultureIgnoreCase))
                return "int";
            else if (typeString.ToUpper().Equals("U32", StringComparison.InvariantCultureIgnoreCase))
                return "bigint";
            else if (typeString.ToUpper().Equals("U64", StringComparison.InvariantCultureIgnoreCase))
                return "bigint";
            else if (typeString.ToUpper().Equals("X64", StringComparison.InvariantCultureIgnoreCase))
                return "bigint";
            else if (typeString.ToUpper().Equals("DATE", StringComparison.InvariantCultureIgnoreCase))
                return "bigint";
            else if (typeString.ToUpper().Equals("POS", StringComparison.InvariantCultureIgnoreCase))
                return "bigint";
            else if (typeString.ToUpper().Equals("PACKED", StringComparison.InvariantCultureIgnoreCase))
                return "bigint";
            else if (typeString.ToUpper().Equals("USERID", StringComparison.InvariantCultureIgnoreCase))
                return "bigint";
            else if (typeString.ToUpper().Equals("PROFILESETTING", StringComparison.InvariantCultureIgnoreCase))
                return "int";
            else if (typeString.ToUpper().Equals("S64", StringComparison.InvariantCultureIgnoreCase))
                return "bigint";
            else if (typeString.ToUpper().Equals("LONG", StringComparison.InvariantCultureIgnoreCase))
                return "bigint";

            return "UNKNOWN";
        }
    }
}
