@echo OFF

set EXEC=X:\gta5\src\dev_ng\tools\StatsToProfileStats\StatsToProfileStats\StatsToProfileStats.exe

cls 

set BEACH1=X:\gta5_dlc\mpPacks\mpBeach\build\dev_ng\common\data\spStatsSetup.xml
set VALENTINE1=X:\gta5_dlc\mpPacks\mpValentines\build\dev_ng\common\data\spStatsSetup.xml
set VALENTINE2=X:\gta5_dlc\mpPacks\mpValentines\build\dev_ng\common\data\mpStatsSetup.xml
set BUSINESS1=X:\gta5_dlc\mpPacks\mpBusiness\build\dev_ng\common\data\spStatsSetup.xml
set BUSINESS2=X:\gta5_dlc\mpPacks\mpBusiness\build\dev_ng\common\data\mpStatsSetup.xml
set BUSINESS1_2=X:\gta5_dlc\mpPacks\mpBusiness2\build\dev_ng\common\data\spStatsSetup.xml
set BUSINESS2_2=X:\gta5_dlc\mpPacks\mpBusiness2\build\dev_ng\common\data\mpStatsSetup.xml
set HIPSTER1=X:\gta5_dlc\mpPacks\mpHipster\build\dev_ng\common\data\spStatsSetup.xml
set HIPSTER2=X:\gta5_dlc\mpPacks\mpHipster\build\dev_ng\common\data\mpStatsSetup.xml
set INDEPENDENCE1=X:\gta5_dlc\mpPacks\mpIndependence\build\dev_ng\common\data\spStatsSetup.xml
set INDEPENDENCE2=X:\gta5_dlc\mpPacks\mpIndependence\build\dev_ng\common\data\mpStatsSetup.xml
set PILOT1=X:\gta5_dlc\mpPacks\mpPilot\build\dev_ng\common\data\spStatsSetup.xml
set PILOT2=X:\gta5_dlc\mpPacks\mpPilot\build\dev_ng\common\data\mpStatsSetup.xml
set LTS1=X:\gta5_dlc\mpPacks\mpLTS\build\dev_ng\common\data\spStatsSetup.xml
set LTS2=X:\gta5_dlc\mpPacks\mpLTS\build\dev_ng\common\data\mpStatsSetup.xml
set HEIST1=X:\gta5_dlc\mpPacks\mpHeist\build\dev_ng\common\data\spStatsSetup.xml
set HEIST2=X:\gta5_dlc\mpPacks\mpHeist\build\dev_ng\common\data\mpStatsSetup.xml
rem set CNC2=X:\gta5_dlc\mpPacks\mpCNC\build\dev_ng\common\data\mpStatsSetup.xml
set SPUPGRADE1=X:\gta5\titleupdate\dev_ng\dlc_patch\spUpgrade\common\data\mpStatsSetup.xml
set SPUPGRADE2=X:\gta5_dlc\spPacks\spUpgrade\build\dev_ng\common\data\spStatsSetup.xml
set CHRISTMAS1=X:\gta5_dlc\mpPacks\mpChristmas2\build\dev_ng\common\data\spStatsSetup.xml
set CHRISTMAS2=X:\gta5_dlc\mpPacks\mpChristmas2\build\dev_ng\common\data\mpStatsSetup.xml
set LUX1=X:\gta5_dlc\mpPacks\mpLuxe\build\dev_ng\common\data\spStatsSetup.xml
set LUX2=X:\gta5_dlc\mpPacks\mpLuxe\build\dev_ng\common\data\mpStatsSetup.xml
set LUX21=X:\gta5_dlc\mpPacks\mpLuxe2\build\dev_ng\common\data\spStatsSetup.xml
set LUX22=X:\gta5_dlc\mpPacks\mpLuxe2\build\dev_ng\common\data\mpStatsSetup.xml
set LOWRIDER1=X:\gta5_dlc\mpPacks\mpLowrider\build\dev_ng\common\data\spStatsSetup.xml
set LOWRIDER2=X:\gta5_dlc\mpPacks\mpLowrider\build\dev_ng\common\data\mpStatsSetup.xml
set LOWRIDER21=X:\gta5_dlc\mpPacks\mpLowrider2\build\dev_ng\common\data\spStatsSetup.xml
set LOWRIDER22=X:\gta5_dlc\mpPacks\mpLowrider2\build\dev_ng\common\data\mpStatsSetup.xml
set HALLOWEEN1=X:\gta5_dlc\mpPacks\mpHalloween\build\dev_ng\common\data\spStatsSetup.xml
set HALLOWEEN2=X:\gta5_dlc\mpPacks\mpHalloween\build\dev_ng\common\data\mpStatsSetup.xml
rem set APARTMENT1=X:\gta5_dlc\mpPacks\mpApartment\build\dev_ng\common\data\spStatsSetup.xml   -- overriden with the file below
set APARTMENT1=x:\gta5\titleupdate\dev_ng\dlc_patch\mpApartment\common\data\spStatsSetup.xml
set APARTMENT2=X:\gta5_dlc\mpPacks\mpApartment\build\dev_ng\common\data\mpStatsSetup.xml
rem set BIKER1=X:\gta5_dlc\mpPacks\mpBiker\build\dev_ng\common\data\spStatsSetup.xml -- No SP weapon for now
set BIKER2=X:\gta5_dlc\mpPacks\mpBiker\build\dev_ng\common\data\mpStatsSetup.xml
set IMPEXP2=X:\gta5_dlc\mpPacks\mpImportExport\build\dev_ng\common\data\mpStatsSetup.xml
rem set GUNRUNNING=X:\gta5_dlc\mpPacks\mpGunrunning\build\dev_ng\common\data\mpStatsSetup.xml  -- overriden with the file below
set GUNRUNNING=X:\gta5\titleupdate\dev_ng\dlc_patch\mpGunrunning\common\data\mpStatsSetup.xml
set SMUGGLER=X:\gta5_dlc\mpPacks\mpSmuggler\build\dev_ng\common\data\mpStatsSetup.xml
set CHRISTMAS2017=X:\gta5_dlc\mpPacks\mpChristmas2017\build\dev_ng\common\data\mpStatsSetup.xml
set ASSAULT=X:\gta5_dlc\mpPacks\mpAssault\build\dev_ng\common\data\mpStatsSetup.xml
set BATTLE=X:\gta5_dlc\mpPacks\mpBattle\build\dev_ng\common\data\mpStatsSetup.xml
set ARENA=X:\gta5_dlc\mpPacks\mpChristmas2018\build\dev_ng\Common\Data\mpStatsSetup.xml
set VINEWOOD=X:\gta5_dlc\mpPacks\mpVinewood\build\dev_ng\Common\data\mpStatsSetup.xml
set HEIST3=X:\gta5_dlc\mpPacks\mpHeist3\build\dev_ng\common\data\mpStatsSetup.xml
set COPSANDCROOKS=x:\gta5_dlc\mpPacks\mpArc1\build\dev_ng\common\data\mpStatsSetup.xml
set MPSUM=x:\gta5_dlc\mpPacks\mpSum\build\dev_ng\common\data\mpStatsSetup.xml
set HEIST4=x:\gta5_dlc\mpPacks\mpHeist4\build\dev_ng\common\data\mpStatsSetup.xml
set TUNER=x:\gta5_dlc\mpPacks\mpTuner\build\dev_ng\common\data\mpStatsSetup.xml
set MPG9EC=x:\gta5_dlc\mpPacks\mpG9EC\build\dev_ng\common\data\mpStatsSetup.xml
set SECURITY=x:\gta5_dlc\mpPacks\mpSecurity\build\dev_ng\common\data\mpStatsSetup.xml
set MPSUM22=x:\gta5_dlc\mpPacks\mpSum2\build\dev_ng\common\data\mpStatsSetup.xml

set IN1=X:\gta5\titleupdate\dev_ng\common\data\spStatsSetup.xml
set IN2=X:\gta5\titleupdate\dev_ng\common\data\mpStatsSetup.xml

set OUT_NGCONSOLE=X:\gta5\xlast\profilestats_ng.dev.csv
set OUTPC=X:\gta5\xlast\profilestats_pc.dev.csv


p4 edit %OUT_NGCONSOLE%
%EXEC% -out %OUT_NGCONSOLE%  -platform pc  -in %IN1% -in %IN2% -in %LTS1% -in %LTS2% -in %INDEPENDENCE1% -in %INDEPENDENCE2% -in %BEACH1% -in %HIPSTER1% -in %HIPSTER2% -in %BUSINESS1% -in %BUSINESS2% -in %BUSINESS1_2% -in %BUSINESS2_2% -in %VALENTINE1% -in %VALENTINE2% -in %CHRISTMAS1%  -in %CHRISTMAS2% -in %PILOT2% -in %SPUPGRADE1% -in %SPUPGRADE2% -in %HEIST1%  -in %HEIST2% -in %LUX1% -in %LUX2% -in %LUX21% -in %LUX22% -in %LOWRIDER1% -in %LOWRIDER2% -in %HALLOWEEN1% -in %HALLOWEEN2% -in %LOWRIDER21% -in %LOWRIDER22% -in %APARTMENT1% -in %APARTMENT2% -in %BIKER2% -in %IMPEXP2% -in %GUNRUNNING% -in %SMUGGLER% -in %CHRISTMAS2017% -in %ASSAULT% -in %BATTLE% -in %ARENA% -in %VINEWOOD% -in %HEIST3% -in %COPSANDCROOKS% -in %MPSUM% -in %HEIST4% -in %TUNER% -in %MPG9EC% -in %SECURITY% -in %MPSUM22%

p4 edit %OUTPC%
%EXEC% -out %OUTPC%  -platform pc  -in %IN1% -in %IN2% -in %LTS1% -in %LTS2% -in %INDEPENDENCE1% -in %INDEPENDENCE2% -in %BEACH1% -in %HIPSTER1% -in %HIPSTER2% -in %BUSINESS1% -in %BUSINESS2% -in %BUSINESS1_2% -in %BUSINESS2_2% -in %VALENTINE1% -in %VALENTINE2% -in %CHRISTMAS1%  -in %CHRISTMAS2% -in %PILOT2% -in %SPUPGRADE1% -in %SPUPGRADE2% -in %HEIST1%  -in %HEIST2% -in %LUX1% -in %LUX2%  -in %LUX21% -in %LUX22% -in %LOWRIDER1% -in %LOWRIDER2% -in %HALLOWEEN1% -in %HALLOWEEN2% -in %LOWRIDER21% -in %LOWRIDER22% -in %APARTMENT1% -in %APARTMENT2% -in %BIKER2% -in %IMPEXP2% -in %GUNRUNNING% -in %SMUGGLER% -in %CHRISTMAS2017% -in %ASSAULT% -in %BATTLE% -in %ARENA% -in %VINEWOOD% -in %HEIST3% -in %COPSANDCROOKS% -in %MPSUM% -in %HEIST4% -in %TUNER% -in %MPG9EC% -in %SECURITY% -in %MPSUM22%

pause
