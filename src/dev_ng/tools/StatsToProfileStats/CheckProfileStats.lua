

-- Run this before checking in the CSVs
-- Checks for missing stats compared to the previous version of the CSV, and prints the added stats


-- Config
local files = 
				{ 
					"X:/gta5/xlast/profilestats_ng.tu.csv", 
					"X:/gta5/xlast/profilestats_pc.tu.csv" 
				}
				
local ignoreMP = { 2, 3 } -- We're removing the MP2_ and MP3_ stats, ignore if they're missing

------------------------------------------------------------------------------- Helpers

-- see if the file exists
function file_exists(file)
  local f = io.open(file, "rb")
  if f then f:close() end
  return f ~= nil
end

-- get all lines from a file
function lines_from(file)
  if not file_exists(file) then return {} end
  lines = {}
  for line in io.lines(file) do 
    lines[#lines + 1] = line
  end
  return lines
end

-- executes a command and returns the output
function exec(command)
	local file = assert(io.popen(command, 'r'))
	local output = file:read('*all')
	file:close()
	return output
end

-- splits a file content in lines
function lines_from_str(str)
  local t = {}
  local function helper(line) table.insert(t, line) return "" end
  helper((str:gsub("(.-)\r?\n", helper)))
  return t
end

-- split
function split(str, pat)
   local t = {}  -- NOTE: use {n = 0} in Lua-5.0
   local fpat = "(.-)" .. pat
   local last_end = 1
   local s, e, cap = str:find(fpat, 1)
   while s do
      if s ~= 1 or cap ~= "" then
	 table.insert(t,cap)
      end
      last_end = e+1
      s, e, cap = str:find(fpat, last_end)
   end
   if last_end <= #str then
      cap = str:sub(last_end)
      table.insert(t, cap)
   end
   return t
end

-- extracts the stat name from the csv line
function extractStats(linesArray)
	local t = {}
	for k,v in pairs(linesArray) do
		local a = split(v, ",")
		table.insert(t, a[2])
	end
	return t
end


------------------------------------------------------------------------------- Main

	
for i, file in pairs(files) do

	print("")
	print("-----  Checking file ".. file.."  ------")
	
	-- content of the local file
	local localContent = lines_from(file)
	-- content of the file on the repo
	local revContent = lines_from_str(exec("p4 print " .. file))
	
	local localStats = extractStats(localContent)
	local revStats = extractStats(revContent)

	
	print("Looking for missing stats...")
	
	for k,revStat in pairs(revStats) do
		local found = false
		for l,stat in pairs(localStats) do
			if revStat == stat then
				found = true;
			end
		end
		
		if not found then
			-- ignored stats
			local ignored = false
			for j, ignore in pairs(ignoreMP) do
				local prefix = "MP"..ignore.."_"
				if string.find(revStat, prefix) then
					ignored = true
				end
			end
			
			if  not ignored then
				print("Missing stat : " .. revStat)
			end
		end
	end

	
	print("")
	print("New stats ".. file)
	
	for l,stat in pairs(localStats) do
		local found = false
		for k,revStat in pairs(revStats) do
			if revStat == stat then
				found = true;
			end
		end
	
		if not found then
			print("New stat : " .. stat)
		end
	end
	
	print("")
	
end

