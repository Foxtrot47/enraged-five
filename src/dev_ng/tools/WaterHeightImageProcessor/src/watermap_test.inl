namespace Test {

#pragma warning(disable:4800)

class fiStream
{
public:
	fiStream(FILE* f) : m_file(f) {}

	static fiStream* Open(const char* path, bool) { return new fiStream(fopen(path, "rb")); }
	void Close() { fclose(m_file); m_file = NULL; delete this; }
	void Read(void* dst, int size) { fread(dst, size, 1, m_file); }

	FILE* m_file;
};

void Assert(bool cond)
{
	assert(cond);
}

bool AssertVerify(bool cond)
{
	assert(cond);
	return cond;
}

void Assertf(bool cond, const char* format, ...)
{
	if (!cond)
	{
		char temp[8192] = "";
		va_list args;
		va_start(args, format);
		vsnprintf(temp, sizeof(temp), format, args);
		va_end(args);

		fprintf(stderr, "%s\n", temp);
		assert(0);
	}
}

bool Verifyf(bool cond, const char* format, ...)
{
	if (!cond)
	{
		char temp[8192] = "";
		va_list args;
		va_start(args, format);
		vsnprintf(temp, sizeof(temp), format, args);
		va_end(args);

		fprintf(stderr, "%s\n", temp);
		assert(0);
	}

	return cond;
}

namespace Vec { enum { X,Y,Z,W }; }

#define BoolV_Out BoolV
#define BoolV_In const BoolV&

#define VecBoolV_Out VecBoolV
#define VecBoolV_In const VecBoolV&

#define ScalarV_Out ScalarV
#define ScalarV_In const ScalarV&

#define Vec2V_Out Vec2V
#define Vec2V_In const Vec2V&

#define Vec3V_Out Vec3V
#define Vec3V_In const Vec3V&

#define Vec4V_Out Vec4V
#define Vec4V_In const Vec4V&

#define V_ZERO 0.0f
#define V_HALF 0.5f
#define V_ONE  1.0f
#define V_TWO  2.0f

#define __BANK (1)
#define BANK_PARAM(x) ,x
#define BANK_ONLY(x) x

#define Color32 Pixel32

#define sysMemSet memset
#define sysMemCpy memcpy

#define RAGE_MAX_PATH 256

#define rage_new new

__declspec(align(16)) class BoolV
{
public:
	BoolV() {}
	BoolV(bool b_) : b(b_) {}
	bool Getb() const { return b; }
	BoolV_Out operator &(BoolV_In rhs) const { return BoolV(b && rhs.b); }
	BoolV_Out operator |(BoolV_In rhs) const { return BoolV(b || rhs.b); }
	bool b;
};

__declspec(align(16)) class VecBoolV
{
public:
	VecBoolV() {}
	VecBoolV(bool f_) : x(f_), y(f_), z(f_), w(f_) {}
	VecBoolV(bool x_, bool y_, bool z_ = false, bool w_ = false) : x(x_), y(y_), z(z_), w(w_) {}
	BoolV_Out GetX() const { return BoolV(x); }
	BoolV_Out GetY() const { return BoolV(y); }
	BoolV_Out GetZ() const { return BoolV(z); }
	BoolV_Out GetW() const { return BoolV(w); }
	VecBoolV_Out operator &(VecBoolV_In rhs) const { return VecBoolV(x & rhs.x, y & rhs.y, z & rhs.z, w & rhs.w); }
	VecBoolV_Out operator |(VecBoolV_In rhs) const { return VecBoolV(x | rhs.x, y | rhs.y, z | rhs.z, w | rhs.w); }
	bool x, y, z, w;
};

__declspec(align(16)) class ScalarV
{
public:
	ScalarV() {}
	ScalarV(float f) : x(f) {}
	float Getf() const { return x; }
	ScalarV_Out operator +(ScalarV_In rhs) const { return ScalarV(x + rhs.x); }
	ScalarV_Out operator -(ScalarV_In rhs) const { return ScalarV(x - rhs.x); }
	ScalarV_Out operator *(ScalarV_In rhs) const { return ScalarV(x * rhs.x); }
	ScalarV_Out operator /(ScalarV_In rhs) const { return ScalarV(x / rhs.x); }
	BoolV_Out operator < (ScalarV_In rhs) const { return BoolV(x <  rhs.x); }
	BoolV_Out operator > (ScalarV_In rhs) const { return BoolV(x >  rhs.x); }
	BoolV_Out operator <=(ScalarV_In rhs) const { return BoolV(x <= rhs.x); }
	BoolV_Out operator >=(ScalarV_In rhs) const { return BoolV(x >= rhs.x); }
	BoolV_Out operator ==(ScalarV_In rhs) const { return BoolV(x == rhs.x); }
	BoolV_Out operator !=(ScalarV_In rhs) const { return BoolV(x != rhs.x); }
	float x, y_unused, z_unused, w_unused;
};

__declspec(align(16)) class Vec2V
{
public:
	Vec2V() {}
	Vec2V(float f_) : x(f_), y(f_) {}
	Vec2V(float x_, float y_) : x(x_), y(y_) {}
	Vec2V(ScalarV_In f_) : x(f_.x), y(f_.x) {}
	Vec2V(ScalarV_In x_, ScalarV_In y_) : x(x_.x), y(y_.x) {}
	template <int a,int b> Vec2V_Out Get() const { const float v[] = {x,y}; return Vec2V(v[a], v[b]); }
	float GetXf() const { return x; }
	float GetYf() const { return y; }
	ScalarV_Out GetX() const { return ScalarV(x); }
	ScalarV_Out GetY() const { return ScalarV(y); }
	Vec2V_Out operator +(Vec2V_In rhs) const { return Vec2V(x + rhs.x, y + rhs.y); }
	Vec2V_Out operator -(Vec2V_In rhs) const { return Vec2V(x - rhs.x, y - rhs.y); }
	Vec2V_Out operator *(Vec2V_In rhs) const { return Vec2V(x * rhs.x, y * rhs.y); }
	Vec2V_Out operator /(Vec2V_In rhs) const { return Vec2V(x / rhs.x, y / rhs.y); }
	Vec2V_Out operator *(ScalarV_In rhs) const { return Vec2V(x * rhs.x, y * rhs.x); }
	Vec2V_Out operator /(ScalarV_In rhs) const { return Vec2V(x / rhs.x, y / rhs.x); }
	VecBoolV_Out operator < (Vec2V_In rhs) const { return VecBoolV(x <  rhs.x, y <  rhs.y); }
	VecBoolV_Out operator > (Vec2V_In rhs) const { return VecBoolV(x >  rhs.x, y >  rhs.y); }
	VecBoolV_Out operator <=(Vec2V_In rhs) const { return VecBoolV(x <= rhs.x, y <= rhs.y); }
	VecBoolV_Out operator >=(Vec2V_In rhs) const { return VecBoolV(x >= rhs.x, y >= rhs.y); }
	VecBoolV_Out operator ==(Vec2V_In rhs) const { return VecBoolV(x == rhs.x, y == rhs.y); }
	VecBoolV_Out operator !=(Vec2V_In rhs) const { return VecBoolV(x != rhs.x, y != rhs.y); }
	float x, y, z_unused, w_unused;
};

__declspec(align(16)) class Vec3V
{
public:
	Vec3V() {}
	Vec3V(float f_) : x(f_), y(f_), z(f_) {}
	Vec3V(float x_, float y_, float z_) : x(x_), y(y_), z(z_) {}
	Vec3V(ScalarV_In f_) : x(f_.x), y(f_.x), z(f_.x) {}
	Vec3V(ScalarV_In x_, ScalarV_In y_, ScalarV_In z_) : x(x_.x), y(y_.x), z(z_.x) {}
	Vec3V(Vec2V_In xy_, ScalarV_In z_) : x(xy_.x), y(xy_.y), z(z_.x) {}
	template <int a,int b,int c> Vec3V_Out Get() const { const float v[] = {x,y,z}; return Vec3V(v[a], v[b], v[c]); }
	float GetXf() const { return x; }
	float GetYf() const { return y; }
	float GetZf() const { return z; }
	ScalarV_Out GetX() const { return ScalarV(x); }
	ScalarV_Out GetY() const { return ScalarV(y); }
	ScalarV_Out GetZ() const { return ScalarV(z); }
	Vec2V_Out GetXY() const { return Vec2V(x, y); }
	Vec3V_Out operator +(Vec3V_In rhs) const { return Vec3V(x + rhs.x, y + rhs.y, z + rhs.z); }
	Vec3V_Out operator -(Vec3V_In rhs) const { return Vec3V(x - rhs.x, y - rhs.y, z - rhs.z); }
	Vec3V_Out operator *(Vec3V_In rhs) const { return Vec3V(x * rhs.x, y * rhs.y, z * rhs.z); }
	Vec3V_Out operator /(Vec3V_In rhs) const { return Vec3V(x / rhs.x, y / rhs.y, z / rhs.z); }
	Vec3V_Out operator *(ScalarV_In rhs) const { return Vec3V(x * rhs.x, y * rhs.x, z * rhs.x); }
	Vec3V_Out operator /(ScalarV_In rhs) const { return Vec3V(x / rhs.x, y / rhs.x, z / rhs.x); }
	VecBoolV_Out operator < (Vec3V_In rhs) const { return VecBoolV(x <  rhs.x, y <  rhs.y, z <  rhs.z); }
	VecBoolV_Out operator > (Vec3V_In rhs) const { return VecBoolV(x >  rhs.x, y >  rhs.y, z >  rhs.z); }
	VecBoolV_Out operator <=(Vec3V_In rhs) const { return VecBoolV(x <= rhs.x, y <= rhs.y, z <= rhs.z); }
	VecBoolV_Out operator >=(Vec3V_In rhs) const { return VecBoolV(x >= rhs.x, y >= rhs.y, z >= rhs.z); }
	VecBoolV_Out operator ==(Vec3V_In rhs) const { return VecBoolV(x == rhs.x, y == rhs.y, z == rhs.z); }
	VecBoolV_Out operator !=(Vec3V_In rhs) const { return VecBoolV(x != rhs.x, y != rhs.y, z != rhs.z); }
	float x, y, z, w_unused;
};

__declspec(align(16)) class Vec4V
{
public:
	Vec4V() {}
	Vec4V(float f_) : x(f_), y(f_), z(f_), w(f_) {}
	Vec4V(float x_, float y_, float z_, float w_) : x(x_), y(y_), z(z_), w(w_) {}
	Vec4V(ScalarV_In f_) : x(f_.x), y(f_.x), z(f_.x), w(f_.x) {}
	Vec4V(ScalarV_In x_, ScalarV_In y_, ScalarV_In z_, ScalarV_In w_) : x(x_.x), y(y_.x), z(z_.x), w(w_.x) {}
	Vec4V(Vec3V_In xyz_, ScalarV_In w_) : x(xyz_.x), y(xyz_.y), z(xyz_.z), w(w_.x) {}
	template <int a,int b,int c,int d> Vec4V_Out Get() const { const float v[] = {x,y,z,w}; return Vec4V(v[a], v[b], v[c], v[d]); }
	float GetXf() const { return x; }
	float GetYf() const { return y; }
	float GetZf() const { return z; }
	float GetWf() const { return w; }
	ScalarV_Out GetX() const { return ScalarV(x); }
	ScalarV_Out GetY() const { return ScalarV(y); }
	ScalarV_Out GetZ() const { return ScalarV(z); }
	ScalarV_Out GetW() const { return ScalarV(w); }
	Vec3V_Out GetXYZ() const { return Vec3V(x, y, z); }
	Vec2V_Out GetXY() const { return Vec2V(x, y); }
	Vec2V_Out GetZW() const { return Vec2V(z, w); }
	Vec4V_Out operator +(Vec4V_In rhs) const { return Vec4V(x + rhs.x, y + rhs.y, z + rhs.z, w + rhs.w); }
	Vec4V_Out operator -(Vec4V_In rhs) const { return Vec4V(x - rhs.x, y - rhs.y, z - rhs.z, w - rhs.w); }
	Vec4V_Out operator *(Vec4V_In rhs) const { return Vec4V(x * rhs.x, y * rhs.y, z * rhs.z, w * rhs.w); }
	Vec4V_Out operator /(Vec4V_In rhs) const { return Vec4V(x / rhs.x, y / rhs.y, z / rhs.z, w / rhs.w); }
	Vec4V_Out operator *(ScalarV_In rhs) const { return Vec4V(x * rhs.x, y * rhs.x, z * rhs.x, w * rhs.x); }
	Vec4V_Out operator /(ScalarV_In rhs) const { return Vec4V(x / rhs.x, y / rhs.x, z / rhs.x, w / rhs.x); }
	VecBoolV_Out operator < (Vec4V_In rhs) const { return VecBoolV(x <  rhs.x, y <  rhs.y, z <  rhs.z, w <  rhs.w); }
	VecBoolV_Out operator > (Vec4V_In rhs) const { return VecBoolV(x >  rhs.x, y >  rhs.y, z >  rhs.z, w >  rhs.w); }
	VecBoolV_Out operator <=(Vec4V_In rhs) const { return VecBoolV(x <= rhs.x, y <= rhs.y, z <= rhs.z, w <= rhs.w); }
	VecBoolV_Out operator >=(Vec4V_In rhs) const { return VecBoolV(x >= rhs.x, y >= rhs.y, z >= rhs.z, w >= rhs.w); }
	VecBoolV_Out operator ==(Vec4V_In rhs) const { return VecBoolV(x == rhs.x, y == rhs.y, z == rhs.z, w == rhs.w); }
	VecBoolV_Out operator !=(Vec4V_In rhs) const { return VecBoolV(x != rhs.x, y != rhs.y, z != rhs.z, w != rhs.w); }
	float x, y, z, w;
};

ScalarV_Out Abs(ScalarV_In v) { return ScalarV(::Abs<float>(v.x)); }
Vec2V_Out Abs(Vec2V_In v) { return Vec2V(::Abs<float>(v.x), ::Abs<float>(v.y)); }
Vec3V_Out Abs(Vec3V_In v) { return Vec3V(::Abs<float>(v.x), ::Abs<float>(v.y), ::Abs<float>(v.z)); }
Vec4V_Out Abs(Vec4V_In v) { return Vec4V(::Abs<float>(v.x), ::Abs<float>(v.y), ::Abs<float>(v.z), ::Abs<float>(v.w)); }

ScalarV_Out Max(ScalarV_In a, ScalarV_In b) { return ScalarV(::Max<float>(a.x, b.x)); }
Vec2V_Out Max(Vec2V_In a, Vec2V_In b) { return Vec2V(::Max<float>(a.x, b.x), ::Max<float>(a.y, b.y)); }
Vec3V_Out Max(Vec3V_In a, Vec3V_In b) { return Vec3V(::Max<float>(a.x, b.x), ::Max<float>(a.y, b.y), ::Max<float>(a.z, b.z)); }
Vec4V_Out Max(Vec4V_In a, Vec4V_In b) { return Vec4V(::Max<float>(a.x, b.x), ::Max<float>(a.y, b.y), ::Max<float>(a.z, b.z), ::Max<float>(a.w, b.w)); }

ScalarV_Out Min(ScalarV_In a, ScalarV_In b) { return ScalarV(::Min<float>(a.x, b.x)); }
Vec2V_Out Min(Vec2V_In a, Vec2V_In b) { return Vec2V(::Min<float>(a.x, b.x), ::Min<float>(a.y, b.y)); }
Vec3V_Out Min(Vec3V_In a, Vec3V_In b) { return Vec3V(::Min<float>(a.x, b.x), ::Min<float>(a.y, b.y), ::Min<float>(a.z, b.z)); }
Vec4V_Out Min(Vec4V_In a, Vec4V_In b) { return Vec4V(::Min<float>(a.x, b.x), ::Min<float>(a.y, b.y), ::Min<float>(a.z, b.z), ::Min<float>(a.w, b.w)); }

ScalarV_Out MagSquared(Vec2V_In v) { return ScalarV(v.x*v.x + v.y*v.y); }
ScalarV_Out MagSquared(Vec3V_In v) { return ScalarV(v.x*v.x + v.y*v.y + v.z*v.z); }
ScalarV_Out MagSquared(Vec4V_In v) { return ScalarV(v.x*v.x + v.y*v.y + v.z*v.z + v.w*v.w); }

int IsGreaterThanAll(ScalarV_In a, ScalarV_In b) { return (a.x > b.x ? 1 : 0); }
int IsGreaterThanAll(Vec2V_In a, Vec2V_In b) { return ((a.x > b.x && a.y > b.y) ? 1 : 0); }
int IsGreaterThanAll(Vec3V_In a, Vec3V_In b) { return ((a.x > b.x && a.y > b.y && a.z > b.z) ? 1 : 0); }
int IsGreaterThanAll(Vec4V_In a, Vec4V_In b) { return ((a.x > b.x && a.y > b.y && a.z > b.z && a.w > b.w) ? 1 : 0); }

int IsGreaterThanOrEqualAll(ScalarV_In a, ScalarV_In b) { return (a.x >= b.x ? 1 : 0); }
int IsGreaterThanOrEqualAll(Vec2V_In a, Vec2V_In b) { return ((a.x >= b.x && a.y >= b.y) ? 1 : 0); }
int IsGreaterThanOrEqualAll(Vec3V_In a, Vec3V_In b) { return ((a.x >= b.x && a.y >= b.y && a.z >= b.z) ? 1 : 0); }
int IsGreaterThanOrEqualAll(Vec4V_In a, Vec4V_In b) { return ((a.x >= b.x && a.y >= b.y && a.z >= b.z && a.w >= b.w) ? 1 : 0); }

int IsLessThanAll(ScalarV_In a, ScalarV_In b) { return (a.x < b.x ? 1 : 0); }
int IsLessThanAll(Vec2V_In a, Vec2V_In b) { return ((a.x < b.x && a.y < b.y) ? 1 : 0); }
int IsLessThanAll(Vec3V_In a, Vec3V_In b) { return ((a.x < b.x && a.y < b.y && a.z < b.z) ? 1 : 0); }
int IsLessThanAll(Vec4V_In a, Vec4V_In b) { return ((a.x < b.x && a.y < b.y && a.z < b.z && a.w < b.w) ? 1 : 0); }

int IsLessThanOrEqualAll(ScalarV_In a, ScalarV_In b) { return (a.x <= b.x ? 1 : 0); }
int IsLessThanOrEqualAll(Vec2V_In a, Vec2V_In b) { return ((a.x <= b.x && a.y <= b.y) ? 1 : 0); }
int IsLessThanOrEqualAll(Vec3V_In a, Vec3V_In b) { return ((a.x <= b.x && a.y <= b.y && a.z <= b.z) ? 1 : 0); }
int IsLessThanOrEqualAll(Vec4V_In a, Vec4V_In b) { return ((a.x <= b.x && a.y <= b.y && a.z <= b.z && a.w <= b.w) ? 1 : 0); }

int IsEqualAll(ScalarV_In a, ScalarV_In b) { return (a.x == b.x ? 1 : 0); }
int IsEqualAll(Vec2V_In a, Vec2V_In b) { return ((a.x == b.x && a.y == b.y) ? 1 : 0); }
int IsEqualAll(Vec3V_In a, Vec3V_In b) { return ((a.x == b.x && a.y == b.y && a.z == b.z) ? 1 : 0); }
int IsEqualAll(Vec4V_In a, Vec4V_In b) { return ((a.x == b.x && a.y == b.y && a.z == b.z && a.w == b.w) ? 1 : 0); }

ScalarV_Out AddScaled(ScalarV_In a, ScalarV_In b, ScalarV_In c) { return ScalarV(a.x + b.x*c.x); }
Vec2V_Out AddScaled(Vec2V_In a, Vec2V_In b, Vec2V_In c) { return Vec2V(a.x + b.x*c.x, a.y + b.y*c.y); }
Vec3V_Out AddScaled(Vec3V_In a, Vec3V_In b, Vec3V_In c) { return Vec3V(a.x + b.x*c.x, a.y + b.y*c.y, a.z + b.z*c.z); }
Vec4V_Out AddScaled(Vec4V_In a, Vec4V_In b, Vec4V_In c) { return Vec4V(a.x + b.x*c.x, a.y + b.y*c.y, a.z + b.z*c.z, a.w + b.w*c.w); }

ScalarV_Out SubtractScaled(ScalarV_In a, ScalarV_In b, ScalarV_In c) { return ScalarV(a.x - b.x*c.x); }
Vec2V_Out SubtractScaled(Vec2V_In a, Vec2V_In b, Vec2V_In c) { return Vec2V(a.x - b.x*c.x, a.y - b.y*c.y); }
Vec3V_Out SubtractScaled(Vec3V_In a, Vec3V_In b, Vec3V_In c) { return Vec3V(a.x - b.x*c.x, a.y - b.y*c.y, a.z - b.z*c.z); }
Vec4V_Out SubtractScaled(Vec4V_In a, Vec4V_In b, Vec4V_In c) { return Vec4V(a.x - b.x*c.x, a.y - b.y*c.y, a.z - b.z*c.z, a.w - b.w*c.w); }

ScalarV_Out Dot(Vec2V_In a, Vec2V_In b) { return ScalarV(a.x*b.x + a.y*b.y); }
ScalarV_Out Dot(Vec3V_In a, Vec3V_In b) { return ScalarV(a.x*b.x + a.y*b.y + a.z*b.z); }
ScalarV_Out Dot(Vec4V_In a, Vec4V_In b) { return ScalarV(a.x*b.x + a.y*b.y + a.z*b.z + a.w*b.w); }

Vec3V_Out Cross(Vec3V_In a, Vec3V_In b) { return Vec3V(a.y*b.z - b.y*a.z, a.z*b.x - b.z*a.x, a.x*b.y - b.x*a.y); }

void Transpose3x3(Vec3V& dst0, Vec3V& dst1, Vec3V& dst2, Vec3V_In src0, Vec3V_In src1, Vec3V_In src2)
{
	dst0 = Vec3V(src0.x, src1.x, src2.x);
	dst1 = Vec3V(src0.y, src1.y, src2.y);
	dst2 = Vec3V(src0.z, src1.z, src2.z);
}

namespace sysEndian {

	static void SwapMe2(u16& x)
	{
		const u16 x0 = (x >> (8*0))&0xff;
		const u16 x1 = (x >> (8*1))&0xff;

		x = (x0 << (8*1)) | (x1 << (8*0));
	}

	static void SwapMe4(u32& x)
	{
		const u32 x0 = (x >> (8*0))&0xff;
		const u32 x1 = (x >> (8*1))&0xff;
		const u32 x2 = (x >> (8*2))&0xff;
		const u32 x3 = (x >> (8*3))&0xff;

		x = (x0 << (8*3)) | (x1 << (8*2)) | (x2 << (8*1)) | (x3 << (8*0));
	}

	template <typename T> static void SwapMe(T&) {}

	template <> static void SwapMe<u16>(u16& x) { SwapMe2(*(u16*)&x); }
	template <> static void SwapMe<s16>(s16& x) { SwapMe2(*(u16*)&x); }
	template <> static void SwapMe<u32>(u32& x) { SwapMe4(*(u32*)&x); }
	template <> static void SwapMe<s32>(int& x) { SwapMe4(*(u32*)&x); }
	template <> static void SwapMe<float>(float& x) { SwapMe4(*(u32*)&x); }
	template <> static void SwapMe<Vec3V>(Vec3V& x) { SwapMe<float>(x.x); SwapMe<float>(x.y); SwapMe<float>(x.z); }
	template <> static void SwapMe<Vec4V>(Vec4V& x) { SwapMe<float>(x.x); SwapMe<float>(x.y); SwapMe<float>(x.z); SwapMe<float>(x.w); }

} // namespace sysEndian

class CWaterMapHeader
{
public:
	void ByteSwap()
	{
		sysEndian::SwapMe(m_dataSizeInBytes);
		sysEndian::SwapMe(m_offsetX);
		sysEndian::SwapMe(m_offsetY);
		sysEndian::SwapMe(m_tileSizeX);
		sysEndian::SwapMe(m_tileSizeY);
		sysEndian::SwapMe(m_numTileCols);
		sysEndian::SwapMe(m_numTileRows);
		sysEndian::SwapMe(m_numTiles);
		sysEndian::SwapMe(m_numRefIDs);
		sysEndian::SwapMe(m_numRiverPoints);
		sysEndian::SwapMe(m_numRivers);
		sysEndian::SwapMe(m_numLakeBoxes);
		sysEndian::SwapMe(m_numLakes);
		sysEndian::SwapMe(m_numPools);
	}

	u32   m_dataSizeInBytes;
	float m_offsetX;
	float m_offsetY;
	float m_tileSizeX;
	float m_tileSizeY;
	u16   m_numTileCols;
	u16   m_numTileRows;
	u32   m_numTiles;
	u32   m_numRefIDs;
	u16   m_numRiverPoints;
	u16   m_numRivers;
	u16   m_numLakeBoxes;
	u16   m_numLakes;
	u16   m_numPools;

	u8    m_sizeofWaterMapHeader;
	u8    m_sizeofWaterMapTileRow;
	u8    m_sizeofWaterMapTile;
	u8    m_sizeofWaterMapRefID;
	u8    m_sizeofWaterMapRiverPoint;
	u8    m_sizeofWaterMapRiver;
	u8    m_sizeofWaterMapLakeBox;
	u8    m_sizeofWaterMapLake;
	u8    m_sizeofWaterMapPool;
};

class CWaterMapTileRow
{
public:
	static bool IsAligned() { return false; }

	void ByteSwap()
	{
		sysEndian::SwapMe(m_first);
		sysEndian::SwapMe(m_count);
		sysEndian::SwapMe(m_start);
	}

	u8  m_first; // column index of first tile
	u8  m_count; // number of tiles
	u16 m_start; // index of first tile in m_tiles
};

class CWaterMapTile
{
public:
	static bool IsAligned() { return false; }

	void ByteSwap()
	{
		sysEndian::SwapMe(m_start);
	}

	u16 m_start; // index of first refID in m_refIDs
};

class CWaterMapRefID
{
public:
	CWaterMapRefID(u16 v) : m_typeIndexSegment(v) {}

	static bool IsAligned() { return false; }

	void ByteSwap()
	{
		sysEndian::SwapMe(m_typeIndexSegment);
	}

	enum Type
	{
		TYPE_OCEAN = 0,
		TYPE_RIVER,
		TYPE_LAKE,
		TYPE_POOL,
	};

	inline bool IsValid() const { return m_typeIndexSegment != 0; }
	inline bool IsLastRefID() const { return (m_typeIndexSegment & 0x8000) != 0; }
	inline Type GetType() const { return (Type)((m_typeIndexSegment >> 13) & 3); }

	inline int GetIndex() const
	{
		if (GetType() == TYPE_POOL)
		{
			return (int)(m_typeIndexSegment & 8191);
		}
		else
		{
			return (int)((m_typeIndexSegment >> 7) & 63);
		}
	}

	inline int GetSegment() const
	{
		if (GetType() == TYPE_POOL)
		{
			return 0;
		}
		else
		{
			return (int)(m_typeIndexSegment & 127);
		}
	}

	u16 m_typeIndexSegment;
};

class CWaterMapRegionBase
{
public:
	static bool IsAligned() { return true; }

	void ByteSwap()
	{
		sysEndian::SwapMe(m_centreAndHeight);
		sysEndian::SwapMe(m_extentAndWeight);
	}

	inline ScalarV_Out GetHeight() const
	{
		return m_centreAndHeight.GetZ();
	}

	inline ScalarV_Out GetWeight(Vec3V_In camPos) const
	{
		const Vec2V v = Abs((camPos - m_centreAndHeight).GetXY());
		const ScalarV d = MagSquared(Max(Vec2V(V_ZERO), v - m_extentAndWeight.GetXY()));

		return Min(ScalarV(V_ONE), d*m_extentAndWeight.GetZ());
	}

#if 0&&__BANK
	void DebugDraw(const Color32& colour = Color32(255,0,0,255)) const
	{
		const Vec3V bmin = m_centreAndHeight - Vec3V(m_extentAndWeight.GetXY(), ScalarV(V_ZERO));
		const Vec3V bmax = m_centreAndHeight + Vec3V(m_extentAndWeight.GetXY(), ScalarV(V_ONE));

		grcDebugDraw::BoxAxisAligned(bmin, bmax, colour, false);
	}
#endif // __BANK

	Vec3V m_centreAndHeight;
	Vec3V m_extentAndWeight;
};

class CWaterMapRiverPoint
{
public:
	static bool IsAligned() { return true; }

	void ByteSwap()
	{
		sysEndian::SwapMe(m_point);
	}

	Vec4V m_point;
};

// TODO -- vectorise
static float DistanceToSegmentSquared(float x, float y, float x0, float y0, float x1, float y1, float* t = NULL)
{
	float nx = x1 - x0;
	float ny = y1 - y0;
	float q = nx*nx + ny*ny;
	float dx = 0.0f;
	float dy = 0.0f;
	float dz = 0.0f;

	if (q < 0.00001f)
	{
		dx = x - (x1 + x0)*0.5f;
		dy = y - (y1 + y0)*0.5f;

		if (t) { *t = 0.5f; }
	}
	else
	{
		const float len = sqrtf(q);

		dz = ((x - x0)*nx + (y - y0)*ny)/len; // projected distance along segment

		if (dz >= len)
		{
			dx = x - x1;
			dy = y - y1;
			dz = 0.0f;

			if (t) { *t = 1.0f; }
		}
		else
		{
			dx = x - x0;
			dy = y - y0;

			if (dz <= 0.0f)
			{
				dz = 0.0f;

				if (t) { *t = 0.0f; }
			}
			else
			{
				if (t) { *t = dz/len; }
			}
		}
	}

	return ::Max<float>(0.0f, dx*dx + dy*dy - dz*dz);
}

class CWaterMapRiver : public CWaterMapRegionBase
{
public:
	void ByteSwap()
	{
		CWaterMapRegionBase::ByteSwap();

		sysEndian::SwapMe(m_count);
		sysEndian::SwapMe(m_pad);
		sysEndian::SwapMe(m_start);
	}

	inline ScalarV_Out GetHeight(Vec3V_In camPos, int segment, const CWaterMapRiverPoint* riverPoints) const
	{
		const Vec3V point0 = riverPoints[(int)m_start + segment + 0].m_point.GetXYZ();
		const Vec3V point1 = riverPoints[(int)m_start + segment + 1].m_point.GetXYZ();

		float t = 0.0f;
		DistanceToSegmentSquared(
			camPos.GetXf(),
			camPos.GetYf(),
			point0.GetXf(),
			point0.GetYf(),
			point1.GetXf(),
			point1.GetYf(), &t);

		return (point0 + (point1 - point0)*ScalarV(t)).GetZ();
	}

	inline ScalarV_Out GetWeight(Vec3V_In camPos, int segment, const CWaterMapRiverPoint* riverPoints) const
	{
		const Vec3V point0 = riverPoints[(int)m_start + segment + 0].m_point.GetXYZ();
		const Vec3V point1 = riverPoints[(int)m_start + segment + 1].m_point.GetXYZ();

		const float d = DistanceToSegmentSquared(
			camPos.GetXf(),
			camPos.GetYf(),
			point0.GetXf(),
			point0.GetYf(),
			point1.GetXf(),
			point1.GetYf());

		return Min(ScalarV(V_ONE), ScalarV(d)*m_extentAndWeight.GetZ());
	}

#if 0&&__BANK
	void DebugDraw(const CWaterMapRiverPoint* riverPoints, u8 alpha, bool) const
	{
		for (int i = 0; i < (int)m_count; i++)
		{
			const Vec3V   point = riverPoints[(int)m_start + i].m_point.GetXYZ();
			const ScalarV width = riverPoints[(int)m_start + i].m_point.GetW();

			const int i0 = Max<int>(i - 1, 0);
			const int i1 = Min<int>(i + 1, (int)m_count - 1);

			const Vec3V v = riverPoints[(int)m_start + i1].m_point.GetXYZ() - riverPoints[(int)m_start + i0].m_point.GetXYZ();
			const Vec3V n = Normalize(Cross(v, Vec3V(V_Z_AXIS_WZERO)));

			const Vec3V p0 = point - n*width*ScalarV(V_HALF);
			const Vec3V p1 = point + n*width*ScalarV(V_HALF);
			const Vec3V p2 = p1 + width*Vec3V(V_Z_AXIS_WZERO);
			const Vec3V p3 = p0 + width*Vec3V(V_Z_AXIS_WZERO);

			grcDebugDraw::Quad(p0, p1, p2, p3, Color32(0,0,255,alpha), true, false);
		}
	}
#endif // __BANK

	u8  m_count; // number of points in this river
	u8  m_pad;
	u16 m_start; // index of first point in m_riverPoints
};

class CWaterMapLakeBox
{
public:
	static bool IsAligned() { return true; }

	void ByteSwap()
	{
		sysEndian::SwapMe(m_box);
	}

	inline Vec2V GetCentre() const { return m_box.GetXY(); }
	inline Vec2V GetExtent() const { return m_box.GetZW(); }

	Vec4V m_box; // centre xy, extent xy
};

class CWaterMapLake : public CWaterMapRegionBase
{
public:
	void ByteSwap()
	{
		CWaterMapRegionBase::ByteSwap();

		sysEndian::SwapMe(m_count);
		sysEndian::SwapMe(m_pad);
		sysEndian::SwapMe(m_start);
	}

	inline ScalarV_Out GetWeight(Vec3V_In camPos, int segment, const CWaterMapLakeBox* lakeBoxes) const
	{
		const CWaterMapLakeBox& box = lakeBoxes[(int)m_start + segment];
		const Vec2V v = Abs(camPos.GetXY() - box.GetCentre());
		const ScalarV d = MagSquared(Max(Vec2V(V_ZERO), v - box.GetExtent()));

		return Min(ScalarV(V_ONE), d*m_extentAndWeight.GetZ());
	}

#if 0&&__BANK
	void DebugDraw(const CWaterMapLakeBox* lakeBoxes, u8 alpha, bool) const
	{
		for (int i = 0; i < (int)m_count; i++)
		{
			const CWaterMapLakeBox& box = lakeBoxes[(int)m_start + i];

			const Vec3V bmin = Vec3V(box.GetCentre() - box.GetExtent(), m_centreAndHeight.GetZ());
			const Vec3V bmax = Vec3V(box.GetCentre() + box.GetExtent(), m_centreAndHeight.GetZ() + ScalarV(V_ONE));

			grcDebugDraw::BoxAxisAligned(bmin, bmax, Color32(255,0,255,alpha), false);
		}

		CWaterMapRegionBase::DebugDraw(Color32(255,255,0,alpha));
	}
#endif // __BANK

	u8  m_count; // number of boxes in this lake
	u8  m_pad;
	u16 m_start; // index of first box in m_lakeBoxes
};

class CWaterMapPool : public CWaterMapRegionBase
{
public:
#if 0&&__BANK
	void DebugDraw(u8 alpha, bool) const
	{
		CWaterMapRegionBase::DebugDraw(Color32(0,255,0,alpha));
	}
#endif // __BANK
};

#if __BANK

class CWaterMapRegionBaseDebugInfo
{
public:
	static bool IsAligned() { return false; }

	void ByteSwap()
	{
	}

	u8 m_colour[4];
};

class CWaterMapRiverDebugInfo : public CWaterMapRegionBaseDebugInfo
{
public:
};

class CWaterMapLakeDebugInfo : public CWaterMapRegionBaseDebugInfo
{
public:
};

class CWaterMapPoolDebugInfo : public CWaterMapRegionBaseDebugInfo
{
public:
};

#endif // __BANK

template <typename T> static T* CWaterMap_GetArrayPtr(u8* data, int& offset, int count, bool bNeedsByteSwap)
{
	if (T::IsAligned())
	{
		offset = (offset + 15)&~15;
	}
	else
	{
		offset = (offset + 3)&~3;
	}

	T* array = reinterpret_cast<T*>(data + offset);

	if (bNeedsByteSwap)
	{
		for (int i = 0; i < count; i++)
		{
			array[i].ByteSwap();
		}
	}

	offset += count*sizeof(T);
	return array;
}

#if __BANK
static bool g_waterMapSystemEnabled          = true;
static bool g_waterMapUpdateEnabled          = true;
static bool g_waterMapRiversEnabled          = true;
static bool g_waterMapLakesEnabled           = true;
static bool g_waterMapPoolsEnabled           = true;
static bool g_waterMapDebugDrawCurrentRegion = false;
static bool g_waterMapDebugDrawAllRivers     = false;
static bool g_waterMapDebugDrawAllLakes      = false;
static bool g_waterMapDebugDrawAllPools      = false;
static int  g_waterMapDebugPoolIndex         = -1;
#endif // __BANK

static int g_currentWaterTileIndices[2] = {-1, -1};

class CWaterMap
{
public:
	CWaterMap()
	{
		sysMemSet(this, 0, sizeof(*this));
	}

	void Load(const char* path)
	{
		fiStream* f = fiStream::Open(path, true);

		if (!Verifyf(f, "water height file \"%s\" not found", path))
		{
			return;
		}

		bool bNeedsByteSwap = false;

		u32 tag = 0;
		f->Read(&tag, sizeof(tag));

		if (tag != 'WMAP')
		{
			sysEndian::SwapMe(tag);

			if (tag != 'WMAP')
			{
				Assertf(0, "water height file \"%s\" is corrupted", path);
				return;
			}
			else
			{
				bNeedsByteSwap = true;
			}
		}

		const u32 expectedVersion = 100;
		u32 version = 0;
		f->Read(&version, sizeof(version));

		if (bNeedsByteSwap)
		{
			sysEndian::SwapMe(version);
		}

		if (version/100 != expectedVersion/100)
		{
			Assertf(0, "water height file \"%s\" version %d.%02d is incorrect, expected version %d", path, version/100, version%100, expectedVersion/100);
			return;
		}

		f->Read(&m_header, sizeof(m_header));

		if (bNeedsByteSwap)
		{
			m_header.ByteSwap();
		}

		Assert(m_header.m_sizeofWaterMapHeader     == sizeof(CWaterMapHeader    ));
		Assert(m_header.m_sizeofWaterMapTileRow    == sizeof(CWaterMapTileRow   ));
		Assert(m_header.m_sizeofWaterMapTile       == sizeof(CWaterMapTile      ));
		Assert(m_header.m_sizeofWaterMapRefID      == sizeof(CWaterMapRefID     ));
		Assert(m_header.m_sizeofWaterMapRiverPoint == sizeof(CWaterMapRiverPoint));
		Assert(m_header.m_sizeofWaterMapRiver      == sizeof(CWaterMapRiver     ));
		Assert(m_header.m_sizeofWaterMapLakeBox    == sizeof(CWaterMapLakeBox   ));
		Assert(m_header.m_sizeofWaterMapLake       == sizeof(CWaterMapLake      ));
		Assert(m_header.m_sizeofWaterMapPool       == sizeof(CWaterMapPool      ));

		m_data = rage_new u8[m_header.m_dataSizeInBytes];

		f->Read(m_data, m_header.m_dataSizeInBytes);

		int offset = 0;

		m_tileRows    = CWaterMap_GetArrayPtr<CWaterMapTileRow   >(m_data, offset, (int)m_header.m_numTileRows   , bNeedsByteSwap);
		m_tiles       = CWaterMap_GetArrayPtr<CWaterMapTile      >(m_data, offset, (int)m_header.m_numTiles      , bNeedsByteSwap);
		m_refIDs      = CWaterMap_GetArrayPtr<CWaterMapRefID     >(m_data, offset, (int)m_header.m_numRefIDs     , bNeedsByteSwap);
		m_riverPoints = CWaterMap_GetArrayPtr<CWaterMapRiverPoint>(m_data, offset, (int)m_header.m_numRiverPoints, bNeedsByteSwap);
		m_rivers      = CWaterMap_GetArrayPtr<CWaterMapRiver     >(m_data, offset, (int)m_header.m_numRivers     , bNeedsByteSwap);
		m_lakeBoxes   = CWaterMap_GetArrayPtr<CWaterMapLakeBox   >(m_data, offset, (int)m_header.m_numLakeBoxes  , bNeedsByteSwap);
		m_lakes       = CWaterMap_GetArrayPtr<CWaterMapLake      >(m_data, offset, (int)m_header.m_numLakes      , bNeedsByteSwap);
		m_pools       = CWaterMap_GetArrayPtr<CWaterMapPool      >(m_data, offset, (int)m_header.m_numPools      , bNeedsByteSwap);

#if __BANK
		m_riverDebugInfo = rage_new CWaterMapRiverDebugInfo[m_header.m_numRivers];
		f->Read(m_riverDebugInfo, (int)m_header.m_numRivers*sizeof(CWaterMapRiverDebugInfo));

		m_lakeDebugInfo = rage_new CWaterMapLakeDebugInfo[m_header.m_numLakes];
		f->Read(m_lakeDebugInfo, (int)m_header.m_numLakes*sizeof(CWaterMapLakeDebugInfo));

		m_poolDebugInfo = rage_new CWaterMapPoolDebugInfo[m_header.m_numPools];
		f->Read(m_poolDebugInfo, (int)m_header.m_numPools*sizeof(CWaterMapPoolDebugInfo));

		if (bNeedsByteSwap)
		{
			for (int i = 0; i < (int)m_header.m_numRivers; i++)
			{
				m_riverDebugInfo[i].ByteSwap();
				Assert(m_riverDebugInfo[i].m_colour[3] == 255);
			}

			for (int i = 0; i < (int)m_header.m_numLakes; i++)
			{
				m_lakeDebugInfo[i].ByteSwap();
				Assert(m_lakeDebugInfo[i].m_colour[3] == 255);
			}

			for (int i = 0; i < (int)m_header.m_numPools; i++)
			{
				m_poolDebugInfo[i].ByteSwap();
				Assert(m_poolDebugInfo[i].m_colour[3] == 255);
			}
		}
#endif // __BANK

		f->Close();
	}

	float GetHeight(Vec3V_In camPos BANK_PARAM(float& out_weight) BANK_PARAM(CWaterMapRefID& out_refID)) const
	{
#if 0&&__BANK
		if (g_waterMapDebugPoolIndex >= 0 &&
			g_waterMapDebugPoolIndex < (int)m_header.m_numPools)
		{
			const CWaterMapPool& pool = m_pools[g_waterMapDebugPoolIndex];

			const Vec2V v = Abs((camPos - pool.m_centreAndHeight).GetXY());
			const float d1 = MagSquared(Max(Vec2V(V_ZERO), v - pool.m_extentAndWeight.GetXY())).Getf();
			const float d2 = pool.m_extentAndWeight.GetZf();

			grcDebugDraw::AddDebugOutput("debug pool[%d] weight = Min(1, %f*%f)", g_waterMapDebugPoolIndex, d1, d2);
		}
#endif // __BANK

		const int i = (int)floorf(+(camPos.GetXf() - m_header.m_offsetX)/m_header.m_tileSizeX);
		const int j = (int)floorf(-(camPos.GetYf() - m_header.m_offsetY)/m_header.m_tileSizeY); // flipped

		g_currentWaterTileIndices[0] = i;
		g_currentWaterTileIndices[1] = j;

		if (i >= 0 && i < (int)m_header.m_numTileCols &&
			j >= 0 && j < (int)m_header.m_numTileRows)
		{
			const CWaterMapTileRow& row = m_tileRows[j];
			const int i2 = i - row.m_first;

			if (i2 >= 0 && i2 < row.m_count)
			{
				const CWaterMapTile& tile = m_tiles[(int)row.m_start + i2];

				if (tile.m_start != 0xffff)
				{
					ScalarV height(V_ZERO);
					ScalarV weight(V_ONE); // it's faster to track "1 - weight^2" than actual weight

					for (int k = 0; ; k++)
					{
						const CWaterMapRefID& refID = m_refIDs[(int)tile.m_start + k];

						switch (refID.GetType())
						{
						case CWaterMapRefID::TYPE_RIVER:
							{
								BANK_ONLY(if (g_waterMapRiversEnabled))
								{
									const int segment = refID.GetSegment();
									const CWaterMapRiver& river = m_rivers[refID.GetIndex()];
									const ScalarV w = river.GetWeight(camPos, segment, m_riverPoints);

									if (IsGreaterThanAll(weight, w))
									{
										weight = w;
										height = river.GetHeight(camPos, segment, m_riverPoints);
										BANK_ONLY(out_refID = refID);
									}
								}
								break;
							}
						case CWaterMapRefID::TYPE_LAKE:
							{
								BANK_ONLY(if (g_waterMapLakesEnabled))
								{
									const int segment = refID.GetSegment();
									const CWaterMapLake& lake = m_lakes[refID.GetIndex()];
									const ScalarV w = lake.GetWeight(camPos, segment, m_lakeBoxes);

									if (IsGreaterThanAll(weight, w))
									{
										weight = w;
										height = lake.GetHeight();
										BANK_ONLY(out_refID = refID);
									}
								}
								break;
							}
						case CWaterMapRefID::TYPE_POOL:
							{
								BANK_ONLY(if (g_waterMapPoolsEnabled))
								{
									const CWaterMapPool& pool = m_pools[refID.GetIndex()];
									const ScalarV w = pool.GetWeight(camPos);

									if (IsGreaterThanAll(weight, w))
									{
										weight = w;
										height = pool.GetHeight();
										BANK_ONLY(out_refID = refID);
									}
								}
								break;
							}
						}

						if (refID.IsLastRefID())
						{
							break;
						}
					}

					BANK_ONLY(out_weight = ::Max<float>(0.0f, 1.0f - sqrtf(weight.Getf())));
					return height.Getf();
				}
			}
		}

		BANK_ONLY(out_weight = 0.0f);
		return 0.0f;
	}

#if 0&&__BANK
	void DebugDraw(Vec3V_In, const CWaterMapRefID& currentRefID) const
	{
		u8 alpha = 255;

		if (g_waterMapDebugDrawCurrentRegion)
		{
			switch (currentRefID.GetType())
			{
			case CWaterMapRefID::TYPE_RIVER : m_rivers[currentRefID.GetIndex()].DebugDraw(m_riverPoints, 255, true); break;
			case CWaterMapRefID::TYPE_LAKE  : m_lakes [currentRefID.GetIndex()].DebugDraw(m_lakeBoxes, 255, true); break;
			case CWaterMapRefID::TYPE_POOL  : m_pools [currentRefID.GetIndex()].DebugDraw(255, true); break;
			}

			alpha = 64;
		}

		if (g_waterMapDebugDrawAllRivers)
		{
			for (int i = 0; i < (int)m_header.m_numRivers; i++)
			{
				m_rivers[i].DebugDraw(m_riverPoints, alpha, false);
			}
		}

		if (g_waterMapDebugDrawAllLakes)
		{
			for (int i = 0; i < (int)m_header.m_numLakes; i++)
			{
				m_lakes[i].DebugDraw(m_lakeBoxes, alpha, false);
			}
		}

		if (g_waterMapDebugDrawAllPools)
		{
			for (int i = 0; i < (int)m_header.m_numPools; i++)
			{
				m_pools[i].DebugDraw(alpha, false);
			}
		}
	}
#endif // __BANK

	CWaterMapHeader      m_header;
	u8*                  m_data;
	CWaterMapTileRow*    m_tileRows;
	CWaterMapTile*       m_tiles;
	CWaterMapRefID*      m_refIDs;
	CWaterMapRiverPoint* m_riverPoints;
	CWaterMapRiver*      m_rivers;
	CWaterMapLakeBox*    m_lakeBoxes;
	CWaterMapLake*       m_lakes;
	CWaterMapPool*       m_pools;

#if __BANK
	CWaterMapRiverDebugInfo* m_riverDebugInfo;
	CWaterMapLakeDebugInfo*  m_lakeDebugInfo;
	CWaterMapPoolDebugInfo*  m_poolDebugInfo;
#endif // __BANK
};

static CWaterMap g_waterMap;

static float g_currentUpdateWaterHeight = 0.0f;
static float g_currentRenderWaterHeight = 0.0f;
#if __BANK
static float          g_currentWaterWeight = 0.0f;
static CWaterMapRefID g_currentWaterRefID(0);
#endif // __BANK

// ================================================================================================

#if __BANK

class CWaterHeightTestImage
{
public:
	static CWaterHeightTestImage& Get()
	{
		static CWaterHeightTestImage s;
		return s;
	}

	static void Create_button()
	{
		const int w = (int)floorf((float)(sm_boundsMaxX - sm_boundsMinX)*sm_cellsPerMetre);
		const int h = (int)floorf((float)(sm_boundsMaxY - sm_boundsMinY)*sm_cellsPerMetre);

		if (!AssertVerify(w >= 1 && h >= 1))
		{
			return;
		}

		//if (sm_saveColourImage)
		{
			//grcImage* pImage = grcImage::Create(w, h, 1, grcImage::A8R8G8B8, grcImage::STANDARD, 0, 0);
			Pixel32* pImage = new Pixel32[w*h];

			if (AssertVerify(pImage))
			{
				Color32* data = pImage;//(Color32*)pImage->GetBits();
				sysMemSet(data, 0xff, w*h*sizeof(Color32));

				for (int j = 0; j < h; j++)
				{
					for (int i = 0; i < w; i++)
					{
						const float x = (float)sm_boundsMinX + (0.5f + (float)i)/sm_cellsPerMetre;
						const float y = (float)sm_boundsMaxY - (0.5f + (float)j)/sm_cellsPerMetre;
						const u8 colourWhite[] = {255, 255, 255, 255};
						const u8* colour = colourWhite;

						float weight = 0.0f;
						CWaterMapRefID refID(0);

						g_waterMap.GetHeight(Vec3V(x, y, 0.0f), weight, refID);

						if (refID.IsValid())
						{
							switch (refID.GetType())
							{
							case CWaterMapRefID::TYPE_RIVER : colour = g_waterMap.m_riverDebugInfo[refID.GetIndex()].m_colour; break;
							case CWaterMapRefID::TYPE_LAKE  : colour = g_waterMap.m_lakeDebugInfo [refID.GetIndex()].m_colour; break;
							case CWaterMapRefID::TYPE_POOL  : colour = g_waterMap.m_poolDebugInfo [refID.GetIndex()].m_colour; break;
							}

							data[i + j*w] = Color32(colour[0], colour[1], colour[2], 255);
						}
						else
						{
							data[i + j*w] = Color32(255,255,255,255);
						}
					}
				}

				//pImage->SaveDDS(sm_saveColourImagePath);
				//pImage->Release();
				SaveImagePixel32("test.png", data, w, h, true);
				delete[] data;
			}
		}
	}

	static char  sm_saveHeightImagePath[RAGE_MAX_PATH];
	static char  sm_saveWeightImagePath[RAGE_MAX_PATH];
	static char  sm_saveColourImagePath[RAGE_MAX_PATH];
	static int   sm_boundsMinX;
	static int   sm_boundsMinY;
	static int   sm_boundsMaxX;
	static int   sm_boundsMaxY;
	static float sm_cellsPerMetre;
};

__COMMENT(static) char  CWaterHeightTestImage::sm_saveHeightImagePath[] = "common:/waterheighttest_height.dds";
__COMMENT(static) char  CWaterHeightTestImage::sm_saveWeightImagePath[] = "common:/waterheighttest_weight.dds";
__COMMENT(static) char  CWaterHeightTestImage::sm_saveColourImagePath[] = "common:/waterheighttest_colour.dds";
__COMMENT(static) int   CWaterHeightTestImage::sm_boundsMinX = -2550 - 150*16; // swimming pool area
__COMMENT(static) int   CWaterHeightTestImage::sm_boundsMinY = -1950 - 150*16;
__COMMENT(static) int   CWaterHeightTestImage::sm_boundsMaxX = 450 + 150*16;
__COMMENT(static) int   CWaterHeightTestImage::sm_boundsMaxY = 1050 + 150*16;
__COMMENT(static) float CWaterHeightTestImage::sm_cellsPerMetre = 0.5f;

#endif // __BANK

void Run()
{
	g_waterMap.Load(LEVEL_PATH "/waterheight.dat");

	CWaterHeightTestImage::Create_button();
}

} // namespace Test
