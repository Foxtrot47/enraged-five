// ==========
// watermap.h
// ==========

#ifndef _WATERMAP_H_
#define _WATERMAP_H_

#include "../../common/common.h"
#include "../../common/mathutil.h"

#include "worldutil.h"

#define RIVER_INFLUENCE_SCALE     (50.0f)
#define POOL_INFLUENCE_MULTIPLIER (80.0f)
#define POOL_INFLUENCE_EXPONENT   (0.25f)
#define POOL_INFLUENCE_MAX_SIZE   (90.0f)

class TileMap;

namespace Water {

class WaterRefID
{
public:
	WaterRefID() {}
	WaterRefID(int type, int segment, int index) : m_type((u8)type), m_segment((u8)segment), m_index((u16)index) {}

	static WaterRefID None() { return WaterRefID(TYPE_NONE, 0, 0); }

	bool operator ==(const WaterRefID& rhs) const { return m_ID == rhs.m_ID; }
	bool operator !=(const WaterRefID& rhs) const { return m_ID != rhs.m_ID; }

	enum Type
	{
		TYPE_OCEAN = 0,
		TYPE_RIVER,
		TYPE_LAKE,
		TYPE_POOL,
		TYPE_NONE,
	};

	union
	{
		struct 
		{
			u8  m_type;
			u8  m_segment; // segment of river, box of lake
			u16 m_index; // index into rivers/lakes/pools
		};

		u32 m_ID;
	};
};

class WaterCell : public WaterRefID
{
public:
	WaterCell() : WaterRefID(WaterRefID::None()), m_flags(0), m_height(0.0f), m_weight(0.0f), m_interp(0.0f) {}
	WaterCell(const WaterRefID& refID, float height, float weight, float interp) : WaterRefID(refID), m_flags(0), m_height(height), m_weight(weight), m_interp(interp) {}

	enum
	{
		FLAG_WATER_CELL = BIT(0), // indicates this cell is actually water, not just nearby water
	};

	u32   m_flags;
	float m_height;
	float m_weight;
	float m_interp;
};

class WaterTile
{
public:
	void AddRefID(const WaterRefID& refID);

	std::vector<WaterRefID> m_refIDs;
};

class WaterRegionBase
{
public:
	WaterRegionBase(int index);

	Pixel32    m_colour;
	int        m_index; // index into rivers/lakes/pools
	float      m_range;
	int        m_i0; // bounds in cells, includes range
	int        m_j0;
	int        m_i1;
	int        m_j1;
	WaterCell* m_cells;
	int        m_numWaterCells;
	Box2       m_bounds; // bounds in worldspace, does not include range
};

template <WaterRefID::Type TYPE> class WaterRegion : public WaterRegionBase
{
public:
	WaterRegion(int index) : WaterRegionBase(index) {}

	WaterRefID GetID(int segment) const
	{
		return WaterRefID(TYPE, segment, m_index);
	}

	WaterRefID::Type GetIDType() const
	{
		return TYPE;
	}
};

class WaterRiverPoint : public Vec4
{
public:
	WaterRiverPoint(const Vec4& v) : Vec4(v), t(0.0f) {}

	float t; // normalised distance along river
};

class WaterRiver : public WaterRegion<WaterRefID::TYPE_RIVER>
{
public:
	WaterRiver(int index) : WaterRegion<WaterRefID::TYPE_RIVER>(index), m_length(0.0f) {}

	int   GetNumSegments() const;
	Box2  GetSegmentBounds(int segment) const;
	float GetWeight(const Vec2& p, int segment) const;
	float GetHeight(const Vec2& p, int segment, float* out_interp = NULL) const;

	float m_length;
	std::vector<WaterRiverPoint> m_points;
};

class WaterLakeBox : public Box2
{
public:
	WaterLakeBox(const Box2& box) : Box2(box) {}
};

class WaterLake : public WaterRegion<WaterRefID::TYPE_LAKE>
{
public:
	WaterLake(int index) : WaterRegion<WaterRefID::TYPE_LAKE>(index), m_height(-1000.0f) {}

	int   GetNumSegments() const;
	Box2  GetSegmentBounds(int segment) const;
	float GetWeight(const Vec2& p, int segment) const;
	float GetHeight(const Vec2& p, int segment, float* out_interp = NULL) const;

	float m_height;
	std::vector<WaterLakeBox> m_boxes;
};

class WaterPoolZone : public Box2
{
public:
	WaterPoolZone(const Box2& bounds) : Box2(bounds) {}
};

class WaterPool : public WaterRegion<WaterRefID::TYPE_POOL>
{
public:
	WaterPool(int index) : WaterRegion<WaterRefID::TYPE_POOL>(index), m_height(-1000.0f) {}

	int   GetNumSegments() const;
	Box2  GetSegmentBounds(int segment) const;
	float GetWeight(const Vec2& p, int segment) const;
	float GetHeight(const Vec2& p, int segment, float* out_interp = NULL) const;

	float m_height;
};

class WaterMap : public WorldMask
{
public:
	WaterMap()
	{
		memset(this, 0, sizeof(*this));
	}

	Vec2 ConvertCellsToWorld(int i, int j) const;
	Box2 ConvertCellsToWorld(int i0, int j0, int i1, int j1) const;
	void ConvertWorldToCells(int& i, int& j, float x, float y) const;
	void ConvertWorldToCells(int& i0, int& j0, int& i1, int& j1, const Box2& box, float range = 0.0f) const;

	void ApplyRegionCells(const WaterRegionBase& region);

	int          m_resolution;
	u8*          m_waterMask1; // water mask derived from actual water height samples
	u8*          m_waterMask2; // full water mask (includes all ocean cells etc.)
	const float* m_waterHeight;
	WaterCell*   m_cells;
	float        m_offsetX;
	float        m_offsetY;
	float        m_cellsPerMetre;
	int          m_tileResX; // cells per tile 
	int          m_tileResY;
	int          m_tileNumX; // w/tileResX
	int          m_tileNumY; // h/tileResY
	WaterTile*   m_tiles;
	bool         m_bUseEDTOnly; // not sure about this .. if i can't get it to work, remove it

	static std::vector<WaterRiver   *> sm_rivers;
	static std::vector<WaterLake    *> sm_lakes;
	static std::vector<WaterPoolZone*> sm_poolZones;
	static std::vector<WaterPool    *> sm_pools;
};

void LoadWaterMap(WaterMap& map, const char* path, const WorldMask& wm, const TileMap& tm, const float* waterHeight, int resolution, bool bUseEDTOnly, float tileSizeInMetres);
void LoadWaterMapRivers(WaterMap& map, const char* path = RS_ASSETS "/non_final/water/rivers.txt", bool bVerbose = true);
void LoadWaterMapLakes(WaterMap& map, const char* path = RS_ASSETS "/non_final/water/lakes.txt", bool bVerbose = true);
void LoadWaterMapPools(WaterMap& map, const char* path = RS_ASSETS "/non_final/water/poolzones.txt", bool bVerbose = true);

void BuildWaterMapRivers(WaterMap& map);
void BuildWaterMapLakes(WaterMap& map);
void BuildWaterMapPools(WaterMap& map);
void BuildWaterMapTiles(WaterMap& map, int margin = 1);

void SaveWaterMapImages(const WaterMap& map, const char* path);

// ================================================================================================

u8* LoadWorldImageMask(const WorldMask& wm, const char* path, bool bInvert = false, bool bClipToWorldMask = true);

void DrawPixelMask(Pixel32* image, int w, int h, const u8* mask, const Pixel32& c1, const Pixel32& c0 = Pixel32(0,0,0,0));
void DrawPixelBlocks(Pixel32* image, int w, int h, int tileResX, int tileResY, const u8* mask, const Pixel32& c1, const Pixel32& c0 = Pixel32(0,0,0,0));
void DrawPixelGrid(Pixel32* image, int w, int h, int tileResX, int tileResY, const Pixel32& colourPrimary, const Pixel32& colourSecondary, float amount);

void DrawBoxOverlay(Pixel32* image, int w, int h, const WaterMap& map, const Box2& box, const Pixel32& colour, float amount);
void DrawPointOverlay(Pixel32* image, int w, int h, const WaterMap& map, const Vec2& point, int r, int thickness, const Pixel32& colour, float amount);
void DrawRiverOverlay(Pixel32* image, int w, int h, const WaterMap& map, const WaterRiver& river, const Pixel32& colour, float amount);
void DrawLakeOverlay(Pixel32* image, int w, int h, const WaterMap& map, const WaterLake& lake, const Pixel32& colour, float amount);
void DrawPoolOverlay(Pixel32* image, int w, int h, const WaterMap& map, const WaterPool& pool, const Pixel32& colour, float amount);

} // namespace Water

// ================================================================================================

namespace WaterFileWriter {

void Save(const Water::WaterMap& map, const char* path, bool bNeedsByteSwap);

} // namespace WaterFileWriter

#endif // _WATERMAP_H_
