// =============
// worldutil.cpp
// =============

#include "../../common/fileutil.h"
#include "../../common/imageutil.h"
#include "../../common/mathutil.h"
#include "../../common/progressdisplay.h"
#include "../../common/stringutil.h"

#include "../../../rage/framework/src/fwgeovis/geovis.h"

#include "worldutil.h"

__COMMENT(static) int TileMap::ReadInt(const char* name, FILE* fp)
{
	char line[1024] = "";

	if (fp && ReadFileLine(line, sizeof(line), fp))
	{
		if (memcmp(line, name, strlen(name)) == 0)
		{
			return atoi(line + strlen(name));
		}
	}

	assert(0); // failed!
	exit(-1);
	return 0;
}

bool TileMap::Load(const char* path)
{
	FILE* fp = fopen(varString("%s/tiles.txt", path).c_str(), "r");

	if (fp)
	{
		m_tileMapX = ReadInt("tiles_x0=", fp)/gv::WORLD_CELLS_PER_TILE;
		m_tileMapY = ReadInt("tiles_y0=", fp)/gv::WORLD_CELLS_PER_TILE;
		m_tileMapW = ReadInt("tiles_x1=", fp)/gv::WORLD_CELLS_PER_TILE - m_tileMapX;
		m_tileMapH = ReadInt("tiles_y1=", fp)/gv::WORLD_CELLS_PER_TILE - m_tileMapY;
		m_downsamp = ReadInt("downsamp=", fp);

		fprintf(
			stdout,
			"tile map is x=[%d..%d], y=[%d..%d]\n",
			gv::NAVMESH_BOUNDS_MIN_X + gv::WORLD_TILE_SIZE*(m_tileMapX),
			gv::NAVMESH_BOUNDS_MIN_Y + gv::WORLD_TILE_SIZE*(m_tileMapY),
			gv::NAVMESH_BOUNDS_MIN_X + gv::WORLD_TILE_SIZE*(m_tileMapX + m_tileMapW),
			gv::NAVMESH_BOUNDS_MIN_Y + gv::WORLD_TILE_SIZE*(m_tileMapY + m_tileMapH)
		);

		const int tileMaskSize = (m_tileMapW*m_tileMapH + 7)/8;

		m_tileMap = new char[m_tileMapW*m_tileMapH];

		for (int j = 0; j < m_tileMapH; j++)
		{
			char line[1024] = "";

			ReadFileLine(line, sizeof(line), fp);

			const char* lineStart = strchr(line, ':');

			if (lineStart)
			{
				lineStart++;

				for (int i = 0; i < m_tileMapW; i++)
				{
					m_tileMap[i + (m_tileMapH - j - 1)*m_tileMapW] = lineStart[i];
				}
			}
		}

		fclose(fp);
		return true;
	}
	else
	{
		fprintf(stdout, "failed to load tile map %s!\n", path);
	}

	return false;
}

WorldMask::WorldMask(const char* path)
{
	memset(this, 0, sizeof(*this));

	if (path)
	{
		const std::string worldMaskPath = varString("%s/assembled_wld.dds", path);

		int w = 0;
		int h = 0;

		if (GetImageDimensions(worldMaskPath.c_str(), w, h))
		{
			m_w = w;
			m_h = h;
		}
		else
		{
			return;
		}

		// find an appropriate span height
		{
			const int spanHeightsToTry[] = {500, 250, 100, 75, 50, 25, 10, 5, 1, 0};

			for (int i = 0; spanHeightsToTry[i] > 0; i++)
			{
				if (h%spanHeightsToTry[i] == 0)
				{
					m_spanHeight = spanHeightsToTry[i];
					break;
				}
			}

			if (m_spanHeight == 0)
			{
				m_spanHeight = h;
			}
		}

		// load world mask
		{
			ProgressDisplay progress("loading world mask (span height = %d)", m_spanHeight);

			const int worldMaskSize = (w*h + 7)/8;
			m_worldMask = new u8[worldMaskSize];
			memset(m_worldMask, 0, worldMaskSize*sizeof(u8));

			for (int span = 0; span < h; span += m_spanHeight)
			{
				float* temp = LoadImageDDSSpan_float(worldMaskPath.c_str(), w, span, m_spanHeight);

				if (temp)
				{
					for (int j = span; j < span + m_spanHeight; j++)
					{
						for (int i = 0; i < w; i++)
						{
							if (temp[i + (j - span)*w] == 1.0f)
							{
								const int index = i + j*w;

								m_worldMask[index/8] |= BIT(index%8);
							}
						}
					}

					delete[] temp;
				}

				progress.Update(span, h);
			}

			progress.End();
		}

		// expand world mask
		{
			ProgressDisplay progress("expanding world mask");

			m_worldMinX = new int[h];
			m_worldMinY = new int[w];
			m_worldMaxX = new int[h];
			m_worldMaxY = new int[w];

			for (int j = 0; j < h; j++)
			{
				m_worldMinX[j] = +999999;
				m_worldMaxX[j] = -999999;
			}

			for (int i = 0; i < w; i++)
			{
				m_worldMinY[i] = +999999;
				m_worldMaxY[i] = -999999;
			}

			for (int j = 0; j < h; j++)
			{
				for (int i = 0; i < w; i++)
				{
					const int index = i + j*w;

					if (m_worldMask[index/8] & BIT(index%8))
					{
						m_worldMinX[j] = Min<int>(i, m_worldMinX[j]);
						m_worldMinY[i] = Min<int>(j, m_worldMinY[i]);
						m_worldMaxX[j] = Max<int>(i, m_worldMaxX[j]);
						m_worldMaxY[i] = Max<int>(j, m_worldMaxY[i]);
					}
				}
			}

			if (1) // dilate world worldMask by 1 pixel
			{
				int* tempMinX = new int[h];
				int* tempMinY = new int[w];
				int* tempMaxX = new int[h];
				int* tempMaxY = new int[w];

				memcpy(tempMinX, m_worldMinX, h*sizeof(int));
				memcpy(tempMinY, m_worldMinY, w*sizeof(int));
				memcpy(tempMaxX, m_worldMaxX, h*sizeof(int));
				memcpy(tempMaxY, m_worldMaxY, w*sizeof(int));

				for (int j = 0; j < h; j++)
				{
					int minX = tempMinX[j];
					int maxX = tempMaxX[j];

					if (j > 0)
					{
						minX = Max<int>(tempMinX[j - 1], minX);
						maxX = Min<int>(tempMaxX[j - 1], maxX);
					}

					if (j < h - 1)
					{
						minX = Max<int>(tempMinX[j + 1], minX);
						maxX = Min<int>(tempMaxX[j + 1], maxX);
					}

					m_worldMinX[j] = minX;
					m_worldMaxX[j] = maxX;
				}

				for (int i = 0; i < w; i++)
				{
					int minY = tempMinY[i];
					int maxY = tempMaxY[i];

					if (i > 0)
					{
						minY = Max<int>(tempMinY[i - 1], minY);
						maxY = Min<int>(tempMaxY[i - 1], maxY);
					}

					if (i < w - 1)
					{
						minY = Max<int>(tempMinY[i + 1], minY);
						maxY = Min<int>(tempMaxY[i + 1], maxY);
					}

					m_worldMinY[i] = minY;
					m_worldMaxY[i] = maxY;
				}

				delete[] tempMinX;
				delete[] tempMinY;
				delete[] tempMaxX;
				delete[] tempMaxY;
			}

			for (int j = 0; j < h; j++)
			{
				for (int i = 0; i < w; i++)
				{
					if (i < m_worldMinX[j] || i > m_worldMaxX[j] ||
						j < m_worldMinY[i] || j > m_worldMaxY[i])
					{
						const int index = i + j*w;

						m_worldMask[index/8] |= BIT(index%8);
					}
				}
			}

			progress.End();
		}
	}
}

WorldMask::~WorldMask()
{
	if (m_worldMask) { delete[] m_worldMask; }
	if (m_worldMinX) { delete[] m_worldMinX; }
	if (m_worldMinY) { delete[] m_worldMinY; }
	if (m_worldMaxX) { delete[] m_worldMaxX; }
	if (m_worldMaxY) { delete[] m_worldMaxY; }
}

template <typename T> static float GetMaxValue();
template <> static float GetMaxValue<u8>() { return 255.0f; }
template <> static float GetMaxValue<u16>() { return 65535.0f; }
template <> static float GetMaxValue<float>() { return 1.0f; }

template <typename T> static T Invert(T value);
template <> static u8 Invert<u8>(u8 value) { return ~value; }
template <> static u16 Invert<u16>(u16 value) { return ~value; }
template <> static float Invert<float>(float value) { return 1.0f - value; }

template <typename T> static T* LoadImageType(const char* path, int& w, int& h);

template <> static u8*      LoadImageType<u8>     (const char* path, int& w, int& h) { return LoadImage_u8     (path, w, h); }
template <> static u16*     LoadImageType<u16>    (const char* path, int& w, int& h) { return LoadImage_u16    (path, w, h); }
template <> static float*   LoadImageType<float>  (const char* path, int& w, int& h) { return LoadImage_float  (path, w, h); }
template <> static Pixel32* LoadImageType<Pixel32>(const char* path, int& w, int& h) { return LoadImage_Pixel32(path, w, h); }

template <typename T> static void AssembleImageFromTiles(const TileMap& tm, int resolution, const char* path, const char* suffix, bool bInvert, bool bClearToWhite, const T& clearValue_, bool bSaveNativeFormat)
{
	char pngPath[1024] = "";
	char ddsPath[1024] = "";

	strcpy(pngPath, varString("%s/assembled%s.png", path, suffix).c_str());
	strcpy(ddsPath, varString("%s/assembled%s.dds", path, suffix).c_str());

	if (!FileExists(pngPath) ||
		!FileExists(ddsPath))
	{
		ProgressDisplay progress("assembling image '%s'", suffix + 1);

		const int pixelsPerCell = resolution*gv::WORLD_CELL_RESOLUTION;
		const int tileResX = pixelsPerCell*gv::WORLD_CELLS_PER_TILE;
		const int tileResY = pixelsPerCell*gv::WORLD_CELLS_PER_TILE;

		const int worldW = tileResX*tm.m_tileMapW;
		const int worldH = tileResX*tm.m_tileMapH;
		T* image = new T[worldW*worldH];
		bool bValidImage = false;

		const T clearValue = bClearToWhite ? T(GetMaxValue<T>()) : clearValue_;

		for (int i = 0; i < worldW*worldH; i++)
		{
			image[i] = clearValue;
		}

		for (int tileY = 0; tileY < tm.m_tileMapH; tileY++)
		{
			for (int tileX = 0; tileX < tm.m_tileMapW; tileX++)
			{
				if (tm.m_tileMap[tileX + tileY*tm.m_tileMapW] == TILE_MAP_EMPTY)
				{
					continue;
				}

				const int sectorX = (tm.m_tileMapX + tileX)*gv::WORLD_CELLS_PER_TILE;
				const int sectorY = (tm.m_tileMapY + tileY)*gv::WORLD_CELLS_PER_TILE;

				int w = tileResX;
				int h = tileResY;
				T* tile = LoadImageType<T>(varString("%s/heightmap_tile_%s%s.dds", path, gv::GetTileNameFromCoords(sectorX, sectorY), suffix).c_str(), w, h);

				if (tile)
				{
					for (int j = 0; j < tileResY; j++)
					{
						for (int i = 0; i < tileResX; i++)
						{
							const int ii = i + tileX*tileResX;
							const int jj = j + tileY*tileResY;

							image[ii + (worldH - jj - 1)*worldW] = tile[i + (tileResY - j - 1)*tileResX];
						}
					}

					delete[] tile;
					bValidImage = true;
				}
				else if (clearValue >= T(0)) // don't clear to zero for empty tiles if we're assembling waterheight (empty pixels should be -1000)
				{
					for (int j = 0; j < tileResY; j++)
					{
						for (int i = 0; i < tileResX; i++)
						{
							const int ii = i + tileX*tileResX;
							const int jj = j + tileY*tileResY;

							image[ii + (worldH - jj - 1)*worldW] = 0;
						}
					}
				}
			}

			progress.Update(tileY, tm.m_tileMapH);
		}

		if (bInvert)
		{
			for (int i = 0; i < worldW*worldH; i++)
			{
				image[i] = Invert<T>(image[i]);
			}
		}

		progress.End(bValidImage ? "done" : "no images found");

		if (bValidImage)
		{
			SaveImage(pngPath, image, worldW, worldH, false, bSaveNativeFormat);
			SaveImage(ddsPath, image, worldW, worldH, false);
		}

		delete[] image;
	}
}

template <> static void AssembleImageFromTiles<Pixel32>(const TileMap& tm, int resolution, const char* path, const char* suffix, bool bInvert, bool bClearToWhite, const Pixel32&, bool)
{
	char pngPath[1024] = "";

	strcpy(pngPath, varString("%s/assembled%s.png", path, suffix).c_str());

	if (!FileExists(pngPath))
	{
		ProgressDisplay progress("assembling image '%s'", suffix + 1);

		const int pixelsPerCell = resolution*gv::WORLD_CELL_RESOLUTION;
		const int tileResX = pixelsPerCell*gv::WORLD_CELLS_PER_TILE;
		const int tileResY = pixelsPerCell*gv::WORLD_CELLS_PER_TILE;

		const int worldW = tileResX*tm.m_tileMapW;
		const int worldH = tileResX*tm.m_tileMapH;
		Pixel32* image = new Pixel32[worldW*worldH];
		bool bValidImage = false;

		const Pixel32 clearValue = bClearToWhite ? Pixel32(255,255,255,255) : Pixel32(0,0,0,255);

		for (int i = 0; i < worldW*worldH; i++)
		{
			image[i] = clearValue;
		}

		for (int tileY = 0; tileY < tm.m_tileMapH; tileY++)
		{
			for (int tileX = 0; tileX < tm.m_tileMapW; tileX++)
			{
				if (tm.m_tileMap[tileX + tileY*tm.m_tileMapW] == TILE_MAP_EMPTY)
				{
					continue;
				}

				const int sectorX = (tm.m_tileMapX + tileX)*gv::WORLD_CELLS_PER_TILE;
				const int sectorY = (tm.m_tileMapY + tileY)*gv::WORLD_CELLS_PER_TILE;

				int w = tileResX;
				int h = tileResY;
				Pixel32* tile = LoadImageType<Pixel32>(varString("%s/heightmap_tile_%s%s.dds", path, gv::GetTileNameFromCoords(sectorX, sectorY), suffix).c_str(), w, h);

				if (tile)
				{
					for (int j = 0; j < tileResY; j++)
					{
						for (int i = 0; i < tileResX; i++)
						{
							const int ii = i + tileX*tileResX;
							const int jj = j + tileY*tileResY;

							image[ii + (worldH - jj - 1)*worldW] = tile[i + (tileResY - j - 1)*tileResX];
						}
					}

					delete[] tile;
					bValidImage = true;
				}
			}

			progress.Update(tileY, tm.m_tileMapH);
		}

		if (bInvert)
		{
			for (int i = 0; i < worldW*worldH; i++)
			{
				image[i].r ^= 0xff;
				image[i].g ^= 0xff;
				image[i].b ^= 0xff;
			}
		}

		progress.End(bValidImage ? "done" : "no images found");

		if (bValidImage)
		{
			SaveImage(pngPath, image, worldW, worldH, false);
		}

		delete[] image;
	}
}

void AssembleImageFromTilesU8(const TileMap& tm, int resolution, const char* path, const char* suffix, bool bInvert, bool bClearToWhite , const u8& clearValue, bool bSaveNativeFormat)
{
	AssembleImageFromTiles<u8>(tm, resolution, path, suffix, bInvert, bClearToWhite, clearValue, bSaveNativeFormat);
}

void AssembleImageFromTilesU16(const TileMap& tm, int resolution, const char* path, const char* suffix, bool bInvert, bool bClearToWhite, const u16& clearValue, bool bSaveNativeFormat)
{
	AssembleImageFromTiles<u16>(tm, resolution, path, suffix, bInvert, bClearToWhite, clearValue, bSaveNativeFormat);
}

void AssembleImageFromTilesFloat(const TileMap& tm, int resolution, const char* path, const char* suffix, bool bInvert, bool bClearToWhite, const float& clearValue)
{
	AssembleImageFromTiles<float>(tm, resolution, path, suffix, bInvert, bClearToWhite, clearValue, false);
}

void AssembleImageFromTilesPixel32(const TileMap& tm, int resolution, const char* path, const char* suffix, bool bInvert, bool bClearToWhite, const u32& clearValue)
{
	AssembleImageFromTiles<Pixel32>(tm, resolution, path, suffix, bInvert, bClearToWhite, Pixel32(clearValue), false);
}
