// ============
// watermap.cpp
// ============

#include "../../common/fileutil.h"
#include "../../common/imageutil.h"
#include "../../common/progressdisplay.h"
#include "../../common/stringutil.h"

#include "../../../rage/framework/src/fwgeovis/geovis.h"

#include "watermap.h"
#include "worldutil.h"

template <typename T> static T ParseFileLine(const char* var, FILE* fp);

template <> static int ParseFileLine<int>(const char* var, FILE* fp)
{
	char line[1024] = "";

	if (ReadNextFileLine(line, sizeof(line), fp))
	{
		if (memcmp(line, var, strlen(var)) == 0 && line[strlen(var)] == '=')
		{
			const char* s = line + strlen(var) + 1;
			return atoi(s);
		}
	}

	assert(0);
	return 0;
}

template <> static float ParseFileLine<float>(const char* var, FILE* fp)
{
	char line[1024] = "";

	if (ReadNextFileLine(line, sizeof(line), fp))
	{
		if (memcmp(line, var, strlen(var)) == 0 && line[strlen(var)] == '=')
		{
			const char* s = line + strlen(var) + 1;
			return (float)atof(s);
		}
	}

	assert(0);
	return 0;
}

template <> static Vec3 ParseFileLine<Vec3>(const char* var, FILE* fp)
{
	char line[1024] = "";

	if (ReadNextFileLine(line, sizeof(line), fp))
	{
		if (memcmp(line, var, strlen(var)) == 0 && line[strlen(var)] == '=')
		{
			const char* s = line + strlen(var) + 1;
			const float x = (float)atof(s);
			s = strchr(s, ',');

			if (s)
			{
				s++;
				const float y = (float)atof(s);
				s = strchr(s, ',');

				if (s)
				{
					s++;
					const float z = (float)atof(s);
					return Vec3(x, y, z);
				}
			}
		}
	}

	assert(0);
	return 0;
}

template <> static Vec4 ParseFileLine<Vec4>(const char* var, FILE* fp)
{
	char line[1024] = "";

	if (ReadNextFileLine(line, sizeof(line), fp))
	{
		if (memcmp(line, var, strlen(var)) == 0 && line[strlen(var)] == '=')
		{
			const char* s = line + strlen(var) + 1;
			const float x = (float)atof(s);
			s = strchr(s, ',');

			if (s)
			{
				s++;
				const float y = (float)atof(s);
				s = strchr(s, ',');

				if (s)
				{
					s++;
					const float z = (float)atof(s);
					s = strchr(s, ',');

					if (s)
					{
						s++;
						const float w = (float)atof(s);
						return Vec4(x, y, z, w);
					}
				}
			}
		}
	}

	assert(0);
	return 0;
}

namespace Water {

void WaterTile::AddRefID(const WaterRefID& refID)
{
	for (int i = 0; i < (int)m_refIDs.size(); i++)
	{
		if (m_refIDs[i] == refID)
		{
			return;
		}
	}

	m_refIDs.push_back(refID);
}

WaterRegionBase::WaterRegionBase(int index) : m_index(index), m_range(0.0f), m_i0(0), m_j0(0), m_i1(0), m_j1(0), m_cells(NULL), m_numWaterCells(0), m_bounds(Box2::Invalid())
{
	int r = 0;
	int g = 0;
	int b = 0;

	//srand((u32)floorf(m_height*100.0f));

	while (Max<int>(r, Max<int>(g, b)) < 64 || r > Min<int>(g, b) + 128) // pick a random colour which isn't too close to red
	{
		r = rand()&0xff;
		g = rand()&0xff;
		b = rand()&0xff;
	}

	m_colour.r = (u8)r;
	m_colour.g = (u8)g;
	m_colour.b = (u8)b;
	m_colour.a = 255;
}

int WaterRiver::GetNumSegments() const
{
	return (int)m_points.size() - 1;
}

Box2 WaterRiver::GetSegmentBounds(int segment) const
{
	const Vec4& p0 = m_points[segment + 0];
	const Vec4& p1 = m_points[segment + 1];

	return Box2(Min<float>(p0.x, p1.x), Min<float>(p0.y, p1.y), Max<float>(p0.x, p1.x), Max<float>(p0.y, p1.y));
}

float WaterRiver::GetWeight(const Vec2& p, int segment) const
{
	const Vec4& p0 = m_points[segment + 0];
	const Vec4& p1 = m_points[segment + 1];

	assert(m_range > 0.0f);
	return Max<float>(0.0f, 1.0f - DistanceToSegment(p.x, p.y, p0.x, p0.y, p1.x, p1.y)/m_range);
}

float WaterRiver::GetHeight(const Vec2& p, int segment, float* out_interp) const
{
	const WaterRiverPoint& p0 = m_points[segment + 0];
	const WaterRiverPoint& p1 = m_points[segment + 1];

	float t = 0.0f;
	DistanceToSegment(p.x, p.y, p0.x, p0.y, p1.x, p1.y, &t);

	if (out_interp)
	{
		*out_interp = p0.t + (p1.t - p0.t)*t;
	}

	return p0.z + (p1.z - p0.z)*t;
}

int WaterLake::GetNumSegments() const
{
	return (int)m_boxes.size();
}

Box2 WaterLake::GetSegmentBounds(int segment) const
{
	return m_boxes[segment];
}

float WaterLake::GetWeight(const Vec2& p, int segment) const
{
	assert(m_range > 0.0f);
	return Max<float>(0.0f, 1.0f - DistanceToBox(p.x, p.y, m_boxes[segment].m_xmin, m_boxes[segment].m_ymin, m_boxes[segment].m_xmax, m_boxes[segment].m_ymax)/m_range);
}

float WaterLake::GetHeight(const Vec2&, int, float* out_interp) const
{
	if (out_interp)
	{
		*out_interp = 0.0f;
	}

	return m_height;
}

int WaterPool::GetNumSegments() const
{
	return 1;
}

Box2 WaterPool::GetSegmentBounds(int) const
{
	return m_bounds;
}

float WaterPool::GetWeight(const Vec2& p, int) const
{
	assert(m_range > 0.0f);
	return Max<float>(0.0f, 1.0f - DistanceToBox(p.x, p.y, m_bounds.m_xmin, m_bounds.m_ymin, m_bounds.m_xmax, m_bounds.m_ymax)/m_range);
}

float WaterPool::GetHeight(const Vec2&, int, float* out_interp) const
{
	if (out_interp)
	{
		*out_interp = 0.0f;
	}

	return m_height;
}

class QueueElem
{
public:
	QueueElem() {}
	QueueElem(int i_, int j_) : i((u16)i_), j((u16)j_) {}

	u16 i, j;
};

template <typename T> static void BuildRegionCells(const WaterMap& map, T& region, const QueueElem* queue = NULL, int queueLength = 0)
{
	map.ConvertWorldToCells(
		region.m_i0,
		region.m_j0,
		region.m_i1,
		region.m_j1,
		region.m_bounds,
		region.m_range);

	const int local_w = region.m_i1 - region.m_i0;
	const int local_h = region.m_j1 - region.m_j0;

	region.m_cells = new WaterCell[local_w*local_h];

	if (map.m_bUseEDTOnly) // build distance field
	{
		if (queue)
		{
			int* temp = new int[local_w*local_h];
			memset(temp, 0, local_w*local_h*sizeof(int));

			for (int i = 0; i < local_w*local_h; i++)
			{
				temp[i] = 1;
			}

			for (int i = 0; i < queueLength; i++)
			{
				if (queue[i].i >= region.m_i0 && queue[i].i < region.m_i1 &&
					queue[i].j >= region.m_j0 && queue[i].j < region.m_j1)
				{
					temp[(queue[i].i - region.m_i0) + (queue[i].j - region.m_j0)*local_w] = 0;
				}
			}

			MaurerEDT(temp, local_w, local_h);

			for (int local_j = 0; local_j < local_h; local_j++)
			{
				for (int local_i = 0; local_i < local_w; local_i++)
				{
					const float dist = Max<float>(0.0f, sqrtf((float)temp[local_i + local_j*local_w]) - 0.5f)/map.m_cellsPerMetre;
					const float weight = Max<float>(0.0f, 1.0f - dist/region.m_range);

					if (weight > 0.0f)
					{
						const float height = 0.0f; // dummy height
						const float interp = 0.0f;
						const WaterCell src(region.GetID(0), height, weight, interp);
						WaterCell& dst = region.m_cells[local_i + local_j*local_w];

						if (dst.m_weight < src.m_weight)
						{
							dst = src;
						}
					}
				}
			}

			delete[] temp;
		}
	}
	else
	{
		const int numSegments = region.GetNumSegments();

		for (int segment = 0; segment < numSegments; segment++)
		{
			int segment_i0 = 0;
			int segment_j0 = 0;
			int segment_i1 = 0;
			int segment_j1 = 0;

			map.ConvertWorldToCells(
				segment_i0,
				segment_j0,
				segment_i1,
				segment_j1,
				region.GetSegmentBounds(segment),
				region.m_range);

			for (int j = segment_j0; j < segment_j1; j++)
			{
				for (int i = segment_i0; i < segment_i1; i++)
				{
					float interp = 0.0f;

					const float height = region.GetHeight(map.ConvertCellsToWorld(i, j), segment, &interp);
					const float weight = region.GetWeight(map.ConvertCellsToWorld(i, j), segment);

					const int local_i = i - region.m_i0;
					const int local_j = j - region.m_j0;

					const WaterCell src(region.GetID(segment), height, weight, interp);
					WaterCell& dst = region.m_cells[local_i + local_j*local_w];

					if (dst.m_weight < src.m_weight)
					{
						dst = src;
					}
				}
			}
		}
	}
}

enum eImageType
{
	IMAGE_TYPE_HEIGHT,
	IMAGE_TYPE_WEIGHT,
	IMAGE_TYPE_COLOUR,
	IMAGE_TYPE_COLOUR_AND_WEIGHT,
};

static Pixel32* DrawCellImage(const WaterCell* cells, int w, int h, eImageType type)
{
	Pixel32* image = new Pixel32[w*h];

	if (type == IMAGE_TYPE_HEIGHT)
	{
		float zmin = +FLT_MAX;
		float zmax = -FLT_MAX;

		for (int i = 0; i < w*h; i++)
		{
			if (cells[i].m_height > -999.0f)
			{
				zmin = Min<float>(cells[i].m_height, zmin);
				zmax = Max<float>(cells[i].m_height, zmax);
			}
		}

		for (int i = 0; i < w*h; i++)
		{
			if (cells[i].m_height > -999.0f)
			{
				const float height = (cells[i].m_height - zmin)/(zmax - zmin);

				image[i].r = (u8)(0.5f + 255.0f*height);
				image[i].g = image[i].r;
				image[i].b = image[i].r;
				image[i].a = 255;
			}
			else
			{
				image[i] = Pixel32(0,0,0,255);
			}
		}
	}
	else if (type == IMAGE_TYPE_WEIGHT)
	{
		for (int i = 0; i < w*h; i++)
		{
			const float weight = Clamp<float>(cells[i].m_weight, 0.0f, 1.0f);

			float r = weight;
			float g = weight;
			float b = weight;
			float t = cells[i].m_interp;

			t *= 16.0f;
			t -= floorf(t);

			r += (0.8f - r)*t;
			g += (1.0f - g)*t;
			b += (0.8f - b)*t;

			image[i].r = (u8)(0.5f + 255.0f*r);
			image[i].g = (u8)(0.5f + 255.0f*g);
			image[i].b = (u8)(0.5f + 255.0f*b);
			image[i].a = 255;
		}
	}
	else if (type == IMAGE_TYPE_COLOUR || type == IMAGE_TYPE_COLOUR_AND_WEIGHT)
	{
		for (int j = 0; j < h; j++)
		{
			for (int i = 0; i < w; i++)
			{
				const int index = i + j*w;
				const WaterCell& cell = cells[index];
				Pixel32 colour(255,255,255,255);

				if (cell.m_weight > 0.0f)
				{
					switch (cell.m_type)
					{
					case WaterRefID::TYPE_OCEAN : colour = Pixel32(0,0,255,255); break;
					case WaterRefID::TYPE_RIVER : colour = WaterMap::sm_rivers[cell.m_index]->m_colour; break;
					case WaterRefID::TYPE_LAKE  : colour = WaterMap::sm_lakes [cell.m_index]->m_colour; break;
					case WaterRefID::TYPE_POOL  : colour = WaterMap::sm_pools [cell.m_index]->m_colour; break;
					}

					if (type == IMAGE_TYPE_COLOUR_AND_WEIGHT)
					{
						float r = (float)colour.r/255.0f;
						float g = (float)colour.g/255.0f;
						float b = (float)colour.b/255.0f;

						r = 1.0f + (r - 1.0f)*sqrtf(cell.m_weight);
						g = 1.0f + (g - 1.0f)*sqrtf(cell.m_weight);
						b = 1.0f + (b - 1.0f)*sqrtf(cell.m_weight);

						colour.r = (u8)(0.5f + 255.0f*r);
						colour.g = (u8)(0.5f + 255.0f*g);
						colour.b = (u8)(0.5f + 255.0f*b);
					}
				}

				image[i + j*w] = colour;
			}
		}
	}

	return image;
}

static void SaveWaterMapImage(const WaterMap& map, const char* path, eImageType type, float maskAmount1, float maskAmount2)
{
	const int w = map.m_w;
	const int h = map.m_h;
	Pixel32* image = DrawCellImage(map.m_cells, w, h, type);

	if (maskAmount1 > 0.0f ||
		maskAmount2 > 0.0f)
	{
		for (int index = 0; index < w*h; index++)
		{
			Pixel32& c = image[index];

			float r = (float)c.r/255.0f;
			float g = (float)c.g/255.0f;
			float b = (float)c.b/255.0f;

			const WaterCell& cell = map.m_cells[index];

			if (cell.m_flags & WaterCell::FLAG_WATER_CELL)
			{
				r += (0.0f - r)*maskAmount1; // cyan
				g += (1.0f - g)*maskAmount1;
				b += (1.0f - b)*maskAmount1;
			}
			else if ((map.m_waterMask1[index/8] & BIT(index%8)) != 0)
			{
				r += (0.0f - r)*maskAmount1; // blue
				g += (0.0f - g)*maskAmount1;
				b += (1.0f - b)*maskAmount1;
			}
			else if ((map.m_waterMask2[index/8] & BIT(index%8)) != 0)
			{
				r += (0.0f - r)*maskAmount2; // blue with lower intensity
				g += (0.0f - g)*maskAmount2;
				b += (1.0f - b)*maskAmount2;
			}

			c.r = (u8)(0.5f + 255.0f*r);
			c.g = (u8)(0.5f + 255.0f*g);
			c.b = (u8)(0.5f + 255.0f*b);
		}
	}

	for (int i = 0; i < (int)map.sm_rivers.size(); i++)
	{
		DrawRiverOverlay(image, w, h, map, *map.sm_rivers[i], Pixel32(255,0,0,255), 0.5f);
	}

	for (int i = 0; i < (int)map.sm_lakes.size(); i++)
	{
		DrawLakeOverlay(image, w, h, map, *map.sm_lakes[i], Pixel32(0,128,255,255), 0.125f);
	}

	for (int i = 0; i < (int)map.sm_pools.size(); i++)
	{
		DrawPoolOverlay(image, w, h, map, *map.sm_pools[i], Pixel32(0,0,255,255), 0.125f);
	}

	Pixel32 colourPrimary(153,255,204,255); // greenish white
	Pixel32 colourSecondary(255,255,255,255);

	if (type == IMAGE_TYPE_COLOUR ||
		type == IMAGE_TYPE_COLOUR_AND_WEIGHT)
	{
		colourPrimary = Pixel32(0,0,0,255);
		colourSecondary = Pixel32(0,0,0,255);
	}

	DrawPixelGrid(image, w, h, map.m_tileResX, map.m_tileResY, colourPrimary, colourSecondary, 0.25f);

	SaveImage(path, image, w, h, false);

	delete[] image;
}

template <typename T> static void SaveRegionCellImage(const T& region)
{
	const int local_w = region.m_i1 - region.m_i0;
	const int local_h = region.m_j1 - region.m_j0;

	if (local_w <= 0 || local_h <= 0)
	{
		return;
	}

	const char* typeStr = "";

	switch (region.GetIDType())
	{
	case WaterRefID::TYPE_NONE  : return;
	case WaterRefID::TYPE_OCEAN : typeStr = "ocean"; break;
	case WaterRefID::TYPE_RIVER : typeStr = "river"; break;
	case WaterRefID::TYPE_LAKE  : typeStr = "lake"; break;
	case WaterRefID::TYPE_POOL  : typeStr = "pool"; break;
	}

	Pixel32* image = DrawCellImage(region.m_cells, local_w, local_h, IMAGE_TYPE_WEIGHT);

	SaveImage(varString("region_%s_%d.png", typeStr, region.m_index).c_str(), image, local_w, local_h, false);

	delete[] image;
}

Vec2 WaterMap::ConvertCellsToWorld(int i, int j) const
{
	const float x = m_offsetX + ((float)i + 0.5f)/m_cellsPerMetre;
	const float y = m_offsetY - ((float)j + 0.5f)/m_cellsPerMetre;

	return Vec2(x, y);
}

Box2 WaterMap::ConvertCellsToWorld(int i0, int j0, int i1, int j1) const
{
	const float xmin = m_offsetX + ((float)i0)/m_cellsPerMetre;
	const float ymax = m_offsetY - ((float)j0)/m_cellsPerMetre;
	const float ymin = m_offsetY - ((float)j1)/m_cellsPerMetre;
	const float xmax = m_offsetX + ((float)i1)/m_cellsPerMetre;

	return Box2(xmin, ymin, xmax, ymax);
}

void WaterMap::ConvertWorldToCells(int& i, int& j, float x, float y) const
{
	i = (int)(0.5f + (x - m_offsetX)*m_cellsPerMetre);
	j = (int)(0.5f - (y - m_offsetY)*m_cellsPerMetre);
}

void WaterMap::ConvertWorldToCells(int& i0, int& j0, int& i1, int& j1, const Box2& box, float range) const
{
	i0 = (int)floorf(+(box.m_xmin - range - m_offsetX)*m_cellsPerMetre);
	j0 = (int)floorf(-(box.m_ymax + range - m_offsetY)*m_cellsPerMetre);
	i1 = (int)ceilf (+(box.m_xmax + range - m_offsetX)*m_cellsPerMetre);
	j1 = (int)ceilf (-(box.m_ymin - range - m_offsetY)*m_cellsPerMetre);
}

void WaterMap::ApplyRegionCells(const WaterRegionBase& region)
{
	const int local_w = region.m_i1 - region.m_i0;

	for (int j = region.m_j0; j < region.m_j1; j++)
	{
		for (int i = region.m_i0; i < region.m_i1; i++)
		{
			if (i >= 0 && i < m_w &&
				j >= 0 && j < m_h)
			{
				const int local_i = i - region.m_i0;
				const int local_j = j - region.m_j0;

				const WaterCell& src = region.m_cells[local_i + local_j*local_w];
				WaterCell& dst = m_cells[i + j*m_w];

				if (dst.m_weight < src.m_weight)
				{
					dst = src;
				}
			}
		}
	}
}

__COMMENT(static) std::vector<WaterRiver   *> WaterMap::sm_rivers;
__COMMENT(static) std::vector<WaterLake    *> WaterMap::sm_lakes;
__COMMENT(static) std::vector<WaterPoolZone*> WaterMap::sm_poolZones;
__COMMENT(static) std::vector<WaterPool    *> WaterMap::sm_pools;

// ================================================================================================

void LoadWaterMap(WaterMap& map, const char* path, const WorldMask& wm, const TileMap& tm, const float* waterHeight, int resolution, bool bUseEDTOnly, float tileSizeInMetres)
{
	const int w = wm.m_w;
	const int h = wm.m_h;
	const int maskSizeInBytes = (w*h + 7)/8;

	map.m_resolution    = resolution;
	map.m_waterMask1    = new u8[maskSizeInBytes];
	map.m_waterMask2    = LoadWorldImageMask(wm, varString("%s/assembled_water.dds", path).c_str());
	map.m_waterHeight   = waterHeight;
	map.m_cells         = new WaterCell[w*h];
	map.m_offsetX       = (float)(gv::NAVMESH_BOUNDS_MIN_X + gv::WORLD_CELL_SIZE*gv::WORLD_CELLS_PER_TILE*(tm.m_tileMapX));
	map.m_offsetY       = (float)(gv::NAVMESH_BOUNDS_MIN_Y + gv::WORLD_CELL_SIZE*gv::WORLD_CELLS_PER_TILE*(tm.m_tileMapY + tm.m_tileMapH));
	map.m_cellsPerMetre = 0.5f*(float)resolution;
	map.m_tileResX      = (int)(tileSizeInMetres*map.m_cellsPerMetre);
	map.m_tileResY      = (int)(tileSizeInMetres*map.m_cellsPerMetre);
	map.m_tileNumX      = w/map.m_tileResX;
	map.m_tileNumY      = h/map.m_tileResY;
	map.m_tiles         = new WaterTile[map.m_tileNumX*map.m_tileNumY];
	map.m_bUseEDTOnly   = bUseEDTOnly;

	fprintf(stdout, "clearing cells .. ");
	fflush(stdout);

	if (bUseEDTOnly)
	{
		memcpy(map.m_waterMask1, map.m_waterMask2, maskSizeInBytes);

		for (int index = 0; index < w*h; index++)
		{
			map.m_cells[index].m_height = waterHeight[index];

			if (map.m_waterMask2[index/8] & BIT(index%8))
			{
				if (map.m_cells[index].m_height < -999.0f)
				{
					map.m_cells[index].m_height = 0.0f;
				}
			}
		}
	}
	else
	{
		memset(map.m_waterMask1, 0, maskSizeInBytes);

		for (int j = 0; j < wm.m_h; j++)
		{
			for (int i = 0; i < wm.m_w; i++)
			{
				const int index = i + j*wm.m_w;

				if (i < wm.m_worldMinX[j] || i > wm.m_worldMaxX[j] ||
					j < wm.m_worldMinY[i] || j > wm.m_worldMaxY[i])
				{
					map.m_cells[index].m_height = -1000.0f;
				}
				else
				{
					map.m_cells[index].m_height = waterHeight[index];

					if (waterHeight[index] > -999.0f)
					{
						map.m_waterMask1[index/8] |= BIT(index%8);
					}
				}
			}
		}
	}

	fprintf(stdout, "ok.\n");

	// copy world mask
	{
		fprintf(stdout, "copying world mask .. ");
		fflush(stdout);

		map.m_w          = wm.m_w;
		map.m_h          = wm.m_h;
		map.m_spanHeight = wm.m_spanHeight;
		map.m_worldMask  = new u8[maskSizeInBytes];
		map.m_worldMinX  = new int[h];
		map.m_worldMinY  = new int[w];
		map.m_worldMaxX  = new int[h];
		map.m_worldMaxY  = new int[w];

		memcpy(map.m_worldMask, wm.m_worldMask, maskSizeInBytes);
		memcpy(map.m_worldMinX, wm.m_worldMinX, h*sizeof(int));
		memcpy(map.m_worldMinY, wm.m_worldMinY, w*sizeof(int));
		memcpy(map.m_worldMaxX, wm.m_worldMaxX, h*sizeof(int));
		memcpy(map.m_worldMaxY, wm.m_worldMaxY, w*sizeof(int));

		fprintf(stdout, "ok.\n");
	}
}

void LoadWaterMapRivers(WaterMap& map, const char* path, bool bVerbose)
{
	//#riversbegin
	//	numrivers=2
	//	#river0
	//		numpoints=3
	//		point0=0,0,0,0
	//		point1=0,0,0,0
	//		point2=0,0,0,0
	//	#river1
	//		numpoints=2
	//		point0=0,0,0,0
	//		point1=0,0,0,0
	//#riversend

	if (map.m_bUseEDTOnly)
	{
		return;
	}

	FILE* f = fopen(path, "r");

	if (f)
	{
		const int numrivers = ParseFileLine<int>("numrivers", f);

		if (bVerbose)
		{
			fprintf(stdout, "loading %d rivers\n", numrivers);
		}

		ProgressDisplay progress("building %d rivers", numrivers);

		for (int riverIndex = 0; riverIndex < numrivers; riverIndex++)
		{
			const int numpoints = ParseFileLine<int>("numpoints", f);

			if (numpoints < 2)
			{
				fprintf(stdout, "ERROR: skipping river %d because it has %d points\n", riverIndex, numpoints);
				continue;
			}

			map.sm_rivers.push_back(new WaterRiver((int)map.sm_rivers.size()));
			WaterRiver& river = *map.sm_rivers.back();

			float x = 0.0f;
			float y = 0.0f;
			float t = 0.0f;

			for (int pointIndex = 0; pointIndex < numpoints; pointIndex++)
			{
				const Vec3 point3 = ParseFileLine<Vec3>(varString("point%d", pointIndex).c_str(), f);
				const Vec4 point(point3.x, point3.y, point3.z, 5.0f*RIVER_INFLUENCE_SCALE); // forcing rivers to have a fixed width

				river.m_points.push_back(point);
				river.m_range = Max<float>(point.w, river.m_range);
				river.m_bounds.m_xmin = Min<float>(point.x, river.m_bounds.m_xmin);
				river.m_bounds.m_ymin = Min<float>(point.y, river.m_bounds.m_ymin);
				river.m_bounds.m_xmax = Max<float>(point.x, river.m_bounds.m_xmax);
				river.m_bounds.m_ymax = Max<float>(point.y, river.m_bounds.m_ymax);

				if (pointIndex > 0)
				{
					const float dx = point.x - x;
					const float dy = point.y - y;

					t += sqrtf(dx*dx + dy*dy);

					x = point.x;
					y = point.y;
				}

				x = point.x;
				y = point.y;
			}

			river.m_length = t;

			x = river.m_points[0].x;
			y = river.m_points[0].y;
			t = 0.0f;

			for (int pointIndex = 1; pointIndex < numpoints; pointIndex++)
			{
				const Vec4& point = river.m_points[pointIndex];

				const float dx = point.x - x;
				const float dy = point.y - y;

				t += sqrtf(dx*dx + dy*dy);

				river.m_points[pointIndex].t = t/river.m_length;

				x = point.x;
				y = point.y;				
			}

			BuildRegionCells(map, river);

			progress.Update(riverIndex, numrivers);
		}

		progress.End();

		fclose(f);

		for (int riverIndex = 0; riverIndex < (int)map.sm_rivers.size(); riverIndex++)
		{
			fprintf(
				stdout,
				"river %d bounds is x=[%f..%f], y=[%f..%f]\n",
				riverIndex,
				map.sm_rivers[riverIndex]->m_bounds.m_xmin,
				map.sm_rivers[riverIndex]->m_bounds.m_xmax,
				map.sm_rivers[riverIndex]->m_bounds.m_ymin,
				map.sm_rivers[riverIndex]->m_bounds.m_ymax
			);
		}
	}
}

void LoadWaterMapLakes(WaterMap& map, const char* path, bool bVerbose)
{
	//#lakesbegin
	//	numlakes=2
	//	#lake0
	//		height=160.0
	//		influence=80.0
	//		numboxes=2
	//		box0=0,0,0,0
	//		box1=0,0,0,0
	//	#lake1
	//		height=-1.0
	//		influence=20.0
	//		numboxes=1
	//		box0=0,0,0,0
	//#lakesend

	if (map.m_bUseEDTOnly)
	{
		return;
	}

	FILE* f = fopen(path, "r");

	if (f)
	{
		const int numlakes = ParseFileLine<int>("numlakes", f);

		if (bVerbose)
		{
			fprintf(stdout, "loading %d lakes\n", numlakes);
		}

		for (int lakeIndex = 0; lakeIndex < numlakes; lakeIndex++)
		{
			const float height = ParseFileLine<float>("height", f);
			const float range = ParseFileLine<float>("influence", f);
			const int numboxes = ParseFileLine<int>("numboxes", f);

			if (numboxes < 1)
			{
				fprintf(stdout, "ERROR: skipping lake %d because it has %d boxes\n", lakeIndex, numboxes);
				continue;
			}

			map.sm_lakes.push_back(new WaterLake((int)map.sm_lakes.size()));
			WaterLake& lake = *map.sm_lakes.back();

			lake.m_height = height;
			lake.m_range = range;

			if (bVerbose)
			{
				fprintf(stdout, "loading %d boxes\n", numboxes);
			}

			for (int boxIndex = 0; boxIndex < numboxes; boxIndex++)
			{
				const Box2 box = ParseFileLine<Vec4>(varString("box%d", boxIndex).c_str(), f);

				lake.m_boxes.push_back(box);
				lake.m_bounds.m_xmin = Min<float>(box.m_xmin, lake.m_bounds.m_xmin);
				lake.m_bounds.m_ymin = Min<float>(box.m_ymin, lake.m_bounds.m_ymin);
				lake.m_bounds.m_xmax = Max<float>(box.m_xmax, lake.m_bounds.m_xmax);
				lake.m_bounds.m_ymax = Max<float>(box.m_ymax, lake.m_bounds.m_ymax);
			}

			BuildRegionCells(map, lake);

			const int local_w = lake.m_i1 - lake.m_i0;

			for (int boxIndex = 0; boxIndex < numboxes; boxIndex++)
			{
				int i0 = 0;
				int j0 = 0;
				int i1 = 0;
				int j1 = 0;
				map.ConvertWorldToCells(i0, j0, i1, j1, lake.m_boxes[boxIndex], 0.0f);

				i0 = Max<int>(0, i0); // clamp to water heightmap bounds
				j0 = Max<int>(0, j0);
				i1 = Min<int>(map.m_w, i1);
				j1 = Min<int>(map.m_h, j1);

				for (int j = j0; j < j1; j++)
				{
					for (int i = i0; i < i1; i++)
					{
						if (map.m_cells[i + j*map.m_w].m_height > -999.0f)
						{
							const int local_i = i - lake.m_i0;
							const int local_j = j - lake.m_j0;

							WaterCell& cell = lake.m_cells[local_i + local_j*local_w];

							if ((cell.m_flags & WaterCell::FLAG_WATER_CELL) == 0)
							{
								cell.m_flags |= WaterCell::FLAG_WATER_CELL;
								lake.m_numWaterCells++;
							}
						}
					}
				}
			}
		}

		fclose(f);
	}
}

void LoadWaterMapPools(WaterMap& map, const char* path, bool bVerbose)
{
	//#poolzonesbegin
	//	numpoolzones=11
	//		poolzone0=-2400,-150,350,1050 #swimming pools
	//		poolzone1=-3050,550,-2850,850 #west of swimming pools
	//		poolzone2=-1550,-750,-950,-150 #south of swimming pools
	//		poolzone3=-200,-400,250,-150 #southeast of swimming pools
	//		poolzone4=-800,-1100,-650,-900 #further south of swimming pools
	//		poolzone5=950,-800,1200,-450 #east park near LA river
	//		poolzone6=1600,-150,2150,750 #reservoir
	//		poolzone7=1350,1050,1500,1250 #northwest of reservoir
	//		poolzone8=-1950,2000,-1800,2150 #north of swimming pools
	//		poolzone9=-2300,3300,-1950,3550 #treatment
	//		poolzone10=-1750,2950,-1500,3150 #southeast of treatment
	//#poolzonesend

	FILE* f = fopen(path, "r");

	if (f)
	{
		const int numpoolzones = ParseFileLine<int>("numpoolzones", f);

		if (bVerbose)
		{
			fprintf(stdout, "loading %d pool zones\n", numpoolzones);
		}

		int poolIndex = 0;

		for (int zoneIndex = 0; zoneIndex < numpoolzones; zoneIndex++)
		{
			const Box2 box = ParseFileLine<Vec4>(varString("poolzone%d", zoneIndex).c_str(), f);

			map.sm_poolZones.push_back(new WaterPoolZone(box));

			int i0 = 0;
			int j0 = 0;
			int i1 = 0;
			int j1 = 0;
			map.ConvertWorldToCells(i0, j0, i1, j1, box, 0.0f);

			i0 = Max<int>(0, i0); // clamp to water heightmap bounds
			j0 = Max<int>(0, j0);
			i1 = Min<int>(map.m_w, i1);
			j1 = Min<int>(map.m_h, j1);

			if (map.m_bUseEDTOnly)
			{
				if (zoneIndex == 0)
				{
					i0 = 0;
					j0 = 0;
					i1 = map.m_w;
					j1 = map.m_h;
				}
				else
				{
					break;
				}
			}

			const int w = i1 - i0;
			const int h = j1 - j0;

			float* heights = new float[w*h];

			for (int j = 0; j < h; j++)
			{
				for (int i = 0; i < w; i++)
				{
					heights[i + j*w] = map.m_cells[(i + i0) + (j + j0)*map.m_w].m_height;
				}
			}

			// find water pools
			{
				ProgressDisplay progress("finding water pools for zone %d (%d,%d,%d,%d)", zoneIndex, (int)box.m_xmin, (int)box.m_ymin, (int)box.m_xmax, (int)box.m_ymax);

				const int visitedSize = (w*h + 7)/8;
				u8* visited = new u8[visitedSize];
				memset(visited, 0, visitedSize*sizeof(u8));

				const int queueSize = Max<int>(1024*1024, map.m_w*map.m_h); // enough?
				int queueSizeMax = 0;
				QueueElem* queue = new QueueElem[queueSize];

				int poolCountInZone = 0;

				for (int j = 0; j < h; j++)
				{
					for (int i = 0; i < w; i++)
					{
						const int index = i + j*w;

						if ((visited[index/8] & BIT(index%8)) == 0 && heights[index] > -999.0f)
						{
							int queueWriteIndex = 0;
							int queueReadIndex = 0;

							queue[queueWriteIndex++] = QueueElem(i, j);
							visited[index/8] |= BIT(index%8);

							while (queueReadIndex < queueWriteIndex)
							{
								const QueueElem p = queue[queueReadIndex++];

								for (int dj = -1; dj <= 1; dj++)
								{
									for (int di = -1; di <= 1; di++)
									{
										const int i2 = (int)p.i + di;
										const int j2 = (int)p.j + dj;

										if ((di == 0) != (dj == 0)) // not both zero or both non-zero
										{
											if (i2 >= 0 && i2 < w &&
												j2 >= 0 && j2 < h)
											{
												const int index2 = i2 + j2*w;

												if ((visited[index2/8] & BIT(index2%8)) == 0 && heights[index2] > -999.0f)
												{
													if (queueWriteIndex < queueSize)
													{
														queue[queueWriteIndex++] = QueueElem(i2, j2);
														queueSizeMax = Max<int>(queueWriteIndex, queueSizeMax);
														visited[index2/8] |= BIT(index2%8);
													}
													else
													{
														fprintf(stderr, "queue overrun!\n");
														system("pause");
														exit(-1);
													}
												}
											}
										}
									}
								}
							}

							map.sm_pools.push_back(new WaterPool(poolIndex));
							WaterPool& pool = *map.sm_pools.back();

							int pool_i0 = queue[0].i;
							int pool_j0 = queue[0].j;
							int pool_i1 = pool_i0;
							int pool_j1 = pool_j0;

							float pool_zmax = heights[queue[0].i + queue[0].j*w];

							for (int qi = 1; qi < queueWriteIndex; qi++)
							{
								pool_i0 = Min<int>(queue[qi].i, pool_i0);
								pool_j0 = Min<int>(queue[qi].j, pool_j0);
								pool_i1 = Max<int>(queue[qi].i, pool_i1);
								pool_j1 = Max<int>(queue[qi].j, pool_j1);

								pool_zmax = Max<float>(heights[queue[qi].i + queue[qi].j*w], pool_zmax);
							}

							pool_i0 += i0 + 0; // relative to zone
							pool_j0 += j0 + 0;
							pool_i1 += i0 + 1;
							pool_j1 += j0 + 1;

							pool.m_bounds = map.ConvertCellsToWorld(pool_i0, pool_j0, pool_i1, pool_j1);
							pool.m_height = pool_zmax;

							const float dx = (pool.m_bounds.m_xmax - pool.m_bounds.m_xmin);
							const float dy = (pool.m_bounds.m_ymax - pool.m_bounds.m_ymin);

							const float poolSize = Min<float>(sqrtf(dx*dx + dy*dy), POOL_INFLUENCE_MAX_SIZE);

							pool.m_range = powf(poolSize, POOL_INFLUENCE_EXPONENT)*POOL_INFLUENCE_MULTIPLIER;

							assert(pool.m_bounds.m_xmin < pool.m_bounds.m_xmax);
							assert(pool.m_bounds.m_ymin < pool.m_bounds.m_ymax);

							BuildRegionCells(map, pool, queue, queueWriteIndex);

							const int local_w = pool.m_i1 - pool.m_i0;

							for (int qi = 0; qi < queueWriteIndex; qi++)
							{
								const int local_i = queue[qi].i + i0 - pool.m_i0;
								const int local_j = queue[qi].j + j0 - pool.m_j0;

								WaterCell& cell = pool.m_cells[local_i + local_j*local_w];

								if ((cell.m_flags & WaterCell::FLAG_WATER_CELL) == 0)
								{
									cell.m_flags |= WaterCell::FLAG_WATER_CELL;
									pool.m_numWaterCells++;
								}
							}

							poolIndex++;
							poolCountInZone++;
						}
					}

					progress.Update(j, h);
				}

				delete[] queue;
				delete[] visited;

				progress.End(varString("%d pools", poolCountInZone).c_str());
			}

			delete[] heights;
		}

		fclose(f);
	}
}

void BuildWaterMapRivers(WaterMap& map)
{
	for (int i = 0; i < (int)map.sm_rivers.size(); i++)
	{
		map.ApplyRegionCells(*map.sm_rivers[i]);
	}
}

void BuildWaterMapLakes(WaterMap& map)
{
	for (int i = 0; i < (int)map.sm_lakes.size(); i++)
	{
		map.ApplyRegionCells(*map.sm_lakes[i]);
	}
}

void BuildWaterMapPools(WaterMap& map)
{
	for (int i = 0; i < (int)map.sm_pools.size(); i++)
	{
		map.ApplyRegionCells(*map.sm_pools[i]);
	}
}

void BuildWaterMapTiles(WaterMap& map, int margin)
{
	int maxIDsPerTile = 0;

	for (int j = 0; j <= map.m_h - map.m_tileResY; j += map.m_tileResY)
	{
		for (int i = 0; i <= map.m_w - map.m_tileResX; i += map.m_tileResX)
		{
			WaterTile& tile = map.m_tiles[(i/map.m_tileResX) + (j/map.m_tileResY)*map.m_tileNumX];

			for (int jj = -margin; jj < map.m_tileResY + margin; jj++)
			{
				for (int ii = -margin; ii < map.m_tileResX + margin; ii++)
				{
					if (i + ii >= 0 && i + ii < map.m_w &&
						j + jj >= 0 && j + jj < map.m_h)
					{
						const WaterCell& cell = map.m_cells[(i + ii) + (j + jj)*map.m_w];

						if (cell.m_type != WaterCell::TYPE_NONE)
						{
							tile.AddRefID(cell);
							maxIDsPerTile = Max<int>((int)tile.m_refIDs.size(), maxIDsPerTile);
						}
					}
				}
			}
		}
	}

	fprintf(stdout, "max IDs per tile = %d\n", maxIDsPerTile);
}

static void RenderGlowMask(Pixel32* image, int w, int h, const u8* mask, const Pixel32& colour, int blurRadius, int blurPasses, float overbright, u16 tileMinValue = 0, int tileSizeDiv = 4)
{
	const int tileSize = (blurRadius*blurPasses + 1)/tileSizeDiv; // NOTE -- dividing tile size by a small amount doesn't seem to affect the output, but makes it much faster
	const int tileW = (w + tileSize - 1)/tileSize;
	const int tileH = (h + tileSize - 1)/tileSize;
	const int tileMaskSizeInBytes = (tileW*tileH + 7)/8;
	u8* tileMask0 = new u8[tileMaskSizeInBytes];
	u8* tileMask1 = new u8[tileMaskSizeInBytes];
	memset(tileMask0, 0, tileMaskSizeInBytes);
	memset(tileMask1, 0, tileMaskSizeInBytes);

	u16* glowSrc = new u16[w*h];
	u16* glowDst = new u16[w*h];

	// set up tile mask and glow source
	{
		ProgressDisplay progress("setting up tile mask and glow source (%dx%d tiles)", tileSize, tileSize);

		for (int j = 0; j < h; j++)
		{
			for (int i = 0; i < w; i++)
			{
				const int index = i + j*w;

				if (mask[index/8] & BIT(index%8))
				{
					const int tileI = i/tileSize;
					const int tileJ = j/tileSize;
					const int tileIndex = tileI + tileJ*tileW;

					tileMask0[tileIndex/8] |= BIT(tileIndex%8);
					glowSrc[index] = 65535;
				}
				else
				{
					glowSrc[index] = 0;
				}
			}

			progress.Update(j, h);
		}

		progress.End();
	}

	memcpy(glowDst, glowSrc, w*h*sizeof(u16));

	// expand tile mask
	{
		ProgressDisplay progress("expanding tile mask");

		int numTiles0 = 0;
		int numTiles1 = 0;

		for (int tileJ = 0; tileJ < tileH; tileJ++)
		{
			for (int tileI = 0; tileI < tileW; tileI++)
			{
				const int tileIndex0 = tileI + tileJ*tileW;

				if (tileMask0[tileIndex0/8] & BIT(tileIndex0%8))
				{
					for (int dj = -1; dj <= 1; dj++)
					{
						for (int di = -1; di <= 1; di++)
						{
							const int ii = tileI + di;
							const int jj = tileJ + dj;

							if (ii >= 0 && ii < tileW &&
								jj >= 0 && jj < tileH)
							{
								const int tileIndex1 = ii + jj*tileW;

								if ((tileMask1[tileIndex1/8] & BIT(tileIndex1%8)) == 0)
								{
									tileMask1[tileIndex1/8] |= BIT(tileIndex1%8);
									numTiles1++;
								}
							}
						}
					}

					numTiles0++;
				}
			}

			progress.Update(tileJ, tileH);
		}

		progress.End(varString("%d tiles, %d expanded tiles (%.2f%% coverage)", numTiles0, numTiles1, 100.0f*(float)numTiles1/(float)(tileW*tileH)).c_str());
	}

	// compute glow
	{
		ProgressDisplay progress("computing glow (%d passes)", blurPasses);

		for (int blurPass = 0; blurPass < blurPasses; blurPass++)
		{
			for (int tileJ = 0; tileJ < tileH; tileJ++)
			{
				for (int tileI = 0; tileI < tileW; tileI++)
				{
					const int tileIndex = tileI + tileJ*tileW;

					if (tileMask1[tileIndex/8] & BIT(tileIndex%8))
					{
						for (int jj = 0; jj < tileSize; jj++)
						{
							for (int ii = 0; ii < tileSize; ii++)
							{
								const int i = ii + tileI*tileSize;
								const int j = jj + tileJ*tileSize;

								if (i < w &&
									j < h)
								{
									float p = 0.0f;
									float q = 0.0f;

									for (int dj = -blurRadius; dj <= blurRadius; dj++)
									{
										for (int di = -blurRadius; di <= blurRadius; di++)
										{
											const int i2 = i + di;
											const int j2 = j + dj;

											if (i2 >= 0 && i2 < w &&
												j2 >= 0 && j2 < h)
											{
												p += (float)glowSrc[i2 + j2*w];
												q += 1.0f;
											}
										}
									}

									u16& dst = glowDst[i + j*w];

									if (q > 0.0f)
									{
										dst = Max<u16>((u16)(0.5f + p/q), dst);
									}
									else
									{
										dst = 0;
									}

									dst = Max<u16>(tileMinValue, dst);
								}
							}
						}
					}
				}
			}

			std::swap(glowSrc, glowDst);
			progress.Update(blurPass, blurPasses);
		}

		progress.End();
	}

	const float cr = (float)colour.r/255.0f;
	const float cg = (float)colour.g/255.0f;
	const float cb = (float)colour.b/255.0f;
	const float ca = (float)colour.a/255.0f;

	// apply glow to image
	{
		ProgressDisplay progress("applying glow to image");

		for (int tileJ = 0; tileJ < tileH; tileJ++)
		{
			for (int tileI = 0; tileI < tileW; tileI++)
			{
				const int tileIndex = tileI + tileJ*tileW;

				if (tileMask1[tileIndex/8] & BIT(tileIndex%8))
				{
					for (int jj = 0; jj < tileSize; jj++)
					{
						for (int ii = 0; ii < tileSize; ii++)
						{
							const int i = ii + tileI*tileSize;
							const int j = jj + tileJ*tileSize;

							if (i < w &&
								j < h)
							{
								const int index = i + j*w;

								if (glowSrc[index] > 0)
								{
									Pixel32& c = image[index];

									float r = (float)c.r/255.0f;
									float g = (float)c.g/255.0f;
									float b = (float)c.b/255.0f;

									const float a = ca*Min<float>(1.0f, (overbright + 1.0f)*(float)glowSrc[index]/65535.0f);

									r += cr*a;
									g += cg*a;
									b += cb*a;

									c.r = (u8)(0.5f + 255.0f*Min<float>(r, 1.0f));
									c.g = (u8)(0.5f + 255.0f*Min<float>(g, 1.0f));
									c.b = (u8)(0.5f + 255.0f*Min<float>(b, 1.0f));
								}
							}
						}
					}
				}
			}

			progress.Update(tileJ, tileH);
		}

		progress.End();
	}

	delete[] tileMask0;
	delete[] tileMask1;
	delete[] glowSrc;
	delete[] glowDst;
}

void SaveWaterMapImages(const WaterMap& map, const char* path)
{
	const int w = map.m_w;
	const int h = map.m_h;

	for (int i = 0; i < (int)map.sm_rivers.size(); i++)
	{
		SaveRegionCellImage(*map.sm_rivers[i]);
	}

	for (int i = 0; i < (int)map.sm_lakes.size(); i++)
	{
		SaveRegionCellImage(*map.sm_lakes[i]);
	}

	//for (int i = 0; i < (int)map.sm_pools.size(); i++)
	//{
	//	SaveRegionCellImage(*map.sm_pools[i]);
	//}

	SaveWaterMapImage(map, varString("%s/composite_watermap_height.png", path).c_str(), IMAGE_TYPE_HEIGHT, 0.5f, 0.25f);
	SaveWaterMapImage(map, varString("%s/composite_watermap_weight.png", path).c_str(), IMAGE_TYPE_WEIGHT, 0.5f, 0.25f);
	SaveWaterMapImage(map, varString("%s/composite_watermap_colour.png", path).c_str(), IMAGE_TYPE_COLOUR_AND_WEIGHT, 0.5f, 0.25f);

	// composite colour image with height in alpha
	{
		int w0 = w;
		int h0 = h;
		Pixel32* colour = LoadImage_Pixel32(varString("%s/composite_watermap_colour.png", path).c_str(), w0, h0);

		if (colour)
		{
			const int scale = 2;
			const int w2 = w/scale;
			const int h2 = h/scale;
			Vec4* image = new Vec4[w2*h2];
			memset(image, 0, w2*h2*sizeof(Vec4));

			for (int j = 0; j < h2; j++)
			{
				for (int i = 0; i < w2; i++)
				{
					image[i + j*w2].x = (float)colour[(i + j*w)*scale].r/255.0f;
					image[i + j*w2].y = (float)colour[(i + j*w)*scale].g/255.0f;
					image[i + j*w2].z = (float)colour[(i + j*w)*scale].b/255.0f;
					image[i + j*w2].w = map.m_cells[(i + j*w)*scale].m_height;

					const float height = map.m_waterHeight[(i + j*w)*scale];

					if (height > -999.0f)
					{
						image[i + j*w2].w = height;
					}
				}
			}

			SaveImage(varString("%s/composite_watermap_colour.dds", path).c_str(), image, w2, h2, false);

			delete[] colour;
			delete[] image;
		}
	}

	Pixel32* image = new Pixel32[w*h];

	// composite water masks (drawable, collision, occluder)
	{
		for (int i = 0; i < w*h; i++)
		{
			image[i] = Pixel32(0,0,0,255);
		}

		const float oceanAmount = 0.25f;

		int w0 = w;
		int h0 = h;
		u8* ocean = LoadImage_u8(varString("%s/assembled_waterocean.png", path).c_str(), w0, h0);

		if (ocean)
		{
			for (int i = 0; i < w*h; i++)
			{
				float r = (float)image[i].r/255.0f;
				float g = (float)image[i].g/255.0f;
				float b = (float)image[i].b/255.0f;

				r += (0.5f - r)*oceanAmount*(float)ocean[i]/255.0f;
				g += (0.5f - g)*oceanAmount*(float)ocean[i]/255.0f;
				b += (1.0f - b)*oceanAmount*(float)ocean[i]/255.0f;

				image[i].r = (u8)(0.5f + 255.0f*r);
				image[i].g = (u8)(0.5f + 255.0f*g);
				image[i].b = (u8)(0.5f + 255.0f*b);
			}

			delete[] ocean;
		}

		u8* waterNoReflectMask = LoadWorldImageMask(map, varString("%s/assembled_waternoreflect.dds", path).c_str());

		if (waterNoReflectMask)
		{
			for (int i = 0; i < w*h; i++)
			{
				if (waterNoReflectMask[i/8] & BIT(i%8))
				{
					float r = (float)image[i].r/255.0f;
					float g = (float)image[i].g/255.0f;
					float b = (float)image[i].b/255.0f;

					r += (1.0f - r)*0.1f;
					g += (1.0f - g)*0.1f;
					b += (1.0f - b)*0.1f;

					image[i].r = (u8)(0.5f + 255.0f*r);
					image[i].g = (u8)(0.5f + 255.0f*g);
					image[i].b = (u8)(0.5f + 255.0f*b);
				}
			}

			delete[] waterNoReflectMask;
		}

		u8* waterDrawMask0 = LoadWorldImageMask(map, varString("%s/assembled_waterdraw.dds", path).c_str());
		u8* waterCollMask0 = LoadWorldImageMask(map, varString("%s/assembled_watercoll.dds", path).c_str());
		u8* waterOcclMask0 = LoadWorldImageMask(map, varString("%s/assembled_wateroccl.dds", path).c_str());

		const int maskSizeInBytes = (w*h + 7)/8;

		u8* waterDrawMask1 = new u8[maskSizeInBytes];
		u8* waterCollMask1 = new u8[maskSizeInBytes];
		u8* waterOcclMask1 = new u8[maskSizeInBytes];

		memset(waterDrawMask1, 0, maskSizeInBytes);
		memset(waterCollMask1, 0, maskSizeInBytes);
		memset(waterOcclMask1, 0, maskSizeInBytes);

		// set up masks for glow
		{
			ProgressDisplay progress("setting up masks for glow");

			for (int j = 0; j < h; j++)
			{
				for (int i = 0; i < w; i++)
				{
					const int index = i + j*w;

					const bool bHasWaterDraw0 = waterDrawMask0 ? ((waterDrawMask0[index/8] & BIT(index%8)) != 0) : false;
					const bool bHasWaterColl0 = waterCollMask0 ? ((waterCollMask0[index/8] & BIT(index%8)) != 0) : false;
					const bool bHasWaterOccl0 = waterOcclMask0 ? ((waterOcclMask0[index/8] & BIT(index%8)) != 0) : false;

					bool bHasWaterDraw1 = false;
					bool bHasWaterColl1 = false;
					bool bHasWaterOccl1 = false;

					const int dr = map.m_resolution*2;

					for (int dj = -dr; dj <= dr; dj++)
					{
						for (int di = -dr; di <= dr; di++)
						{
							const int ii = i + di;
							const int jj = j + dj;

							if (ii >= 0 && ii < w &&
								jj >= 0 && jj < h)
							{
								const int index2 = ii + jj*w;

								if (waterDrawMask0[index2/8] & BIT(index2%8)) { bHasWaterDraw1 = true; }
								if (waterCollMask0[index2/8] & BIT(index2%8)) { bHasWaterColl1 = true; }
								if (waterOcclMask0[index2/8] & BIT(index2%8)) { bHasWaterOccl1 = true; }
							}
						}
					}

					if (bHasWaterDraw0 && !(bHasWaterColl1 && bHasWaterOccl1)) { waterDrawMask1[index/8] |= BIT(index%8); }
					if (bHasWaterColl0 &&  (bHasWaterDraw1 != bHasWaterOccl1)) { waterCollMask1[index/8] |= BIT(index%8); }
					if (bHasWaterOccl0 && !(bHasWaterDraw1 && bHasWaterColl1)) { waterOcclMask1[index/8] |= BIT(index%8); }
				}

				progress.Update(j, h);
			}

			progress.End();
		}

		const int blurRadius = 5*map.m_resolution;
		const int blurPasses = 32;
		const float overbright = 3.0f;

		RenderGlowMask(image, w, h, waterDrawMask1, Pixel32(255,0,0,128), blurRadius, blurPasses, overbright);
		RenderGlowMask(image, w, h, waterCollMask1, Pixel32(0,255,0,128), blurRadius, blurPasses, overbright);
		RenderGlowMask(image, w, h, waterOcclMask1, Pixel32(0,0,255,128), blurRadius, blurPasses, overbright);

		// apply final masks
		{
			ProgressDisplay progress("applying final masks");

			for (int j = 0; j < h; j++)
			{
				for (int i = 0; i < w; i++)
				{
					const int index = i + j*w;
					Pixel32& c = image[index];

					// valid colours:
					// --------------
					// * white - drawable collision and occluder
					// * green - collision but no drawable or occluder (this is ok if there is a water_shallow/water_poolenv drawable or if this is interior water)

					// invalid colours (these are errors):
					// -----------------------------------
					// * red - drawable but no collision or occluder
					// * blue - occluder but not drawable or collision
					// * cyan - collision and occluder but not drawable
					// * magenta - drawable and occluder but no collision
					// * yellow - drawable and collision but no occluder

					if (waterDrawMask0[index/8] & BIT(index%8)) { c.r = 255; }
					if (waterCollMask0[index/8] & BIT(index%8)) { c.g = 255; }
					if (waterOcclMask0[index/8] & BIT(index%8)) { c.b = 255; }
				}

				progress.Update(j, h);
			}

			progress.End();
		}

		delete[] waterDrawMask0;
		delete[] waterCollMask0;
		delete[] waterOcclMask0;

		delete[] waterDrawMask1;
		delete[] waterCollMask1;
		delete[] waterOcclMask1;

		SaveImage(varString("%s/composite_water.png", path).c_str(), image, w, h, false);
	}

	// composite mirror masks (mirror drawable, mirror portal, water surface portal)
	{
		for (int i = 0; i < w*h; i++)
		{
			image[i] = Pixel32(0,0,0,255);
		}

		u8* mirrorDrawableMask      = LoadWorldImageMask(map, varString("%s/assembled_mirrordraw.dds", path).c_str());
		u8* mirrorSurfacePortalMask = LoadWorldImageMask(map, varString("%s/assembled_mirrorportal.dds", path).c_str());
		u8* waterSurfacePortalMask  = LoadWorldImageMask(map, varString("%s/assembled_waterportal.dds", path).c_str());

		const int blurRadius = 5*map.m_resolution;
		const int blurPasses = 40;
		const float overbright = 3.0f;

		RenderGlowMask(image, w, h, mirrorDrawableMask,      Pixel32(255,  0,  0,96), blurRadius, blurPasses, overbright, 0x0800);
		RenderGlowMask(image, w, h, mirrorSurfacePortalMask, Pixel32(  0,255,255,96), blurRadius, blurPasses, overbright, 0x0800);
		RenderGlowMask(image, w, h, waterSurfacePortalMask,  Pixel32(  0,  0,255,64), blurRadius, blurPasses, overbright, 0);

		// apply final masks
		{
			ProgressDisplay progress("applying final masks");

			for (int j = 0; j < h; j++)
			{
				for (int i = 0; i < w; i++)
				{
					const int index = i + j*w;
					Pixel32& c = image[index];

					// valid colours:
					// --------------
					// * white - mirror drawable and mirror portal
					// * blue - water portal

					// invalid colours (these are errors):
					// -----------------------------------
					// * red - water drawable but no water portal
					// * cyan - water portal but no water drawable

					if (mirrorDrawableMask     [index/8] & BIT(index%8)) { c.r = 255; }
					if (mirrorSurfacePortalMask[index/8] & BIT(index%8)) { c.g = 255; c.b = 255; }
					if (waterSurfacePortalMask [index/8] & BIT(index%8)) { c.b = Max<u8>(128, c.b); }
				}

				progress.Update(j, h);
			}

			progress.End();
		}

		delete[] mirrorDrawableMask;
		delete[] mirrorSurfacePortalMask;
		delete[] waterSurfacePortalMask;

		SaveImage(varString("%s/composite_mirror.png", path).c_str(), image, w, h, false);
	}

	// other glow masks (mirror drawable, mirror portal and water surface portal)
	/*{
		const char* maskName[] =
		{
			"assembled_mirrordraw",
			"assembled_mirrorportal",
			"assembled_waterportal",
		};

		for (int m = 0; m < NELEM(maskName); m++)
		{
			for (int i = 0; i < w*h; i++)
			{
				image[i] = Pixel32(0,0,0,255);
			}

			u8* mask = LoadWorldImageMask(map, varString("%s/%s.dds", path, maskName[m]).c_str());

			if (mask)
			{
				const int blurRadius = 5*map.m_resolution;
				const int blurPasses = 32;
				const float overbright = 3.0f;

				RenderGlowMask(image, w, h, mask, Pixel32(255,255,255,128), blurRadius, blurPasses, overbright);

				// apply final masks
				{
					ProgressDisplay progress("applying final mask");

					for (int j = 0; j < h; j++)
					{
						for (int i = 0; i < w; i++)
						{
							const int index = i + j*w;

							Pixel32& c = image[index];

							if (mask[index/8] & BIT(index%8))
							{
								c.r = 255;
								c.g = 255;
								c.b = 255;
							}
						}

						progress.Update(j, h);
					}

					progress.End();
				}

				SaveImage(varString("%s/%s_glow.png", path, maskName[m]).c_str(), image, w, h, false);
			}
		}
	}*/

	// composite IDs per tile (density)
	{
		for (int i = 0; i < w*h; i++)
		{
			image[i] = Pixel32(0,0,0,255);
		}

		float* temp = new float[map.m_tileNumX*map.m_tileNumY];
		int maxIDsPerTile = 0;

		for (int i = 0; i < map.m_tileNumX*map.m_tileNumY; i++)
		{
			maxIDsPerTile = Max<int>((int)map.m_tiles[i].m_refIDs.size(), maxIDsPerTile);
		}

		for (int j = 0; j < map.m_tileNumY; j++)
		{
			int tileColFirst = 0;
			int tileColLast = -1;

			for (int i = 0; i < map.m_tileNumX; i++)
			{
				if (map.m_tiles[i + j*map.m_tileNumX].m_refIDs.size() > 0)
				{
					tileColFirst = i;
					break;
				}
			}

			for (int i = map.m_tileNumX - 1; i >= 0; i--)
			{
				if (map.m_tiles[i + j*map.m_tileNumX].m_refIDs.size() > 0)
				{
					tileColLast = i;
					break;
				}
			}

			for (int i = 0; i < map.m_tileNumX; i++)
			{
				const int isInRLESpan = (i >= tileColFirst && i <= tileColLast && tileColFirst <= tileColLast) ? 1 : 0;
				const int index = i + j*map.m_tileNumX;

				temp[index] = (float)((int)map.m_tiles[index].m_refIDs.size() + isInRLESpan)/(float)(maxIDsPerTile + 1);
			}
		}

		for (int j = 0; j <= h - map.m_tileResY; j += map.m_tileResY)
		{
			for (int i = 0; i <= w - map.m_tileResX; i += map.m_tileResX)
			{
				for (int jj = 0; jj < map.m_tileResY; jj++)
				{
					for (int ii = 0; ii < map.m_tileResX; ii++)
					{
						float value = 0.0f;

						for (int dj = 0; dj <= 1; dj++)
						{
							for (int di = 0; di <= 1; di++)
							{
								const int gi = (Max<int>(i + ii + di - 1, 0)/map.m_tileResX) + (Min<int>(j + jj + dj, h - 1)/map.m_tileResY)*(w/map.m_tileResX);
								value = Max<float>(temp[gi], value);
							}
						}

						Pixel32& c = image[(i + ii) + (j + jj)*w];

						c.r = (u8)(0.5f + 255.0f*value);
						c.g = c.r;
						c.b = c.r;
					}
				}
			}
		}

		delete[] temp;

		DrawPixelMask(image, w, h, map.m_waterMask2, Pixel32(0,0,255,64));
		DrawPixelMask(image, w, h, map.m_waterMask1, Pixel32(0,0,255,255));
		DrawPixelGrid(image, w, h, map.m_tileResX, map.m_tileResY, Pixel32(153,255,204,255), Pixel32(255,255,255,255), 0.125f);

		for (int i = 0; i < (int)map.sm_rivers.size(); i++)
		{
			DrawRiverOverlay(image, w, h, map, *map.sm_rivers[i], Pixel32(255,0,0,255), 0.5f);
		}

		SaveImage(varString("%s/composite_watermap_density.png", path).c_str(), image, w, h, false);
	}

	// composite grid
	{
		u8* waterDrawMask = LoadWorldImageMask(map, varString("%s/assembled_waterdraw.dds", path).c_str());
		u8* waterOcclMask = LoadWorldImageMask(map, varString("%s/assembled_wateroccl.dds", path).c_str());

		for (int i = 0; i < w*h; i++)
		{
			image[i] = Pixel32(0,0,0,255);
		}

		DrawPixelBlocks(image, w, h, map.m_tileResX, map.m_tileResY, map.m_waterMask2, Pixel32(0,0,80,255));
		DrawPixelBlocks(image, w, h, map.m_tileResX, map.m_tileResY, waterOcclMask, Pixel32(0,80,80,255));

		for (int i = 0; i < (int)map.sm_poolZones.size(); i++)
		{
			DrawBoxOverlay(image, w, h, map, *map.sm_poolZones[i], Pixel32(255,128,0,255), 0.125f);
		}

		DrawPixelMask(image, w, h, map.m_waterMask2, Pixel32(0,0,255,255));
		DrawPixelMask(image, w, h, waterOcclMask, Pixel32(0,255,255,255));

		if (waterDrawMask)
		{
			for (int i = 0; i < w*h; i++)
			{
				if (waterDrawMask[i/8] & BIT(i%8))
				{
					image[i].r = 255;
				}
			}
		}

		DrawPixelGrid(image, w, h, map.m_tileResX, map.m_tileResY, Pixel32(153,255,204,255), Pixel32(255,255,255,255), 1.0f);

		delete[] waterOcclMask;

		SaveImage(varString("%s/composite_watermap_grid.png", path).c_str(), image, w, h, false);
	}

	delete[] image;
}

// ================================================================================================

u8* LoadWorldImageMask(const WorldMask& wm, const char* path, bool bInvert, bool bClipToWorldMask)
{
	const int w = wm.m_w;
	const int h = wm.m_h;

	const int maskSizeInBytes = (w*h + 7)/8;
	u8* mask = new u8[maskSizeInBytes];
	memset(mask, 0, maskSizeInBytes);

	ProgressDisplay progress("loading world image mask");

	for (int span = 0; span < h; span += wm.m_spanHeight)
	{
		float* temp = LoadImageDDSSpan_float(path, w, span, wm.m_spanHeight);

		if (temp)
		{
			for (int j = 0; j < wm.m_spanHeight; j++)
			{
				for (int i = 0; i < w; i++)
				{
					if (bInvert ? (temp[i + j*w] < 1.0f) : (temp[i + j*w] > 0.0f))
					{
						const int ii = i;
						const int jj = j + span;

						const int index = ii + jj*w;

						if (bClipToWorldMask)
						{
							if (ii < wm.m_worldMinX[jj] || ii > wm.m_worldMaxX[jj] ||
								jj < wm.m_worldMinY[ii] || jj > wm.m_worldMaxY[ii])
							{
								//continue;
							}

							if ((wm.m_worldMask[index/8] & BIT(index%8)) == 0)
							{
								continue;
							}
						}

						mask[index/8] |= BIT(index%8);
					}
				}
			}

			delete[] temp;
		}
		else
		{
			break;
		}

		progress.Update(span, h);
	}

	progress.End();

	return mask;
}

void DrawPixelMask(Pixel32* image, int w, int h, const u8* mask, const Pixel32& c1, const Pixel32& c0)
{
	if (mask == NULL)
	{
		return;
	}

	if (c1.a > 0 || c0.a > 0)
	{
		ProgressDisplay progress("drawing pixel mask");

		for (int j = 0; j < h; j++)
		{
			for (int i = 0; i < w; i++)
			{
				const int index = i + j*w;
				const Pixel32& fill = (mask[index/8] & BIT(index%8)) ? c1 : c0;

				if (fill.a > 0)
				{
					Pixel32& c = image[index];

					float r = (float)c.r/255.0f;
					float g = (float)c.g/255.0f;
					float b = (float)c.b/255.0f;

					r += ((float)fill.r/255.0f - r)*(float)fill.a/255.0f;
					g += ((float)fill.g/255.0f - g)*(float)fill.a/255.0f;
					b += ((float)fill.b/255.0f - b)*(float)fill.a/255.0f;

					c.r = (u8)(0.5f + 255.0f*r);
					c.g = (u8)(0.5f + 255.0f*g);
					c.b = (u8)(0.5f + 255.0f*b);
				}
			}

			progress.Update(j, h);
		}

		progress.End();
	}
}

void DrawPixelBlocks(Pixel32* image, int w, int h, int tileResX, int tileResY, const u8* mask, const Pixel32& c1, const Pixel32& c0)
{
	if (mask == NULL)
	{
		return;
	}

	if (c1.a > 0 || c0.a > 0)
	{
		const int bw = w/tileResX;
		const int bh = h/tileResY;
		const int blockMaskSizeInBytes = (bw*bh + 7)/8;
		u8* blockMask = new u8[blockMaskSizeInBytes];
		memset(blockMask, 0, blockMaskSizeInBytes);

		// create block mask
		{
			ProgressDisplay progress("creating block mask");

			for (int j = 0; j <= h - tileResY; j += tileResY)
			{
				for (int i = 0; i <= w - tileResX; i += tileResX)
				{
					bool bFlag = false;

					for (int jj = 0; jj < tileResY; jj++)
					{
						for (int ii = 0; ii < tileResX; ii++)
						{
							const int index = (i + ii) + (j + jj)*w;

							if (mask[index/8] & BIT(index%8))
							{
								bFlag = true;
								break;
							}
						}

						if (bFlag)
						{
							break;
						}
					}

					if (bFlag)
					{
						const int bi = (i/tileResX) + (j/tileResY)*bw;

						blockMask[bi/8] |= BIT(bi%8);
					}
				}

				progress.Update(j, h);
			}

			progress.End();
		}

		// draw block pixels
		{
			ProgressDisplay progress("drawing pixel blocks");

			for (int j = 0; j <= h - tileResY; j += tileResY)
			{
				for (int i = 0; i <= w - tileResX; i += tileResX)
				{
					for (int jj = 0; jj < tileResY; jj++)
					{
						for (int ii = 0; ii < tileResX; ii++)
						{
							bool bFlag = false;

							for (int dj = 0; dj <= 1; dj++)
							{
								for (int di = 0; di <= 1; di++)
								{
									const int gi = (Max<int>(i + ii + di - 1, 0)/tileResX) + (Min<int>(j + jj + dj, h - 1)/tileResY)*(w/tileResX);

									if (blockMask[gi/8] & BIT(gi%8))
									{
										bFlag = true;
										break;
									}
								}
							}

							const Pixel32& fill = bFlag ? c1 : c0;

							if (fill.a > 0)
							{
								Pixel32& c = image[(i + ii) + (j + jj)*w];

								float r = (float)c.r/255.0f;
								float g = (float)c.g/255.0f;
								float b = (float)c.b/255.0f;

								r += ((float)fill.r/255.0f - r)*(float)fill.a/255.0f;
								g += ((float)fill.g/255.0f - g)*(float)fill.a/255.0f;
								b += ((float)fill.b/255.0f - b)*(float)fill.a/255.0f;

								c.r = (u8)(0.5f + 255.0f*r);
								c.g = (u8)(0.5f + 255.0f*g);
								c.b = (u8)(0.5f + 255.0f*b);
							}
						}
					}
				}

				progress.Update(j, h);
			}

			progress.End();
		}

		delete[] blockMask;
	}
}

void DrawPixelGrid(Pixel32* image, int w, int h, int tileResX, int tileResY, const Pixel32& colourPrimary, const Pixel32& colourSecondary, float amount)
{
	if (amount > 0.0f)
	{
		ProgressDisplay progress("drawing pixel grid");

		const float r_primary = (float)colourPrimary.r/255.0f;
		const float g_primary = (float)colourPrimary.g/255.0f;
		const float b_primary = (float)colourPrimary.b/255.0f;

		const float r_secondary = (float)colourSecondary.r/255.0f;
		const float g_secondary = (float)colourSecondary.g/255.0f;
		const float b_secondary = (float)colourSecondary.b/255.0f;

		for (int j = 0; j <= h - tileResY; j += tileResY)
		{
			for (int i = 0; i <= w - tileResX; i += tileResX)
			{
				for (int jj = 0; jj < tileResY; jj++)
				{
					for (int ii = 0; ii < tileResX; ii++)
					{
						Pixel32& c = image[(i + ii) + (j + jj)*w];

						float r = (float)c.r/255.0f;
						float g = (float)c.g/255.0f;
						float b = (float)c.b/255.0f;

						float edgeScaleX = 0.25f;
						float edgeScaleY = 0.25f;
						float edgeWidthX = 3.0f;
						float edgeWidthY = 3.0f;

						const int u_ = i + ii;
						const int v_ = h - (j + jj) - 1;
						const int u  = u_/tileResX;
						const int v  = v_/tileResY;
						const int uu = u_%tileResX;
						const int vv = v_%tileResY;
						const int eu = (u_ + tileResX/2)/tileResX;
						const int ev = (v_ + tileResY/2)/tileResY;

						if (eu%10 == 0) { edgeScaleX = 0.3333f; edgeWidthX = 5.0f; }
						if (ev%10 == 0) { edgeScaleY = 0.3333f; edgeWidthY = 5.0f; }

						const float edge_i = edgeScaleX*powf(Max<float>(0.0f, 1.0f - (float)Min<int>(u_%tileResX, tileResX - u_%tileResX)/edgeWidthX), 2.0f);
						const float edge_j = edgeScaleY*powf(Max<float>(0.0f, 1.0f - (float)Min<int>(v_%tileResY, tileResY - v_%tileResY)/edgeWidthY), 2.0f);

						const float edge = amount*Max<float>(edge_i, edge_j);

						r += (r_primary - r)*edge;
						g += (g_primary - g)*edge;
						b += (b_primary - b)*edge;

						if (eu%50 == 0)
						{
							edgeScaleX = 0.5f;
							edgeWidthX = 8.0f;

							const float edge2 = amount*edgeScaleX*powf(Max<float>(0.0f, 1.0f - (float)Min<int>(u_%tileResX, tileResX - u_%tileResX)/edgeWidthX), 2.0f);

							r += (r_secondary - r)*edge2;
							g += (g_secondary - g)*edge2;
							b += (b_secondary - b)*edge2;
						}

						if (ev%50 == 0)
						{
							edgeScaleY = 0.5f;
							edgeWidthY = 8.0f;

							const float edge2 = amount*edgeScaleY*powf(Max<float>(0.0f, 1.0f - (float)Min<int>(v_%tileResY, tileResY - v_%tileResY)/edgeWidthY), 2.0f);

							r += (r_secondary - r)*edge2;
							g += (g_secondary - g)*edge2;
							b += (b_secondary - b)*edge2;
						}

						c.r = (u8)(0.5f + 255.0f*r);
						c.g = (u8)(0.5f + 255.0f*g);
						c.b = (u8)(0.5f + 255.0f*b);
					}
				}
			}

			progress.Update(j, h);
		}

		progress.End();
	}
}

void DrawBoxOverlay(Pixel32* image, int w, int h, const WaterMap& map, const Box2& box, const Pixel32& colour, float amount)
{
	if (amount > 0.0f)
	{
		int i0 = 0;
		int j0 = 0;
		int i1 = 0;
		int j1 = 0;
		map.ConvertWorldToCells(i0, j0, i1, j1, box);

		i0 = Max<int>(0, i0); // clamp to water heightmap bounds
		j0 = Max<int>(0, j0);
		i1 = Min<int>(map.m_w, i1);
		j1 = Min<int>(map.m_h, j1);

		for (int j = j0; j < j1; j++)
		{
			for (int i = i0; i < i1; i++)
			{
				Pixel32& c = image[i + j*w];

				float r = (float)c.r/255.0f;
				float g = (float)c.g/255.0f;
				float b = (float)c.b/255.0f;

				r += ((float)colour.r/255.0f - r)*amount;
				g += ((float)colour.g/255.0f - g)*amount;
				b += ((float)colour.b/255.0f - b)*amount;

				c.r = (u8)(0.5f + 255.0f*r);
				c.g = (u8)(0.5f + 255.0f*g);
				c.b = (u8)(0.5f + 255.0f*b);
			}
		}
	}
}

void DrawPointOverlay(Pixel32* image, int w, int h, const WaterMap& map, const Vec2& point, int r, int thickness, const Pixel32& colour, float amount)
{
	if (amount > 0.0f)
	{
		int i = 0;
		int j = 0;
		map.ConvertWorldToCells(i, j, point.x, point.y);

		for (int dj = -(r - 1); dj <= (r - 1); dj++)
		{
			for (int di = -(r - 1); di <= (r - 1); di++)
			{
				if (Abs<int>(di) <= thickness ||
					Abs<int>(dj) <= thickness)
				{
					float amount2 = amount*(1.0f - (float)Max<int>(Abs<int>(di), Abs<int>(dj))/(float)(r - 1));

					if (amount2 > 0.0f)
					{
						const int ii = i + di;
						const int jj = j + dj;

						if (ii >= 0 && ii < w &&
							jj >= 0 && jj < h)
						{
							Pixel32& c = image[ii + jj*w];

							float r = (float)c.r/255.0f;
							float g = (float)c.g/255.0f;
							float b = (float)c.b/255.0f;

							r += ((float)colour.r/255.0f - r)*amount2;
							g += ((float)colour.g/255.0f - g)*amount2;
							b += ((float)colour.b/255.0f - b)*amount2;

							c.r = (u8)(0.5f + 255.0f*r);
							c.g = (u8)(0.5f + 255.0f*g);
							c.b = (u8)(0.5f + 255.0f*b);
						}
					}
				}
			}
		}
	}
}

void DrawRiverOverlay(Pixel32* image, int w, int h, const WaterMap& map, const WaterRiver& river, const Pixel32& colour, float amount)
{
	for (int pointIndex = 0; pointIndex < (int)river.m_points.size(); pointIndex++)
	{
		const Vec4& p = river.m_points[pointIndex];
		DrawPointOverlay(image, w, h, map, Vec2(p.x, p.y), 10, 1, colour, amount);
	}
}

void DrawLakeOverlay(Pixel32* image, int w, int h, const WaterMap& map, const WaterLake& lake, const Pixel32& colour, float amount)
{
	for (int boxIndex = 0; boxIndex < (int)lake.m_boxes.size(); boxIndex++)
	{
		DrawBoxOverlay(image, w, h, map, lake.m_boxes[boxIndex], colour, amount);
	}
}

void DrawPoolOverlay(Pixel32* image, int w, int h, const WaterMap& map, const WaterPool& pool, const Pixel32& colour, float amount)
{
	DrawBoxOverlay(image, w, h, map, pool.m_bounds, colour, amount);
}

} // namespace Water

// ================================================================================================
// ================================================================================================

// the classes here must match GameWorldWaterHeight.cpp in game
namespace WaterFileWriter {

__declspec(align(16)) class Vec3V
{
public:
	float x, y, z, w_unused;
};

__declspec(align(16)) class Vec4V
{
public:
	float x, y, z, w;
};

namespace sysEndian {

static void SwapMe2(u16& x)
{
	const u16 x0 = (x >> (8*0))&0xff;
	const u16 x1 = (x >> (8*1))&0xff;

	x = (x0 << (8*1)) | (x1 << (8*0));
}

static void SwapMe4(u32& x)
{
	const u32 x0 = (x >> (8*0))&0xff;
	const u32 x1 = (x >> (8*1))&0xff;
	const u32 x2 = (x >> (8*2))&0xff;
	const u32 x3 = (x >> (8*3))&0xff;

	x = (x0 << (8*3)) | (x1 << (8*2)) | (x2 << (8*1)) | (x3 << (8*0));
}

template <typename T> static void SwapMe(T&) {}

template <> static void SwapMe<u16>(u16& x) { SwapMe2(*(u16*)&x); }
template <> static void SwapMe<s16>(s16& x) { SwapMe2(*(u16*)&x); }
template <> static void SwapMe<u32>(u32& x) { SwapMe4(*(u32*)&x); }
template <> static void SwapMe<s32>(int& x) { SwapMe4(*(u32*)&x); }
template <> static void SwapMe<float>(float& x) { SwapMe4(*(u32*)&x); }
template <> static void SwapMe<Vec3V>(Vec3V& x) { SwapMe<float>(x.x); SwapMe<float>(x.y); SwapMe<float>(x.z); }
template <> static void SwapMe<Vec4V>(Vec4V& x) { SwapMe<float>(x.x); SwapMe<float>(x.y); SwapMe<float>(x.z); SwapMe<float>(x.w); }

} // namespace sysEndian

class CWaterMapHeader
{
public:
	void ByteSwap()
	{
		sysEndian::SwapMe(m_dataSizeInBytes);
		sysEndian::SwapMe(m_offsetX);
		sysEndian::SwapMe(m_offsetY);
		sysEndian::SwapMe(m_tileSizeX);
		sysEndian::SwapMe(m_tileSizeY);
		sysEndian::SwapMe(m_numTileCols);
		sysEndian::SwapMe(m_numTileRows);
		sysEndian::SwapMe(m_numTiles);
		sysEndian::SwapMe(m_numRefIDs);
		sysEndian::SwapMe(m_numRiverPoints);
		sysEndian::SwapMe(m_numRivers);
		sysEndian::SwapMe(m_numLakeBoxes);
		sysEndian::SwapMe(m_numLakes);
		sysEndian::SwapMe(m_numPools);
	}

	u32   m_dataSizeInBytes;
	float m_offsetX;
	float m_offsetY;
	float m_tileSizeX;
	float m_tileSizeY;
	u16   m_numTileCols;
	u16   m_numTileRows;
	u32   m_numTiles;
	u32   m_numRefIDs;
	u16   m_numRiverPoints;
	u16   m_numRivers;
	u16   m_numLakeBoxes;
	u16   m_numLakes;
	u16   m_numPools;

	u8    m_sizeofWaterMapHeader;
	u8    m_sizeofWaterMapTileRow;
	u8    m_sizeofWaterMapTile;
	u8    m_sizeofWaterMapRefID;
	u8    m_sizeofWaterMapRiverPoint;
	u8    m_sizeofWaterMapRiver;
	u8    m_sizeofWaterMapLakeBox;
	u8    m_sizeofWaterMapLake;
	u8    m_sizeofWaterMapPool;
};

class CWaterMapTileRow
{
public:
	static bool IsAligned() { return false; }

	void ByteSwap()
	{
		sysEndian::SwapMe(m_first);
		sysEndian::SwapMe(m_count);
		sysEndian::SwapMe(m_start);
	}

	u8  m_first; // column index of first tile
	u8  m_count; // number of tiles
	u16 m_start; // index of first tile in m_tiles
};

class CWaterMapTile
{
public:
	static bool IsAligned() { return false; }

	void ByteSwap()
	{
		sysEndian::SwapMe(m_start);
	}

	u16 m_start; // index of first refID in m_refIDs (
};

class CWaterMapRefID
{
public:
	static bool IsAligned() { return false; }

	void ByteSwap()
	{
		sysEndian::SwapMe(m_typeIndexSegment);
	}

	enum Type
	{
		TYPE_OCEAN = 0,
		TYPE_RIVER,
		TYPE_LAKE,
		TYPE_POOL,
	};

	void Set(const Water::WaterRefID& refID, bool bLastRefInTile)
	{
		if (refID.m_type == Water::WaterCell::TYPE_POOL)
		{
			// last:1, type:2, index:13
			assert(refID.m_type < 4);
			assert(refID.m_index < 8192);
			assert(refID.m_segment == 0);
			m_typeIndexSegment = (((u16)refID.m_type) << 13) | (refID.m_index << 0);
		}
		else
		{
			// last:1, type:2, index:6, segment:7
			assert(refID.m_type < 4);
			assert(refID.m_index < 64);
			assert(refID.m_segment < 128);
			m_typeIndexSegment = (((u16)refID.m_type) << 13) | (refID.m_index << 7) | (u16)refID.m_segment;
		}

		if (bLastRefInTile)
		{
			m_typeIndexSegment |= 0x8000;
		}
	}

	u16 m_typeIndexSegment;
};

class CWaterMapRegionBase
{
public:
	static bool IsAligned() { return true; }

	void ByteSwap()
	{
		sysEndian::SwapMe(m_centreAndHeight);
		sysEndian::SwapMe(m_extentAndWeight);
	}

	Vec3V m_centreAndHeight;
	Vec3V m_extentAndWeight;
};

class CWaterMapRiverPoint
{
public:
	static bool IsAligned() { return true; }

	void ByteSwap()
	{
		sysEndian::SwapMe(m_point);
	}

	Vec4V m_point;
};

class CWaterMapRiver : public CWaterMapRegionBase
{
public:
	void ByteSwap()
	{
		CWaterMapRegionBase::ByteSwap();

		sysEndian::SwapMe(m_count);
		sysEndian::SwapMe(m_pad);
		sysEndian::SwapMe(m_start);
	}

	u8  m_count; // number of points in this river
	u8  m_pad;
	u16 m_start; // index of first point in m_riverPoints
};

class CWaterMapLakeBox
{
public:
	static bool IsAligned() { return true; }

	void ByteSwap()
	{
		sysEndian::SwapMe(m_box);
	}

	Vec4V m_box; // centre xy, extent xy
};

class CWaterMapLake : public CWaterMapRegionBase
{
public:
	void ByteSwap()
	{
		CWaterMapRegionBase::ByteSwap();

		sysEndian::SwapMe(m_count);
		sysEndian::SwapMe(m_pad);
		sysEndian::SwapMe(m_start);
	}

	u8  m_count; // number of boxes in this lake
	u8  m_pad;
	u16 m_start; // index of first box in m_lakeBoxes
};

class CWaterMapPool : public CWaterMapRegionBase
{
public:
};

class CWaterMapRegionBaseDebugInfo
{
public:
	static bool IsAligned() { return false; }

	void ByteSwap()
	{
	}

	u8 m_colour[4];
};

class CWaterMapRiverDebugInfo : public CWaterMapRegionBaseDebugInfo
{
public:
};

class CWaterMapLakeDebugInfo : public CWaterMapRegionBaseDebugInfo
{
public:
};

class CWaterMapPoolDebugInfo : public CWaterMapRegionBaseDebugInfo
{
public:
};

template <typename T> static int UpdateDataOffset(int& offset, int& current, int count)
{
	if (T::IsAligned())
	{
		offset = (offset + 15)&~15;
	}
	else
	{
		offset = (offset + 3)&~3;
	}

	current = offset;
	offset += count*sizeof(T);

	return count;
}

void Save(const Water::WaterMap& map, const char* path, bool bNeedsByteSwap)
{
	FILE* f = fopen(path, "wb");

	if (f)
	{
		u32 tag = 'WMAP';
		u32 version = 100;

		if (bNeedsByteSwap)
		{
			sysEndian::SwapMe(tag);
			sysEndian::SwapMe(version);
		}

		fwrite(&tag, sizeof(tag), 1, f);
		fwrite(&version, sizeof(version), 1, f);

		int numTiles       = 0;
		int numRefIDs      = 0;
		int numRiverPoints = 0;
		int numRivers      = (int)map.sm_rivers.size();
		int numLakeBoxes   = 0;
		int numLakes       = (int)map.sm_lakes.size();
		int numPools       = (int)map.sm_pools.size();

		int* tileColFirst = new int[map.m_tileNumY];
		int* tileColLast  = new int[map.m_tileNumY];

		for (int j = 0; j < map.m_tileNumY; j++)
		{
			tileColFirst[j] = 0;
			tileColLast[j] = -1;

			for (int i = 0; i < map.m_tileNumX; i++)
			{
				if (map.m_tiles[i + j*map.m_tileNumX].m_refIDs.size() > 0)
				{
					tileColFirst[j] = i;
					break;
				}
			}

			for (int i = map.m_tileNumX - 1; i >= 0; i--)
			{
				if (map.m_tiles[i + j*map.m_tileNumX].m_refIDs.size() > 0)
				{
					tileColLast[j] = i;
					break;
				}
			}

			if (tileColFirst[j] <= tileColLast[j])
			{
				numTiles += tileColLast[j] - tileColFirst[j] + 1;

				for (int i = tileColFirst[j]; i <= tileColLast[j]; i++)
				{
					numRefIDs += (int)map.m_tiles[i + j*map.m_tileNumX].m_refIDs.size();
				}
			}
		}

		for (int i = 0; i < (int)map.sm_rivers.size(); i++)
		{
			numRiverPoints += (int)map.sm_rivers[i]->m_points.size();
		}

		for (int i = 0; i < (int)map.sm_lakes.size(); i++)
		{
			numLakeBoxes += (int)map.sm_lakes[i]->m_boxes.size();
		}

		CWaterMapHeader header;

		int dataOffsetTileRows    = 0;
		int dataOffsetTiles       = 0;
		int dataOffsetRefIDs      = 0;
		int dataOffsetRiverPoints = 0;
		int dataOffsetRivers      = 0;
		int dataOffsetLakeBoxes   = 0;
		int dataOffsetLakes       = 0;
		int dataOffsetPools       = 0;
		int dataOffset            = 0;

		header.m_offsetX          = map.m_offsetX;
		header.m_offsetY          = map.m_offsetY;
		header.m_tileSizeX        = (float)map.m_tileResX/map.m_cellsPerMetre;
		header.m_tileSizeY        = (float)map.m_tileResY/map.m_cellsPerMetre;
		header.m_numTileCols      = (u16)map.m_tileNumX;
		header.m_numTileRows      = (u16)UpdateDataOffset<CWaterMapTileRow   >(dataOffset, dataOffsetTileRows   , map.m_tileNumY);
		header.m_numTiles         = (u32)UpdateDataOffset<CWaterMapTile      >(dataOffset, dataOffsetTiles      , numTiles      );
		header.m_numRefIDs        = (u32)UpdateDataOffset<CWaterMapRefID     >(dataOffset, dataOffsetRefIDs     , numRefIDs     );
		header.m_numRiverPoints   = (u16)UpdateDataOffset<CWaterMapRiverPoint>(dataOffset, dataOffsetRiverPoints, numRiverPoints);
		header.m_numRivers        = (u16)UpdateDataOffset<CWaterMapRiver     >(dataOffset, dataOffsetRivers     , numRivers     );
		header.m_numLakeBoxes     = (u16)UpdateDataOffset<CWaterMapLakeBox   >(dataOffset, dataOffsetLakeBoxes  , numLakeBoxes  );
		header.m_numLakes         = (u16)UpdateDataOffset<CWaterMapLake      >(dataOffset, dataOffsetLakes      , numLakes      );
		header.m_numPools         = (u16)UpdateDataOffset<CWaterMapPool      >(dataOffset, dataOffsetPools      , numPools      );
		header.m_dataSizeInBytes  = dataOffset;

		header.m_sizeofWaterMapHeader     = (u8)sizeof(CWaterMapHeader    );
		header.m_sizeofWaterMapTileRow    = (u8)sizeof(CWaterMapTileRow   );
		header.m_sizeofWaterMapTile       = (u8)sizeof(CWaterMapTile      );
		header.m_sizeofWaterMapRefID      = (u8)sizeof(CWaterMapRefID     );
		header.m_sizeofWaterMapRiverPoint = (u8)sizeof(CWaterMapRiverPoint);
		header.m_sizeofWaterMapRiver      = (u8)sizeof(CWaterMapRiver     );
		header.m_sizeofWaterMapLakeBox    = (u8)sizeof(CWaterMapLakeBox   );
		header.m_sizeofWaterMapLake       = (u8)sizeof(CWaterMapLake      );
		header.m_sizeofWaterMapPool       = (u8)sizeof(CWaterMapPool      );

		if (bNeedsByteSwap)
		{
			header.ByteSwap();
		}

		fwrite(&header, sizeof(header), 1, f);

		// report memory sizes
		{
			int numTiles1Ref = 0;
			int numTiles2OrMoreRefs = 0;

			for (int i = 0; i < map.m_tileNumX*map.m_tileNumY; i++)
			{
				if (map.m_tiles[i].m_refIDs.size() == 1)
				{
					numTiles1Ref++;
				}
				else if (map.m_tiles[i].m_refIDs.size() >= 2)
				{
					numTiles2OrMoreRefs++;
				}
			}

			const int dataSizeHeader      = sizeof(CWaterMapHeader);
			const int dataSizeTileRows    = dataOffsetTiles       - dataOffsetTileRows   ;
			const int dataSizeTiles       = dataOffsetRefIDs      - dataOffsetTiles      ;
			const int dataSizeRefIDs      = dataOffsetRiverPoints - dataOffsetRefIDs     ;
			const int dataSizeRiverPoints = dataOffsetRivers      - dataOffsetRiverPoints;
			const int dataSizeRivers      = dataOffsetLakeBoxes   - dataOffsetRivers     ;
			const int dataSizeLakeBoxes   = dataOffsetLakes       - dataOffsetLakeBoxes  ;
			const int dataSizeLakes       = dataOffsetPools       - dataOffsetLakes      ;
			const int dataSizePools       = dataOffset            - dataOffsetPools      ;
			const int dataSizeTotal       = dataOffset            + dataSizeHeader       ;

			int debugInfoSize = 0;

			debugInfoSize += numRivers*sizeof(CWaterMapRiverDebugInfo);
			debugInfoSize += numLakes*sizeof(CWaterMapLakeDebugInfo);
			debugInfoSize += numPools*sizeof(CWaterMapPoolDebugInfo);

			fprintf(stdout, "    %.3fKB header\n", (float)dataSizeHeader/1024.0f);
			fprintf(stdout, "    %.3fKB tile rows"     " (%d x %d bytes)\n", (float)dataSizeTileRows   /1024.0f, map.m_tileNumY, sizeof(CWaterMapTileRow   ));
			fprintf(stdout, "    %.3fKB tiles"         " (%d x %d bytes), %d with 1 ref, %d with >1 ref\n", (float)dataSizeTiles/1024.0f, numTiles, sizeof(CWaterMapTile), numTiles1Ref, numTiles2OrMoreRefs);
			fprintf(stdout, "    %.3fKB reference IDs" " (%d x %d bytes)\n", (float)dataSizeRefIDs     /1024.0f, numRefIDs     , sizeof(CWaterMapRefID     ));
			fprintf(stdout, "    %.3fKB river points"  " (%d x %d bytes)\n", (float)dataSizeRiverPoints/1024.0f, numRiverPoints, sizeof(CWaterMapRiverPoint));
			fprintf(stdout, "    %.3fKB rivers"        " (%d x %d bytes)\n", (float)dataSizeRivers     /1024.0f, numRivers     , sizeof(CWaterMapRiver     ));
			fprintf(stdout, "    %.3fKB lake boxes"    " (%d x %d bytes)\n", (float)dataSizeLakeBoxes  /1024.0f, numLakeBoxes  , sizeof(CWaterMapLakeBox   ));
			fprintf(stdout, "    %.3fKB lakes"         " (%d x %d bytes)\n", (float)dataSizeLakes      /1024.0f, numLakes      , sizeof(CWaterMapLake      ));
			fprintf(stdout, "    %.3fKB pools"         " (%d x %d bytes)\n", (float)dataSizePools      /1024.0f, numPools      , sizeof(CWaterMapPool      ));
			fprintf(stdout, "    %.3fKB total\n"     , (float)dataSizeTotal/1024.0f);
			fprintf(stdout, "    %.3fKB debug info\n", (float)debugInfoSize/1024.0f);
		}

		const int dataSizeInBytes = dataOffset;
		u8* data = new u8[dataSizeInBytes];
		memset(data, 0, dataSizeInBytes);

		CWaterMapTileRow   * tileRows    = reinterpret_cast<CWaterMapTileRow   *>(data + dataOffsetTileRows   );
		CWaterMapTile      * tiles       = reinterpret_cast<CWaterMapTile      *>(data + dataOffsetTiles      );
		CWaterMapRefID     * refIDs      = reinterpret_cast<CWaterMapRefID     *>(data + dataOffsetRefIDs     );
		CWaterMapRiverPoint* riverPoints = reinterpret_cast<CWaterMapRiverPoint*>(data + dataOffsetRiverPoints);
		CWaterMapRiver     * rivers      = reinterpret_cast<CWaterMapRiver     *>(data + dataOffsetRivers     );
		CWaterMapLakeBox   * lakeBoxes   = reinterpret_cast<CWaterMapLakeBox   *>(data + dataOffsetLakeBoxes  );
		CWaterMapLake      * lakes       = reinterpret_cast<CWaterMapLake      *>(data + dataOffsetLakes      );
		CWaterMapPool      * pools       = reinterpret_cast<CWaterMapPool      *>(data + dataOffsetPools      );

		int tileRowIndex    = 0;
		int tileIndex       = 0;
		int refIDIndex      = 0;
		int riverPointIndex = 0;
		int riverIndex      = 0;
		int lakeBoxIndex    = 0;
		int lakeIndex       = 0;
		int poolIndex       = 0;

		for (int j = 0; j < map.m_tileNumY; j++)
		{
			if (tileColFirst[j] <= tileColLast[j])
			{
				tileRows[tileRowIndex].m_first = (u8)tileColFirst[j];
				tileRows[tileRowIndex].m_count = (u8)(tileColLast[j] - tileColFirst[j] + 1);
				tileRows[tileRowIndex].m_start = (u16)tileIndex;

				for (int i = tileColFirst[j]; i <= tileColLast[j]; i++)
				{
					const int numRefIDs = (int)map.m_tiles[i + j*map.m_tileNumX].m_refIDs.size();

					tiles[tileIndex].m_start = (numRefIDs > 0) ? (u16)refIDIndex : 0xffff;

					for (int k = 0; k < numRefIDs; k++)
					{
						refIDs[refIDIndex].Set(map.m_tiles[i + j*map.m_tileNumX].m_refIDs[k], k == numRefIDs - 1);
						refIDIndex++;
					}

					tileIndex++;
				}
			}

			tileRowIndex++;
		}

		delete[] tileColFirst;
		delete[] tileColLast;

		for (int i = 0; i < (int)map.sm_rivers.size(); i++)
		{
			rivers[riverIndex].m_centreAndHeight.x = map.sm_rivers[i]->m_bounds.GetCentreX();
			rivers[riverIndex].m_centreAndHeight.y = map.sm_rivers[i]->m_bounds.GetCentreY();
			rivers[riverIndex].m_centreAndHeight.z = 0.0f;
			rivers[riverIndex].m_extentAndWeight.x = map.sm_rivers[i]->m_bounds.GetExtentX();
			rivers[riverIndex].m_extentAndWeight.y = map.sm_rivers[i]->m_bounds.GetExtentY();
			rivers[riverIndex].m_extentAndWeight.z = 1.0f/(map.sm_rivers[i]->m_range*map.sm_rivers[i]->m_range);

			rivers[riverIndex].m_count = (int)map.sm_rivers[i]->m_points.size();
			rivers[riverIndex].m_start = riverPointIndex;

			for (int k = 0; k < (int)map.sm_rivers[i]->m_points.size(); k++)
			{
				riverPoints[riverPointIndex].m_point.x = map.sm_rivers[i]->m_points[k].x;
				riverPoints[riverPointIndex].m_point.y = map.sm_rivers[i]->m_points[k].y;
				riverPoints[riverPointIndex].m_point.z = map.sm_rivers[i]->m_points[k].z;
				riverPoints[riverPointIndex].m_point.w = map.sm_rivers[i]->m_points[k].w;
				riverPointIndex++;
			}

			riverIndex++;
		}

		for (int i = 0; i < (int)map.sm_lakes.size(); i++)
		{
			lakes[lakeIndex].m_centreAndHeight.x = map.sm_lakes[i]->m_bounds.GetCentreX();
			lakes[lakeIndex].m_centreAndHeight.y = map.sm_lakes[i]->m_bounds.GetCentreY();
			lakes[lakeIndex].m_centreAndHeight.z = map.sm_lakes[i]->m_height;
			lakes[lakeIndex].m_extentAndWeight.x = map.sm_lakes[i]->m_bounds.GetExtentX();
			lakes[lakeIndex].m_extentAndWeight.y = map.sm_lakes[i]->m_bounds.GetExtentY();
			lakes[lakeIndex].m_extentAndWeight.z = 1.0f/(map.sm_lakes[i]->m_range*map.sm_lakes[i]->m_range);

			lakes[lakeIndex].m_count = (int)map.sm_lakes[i]->m_boxes.size();
			lakes[lakeIndex].m_start = lakeBoxIndex;

			for (int k = 0; k < (int)map.sm_lakes[i]->m_boxes.size(); k++)
			{
				lakeBoxes[lakeBoxIndex].m_box.x = (map.sm_lakes[i]->m_boxes[k].m_xmax + map.sm_lakes[i]->m_boxes[k].m_xmin)*0.5f;
				lakeBoxes[lakeBoxIndex].m_box.y = (map.sm_lakes[i]->m_boxes[k].m_ymax + map.sm_lakes[i]->m_boxes[k].m_ymin)*0.5f;
				lakeBoxes[lakeBoxIndex].m_box.z = (map.sm_lakes[i]->m_boxes[k].m_xmax - map.sm_lakes[i]->m_boxes[k].m_xmin)*0.5f;
				lakeBoxes[lakeBoxIndex].m_box.w = (map.sm_lakes[i]->m_boxes[k].m_ymax - map.sm_lakes[i]->m_boxes[k].m_ymin)*0.5f;
				lakeBoxIndex++;
			}

			lakeIndex++;
		}

		for (int i = 0; i < (int)map.sm_pools.size(); i++)
		{
			pools[poolIndex].m_centreAndHeight.x = map.sm_pools[i]->m_bounds.GetCentreX();
			pools[poolIndex].m_centreAndHeight.y = map.sm_pools[i]->m_bounds.GetCentreY();
			pools[poolIndex].m_centreAndHeight.z = map.sm_pools[i]->m_height;
			pools[poolIndex].m_extentAndWeight.x = map.sm_pools[i]->m_bounds.GetExtentX();
			pools[poolIndex].m_extentAndWeight.y = map.sm_pools[i]->m_bounds.GetExtentY();
			pools[poolIndex].m_extentAndWeight.z = 1.0f/(map.sm_pools[i]->m_range*map.sm_pools[i]->m_range);
			poolIndex++;
		}

		if (bNeedsByteSwap)
		{
			for (int i = 0; i < tileRowIndex   ; i++) { tileRows   [i].ByteSwap(); }
			for (int i = 0; i < tileIndex      ; i++) { tiles      [i].ByteSwap(); }
			for (int i = 0; i < refIDIndex     ; i++) { refIDs     [i].ByteSwap(); }
			for (int i = 0; i < riverPointIndex; i++) { riverPoints[i].ByteSwap(); }
			for (int i = 0; i < riverIndex     ; i++) { rivers     [i].ByteSwap(); }
			for (int i = 0; i < lakeBoxIndex   ; i++) { lakeBoxes  [i].ByteSwap(); }
			for (int i = 0; i < lakeIndex      ; i++) { lakes      [i].ByteSwap(); }
			for (int i = 0; i < poolIndex      ; i++) { pools      [i].ByteSwap(); }
		}

		fwrite(data, dataSizeInBytes, 1, f);

		// river debug info
		{
			CWaterMapRiverDebugInfo* riverDebugInfo = new CWaterMapRiverDebugInfo[(int)map.sm_rivers.size()];

			for (int i = 0; i < (int)map.sm_rivers.size(); i++)
			{
				riverDebugInfo[i].m_colour[0] = map.sm_rivers[i]->m_colour.r;
				riverDebugInfo[i].m_colour[1] = map.sm_rivers[i]->m_colour.g;
				riverDebugInfo[i].m_colour[2] = map.sm_rivers[i]->m_colour.b;
				riverDebugInfo[i].m_colour[3] = 255;

				if (bNeedsByteSwap)
				{
					riverDebugInfo[i].ByteSwap();
				}
			}

			fwrite(riverDebugInfo, (int)map.sm_rivers.size(), sizeof(CWaterMapRiverDebugInfo), f);

			delete[] riverDebugInfo;
		}

		// lake debug info
		{
			CWaterMapLakeDebugInfo* lakeDebugInfo = new CWaterMapLakeDebugInfo[(int)map.sm_lakes.size()];

			for (int i = 0; i < (int)map.sm_lakes.size(); i++)
			{
				lakeDebugInfo[i].m_colour[0] = map.sm_lakes[i]->m_colour.r;
				lakeDebugInfo[i].m_colour[1] = map.sm_lakes[i]->m_colour.g;
				lakeDebugInfo[i].m_colour[2] = map.sm_lakes[i]->m_colour.b;
				lakeDebugInfo[i].m_colour[3] = 255;

				if (bNeedsByteSwap)
				{
					lakeDebugInfo[i].ByteSwap();
				}
			}

			fwrite(lakeDebugInfo, (int)map.sm_lakes.size(), sizeof(CWaterMapLakeDebugInfo), f);

			delete[] lakeDebugInfo;
		}

		// pool debug info
		{
			CWaterMapPoolDebugInfo* poolDebugInfo = new CWaterMapPoolDebugInfo[(int)map.sm_pools.size()];

			for (int i = 0; i < (int)map.sm_pools.size(); i++)
			{
				poolDebugInfo[i].m_colour[0] = map.sm_pools[i]->m_colour.r;
				poolDebugInfo[i].m_colour[1] = map.sm_pools[i]->m_colour.g;
				poolDebugInfo[i].m_colour[2] = map.sm_pools[i]->m_colour.b;
				poolDebugInfo[i].m_colour[3] = 255;

				if (bNeedsByteSwap)
				{
					poolDebugInfo[i].ByteSwap();
				}
			}

			fwrite(poolDebugInfo, (int)map.sm_pools.size(), sizeof(CWaterMapPoolDebugInfo), f);

			delete[] poolDebugInfo;
		}

		fclose(f);
	}
	else
	{
		fprintf(stdout, "failed to save %s, maybe it's not checked out?\n", path);
	}
}

} // namespace WaterFileWriter
