// =============================
// WaterHeightImageProcessor.cpp
// =============================

#include "../../common/common.h"

#include "../../common/dds.h"
#include "../../common/fileutil.h"
#include "../../common/imageutil.h"
#include "../../common/mathutil.h"
#include "../../common/progressdisplay.h"
#include "../../common/stringutil.h"

#include "watermap.h"
#include "worldutil.h"

#define WATERMAP_TEST (0)

#if WATERMAP_TEST
#include "watermap_test.inl"
#endif // WATERMAP_TEST

int main(int argc, const char* argv[])
{
	PrintDirectoryPaths();

	if (argc <= 1) // no commandline args
	{
#if WATERMAP_TEST
		if (1)
		{
			Test::Run();
			exit(0);
		}
#endif // WATERMAP_TEST
	}

	bool bRenderImages = true;
	bool bPauseWhenDone = IsDebuggerPresent() || argc <= 1;

	char path[1024] = RS_ASSETS "/reports/heightmap/images_2_water";

	if (argc > 1)
	{
		strcpy(path, argv[1]);

		for (char* s = path; *s; s++)
		{
			if (*s == '\\')
			{
				*s = '/';
			}
		}
	}

	for (int i = 2; i < argc; i++) // process other commandline args
	{
		if (strcmp(argv[i], "-noimages") == 0)
		{
			bRenderImages = false;
		}
		else if (strcmp(argv[i], "-pause") == 0)
		{
			bPauseWhenDone = true;
		}
	}

	int resolution = 0;

	// extract resolution from path
	{
		const char* s = strrchr(path, '/');

		if (s)
		{
			s = strchr(s, '_');

			if (s)
			{
				resolution = atoi(s + 1);
			}
		}
	}

	if (resolution < 1 || resolution > 2)
	{
		fprintf(stdout, "resolution = %d not supported, must be [1..2]\n\n", resolution);
		exit(0);
	}

	TileMap tm;

	if (tm.Load(path))
	{
		if (1)
		{
			AssembleImageFromTilesU8   (tm, resolution, path, "_wld");
			AssembleImageFromTilesU8   (tm, resolution, path, "_water", false, true);
			AssembleImageFromTilesU8   (tm, resolution, path, "_watercoll");
			AssembleImageFromTilesU8   (tm, resolution, path, "_wateroccl");
			AssembleImageFromTilesU8   (tm, resolution, path, "_waterdraw");
			AssembleImageFromTilesU8   (tm, resolution, path, "_waterocean", false, true);
			AssembleImageFromTilesU8   (tm, resolution, path, "_waternoreflect");
			AssembleImageFromTilesFloat(tm, resolution, path, "_waterheight", false, false, -1000.0f);
			AssembleImageFromTilesU8   (tm, resolution, path, "_waterportal");
			AssembleImageFromTilesU8   (tm, resolution, path, "_mirrordraw");
			AssembleImageFromTilesU8   (tm, resolution, path, "_mirrorportal");
		//	AssembleImageFromTilesU16  (tm, resolution, path, "_max", false, false, 0, true);
		//	AssembleImageFromTilesU16  (tm, resolution, path, "_maxneg", false, false, 0, true);
		}

		WorldMask wm(path);

		if (wm.m_worldMask)
		{
			float* waterHeight = LoadImage_float(varString("%s/assembled_waterheight.dds", path).c_str(), wm.m_w, wm.m_h);

			if (waterHeight)
			{
				fprintf(stdout, "building waterheight for %dx%d image\n", wm.m_w, wm.m_h);

				const bool bUseEDTOnly = false; // TODO -- hook up to commandline

				Water::WaterMap map;

				Water::LoadWaterMap(map, path, wm, tm, waterHeight, resolution, bUseEDTOnly, 50.0f);
				Water::LoadWaterMapRivers(map);
				Water::LoadWaterMapLakes(map);
				Water::LoadWaterMapPools(map);

				Water::BuildWaterMapRivers(map);
				Water::BuildWaterMapLakes(map);
				Water::BuildWaterMapPools(map);
				Water::BuildWaterMapTiles(map);

			//	const char* dat_path = "waterheight.dat";
				const char* dat_path = RS_LEVELS"/waterheight.dat";

				WaterFileWriter::Save(map, dat_path, true);

				if (bRenderImages)
				{
					Water::SaveWaterMapImages(map, path);
				}

				delete[] waterHeight;
			}
		}
	}

	if (bPauseWhenDone)
	{
		system("pause");
	}

	return 0;
}
