// ===========
// worldutil.h
// ===========

#ifndef _WORLDUTIL_H_
#define _WORLDUTIL_H_

#include "../../common/common.h"

#define TILE_MAP_EMPTY '.'
#define TILE_MAP_HOLES 'o'
#define TILE_MAP_OK    '_'

class TileMap
{
public:
	static int ReadInt(const char* name, FILE* fp);

	bool Load(const char* path);

	char* m_tileMap;
	int   m_tileMapX;
	int   m_tileMapY;
	int   m_tileMapW;
	int   m_tileMapH;
	int   m_downsamp;
};

class WorldMask
{
public:
	WorldMask(const char* path = NULL);
	~WorldMask();

	int  m_w;
	int  m_h;
	int  m_spanHeight;
	u8*  m_worldMask;
	int* m_worldMinX;
	int* m_worldMinY;
	int* m_worldMaxX;
	int* m_worldMaxY;
};

void AssembleImageFromTilesU8(const TileMap& tm, int resolution, const char* path, const char* suffix, bool bInvert = false, bool bClearToWhite = false, const u8& clearValue = 0, bool bSaveNativeFormat = false);
void AssembleImageFromTilesU16(const TileMap& tm, int resolution, const char* path, const char* suffix, bool bInvert = false, bool bClearToWhite = false, const u16& clearValue = 0, bool bSaveNativeFormat = false);
void AssembleImageFromTilesFloat(const TileMap& tm, int resolution, const char* path, const char* suffix, bool bInvert = false, bool bClearToWhite = false, const float& clearValue = 0.0f);
void AssembleImageFromTilesPixel32(const TileMap& tm, int resolution, const char* path, const char* suffix, bool bInvert = false, bool bClearToWhite = false, const u32& clearValue = 0);

#endif // _WORLDUTIL_H_
