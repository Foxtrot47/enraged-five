// ===============
// tifftagparser.h
// ===============

#ifndef _TIFFTAGPARSER_H_
#define _TIFFTAGPARSER_H_

#include "../common.h"

#define TIFF_SUPPORT_MULTIPLE_DIRECTORIES (0)

class TIFFTag
{
public:
	u16 m_tagId;
	u16 m_dataType;
	u32 m_dataCount;
	u32 m_dataOffset;
};

class TIFF
{
public:
	static TIFF* Load(const char* path);
	void Unload();
	bool IsBigEndian() const;
	int GetTagCount() const;
	const TIFFTag& GetTag(int index) const;
	const TIFFTag* FindTag(int tagId) const;
	int FindTagValueAsInt(int tagId) const;
	float FindTagValueAsFloat(int tagId) const;
	static std::string GetTagName(int tagId);
	void* GetTagData(const TIFFTag& tag) const;
	int GetTagDataElementAsInt(const TIFFTag& tag, const void* data, int index) const;
	float GetTagDataElementAsFloat(const TIFFTag& tag, const void* data, int index) const;
	std::string GetTagDataDesc(const TIFFTag& tag, int maxcount) const;

private:
	TIFF() {} // private c'tors .. use TIFF::Load
	TIFF(FILE* file, int filesize, bool bigendian) : m_file(file), m_filesize(filesize), m_bigendian(bigendian) {}

	FILE* m_file;
	int m_filesize;
	bool m_bigendian;
	std::vector<TIFFTag> m_tags;
};

#endif // _TIFFTAGPARSER_H_
