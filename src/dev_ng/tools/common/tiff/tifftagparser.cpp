// =================
// tifftagparser.cpp
// =================

#include "tifftagparser.h"
#include "tifftags.h"

#include "../fileutil.h"

// note: TIFFTAG_PHOTOSHOP is a series of resources which describe the thumbnail
// see http://www.awaresystems.be/imaging/tiff/tifftags/docs/photoshopthumbnail.html

// TIFFTAG 37724 describes the photoshop layers.
// see http://partners.adobe.com/public/developer/en/tiff/TIFFphotoshop.pdf
// see http://forums.adobe.com/servlet/JiveServlet/download/3265919-53765/Photoshop%20File%20Formats.pdf

class _TIFFHeader
{
public:
	u16 m_magic; // 'II' or 'MM'
	u16 m_version; // 42
	u32 m_diroff;
};

enum _TIFFTagDataType
{
	_TIFF_DATATYPE_NONE      = 0,
	_TIFF_DATATYPE_BYTE      = 1, // u8
	_TIFF_DATATYPE_ASCII     = 2, // char string (variable)
	_TIFF_DATATYPE_SHORT     = 3, // u16
	_TIFF_DATATYPE_LONG      = 4, // u32
	_TIFF_DATATYPE_RATIONAL  = 5, // u32 + u32
	_TIFF_DATATYPE_SBYTE     = 6, // s8
	_TIFF_DATATYPE_UNDEFINE  = 7, // byte data (variable)
	_TIFF_DATATYPE_SSHORT    = 8, // s16
	_TIFF_DATATYPE_SLONG     = 9, // s32
	_TIFF_DATATYPE_SRATIONAL = 10, // s32 + s32
	_TIFF_DATATYPE_FLOAT     = 11, // float
	_TIFF_DATATYPE_DOUBLE    = 12, // double
	_TIFF_DATATYPE_IFD       = 13, // u32 (offset)
	_TIFF_DATATYPE_COUNT     = 14,
};

static const int g_TIFFDataTypeSizes[] =
{
	0,
	1,
	1,
	2,
	4,
	8,
	1,
	1,
	2,
	4,
	8,
	4,
	8,
	4,
};
CompileTimeAssert(NELEM(g_TIFFDataTypeSizes) == _TIFF_DATATYPE_COUNT);

static const char* g_TIFFDataTypeStrings[] =
{
	"NONE",
	"BYTE",
	"ASCII",
	"SHORT",
	"LONG",
	"RATIONAL",
	"SBYTE",
	"UNDEFINE",
	"SSHORT",
	"SLONG",
	"SRATIONAL",
	"FLOAT", 
	"DOUBLE",
	"IFD",
};
CompileTimeAssert(NELEM(g_TIFFDataTypeStrings) == _TIFF_DATATYPE_COUNT);

class _TIFFTagEnumDesc
{
public:
	_TIFFTagEnumDesc(const char* name, int value) : m_name(name), m_value(value) {}
	std::string m_name;
	int m_value;
};

class _TIFFTagDesc
{
public:
	_TIFFTagDesc(const char* name, int tagId) : m_name(name), m_tagId(tagId) {}
	std::string m_name;
	int m_tagId;
	std::vector<_TIFFTagEnumDesc*> m_enums;
};

static std::vector<_TIFFTagDesc*> g_TIFFTagDescs;

static const _TIFFTagDesc* _TIFFFindTagDesc(int tagId)
{
	if (g_TIFFTagDescs.size() == 0) {
		FILE* fp = fopen(RS_CODEBRANCH "/tools/common/tiff/tifftags.h", "r");
		std::string desc = "";
		if (fp) {
			char line[1024] = "";
			while (fgets(line, sizeof(line), fp)) {
				const char* sep = " \t\r\n";
				const char* def = strtok(line, sep);
				if (strcmp(def, "#define") == 0) {
					const char* name = strtok(NULL, sep);
					if (name) {
						const char* tagstart = "TIFFTAG_";
						const bool istag = (memcmp(name, tagstart, strlen(tagstart)) == 0);
						const char* valstr = strtok(NULL, sep);
						if (valstr) {
							const int val = atoi(valstr);
							if (istag)
								g_TIFFTagDescs.push_back(new _TIFFTagDesc(name, val));
							else
								g_TIFFTagDescs.back()->m_enums.push_back(new _TIFFTagEnumDesc(name, val));
						}
					}
				}
			}

			fclose(fp);
		}
	}

	for (int i = 0; i < (int)g_TIFFTagDescs.size(); i++) {
		if (g_TIFFTagDescs[i]->m_tagId == tagId) {
			return g_TIFFTagDescs[i];
		}
	}

	return NULL;
}

static char g_TIFFErrorString[512] = "";

__COMMENT(static) TIFF* TIFF::Load(const char* path)
{
	g_TIFFErrorString[0] = '\0';

	FILE* file = fopen(path, "rb");

	if (file)
	{
		fseek(file, 0, SEEK_END);
		const int filesize = (int)ftell(file);
		fseek(file, 0, SEEK_SET);

		_TIFFHeader header;
		fread(&header, sizeof(header), 1, file);
		bool bigendian = false;

		if (((const char*)&header.m_magic)[0] == 'I' && ((const char*)&header.m_magic)[1] == 'I')
		{
			// little endian (intel)
		}
		else if (((const char*)&header.m_magic)[0] == 'M' && ((const char*)&header.m_magic)[1] == 'M')
		{
			bigendian = true;
		}
		else
		{
			sprintf(g_TIFFErrorString, "%s has bad format! header = '%c%c', version = %d\n", path, ((const char*)&header.m_magic)[0], ((const char*)&header.m_magic)[1], header.m_version);
			fclose(file);
			return NULL;
		}

		if (bigendian)
		{
			ByteSwap(header.m_version);
			ByteSwap(header.m_diroff);
		}

		if (header.m_version != 42)
		{
			sprintf(g_TIFFErrorString, "%s has bad format! header = '%c%c', version = %d\n", path, ((const char*)&header.m_magic)[0], ((const char*)&header.m_magic)[1], header.m_version);
			fclose(file);
			return NULL;
		}

		TIFF* tiff = new TIFF(file, filesize, bigendian);

		fseek(file, header.m_diroff, SEEK_SET);

		while (true) 
		{
			u16 numdirentries = 0;
			fread(&numdirentries, sizeof(numdirentries), 1, file);

			if (bigendian)
			{
				ByteSwap(numdirentries);
			}

			for (int i = 0; i < (int)numdirentries; i++)
			{
				TIFFTag tag;
				fread(&tag, sizeof(tag), 1, file);

				if (bigendian)
				{
					ByteSwap(tag.m_tagId);
					ByteSwap(tag.m_dataType);
					ByteSwap(tag.m_dataCount);
					ByteSwap(tag.m_dataOffset);
				}

				tiff->m_tags.push_back(tag);
			}

#if TIFF_SUPPORT_MULTIPLE_DIRECTORIES
			u32 nextdiroffset = 0;
			fread(&nextdiroffset, sizeof(nextdiroffset), 1, file);

			if (bigendian)
			{
				ByteSwap(nextdiroffset);
			}

			if (nextdiroffset != 0)
			{
				fseek(file, nextdiroffset, SEEK_SET);
			}
			else
#endif // TIFF_SUPPORT_MULTIPLE_DIRECTORIES
			{
				break;
			}
		}

		return tiff;
	}

	return NULL;
}

void TIFF::Unload()
{
	if (this)
	{
		if (m_file)
		{
			fclose(m_file);
		}

		delete this;
	}
}

bool TIFF::IsBigEndian() const
{
	return m_bigendian;
}

int TIFF::GetTagCount() const
{
	return (int)m_tags.size();
}

const TIFFTag& TIFF::GetTag(int index) const
{
	assert(index >= 0 && index < (int)m_tags.size());
	return m_tags[index];
}

const TIFFTag* TIFF::FindTag(int tagId) const
{
	for (int i = 0; i < (int)m_tags.size(); i++)
	{
		if (m_tags[i].m_tagId == tagId)
		{
			return &m_tags[i];
		}
	}

	return NULL;
}

int TIFF::FindTagValueAsInt(int tagId) const
{
	const TIFFTag* tag = FindTag(tagId);

	if (tag)
	{
		void* data = GetTagData(*tag);
		const int value = GetTagDataElementAsInt(*tag, data, 0);
		delete[] data;
		return value;
	}

	return -1;
}

float TIFF::FindTagValueAsFloat(int tagId) const
{
	const TIFFTag* tag = FindTag(tagId);

	if (tag)
	{
		void* data = GetTagData(*tag);
		const float value = GetTagDataElementAsFloat(*tag, data, 0);
		delete[] data;
		return value;
	}

	return -1;
}

__COMMENT(static) std::string TIFF::GetTagName(int tagId)
{
	const _TIFFTagDesc* tagDesc = _TIFFFindTagDesc(tagId);

	if (tagDesc)
	{
		return tagDesc->m_name;
	}
	else
	{
		char temp[512] = "";
		sprintf(temp, "TIFFTAG_UNKNOWN(%d)", tagId);
		return temp;
	}
}

void* TIFF::GetTagData(const TIFFTag& tag) const
{
	if (tag.m_dataType < _TIFF_DATATYPE_COUNT)
	{
		const int datasize = tag.m_dataCount*g_TIFFDataTypeSizes[tag.m_dataType];
		u8* data = new u8[Max<int>(4, datasize)];

		if (datasize <= 4)
		{
			const u32 temp = ByteSwapBE(tag.m_dataOffset, m_bigendian);
			memcpy(data, &temp, sizeof(temp));
		}
		else
		{
			const int filepos = (int)ftell(m_file);
			fseek(m_file, tag.m_dataOffset, SEEK_SET);
			fread(data, datasize, 1, m_file);
			fseek(m_file, filepos, SEEK_SET);
		}

		return data;
	}
	else
	{
		return NULL;
	}
}

template <typename T> std::string _TIFFGetTagDataAsString(u8*& data, const _TIFFTagDesc* tagdesc, bool bigendian)
{
	const T value = *(const T*)data;
	data += sizeof(T);

	if (tagdesc)
	{
		for (int i = 0; i < (int)tagdesc->m_enums.size(); i++)
		{
			if (tagdesc->m_enums[i]->m_value == value)
			{
				return tagdesc->m_enums[i]->m_name;
			}
		}
	}

	char temp[512] = "";
	sprintf(temp, "%d", value);
	return temp;
}

int TIFF::GetTagDataElementAsInt(const TIFFTag& tag, const void* data, int index) const
{
	if (index >= 0 && index < (int)tag.m_dataCount)
	{
		switch (tag.m_dataType)
		{
		case _TIFF_DATATYPE_BYTE   : return (int)ByteSwapBE(((const u8 *)data)[index], m_bigendian);
		case _TIFF_DATATYPE_SHORT  : return (int)ByteSwapBE(((const u16*)data)[index], m_bigendian);
		case _TIFF_DATATYPE_LONG   : return (int)ByteSwapBE(((const u32*)data)[index], m_bigendian);
		case _TIFF_DATATYPE_SBYTE  : return (int)ByteSwapBE(((const s8 *)data)[index], m_bigendian);
		case _TIFF_DATATYPE_SSHORT : return (int)ByteSwapBE(((const s16*)data)[index], m_bigendian);
		case _TIFF_DATATYPE_SLONG  : return (int)ByteSwapBE(((const s32*)data)[index], m_bigendian);
		}

		return (int)GetTagDataElementAsFloat(tag, data, index);
	}

	return -1;
}

float TIFF::GetTagDataElementAsFloat(const TIFFTag& tag, const void* data, int index) const
{
	if (index >= 0 && index < (int)tag.m_dataCount)
	{
		switch (tag.m_dataType)
		{
		case _TIFF_DATATYPE_FLOAT  : return (float)ByteSwapBE(((const float *)data)[index], m_bigendian);
		case _TIFF_DATATYPE_DOUBLE : return (float)ByteSwapBE(((const double*)data)[index], m_bigendian);
		}

		if (tag.m_dataType == _TIFF_DATATYPE_RATIONAL)
		{
			const u32 numer = ByteSwapBE(((const u32*)data)[0 + 2*index], m_bigendian);
			const u32 denom = ByteSwapBE(((const u32*)data)[1 + 2*index], m_bigendian);
			return (float)numer/(float)denom;
		}
		else if (tag.m_dataType == _TIFF_DATATYPE_SRATIONAL)
		{
			const s32 numer = ByteSwapBE(((const s32*)data)[0 + 2*index], m_bigendian);
			const s32 denom = ByteSwapBE(((const s32*)data)[1 + 2*index], m_bigendian);
			return (float)numer/(float)denom;
		}
		else
		{
			return (float)GetTagDataElementAsInt(tag, data, index);
		}
	}

	return -1;
}

std::string TIFF::GetTagDataDesc(const TIFFTag& tag, int maxcount) const
{
	std::string desc = "";
	char temp[512] = "";

	if (tag.m_dataType == _TIFF_DATATYPE_UNDEFINE || (int)tag.m_dataCount > maxcount)
	{
		if (tag.m_dataType < _TIFF_DATATYPE_COUNT)
		{
			const int datasize = tag.m_dataCount*g_TIFFDataTypeSizes[tag.m_dataType];
			sprintf(temp, "(%d bytes data [type=%s])", datasize, g_TIFFDataTypeStrings[tag.m_dataType]);
			desc = temp;
		}
		else
		{
			sprintf(temp, "(%d elements data [type=%d])", tag.m_dataCount, tag.m_dataType);
			desc = temp;
		}
	}
	else
	{
		u8* tagdata = (u8*)GetTagData(tag);

		if (tagdata)
		{
			const _TIFFTagDesc* tagdesc = _TIFFFindTagDesc(tag.m_tagId);
			u8* data = tagdata;

			for (int i = 0; i < (int)tag.m_dataCount; i++)
			{
				if (i > 0)
				{
					desc += ",";
				}

				if (tag.m_dataType == _TIFF_DATATYPE_ASCII || (tag.m_dataType == _TIFF_DATATYPE_BYTE && tag.m_tagId == TIFFTAG_XMLPACKET))
				{
					desc = (const char*)data;
					break;
				}
				else if (tag.m_dataType == _TIFF_DATATYPE_FLOAT)
				{
					const float value = ByteSwapBE(*(const float*)data, m_bigendian); data += sizeof(float);
					sprintf(temp, "%f", value);
					desc += temp;
				}
				else if (tag.m_dataType == _TIFF_DATATYPE_DOUBLE)
				{
					const double value = ByteSwapBE(*(const double*)data, m_bigendian); data += sizeof(double);
					sprintf(temp, "%f", value);
					desc += temp;
				}
				else if (tag.m_dataType == _TIFF_DATATYPE_RATIONAL)
				{
					const u32 numer = ByteSwapBE(*(const u32*)data, m_bigendian); data += sizeof(u32);
					const u32 denom = ByteSwapBE(*(const u32*)data, m_bigendian); data += sizeof(u32);
					const float value = (float)numer/(float)denom;
					sprintf(temp, "%f", value);
					desc += temp;
				}
				else if (tag.m_dataType == _TIFF_DATATYPE_SRATIONAL)
				{
					const s32 numer = ByteSwapBE(*(const s32*)data, m_bigendian); data += sizeof(s32);
					const s32 denom = ByteSwapBE(*(const s32*)data, m_bigendian); data += sizeof(s32);
					const float value = (float)numer/(float)denom;
					sprintf(temp, "%f", value);
					desc += temp;
				}
				else if (tag.m_dataType == _TIFF_DATATYPE_IFD)
				{
					const u32 value = ByteSwapBE(*(const u32*)data, m_bigendian); data += sizeof(u32);
					sprintf(temp, "0x%08x", value);
					desc += temp;
				}
				else
				{
					switch (tag.m_dataType)
					{
					case _TIFF_DATATYPE_BYTE   : desc += _TIFFGetTagDataAsString<u8 >(data, tagdesc, m_bigendian); break;
					case _TIFF_DATATYPE_SHORT  : desc += _TIFFGetTagDataAsString<u16>(data, tagdesc, m_bigendian); break;
					case _TIFF_DATATYPE_LONG   : desc += _TIFFGetTagDataAsString<u32>(data, tagdesc, m_bigendian); break;
					case _TIFF_DATATYPE_SBYTE  : desc += _TIFFGetTagDataAsString<s8 >(data, tagdesc, m_bigendian); break;
					case _TIFF_DATATYPE_SSHORT : desc += _TIFFGetTagDataAsString<s16>(data, tagdesc, m_bigendian); break;
					case _TIFF_DATATYPE_SLONG  : desc += _TIFFGetTagDataAsString<s32>(data, tagdesc, m_bigendian); break;
					default                    : desc += "???"; i = (int)tag.m_dataCount; break;
					}
				}
			}

			delete[] tagdata;
		}
	}

	return desc;
}
