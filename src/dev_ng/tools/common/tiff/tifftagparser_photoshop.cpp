// ===========================
// tifftagparser_photoshop.cpp
// ===========================

#include "../common.h"
#include "../fileutil.h"

#include "tifftagparser.h"
#include "tifftagparser_photoshop.h"
#include "tifftags.h"

#define KEY_CHARS(key) ((key)>>24)&0xff, ((key)>>16)&0xff, ((key)>>8)&0xff, (key)&0xff

static const char* TIFFPhotoshopLayersInfo_GetFlagsDesc(u8 flags)
{
	static char temp[256] = "";

	temp[0] = '\0';
	if (flags & BIT(0)) { strcat(temp, temp[0] ? "," : ""); strcat(temp, "transparency_protected"); }
	if (flags & BIT(1)) { strcat(temp, temp[0] ? "," : ""); strcat(temp, "visible"); }
	if (flags & BIT(2)) { strcat(temp, temp[0] ? "," : ""); strcat(temp, "obsolete"); }
	if (flags & BIT(3)) { strcat(temp, temp[0] ? "," : ""); strcat(temp, "bit4_is_valid"); }
	if (flags & BIT(4)) { strcat(temp, temp[0] ? "," : ""); strcat(temp, "pixeldata_irrelevant"); }
	if (flags & BIT(5)) { strcat(temp, temp[0] ? "," : ""); strcat(temp, "bit5"); }
	if (flags & BIT(6)) { strcat(temp, temp[0] ? "," : ""); strcat(temp, "bit6"); }
	if (flags & BIT(7)) { strcat(temp, temp[0] ? "," : ""); strcat(temp, "bit7"); }

	return temp;
}

static const char* TIFFPhotoshopLayersInfo_GetKeyDesc(u32 key)
{
	/*
	=======================================================================
	scanning through x:/rdr3/art/textures, here are the tags i encountered:

	base:
	------------------------------
	'Layr' count = 16816 (layer)
	'Lr16' count = 220 (layer 16)
	'MTrn' count = 43 (merged transparency)

	blend mode:
	------------------------------
	'norm' count = 180516 (normal)
	'diff' count = 461 (difference)
	'sat ' count = 74 (saturation)
	'sLit' count = 4698 (soft light)
	'mul ' count = 6601 (multiply)
	'hLit' count = 3433 (hard light)
	'lbrn' count = 742 (linear burn)
	'scrn' count = 2786 (screen)
	'dark' count = 3477 (darken)
	'over' count = 20190 (overlay)
	'lum ' count = 815 (luminosity)
	'lite' count = 1782 (lighten)
	'colr' count = 686 (color)
	'diss' count = 52 (dissolve)
	'pLit' count = 228 (pin light)
	'div ' count = 403 (color dodge)
	'dkCl' count = 152 (darken color)
	'hue ' count = 125 (hue)
	'vLit' count = 233 (vivid light)
	'lLit' count = 545 (linear light)
	'idiv' count = 484 (color burn)
	'lgCl' count = 118 (lighter color)
	'hMix' count = 34 (hard mix)
	'smud' count = 157 (exclusion)
	'lddg' count = 194 (linear dodge)
	'fdiv' count = 2 (divide)

	layer info:
	------------------------------
	'luni' count = 228988 (unicode layer name)
	'lnsr' count = 178315 (layer name source setting)
	'lyid' count = 228988 (layer id)
	'clbl' count = 203630 (blend clipping elements)
	'infx' count = 203630 (blend interior elements)
	'knko' count = 203630 (knockout setting)
	'lspf' count = 228988 (protected setting)
	'lclr' count = 228988 (sheet color setting)
	'shmd' count = 52539 (metadata setting)
	'fxrp' count = 228988 (reference point)
	'brit' count = 4665 (adjustment layer - brightness/contrast)
	'CgEd' count = 2458 (content generator extra data)
	'blnc' count = 1367 (adjustment layer - color balance)
	'lsct' count = 25358 (section divider setting)
	'hue2' count = 5285 (adjustment layer - hue/saturation)
	'curv' count = 699 (adjustment layer - curves)
	'levl' count = 1875 (adjustment layer - levels)
	'TySh' count = 684 (type tool)
	'PlLd' count = 15 (placed layer data (old 2))
	'SoLd' count = 15 (placed layer data)
	'lfx2' count = 6252 (object-based effects layer info)
	'lrFX' count = 6252 (effects layer)
	'iOpa' count = 13258 (unknown)
	'lyvr' count = 1141 (layer version)
	'lmgm' count = 136 (layer mask as global mask)
	'grdm' count = 47 (adjustment layer - gradient map)
	'brst' count = 1812 (channel blending restriction setting)
	'phfl' count = 27 (adjustment layer - photo filter)
	'SoCo' count = 152 (adjustment layer - solid color)
	'vmsk' count = 129 (vector mask setting)
	'vmgm' count = 8 (vector mask as global mask)
	'blwh' count = 4 (adjustment layer - black & white)
	'nvrt' count = 3 (adjustment layer - invert)
	'mixr' count = 4 (adjustment layer - channel mixer)
	'GdFl' count = 6 (adjustment layer - gradient)
	'tsly' count = 1 (transparency shapes layer)
	'vibA' count = 4 (adjustment layer - vibrance)
	'selc' count = 1 (adjustment layer - selective color)
	=======================================================================
	*/

	switch (key)
	{
	case 'Layr': return "layer";
	case 'Lr16': return "layer 16";
	case 'Lr32': return "layer 32";
	case 'MTrn': case 'Mtrn': return "merged transparency";
	case 'MT16': case 'Mt16': return "merged transparency 16";
	case 'MT32': case 'Mt32': return "merged transparency 32";

	// blend modes
	case 'norm': return "normal";
	case 'dark': return "darken";
	case 'lite': return "lighten";
	case 'hue ': return "hue";
	case 'sat ': return "saturation";
	case 'colr': return "color";
	case 'lum ': return "luminosity";
	case 'mul ': return "multiply";
	case 'scrn': return "screen";
	case 'diss': return "dissolve";
	case 'over': return "overlay";
	case 'hLit': return "hard light";
	case 'sLit': return "soft light";
	case 'diff': return "difference";
	case 'smud': return "exclusion";
	case 'div ': return "color dodge";
	case 'idiv': return "color burn";
	case 'lbrn': return "linear burn";
	case 'lddg': return "linear dodge";
	case 'vLit': return "vivid light";
	case 'lLit': return "linear light";
	case 'pLit': return "pin light";
	case 'hMix': return "hard mix";
	case 'pass': return "pass through";
	case 'dkCl': return "darken color";
	case 'lgCl': return "lighter color";
	case 'fsub': return "subtract";
	case 'fdiv': return "divide";

	// layer info
	case 'SoCo': return "adjustment layer - solid color";
	case 'GdFl': return "adjustment layer - gradient";
	case 'PtFl': return "adjustment layer - pattern";
	case 'brit': return "adjustment layer - brightness/contrast";
	case 'levl': return "adjustment layer - levels";
	case 'curv': return "adjustment layer - curves";
	case 'expA': return "adjustment layer - exposure";
	case 'vibA': return "adjustment layer - vibrance";
//	case 'hue ': return "adjustment layer - hue/saturation (old)";
	case 'hue2': return "adjustment layer - hue/saturation";
	case 'blnc': return "adjustment layer - color balance";
	case 'blwh': return "adjustment layer - black & white";
	case 'phfl': return "adjustment layer - photo filter";
	case 'mixr': return "adjustment layer - channel mixer";
	case 'nvrt': return "adjustment layer - invert";
	case 'post': return "adjustment layer - posterize";
	case 'thrs': return "adjustment layer - threshold";
	case 'grdm': return "adjustment layer - gradient map";
	case 'selc': return "adjustment layer - selective color";
	case 'lrFX': return "effects layer";
	case 'tySh': return "type tool (old)";
	case 'TySh': return "type tool";
	case 'luni': return "unicode layer name";
	case 'lyid': return "layer id";
	case 'lfx2': return "object-based effects layer info";
	case 'Patt': return "pattern";
	case 'Pat2': return "pattern (type 2)";
	case 'Pat3': return "pattern (type 3)";
	case 'Anno': return "annotation";
	case 'clbl': return "blend clipping elements";
	case 'infx': return "blend interior elements";
	case 'knko': return "knockout setting";
	case 'lspf': return "protected setting";
	case 'lclr': return "sheet color setting";
	case 'fxrp': return "reference point";
	case 'gdrm': return "gradient setting";
	case 'lsct': return "section divider setting";
	case 'brst': return "channel blending restriction setting";
	case 'vmsk': return "vector mask setting";
	case 'ffxi': return "foreign effect id";
	case 'lnsr': return "layer name source setting";
	case 'shpa': return "pattern data";
	case 'shmd': return "metadata setting";
	case 'lyvr': return "layer version";
	case 'tsly': return "transparency shapes layer";
	case 'lmgm': return "layer mask as global mask";
	case 'vmgm': return "vector mask as global mask";
	case 'plLd': return "placed layer data (old)";
	case 'PlLd': return "placed layer data (old)";
	case 'SoLd': return "placed layer data";
	case 'lnkD': return "linked layer data";
	case 'lnk2': return "linked layer data (type 2)";
	case 'lnk3': return "linked layer data (type 3)";
	case 'CgEd': return "content generator extra data";
	case 'Txt2': return "text engine data";
	case 'FMsk': return "filter mask";
	case 'LMsk': return "user mask";
	case 'FXid': return "filter effect (X)";
	case 'FEid': return "filter effect (E)";

	case 'iOpa': return "unknown"; // i'm seeing a lot of this .. what is it?

	// effects layer info
	case 'cmnS': return "effects layer info - common state";
	case 'dsdw': return "effects layer info - drop shadow";
	case 'isdw': return "effects layer info - inner shadow";
	case 'oglw': return "effects layer info - outer glow";
	case 'iglw': return "effects layer info - inner glow";
	case 'bevl': return "effects layer info - bevel";
	case 'sofi': return "effects layer info - solid fill";

	case 'Alph':
	case 'PxSD': return "unknown"; // these are referenced in Photoshop File Formats.pdf but i don't know what they are exactly
	}

	return "unknown";
}

static bool TIFFPhotoshopLayersInfo_ParseTag(const char* name, bool readsize, u32& out_key, const u8*& data, bool bigendian, bool verbose)
{
	const u32 sig = ByteSwapBE(*(const u32*)data, bigendian); data += sizeof(u32);
	const u32 key = ByteSwapBE(*(const u32*)data, bigendian); data += sizeof(u32);

	out_key = key;

	if (sig != '8BIM')
	{
		return false;
	}
	else if (readsize)
	{
		const u32 size = ByteSwapBE(*(const u32*)data, bigendian); data += sizeof(u32) + size;
		if (verbose) { printf("%s'%c%c%c%c' - %s (%d bytes)\n", name, KEY_CHARS(key), TIFFPhotoshopLayersInfo_GetKeyDesc(key), size); }
	}
	else
	{
		if (verbose) { printf("%s'%c%c%c%c' - %s\n", name, KEY_CHARS(key), TIFFPhotoshopLayersInfo_GetKeyDesc(key)); }
	}

	return true;
}

#define _PARSETAG(name,showsize) { if (!TIFFPhotoshopLayersInfo_ParseTag(name, showsize, key, data, tiff->IsBigEndian(), verbose)) { assert(0); success = false; break; } }
#define _PARSEINT(type) ByteSwapBE(*(const type*)data, tiff->IsBigEndian()); data += sizeof(type)
#define _ASSERTVERIFY(code) { if (!(code)) { assert(0); success = false; break; } }

TIFFPhotoshopLayersInfo::TIFFPhotoshopLayersInfo(const TIFF* tiff, bool verbose)
{
	const TIFFTag* tt = tiff->FindTag(TIFFTAG_PHOTOSHOPLAYERS);
	if (tt)
	{
		u8* databuf = (u8*)tiff->GetTagData(*tt);
		if (databuf)
		{
			const u8* data = databuf;
			bool success = true;
			do
			{
				char temp[512] = "";
				u32 key = 0;
				const char* header = (char*)data; data += strlen(header) + 1; if (verbose) { printf("header = \"%s\"\n", header); }

				_ASSERTVERIFY(strcmp(header, "Adobe Photoshop Document Data Block") == 0);

				_PARSETAG("tag", false);

				if (key == 'MTrn')
				{
					const u32 tagsize = _PARSEINT(u32); if (verbose) { printf("skipping %d bytes ...\n", tagsize); } data += tagsize;

					_PARSETAG("tag", false);
				}

				const u32 layertagsize = _PARSEINT(u32); if (verbose) { printf("layertagsize = %d\n", layertagsize); }
				const int layercount = (int)_PARSEINT(u16); if (verbose) { printf("layercount = %d\n", layercount); }

				_ASSERTVERIFY(layercount <= 1024);

				for (int layerindex = 0; layerindex < layercount; layerindex++)
				{
					if (verbose) { printf("layer[%d] ==========================================\n", layerindex); }

					const s32 layerboundstop = _PARSEINT(s32);
					const s32 layerboundsleft = _PARSEINT(s32);
					const s32 layerboundsbottom = _PARSEINT(s32);
					const s32 layerboundsright = _PARSEINT(s32); if (verbose) { printf("  layerbounds = %d,%d,%d,%d\n", layerboundsleft, layerboundstop, layerboundsright, layerboundsbottom); }
					const int channelcount = (int)_PARSEINT(u16); if (verbose) { printf("  channelcount = %d\n", channelcount); }

					_ASSERTVERIFY(channelcount < 16);

					char channelsstr[512] = "";
					for (int channelindex = 0; channelindex < channelcount; channelindex++)
					{
						const int channelid = (int)_PARSEINT(s16);
						const u32 channelsize = _PARSEINT(u32);

						if (channelindex > 0) { strcat(channelsstr, ","); }

						switch (channelid)
						{
						case 0: strcat(channelsstr, "red"); break;
						case 1: strcat(channelsstr, "green"); break;
						case 2: strcat(channelsstr, "blue"); break;
						case -1: strcat(channelsstr, "transparency"); break;
						case -2: strcat(channelsstr, "layermask"); break;
						case -3: strcat(channelsstr, "reallayermask"); break;
						default: { sprintf(temp, "channelid_%d", channelid); strcat(channelsstr, temp); assert(0); }
						}

						sprintf(temp, ",%d", channelsize);
						strcat(channelsstr, temp);
					}

					if (verbose) { printf("  channels = %s\n", channelsstr); }

					_PARSETAG("  blendmode = ", false);

					const u8 opacity = _PARSEINT(u8); if (verbose) { printf("  opacity = %d\n", opacity); }
					const u8 clipping = _PARSEINT(u8); if (verbose) { printf("  clipping = %d\n", clipping); }
					const u8 flags = _PARSEINT(u8); if (verbose) { printf("  flags = 0x%02x (%s)\n", flags, TIFFPhotoshopLayersInfo_GetFlagsDesc(flags)); }
					const u8 padding = _PARSEINT(u8);
					const u32 extradatasize = _PARSEINT(u32); if (verbose) { printf("  extradatasize = %d\n", extradatasize); }
					const u32 layermaskdatasize = _PARSEINT(u32); if (verbose) { printf("  layermaskdatasize = %d\n", layermaskdatasize); }

					_ASSERTVERIFY(clipping == 0 || clipping == 1);
					//_ASSERTVERIFY((flags >> 5) == 0); // -- i'm seeing some unknown flags e.g. 0x40
					_ASSERTVERIFY(padding == 0);

					if (1 && layermaskdatasize == 20) // parse layer mask data (optional)
					{
						const u8* data2 = data + layermaskdatasize;

						const s32 layermaskboundstop = _PARSEINT(s32);
						const s32 layermaskboundsleft = _PARSEINT(s32);
						const s32 layermaskboundsbottom = _PARSEINT(s32);
						const s32 layermaskboundsright = _PARSEINT(s32); if (verbose) { printf("  layermaskbounds = %d,%d,%d,%d\n", layermaskboundsleft, layermaskboundstop, layermaskboundsright, layermaskboundsbottom); }
						const u8 layermaskdefaultcolour = _PARSEINT(u8); if (verbose) { printf("  layermaskdefaultcolour = %d\n", layermaskdefaultcolour); }
						const u8 layermaskflags = _PARSEINT(u8); if (verbose) { printf("  layermaskflags = 0x%02x\n", layermaskflags); }
						const u16 layermaskpadding = _PARSEINT(u16);

						_ASSERTVERIFY(layermaskdefaultcolour == 0 || layermaskdefaultcolour == 255);
						//_ASSERTVERIFY((layermaskflags >> 3) == 0); // -- i'm seeing some unknown flags e.g. 0x08
						_ASSERTVERIFY(layermaskpadding == 0);
						_ASSERTVERIFY(data == data2);
					}
					else
					{
						data += layermaskdatasize;
					}

					const u32 layerblendingrangessize = _PARSEINT(u32); if (verbose) { printf("  layerblendingrangessize = %d\n", layerblendingrangessize); }

					if (1 && layerblendingrangessize > 0) // parse layer blending ranges (optional)
					{
						//_ASSERTVERIFY(layerblendingrangessize == 4*(1 + channelcount)); // -- hmm this is not what i'm expecting
						_ASSERTVERIFY(layerblendingrangessize%4 == 0);

						for (int i = 0; i < (int)layerblendingrangessize; i += 4)
						{
							const u8 black0 = _PARSEINT(u8);
							const u8 black1 = _PARSEINT(u8);
							const u8 white0 = _PARSEINT(u8);
							const u8 white1 = _PARSEINT(u8);

							if (verbose && (black0 != 0 || black1 != 0 || white0 != 255 || white1 != 255))
							{
								printf("  blendingrange[%d] = src:[%d..%d], dst:[%d..%d]\n", i/4, black0, white0, black1, white1);
							}
						}
					}
					else
					{
						data += layerblendingrangessize;
					}

					const u8 layernamelen = _PARSEINT(u8);
					char layername[256] = ""; memcpy(layername, data, layernamelen); layername[layernamelen] = '\0'; data += layernamelen;
					m_layernames.push_back(std::string(layername)); if (verbose) { printf("  layername = \"%s\"\n", layername); }
					while (*data == 0) { data++; } // for some reason there's some padding here .. oh i think the string length is padded to 4 bytes?

					for (int tagindex = 0; ByteSwapBE(*(const u32*)data, tiff->IsBigEndian()) == '8BIM'; tagindex++)
					{
						const u8* data2 = data;
						_PARSETAG("    ", true);

						if (key == 'lrFX')
						{
							std::swap(data, data2);
							data += sizeof(u32); // '8BIM'
							data += sizeof(u32); // 'lrFX'
							data += sizeof(u32); // size

							const int version = (int)_PARSEINT(u16); assert(version == 0);
							const int fxcount = (int)_PARSEINT(u16); assert(fxcount > 0 && fxcount <= 16);

							for (int fxi = 0; fxi < fxcount; fxi++)
							{
								_PARSETAG("      ", true);
							}

							std::swap(data, data2);
						}
					}
				}

				if (verbose) { printf("\n"); }
			}
			while (false);

			delete[] databuf;
		}
	}
}

#undef _PARSETAG
#undef _PARSEINT
#undef _ASSERTVERIFY
