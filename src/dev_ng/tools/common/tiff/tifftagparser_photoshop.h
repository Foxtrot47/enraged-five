// =========================
// tifftagparser_photoshop.h
// =========================

#ifndef _TIFFTAGPARSER_PHOTOSHOP_H_
#define _TIFFTAGPARSER_PHOTOSHOP_H_

#include "../common.h"

// TIFFTAG 37724 (TIFFTAG_PHOTOSHOPLAYERS) describes the photoshop layers.
// see http://partners.adobe.com/public/developer/en/tiff/TIFFphotoshop.pdf
// see http://forums.adobe.com/servlet/JiveServlet/download/3265919-53765/Photoshop%20File%20Formats.pdf

// TIFFTAG_PHOTOSHOP is a series of resources which describe the thumbnail
// see http://www.awaresystems.be/imaging/tiff/tifftags/docs/photoshopthumbnail.html

class TIFF;

class TIFFPhotoshopLayersInfo
{
public:
	TIFFPhotoshopLayersInfo(const TIFF* tiff, bool verbose = false);

	std::vector<std::string> m_layernames;
};

#endif // _TIFFTAGPARSER_PHOTOSHOP_H_
