// ==========
// fileutil.h
// ==========

#ifndef _COMMON_FILEUTIL_H_
#define _COMMON_FILEUTIL_H_

#include "common.h"

void PrintDirectoryPaths();

void BuildSearchList(std::vector<std::string>& files, const char* path, const char* ext1 = NULL, const char* ext2 = NULL);

bool FileExists(const char* path);
bool FileExistsAndIsNotZeroBytes(const char* path);

bool ReadFileLine(char* line, int lineMax, FILE* fp);
bool ReadNextFileLine(char* line, int lineMax, FILE* fp);

int rage_fgetline(char* dest, int maxSize, FILE* S);

void ByteSwapData(void* data, int dataSize, int swapSize);

template <typename T> inline static void ByteSwap(T& data)
{
	ByteSwapData(&data, sizeof(data), sizeof(data));
}

template <typename T> inline static T ByteSwapBE(T data, bool bigendian)
{
	T temp = data;

	if (bigendian)
	{
		ByteSwap<T>(temp);
	}

	return temp;
}

#endif // _COMMON_FILEUTIL_H_
