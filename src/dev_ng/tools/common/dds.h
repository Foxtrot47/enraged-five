//
// grcore/dds.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef GRCORE_DDS_H
#define GRCORE_DDS_H

#include "common.h"

#define MAKE_MAGIC_NUMBER(a,b,c,d) ((a) | ((b) << 8) | ((c) << 16) | ((d) << 24))
#define EXPAND_MAGIC_NUMBER(x) ((x) & 255), (((x) >> 8) & 255), (((x) >> 16) & 255), (((x) >> 24) & 255)

// http://msdn.microsoft.com/library/default.asp?url=/library/en-us/directx9_c/directx/graphics/reference/DDSFileReference/ddsfileformat.asp

enum {
	DDPF_ALPHAPIXELS = 0x00000001,
	DDPF_ALPHA       = 0x00000002,
	DDPF_FOURCC      = 0x00000004,
	DDPF_RGB         = 0x00000040,
	DDPF_LUMINANCE   = 0x00020000,
};

struct DDPIXELFORMAT {
	u32 dwSize;        // Size of structure. This member must be set to 32.
	u32 dwFlags;       // Flags to indicate valid fields. Uncompressed formats will usually use DDPF_RGB to indicate an RGB format, while compressed formats will use DDPF_FOURCC with a four-character code.
	u32 dwFourCC;      // This is the four-character code for compressed formats. dwFlags should include DDPF_FOURCC in this case. For DXTn compression, this is set to "DXT1", "DXT2", "DXT3", "DXT4", or "DXT5".
	u32 dwRGBBitCount; // For RGB formats, this is the total number of bits in the format. dwFlags should include DDPF_RGB in this case. This value is usually 16, 24, or 32. For A8R8G8B8, this value would be 32.
	u32 dwRBitMask;
	u32 dwGBitMask;
	u32 dwBBitMask;    // For RGB formats, these three fields contain the masks for the red, green, and blue channels. For A8R8G8B8, these values would be 0x00ff0000, 0x0000ff00, and 0x000000ff respectively.
	u32 dwABitMask;    // For RGB formats, this contains the mask for the alpha channel, if any. dwFlags should include DDPF_ALPHAPIXELS in this case. For A8R8G8B8, this value would be 0xff000000.

	DDPIXELFORMAT() {}
	DDPIXELFORMAT(int) { memset(this, 0, sizeof(this)); }
};

#define DDPIXELFORMAT9 DDPIXELFORMAT

enum {
	DDSCAPS_ALPHA              = 0x00000002,
	DDSCAPS_COMPLEX            = 0x00000008,
	DDSCAPS_TEXTURE            = 0x00001000,
	DDSCAPS_MIPMAP             = 0x00400000,
};

enum {
	DDSCAPS2_CUBEMAP           = 0x00000200,
	DDSCAPS2_CUBEMAP_POSITIVEX = 0x00000400,
	DDSCAPS2_CUBEMAP_NEGATIVEX = 0x00000800,
	DDSCAPS2_CUBEMAP_POSITIVEY = 0x00001000,
	DDSCAPS2_CUBEMAP_NEGATIVEY = 0x00002000,
	DDSCAPS2_CUBEMAP_POSITIVEZ = 0x00004000,
	DDSCAPS2_CUBEMAP_NEGATIVEZ = 0x00008000,
	DDSCAPS2_VOLUME            = 0x00200000,
};

#define DDSCAPS2_CUBEMAP_ALLFACES ( \
	DDSCAPS2_CUBEMAP_POSITIVEX |\
	DDSCAPS2_CUBEMAP_NEGATIVEX |\
	DDSCAPS2_CUBEMAP_POSITIVEY |\
	DDSCAPS2_CUBEMAP_NEGATIVEY |\
	DDSCAPS2_CUBEMAP_POSITIVEZ |\
	DDSCAPS2_CUBEMAP_NEGATIVEZ )

struct DDCAPS2 {
	u32 dwCaps1;
	u32 dwCaps2;
	u32 Reserved[2];
};

enum {
	DDSD_CAPS        = 0x00000001,
	DDSD_HEIGHT      = 0x00000002,
	DDSD_WIDTH       = 0x00000004,
	DDSD_PITCH       = 0x00000008,
	DDSD_PIXELFORMAT = 0x00001000,
	DDSD_MIPMAPCOUNT = 0x00020000,
	DDSD_LINEARSIZE  = 0x00080000,
	DDSD_DEPTH       = 0x00800000,
};

struct DDSURFACEDESC2 {
	u32 dwSize;                    // Size of structure. This member must be set to 124.
	u32 dwFlags;                   // Flags to indicate valid fields. Always include DDSD_CAPS, DDSD_PIXELFORMAT, DDSD_WIDTH, DDSD_HEIGHT.
	u32 dwHeight;                  // Height of the main image in pixels
	u32 dwWidth;                   // Width of the main image in pixels
	u32 dwPitchOrLinearSize;       // For uncompressed formats, this is the number of bytes per scan line (DWORD> aligned) for the main image. dwFlags should include DDSD_PITCH in this case. For compressed formats, this is the total number of bytes for the main image. dwFlags should be include DDSD_LINEARSIZE in this case.
	u32 dwDepth;                   // For volume textures, this is the depth of the volume. dwFlags should include DDSD_DEPTH in this case.
	u32 dwMipMapCount;             // For items with mipmap levels, this is the total number of levels in the mipmap chain of the main image. dwFlags should include DDSD_MIPMAPCOUNT in this case.
	u32 dwReserved1[4];            // Unused
	float fGamma;                  // gamma value that this texture was saved at.
	float fColorExp[3];            // Color scale (pre-calculated exponent) (R,G,B) for HDR values
	float fColorOfs[3];            // Color offset (R,G,B) for HDR values
	DDPIXELFORMAT ddpfPixelFormat; // 32-byte value that specifies the pixel format structure.
	DDCAPS2 ddsCaps;               // 16-byte value that specifies the capabilities structure.
	u32 dwReserved2;               // Unused
};

enum DDS_D3DFORMAT { // copied from d3d9types.h 'enum _D3DFORMAT', but prefixed "DDS_" to avoid name conflicts
	DDS_D3DFMT_UNKNOWN              = 0,
	DDS_D3DFMT_R8G8B8               = 20,
	DDS_D3DFMT_A8R8G8B8             = 21,
	DDS_D3DFMT_X8R8G8B8             = 22,
	DDS_D3DFMT_R5G6B5               = 23,
	DDS_D3DFMT_X1R5G5B5             = 24,
	DDS_D3DFMT_A1R5G5B5             = 25,
	DDS_D3DFMT_A4R4G4B4             = 26,
	DDS_D3DFMT_R3G3B2               = 27,
	DDS_D3DFMT_A8                   = 28,
	DDS_D3DFMT_A8R3G3B2             = 29,
	DDS_D3DFMT_X4R4G4B4             = 30,
	DDS_D3DFMT_A2B10G10R10          = 31,
	DDS_D3DFMT_A8B8G8R8             = 32,
	DDS_D3DFMT_X8B8G8R8             = 33,
	DDS_D3DFMT_G16R16               = 34,
	DDS_D3DFMT_A2R10G10B10          = 35,
	DDS_D3DFMT_A16B16G16R16         = 36,

	DDS_D3DFMT_A8P8                 = 40,
	DDS_D3DFMT_P8                   = 41,

	DDS_D3DFMT_L8                   = 50,
	DDS_D3DFMT_A8L8                 = 51,
	DDS_D3DFMT_A4L4                 = 52,

	DDS_D3DFMT_V8U8                 = 60,
	DDS_D3DFMT_L6V5U5               = 61,
	DDS_D3DFMT_X8L8V8U8             = 62,
	DDS_D3DFMT_Q8W8V8U8             = 63,
	DDS_D3DFMT_V16U16               = 64,
	DDS_D3DFMT_A2W10V10U10          = 67,

	DDS_D3DFMT_UYVY                 = MAKE_MAGIC_NUMBER('U', 'Y', 'V', 'Y'),
	DDS_D3DFMT_R8G8_B8G8            = MAKE_MAGIC_NUMBER('R', 'G', 'B', 'G'),
	DDS_D3DFMT_YUY2                 = MAKE_MAGIC_NUMBER('Y', 'U', 'Y', '2'),
	DDS_D3DFMT_G8R8_G8B8            = MAKE_MAGIC_NUMBER('G', 'R', 'G', 'B'),
	DDS_D3DFMT_DXT1                 = MAKE_MAGIC_NUMBER('D', 'X', 'T', '1'),
	DDS_D3DFMT_DXT2                 = MAKE_MAGIC_NUMBER('D', 'X', 'T', '2'),
	DDS_D3DFMT_DXT3                 = MAKE_MAGIC_NUMBER('D', 'X', 'T', '3'),
	DDS_D3DFMT_DXT4                 = MAKE_MAGIC_NUMBER('D', 'X', 'T', '4'),
	DDS_D3DFMT_DXT5                 = MAKE_MAGIC_NUMBER('D', 'X', 'T', '5'),

	DDS_D3DFMT_CTX1                 = MAKE_MAGIC_NUMBER('C', 'T', 'X', '1'), // xenon only
	DDS_D3DFMT_DXT3A                = MAKE_MAGIC_NUMBER('A', 'X', 'T', '3'), // xenon only
	DDS_D3DFMT_DXT3A_1111           = MAKE_MAGIC_NUMBER('A', '1', 'T', '3'), // xenon only
	DDS_D3DFMT_DXT5A                = MAKE_MAGIC_NUMBER('A', 'T', 'I', '1'), // xenon/d3d11/orbis
	DDS_D3DFMT_DXN                  = MAKE_MAGIC_NUMBER('A', 'T', 'I', '2'), // xenon/d3d11/orbis
	DDS_D3DFMT_BC6                  = MAKE_MAGIC_NUMBER('B', 'C', '6', ' '), // d3d11/orbis
	DDS_D3DFMT_BC7                  = MAKE_MAGIC_NUMBER('B', 'C', '7', ' '), // d3d11/orbis

	DDS_D3DFMT_R8                   = MAKE_MAGIC_NUMBER('R', '8', ' ', ' '), // custom (works on all platforms)
	DDS_D3DFMT_R16                  = MAKE_MAGIC_NUMBER('R', '1', '6', ' '), // custom (works on all platforms)
	DDS_D3DFMT_G8R8                 = MAKE_MAGIC_NUMBER('G', '8', 'R', '8'), // custom (works on all platforms)

	DDS_D3DFMT_D16_LOCKABLE         = 70,
	DDS_D3DFMT_D32                  = 71,
	DDS_D3DFMT_D15S1                = 73,
	DDS_D3DFMT_D24S8                = 75,
	DDS_D3DFMT_D24X8                = 77,
	DDS_D3DFMT_D24X4S4              = 79,
	DDS_D3DFMT_D16                  = 80,

	DDS_D3DFMT_D32F_LOCKABLE        = 82,
	DDS_D3DFMT_D24FS8               = 83,

	DDS_D3DFMT_D32_LOCKABLE         = 84,
	DDS_D3DFMT_S8_LOCKABLE          = 85,

	DDS_D3DFMT_L16                  = 81,

	DDS_D3DFMT_VERTEXDATA           = 100,
	DDS_D3DFMT_INDEX16              = 101,
	DDS_D3DFMT_INDEX32              = 102,

	DDS_D3DFMT_Q16W16V16U16         = 110,

	DDS_D3DFMT_MULTI2_ARGB8         = MAKE_MAGIC_NUMBER('M','E','T','1'),

	DDS_D3DFMT_R16F                 = 111,
	DDS_D3DFMT_G16R16F              = 112,
	DDS_D3DFMT_A16B16G16R16F        = 113,

	DDS_D3DFMT_R32F                 = 114,
	DDS_D3DFMT_G32R32F              = 115,
	DDS_D3DFMT_A32B32G32R32F        = 116,

	DDS_D3DFMT_CxV8U8               = 117,

	DDS_D3DFMT_A1                   = 118,

	DDS_D3DFMT_A2B10G10R10_XR_BIAS  = 119,

	DDS_D3DFMT_BINARYBUFFER         = 199,

	DDS_D3DFMT_FORCE_DWORD          = 0x7fffffff
};

struct DDS_HEADER_DXT10
{
	u32 dxgiFormat;
	u32 resourceDimension;
	u32 miscFlag; // see DDS_RESOURCE_MISC_FLAG
	u32 arraySize;
	u32 miscFlags2; // see DDS_MISC_FLAGS2
};

enum
{
	// these are used in DDS_HEADER_DXT10 resourceDimension 
	DDS_DIMENSION_TEXTURE1D = 2,
	DDS_DIMENSION_TEXTURE2D = 3,
	DDS_DIMENSION_TEXTURE3D = 4,

	// these are used in DDS_HEADER_DXT10 miscFlag
	DDS_RESOURCE_MISC_TEXTURECUBE = 0x4,
};

enum DXGI_FORMAT
{
	DXGI_FORMAT_UNKNOWN                     = 0,
	DXGI_FORMAT_R32G32B32A32_TYPELESS       = 1,
	DXGI_FORMAT_R32G32B32A32_FLOAT          = 2,
	DXGI_FORMAT_R32G32B32A32_UINT           = 3,
	DXGI_FORMAT_R32G32B32A32_SINT           = 4,
	DXGI_FORMAT_R32G32B32_TYPELESS          = 5,
	DXGI_FORMAT_R32G32B32_FLOAT             = 6,
	DXGI_FORMAT_R32G32B32_UINT              = 7,
	DXGI_FORMAT_R32G32B32_SINT              = 8,
	DXGI_FORMAT_R16G16B16A16_TYPELESS       = 9,
	DXGI_FORMAT_R16G16B16A16_FLOAT          = 10,
	DXGI_FORMAT_R16G16B16A16_UNORM          = 11,
	DXGI_FORMAT_R16G16B16A16_UINT           = 12,
	DXGI_FORMAT_R16G16B16A16_SNORM          = 13,
	DXGI_FORMAT_R16G16B16A16_SINT           = 14,
	DXGI_FORMAT_R32G32_TYPELESS             = 15,
	DXGI_FORMAT_R32G32_FLOAT                = 16,
	DXGI_FORMAT_R32G32_UINT                 = 17,
	DXGI_FORMAT_R32G32_SINT                 = 18,
	DXGI_FORMAT_R32G8X24_TYPELESS           = 19,
	DXGI_FORMAT_D32_FLOAT_S8X24_UINT        = 20,
	DXGI_FORMAT_R32_FLOAT_X8X24_TYPELESS    = 21,
	DXGI_FORMAT_X32_TYPELESS_G8X24_UINT     = 22,
	DXGI_FORMAT_R10G10B10A2_TYPELESS        = 23,
	DXGI_FORMAT_R10G10B10A2_UNORM           = 24,
	DXGI_FORMAT_R10G10B10A2_UINT            = 25,
	DXGI_FORMAT_R11G11B10_FLOAT             = 26,
	DXGI_FORMAT_R8G8B8A8_TYPELESS           = 27,
	DXGI_FORMAT_R8G8B8A8_UNORM              = 28,
	DXGI_FORMAT_R8G8B8A8_UNORM_SRGB         = 29,
	DXGI_FORMAT_R8G8B8A8_UINT               = 30,
	DXGI_FORMAT_R8G8B8A8_SNORM              = 31,
	DXGI_FORMAT_R8G8B8A8_SINT               = 32,
	DXGI_FORMAT_R16G16_TYPELESS             = 33,
	DXGI_FORMAT_R16G16_FLOAT                = 34,
	DXGI_FORMAT_R16G16_UNORM                = 35,
	DXGI_FORMAT_R16G16_UINT                 = 36,
	DXGI_FORMAT_R16G16_SNORM                = 37,
	DXGI_FORMAT_R16G16_SINT                 = 38,
	DXGI_FORMAT_R32_TYPELESS                = 39,
	DXGI_FORMAT_D32_FLOAT                   = 40,
	DXGI_FORMAT_R32_FLOAT                   = 41,
	DXGI_FORMAT_R32_UINT                    = 42,
	DXGI_FORMAT_R32_SINT                    = 43,
	DXGI_FORMAT_R24G8_TYPELESS              = 44,
	DXGI_FORMAT_D24_UNORM_S8_UINT           = 45,
	DXGI_FORMAT_R24_UNORM_X8_TYPELESS       = 46,
	DXGI_FORMAT_X24_TYPELESS_G8_UINT        = 47,
	DXGI_FORMAT_R8G8_TYPELESS               = 48,
	DXGI_FORMAT_R8G8_UNORM                  = 49,
	DXGI_FORMAT_R8G8_UINT                   = 50,
	DXGI_FORMAT_R8G8_SNORM                  = 51,
	DXGI_FORMAT_R8G8_SINT                   = 52,
	DXGI_FORMAT_R16_TYPELESS                = 53,
	DXGI_FORMAT_R16_FLOAT                   = 54,
	DXGI_FORMAT_D16_UNORM                   = 55,
	DXGI_FORMAT_R16_UNORM                   = 56,
	DXGI_FORMAT_R16_UINT                    = 57,
	DXGI_FORMAT_R16_SNORM                   = 58,
	DXGI_FORMAT_R16_SINT                    = 59,
	DXGI_FORMAT_R8_TYPELESS                 = 60,
	DXGI_FORMAT_R8_UNORM                    = 61,
	DXGI_FORMAT_R8_UINT                     = 62,
	DXGI_FORMAT_R8_SNORM                    = 63,
	DXGI_FORMAT_R8_SINT                     = 64,
	DXGI_FORMAT_A8_UNORM                    = 65,
	DXGI_FORMAT_R1_UNORM                    = 66,
	DXGI_FORMAT_R9G9B9E5_SHAREDEXP          = 67,
	DXGI_FORMAT_R8G8_B8G8_UNORM             = 68,
	DXGI_FORMAT_G8R8_G8B8_UNORM             = 69,
	DXGI_FORMAT_BC1_TYPELESS                = 70,
	DXGI_FORMAT_BC1_UNORM                   = 71,
	DXGI_FORMAT_BC1_UNORM_SRGB              = 72,
	DXGI_FORMAT_BC2_TYPELESS                = 73,
	DXGI_FORMAT_BC2_UNORM                   = 74,
	DXGI_FORMAT_BC2_UNORM_SRGB              = 75,
	DXGI_FORMAT_BC3_TYPELESS                = 76,
	DXGI_FORMAT_BC3_UNORM                   = 77,
	DXGI_FORMAT_BC3_UNORM_SRGB              = 78,
	DXGI_FORMAT_BC4_TYPELESS                = 79,
	DXGI_FORMAT_BC4_UNORM                   = 80,
	DXGI_FORMAT_BC4_SNORM                   = 81,
	DXGI_FORMAT_BC5_TYPELESS                = 82,
	DXGI_FORMAT_BC5_UNORM                   = 83,
	DXGI_FORMAT_BC5_SNORM                   = 84,
	DXGI_FORMAT_B5G6R5_UNORM                = 85,
	DXGI_FORMAT_B5G5R5A1_UNORM              = 86,
	DXGI_FORMAT_B8G8R8A8_UNORM              = 87,
	DXGI_FORMAT_B8G8R8X8_UNORM              = 88,
	DXGI_FORMAT_R10G10B10_XR_BIAS_A2_UNORM  = 89,
	DXGI_FORMAT_B8G8R8A8_TYPELESS           = 90,
	DXGI_FORMAT_B8G8R8A8_UNORM_SRGB         = 91,
	DXGI_FORMAT_B8G8R8X8_TYPELESS           = 92,
	DXGI_FORMAT_B8G8R8X8_UNORM_SRGB         = 93,
	DXGI_FORMAT_BC6H_TYPELESS               = 94,
	DXGI_FORMAT_BC6H_UF16                   = 95,
	DXGI_FORMAT_BC6H_SF16                   = 96,
	DXGI_FORMAT_BC7_TYPELESS                = 97,
	DXGI_FORMAT_BC7_UNORM                   = 98,
	DXGI_FORMAT_BC7_UNORM_SRGB              = 99,
	DXGI_FORMAT_AYUV                        = 100,
	DXGI_FORMAT_Y410                        = 101,
	DXGI_FORMAT_Y416                        = 102,
	DXGI_FORMAT_NV12                        = 103,
	DXGI_FORMAT_P010                        = 104,
	DXGI_FORMAT_P016                        = 105,
	DXGI_FORMAT_420_OPAQUE                  = 106,
	DXGI_FORMAT_YUY2                        = 107,
	DXGI_FORMAT_Y210                        = 108,
	DXGI_FORMAT_Y216                        = 109,
	DXGI_FORMAT_NV11                        = 110,
	DXGI_FORMAT_AI44                        = 111,
	DXGI_FORMAT_IA44                        = 112,
	DXGI_FORMAT_P8                          = 113,
	DXGI_FORMAT_A8P8                        = 114,
	DXGI_FORMAT_B4G4R4A4_UNORM              = 115,
	DXGI_FORMAT_R10G10B10_7E3_A2_FLOAT      = 116,
	DXGI_FORMAT_R10G10B10_6E4_A2_FLOAT      = 117,
	DXGI_FORMAT_LAST                        = 118,
};

enum DDS_IMAGE_TYPE
{
	DDS_IMAGE_TYPE_UNKNOWN = 1,
	DDS_IMAGE_TYPE_2D,
	DDS_IMAGE_TYPE_3D,
	DDS_IMAGE_TYPE_CUBE,
};

DDS_D3DFORMAT GetDDSFormat(const DDPIXELFORMAT& ddpf);
const char* GetD3DFormatStr(DDS_D3DFORMAT format);
bool CompareDDSFormatMask(const DDPIXELFORMAT& ddpf, u32 flags, u32 bitCount, u32 rMask, u32 gMask, u32 bMask, u32 aMask);
DXGI_FORMAT GetDX10Format(const DDPIXELFORMAT& ddpf);
DDPIXELFORMAT GetDDSPixelFormatFromDX10Format(DXGI_FORMAT dxgiFormat);
bool GetDDSInfo(const char* path, int& w, int& h, int* mips, DXGI_FORMAT* dxgiFormat, DDS_IMAGE_TYPE* imageType, int* arrayCount);
void WriteDDSHeader(FILE* dds, DDS_IMAGE_TYPE type, int w, int h, int d, int mips, int layers, const DDPIXELFORMAT& ddpf, DXGI_FORMAT dxgiFormat = DXGI_FORMAT_UNKNOWN, bool bSaveDX10 = false);
int GetBlockSize(DXGI_FORMAT format);
int GetBitsPerPixel(DXGI_FORMAT format);
int GetMaxMipCount(int w, int h, int d, bool bAllowNonPow2Scaling = false);
int GetImageSizeInBytes(int w, int h, int d, int mips, DXGI_FORMAT dxgiFormat, DDS_IMAGE_TYPE imageType, int arrayCount);
bool IsDX9FormatCompressedOrPacked(DDS_D3DFORMAT format);
bool IsDX10FormatCompressedOrPacked(DXGI_FORMAT format);

#endif
