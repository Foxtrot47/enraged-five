// =============
// imageutil.cpp
// =============

#include "dds.h"
#include "imageutil.h"
#include "mathutil.h"

#if defined(_FREEIMAGE)
static void FreeImageInit()
{
	static bool once = true;

	if (once)
	{
		FreeImage_Initialise();
		once = false;
	}
}
#endif // defined(_FREEIMAGE)

bool GetImageDimensions(const char* path, int& w, int& h)
{
	if (strstr(path, ".dds"))
	{
		FILE* ddsFile = fopen(path, "rb");

		if (ddsFile)
		{
			DDSURFACEDESC2 ddsHeader;

			fseek(ddsFile, 4, SEEK_SET); // skip past tag
			fread(&ddsHeader, sizeof(DDSURFACEDESC2), 1, ddsFile);
			fclose(ddsFile);

			if (ddsHeader.dwSize == sizeof(DDSURFACEDESC2))
			{
				w = (int)ddsHeader.dwWidth;
				h = (int)ddsHeader.dwHeight;
				return true;
			}
		}
	}
#if defined(_FREEIMAGE)
	else
	{
		FreeImageInit();

		FREE_IMAGE_FORMAT fif = FreeImage_GetFileType(path);

		if (fif == FIF_UNKNOWN)
		{
			fif = FreeImage_GetFIFFromFilename(path);
		}

		if (fif != FIF_UNKNOWN && FreeImage_FIFSupportsReading(fif))
		{
			FIBITMAP* dib = FreeImage_Load(fif, path, FIF_LOAD_NOPIXELS);

			if (dib)
			{
				w = FreeImage_GetWidth(dib);
				h = FreeImage_GetHeight(dib);
				FreeImage_Unload(dib);
				return true;
			}
		}
	}
#endif // defined(_FREEIMAGE)

	w = 0;
	h = 0;
	return false;
}

template <typename DstType, typename SrcType> static DstType* LoadImageDDSInternal_T(const void* src_, int w, int h, int y0 = 0)
{
	const SrcType* src = reinterpret_cast<const SrcType*>(src_) + w*y0;
	DstType* dst = rage_new DstType[w*h];

	for (int i = 0; i < w*h; i++)
	{
		dst[i] = ConvertPixel<DstType,SrcType>(src[i]);
	}

	return dst;
}

template <typename DstType> static DstType* LoadImageDDS_T(const char* path, int& w, int& h)
{
	DstType* image = NULL;

	if (strstr(path, ".dds"))
	{
		FILE* ddsFile = fopen(path, "rb");

		if (ddsFile)
		{
			fseek(ddsFile, 0, SEEK_END);
			const u32 ddsSize = (u32)ftell(ddsFile);
			fseek(ddsFile, 0, SEEK_SET);

			if (ddsSize > sizeof(DDSURFACEDESC2))
			{
				u8* ddsData = rage_new u8[ddsSize];
				fread(ddsData, ddsSize, 1, ddsFile);

				const DDSURFACEDESC2* ddsHeader = (DDSURFACEDESC2*)(ddsData + sizeof(u32));
				const void*           ddsPixels = (const u8*)ddsHeader + ddsHeader->dwSize;

				Assert(ddsHeader->dwSize == sizeof(DDSURFACEDESC2));

				if ((w == 0 && h == 0) || (w == (int)ddsHeader->dwWidth && h == (int)ddsHeader->dwHeight))
				{
					w = (int)ddsHeader->dwWidth;
					h = (int)ddsHeader->dwHeight;

					DDS_D3DFORMAT format = GetDDSFormat(ddsHeader->ddpfPixelFormat);
					int headerSize = sizeof(u32) + sizeof(DDSURFACEDESC2);

					if (format == MAKE_MAGIC_NUMBER('D','X','1','0'))
					{
						const DDS_HEADER_DXT10* dx10header = (const DDS_HEADER_DXT10*)ddsPixels;
						ddsPixels = (u8*)ddsPixels + sizeof(DDS_HEADER_DXT10);
						format = GetDDSFormat(GetDDSPixelFormatFromDX10Format((DXGI_FORMAT)dx10header->dxgiFormat));
						headerSize += sizeof(DDS_HEADER_DXT10);
					}

					if (format == DDS_D3DFMT_L8 && ddsSize >= headerSize + w*h*sizeof(u8))
					{
						image = LoadImageDDSInternal_T<DstType,u8>(ddsPixels, w, h);
					}
					else if (format == DDS_D3DFMT_L16 && ddsSize >= headerSize + w*h*sizeof(u16))
					{
						image = LoadImageDDSInternal_T<DstType,u16>(ddsPixels, w, h);
					}
					else if (format == DDS_D3DFMT_R32F && ddsSize >= headerSize + w*h*sizeof(float))
					{
						image = LoadImageDDSInternal_T<DstType,float>(ddsPixels, w, h);
					}
					else if (format == DDS_D3DFMT_A32B32G32R32F && ddsSize >= headerSize + w*h*sizeof(Vec4))
					{
						image = LoadImageDDSInternal_T<DstType,Vec4>(ddsPixels, w, h);
					}
					else if (format == DDS_D3DFMT_A8R8G8B8 && ddsSize >= headerSize + w*h*sizeof(Pixel32))
					{
						image = LoadImageDDSInternal_T<DstType,Pixel32>(ddsPixels, w, h);
					}
					else if (format == DDS_D3DFMT_A16B16G16R16 && ddsSize >= headerSize + w*h*sizeof(Pixel64))
					{
						image = LoadImageDDSInternal_T<DstType,Pixel64>(ddsPixels, w, h);

						// fix inconsistency when loading dds as RGBA16
						{
							Pixel64* pixels64 = reinterpret_cast<Pixel64*>(image);

							for (int i = 0; i < w*h; i++)
							{
								std::swap(pixels64[i].r, pixels64[i].b);
							}
						}
					}
					else
					{
						// unsupported format
					}
				}
				else
				{
					// unexpected resolution
				}

				delete[] ddsData;
			}

			fclose(ddsFile);
		}
	}

	return image;
}

#if defined(_FREEIMAGE)
template <typename DstType, typename SrcType> static DstType* LoadImageFreeImageInternal_T(FIBITMAP* dib, int w, int h, FREE_IMAGE_FORMAT fif)
{
	DstType* dst = rage_new DstType[w*h];
	DstType* row = dst;

	for (int j = 0; j < h; j++)
	{
		const SrcType* pixels = reinterpret_cast<const SrcType*>(FreeImage_GetScanLine(dib, h - j - 1));

		// fix inconsistency when FreeImage loads png or tif as RGBA16
		if (GetType<SrcType>::T == _Pixel64 && FreeImage_GetImageType(dib) == FIT_RGBA16 && (fif == FIF_PNG || fif == FIF_TIFF))
		{
			for (int i = 0; i < w; i++)
			{
				Pixel64 pixel = reinterpret_cast<const Pixel64*>(pixels)[i];
				std::swap(pixel.r, pixel.b);
				row[i] = ConvertPixel<DstType,SrcType>(reinterpret_cast<const SrcType&>(pixel));
			}
		}
		else
		{
			for (int i = 0; i < w; i++)
			{
				row[i] = ConvertPixel<DstType,SrcType>(pixels[i]);
			}
		}

		row += w;
	}

	return dst;
}
#endif // defined(_FREEIMAGE)

template <typename DstType> static DstType* LoadImage_T(const char* path, int& w, int& h, int sampleIndex = -1)
{
	DstType* image = LoadImageDDS_T<DstType>(path, w, h);

	if (image)
	{
		return image;
	}

#if defined(_FREEIMAGE)
	FreeImageInit();

	FREE_IMAGE_FORMAT fif = FreeImage_GetFileType(path);
	FIBITMAP* dib = NULL;

	if (fif == FIF_UNKNOWN)
	{
		fif = FreeImage_GetFIFFromFilename(path);
	}

	if (fif != FIF_UNKNOWN && FreeImage_FIFSupportsReading(fif))
	{
		int flags = 0;

		if (fif == FIF_TIFF && sampleIndex >= 0)
		{
			flags |= TIFF_LOAD_SAMPLE;
			flags |= (sampleIndex << TIFF_LOAD_SAMPLE_INDEX_SHIFT);
		}

		dib = FreeImage_Load(fif, path, flags);
	}

	if (dib == NULL)
	{
		return NULL;
	}

	if ((w == 0 && h == 0) || (w == (int)FreeImage_GetWidth(dib) && h == (int)FreeImage_GetHeight(dib)))
	{
		w = FreeImage_GetWidth(dib);
		h = FreeImage_GetHeight(dib);

		const FREE_IMAGE_TYPE fit = FreeImage_GetImageType(dib);

		if (fit == FIT_BITMAP)
		{
			if (FreeImage_GetBPP(dib) == 8)
			{
				image = LoadImageFreeImageInternal_T<DstType,u8>(dib, w, h, fif);
			}
			else
			{
				if (FreeImage_GetBPP(dib) != 32)
				{
					FIBITMAP* dib2 = FreeImage_ConvertTo32Bits(dib);
					FreeImage_Unload(dib);
					dib = dib2;
				}

				if (dib)
				{
					image = LoadImageFreeImageInternal_T<DstType,Pixel32>(dib, w, h, fif);
				}
				else
				{
					return NULL;
				}
			}
		}
		else if (fit == FIT_UINT16)
		{
			image = LoadImageFreeImageInternal_T<DstType,u16>(dib, w, h, fif);
		}
		else if (fit == FIT_FLOAT)
		{
			image = LoadImageFreeImageInternal_T<DstType,float>(dib, w, h, fif);
		}
		else if (fit == FIT_RGBF)
		{
			image = LoadImageFreeImageInternal_T<DstType,Vec3>(dib, w, h, fif);
		}
		else if (fit == FIT_RGBAF)
		{
			image = LoadImageFreeImageInternal_T<DstType,Vec4>(dib, w, h, fif);
		}
		else if (fit == FIT_RGBA16)
		{
			image = LoadImageFreeImageInternal_T<DstType,Pixel64>(dib, w, h, fif);
		}
		else
		{
			// unsupported format
		}

		FreeImage_Unload(dib);
	}
	else
	{
		// unexpected resolution
	}
#endif // defined(_FREEIMAGE)

	return image;
}

u8     * LoadImage_u8     (const char* path, int& w, int& h, int sampleIndex) { return LoadImage_T<u8     >(path, w, h, sampleIndex); }
u16    * LoadImage_u16    (const char* path, int& w, int& h, int sampleIndex) { return LoadImage_T<u16    >(path, w, h, sampleIndex); }
float  * LoadImage_float  (const char* path, int& w, int& h, int sampleIndex) { return LoadImage_T<float  >(path, w, h, sampleIndex); }
Vec3   * LoadImage_Vec3   (const char* path, int& w, int& h, int sampleIndex) { return LoadImage_T<Vec3   >(path, w, h, sampleIndex); }
Vec4   * LoadImage_Vec4   (const char* path, int& w, int& h, int sampleIndex) { return LoadImage_T<Vec4   >(path, w, h, sampleIndex); }
Pixel32* LoadImage_Pixel32(const char* path, int& w, int& h, int sampleIndex) { return LoadImage_T<Pixel32>(path, w, h, sampleIndex); }
Pixel64* LoadImage_Pixel64(const char* path, int& w, int& h, int sampleIndex) { return LoadImage_T<Pixel64>(path, w, h, sampleIndex); }

template <typename DstType> static DstType* LoadImageDDSSpan_T(const char* path, int w, int y0, int h)
{
	DstType* image = NULL;

	if (strstr(path, ".dds"))
	{
		FILE* ddsFile = fopen(path, "rb");

		if (ddsFile)
		{
			fseek(ddsFile, 0, SEEK_END);
			const u32 ddsSize = (u32)ftell(ddsFile);
			fseek(ddsFile, 0, SEEK_SET);

			if (ddsSize > sizeof(DDSURFACEDESC2))
			{
				u8* ddsData = rage_new u8[ddsSize];
				fread(ddsData, ddsSize, 1, ddsFile);

				const DDSURFACEDESC2* ddsHeader = (DDSURFACEDESC2*)(ddsData + sizeof(u32));
				const void*           ddsPixels = (const u8*)ddsHeader + ddsHeader->dwSize;

				Assert(ddsHeader->dwSize == sizeof(DDSURFACEDESC2));

				if (w == (int)ddsHeader->dwWidth && y0 >= 0 && y0 + h <= (int)ddsHeader->dwHeight)
				{
					w = (int)ddsHeader->dwWidth;
					h = (int)ddsHeader->dwHeight;

					DDS_D3DFORMAT format = GetDDSFormat(ddsHeader->ddpfPixelFormat);
					int headerSize = sizeof(u32) + sizeof(DDSURFACEDESC2);

					if (format == MAKE_MAGIC_NUMBER('D','X','1','0'))
					{
						const DDS_HEADER_DXT10* dx10header = (const DDS_HEADER_DXT10*)ddsPixels;
						ddsPixels = (u8*)ddsPixels + sizeof(DDS_HEADER_DXT10);
						format = GetDDSFormat(GetDDSPixelFormatFromDX10Format((DXGI_FORMAT)dx10header->dxgiFormat));
						headerSize += sizeof(DDS_HEADER_DXT10);
					}

					if (format == DDS_D3DFMT_L8 && ddsSize >= headerSize + w*(y0 + h)*sizeof(u8))
					{
						image = LoadImageDDSInternal_T<DstType,u8>(ddsPixels, w, h);
					}
					else if (format == DDS_D3DFMT_L16 && ddsSize >= headerSize + w*(y0 + h)*sizeof(u16))
					{
						image = LoadImageDDSInternal_T<DstType,u16>(ddsPixels, w, h);
					}
					else if (format == DDS_D3DFMT_R32F && ddsSize >= headerSize + w*(y0 + h)*sizeof(float))
					{
						image = LoadImageDDSInternal_T<DstType,float>(ddsPixels, w, h);
					}
					else if (format == DDS_D3DFMT_A32B32G32R32F && ddsSize >= headerSize + w*(y0 + h)*sizeof(Vec4))
					{
						image = LoadImageDDSInternal_T<DstType,Vec4>(ddsPixels, w, h);
					}
					else if (format == DDS_D3DFMT_A8R8G8B8 && ddsSize >= headerSize + w*(y0 + h)*sizeof(Pixel32))
					{
						image = LoadImageDDSInternal_T<DstType,Pixel32>(ddsPixels, w, h);
					}
					else if (format == DDS_D3DFMT_A16B16G16R16 && ddsSize >= headerSize + w*(y0 + h)*sizeof(Pixel64))
					{
						image = LoadImageDDSInternal_T<DstType,Pixel64>(ddsPixels, w, h);
					}
					else
					{
						// unsupported format
					}
				}
				else
				{
					// unexpected resolution
				}

				delete[] ddsData;
			}

			fclose(ddsFile);
		}
	}

	return image;
}

u8     * LoadImageDDSSpan_u8     (const char* path, int w, int y0, int h) { return LoadImageDDSSpan_T<u8     >(path, w, y0, h); }
u16    * LoadImageDDSSpan_u16    (const char* path, int w, int y0, int h) { return LoadImageDDSSpan_T<u16    >(path, w, y0, h); }
float  * LoadImageDDSSpan_float  (const char* path, int w, int y0, int h) { return LoadImageDDSSpan_T<float  >(path, w, y0, h); }
Vec3   * LoadImageDDSSpan_Vec3   (const char* path, int w, int y0, int h) { return LoadImageDDSSpan_T<Vec3   >(path, w, y0, h); }
Vec4   * LoadImageDDSSpan_Vec4   (const char* path, int w, int y0, int h) { return LoadImageDDSSpan_T<Vec4   >(path, w, y0, h); }
Pixel32* LoadImageDDSSpan_Pixel32(const char* path, int w, int y0, int h) { return LoadImageDDSSpan_T<Pixel32>(path, w, y0, h); }
Pixel64* LoadImageDDSSpan_Pixel64(const char* path, int w, int y0, int h) { return LoadImageDDSSpan_T<Pixel64>(path, w, y0, h); }

template <typename DstType> static DDPIXELFORMAT GetDDSFormatInternal_T();

template <> DDPIXELFORMAT GetDDSFormatInternal_T<u8>()
{
	DDPIXELFORMAT ddpf;
	memset(&ddpf, 0, sizeof(ddpf));
	ddpf.dwSize = sizeof(ddpf);
	ddpf.dwFlags = DDPF_LUMINANCE;
	ddpf.dwRGBBitCount = 8;
	ddpf.dwRBitMask = 0x000000ff;
	return ddpf;
}

template <> DDPIXELFORMAT GetDDSFormatInternal_T<u16>()
{
	DDPIXELFORMAT ddpf;
	memset(&ddpf, 0, sizeof(ddpf));
	ddpf.dwSize = sizeof(ddpf);
	ddpf.dwFlags = DDPF_LUMINANCE;
	ddpf.dwRGBBitCount = 16;
	ddpf.dwRBitMask = 0x0000ffff;
	return ddpf;
}

template <> DDPIXELFORMAT GetDDSFormatInternal_T<float>()
{
	DDPIXELFORMAT ddpf;
	memset(&ddpf, 0, sizeof(ddpf));
	ddpf.dwSize = sizeof(ddpf);
	ddpf.dwFlags = DDPF_FOURCC;
	ddpf.dwFourCC = DDS_D3DFMT_R32F;
	return ddpf;
}

template <> DDPIXELFORMAT GetDDSFormatInternal_T<Vec3>(); // not supported

template <> DDPIXELFORMAT GetDDSFormatInternal_T<Vec4>()
{
	DDPIXELFORMAT ddpf;
	memset(&ddpf, 0, sizeof(ddpf));
	ddpf.dwSize = sizeof(ddpf);
	ddpf.dwFlags = DDPF_FOURCC;
	ddpf.dwFourCC = DDS_D3DFMT_A32B32G32R32F;
	return ddpf;
}

template <> DDPIXELFORMAT GetDDSFormatInternal_T<Pixel32>()
{
	DDPIXELFORMAT ddpf;
	memset(&ddpf, 0, sizeof(ddpf));
	ddpf.dwSize = sizeof(ddpf);
	ddpf.dwFlags = DDPF_RGB | DDPF_ALPHAPIXELS;
	ddpf.dwRGBBitCount = 32;
	ddpf.dwRBitMask = 0x00ff0000;
	ddpf.dwGBitMask = 0x0000ff00;
	ddpf.dwBBitMask = 0x000000ff;
	ddpf.dwABitMask = 0xff000000;
	return ddpf;
}

template <> DDPIXELFORMAT GetDDSFormatInternal_T<Pixel64>()
{
	DDPIXELFORMAT ddpf;
	memset(&ddpf, 0, sizeof(ddpf));
	ddpf.dwSize = sizeof(ddpf);
	ddpf.dwFlags = DDPF_FOURCC;
	ddpf.dwFourCC = DDS_D3DFMT_A16B16G16R16;
	return ddpf;
}

template <typename DstType, typename SrcType> static bool SaveImageDDS_T(const char* path, const SrcType* image, int w, int h)
{
	if (strstr(path, ".dds"))
	{
		FILE* dds = fopen(path, "wb");

		if (dds)
		{
			WriteDDSHeader(dds, DDS_IMAGE_TYPE_2D, w, h, 1, 1, 1, GetDDSFormatInternal_T<DstType>());

			// fix inconsistency when saving dds as RGBA16
			const bool bSwapRBForPixel64 = (GetType<DstType>::T == _Pixel64);

			if (GetType<DstType>::T == GetType<SrcType>::T && !bSwapRBForPixel64)
			{
				fwrite(image, sizeof(DstType), w*h, dds);
			}
			else
			{
				DstType* pixels = rage_new DstType[w];

				for (int j = 0; j < h; j++)
				{
					for (int i = 0; i < w; i++)
					{
						pixels[i] = ConvertPixel<DstType,SrcType>(image[i]);
					}

					if (bSwapRBForPixel64)
					{
						Pixel64* pixels64 = reinterpret_cast<Pixel64*>(pixels);

						for (int i = 0; i < w; i++)
						{
							std::swap(pixels64[i].r, pixels64[i].b);
						}
					}

					fwrite(pixels, sizeof(DstType), w, dds);
					image += w; // next row
				}

				delete[] pixels;
			}

			fclose(dds);
			return true;
		}
	}

	return false;
}

#if defined(_FREEIMAGE)
template <typename DstType> static FIBITMAP* AllocImageFreeImageInternal_T(int w, int h, FREE_IMAGE_FORMAT fif);

template <> static FIBITMAP* AllocImageFreeImageInternal_T<u8>(int w, int h, FREE_IMAGE_FORMAT)
{
	return FreeImage_Allocate(w, h, 8);
}

template <> static FIBITMAP* AllocImageFreeImageInternal_T<u16>(int w, int h, FREE_IMAGE_FORMAT fif)
{
	if (FreeImage_FIFSupportsExportType(fif, FIT_UINT16))
	{
		return FreeImage_AllocateT(FIT_UINT16, w, h, 16);
	}
	else
	{
		return NULL;
	}
}

template <> static FIBITMAP* AllocImageFreeImageInternal_T<float>(int w, int h, FREE_IMAGE_FORMAT fif)
{
	if (FreeImage_FIFSupportsExportType(fif, FIT_FLOAT))
	{
		return FreeImage_AllocateT(FIT_FLOAT, w, h, 32);
	}
	else
	{
		return NULL;
	}
}

template <> static FIBITMAP* AllocImageFreeImageInternal_T<Vec3>(int w, int h, FREE_IMAGE_FORMAT fif)
{
	if (FreeImage_FIFSupportsExportType(fif, FIT_RGBF))
	{
		return FreeImage_AllocateT(FIT_RGBF, w, h, 96);
	}
	else
	{
		return NULL;
	}
}

template <> static FIBITMAP* AllocImageFreeImageInternal_T<Vec4>(int w, int h, FREE_IMAGE_FORMAT fif)
{
	if (FreeImage_FIFSupportsExportType(fif, FIT_RGBAF))
	{
		return FreeImage_AllocateT(FIT_RGBAF, w, h, 128);
	}
	else
	{
		return NULL;
	}
}

class Pixel24 // required for saving colour images as jpg
{
public:
	Pixel24() {}
	Pixel24(int r_, int g_, int b_) : b((u8)b_), g((u8)g_), r((u8)r_) {}

	u8 b, g, r;
};

template <> __forceinline Pixel24 ConvertPixel<Pixel24,u8>(const u8& p) { return Pixel24(p, p, p); }
template <> __forceinline Pixel24 ConvertPixel<Pixel24,u16>(const u16& p) { const u8 q = ConvertPixel<u8,u16>(p); return Pixel24(q, q, q); }
template <> __forceinline Pixel24 ConvertPixel<Pixel24,float>(const float& p) { const u8 q = ConvertPixel<u8,float>(p); return Pixel24(q, q, q); }
template <> __forceinline Pixel24 ConvertPixel<Pixel24,Vec3>(const Vec3& p) { return Pixel24(ConvertPixel<u8,float>(p.x), ConvertPixel<u8,float>(p.y), ConvertPixel<u8,float>(p.z)); }
template <> __forceinline Pixel24 ConvertPixel<Pixel24,Vec4>(const Vec4& p) { return Pixel24(ConvertPixel<u8,float>(p.x), ConvertPixel<u8,float>(p.y), ConvertPixel<u8,float>(p.z)); }
template <> __forceinline Pixel24 ConvertPixel<Pixel24,Pixel32>(const Pixel32& p) { return Pixel24(p.r, p.g, p.b); }
template <> __forceinline Pixel24 ConvertPixel<Pixel24,Pixel64>(const Pixel64& p) { return Pixel24(ConvertPixel<u8,u16>(p.r), ConvertPixel<u8,u16>(p.g), ConvertPixel<u8,u16>(p.b)); }

template <> static FIBITMAP* AllocImageFreeImageInternal_T<Pixel24>(int w, int h, FREE_IMAGE_FORMAT)
{
	return FreeImage_Allocate(w, h, 24);
}

template <> static FIBITMAP* AllocImageFreeImageInternal_T<Pixel32>(int w, int h, FREE_IMAGE_FORMAT)
{
	return FreeImage_Allocate(w, h, 32);
}

template <> static FIBITMAP* AllocImageFreeImageInternal_T<Pixel64>(int w, int h, FREE_IMAGE_FORMAT fif)
{
	if (FreeImage_FIFSupportsExportType(fif, FIT_RGBA16))
	{
		return FreeImage_AllocateT(FIT_RGBA16, w, h, 64);
	}
	else
	{
		return NULL;
	}
}

template <typename DstType, typename SrcType> static bool SaveImageFreeImageInternal_T(const char* path, const SrcType* image, int w, int h, FREE_IMAGE_FORMAT fif)
{
	FIBITMAP* dib = AllocImageFreeImageInternal_T<DstType>(w, h, fif);
	bool success = false;

	if (dib)
	{
		for (int j = 0; j < h; j++)
		{
			DstType* pixels = reinterpret_cast<DstType*>(FreeImage_GetScanLine(dib, h - j - 1));

			for (int i = 0; i < w; i++)
			{
				pixels[i] = ConvertPixel<DstType,SrcType>(image[i]);
			}

			// fix inconsistency when FreeImage saves png or tif as RGBA16
			if (GetType<DstType>::T == _Pixel64 && (fif == FIF_PNG || fif == FIF_TIFF))
			{
				Pixel64* pixels64 = reinterpret_cast<Pixel64*>(pixels);

				for (int i = 0; i < w; i++)
				{
					std::swap(pixels64[i].r, pixels64[i].b);
				}
			}

			image += w; // next row
		}

		success = FreeImage_Save(fif, dib, path) != 0;

		FreeImage_Unload(dib);
	}

	return success;
}
#endif // defined(_FREEIMAGE)

template <typename DstType, typename SrcType> static bool SaveImage_T(const char* path, const SrcType* image, int w, int h, bool bAutoOpen, bool bNative)
{
	bool success = SaveImageDDS_T<DstType,SrcType>(path, image, w, h);

#if defined(_FREEIMAGE)
	if (!success)
	{
		FreeImageInit();

		const FREE_IMAGE_FORMAT fif = FreeImage_GetFIFFromFilename(path);

		if (fif == FIF_UNKNOWN)
		{
			return false; // unknown filetype
		}

		if (!FreeImage_FIFSupportsWriting(fif))
		{
			return false; // cannot save to this filetype
		}

		if (bNative)
		{
			success = SaveImageFreeImageInternal_T<DstType,SrcType>(path, image, w, h, fif);
		}

		if (!success)
		{
			if (GetType<SrcType>::N > 1)
			{
				success = SaveImageFreeImageInternal_T<Pixel32,SrcType>(path, image, w, h, fif);

				if (!success) // maybe it's a format which doesn't support alpha, like jpg
				{
					success = SaveImageFreeImageInternal_T<Pixel24,SrcType>(path, image, w, h, fif);
				}
			}
			else // scalar->greyscale
			{
				success = SaveImageFreeImageInternal_T<u8,SrcType>(path, image, w, h, fif);
			}
		}
	}
#else
	(void)bNative;
#endif

	if (success && bAutoOpen)
	{
		system(path);
	}

	return success;
}

bool SaveImage(const char* path, const u8     * image, int w, int h, bool bAutoOpen, bool bNative) { return SaveImage_T<u8     ,u8     >(path, image, w, h, bAutoOpen, bNative); }
bool SaveImage(const char* path, const u16    * image, int w, int h, bool bAutoOpen, bool bNative) { return SaveImage_T<u16    ,u16    >(path, image, w, h, bAutoOpen, bNative); }
bool SaveImage(const char* path, const float  * image, int w, int h, bool bAutoOpen, bool bNative) { return SaveImage_T<float  ,float  >(path, image, w, h, bAutoOpen, bNative); }
bool SaveImage(const char* path, const Vec3   * image, int w, int h, bool bAutoOpen, bool bNative) { return SaveImage_T<Vec4   ,Vec3   >(path, image, w, h, bAutoOpen, bNative); } // converts to Vec4
bool SaveImage(const char* path, const Vec4   * image, int w, int h, bool bAutoOpen, bool bNative) { return SaveImage_T<Vec4   ,Vec4   >(path, image, w, h, bAutoOpen, bNative); }
bool SaveImage(const char* path, const Pixel32* image, int w, int h, bool bAutoOpen, bool bNative) { return SaveImage_T<Pixel32,Pixel32>(path, image, w, h, bAutoOpen, bNative); }
bool SaveImage(const char* path, const Pixel64* image, int w, int h, bool bAutoOpen, bool bNative) { return SaveImage_T<Pixel64,Pixel64>(path, image, w, h, bAutoOpen, bNative); }

// given an array of w floats in 0-1 range, build an antialiased "graph" image of dimensions (w,h)
float* Graph(float* dst, const float* src, int w, int h)
{
	if (h == 0)
	{
		h = w/2;
	}

	for (int x = 0; x < w; x++)
	{
		float v0 = Clamp<float>(1.0f - src[Max<int>(0, x - 1)], 0.0f, 1.0f - 0.00001f)*(float)h; // [0..h]
		float v1 = Clamp<float>(1.0f - src[Min<int>(x, w - 1)], 0.0f, 1.0f - 0.00001f)*(float)h; // [0..h]
		float h0 = Min<float>(v0, v1);
		float h1 = Max<float>(v0, v1);
		float h0_floor = floorf(h0);

		int ymin = Clamp<int>((int)floorf(h0), 0, h - 1);
		int ymax = Clamp<int>((int)floorf(h1), 0, h - 1) + 1;

		float* p = &dst[x]; // pointer to column data (dst)

		for (int y = 0; y < ymin; y++) // over
		{
			*p = 1.0f, p += w;
		}

		if (ymax - ymin == 1)
		{
			const float coverage = (h1 + h0)/2.0f - h0_floor;

			*p = coverage, p += w;
		}
		else
		{
			float y0 = h0_floor + 0.0f;
			float y1 = h0_floor + 1.0f;
			float dx = 1.0f/(h1 - h0);
			float x0 = (y0 - h0)*dx;
			float x1 = (y1 - h0)*dx;

			for (int y = ymin; y < ymax; y++)
			{
				// compute coverage for pixel (x,y) - "bounds" of pixel is [0..1],[y..y+1]
				// intersect with line from (0,h0) to (1,h1)

				float coverage = 1.0f - (x1 + x0)/2.0f;

				coverage -= Max<float>(0.0f, -x0       )*(h0 - y0)/2.0f;
				coverage -= Max<float>(0.0f,  x1 - 1.0f)*(h1 - y1)/2.0f;

				*p = coverage, p += w;

				y0 = y1; y1 += 1.0f;
				x0 = x1; x1 += dx;
			}
		}

		for (int y = ymax; y < h; y++) // under
		{
			*p = 0.0f, p += w;
		}
	}

	return dst;
}

// ================================================================================================

#ifdef _DEBUG
template <typename T> static float ImageTestGetMaxDiff_T(const T* imageA, const T* imageB, int w, int h)
{
	float maxdiff = 0.0f;

	for (int i = 0; i < w*h; i++)
	{
		maxdiff = Max<float>(Abs<float>(ConvertPixel<float,T>(imageA[i]) - ConvertPixel<float,T>(imageB[i])), maxdiff);
	}

	return maxdiff;
}

template <> static float ImageTestGetMaxDiff_T<Vec3>(const Vec3* imageA, const Vec3* imageB, int w, int h)
{
	float maxdiff = 0.0f;

	for (int i = 0; i < w*h; i++)
	{
		maxdiff = Max<float>(Abs<float>(imageA[i].x - imageB[i].x), maxdiff);
		maxdiff = Max<float>(Abs<float>(imageA[i].y - imageB[i].y), maxdiff);
		maxdiff = Max<float>(Abs<float>(imageA[i].z - imageB[i].z), maxdiff);
	}

	return maxdiff;
}

template <> static float ImageTestGetMaxDiff_T<Vec4>(const Vec4* imageA, const Vec4* imageB, int w, int h)
{
	float maxdiff = 0.0f;

	for (int i = 0; i < w*h; i++)
	{
		maxdiff = Max<float>(Abs<float>(imageA[i].x - imageB[i].x), maxdiff);
		maxdiff = Max<float>(Abs<float>(imageA[i].y - imageB[i].y), maxdiff);
		maxdiff = Max<float>(Abs<float>(imageA[i].z - imageB[i].z), maxdiff);
		maxdiff = Max<float>(Abs<float>(imageA[i].w - imageB[i].w), maxdiff);
	}

	return maxdiff;
}

template <> static float ImageTestGetMaxDiff_T<Pixel32>(const Pixel32* imageA, const Pixel32* imageB, int w, int h)
{
	float maxdiff = 0.0f;

	for (int i = 0; i < w*h; i++)
	{
		maxdiff = Max<float>(Abs<float>(ConvertPixel<float,u8>(imageA[i].r) - ConvertPixel<float,u8>(imageB[i].r)), maxdiff);
		maxdiff = Max<float>(Abs<float>(ConvertPixel<float,u8>(imageA[i].g) - ConvertPixel<float,u8>(imageB[i].g)), maxdiff);
		maxdiff = Max<float>(Abs<float>(ConvertPixel<float,u8>(imageA[i].b) - ConvertPixel<float,u8>(imageB[i].b)), maxdiff);
		maxdiff = Max<float>(Abs<float>(ConvertPixel<float,u8>(imageA[i].a) - ConvertPixel<float,u8>(imageB[i].a)), maxdiff);
	}

	return maxdiff;
}

template <> static float ImageTestGetMaxDiff_T<Pixel64>(const Pixel64* imageA, const Pixel64* imageB, int w, int h)
{
	float maxdiff = 0.0f;

	for (int i = 0; i < w*h; i++)
	{
		maxdiff = Max<float>(Abs<float>(ConvertPixel<float,u16>(imageA[i].r) - ConvertPixel<float,u16>(imageB[i].r)), maxdiff);
		maxdiff = Max<float>(Abs<float>(ConvertPixel<float,u16>(imageA[i].g) - ConvertPixel<float,u16>(imageB[i].g)), maxdiff);
		maxdiff = Max<float>(Abs<float>(ConvertPixel<float,u16>(imageA[i].b) - ConvertPixel<float,u16>(imageB[i].b)), maxdiff);
		maxdiff = Max<float>(Abs<float>(ConvertPixel<float,u16>(imageA[i].a) - ConvertPixel<float,u16>(imageB[i].a)), maxdiff);
	}

	return maxdiff;
}

void ImageTest()
{
	const char* exts[] = {"dds", "png", "tga", "tif", "bmp", "jpg"};

	// test scalar formats
	{
		const int w = 256;
		const int h = 256;

		u8* test_u8 = rage_new u8[w*h];
		u16* test_u16 = rage_new u16[w*h];
		float* test_float = rage_new float[w*h];

		for (int j = 0; j < h; j++)
		{
			for (int i = 0; i < w; i++)
			{
				const float f = (float)i/(float)(w - 1);

				test_u8[i + j*w] = ConvertPixel<u8,float>(f);
				test_u16[i + j*w] = ConvertPixel<u16,float>(f);
				test_float[i + j*w] = f;
			}
		}

		for (int i = 0; i < NELEM(exts); i++)
		{
			char path_u8[64] = "";
			char path_u16[64] = "";
			char path_float[64] = "";

			sprintf(path_u8, "test_u8.%s", exts[i]);
			sprintf(path_u16, "test_u16.%s", exts[i]);
			sprintf(path_float, "test_float.%s", exts[i]);

			SaveImage(path_u8, test_u8, w, h, false, true);
			SaveImage(path_u16, test_u16, w, h, false, true);
			SaveImage(path_float, test_float, w, h, false, true);

			int w_ = w;
			int h_ = h;
			u8* test2_u8 = LoadImage_u8(path_u8, w_, h_);
			u16* test2_u16 = LoadImage_u16(path_u16, w_, h_);
			float* test2_float = LoadImage_float(path_float, w_, h_);

			const float maxdiff_u8 = ImageTestGetMaxDiff_T<u8>(test_u8, test2_u8, w, h);
			const float maxdiff_u16 = ImageTestGetMaxDiff_T<u16>(test_u16, test2_u16, w, h);
			const float maxdiff_float = ImageTestGetMaxDiff_T<float>(test_float, test2_float, w, h);

			printf("%f maxdiff %s[u8]\n", maxdiff_u8, exts[i]);
			printf("%f maxdiff %s[u16]\n", maxdiff_u16, exts[i]);
			printf("%f maxdiff %s[float]\n", maxdiff_float, exts[i]);

			delete[] test2_u8;
			delete[] test2_u16;
			delete[] test2_float;
		}

		delete[] test_u8;
		delete[] test_u16;
		delete[] test_float;		
	}

	// test vector formats
	{
		const int w = 256;
		const int h = 256;

		Vec3* test_Vec3 = rage_new Vec3[w*h];
		Vec4* test_Vec4 = rage_new Vec4[w*h];
		Pixel32* test_Pixel32 = rage_new Pixel32[w*h];
		Pixel64* test_Pixel64 = rage_new Pixel64[w*h];

		for (int j = 0; j < h; j++)
		{
			for (int i = 0; i < w; i++)
			{
				const float fi = (float)i/(float)(w - 1);
				const float fj = (float)j/(float)(h - 1);
				const float fk = Max<float>(0.0f, 1.0f - sqrtf((2.0f*fi - 1.0f)*(2.0f*fi - 1.0f) + (2.0f*fj - 1.0f)*(2.0f*fj - 1.0f)));

				test_Vec3[i + j*w] = Vec3(fi, fj, fk);
				test_Vec4[i + j*w] = Vec4(fi, fj, fk, 1.0f);
				test_Pixel32[i + j*w] = ConvertPixel<Pixel32,Vec3>(Vec3(fi, fj, fk));
				test_Pixel64[i + j*w] = ConvertPixel<Pixel64,Vec3>(Vec3(fi, fj, fk));
			}
		}

		for (int i = 0; i < NELEM(exts); i++)
		{
			char path_Vec3[64] = "";
			char path_Vec4[64] = "";
			char path_Pixel32[64] = "";
			char path_Pixel64[64] = "";

			sprintf(path_Vec3, "test_Vec3.%s", exts[i]);
			sprintf(path_Vec4, "test_Vec4.%s", exts[i]);
			sprintf(path_Pixel32, "test_Pixel32.%s", exts[i]);
			sprintf(path_Pixel64, "test_Pixel64.%s", exts[i]);

			SaveImage(path_Vec3, test_Vec3, w, h, false, true);
			SaveImage(path_Vec4, test_Vec4, w, h, false, true);
			SaveImage(path_Pixel32, test_Pixel32, w, h, false, true);
			SaveImage(path_Pixel64, test_Pixel64, w, h, false, true);

			int w_ = w;
			int h_ = h;
			Vec3* test2_Vec3 = LoadImage_Vec3(path_Vec3, w_, h_);
			Vec4* test2_Vec4 = LoadImage_Vec4(path_Vec4, w_, h_);
			Pixel32* test2_Pixel32 = LoadImage_Pixel32(path_Pixel32, w_, h_);
			Pixel64* test2_Pixel64 = LoadImage_Pixel64(path_Pixel64, w_, h_);

			const float maxdiff_Vec3 = ImageTestGetMaxDiff_T<Vec3>(test_Vec3, test2_Vec3, w, h);
			const float maxdiff_Vec4 = ImageTestGetMaxDiff_T<Vec4>(test_Vec4, test2_Vec4, w, h);
			const float maxdiff_Pixel32 = ImageTestGetMaxDiff_T<Pixel32>(test_Pixel32, test2_Pixel32, w, h);
			const float maxdiff_Pixel64 = ImageTestGetMaxDiff_T<Pixel64>(test_Pixel64, test2_Pixel64, w, h);

			printf("%f maxdiff %s[Vec3]\n", maxdiff_Vec3, exts[i]);
			printf("%f maxdiff %s[Vec4]\n", maxdiff_Vec4, exts[i]);
			printf("%f maxdiff %s[Pixel32]\n", maxdiff_Pixel32, exts[i]);
			printf("%f maxdiff %s[Pixel64]\n", maxdiff_Pixel64, exts[i]);

			delete[] test2_Vec3;
			delete[] test2_Vec4;
			delete[] test2_Pixel32;
			delete[] test2_Pixel64;
		}

		delete[] test_Vec3;
		delete[] test_Vec4;
		delete[] test_Pixel32;
		delete[] test_Pixel64;
	}
}
#endif // _DEBUG
