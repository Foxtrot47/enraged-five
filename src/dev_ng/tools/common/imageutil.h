// ===========
// imageutil.h
// ===========

#ifndef _COMMON_IMAGEUTIL_H_
#define _COMMON_IMAGEUTIL_H_

#include "common.h"

class Vec3;
class Vec4;
class Pixel32;
class Pixel64;

bool GetImageDimensions(const char* path, int& w, int& h);

u8     * LoadImage_u8     (const char* path, int& w, int& h, int sampleIndex = -1);
u16    * LoadImage_u16    (const char* path, int& w, int& h, int sampleIndex = -1);
float  * LoadImage_float  (const char* path, int& w, int& h, int sampleIndex = -1);
Vec3   * LoadImage_Vec3   (const char* path, int& w, int& h, int sampleIndex = -1);
Vec4   * LoadImage_Vec4   (const char* path, int& w, int& h, int sampleIndex = -1);
Pixel32* LoadImage_Pixel32(const char* path, int& w, int& h, int sampleIndex = -1);
Pixel64* LoadImage_Pixel64(const char* path, int& w, int& h, int sampleIndex = -1);

#define LoadImage LoadImage_float

u8     * LoadImageDDSSpan_u8     (const char* path, int w, int y0, int h);
u16    * LoadImageDDSSpan_u16    (const char* path, int w, int y0, int h);
float  * LoadImageDDSSpan_float  (const char* path, int w, int y0, int h);
Vec3   * LoadImageDDSSpan_Vec3   (const char* path, int w, int y0, int h);
Vec4   * LoadImageDDSSpan_Vec4   (const char* path, int w, int y0, int h);
Pixel32* LoadImageDDSSpan_Pixel32(const char* path, int w, int y0, int h);
Pixel64* LoadImageDDSSpan_Pixel64(const char* path, int w, int y0, int h);

bool SaveImage(const char* path, const u8     * image, int w, int h, bool bAutoOpen = false, bool bNative = false);
bool SaveImage(const char* path, const u16    * image, int w, int h, bool bAutoOpen = false, bool bNative = false);
bool SaveImage(const char* path, const float  * image, int w, int h, bool bAutoOpen = false, bool bNative = false);
bool SaveImage(const char* path, const Vec3   * image, int w, int h, bool bAutoOpen = false, bool bNative = false);
bool SaveImage(const char* path, const Vec4   * image, int w, int h, bool bAutoOpen = false, bool bNative = false);
bool SaveImage(const char* path, const Pixel32* image, int w, int h, bool bAutoOpen = false, bool bNative = false);
bool SaveImage(const char* path, const Pixel64* image, int w, int h, bool bAutoOpen = false, bool bNative = false);

template <typename DstType, typename SrcType> inline DstType* ConvertImage(const SrcType* image, int w, int h)
{
	DstType* dst = new DstType[w*h];

	for (int i = 0; i < w*h; i++)
	{
		dst[i] = ConvertPixel<DstType,SrcType>(image[i]);
	}

	return dst;
}

float* Graph(float* dst, const float* src, int w, int h);

#ifdef _DEBUG
void ImageTest();
#endif // _DEBUG

#endif // _COMMON_IMAGEUTIL_H_
