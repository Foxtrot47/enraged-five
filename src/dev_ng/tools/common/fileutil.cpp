// ============
// fileutil.cpp
// ============

#include "fileutil.h"

void PrintDirectoryPaths()
{
	fprintf(stdout, "RS_PROJECT = %s\n", RS_PROJECT);
	fprintf(stdout, "RS_BRANCHSUFFIX = %s\n", RS_BRANCHSUFFIX);
	fprintf(stdout, "RS_PROJROOT = %s\n", RS_PROJROOT);
	fprintf(stdout, "RS_BUILDROOT = %s\n", RS_BUILDROOT);
	fprintf(stdout, "RS_BUILDBRANCH = %s\n", RS_BUILDBRANCH);
	fprintf(stdout, "RS_CODEBRANCH = %s\n", RS_CODEBRANCH);
	fprintf(stdout, "RS_ASSETS = %s\n", RS_ASSETS);
	fprintf(stdout, "RS_TOOLSROOT = %s\n", RS_TOOLSROOT);

	fprintf(stdout, "RS_COMMON = %s\n", RS_COMMON);
	fprintf(stdout, "RS_LEVELS = %s\n", RS_LEVELS);
}

void BuildSearchList(std::vector<std::string>& files, const char* path, const char* ext1, const char* ext2)
{
	char cmd[1024] = "";
	sprintf(cmd, "dir $S $B %s > %s", path, RS_TEMPFILE);

	for (char* s = cmd; *s; s++)
	{
		if (*s == '/') { *s = '\\'; }
		else if (*s == '$') { *s = '/'; }
	}

	system(cmd);

	FILE* fp = fopen(RS_TEMPFILE, "r");

	if (fp)
	{
		char line[1024] = "";

		while (ReadFileLine(line, sizeof(line), fp))
		{
			for (char* s = line; *s; s++) { if (*s == '\\') { *s = '/'; } }
			_strlwr(line);

			const char* ext = strrchr(line, '.');

			if (ext)
			{
				if ((ext1 && strcmp(ext, ext1) == 0) ||
					(ext2 && strcmp(ext, ext2) == 0) ||
					(ext1 == NULL && ext2 == NULL))
				{
					files.push_back(line);
				}
			}
		}
	}
}

bool FileExists(const char* path)
{
	FILE* fp = fopen(path, "rb");

	if (fp)
	{
		fclose(fp);
		return true;
	}

	return false;
}

bool FileExistsAndIsNotZeroBytes(const char* path)
{
	FILE* fp = fopen(path, "rb");

	if (fp)
	{
		fseek(fp, 0, SEEK_END);
		const int size = ftell(fp);
		fclose(fp);
		return size > 0;
	}

	return false;
}

bool ReadFileLine(char* line, int lineMax, FILE* fp)
{
	if (fgets(line, lineMax, fp))
	{
		char temp[4096] = "";
		strcpy(temp, line);
		const char* start = temp;
		while (*start == ' ' || *start == '\t')
		{
			start++; // skip leading whitespace
		}
		strcpy(line, start);
		if (strrchr(line, '\r')) { strrchr(line, '\r')[0] = '\0'; } // remove trailing newline
		if (strrchr(line, '\n')) { strrchr(line, '\n')[0] = '\0'; }
		return true;
	}

	return false;
}

bool ReadNextFileLine(char* line, int lineMax, FILE* fp)
{
	while (ReadFileLine(line, lineMax, fp))
	{
		if (line[0] != '#')
		{
			return true;
		}
	}

	return false;
}

// %RS_CODEBRANCH%\rage\base\src\file\stream.cpp
int rage_fgetline(char* dest, int maxSize, FILE* S)
{
	int stored;
	do {
		if (fgets(dest,maxSize,S))
			stored = (int)strlen(dest);
		else
			stored = 0;
		if (stored == 0)
			break; // real EOF
		while (stored && dest[stored-1] <= 32)
			--stored;
		dest[stored] = 0;
		// consume empty lines:
	} while (!stored);
	return stored;
}

void ByteSwapData(void* data, int dataSize, int swapSize)
{
	u8* data0 = (u8*)data;
	u8* data1 = data0 + dataSize;

	if (swapSize == 2)
	{
		for (; data0 < data1; data0 += 2)
		{
			const u8 swap[2] = {data0[0], data0[1]};

			data0[0] = swap[1];
			data0[1] = swap[0];
		}
	}
	else if (swapSize == 4)
	{
		for (; data0 < data1; data0 += 4)
		{
			const u8 swap[4] = {data0[0], data0[1], data0[2], data0[3]};

			data0[0] = swap[3];
			data0[1] = swap[2];
			data0[2] = swap[1];
			data0[3] = swap[0];
		}
	}
	else if (swapSize == 8)
	{
		for (; data0 < data1; data0 += 8)
		{
			const u8 swap[8] = {data0[0], data0[1], data0[2], data0[3], data0[4], data0[5], data0[6], data0[7]};

			data0[0] = swap[7];
			data0[1] = swap[6];
			data0[2] = swap[5];
			data0[3] = swap[4];
			data0[4] = swap[3];
			data0[5] = swap[2];
			data0[6] = swap[1];
			data0[7] = swap[0];
		}
	}
}
