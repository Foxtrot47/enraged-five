#include "dds.h"

static bool CompareDDSFormatMask(const DDPIXELFORMAT& ddpf, u32 flags, u32 bitCount, u32 rMask, u32 gMask, u32 bMask, u32 aMask)
{
	return
	(
		ddpf.dwFlags       == flags    &&
		ddpf.dwRGBBitCount == bitCount &&
		ddpf.dwRBitMask    == rMask    &&
		ddpf.dwGBitMask    == gMask    &&
		ddpf.dwBBitMask    == bMask    &&
		ddpf.dwABitMask    == aMask
	);
}

DDS_D3DFORMAT GetDDSFormat(const DDPIXELFORMAT& ddpf)
{
	if (ddpf.dwFlags & DDPF_FOURCC)
	{
		if (ddpf.dwFourCC == MAKE_MAGIC_NUMBER('B','C','4','U')) { return DDS_D3DFMT_DXT5A; }
		if (ddpf.dwFourCC == MAKE_MAGIC_NUMBER('B','C','5','U')) { return DDS_D3DFMT_DXN; }

		return (DDS_D3DFORMAT)ddpf.dwFourCC;
	}
	else if (CompareDDSFormatMask(ddpf, DDPF_RGB       | DDPF_ALPHAPIXELS, 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000)) { return DDS_D3DFMT_A8R8G8B8   ; }
	else if (CompareDDSFormatMask(ddpf, DDPF_RGB       | DDPF_ALPHAPIXELS, 32, 0x000000FF, 0x0000FF00, 0x00FF0000, 0xFF000000)) { return DDS_D3DFMT_A8B8G8R8   ; }
	else if (CompareDDSFormatMask(ddpf, DDPF_ALPHA                       ,  8, 0x00000000, 0x00000000, 0x00000000, 0x000000FF)) { return DDS_D3DFMT_A8         ; }
	else if (CompareDDSFormatMask(ddpf, DDPF_LUMINANCE                   ,  8, 0x000000FF, 0x00000000, 0x00000000, 0x00000000)) { return DDS_D3DFMT_L8         ; }
	else if (CompareDDSFormatMask(ddpf, DDPF_LUMINANCE | DDPF_ALPHAPIXELS, 16, 0x000000FF, 0x00000000, 0x00000000, 0x0000FF00)) { return DDS_D3DFMT_A8L8       ; }
	else if (CompareDDSFormatMask(ddpf, DDPF_RGB       | DDPF_ALPHAPIXELS, 16, 0x00000F00, 0x000000F0, 0x0000000F, 0x0000F000)) { return DDS_D3DFMT_A4R4G4B4   ; }
	else if (CompareDDSFormatMask(ddpf, DDPF_RGB       | DDPF_ALPHAPIXELS, 16, 0x00007C00, 0x000003E0, 0x0000001F, 0x00008000)) { return DDS_D3DFMT_A1R5G5B5   ; }
	else if (CompareDDSFormatMask(ddpf, DDPF_RGB                         , 16, 0x0000F800, 0x000007E0, 0x0000001F, 0x00000000)) { return DDS_D3DFMT_R5G6B5     ; }
	else if (CompareDDSFormatMask(ddpf, DDPF_RGB                         ,  8, 0x000000E0, 0x0000001C, 0x00000003, 0x0000FF00)) { return DDS_D3DFMT_R3G3B2     ; }
	else if (CompareDDSFormatMask(ddpf, DDPF_RGB       | DDPF_ALPHAPIXELS, 16, 0x000000E0, 0x0000001C, 0x00000003, 0x0000FF00)) { return DDS_D3DFMT_A8R3G3B2   ; }
	else if (CompareDDSFormatMask(ddpf, DDPF_LUMINANCE | DDPF_ALPHAPIXELS,  8, 0x0000000F, 0x00000000, 0x00000000, 0x000000F0)) { return DDS_D3DFMT_A4L4       ; }
	else if (CompareDDSFormatMask(ddpf, DDPF_RGB       | DDPF_ALPHAPIXELS, 32, 0x3FF00000, 0x000FFC00, 0x000003FF, 0xC0000000)) { return DDS_D3DFMT_A2R10G10B10; }
	else if (CompareDDSFormatMask(ddpf, DDPF_RGB       | DDPF_ALPHAPIXELS, 32, 0x000003FF, 0x000FFC00, 0x3FF00000, 0xC0000000)) { return DDS_D3DFMT_A2B10G10R10; }
	else if (CompareDDSFormatMask(ddpf, DDPF_RGB                         , 32, 0x0000FFFF, 0xFFFF0000, 0x00000000, 0x00000000)) { return DDS_D3DFMT_G16R16     ; }
	else if (CompareDDSFormatMask(ddpf, DDPF_LUMINANCE                   , 16, 0x0000FFFF, 0x00000000, 0x00000000, 0x00000000)) { return DDS_D3DFMT_L16        ; }
	// support XRGB/XBGR formats too ..
	else if (CompareDDSFormatMask(ddpf, DDPF_RGB                         , 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0x00000000)) { return DDS_D3DFMT_A8R8G8B8   ; } // X8R8G8B8
	else if (CompareDDSFormatMask(ddpf, DDPF_RGB                         , 32, 0x000000FF, 0x0000FF00, 0x00FF0000, 0x00000000)) { return DDS_D3DFMT_A8B8G8R8   ; } // X8B8G8R8
	else if (CompareDDSFormatMask(ddpf, DDPF_RGB                         , 16, 0x00000F00, 0x000000F0, 0x0000000F, 0x00000000)) { return DDS_D3DFMT_A4R4G4B4   ; } // X4R4G4B4
	else if (CompareDDSFormatMask(ddpf, DDPF_RGB                         , 16, 0x00007C00, 0x000003E0, 0x0000001F, 0x00000000)) { return DDS_D3DFMT_A1R5G5B5   ; } // X1R5G5B5

	return DDS_D3DFMT_UNKNOWN;
}

const char* GetD3DFormatStr(DDS_D3DFORMAT format)
{
	switch ((int)format)
	{
#define DEF_DDS_D3DFMT_CASE(f) case f: return #f; break
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_UNKNOWN            );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_R8G8B8             );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_A8R8G8B8           );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_X8R8G8B8           );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_R5G6B5             );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_X1R5G5B5           );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_A1R5G5B5           );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_A4R4G4B4           );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_R3G3B2             );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_A8                 );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_A8R3G3B2           );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_X4R4G4B4           );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_A2B10G10R10        );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_A8B8G8R8           );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_X8B8G8R8           );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_G16R16             );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_A2R10G10B10        );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_A16B16G16R16       );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_A8P8               );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_P8                 );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_L8                 );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_A8L8               );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_A4L4               );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_V8U8               );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_L6V5U5             );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_X8L8V8U8           );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_Q8W8V8U8           );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_V16U16             );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_A2W10V10U10        );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_UYVY               );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_R8G8_B8G8          );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_YUY2               );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_G8R8_G8B8          );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_DXT1               );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_DXT2               );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_DXT3               );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_DXT4               );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_DXT5               );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_DXT5A              );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_DXN                );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_BC6                );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_BC7                );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_R8                 );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_R16                );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_G8R8               );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_D16_LOCKABLE       );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_D32                );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_D15S1              );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_D24S8              );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_D24X8              );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_D24X4S4            );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_D16                );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_D32F_LOCKABLE      );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_D24FS8             );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_D32_LOCKABLE       );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_S8_LOCKABLE        );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_L16                );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_VERTEXDATA         );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_INDEX16            );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_INDEX32            );
#undef DEF_DDS_D3DFMT_CASE
	}

	return "DDS_D3DFMT_";
}

DXGI_FORMAT GetDX10Format(const DDPIXELFORMAT& ddpf)
{
	switch (ddpf.dwFourCC)
	{
	case DDS_D3DFMT_A8R8G8B8            : return DXGI_FORMAT_B8G8R8A8_UNORM            ;
	case DDS_D3DFMT_X8R8G8B8            : return DXGI_FORMAT_B8G8R8X8_UNORM            ;
	case DDS_D3DFMT_A8B8G8R8            : return DXGI_FORMAT_R8G8B8A8_UNORM            ;
	case DDS_D3DFMT_X8B8G8R8            : return DXGI_FORMAT_R8G8B8A8_UNORM            ; // close enough (should be R8G8B8X8_UNORM)
	case DDS_D3DFMT_R5G6B5              : return DXGI_FORMAT_B5G6R5_UNORM              ;
	case DDS_D3DFMT_X1R5G5B5            : return DXGI_FORMAT_B5G5R5A1_UNORM            ; // close enough (should be B5G5R5X1_UNORM)
	case DDS_D3DFMT_A1R5G5B5            : return DXGI_FORMAT_B5G5R5A1_UNORM            ;
	case DDS_D3DFMT_A4R4G4B4            : return DXGI_FORMAT_B4G4R4A4_UNORM            ;
	case DDS_D3DFMT_R3G3B2              : return DXGI_FORMAT_UNKNOWN                   ;
	case DDS_D3DFMT_A8                  : return DXGI_FORMAT_A8_UNORM                  ;
	case DDS_D3DFMT_A8R3G3B2            : return DXGI_FORMAT_UNKNOWN                   ;
	case DDS_D3DFMT_X4R4G4B4            : return DXGI_FORMAT_B4G4R4A4_UNORM            ; // close enough (should be B4G4R4X4_UNORM)
	case DDS_D3DFMT_A2B10G10R10         : return DXGI_FORMAT_R10G10B10A2_UNORM         ;
	case DDS_D3DFMT_A2R10G10B10         : return DXGI_FORMAT_R10G10B10A2_UNORM         ; // meh .. closest i guess
	case DDS_D3DFMT_G16R16              : return DXGI_FORMAT_R16G16_UNORM              ;
	case DDS_D3DFMT_A16B16G16R16        : return DXGI_FORMAT_R16G16B16A16_UNORM        ;
	case DDS_D3DFMT_L8                  : return DXGI_FORMAT_R8_UNORM                  ; // meh .. closest i guess
	case DDS_D3DFMT_A8L8                : return DXGI_FORMAT_R8G8_UNORM                ; // meh .. closest i guess
	case DDS_D3DFMT_A4L4                : return DXGI_FORMAT_UNKNOWN                   ;
	case DDS_D3DFMT_R8G8_B8G8           : return DXGI_FORMAT_R8G8_B8G8_UNORM           ;
	case DDS_D3DFMT_G8R8_G8B8           : return DXGI_FORMAT_G8R8_G8B8_UNORM           ;
	case DDS_D3DFMT_DXT1                : return DXGI_FORMAT_BC1_UNORM                 ;
	case DDS_D3DFMT_DXT2                : return DXGI_FORMAT_BC2_UNORM                 ; // close enough
	case DDS_D3DFMT_DXT3                : return DXGI_FORMAT_BC2_UNORM                 ;
	case DDS_D3DFMT_DXT4                : return DXGI_FORMAT_BC3_UNORM                 ; // close enough
	case DDS_D3DFMT_DXT5                : return DXGI_FORMAT_BC3_UNORM                 ;
	case DDS_D3DFMT_DXT5A               : return DXGI_FORMAT_BC4_UNORM                 ;
	case DDS_D3DFMT_DXN                 : return DXGI_FORMAT_BC5_UNORM                 ;
	case DDS_D3DFMT_R16F                : return DXGI_FORMAT_R16_FLOAT                 ;
	case DDS_D3DFMT_G16R16F             : return DXGI_FORMAT_R16G16_FLOAT              ;
	case DDS_D3DFMT_A16B16G16R16F       : return DXGI_FORMAT_R16G16B16A16_FLOAT        ;
	case DDS_D3DFMT_R32F                : return DXGI_FORMAT_R32_FLOAT                 ;
	case DDS_D3DFMT_G32R32F             : return DXGI_FORMAT_R32G32_FLOAT              ;
	case DDS_D3DFMT_A32B32G32R32F       : return DXGI_FORMAT_R32G32B32A32_FLOAT        ;
	case DDS_D3DFMT_A1                  : return DXGI_FORMAT_UNKNOWN                   ;
	case DDS_D3DFMT_A2B10G10R10_XR_BIAS : return DXGI_FORMAT_R10G10B10_XR_BIAS_A2_UNORM;
	// signed formats ..
	case DDS_D3DFMT_V8U8                : return DXGI_FORMAT_R8G8_SNORM                ;
	case DDS_D3DFMT_X8L8V8U8            : return DXGI_FORMAT_R8G8B8A8_SNORM            ; // close enough (should be R8G8B8X8_SNORM)
	case DDS_D3DFMT_Q8W8V8U8            : return DXGI_FORMAT_R8G8B8A8_SNORM            ;
	case DDS_D3DFMT_V16U16              : return DXGI_FORMAT_R16G16_SNORM              ;
	case DDS_D3DFMT_A2W10V10U10         : return DXGI_FORMAT_R10G10B10A2_UNORM         ; // meh .. closest i guess
	case DDS_D3DFMT_Q16W16V16U16        : return DXGI_FORMAT_R16G16B16A16_SNORM        ;
	}

	if (ddpf.dwFourCC == MAKE_MAGIC_NUMBER('B','C','4','U')) { return DXGI_FORMAT_BC4_UNORM; }
	if (ddpf.dwFourCC == MAKE_MAGIC_NUMBER('B','C','5','U')) { return DXGI_FORMAT_BC5_UNORM; }

	if (CompareDDSFormatMask(ddpf, DDPF_RGB       | DDPF_ALPHAPIXELS, 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000)) { return DXGI_FORMAT_B8G8R8A8_UNORM   ; }
	if (CompareDDSFormatMask(ddpf, DDPF_RGB       | DDPF_ALPHAPIXELS, 32, 0x000000FF, 0x0000FF00, 0x00FF0000, 0xFF000000)) { return DXGI_FORMAT_R8G8B8A8_UNORM   ; }
	if (CompareDDSFormatMask(ddpf, DDPF_ALPHA                       ,  8, 0x00000000, 0x00000000, 0x00000000, 0x000000FF)) { return DXGI_FORMAT_A8_UNORM         ; }
	if (CompareDDSFormatMask(ddpf, DDPF_LUMINANCE                   ,  8, 0x000000FF, 0x00000000, 0x00000000, 0x00000000)) { return DXGI_FORMAT_R8_UNORM         ; } // meh .. closest i guess
	if (CompareDDSFormatMask(ddpf, DDPF_LUMINANCE | DDPF_ALPHAPIXELS, 16, 0x000000FF, 0x00000000, 0x00000000, 0x0000FF00)) { return DXGI_FORMAT_R8G8_UNORM       ; } // meh .. closest i guess
	if (CompareDDSFormatMask(ddpf, DDPF_RGB       | DDPF_ALPHAPIXELS, 16, 0x00000F00, 0x000000F0, 0x0000000F, 0x0000F000)) { return DXGI_FORMAT_B4G4R4A4_UNORM   ; }
	if (CompareDDSFormatMask(ddpf, DDPF_RGB       | DDPF_ALPHAPIXELS, 16, 0x00007C00, 0x000003E0, 0x0000001F, 0x00008000)) { return DXGI_FORMAT_B5G5R5A1_UNORM   ; }
	if (CompareDDSFormatMask(ddpf, DDPF_RGB                         , 16, 0x0000F800, 0x000007E0, 0x0000001F, 0x00000000)) { return DXGI_FORMAT_B5G6R5_UNORM     ; }
	if (CompareDDSFormatMask(ddpf, DDPF_RGB                         ,  8, 0x000000E0, 0x0000001C, 0x00000003, 0x0000FF00)) { return DXGI_FORMAT_UNKNOWN          ; } // 332 not supported
	if (CompareDDSFormatMask(ddpf, DDPF_RGB       | DDPF_ALPHAPIXELS, 16, 0x000000E0, 0x0000001C, 0x00000003, 0x0000FF00)) { return DXGI_FORMAT_UNKNOWN          ; } // 8332 not supported
	if (CompareDDSFormatMask(ddpf, DDPF_LUMINANCE | DDPF_ALPHAPIXELS,  8, 0x0000000F, 0x00000000, 0x00000000, 0x000000F0)) { return DXGI_FORMAT_UNKNOWN          ; } // 44 not supported
	if (CompareDDSFormatMask(ddpf, DDPF_RGB       | DDPF_ALPHAPIXELS, 32, 0x3FF00000, 0x000FFC00, 0x000003FF, 0xC0000000)) { return DXGI_FORMAT_R10G10B10A2_UNORM; } // meh .. closest i guess
	if (CompareDDSFormatMask(ddpf, DDPF_RGB       | DDPF_ALPHAPIXELS, 32, 0x000003FF, 0x000FFC00, 0x3FF00000, 0xC0000000)) { return DXGI_FORMAT_R10G10B10A2_UNORM; }
	if (CompareDDSFormatMask(ddpf, DDPF_RGB                         , 32, 0x0000FFFF, 0xFFFF0000, 0x00000000, 0x00000000)) { return DXGI_FORMAT_R16G16_UNORM     ; }
	if (CompareDDSFormatMask(ddpf, DDPF_LUMINANCE                   , 16, 0x0000FFFF, 0x00000000, 0x00000000, 0x00000000)) { return DXGI_FORMAT_R16_UNORM        ; } // meh .. closest i guess
	if (CompareDDSFormatMask(ddpf, DDPF_RGB                         ,  8, 0x000000FF, 0x00000000, 0x00000000, 0x00000000)) { return DXGI_FORMAT_R8_UNORM         ; }
	if (CompareDDSFormatMask(ddpf, DDPF_RGB                         , 16, 0x0000FFFF, 0x00000000, 0x00000000, 0x00000000)) { return DXGI_FORMAT_R16_UNORM        ; }
	if (CompareDDSFormatMask(ddpf, DDPF_RGB                         , 16, 0x000000FF, 0x0000FF00, 0x00000000, 0x00000000)) { return DXGI_FORMAT_R8G8_UNORM       ; }
	// support XRGB/XBGR formats too ..
	if (CompareDDSFormatMask(ddpf, DDPF_RGB                         , 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0x00000000)) { return DXGI_FORMAT_B8G8R8X8_UNORM   ; }
	if (CompareDDSFormatMask(ddpf, DDPF_RGB                         , 32, 0x000000FF, 0x0000FF00, 0x00FF0000, 0x00000000)) { return DXGI_FORMAT_R8G8B8A8_UNORM   ; } // close enough (should be R8G8B8X8)
	if (CompareDDSFormatMask(ddpf, DDPF_RGB                         , 16, 0x00000F00, 0x000000F0, 0x0000000F, 0x00000000)) { return DXGI_FORMAT_B4G4R4A4_UNORM   ; } // close enough (should be B4G4R4X4)
	if (CompareDDSFormatMask(ddpf, DDPF_RGB                         , 16, 0x00007C00, 0x000003E0, 0x0000001F, 0x00000000)) { return DXGI_FORMAT_B5G5R5A1_UNORM   ; } // close enough (should be B5G5R5X1)
	if (CompareDDSFormatMask(ddpf, DDPF_RGB                         , 32, 0x3FF00000, 0x000FFC00, 0x000003FF, 0xC0000000)) { return DXGI_FORMAT_R10G10B10A2_UNORM; } // meh .. closest i guess
	if (CompareDDSFormatMask(ddpf, DDPF_RGB                         , 32, 0x000003FF, 0x000FFC00, 0x3FF00000, 0xC0000000)) { return DXGI_FORMAT_R10G10B10A2_UNORM; } // close enough (should be R10G10B10X2)

	return DXGI_FORMAT_UNKNOWN;
}

static DDPIXELFORMAT MakeDDSPixelFormat(u32 flags, u32 bits, u32 rmask, u32 gmask, u32 bmask, u32 amask, u32 fourCC = 0)
{
	DDPIXELFORMAT ddpf;
	memset(&ddpf, 0, sizeof(ddpf));
	ddpf.dwSize = sizeof(ddpf);
	ddpf.dwFlags = flags;
	ddpf.dwRGBBitCount = bits;
	ddpf.dwRBitMask = rmask;
	ddpf.dwGBitMask = gmask;
	ddpf.dwBBitMask = bmask;
	ddpf.dwABitMask = amask;
	ddpf.dwFourCC = fourCC;
	return ddpf;
}

DDPIXELFORMAT GetDDSPixelFormatFromDX10Format(DXGI_FORMAT dxgiFormat)
{
	switch (dxgiFormat)
	{
	case DXGI_FORMAT_B8G8R8A8_UNORM            : return MakeDDSPixelFormat(DDPF_RGB       | DDPF_ALPHAPIXELS, 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);
	case DXGI_FORMAT_R8G8B8A8_UNORM            : return MakeDDSPixelFormat(DDPF_RGB       | DDPF_ALPHAPIXELS, 32, 0x000000FF, 0x0000FF00, 0x00FF0000, 0xFF000000);
	case DXGI_FORMAT_A8_UNORM                  : return MakeDDSPixelFormat(DDPF_ALPHA                       ,  8, 0x00000000, 0x00000000, 0x00000000, 0x000000FF);
	case DXGI_FORMAT_R8_UNORM                  : return MakeDDSPixelFormat(DDPF_LUMINANCE                   ,  8, 0x000000FF, 0x00000000, 0x00000000, 0x00000000); // meh .. closest i guess
	case DXGI_FORMAT_R8G8_UNORM                : return MakeDDSPixelFormat(DDPF_LUMINANCE | DDPF_ALPHAPIXELS, 16, 0x000000FF, 0x00000000, 0x00000000, 0x0000FF00); // meh .. closest i guess
	case DXGI_FORMAT_B4G4R4A4_UNORM            : return MakeDDSPixelFormat(DDPF_RGB       | DDPF_ALPHAPIXELS, 16, 0x00000F00, 0x000000F0, 0x0000000F, 0x0000F000);
	case DXGI_FORMAT_B5G5R5A1_UNORM            : return MakeDDSPixelFormat(DDPF_RGB       | DDPF_ALPHAPIXELS, 16, 0x00007C00, 0x000003E0, 0x0000001F, 0x00008000);
	case DXGI_FORMAT_B5G6R5_UNORM              : return MakeDDSPixelFormat(DDPF_RGB                         , 16, 0x0000F800, 0x000007E0, 0x0000001F, 0x00000000);
	case DXGI_FORMAT_R10G10B10A2_UNORM         : return MakeDDSPixelFormat(DDPF_RGB       | DDPF_ALPHAPIXELS, 32, 0x000003FF, 0x000FFC00, 0x3FF00000, 0xC0000000);
	case DXGI_FORMAT_R16G16_UNORM              : return MakeDDSPixelFormat(DDPF_RGB                         , 32, 0x0000FFFF, 0xFFFF0000, 0x00000000, 0x00000000);
	case DXGI_FORMAT_R16_UNORM                 : return MakeDDSPixelFormat(DDPF_LUMINANCE                   , 16, 0x0000FFFF, 0x00000000, 0x00000000, 0x00000000); // meh .. closest i guess

	case DXGI_FORMAT_B8G8R8X8_UNORM            : return MakeDDSPixelFormat(DDPF_RGB                         , 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0x00000000);
	
	case DXGI_FORMAT_R16G16B16A16_UNORM        : return MakeDDSPixelFormat(DDPF_FOURCC, 0, 0,0,0,0, DDS_D3DFMT_A16B16G16R16       );
	case DXGI_FORMAT_G8R8_G8B8_UNORM           : return MakeDDSPixelFormat(DDPF_FOURCC, 0, 0,0,0,0, DDS_D3DFMT_G8R8_G8B8          );
	case DXGI_FORMAT_R8G8_B8G8_UNORM           : return MakeDDSPixelFormat(DDPF_FOURCC, 0, 0,0,0,0, DDS_D3DFMT_R8G8_B8G8          );
	case DXGI_FORMAT_BC1_UNORM                 : return MakeDDSPixelFormat(DDPF_FOURCC, 0, 0,0,0,0, DDS_D3DFMT_DXT1               );
	case DXGI_FORMAT_BC2_UNORM                 : return MakeDDSPixelFormat(DDPF_FOURCC, 0, 0,0,0,0, DDS_D3DFMT_DXT3               );
	case DXGI_FORMAT_BC3_UNORM                 : return MakeDDSPixelFormat(DDPF_FOURCC, 0, 0,0,0,0, DDS_D3DFMT_DXT5               );
	case DXGI_FORMAT_BC4_UNORM                 : return MakeDDSPixelFormat(DDPF_FOURCC, 0, 0,0,0,0, DDS_D3DFMT_DXT5A              );
	case DXGI_FORMAT_BC5_UNORM                 : return MakeDDSPixelFormat(DDPF_FOURCC, 0, 0,0,0,0, DDS_D3DFMT_DXN                );
	case DXGI_FORMAT_BC6H_UF16                 : return MakeDDSPixelFormat(DDPF_FOURCC, 0, 0,0,0,0, DDS_D3DFMT_BC6                );
	case DXGI_FORMAT_BC7_UNORM                 : return MakeDDSPixelFormat(DDPF_FOURCC, 0, 0,0,0,0, DDS_D3DFMT_BC7                );
	case DXGI_FORMAT_R16_FLOAT                 : return MakeDDSPixelFormat(DDPF_FOURCC, 0, 0,0,0,0, DDS_D3DFMT_R16F               );
	case DXGI_FORMAT_R16G16_FLOAT              : return MakeDDSPixelFormat(DDPF_FOURCC, 0, 0,0,0,0, DDS_D3DFMT_G16R16F            );
	case DXGI_FORMAT_R16G16B16A16_FLOAT        : return MakeDDSPixelFormat(DDPF_FOURCC, 0, 0,0,0,0, DDS_D3DFMT_A16B16G16R16F      );
	case DXGI_FORMAT_R32_FLOAT                 : return MakeDDSPixelFormat(DDPF_FOURCC, 0, 0,0,0,0, DDS_D3DFMT_R32F               );
	case DXGI_FORMAT_R32G32_FLOAT              : return MakeDDSPixelFormat(DDPF_FOURCC, 0, 0,0,0,0, DDS_D3DFMT_G32R32F            );
	case DXGI_FORMAT_R32G32B32A32_FLOAT        : return MakeDDSPixelFormat(DDPF_FOURCC, 0, 0,0,0,0, DDS_D3DFMT_A32B32G32R32F      );
	case DXGI_FORMAT_R10G10B10_XR_BIAS_A2_UNORM: return MakeDDSPixelFormat(DDPF_FOURCC, 0, 0,0,0,0, DDS_D3DFMT_A2B10G10R10_XR_BIAS);
	// signed formats ..
	case DXGI_FORMAT_R8G8_SNORM                : return MakeDDSPixelFormat(DDPF_FOURCC, 0, 0,0,0,0, DDS_D3DFMT_V8U8               );
	case DXGI_FORMAT_R8G8B8A8_SNORM            : return MakeDDSPixelFormat(DDPF_FOURCC, 0, 0,0,0,0, DDS_D3DFMT_Q8W8V8U8           );
	case DXGI_FORMAT_R16G16_SNORM              : return MakeDDSPixelFormat(DDPF_FOURCC, 0, 0,0,0,0, DDS_D3DFMT_V16U16             );
	case DXGI_FORMAT_R16G16B16A16_SNORM        : return MakeDDSPixelFormat(DDPF_FOURCC, 0, 0,0,0,0, DDS_D3DFMT_Q16W16V16U16       );
	}

	DDPIXELFORMAT ddpf;
	memset(&ddpf, 0, sizeof(ddpf));
	return ddpf;
}

bool GetDDSInfo(const char* path, int& w, int& h, int* mips, DXGI_FORMAT* dxgiFormat, DDS_IMAGE_TYPE* imageType, int* arrayCount)
{
	FILE* dds = fopen(path, "rb");

	if (dds)
	{
		u32 magic = 0;
		fread(&magic, sizeof(magic), 1, dds);
		if (magic != MAKE_MAGIC_NUMBER('D','D','S',' '))
		{
			printf("ERROR: dds verification failed for %s, magic = 0x%08x (expected 0x%08x)\n", path, magic, MAKE_MAGIC_NUMBER('D','D','S',' '));
			fclose(dds);
			return false;
		}

		DDSURFACEDESC2 header;
		fread(&header, sizeof(header), 1, dds);
		if (header.dwSize != sizeof(header))
		{
			printf("ERROR: dds verification failed for %s, header size = %d (expected %d)\n", path, header.dwSize, sizeof(header));
			fclose(dds);
			return false;
		}
		else if (header.ddsCaps.dwCaps2 & DDSCAPS2_VOLUME)
		{
			printf("ERROR: dds verification failed for %s, volume texture arrays not supported\n", path);
			fclose(dds);
			return false;
		}
		else if (header.dwMipMapCount > 1)
		{
			const int maxMips = GetMaxMipCount((int)header.dwWidth, (int)header.dwHeight, Max<int>(1, (int)header.dwDepth));

			if ((int)header.dwMipMapCount > maxMips)
			{
				printf("ERROR: dds verification failed for %s, too many mips (w=%d,h=%d,d=%d,mips=%d), max for this size is %d\n", path, header.dwWidth, header.dwHeight, header.dwDepth, header.dwMipMapCount, maxMips);
				fclose(dds);
				return false;
			}
		}

		const u32 allFaces =
			DDSCAPS2_CUBEMAP_POSITIVEX|
			DDSCAPS2_CUBEMAP_NEGATIVEX|
			DDSCAPS2_CUBEMAP_POSITIVEY|
			DDSCAPS2_CUBEMAP_NEGATIVEY|
			DDSCAPS2_CUBEMAP_POSITIVEZ|
			DDSCAPS2_CUBEMAP_NEGATIVEZ;

		DDS_IMAGE_TYPE type = DDS_IMAGE_TYPE_UNKNOWN;

		if (header.ddsCaps.dwCaps2 & DDSCAPS2_CUBEMAP)
		{
			if ((header.ddsCaps.dwCaps2 & allFaces) != allFaces)
			{
				printf("ERROR: dds verification failed for %s, cubemap missing faces (0x%08x, expected 0x%08x)\n", path, header.ddsCaps.dwCaps2 & allFaces, allFaces);
				fclose(dds);
				return false;
			}

			if (header.ddsCaps.dwCaps2 & DDSCAPS2_VOLUME)
			{
				printf("ERROR: dds verification failed for %s, both cubemap and volume flags set\n", path);
				fclose(dds);
				return false;
			}

			type = DDS_IMAGE_TYPE_CUBE;
		}
		else if (header.ddsCaps.dwCaps2 & DDSCAPS2_VOLUME)
		{
			if (header.ddsCaps.dwCaps2 & allFaces)
			{
				printf("ERROR: dds verification failed for %s, volume has cubemap faces (0x%08x)\n", path, header.ddsCaps.dwCaps2 & allFaces);
				fclose(dds);
				return false;
			}

			type = DDS_IMAGE_TYPE_3D;
		}
		else
		{
			type = DDS_IMAGE_TYPE_2D;
		}

		DXGI_FORMAT format = DXGI_FORMAT_UNKNOWN;

		if (header.ddpfPixelFormat.dwFourCC == MAKE_MAGIC_NUMBER('D','X','1','0'))
		{
			DDS_HEADER_DXT10 dx10header;
			fread(&dx10header, sizeof(dx10header), 1, dds);

			if (type == DDS_IMAGE_TYPE_2D && dx10header.resourceDimension != DDS_DIMENSION_TEXTURE2D)
			{
				if (dx10header.resourceDimension == DDS_DIMENSION_TEXTURE1D && header.dwHeight == 1)
				{
					// ok i guess .. 1D texture
				}
				else
				{
					printf("ERROR: dds verification failed for %s, 2D image has resourceDimension=%d\n", path, dx10header.resourceDimension);
					fclose(dds);
					return false;
				}
			}
			else if (type == DDS_IMAGE_TYPE_3D && dx10header.resourceDimension != DDS_DIMENSION_TEXTURE3D)
			{
				printf("ERROR: dds verification failed for %s, volume image has resourceDimension=%d\n", path, dx10header.resourceDimension);
				fclose(dds);
				return false;
			}
			else if (type == DDS_IMAGE_TYPE_CUBE && dx10header.resourceDimension != DDS_DIMENSION_TEXTURE2D)
			{
				printf("ERROR: dds verification failed for %s, cubemap image has resourceDimension=%d\n", path, dx10header.resourceDimension);
				fclose(dds);
				return false;
			}

			const bool hasCubemapFlag = ((dx10header.miscFlag & DDS_RESOURCE_MISC_TEXTURECUBE) != 0);

			if (hasCubemapFlag != (type == DDS_IMAGE_TYPE_CUBE))
			{
				printf("ERROR: dds verification failed for %s, dx10 cubemap flag %s\n", path, hasCubemapFlag ? "set" : "not set");
				fclose(dds);
				return false;
			}

			format = (DXGI_FORMAT)dx10header.dxgiFormat;

			if (dxgiFormat) { *dxgiFormat = format; }
			if (arrayCount) { *arrayCount = dx10header.arraySize; }
		}
		else
		{
			format = (DXGI_FORMAT)GetDX10Format(header.ddpfPixelFormat);

			if (dxgiFormat) { *dxgiFormat = format; }
			if (arrayCount) { *arrayCount = 1; }
		}

		if (format == DXGI_FORMAT_UNKNOWN ||
			format >= DXGI_FORMAT_LAST)
		{
			printf("ERROR: dds verification failed for %s, format is unknown\n", path);
			fclose(dds);
			return false;
		}

		w = (int)header.dwWidth;
		h = (int)header.dwHeight;

		if (mips) { *mips = Max<int>(1, (int)header.dwMipMapCount); }
		if (imageType) { *imageType = type; }

		fclose(dds);
		return true;
	}

	return false;
}

void WriteDDSHeader(FILE* dds, DDS_IMAGE_TYPE type, int w, int h, int d, int mips, int layers, const DDPIXELFORMAT& ddpf, DXGI_FORMAT dxgiFormat, bool bSaveDX10)
{
	const u32 magic = MAKE_MAGIC_NUMBER('D','D','S',' ');
	fwrite(&magic, sizeof(magic), 1, dds);

	DDSURFACEDESC2 header;
	memset(&header, 0, sizeof(header));

	header.dwSize = sizeof(header);
	header.dwFlags = DDSD_CAPS | DDSD_WIDTH | DDSD_HEIGHT | DDSD_PIXELFORMAT;
	header.dwWidth = w;
	header.dwHeight = h;
	header.ddsCaps.dwCaps1 = DDSCAPS_TEXTURE;

	if (mips > 1)
	{
		header.dwFlags |= DDSD_MIPMAPCOUNT;
		header.dwMipMapCount = mips;
	}

	if (type == DDS_IMAGE_TYPE_3D)
	{
		header.dwFlags |= DDSD_DEPTH;
		header.dwDepth = d;
		header.ddsCaps.dwCaps2 |= DDSCAPS2_VOLUME;
	}
	else if (type == DDS_IMAGE_TYPE_CUBE)
	{
		assert(w == h);
		header.ddsCaps.dwCaps2 |= DDSCAPS2_CUBEMAP | DDSCAPS2_CUBEMAP_ALLFACES;
	}

	if (layers > 1 || bSaveDX10)
	{
		header.ddpfPixelFormat.dwSize = sizeof(header.ddpfPixelFormat);
		header.ddpfPixelFormat.dwFlags = DDPF_FOURCC;
		header.ddpfPixelFormat.dwFourCC = MAKE_MAGIC_NUMBER('D','X','1','0');

		if (dxgiFormat == DXGI_FORMAT_UNKNOWN && ddpf.dwSize != 0) // derive dx10 format from dx9 format
		{
			dxgiFormat = GetDX10Format(ddpf);
		}
	}
	else if (ddpf.dwSize != 0) // use dx9 format
	{
		assert(ddpf.dwSize == sizeof(header.ddpfPixelFormat));
		header.ddpfPixelFormat = ddpf;
	}
	else // try to derive dx9 format from dx10 format ..
	{
		assert(dxgiFormat != DXGI_FORMAT_UNKNOWN);
		header.ddpfPixelFormat = GetDDSPixelFormatFromDX10Format(dxgiFormat);

		if (header.ddpfPixelFormat.dwSize == 0)
		{
			// couldn't derive dx9 format from dx10 format .. so use the dx10 format
			header.ddpfPixelFormat.dwSize = sizeof(header.ddpfPixelFormat);
			header.ddpfPixelFormat.dwFlags = DDPF_FOURCC;
			header.ddpfPixelFormat.dwFourCC = MAKE_MAGIC_NUMBER('D','X','1','0');
		}
	}

	fwrite(&header, sizeof(header), 1, dds);

	if (header.ddpfPixelFormat.dwFourCC == MAKE_MAGIC_NUMBER('D','X','1','0'))
	{
		DDS_HEADER_DXT10 dx10header;
		memset(&dx10header, 0, sizeof(dx10header));
		assert(dxgiFormat != DXGI_FORMAT_UNKNOWN);
		dx10header.dxgiFormat = dxgiFormat;
		dx10header.resourceDimension = (type == DDS_IMAGE_TYPE_3D) ? DDS_DIMENSION_TEXTURE3D : DDS_DIMENSION_TEXTURE2D;
		dx10header.miscFlag = (type == DDS_IMAGE_TYPE_CUBE) ? DDS_RESOURCE_MISC_TEXTURECUBE : 0;
		dx10header.arraySize = layers;
		fwrite(&dx10header, sizeof(dx10header), 1, dds);
	}
}

int GetBlockSize(DXGI_FORMAT format)
{
	if ((format >= DXGI_FORMAT_BC1_TYPELESS && format <= DXGI_FORMAT_BC5_SNORM) || // DXT1, DXT3, DXT5, DXT5A, DXN
		(format >= DXGI_FORMAT_BC6H_TYPELESS && format <= DXGI_FORMAT_BC7_UNORM_SRGB)) // BC6H or BC7
	{
		return 4;
	}
	else
	{
		return 1;
	}
}

int GetBitsPerPixel(DXGI_FORMAT format)
{
	switch (format)
	{
	case DXGI_FORMAT_R32G32B32A32_TYPELESS:
	case DXGI_FORMAT_R32G32B32A32_FLOAT:
	case DXGI_FORMAT_R32G32B32A32_UINT:
	case DXGI_FORMAT_R32G32B32A32_SINT:
		return 128;

	case DXGI_FORMAT_R32G32B32_TYPELESS:
	case DXGI_FORMAT_R32G32B32_FLOAT:
	case DXGI_FORMAT_R32G32B32_UINT:
	case DXGI_FORMAT_R32G32B32_SINT:
		return 96;

	case DXGI_FORMAT_R16G16B16A16_TYPELESS:
	case DXGI_FORMAT_R16G16B16A16_FLOAT:
	case DXGI_FORMAT_R16G16B16A16_UNORM:
	case DXGI_FORMAT_R16G16B16A16_UINT:
	case DXGI_FORMAT_R16G16B16A16_SNORM:
	case DXGI_FORMAT_R16G16B16A16_SINT:
	case DXGI_FORMAT_R32G32_TYPELESS:
	case DXGI_FORMAT_R32G32_FLOAT:
	case DXGI_FORMAT_R32G32_UINT:
	case DXGI_FORMAT_R32G32_SINT:
		return 64;

	case DXGI_FORMAT_D32_FLOAT_S8X24_UINT:
	case DXGI_FORMAT_R32_FLOAT_X8X24_TYPELESS:
	case DXGI_FORMAT_X32_TYPELESS_G8X24_UINT:
	case DXGI_FORMAT_R32G8X24_TYPELESS:
		// According to AMD - Not sure if its the same for NVida but I hope so.
		return 40;

	case DXGI_FORMAT_R10G10B10A2_TYPELESS:
	case DXGI_FORMAT_R10G10B10A2_UNORM:
	case DXGI_FORMAT_R10G10B10A2_UINT:
	case DXGI_FORMAT_R10G10B10_XR_BIAS_A2_UNORM:
	case DXGI_FORMAT_R11G11B10_FLOAT:
	case DXGI_FORMAT_R8G8B8A8_TYPELESS:
	case DXGI_FORMAT_R8G8B8A8_UNORM:
	case DXGI_FORMAT_R8G8B8A8_UNORM_SRGB:
	case DXGI_FORMAT_R8G8B8A8_UINT:
	case DXGI_FORMAT_R8G8B8A8_SNORM:
	case DXGI_FORMAT_R8G8B8A8_SINT:
	case DXGI_FORMAT_R16G16_TYPELESS:
	case DXGI_FORMAT_R16G16_FLOAT:
	case DXGI_FORMAT_R16G16_UNORM:
	case DXGI_FORMAT_R16G16_UINT:
	case DXGI_FORMAT_R16G16_SNORM:
	case DXGI_FORMAT_R16G16_SINT:
	case DXGI_FORMAT_R32_TYPELESS:
	case DXGI_FORMAT_D32_FLOAT:
	case DXGI_FORMAT_R32_FLOAT:
	case DXGI_FORMAT_R32_UINT:
	case DXGI_FORMAT_R32_SINT:
	case DXGI_FORMAT_R24G8_TYPELESS:
	case DXGI_FORMAT_D24_UNORM_S8_UINT:
	case DXGI_FORMAT_R24_UNORM_X8_TYPELESS:
	case DXGI_FORMAT_X24_TYPELESS_G8_UINT:
	case DXGI_FORMAT_R9G9B9E5_SHAREDEXP:
	case DXGI_FORMAT_R8G8_B8G8_UNORM:
	case DXGI_FORMAT_G8R8_G8B8_UNORM:
	case DXGI_FORMAT_B8G8R8A8_UNORM:
	case DXGI_FORMAT_B8G8R8X8_UNORM:
		return 32;

	case DXGI_FORMAT_R8G8_TYPELESS:
	case DXGI_FORMAT_R8G8_UNORM:
	case DXGI_FORMAT_R8G8_UINT:
	case DXGI_FORMAT_R8G8_SNORM:
	case DXGI_FORMAT_R8G8_SINT:
	case DXGI_FORMAT_R16_TYPELESS:
	case DXGI_FORMAT_R16_FLOAT:
	case DXGI_FORMAT_D16_UNORM:
	case DXGI_FORMAT_R16_UNORM:
	case DXGI_FORMAT_R16_UINT:
	case DXGI_FORMAT_R16_SNORM:
	case DXGI_FORMAT_R16_SINT:
	case DXGI_FORMAT_B5G6R5_UNORM:
	case DXGI_FORMAT_B5G5R5A1_UNORM:
		return 16;

	case DXGI_FORMAT_R8_TYPELESS:
	case DXGI_FORMAT_R8_UNORM:
	case DXGI_FORMAT_R8_UINT:
	case DXGI_FORMAT_R8_SNORM:
	case DXGI_FORMAT_R8_SINT:
	case DXGI_FORMAT_A8_UNORM:
	case DXGI_FORMAT_BC2_TYPELESS:
	case DXGI_FORMAT_BC2_UNORM:
	case DXGI_FORMAT_BC2_UNORM_SRGB:
	case DXGI_FORMAT_BC3_TYPELESS:
	case DXGI_FORMAT_BC3_UNORM:
	case DXGI_FORMAT_BC3_UNORM_SRGB:
	case DXGI_FORMAT_BC5_TYPELESS:
	case DXGI_FORMAT_BC5_UNORM:
	case DXGI_FORMAT_BC5_SNORM:
	case DXGI_FORMAT_BC6H_TYPELESS:
	case DXGI_FORMAT_BC6H_UF16:
	case DXGI_FORMAT_BC6H_SF16:
	case DXGI_FORMAT_BC7_TYPELESS:
	case DXGI_FORMAT_BC7_UNORM:
	case DXGI_FORMAT_BC7_UNORM_SRGB:
		return 8;

	case DXGI_FORMAT_BC1_TYPELESS:
	case DXGI_FORMAT_BC1_UNORM:
	case DXGI_FORMAT_BC1_UNORM_SRGB:
	case DXGI_FORMAT_BC4_TYPELESS:
	case DXGI_FORMAT_BC4_UNORM:
	case DXGI_FORMAT_BC4_SNORM:
		return 4;

	case DXGI_FORMAT_R1_UNORM:
		return 1;

	case DXGI_FORMAT_P8:
		return 8;

	case DXGI_FORMAT_A8P8:
	case DXGI_FORMAT_B4G4R4A4_UNORM:
		return 16;

	case DXGI_FORMAT_R10G10B10_7E3_A2_FLOAT:
	case DXGI_FORMAT_R10G10B10_6E4_A2_FLOAT:
		return 32;

	default:
		return 0;
	}
}

int GetMaxMipCount(int w, int h, int d, bool bAllowNonPow2Scaling)
{
	for (int i = 0; i < 20; i++)
	{
		const int mw = Max<int>(1, w>>i);
		const int mh = Max<int>(1, h>>i);
		const int md = Max<int>(1, d>>i);

		if (mw == 1 && mh == 1 && md == 1)
		{
			return i + 1; // this mip index is valid, but the next one won't be
		}
		else if ((w%mw != 0 || h%mh != 0 || d%md != 0) && !bAllowNonPow2Scaling)
		{
			return i; // this mip index is not valid, so the previous one was the last valid one
		}
	}

	assert(0);
	return 1; // should never get here
}

int GetImageSizeInBytes(int w, int h, int d, int mips, DXGI_FORMAT dxgiFormat, DDS_IMAGE_TYPE imageType, int arrayCount)
{
	const int bpp = GetBitsPerPixel(dxgiFormat);
	const int blockSize = GetBlockSize(dxgiFormat); // will be either 1 or 4
	int sizeInBytes = 0;

	for (int mip = 0; mip < mips; mip++)
	{
		const int mw = Max<int>(1, w>>mip);
		const int mh = Max<int>(1, h>>mip);
		const int md = Max<int>(1, d>>mip);

		const int physicalW = (mw + blockSize - 1)&~(blockSize - 1);
		const int physicalH = (mh + blockSize - 1)&~(blockSize - 1);

		sizeInBytes += (physicalW*physicalH*md*bpp)/8;
	}

	if (imageType == DDS_IMAGE_TYPE_CUBE)
	{
		sizeInBytes *= 6;
	}

	return sizeInBytes*arrayCount;
}

bool IsDX9FormatCompressedOrPacked(DDS_D3DFORMAT format)
{
	if (format == DDS_D3DFMT_UYVY      ) { return true; }
	if (format == DDS_D3DFMT_YUY2      ) { return true; }
	if (format == DDS_D3DFMT_R8G8_B8G8 ) { return true; }
	if (format == DDS_D3DFMT_G8R8_G8B8 ) { return true; }
	if (format == DDS_D3DFMT_DXT1      ) { return true; }
	if (format == DDS_D3DFMT_DXT2      ) { return true; }
	if (format == DDS_D3DFMT_DXT3      ) { return true; }
	if (format == DDS_D3DFMT_DXT4      ) { return true; }
	if (format == DDS_D3DFMT_DXT5      ) { return true; }

	if (format == DDS_D3DFMT_CTX1      ) { return true; }
	if (format == DDS_D3DFMT_DXT3A     ) { return true; }
	if (format == DDS_D3DFMT_DXT3A_1111) { return true; }
	if (format == DDS_D3DFMT_DXT5A     ) { return true; } // BC4
	if (format == DDS_D3DFMT_DXN       ) { return true; } // BC5

	return false;
}

bool IsDX10FormatCompressedOrPacked(DXGI_FORMAT format)
{
	if ((format >= DXGI_FORMAT_BC1_TYPELESS && format <= DXGI_FORMAT_BC5_SNORM) || // DXT1, DXT3, DXT5, DXT5A, DXN
		(format >= DXGI_FORMAT_BC6H_TYPELESS && format <= DXGI_FORMAT_BC7_UNORM_SRGB)) // BC6H or BC7
	{
		return true;
	}
	else if (format >= DXGI_FORMAT_AYUV && format <= DXGI_FORMAT_IA44) // whatever these are ..
	{
		return true;
	}
	else if (format == DXGI_FORMAT_R8G8_B8G8_UNORM || format == DXGI_FORMAT_G8R8_G8B8_UNORM)
	{
		return true;
	}
	else
	{
		return false;
	}
}
