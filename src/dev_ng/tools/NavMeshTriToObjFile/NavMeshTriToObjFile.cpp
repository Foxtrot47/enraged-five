#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>

typedef unsigned char          u8;
typedef unsigned short         u16;
typedef unsigned int           u32;
typedef unsigned long long int u64;

class Vec3
{
public:
	Vec3() {}
	Vec3(float x_, float y_, float z_) : x(x_), y(y_), z(z_) {}
	Vec3(float f_) : x(f_), y(f_), z(f_) {}

	float x, y, z;
};

class Tri3
{
public:
	Vec3 p[3];
};

static void ConvertTriToObj(std::vector<Tri3>& tris, const char* path)
{
	FILE* f = fopen(path, "rb");

	if (f)
	{
		fprintf(stdout, "loading \"%s\" .. ", path);
		fflush(stdout);

		char header[8];
		fread(header, sizeof(header), 1, f);
		const bool bIsHeightMapTriFile = strstr(header, "HEIGHT") != NULL;

		u32 iReserved;
		fread(&iReserved, sizeof(iReserved), 1, f);

		u32 iNumTriangles;
		fread(&iNumTriangles, sizeof(iNumTriangles), 1, f);

		fseek(f, 8*sizeof(float), SEEK_CUR); // skip sector extents
		fseek(f, 4*sizeof(int), SEEK_CUR);

		char bHasWater = 0;
		float fWaterLevel = 0.0f;
		char bHasVariableWaterLevel = 0;

		fread(&bHasWater, sizeof(bHasWater), 1, f);
		fread(&fWaterLevel, sizeof(fWaterLevel), 1, f);
		fread(&bHasVariableWaterLevel, sizeof(bHasVariableWaterLevel), 1, f);

		if (bHasVariableWaterLevel)
		{
			float fWaterSamplingStepSize = 0.0f;
			int iNumWaterSamples = 0;

			fread(&fWaterSamplingStepSize, sizeof(fWaterSamplingStepSize), 1, f);
			fread(&iNumWaterSamples, sizeof(iNumWaterSamples), 1, f);

			fseek(f, iNumWaterSamples*sizeof(u16), SEEK_CUR); // skip water samples
		}

		for (u32 i = 0; i < iNumTriangles; i++)
		{
			fseek(f, 5, SEEK_CUR); // skip triangle properties: iBakedInBitField, iBitField1, iCollisionTypeFlags, iPedDensity

			if (bIsHeightMapTriFile)
			{
				fseek(f, 3*sizeof(u8) + 3*sizeof(u32) + sizeof(u64) + 3*sizeof(u8), SEEK_CUR); // skip heightmap-specific properties
				//fseek(f, sizeof(u32), SEEK_CUR); // skip 'm_iPropGroupMask'
			}

			Tri3 tri;

			for (int j = 0; j < 3; j++)
			{
				fread(&tri.p[j].x, sizeof(float), 1, f);
				fread(&tri.p[j].y, sizeof(float), 1, f);
				fread(&tri.p[j].z, sizeof(float), 1, f);
			}

			tris.push_back(tri);
		}

		fclose(f);
		fprintf(stdout, "ok.\n");
	}
	else
	{
		fprintf(stdout, "failed!\n", path);
	}
}

int main(int argc, const char* argv[])
{
	std::vector<Tri3> tris;
	bool bSingleTile = false;

	if (argc <= 1)
	{
		fprintf(stdout, "to use, drag navmesh .tri files onto this executable ..\n");
	}
	else if (argc == 2)
	{
		const int r = 2; // radius = 1 means just a single tile, radius = 2 means 3x3, etc.

		if (r > 1)
		{
			char path[256] = "";
			strcpy(path, argv[1]);
			char* istr = strchr(path, '[');
			char* jstr = istr ? strchr(istr + 1, '[') : NULL;

			if (jstr)
			{
				const int i = atoi(istr + 1);
				const int j = atoi(jstr + 1);
				istr[0] = '\0';

				for (int dj = -3*(r - 1); dj <= 3*(r - 1); dj += 3)
				{
					for (int di = -3*(r - 1); di <= 3*(r - 1); di += 3)
					{
						char path2[256] = "";
						sprintf(path2, "%s[%d][%d].tri", path, i + di, j + dj);
						ConvertTriToObj(tris, path2);
					}
				}
			}
			else
			{
				bSingleTile = true;
			}
		}
		else
		{
			bSingleTile = true;
		}

		if (bSingleTile)
		{
			ConvertTriToObj(tris, argv[1]);
		}
	}
	else
	{
		for (int i = 1; i < argc; i++)
		{
			ConvertTriToObj(tris, argv[i]);
		}
	}

	if (tris.size() > 0)
	{
		char outpath[512] = "out.obj";

		if (bSingleTile)
		{
			strcpy(outpath, argv[1]);
			if (strrchr(outpath, '.'))
				strrchr(outpath, '.')[0] = '\0';
			strcat(outpath, ".obj");
		}

		fprintf(stdout, "creating obj file \"%s\" with %d triangles .. ", outpath, (int)tris.size());
		fflush(stdout);

		FILE* f = fopen(outpath, "w");

		if (f)
		{
			for (int i = 0; i < (int)tris.size(); i++)
			{
				for (int j = 0; j < 3; j++)
				{
					fprintf(f, "v %f %f %f\n", tris[i].p[j].x, tris[i].p[j].y, tris[i].p[j].z);
				}
			}

			for (int i = 0; i < (int)tris.size(); i++)
			{
				fprintf(f, "f %d %d %d\n", 3 + i*3, 2 + i*3, 1 + i*3);
			}

			fclose(f);
			fprintf(stdout, "ok.\n");
			system(outpath);
		}
		else
		{
			fprintf(stdout, "failed!\n", outpath);
		}
	}
	else
	{
		fprintf(stdout, "no triangles produced!\n");		
	}

	system("pause");
	return 0;
}
