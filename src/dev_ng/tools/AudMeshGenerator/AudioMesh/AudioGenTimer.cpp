/////////////////////////////////////////////////////////////////////////////////
// FILE		: CAudioLevelMesh.cpp
// PURPOSE	: To load in level geometry for the audio mesh generation tool.
// AUTHOR	: C. Walder (based on Recast be Mikko Menonen)
// STARTED	: 31/08/2009
///////////////////////////////////////////////////////////////////////////////

#include "AudioGenTimer.h"

//#include "system/xtl.h"
//#include <windows.h>

#include "system/timer.h"

agTimeVal agGetPerformanceTimer()
{
	__int64 count;
	//QueryPerformanceCounter((LARGE_INTEGER*)&count);
	count = sysTimer::GetTicks();
	return count;
}

int agGetDeltaTimeUsec(agTimeVal start, agTimeVal end)
{
	static __int64 freq = 0;
	if (freq == 0)
		freq = (__int64)sysTimer::GetCpuSpeed(); //QueryPerformanceFrequency((LARGE_INTEGER*)&freq);
	__int64 elapsed = end - start;
	return (int)(elapsed*1000000 / freq);
}
