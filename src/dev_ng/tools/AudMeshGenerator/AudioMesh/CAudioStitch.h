#ifndef C_AUDSTITCH_H
#define C_AUDSTITCH_H


////////////////////////////////////////////////////////
//CAudioStitch forms connections from an audio mesh built with CAudioGen to its neighbouring meshes
#include "fwnavgen/toolnavmesh.h"

#include "CMeshGen.h"

//Non-class structs and functions
#include "AudioGen.h"
#include "AudioGenLog.h"


class CAudioStitch : public CMeshGen
{
public:
	CAudioStitch();
	virtual ~CAudioStitch();

	virtual void HandleSettings();
	virtual void HandleDebugMode();

	virtual void HandleRender();
	virtual void HandleRenderOverlay();
	virtual void HandleMeshHasChanged(const float* verts, int nverts, const int* tris, const float* trinorms,
			int ntris, const float* bmin, const float* bmax, const char * path, char * meshName);

	virtual bool HandleBuild();

	virtual void ResetSettings();


protected:

	int GetXSectorOffset(int dir)
	{
		const int offset[4] = {-2, 0, 2, 0};
		return offset[dir];
	}

	int GetYSectorOffset(int dir)
	{
		const int offset[4] = {0, 2, 0, -2};
		return offset[dir];
	}

	const float* m_verts;
	int m_nverts;
	const int* m_tris;
	const float* m_trinorms;
	int m_ntris;
	float m_bmin[3], m_bmax[3];
	int m_xsector, m_ysector;


	//Stuff to do with the generated mesh goes here
	
	//More stuff to go here for post-voxellation process
	
	agBuildTimes m_buildTimes;
	agCompactHeightField *m_compact;
	agCompactHeightField *m_ncompacts[4];

	agPolyMesh *m_mesh;
	agPolyMesh *m_nmeshes[4];

	char *m_path;
	char *m_meshName;

	enum DrawMode
	{
		DRAWMODE_MESH,
		DRAWMODE_COMPACT,
		DRAWMODE_POLYMESH,
		DRAWMODE_NEIGHBOURS,
		DRAWMODE_COMPACT_NEIGHBOURS,
		DRAWMODE_STITCH,
		DRAWMODE_STITCH_REGS,
		MAX_DRAWMODE
	};

	DrawMode m_drawMode;

	void cleanup();
		
};

#endif

