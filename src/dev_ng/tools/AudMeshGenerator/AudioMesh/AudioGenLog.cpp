/////////////////////////////////////////////////////////////////////////////////
// FILE		: CAudioLevelMesh.cpp
// PURPOSE	: To load in level geometry for the audio mesh generation tool.
// AUTHOR	: C. Walder (based on Recast be Mikko Menonen)
// STARTED	: 31/08/2009
///////////////////////////////////////////////////////////////////////////////

#include "AudioGenLog.h"
#include <stdio.h>
#include <stdarg.h>

static agLog* g_log = 0;
static agBuildTimes* g_btimes = 0;

agLog::agLog() :
	m_messageCount(0),
	m_textPoolSize(0)
{
}

agLog::~agLog()
{
	if (g_log == this)
		g_log = 0;
}

void agLog::log(agLogCategory category, const char* format, ...)
{
	if (m_messageCount >= MAX_MESSAGES)
		return;
	char* dst = &m_textPool[m_textPoolSize];
	int n = TEXT_POOL_SIZE - m_textPoolSize;
	if (n < 2)
		return;
	// Store category
	*dst = (char)category;
	n--;
	// Store message
	va_list ap;
	va_start(ap, format);
	int ret = vsnprintf(dst+1, n-1, format, ap);
	va_end(ap);
	if (ret > 0)
		m_textPoolSize += ret+2;
	m_messages[m_messageCount++] = dst;
}

void agSetLog(agLog* log)
{
	g_log = log;
}

agLog* agGetLog()
{
	return g_log;
}

void agSetBuildTimes(agBuildTimes* btimes)
{
	g_btimes = btimes;
}

agBuildTimes* agGetBuildTimes()
{
	return g_btimes;
}
