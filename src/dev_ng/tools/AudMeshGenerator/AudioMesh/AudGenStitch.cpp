#include <float.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "AudioGen.h"
#include "AudioGenLog.h"
#include "AudioGenTimer.h"

u8 agGetDirStitchFlag(int dir)
{
	u8 flags[4] = {AG_STITCH_0, AG_STITCH_1, AG_STITCH_2, AG_STITCH_3};
	return flags[dir];
}

//This looks in the direction specified by dir, and populates regs with all adjacent, audible regions (e.g. could be several floors)
void agGetNextAudibleRegionsDir(const agCompactHeightField &compact, const agCompactSpan &span, int x, int y, agIntArray &regs, int dir)
{
	int ax = x + agGetDirOffsetX(dir);
	int ay = y + agGetDirOffsetY(dir);

	if(ax <0 || ax >= compact.width || ay <0 || ay >= compact.length)
	{
		return;
	}

	agCompactCell &cell = compact.cells[ax + compact.width*ay];

	for(int i=cell.index, endi = (cell.index+cell.count); i<endi; ++i)
	{
		agCompactSpan & aspan = compact.spans[i];
		if(aspan.reg != AG_BORDER_REG)
		{
			if((aspan.y < (span.y+span.height)) && ((aspan.y+aspan.height) > span.y))
			{
				regs.push(aspan.reg);	
			}
		}
	}

	if(!regs.size())
	{
		for(int i=cell.index, endi = (cell.index+cell.count); i<endi; ++i)
		{
			agCompactSpan & aspan = compact.spans[i];
			if((aspan.y < (span.y+span.height)) && ((aspan.y+aspan.height) > span.y))
			{
				agGetNextAudibleRegionsDir(compact, aspan, ax, ay, regs, dir);
			}
		}
	}
}


void agStitchMeshAudioConnections(agPolyMesh &mesh, const agCompactHeightField & compact, const agCompactHeightField & ncompact, int dir)
{
	int w = compact.width;
	int l = compact.length;
	int flag = agGetDirStitchFlag(dir);

	agIntArray localRegs;
	agIntArray neighbourRegs;
	int nstitches = 0;

	for(int y = 0; y<l; ++y)
	{
		for(int x=0; x<w; ++x)
		{
			agCompactCell &cell = compact.cells[x+w*y];
			for(int i = cell.index, endi = cell.index+cell.count; i<endi; ++i)
			{
				//Find spans that have been marked as stitched in this direction
				agCompactSpan &span = compact.spans[i];
				if(span.flags&flag)
				{
					//Get the region(s) to be stitched from the main mesh by looking in the opposite diraction from dir
					agIntArray regs;
					agGetNextAudibleRegionsDir(compact, span, x, y, regs, (dir+2)&0x3);

					//Work out which cell on the neighbour mesh to begin the stitch and get the stitch regions
					int nx = x;
					int ny = y;

					switch(dir)
					{
						case 0: nx = ncompact.width-1; break;
						case 1: ny = 0; break;
						case 2: nx = 0; break;
						case 3: ny = ncompact.length-1; break;
					}

					agIntArray stitchRegs;
					agCompactCell &ncell = ncompact.cells[nx+ncompact.width*ny];
					for(int index=ncell.index, endex= ncell.index+ncell.count; index<endex; ++index)
					{
						//If the stitch span is audible get the next audible region(s) from the neighbour mesh
						agCompactSpan &nspan = ncompact.spans[index];
						if((nspan.y < (span.y+span.height)) && ((nspan.y+nspan.height) > span.y))
						{
							agGetNextAudibleRegionsDir(ncompact, nspan, nx, ny, stitchRegs, dir);
						}


						//Check to see if we already have these stitch connections and if not add them in
						for(int j=0; j<regs.size(); ++j)
						{
							for(int k=0; k<stitchRegs.size(); ++k)
							{
								agCompactRegion &reg1 = compact.regions[regs[j]];
								agCompactRegion &reg2 = ncompact.regions[stitchRegs[k]];
								bool allreadyIn = false;
								for(int a=0; a<nstitches; ++a)
								{
									if(reg1.id == localRegs[a] && reg2.id == neighbourRegs[a])
									{
										allreadyIn = true;
										break;
									}
								}
								if(!allreadyIn)
								{
									localRegs.push(reg1.id);
									neighbourRegs.push(reg2.id);
									++nstitches;	
								}
							}
						}
					}
				}
			}
		}
	}

	//Populate the polymesh with the stitches that we found
	if(nstitches)
	{
		mesh.nstitches[dir] = (u16)nstitches;
		mesh.localStitches[dir] = rage_new u16[nstitches];
		mesh.neighbourStitches[dir] = rage_new u16[nstitches];
		for(int i=0; i<nstitches; ++i)
		{
			mesh.localStitches[dir][i] = (u16)localRegs[i];
			mesh.neighbourStitches[dir][i] = (u16)neighbourRegs[i];
			mesh.regFlags[localRegs[i]] |= agGetDirStitchFlag(dir);
		}
	}

}

void agStitchMeshes(agPolyMesh &mesh, const agCompactHeightField &compact, const agCompactHeightField * const ncompacts[4])
{
	//Allocate memory for the region flags on the polymesh...there might be somwhere eles more approriate for this?
	mesh.regFlags = rage_new u8[mesh.nregs];
	memset(mesh.regFlags, 0, sizeof(u8)*mesh.nregs);

	for(int dir=0; dir<4; ++dir)
	{
		if(ncompacts[dir])
		{
			agStitchMeshAudioConnections(mesh, compact, *ncompacts[dir], dir);
		}
	}
}


