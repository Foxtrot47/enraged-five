#ifndef C_MESHGEN_H
#define C_MESHGEN_H

#include "AudioGen.h"


class CMeshGen
{
	public:
	CMeshGen() {};
	virtual ~CMeshGen() {}; 

	virtual void HandleSettings() {}; 
	virtual void HandleDebugMode() {};

	virtual void HandleRender() {};
	virtual void HandleRenderOverlay() {};
	virtual void HandleMeshHasChanged(const float* UNUSED_PARAM(verts), int UNUSED_PARAM(nverts), const int* UNUSED_PARAM(tris), const float* UNUSED_PARAM(trinorms),
			int UNUSED_PARAM(ntris), const float* UNUSED_PARAM(bmin), const float* UNUSED_PARAM(bmax), const char * UNUSED_PARAM(path), char * UNUSED_PARAM(meshName)) {};

	virtual bool HandleBuild() { return false; };

	virtual void ResetSettings() {};
	
	//This is used by the command line interface to apply inputs normally done by the gui
	virtual void ApplyInputs(AudGenInputs &UNUSED_PARAM(inputs)) {};
};

#endif
