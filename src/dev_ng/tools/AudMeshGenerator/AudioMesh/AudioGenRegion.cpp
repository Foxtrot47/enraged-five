#include <float.h>
//#define _USE_MATH_DEFINES
//#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "AudioGen.h"
#include "AudioGenLog.h"
#include "AudioGenTimer.h"

static void adjustConnectionDistance(const agCompactVolumefield &compact, const agCompactVoxel &vox, u16 *src, int face, int dir, int x, int y, int i)
{ 
	if(agGetConnectionData(vox, face, dir) != AG_NULL_CONNECTION)
	{
		int cidx = agGetFaceConnection(compact, vox, face, dir, x, y); //index of the connection voxel
		const agCompactVoxel &cvox = compact.voxels[cidx];
		if(src[cidx]+2 < src[i])
		{
			src[i] = src[cidx]+2; //adjacent connection is closer to a boundary so set new distance
		}

		int dir2 = (dir+3)&0x03;
		if(agGetConnectionData(cvox, face, dir2) != AG_NULL_CONNECTION)
		{
			int ccidx = agGetFaceConnection(compact, cvox, face, dir2, x + agGetDirOffsetX(face, dir), y + agGetDirOffsetY(face, dir));
			if(src[ccidx] +3 < src[i])
			{
				src[i] = src[ccidx]+3; //diagonal connection is closer to boundary so set new distance
			}
		}
	}
}

static u16* calculateDistanceField(const agCompactVolumefield &compact, u16 *topSrc, u16 *maxDist, u16 *botSrc, 
		u16 *leftSrc, u16 *rightSrc, u16 *frontSrc, u16 *backSrc)
{
	const int w = compact.width;
	const int l = compact.length;

	//Init distance and points
//	for(int i=0; i<compact.voxelCount; ++i)
//	{
//		topSrc[i] = 0xffff;
//		botSrc[i] = 0xffff;
//		leftSrc[i] = 0xffff;
//		rightSrc[i] = 0xffff;
//		frontSrc[i] = 0xffff;
//		backSrc[i] = 0xffff;
//	}
//	
	//any reason we can't just do a memset?
	memset(topSrc, 0xff, sizeof(u16)*compact.voxelCount);
	memset(botSrc, 0xff, sizeof(u16)*compact.voxelCount);
	memset(leftSrc, 0xff, sizeof(u16)*compact.voxelCount);
	memset(rightSrc, 0xff, sizeof(u16)*compact.voxelCount);
	memset(frontSrc, 0xff, sizeof(u16)*compact.voxelCount);
	memset(backSrc, 0xff, sizeof(u16)*compact.voxelCount);

	//Mark boundary cells
//	for (int y = 0; y<l; ++y)
	{
//		for( int x=0; x<w; ++x)
		{
//			const agCompact3dCell &cell = compact.cells[x+y*w];
			for(int i =0; i<compact.voxelCount; ++i)	//for(int i = (int)cell.index, endi = (int)(cell.index+cell.count); i < endi; ++i)
			{
				const agCompactVoxel &vox = compact.voxels[i];
				int topCount = 0, botCount = 0, leftCount = 0,  rightCount = 0, frontCount = 0, backCount = 0; //variables to count how many connections the face has
				for (int dir = 0; dir < 4; ++dir)
				{
					if(vox.faces[AG_VOX_TOP].flags&AG_FACE_ACTIVE && agGetConnectionData(vox, AG_VOX_TOP, dir) != AG_NULL_CONNECTION)
					{
						topCount++;
					}
					if(vox.faces[AG_VOX_BOTTOM].flags&AG_FACE_ACTIVE && agGetConnectionData(vox, AG_VOX_BOTTOM, dir) != AG_NULL_CONNECTION)
					{
						botCount++;
					}
					if(vox.faces[AG_VOX_LEFT].flags&AG_FACE_ACTIVE && agGetConnectionData(vox, AG_VOX_LEFT, dir) != AG_NULL_CONNECTION)
					{
						leftCount++;
					}
					if(vox.faces[AG_VOX_RIGHT].flags&AG_FACE_ACTIVE && agGetConnectionData(vox, AG_VOX_RIGHT, dir) != AG_NULL_CONNECTION)
					{
						rightCount++;
					}
					if(vox.faces[AG_VOX_FRONT].flags&AG_FACE_ACTIVE && agGetConnectionData(vox, AG_VOX_FRONT, dir) != AG_NULL_CONNECTION)
					{
						frontCount++;
					}
					if(vox.faces[AG_VOX_BACK].flags&AG_FACE_ACTIVE && agGetConnectionData(vox, AG_VOX_BACK, dir) != AG_NULL_CONNECTION)
					{
						backCount++;
					}
				}
				if(topCount != 4)
				{
					//if the face doesn't have connections on four sides it lies on
					//a boundary and has a distance of 0
					topSrc[i] = 0;
				}
				if(botCount != 4)
				{
					botSrc[i] = 0;
				}
				if( leftCount != 4)
				{
					leftSrc[i] = 0;
				}
				if(rightCount != 4)
				{
					rightSrc[i] = 0;
				}
				if(frontCount < 4)
				{
					frontSrc[i] = 0;
				}
				if(backCount != 4)
				{
					backSrc[i] = 0;
				}
			}
		}
	}


	//if(1) return topSrc;

	//A local distance matrix is applied in two consecutive passes over the voxel grid.
	//Each pass propogates the local distance computed by the addition of known neighbourhood 
	//value to values obtained from the local distance matrix, sweeping diagonally across the grid.
	//Pass 1
	for(int y = 0; y<l; ++y)
	{ 
		for(int x = 0; x<w; ++x) 
		{
			const agCompact3dCell &cell = compact.cells[x+y*w];
			for(u32 i = cell.index, endi = (cell.index+cell.count); i<endi; ++i)
			{

				const agCompactVoxel &vox = compact.voxels[i];

				adjustConnectionDistance(compact, vox, topSrc, AG_VOX_TOP, 0, x, y, i);
				adjustConnectionDistance(compact, vox, topSrc, AG_VOX_TOP, 3, x, y, i);
				adjustConnectionDistance(compact, vox, botSrc, AG_VOX_BOTTOM, 0, x, y, i);
				adjustConnectionDistance(compact, vox, botSrc, AG_VOX_BOTTOM, 3, x, y, i);
				adjustConnectionDistance(compact, vox, leftSrc, AG_VOX_LEFT, 0, x, y, i);
				adjustConnectionDistance(compact, vox, leftSrc, AG_VOX_LEFT, 3, x, y, i);
				adjustConnectionDistance(compact, vox, rightSrc, AG_VOX_RIGHT, 0, x, y, i);
				adjustConnectionDistance(compact, vox, rightSrc, AG_VOX_RIGHT, 3, x, y, i);
				adjustConnectionDistance(compact, vox, frontSrc, AG_VOX_FRONT, 0, x, y, i);
				adjustConnectionDistance(compact, vox, frontSrc, AG_VOX_FRONT, 3, x, y, i);
				adjustConnectionDistance(compact, vox, backSrc, AG_VOX_BACK, 0, x, y, i);
				adjustConnectionDistance(compact, vox, backSrc, AG_VOX_BACK, 3, x, y, i);
			}
		}
	}

	//Pass 2
	for(int y = l-1; y>=0; --y)
	{
		for(int x = w-1; x>=0; --x)
		{
			const agCompact3dCell &cell = compact.cells[x+y*w];
			for(int i= (int)cell.index, endi = (int)(cell.index+cell.count); i<endi; ++i)
			{
				agCompactVoxel &vox = compact.voxels[i];

				adjustConnectionDistance(compact, vox, topSrc, AG_VOX_TOP, 2, x, y, i);
				adjustConnectionDistance(compact, vox, topSrc, AG_VOX_TOP, 1, x, y, i);
				adjustConnectionDistance(compact, vox, botSrc, AG_VOX_BOTTOM, 2, x, y, i);
				adjustConnectionDistance(compact, vox, botSrc, AG_VOX_BOTTOM, 1, x, y, i);
				adjustConnectionDistance(compact, vox, leftSrc, AG_VOX_LEFT, 2, x, y, i);
				adjustConnectionDistance(compact, vox, leftSrc, AG_VOX_LEFT, 1, x, y, i);
				adjustConnectionDistance(compact, vox, rightSrc, AG_VOX_RIGHT, 2, x, y, i);
				adjustConnectionDistance(compact, vox, rightSrc, AG_VOX_RIGHT, 1, x, y, i);
				adjustConnectionDistance(compact, vox, frontSrc, AG_VOX_FRONT, 2, x, y, i);
				adjustConnectionDistance(compact, vox, frontSrc, AG_VOX_FRONT, 1, x, y, i);
				adjustConnectionDistance(compact, vox, backSrc, AG_VOX_BACK, 2, x, y, i);
				adjustConnectionDistance(compact, vox, backSrc, AG_VOX_BACK, 1, x, y, i);
			}
		}
	}

	for (int i = 0; i<compact.voxelCount; ++i)
	{
		maxDist[AG_VOX_TOP] = Max(topSrc[i], maxDist[AG_VOX_TOP]);
		maxDist[AG_VOX_BOTTOM] = Max(botSrc[i], maxDist[AG_VOX_BOTTOM]);
		maxDist[AG_VOX_LEFT] = Max(leftSrc[i], maxDist[AG_VOX_LEFT]);
		maxDist[AG_VOX_RIGHT] = Max(rightSrc[i], maxDist[AG_VOX_RIGHT]);
		maxDist[AG_VOX_FRONT] = Max(frontSrc[i], maxDist[AG_VOX_FRONT]);
		maxDist[AG_VOX_BACK] = Max(backSrc[i], maxDist[AG_VOX_BACK]);
	}

	return topSrc;
}

bool agBuildDistanceFields(agCompactVolumefield &compact)
{
	agTimeVal startTime = agGetPerformanceTimer();

	u16 *dist0 = rage_new u16[compact.voxelCount];
	if(!dist0)
	{
		if(agGetLog())
		{
			agGetLog()->log(AG_LOG_ERROR, "agDBuildDistanceField: out of memory 'dist0' (voxelCount:%d)", compact.voxelCount);
		}
		return false;
	}

	u16 *dist1 = rage_new u16[compact.voxelCount];
	if(!dist1)
	{
		if(agGetLog())
		{
			agGetLog()->log(AG_LOG_ERROR, "agBuildDistanceFields: out of memory 'dist1' (voxelCount: %d)", compact.voxelCount); 
		}
		delete [] dist0;
		delete [] dist1;
		return false;
	}

	u16 *dist2 = rage_new u16[compact.voxelCount];
	if(!dist2)
	{
		if(agGetLog())
		{
			agGetLog()->log(AG_LOG_ERROR, "agBuildDistanceFields: out of memory 'dist2' (voxelCount: %d)", compact.voxelCount); 
		}
		delete [] dist0;
		delete [] dist1;
		delete [] dist2;
		return false;
	}

	u16 *dist3 = rage_new u16[compact.voxelCount];
	if(!dist3)
	{
		if(agGetLog())
		{
			agGetLog()->log(AG_LOG_ERROR, "agBuildDistanceFields: out of memory 'dist3' (voxelCount: %d)", compact.voxelCount); 
		}
		delete [] dist0;
		delete [] dist1;
		delete [] dist2;
		delete [] dist3;
		return false;
	}

	u16 *dist4 = rage_new u16[compact.voxelCount];
	if(!dist4)
	{
		if(agGetLog())
		{
			agGetLog()->log(AG_LOG_ERROR, "agBuildDistanceFields: out of memory 'dist4' (voxelCount: %d)", compact.voxelCount); 
		}
		delete [] dist0;
		delete [] dist1;
		delete [] dist2;
		delete [] dist3;
		delete [] dist4;
		return false;
	}

	u16 *dist5 = rage_new u16[compact.voxelCount];
	if(!dist5)
	{
		if(agGetLog())
		{
			agGetLog()->log(AG_LOG_ERROR, "agBuildDistanceFields: out of memory 'dist5' (voxelCount: %d)", compact.voxelCount); 
		}
		delete [] dist0;
		delete [] dist1;
		delete [] dist2;
		delete [] dist3;
		delete [] dist4;
		delete [] dist5;
		return false;
	}

	u16 *topSrc = dist0;
	u16 *botSrc = dist1;
	u16 *leftSrc = dist2;
	u16 *rightSrc = dist3;
	u16 *frontSrc = dist4;
	u16 *backSrc = dist5;

	agTimeVal distStartTime = agGetPerformanceTimer();

	u16 maxDist[AG_NUM_FACES] = {0};
	calculateDistanceField(compact, topSrc,  maxDist, botSrc, leftSrc, rightSrc, frontSrc, backSrc);


	for(int i = 0; i < AG_NUM_FACES; ++i)
	{
		compact.maxDistance[i] = maxDist[i];
	}
	agTimeVal distEndTime = agGetPerformanceTimer();

	//agTimeVal blurStartTime = agGetPerformanceTimer();

	//Do blur...whatever that is
	
	//Store distance
	for(int i=0; i<compact.voxelCount; ++i)
	{
		compact.voxels[i].faces[AG_VOX_TOP].dist = topSrc[i];
		compact.voxels[i].faces[AG_VOX_BOTTOM].dist = botSrc[i];
		compact.voxels[i].faces[AG_VOX_LEFT].dist =  leftSrc[i];
		compact.voxels[i].faces[AG_VOX_RIGHT].dist = rightSrc[i];
		compact.voxels[i].faces[AG_VOX_FRONT].dist = frontSrc[i];
		compact.voxels[i].faces[AG_VOX_BACK].dist = backSrc[i];
	}

	delete [] dist0;
	delete [] dist1;
	delete [] dist2;
	delete [] dist3;
	delete [] dist4;
	delete [] dist5;

	agTimeVal endTime = agGetPerformanceTimer();

	if(agGetBuildTimes())
	{
		agGetBuildTimes()->buildDistanceField += agGetDeltaTimeUsec(startTime, endTime);
		agGetBuildTimes()->buildDistanceFieldDist += agGetDeltaTimeUsec(distStartTime, distEndTime);
	}
	return true;
}

bool agBuildRegions(agCompactVolumefield &UNUSED_PARAM(compact), int UNUSED_PARAM(minRegionSize), int UNUSED_PARAM(mergeRegionSize))
{
#if 0
	agTimeVal startTime = agGetPerformanceTimer();

	int w = compact.width;
	int l = compact.length;

	u16 *tmp1 = rage_new u16[compact.voxelCount*2];
	if(!tmp1)
	{
		if(agGetLog())
		{
			agGetLog()->log(AG_LOG_ERROR, "agBuildRegions: Out of memory 'tmp1' (voxelcount:%d).", compact.voxelCount);
		}
		return false;
	}

	u16 *tmp2 = rage_new u16[compact.voxelCount*2];
	if(!tmp2)
	{
		if(agGetLog())
		{
			agGetLog()->log(AG_LOG_ERROR, "agBuildRegions: out of memory 'tmp2' (voxelcount:%d).", compact.voxelCount);
		}
	}

	agTimeVal regStartTime = agGetPerformanceTimer();

	agIntArray stack(1024);
	agIntArray visited(1024);
#endif
	//memset(src, 0 sizeof(u16)*compact.voxelCount*2);
	return true;

}

///////HeightField regions


//Distance field  functions
static unsigned short* calculateDistanceField(agCompactHeightField& compact, unsigned short* src, unsigned short* UNUSED_PARAM(dst), unsigned short& maxDist)
{
	const int w = compact.width;
	const int h = compact.length;
	
	// Init distance and points.
//	for (int i = 0; i < compact.spanCount; ++i)
//		src[i] = 0xffff;
	
	memset(src, 0xff, sizeof(u16)*compact.spanCount);
	
	// Mark boundary cells.
	for (int y = 0; y < h; ++y)
	{
		for (int x = 0; x < w; ++x)
		{
			const agCompactCell& cell = compact.cells[x+y*w];
			for (int i = (int)cell.index, ni = (int)(cell.index+cell.count); i < ni; ++i)
			{
				const agCompactSpan& span = compact.spans[i];
				int neighbourCount = 0;
				for (int dir = 0; dir < 4; ++dir)
				{
					if (agGetCon(span, dir) != 0xf)
						neighbourCount++;
				}
				if (neighbourCount != 4)
					src[i] = 0;
			}
		}
	}

	
	// Pass 1
	for (int y = 0; y < h; ++y)
	{
		for (int x = 0; x < w; ++x)
		{
			const agCompactCell& cell = compact.cells[x+y*w];
			for (int i = (int)cell.index, endi = (int)(cell.index+cell.count); i < endi; ++i)
			{
				const agCompactSpan& span = compact.spans[i];
				
				if (agGetCon(span, 0) != 0xf)
				{
					// (-1,0)
					const int ax = x + agGetDirOffsetX(0);
					const int ay = y + agGetDirOffsetY(0);
					const int ai = (int)compact.cells[ax+ay*w].index + agGetCon(span, 0);
					const agCompactSpan& aspan = compact.spans[ai];
					if (src[ai]+2 < src[i])
						src[i] = src[ai]+2;
					
					// (-1,-1)
					if (agGetCon(aspan, 3) != 0xf)
					{
						const int aax = ax + agGetDirOffsetX(3);
						const int aay = ay + agGetDirOffsetY(3);
						const int aai = (int)compact.cells[aax+aay*w].index + agGetCon(aspan, 3);
						if (src[aai]+3 < src[i])
							src[i] = src[aai]+3;
					}
				}
				if (agGetCon(span, 3) != 0xf)
				{
					// (0,-1)
					const int ax = x + agGetDirOffsetX(3);
					const int ay = y + agGetDirOffsetY(3);
					const int ai = (int)compact.cells[ax+ay*w].index + agGetCon(span, 3);
					const agCompactSpan& aspan = compact.spans[ai];
					if (src[ai]+2 < src[i])
						src[i] = src[ai]+2;
					
					// (1,-1)
					if (agGetCon(aspan, 2) != 0xf)
					{
						const int aax = ax + agGetDirOffsetX(2);
						const int aay = ay + agGetDirOffsetY(2);
						const int aai = (int)compact.cells[aax+aay*w].index + agGetCon(aspan, 2);
						if (src[aai]+3 < src[i])
							src[i] = src[aai]+3;
					}
				}
			}
		}
	}
	
	// Pass 2
	for (int y = h-1; y >= 0; --y)
	{
		for (int x = w-1; x >= 0; --x)
		{
			const agCompactCell& cell = compact.cells[x+y*w];
			for (int i = (int)cell.index, endi = (int)(cell.index+cell.count); i < endi; ++i)
			{
				const agCompactSpan& span = compact.spans[i];
				
				if (agGetCon(span, 2) != 0xf)
				{
					// (1,0)
					const int ax = x + agGetDirOffsetX(2);
					const int ay = y + agGetDirOffsetY(2);
					const int ai = (int)compact.cells[ax+ay*w].index + agGetCon(span, 2);
					const agCompactSpan& aspan = compact.spans[ai];
					if (src[ai]+2 < src[i])
						src[i] = src[ai]+2;
					
					// (1,1)
					if (agGetCon(aspan, 1) != 0xf)
					{
						const int aax = ax + agGetDirOffsetX(1);
						const int aay = ay + agGetDirOffsetY(1);
						const int aai = (int)compact.cells[aax+aay*w].index + agGetCon(aspan, 1);
						if (src[aai]+3 < src[i])
							src[i] = src[aai]+3;
					}
				}
				if (agGetCon(span, 1) != 0xf)
				{
					// (0,1)
					const int ax = x + agGetDirOffsetX(1);
					const int ay = y + agGetDirOffsetY(1);
					const int ai = (int)compact.cells[ax+ay*w].index + agGetCon(span, 1);
					const agCompactSpan& aspan = compact.spans[ai];
					if (src[ai]+2 < src[i])
						src[i] = src[ai]+2;
					
					// (-1,1)
					if (agGetCon(aspan, 0) != 0xf)
					{
						const int aax = ax + agGetDirOffsetX(0);
						const int aay = ay + agGetDirOffsetY(0);
						const int aai = (int)compact.cells[aax+aay*w].index + agGetCon(aspan, 0);
						if (src[aai]+3 < src[i])
							src[i] = src[aai]+3;
					}
				}
			}
		}
	}	
	
	maxDist = 0;
	for (int i = 0; i < compact.spanCount; ++i)
		maxDist = Max(src[i], maxDist);
	
	return src;
	
}


static unsigned short* boxBlur(agCompactHeightField& compact, int thr,
							   unsigned short* src, unsigned short* dst)
{
	const int w = compact.width;
	const int h = compact.length;
	
	thr *= 2;
	
	for (int y = 0; y < h; ++y)
	{
		for (int x = 0; x < w; ++x)
		{
			const agCompactCell& cell = compact.cells[x+y*w];
			for (int i = (int)cell.index, endi = (int)(cell.index+cell.count); i < endi; ++i)
			{
				const agCompactSpan& span = compact.spans[i];
				int cd = (int)src[i];
				if (cd <= thr)
				{
					dst[i] = (u16)cd;
					continue;
				}

				int d = cd;
				for (int dir = 0; dir < 4; ++dir)
				{
					if (agGetCon(span, dir) != 0xf)
					{
						const int ax = x + agGetDirOffsetX(dir);
						const int ay = y + agGetDirOffsetY(dir);
						const int ai = (int)compact.cells[ax+ay*w].index + agGetCon(span, dir);
						d += (int)src[ai];
						
						const agCompactSpan& aspan = compact.spans[ai];
						const int dir2 = (dir+1) & 0x3;
						if (agGetCon(aspan, dir2) != 0xf)
						{
							const int ax2 = ax + agGetDirOffsetX(dir2);
							const int ay2 = ay + agGetDirOffsetY(dir2);
							const int ai2 = (int)compact.cells[ax2+ay2*w].index + agGetCon(aspan, dir2);
							d += (int)src[ai2];
						}
						else
						{
							d += cd;
						}
					}
					else
					{
						d += cd*2;
					}
				}
				dst[i] = (unsigned short)((d+5)/9);
			}
		}
	}
	return dst;
}


bool agBuildDistanceField(agCompactHeightField &compact)
{
	agTimeVal startTime = agGetPerformanceTimer();
	
	unsigned short* dist0 = rage_new unsigned short[compact.spanCount];
	if (!dist0)
	{
		if (agGetLog())
			agGetLog()->log(AG_LOG_ERROR, "agBuildDistanceField: Out of memory 'dist0' (%d).", compact.spanCount);
		return false;
	}
	unsigned short* dist1 = rage_new unsigned short[compact.spanCount];
	if (!dist1)
	{
		if (agGetLog())
			agGetLog()->log(AG_LOG_ERROR, "agBuildDistanceField: Out of memory 'dist1' (%d).", compact.spanCount);
		delete [] dist0;
		return false;
	}
	
	unsigned short* src = dist0;
	unsigned short* dst = dist1;

	unsigned short maxDist = 0;

	agTimeVal distStartTime = agGetPerformanceTimer();
	
	calculateDistanceField(compact, src, dst, maxDist);
	
	compact.maxDistance = maxDist;
	
	agTimeVal distEndTime = agGetPerformanceTimer();
	
	agTimeVal blurStartTime = agGetPerformanceTimer();
	
	// Blur
	if (boxBlur(compact, 1, src, dst) != src)
		agSwap(src, dst);
	
	// Store distance.
	for (int i = 0; i < compact.spanCount; ++i)
		compact.spans[i].dist = src[i];
	
	agTimeVal blurEndTime = agGetPerformanceTimer();
	
	delete [] dist0;
	delete [] dist1;
	
	agTimeVal endTime = agGetPerformanceTimer();
	
/*	if (rcGetLog())
	{
		rcGetLog()->log(RC_LOG_PROGRESS, "Build distance field: %.3f ms", rcGetDeltaTimeUsec(startTime, endTime)/1000.0f);
		rcGetLog()->log(RC_LOG_PROGRESS, " - dist: %.3f ms", rcGetDeltaTimeUsec(distStartTime, distEndTime)/1000.0f);
		rcGetLog()->log(RC_LOG_PROGRESS, " - blur: %.3f ms", rcGetDeltaTimeUsec(blurStartTime, blurEndTime)/1000.0f);
	}*/
	if (agGetBuildTimes())
	{
		agGetBuildTimes()->buildDistanceField += agGetDeltaTimeUsec(startTime, endTime);
		agGetBuildTimes()->buildDistanceFieldDist += agGetDeltaTimeUsec(distStartTime, distEndTime);
		agGetBuildTimes()->buildDistanceFieldBlur += agGetDeltaTimeUsec(blurStartTime, blurEndTime);
	}
	
	return true;
}

//Region functions

static void addUniqueFloorRegion(agRegion& reg, unsigned short n)
{
	for (int i = 0; i < reg.floors.size(); ++i)
		if (reg.floors[i] == n)
			return;
	reg.floors.push(n);
}

static bool isSolidEdge(agCompactHeightField& compact, unsigned short* src,
						int x, int y, int i, int dir)
{
	const agCompactSpan& span = compact.spans[i];
	unsigned short r = 0;
	if (agGetCon(span, dir) != 0xf)
	{
		const int ax = x + agGetDirOffsetX(dir);
		const int ay = y + agGetDirOffsetY(dir);
		const int ai = (int)compact.cells[ax+ay*compact.width].index + agGetCon(span, dir);
		r = src[ai*2];
	}
	if (r == src[i*2])
		return false;
	return true;
}

static void walkContour(int x, int y, int i, int dir,
						agCompactHeightField& compact,
						unsigned short* src,
						agIntArray& cont)
{
	int startDir = dir;
	int starti = i;

	const agCompactSpan& stSpan = compact.spans[i];
	unsigned short curReg = 0;
	if (agGetCon(stSpan, dir) != 0xf)
	{
		const int ax = x + agGetDirOffsetX(dir);
		const int ay = y + agGetDirOffsetY(dir);
		const int ai = (int)compact.cells[ax+ay*compact.width].index + agGetCon(stSpan, dir);
		curReg = src[ai*2];
	}
	cont.push(curReg);
			
	int iter = 0;
	while (++iter < 40000)
	{
		const agCompactSpan& span = compact.spans[i];
		
		if (isSolidEdge(compact, src, x, y, i, dir))
		{
			// Choose the edge corner
			unsigned short r = 0;
			if (agGetCon(span, dir) != 0xf)
			{
				const int ax = x + agGetDirOffsetX(dir);
				const int ay = y + agGetDirOffsetY(dir);
				const int ai = (int)compact.cells[ax+ay*compact.width].index + agGetCon(span, dir);
				r = src[ai*2];
			}
			if (r != curReg)
			{
				curReg = r;
				cont.push(curReg);
			}
			
			dir = (dir+1) & 0x3;  // Rotate CW
		}
		else
		{
			int ni = -1;
			const int nx = x + agGetDirOffsetX(dir);
			const int ny = y + agGetDirOffsetY(dir);
			if (agGetCon(span, dir) != 0xf)
			{
				const agCompactCell& ncell = compact.cells[nx+ny*compact.width];
				ni = (int)ncell.index + agGetCon(span, dir);
			}
			if (ni == -1)
			{
				// Should not happen.
				Assertf(ni >=0, "Sanity check: should never fail to get neighbour on a contour");
				return;
			}
			x = nx;
			y = ny;
			i = ni;
			dir = (dir+3) & 0x3;	// Rotate CCW
		}
		
		if (starti == i && startDir == dir)
		{
			break;
		}
	}

	// Remove adjacent duplicates.
	if (cont.size() > 1)
	{
		for (int i = 0; i < cont.size(); )
		{
			int ni = (i+1) % cont.size();
			if (cont[i] == cont[ni])
			{
				for (int j = i; j < cont.size()-1; ++j)
					cont[j] = cont[j+1];
				cont.pop();
			}
			else
				++i;
		}
	}
}

static bool isRegionConnectedToBorder(const agRegion& reg)
{
	// Region is connected to border if
	// one of the neighbours is null id.
	for (int i = 0; i < reg.connections.size(); ++i)
	{
		if (reg.connections[i] == 0)
			return true;
	}
	return false;
}

static bool canMergeWithRegion(agRegion& reg, unsigned short id)
{
	int n = 0;
	//If the regions have multiple connections we can't merge
	for (int i = 0; i < reg.connections.size(); ++i)
	{
		if (reg.connections[i] == id)
			n++;
	}
	if (n > 1)
		return false;
	//If the region overlaps vertically we can't merge
	for (int i = 0; i < reg.floors.size(); ++i)
	{
		if (reg.floors[i] == id)
			return false;
	}
	return true;
}

static void removeAdjacentNeighbours(agRegion& reg)
{
	// Remove adjacent duplicates.
	for (int i = 0; i < reg.connections.size() && reg.connections.size() > 1; )
	{
		int ni = (i+1) % reg.connections.size();
		if (reg.connections[i] == reg.connections[ni])
		{
			// Remove duplicate
			for (int j = i; j < reg.connections.size()-1; ++j)
				reg.connections[j] = reg.connections[j+1];
			reg.connections.pop();
		}
		else
			++i;
	}
}

static bool mergeRegions(agRegion& rega, agRegion& regb)
{
	unsigned short aid = rega.id;
	unsigned short bid = regb.id;
	
	// Duplicate current neighbourhood.
	agIntArray acon;
	acon.resize(rega.connections.size());
	for (int i = 0; i < rega.connections.size(); ++i)
		acon[i] = rega.connections[i];
	agIntArray& bcon = regb.connections;
	
	// Find insertion point on A.
	int insa = -1;
	for (int i = 0; i < acon.size(); ++i)
	{
		if (acon[i] == bid)
		{
			insa = i;
			break;
		}
	}
	if (insa == -1)
		return false;
	
	// Find insertion point on B.
	int insb = -1;
	for (int i = 0; i < bcon.size(); ++i)
	{
		if (bcon[i] == aid)
		{
			insb = i;
			break;
		}
	}
	if (insb == -1)
		return false;
	
	// Merge neighbours.
	rega.connections.resize(0);
	for (int i = 0, ni = acon.size(); i < ni-1; ++i)
		rega.connections.push(acon[(insa+1+i) % ni]);
		
	for (int i = 0, ni = bcon.size(); i < ni-1; ++i)
		rega.connections.push(bcon[(insb+1+i) % ni]);
	
	removeAdjacentNeighbours(rega);
	
	for (int j = 0; j < regb.floors.size(); ++j)
		addUniqueFloorRegion(rega, (u16)regb.floors[j]);
	rega.count += regb.count;
	regb.count = 0;
	regb.connections.resize(0);

	return true;
}

static void replaceNeighbour(agRegion& reg, unsigned short oldId, unsigned short newId)
{
	bool neiChanged = false;
	for (int i = 0; i < reg.connections.size(); ++i)
	{
		if (reg.connections[i] == oldId)
		{
			reg.connections[i] = newId;
			neiChanged = true;
		}
	}
	for (int i = 0; i < reg.floors.size(); ++i)
	{
		if (reg.floors[i] == oldId)
			reg.floors[i] = newId;
	}
	if (neiChanged)
		removeAdjacentNeighbours(reg);
}

static bool filterSmallRegions(int minRegionSize, int mergeRegionSize,
							   unsigned short& maxRegionId,
							   agCompactHeightField& compact,
							   unsigned short* src)
{
	const int w = compact.width;
	const int l = compact.length;

	int nreg = maxRegionId+1;
	agRegion* regions = rage_new agRegion[nreg];
	if (!regions)
	{
		if (agGetLog())
			agGetLog()->log(AG_LOG_ERROR, "filterSmallRegions: Out of memory 'regions' (%d).", nreg);
		return false;
	}
	
	for (int i = 0; i < nreg; ++i)
		regions[i].id = (unsigned short)i;

	// Find edge of a region and find connections around the contour.
	for (int y = 0; y < l; ++y)
	{
		for (int x = 0; x < w; ++x)
		{
			const agCompactCell& cell = compact.cells[x+y*w];
			for (int i = (int)cell.index, endi = (int)(cell.index+cell.count); i < endi; ++i)
			{
				unsigned short r = src[i*2];
				if (r == 0 || r >= nreg)
					continue;
				
				agRegion& reg = regions[r];
				reg.count++;
				

				// Update floors.
				for (int j = (int)cell.index; j < endi; ++j)
				{
					if (i == j) continue;
					unsigned short floorId = src[j*2];
					if (floorId == 0 || floorId >= nreg)
						continue;
					addUniqueFloorRegion(reg, floorId);
				}
				
				// Have found contour
				if (reg.connections.size() > 0)
					continue;
				
				// Check if this cell is next to a border.
				int ndir = -1;
				for (int dir = 0; dir < 4; ++dir)
				{
					if (isSolidEdge(compact, src, x, y, i, dir))
					{
						ndir = dir;
						break;
					}
				}
				
				if (ndir != -1)
				{
					// The cell is at border.
					// Walk around the contour to find all the neighbours.
					walkContour(x, y, i, ndir, compact, src, reg.connections);
				}
			}
		}
	}
	
	// Remove too small unconnected regions.
	for (int i = 0; i < nreg; ++i)
	{
		agRegion& reg = regions[i];
		if (reg.id == 0 || (reg.id & AG_BORDER_REG))
			continue;			
		if (reg.count == 0)
			continue;
		
		if (reg.connections.size() == 1 && reg.connections[0] == 0)
		{
			if (reg.count < minRegionSize)
			{
				// Non-connected small region, remove.
				reg.count = 0;
				reg.id = 0;
			}
		}
	}
		
		
	// Merge too small regions to neighbour regions.
	int mergeCount = 0 ;
	do
	{
		mergeCount = 0;
		for (int i = 0; i < nreg; ++i)
		{
			agRegion& reg = regions[i];
			if (reg.id == 0 || (reg.id & AG_BORDER_REG))
				continue;			
			if (reg.count == 0)
				continue;
				
			// Check to see if the region should be merged.
			if (reg.count > mergeRegionSize && isRegionConnectedToBorder(reg))
				continue;
				
			// Small region with more than 1 connection.
			// Or region which is not connected to a border at all.
			// Find smallest neighbour region that connects to this one.
			int smallest = 0xfffffff;
			unsigned short mergeId = reg.id;
			for (int j = 0; j < reg.connections.size(); ++j)
			{
				if (reg.connections[j] & AG_BORDER_REG) continue;
				agRegion& mreg = regions[reg.connections[j]];
				if (mreg.id == 0 || (mreg.id & AG_BORDER_REG)) continue;
				if (mreg.count < smallest &&
					canMergeWithRegion(reg, mreg.id) &&
					canMergeWithRegion(mreg, reg.id))
				{
					smallest = mreg.count;
					mergeId = mreg.id;
				}
			}
			// Found new id.
			if (mergeId != reg.id)
			{
				unsigned short oldId = reg.id;
				agRegion& target = regions[mergeId];
				
				// Merge neighbours.
				if (mergeRegions(target, reg))
				{
					// Fixup regions pointing to current region.
					for (int j = 0; j < nreg; ++j)
					{
						if (regions[j].id == 0 || (regions[j].id & AG_BORDER_REG)) continue;
						// If another region was already merged into current region
						// change the nid of the previous region too.
						if (regions[j].id == oldId)
							regions[j].id = mergeId;
						// Replace the current region with the new one if the
						// current regions is neighbour.
						replaceNeighbour(regions[j], oldId, mergeId);
					}
					mergeCount++;
				}
			}
		}
	}
	while (mergeCount > 0);

	// Compress region Ids.
	for (int i = 0; i < nreg; ++i)
	{
		regions[i].remap = false;
		if (regions[i].id == 0) continue;	// Skip nil regions.
		if (regions[i].id & AG_BORDER_REG) continue;	// Skip external regions.
		regions[i].remap = true;
	}

	unsigned short regIdGen = 0;
	for (int i = 0; i < nreg; ++i)
	{
		if (!regions[i].remap)
			continue;
		unsigned short oldId = regions[i].id;
		unsigned short newId = ++regIdGen;
		for (int j = i; j < nreg; ++j)
		{
			if (regions[j].id == oldId)
			{
				regions[j].id = newId;
				regions[j].remap = false;
			}
		}
	}
	maxRegionId = regIdGen;
		
	// Remap regions.
	for (int i = 0; i < compact.spanCount; ++i)
	{
		if ((src[i*2] & AG_BORDER_REG) == 0)
			src[i*2] = regions[src[i*2]].id;
	}

	bool ret = true;
	
	//Prepare compact regions which will be used to setup audio connections
	compact.regions = rage_new agCompactRegion[maxRegionId+1];
	if(!compact.regions)
	{
		if(agGetLog())
		{
			agGetLog()->log(AG_LOG_ERROR, "agFilterSmallRegions: Out of memory 'compact.regions' (%d)", maxRegionId+1);
		}
		ret = false;
	}
	else
	{
		memset(compact.regions, 0, sizeof(agCompactRegion)*maxRegionId+1);
		for(int i = 0; i < (maxRegionId+1); ++i)
		{
			compact.regions[i].id = (u16)i; 
		}
	}

	delete [] regions;
	
	return ret;
}

static bool floodRegion(int x, int y, int i,
						unsigned short level, unsigned short minLevel, unsigned short r,
						agCompactHeightField& compact,
						unsigned short* src,
						agIntArray& stack)
{
	const int w = compact.width;
	
	// Flood fill mark region.
	stack.resize(0);
	stack.push((int)x);
	stack.push((int)y);
	stack.push((int)i);
	src[i*2] = r;
	src[i*2+1] = 0;
	
	unsigned short lev = level >= minLevel+2 ? level-2 : minLevel;
	int count = 0;
	
	while (stack.size() > 0)
	{
		int ci = stack.pop();
		int cy = stack.pop();
		int cx = stack.pop();
		
		const agCompactSpan& span = compact.spans[ci];
		
		// Check if any of the neighbours already have a valid region set.
		unsigned short ar = 0;
		for (int dir = 0; dir < 4; ++dir)
		{
			// 8 connected
			if (agGetCon(span, dir) != 0xf)
			{
				const int ax = cx + agGetDirOffsetX(dir);
				const int ay = cy + agGetDirOffsetY(dir);
				const int ai = (int)compact.cells[ax+ay*w].index + agGetCon(span, dir);
				unsigned short nr = src[ai*2];
				if (nr != 0 && nr != r)
					ar = nr;
				
				const agCompactSpan& aspan = compact.spans[ai];
				
				const int dir2 = (dir+1) & 0x3;
				if (agGetCon(aspan, dir2) != 0xf)
				{
					const int ax2 = ax + agGetDirOffsetX(dir2);
					const int ay2 = ay + agGetDirOffsetY(dir2);
					const int ai2 = (int)compact.cells[ax2+ay2*w].index + agGetCon(aspan, dir2);
					
					unsigned short nr = src[ai2*2];
					if (nr != 0 && nr != r)
						ar = nr;
				}				
			}
		}
		if (ar != 0)
		{
			src[ci*2] = 0;
			continue;
		}
		count++;
		
		// Expand neighbours.
		for (int dir = 0; dir < 4; ++dir)
		{
			if (agGetCon(span, dir) != 0xf)
			{
				const int ax = cx + agGetDirOffsetX(dir);
				const int ay = cy + agGetDirOffsetY(dir);
				const int ai = (int)compact.cells[ax+ay*w].index + agGetCon(span, dir);
				if (compact.spans[ai].dist >= lev)
				{
					if (src[ai*2] == 0)
					{
						src[ai*2] = r;
						src[ai*2+1] = 0;
						stack.push(ax);
						stack.push(ay);
						stack.push(ai);
					}
				}
			}
		}
	}
	
	return count > 0;
}

static unsigned short* expandRegions(int maxIter, unsigned short level,
									 agCompactHeightField& compact,
									 unsigned short* src,
									 unsigned short* dst,
									 agIntArray& stack)
{
	const int w = compact.width;
	const int l = compact.length;

	// Find cells revealed by the raised level.
	stack.resize(0);
	for (int y = 0; y < l; ++y)
	{
		for (int x = 0; x < w; ++x)
		{
			const agCompactCell& cell = compact.cells[x+y*w];
			for (int i = (int)cell.index, endi = (int)(cell.index+cell.count); i < endi; ++i)
			{
				if (compact.spans[i].dist >= level && src[i*2] == 0)
				{
					stack.push(x);
					stack.push(y);
					stack.push(i);
				}
			}
		}
	}
	
	int iter = 0;
	while (stack.size() > 0)
	{
		int failed = 0;
		
		memcpy(dst, src, sizeof(unsigned short)*compact.spanCount*2);
		
		for (int j = 0; j < stack.size(); j += 3)
		{
			int x = stack[j+0];
			int y = stack[j+1];
			int i = stack[j+2];
			if (i < 0)
			{
				failed++;
				continue;
			}
			
			unsigned short r = src[i*2];
			unsigned short d2 = 0xffff;
			const agCompactSpan& span = compact.spans[i];
			for (int dir = 0; dir < 4; ++dir)
			{
				if (agGetCon(span, dir) == 0xf) continue;
				const int ax = x + agGetDirOffsetX(dir);
				const int ay = y + agGetDirOffsetY(dir);
				const int ai = (int)compact.cells[ax+ay*w].index + agGetCon(span, dir);
				if (src[ai*2] > 0 && (src[ai*2] & AG_BORDER_REG) == 0)
				{
					if ((int)src[ai*2+1]+2 < (int)d2)
					{
						r = src[ai*2];
						d2 = src[ai*2+1]+2;
					}
				}
			}
			if (r)
			{
				stack[j+2] = -1; // mark as used
				dst[i*2] = r;
				dst[i*2+1] = d2;
			}
			else
			{
				failed++;
			}
		}
		
		// agSwap source and dest.
		agSwap(src, dst);
		
		//Everything in the stack have been used so we're done here
		if (failed*3 == stack.size())
			break;
		
		//Check to see if we've reach our iteration limit
		if (level > 0)
		{
			++iter;
			if (iter >= maxIter)
				break;
		}
	}
	
	return src;
}



static void paintRectRegion(int minx, int maxx, int miny, int maxy,
							unsigned short regId, unsigned short minLevel,
							agCompactHeightField& compact, unsigned short* src)
{
	//Iterate through a rectangular section of cells and assing all spans the specified region id
	const int w = compact.width;
	for (int y = miny; y < maxy; ++y)
	{
		for (int x = minx; x < maxx; ++x)
		{
			const agCompactCell& cell = compact.cells[x+y*w];
			for (int i = (int)cell.index, endi = (int)(cell.index+cell.count); i < endi; ++i)
			{
				if (compact.spans[i].dist >= minLevel)
					src[i*2] = regId;
			}
		}
	}
}

//Perfoms the core test to decide if adjacent regions are audible
static void testAudability(agCompactHeightField &compact, agCompactSpan &span, agCompactRegion &reg, int x, int y, int dir)
{
	int w = compact.width;
	int l = compact.length;
	int ax = x + agGetDirOffsetX(dir);
	int ay = y + agGetDirOffsetY(dir);

	//If we're outside the bounding box, set flags for stitching and exit
	if(ax < 0 || ay <0 || ax >= w || ay >= l)
	{
		if(ax < 0)
		{
			span.flags |= AG_STITCH_0;
		}
		if(ay >= l)
		{
			span.flags |= AG_STITCH_1;
		}
		if(ax >= w)
		{
			span.flags |= AG_STITCH_2;
		}
		if(ay < 0)
		{
			span.flags |= AG_STITCH_3;
		}
		return;
	}

	//Get the adjacent cell in the given direction
	agCompactCell &ncell = compact.cells[ax +ay*w];

	//Search through all the spans on the cell
	for(u32 j = ncell.index, endj = ncell.index+ncell.count; j<endj; ++j)
	{
		agCompactSpan &nspan = compact.spans[j];
		if(!nspan.reg)
		{
			//This span has no region (e.g. it's on an edge) but can potentially transmit audibility so we iterate
			if((nspan.y < (span.y+span.height)) && ((nspan.y+nspan.height) > span.y))
			{
				testAudability(compact, nspan, reg, ax, ay, dir);
			}
		}
		else if(nspan.reg != reg.id) //We only care if it's a new region
		{
			//Check if the span height overlaps and if it does add it to 
			if((nspan.y < (span.y+span.height)) && ((nspan.y+nspan.height) > span.y))
			{
				bool alreadySet = false;
				for(int k=0; k<reg.audConnections.size(); ++k)
				{
					if(reg.audConnections[k] == nspan.reg)
					{
						alreadySet = true;
						break;
					}
				}
				if(!alreadySet)
				{
					reg.audConnections.push(nspan.reg);
				}
			}
		}
	} 
}


static bool setupRegionAudability(agCompactHeightField& compact)
{
	if(!compact.regions)
	{
		Assertf(compact.regions, "trying to setupRegionAudability but compact doesn't have any regions");
		return false;
	}	

	int w = compact.width;
	int l = compact.length;
	u32 maxConnections = 0, totalConnections = 0;
	for(int y=0; y<l; ++y)
	{
		for(int x=0; x<w; ++x)
		{
			agCompactCell &cell = compact.cells[x + y*w];
			for(u32 i= cell.index, endi = cell.index+cell.count; i<endi; ++i)
			{
				agCompactSpan &span = compact.spans[i];
				if(!span.reg)
				{
					continue;
				}
				agCompactRegion &reg = compact.regions[span.reg];

				for(int dir=0; dir<4; ++dir)
				{
					testAudability(compact, span, reg, x, y, dir);
				}
				
				if(reg.audConnections.size() > (int)maxConnections)
				{
					maxConnections = reg.audConnections.size();
				}
			}
		}
	}

	for(int i=0; i<compact.maxRegions; ++i)
	{
		totalConnections += compact.regions[i].audConnections.size();
	}
	compact.totalConnections = (u16)totalConnections;
	if(agGetLog())
	{
		agGetLog()->log(AG_LOG_PROGRESS, "Max number of connections is %i", maxConnections);
		agGetLog()->log(AG_LOG_PROGRESS, "Total number of connections is %i", totalConnections);
	}
	return true;
}



bool agBuildRegions(agCompactHeightField& compact,
					int walkableRadius, int UNUSED_PARAM(borderSize),
					int minRegionSize, int mergeRegionSize)
{
	agTimeVal startTime = agGetPerformanceTimer();
	
	const int w = compact.width;
	const int l = compact.length;
	
	unsigned short* tmp1 = rage_new unsigned short[compact.spanCount*2];
	if (!tmp1)
	{
		if (agGetLog())
			agGetLog()->log(AG_LOG_ERROR, "agBuildDistanceField: Out of memory 'tmp1' (%d).", compact.spanCount*2);
		return false;
	}
	unsigned short* tmp2 = rage_new unsigned short[compact.spanCount*2];
	if (!tmp2)
	{
		if (agGetLog())
			agGetLog()->log(AG_LOG_ERROR, "agBuildDistanceField: Out of memory 'tmp2' (%d).", compact.spanCount*2);
		delete [] tmp1;
		return false;
	}
	
	agTimeVal regStartTime = agGetPerformanceTimer();
	
	agIntArray stack(1024);
	agIntArray visited(1024);
	
	unsigned short* src = tmp1;
	unsigned short* dst = tmp2;
	
	memset(src, 0, sizeof(unsigned short) * compact.spanCount*2);
	
	unsigned short regionId = 1;
	unsigned short level = (u16)((compact.maxDistance+1) & ~1);
	
	unsigned short minLevel = (unsigned short)(walkableRadius*2);
	
	const int expandIters = 4 + walkableRadius * 2;

//	borderSize = 1;
//	// Mark border regions. TODO: How will this work with stitching sections together? what's the process there?
//	paintRectRegion(0, borderSize, 0, l, regionId|AG_BORDER_REG, minLevel, compact, src); regionId++;
//	paintRectRegion(w-borderSize, w, 0, l, regionId|AG_BORDER_REG, minLevel, compact, src); regionId++;
//	paintRectRegion(0, w, 0, borderSize, regionId|AG_BORDER_REG, minLevel, compact, src); regionId++;
//	paintRectRegion(0, w, l-borderSize, l, regionId|AG_BORDER_REG, minLevel, compact, src); regionId++;



	agTimeVal expTime = 0;
	agTimeVal floodTime = 0;
	
	//Perform watershed processing
	while (level > minLevel)
	{
		level = level >= 2 ? level-2 : 0;
		
		agTimeVal expStartTime = agGetPerformanceTimer();
		
		// Expand current regions until no empty connected cells found.
		if (expandRegions(expandIters, level, compact, src, dst, stack) != src)
			agSwap(src, dst);
		
		expTime += agGetPerformanceTimer() - expStartTime;
		
		agTimeVal floodStartTime = agGetPerformanceTimer();
		
		// Mark new regions with IDs.
		for (int y = 0; y < l; ++y)
		{
			for (int x = 0; x < w; ++x)
			{
				const agCompactCell& cell = compact.cells[x+y*w];
				for (int i = (int)cell.index, endi = (int)(cell.index+cell.count); i < endi; ++i)
				{
					if (compact.spans[i].dist < level || src[i*2] != 0)
						continue;
					
					if (floodRegion(x, y, i, minLevel, level, regionId, compact, src, stack))
						regionId++;
				}
			}
		}
		
		floodTime += agGetPerformanceTimer() - floodStartTime;
		
	}
	
	// Expand current regions until no empty connected cells found.
	if (expandRegions(expandIters*8, minLevel, compact, src, dst, stack) != src)
		agSwap(src, dst);
	
	agTimeVal regEndTime = agGetPerformanceTimer();
	
	agTimeVal filterStartTime = agGetPerformanceTimer();
	
	// Filter out small regions.
	compact.maxRegions = regionId;
	if (!filterSmallRegions(minRegionSize, mergeRegionSize, compact.maxRegions, compact, src))
		return false;
	
	agTimeVal filterEndTime = agGetPerformanceTimer();
	
	// Write the result out.
	for (int i = 0; i < compact.spanCount; ++i)
		compact.spans[i].reg = src[i*2];
	//Generate audio region 
	if(!setupRegionAudability(compact))
	{
		return false;
	}
	
	delete [] tmp1;
	delete [] tmp2;
	
	agTimeVal endTime = agGetPerformanceTimer();
	
	if (agGetBuildTimes())
	{
		agGetBuildTimes()->buildRegions += agGetDeltaTimeUsec(startTime, endTime);
		agGetBuildTimes()->buildRegionsReg += agGetDeltaTimeUsec(regStartTime, regEndTime);
		agGetBuildTimes()->buildRegionsExp += agGetDeltaTimeUsec(0, expTime);
		agGetBuildTimes()->buildRegionsFlood += agGetDeltaTimeUsec(0, floodTime);
		agGetBuildTimes()->buildRegionsFilter += agGetDeltaTimeUsec(filterStartTime, filterEndTime);
	}
		
	return true;
}
	
