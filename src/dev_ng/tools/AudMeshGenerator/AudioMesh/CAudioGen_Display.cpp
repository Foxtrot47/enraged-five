/////////////////////////////////////////////////////////////////////////////////
// FILE		: CAudioGen_Display.cpp
// PURPOSE	: To form connections between level geometry audio meshes.
// AUTHOR	: C. Walder (based on Recast by Mikko Menonen)
// STARTED	: 31/08/2009
///////////////////////////////////////////////////////////////////////////////
//

#include "system/xtl.h"

#include <float.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "CAudioGen_Display.h"
#include "AudioGenLog.h"
#include "AudioGenTimer.h"
#include "AudioGenDebugDraw.h"

#include "Contrib/Recast/imgui.h"
#include "Contrib/sdl/include/SDL.h"
#include "Contrib/sdl/include/SDL_opengl.h"

extern int g_MaxNumSectors;


CAudioGen_Display::CAudioGen_Display():
	m_verts(NULL),
	m_nverts(0),
	m_tris(NULL),
	m_trinorms(NULL),
	m_ntris(0),
	m_xsector(-1),
	m_ysector(-1),
	m_mesh(0),
	m_drawMode(DRAWMODE_MESH),
	m_path(0),
	m_meshName(0)
{
	m_nmeshes[0] = 0; m_nmeshes[1] = 0; m_nmeshes[2] = 0; m_nmeshes[3] = 0;
	m_path = rage_new char[128];
	m_meshName = rage_new char[128];
	ResetSettings();
}

CAudioGen_Display::~CAudioGen_Display()
{
	cleanup();
	delete [] m_path;
	m_path = 0;
	delete [] m_meshName;
	m_meshName = 0;
}

void CAudioGen_Display::cleanup()
{
	delete m_mesh;
	m_mesh = 0;
	delete m_nmeshes[0];
	m_nmeshes[0] = 0;
	delete m_nmeshes[1];
	m_nmeshes[1] = 0;
	delete m_nmeshes[2];
	m_nmeshes[2] = 0;
	delete m_nmeshes[3];
	m_nmeshes[3] = 0;
}

void CAudioGen_Display::ResetSettings()
{
	m_xsector = -1;
	m_ysector = -1;
	memset(m_bmin, 0, sizeof(float)*3);
	memset(m_bmax, 0, sizeof(float)*3);
	memset(m_path, 0, sizeof(char)*128);
	memset(m_meshName, 0, sizeof(char)*128);
}

void CAudioGen_Display::HandleRender()
{
	if(!m_verts || ! m_tris || !m_trinorms )
	{
		return;
	}

	//Draw mesh
	if(m_drawMode == DRAWMODE_MESH)
	{
		agDebugDrawMesh(m_verts, m_nverts, m_tris, m_trinorms, m_ntris, 0);
	}

	glDisable(GL_FOG);
	glDepthMask(GL_FALSE);


	//Draw bounds
	float col[4] = {1, 1, 1, 0.5};
	agDebugDrawBoxWire(m_bmin[0], m_bmin[1], m_bmin[2], m_bmax[0], m_bmax[1], m_bmax[2], col);


	glDepthMask(GL_TRUE);

	if(m_mesh && m_drawMode == DRAWMODE_POLYMESH)
	{
		agDebugDrawMesh(m_verts, m_nverts, m_tris, m_trinorms, m_ntris, 0);
		glDepthMask(GL_FALSE);
		agDebugDrawPolyMesh(*m_mesh);
		glDepthMask(GL_TRUE);
	} 

	if(m_mesh && (m_drawMode == DRAWMODE_NEIGHBOURS))
	{
		agDebugDrawMesh(m_verts, m_nverts, m_tris, m_trinorms, m_ntris, 0);
		glDepthMask(GL_FALSE);
		agDebugDrawPolyMesh(*m_mesh);
		if(m_nmeshes[0])
			agDebugDrawPolyMesh(*m_nmeshes[0]);
		if(m_nmeshes[1])
			agDebugDrawPolyMesh(*m_nmeshes[1]);
		if(m_nmeshes[2])
			agDebugDrawPolyMesh(*m_nmeshes[2]);
		if(m_nmeshes[3])
			agDebugDrawPolyMesh(*m_nmeshes[3]);
		glDepthMask(GL_TRUE);
	}
 
	if(m_mesh && m_drawMode == DRAWMODE_STITCH)
	{
		agDebugDrawMesh(m_verts, m_nverts, m_tris, m_trinorms, m_ntris, 0);
		glDepthMask(GL_FALSE);
		agDebugDrawStitchedPolys(*m_mesh);
		if(m_nmeshes[0])
			agDebugDrawPolyMesh(*m_nmeshes[0]);
		if(m_nmeshes[1])
			agDebugDrawPolyMesh(*m_nmeshes[1]);
		if(m_nmeshes[2])
			agDebugDrawPolyMesh(*m_nmeshes[2]);
		if(m_nmeshes[3])
			agDebugDrawPolyMesh(*m_nmeshes[3]);
		agDebugDrawAudMeshStitching(*m_mesh, m_nmeshes);
		glDepthMask(GL_TRUE);
	} 

	if(m_mesh && m_drawMode == DRAWMODE_STITCH_REGS)
	{
		agDebugDrawMesh(m_verts, m_nverts, m_tris, m_trinorms, m_ntris, 0);
		glDepthMask(GL_FALSE);
		agDebugDrawRegsMesh(*m_mesh);
		if(m_nmeshes[0])
			agDebugDrawRegsMesh(*m_nmeshes[0]);
		if(m_nmeshes[1])
			agDebugDrawRegsMesh(*m_nmeshes[1]);
		if(m_nmeshes[2])
			agDebugDrawRegsMesh(*m_nmeshes[2]);
		if(m_nmeshes[3])
			agDebugDrawRegsMesh(*m_nmeshes[3]);
		agDebugDrawAudMeshStitching(*m_mesh, m_nmeshes);
		glDepthMask(GL_TRUE);
	}
}

void CAudioGen_Display::HandleRenderOverlay()
{
	//Nothing to see here.....(would be the toolRender Overlay if there was one)
}

void CAudioGen_Display::HandleMeshHasChanged(const float* verts, int nverts, 
		const int* tris, const float* trinorms,
		int ntris, const float* bmin, const float* bmax, const char * path, char * meshName)
{
	m_verts = verts;
	m_nverts = nverts;
	m_tris = tris;
	m_trinorms = trinorms;
	m_ntris = ntris;
	vcopy(m_bmin, bmin);
	vcopy(m_bmax, bmax);
	FastAssert(path);
	FastAssert(meshName);
	strcpy(m_path, path);
	strcpy(m_meshName, meshName);
}

void CAudioGen_Display::HandleSettings()
{
	//Add in stitching settings

//	imguiLabel("Rasterization");
//	imguiSlider("Cell Size", &m_cellSize, 0.1f, 10.f, 0.01f);
//	imguiSlider("Cell height", &m_cellHeight, 0.1f, 10.f, 0.01f);
//
//	imguiSeparator();
	
}

bool CAudioGen_Display::HandleBuild()
{
	if(!m_verts || !m_tris)
	{
		if(agGetLog())
		{
			agGetLog()->log(AG_LOG_ERROR, "Audio Display: Input mesh is not specified.");
		}
		return false;
	}

	cleanup();

	//Pull the sector indices out of the geometry file name
	sscanf(m_meshName, "navmesh[%d][%d].tri", &m_xsector, &m_ysector);

	//Check our main mesh is in the level bounds (it almost certaily is if we've got this far)
	if(m_xsector < 0 || m_xsector > g_MaxNumSectors || m_ysector <0 || m_ysector > g_MaxNumSectors)
	{
		if(agGetLog())
		{
			agGetLog()->log(AG_LOG_ERROR, "Audio Stitch: geometry sectors are out of bounds, check the filename is coming through ok");
		}
		return false;
	}

	//Load up the main mesh file
	char meshFile[256] = {0};
	formatf(meshFile, "%s/audmesh[%d][%d].iam", m_path, m_xsector, m_ysector);

	m_mesh = rage_new agPolyMesh();
	Assertf(m_mesh, "Audio Stitch: out of memory creating main polymesh");
	if(!m_mesh) return false;

	if(!agLoadSurfaceBinary(meshFile, *m_mesh))
	{
		//Couldn't load the surface so let's bail
		delete m_mesh; m_mesh = 0;
		return false;
	}

	//Load in the neighbour meshes 
	char nmeshFile[256] = {0};
	for(int dir=0; dir<4; ++dir)
	{
		int xsec = m_xsector + GetXSectorOffset(dir);
		int ysec = m_ysector + GetYSectorOffset(dir);
		if(xsec<0 || xsec>g_MaxNumSectors || ysec<0 || ysec>g_MaxNumSectors)
		{
			continue;
		}

		m_nmeshes[dir] = rage_new agPolyMesh();
		Assertf(m_nmeshes[dir], "Audio Stitch: out of memory creating neighbour polymesh %d", dir);
		if(!m_nmeshes) return false;
		

		formatf(nmeshFile, "%s/audmesh[%d][%d].iam", m_path, xsec, ysec);

		if(!agLoadSurfaceBinary(nmeshFile, *m_nmeshes[dir]))
		{
			//Couldn't load neighbour polymesh...there probably isn't one so clean up
			delete m_nmeshes[dir]; 
			m_nmeshes[dir] = 0;
		}
	}

	return true;	
}

void CAudioGen_Display::HandleDebugMode()
{
	//Check which modes are valid
	bool valid[MAX_DRAWMODE] = {0};

	if(m_verts && m_tris)
	{
		valid[DRAWMODE_MESH] = true;
		valid[DRAWMODE_POLYMESH] = m_mesh !=0;
		valid[DRAWMODE_NEIGHBOURS] = m_mesh !=0;
		valid[DRAWMODE_STITCH] = m_mesh != 0 && m_mesh->regFlags != 0;
		valid[DRAWMODE_STITCH_REGS] = m_mesh != 0 && m_mesh->regFlags != 0;
	}

	int unavail = 0;
	for(int i = 0; i < MAX_DRAWMODE; ++i)
	{
		if(!valid[i]) unavail++;
	}

	if(unavail == MAX_DRAWMODE)
	{
		return;
	}

	imguiLabel("Draw");

	if(imguiCheck("Input Mesh", m_drawMode == DRAWMODE_MESH, valid[DRAWMODE_MESH]))
	{
		m_drawMode = DRAWMODE_MESH;
	}
	if(imguiCheck("Polymesh", m_drawMode == DRAWMODE_POLYMESH, valid[DRAWMODE_POLYMESH]))
	{
		m_drawMode = DRAWMODE_POLYMESH;
	}
	if(imguiCheck("Polymesh neighbours", m_drawMode == DRAWMODE_NEIGHBOURS, valid[DRAWMODE_NEIGHBOURS]))
	{
		m_drawMode = DRAWMODE_NEIGHBOURS;
	}
	if(imguiCheck("Stitching", m_drawMode == DRAWMODE_STITCH, valid[DRAWMODE_STITCH]))
	{
		m_drawMode = DRAWMODE_STITCH;
	}
	if(imguiCheck("Stitching Regs", m_drawMode == DRAWMODE_STITCH_REGS, valid[DRAWMODE_STITCH_REGS]))
	{
		m_drawMode = DRAWMODE_STITCH_REGS;
	}
}









