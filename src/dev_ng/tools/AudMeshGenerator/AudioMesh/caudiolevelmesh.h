
#ifndef AUD_LEVELMESH
#define AUD_LEVELMESH

#include "fwnavgen/toolnavmesh.h"

#include "vector\vector3.h"

class CAudioLevelMesh
{
public:
	CAudioLevelMesh();
	~CAudioLevelMesh();

	bool Load(const char* inputFileName, bool isMainMesh = true);
	inline const float* GetNormals() const { return m_normals; }
	inline const float* GetVerts() const { return m_verts; }
	inline const int* GetTris() const { return m_tris; }
	inline int GetVertCount() const { return m_vertCount; }
	inline int GetTriCount() const { return m_triCount; }
	inline const float * GetBMin() const { return &m_bmin[0]; }
	inline const float * GetBMax() const { return &m_bmax[0]; }

private:

	void AddVertex(float x, float y, float z, int& cap);
	void AddTriangle(int a, int b, int c, int &cap);
	void AddNormal(float x, float y, float z, int &cap);


	Vector3 m_bmin;
	Vector3 m_bmax;
	float *m_verts;
	int *m_tris;
	float *m_normals;
	int m_vertCount;
	int m_triCount;
	int m_normalCount;
	int m_vertexCap;
	int m_triCap;
	int m_normalCap;
};

#endif

