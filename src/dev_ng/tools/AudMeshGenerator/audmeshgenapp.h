#ifndef NAVMESHGENERATOR_NAVMESHGENAPP_H
#define NAVMESHGENERATOR_NAVMESHGENAPP_H

//#include "fwnavgen/toolnavmesh.h"

#include "fwnavgen/basetool.h"
#include "fwsys/gameskeleton.h"

//-----------------------------------------------------------------------------

class CAudMeshScene
{
public:
	static void		Init					(unsigned initMode);
//	static void		Shutdown				(unsigned shutdownMode);

	static void		Update					();
};

//-----------------------------------------------------------------------------

class CLevelProcessToolGameInterfaceGta : public fwLevelProcessToolGameInterface
{
public:
	virtual bool InitGame();
	virtual void ShutdownGame();

	virtual void Update1();
	virtual void Update2();

protected:
	void InitForNavMeshExport();
	void InitLevelForNavMeshExport(int level);

	void RegisterGameSkeletonFunctions();
	void RegisterCoreInitFunctions();
	void RegisterBeforeMapLoadedInitFunctions();
	void RegisterAfterMapLoadedInitFunctions();
	void RegisterSessionInitFunctions();

	gameSkeleton	m_NavGenSkeleton;
};

//-----------------------------------------------------------------------------

#endif	// NAVMESHGENERATOR_NAVMESHGENAPP_H
