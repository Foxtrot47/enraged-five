#include "materialinterface.h"
#include "fwnavgen/datatypes.h"
#include "fragment/instance.h"
#include "objects/object.h"
#include "physics/gtaMaterialManager.h"
#include "scene/Entity.h"
#include "vehicleAI/pathfind.h"

//-----------------------------------------------------------------------------

bool CNavMeshMaterialInterfaceGta::GetIsBakedInAttribute1(Vector3 * UNUSED_PARAM(pTriVerts), phMaterialMgr::Id UNUSED_PARAM(iSurfaceType))
{
	return false;
}

bool CNavMeshMaterialInterfaceGta::GetIsBakedInAttribute2(Vector3 * UNUSED_PARAM(pTriVerts), phMaterialMgr::Id UNUSED_PARAM(iSurfaceType))
{
	return false;
}

bool CNavMeshMaterialInterfaceGta::GetIsBakedInAttribute3(Vector3 * UNUSED_PARAM(pTriVerts), phMaterialMgr::Id UNUSED_PARAM(iSurfaceType))
{
	return false;
}

bool CNavMeshMaterialInterfaceGta::GetIsBakedInAttribute4(Vector3 * UNUSED_PARAM(pTriVerts), phMaterialMgr::Id UNUSED_PARAM(iSurfaceType))
{
	return false;
}

bool CNavMeshMaterialInterfaceGta::GetIsBakedInAttribute5(Vector3 * UNUSED_PARAM(pTriVerts), phMaterialMgr::Id UNUSED_PARAM(iSurfaceType))
{
	return false;
}

bool CNavMeshMaterialInterfaceGta::GetIsBakedInAttribute6(Vector3 * UNUSED_PARAM(pTriVerts), phMaterialMgr::Id UNUSED_PARAM(iSurfaceType))
{
	return false;
}

bool CNavMeshMaterialInterfaceGta::GetIsBakedInAttribute7(Vector3 * UNUSED_PARAM(pTriVerts), phMaterialMgr::Id UNUSED_PARAM(iSurfaceType))
{
	return false;
}

bool CNavMeshMaterialInterfaceGta::GetIsBakedInAttribute8(Vector3 * UNUSED_PARAM(pTriVerts), phMaterialMgr::Id UNUSED_PARAM(iSurfaceType))
{
	return false;
}

bool CNavMeshMaterialInterfaceGta::IsThisSurfaceTypeGlass(phMaterialMgr::Id iSurfaceType)
{
	return PGTAMATERIALMGR->GetIsGlass(iSurfaceType);
}

bool CNavMeshMaterialInterfaceGta::IsThisSurfaceTypeStairs(phMaterialMgr::Id iSurfaceType)
{
	const bool bStairs = PGTAMATERIALMGR->GetPolyFlagStairs(iSurfaceType);
	return bStairs;
}

bool CNavMeshMaterialInterfaceGta::IsThisSurfaceTypePavement(phMaterialMgr::Id iSurfaceType)
{
	// New-style pavement markup which is done per-poly, using a few special per-poly bits
	// stored in the iSurfaceType value.
	bool bPerPolyPavement = PGTAMATERIALMGR->GetPolyFlagWalkablePath(iSurfaceType);
	if(bPerPolyPavement)
		return true;

	VfxGroup_e iSurfaceGroup = PGTAMATERIALMGR->GetMtlVfxGroup(iSurfaceType);
	if(iSurfaceGroup == VFXGROUP_PAVING)
		return true;

	return false;
}

bool CNavMeshMaterialInterfaceGta::IsThisSurfaceTypeRoad(Vector3 * pTriVerts, phMaterialMgr::Id UNUSED_PARAM(iSurfaceType))
{
	bool b = ThePaths.CollisionTriangleIntersectsRoad(pTriVerts, true);
	return b;
	//return PGTAMATERIALMGR->GetIsRoad(iSurfaceType);
}

bool CNavMeshMaterialInterfaceGta::IsThisSurfaceTypeSeeThrough(phMaterialMgr::Id iSurfaceType)
{
	bool b = PGTAMATERIALMGR->GetIsSeeThrough(iSurfaceType);
	return b;
}

bool CNavMeshMaterialInterfaceGta::IsThisSurfaceTypeShootThrough(phMaterialMgr::Id iSurfaceType)
{
	bool b = PGTAMATERIALMGR->GetIsShootThrough(iSurfaceType);
	return b;
}

bool CNavMeshMaterialInterfaceGta::IsThisSurfaceAllowedForNetworkRespawning(phMaterialMgr::Id iSurfaceType)
{
	bool b = (PGTAMATERIALMGR->GetPolyFlagNoNetworkSpawn(iSurfaceType)==false);
	return b;
}

bool CNavMeshMaterialInterfaceGta::IsThisSurfaceWalkable(Vector3 * UNUSED_PARAM(pTriVerts), phMaterialMgr::Id iSurfaceType)
{
	if(PGTAMATERIALMGR->GetPolyFlagNoNavmesh(iSurfaceType))
		return false;

	return true;
}

// This returns the ped density associated with the surface.
// NOTE : iSurface type is not a full 32 bits of index.  In fact some of these 32 bits are used
// to represent other per-polygon data.  This is to support new stuff such as per-poly ped density.
// In order to use this in tandem with the original material-based ped density, we'll for now
// take the max of the two ped-density values obtained with both old & new methods.
float CNavMeshMaterialInterfaceGta::GetPedDensityForThisSurfaceType(phMaterialMgr::Id iSurfaceType)
{
	int iVal = PGTAMATERIALMGR->UnpackPedDensity(iSurfaceType);

	// "paving_slabs" material shall default to a medium ped-density (3)
	// for now.  Once out-sourced roads are back in the studio we will
	// turn this off, and map artists will need to set it manually
	// via the packed ped-density stored in the collision polys.
	VfxGroup_e iSurfaceGroup = PGTAMATERIALMGR->GetMtlVfxGroup(iSurfaceType);
	if(iSurfaceGroup == VFXGROUP_PAVING)
	{
		if(iVal > 0)
			return (float)iVal;
		else
			return 3.0f;
	}
	
	return (float)iVal;
}

u32 CNavMeshMaterialInterfaceGta::GetDynamicEntityInfoFlags(const fwEntity * pEntity)
{
	u32 iFlags = 0;

	fragInst * pFragInst = pEntity->GetFragInst();
	const bool bHasUprootLimit = (pFragInst!=NULL && (pFragInst->GetTypePhysics()->GetMinMoveForce() > 0.0f) && !((CObject*)pEntity)->m_nObjectFlags.bHasBeenUprooted);
	if( bHasUprootLimit )
	{
		iFlags |= fwDynamicEntityInfo::FLAG_UPROOTABLE_OBJECT;
	}
	CBaseModelInfo * pModelInfo = CModelInfo::GetBaseModelInfo(pEntity->GetModelId());
	if(pModelInfo && pModelInfo->GetIsClimbableByAI())
	{
		iFlags |= fwDynamicEntityInfo::FLAG_CLIMABABLE_BY_AI;
	}

	return iFlags;
}

//-----------------------------------------------------------------------------
