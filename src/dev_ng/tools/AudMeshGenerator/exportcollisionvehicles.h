#ifndef NAVMESHGENERATOR_EXPORTCOLLISIONVEHICLES_H
#define NAVMESHGENERATOR_EXPORTCOLLISIONVEHICLES_H

#include "entity/archetypemanager.h"

class CAudMeshDataExporterTool;
class CVehicleModelInfo;

//-----------------------------------------------------------------------------

namespace CNavMeshVehicleDataFunctions
{
	//***********************************************************************
	//	Export the collision geometry for all vehicles in the game which are
	//	large enough to require a navmesh - boats, etc.
	//***********************************************************************
	void ExportCollisionForAllVehicles(CAudMeshDataExporterTool &exporter, const char *pOutputPath);

	void ExportCollisionForVehicle(CAudMeshDataExporterTool &exporter, fwModelId iModelId, const char *pOutputPath);

	struct VehModelInfo
	{
		CVehicleModelInfo * m_pModelInfo;
		fwModelId m_ModelId;
		char * m_pModelName;
	};
}

//-----------------------------------------------------------------------------

#endif	// NAVMESHGENERATOR_EXPORTCOLLISIONVEHICLES_H
