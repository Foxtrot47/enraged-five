#include "system/xtl.h"

#include "AudioMeshMaker.h"
#include "ai/navmesh/navmeshextents.h"
#include "phcore/materialmgrimpl.h"
#include "phcore/surface.h"
#include "system/FileMgr.h"
#include "system/Param.h"
//#include "GameDataFolder.h"
#include "CAudioMeshMaker.h"

#include "fwnavgen/NavMeshMaker.h"

#include <time.h>
#include <direct.h>

#ifdef _DEBUG
#include <crtdbg.h>
#endif

//#define USE_STOCK_ALLOCATOR	// include before "system/main.h"
//#include "system/main.h"

char logFileName[256];

// Which mode this has been invoked as
int g_Mode = -1;
// The name of a command file which will be used
char * g_CommandFileName = NULL;
// Create audio mesh
bool g_AudioMesh = false;
// Audio mesh params file
const char * g_AudioMeshParamsFile = NULL;

vector<TAudioMeshParams*> g_SourceAudioMeshes;

// An input path to be prepended to all input file-names
const char * g_InputPath = NULL;
// An output path to be prepended to all output file-names
const char * g_OutputPath = NULL;
// This external is defined in GameDataFolder.h, it is set via the -gamedatafolder cmdline parameter
// and is required so that the physics material manager can load the game's material types in
char * g_pGameFolder = _strdup("X:\\gta\\build");

char g_LogFilePrefix[64];

// This file is used to log compilation info to
FILE * g_pLogFile = NULL;
// Not sure why this was needed
char *XEX_TITLE_ID = NULL;

int g_MaxNumSectors = 300;


PARAM(audiomeshgen, "Tells the audio mesh maker to run its generate phase");
PARAM(audiomeshstitch, "Tells the audio mesh maker to run its stitch phase");
PARAM(audioMeshParams, "File containing parameters for audio mesh generation and stitching");
PARAM(inputpath, "Path to directory containing exported geometry files");
PARAM(deleteidentical, "Used to delete identical .iam files between two directories");
PARAM(outputpath, "Path to build to, will use input path if not present");
PARAM(copyintosubfolders, "Copy the stitched files into subfolders to be turned into rpfs");

TAudioMeshParams::~TAudioMeshParams()
{
	if(m_pFileName)
		free(m_pFileName);
}

void
CAudioMeshMakerApp::OutputText(char * pText)
{
	printf(pText);

	// Print out to g_pLogFile - but not the g_pBlankLine's..
	if(g_pLogFile)
	{
		if(*pText != '\r')
		{
			fprintf(g_pLogFile, pText);
		}
		else
		{
			fprintf(g_pLogFile, "\n");
		}
		fflush(g_pLogFile);
	}

	//OutputDebugString(pText);
}

void LogArgs()
{
	if(g_pLogFile)
	{
		int argc = sysParam::GetArgCount();
		char ** argv = sysParam::GetArgArray();

		fprintf(g_pLogFile, "--------------------------------------------------------------------\n");
		fprintf(g_pLogFile, "NumArgs : %i\n", argc);

		for(int a=0; a<argc; a++)
		{
			fprintf(g_pLogFile, "Args %i: %s\n", a, argv[a]);
		}

		fprintf(g_pLogFile, "--------------------------------------------------------------------\n");
	}
}

int AudMeshMakerMain()
{
	/*int argc = sysParam::GetArgCount();
	char ** argv = sysParam::GetArgArray();*/

#if __DEV && !__OPTIMIZED
	// Enable the CRT to debug memory leak info at exit
	_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
#endif

	// We have to make sure that there is a valid working directory.  When NavMeshMaker is run, it
	// should ideally have a full working directory specified in the args parsed below.
	_chdir(g_pGameFolder);

	// Parse the parameters passed to the program
	/*if(!CAudioMeshMakerApp::ParseParams(argc, argv))
	{
		Displayf("Failed to parse params (%d specified)", argc-1);
		return -1;
	}*/

	PARAM_audioMeshParams.Get(g_AudioMeshParamsFile);

	g_AudioMesh = true; 

	PARAM_inputpath.Get(g_InputPath);

	if(!g_OutputPath)
	{
		g_OutputPath = _strdup(g_InputPath);
	}

#if 0 //__DEV
	RAGE_LOG_DISABLE = 1;
#endif

//	CFileMgr::Initialise();

	CPathServerExtents::m_iNumSectorsPerNavMesh = 0;
	CAudioMeshMakerApp::ReadDatFile();

	if(CPathServerExtents::m_iNumSectorsPerNavMesh==0)
	{
		printf("g_iNumSectorsPerNavMesh==0\n");
		return -1;
	}

	CAudioMeshMakerApp::OutputText("\n");

	CAudioMeshMakerApp::InitMeshFilenames();

	if(g_CommandFileName)
	{
		if(!CAudioMeshMakerApp::ReadCmdFile())
		{
			CAudioMeshMakerApp::ClearData();
			return -1;
		}
	}

	if(PARAM_audiomeshgen.Get())
	{
		g_AudioMesh = false;
		sprintf(logFileName, "%s/%s%s", g_InputPath, g_LogFilePrefix, "audiomesh_log.txt");
		g_pLogFile = fopen(logFileName, "wt");
		CAudioMeshMakerApp::OutputText(logFileName);
		LogArgs();

		if(CAudioMeshMakerApp::GenerateAudioMeshes())
		{
			CAudioMeshMakerApp::ClearData();
			return -1;
		}
	}

	if(PARAM_audiomeshstitch.Get())
	{
		g_AudioMesh = false;
		sprintf(logFileName, "%s/%s%s", g_InputPath, g_LogFilePrefix, "audiostitch_log.txt");
		g_pLogFile = fopen(logFileName, "wt");
		CAudioMeshMakerApp::OutputText(logFileName);
		LogArgs();

		if(CAudioMeshMakerApp::StitchAudioMeshes())
		{
			CAudioMeshMakerApp::ClearData();
			return -1;
		}
	}

	if(PARAM_copyintosubfolders.Get())
	{
		g_AudioMesh = false;
		CAudioMeshMakerApp::CopyFilesIntoSubfoldersForRpfCreation("iam");
		return -1;
	}

	if(PARAM_deleteidentical.Get())
	{
		//CAudioMeshMakerApp::CompareAndDeleteIdenticalAudFiles(<directory1>, <directory2>);
	}

	if(g_AudioMesh)
	{
		CAudioMeshMaker audioMesh;
		return audioMesh.Main();
	}

	CAudioMeshMakerApp::ClearData();

	printf("Done!");
	return 1;
}



void
CAudioMeshMakerApp::ClearData(void)
{
	u32 i;

	g_Mode = -1;

	free(g_CommandFileName);
	g_CommandFileName = NULL;

	//free(g_InputPath);
	//g_InputPath = NULL;

	//free(g_OutputPath);
	//g_OutputPath = NULL;

	for(i=0; i<g_SourceAudioMeshes.size(); i++)
	{
		TAudioMeshParams * pAudioParams = g_SourceAudioMeshes[i];
		delete pAudioParams;
	}
	g_SourceAudioMeshes.clear();

	if(g_pLogFile)
	{
		fclose(g_pLogFile);
		g_pLogFile = NULL;
	}
}

//**************************************************************
// pGameDataPath will be something like "x:/gta5/build/dev"

void CAudioMeshMakerApp::ReadDatFile()
{
	char pathForNavDatFile[512];
	sprintf(pathForNavDatFile, "%s\\common\\data\\nav.dat", g_pGameFolder);

	FILE * pFile = fopen(pathForNavDatFile, "rt");
	if(!pFile)
	{
		CPathServerExtents::m_iNumSectorsPerNavMesh = 3;
		return;
	}

	char string[256];

	while(fgets(string, 256, pFile))
	{
		if(string[0] == '#')
			continue;

		int iNumMatched = 0;
		int iValue;

		// SECTORS_PER_NAVMESH defines the resolution which this game's navmesh system works at.
		iNumMatched = sscanf(string, "SECTORS_PER_NAVMESH = %i", &iValue);
		if(iNumMatched)
		{
			CPathServerExtents::m_iNumSectorsPerNavMesh = iValue;
			continue;
		}
	}
	CPathServerExtents::m_iNumSectorsPerNavMesh = 3;

	fclose(pFile);
}

//bool
//CAudioMeshMakerApp::ParseParams(int argc, char *argv[])
//{
//	// First argument is always the .exe itself
//	int argIndex = 1;
//
//	// Read in the arguments
//	while(argIndex < argc)
//	{
//		char * pArg = argv[argIndex];
//		if(!pArg)
//		{
//			break;
//		}
//
//		// Specifying -? or /? or /h or -h will show the usage
//		else if(stricmp(pArg, "-?")==0 || stricmp(pArg, "/?")==0 || stricmp(pArg, "-h")==0 || stricmp(pArg, "/?")==0)
//		{
//			ShowUsage();
//			return false;
//		}
//		// If specified, an input path is expected to follow
//		else if(stricmp(pArg, "-inputpath")==0)
//		{
//			// Read the path
//			argIndex++;
//			pArg = argv[argIndex];
//			if(!pArg || !*pArg)
//			{
//				OutputText("Error : -inputpath was used, but no path was found.\n");
//				return false;
//			}
//			free(g_InputPath);
//			g_InputPath = _strdup(pArg);
//		}
//		// If specified, an output path is expected to follow
//		else if(stricmp(pArg, "-outputpath")==0)
//		{
//			// Read the path
//			argIndex++;
//			pArg = argv[argIndex];
//			if(!pArg || !*pArg)
//			{
//				OutputText("Error : -outputpath was used, but no path was found.\n");
//				return false;
//			}
//			free(g_OutputPath);
//			g_OutputPath = _strdup(pArg);
//		}
//		// If specified, the path to the game's data folder is expected to follow
//		else if(stricmp(pArg, "-gamepath")==0)
//		{
//			// Read the path
//			argIndex++;
//			pArg = argv[argIndex];
//			if(!pArg || !*pArg)
//			{
//				OutputText("Error : -gamepath was used, but no path was found.\n");
//				return false;
//			}
//			free(g_pGameFolder);
//			g_pGameFolder = _strdup(pArg);
//
//			// We HAVE to set the current working directory to equal this path.  This is because
//			// the game now uses virtual relative devices to look for assert & this assumes that
//			// they are all relative to the current working directory.
//			_chdir(g_pGameFolder);
//		}
//		// If specified, then a text file is being used to detail all the files to process
//		else if(stricmp(pArg, "-cmdfile")==0)
//		{
//			// Read the command filename
//			argIndex++;
//			pArg = argv[argIndex];
//			if(!pArg || !*pArg)
//			{
//				OutputText("Error : -cmdfile was used, but no command file was specified.\n");
//				return false;
//			}
//			g_CommandFileName = _strdup(pArg);
//		}
//		else if(stricmp(pArg, "-audiomesh")==0)
//		{
//			g_Mode = GenerateAudMeshes;
//		}
//		else if(stricmp(pArg, "-audiostitch")==0)
//		{
//			g_Mode = StitchAudMeshes;
//		}
//		else if(stricmp(pArg, "-audioparamfile")==0)
//		{
//			// Read the path to the param file
//			argIndex++;
//			pArg = argv[argIndex];
//			if(!pArg || !*pArg)
//			{
//				OutputText("Error : -audioparamfile was used, but no path was found.\n");
//				return false;
//			}
//			free(g_AudioMeshParamsFile);
//			g_AudioMeshParamsFile = _strdup(pArg);
//		}
//
//		argIndex++;
//	}
//
//	return true;
//}

TAudioMeshParams * CAudioMeshMakerApp::InitAudioMeshParams(int iSectorX, int iSectorY, char * pAudioMeshName)
{
	TAudioMeshParams * pMeshParams = rage_new TAudioMeshParams();

	pMeshParams->m_iStartSectorX = iSectorX;
	pMeshParams->m_iStartSectorY = iSectorY;

	// Depending upon what MODE we're currently in, replace/append file extension with appropriate type
	char * pExt = "tri";
	char filename[256];

	if(g_InputPath)
	{
		if(pAudioMeshName)
		{
			sprintf_s(filename, "%s/%s.%s", g_InputPath, pAudioMeshName, pExt);
		}
		else
		{
			sprintf_s(filename, "%s/navmesh[%i][%i].%s", g_InputPath, pMeshParams->m_iStartSectorX, pMeshParams->m_iStartSectorY, pExt);
		}
	}
	else
	{
		if(pAudioMeshName)
		{
			sprintf_s(filename, "%s.%s", pAudioMeshName, pExt);
		}
		else
		{
			sprintf_s(filename, "navmesh[%i][%i].%s", pMeshParams->m_iStartSectorX, pMeshParams->m_iStartSectorY, pExt);
		}
	}

	pMeshParams->m_pFileName = _strdup(filename);

	return pMeshParams;
}

void
CAudioMeshMakerApp::ShowUsage(void)
{
	OutputText("AudioMeshMaker\n");
	OutputText("A command-line tool for creating audio meshes.\n");
	OutputText("(c) Rockstar North 2010, author Colin Walder\n");
	OutputText("\n");
}


void CAudioMeshMakerApp::InitMeshFilenames()
{
	int iNumX = CPathServerExtents::m_iNumNavMeshesInX;
	int iNumY = CPathServerExtents::m_iNumNavMeshesInY;

	for(int y=0; y<iNumY; y++)
	{
		for(int x=0; x<iNumX; x++)
		{
			TAudioMeshParams * pMeshParams = InitAudioMeshParams(x*CPathServerExtents::m_iNumSectorsPerNavMesh, y*CPathServerExtents::m_iNumSectorsPerNavMesh);
			g_SourceAudioMeshes.push_back(pMeshParams);
		}
	}
}

bool CAudioMeshMakerApp::ReadCmdFile()
{
	char textBuf[256];
	char fileNameBuf[256];

	FILE * filePtr = fopen(g_CommandFileName, "rt");

	if(!filePtr)
	{
		sprintf(textBuf, "Note : couldn't open command file \"%s\".\n", g_CommandFileName);
		OutputText(textBuf);
		return true;
	}

	do
	{
		char * pRet = fgets(fileNameBuf, 256, filePtr);
		if(!pRet)
			break;

		if(fileNameBuf[0]=='#' || fileNameBuf[0]=='\t' || fileNameBuf[0]==10)
			continue;

		// Find which navmesh this is
		int iX = -1, iY = -1;
		char * pIndexStart = strstr(fileNameBuf, "[");
		if(!pIndexStart)
			continue;

		int iNumScanned = sscanf(pIndexStart, "[%i][%i]", &iX, &iY);

		if(iNumScanned==2)
		{
			u32 n;
			for(n=0; n<g_SourceAudioMeshes.size(); n++)
			{
				TAudioMeshParams * pAudioParams = g_SourceAudioMeshes[n];
				if(pAudioParams->m_iStartSectorX==iX && pAudioParams->m_iStartSectorY==iY)
					break;
			}
			// If we don't already have this entry, then create a new one.
			if(n==g_SourceAudioMeshes.size())
			{
				TAudioMeshParams * pMeshParams = InitAudioMeshParams(iX, iY);
				g_SourceAudioMeshes.push_back(pMeshParams);
			}
		}

	} while(1);

	fclose(filePtr);
	return true;

}

bool CAudioMeshMakerApp::StitchAudioMeshes(void)
{
	if(!g_InputPath)
	{
		return false;
	}

	//int count = 0;

	for(u32 m=0; m<g_SourceAudioMeshes.size(); m++)
	{
		TAudioMeshParams * pAudioParams = g_SourceAudioMeshes[m];

		//TODO: delete old files

		CAudioMeshMaker audioMeshMaker;	

		char fileName[256] = {0};
		formatf(fileName, "navmesh[%i][%i].tri", pAudioParams->m_iStartSectorX, pAudioParams->m_iStartSectorY);

		audioMeshMaker.Main(fileName, g_AudioMeshParamsFile, "Audio mesh stitch", g_InputPath);
	}

	return true;
}



bool CAudioMeshMakerApp::GenerateAudioMeshes(void)
{
	if(!g_InputPath)
	{
		return false;
	}

	//int count = 0;

	for(u32 m=0; m<g_SourceAudioMeshes.size(); m++)
	{
		TAudioMeshParams * pAudioParams = g_SourceAudioMeshes[m];

		//TODO: delete old files

		CAudioMeshMaker audioMeshMaker;	

		char fileName[256] = {0};
		formatf(fileName, "navmesh[%i][%i].tri", pAudioParams->m_iStartSectorX, pAudioParams->m_iStartSectorY);

		audioMeshMaker.Main(fileName, g_AudioMeshParamsFile, "Audio mesh builder", g_InputPath); 
	}

	return true;
}




//*******************************************************************
//	CompareAndDeleteIdenticalAudFiles
//	Given a pFolder1 and pFolder2, this function will scan through
//	all files with the "inv" extension and will compare the CRCs
//	of files with the same filename.  If the CRCs are identical then
//	the "inv" file in the FIRST FOLDER will be deleted!
//	This is intended to be used for removing duplicate files from the
//	D/L content

bool CAudioMeshMakerApp::CompareAndDeleteIdenticalAudFiles(char * UNUSED_PARAM(pFolder1), char * UNUSED_PARAM(pFolder2))
{
	/*char searchString[MAX_PATH];
	sprintf(searchString, "%s/*.iam", pFolder2);

	WIN32_FIND_DATA findData;
	ZeroMemory(&findData, sizeof(WIN32_FIND_DATA));

	HANDLE h1 = FindFirstFile(searchString, &findData);
	if(h1==INVALID_HANDLE_VALUE)
		return false;

	while(h1!=INVALID_HANDLE_VALUE)
	{
		// We have found an "inv" file in folder 2
		// See if it also exists in folder 1.

		char * pFilename2 = (char*)findData.cFileName;

		if(pFilename2)
		{
			char fileName1[MAX_PATH];
			sprintf(fileName1, "%s/%s", pFolder1, pFilename2);
			char fileName2[MAX_PATH];
			sprintf(fileName2, "%s/%s", pFolder2, pFilename2);

			u32 iCRC2 = CalcFileCRC(fileName2);
			u32 iCRC1 = CalcFileCRC(fileName1);

			// CRCs are valid and are also identical.  We can delete the 1st file.
			if(iCRC1 && iCRC2 && (iCRC1==iCRC2))
			{
				DeleteFileA(fileName1);
			}
		}

		if(!FindNextFile(h1, &findData))
			break;
	}
*/
	return true;
}

void CAudioMeshMakerApp::CopyFilesIntoSubfoldersForRpfCreation(char * pFileExtension)
{
	static const int iNumSubFolders = 10;

	int s;

	//-----------------------------
	// Firstly create the folders

	for(s=0; s<iNumSubFolders; s++)
	{
		char folderName[512];
		sprintf(folderName, "%s/%i", g_OutputPath, s);

		BOOL bSuccess = CreateDirectory(folderName, NULL);
		if(!bSuccess)
			printf("Couldn't create directory \"%s\"\n", folderName);
	}

	//---------------------------------------------------------------------------------
	// Now count how many files of the given extension exist in the main output folder

	int iNumFiles = 0;

	char searchPath[512];
	sprintf(searchPath, "%s/*.%s", g_OutputPath, pFileExtension);
	WIN32_FIND_DATA findData;

	HANDLE hFile = FindFirstFile(searchPath, &findData);
	while(hFile != INVALID_HANDLE_VALUE)
	{
		// Only count and copy the files we care about, either the regular navmeshes
		// or the height meshes. Note that we could probably have used searchPath[] for
		// this, but having a separate function may be easier to maintain if we change
		// the naming convention.
		//if(ShouldCopyIntoSubfolder(findData.cFileName))
		{
			iNumFiles++;
		}

		if(!FindNextFile(hFile, &findData))
			break;
	}

	//-------------------------------------------
	// Now copy files into the subfolders

	int iNumPerFolder = (iNumFiles / iNumSubFolders) +1;
	int iCurrentFolder = 0;
	int iNumInThisFolder = 0;

	hFile = FindFirstFile(searchPath, &findData);
	while(hFile != INVALID_HANDLE_VALUE)
	{
		//if(ShouldCopyIntoSubfolder(findData.cFileName))
		{
			char oldFileName[512];
			sprintf(oldFileName, "%s/%s", g_OutputPath, findData.cFileName);
			char newFileName[512];
			sprintf(newFileName, "%s/%i/%s", g_OutputPath, iCurrentFolder, findData.cFileName);

			BOOL bMovedOk = CopyFile(oldFileName, newFileName, FALSE);
			if(!bMovedOk)
				printf("Couldn't copy file \"%s\" to \"%s\"\n", findData.cFileName, newFileName);

			iNumInThisFolder++;
			if(iNumInThisFolder >= iNumPerFolder)
			{
				iNumInThisFolder = 0;
				iCurrentFolder++;
				if(iCurrentFolder >= iNumSubFolders)
					iCurrentFolder = iNumSubFolders-1;
			}
		}

		if(!FindNextFile(hFile, &findData))
			break;
	}

}

