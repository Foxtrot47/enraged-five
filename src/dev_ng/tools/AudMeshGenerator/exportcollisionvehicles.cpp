#include "exportcollisionvehicles.h"
#include "exportcollision.h"

// Rage headers
#include "phbound/boundcomposite.h"

// Framework headers
#include "fwscene/stores/physicsstore.h"
#include "Vehicles/vehicle.h"
#include "Vehicles/VehicleFactory.h"

// Game headers
#include "scene/world/GameWorld.h"
#include "streaming/streaming.h"

using namespace std;

//-----------------------------------------------------------------------------

namespace CNavMeshVehicleDataFunctions
{
	int CompareModelInfosCB(const CNavMeshVehicleDataFunctions::VehModelInfo ** ppA, const CNavMeshVehicleDataFunctions::VehModelInfo ** ppB);
	void GetRelevantVehicleComponentBoundsForExport(CVehicle * pNewVehicle, vector<phBound*> & relevantParts);
}

//-----------------------------------------------------------------------------

atArray<CNavMeshVehicleDataFunctions::VehModelInfo*> g_vehicleModelInfos;

void EnumStoreFn(CVehicleModelInfo * mi)
{
	if(mi)
	{
		CNavMeshVehicleDataFunctions::VehModelInfo * pModelInf = rage_new CNavMeshVehicleDataFunctions::VehModelInfo();
		pModelInf->m_pModelInfo = mi;
		pModelInf->m_ModelId.Invalidate();

		CModelInfo::GetBaseModelInfoFromName(mi->GetModelName(), &pModelInf->m_ModelId);

		g_vehicleModelInfos.PushAndGrow(pModelInf);
	}
}

int CNavMeshVehicleDataFunctions::CompareModelInfosCB(const CNavMeshVehicleDataFunctions::VehModelInfo ** ppA, const CNavMeshVehicleDataFunctions::VehModelInfo ** ppB)
{
	return stricmp( (*ppA)->m_pModelInfo->GetModelName(), (*ppB)->m_pModelInfo->GetModelName() );
}


void CNavMeshVehicleDataFunctions::ExportCollisionForAllVehicles(CAudMeshDataExporterTool &exporter, const char *pOutputPath)
{
	Displayf("CNavMeshDataExporterTool::ExportCollisionForAllVehicles()\n");

	fwArchetypeDynamicFactory<CVehicleModelInfo>& vehModelInfoStore = CModelInfo::GetVehicleModelInfoStore();
	vehModelInfoStore.ForAllItemsUsed(EnumStoreFn);

	qsort(g_vehicleModelInfos.GetElements(), g_vehicleModelInfos.GetCount(), sizeof(CNavMeshVehicleDataFunctions::VehModelInfo*), (int (/*__cdecl*/ *)(const void*, const void*))CompareModelInfosCB);

	// Write the names of all the vehicles which have tri files, to a "vehicles.lst" file
	char listFilename[512];
	formatf(listFilename, "%s/vehicles.lst", pOutputPath);
	fiStream * pListFile = fiStream::Create(listFilename);

	// Now go through the list of vehicles, and export collision for those which require navmesh
	int i;
	for(i=0; i<g_vehicleModelInfos.GetCount(); i++)
	{
		fwModelId carModelId = g_vehicleModelInfos[i]->m_ModelId;
		if( carModelId.IsValid() )
		{
			CVehicleModelInfo * pVehModelInfo = g_vehicleModelInfos[i]->m_pModelInfo;
			
			//Ensure the vehicle should generate a nav mesh.
			if(pVehModelInfo->GetVehicleFlag(CVehicleModelInfoFlags::FLAG_GEN_NAVMESH))
			{
				//Read the nav mesh configuration parameters.
				float fMaxHeightChange = 0.35f;
				float fMinZDist = 0.5f;

				int iHandlingId = pVehModelInfo->GetHandlingId();
				CHandlingData * pHandling = CHandlingDataMgr::GetHandlingData(iHandlingId);
				Assert(pHandling);
				Assert(pHandling->m_pBoardingPoints);

				if( pHandling->m_pBoardingPoints )
				{
					fMaxHeightChange = Max(fMaxHeightChange, pHandling->m_pBoardingPoints->GetNavMaxHeightChange());
					fMinZDist = Max(fMinZDist, pHandling->m_pBoardingPoints->GetNavMinZDist());
				}

				if(pListFile)
					fprintf(pListFile, "[%s]        -numsamples 175 -maxheightchange %f -nodesminzdist %f\x0D\x0A", pVehModelInfo->GetModelName(), fMaxHeightChange, fMinZDist);

				Displayf("Exporting vehicle \'%s\'", pVehModelInfo->GetModelName());
				ExportCollisionForVehicle(exporter, carModelId, pOutputPath);
			}
		}
	}

	if(pListFile)
		pListFile->Close();
	pListFile = NULL;
}


void CNavMeshVehicleDataFunctions::ExportCollisionForVehicle(
		CAudMeshDataExporterTool &exporter, fwModelId iModelId, const char *outputPath)
{
	if( !iModelId.IsValid() )
		return;

	CVehicleModelInfo *pVehModelInfo = (CVehicleModelInfo *)CModelInfo::GetBaseModelInfo(iModelId);
	if(!pVehModelInfo)
		return;

	const char * pName = pVehModelInfo->GetModelName();
	if(!pName)
		return;

	bool bForceLoad = false;
	if(!CStreaming::HasObjectLoaded(iModelId.ConvertToStreamingIndex(), CModelInfo::GetStreamingModuleId()))
	{
		CStreaming::RequestObject(iModelId.ConvertToStreamingIndex(), CModelInfo::GetStreamingModuleId(), STRFLAG_FORCE_LOAD|STRFLAG_PRIORITY_LOAD);
		bForceLoad = true;
	}

	s32 nBoundIndex = pVehModelInfo->GetPhysicsDictionary();
	if(nBoundIndex > -1 && !g_PhysicsStore.HasObjectLoaded(nBoundIndex))
	{
		g_PhysicsStore.StreamingRequest(nBoundIndex, STRFLAG_FORCE_LOAD|STRFLAG_PRIORITY_LOAD);
		bForceLoad = true;
	}

	if(bForceLoad)
	{
		CStreaming::LoadAllRequestedObjects(true);
	}

	if(!CStreaming::HasObjectLoaded(iModelId.ConvertToStreamingIndex(), CModelInfo::GetStreamingModuleId()))
	{
		return;
	}

	Matrix34 tempMat;
	tempMat.Identity();

	CVehicle *pNewVehicle = CVehicleFactory::GetFactory()->Create(iModelId, ENTITY_OWNEDBY_NAVMESHEXPORTER, POPTYPE_TOOL, &tempMat);
	if(pNewVehicle)
	{
		pNewVehicle->SetIsAbandoned();

		CGameWorld::Add(pNewVehicle, CGameWorld::OUTSIDE );

		char filename[512];
		formatf(filename, "%s/%s.tri", outputPath, pName);

		exporter.ResetAndOpenTriFile(filename);

		vector<Vector3> triangleVertices;
		vector<phMaterialMgr::Id> triangleMaterials;
		vector<u16> colPolyFlags;

		vector<phBound*> relevantParts;
		GetRelevantVehicleComponentBoundsForExport(pNewVehicle, relevantParts);

		//ExportBound(pNewVehicle->GetPhysArch()->GetBound(), NULL, tempMat, triangleVertices, triangleMaterials, colPolyFlags, 0);

		for(u32 p=0; p<relevantParts.size(); p++)
		{
			phBound * pPart = relevantParts[p];
			exporter.ExportBound(pPart, NULL, tempMat, triangleVertices, triangleMaterials, colPolyFlags, EXPORTPOLYFLAG_VEHICLE);
		}

		exporter.FlushTriListToTriFile(triangleVertices, triangleMaterials, colPolyFlags);

		exporter.CloseTriFile(true);

		CGameWorld::Remove(pNewVehicle);
		CVehicleFactory::GetFactory()->Destroy(pNewVehicle);
		CStreaming::SetObjectIsDeletable(iModelId.ConvertToStreamingIndex(), CModelInfo::GetStreamingModuleId());
		//CStreaming::Update();
	}
}



void CNavMeshVehicleDataFunctions::GetRelevantVehicleComponentBoundsForExport(CVehicle * pNewVehicle, vector<phBound*> & relevantParts)
{
	phBound * pBaseBound = pNewVehicle->GetPhysArch()->GetBound();

	if(pNewVehicle->GetVehicleType()==VEHICLE_TYPE_BOAT)
	{
		Assert(pBaseBound->GetType()==phBound::COMPOSITE);
		if(pBaseBound->GetType()!=phBound::COMPOSITE)
		{
			relevantParts.push_back(pBaseBound);
			return;
		}

		phBoundComposite * pComposite = (phBoundComposite*)pBaseBound;

		// For boats, just add the main hull part which is identified with 
		const int iComponent = pNewVehicle->GetFragInst()->GetComponentFromBoneIndex(pNewVehicle->GetBoneIndex(VEH_BODYSHELL));
		if(iComponent!=-1)
		{
			phBound * pBodyshell = pComposite->GetBound(iComponent);
			if(pBodyshell)
			{
				relevantParts.push_back(pBodyshell);
				return;
			}
		}
	}

	// If that didn't work or this wasn't a boat, just add the composite bound
	relevantParts.push_back(pBaseBound);
}

//-----------------------------------------------------------------------------
