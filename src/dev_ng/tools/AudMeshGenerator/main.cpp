#define LARGE_BUDDY_HEAP

#if __WIN32PC

#define SIMPLE_HEAP_SIZE		(200*1024)
#define SIMPLE_PHYSICAL_SIZE	(500*1024)

#else

#error "Not supported for this platform.")

#endif	// __WIN32PC

//#include "system/xtl.h"

#include "audmeshgenapp.h"
#include "exportcollision.h"
#include "fwnavgen/config.h"
#include "parser/manager.h"
#include "system/FileMgr.h"
#include "physics/gtaArchetype.h"

#define PGKNOWNREFPOOLSIZE 32768	// Must be a multiple of 32. This overrides the size of pgBaseKnownReferencePool, which seems to be needed for this tool to work.

#include "fwnavgen/main.h"

//bool g_MemVizEnabled;

XPARAM(exportsectors);
XPARAM(exportall);
PARAM(noptfx, "disables particle effects");
PARAM(exportRegions, "exports meshes contained within specified game coordinates");

int AudMeshMakerMain();

namespace rage
{
	XPARAM(dontwarnonmissingmodule);
}

int Main()
{
	PARAM_noptfx.Set("");
	PARAM_dontwarnonmissingmodule.Set("");

	if(PARAM_exportsectors.Get() || PARAM_exportall.Get() || PARAM_exportRegions.Get())
	{
		fwLevelProcessToolImpl<CLevelProcessToolGameInterfaceGta, CAudMeshDataExporterTool> myApp;
		
		const u32 AUDIOMESH_EXPORT_COLLISON_ARCHETYPES = (ArchetypeFlags::GTA_MAP_TYPE_MOVER | ArchetypeFlags::GTA_OBJECT_TYPE | ArchetypeFlags::GTA_RIVER_TYPE | ArchetypeFlags::GTA_GLASS_TYPE);
		myApp.SetCollisionArchetypeFlags(AUDIOMESH_EXPORT_COLLISON_ARCHETYPES);
		
		myApp.Run();
	}
	else
	{
		PF_INIT_TIMEBARS_NO_ABSOLUTE("Main", 1260);
		PF_INIT_STARTUPBAR("Startup", 1260);


		// Load configuration data. INIT_PARSER needs to be called before we can do that,
		// and initializing CFileMgr allows us to use the type of file paths that we normally
		// use in this game ("common:/...", etc). In the -exportsectors/-exportall case above,
		// this is all done within the fwLevelProcessToolImpl::Run() call.
		CFileMgr::Initialise();
		INIT_PARSER;
		CAudMeshDataExporterTool::InitConfig();

		AudMeshMakerMain();

		CAudMeshDataExporterTool::ShutdownConfig();
		SHUTDOWN_PARSER;
		// No Shutdown() in CFileMgr?
		//	CFileMgr::Shutdown();
	}



#if __WIN32PC
	// Hack to work around various shutdown problems, such as systems that assert or crash
	// just from static objects being created and then destroyed again.
	//ExitProcess(0); 
	return 0;
#else
	return 0;
#endif
}
