#ifndef NAVMESHGENERATOR_EXPORTCOLLISION_H
#define NAVMESHGENERATOR_EXPORTCOLLISION_H

#include "fwscene/stores/mapdatastore.h"
#include "fwnavgen/baseexportcollisiongrid.h"
#include "fwutil/PtrList.h"
#include "pathserver/ExportCollision.h"
#include "scene/RegdRefTypes.h"
#include "templates/LinkList.h"

namespace rage
{
	class fiStream;
	class Vector2;
	class Vector3;
}

class CIplStore;
class CAudMeshDataExporterTool;
class CPathFind;

//-----------------------------------------------------------------------------

//*********************************************************************************
//
//	ExportCollision.h
//
//	The classes/functions in this file are concerned with exporting the collision
//	data from within the game, as small .bnd or .tri files.  These files can then
//	be read in by the NavMeshMaker tool to create the .nav navmeshes for the game.
//
//*********************************************************************************

class CAudMeshDataExporterInterfaceTool : public CNavMeshDataExporterInterface
{
public:
	explicit CAudMeshDataExporterInterfaceTool(CAudMeshDataExporterTool &tool);

	virtual bool EntityIntersectsCurrentNavMesh(CEntity* pEntity) const;

	virtual bool ExtentsIntersectCurrentNavMesh(Vec3V_In vMin, Vec3V_In vMax) const;

	virtual bool IsAlreadyInPhysicsLevel(CPhysical* pPhysical, phArchetype* pArchetype) const;

	//virtual fwPtrList & BuildActiveBuildingsList();

	void AppendToActiveBuildingsArray(atArray<CEntity*>& entityArray);

	virtual void AddSearches(atArray<fwBoxStreamerSearch>& searchList);

	CAudMeshDataExporterTool*	m_Tool;
};


class CAudMeshDataExporterTool : public fwExportCollisionGridTool
{
public:
	CAudMeshDataExporterTool();
	virtual ~CAudMeshDataExporterTool();

	// -- fwLevelProcessToolExporterInterface interface --

	virtual void PreInit();
	virtual void Init();
	virtual void Shutdown();

	virtual bool UpdateExport();

	// -- CAudMeshDataExporterTool interface --

	static void InitConfig();
	static void ShutdownConfig();

	bool EntityIntersectsCurrentNavMesh(CEntity * pEntity);
	bool ExtentsIntersectCurrentNavMesh(Vec3V_In vMin, Vec3V_In vMax);

	static bool IsAlreadyInPhysicsLevel(CPhysical * pPhysical, phArchetype * pArchetype=NULL);

	void ResetAndOpenTriFile(const char *pFilename);

	//fwPtrList & BuildActiveBuildingsList();

	void AppendToActiveBuildingsArray(atArray<CEntity*>& entityArray);


	void AddSearches(atArray<fwBoxStreamerSearch>& searchList);

protected:
	virtual void GridCellFinished();
	virtual void GridProcessFinished();
	virtual void UpdateStreamingPos(const Vector3 &pos);

	void NotifyMainGameLoopHasStarted();

	// Opportunity to set the default of IPL groups across the map
	void RequestDefaultIplGroups();

	//**********************************************************************
	//	Exports the .bnd files for every audmesh in the world.
	//	Each .bnd file will contain the geometry for a 2x2 block of sectors
	//**********************************************************************

	virtual bool StartCollisionExport();
	void OpenExportLog();

	void AddInteriorsToExportList(std::vector<CEntity*> & entitiesToExport /*, std::vector<fwSphere> & portalBoundaries*/);

	//	Called every frame when collision export is active.
	bool ProcessCollisionExport();

	void DisplayPoolUsage();

	void LaunchAudMeshCompiler();

	//****************************************************************************
	//	Exports a bound.  Calls the appropriate function.
	//****************************************************************************

	bool MaybeExportCollisionForEntity(CEntity * pEntity, const fragType * pFragType, Vector3 & vAudMeshMinsExtra, Vector3 & vNavMeshMaxsExtra, std::vector<Vector3> & triangleVertices, std::vector<phMaterialMgr::Id> & triangleMaterials, std::vector<u16> & colPolyFlags);
	static bool AddLadderFromMapCB(CEntity * pEntity, void * pData);

	static bool AddDynamicObjectFromMapCB(CEntity * pEntity, void * pData);

	bool RequestAndWaitForCompEntities();

	static void GetAllBuildingsWithPhysics(fwMapDataStore &store, CLinkList<CEntity*>* pResultsList);

	static void ScanForBuildings(atArray<RegdEnt>& aRequiredBuildings);

	//************************************************************************************
	// Function for finding the average water level for current navmesh
	//************************************************************************************

	void FindWaterLevelForCurrentNavMesh();

	// PURPOSE:	Temporarily stores the name of the "extras" file, so it's available to GridCellFinished()
	//			when it's time to write the file.
	ConstString							m_ExtrasFileName;

	fwPtrListSingleLink					m_ActiveBuildingsList;
	CAudMeshDataExporterInterfaceTool	m_ExporterInterface;
	std::vector<CEntity*>				m_PotentialEntitiesToExport;

	std::vector<spdSphere>				m_PortalBoundaries;
	std::vector<fwEntity*>				m_DynamicEntitiesList;

	fiStream*							m_pExportCollisionLogFile;

	int m_iPeakNumRefs;
	int m_iPeakNumBuildings;
	int m_iPeakNumObjects;
	int m_iPeakNumDummyObjects;
	int m_iPeakNumPtrSingle;
	int m_iPeakNumPtrDouble;

	float m_fTimeUntilWeExportThisNavMesh;

	bool m_bErrorsInExport;
	bool m_bHasBuildingsWithPhysics;
	bool m_bHasCompEntities;

	bool m_bLaunchCompilerAfterExporting;

	bool m_bAlwaysWaitForStreaming;
	bool m_bExportObjects;
	bool m_bExportOctrees;
	bool m_bExportInteriors;
	bool m_bPrintOutAllLoadedCollision;
};

//-----------------------------------------------------------------------------

#endif	// NAVMESHGENERATOR_EXPORTCOLLISION_H
