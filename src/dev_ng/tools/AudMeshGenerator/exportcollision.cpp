#include "exportcollision.h"
#include "config.h"
#include "exportcollisionvehicles.h"

#include "fwnavgen/datatypes.h"
#include "fwnavgen/helperfunc.h"

// Rage headers
#include "diag/output.h"
#include "Physics/iterator.h"
#include "system/param.h"
#include "system/xtl.h"		// For MEMORYSTATUS, etc.

// Framework headers
#include "fwscene/stores/boxstreamer.h"
#include "fwscene/stores/mapdatastore.h"
#include "fwscene/stores/staticboundsstore.h"
#include "fwscene/world/WorldLimits.h"
#include "spatialdata\sphere.h"

// Game headers
#include "camera/viewports/ViewportManager.h"
#include "Objects/DummyObject.h"
#include "Objects/Object.h"
#include "Objects/ObjectPopulation.h"
#include "PathServer/PathServer.h"
#include "Physics/Physics.h"
#include "Renderer/PlantsMgr.h"
#include "Renderer/Water.h"
#include "scene/entities/compEntity.h"
#include "scene/FocusEntity.h"
#include "scene/portals/Portal.h"
#include "scene/streamer/streamvolume.h"
#include "scene/world/GameWorld.h"
#include "script/script.h"			// CTheScripts
#include "streaming/streaming.h"
#include "Task/Movement/Climbing/TaskGoToAndClimbLadder.h"
#include "VehicleAi/PathFind.h"

//-----------------------------------------------------------------

PARAM(exportall, "This will export all navmeshes");
PARAM(exportsectors, "This will export specified navmeshes (x1,y1,x2,y2)");
PARAM(exportregion, "This will export all navmeshes intersecting the woorld coords (x1,y1,x2,y2)");
PARAM(exportFolder, "Specifies the folder that the navmeshes should be written to.");
PARAM(scheduledCompile, "Specifies that this is a scheduled navmesh compile, and completed.txt must be written at the end.");
PARAM(audgenBatchFile, "-audGenBatchFile specifies the batch file to run when generating audmeshes");
PARAM(audgenCmdFile, "-audGenCmdFile specifies the cmd-file to use when generating audmeshes");
PARAM(audgencfg, "Path and file name for the audmesh tool's configuration file, if desired.");

//-----------------------------------------------------------------

XPARAM(nopeds);
XPARAM(nocars);
XPARAM(noambient);
XPARAM(nonetwork);

#if __WIN32PC && __BANK
XPARAM(hidewidgets);			// Currently defined in 'bank/pane.cpp'.
#endif

namespace rage
{
	XPARAM(noaudio);
	XPARAM(noaudiothread);
	XPARAM(nowaveslots);

#if __WIN32PC && !__FINAL
	XPARAM(hidewindow);			// Currently defined in 'grcore/device_win32.cpp'.
#endif
}	// namespace rage

#if 0
void PrintInterior(CInteriorInst * pIntInst)
{
	const char * pInteriorName = pIntInst->GetModelName();

	// This is an interior.  Go through all the entities in the interior & export.
	CEntity * pIntEntity = NULL;
	pIntInst->RestartEntityInInteriorList();
	while(pIntInst->GetNextEntityInInteriorList(&pIntEntity))
	{
		const char * pEntityName = pIntEntity->GetModelName();
		if(stricmp(pEntityName, testEntityName)==0)
		{
			bBreak = true;
		}
	}
}
#endif

CAudMeshDataExporterInterfaceTool::CAudMeshDataExporterInterfaceTool(CAudMeshDataExporterTool &tool)
		: m_Tool(&tool)
{}


bool CAudMeshDataExporterInterfaceTool::EntityIntersectsCurrentNavMesh(CEntity* pEntity) const
{
	return m_Tool->EntityIntersectsCurrentNavMesh(pEntity);
	//return false;
}

bool CAudMeshDataExporterInterfaceTool::ExtentsIntersectCurrentNavMesh(Vec3V_In vMin, Vec3V_In vMax) const
{
	return m_Tool->ExtentsIntersectCurrentNavMesh(vMin, vMax);
}

bool CAudMeshDataExporterInterfaceTool::IsAlreadyInPhysicsLevel(CPhysical* pPhysical, phArchetype* pArchetype) const
{
	return CAudMeshDataExporterTool::IsAlreadyInPhysicsLevel(pPhysical, pArchetype);
}

void CAudMeshDataExporterInterfaceTool::AppendToActiveBuildingsArray(atArray<CEntity*>& entityArray)
{
	m_Tool->AppendToActiveBuildingsArray(entityArray);
}

void CAudMeshDataExporterInterfaceTool::AddSearches(atArray<fwBoxStreamerSearch>& searchList)
{
	m_Tool->AddSearches(searchList);
}

void CAudMeshDataExporterTool::PreInit()
{
	Assert(CNavMeshDataExporter::GetExportMode() == CNavMeshDataExporter::eNotExporting);
	CNavMeshDataExporter::SetExportMode(CNavMeshDataExporter::eWaitingToStartExport);

	// This is needed for now to prevent the pools from becoming too small when the values
	// from the configuration file override the larger values in code. We may want to
	// solve this in a different way, perhaps either loading a secondary configuration
	// file containing the larger pool values, or adding a conditional section within
	// the single configuration file which is somehow only used from this tool.
	fwConfigManager::SetUseLargerPoolFromCodeOrData(true);

	PARAM_noaudio.Set("");
	PARAM_noaudiothread.Set("");
	PARAM_nowaveslots.Set("");
	PARAM_nopeds.Set("");
	PARAM_nocars.Set("");
	PARAM_noambient.Set("");
	PARAM_nonetwork.Set("");

#if __WIN32PC && !__FINAL
	PARAM_hidewindow.Set("");
#endif

#if __WIN32PC && __BANK
	PARAM_hidewidgets.Set("");
#endif
}


void CAudMeshDataExporterTool::Init()
{
	fwExportCollisionGridTool::Init();

	InitConfig();

	RequestDefaultIplGroups();
}


void CAudMeshDataExporterTool::Shutdown()
{
	ShutdownConfig();

	fwExportCollisionGridTool::Shutdown();
}


void CAudMeshDataExporterTool::InitConfig()
{
	const char *cfgFileName = NULL;
	PARAM_audgencfg.Get(cfgFileName);

	if(cfgFileName && cfgFileName[0])
	{
		Displayf("Loading AudMeshGenerator configuration file from '%s'.", cfgFileName);
	}
	else
	{
		Displayf("No configuration file specified for AudMeshGenerator.");
	}

	fwNavGenConfigManagerImpl<CAudMeshGeneratorConfig>::InitClass(cfgFileName);
}


void CAudMeshDataExporterTool::ShutdownConfig()
{
	fwNavGenConfigManagerImpl<CAudMeshGeneratorConfig>::ShutdownClass();
}


bool CAudMeshDataExporterTool::UpdateExport()
{
	// When exporting navmeshes, use a cut-down game update loop.
	CAudMeshDataExporterTool::NotifyMainGameLoopHasStarted();

	if(CNavMeshDataExporter::IsExportingCollision())
	{
		CAudMeshDataExporterTool::ProcessCollisionExport();
	}

	return false;
}

void CAudMeshDataExporterTool::RequestDefaultIplGroups()
{
	char * pIplNames[] =
	{
		"jetsteal1",
		"cargoship",
		"pcargoship",
		"bnkheist_apt_norm",

		"prologue01",
		"prologue02",
		"prologue03",
		"prologue04",
		"prologue05",
		"prologue06",
		"prologuerd",

		"prologue01b",
		"prologue02b",
		"prologue03b",
		"prologue04b",
		"prologue05b",
		"prologue06b",
		"prologuerdb",

		"prologue03_grv_dug",

		"CJ_IOABoat",
		"trailerpark_ipl_group1",
		"ch1_02_open",
		"scafstartimap",
		"methtrailer_grp1",

		"farm",
		"farm_props",
		0
	};

	int c=0;
	char * pName = pIplNames[c];
	while(pName)
	{
		s32 index = INSTANCE_STORE.FindSlot(pName);
		if(index != -1)
		{
			INSTANCE_STORE.RequestGroup(index, pName, true);
		}

		c++;
		pName = pIplNames[c];
	}
}

//-----------------------------------------------------------------

// TODO: Probably move the definition of many of these to here
XPARAM(exportsectors);
XPARAM(exportFolder);
XPARAM(scheduledCompile);
XPARAM(audgenBatchFile);
XPARAM(audgenCmdFile);
XPARAM(noBlockOnLostFocus);

//NAVMESH_OPTIMISATIONS()
//OPTIMISATIONS_OFF()

using namespace std;

namespace
{

// how much area of empty sea to export for navmesh outside inhabited world extents
float g_fSeaBorderSize = 300.0f;

// This is the range which is used for loading in data around the origin, when we are exporting
// collision.  This value is added to the default BOUNDS_BB_EXTRASIZE value in fwStaticBoundsStore.

float TIME_TO_ALLOW_STREAMING_TO_CATCH_UP = 1.0f;
float MAX_TIME_TO_WAIT_FOR_STREAMING_TO_LOAD = 1.0f;
float fAudMeshMinMaxExtra = 0.0f;

Vector3 vExpandForInteriors(0.0f, 0.0f, 0.0f);
CLinkList<CEntity*> dbgBuildingsWithPhysicsList;
//vector<fwSphere> * g_pPortalBoundaries = NULL;


bool IsEntityInList(CEntity * pEntity, vector<CEntity*> & potentialEntitiesToExport)
{
	for(u32 e=0; e<potentialEntitiesToExport.size(); e++)
		if(potentialEntitiesToExport[e]==pEntity)
			return true;
	return false;
}


void CheckEntityName(CEntity * pEntity)
{
	static char entityName[256];
	static bool b1stTime=true;
	static Vector3 vMatchPos( 434.75f, -981.91f, 31.07f );
	if(b1stTime)
	{
		strcpy(entityName, "v_ilev_ph_doorframe");
		b1stTime = false;
	}

	if( !pEntity->IsArchetypeSet() || !CModelInfo::IsValidModelInfo( pEntity->GetModelIndex() ))
		return;

	const fwTransform & xf = pEntity->GetTransform();
	Vector3 vPos = VEC3V_TO_VECTOR3(xf.GetPosition());

	CBaseModelInfo * pModelInfo = pEntity->GetBaseModelInfo();
	const char * pEntityName = pModelInfo->GetModelName();
	if(stricmp(pEntityName, entityName)==0)
	{
		if(vPos.IsClose(vMatchPos, 0.25f) || vMatchPos.IsClose(VEC3_ZERO, 0.25f))
		{
			bool bMatched = true;
			bMatched = bMatched;
			vPos = vPos;
		}
	}
}


bool AddInteriorsToExportListCB(CEntity* pEntity, void* data)
{
	Assert(pEntity);
	vector<CEntity*> * entitiesToExport = reinterpret_cast<vector<CEntity*> *>(data);
	CBaseModelInfo * pModelInfo = pEntity->GetBaseModelInfo();
	Assert(pModelInfo);

	if(pModelInfo->GetModelType() == MI_TYPE_MLO)
	{
		entitiesToExport->push_back(pEntity);
		CInteriorInst * pIntInst = (CInteriorInst*)pEntity;

#if __DEV
		//PrintInterior(pIntInst);
#endif
		// Add to the "portalBoundaries" list all the portals which link this interior to
		// the main map.  The reason for this is that we wish to remove the ped-density from
		// all navmesh polys intersecting the portal - which will remove some awkward
		// boundary cases from the ped-generation logic.
/*
		u32 iNumRoomZeroPortals = pIntInst->GetNumPortalsInRoom(0);
		for(u32 i=0; i<iNumRoomZeroPortals; i++)
		{
			s32 iPtlIdx = pIntInst->GetPortalIdxInRoom(0, i);
			if(iPtlIdx < pIntInst->GetNumPortals())
			{
				fwPortalCorners & portal = pIntInst->GetPortal(iPtlIdx);

				fwSphere sphere;
				portal.CalcBoundingSphere(sphere);
				if(g_pPortalBoundaries)
					g_pPortalBoundaries->push_back(sphere);
			}
		}
*/
		// This is an interior.  Go through all the entities in the interior & export.
		CEntity * pIntEntity = NULL;
		pIntInst->RestartEntityInInteriorList();
		while(pIntInst->GetNextEntityInInteriorList(&pIntEntity))
		{
			if(pIntEntity && !IsEntityInList(pIntEntity, *entitiesToExport))
			{
				entitiesToExport->push_back(pIntEntity);
			}
		}
	}

	return true;
}



void ReplaceChar(char * pString, char cReplace, char cWith)
{
	while(*pString)
	{
		if(*pString==cReplace)
			*pString=cWith;
		pString++;
	}
}

}	// anon namespace

// Set this to 1 to prevent any triangle exporting/clipping/etc - just to do the streaming request stuff
#define __AUDMESH_EXPORT_DISABLE_COLLISION_EXPORT	0
#define __AUDMESH_EXPORT_ONLY_MOVER_COLLISION		1
#define __BREAK_ON_ENTITY_NAME						0

#define NAVMESH_EXPORT_COLLISON_ARCHETYPES			(ArchetypeFlags::GTA_MAP_TYPE_MOVER | ArchetypeFlags::GTA_OBJECT_TYPE | ArchetypeFlags::GTA_RIVER_TYPE | ArchetypeFlags::GTA_GLASS_TYPE)

#if __WIN32
#pragma warning(push)
#pragma warning(disable: 4355) // 'this' used in base member initializer list
#endif

CAudMeshDataExporterTool::CAudMeshDataExporterTool()
		: m_ExporterInterface(*this)
{
	m_fTimeUntilWeExportThisNavMesh = 0.0f;

	m_bErrorsInExport = false;
	m_bHasBuildingsWithPhysics = false;
	m_bHasCompEntities = false;

	m_pExportCollisionLogFile = NULL;

	m_bLaunchCompilerAfterExporting = false;

	m_bAlwaysWaitForStreaming = false;
	m_bExportObjects = true;
	m_bExportOctrees = true;
	m_bExportInteriors = true;
	m_bPrintOutAllLoadedCollision = false;

	m_iPeakNumRefs = 0;
	m_iPeakNumBuildings = 0;
	m_iPeakNumObjects = 0;
	m_iPeakNumDummyObjects = 0;
	m_iPeakNumPtrSingle = 0;
	m_iPeakNumPtrDouble = 0;
}

#if __WIN32
#pragma warning(pop)
#endif

CAudMeshDataExporterTool::~CAudMeshDataExporterTool()
{
}


void CAudMeshDataExporterTool::OpenExportLog()
{
	char filename[512];
	formatf(filename, "%s\\export_log.txt", m_OutputPath);

	m_pExportCollisionLogFile = fiStream::Create(filename);
}

// Helper function to change the width of a fwVariableBoxStreamer, as if it had
// been set before its Init() call.
// Note: would perhaps be handy to turn this into fwVariableBoxStreamer::SetAllWidths()
// or something.
/*
static void sChangeVarBoxStreamerWidth(fwBoxStreamerNew& streamer, float newWidth)
{
	strStreamingModule* pMod = streamer.GetStreamingModule();
	if(!Verifyf(pMod, "Expected streaming module"))
	{
		return;
	}

	const int cnt = pMod->GetCount();
	for(int i = 0; i < cnt; i++)
	{
		streamer.SetWidth(i, newWidth);
	}

	static_cast<fwBoxStreamerNew&>(streamer).SetWidth(newWidth);
}
*/

bool CAudMeshDataExporterTool::StartCollisionExport()
{
	Displayf("CAudMeshDataExporterTool::StartCollisionExport()\n");

	// TODO: Think more about what to do about this stuff. Will we ever need to launch anything this way?
	m_bLaunchCompilerAfterExporting = true;

	m_OutputPath[0] = 0;
	GetOutputFolder();
	if(!m_OutputPath[0])
		return false;

	OpenExportLog();

	// Turn off rendering, etc.
	// Ideally we'd want to stop creation of D3D device objects too
//	gVpMan.GetRenderSettings().ExcludeFlags(0xFFFFFFFF);

	CPathServerExtents::m_iNumSectorsPerNavMesh = CPathServer::GetNavMeshStore(kNavDomainRegular)->GetNumSectorsPerMesh();

//	const float fExportWidth = CPathServer::GetNavMeshStore(kNavDomainRegular)->GetMeshSize();
//	sChangeVarBoxStreamerWidth(g_StaticBoundsStore.GetBoxStreamer(), fExportWidth);
//	INSTANCE_STORE.GetBoxStreamer().SetWidth(fExportWidth);


	// Also done by BaseStartCollisionExport(), but might need these values for ExportCollisionForAllVehicles().
	// TODO: Think more about this part, perhaps something could be reordered to avoid doing
	// this in two places.
	m_fWidthOfNavMesh = CPathServerExtents::GetWorldWidthOfSector() * CPathServerExtents::m_iNumSectorsPerNavMesh;
	m_fDepthOfNavMesh = CPathServerExtents::GetWorldWidthOfSector() * CPathServerExtents::m_iNumSectorsPerNavMesh;


/* Currently no vehicle audio audmesh-ing
	// Firstly export collision for all the large vehicles in the game
	if(CAudMeshGeneratorConfig::Get().m_ExportVehicles)
	{
		CNavMeshVehicleDataFunctions::ExportCollisionForAllVehicles(*this, m_OutputPath);

		Displayf("Completed CNavMeshDataExporterTool::ExportCollisionForAllVehicles()\n");
	}
*/

	if(!fwExportCollisionGridTool::StartCollisionExport())
	{
		return false;
	}


	ThePaths.bLoadAllRegions = true;
	CTheScripts::SetAllowGameToPauseForStreaming(true);
	m_bHasBuildingsWithPhysics = false;
	m_bHasCompEntities = false;
//	gVpMan.GetRenderSettings().SetFlags(0);
	gbPlantMgrActive = FALSE;

	CNavMeshDataExporter::SetActiveExporter(&m_ExporterInterface);
	Assert(CNavMeshDataExporter::GetExportMode() == CNavMeshDataExporter::eWaitingToStartExport);
	CNavMeshDataExporter::SetExportMode(CNavMeshDataExporter::eMapExport);

	CPhysics::SetScanForBuildingsFunctor(MakeFunctor(CAudMeshDataExporterTool::ScanForBuildings));

	m_fTimeUntilWeExportThisNavMesh = TIME_TO_ALLOW_STREAMING_TO_CATCH_UP;

	m_iPeakNumRefs = 0;
	m_iPeakNumBuildings = 0;
	m_iPeakNumObjects = 0;
	m_iPeakNumPtrSingle = 0;
	m_iPeakNumPtrDouble = 0;

	dbgBuildingsWithPhysicsList.Init(4096);

#if __WIN32PC
	// Make sure that the application will still run when it loses focus
	GRCDEVICE.SetBlockOnLostFocus(false);
	// Make sure that Rage won't halt the app with dialog boxes, or automatically quit upon an error
	diagOutput::DisablePopUpErrors();
	diagOutput::DisablePopUpQuits();
#endif

	return true;
}


bool CAudMeshDataExporterTool::EntityIntersectsCurrentNavMesh(CEntity * pEntity)
{
	/*if(!pEntity->GetIsTypeDummyObject() && !pEntity->GetIsTypeComposite())
	{
		phInst* pInst = pEntity->GetFragInst();
		if(pInst==NULL)
			pInst = pEntity->GetCurrentPhysicsInst();

		phArchetype * pArchetype = NULL;
		if(pInst)
			pArchetype = pInst->GetArchetype();

		if(!pArchetype)
			return false;
	}*/

	Vector3 vMid = (m_vNavMeshMins + m_vNavMeshMaxs) * 0.5f;
	Vector3 vNavMeshMinsExtra = m_vNavMeshMins - Vector3(fAudMeshMinMaxExtra, fAudMeshMinMaxExtra, 0.0f);
	Vector3 vNavMeshMaxsExtra = m_vNavMeshMaxs + Vector3(fAudMeshMinMaxExtra, fAudMeshMinMaxExtra, 0.0f);
	Vector3 vEntityMin = pEntity->GetBoundingBoxMin();
	Vector3 vEntityMax = pEntity->GetBoundingBoxMax();

	const bool bBoundsAreInWorldSpace = false; //pEntity->GetIsTypeComposite();
	if(!bBoundsAreInWorldSpace)
	{
		fwNavToolHelperFuncs::GetRotatedMinMax(vEntityMin, vEntityMax, MAT34V_TO_MATRIX34(pEntity->GetMatrix()));
	}

	if(pEntity->GetIsTypeMLO())
	{
		vEntityMin -= vExpandForInteriors;
		vEntityMin += vExpandForInteriors;
	}

	if(vEntityMin.x > vNavMeshMaxsExtra.x || vEntityMin.y > vNavMeshMaxsExtra.y || vEntityMin.z > vNavMeshMaxsExtra.z ||
		vNavMeshMinsExtra.x > vEntityMax.x || vNavMeshMinsExtra.y > vEntityMax.y || vNavMeshMinsExtra.z > vEntityMax.z)
	{
		return false;
	}
	else
	{
		return true;
	}
}

bool CAudMeshDataExporterTool::ExtentsIntersectCurrentNavMesh(Vec3V_In vMinV, Vec3V_In vMaxV)
{
	Vector3 vNavMeshMinsExtra = m_vNavMeshMins - Vector3(fAudMeshMinMaxExtra, fAudMeshMinMaxExtra, 0.0f);
	Vector3 vNavMeshMaxsExtra = m_vNavMeshMaxs + Vector3(fAudMeshMinMaxExtra, fAudMeshMinMaxExtra, 0.0f);
	Vector3 vEntityMin = VEC3V_TO_VECTOR3(vMinV);
	Vector3 vEntityMax = VEC3V_TO_VECTOR3(vMaxV);

	if(vEntityMin.x > vNavMeshMaxsExtra.x || vEntityMin.y > vNavMeshMaxsExtra.y || vEntityMin.z > vNavMeshMaxsExtra.z ||
		vNavMeshMinsExtra.x > vEntityMax.x || vNavMeshMinsExtra.y > vEntityMax.y || vNavMeshMinsExtra.z > vEntityMax.z)
	{
		return false;
	}
	else
	{
		return true;
	}
}

bool g_bAllInteriorsInstanced = false;
bool g_bWaitingForInteriorArchetypes = false;

void CAudMeshDataExporterTool::AddSearches(atArray<fwBoxStreamerSearch>& searchList)
{
	fwBoxStreamerAssetFlags iStreamTypes = fwBoxStreamerAsset::FLAG_STATICBOUNDS_MOVER | fwBoxStreamerAsset::MASK_MAPDATA;

#if !__AUDMESH_EXPORT_ONLY_MOVER_COLLISION
	iStreamTypes |= fwBoxStreamerAsset::FLAG_STATICBOUNDS_WEAPONS;
#endif

	fwBoxStreamerSearch customSearch(
		spdAABB(VECTOR3_TO_VEC3V(m_vNavMeshMins), VECTOR3_TO_VEC3V(m_vNavMeshMaxs)),
		fwBoxStreamerSearch::TYPE_NAVMESHEXPORT,
		iStreamTypes);

	searchList.Grow() = customSearch;
}

void CAudMeshDataExporterTool::ResetAndOpenTriFile(const char *pFilename)
{
	Assert(!m_pCurrentTriFile);
	m_vNavMeshMins = Vector3(-m_fWidthOfNavMesh,-m_fDepthOfNavMesh,-1000.0f);
	m_vNavMeshMaxs = Vector3(m_fWidthOfNavMesh,m_fDepthOfNavMesh,1000.0f);

	m_iHasWaterInCurrentNavMesh = false;
	m_fAverageWaterLevelInCurrentNavMesh =  0.0f;
	m_pWaterLevels = NULL;

	OpenTriFile(pFilename);
}


void CAudMeshDataExporterTool::GridCellFinished()
{
	m_PortalBoundaries.clear();
	m_DynamicEntitiesList.clear();
}


void CAudMeshDataExporterTool::GridProcessFinished()
{
	CNavMeshDataExporter::SetExportMode(CNavMeshDataExporter::eNotExporting);
	CNavMeshDataExporter::SetActiveExporter(NULL);

//	gVpMan.GetRenderSettings().SetFlags(0x0); // all off

	printf("Finished Exporting.\n");

	if(m_pExportCollisionLogFile)
		m_pExportCollisionLogFile->Close();

	if(m_bLaunchCompilerAfterExporting)
	{
		LaunchAudMeshCompiler();

		if(m_bErrorsInExport)
		{
			while(1)
			{
				sysIpcYield(0);
			}
		}
		else
		{
#if __WIN32PC
			// Hack to work around various shutdown problems, such as systems that assert or crash
			// just from static objects being created and then destroyed again.
			ExitProcess(0);
#else
			exit(0);
#endif
		}
	}
}


void CAudMeshDataExporterTool::UpdateStreamingPos(const Vector3 &pos)
{
	CGameWorld::SetPlayerFallbackPos(pos);

	CFocusEntityMgr::GetMgr().SetPosAndVel(pos, VEC3_ZERO);
}


void CAudMeshDataExporterTool::NotifyMainGameLoopHasStarted()
{
	static float fTimeToWaitBeforeExport = 8.0f;
	fTimeToWaitBeforeExport -= fwTimer::GetTimeStep();

	if(fTimeToWaitBeforeExport > 0.0f)
		return;

	// First time, we'll check to see if the "-exportall" param exists.  If so then start exporting all the navmeshes from the world.
	static bool bHaveCheckedParams=false;
	if(!bHaveCheckedParams)
	{
		Displayf("CAudMeshDataExporterTool::NotifyMainGameLoopHasStarted()\n");

		bHaveCheckedParams = true;

		if(!ParseArgumentsAndStartExportCollision())
		{
			// Failure
		}
	}
}


void CAudMeshDataExporterTool::DisplayPoolUsage()
{
#if  __WIN32
	MEMORYSTATUS ms;
	GlobalMemoryStatus(&ms);
#endif

#if __WIN32
	static sysMemAllocator & rageMemAllocator = sysMemAllocator::GetMaster();
	s32 iTotalRageMemUsed = rageMemAllocator.GetMemoryUsed();
#endif

//	int iNumRefs = fwKnownRefHolder::GetPool()->GetNoOfUsedSpaces();
//	m_iPeakNumRefs = Max(m_iPeakNumRefs, iNumRefs);

	m_iPeakNumBuildings = Max(m_iPeakNumBuildings, CBuilding::GetPool()->GetNoOfUsedSpaces());
	m_iPeakNumObjects = Max(m_iPeakNumObjects, CObject::GetPool()->GetNoOfUsedSpaces());
	m_iPeakNumDummyObjects = Max(m_iPeakNumDummyObjects, CDummyObject::GetPool()->GetNoOfUsedSpaces());
//	m_iPeakNumPtrSingle = Max(m_iPeakNumPtrSingle, fwPtrNodeSingleLink::GetPool()->GetNoOfUsedSpaces());
//	if(fwPtrNodeDoubleLink::GetPool())
//		m_iPeakNumPtrDouble = Max(m_iPeakNumPtrDouble, fwPtrNodeDoubleLink::GetPool()->GetNoOfUsedSpaces());

	Displayf("------------------------------------------------------------------\n");

	Displayf("Now Exporting : \"%s\\navmesh[%i][%i].tri\" (subsection %i)\n", m_OutputPath, m_iCurrentSectorX, m_iCurrentSectorY, m_iNavMeshSubSection);

	Displayf("(%.1f, %.1f, %.1f) to (%.1f, %.1f, %.1f)\n", m_vNavMeshMins.x, m_vNavMeshMins.y, m_vNavMeshMins.z, m_vNavMeshMaxs.x, m_vNavMeshMaxs.y, m_vNavMeshMaxs.z);

	Displayf("Current Num Buildings : %i/%i (Peak:%i)\n", CBuilding::GetPool()->GetNoOfUsedSpaces(), CBuilding::GetPool()->GetSize(), m_iPeakNumBuildings);

	Displayf("Current Num Objects : %i/%i (Peak:%i)\n", CObject::GetPool()->GetNoOfUsedSpaces(), CObject::GetPool()->GetSize(), m_iPeakNumObjects);

	Displayf("Current Num Dummys : %i/%i (Peak:%i)\n", CDummyObject::GetPool()->GetNoOfUsedSpaces(), CDummyObject::GetPool()->GetSize(), m_iPeakNumDummyObjects);

#if __WIN32
	Displayf("AvailPhysical : %u Mb, AvailVirtual  : %u Mb, RAGE Used : %i Mb\n", ms.dwAvailPhys / (1024*1024), ms.dwAvailVirtual / (1024*1024), iTotalRageMemUsed / (1024*1024));
#endif

	Displayf("------------------------------------------------------------------\n");

	if(m_pExportCollisionLogFile)
	{
		fprintf(m_pExportCollisionLogFile, "------------------------------------------------------------------\n");
		fprintf(m_pExportCollisionLogFile, "Now Exporting : \"%s\\navmesh[%i][%i].tri\"\n", m_OutputPath, m_iCurrentSectorX, m_iCurrentSectorY);
		fprintf(m_pExportCollisionLogFile, "Current Num Buildings : %i/%i (Peak:%i)\n", CBuilding::GetPool()->GetNoOfUsedSpaces(), CBuilding::GetPool()->GetSize(), m_iPeakNumBuildings);
		fprintf(m_pExportCollisionLogFile, "Current Num Objects : %i/%i (Peak:%i)\n", CObject::GetPool()->GetNoOfUsedSpaces(), CObject::GetPool()->GetSize(), m_iPeakNumObjects);
		fprintf(m_pExportCollisionLogFile, "Current Num Dummys : %i/%i (Peak:%i)\n", CDummyObject::GetPool()->GetNoOfUsedSpaces(), CDummyObject::GetPool()->GetSize(), m_iPeakNumDummyObjects);
#if __WIN32
		fprintf(m_pExportCollisionLogFile, "AvailPhysical : %u Mb, AvailVirtual  : %u Mb, RAGE Used : %i Mb\n", ms.dwAvailPhys / (1024*1024), ms.dwAvailVirtual / (1024*1024), iTotalRageMemUsed / (1024*1024));
#endif
		fprintf(m_pExportCollisionLogFile, "------------------------------------------------------------------\n");
		m_pExportCollisionLogFile->Flush();
	}
}


void CAudMeshDataExporterTool::AddInteriorsToExportList(vector<CEntity*> & entitiesToExport /*, vector<fwSphere> & portalBoundaries*/)
{
	//g_pPortalBoundaries = &portalBoundaries;

	fwIsBoxIntersectingApprox cullBox(spdAABB( VECTOR3_TO_VEC3V(m_vNavMeshMins), VECTOR3_TO_VEC3V(m_vNavMeshMaxs)));

	CGameWorld::ForAllEntitiesIntersecting(
		&cullBox,
		AddInteriorsToExportListCB,
		(void*) &entitiesToExport,
		ENTITY_TYPE_MASK_MLO,
		SEARCH_LOCATION_INTERIORS|SEARCH_LOCATION_EXTERIORS,
		SEARCH_LODTYPE_ALL,
		SEARCH_OPTION_FORCE_PPU_CODEPATH
	);

}

// HACK: Make sure we don't already have physics added for this entity.
// TODO: Get to the bottom of why this is occurring
bool CAudMeshDataExporterTool::IsAlreadyInPhysicsLevel(CPhysical * pPhysical, phArchetype * pArchetype)
{
	static dev_bool bIgnoreTest = true;
	if(bIgnoreTest)
		return false;

	Vector3 vEntityPos = VEC3V_TO_VECTOR3(pPhysical->GetTransform().GetPosition());
	const int iMaxObjs = CPhysics::GetLevel()->GetMaxObjects();
	for(int l=0; l<iMaxObjs; l++)
	{
		if(CPhysics::GetLevel()->GetState(l)!=phLevelNew::OBJECTSTATE_NONEXISTENT)
		{
			phInst * pInstance = CPhysics::GetLevel()->GetInstance(l);
			CEntity * pUserData = (CEntity*)pInstance->GetUserData();
			if(pUserData==pPhysical)
				return true;
			if(pArchetype)
			{
				Vector3 vInstPos( pInstance->GetPosition().GetXf(), pInstance->GetPosition().GetYf(), pInstance->GetPosition().GetZf() );
				if(pInstance->GetArchetype()==pArchetype && vEntityPos.IsClose(vInstPos, 0.01f))
					return true;
			}
		}
	}
	return false;
}

const int iNumWaitFrames = 4;
int iWaitFrames = iNumWaitFrames;

bool CAudMeshDataExporterTool::ProcessCollisionExport()
{
	if(CNavMeshDataExporter::GetExportMode() != CNavMeshDataExporter::eMapExport)
	{
		Assert(0);
		return false;
	}

	if(0)	//TIME_TO_ALLOW_STREAMING_TO_CATCH_UP - m_fTimeUntilWeExportThisNavMesh < TIME_TO_BE_IN_LIMBO)
	{
		static const float fOffMap=WORLDLIMITS_REP_XMIN-1000.0f;
		m_vNavMeshMins = Vector3(fOffMap,fOffMap,0.0f);
		m_vNavMeshMaxs = Vector3(fOffMap,fOffMap,0.0f);
		m_vCentreOfCurrentNavMesh = (m_vNavMeshMins + m_vNavMeshMaxs) / 2.0f;
	}
	else
	{
		CalcCurrentNavMeshExtents();
	}

	fwTimer::Update();

	CGameWorld::SetPlayerFallbackPos(m_vCentreOfCurrentNavMesh);
	CFocusEntityMgr::GetMgr().SetPosAndVel(m_vCentreOfCurrentNavMesh, VEC3_ZERO);

	static bool bOutputMissingIPLs = true;
	static bool bOutputWaitExtra = true;

	bool bAllIPLsLoaded = true;

	fwMapDataStore& store = fwMapDataStore::GetStore();

	spdAABB aabb(VECTOR3_TO_VEC3V(m_vNavMeshMins), VECTOR3_TO_VEC3V(m_vNavMeshMaxs));
	atArray<u32> slotList;
	store.GetBoxStreamer().GetIntersectingAABB(aabb, fwBoxStreamerAsset::MASK_MAPDATA, slotList);
	for (s32 i=0; i<slotList.GetCount(); i++)
	{
		u32 slot = slotList[i];
		if (slot != 0xffffffff)
		{
			bool bLoaded = store.HasObjectLoaded(slot);
			if (!bLoaded)
			{
				bAllIPLsLoaded = false;

				store.StreamingRequest(slot, STRFLAG_PRIORITY_LOAD|STRFLAG_FORCE_LOAD);
			}
		}
	}

	if(!bAllIPLsLoaded)
		bOutputMissingIPLs = false;

	m_bHasCompEntities = false;
	bool bAllCompEntitiesInStartState = true;

	static bool bBreak = false;
	for(int i = 0; i < CCompEntity::GetPool()->GetSize(); i++)
	{
		CCompEntity* pComp = CCompEntity::GetPool()->GetSlot(i);
		if(pComp)
		{
#if __DEV
			CheckEntityName(pComp);
#endif
			if(EntityIntersectsCurrentNavMesh(pComp))
			{
				if(pComp->GetState()!=CE_STATE_START)
				{
					bAllCompEntitiesInStartState = false;
				}

				if(pComp->GetState() != CE_STATE_SYNC_STARTING &&
					pComp->GetState() != CE_STATE_STARTING && 
					pComp->GetState() != CE_STATE_START)
				{
					pComp->SetToStarting();
				}
				
				m_bHasCompEntities = true;

				pComp->ImmediateUpdate();
			}
		}
	}

	//**********************************************************************

	bool bHasObjectsIntersectingNavMesh = false;

	int o;
	for(o=0; o<CObject::GetPool()->GetSize(); o++)
	{
		CObject * pObj = CObject::GetPool()->GetSlot(o);
		if(pObj && EntityIntersectsCurrentNavMesh(pObj))
		{
			bHasObjectsIntersectingNavMesh = true;

			CBaseModelInfo* pModelInfo = pObj->GetBaseModelInfo();
			bool bAddPhysics = pModelInfo && (!pModelInfo->GetFragType() || pModelInfo->GetFragType()->GetNumEnvCloths()==0);

			if(bAddPhysics)
			{
				// If model is not loaded
				if(!CModelInfo::HaveAssetsLoaded(pObj->GetModelId()))
				{
					CModelInfo::RequestAssets(pObj->GetModelId(), STRFLAG_FORCE_LOAD);
				}
				pObj->InitPhys();
			}
		}
	}
	for(o=0; o<CDummyObject::GetPool()->GetSize(); o++)
	{
		CDummyObject * pDummyObj = CDummyObject::GetPool()->GetSlot(o);
		if(pDummyObj && EntityIntersectsCurrentNavMesh(pDummyObj))
			bHasObjectsIntersectingNavMesh = true;
	}

	//**********************************************************************

	//sysIpcSleep(0);

	//CStreaming::LoadAllRequestedObjects();

	RequestDefaultIplGroups();


	//****************************************************************************************
	// If we have no buildings with physics to load, then skip the waiting process.
	// NB : Maybe this will cause problems when we want to load in objects?

	const bool bCanSkipWait = 
		(m_bHasBuildingsWithPhysics == false) &&
		(bHasObjectsIntersectingNavMesh == false) &&
		(m_bHasCompEntities == false) &&
		(bAllIPLsLoaded == true) &&
		(g_bWaitingForInteriorArchetypes == false) &&
		(m_bAlwaysWaitForStreaming == false);

	//****************************************************************************************
	// Always allow a certain amount of time for streaming to request physics models nearby..

	m_fTimeUntilWeExportThisNavMesh -= fwTimer::GetTimeStep();
	iWaitFrames--;

	if((m_fTimeUntilWeExportThisNavMesh > 0.0f && !bCanSkipWait) || iWaitFrames > 0)
	{
		return false;
	}

	//************************************************************************************************
	// Wait for the physics to load for all the buildings
	// This can get caught in an infinite loop, because sometimes the streaming is unable to
	// load everything.  To catch this case I've got a maximum time which we'll wait for the streaming
	// and I've also put a CStreaming::Purge.. at the end of the export so we're not left waiting for
	// the same object(s) forever

	const bool bWaitLonger =
		(CStreaming::GetNumberObjectsRequested() > 0) ||
		(g_bAllInteriorsInstanced == false) ||
		(g_bWaitingForInteriorArchetypes == true) ||
		(bAllIPLsLoaded == false) ||
		(bAllCompEntitiesInStartState == false);

	if( (bWaitLonger && m_fTimeUntilWeExportThisNavMesh > -MAX_TIME_TO_WAIT_FOR_STREAMING_TO_LOAD) ) 
	{
		if(bOutputWaitExtra)
			Displayf("Not all entities are loaded.\n");
		bOutputWaitExtra = false;
		return false;
	}

	CStreaming::LoadAllRequestedObjects();

	bOutputMissingIPLs = true;
	bOutputWaitExtra = true;

	m_bPrintOutAllLoadedCollision = false;
	if(m_bPrintOutAllLoadedCollision)
	{

		Displayf("---------------------------\n");
		Displayf("Buildings with physics\n");
		Displayf("---------------------------\n");

		dbgBuildingsWithPhysicsList.Clear();

		GetAllBuildingsWithPhysics(INSTANCE_STORE, &dbgBuildingsWithPhysicsList);
		CLink<CEntity*> * pBuildingPtr = dbgBuildingsWithPhysicsList.GetFirst();
		while(pBuildingPtr)
		{
			CEntity* pEntity = pBuildingPtr->item;
			if(pEntity)
			{
				CBaseModelInfo* pModelInfo = pEntity->GetBaseModelInfo();
				const char * pRoomName = pModelInfo->GetModelName();

				Displayf("%s (MI:%i)\n", pRoomName, pEntity->GetModelIndex());

				if(pModelInfo->HasPhysics() && !pEntity->GetCurrentPhysicsInst())
				{
					Displayf("PHYSICS NOT LOADED FOR THIS BUILDING\n");
				}
			}
			else
			{
				Displayf("(NULL)\n");
			}
			pBuildingPtr = pBuildingPtr->GetNext();
		}
		dbgBuildingsWithPhysicsList.Clear();

		Displayf("--------------------------------\n");
		Displayf("Physics building request list\n");
		Displayf("--------------------------------\n");


		CLink<RegdEnt> * pTestPtr = CPhysics::ms_physicsReqList.GetFirst();
		while(pTestPtr)
		{
			CEntity * pEntity = pTestPtr->item;
			if(pEntity)
			{
				CBaseModelInfo* pModelInfo = pEntity->GetBaseModelInfo();
				const char * pRoomName = pModelInfo->GetModelName();

				Displayf("%s (MI:%i)\n", pRoomName, pEntity->GetModelIndex());
			}
			else
			{
				Displayf("(NULL)\n");
			}
			pTestPtr = pTestPtr->GetNext();
		}

		Displayf("---------------------------\n");
		Displayf("Objects\n");
		Displayf("---------------------------\n");

		CObject::Pool* pObjectPool = CObject::GetPool();

		for(int o=0; o<pObjectPool->GetSize(); o++)
		{
			CObject * pObj = pObjectPool->GetSlot(o);
			if(!pObj)
				continue;

			CBaseModelInfo * pModelInfo = pObj->GetBaseModelInfo();
			Assert(pModelInfo);

			Displayf("%s (MI:%i)\n", pModelInfo->GetModelName(), pObj->GetModelIndex());
		}

		Displayf("---------------------------\n");
		Displayf("Dummy Objects\n");
		Displayf("---------------------------\n");

		CDummyObject::Pool* pDummyPool = CDummyObject::GetPool();

		for(int d=0; d<pDummyPool->GetSize(); d++)
		{
			CDummyObject * pDummy = pDummyPool->GetSlot(d);
			if(!pDummy)
				continue;

			CBaseModelInfo * pModelInfo = pDummy->GetBaseModelInfo();
			Assert(pModelInfo);

			Displayf("%s (MI:%i)\n", pModelInfo->GetModelName(), pDummy->GetModelIndex());
		}

		Displayf("\n");
	}

#if !__AUDMESH_EXPORT_DISABLE_COLLISION_EXPORT

	bool flushStreams = m_bHasBuildingsWithPhysics;

	m_bHasBuildingsWithPhysics = false;
	m_bHasCompEntities = false;
	fwTimer::Suspend();

	char extrasFileName[1024];
	GenerateFileNames(m_TriFileName, sizeof(m_TriFileName), extrasFileName, sizeof(extrasFileName));

	// Stop rage from whingeing
	phBound::SetBoundFlags(0);

	// We'll build up a list of triangles & surface-types
	// At the end we will create a physics bounds from all of the triangles, and save
	// We don't build up the bound as we go along, because we can't add verts/polys to a phBoundGeometry dynamically
	vector<Vector3> triangleVertices;
	vector<phMaterialMgr::Id> triangleMaterials;
	vector<u16> colPolyFlags;

	m_iNumTrianglesThisTriFile = 0;
	DisplayPoolUsage();


	Vector3 vMid = (m_vNavMeshMins + m_vNavMeshMaxs) * 0.5f;
	Vector3 vNavMeshMinsExtra = m_vNavMeshMins - Vector3(fAudMeshMinMaxExtra, fAudMeshMinMaxExtra, 0.0f);
	Vector3 vNavMeshMaxsExtra = m_vNavMeshMaxs + Vector3(fAudMeshMinMaxExtra, fAudMeshMinMaxExtra, 0.0f);



	//************************************************************
	//	Find out the water level.
	//	This data will be written into the start of the tri file.
	//************************************************************

	FindWaterLevelForCurrentNavMesh();


	//********************************************
	// Now export all the bounds from the octree
	//********************************************

	m_PotentialEntitiesToExport.clear();


	if(m_bExportOctrees)
	{
		phLevelNew * phLevel = CPhysics::GetLevel();

		phIterator iterator(phIterator::PHITERATORLOCKTYPE_READLOCK);

		// Use this option to get all objects matching the req'd state
		iterator.InitCull_All();
		iterator.SetStateIncludeFlags(phLevelNew::STATE_FLAG_FIXED);

		u16 iIndexInLevel = phLevel->GetFirstCulledObject(iterator);

		while(iIndexInLevel != phInst::INVALID_INDEX)
		{
			phInst * pInstance = phLevel->GetInstance(iIndexInLevel);
			if(pInstance)
			{
				CEntity * pOctreeEntity = (CEntity*)pInstance->GetUserData();
				Assert(pOctreeEntity);

				if(pOctreeEntity)
				{
#if __DEV
					CheckEntityName(pOctreeEntity);
#endif
					if(pOctreeEntity->GetIsTypeBuilding() && !IsEntityInList(pOctreeEntity, m_PotentialEntitiesToExport))
					{
						m_PotentialEntitiesToExport.push_back(pOctreeEntity);
					}
				}
			}

			iIndexInLevel = phLevel->GetNextCulledObject(iterator);
		}
	}

	//**************************************************************************
	// Now export all the objects in the world which are 'fixed for navigation'
	//**************************************************************************

	if(m_bExportObjects)
	{
		CObject::Pool* pObjectPool = CObject::GetPool();

		for(int o=0; o<pObjectPool->GetSize(); o++)
		{
			CObject * pObj = pObjectPool->GetSlot(o);
			if(!pObj)
				continue;

#if __DEV
			CheckEntityName(pObj);
#endif

			if(IsEntityInList(pObj, m_PotentialEntitiesToExport))
				continue;

			CBaseModelInfo * pModelInfo = pObj->GetBaseModelInfo();

			bool bObjectIntersectsNavMesh = EntityIntersectsCurrentNavMesh(pObj);
			if(!bObjectIntersectsNavMesh)
			{
				static bool bForce=true;
				CObjectPopulation::ConvertToDummyObject(pObj, bForce);

				continue;
			}

			bool bFixedForNav = pModelInfo->GetIsFixedForNavigation() != 0;
			bool bFixedFlagSet = pObj->IsBaseFlagSet(fwEntity::IS_FIXED) || pModelInfo->GetIsFixed();
			bool bScriptObject = (pObj->GetOwnedBy() == ENTITY_OWNEDBY_SCRIPT);

			if(bScriptObject)
				continue;
/* Not interested in ladders for audio

			bool bIsLadder = pModelInfo->GetIsLadder() != 0;

			// Export ladder info so we can link ladders up in the navmesh
			if(bIsLadder && EntityIntersectsCurrentNavMesh(pObj))
			{
				ExportLadderInfo(pObj, &m_LadderInfos);
			}

			*/

			// Don't export any geometry which isn't avoided by peds
			if(pModelInfo->GetNotAvoidedByPeds())
				continue;

			// If this object is not fixed in any way, then don't export its geometry
			if(!bFixedFlagSet && !bFixedForNav)
				continue;

			// Also add ladders to the export list, as we will want to export them if they are fixed objects
			m_PotentialEntitiesToExport.push_back(pObj);
		}
	}


	//*****************************************************************
	// Now export all the extra bounds from the buildings in the world
	//*****************************************************************

	static CLinkList<CEntity*> * pBuildingsWithPhysicsList = NULL;
	if(!pBuildingsWithPhysicsList)
	{
		pBuildingsWithPhysicsList = rage_new CLinkList<CEntity*>;
		memset(pBuildingsWithPhysicsList, 0, sizeof(CLinkList<CEntity*>));
		pBuildingsWithPhysicsList->Init(32768);
		pBuildingsWithPhysicsList->Clear();
	}

	GetAllBuildingsWithPhysics(INSTANCE_STORE, pBuildingsWithPhysicsList);
	CLink<CEntity*> * pBuildingPtr = pBuildingsWithPhysicsList->GetFirst();

	while(pBuildingPtr)
	{
		CEntity* pEntity = pBuildingPtr->item;

		if(pEntity)
		{
#if __DEV
			CheckEntityName(pEntity);
#endif
			if(!IsEntityInList(pEntity, m_PotentialEntitiesToExport))
			{
				m_PotentialEntitiesToExport.push_back(pEntity);
			}
		}
		pBuildingPtr = pBuildingPtr->GetNext();
	}
	pBuildingsWithPhysicsList->Clear();

	//*************************
	// Go though IPL store.
	//*************************

	for(s32 i=0; i<INSTANCE_STORE.GetSize(); i++)
	{
		fwMapDataDef* pDef = INSTANCE_STORE.GetSlot(i);
		const bool bStreamed = true;

		if(pDef && pDef->IsLoaded() && bStreamed
			&& INSTANCE_STORE.GetStreamingFlags(i)&STR_DONTDELETE_MASK)
		{
			fwEntity** entities = pDef->GetEntities();

			if (entities)
			{
				for (u32 i=0; i<pDef->GetNumEntities(); i++)
				{
					CEntity* pEntity = (CEntity*)entities[i];
					if(pEntity)
					{
#if __DEV
						CheckEntityName(pEntity);
#endif
						if(EntityIntersectsCurrentNavMesh((CEntity*)pEntity) && !IsEntityInList((CEntity*)pEntity, m_PotentialEntitiesToExport))
						{
							m_PotentialEntitiesToExport.push_back((CEntity*)pEntity);
						}
					}
				}
			}
		}
	}

	//**********************************************
	// Gather interiors and add all entities within
	//***********************************************

	AddInteriorsToExportList(m_PotentialEntitiesToExport /*, portalBoundaries*/);

	int iSize = CInteriorInst::GetPool()->GetSize();
	for(int i=0; i<iSize; i++)
	{
		CInteriorInst * pIntInst = CInteriorInst::GetPool()->GetSlot(i);
		if(!pIntInst)
			continue;

		if(!IsEntityInList(pIntInst, m_PotentialEntitiesToExport))
		{
			m_PotentialEntitiesToExport.push_back(pIntInst);
		}

		AddInteriorsToExportListCB(pIntInst, (void*)&m_PotentialEntitiesToExport);
	}

	//*******************************************************
	// Go through comp entities and all all entities within
	//*******************************************************

	// When we have reached CE_STATE_START, we can check to see if we should have
	// physics, and if so, wait for that to stream in and get set up. Note that it's
	// the child object we're looking at here.

	for(int i = 0; i < CCompEntity::GetPool()->GetSize(); i++)
	{
		CCompEntity* pComp = CCompEntity::GetPool()->GetSlot(i);
		if(pComp)
		{
#if __DEV
			CheckEntityName(pComp);
#endif
			if(EntityIntersectsCurrentNavMesh(pComp) && !IsEntityInList(pComp, m_PotentialEntitiesToExport))
			{
				m_PotentialEntitiesToExport.push_back(pComp);
			}
		}
	}

	for(u32 e=0; e<m_PotentialEntitiesToExport.size(); e++)
	{
		CEntity * pEntity = m_PotentialEntitiesToExport[e];
		if(pEntity && pEntity->GetIsTypeComposite())
		{
			CCompEntity * pCompEntity = (CCompEntity*)pEntity;

			const atFixedArray<RegdObj, 10> & objects = pCompEntity->GetChildObjects();
			for(s32 o=0; o<objects.GetCount(); o++)
			{
				CObject* pChild = objects[o];
				if(pChild && !IsEntityInList(pChild, m_PotentialEntitiesToExport))
				{
					m_PotentialEntitiesToExport.push_back(pChild);
				}
			}
		}
	}

	//************************************************************************************

	u32 e;
	for(e=0; e<m_PotentialEntitiesToExport.size(); e++)
	{
		CEntity * pEntity = m_PotentialEntitiesToExport[e];

		if(!pEntity)
			continue;

#if __BREAK_ON_ENTITY_NAME
		CheckEntityName(pEntity);
#endif

		fragInst * pFrag = pEntity->GetFragInst();
		const fragType * pFragType = pFrag ? pFrag->GetType() : NULL;

		MaybeExportCollisionForEntity(pEntity, pFragType, vNavMeshMinsExtra, vNavMeshMaxsExtra, triangleVertices, triangleMaterials, colPolyFlags);
	}

	//*************************************************************************************************************************

	Vector3 vActualMinOfGeometry(FLT_MAX, FLT_MAX, FLT_MAX);
	Vector3 vActualMaxOfGeometry(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	u32 iNumVerts = triangleVertices.size();

	u32 v;
	for(v=0; v<iNumVerts; v++)
	{
		Vector3 * vVert = &triangleVertices[v];
		if(vVert->x < vActualMinOfGeometry.x) vActualMinOfGeometry.x = vVert->x;
		if(vVert->y < vActualMinOfGeometry.y) vActualMinOfGeometry.y = vVert->y;
		if(vVert->z < vActualMinOfGeometry.z) vActualMinOfGeometry.z = vVert->z;
		if(vVert->x > vActualMaxOfGeometry.x) vActualMaxOfGeometry.x = vVert->x;
		if(vVert->y > vActualMaxOfGeometry.y) vActualMaxOfGeometry.y = vVert->y;
		if(vVert->z > vActualMaxOfGeometry.z) vActualMaxOfGeometry.z = vVert->z;
	}

	if(iNumVerts)
	{
		Displayf("Actual Mins Of Geometry : (%.2f, %.2f, %.2f)\n", vActualMinOfGeometry.x, vActualMinOfGeometry.y, vActualMinOfGeometry.z);
		Displayf("Actual Maxs Of Geometry : (%.2f, %.2f, %.2f)\n", vActualMaxOfGeometry.x, vActualMaxOfGeometry.y, vActualMaxOfGeometry.z);
	}

	//*****************************************************************
	// Only export tri/extras files if this navmsh section intersects
	// the world extents + a size for open sea around the world

	spdAABB worldBoxWithSeaBorder = g_WorldLimits_MapDataExtentsAABB;
	ScalarV fSeaBorderSize;
	fSeaBorderSize.Setf(g_fSeaBorderSize);
	worldBoxWithSeaBorder.GrowUniform(fSeaBorderSize);

	Vector3 bbMin = VEC3V_TO_VECTOR3( worldBoxWithSeaBorder.GetMin() );
	Vector3 bbMax = VEC3V_TO_VECTOR3( worldBoxWithSeaBorder.GetMax() );

	const fwNavGenConfig &cfg = fwNavGenConfigManager::GetInstance().GetConfig();

	bbMin.z = cfg.m_ExporterMinZCutoff;
	bbMax.z = cfg.m_ExporterMaxZCutoff;

	// May as well at least have an assert somewhere, now when these values are
	// specified in a data file. /FF
	Assertf(bbMin.z < bbMax.z, "Invalid Z cutoff values %.1f/%.1f.", bbMin.z, bbMax.z);

	worldBoxWithSeaBorder.Set(VECTOR3_TO_VEC3V(bbMin), VECTOR3_TO_VEC3V(bbMax));

	if(worldBoxWithSeaBorder.IntersectsAABB(spdAABB(VECTOR3_TO_VEC3V(m_vNavMeshMins), VECTOR3_TO_VEC3V(m_vNavMeshMaxs))))
	{

		//*************************************************************************
		//	Flush any remaining collision triangles to disk

		if(triangleVertices.size() || m_iHasWaterInCurrentNavMesh)
		{
			FlushTriListToTriFile(triangleVertices, triangleMaterials, colPolyFlags);
		}

		//*************************************************************************
		//	Output the extras file, if we need one



/* Not interested in ladders etc

		// Find nearby ladders
		Vector3 vExtra(50.0f,50.0f,50.0f);
		fwIsBoxIntersectingApprox cullBox(spdAABB(VECTOR3_TO_VEC3V(m_vNavMeshMins-vExtra), VECTOR3_TO_VEC3V(m_vNavMeshMaxs+vExtra)));
		const Vec3V cullSphereCenterV = VECTOR3_TO_VEC3V((m_vNavMeshMins+m_vNavMeshMaxs)*0.5f);
		const ScalarV cullSphereRadiusV(m_fWidthOfNavMesh*2.0f);
		fwIsSphereIntersecting cullSphere(spdSphere(cullSphereCenterV, cullSphereRadiusV));

		//********************************************************
		// Gather a list of ladders from this section of the map

		vector<CEntity*> laddersFromMap;
		CGameWorld::ForAllEntitiesIntersecting(
			&cullBox,
			AddLadderFromMapCB,
			&laddersFromMap,
			ENTITY_TYPE_MASK_BUILDING,
			SEARCH_LOCATION_EXTERIORS | SEARCH_LOCATION_INTERIORS,
			SEARCH_LODTYPE_ALL,
			SEARCH_OPTION_FORCE_PPU_CODEPATH);

		//*************************************************************************************************
		// Now go through this list and export the ladders
		// I had to break this code up because we can no longer do nested ForAllEntitiesIntersecting()..

		for(u32 l=0; l<laddersFromMap.size(); l++)
		{
			CEntity * pLadderEntity = laddersFromMap[l];
			ScanForLaddersToExport(pLadderEntity, &m_LadderInfos);
		}

		//*****************************************************************************
		// Output the positions & radii of all dynamic objects in this area of map
		// This is used to reset the "bNetworkSpawnCandidate" flag on polygons which
		// are intersecting dynamic objects

		m_DynamicEntitiesList.clear();
		//u32 iFlags = ENTITY_TYPE_MASK_OBJECT|ENTITY_TYPE_MASK_MLO|ENTITY_TYPE_MASK_COMPOSITE|ENTITY_TYPE_MASK_DUMMY_OBJECT;
		//u32 iFlags = ENTITY_TYPE_MASK_OBJECT|ENTITY_TYPE_MASK_COMPOSITE|ENTITY_TYPE_MASK_DUMMY_OBJECT;
		//CGameWorld::ForAllEntitiesIntersecting(&cullBox, AddDynamicObjectFromMapCB, &m_DynamicEntitiesList, iFlags, iCtrlFlags, SEARCH_LODTYPE_HIGHDETAIL, SEARCH_OPTION_FORCE_PPU_CODEPATH);

		CObject::Pool* pObjectPool = CObject::GetPool();

		for(int o=0; o<pObjectPool->GetSize(); o++)
		{
			CObject * pObj = pObjectPool->GetSlot(o);
			if(!pObj)
				continue;

			if(EntityIntersectsCurrentNavMesh(pObj))
			{
				AddDynamicObjectFromMapCB(pObj, &m_DynamicEntitiesList);
			}
		}

		//**************************
		// Find car nodes in area
		FindCarNodesInArea(ThePaths, m_vNavMeshMins, m_vNavMeshMaxs, m_CarNodesList);

	*/

		//*****************************************************************************
		// Output the positions & radii of all dynamic objects in this area of map
		// This is used to reset the "bNetworkSpawnCandidate" flag on polygons which
		// are intersecting dynamic objects

		m_DynamicEntitiesList.clear();

		CObject::Pool* pObjectPool = CObject::GetPool();

		for(int o=0; o<pObjectPool->GetSize(); o++)
		{
			CObject * pObj = pObjectPool->GetSlot(o);
			if(!pObj)
				continue;

			if(EntityIntersectsCurrentNavMesh(pObj))
			{
				AddDynamicObjectFromMapCB(pObj, &m_DynamicEntitiesList);
			}
		}


	}

	CloseTriFile(true);

	triangleVertices.clear();
	triangleMaterials.clear();

	Displayf("Num triangles exported : %i\n", m_iNumTrianglesThisTriFile);

// Maybe this would be useful? /FF
#if 0
	u8 bHasVariableWaterLevel = (m_pWaterLevels != NULL);
	s32 iNumSamples = 0;
	if(bHasVariableWaterLevel)
	{
		u32 iBlockSize = CPathServerExtents::m_iNumSectorsPerNavMesh;
		float fSampleSpacing = (CPathServerExtents::GetWorldWidthOfSector() * ((float)iBlockSize)) / ms_fWaterSamplingStepSize;
		iNumSamples = (s32)(fSampleSpacing * fSampleSpacing);
	}
	Displayf("Num water samples exported : %i", iNumSamples);
#endif

	if(m_pExportCollisionLogFile)
	{
		fprintf(m_pExportCollisionLogFile, "Num triangles exported : %i\n", m_iNumTrianglesThisTriFile);
	}

#endif	//__AUDMESH_EXPORT_DISABLE_COLLISION_EXPORT

	//----------------------------
	// Clear forced flags on IPLs

	slotList.clear();
	store.GetBoxStreamer().GetIntersectingAABB(aabb, fwBoxStreamerAsset::MASK_MAPDATA, slotList);
	for (s32 i=0; i<slotList.GetCount(); i++)
	{
		u32 slot = slotList[i];
		if (slot != 0xffffffff)
		{
			bool bLoaded = store.HasObjectLoaded(slot);
			if (!bLoaded)
			{
				if(bOutputMissingIPLs)
					Displayf("IMAP file %s not loaded!", store.GetName(slot) );
				bAllIPLsLoaded = false;

				store.ClearRequiredFlag(slot, STRFLAG_PRIORITY_LOAD|STRFLAG_FORCE_LOAD);
			}
		}
	}


	iWaitFrames = iNumWaitFrames;
	m_fTimeUntilWeExportThisNavMesh = TIME_TO_ALLOW_STREAMING_TO_CATCH_UP;

	m_ExtrasFileName = extrasFileName;
	if(!NextCellOrSubSection())
	{
		return false;
	}

	//------------------------------------
	// Reset state on any comp entities

	for(int i = 0; i < CCompEntity::GetPool()->GetSize(); i++)
	{
		CCompEntity* pComp = CCompEntity::GetPool()->GetSlot(i);
		if(pComp && !EntityIntersectsCurrentNavMesh(pComp))
		{
			pComp->Reset();
			pComp->ImmediateUpdate();
		}
	}

	fwTimer::Resume();

	//***********************************************************************************
	// WORKAROUND for interiors leaking fwArchetypes under the new streaming archetype code.
	// If we had interiors streamed in this navmesh, then perform a streaming flush.

	if(flushStreams)
	{
		CStreaming::GetCleanup().RequestFlush(false);
	}

	g_bWaitingForInteriorArchetypes = false;

	return true;
}


void CAudMeshDataExporterTool::LaunchAudMeshCompiler()
{
	printf("Launching NavMeshMaker....\n");
	char fullBatchFileName[512];

	if(PARAM_scheduledCompile.Get())
	{
		formatf(fullBatchFileName, "%s\\export_complete.txt", m_OutputPath);
		fiStream * pFile = fiStream::Create(fullBatchFileName);
		Assert(pFile);

		pFile->Close();
		return;
	}
	else
	{
		const char * pExportPath = m_OutputPath;
		const char * pCmdFileName = NULL;
		const char * pBatchFileName = NULL;

		if(!PARAM_audgenBatchFile.Get(pBatchFileName))
		{
			Assertf(0, "You need to specify a batch file with \'-navGenBatchFile\'");
			return;
		}

		if(!PARAM_audgenCmdFile.Get(pCmdFileName))
		{
			Assertf(0, "You need to specify a cmd file with \'-navGenCmdFile\'");
			return;
		}

		ReplaceChar(const_cast<char*>(pBatchFileName), '\\', '/');
		ReplaceChar(const_cast<char*>(pExportPath), '\\', '/');
		ReplaceChar(const_cast<char*>(pCmdFileName), '\\', '/');

		formatf(
			fullBatchFileName,
			"%s %s %s",
			pBatchFileName,
			pExportPath,
			pCmdFileName
		);
	}

#if __WIN32PC

	ShellExecute(
		GetDesktopWindow(),
		"open",
		fullBatchFileName,
		NULL,
		"x:/tools_release/navgen/NavMeshMaker",
		SW_SHOW
		);

#else

	sysExec(fullBatchFileName);

#endif

}

//***********************************************************************
//	Not actually an export function.  This just adds the info about this
//	ladder to a list, to be exported later


/* Not interested in ladders for audio

bool CAudMeshDataExporterTool::ExportLadderInfo(CEntity * pLadder, vector<fwNavLadderInfo> * laddersList)
{
	Vector3 vTopOfLadder, vBottomOfLadder;
	bool bCanGetOffAtTop;
	static const float fSameLadderEps = 1.0f;

	int iLadderIndex=-1;
	bool bOk = CTaskGoToAndClimbLadder::FindTopAndBottomOfAllLadderSections(pLadder, iLadderIndex, vTopOfLadder, vBottomOfLadder, bCanGetOffAtTop);

	if(!bOk)
		return false;

	if(vTopOfLadder.z < vBottomOfLadder.z)
	{
		float z = vTopOfLadder.z;
		vTopOfLadder.z = vBottomOfLadder.z;
		vBottomOfLadder.z = z;
	}

	if(vTopOfLadder.x < m_vNavMeshMins.x || vTopOfLadder.y < m_vNavMeshMins.y ||
		vTopOfLadder.x > m_vNavMeshMaxs.x || vTopOfLadder.y > m_vNavMeshMaxs.y)
		return false;

	if(!bCanGetOffAtTop)
		return false;

	// See if we already have this ladder in our list.  This will happen because some ladders are
	// made up of multiple CObject sections, and this function will be called for each sections.
	// The calculated top & bottom will be of all the ladder sections.
	for(u32 l=0; l<laddersList->size(); l++)
	{
		fwNavLadderInfo & ladderInfo = (*laddersList)[l];
		if(ladderInfo.GetBase().IsClose(vBottomOfLadder, fSameLadderEps) &&
			ladderInfo.GetTop().IsClose(vTopOfLadder, fSameLadderEps))
		{
			return false;
		}
	}

	Vector3 vNormal;
	s32 iEffectIndex = 0;
	if( CTaskGoToAndClimbLadder::GetNormalOfLadderFrom2dFx(pLadder, vNormal, iEffectIndex) )
	{
		fwNavLadderInfo ladderInfo;
		ladderInfo.SetTop(vTopOfLadder);
		ladderInfo.SetBase(vBottomOfLadder);
		ladderInfo.SetNormal(vNormal);

		laddersList->push_back(ladderInfo);
	}

	return true;
}

bool CAudMeshDataExporterTool::AddLadderFromMapCB(CEntity * pEntity, void * pData)
{
	vector<CEntity*> * ladderEntitiesFromMap = (vector<CEntity*>*) pData;
	ladderEntitiesFromMap->push_back(pEntity);
	return true;
}

bool CAudMeshDataExporterTool::ScanForLaddersToExport(CEntity * pEntity, vector<fwNavLadderInfo> * laddersList)
{
	if( !CModelInfo::IsValidModelInfo(pEntity->GetModelIndex() ))
		return true;

	CBaseModelInfo * pModelInfo = pEntity->GetBaseModelInfo();

	if(!pModelInfo->GetNum2dEffects())
	{
		//Assertf(false, "%s:Ladder doesn't have any 2dEffects defined.\n", pModelInfo->GetModelName());
		return true;
	}

	int iNumEffects = pModelInfo->GetNum2dEffects();
	for(int c=0; c<iNumEffects; c++)
	{
		C2dEffect* pEffect = pModelInfo->Get2dEffect(c);
		if(pEffect->GetType() == ET_LADDER)
		{
			ExportLadderInfo(pEntity, laddersList);
		}
	}

	return true;
}

*/

bool CAudMeshDataExporterTool::AddDynamicObjectFromMapCB(CEntity * pEntity, void * pData)
{
	switch(pEntity->GetType())
	{
		case ENTITY_TYPE_OBJECT:
			break;
		default:
			return true;
	}
	vector<fwEntity*> * entitiesFromMap = (vector<fwEntity*>*) pData;
	entitiesFromMap->push_back((fwEntity*)pEntity);
	return true;
}



//////////////////////////////////////////////////////////////////////////
// FUNCTION:	GetAllBuildingsWithPhysics
// PURPOSE:		adds all loaded buildings with physics insts to results list
//////////////////////////////////////////////////////////////////////////

void CAudMeshDataExporterTool::GetAllBuildingsWithPhysics(fwMapDataStore &store, CLinkList<CEntity*>* pResultsList)
{
	atArray<RegdEnt>& list = CPhysics::ms_buildingsWithPhysics;
	for(int i = 0; i < list.GetCount(); i++)
	{
		CEntity* pEntity = list[i];
		if(pEntity && pEntity->GetCurrentPhysicsInst() && pEntity->GetIsTypeBuilding())
		{
			pResultsList->Insert(pEntity);
		}
	}
}

bool CAudMeshDataExporterTool::RequestAndWaitForCompEntities()
{
	bool waitingForSomething = false;

	// Note: I'm not sure if it's better to loop over the CCompEntity objects
	// in the pool, or loop over the IPLs like GetAllBuildingsWithPhysics() does.
	// But, the CCompEntity iteration seems a little simpler and less likely to
	// go wrong.

	for(int i = 0; i < CCompEntity::GetPool()->GetSize(); i++)
	{
		CCompEntity* pComp = CCompEntity::GetPool()->GetSlot(i);
		if(!pComp)
		{
			continue;
		}

		m_bHasCompEntities = true;

		// First, we wait for CCompEntity to get to the CE_STATE_START state,
		// at which point it should have switched to the start model that we
		// may want to include in the audmesh.
		if(pComp->GetState() != CE_STATE_START)
		{
			pComp->ImmediateUpdate();

			// Not ready yet.
			waitingForSomething = true;

			// Continue with the next object.
			continue;
		}

		// When we have reached CE_STATE_START, we can check to see if we should have
		// physics, and if so, wait for that to stream in and get set up. Note that it's
		// the child object we're looking at here.
		CObject* pChild = pComp->GetPrimaryAnimatedObject();
		if(pChild)
		{
			if(pChild->IsArchetypeSet() && CModelInfo::IsValidModelInfo(pChild->GetModelIndex()))
			{
				CBaseModelInfo* pModelInfo = pChild->GetBaseModelInfo();
				if(!(pModelInfo->GetPhysicsDictionary() == -1 && pModelInfo->GetDrawableType() != CBaseModelInfo::DT_FRAGMENT))
				{
					if(!pChild->GetCurrentPhysicsInst())
					{
						waitingForSomething = true;
					}
				}
			}
		}
	}

	// Note that once the CCompEntity objects have streamed in completely, we shouldn't have
	// to do anything special to include them in the navmesh - their child objects should
	// have been inserted in the data structures we pull other objects from.

	return !waitingForSomething;
}


void CAudMeshDataExporterTool::ScanForBuildings(atArray<RegdEnt>& aRequiredBuildings)
{
	// This is probably not technically necessary when we have an active exporter,
	// but it's being called just in case, so that the new box-based code can't
	// cause any objects to go missing that were present before.
//	CPhysics::ScanForBuildingsNearPlayer(aRequiredBuildings);

	CNavMeshDataExporterInterface* ex = CNavMeshDataExporter::GetActiveExporter();
	if(!ex)
	{
		// The export may have ended or something, no worries.
		return;
	}

	// Since CNavMeshDataExporterTool contains the CAudMeshDataExporterInterfaceTool,
	// that's certainly what we would expect here.
	Assert(dynamic_cast<CAudMeshDataExporterInterfaceTool*>(ex));

	// Need to cast it to find the CNavMeshDataExporterTool instance to get the bounding box of the current cell.
	CAudMeshDataExporterInterfaceTool* exCast = static_cast<CAudMeshDataExporterInterfaceTool*>(ex);
	Vector3 meshMins = exCast->m_Tool->m_vNavMeshMins;
	Vector3 meshMaxs = exCast->m_Tool->m_vNavMeshMaxs;

	// Create a bounding box and do a search, using the same callback as in
	// the usual CPhysics::ScanForBuildingsNearPlayer().
	spdAABB bbox(VECTOR3_TO_VEC3V(meshMins), VECTOR3_TO_VEC3V(meshMaxs));
	fwIsBoxIntersectingBB searchVol(bbox);
	CGameWorld::ForAllEntitiesIntersecting
	(
		&searchVol,
		CPhysics::AppendBuildingCB,
		(void*) &aRequiredBuildings,
		ENTITY_TYPE_MASK_BUILDING | ENTITY_TYPE_MASK_ANIMATED_BUILDING, //ENTITY_TYPE_MASK_MLO,
		SEARCH_LOCATION_EXTERIORS,
		SEARCH_LODTYPE_HIGHDETAIL,
		SEARCH_OPTION_FORCE_PPU_CODEPATH
	);

}


/* Not interested in car nodes for audio

void CAudMeshDataExporterTool::FindCarNodesInArea(CPathFind &paths, const Vector3 & vMin, const Vector3 & vMax, atArray<Vector3> & carNodesList)
{
	carNodesList.clear();

	for(int Region=0; Region<PATHFINDREGIONS; Region++)
	{
		// Only if this Region is loaded
		if(!paths.apRegions[Region] || !paths.apRegions[Region]->aNodes)
		{
			continue;
		}

		int iStart = 0;
		int iEnd = paths.apRegions[Region]->NumNodesCarNodes;

		for(int n=iStart; n<iEnd; n++)
		{
			CPathNode * pNode = &((paths.apRegions[Region]->aNodes)[n]);
			if(pNode && pNode->m_1.m_specialFunction!=SPECIAL_USE_NONE)
			{
				continue;
			}

			Vector3 vPos;
			pNode->GetCoors(vPos);

			if(vPos.x >= vMin.x && vPos.x <= vMax.x && vPos.y >= vMin.y && vPos.y <= vMax.y)
			{
				carNodesList.PushAndGrow(vPos);
			}
		}
	}
}

*/

void CAudMeshDataExporterTool::FindWaterLevelForCurrentNavMesh()
{
	float fAverageHeight = 0.0f;
	int iNumHits = 0;

	float fX,fY;
	static const float fZ = 100.0f;

	float fSampleSpacing = (CPathServerExtents::GetWorldWidthOfSector() * ((float)CPathServerExtents::m_iNumSectorsPerNavMesh)) / ms_fWaterSamplingStepSize;
	s32 iNumSamples = (s32)(fSampleSpacing * fSampleSpacing);

	bool bVariableWaterHeight = false;
	float fLastValidSample = -32000.0f;

	if(m_pWaterLevels)
	{
		delete[] m_pWaterLevels;
		m_pWaterLevels = NULL;
	}

	m_pWaterLevels = rage_new s16[iNumSamples];

	int iSample=0;

	for(fY=m_vNavMeshMins.y; fY<m_vNavMeshMaxs.y; fY+=ms_fWaterSamplingStepSize)
	{
		for(fX=m_vNavMeshMins.x; fX<m_vNavMeshMaxs.x; fX+=ms_fWaterSamplingStepSize)
		{
			float fWaterHeight = 0.0f;
			bool bHitWater = Water::GetWaterLevelNoWaves(Vector3(fX, fY, fZ), &fWaterHeight, 2000.0f, 2000.0f, NULL);

			if(bHitWater)
			{
				iNumHits++;
				fAverageHeight+=fWaterHeight;

				m_pWaterLevels[iSample] = (s16)fWaterHeight;

				if(fLastValidSample != -32000.0f && fLastValidSample != fWaterHeight)
				{
					bVariableWaterHeight = true;
				}

				fLastValidSample = fWaterHeight;
			}
			else
			{
				m_pWaterLevels[iSample] = -32000;
			}

			iSample++;
		}
	}

	// If we hit less water than the samples we took, then this counts as variable water heights
	if(iNumHits != 0 && iNumHits != iNumSamples)
	{
		bVariableWaterHeight = true;
	}

	if(iNumHits>0)
	{
		fAverageHeight /= ((float)iNumHits);
		m_iHasWaterInCurrentNavMesh = true;
		m_fAverageWaterLevelInCurrentNavMesh = fAverageHeight;
	}
	else
	{
		m_iHasWaterInCurrentNavMesh = false;
		m_fAverageWaterLevelInCurrentNavMesh = 0.0f;
	}

	//**************************************************************************
	//	If there was a variable water height (or if there were some samples
	//	with water & some without) then we'll need to save out a file detailing
	//	the water levels for this navmesh region.

	if(!bVariableWaterHeight)
	{
		if(m_pWaterLevels)
		{
			delete[] m_pWaterLevels;
			m_pWaterLevels = NULL;
		}
	}
}

// build list of objects which need to have physics loaded
// ToDo: only pull in objects which are necessary. (rooms with active portal tracking taking place).
void CAudMeshDataExporterTool::AppendToActiveBuildingsArray(atArray<CEntity*>& entityArray)
{
	g_bAllInteriorsInstanced = true;

	if(!m_bExportInteriors)
	{
		return;
	}
	
	static const float fExtraRadius = 30.0f;

	int iSize = CInteriorProxy::GetPool()->GetSize();
	for(int i=0; i<iSize; i++)
	{
		CInteriorProxy * pIntProxy = CInteriorProxy::GetPool()->GetSlot(i);
		if(!pIntProxy)
			continue;

		bool bRequestThisInterior = false;

		/*
		Vector3 vIntCentre;
		pIntProxy->GetPosition(vIntCentre);
		float fIntRadius = pIntProxy->GetBoundRadius() + fExtraRadius;

		const Vector3 vIntMin = vIntCentre - Vector3(fIntRadius,fIntRadius,fIntRadius);
		const Vector3 vIntMax = vIntCentre + Vector3(fIntRadius,fIntRadius,fIntRadius);
		*/

		spdAABB bbox;
		pIntProxy->GetBoundBox(bbox);
		Vector3 vIntMin = VEC3V_TO_VECTOR3(bbox.GetMin());
		Vector3 vIntMax = VEC3V_TO_VECTOR3(bbox.GetMax());
		vIntMin -= Vector3(fExtraRadius, fExtraRadius, fExtraRadius);
		vIntMax += Vector3(fExtraRadius, fExtraRadius, fExtraRadius);

		bRequestThisInterior = fwNavToolHelperFuncs::BoundingBoxesOverlap(vIntMin, vIntMax, m_vNavMeshMins, m_vNavMeshMaxs);

		// Is this the interior which we are currently exporting?

		if(bRequestThisInterior)
		{
			m_bHasBuildingsWithPhysics = true;

			pIntProxy->SetRequestedState(CInteriorProxy::RM_SCRIPT, CInteriorProxy::PS_FULL_WITH_COLLISIONS, true);
			
			if(pIntProxy->GetCurrentState() != CInteriorProxy::PS_FULL_WITH_COLLISIONS)
				g_bAllInteriorsInstanced = false;

			CInteriorInst * pIntInst = pIntProxy->GetInteriorInst();

			if(pIntInst)
			{
				entityArray.Append() = pIntInst;

				if(!pIntInst->m_bInUse)
				{
					CPortal::AddToActiveInteriorList(pIntInst);

					const char * pIntName = fwArchetypeManager::GetArchetypeName(pIntInst->GetModelIndex());
					printf("Populating interior \"%s\" (0x%x)\n", pIntName, pIntInst);
					Displayf("Populating interior \"%s\" (0x%x)\n", pIntName, pIntInst);			
				}

				pIntInst->m_bInUse = true;

				const u32 reqFlags = STRFLAG_LOADSCENE|STRFLAG_PRIORITY_LOAD|STRFLAG_FORCE_LOAD;
				if(pIntInst->m_bInUse)
					pIntInst->RequestObjects(reqFlags, NULL, true);

				CStreaming::LoadAllRequestedObjects();

				// convert dummy objects to real ones (every frame)
				if(pIntInst->m_bInstanceDataGenerated)
					pIntInst->AppendPortalActiveObjects(entityArray);
				pIntInst->AppendActiveObjects(entityArray);


				//---------------------------------------------
				// Instance comp entities within this interior

				static bool bBreak = false;
				CEntity * pIntEntity = NULL;
				pIntInst->RestartEntityInInteriorList();
				while(pIntInst->GetNextEntityInInteriorList(&pIntEntity))
				{
#if __DEV
					CheckEntityName(pIntEntity);
#endif
					if(pIntEntity->GetIsTypeComposite())
					{
						CCompEntity * pComp = (CCompEntity*)pIntEntity;
						if(pComp->GetState() != CE_STATE_SYNC_STARTING &&
							pComp->GetState() != CE_STATE_STARTING && 
							pComp->GetState() != CE_STATE_START)
						{
							pComp->SetToStarting();
						}
						pComp->ImmediateUpdate();
					}
				}			

#if __DEV
				//PrintInterior(pIntInst);
#endif
			}	// pIntInst != NULL
		}
		else
		{
			pIntProxy->SetRequestedState(CInteriorProxy::RM_SCRIPT, CInteriorProxy::PS_NONE, true);
			pIntProxy->CleanupInteriorImmediately();
			pIntProxy->SetRequiredByLoadScene(false);

			//pIntProxy->CleanupInteriorImmediately();
			//pIntInst->DepopulateInterior();
			//pIntInst->RemovePhysics();
			//pIntInst->DeleteInst();
			//pIntInst->m_bInUse = false;		

			CInteriorInst * pIntInst = pIntProxy->GetInteriorInst();
			if(pIntInst)
			{
				CPortal::RemoveFromActiveInteriorList(pIntInst);
			}
		}
	}
}


bool CAudMeshDataExporterTool::MaybeExportCollisionForEntity(CEntity * pEntity, const fragType * pFragType, Vector3 & vNavMeshMinsExtra, Vector3 & vNavMeshMaxsExtra, vector<Vector3> & triangleVertices, vector<phMaterialMgr::Id> & triangleMaterials, vector<u16> & colPolyFlags)
{
#if __DEV
	CheckEntityName(pEntity);
#endif

	// Never export dummy objects
	if(pEntity->GetIsTypeDummyObject())
		return false;

	CBaseModelInfo * pModelInfo = pEntity->GetBaseModelInfo();

	u16 iColPolyFlag=0;

	// Don't export trees
	if(pModelInfo && pModelInfo->GetIsTree())
		iColPolyFlag |= EXPORTPOLYFLAG_DONT_CREATE_NAVMESH_ON_THIS;

	//if(pModelInfo && pModelInfo->GetDoesNotProvideCover())
	//	iColPolyFlag |= EXPORTPOLYFLAG_DOESNTPROVIDECOVER;

	// Set a flag for interiors
	if(pEntity->GetIsTypeMLO())
		iColPolyFlag |= EXPORTPOLYFLAG_INTERIOR;

	// Set a flag for river bounds
	if(pEntity->GetCurrentPhysicsInst() &&
		pEntity->GetCurrentPhysicsInst()->GetArchetype() &&
		(pEntity->GetCurrentPhysicsInst()->GetArchetype()->GetTypeFlags() & ArchetypeFlags::GTA_RIVER_TYPE)!=0)
	{
		iColPolyFlag |= EXPORTPOLYFLAG_RIVER;
	}

	bool bFixedForNav = false;
	bool bFixedFlagSet = false;
	bool bClimbableObject = false;

	// Only ever export objects if they have the "fixed for navigation" flag set.  This means that we can
	// cut them into the navmesh, since they will never ever move or be traversable - even if fragmented.
	if(pEntity->GetIsTypeObject())
	{
		CObject * pObj = (CObject*)pEntity;
		CBaseModelInfo * pModelInfo = pObj->GetBaseModelInfo();

		bFixedForNav = (pModelInfo->GetIsFixedForNavigation() != 0);
		bFixedFlagSet = pObj->IsBaseFlagSet(fwEntity::IS_FIXED) || pModelInfo->GetIsFixed();
		bClimbableObject = pModelInfo->GetIsClimbableByAI();

		bool bScriptObject = (pObj->GetOwnedBy() == ENTITY_OWNEDBY_SCRIPT);

		if(bScriptObject)
			return false;

		// Don't export any geometry which isn't avoided by peds
		if(pModelInfo->GetNotAvoidedByPeds())
			return false;

		// If this object is not fixed in any way, then don't export its geometry
		if(!bFixedFlagSet && !bFixedForNav && !bClimbableObject)
			return false;

		if(pModelInfo->GetUsesDoorPhysics())
			return false;

		if(bFixedFlagSet || bFixedForNav)
		{
			iColPolyFlag |= EXPORTPOLYFLAG_FIXEDOBJECT;
		}
		else if(bClimbableObject) // never set both FIXED and CLIMBABLE flags
		{
			iColPolyFlag |= EXPORTPOLYFLAG_CLIMBABLEOBJECT;
		}
	}

	phInst * pInstance = pEntity->GetCurrentPhysicsInst();

	if(pInstance)
	{
#if __AUDMESH_EXPORT_ONLY_MOVER_COLLISION
		const u32 typeFlags = CPhysics::GetLevel()->GetInstanceTypeFlags(pInstance->GetLevelIndex());
		if((typeFlags & NAVMESH_EXPORT_COLLISON_ARCHETYPES)==0)
			return false;
#endif

		phArchetype * pArchetype = pInstance->GetArchetype();
		phBound * pBound = pArchetype->GetBound();

		if(pBound)
		{
			const Matrix34 & instanceMat = RCC_MATRIX34(pInstance->GetMatrix());

			// Check that the min/max of this bound intersect the min/max of this navmesh area
			Vector3 vBoundMin;
			Vector3 vBoundMax;
			fwNavToolHelperFuncs::TransformObjectAABB(instanceMat,
					RCC_VECTOR3(pBound->GetBoundingBoxMin()),
					RCC_VECTOR3(pBound->GetBoundingBoxMax()),
					vBoundMin, vBoundMax);

			// We pass down a flag which tells that this object is fixed - in this way FIXED objects
			// generally DON'T HAVE NAVMESH CREATED UPON THEM.  However, if the fixed object is over
			// a certain size then we ignore this rule and allow it to generate navmesh - this is
			// because the artists are sometimes fond of placing huge fixed objects on the map, and
			// its crap when peds can't navigate on them.
			static const float fMinSizeForFixed = 4.0f;
			const Vector3 vBoundBoxSize = vBoundMax - vBoundMin;
			// Note: the condition below is suspicious. It's possible that it should be something like
			// !bFixedForNav && (Abs...) instead. The way it is now, bFixedForNav objects will always have
			// nav mesh on them, regardless of size, which may not be intentional. Fredrik and James discussed
			// this issue, and the conclusion was that it may be incorrect the way it is, but that it's best to
			// wait with a change until some situation comes up in practice. /FF
			if(bFixedForNav || (Abs(vBoundBoxSize.x) > fMinSizeForFixed && Abs(vBoundBoxSize.y) > fMinSizeForFixed))
			{
				iColPolyFlag &= ~EXPORTPOLYFLAG_FIXEDOBJECT;
			}

			if(pEntity->GetIsTypeMLO())
			{
				vBoundMin -= vExpandForInteriors;
				vBoundMax += vExpandForInteriors;
			}

#if __CHECK_TRIANGLES_AGAINST_NAVMESH_EXTENTS
			// Test against the bounds of the current navmesh
			if(vBoundMax.x < vNavMeshMinsExtra.x || vBoundMax.y < vNavMeshMinsExtra.y || vBoundMax.z < vNavMeshMinsExtra.z ||
				vBoundMin.x > vNavMeshMaxsExtra.x || vBoundMin.y > vNavMeshMaxsExtra.y || vBoundMin.z > vNavMeshMaxsExtra.z)
			{
				// No intersection possible
				return false;
			}
#else
			vNavMeshMinsExtra; vNavMeshMaxsExtra;
#endif

			ExportBound(pBound, pFragType, instanceMat, triangleVertices, triangleMaterials, colPolyFlags, iColPolyFlag);
			return true;
		}
	}

	return false;
}

//-----------------------------------------------------------------------------
