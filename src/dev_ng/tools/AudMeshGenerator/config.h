//
// navmeshgenerator/config.h
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef NAVMESHGENERATOR_CONFIG_H
#define NAVMESHGENERATOR_CONFIG_H

//#include "fwnavgen/toolnavmesh.h"

#include "fwnavgen/config.h"

//-----------------------------------------------------------------------------

struct fwAudGenConfig : public fwNavGenConfig
{

};

struct CAudMeshGeneratorConfig : public fwAudGenConfig
{
	static const CAudMeshGeneratorConfig& Get()
	{	return static_cast<const CAudMeshGeneratorConfig&>(fwNavGenConfigManager::GetInstance().GetConfig());	}

	bool	m_ExportVehicles;

	PAR_PARSABLE;
};

//-----------------------------------------------------------------------------

#endif	// NAVMESHGENERATOR_CONFIG_H

// End of file navmeshgenerator/config.h
