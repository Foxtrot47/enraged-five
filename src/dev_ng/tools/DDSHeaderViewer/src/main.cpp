// ========
// main.cpp
// ========

#include "common.h"
#include "image_DDS_helper.h"

int main(int argc, const char* argv[])
{
	if (argc > 1)
	{
		for (int i = 1; i < argc; i++)
		{
			printf("%s\n", argv[i]);
			printf("%s\n", DDS_helper::GetDescriptionString(argv[i]).c_str());
		}
	}
	else
	{
		printf("usage - drag DDS images onto executable\n");
	}

	system("pause");
	return 0;
}
