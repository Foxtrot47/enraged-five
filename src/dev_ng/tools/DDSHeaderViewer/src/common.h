// ========
// common.h
// ========

#ifndef COMMON_H
#define COMMON_H
#pragma once

#ifndef _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#endif
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <assert.h>
#include <math.h>

// STL
#include <string>
#include <vector>
#include <algorithm>

#define __COMMENT(x)
#define COMMA ,
#define EMPTY
#define STRING(x) #x
#define ASSERT(cond) assert(cond)
#define STATIC_ASSERT(expr,msg) typedef char STATIC_ASSERT_##msg[1][(expr)?1:0]
#define FOREACH(DEF) FOREACH_##DEF(DEF)
#define MACRO_EXPAND(...) __VA_ARGS__
#define SIZEOF(v) ((int)sizeof(v))
#define NUMBEROF(v) (SIZEOF(v)/SIZEOF((v)[0]))
#define INDEX_NONE (-1)

typedef signed char            schar8_t;
typedef signed short int       sint16_t;
typedef signed long int        sint32_t;
typedef signed long long int   sint64_t;
typedef unsigned char          uchar8_t; 
typedef unsigned short int     uint16_t;
typedef unsigned long int      uint32_t;
typedef unsigned long long int uint64_t;
typedef float                  real32_t;
typedef double                 real64_t;

template <typename T> __forceinline T Min(T a, T b) { return (a) <= (b) ? (a) : (b); }
template <typename T> __forceinline T Max(T a, T b) { return (a) >= (b) ? (a) : (b); }
template <typename T> __forceinline T Abs(T x) { return x >= T(0) ? x : -x; }
template <typename T> __forceinline T Clamp(T x, T minval, T maxval) { return Min<T>(Max<T>(minval, x), maxval); }

class CString : public std::string
{
public:
	__forceinline CString();
	__forceinline CString(const char* fmt, ...);
	__forceinline CString(const char* fmt, va_list args);

private:
	__forceinline void va_assign(const char* fmt, va_list args);
};

__forceinline CString::CString()
{
}

__forceinline CString::CString(const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	va_assign(fmt, args);
	va_end(args);
}

__forceinline CString::CString(const char* fmt, va_list args)
{
	va_assign(fmt, args);
}

__COMMENT(private) __forceinline void CString::va_assign(const char* fmt, va_list args)
{
	char temp[4096] = "";
	_vsnprintf(temp, NUMBEROF(temp), fmt, args);
	std::string::assign(temp);
}

#endif // COMMON_H
