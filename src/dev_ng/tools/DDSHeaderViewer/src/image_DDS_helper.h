// ==================
// image_DDS_helper.h
// ==================

#ifndef IMAGE_DDS_HELPER_H
#define IMAGE_DDS_HELPER_H
#pragma once

#include "image_DDS.h"

namespace DDS_helper {

bool IsPrintableFourCC(uint32_t cc);

D3DFORMAT GetFormatFromMask(int n_, uint32_t r_, uint32_t g_, uint32_t b_, uint32_t a_);
D3DFORMAT GetFormatFromFourCC(uint32_t cc, bool bSupportDXT = false);
int       GetFormatBitsPerPixel(D3DFORMAT fmt);

std::string GetFormatStringFromFourCC(uint32_t cc);
std::string GetFormatStringFromFormat(D3DFORMAT fmt, bool bSupportDXT = true);
std::string GetArrayString(const uint32_t* data, int count);
std::string GetDescriptionString(const char* path);
std::string GetDescriptionString(const DDSURFACEDESC2& header);

} // namespace DDS_helper

#endif // IMAGE_DDS_HELPER_H
