// ===========
// image_DDS.h
// ===========

#ifndef IMAGE_DDS_H
#define IMAGE_DDS_H
#pragma once

// http://msdn.microsoft.com/en-us/library/bb943991(VS.85).aspx
#ifndef MAKEFOURCC
#define MAKEFOURCC(ch0,ch1,ch2,ch3) ( \
	(uint32_t(ch0) << (8*0)) | \
	(uint32_t(ch1) << (8*1)) | \
	(uint32_t(ch2) << (8*2)) | \
	(uint32_t(ch3) << (8*3)) ) \
	// end.
#endif

#define DDPF_RGBA           (DDPF_RGB       | DDPF_ALPHAPIXELS)
#define DDPF_ALPHALUMINANCE (DDPF_LUMINANCE | DDPF_ALPHAPIXELS)
#define DDPF_BUMPDUDVALPHA  (DDPF_BUMPDUDV  | DDPF_ALPHAPIXELS)

// enum D3DFORMAT from d3d9types.h
#define FOREACH_DEF_D3DFMT(DEF) \
	DEF(R8G8B8        ,  20,  24, 0x00ff0000,0x0000ff00,0x000000ff,0x00000000, DDPF_RGB           ) \
	DEF(A8R8G8B8      ,  21,  32, 0x00ff0000,0x0000ff00,0x000000ff,0xff000000, DDPF_RGBA          ) \
	DEF(X8R8G8B8      ,  22,  32, 0x00ff0000,0x0000ff00,0x000000ff,0x00000000, DDPF_RGB           ) \
	DEF(A8B8G8R8      ,  32,  32, 0x000000ff,0x0000ff00,0x00ff0000,0xff000000, DDPF_RGBA          ) \
	DEF(X8B8G8R8      ,  33,  32, 0x000000ff,0x0000ff00,0x00ff0000,0x00000000, DDPF_RGB           ) \
	\
	DEF(A2R10G10B10   ,  35,  32, 0x3ff00000,0x000ffc00,0x000003ff,0xc0000000, DDPF_RGBA          ) \
	DEF(A2B10G10R10   ,  31,  32, 0x000003ff,0x000ffc00,0x3ff00000,0xc0000000, DDPF_RGBA          ) \
	\
	DEF(R5G6B5        ,  23,  16, 0x0000f800,0x000007e0,0x0000001f,0x00000000, DDPF_RGB           ) \
	DEF(A1R5G5B5      ,  25,  16, 0x00007c00,0x000003e0,0x0000001f,0x00008000, DDPF_RGBA          ) \
	DEF(X1R5G5B5      ,  24,  16, 0x00007c00,0x000003e0,0x0000001f,0x00000000, DDPF_RGB           ) \
	DEF(A4R4G4B4      ,  26,  16, 0x00000f00,0x000000f0,0x0000000f,0x0000f000, DDPF_RGBA          ) \
	DEF(X4R4G4B4      ,  30,  16, 0x00000f00,0x000000f0,0x0000000f,0x00000000, DDPF_RGB           ) \
	\
	DEF(R3G3B2        ,  27,   8, 0x000000e0,0x0000001c,0x00000003,0x00000000, DDPF_RGB           ) \
	DEF(A8R3G3B2      ,  29,  16, 0x000000e0,0x0000001c,0x00000003,0x0000ff00, DDPF_RGBA          ) \
	\
	DEF(A8            ,  28,   8, 0x00000000,0x00000000,0x00000000,0x000000ff, DDPF_ALPHA         ) \
	DEF(L8            ,  50,   8, 0x000000ff,0x00000000,0x00000000,0x00000000, DDPF_LUMINANCE     ) \
	DEF(A8L8          ,  51,  16, 0x000000ff,0x00000000,0x00000000,0x0000ff00, DDPF_ALPHALUMINANCE) \
	DEF(A4L4          ,  52,   8, 0x0000000f,0x00000000,0x00000000,0x000000f0, DDPF_ALPHALUMINANCE) \
	\
	DEF(L16           ,  81,  16, 0x0000ffff,0x00000000,0x00000000,0x00000000, DDPF_LUMINANCE     ) \
	DEF(G16R16        ,  34,  32, 0x0000ffff,0xffff0000,0x00000000,0x00000000, DDPF_RGB           ) \
	DEF(A16B16G16R16  ,  36,  64, 0x00000000,0x00000000,0x00000000,0x00000000, DDPF_FOURCC        ) \
	\
	DEF(R16F          , 111,  16, 0x00000000,0x00000000,0x00000000,0x00000000, DDPF_FOURCC        ) \
	DEF(G16R16F       , 112,  32, 0x00000000,0x00000000,0x00000000,0x00000000, DDPF_FOURCC        ) \
	DEF(A16B16G16R16F , 113,  64, 0x00000000,0x00000000,0x00000000,0x00000000, DDPF_FOURCC        ) \
	\
	DEF(R32F          , 114,  32, 0x00000000,0x00000000,0x00000000,0x00000000, DDPF_FOURCC        ) \
	DEF(G32R32F       , 115,  64, 0x00000000,0x00000000,0x00000000,0x00000000, DDPF_FOURCC        ) \
	DEF(A32B32G32R32F , 116, 128, 0x00000000,0x00000000,0x00000000,0x00000000, DDPF_FOURCC        ) \
	// end.

#define FOREACH_DEF_D3DFMT_unsupported(DEF) \
	DEF(DXT1          , MAKEFOURCC('D','X','T','1'), 4, 0,0,0,0, DDPF_FOURCC) \
	DEF(DXT2          , MAKEFOURCC('D','X','T','2'), 8, 0,0,0,0, DDPF_FOURCC) \
	DEF(DXT3          , MAKEFOURCC('D','X','T','3'), 8, 0,0,0,0, DDPF_FOURCC) \
	DEF(DXT4          , MAKEFOURCC('D','X','T','4'), 8, 0,0,0,0, DDPF_FOURCC) \
	DEF(DXT5          , MAKEFOURCC('D','X','T','5'), 8, 0,0,0,0, DDPF_FOURCC) \
	DEF(DXTN          , MAKEFOURCC('A','T','I','2'), 8, 0,0,0,0, DDPF_FOURCC) \
	\
	DEF(UYVY          , MAKEFOURCC('U','Y','V','Y'), 0, 0,0,0,0, DDPF_FOURCC) \
	DEF(YUY2          , MAKEFOURCC('Y','U','Y','2'), 0, 0,0,0,0, DDPF_FOURCC) \
	DEF(R8G8_B8G8     , MAKEFOURCC('R','G','B','G'), 0, 0,0,0,0, DDPF_FOURCC) \
	DEF(G8R8_G8B8     , MAKEFOURCC('G','R','G','B'), 0, 0,0,0,0, DDPF_FOURCC) \
	DEF(MULTI2_ARGB8  , MAKEFOURCC('M','E','T','1'), 0, 0,0,0,0, DDPF_FOURCC) \
	\
	DEF(V8U8          ,  60,  16, 0x000000ff,0x0000ff00,0x00000000,0x00000000, DDPF_BUMPDUDV     ) \
	DEF(CxV8U8        , 117,  16, 0x000000ff,0x0000ff00,0x00000000,0x00000000, DDPF_FOURCC       ) __COMMENT("this is the only FOURCC format with a mask") \
	DEF(L6V5U5        ,  61,  16, 0x0000001f,0x000003e0,0x0000fc00,0x00000000, DDPF_BUMPLUMINANCE) \
	DEF(X8L8V8U8      ,  62,  32, 0x000000ff,0x0000ff00,0x00ff0000,0x00000000, DDPF_BUMPLUMINANCE) \
	DEF(Q8W8V8U8      ,  63,  32, 0x000000ff,0x0000ff00,0x00ff0000,0xff000000, DDPF_BUMPDUDV     ) \
	DEF(A2W10V10U10   ,  67,  32, 0x3ff00000,0x000ffc00,0x000003ff,0xc0000000, DDPF_BUMPDUDVALPHA) \
	DEF(V16U16        ,  64,  32, 0x0000ffff,0xffff0000,0x00000000,0x00000000, DDPF_BUMPDUDV     ) \
	DEF(Q16W16V16U16  , 110,  64, 0x00000000,0x00000000,0x00000000,0x00000000, DDPF_FOURCC       ) \
	\
	DEF(P8            ,  41,   8, 0x00000000,0x00000000,0x00000000,0x00000000, 0) \
	DEF(A8P8          ,  40,  16, 0x00000000,0x00000000,0x00000000,0x00000000, 0) \
	DEF(A1            , 118,   1, 0x00000000,0x00000000,0x00000000,0x00000000, 0) \
	\
	DEF(D16_LOCKABLE  ,  70,  16, 0x00000000,0x00000000,0x00000000,0x00000000, 0) \
	DEF(D32           ,  71,  32, 0x00000000,0x00000000,0x00000000,0x00000000, 0) \
	DEF(D15S1         ,  73,  16, 0x00000000,0x00000000,0x00000000,0x00000000, 0) \
	DEF(D24S8         ,  75,  32, 0x00000000,0x00000000,0x00000000,0x00000000, 0) \
	DEF(D24X8         ,  77,  32, 0x00000000,0x00000000,0x00000000,0x00000000, 0) \
	DEF(D24X4S4       ,  79,  32, 0x00000000,0x00000000,0x00000000,0x00000000, 0) \
	DEF(D16           ,  80,  16, 0x00000000,0x00000000,0x00000000,0x00000000, 0) \
	DEF(D32F_LOCKABLE ,  82,  32, 0x00000000,0x00000000,0x00000000,0x00000000, 0) \
	DEF(D24FS8        ,  83,  32, 0x00000000,0x00000000,0x00000000,0x00000000, 0) \
	DEF(D32_LOCKABLE  ,  84,  32, 0x00000000,0x00000000,0x00000000,0x00000000, 0) \
	DEF(S8_LOCKABLE   ,  85,   8, 0x00000000,0x00000000,0x00000000,0x00000000, 0) \
	\
	DEF(VERTEXDATA    , 100,   0, 0x00000000,0x00000000,0x00000000,0x00000000, 0) \
	DEF(INDEX16       , 101,  16, 0x00000000,0x00000000,0x00000000,0x00000000, 0) \
	DEF(INDEX32       , 102,  32, 0x00000000,0x00000000,0x00000000,0x00000000, 0) \
	DEF(BINARYBUFFER  , 199,   0, 0x00000000,0x00000000,0x00000000,0x00000000, 0) \
	// end.

// ================================================================================================

class DDSD_ // useful for examining in the debugger
{
public:
	uint32_t _DDSD_CAPS             :1; // [00]
	uint32_t _DDSD_HEIGHT           :1; // [01]
	uint32_t _DDSD_WIDTH            :1; // [02]
	uint32_t _DDSD_PITCH            :1; // [03]
	uint32_t _DDSD_UNKNOWN_BIT_04   :1; // [04]
	uint32_t _DDSD_BACKBUFFERCOUNT  :1; // [05]
	uint32_t _DDSD_ZBUFFERBITDEPTH  :1; // [06]
	uint32_t _DDSD_ALPHABITDEPTH    :1; // [07]
	uint32_t _DDSD_UNKNOWN_BIT_08   :1; // [08]
	uint32_t _DDSD_UNKNOWN_BIT_09   :1; // [09]
	uint32_t _DDSD_UNKNOWN_BIT_10   :1; // [10]
	uint32_t _DDSD_LPSURFACE        :1; // [11]
	uint32_t _DDSD_PIXELFORMAT      :1; // [12]
	uint32_t _DDSD_CKDESTOVERLAY    :1; // [13]
	uint32_t _DDSD_CKDESTBLT        :1; // [14]
	uint32_t _DDSD_CKSRCOVERLAY     :1; // [15]
	uint32_t _DDSD_CKSRCBLT         :1; // [16]
	uint32_t _DDSD_MIPMAPCOUNT      :1; // [17]
	uint32_t _DDSD_REFRESHRATE      :1; // [18]
	uint32_t _DDSD_LINEARSIZE       :1; // [19]
	uint32_t _DDSD_TEXTURESTAGE     :1; // [20]
	uint32_t _DDSD_FVF              :1; // [21]
	uint32_t _DDSD_SRCVBHANDLE      :1; // [22]
	uint32_t _DDSD_DEPTH            :1; // [23]
	uint32_t _DDSD_UNKNOWN_BIT_24_31:8; // [24-31]

	__forceinline void foo()
	{
		STATIC_ASSERT(SIZEOF(*this) == SIZEOF(uint32_t), DDSD_wrong_sizeof);
	}

	std::string GetString(uint32_t mask = -1) const;
};

class DDPF_ // useful for examining in the debugger
{
public:
	uint32_t _DDPF_ALPHAPIXELS      :1; // [00]
	uint32_t _DDPF_ALPHA            :1; // [01]
	uint32_t _DDPF_FOURCC           :1; // [02]
	uint32_t _DDPF_PALETTEINDEXED4  :1; // [03]
	uint32_t _DDPF_PALETTEINDEXEDTO8:1; // [04] 
	uint32_t _DDPF_PALETTEINDEXED8  :1; // [05]
	uint32_t _DDPF_RGB              :1; // [06]
	uint32_t _DDPF_COMPRESSED       :1; // [07]
	uint32_t _DDPF_RGBTOYUV         :1; // [08] 
	uint32_t _DDPF_YUV              :1; // [09] 
	uint32_t _DDPF_ZBUFFER          :1; // [10] 
	uint32_t _DDPF_PALETTEINDEXED1  :1; // [11]
	uint32_t _DDPF_PALETTEINDEXED2  :1; // [12]
	uint32_t _DDPF_ZPIXELS          :1; // [13]
	uint32_t _DDPF_STENCILBUFFER    :1; // [14]
	uint32_t _DDPF_ALPHAPREMULT     :1; // [15]
	uint32_t _DDPF_UNKNOWN_BIT_16   :1; // [16]
	uint32_t _DDPF_LUMINANCE        :1; // [17]
	uint32_t _DDPF_BUMPLUMINANCE    :1; // [18]
	uint32_t _DDPF_BUMPDUDV         :1; // [19]
	uint32_t _DDPF_UNKNOWN_BIT_20_31:12;// [20-31]

	__forceinline void foo()
	{
		STATIC_ASSERT(SIZEOF(*this) == SIZEOF(uint32_t), DDPF_wrong_sizeof);
	}

	std::string GetString(uint32_t mask = -1) const;
};

class DDSCAPS1_ // useful for examining in the debugger
{
public:
	uint32_t _DDSCAPS_RESERVED1        :1; // [00]
	uint32_t _DDSCAPS_ALPHA            :1; // [01]
	uint32_t _DDSCAPS_BACKBUFFER       :1; // [02]
	uint32_t _DDSCAPS_COMPLEX          :1; // [03]
	uint32_t _DDSCAPS_FLIP             :1; // [04]
	uint32_t _DDSCAPS_FRONTBUFFER      :1; // [05]
	uint32_t _DDSCAPS_OFFSCREENPLAIN   :1; // [06]
	uint32_t _DDSCAPS_OVERLAY          :1; // [07]
	uint32_t _DDSCAPS_PALETTE          :1; // [08]
	uint32_t _DDSCAPS_PRIMARYSURFACE   :1; // [09]
	uint32_t _DDSCAPS_RESERVED3        :1; // [10]
	uint32_t _DDSCAPS_SYSTEMMEMORY     :1; // [11]
	uint32_t _DDSCAPS_TEXTURE          :1; // [12]
	uint32_t _DDSCAPS_3DDEVICE         :1; // [13]
	uint32_t _DDSCAPS_VIDEOMEMORY      :1; // [14]
	uint32_t _DDSCAPS_VISIBLE          :1; // [15]
	uint32_t _DDSCAPS_WRITEONLY        :1; // [16]
	uint32_t _DDSCAPS_ZBUFFER          :1; // [17]
	uint32_t _DDSCAPS_OWNDC            :1; // [18]
	uint32_t _DDSCAPS_LIVEVIDEO        :1; // [19]
	uint32_t _DDSCAPS_HWCODEC          :1; // [20]
	uint32_t _DDSCAPS_MODEX            :1; // [21]
	uint32_t _DDSCAPS_MIPMAP           :1; // [22]
	uint32_t _DDSCAPS_RESERVED2        :1; // [23]
	uint32_t _DDSCAPS_UNKNOWN_BIT_24   :1; // [24]
	uint32_t _DDSCAPS_UNKNOWN_BIT_25   :1; // [25]
	uint32_t _DDSCAPS_ALLOCONLOAD      :1; // [26]
	uint32_t _DDSCAPS_VIDEOPORT        :1; // [27]
	uint32_t _DDSCAPS_LOCALVIDMEM      :1; // [28]
	uint32_t _DDSCAPS_NONLOCALVIDMEM   :1; // [29]
	uint32_t _DDSCAPS_STANDARDVGAMODE  :1; // [30]
	uint32_t _DDSCAPS_OPTIMIZED        :1; // [31]

	__forceinline void foo()
	{
		STATIC_ASSERT(SIZEOF(*this) == SIZEOF(uint32_t), DDSCAPS1_wrong_sizeof);
	}

	std::string GetString(uint32_t mask = -1) const;
};

class DDSCAPS2_ // useful for examining in the debugger
{
public:
	uint32_t _DDSCAPS2_UNKNOWN_BIT_00       :1; // [00]
	uint32_t _DDSCAPS2_RESERVED4            :1; // [01]
	uint32_t _DDSCAPS2_HINTDYNAMIC          :1; // [02]
	uint32_t _DDSCAPS2_HINTSTATIC           :1; // [03]
	uint32_t _DDSCAPS2_TEXTUREMANAGE        :1; // [04]
	uint32_t _DDSCAPS2_RESERVED1            :1; // [05]
	uint32_t _DDSCAPS2_RESERVED2            :1; // [06]
	uint32_t _DDSCAPS2_OPAQUE               :1; // [07]
	uint32_t _DDSCAPS2_HINTANTIALIASING     :1; // [08]
	uint32_t _DDSCAPS2_CUBEMAP              :1; // [09]
	uint32_t _DDSCAPS2_CUBEMAP_POSITIVEX    :1; // [10]
	uint32_t _DDSCAPS2_CUBEMAP_NEGATIVEX    :1; // [11]
	uint32_t _DDSCAPS2_CUBEMAP_POSITIVEY    :1; // [12]
	uint32_t _DDSCAPS2_CUBEMAP_NEGATIVEY    :1; // [13]
	uint32_t _DDSCAPS2_CUBEMAP_POSITIVEZ    :1; // [14]
	uint32_t _DDSCAPS2_CUBEMAP_NEGATIVEZ    :1; // [15]
	uint32_t _DDSCAPS2_MIPMAPSUBLEVEL       :1; // [16]
	uint32_t _DDSCAPS2_D3DTEXTUREMANAGE     :1; // [17]
	uint32_t _DDSCAPS2_DONOTPERSIST         :1; // [18]
	uint32_t _DDSCAPS2_STEREOSURFACELEFT    :1; // [19]
	uint32_t _DDSCAPS2_UNKNOWN_BIT_20       :1; // [20]
	uint32_t _DDSCAPS2_VOLUME               :1; // [21]
	uint32_t _DDSCAPS2_NOTUSERLOCKABLE      :1; // [22]
	uint32_t _DDSCAPS2_POINTS               :1; // [23]
	uint32_t _DDSCAPS2_RTPATCHES            :1; // [24]
	uint32_t _DDSCAPS2_NPATCHES             :1; // [25]
	uint32_t _DDSCAPS2_RESERVED3            :1; // [26]
	uint32_t _DDSCAPS2_UNKNOWN_BIT_27       :1; // [27]
	uint32_t _DDSCAPS2_DISCARDBACKBUFFER    :1; // [28]
	uint32_t _DDSCAPS2_ENABLEALPHACHANNEL   :1; // [29]
	uint32_t _DDSCAPS2_EXTENDEDFORMATPRIMARY:1; // [30]
	uint32_t _DDSCAPS2_ADDITIONALPRIMARY    :1; // [31]

	__forceinline void foo()
	{
		STATIC_ASSERT(SIZEOF(*this) == SIZEOF(uint32_t), DDSCAPS2_wrong_sizeof);
	}

	std::string GetString(uint32_t mask = -1) const;
};

// ================================================================================================

#ifndef DDRAW_INCLUDED // skip the following if ddraw.h has been included ..

enum D3DFORMAT
{
	D3DFMT_UNKNOWN = 0,
	#define DEF_D3DFMT(name,id,n,r,g,b,a,flags) D3DFMT_##name = id,
	FOREACH(DEF_D3DFMT)
	#undef  DEF_D3DFMT
	#define DEF_D3DFMT_unsupported(name,id,n,r,g,b,a,flags) D3DFMT_##name = id,
	FOREACH(DEF_D3DFMT_unsupported)
	#undef  DEF_D3DFMT_unsupported
	D3DFMT_FORCE_DWORD = 0x7fffffffL
};

#define DDSD_CAPS                      0x00000001L
#define DDSD_HEIGHT                    0x00000002L
#define DDSD_WIDTH                     0x00000004L
#define DDSD_PITCH                     0x00000008L
#define DDSD_UNKNOWN_BIT_04            0x00000010L // not defined in ddraw.h
#define DDSD_BACKBUFFERCOUNT           0x00000020L
#define DDSD_ZBUFFERBITDEPTH           0x00000040L
#define DDSD_ALPHABITDEPTH             0x00000080L
#define DDSD_UNKNOWN_BIT_08            0x00000100L // not defined in ddraw.h
#define DDSD_UNKNOWN_BIT_09            0x00000200L // not defined in ddraw.h
#define DDSD_UNKNOWN_BIT_10            0x00000400L // not defined in ddraw.h
#define DDSD_LPSURFACE                 0x00000800L
#define DDSD_PIXELFORMAT               0x00001000L
#define DDSD_CKDESTOVERLAY             0x00002000L
#define DDSD_CKDESTBLT                 0x00004000L
#define DDSD_CKSRCOVERLAY              0x00008000L
#define DDSD_CKSRCBLT                  0x00010000L
#define DDSD_MIPMAPCOUNT               0x00020000L
#define DDSD_REFRESHRATE               0x00040000L
#define DDSD_LINEARSIZE                0x00080000L
#define DDSD_TEXTURESTAGE              0x00100000L
#define DDSD_FVF                       0x00200000L
#define DDSD_SRCVBHANDLE               0x00400000L
#define DDSD_DEPTH                     0x00800000L
#define DDSD_UNKNOWN_BIT_24_31         0xff000000L // not defined in draw.h
#define DDSD_ALL                       0x00fff8efL // <-- was be 0x00fff9eeL .. but that was incorrect

#define DDPF_ALPHAPIXELS               0x00000001L
#define DDPF_ALPHA                     0x00000002L
#define DDPF_FOURCC                    0x00000004L
#define DDPF_PALETTEINDEXED4           0x00000008L
#define DDPF_PALETTEINDEXEDTO8         0x00000010L
#define DDPF_PALETTEINDEXED8           0x00000020L
#define DDPF_RGB                       0x00000040L
#define DDPF_COMPRESSED                0x00000080L
#define DDPF_RGBTOYUV                  0x00000100L
#define DDPF_YUV                       0x00000200L
#define DDPF_ZBUFFER                   0x00000400L
#define DDPF_PALETTEINDEXED1           0x00000800L
#define DDPF_PALETTEINDEXED2           0x00001000L
#define DDPF_ZPIXELS                   0x00002000L
#define DDPF_STENCILBUFFER             0x00004000L
#define DDPF_ALPHAPREMULT              0x00008000L
#define DDPF_UNKNOWN_BIT_16            0x00010000L // not defined in ddraw.h
#define DDPF_LUMINANCE                 0x00020000L
#define DDPF_BUMPLUMINANCE             0x00040000L
#define DDPF_BUMPDUDV                  0x00080000L
#define DDPF_UNKNOWN_BIT_20_31         0xfff00000L // not defined in draw.h
#define DDPF_ALL                       0x000effffL

#define DDSCAPS_RESERVED1              0x00000001L
#define DDSCAPS_ALPHA                  0x00000002L
#define DDSCAPS_BACKBUFFER             0x00000004L
#define DDSCAPS_COMPLEX                0x00000008L
#define DDSCAPS_FLIP                   0x00000010L
#define DDSCAPS_FRONTBUFFER            0x00000020L
#define DDSCAPS_OFFSCREENPLAIN         0x00000040L
#define DDSCAPS_OVERLAY                0x00000080L
#define DDSCAPS_PALETTE                0x00000100L
#define DDSCAPS_PRIMARYSURFACE         0x00000200L
#define DDSCAPS_RESERVED3              0x00000400L
#define DDSCAPS_SYSTEMMEMORY           0x00000800L
#define DDSCAPS_TEXTURE                0x00001000L
#define DDSCAPS_3DDEVICE               0x00002000L
#define DDSCAPS_VIDEOMEMORY            0x00004000L
#define DDSCAPS_VISIBLE                0x00008000L
#define DDSCAPS_WRITEONLY              0x00010000L
#define DDSCAPS_ZBUFFER                0x00020000L
#define DDSCAPS_OWNDC                  0x00040000L
#define DDSCAPS_LIVEVIDEO              0x00080000L
#define DDSCAPS_HWCODEC                0x00100000L
#define DDSCAPS_MODEX                  0x00200000L
#define DDSCAPS_MIPMAP                 0x00400000L
#define DDSCAPS_RESERVED2              0x00800000L
#define DDSCAPS_UNKNOWN_BIT_24         0x01000000L // not defined in ddraw.h
#define DDSCAPS_UNKNOWN_BIT_25         0x02000000L // not defined in ddraw.h
#define DDSCAPS_ALLOCONLOAD            0x04000000L
#define DDSCAPS_VIDEOPORT              0x08000000L
#define DDSCAPS_LOCALVIDMEM            0x10000000L
#define DDSCAPS_NONLOCALVIDMEM         0x20000000L
#define DDSCAPS_STANDARDVGAMODE        0x40000000L
#define DDSCAPS_OPTIMIZED              0x80000000L
#define DDSCAPS_ALL                    0xfcffffffL
#define DDSCAPS_PRIMARYSURFACELEFT     0x00000000L // zero

#define DDSCAPS2_UNKNOWN_BIT_00        0x00000001L // not defined in ddraw.h
#define DDSCAPS2_RESERVED4             0x00000002L
#define DDSCAPS2_HINTDYNAMIC           0x00000004L
#define DDSCAPS2_HINTSTATIC            0x00000008L
#define DDSCAPS2_TEXTUREMANAGE         0x00000010L
#define DDSCAPS2_RESERVED1             0x00000020L
#define DDSCAPS2_RESERVED2             0x00000040L
#define DDSCAPS2_OPAQUE                0x00000080L
#define DDSCAPS2_HINTANTIALIASING      0x00000100L
#define DDSCAPS2_CUBEMAP               0x00000200L
#define DDSCAPS2_CUBEMAP_POSITIVEX     0x00000400L
#define DDSCAPS2_CUBEMAP_NEGATIVEX     0x00000800L
#define DDSCAPS2_CUBEMAP_POSITIVEY     0x00001000L
#define DDSCAPS2_CUBEMAP_NEGATIVEY     0x00002000L
#define DDSCAPS2_CUBEMAP_POSITIVEZ     0x00004000L
#define DDSCAPS2_CUBEMAP_NEGATIVEZ     0x00008000L
#define DDSCAPS2_MIPMAPSUBLEVEL        0x00010000L
#define DDSCAPS2_D3DTEXTUREMANAGE      0x00020000L
#define DDSCAPS2_DONOTPERSIST          0x00040000L
#define DDSCAPS2_STEREOSURFACELEFT     0x00080000L
#define DDSCAPS2_UNKNOWN_BIT_20        0x00100000L // not defined in ddraw.h
#define DDSCAPS2_VOLUME                0x00200000L
#define DDSCAPS2_NOTUSERLOCKABLE       0x00400000L
#define DDSCAPS2_POINTS                0x00800000L
#define DDSCAPS2_RTPATCHES             0x01000000L
#define DDSCAPS2_NPATCHES              0x02000000L
#define DDSCAPS2_RESERVED3             0x04000000L
#define DDSCAPS2_UNKNOWN_BIT_27        0x08000000L // not defined in ddraw.h
#define DDSCAPS2_DISCARDBACKBUFFER     0x10000000L
#define DDSCAPS2_ENABLEALPHACHANNEL    0x20000000L
#define DDSCAPS2_EXTENDEDFORMATPRIMARY 0x40000000L
#define DDSCAPS2_ADDITIONALPRIMARY     0x80000000L
#define DDSCAPS2_ALL                   0xf7effffeL
#define DDSCAPS2_HARDWAREDEINTERLACE   0x00000000L // zero
#define DDSCAPS2_CUBEMAP_ALLFACES      (DDSCAPS2_CUBEMAP_POSITIVEX | DDSCAPS2_CUBEMAP_NEGATIVEX | DDSCAPS2_CUBEMAP_POSITIVEY | DDSCAPS2_CUBEMAP_NEGATIVEY | DDSCAPS2_CUBEMAP_POSITIVEZ | DDSCAPS2_CUBEMAP_NEGATIVEZ)

class DDPIXELFORMAT // equivalent to DDS_PIXELFORMAT
{
public:
	uint32_t dwSize;
	union { uint32_t dwFlags; DDPF_ dwFlags_ ; };
	union { uint32_t dwFourCC; char dwFourCC_[4]; D3DFORMAT dwFormat_; };
	uint32_t dwRGBBitCount;
	uint32_t dwRBitMask;
	uint32_t dwGBitMask;
	uint32_t dwBBitMask;
	uint32_t dwABitMask;

	__forceinline void foo()
	{
		STATIC_ASSERT(SIZEOF(*this) == 32, DDPIXELFORMAT_wrong_sizeof);
	}

	D3DFORMAT GetFormat(bool bMatchFlags, bool bAllowUnsupportedFormats) const;
};

class DDSCAPS2
{
public:
	union { uint32_t dwCaps1; DDSCAPS1_ dwCaps1_; };
	union { uint32_t dwCaps2; DDSCAPS2_ dwCaps2_; };
	uint32_t dwReserved[2];
};

class DDSURFACEDESC2 // equivalent to DDS_HEADER (includes dwMagic)
{
public:
	uint32_t dwMagic; // must be 'DDS '
	uint32_t dwSize;  // must be 124
	union { uint32_t dwFlags; DDSD_ dwFlags_; };
	uint32_t dwHeight;
	uint32_t dwWidth;
	uint32_t dwPitchOrLinearSize;
	uint32_t dwDepth;
	uint32_t dwMipMapCount;
	uint32_t dwReserved1[11];
	DDPIXELFORMAT ddpf;
    DDSCAPS2 ddsCaps;
	uint32_t dwReserved2[1];

	__forceinline void foo()
	{
		STATIC_ASSERT(SIZEOF(*this) - SIZEOF(uint32_t) == 124, DDSURFACEDESC2_wrong_sizeof);
	}
};

#endif // DDRAW_INCLUDED
#endif // IMAGE_DDS_H
