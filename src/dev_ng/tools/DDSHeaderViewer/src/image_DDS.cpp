// =============
// image_DDS.cpp
// =============

#include "common.h"
#include "image_DDS.h"

D3DFORMAT DDPIXELFORMAT::GetFormat(bool bMatchFlags, bool bAllowUnsupportedFormats) const
{
	if (dwFlags & DDPF_FOURCC)
	{
		return (D3DFORMAT)dwFourCC;
	}
	else // try to deduce the format from the RGB masks
	{
		const uint32_t n_ = dwRGBBitCount;
		const uint32_t r_ = dwRBitMask;
		const uint32_t g_ = dwGBitMask;
		const uint32_t b_ = dwBBitMask;
		const uint32_t a_ = dwABitMask;
		const uint32_t f_ = dwFlags;

		if ((r_|g_|b_|a_) != 0 && n_ != 0)
		{
			#define DEF_D3DFMT(name,id,n,r,g,b,a,f) \
				if (n==n_ && r==r_ && g==g_ && b==b_ && a==a_ && (f==f_ || !bMatchFlags)) \
				{ \
					return D3DFMT_##name; \
				} \
				// end.
			FOREACH(DEF_D3DFMT)
			#undef  DEF_D3DFMT

			if (bAllowUnsupportedFormats)
			{
				#define DEF_D3DFMT_unsupported(name,id,n,r,g,b,a,f) \
					if (n==n_ && r==r_ && g==g_ && b==b_ && a==a_ && (f==f_ || !bMatchFlags)) \
					{ \
						return D3DFMT_##name; \
					} \
					// end.
				FOREACH(DEF_D3DFMT_unsupported)
				#undef  DEF_D3DFMT_unsupported
			}
		}
	}

	return D3DFMT_UNKNOWN;
}
