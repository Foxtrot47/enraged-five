// ====================
// image_DDS_helper.cpp
// ====================

#include "common.h"
#include "image_DDS.h"
#include "image_DDS_helper.h"

#define DEF_GetStringFlag(name) \
	if (f_._##name != 0) \
	{ \
		if (str.length() > 0) \
		{ \
			str += ","; \
		} \
		const uint32_t bits = f_._##name; \
		if (0) { str += CString(STRING(##name) "=%d", bits); } \
		else   { str +=         STRING(##name); } \
	} \
	// end.

std::string DDSD_::GetString(uint32_t mask) const
{
	union { uint32_t f; DDSD_ f_; }; f = mask & *(const uint32_t*)this;
	std::string str;

	DEF_GetStringFlag(DDSD_CAPS             );
	DEF_GetStringFlag(DDSD_HEIGHT           );
	DEF_GetStringFlag(DDSD_WIDTH            );
	DEF_GetStringFlag(DDSD_PITCH            );
	DEF_GetStringFlag(DDSD_UNKNOWN_BIT_04   );
	DEF_GetStringFlag(DDSD_BACKBUFFERCOUNT  );
	DEF_GetStringFlag(DDSD_ZBUFFERBITDEPTH  );
	DEF_GetStringFlag(DDSD_ALPHABITDEPTH    );
	DEF_GetStringFlag(DDSD_UNKNOWN_BIT_08   );
	DEF_GetStringFlag(DDSD_UNKNOWN_BIT_09   );
	DEF_GetStringFlag(DDSD_UNKNOWN_BIT_10   );
	DEF_GetStringFlag(DDSD_LPSURFACE        );
	DEF_GetStringFlag(DDSD_PIXELFORMAT      );
	DEF_GetStringFlag(DDSD_CKDESTOVERLAY    );
	DEF_GetStringFlag(DDSD_CKDESTBLT        );
	DEF_GetStringFlag(DDSD_CKSRCOVERLAY     );
	DEF_GetStringFlag(DDSD_CKSRCBLT         );
	DEF_GetStringFlag(DDSD_MIPMAPCOUNT      );
	DEF_GetStringFlag(DDSD_REFRESHRATE      );
	DEF_GetStringFlag(DDSD_LINEARSIZE       );
	DEF_GetStringFlag(DDSD_TEXTURESTAGE     );
	DEF_GetStringFlag(DDSD_FVF              );
	DEF_GetStringFlag(DDSD_SRCVBHANDLE      );
	DEF_GetStringFlag(DDSD_DEPTH            );
	DEF_GetStringFlag(DDSD_UNKNOWN_BIT_24_31);

	return str;
}

std::string DDPF_::GetString(uint32_t mask) const
{
	union { uint32_t f; DDPF_ f_; }; f = mask & *(const uint32_t*)this;
	std::string str;

	DEF_GetStringFlag(DDPF_ALPHAPIXELS      );
	DEF_GetStringFlag(DDPF_ALPHA            );
	DEF_GetStringFlag(DDPF_FOURCC           );
	DEF_GetStringFlag(DDPF_PALETTEINDEXED4  );
	DEF_GetStringFlag(DDPF_PALETTEINDEXEDTO8);
	DEF_GetStringFlag(DDPF_PALETTEINDEXED8  );
	DEF_GetStringFlag(DDPF_RGB              );
	DEF_GetStringFlag(DDPF_COMPRESSED       );
	DEF_GetStringFlag(DDPF_RGBTOYUV         );
	DEF_GetStringFlag(DDPF_YUV              );
	DEF_GetStringFlag(DDPF_ZBUFFER          );
	DEF_GetStringFlag(DDPF_PALETTEINDEXED1  );
	DEF_GetStringFlag(DDPF_PALETTEINDEXED2  );
	DEF_GetStringFlag(DDPF_ZPIXELS          );
	DEF_GetStringFlag(DDPF_STENCILBUFFER    );
	DEF_GetStringFlag(DDPF_ALPHAPREMULT     );
	DEF_GetStringFlag(DDPF_UNKNOWN_BIT_16   );
	DEF_GetStringFlag(DDPF_LUMINANCE        );
	DEF_GetStringFlag(DDPF_BUMPLUMINANCE    );
	DEF_GetStringFlag(DDPF_BUMPDUDV         );
	DEF_GetStringFlag(DDPF_UNKNOWN_BIT_20_31);

	return str;
}

std::string DDSCAPS1_::GetString(uint32_t mask) const
{
	union { uint32_t f; DDSCAPS1_ f_; }; f = mask & *(const uint32_t*)this;
	std::string str;

	DEF_GetStringFlag(DDSCAPS_RESERVED1        );
	DEF_GetStringFlag(DDSCAPS_ALPHA            );
	DEF_GetStringFlag(DDSCAPS_BACKBUFFER       );
	DEF_GetStringFlag(DDSCAPS_COMPLEX          );
	DEF_GetStringFlag(DDSCAPS_FLIP             );
	DEF_GetStringFlag(DDSCAPS_FRONTBUFFER      );
	DEF_GetStringFlag(DDSCAPS_OFFSCREENPLAIN   );
	DEF_GetStringFlag(DDSCAPS_OVERLAY          );
	DEF_GetStringFlag(DDSCAPS_PALETTE          );
	DEF_GetStringFlag(DDSCAPS_PRIMARYSURFACE   );
	DEF_GetStringFlag(DDSCAPS_RESERVED3        );
	DEF_GetStringFlag(DDSCAPS_SYSTEMMEMORY     );
	DEF_GetStringFlag(DDSCAPS_TEXTURE          );
	DEF_GetStringFlag(DDSCAPS_3DDEVICE         );
	DEF_GetStringFlag(DDSCAPS_VIDEOMEMORY      );
	DEF_GetStringFlag(DDSCAPS_VISIBLE          );
	DEF_GetStringFlag(DDSCAPS_WRITEONLY        );
	DEF_GetStringFlag(DDSCAPS_ZBUFFER          );
	DEF_GetStringFlag(DDSCAPS_OWNDC            );
	DEF_GetStringFlag(DDSCAPS_LIVEVIDEO        );
	DEF_GetStringFlag(DDSCAPS_HWCODEC          );
	DEF_GetStringFlag(DDSCAPS_MODEX            );
	DEF_GetStringFlag(DDSCAPS_MIPMAP           );
	DEF_GetStringFlag(DDSCAPS_RESERVED2        );
	DEF_GetStringFlag(DDSCAPS_UNKNOWN_BIT_24   );
	DEF_GetStringFlag(DDSCAPS_UNKNOWN_BIT_25   );
	DEF_GetStringFlag(DDSCAPS_ALLOCONLOAD      );
	DEF_GetStringFlag(DDSCAPS_VIDEOPORT        );
	DEF_GetStringFlag(DDSCAPS_LOCALVIDMEM      );
	DEF_GetStringFlag(DDSCAPS_NONLOCALVIDMEM   );
	DEF_GetStringFlag(DDSCAPS_STANDARDVGAMODE  );
	DEF_GetStringFlag(DDSCAPS_OPTIMIZED        );

	return str;
}

std::string DDSCAPS2_::GetString(uint32_t mask) const
{
	union { uint32_t f; DDSCAPS2_ f_; }; f = mask & *(const uint32_t*)this;
	std::string str;

	DEF_GetStringFlag(DDSCAPS2_UNKNOWN_BIT_00       );
	DEF_GetStringFlag(DDSCAPS2_RESERVED4            );
	DEF_GetStringFlag(DDSCAPS2_HINTDYNAMIC          );
	DEF_GetStringFlag(DDSCAPS2_HINTSTATIC           );
	DEF_GetStringFlag(DDSCAPS2_TEXTUREMANAGE        );
	DEF_GetStringFlag(DDSCAPS2_RESERVED1            );
	DEF_GetStringFlag(DDSCAPS2_RESERVED2            );
	DEF_GetStringFlag(DDSCAPS2_OPAQUE               );
	DEF_GetStringFlag(DDSCAPS2_HINTANTIALIASING     );
	DEF_GetStringFlag(DDSCAPS2_CUBEMAP              );
	DEF_GetStringFlag(DDSCAPS2_CUBEMAP_POSITIVEX    );
	DEF_GetStringFlag(DDSCAPS2_CUBEMAP_NEGATIVEX    );
	DEF_GetStringFlag(DDSCAPS2_CUBEMAP_POSITIVEY    );
	DEF_GetStringFlag(DDSCAPS2_CUBEMAP_NEGATIVEY    );
	DEF_GetStringFlag(DDSCAPS2_CUBEMAP_POSITIVEZ    );
	DEF_GetStringFlag(DDSCAPS2_CUBEMAP_NEGATIVEZ    );
	DEF_GetStringFlag(DDSCAPS2_MIPMAPSUBLEVEL       );
	DEF_GetStringFlag(DDSCAPS2_D3DTEXTUREMANAGE     );
	DEF_GetStringFlag(DDSCAPS2_DONOTPERSIST         );
	DEF_GetStringFlag(DDSCAPS2_STEREOSURFACELEFT    );
	DEF_GetStringFlag(DDSCAPS2_UNKNOWN_BIT_20       );
	DEF_GetStringFlag(DDSCAPS2_VOLUME               );
	DEF_GetStringFlag(DDSCAPS2_NOTUSERLOCKABLE      );
	DEF_GetStringFlag(DDSCAPS2_POINTS               );
	DEF_GetStringFlag(DDSCAPS2_RTPATCHES            );
	DEF_GetStringFlag(DDSCAPS2_NPATCHES             );
	DEF_GetStringFlag(DDSCAPS2_RESERVED3            );
	DEF_GetStringFlag(DDSCAPS2_UNKNOWN_BIT_27       );
	DEF_GetStringFlag(DDSCAPS2_DISCARDBACKBUFFER    );
	DEF_GetStringFlag(DDSCAPS2_ENABLEALPHACHANNEL   );
	DEF_GetStringFlag(DDSCAPS2_EXTENDEDFORMATPRIMARY);
	DEF_GetStringFlag(DDSCAPS2_ADDITIONALPRIMARY    );

	return str;
}

#undef DEF_GetStringFlag

// ================================================================================================

namespace DDS_helper {

bool IsPrintableFourCC(uint32_t cc)
{
	for (int i = 0; i < 4; i++)
	{
		const uchar8_t ch = (uchar8_t)(cc >> (8*i));
		
		if (!(ch == ' ') &&
			!(ch >= '0' && ch <= '9') &&
			!(ch >= 'A' && ch <= 'Z'))
		{
			return false;
		}
	}

	return true;
}

D3DFORMAT GetFormatFromMask(int n_, uint32_t r_, uint32_t g_, uint32_t b_, uint32_t a_)
{
	if ((r_|g_|b_|a_) != 0 && n_ != 0)
	{
		ASSERT((r_ & g_) == 0);
		ASSERT((r_ & b_) == 0);
		ASSERT((r_ & a_) == 0);
		ASSERT((g_ & b_) == 0);
		ASSERT((g_ & a_) == 0);
		ASSERT((b_ & a_) == 0);

		#define DEF_D3DFMT(name,id,n,r,g,b,a,flags) \
			if (n==n_ && r==r_ && g==g_ && b==b_ && a==a_) \
			{ \
				return D3DFMT_##name; \
			} \
			// end.
		FOREACH(DEF_D3DFMT)
		#undef  DEF_D3DFMT
	}

	return D3DFMT_UNKNOWN;
}

D3DFORMAT GetFormatFromFourCC(uint32_t cc, bool bSupportDXT)
{
	#define DEF_D3DFMT(name,id,n,r,g,b,a,flags) \
		if (cc == id) \
		{ \
			return (D3DFORMAT)cc; \
		} \
		// end.
	FOREACH(DEF_D3DFMT)
	#undef  DEF_D3DFMT

	if (bSupportDXT) // force support for DXT formats because they are so common .. eventually need to support these properly
	{
		if (cc == MAKEFOURCC('D','X','T','1') ||
			cc == MAKEFOURCC('D','X','T','3') ||
			cc == MAKEFOURCC('D','X','T','5') ||
			cc == MAKEFOURCC('A','T','I','2'))
		{
			return (D3DFORMAT)cc;
		}
	}

	return D3DFMT_UNKNOWN;
}

int GetFormatBitsPerPixel(D3DFORMAT fmt)
{
	#define DEF_D3DFMT(name,id,n,r,g,b,a,flags) \
		if (fmt == D3DFMT_##name) \
		{ \
			return n; \
		} \
		// end.
	FOREACH(DEF_D3DFMT)
	#undef  DEF_D3DFMT

	return D3DFMT_UNKNOWN;
}

std::string GetFormatStringFromFourCC(uint32_t cc)
{
	if (IsPrintableFourCC(cc))
	{
		const uchar8_t ch0 = (uchar8_t)(cc >> (8*0));
		const uchar8_t ch1 = (uchar8_t)(cc >> (8*1));
		const uchar8_t ch2 = (uchar8_t)(cc >> (8*2));
		const uchar8_t ch3 = (uchar8_t)(cc >> (8*3));

		return CString("'%c%c%c%c'", ch0,ch1,ch2,ch3);
	}

	return CString("(%d)", cc);
}

std::string GetFormatStringFromFormat(D3DFORMAT fmt, bool bSupportDXT)
{
	#define DEF_D3DFMT(name,id,n,r,g,b,a,flags) \
		if (fmt == D3DFMT_##name) \
		{ \
			return "D3DFMT_" STRING(name); \
		} \
		// end.
	FOREACH(DEF_D3DFMT)
	#undef  DEF_D3DFMT

	if (bSupportDXT) // force support for DXT formats because they are so common .. eventually need to support these properly
	{
		if ((int)fmt == MAKEFOURCC('D','X','T','1')) { return "D3DFMT_DXT1"; }
		if ((int)fmt == MAKEFOURCC('D','X','T','3')) { return "D3DFMT_DXT3"; }
		if ((int)fmt == MAKEFOURCC('D','X','T','5')) { return "D3DFMT_DXT5"; }
		if ((int)fmt == MAKEFOURCC('A','T','I','2')) { return "D3DFMT_ATI2"; }
	}

	return "D3DFMT_UNKNOWN" + GetFormatStringFromFourCC((uint32_t)fmt);
}

std::string GetArrayString(const uint32_t* data, int count)
{
	bool bNonZero = false;

	for (int i = 0; i < count; i++)
	{
		if (data[i] != 0)
		{
			bNonZero = true;
		}
	}

	if (bNonZero)
	{
		std::string str("[");

		for (int i = 0; i < count; i++)
		{
			str += CString("%s%d", i > 0 ? "," : "", data[i]);
		}

		return str + "]";
	}

	return "0";
}

std::string GetDescriptionString(const char* path)
{
	FILE* f = fopen(path, "rb");

	if (f)
	{
		fseek(f, 0, SEEK_END);
		int siz = (int)ftell(f);
		fseek(f, 0, SEEK_SET);

		if (siz >= SIZEOF(DDSURFACEDESC2))
		{
			DDSURFACEDESC2 header;

			fread(&header, SIZEOF(header), 1, f);
			fclose(f);

			return GetDescriptionString(header);
		}

		fclose(f);
	}

	return "";
}

std::string GetDescriptionString(const DDSURFACEDESC2& header)
{
	std::string str;

	str += CString("header.dwMagic...............%s\n",          GetFormatStringFromFourCC(header.dwMagic).c_str());
	str += CString("header.dwSize................%d\n",          header.dwSize);
	str += CString("header.dwFlags...............0x%08x:[%s]\n", header.dwFlags, header.dwFlags_.GetString().c_str());
	str += CString("header.dwHeight..............%d\n",          header.dwHeight);
	str += CString("header.dwWidth...............%d\n",          header.dwWidth);
	str += CString("header.dwPitchOrLinearSize...%d\n",          header.dwPitchOrLinearSize);
	str += CString("header.dwDepth...............%d\n",          header.dwDepth);
	str += CString("header.dwMipMapCount.........%d\n",          header.dwMipMapCount);
	str += CString("header.dwReserved1...........%s\n",          GetArrayString(header.dwReserved1, NUMBEROF(header.dwReserved1)).c_str());
	str += CString("header.ddpf.dwSize...........%d\n",          header.ddpf.dwSize);
	str += CString("header.ddpf.dwFlags..........0x%08x:[%s]\n", header.ddpf.dwFlags, header.ddpf.dwFlags_.GetString().c_str());
	str += CString("header.ddpf.dwFourCC.........%s\n",          GetFormatStringFromFormat(header.ddpf.dwFormat_).c_str());
	str += CString("header.ddpf.dwRGBBitCount....%d\n",          header.ddpf.dwRGBBitCount);
	str += CString("header.ddpf.dwRBitMask.......0x%08x\n",      header.ddpf.dwRBitMask);
	str += CString("header.ddpf.dwGBitMask.......0x%08x\n",      header.ddpf.dwGBitMask);
	str += CString("header.ddpf.dwBBitMask.......0x%08x\n",      header.ddpf.dwBBitMask);
	str += CString("header.ddpf.dwABitMask.......0x%08x\n",      header.ddpf.dwABitMask);
	str += CString("header.ddsCaps.dwCaps1.......0x%08x:[%s]\n", header.ddsCaps.dwCaps1, header.ddsCaps.dwCaps1_.GetString().c_str());
	str += CString("header.ddsCaps.dwCaps2.......0x%08x:[%s]\n", header.ddsCaps.dwCaps2, header.ddsCaps.dwCaps2_.GetString().c_str());
	str += CString("header.ddsCaps.dwReserved....%s\n",          GetArrayString(header.ddsCaps.dwReserved, NUMBEROF(header.ddsCaps.dwReserved)).c_str());
	str += CString("header.dwReserved2...........%s\n",          GetArrayString(header.dwReserved2, NUMBEROF(header.dwReserved2)).c_str());

	return str;
}

// ================================================================================================

static const char* gDebugFilePath = NULL;

static void debugf(FILE* f, const char* fmt = NULL, ...)
{
	static std::vector<FILE*> streams;

	if (f != NULL && fmt == NULL)
	{
		streams.push_back(f);
	}
	else
	{
		va_list args;
		va_start(args, fmt);

		for (int i = 0; i < (int)streams.size(); i++)
		{
			if (gDebugFilePath)
			{
				fprintf(streams[i], "\n%s\n", gDebugFilePath);
			}

			vfprintf(streams[i], fmt, args);
			fflush  (streams[i]);
		}

		gDebugFilePath = NULL;

		va_end(args);
	}
}

template <typename T> class CAutoPush
{
public:
	__forceinline CAutoPush(T* var, const T& value) : mVar(var), mDefault(*var)
	{
		*mVar = value;
	}

	__forceinline ~CAutoPush() // pop
	{
		*mVar = mDefault;
	}

private:
	T* mVar;
	T  mDefault;
};

#define AUTOPUSH(T,var,value) const CAutoPush<T> AUTOPUSH_##var(&var,value)

static bool ValidateDDS(const char* path, const char* logpath = "log.txt")
{
	AUTOPUSH(const char*, gDebugFilePath, path);

	// init
	{
		static bool bOnce = true;

		if (bOnce)
		{
			bOnce = false;
			debugf(stdout);
			debugf(fopen(logpath, "w"));
		}
	}

	if (0)
	{
		debugf(NULL, "loading \"%s\"..\n", path);
	}

	FILE* f = fopen(path, "rb");

	if (f == NULL)
	{
		debugf(NULL, "fopen failed!\n");
		return false;
	}

	bool bSuccess = true;

	do
	{
		fseek(f, 0, SEEK_END);
		int siz = (int)ftell(f);
		fseek(f, 0, SEEK_SET);

		if (siz < SIZEOF(DDSURFACEDESC2))
		{
			debugf(NULL, "file size is smaller than DDSURFACEDESC2 by %d bytes!\n", SIZEOF(DDSURFACEDESC2) - siz);
			bSuccess = false;
			break;
		}

		// ==============================================================================================
		DDSURFACEDESC2 header;

		fread(&header, SIZEOF(header), 1, f);

		if (header.dwMagic != MAKEFOURCC('D','D','S',' '))
		{
			debugf(NULL, "header.dwMagic expected 'DDS ', got [0x%08x]!\n", header.dwMagic);
			bSuccess = false;
			break; // can't handle this
		}
		if (header.dwSize != SIZEOF(header) - SIZEOF(uint32_t))
		{
			debugf(NULL, "header.dwSize expected %d bytes, got %d!\n", SIZEOF(header) - SIZEOF(uint32_t), header.dwSize);
			bSuccess = false;
		}
		if (header.ddpf.dwSize != SIZEOF(header.ddpf))
		{
			debugf(NULL, "header.ddpf.dwSize expected %d bytes, got %d!\n", SIZEOF(header.ddpf), header.ddpf.dwSize);
			bSuccess = false;
		}

		if (1) // try just matching the format directly ..
		{
			D3DFORMAT fmt = header.ddpf.GetFormat(true, true);

			if (fmt == D3DFMT_UNKNOWN)
			{
				debugf(NULL, "header.ddpf could not match D3DFORMAT!\n");
				bSuccess = false;
			}
		}

		// ==============================================================================================

		uint32_t maxDimension = Max<uint32_t>(Max<uint32_t>(header.dwWidth, header.dwHeight), header.dwDepth);
		uint32_t maxMipCount = 0;
		while ((1UL<<maxMipCount) <= maxDimension) { maxMipCount++; }
		// e.g. if maxDimension=256, maxMipCount will be 9 .. this is the INCLUSIVE mip count

		if (header.dwWidth < 1 || header.dwWidth > 4096)
		{
			debugf(NULL, "header.dwWidth expected [1..4096], got %d!\n", header.dwWidth);
			bSuccess = false;
		}
		if (header.dwHeight < 1 || header.dwHeight > 4096)
		{
			debugf(NULL, "header.dwHeight expected [1..4096], got %d!\n", header.dwHeight);
			bSuccess = false;
		}
		//if ((header.dwWidth & (header.dwWidth - 1)) != 0)
		//{
		//	debugf(NULL, "header.dwWidth expected pow-of-2, got %d!\n", header.dwWidth);
		//	bSuccess = false;
		//}
		//if ((header.dwHeight & (header.dwHeight - 1)) != 0)
		//{
		//	debugf(NULL, "header.dwHeight expected pow-of-2, got %d!\n", header.dwHeight);
		//	bSuccess = false;
		//}
		if ((header.dwDepth & (header.dwDepth - 1)) != 0)
		{
			debugf(NULL, "header.dwDepth expected pow-of-2, got %d!\n", header.dwDepth);
			bSuccess = false;
		}
		if (header.dwMipMapCount > maxMipCount)
		{
			debugf(NULL, "header.dwMipMapCount expected <= %d, got %d!\n", maxMipCount, header.dwMipMapCount);
			bSuccess = false;
		}

		// ==============================================================================================
		uint32_t flags = header.dwFlags;

		flags &= ~DDSD_CAPS; if ((header.dwFlags & DDSD_CAPS)==0)
		{
			debugf(NULL, "header.dwFlags does not include DDSD_CAPS!\n");
			bSuccess = false;
		}
		flags &= ~DDSD_HEIGHT; if ((header.dwFlags & DDSD_HEIGHT)==0)
		{
			debugf(NULL, "header.dwFlags does not include DDSD_HEIGHT!\n");
			bSuccess = false;
		}
		flags &= ~DDSD_WIDTH; if ((header.dwFlags & DDSD_WIDTH)==0)
		{
			debugf(NULL, "header.dwFlags does not include DDSD_WIDTH!\n");
			bSuccess = false;
		}
		flags &= ~DDSD_PIXELFORMAT; if ((header.dwFlags & DDSD_PIXELFORMAT)==0)
		{
			debugf(NULL, "header.dwFlags does not include DDSD_PIXELFORMAT!\n");
			bSuccess = false;
		}
		flags &= ~DDSD_MIPMAPCOUNT; if ((header.dwFlags & DDSD_MIPMAPCOUNT)!=0) // expect mipmap count
		{
			if (header.dwMipMapCount == 0)
			{
				debugf(NULL, "header.dwFlags includes DDSD_MIPMAPCOUNT but dwMipMapCount is zero!\n");
				bSuccess = false;
			}
		}
		else // expect no mipmap count
		{
			if (header.dwMipMapCount != 0)
			{
				debugf(NULL, "header.dwFlags does not include DDSD_MIPMAPCOUNT but dwMipMapCount is %d!\n", header.dwMipMapCount);
				bSuccess = false;
			}
		}
		flags &= ~DDSD_DEPTH; if ((header.dwFlags & DDSD_DEPTH)!=0) // expect depth
		{
			if (header.dwDepth == 0)
			{
				debugf(NULL, "header.dwFlags includes DDSD_DEPTH but dwDepth is zero!\n");
				bSuccess = false;
			}
		}
		else // expect no depth
		{
			if (header.dwDepth != 0)
			{
				debugf(NULL, "header.dwFlags does not include DDSD_DEPTH but dwDepth is %d!\n", header.dwDepth);
				bSuccess = false;
			}
		}
		flags &= ~DDSD_PITCH; if ((header.dwFlags & DDSD_PITCH)!=0) // expect pitch (or linear size)
		{
			if (header.dwPitchOrLinearSize == 0)
			{
				debugf(NULL, "header.dwFlags includes DDSD_PITCH but dwPitchOrLinearSize is zero!\n");
				bSuccess = false;
			}
		}
		flags &= ~DDSD_LINEARSIZE; if ((header.dwFlags & DDSD_LINEARSIZE)!=0) // expect linear size (or pitch)
		{
			if (header.dwPitchOrLinearSize == 0)
			{
				debugf(NULL, "header.dwFlags includes DDSD_LINEARSIZE but dwPitchOrLinearSize is zero!\n");
				bSuccess = false;
			}
		}
		if ((header.dwFlags & (DDSD_PITCH | DDSD_LINEARSIZE))==0) // expect no pitch or linear size
		{
			if (header.dwPitchOrLinearSize != 0)
			{
				debugf(NULL, "header.dwFlags does not include DDSD_PITCH or DDSD_LINEARSIZE but dwPitchOrLinearSize is %d!\n", header.dwPitchOrLinearSize);
				bSuccess = false;
			}
		}
		else if ((header.dwFlags & (DDSD_PITCH | DDSD_LINEARSIZE)) == (DDSD_PITCH | DDSD_LINEARSIZE))
		{
			debugf(NULL, "header.dwFlags includes both DDSD_PITCH and DDSD_LINEARSIZE!\n");
			bSuccess = false;
		}
		if (flags)
		{
			debugf(NULL, "header.dwFlags includes unexpected flags [%s]!\n", header.dwFlags_.GetString(flags).c_str());
			bSuccess = false;
		}

		// ==============================================================================================
		uint32_t caps = header.ddsCaps.dwCaps1;
		bool bHasAlphaCapsFlag = false;

		caps &= ~DDSCAPS_COMPLEX; if ((header.ddsCaps.dwCaps1 & DDSCAPS_COMPLEX)!=0) // expect complex
		{
			if (header.dwMipMapCount == 0 && (header.ddsCaps.dwCaps2 & DDSCAPS2_CUBEMAP)==0 && (header.ddsCaps.dwCaps2 & DDSCAPS2_VOLUME)==0)
			{
				debugf(NULL, "header.ddsCaps.dwCaps1 includes DDSCAPS_COMPLEX but no mip chain, cubemap or volume texture!\n");
				bSuccess = false;
			}
		}
		else // expect not complex
		{
			if (header.dwMipMapCount != 0)
			{
				debugf(NULL, "header.ddsCaps.dwCaps1 does not include DDSCAPS_COMPLEX but dwMipMapCount = %d!\n", header.dwMipMapCount);
				bSuccess = false;
			}
			else if ((header.ddsCaps.dwCaps2 & DDSCAPS2_CUBEMAP)!=0)
			{
				debugf(NULL, "header.ddsCaps.dwCaps1 does not include DDSCAPS_COMPLEX but DDSCAPS2_CUBEMAP is specified!\n");
				bSuccess = false;
			}
			else if ((header.ddsCaps.dwCaps2 & DDSCAPS2_VOLUME)!=0)
			{
				debugf(NULL, "header.ddsCaps.dwCaps1 does not include DDSCAPS_COMPLEX but DDSCAPS2_VOLUME is specified!\n");
				bSuccess = false;
			}
		}
		caps &= ~DDSCAPS_TEXTURE; if ((header.ddsCaps.dwCaps1 & DDSCAPS_TEXTURE)==0)
		{
			debugf(NULL, "header.ddsCaps.dwCaps1 includes DDSCAPS_TEXTURE!\n");
			bSuccess = false;
		}
		caps &= ~DDSCAPS_MIPMAP; if ((header.ddsCaps.dwCaps1 & DDSCAPS_MIPMAP)!=0) // expect mipmaps
		{
			if (header.dwMipMapCount == 0)
			{
				debugf(NULL, "header.ddsCaps.dwCaps1 includes DDSCAPS_MIPMAP but dwMipMapCount zero!\n");
				bSuccess = false;
			}
		}
		else // expect no mipmaps
		{
			if (header.dwMipMapCount != 0)
			{
				debugf(NULL, "header.ddsCaps.dwCaps1 does not include DDSCAPS_MIPMAP but dwMipMapCount = %d!\n", header.dwMipMapCount);
				bSuccess = false;
			}
		}
		caps &= ~DDSCAPS_ALPHA; if ((header.ddsCaps.dwCaps1 & DDSCAPS_ALPHA)!=0)
		{
			bHasAlphaCapsFlag = true; // make a note of this .. check it against the pixel format later
		}
		if (caps)
		{
			debugf(NULL, "header.ddsCaps.dwCaps1 includes unexpected flags [%s]!\n", header.ddsCaps.dwCaps1_.GetString(caps).c_str());
			bSuccess = false;
		}

		// ==============================================================================================
		uint32_t caps2 = header.ddsCaps.dwCaps2;

		caps2 &= ~DDSCAPS2_VOLUME; if ((header.ddsCaps.dwCaps2 & DDSCAPS2_VOLUME)!=0) // expect volume texture
		{
			if ((header.dwDepth < 1 || header.dwDepth > 256))
			{
				debugf(NULL, "header.dwDepth expected [1..256] for volume texture, got %d!\n", header.dwDepth);
				bSuccess = false;
			}
		}
		else // expect not volume texture
		{
			if (header.dwDepth != 0)
			{
				debugf(NULL, "header.dwDepth expected 0 for non-volume texture, got %d!\n", header.dwDepth);
				bSuccess = false;
			}
		}
		caps2 &= ~(DDSCAPS2_CUBEMAP | DDSCAPS2_CUBEMAP_ALLFACES); if ((header.ddsCaps.dwCaps2 & DDSCAPS2_CUBEMAP)!=0) // expect cubemap
		{
			if ((~header.ddsCaps.dwCaps2 & DDSCAPS2_CUBEMAP_ALLFACES)!=0)
			{
				debugf(NULL, "header.ddsCaps.dwCaps2 expected all cubemap faces for cubemap, missing 0x%08x!\n", ~header.ddsCaps.dwCaps2 & DDSCAPS2_CUBEMAP_ALLFACES);
				bSuccess = false;
			}
			if (header.dwWidth != header.dwHeight)
			{
				debugf(NULL, "cubemap not square! dwWidth = %d, dwHeight = %d!\n", header.dwWidth, header.dwHeight);
				bSuccess = false;
			}
			if ((header.ddsCaps.dwCaps2 & DDSCAPS2_VOLUME)!=0)
			{
				debugf(NULL, "header.ddsCaps.dwCaps2 includes both DDSCAPS2_CUBEMAP and DDSCAPS2_VOLUME!\n");
				bSuccess = false;
			}
		}
		else // expect not cubemap
		{
			if ((header.ddsCaps.dwCaps2 & DDSCAPS2_CUBEMAP_ALLFACES)!=0)
			{
				debugf(NULL, "header.ddsCaps.dwCaps2 does not include DDSCAPS2_CUBEMAP but specifies cubemap faces 0x%08x!\n", header.ddsCaps.dwCaps2 & DDSCAPS2_CUBEMAP_ALLFACES);
				bSuccess = false;
			}
		}
		if (caps2)
		{
			debugf(NULL, "header.ddsCaps.dwCaps2 includes unexpected flags [%s]!\n", header.ddsCaps.dwCaps2_.GetString(caps2).c_str());
			bSuccess = false;
		}

		// ==============================================================================================
		for (int i = 0; i < NUMBEROF(header.dwReserved1); i++)
		{
			if (header.dwReserved1[i] != 0)
			{
				//debugf(NULL, "header.dwReserved1[%d] expected zero, but it is %d [0x%08x]!\n", i, header.dwReserved1[i], header.dwReserved1[i]);
				//bSuccess = false;
				break;
			}
		}
		for (int i = 0; i < NUMBEROF(header.dwReserved2); i++)
		{
			if (header.dwReserved2[i] != 0)
			{
				//debugf(NULL, "header.dwReserved2[%d] expected zero, but it is %d [0x%08x]!\n", i, header.dwReserved2[i], header.dwReserved2[i]);
				//bSuccess = false;
				break;
			}
		}
		for (int i = 0; i < NUMBEROF(header.ddsCaps.dwReserved); i++)
		{
			if (header.ddsCaps.dwReserved[i] != 0)
			{
				//debugf(NULL, "header.ddsCaps.dwReserved[%d] expected zero, but it is %d [0x%08x]!\n", i, header.ddsCaps.dwReserved[i], header.ddsCaps.dwReserved[i]);
				//bSuccess = false;
				break;
			}
		}

		// ==============================================================================================
		D3DFORMAT fmt = D3DFMT_UNKNOWN;
		uint32_t  bpp = 0; // bits per pixel
		uint32_t  pff = header.ddpf.dwFlags;

		pff &= ~DDPF_FOURCC; if ((header.ddpf.dwFlags & DDPF_FOURCC)!=0) // expect FOURCC
		{
			if (header.ddpf.dwFourCC == 0)
			{
				debugf(NULL, "header.ddpf.dwFlags includes DDPF_FOURCC but dwFourCC is zero!\n");
				bSuccess = false;
			}
			else
			{
				fmt = GetFormatFromFourCC(header.ddpf.dwFourCC, true); // will be D3DFMT_UNKNOWN if not a supported FOURCC code

				if (fmt == D3DFMT_UNKNOWN)
				{
					debugf(NULL, "header.ddpf.dwFourCC %s is not supported!\n", GetFormatStringFromFourCC(header.ddpf.dwFourCC).c_str());
					bSuccess = false;
				}
			}

			if (header.ddpf.dwRBitMask | header.ddpf.dwGBitMask | header.ddpf.dwBBitMask | header.ddpf.dwABitMask)
			{
				if (header.ddpf.dwFourCC != D3DFMT_CxV8U8) // for some reason this is a FOURCC format but it has a mask ..
				{
					debugf(NULL, "header.ddpf.dwFlags includes DDPF_FOURCC %s but mask is not zero! [0x%08x,0x%08x,0x%08x,0x%08x]\n",
						GetFormatStringFromFourCC(header.ddpf.dwFourCC).c_str(),
						header.ddpf.dwRBitMask,
						header.ddpf.dwGBitMask,
						header.ddpf.dwBBitMask,
						header.ddpf.dwABitMask
						);
					bSuccess = false;
				}
			}
			else if (header.ddpf.dwRGBBitCount != 0) // this seems to happen with some DXT3 and DXT5's ... not sure why
			{
				if (!(header.ddpf.dwFourCC == D3DFMT_DXT3 && header.ddpf.dwRGBBitCount == 256) &&
					!(header.ddpf.dwFourCC == D3DFMT_DXT5 && header.ddpf.dwRGBBitCount == 512))
				{
					debugf(NULL, "header.ddpf.dwFlags includes DDPF_FOURCC %s but dwRGBBitCount is %d!\n", GetFormatStringFromFourCC(header.ddpf.dwFourCC).c_str(), header.ddpf.dwRGBBitCount);
					bSuccess = false;
				}
			}

			pff &= ~DDPF_ALPHA; if (header.ddpf.dwFlags & DDPF_ALPHA)
			{
				debugf(NULL, "header.ddpf.dwFlags includes both DDPF_FOURCC and DDPF_ALPHA!\n");
				bSuccess = false;
			}
			pff &= ~DDPF_ALPHAPIXELS; if (header.ddpf.dwFlags & DDPF_ALPHAPIXELS)
			{
				debugf(NULL, "header.ddpf.dwFlags includes both DDPF_FOURCC and DDPF_ALPHAPIXELS!\n");
				bSuccess = false;
			}
			pff &= ~DDPF_RGB; if (header.ddpf.dwFlags & DDPF_RGB)
			{
				debugf(NULL, "header.ddpf.dwFlags includes both DDPF_FOURCC and DDPF_RGB!\n");
				bSuccess = false;
			}
			pff &= ~DDPF_LUMINANCE; if (header.ddpf.dwFlags & DDPF_LUMINANCE)
			{
				debugf(NULL, "header.ddpf.dwFlags includes both DDPF_FOURCC and DDPF_LUMINANCE!\n");
				bSuccess = false;
			}
			if (bHasAlphaCapsFlag)
			{
				debugf(NULL, "header.ddpf.dwFlags includes DDPF_FOURCC and header.ddsCaps.dwCaps1 includes DDSCAPS_ALPHA!\n");
				bSuccess = false;
			}
		}
		else // expect no FOURCC
		{
			if (header.ddpf.dwFourCC != 0)
			{
				debugf(NULL, "header.ddpf.dwFlags does not include DDPF_FOURCC but dwFourCC is %s!\n", GetFormatStringFromFourCC(header.ddpf.dwFourCC).c_str());
				bSuccess = false;
			}

			pff &= ~(DDPF_ALPHAPIXELS | DDPF_ALPHA | DDPF_RGB | DDPF_LUMINANCE);

			const bool bHasAlphaPixels = (header.ddpf.dwFlags & DDPF_ALPHAPIXELS)!=0;
			const bool bHasAlpha       = (header.ddpf.dwFlags & DDPF_ALPHA      )!=0;
			const bool bHasRGB         = (header.ddpf.dwFlags & DDPF_RGB        )!=0;
			const bool bHasLuminance   = (header.ddpf.dwFlags & DDPF_LUMINANCE  )!=0;

			bool bValidPattern = true;

			if (false == bHasAlphaPixels && // RGB (or RG)
				false == bHasAlpha       &&
				true  == bHasRGB         &&
				false == bHasLuminance   )
			{
			}
			else
			if (true  == bHasAlphaPixels && // RGBA
				false == bHasAlpha       &&
				true  == bHasRGB         &&
				false == bHasLuminance   )
			{
			}
			else
			if (false == bHasAlphaPixels && // A
				true  == bHasAlpha       &&
				false == bHasRGB         &&
				false == bHasLuminance   )
			{
			}
			else
			if (false == bHasAlphaPixels && // L
				false == bHasAlpha       &&
				false == bHasRGB         &&
				true  == bHasLuminance   )
			{
			}
			else
			if (true  == bHasAlphaPixels && // AL
				false == bHasAlpha       &&
				false == bHasRGB         &&
				true  == bHasLuminance   )
			{
			}
			else
			{
				debugf(NULL, "header.ddpf.dwFlags invalid combination: [%s]!\n", header.ddpf.dwFlags_.GetString(DDPF_ALPHAPIXELS | DDPF_ALPHA | DDPF_RGB | DDPF_LUMINANCE).c_str());
				bSuccess = false;
				bValidPattern = false;
			}

			if (bHasRGB) // RG mask must be non-zero, but B can be zero if R|G = 0xffffffff
			{
				const uint32_t RG = header.ddpf.dwRBitMask | header.ddpf.dwGBitMask;

				if (header.ddpf.dwRBitMask == 0 || header.ddpf.dwGBitMask == 0 || (header.ddpf.dwBBitMask == 0 && RG != 0xffffffff))
				{
					debugf(NULL, "header.ddpf.dwFlags includes DDPF_RGB but RGB mask is [0x%08x,0x%08x,0x%08x]!\n", header.ddpf.dwRBitMask, header.ddpf.dwGBitMask, header.ddpf.dwBBitMask);
					bSuccess = false;
					bValidPattern = false;
				}
			}
			else if (bHasLuminance) // luminance is stored in R channel, GB must be zero
			{
				if (header.ddpf.dwRBitMask == 0 || header.ddpf.dwGBitMask != 0 || header.ddpf.dwBBitMask != 0)
				{
					debugf(NULL, "header.ddpf.dwFlags includes DDPF_LUMINANCE but RGB mask is [0x%08x,0x%08x,0x%08x]!\n", header.ddpf.dwRBitMask, header.ddpf.dwGBitMask, header.ddpf.dwBBitMask);
					bSuccess = false;
					bValidPattern = false;
				}
			}
			else // neither RGB nor LUMINANCE
			{
				if (header.ddpf.dwRBitMask != 0 || header.ddpf.dwGBitMask != 0 || header.ddpf.dwBBitMask != 0)
				{
					debugf(NULL, "header.ddpf.dwFlags does not include DDPF_RGB or DDPF_LUMINANCE but RGB mask is [0x%08x,0x%08x,0x%08x]!\n", header.ddpf.dwRBitMask, header.ddpf.dwGBitMask, header.ddpf.dwBBitMask);
					bSuccess = false;
					bValidPattern = false;
				}
			}
			if (bHasAlphaPixels || bHasAlpha)
			{
				if (header.ddpf.dwABitMask == 0)
				{
					debugf(NULL, "header.ddpf.dwFlags includes DDPF_ALPHAPIXELS or DDPF_ALPHA but alpha mask is zero!\n");
					bSuccess = false;
					bValidPattern = false;
				}
			}
			else // no alpha
			{
				if (header.ddpf.dwABitMask != 0)
				{
					debugf(NULL, "header.ddpf.dwFlags does not include DDPF_ALPHAPIXELS or DDPF_ALPHA but alpha mask is 0x%08x!\n", header.ddpf.dwABitMask);
					bSuccess = false;
					bValidPattern = false;
				}
			}

			// i'm not sure why this is hitting .. DDSCAPS_ALPHA seems to *only* be used with DDPF_ALPHAPIXELS + DDPF_RGB, but not the other way around
			{
				if (bHasAlphaPixels && !bHasAlphaCapsFlag)
				{
					//debugf(NULL, "header.ddpf.dwFlags includes DDPF_ALPHAPIXELS but header.ddsCaps.dwCaps1 does not include DDSCAPS_ALPHA!\n");
					//bSuccess = false;
				}
				else if (!bHasAlphaPixels && bHasAlphaCapsFlag)
				{
					debugf(NULL, "header.ddpf.dwFlags does not include DDPF_ALPHAPIXELS but header.ddsCaps.dwCaps1 does include DDSCAPS_ALPHA!\n");
					bSuccess = false;
				}
			}

			if (bValidPattern)
			{
				fmt = GetFormatFromMask(
					header.ddpf.dwRGBBitCount,
					header.ddpf.dwRBitMask,
					header.ddpf.dwGBitMask,
					header.ddpf.dwBBitMask,
					header.ddpf.dwABitMask
					);

				if (fmt != D3DFMT_UNKNOWN)
				{
					bpp = GetFormatBitsPerPixel(fmt);

					if (header.ddpf.dwRGBBitCount != bpp)
					{
						debugf(NULL, "header.ddpf.dwFourCC specified %s which is %d bpp but dwRGBBitCount is %d!\n", GetFormatStringFromFormat(fmt, true).c_str(), bpp, header.ddpf.dwRGBBitCount);
						bSuccess = false;
						bpp = 0; // can't handle this
					}
				}
				else
				{
					debugf(NULL, "header.ddpf.dwRGBBitCount %d with RGBA mask [0x%08x,0x%08x,0x%08x,0x%08x] not recognized!\n",
						header.ddpf.dwRGBBitCount,
						header.ddpf.dwRBitMask,
						header.ddpf.dwGBitMask,
						header.ddpf.dwBBitMask,
						header.ddpf.dwABitMask
						);
					bSuccess = false;
				}
			}
		}
		if (pff)
		{
			debugf(NULL, "header.ddpf.dwFlags includes unexpected flags [%s]!\n", header.ddpf.dwFlags_.GetString(pff).c_str());
			bSuccess = false;
		}

		// ========================================================================================

		// TODO -- validate dwPitchOrLinearSize against bpp*dwWidth ...
		// TODO -- validate filesize, etc.
	}
	while (0);

	fclose(f);

	return bSuccess;
}

static int gNumFilesPassed = 0;
static int gNumFilesTotal  = 0;

void ValidateDDS_dir(const char* dirpath)
{
	FILE* dir = fopen(dirpath, "r");

	if (dir)
	{
		char line[2048] = "";

		while (fgets(line, SIZEOF(line), dir))
		{
			if (strrchr(line, '\n')) { strrchr(line, '\n')[0] = '\0'; }
			if (strrchr(line, '\r')) { strrchr(line, '\r')[0] = '\0'; }

			const char* ext = strrchr(line, '.');

			if (ext && _stricmp(ext + 1, "DDS") == 0)
			{
				if (ValidateDDS(line, "log.txt"))
				{
					gNumFilesPassed++;
				}

				gNumFilesTotal++;
			}
			else if (ext && _stricmp(ext + 1, "TXT") == 0)
			{
				ValidateDDS_dir(line);
			}
		}

		fclose(dir);
	}

	if (_stricmp(dirpath, "dir.txt") == 0)
	{
		debugf(NULL, "\n");
		debugf(NULL, "done (%d out of %d files passed, %.2f%%).\n", gNumFilesPassed, gNumFilesTotal, 100.0f*(float)gNumFilesPassed/(float)gNumFilesTotal);
	}
}

} // namespace DDS_helper
