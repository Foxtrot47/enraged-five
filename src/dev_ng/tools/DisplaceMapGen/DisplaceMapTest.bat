copy Release\DisplaceMapGen.exe DisplaceMapGen.exe

DisplaceMapGen.exe -in test.tga -out test_displace.tga -normal -raylen 32 -numrays 64
DisplaceMapGen.exe -xml sample.xml
pause