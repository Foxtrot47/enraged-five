#include "DisplacementGenerator.h"

#include "imageutil.h"
#include "image.h"

void DisplacementGenerator::Setup(const Image* pSourceImage, Image* pDestImage)
 { 
	mp_SourceImage = pSourceImage; 
	mp_DestImage = pDestImage; 

	ResetProgress();
 }

void DisplacementGenerator::SetProgress(float percentage)
{	
	//this is used to reset the cursor position back to the start of the row, doesn't seem be an easier way to do it
	static const char* backspaces = "\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b";
	static u32 numberOfDivisions = 40;
	
	u32 currentPercentComplete = static_cast<u32>(percentage * 100.0f);

	//only update on whole percentage changes
	if(currentPercentComplete <= m_LastPercentagePrinted)
	{
		return;
	}

	string progressBar;

	for(u32 i = 0; i < numberOfDivisions; ++i)
	{
		float currentPercentage = i * (100.0f / numberOfDivisions );

		if(currentPercentage < percentage * 100.0f)
		{
			progressBar += char(254);
		}
		else
		{
			progressBar += " ";
		}
	}
	
	printf("[%s] %u%%%s", progressBar.c_str(), currentPercentComplete, backspaces);
	m_LastPercentagePrinted = currentPercentComplete;
}

//////////////////////////////////////////////////////////////////////////
float NormalToDisplacement::Convert()
{
	//this is based on the nVidia paper http://developer.download.nvidia.com/assets/gamedev/docs/nmap2displacement.pdf

	u32 imageWidth = GetSource()->GetWidth();
	u32 imageHeight = GetSource()->GetHeight();

	//setup arrays
	m_DepthDifferenceMap.resize(imageWidth);
	m_DisplacementScratch.resize(imageWidth);

	for(u32 i = 0; i < imageWidth; ++i)
	{
		m_DepthDifferenceMap[i].resize(imageHeight);
		m_DisplacementScratch[i].resize(imageHeight);
	}

	//first we need to create a depth difference map which stores the delta across the pixel in both the x and y directions
	for(u32 x = 0; x < imageWidth; ++x)
	{
		for(u32 y = 0; y < imageHeight; ++y)
		{
			//extract the normal from the normal map
			Vec3 normalValueInTexture = GetSource()->GetPixel(x, y);
			Vec3 normal = Vec3(normalValueInTexture.x * 2.0f - 1.0f, normalValueInTexture.y * 2.0f - 1.0f, normalValueInTexture.z);
			normal = Normalise(normal);

			if(m_flipX)
			{
				 normal.x = -normal.x;
			}

			if(m_flipY)
			{
				normal.y = -normal.y;
			}

			m_DepthDifferenceMap[x][y].x = normal.x;
			m_DepthDifferenceMap[x][y].y = normal.y;
		}
	}

	//loop around the texture and fire rays to work out the average height displacement
	
	//workout what angle we need to rotate the rays by to get the right number of rays
	float incrementAngle = float(M_PI * 2) / float(m_NumberOfRays);
	
	//for normalization at the end
	float minValue = FLT_MAX;
	float maxValue = FLT_MIN;
	
	for (s32 x = 0; x < (s32)imageWidth; ++x)
	{
		for (s32 y = 0; y < (s32)imageHeight; ++y)
		{	
			float currentAngle = 0.0f;
			float height = 0.0f;
	
			for(u32 r = 0; r < m_NumberOfRays; ++r)
			{
				//rays fired in polar coordinates, need these to calculate the texture coordinates
				float cosineOfAngle = cos(currentAngle);
				float sineOfAngle = sin(currentAngle);
	
				for(u32 cl = 0; cl < m_LengthOfRays - 1; cl++)
				{
					u32 nextPointOnRay = cl + 1;
	
					Vec2 currentCoord = Vec2(x + (cl * cosineOfAngle), y + (cl * sineOfAngle));
					Vec2 nextCoord = Vec2(x + (nextPointOnRay * cosineOfAngle), y + (nextPointOnRay * sineOfAngle));
	
					//if the next coord is off the edge of the texture space then the ray stops here
					if(nextCoord.x < 0 || nextCoord.x >= imageWidth || nextCoord.y< 0 || nextCoord.y >= imageHeight)
					{
						break;
					}
	
					//work out the height difference between this position and the next
					Vec2 heightDifference = m_DepthDifferenceMap[u32(currentCoord.x)][u32(currentCoord.y)] * (nextCoord - currentCoord);
					height += heightDifference.x + heightDifference.y;
				}
	
				//rotate to the next ray angle
				currentAngle += incrementAngle;
			}
	
			//store the displacement and work out if there are new min max values
			float displacment = height / (float)m_NumberOfRays;
			m_DisplacementScratch[x][y] = displacment;

			minValue = Min(minValue, displacment);
			maxValue = Max(maxValue, displacment);
	
			float current = float(y + (x * imageHeight));
			float total = float(imageWidth * imageHeight);

			SetProgress(current / (total - 1));
		}
	}

	//output to the final texture, normalize between 0 and 1 and combine with original normal map
	for(u32 x = 0; x < imageWidth; ++x)
	{			
		for(u32 y = 0; y < imageHeight; ++y)
		{
			float finalDisplacment =  (m_DisplacementScratch[x][y] - minValue) / (maxValue - minValue);
			GetDest()->SetPixel(x, y, finalDisplacment);
		}
	}

	//return the height difference, this is a good estimate for the displacment scale that should be used in the game
	return maxValue - minValue;
}

//////////////////////////////////////////////////////////////////////////

float ColorToDisplacement::Convert()
{
	for (u32 x = 0; x < GetSource()->GetWidth(); ++x)
	{
		for (u32 y = 0; y < GetSource()->GetHeight(); ++y)
		{	
			Vec3 color = GetSource()->GetPixel(x, y);

			//figure out if were closer to the high or the low color and then use that value as a basis for the displacment
			float averageDistanceToHighColor = abs((m_HighColor.x - color.x) + (m_HighColor.y - color.y) + (m_HighColor.z - color.z)) / 3;
			float averageDistanceToLowColor = abs((m_LowColor.x - color.x) + (m_LowColor.y - color.y) + (m_LowColor.z - color.z)) / 3;

			if(averageDistanceToHighColor < averageDistanceToLowColor)
			{
				GetDest()->SetPixel(x, y, 1.0f - averageDistanceToHighColor);
			}
			else
			{
				GetDest()->SetPixel(x, y, averageDistanceToLowColor);
			}
		}
	}

	//we can't estimate the scale present in the original texture, as
	//were not working with any depth information so we'll just return a reasonable default
	return 2.0f;
}