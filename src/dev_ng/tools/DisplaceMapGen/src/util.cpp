#include "util.h"

#define MAX_MESSAGE_LENGTH 256

void OnError(bool isFatal, char* fmt, ...)
{
	char buffer[MAX_MESSAGE_LENGTH];

	va_list args;
	va_start(args,fmt);
	::vsprintf_s(buffer, fmt, args);
	va_end(args);

	if(isFatal)
	{
		printf("Error: %s\n", buffer);
		exit(-1);
	}
	else
	{
		printf("Warning: %s\n", buffer);
	}
}

Vec3 GetColorFromString(std::string string, char seperator)
{
	Vec3 output;

	int numberOfElemenetsProcessed = 0;
	size_t currentPosition = 0;

	while(numberOfElemenetsProcessed < 3)
	{
		currentPosition = string.find(seperator);

		if(currentPosition == -1)
		{
			currentPosition = string.length();
		}

		std::string substr = string.substr(0, currentPosition).c_str();

		if(currentPosition != string.length())
		{
			string = string.substr(currentPosition + 1, string.length());
		}

		float value = Clamp(static_cast<float>(atof(substr.c_str())) / 255.0f, 0.0f, 1.0f);

		switch(numberOfElemenetsProcessed)
		{
			case 0: output.x = value; break;
			case 1: output.y = value; break;
			case 2: output.z = value; break;
		};

		++numberOfElemenetsProcessed;
	}

	return output;
}

std::string GenerateOutputName(std::string inputName)
{
	std::string outputName;

	size_t endOfFileNameIndex = inputName.find('.');

	if(endOfFileNameIndex != -1)
	{
		outputName = inputName;
		outputName.insert(endOfFileNameIndex, "_d");
	}

	return outputName;
}