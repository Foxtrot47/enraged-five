// =============
// imageutil.cpp
// =============

#include "imageutil.h"

#include "dds.h"
#include "mathutil.h"
#include "progressdisplay.h"

bool GetImageDimensionsDDS(const char* path, int& w, int& h)
{
	if (strstr(path, ".dds"))
	{
		FILE* ddsFile = fopen(path, "rb");

		if (ddsFile)
		{
			DDSURFACEDESC2 ddsHeader;

			fseek(ddsFile, 4, SEEK_SET); // skip past tag
			fread(&ddsHeader, sizeof(DDSURFACEDESC2), 1, ddsFile);
			fclose(ddsFile);

			if (ddsHeader.dwSize == sizeof(DDSURFACEDESC2))
			{
				w = (int)ddsHeader.dwWidth;
				h = (int)ddsHeader.dwHeight;

				return true;
			}
		}
	}

	return false;
}

float* LoadImageDDS(const char* path, int& w, int& h)
{
	float* image = NULL;

	if (strstr(path, ".dds"))
	{
		FILE* ddsFile = fopen(path, "rb");

		if (ddsFile)
		{
			fseek(ddsFile, 0, SEEK_END);
			const u32 ddsSize = (u32)ftell(ddsFile);
			fseek(ddsFile, 0, SEEK_SET);

			if (ddsSize > sizeof(DDSURFACEDESC2))
			{
				u8* ddsData = new u8[ddsSize];
				fread(ddsData, ddsSize, 1, ddsFile);

				const DDSURFACEDESC2* ddsHeader = (DDSURFACEDESC2*)(ddsData + 4);
				const void*           ddsPixels = (const u8*)ddsHeader + ddsHeader->dwSize;

				assert(ddsHeader->dwSize == sizeof(DDSURFACEDESC2));

				if ((w == 0 && h == 0) || (w == (int)ddsHeader->dwWidth && h == (int)ddsHeader->dwHeight))
				{
					w = (int)ddsHeader->dwWidth;
					h = (int)ddsHeader->dwHeight;

					if (GetDDSFormat(ddsHeader->ddpfPixelFormat) == DDS_D3DFMT_A8R8G8B8 && ddsSize >= ddsHeader->dwSize + w*h*sizeof(Pixel32))
					{
						const Pixel32* pixels = (const Pixel32*)ddsPixels;
						image = new float[w*h];

						for (int i = 0; i < w*h; i++)
						{
							image[i] = (float)pixels[i].r/255.0f;
						}
					}

					if (GetDDSFormat(ddsHeader->ddpfPixelFormat) == DDS_D3DFMT_L8 && ddsSize >= ddsHeader->dwSize + w*h*sizeof(u8))
					{
						const u8* pixels = (const u8*)ddsPixels;
						image = new float[w*h];

						for (int i = 0; i < w*h; i++)
						{
							image[i] = (float)pixels[i]/255.0f;
						}
					}

					if (GetDDSFormat(ddsHeader->ddpfPixelFormat) == DDS_D3DFMT_L16 && ddsSize >= ddsHeader->dwSize + w*h*sizeof(u16))
					{
						const u16* pixels = (const u16*)ddsPixels;
						image = new float[w*h];

						for (int i = 0; i < w*h; i++)
						{
							image[i] = (float)pixels[i]/65535.0f;
						}
					}

					if (GetDDSFormat(ddsHeader->ddpfPixelFormat) == DDS_D3DFMT_R32F && ddsSize >= ddsHeader->dwSize + w*h*sizeof(float))
					{
						const float* pixels = (const float*)ddsPixels;
						image = new float[w*h];

						for (int i = 0; i < w*h; i++)
						{
							image[i] = pixels[i];
						}
					}
				}

				delete[] ddsData;
			}

			fclose(ddsFile);
		}
	}

	return image;
}

float* LoadImageDDSSpan(const char* path, int w, int y0, int h)
{
	float* image = NULL;

	if (strstr(path, ".dds"))
	{
		FILE* ddsFile = fopen(path, "rb");

		if (ddsFile)
		{
			fseek(ddsFile, 0, SEEK_END);
			const u32 ddsSize = (u32)ftell(ddsFile);
			fseek(ddsFile, 0, SEEK_SET);

			if (ddsSize > sizeof(DDSURFACEDESC2))
			{
				u8* ddsData = new u8[ddsSize];
				fread(ddsData, ddsSize, 1, ddsFile);

				const DDSURFACEDESC2* ddsHeader = (DDSURFACEDESC2*)(ddsData + 4);
				const void*           ddsPixels = (const u8*)ddsHeader + ddsHeader->dwSize;

				assert(ddsHeader->dwSize == sizeof(DDSURFACEDESC2));

				if (w == (int)ddsHeader->dwWidth && y0 >= 0 && y0 + h <= (int)ddsHeader->dwHeight)
				{
					if (GetDDSFormat(ddsHeader->ddpfPixelFormat) == DDS_D3DFMT_A8R8G8B8 && ddsSize >= ddsHeader->dwSize + w*h*sizeof(Pixel32))
					{
						const Pixel32* pixels = (const Pixel32*)ddsPixels + w*y0;
						image = new float[w*h];

						for (int i = 0; i < w*h; i++)
						{
							image[i] = (float)pixels[i].r/255.0f;
						}
					}

					if (GetDDSFormat(ddsHeader->ddpfPixelFormat) == DDS_D3DFMT_L8 && ddsSize >= ddsHeader->dwSize + w*h*sizeof(u8))
					{
						const u8* pixels = (const u8*)ddsPixels + w*y0;
						image = new float[w*h];

						for (int i = 0; i < w*h; i++)
						{
							image[i] = (float)pixels[i]/255.0f;
						}
					}

					if (GetDDSFormat(ddsHeader->ddpfPixelFormat) == DDS_D3DFMT_L16 && ddsSize >= ddsHeader->dwSize + w*h*sizeof(u16))
					{
						const u16* pixels = (const u16*)ddsPixels + w*y0;
						image = new float[w*h];

						for (int i = 0; i < w*h; i++)
						{
							image[i] = (float)pixels[i]/65535.0f;
						}
					}

					if (GetDDSFormat(ddsHeader->ddpfPixelFormat) == DDS_D3DFMT_R32F && ddsSize >= ddsHeader->dwSize + w*h*sizeof(float))
					{
						const float* pixels = (const float*)ddsPixels + w*y0;
						image = new float[w*h];

						for (int i = 0; i < w*h; i++)
						{
							image[i] = pixels[i];
						}
					}
				}

				delete[] ddsData;
			}

			fclose(ddsFile);
		}
	}

	return image;
}

u8* LoadImageU8DDS(const char* path, int& w, int& h)
{
	u8* image = NULL;

	if (strstr(path, ".dds"))
	{
		FILE* ddsFile = fopen(path, "rb");

		if (ddsFile)
		{
			fseek(ddsFile, 0, SEEK_END);
			const u32 ddsSize = (u32)ftell(ddsFile);
			fseek(ddsFile, 0, SEEK_SET);

			if (ddsSize > sizeof(DDSURFACEDESC2))
			{
				u8* ddsData = new u8[ddsSize];
				fread(ddsData, ddsSize, 1, ddsFile);

				const DDSURFACEDESC2* ddsHeader = (DDSURFACEDESC2*)(ddsData + 4);
				const void*           ddsPixels = (const u8*)ddsHeader + ddsHeader->dwSize;

				assert(ddsHeader->dwSize == sizeof(DDSURFACEDESC2));

				if ((w == 0 && h == 0) || (w == (int)ddsHeader->dwWidth && h == (int)ddsHeader->dwHeight))
				{
					w = (int)ddsHeader->dwWidth;
					h = (int)ddsHeader->dwHeight;

					if (GetDDSFormat(ddsHeader->ddpfPixelFormat) == DDS_D3DFMT_A8R8G8B8 && ddsSize >= ddsHeader->dwSize + w*h*sizeof(Pixel32))
					{
						const Pixel32* pixels = (const Pixel32*)ddsPixels;
						image = new u8[w*h];

						for (int i = 0; i < w*h; i++)
						{
							image[i] = pixels[i].r;
						}
					}

					if (GetDDSFormat(ddsHeader->ddpfPixelFormat) == DDS_D3DFMT_L8 && ddsSize >= ddsHeader->dwSize + w*h*sizeof(u8))
					{
						const u8* pixels = (const u8*)ddsPixels;
						image = new u8[w*h];

						for (int i = 0; i < w*h; i++)
						{
							image[i] = pixels[i];
						}
					}

					if (GetDDSFormat(ddsHeader->ddpfPixelFormat) == DDS_D3DFMT_L16 && ddsSize >= ddsHeader->dwSize + w*h*sizeof(u16))
					{
						const u16* pixels = (const u16*)ddsPixels;
						image = new u8[w*h];

						for (int i = 0; i < w*h; i++)
						{
							image[i] = pixels[i]>>8;
						}
					}

					if (GetDDSFormat(ddsHeader->ddpfPixelFormat) == DDS_D3DFMT_R32F && ddsSize >= ddsHeader->dwSize + w*h*sizeof(float))
					{
						const float* pixels = (const float*)ddsPixels;
						image = new u8[w*h];

						for (int i = 0; i < w*h; i++)
						{
							image[i] = (u8)Clamp<float>(0.5f + 255.0f*pixels[i], 0.0f, 255.0f);
						}
					}
				}

				delete[] ddsData;
			}

			fclose(ddsFile);
		}
	}

	return image;
}

u16* LoadImageU16DDS(const char* path, int& w, int& h)
{
	u16* image = NULL;

	if (strstr(path, ".dds"))
	{
		FILE* ddsFile = fopen(path, "rb");

		if (ddsFile)
		{
			fseek(ddsFile, 0, SEEK_END);
			const u32 ddsSize = (u32)ftell(ddsFile);
			fseek(ddsFile, 0, SEEK_SET);

			if (ddsSize > sizeof(DDSURFACEDESC2))
			{
				u8* ddsData = new u8[ddsSize];
				fread(ddsData, ddsSize, 1, ddsFile);

				const DDSURFACEDESC2* ddsHeader = (DDSURFACEDESC2*)(ddsData + 4);
				const void*           ddsPixels = (const u8*)ddsHeader + ddsHeader->dwSize;

				assert(ddsHeader->dwSize == sizeof(DDSURFACEDESC2));

				if ((w == 0 && h == 0) || (w == (int)ddsHeader->dwWidth && h == (int)ddsHeader->dwHeight))
				{
					w = (int)ddsHeader->dwWidth;
					h = (int)ddsHeader->dwHeight;

					if (GetDDSFormat(ddsHeader->ddpfPixelFormat) == DDS_D3DFMT_A8R8G8B8 && ddsSize >= ddsHeader->dwSize + w*h*sizeof(Pixel32))
					{
						const Pixel32* pixels = (const Pixel32*)ddsPixels;
						image = new u16[w*h];

						for (int i = 0; i < w*h; i++)
						{
							image[i] = pixels[i].r;
							image[i] |= (image[i]<<8);
						}
					}

					if (GetDDSFormat(ddsHeader->ddpfPixelFormat) == DDS_D3DFMT_L8 && ddsSize >= ddsHeader->dwSize + w*h*sizeof(u8))
					{
						const u8* pixels = (const u8*)ddsPixels;
						image = new u16[w*h];

						for (int i = 0; i < w*h; i++)
						{
							image[i] = pixels[i];
							image[i] |= (image[i]<<8);
						}
					}

					if (GetDDSFormat(ddsHeader->ddpfPixelFormat) == DDS_D3DFMT_L16 && ddsSize >= ddsHeader->dwSize + w*h*sizeof(u16))
					{
						const u16* pixels = (const u16*)ddsPixels;
						image = new u16[w*h];

						for (int i = 0; i < w*h; i++)
						{
							image[i] = pixels[i];
						}
					}

					if (GetDDSFormat(ddsHeader->ddpfPixelFormat) == DDS_D3DFMT_R32F && ddsSize >= ddsHeader->dwSize + w*h*sizeof(float))
					{
						const float* pixels = (const float*)ddsPixels;
						image = new u16[w*h];

						for (int i = 0; i < w*h; i++)
						{
							image[i] = (u16)Clamp<float>(0.5f + 65535.0f*pixels[i], 0.0f, 65535.0f);
						}
					}
				}

				delete[] ddsData;
			}

			fclose(ddsFile);
		}
	}

	return image;
}

Pixel32* LoadImagePixel32DDS(const char* path, int& w, int& h)
{
	Pixel32* image = NULL;

	if (strstr(path, ".dds"))
	{
		FILE* ddsFile = fopen(path, "rb");

		if (ddsFile)
		{
			fseek(ddsFile, 0, SEEK_END);
			const u32 ddsSize = (u32)ftell(ddsFile);
			fseek(ddsFile, 0, SEEK_SET);

			if (ddsSize > sizeof(DDSURFACEDESC2))
			{
				u8* ddsData = new u8[ddsSize];
				fread(ddsData, ddsSize, 1, ddsFile);

				const DDSURFACEDESC2* ddsHeader = (DDSURFACEDESC2*)(ddsData + 4);
				const void*           ddsPixels = (const u8*)ddsHeader + ddsHeader->dwSize;

				assert(ddsHeader->dwSize == sizeof(DDSURFACEDESC2));

				if ((w == 0 && h == 0) || (w == (int)ddsHeader->dwWidth && h == (int)ddsHeader->dwHeight))
				{
					w = (int)ddsHeader->dwWidth;
					h = (int)ddsHeader->dwHeight;

					if (GetDDSFormat(ddsHeader->ddpfPixelFormat) == DDS_D3DFMT_A8R8G8B8 && ddsSize >= ddsHeader->dwSize + w*h*sizeof(Pixel32))
					{
						const Pixel32* pixels = (const Pixel32*)ddsPixels;
						image = new Pixel32[w*h];

						for (int i = 0; i < w*h; i++)
						{
							image[i] = pixels[i];
						}
					}
					else
					{
						assert(0); // we don't support other formats when loading Pixel32 image
					}
				}

				delete[] ddsData;
			}

			fclose(ddsFile);
		}
	}

	return image;
}

Vec3* LoadImageRGBDDS(const char* path, int& w, int& h)
{
	return NULL; // TODO
}

float* LoadImage(const char* path, int& w, int& h)
{
	float* image = LoadImageDDS(path, w, h);

	if (image)
	{
		return image;
	}

	FREE_IMAGE_FORMAT fif = FreeImage_GetFileType(path);
	FIBITMAP *dib = NULL;

	if (fif == FIF_UNKNOWN)
	{
		fif = FreeImage_GetFIFFromFilename(path);
	}

	if (fif != FIF_UNKNOWN && FreeImage_FIFSupportsReading(fif))
	{
		dib = FreeImage_Load(fif, path);
	}

	if (dib && FreeImage_GetBPP(dib) != 32)
	{
		FIBITMAP *dib2 = FreeImage_ConvertTo32Bits(dib);
		FreeImage_Unload(dib);
		dib = dib2;
	}

	if (dib)
	{
		if ((w == 0 && h == 0) || (w == FreeImage_GetWidth(dib) && h == FreeImage_GetHeight(dib)))
		{
			w = FreeImage_GetWidth(dib);
			h = FreeImage_GetHeight(dib);

			const Pixel32* pixels = reinterpret_cast<const Pixel32*>(FreeImage_GetBits(dib));

			image = new float[w*h];

			for (int j = 0; j < h; j++)
			{
				for (int i = 0; i < w; i++)
				{
					image[i + j*w] = (float)pixels[i + (h - j - 1)*w].r/255.0f;
				}
			}
		}

		FreeImage_Unload(dib);
	}

	return image;
}

u8* LoadImageU8(const char* path, int& w, int& h)
{
	u8* image = LoadImageU8DDS(path, w, h);

	if (image)
	{
		return image;
	}

	FREE_IMAGE_FORMAT fif = FreeImage_GetFileType(path);
	FIBITMAP *dib = NULL;

	if (fif == FIF_UNKNOWN)
	{
		fif = FreeImage_GetFIFFromFilename(path);
	}

	if (fif != FIF_UNKNOWN && FreeImage_FIFSupportsReading(fif))
	{
		dib = FreeImage_Load(fif, path);
	}

	if (dib && FreeImage_GetBPP(dib) != 32)
	{
		FIBITMAP *dib2 = FreeImage_ConvertTo32Bits(dib);
		FreeImage_Unload(dib);
		dib = dib2;
	}

	if (dib)
	{
		if ((w == 0 && h == 0) || (w == FreeImage_GetWidth(dib) && h == FreeImage_GetHeight(dib)))
		{
			w = FreeImage_GetWidth(dib);
			h = FreeImage_GetHeight(dib);

			const Pixel32* pixels = reinterpret_cast<const Pixel32*>(FreeImage_GetBits(dib));

			image = new u8[w*h];

			for (int j = 0; j < h; j++)
			{
				for (int i = 0; i < w; i++)
				{
					image[i + j*w] = pixels[i + (h - j - 1)*w].r;
				}
			}
		}

		FreeImage_Unload(dib);
	}

	return image;
}

Vec3* LoadImageRGB(const char* path, int& w, int& h)
{
	Vec3* image = LoadImageRGBDDS(path, w, h);

	if (image)
	{
		return image;
	}

	FREE_IMAGE_FORMAT fif = FreeImage_GetFileType(path);
	FIBITMAP *dib = NULL;

	if (fif == FIF_UNKNOWN)
	{
		fif = FreeImage_GetFIFFromFilename(path);
	}

	if (fif != FIF_UNKNOWN && FreeImage_FIFSupportsReading(fif))
	{
		dib = FreeImage_Load(fif, path);
	}

	if (dib && FreeImage_GetBPP(dib) != 32)
	{
		FIBITMAP *dib2 = FreeImage_ConvertTo32Bits(dib);
		FreeImage_Unload(dib);
		dib = dib2;
	}

	if (dib)
	{
		if ((w == 0 && h == 0) || (w == FreeImage_GetWidth(dib) && h == FreeImage_GetHeight(dib)))
		{
			w = FreeImage_GetWidth(dib);
			h = FreeImage_GetHeight(dib);

			const Pixel32* pixels = reinterpret_cast<const Pixel32*>(FreeImage_GetBits(dib));

			image = new Vec3[w*h];

			for (int j = 0; j < h; j++)
			{
				for (int i = 0; i < w; i++)
				{
					image[i + j*w].x = (float)pixels[i + (h - j - 1)*w].r/255.0f;
					image[i + j*w].y = (float)pixels[i + (h - j - 1)*w].g/255.0f;
					image[i + j*w].z = (float)pixels[i + (h - j - 1)*w].b/255.0f;
				}
			}
		}

		FreeImage_Unload(dib);
	}

	return image;
}

Pixel32* LoadImagePixel32(const char* path, int& w, int& h)
{
	Pixel32* image = LoadImagePixel32DDS(path, w, h);

	if (image)
	{
		return image;
	}

	FREE_IMAGE_FORMAT fif = FreeImage_GetFileType(path);
	FIBITMAP *dib = NULL;

	if (fif == FIF_UNKNOWN)
	{
		fif = FreeImage_GetFIFFromFilename(path);
	}

	if (fif != FIF_UNKNOWN && FreeImage_FIFSupportsReading(fif))
	{
		dib = FreeImage_Load(fif, path);
	}

	if (dib && FreeImage_GetBPP(dib) != 32)
	{
		FIBITMAP *dib2 = FreeImage_ConvertTo32Bits(dib);
		FreeImage_Unload(dib);
		dib = dib2;
	}

	if (dib)
	{
		if ((w == 0 && h == 0) || (w == FreeImage_GetWidth(dib) && h == FreeImage_GetHeight(dib)))
		{
			w = FreeImage_GetWidth(dib);
			h = FreeImage_GetHeight(dib);

			const Pixel32* pixels = reinterpret_cast<const Pixel32*>(FreeImage_GetBits(dib));

			image = new Pixel32[w*h];

			for (int j = 0; j < h; j++)
			{
				for (int i = 0; i < w; i++)
				{
					image[i + j*w] = pixels[i + (h - j - 1)*w];
				}
			}
		}

		FreeImage_Unload(dib);
	}

	return image;
}

bool SaveImageDDS(const char* path, const float* image, int w, int h, bool bAutoOpen)
{
	if (strstr(path, ".dds"))
	{
		ProgressDisplayAuto progress("saving \"%s\"", path);
		FILE* dds = fopen(path, "wb");

		if (dds)
		{
			const u32 ddsMagic = MAKE_MAGIC_NUMBER('D','D','S',' ');
			DDSURFACEDESC2 ddsHeader;
			memset(&ddsHeader, 0, sizeof(ddsHeader));

			ddsHeader.dwSize = sizeof(ddsHeader);
			ddsHeader.dwFlags = 0x00001007; // DDSD_CAPS | DDSD_HEIGHT | DDSD_WIDTH | DDSD_PIXELFORMAT
			ddsHeader.dwWidth = w;
			ddsHeader.dwHeight = h;
			ddsHeader.ddpfPixelFormat.dwSize = sizeof(ddsHeader.ddpfPixelFormat);
			ddsHeader.ddpfPixelFormat.dwFlags = DDPF_FOURCC;
			ddsHeader.ddpfPixelFormat.dwFourCC = DDS_D3DFMT_R32F;
			ddsHeader.ddsCaps.dwCaps1 = 0x00001002; // DDSCAPS_ALPHA | DDSCAPS_TEXTURE

			fwrite(&ddsMagic, sizeof(ddsMagic), 1, dds);
			fwrite(&ddsHeader, sizeof(ddsHeader), 1, dds);
			fwrite(image, sizeof(float), w*h, dds);
			fclose(dds);

			if (bAutoOpen)
			{
				system(path);
			}

			return true;
		}
		else
		{
			progress.End("failed");
		}
	}

	return false;
}

bool SaveImageDDS(const char* path, const u8* image, int w, int h, bool bAutoOpen)
{
	if (strstr(path, ".dds"))
	{
		ProgressDisplayAuto progress("saving \"%s\"", path);
		FILE* dds = fopen(path, "wb");

		if (dds)
		{
			const u32 ddsMagic = MAKE_MAGIC_NUMBER('D','D','S',' ');
			DDSURFACEDESC2 ddsHeader;
			memset(&ddsHeader, 0, sizeof(ddsHeader));

			ddsHeader.dwSize = sizeof(ddsHeader);
			ddsHeader.dwFlags = 0x00001007; // DDSD_CAPS | DDSD_HEIGHT | DDSD_WIDTH | DDSD_PIXELFORMAT
			ddsHeader.dwWidth = w;
			ddsHeader.dwHeight = h;
			ddsHeader.ddpfPixelFormat.dwSize = sizeof(ddsHeader.ddpfPixelFormat);
			ddsHeader.ddpfPixelFormat.dwFlags = DDPF_LUMINANCE;
			ddsHeader.ddpfPixelFormat.dwRGBBitCount = 8;
			ddsHeader.ddpfPixelFormat.dwRBitMask = 0x000000ff;
			ddsHeader.ddsCaps.dwCaps1 = 0x00001002; // DDSCAPS_ALPHA | DDSCAPS_TEXTURE

			fwrite(&ddsMagic, sizeof(ddsMagic), 1, dds);
			fwrite(&ddsHeader, sizeof(ddsHeader), 1, dds);
			fwrite(image, sizeof(u8), w*h, dds);
			fclose(dds);

			if (bAutoOpen)
			{
				system(path);
			}

			return true;
		}
		else
		{
			progress.End("failed");
		}
	}

	return false;
}

bool SaveImageDDS(const char* path, const u16* image, int w, int h, bool bAutoOpen)
{
	if (strstr(path, ".dds"))
	{
		ProgressDisplayAuto progress("saving \"%s\"", path);
		FILE* dds = fopen(path, "wb");

		if (dds)
		{
			const u32 ddsMagic = MAKE_MAGIC_NUMBER('D','D','S',' ');
			DDSURFACEDESC2 ddsHeader;
			memset(&ddsHeader, 0, sizeof(ddsHeader));

			ddsHeader.dwSize = sizeof(ddsHeader);
			ddsHeader.dwFlags = 0x00001007; // DDSD_CAPS | DDSD_HEIGHT | DDSD_WIDTH | DDSD_PIXELFORMAT
			ddsHeader.dwWidth = w;
			ddsHeader.dwHeight = h;
			ddsHeader.ddpfPixelFormat.dwSize = sizeof(ddsHeader.ddpfPixelFormat);
			ddsHeader.ddpfPixelFormat.dwFlags = DDPF_LUMINANCE;
			ddsHeader.ddpfPixelFormat.dwRGBBitCount = 16;
			ddsHeader.ddpfPixelFormat.dwRBitMask = 0x0000ffff;
			ddsHeader.ddsCaps.dwCaps1 = 0x00001002; // DDSCAPS_ALPHA | DDSCAPS_TEXTURE

			fwrite(&ddsMagic, sizeof(ddsMagic), 1, dds);
			fwrite(&ddsHeader, sizeof(ddsHeader), 1, dds);
			fwrite(image, sizeof(u16), w*h, dds);
			fclose(dds);

			if (bAutoOpen)
			{
				system(path);
			}

			return true;
		}
		else
		{
			progress.End("failed");
		}
	}

	return false;
}

bool SaveImageRGBDDS(const char* path, const Vec3* image, int w, int h, bool bAutoOpen)
{
	if (strstr(path, ".dds"))
	{
		if (0) // TODO
		{
			if (bAutoOpen)
			{
				system(path);
			}
		}
	}

	return false;
}

bool SaveImageRGBADDS(const char* path, const Vec4* image, int w, int h, bool bAutoOpen)
{
	if (strstr(path, ".dds"))
	{
		ProgressDisplayAuto progress("saving %dx%d image \"%s\"", w, h, path);
		FILE* dds = fopen(path, "wb");

		if (dds)
		{
			const u32 ddsMagic = MAKE_MAGIC_NUMBER('D','D','S',' ');
			DDSURFACEDESC2 ddsHeader;
			memset(&ddsHeader, 0, sizeof(ddsHeader));

			ddsHeader.dwSize = sizeof(ddsHeader);
			ddsHeader.dwFlags = 0x00001007; // DDSD_CAPS | DDSD_HEIGHT | DDSD_WIDTH | DDSD_PIXELFORMAT
			ddsHeader.dwWidth = w;
			ddsHeader.dwHeight = h;
			ddsHeader.ddpfPixelFormat.dwSize = sizeof(ddsHeader.ddpfPixelFormat);
			ddsHeader.ddpfPixelFormat.dwFlags = DDPF_FOURCC;
			ddsHeader.ddpfPixelFormat.dwFourCC = DDS_D3DFMT_A32B32G32R32F;
			ddsHeader.ddsCaps.dwCaps1 = 0x00001002; // DDSCAPS_ALPHA | DDSCAPS_TEXTURE

			fwrite(&ddsMagic, sizeof(ddsMagic), 1, dds);
			fwrite(&ddsHeader, sizeof(ddsHeader), 1, dds);
			fwrite(image, sizeof(Vec4), w*h, dds);
			fclose(dds);

			if (bAutoOpen)
			{
				system(path);
			}

			return true;
		}
		else
		{
			progress.End("failed");
		}
	}

	return false;
}

bool SaveImagePixel32DDS(const char* path, const Pixel32* image, int w, int h, bool bAutoOpen)
{
	if (strstr(path, ".dds"))
	{
		if (0) // TODO
		{
			if (bAutoOpen)
			{
				system(path);
			}
		}
	}

	return false;
}

bool SaveImage(const char* path, const float* image, int w, int h, bool bAutoOpen)
{
	if (SaveImageDDS(path, image, w, h, bAutoOpen))
	{
		return true;
	}

	ProgressDisplayAuto progress("saving \"%s\"", path);

	FREE_IMAGE_FORMAT fif = FreeImage_GetFIFFromFilename(path);

	if (fif == FIF_UNKNOWN)
	{
		return false; // unknown filetype
	}

	if (!FreeImage_FIFSupportsWriting(fif))
	{
		return false; // cannot save to this filetype
	}

	FIBITMAP *dib = FreeImage_Allocate(w, h, 32);

	if (dib == NULL)
	{
		return false; // failed to allocate DIB
	}

	Pixel32* pixels = reinterpret_cast<Pixel32*>(FreeImage_GetBits(dib));

	for (int j = 0; j < h; j++)
	{
		for (int i = 0; i < w; i++)
		{
			Pixel32& c = pixels[i + (h - j - 1)*w];

			c.r = (u32)Clamp<float>(0.5f + 255.0f*image[i + j*w], 0.0f, 255.0f);
			c.g = c.r;
			c.b = c.r;
			c.a = 255;
		}
	}

	const bool success = FreeImage_Save(fif, dib, path) != 0;

	FreeImage_Unload(dib);

	if (success && bAutoOpen)
	{
		system(path);
	}

	return success;
}

bool SaveImage(const char* path, const u8* image, int w, int h, bool bAutoOpen)
{
	if (SaveImageDDS(path, image, w, h, bAutoOpen))
	{
		return true;
	}

	ProgressDisplayAuto progress("saving \"%s\"", path);

	FREE_IMAGE_FORMAT fif = FreeImage_GetFIFFromFilename(path);

	if (fif == FIF_UNKNOWN)
	{
		return false; // unknown filetype
	}

	if (!FreeImage_FIFSupportsWriting(fif))
	{
		return false; // cannot save to this filetype
	}

	FIBITMAP *dib = FreeImage_Allocate(w, h, 32);

	if (dib == NULL)
	{
		return false; // failed to allocate DIB
	}

	Pixel32* pixels = reinterpret_cast<Pixel32*>(FreeImage_GetBits(dib));

	for (int j = 0; j < h; j++)
	{
		for (int i = 0; i < w; i++)
		{
			Pixel32& c = pixels[i + (h - j - 1)*w];

			c.r = image[i + j*w];
			c.g = c.r;
			c.b = c.r;
			c.a = 255;
		}
	}

	const bool success = FreeImage_Save(fif, dib, path) != 0;

	FreeImage_Unload(dib);

	if (success && bAutoOpen)
	{
		system(path);
	}

	return success;
}

bool SaveImage(const char* path, const u16* image, int w, int h, bool bAutoOpen)
{
	if (SaveImageDDS(path, image, w, h, bAutoOpen))
	{
		return true;
	}

	ProgressDisplayAuto progress("saving \"%s\"", path);

	FREE_IMAGE_FORMAT fif = FreeImage_GetFIFFromFilename(path);

	if (fif == FIF_UNKNOWN)
	{
		return false; // unknown filetype
	}

	if (!FreeImage_FIFSupportsWriting(fif))
	{
		return false; // cannot save to this filetype
	}

	FIBITMAP *dib = FreeImage_Allocate(w, h, 32);

	if (dib == NULL)
	{
		return false; // failed to allocate DIB
	}

	Pixel32* pixels = reinterpret_cast<Pixel32*>(FreeImage_GetBits(dib));

	for (int j = 0; j < h; j++)
	{
		for (int i = 0; i < w; i++)
		{
			Pixel32& c = pixels[i + (h - j - 1)*w];

			c.r = image[i + j*w]>>8;
			c.g = c.r;
			c.b = c.r;
			c.a = 255;
		}
	}

	const bool success = FreeImage_Save(fif, dib, path) != 0;

	FreeImage_Unload(dib);

	if (success && bAutoOpen)
	{
		system(path);
	}

	return success;
}

bool SaveImageRGB(const char* path, const Vec3* image, int w, int h, bool bAutoOpen)
{
	if (SaveImageRGBDDS(path, image, w, h, bAutoOpen))
	{
		return true;
	}

	ProgressDisplayAuto progress("saving \"%s\"", path);

	FREE_IMAGE_FORMAT fif = FreeImage_GetFIFFromFilename(path);

	if (fif == FIF_UNKNOWN)
	{
		return false; // unknown filetype
	}

	if (!FreeImage_FIFSupportsWriting(fif))
	{
		return false; // cannot save to this filetype
	}

	FIBITMAP *dib = FreeImage_Allocate(w, h, 32);

	if (dib == NULL)
	{
		return false; // failed to allocate DIB
	}

	Pixel32* pixels = reinterpret_cast<Pixel32*>(FreeImage_GetBits(dib));

	for (int j = 0; j < h; j++)
	{
		for (int i = 0; i < w; i++)
		{
			Pixel32& c = pixels[i + (h - j - 1)*w];

			c.r = (u32)Clamp<float>(0.5f + 255.0f*image[i + j*w].x, 0.0f, 255.0f);
			c.g = (u32)Clamp<float>(0.5f + 255.0f*image[i + j*w].y, 0.0f, 255.0f);
			c.b = (u32)Clamp<float>(0.5f + 255.0f*image[i + j*w].z, 0.0f, 255.0f);
			c.a = 255;
		}
	}

	const bool success = FreeImage_Save(fif, dib, path) != 0;

	FreeImage_Unload(dib);

	if (success && bAutoOpen)
	{
		system(path);
	}

	return success;
}

bool SaveImagePixel32(const char* path, const Pixel32* image, int w, int h, bool bAutoOpen)
{
	if (SaveImagePixel32DDS(path, image, w, h, bAutoOpen))
	{
		return true;
	}

	ProgressDisplayAuto progress("saving \"%s\"", path);

	FREE_IMAGE_FORMAT fif = FreeImage_GetFIFFromFilename(path);

	if (fif == FIF_UNKNOWN)
	{
		return false; // unknown filetype
	}

	if (!FreeImage_FIFSupportsWriting(fif))
	{
		return false; // cannot save to this filetype
	}

	FIBITMAP *dib = FreeImage_Allocate(w, h, 32);

	if (dib == NULL)
	{
		return false; // failed to allocate DIB
	}

	Pixel32* pixels = reinterpret_cast<Pixel32*>(FreeImage_GetBits(dib));

	for (int j = 0; j < h; j++)
	{
		for (int i = 0; i < w; i++)
		{
			pixels[i + (h - j - 1)*w] = image[i + j*w];
		}
	}

	const bool success = FreeImage_Save(fif, dib, path) != 0;

	FreeImage_Unload(dib);

	if (success && bAutoOpen)
	{
		system(path);
	}

	return success;
}

// given an array of w floats in 0-1 range, build an antialiased "graph" image of dimensions (w,h)
float* Graph(float* dst, const float* src, int w, int h)
{
	if (h == 0)
	{
		h = w/2;
	}

	for (int x = 0; x < w; x++)
	{
		float v0 = Clamp<float>(1.0f - src[Max<int>(0, x - 1)], 0.0f, 1.0f - 0.00001f)*(float)h; // [0..h]
		float v1 = Clamp<float>(1.0f - src[Min<int>(x, w - 1)], 0.0f, 1.0f - 0.00001f)*(float)h; // [0..h]
		float h0 = Min<float>(v0, v1);
		float h1 = Max<float>(v0, v1);
		float h0_floor = floorf(h0);

		int ymin = Clamp<int>((int)floorf(h0), 0, h - 1);
		int ymax = Clamp<int>((int)floorf(h1), 0, h - 1) + 1;

		float* p = &dst[x]; // pointer to column data (dst)

		for (int y = 0; y < ymin; y++) // over
		{
			*p = 1.0f, p += w;
		}

		if (ymax - ymin == 1)
		{
			const float coverage = (h1 + h0)/2.0f - h0_floor;

			*p = coverage, p += w;
		}
		else
		{
			float y0 = h0_floor + 0.0f;
			float y1 = h0_floor + 1.0f;
			float dx = 1.0f/(h1 - h0);
			float x0 = (y0 - h0)*dx;
			float x1 = (y1 - h0)*dx;

			for (int y = ymin; y < ymax; y++)
			{
				// compute coverage for pixel (x,y) - "bounds" of pixel is [0..1],[y..y+1]
				// intersect with line from (0,h0) to (1,h1)

				float coverage = 1.0f - (x1 + x0)/2.0f;

				coverage -= Max<float>(0.0f, -x0       )*(h0 - y0)/2.0f;
				coverage -= Max<float>(0.0f,  x1 - 1.0f)*(h1 - y1)/2.0f;

				*p = coverage, p += w;

				y0 = y1; y1 += 1.0f;
				x0 = x1; x1 += dx;
			}
		}

		for (int y = ymax; y < h; y++) // under
		{
			*p = 0.0f, p += w;
		}
	}

	return dst;
}
