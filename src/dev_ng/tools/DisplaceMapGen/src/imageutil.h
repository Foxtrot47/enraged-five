// ===========
// imageutil.h
// ===========

#ifndef _IMAGEUTIL_H_
#define _IMAGEUTIL_H_

#include "common.h"

class Vec3;
class Vec4;
class Pixel32;

bool GetImageDimensionsDDS(const char* path, int& w, int& h);
float* LoadImageDDS(const char* path, int& w, int& h);
float* LoadImageDDSSpan(const char* path, int w, int y0, int h);
u8* LoadImageU8DDS(const char* path, int& w, int& h);
u16* LoadImageU16DDS(const char* path, int& w, int& h);
Pixel32* LoadImagePixel32DDS(const char* path, int& w, int& h);
Vec3* LoadImageRGBDDS(const char* path, int& w, int& h);
float* LoadImage(const char* path, int& w, int& h);
u8* LoadImageU8(const char* path, int& w, int& h);
Vec3* LoadImageRGB(const char* path, int& w, int& h);
Pixel32* LoadImagePixel32(const char* path, int& w, int& h);

bool SaveImageDDS(const char* path, const float* image, int w, int h, bool bAutoOpen);
bool SaveImageDDS(const char* path, const u8* image, int w, int h, bool bAutoOpen);
bool SaveImageDDS(const char* path, const u16* image, int w, int h, bool bAutoOpen);
bool SaveImageRGBDDS(const char* path, const Vec3* image, int w, int h, bool bAutoOpen);
bool SaveImageRGBADDS(const char* path, const Vec4* image, int w, int h, bool bAutoOpen);
bool SaveImagePixel32DDS(const char* path, const Pixel32* image, int w, int h, bool bAutoOpen);
bool SaveImage(const char* path, const float* image, int w, int h, bool bAutoOpen);
bool SaveImage(const char* path, const u8* image, int w, int h, bool bAutoOpen);
bool SaveImage(const char* path, const u16* image, int w, int h, bool bAutoOpen);
bool SaveImageRGB(const char* path, const Vec3* image, int w, int h, bool bAutoOpen);
bool SaveImagePixel32(const char* path, const Pixel32* image, int w, int h, bool bAutoOpen);

float* Graph(float* dst, const float* src, int w, int h);

#endif // _IMAGEUTIL_H_
