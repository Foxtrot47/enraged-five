#ifndef _IMAGE_H_
#define _IMAGE_H_

#include "common.h"
#include "mathutil.h"

class Image
{
public:
	Image()
		:	mp_Image(NULL)
		,	m_Width(0)
		,	m_Height(0)
	{
	}

	~Image()
	{
		FreeImage_Unload(mp_Image);
	}

	u32 GetWidth() const { return m_Width; } 
	u32 GetHeight() const { return m_Height; } 

	bool IsValidFormat() const { return mp_FileFormat != FIF_UNKNOWN; }

	bool Load(const char* filename)
	{
		FREE_IMAGE_FORMAT mp_FileFormat = FreeImage_GetFIFFromFilename(filename);

		if(IsValidFormat())
		{
			mp_Image = FreeImage_Load(mp_FileFormat, filename);
		}

		bool success = mp_Image != NULL;

		if(success)
		{
			m_Width = FreeImage_GetWidth(mp_Image);
			m_Height = FreeImage_GetHeight(mp_Image);
		}

		return success;
	}

	bool Save(const char* filename)
	{
		return FreeImage_Save(FreeImage_GetFIFFromFilename(filename), mp_Image, filename) != 0;
	}

	Vec3 GetPixel(u32 x, u32 y) const
	{
		RGBQUAD pixelColour;

		//note free image origin is at the bottom left, so the y is adjusted to start at the top left
		FreeImage_GetPixelColor(mp_Image, x, (m_Height - 1) - y, &pixelColour);

		return Vec3(pixelColour.rgbRed * (1.0f / 255.0f), pixelColour.rgbGreen * (1.0f / 255.0f), pixelColour.rgbBlue * (1.0f / 255.0f));
	}

	void SetPixel(u32 x, u32 y, Vec3 color)
	{
		RGBQUAD pixelColour;

		pixelColour.rgbRed =  static_cast<u8>(Clamp(color.x, 0.0f, 1.0f) * 255);
		pixelColour.rgbGreen =  static_cast<u8>(Clamp(color.y, 0.0f, 1.0f) * 255);
		pixelColour.rgbBlue =  static_cast<u8>(Clamp(color.z, 0.0f, 1.0f) * 255);
		pixelColour.rgbReserved = 255; //no alpha

		//note free image origin is at the bottom left, so the y is adjusted to start at the top left
		FreeImage_SetPixelColor(mp_Image, x, (m_Height - 1) - y, &pixelColour);
	}

	void SetPixel(u32 x, u32 y, float value)
	{
		SetPixel(x, y, Vec3(value, value, value));
	}

private:
	FIBITMAP* mp_Image;
	FREE_IMAGE_FORMAT mp_FileFormat;

	u32	m_Width;
	u32 m_Height;
};

#endif