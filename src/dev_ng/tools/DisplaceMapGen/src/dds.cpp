#include "dds.h"

static bool CompareDDSFormatMask(const DDPIXELFORMAT& ddpf, u32 flags, u32 bitCount, u32 rMask, u32 gMask, u32 bMask, u32 aMask)
{
	return
	(
		ddpf.dwFlags       == flags    &&
		ddpf.dwRGBBitCount == bitCount &&
		ddpf.dwRBitMask    == rMask    &&
		ddpf.dwGBitMask    == gMask    &&
		ddpf.dwBBitMask    == bMask    &&
		ddpf.dwABitMask    == aMask
	);
}

DDS_D3DFORMAT GetDDSFormat(const DDPIXELFORMAT& ddpf)
{
	if (ddpf.dwFlags & DDPF_FOURCC)
	{
		return (DDS_D3DFORMAT)ddpf.dwFourCC;
	}
	else if (CompareDDSFormatMask(ddpf, DDPF_RGB       | DDPF_ALPHAPIXELS, 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000)) { return DDS_D3DFMT_A8R8G8B8   ; }
	else if (CompareDDSFormatMask(ddpf, DDPF_RGB       | DDPF_ALPHAPIXELS, 32, 0x000000FF, 0x0000FF00, 0x00FF0000, 0xFF000000)) { return DDS_D3DFMT_A8B8G8R8   ; }
	else if (CompareDDSFormatMask(ddpf, DDPF_ALPHA                       ,  8, 0x00000000, 0x00000000, 0x00000000, 0x000000FF)) { return DDS_D3DFMT_A8         ; }
	else if (CompareDDSFormatMask(ddpf, DDPF_LUMINANCE                   ,  8, 0x000000FF, 0x00000000, 0x00000000, 0x00000000)) { return DDS_D3DFMT_L8         ; }
	else if (CompareDDSFormatMask(ddpf, DDPF_LUMINANCE | DDPF_ALPHAPIXELS, 16, 0x000000FF, 0x00000000, 0x00000000, 0x0000FF00)) { return DDS_D3DFMT_A8L8       ; }
	else if (CompareDDSFormatMask(ddpf, DDPF_RGB       | DDPF_ALPHAPIXELS, 16, 0x00000F00, 0x000000F0, 0x0000000F, 0x0000F000)) { return DDS_D3DFMT_A4R4G4B4   ; }
	else if (CompareDDSFormatMask(ddpf, DDPF_RGB       | DDPF_ALPHAPIXELS, 16, 0x00007C00, 0x000003E0, 0x0000001F, 0x00008000)) { return DDS_D3DFMT_A1R5G5B5   ; }
	else if (CompareDDSFormatMask(ddpf, DDPF_RGB                         , 16, 0x0000F800, 0x000007E0, 0x0000001F, 0x00000000)) { return DDS_D3DFMT_R5G6B5     ; }
	else if (CompareDDSFormatMask(ddpf, DDPF_RGB                         ,  8, 0x000000E0, 0x0000001C, 0x00000003, 0x0000FF00)) { return DDS_D3DFMT_R3G3B2     ; }
	else if (CompareDDSFormatMask(ddpf, DDPF_RGB       | DDPF_ALPHAPIXELS, 16, 0x000000E0, 0x0000001C, 0x00000003, 0x0000FF00)) { return DDS_D3DFMT_A8R3G3B2   ; }
	else if (CompareDDSFormatMask(ddpf, DDPF_LUMINANCE | DDPF_ALPHAPIXELS,  8, 0x0000000F, 0x00000000, 0x00000000, 0x000000F0)) { return DDS_D3DFMT_A4L4       ; }
	else if (CompareDDSFormatMask(ddpf, DDPF_RGB       | DDPF_ALPHAPIXELS, 32, 0x3FF00000, 0x000FFC00, 0x000003FF, 0xC0000000)) { return DDS_D3DFMT_A2R10G10B10; }
	else if (CompareDDSFormatMask(ddpf, DDPF_RGB       | DDPF_ALPHAPIXELS, 32, 0x000003FF, 0x000FFC00, 0x3FF00000, 0xC0000000)) { return DDS_D3DFMT_A2B10G10R10; }
	else if (CompareDDSFormatMask(ddpf, DDPF_RGB                         , 32, 0x0000FFFF, 0xFFFF0000, 0x00000000, 0x00000000)) { return DDS_D3DFMT_G16R16     ; }
	else if (CompareDDSFormatMask(ddpf, DDPF_LUMINANCE                   , 16, 0x0000FFFF, 0x00000000, 0x00000000, 0x00000000)) { return DDS_D3DFMT_L16        ; }
	// support XRGB/XBGR formats too ..
	else if (CompareDDSFormatMask(ddpf, DDPF_RGB                         , 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0x00000000)) { return DDS_D3DFMT_A8R8G8B8   ; } // X8R8G8B8
	else if (CompareDDSFormatMask(ddpf, DDPF_RGB                         , 32, 0x000000FF, 0x0000FF00, 0x00FF0000, 0x00000000)) { return DDS_D3DFMT_A8B8G8R8   ; } // X8B8G8R8
	else if (CompareDDSFormatMask(ddpf, DDPF_RGB                         , 16, 0x00000F00, 0x000000F0, 0x0000000F, 0x00000000)) { return DDS_D3DFMT_A4R4G4B4   ; } // X4R4G4B4
	else if (CompareDDSFormatMask(ddpf, DDPF_RGB                         , 16, 0x00007C00, 0x000003E0, 0x0000001F, 0x00000000)) { return DDS_D3DFMT_A1R5G5B5   ; } // X1R5G5B5

	return DDS_D3DFMT_UNKNOWN;
}

const char* GetD3DFormatStr(DDS_D3DFORMAT fmt)
{
	switch ((int)fmt)
	{
#define DEF_DDS_D3DFMT_CASE(f) case f: return #f; break
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_UNKNOWN            );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_R8G8B8             );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_A8R8G8B8           );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_X8R8G8B8           );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_R5G6B5             );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_X1R5G5B5           );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_A1R5G5B5           );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_A4R4G4B4           );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_R3G3B2             );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_A8                 );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_A8R3G3B2           );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_X4R4G4B4           );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_A2B10G10R10        );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_A8B8G8R8           );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_X8B8G8R8           );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_G16R16             );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_A2R10G10B10        );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_A16B16G16R16       );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_A8P8               );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_P8                 );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_L8                 );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_A8L8               );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_A4L4               );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_V8U8               );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_L6V5U5             );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_X8L8V8U8           );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_Q8W8V8U8           );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_V16U16             );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_A2W10V10U10        );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_UYVY               );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_R8G8_B8G8          );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_YUY2               );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_G8R8_G8B8          );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_DXT1               );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_DXT2               );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_DXT3               );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_DXT4               );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_DXT5               );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_D16_LOCKABLE       );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_D32                );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_D15S1              );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_D24S8              );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_D24X8              );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_D24X4S4            );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_D16                );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_D32F_LOCKABLE      );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_D24FS8             );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_D32_LOCKABLE       );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_S8_LOCKABLE        );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_L16                );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_VERTEXDATA         );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_INDEX16            );
		DEF_DDS_D3DFMT_CASE(DDS_D3DFMT_INDEX32            );
#undef DEF_DDS_D3DFMT_CASE
	}

	return "DDS_D3DFMT_";
}