// ============
// fileutil.cpp
// ============

#include "fileutil.h"

#include "mathutil.h"

bool FileExists(const char* path)
{
	FILE* fp = fopen(path, "rb");

	if (fp)
	{
		fclose(fp);
		return true;
	}

	return false;
}

bool FileExistsAndIsNotZeroBytes(const char* path)
{
	FILE* fp = fopen(path, "rb");

	if (fp)
	{
		fseek(fp, 0, SEEK_END);
		const int size = ftell(fp);
		fclose(fp);
		return size > 0;
	}

	return false;
}

bool ReadFileLine(char* line, int lineMax, FILE* fp)
{
	if (fgets(line, lineMax, fp))
	{
		char temp[4096] = "";
		strcpy(temp, line);
		const char* start = temp;
		while (*start == ' ' || *start == '\t')
		{
			start++; // skip leading whitespace
		}
		strcpy(line, start);
		if (strrchr(line, '\r')) { strrchr(line, '\r')[0] = '\0'; } // remove trailing newline
		if (strrchr(line, '\n')) { strrchr(line, '\n')[0] = '\0'; }
		return true;
	}

	return false;
}

bool ReadNextFileLine(char* line, int lineMax, FILE* fp)
{
	while (ReadFileLine(line, lineMax, fp))
	{
		if (line[0] != '#')
		{
			return true;
		}
	}

	return false;
}

template <> int ParseFileLine<int>(const char* var, FILE* fp)
{
	char line[1024] = "";

	if (ReadNextFileLine(line, sizeof(line), fp))
	{
		if (memcmp(line, var, strlen(var)) == 0 && line[strlen(var)] == '=')
		{
			const char* s = line + strlen(var) + 1;
			return atoi(s);
		}
	}

	assert(0);
	return 0;
}

template <> float ParseFileLine<float>(const char* var, FILE* fp)
{
	char line[1024] = "";

	if (ReadNextFileLine(line, sizeof(line), fp))
	{
		if (memcmp(line, var, strlen(var)) == 0 && line[strlen(var)] == '=')
		{
			const char* s = line + strlen(var) + 1;
			return (float)atof(s);
		}
	}

	assert(0);
	return 0;
}

template <> Vec3 ParseFileLine<Vec3>(const char* var, FILE* fp)
{
	char line[1024] = "";

	if (ReadNextFileLine(line, sizeof(line), fp))
	{
		if (memcmp(line, var, strlen(var)) == 0 && line[strlen(var)] == '=')
		{
			const char* s = line + strlen(var) + 1;
			const float x = (float)atof(s);
			s = strchr(s, ',');

			if (s)
			{
				s++;
				const float y = (float)atof(s);
				s = strchr(s, ',');

				if (s)
				{
					s++;
					const float z = (float)atof(s);
					return Vec3(x, y, z);
				}
			}
		}
	}

	assert(0);
	return 0;
}

template <> Vec4 ParseFileLine<Vec4>(const char* var, FILE* fp)
{
	char line[1024] = "";

	if (ReadNextFileLine(line, sizeof(line), fp))
	{
		if (memcmp(line, var, strlen(var)) == 0 && line[strlen(var)] == '=')
		{
			const char* s = line + strlen(var) + 1;
			const float x = (float)atof(s);
			s = strchr(s, ',');

			if (s)
			{
				s++;
				const float y = (float)atof(s);
				s = strchr(s, ',');

				if (s)
				{
					s++;
					const float z = (float)atof(s);
					s = strchr(s, ',');

					if (s)
					{
						s++;
						const float w = (float)atof(s);
						return Vec4(x, y, z, w);
					}
				}
			}
		}
	}

	assert(0);
	return 0;
}

void ByteSwapData(void* data, int dataSize, int swapSize)
{
	u8* data0 = (u8*)data;
	u8* data1 = data0 + dataSize;

	if (swapSize == 2)
	{
		for (; data0 < data1; data0 += 2)
		{
			const u8 swap[2] = {data0[0], data0[1]};

			data0[0] = swap[1];
			data0[1] = swap[0];
		}
	}
	else if (swapSize == 4)
	{
		for (; data0 < data1; data0 += 4)
		{
			const u8 swap[4] = {data0[0], data0[1], data0[2], data0[3]};

			data0[0] = swap[3];
			data0[1] = swap[2];
			data0[2] = swap[1];
			data0[3] = swap[0];
		}
	}
}
