#ifndef _DISPLACMENT_GENERATOR_H_
#define _DISPLACMENT_GENERATOR_H_

#include <vector>

#include "common.h"
#include "mathutil.h"

class Image;

class DisplacementGenerator
{
public:
	virtual ~DisplacementGenerator() {}
	void Setup(const Image* pSourceImage, Image* pDestImage);
	virtual float Convert() = 0; //returns value is scale factor which can be used a base for how much displacement is applied in game

protected:
	const Image* GetSource() { return mp_SourceImage; }
	Image* GetDest() { return mp_DestImage; }

	void ResetProgress() { m_LastPercentagePrinted = 0; }
	void SetProgress(float percentage);

private:
	const Image* mp_SourceImage;
	Image* mp_DestImage;

	u32 m_LastPercentagePrinted;
};

//////////////////////////////////////////////////////////////////////////

class NormalToDisplacement : public DisplacementGenerator
{
public:
	NormalToDisplacement()
	: m_NumberOfRays(32)
	, m_LengthOfRays(32)
	, m_flipX(false)
	, m_flipY(false)
	{}

	void SetNumberOfRays(u32 numberOfRays) { m_NumberOfRays = numberOfRays; }
	void SetLenghtOfRays(u32 lengthOfRay) { m_LengthOfRays = lengthOfRay; }
	void SetFlipX(bool flippedX) { m_flipX = flippedX; }
	void SetFlipY(bool flippedY) { m_flipY = flippedY; }

	float Convert();

private:
	vector< vector<Vec2> > m_DepthDifferenceMap;
	vector< vector<float> > m_DisplacementScratch;

	u32 m_NumberOfRays;
	u32 m_LengthOfRays;

	bool m_flipX;
	bool m_flipY;
};

//////////////////////////////////////////////////////////////////////////
class ColorToDisplacement : public DisplacementGenerator
{
public:
	ColorToDisplacement()
	: m_LowColor(Vec3(0.0f, 0.0f, 0.0f))
	, m_HighColor(Vec3(1.0f, 1.0f, 1.0f))
	{}

	void SetLowColor(Vec3 &lowColor) { m_LowColor = lowColor; }
	void SetHighColor(Vec3& highColor) { m_HighColor = highColor; }
	float Convert();

private:
	Vec3 m_LowColor;
	Vec3 m_HighColor;
};

#endif