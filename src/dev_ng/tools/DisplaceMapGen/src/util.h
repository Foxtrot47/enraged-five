#ifndef _UTIL_H_
#define _UTIL_H_

#include "mathutil.h"

void OnError(bool isFatal, char* fmt, ...);
Vec3 GetColorFromString(std::string string, char seperator = ',');
std::string GenerateOutputName(std::string inputName);

#endif