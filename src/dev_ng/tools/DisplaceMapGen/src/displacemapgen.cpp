#include "common.h"
#include "imageutil.h"

#include "util.h"
#include "image.h"
#include "../external/TinyXML/tinyxml2.h"

#include "DisplacementGenerator.h"

using namespace tinyxml2;

static const string s_HelpText = 
string("-help : displays this help message\n\n") +
string("-in : path to source image\n\n") +
string("-out : path for the generated displacement ma.\n\n") +
string("-normal : specifies the source is a normal map\n\n") + 
string("  -numrays : specifies the number of rays to use when calculating\n   the displacement using the -normal switch above\n   Min 32, max 256, default is 32, more rays might improve quality but\n   increases computation time\n\n") +
string("  -raylen  : specifies the length of the rays to use when calculating\n   the displacement using the -normal switch above\n   Min 32, max 256, default is 32, longer rays might improve quality but\n   increases computation time\n\n") +
string("  -flipx  : flips the x part of the normal\n\n") +
string("  -fiipy  : flips the y part of the normal\n\n") +
string("-color : specifies the source is a color map\n\n") +
string("  -lowcol : specifies the color you want to represent the\n   lowest displacement, using -color switch above.\n   R G B ranged 0 to 255, this defaults to black\n\n") +
string("  -hicol : specifies the color you want to represent the\n   height displacement, using -color switch above.\n   R G B ranged 0 to 255, this defaults to white\n");

static NormalToDisplacement g_NormToDisplaceConvetor;
static ColorToDisplacement g_ColorToDisplacementConvertor;

bool ConvertFile(DisplacementGenerator* convertor, const char* inputFile, const char* outputFile, bool fatalErrors)
{
	Image sourceImage, destImage;
	
	if(sourceImage.Load(inputFile) && destImage.Load(inputFile))
	{
		printf("\nInput Filename: %s\nOutput Filename: %s\n\n", inputFile, outputFile);

		u32 startTime = GetTickCount();

		convertor->Setup(&sourceImage, &destImage); 
		float estimatedDisplacementScale = convertor->Convert();

		u32 endTime = GetTickCount();

		printf("\nGeneration took %ims, Estimated displacement scale factor is %f\n", endTime - startTime, estimatedDisplacementScale);

		if(destImage.Save(outputFile) == false)
		{
			OnError(fatalErrors, "Could not open file %s for saving", outputFile);
		}
		else
		{
			return true;
		}
	}
	else
	{
		if(sourceImage.IsValidFormat())
		{
			OnError(fatalErrors, "Could not open file as a source for generation %s", inputFile);
		}
		else
		{
			OnError(fatalErrors, "Unsupported file type %s", inputFile);
		}
	}

	return false;
}

void ParseXmlAndConvert(const char* filePath)
{
	tinyxml2::XMLDocument xmlDoc;

	int result = xmlDoc.LoadFile(filePath);

	switch (result)
	{
		case XML_ERROR_EMPTY_DOCUMENT: 
			OnError(true, "XML file %s is empty", filePath); 
			break;
		case XML_ERROR_FILE_READ_ERROR: 
			OnError(true, "Couldn't not open XML file %s", filePath); 
		break;
		case 0:
			printf("XML file %s loaded correctly\n", filePath);
		break;
		default:
			OnError(true, "Unknown error opening XML file %s", filePath); 
		break;
	}

	XMLElement* root = xmlDoc.FirstChildElement("images");

	if(root == NULL)
	{
		OnError(true, "Missing root tag <images> in XML file");
	}

	const XMLElement* currentChild = root->FirstChildElement();

	while(currentChild != NULL)
	{
		if(currentChild->FindAttribute("in") && currentChild->FindAttribute("tech"))
		{
			std::string inputFile = currentChild->Attribute("in");
			std::string outputFile;

			if(currentChild->FindAttribute("out"))
			{
				outputFile = currentChild->Attribute("out");
			}
			else
			{
				outputFile = GenerateOutputName(inputFile);

				if(outputFile == "")
				{
					OnError(false, "No output file specified, failed to generate one from the input, skipping conversion");
					continue;
				}
			}

			std::string technique = currentChild->Attribute("tech");

			DisplacementGenerator* currentConvertor = NULL;

			if(technique == "normal")
			{
				if(currentChild->FindAttribute("raylen"))
				{
					g_NormToDisplaceConvetor.SetLenghtOfRays(currentChild->IntAttribute("raylen"));
				}

				if(currentChild->FindAttribute("numrays"))
				{
					g_NormToDisplaceConvetor.SetNumberOfRays(currentChild->IntAttribute("numrays"));
				}

				bool flipX = currentChild->FindAttribute("flipx") && XMLUtil::StringEqual(currentChild->Attribute("flipx"), "true");
				bool flipY = currentChild->FindAttribute("flipy") && XMLUtil::StringEqual(currentChild->Attribute("flipy"), "true");
				
				g_NormToDisplaceConvetor.SetFlipX(flipX);
				g_NormToDisplaceConvetor.SetFlipY(flipY);

				currentConvertor = &g_NormToDisplaceConvetor;
			}
			else if(technique == "color")
			{

				if(currentChild->FindAttribute("lowcol"))
				{
					g_ColorToDisplacementConvertor.SetLowColor(GetColorFromString(currentChild->Attribute("lowcol")));
				}

				if(currentChild->FindAttribute("hicol"))
				{
					g_ColorToDisplacementConvertor.SetLowColor(GetColorFromString(currentChild->Attribute("hicol")));
				}

				currentConvertor = &g_ColorToDisplacementConvertor;
			}
			else
			{
				OnError(false, "Unrecognized technique %s in xml, skipping conversion", technique.c_str());
				continue;
			}

			ConvertFile(currentConvertor, inputFile.c_str(), outputFile.c_str(), false);
		}
		else
		{
			OnError(false, "Needs both an \"in\" attribute and \"tech\" attribute, skipping conversion");
		}

		currentChild = currentChild->NextSiblingElement();
	}
}
 
int main(int argumentCount, const char* arguments[])
{
	printf("DisplaceMapGen v1.1\n");

	if(argumentCount == 1)
	{
		printf(s_HelpText.c_str());
		exit(-1);
	}
	else
	{
		string inputFile;
		string outputFile;

		DisplacementGenerator* convertor = NULL; 

		int argumentIndex = 1; //first parameter is always the program name
		while(argumentIndex < argumentCount)
		{
			string currentArg = arguments[argumentIndex];

			if(currentArg == "-help")
			{
				printf(s_HelpText.c_str());
				exit(-1);
			}
			else if(currentArg == "-xml")
			{
				if(argumentCount != 3)
				{
					OnError(true, "-Xml must be the only command line");
				}

				if(argumentIndex + 1 < argumentCount)
				{
					ParseXmlAndConvert(arguments[++argumentIndex]);
				}
				else
				{
					OnError(true, "-xml requires a path to a source file");
				}

				return 0;
			}
			else if(currentArg == "-in")
			{
				if(argumentIndex + 1 < argumentCount)
				{
					inputFile = arguments[++argumentIndex];
				}
				else
				{
					OnError(true, "-in requires a path to load the source image");
				}
			}
			else if(currentArg == "-out")
			{
				if(argumentIndex + 1 < argumentCount)
				{
					outputFile = arguments[++argumentIndex];
				}
				else
				{
					OnError(true, "-out requires a path to save the displacement map to");
				}
			}
			else if(currentArg == "-normal")
			{
				convertor = &g_NormToDisplaceConvetor;
			}
			else if(currentArg == "-numrays")
			{
				if(argumentIndex + 1 < argumentCount)
				{
					u32 numberOfRays = atoi(arguments[++argumentIndex]);
					g_NormToDisplaceConvetor.SetNumberOfRays(Clamp(numberOfRays, (u32)32, (u32)256));
				}
				else
				{
					OnError(true, "-numrays requires a number for the amount of rays");
				}
			}
			else if(currentArg == "-raylen")
			{
				if(argumentIndex + 1 < argumentCount)
				{
					u32 rayLenght = atoi(arguments[++argumentIndex]);
					g_NormToDisplaceConvetor.SetLenghtOfRays(Clamp(rayLenght, (u32)32, (u32)256));
				}
				else
				{
					OnError(true, "-raylen requires a number for the length of rays");
				}
			}
			else if(currentArg == "-flipx")
			{
				g_NormToDisplaceConvetor.SetFlipX(true);
			}
			else if(currentArg == "-flipy")
			{
				g_NormToDisplaceConvetor.SetFlipY(true);
			}
			else if(currentArg == "-color")
			{
				convertor = &g_ColorToDisplacementConvertor;
			}
			else if(currentArg == "-lowcol" || currentArg == "-hicol")
			{
				if(argumentIndex + 3 < argumentCount)
				{
					Vec3 colour;

					colour.x = Clamp(static_cast<float>(atof(arguments[++argumentIndex])) / 255.0f, 0.0f, 1.0f);
					colour.y = Clamp(static_cast<float>(atof(arguments[++argumentIndex])) / 255.0f, 0.0f, 1.0f);
					colour.z = Clamp(static_cast<float>(atof(arguments[++argumentIndex])) / 255.0f, 0.0f, 1.0f);

					if(currentArg == "-lowcol")
					{
						g_ColorToDisplacementConvertor.SetLowColor(colour);
					}
					else
					{
						g_ColorToDisplacementConvertor.SetHighColor(colour);
					}
				}
				else
				{
					OnError(true, "-lowcol or -hicol requires 3 numbers representing, Red Green and Blue");

				}
			}
			else
			{
				OnError(true, "Unknown command: %s", currentArg.c_str());
			}

			++argumentIndex;
		}

		if(convertor == NULL)
		{
			OnError(true, "No generation technique supplied, use -normal or -color");
		}

		if(outputFile == "")
		{
			outputFile = GenerateOutputName(inputFile);

			if(outputFile == "")
			{
				OnError(true, "No output file specified, failed to generate one from the input");
			}
		}

		ConvertFile(convertor, inputFile.c_str(), outputFile.c_str(), true);
	}
	
	printf("Completed operation");
	return 0;
}