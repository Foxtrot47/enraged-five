// ==========
// fileutil.h
// ==========

#ifndef _FILEUTIL_H_
#define _FILEUTIL_H_

#include "common.h"

bool FileExists(const char* path);
bool FileExistsAndIsNotZeroBytes(const char* path);

bool ReadFileLine(char* line, int lineMax, FILE* fp);
bool ReadNextFileLine(char* line, int lineMax, FILE* fp);

template <typename T> T ParseFileLine(const char* var, FILE* fp);

void ByteSwapData(void* data, int dataSize, int swapSize);

template <typename T> static void ByteSwap(T& data)
{
	ByteSwapData(&data, sizeof(data), sizeof(data));
}

#endif // _FILEUTIL_H_
