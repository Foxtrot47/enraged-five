#ifndef _COMMON_H_
#define _COMMON_H_

#define _CRT_SECURE_NO_WARNINGS
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <assert.h>
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <algorithm>
#include <process.h> 

using namespace std;

// http://freeimage.sourceforge.net
#pragma comment(lib, "external/FreeImage3154/FreeImage.lib")
#include          "../external/FreeImage3154/FreeImage.h"

#ifdef LoadImage
#undef LoadImage // what?
#endif

typedef unsigned char          u8;
typedef char                   s8;
typedef unsigned short         u16;
typedef short                  s16;
typedef unsigned int           u32;
typedef int                    s32;
typedef unsigned long long int u64;
typedef long long int          s64;

template <typename T> __forceinline T Min(T a, T b) { return a <= b ? a : b; }
template <typename T> __forceinline T Max(T a, T b) { return a >= b ? a : b; }
template <typename T> __forceinline T Abs(T a) { return a >= T(0) ? a : -a; }
template <typename T> __forceinline T Clamp(T value, T valueMin, T valueMax) { return Min<T>(Max<T>(value, valueMin), valueMax); }

#define __COMMENT(x)

#define BIT(n) (1<<(n))
#define CLEAR_BIT(mask,index) mask[(index)/8] &= ~BIT((index)%8)
#define SET_BIT(mask,index) mask[(index)/8] |= BIT((index)%8)
#define TEST_BIT(mask,index) (mask[(index)/8] & BIT((index)%8))

#define NELEM(arr) (sizeof(arr)/sizeof((arr)[0]))

#define COMMON_PATH "x:/gta5/build/dev/common"
#define LEVEL_PATH  "x:/gta5/build/dev/common/data/levels/gta5"
#define TOOLS_PATH  "x:/gta5/src/dev/tools"

#endif // _COMMON_H_
