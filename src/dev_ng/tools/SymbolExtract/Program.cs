
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using System.Globalization;

using Dia2Lib;
using System.Runtime.InteropServices;

using System.Collections;
using System.Reflection;

namespace Dia2Lib
{
    [Guid("0CF4B60E-35B1-4c6c-BDD8-854B9C8E3857")]
    [InterfaceType(1)]
    public interface IDiaSectionContrib
    {
        IDiaSymbol compiland { get; }
        uint addressSection { get; }
        uint addressOffset { get; }
        uint relativeVirtualAddress { get; }
        ulong virtualAddress { get; }
        uint length { get; }
        bool notPaged { get; }
        bool code { get; }
        bool initializedData { get; }
        bool uninitializedData { get; }
        bool remove { get; }
        bool comdat { get; }
        bool discardable { get; }
        bool notCached { get; }
        bool share { get; }
        bool execute { get; }
        bool read { get; }
        bool write { get; }
        uint dataCrc { get; }
        uint relocationsCrc { get; }
        uint compilandId { get; }
        bool code16bit { get; }
    }

    [Guid("1994DEB2-2C82-4b1d-A57F-AFF424D54A68")]
    [InterfaceType(1)]
    public interface IDiaEnumSectionContribs
    {
        [DispId(1)]
        int count { get; }

        void Clone(out IDiaEnumSectionContribs ppenum);
        IDiaSectionContrib Item(uint index);
        void Next(uint celt, out IDiaSectionContrib rgelt, out uint pceltFetched);
        void Reset();
        void Skip(uint celt);
    }

    enum NameSearchOptions
    {
        nsNone = 0,
        nsfCaseSensitive = 0x1,
        nsfCaseInsensitive = 0x2,
        nsfFNameExt = 0x4,
        nsfRegularExpression = 0x8,
        nsfUndecoratedName = 0x10,
        nsCaseSensitive = nsfCaseSensitive,
        nsCaseInsensitive = nsfCaseInsensitive,
        nsFNameExt = (nsfCaseInsensitive | nsfFNameExt),
        nsRegularExpression = (nsfRegularExpression | nsfCaseSensitive),
        nsCaseInRegularExpression = (nsfRegularExpression | nsfCaseInsensitive)
    } ;

    enum DataKind
    {
        DataIsUnknown,
        DataIsLocal,
        DataIsStaticLocal,
        DataIsParam,
        DataIsObjectPtr,
        DataIsFileStatic,
        DataIsGlobal,
        DataIsMember,
        DataIsStaticMember,
        DataIsConstant
    }
}

namespace SymbolExtract
{
    class Symbol
    {
        public int size;
        public int count;
        public int rva;
        public string name;
        public string short_name;
        public string source_filename;
        public string section;
    };

    class SymbolExtract
    {
        private static string PathCanonicalize(string path)
        {
            if (path.Length == 0)
                return path;

            string[] dirs = path.Split("/\\".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            List<string> outDirs = new List<string>();
            int skipCount = 0;
            for (int i = dirs.Length - 1; i >= 0; --i)
            {
                string dir = dirs[i];
                if (dir == ".")
                {
                }
                else if (dir == "..")
                    ++skipCount;
                else if (skipCount > 0)
                    --skipCount;
                else
                    outDirs.Add(dir);
            }

            string outPath = "";
            if (path[0] == '\\' || path[0] == '/')
            {
                outPath = "\\";
            }

            for (int i = 0; i < skipCount; ++i)
            {
                outPath += "..\\";
            }

            for (int i = outDirs.Count - 1; i >= 0; --i)
            {
                outPath += outDirs[i];
                outPath += "\\";
            }

            if (outPath.Length > 1 && path[path.Length - 1] != '\\' && path[path.Length - 1] != '/')
            {
                outPath = outPath.Remove(outPath.Length - 1);
            }

            return outPath;
        }

        private static void ParseBsdSymbol(string line, out Symbol symbol)
        {
            symbol = null;

            int rva = 0;
            int size = 0;
            string name;
            string section = "";
            string sourceFilename = "";

            string[] tokens = line.Split((char[])null, 2, StringSplitOptions.RemoveEmptyEntries);
            if (tokens.Length < 2)
                return;

            if (tokens[0].Length > 1)
            {
                rva = Int32.Parse(tokens[0], NumberStyles.AllowHexSpecifier);
                tokens = tokens[1].Split((char[])null, 2, StringSplitOptions.RemoveEmptyEntries);
                if (tokens.Length < 2)
                    return;
            }

            if (tokens[0].Length > 1)
            {
                try
                {
                    size = Int32.Parse(tokens[0], NumberStyles.AllowHexSpecifier);
                }
                catch (System.Exception)
                {
                }
                tokens = tokens[1].Split((char[])null, 2, StringSplitOptions.RemoveEmptyEntries);
                if (tokens.Length < 2)
                    return;
            }

            section = tokens[0];
            tokens = tokens[1].Split("\t\r\n".ToCharArray(), 2, StringSplitOptions.RemoveEmptyEntries);
            if (tokens.Length < 1)
                return;

            name = tokens[0];
            if (tokens.Length > 1)
            {
                sourceFilename = tokens[1];
            }

            symbol = new Symbol();
            symbol.name = name;
            symbol.short_name = name;
            symbol.rva = rva;
            symbol.size = size;
            symbol.count = 1;
            symbol.section = section;
            symbol.source_filename = sourceFilename;
        }

        private static void ParseSysvSymbol(string line, out Symbol symbol)
        {
            symbol = null;

            string[] tokens = line.Split("|".ToCharArray(), 7);

            if (tokens.Length < 7)
                return;

            int rva = 0;
            int size = 0;
            string name;
            string section = "";
            string sourceFilename = "";

            name = tokens[0].Trim();

            if (tokens[1].Trim().Length > 0)
            {
                rva = Int32.Parse(tokens[1], NumberStyles.AllowHexSpecifier);
            }
            if (tokens[4].Trim().Length > 0)
            {
                try
                {
                    size = Int32.Parse(tokens[4], NumberStyles.AllowHexSpecifier);
                }
                catch (System.Exception)
                {
                }
            }
            tokens = tokens[6].Split("\t\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            if (tokens.Length > 0)
            {
                section = tokens[0].Trim();
            }
            if (tokens.Length > 1)
            {
                sourceFilename = tokens[1].Trim();
            }

            symbol = new Symbol();
            symbol.name = name;
            symbol.short_name = name;
            symbol.rva = rva;
            symbol.size = size;
            symbol.count = 1;
            symbol.section = section;
            symbol.source_filename = sourceFilename;
        }

        private static string FindSourceFileForRVA(IDiaSession session, uint rva, uint rvaLength)
        {
            IDiaEnumLineNumbers enumLineNumbers;
            session.findLinesByRVA(rva, rvaLength, out enumLineNumbers);
            if (enumLineNumbers != null)
            {
                for (; ; )
                {
                    uint numFetched = 1;
                    IDiaLineNumber lineNumber;
                    enumLineNumbers.Next(numFetched, out lineNumber, out numFetched);
                    if (lineNumber == null || numFetched < 1)
                        break;

                    IDiaSourceFile sourceFile = lineNumber.sourceFile;
                    if (sourceFile != null)
                    {
                        return sourceFile.fileName.ToLower();
                    }
                }
            }
            return "";
        }


        private static IDiaEnumSectionContribs GetEnumSectionContribs(IDiaSession session)
        {
            IDiaEnumTables tableEnum;
            session.getEnumTables(out tableEnum);

            for (; ; )
            {
                uint numFetched = 1;
                IDiaTable table = null;
                tableEnum.Next(numFetched, ref table, ref numFetched);
                if (table == null || numFetched < 1)
                    break;

                try
                {
                    IDiaEnumSectionContribs enumSectionContribs = (IDiaEnumSectionContribs)table;
                    if (enumSectionContribs != null)
                        return enumSectionContribs;
                }
                catch (Exception)
                {
                }
            }

            return null;
        }

        private enum SourceFileType
        {
            cpp,
            unknown,
            h
        };

        private static SourceFileType GetSourceFileType(string filename)
        {
            string ext = Path.GetExtension(filename).ToLower();
            if (String.Compare(ext, 0, ".c", 0, 2) == 0)
                return SourceFileType.cpp;
            if (String.Compare(ext, 0, ".h", 0, 2) == 0 ||
                ext == ".pch")
                return SourceFileType.h;
            return SourceFileType.unknown;

        }
        private static string FindBestSourceFileForCompiland(IDiaSession session, IDiaSymbol compiland, Dictionary<string, int> sourceFileUsage)
        {
            string bestSourceFileName = "";
            IDiaEnumSourceFiles enumSourceFiles;
            session.findFile(compiland, null, 0, out enumSourceFiles);
            if (enumSourceFiles != null)
            {
                int bestSourceFileCount = int.MaxValue;
                SourceFileType bestSourceFileType = SourceFileType.h;


                for (; ; )
                {
                    IDiaSourceFile sourceFile;
                    uint numFetched = 1;
                    enumSourceFiles.Next(numFetched, out sourceFile, out numFetched);
                    if (sourceFile == null || numFetched < 1)
                        break;
                    int usage = sourceFileUsage[sourceFile.fileName];
                    if (usage < bestSourceFileCount)
                    {
                        bestSourceFileName = sourceFile.fileName;
                        bestSourceFileType = GetSourceFileType(sourceFile.fileName);
                        bestSourceFileCount = usage;
                    }
                    else if (usage == bestSourceFileCount && bestSourceFileType != SourceFileType.cpp)
                    {
                        SourceFileType type = GetSourceFileType(sourceFile.fileName);
                        if (type < bestSourceFileType)
                        {
                            bestSourceFileName = sourceFile.fileName;
                            bestSourceFileType = type;
                        }
                    }
                }
            }
            return bestSourceFileName.ToLower();
        }

        private static IDiaSectionContrib FindSectionContribForRVA(int rva, List<IDiaSectionContrib> sectionContribs)
        {
            int i0 = 0, i1 = sectionContribs.Count;
            while (i0 < i1)
            {
                int i = (i1 + i0) / 2;
                if (sectionContribs[i].relativeVirtualAddress > rva)
                {
                    i1 = i;
                }
                else if (sectionContribs[i].relativeVirtualAddress + sectionContribs[i].length <= rva)
                {
                    i0 = i + 1;
                }
                else
                {
                    return sectionContribs[i];
                }
            }
            return null;
        }

        private static void BuildCompilandFileMap(IDiaSession session, Dictionary<uint, string> compilandFileMap)
        {
            IDiaSymbol globalScope = session.globalScope;

            Dictionary<string, int> sourceFileUsage = new Dictionary<string, int>();
            {
                IDiaEnumSymbols enumSymbols;
                globalScope.findChildren(Dia2Lib.SymTagEnum.SymTagCompiland, null, 0, out enumSymbols);

                for (; ; )
                {
                    uint numFetched = 1;
                    IDiaSymbol compiland;
                    enumSymbols.Next(numFetched, out compiland, out numFetched);
                    if (compiland == null || numFetched < 1)
                        break;

                    IDiaEnumSourceFiles enumSourceFiles;
                    session.findFile(compiland, null, 0, out enumSourceFiles);
                    if (enumSourceFiles != null)
                    {
                        for (; ; )
                        {
                            IDiaSourceFile sourceFile;
                            uint numFetched2 = 1;
                            enumSourceFiles.Next(numFetched2, out sourceFile, out numFetched2);
                            if (sourceFile == null || numFetched2 < 1)
                                break;
                            if (sourceFileUsage.ContainsKey(sourceFile.fileName))
                            {
                                sourceFileUsage[sourceFile.fileName]++;
                            }
                            else
                            {
                                sourceFileUsage.Add(sourceFile.fileName, 1);
                            }
                        }
                    }
                }
            }

            {
                IDiaEnumSymbols enumSymbols;
                globalScope.findChildren(Dia2Lib.SymTagEnum.SymTagCompiland, null, 0, out enumSymbols);

                for (; ; )
                {
                    uint numFetched = 1;
                    IDiaSymbol compiland;
                    enumSymbols.Next(numFetched, out compiland, out numFetched);
                    if (compiland == null || numFetched < 1)
                        break;

                    compilandFileMap.Add(compiland.symIndexId, FindBestSourceFileForCompiland(session, compiland, sourceFileUsage));
                }
            }
        }

        private static void BuildSectionContribTable(IDiaSession session, List<IDiaSectionContrib> sectionContribs)
        {
            {
                IDiaEnumSectionContribs enumSectionContribs = GetEnumSectionContribs(session);
                if (enumSectionContribs != null)
                {
                    for (; ; )
                    {
                        uint numFetched = 1;
                        IDiaSectionContrib diaSectionContrib;
                        enumSectionContribs.Next(numFetched, out diaSectionContrib, out numFetched);
                        if (diaSectionContrib == null || numFetched < 1)
                            break;

                        sectionContribs.Add(diaSectionContrib);

                    }
                }
            }
            sectionContribs.Sort(
                delegate(IDiaSectionContrib s0, IDiaSectionContrib s1)
                {
                    return (int)s0.relativeVirtualAddress - (int)s1.relativeVirtualAddress;
                });
        }


        private static void ReadSymbols(List<Symbol> symbols, string filename, string searchPath)
        {
            DiaSourceClass diaSource = new DiaSourceClass();

            if (Path.GetExtension(filename).ToLower() == ".pdb")
            {
                diaSource.loadDataFromPdb(filename);
            }
            else
            {
                diaSource.loadDataForExe(filename, searchPath, null);
            }

            IDiaSession diaSession;
            diaSource.openSession(out diaSession);

            //Console.WriteLine("Reading section info...");
            List<IDiaSectionContrib> sectionContribs = new List<IDiaSectionContrib>();
            BuildSectionContribTable(diaSession, sectionContribs);

            //Console.WriteLine("Reading source file info...");
            Dictionary<uint, string> compilandFileMap = new Dictionary<uint, string>();
            BuildCompilandFileMap(diaSession, compilandFileMap);

            IDiaSymbol globalScope = diaSession.globalScope;

            {
                IDiaEnumSymbols enumSymbols;
                globalScope.findChildren(Dia2Lib.SymTagEnum.SymTagData, null, 0, out enumSymbols);

                uint numSymbols = (uint)enumSymbols.count;
                uint symbolsRead = 0;
                uint percentComplete = 0;

                //Console.Write("Reading data symbols...");
                //Console.Write(" {0,3}% complete\b\b\b\b\b\b\b\b\b\b\b\b\b", percentComplete);
                for (; ; )
                {
                    uint numFetched = 1;
                    IDiaSymbol diaSymbol;
                    enumSymbols.Next(numFetched, out diaSymbol, out numFetched);
                    if (diaSymbol == null || numFetched < 1)
                        break;

                    uint newPercentComplete = 100 * ++symbolsRead / numSymbols;
                    if (percentComplete < newPercentComplete)
                    {
                        percentComplete = newPercentComplete;
                        //Console.Write("{0,3}\b\b\b", percentComplete);
                    }

                    if (diaSymbol.type == null)
                        continue;

                    switch ((DataKind)diaSymbol.dataKind)
                    {
                        case DataKind.DataIsLocal:
                        case DataKind.DataIsParam:
                        case DataKind.DataIsObjectPtr:
                        case DataKind.DataIsMember:
                            continue;
                    }

                    Symbol symbol = new Symbol();
                    symbol.size = (int)diaSymbol.type.length;
                    symbol.count = 1;
                    symbol.rva = (int)diaSymbol.relativeVirtualAddress;
                    symbol.short_name = diaSymbol.name == null ? "" : diaSymbol.name;
                    symbol.name = diaSymbol.undecoratedName == null ? symbol.short_name : diaSymbol.undecoratedName;
                    IDiaSectionContrib sectionContrib = FindSectionContribForRVA(symbol.rva, sectionContribs);
                    symbol.source_filename = sectionContrib == null ? "" : compilandFileMap[sectionContrib.compilandId];
                    symbol.section = sectionContrib == null ? "data" : (sectionContrib.uninitializedData ? "bss" : (sectionContrib.write ? "data" : "rdata"));

                    symbols.Add(symbol);
                }
                //Console.WriteLine("{0,3}%", 100);
            }

            {
                IDiaEnumSymbols enumSymbols;
                globalScope.findChildren(Dia2Lib.SymTagEnum.SymTagFunction, null, 0, out enumSymbols);

                uint numSymbols = (uint)enumSymbols.count;
                uint symbolsRead = 0;
                uint percentComplete = 0;

                //Console.Write("Reading function symbols...");
                // Console.Write(" {0,3}% complete\b\b\b\b\b\b\b\b\b\b\b\b\b", percentComplete);
                for (; ; )
                {
                    uint numFetched = 1;
                    IDiaSymbol diaSymbol;
                    enumSymbols.Next(numFetched, out diaSymbol, out numFetched);
                    if (diaSymbol == null || numFetched < 1)
                        break;

                    uint newPercentComplete = 100 * ++symbolsRead / numSymbols;
                    if (percentComplete < newPercentComplete)
                    {
                        percentComplete = newPercentComplete;
                        //Console.Write("{0,3}\b\b\b", percentComplete);
                    }

                    if (diaSymbol.length == 0)
                        continue;

                    Symbol symbol = new Symbol();
                    symbol.short_name = diaSymbol.name == null ? "" : diaSymbol.name;
                    symbol.name = diaSymbol.undecoratedName == null ? symbol.short_name : diaSymbol.undecoratedName;
                    symbol.rva = (int)diaSymbol.relativeVirtualAddress;
                    symbol.source_filename = FindSourceFileForRVA(diaSession, diaSymbol.relativeVirtualAddress, (uint)diaSymbol.length);
                    symbol.section = "code";
                    symbol.size = (int)diaSymbol.length;
                    symbol.count = 1;

                    symbols.Add(symbol);
                }
                //Console.WriteLine("{0,3}%", 100);
            }

            //Console.WriteLine("Subtracting overlapping symbols...");
            {
                symbols.Sort(
                    delegate(Symbol s0, Symbol s1)
                    {
                        if (s0.rva == s1.rva)
                        {
                            return s1.name.Length - s0.name.Length;
                        }
                        return s0.rva - s1.rva;
                    });

                int highWaterMark = 0;
                for (int i = 0, count = symbols.Count; i < count; ++i)
                {
                    Symbol s = symbols[i];
                    int symbolStart = s.rva;
                    int symbolEnd = s.rva + s.size;
                    int overlapStart = symbolStart;
                    int overlapEnd = Math.Max(overlapStart, Math.Min(symbolEnd, highWaterMark));
                    s.size -= overlapEnd - symbolStart;
                    highWaterMark = Math.Max(highWaterMark, symbolEnd);
                }
            }

        }

        private static void WriteSymbolList(TextWriter writer, List<Symbol> symbolList, int maxCount)
        {

            int count = maxCount;
            foreach (Symbol s in symbolList)
            {
                if (count-- == 0)
                    break;

                writer.WriteLine("{0} 0x{1:X8} {2}",
                    s.section, 
                    s.size,
                    s.name);
            }
            writer.WriteLine();
        }

        private static bool ParseArgs(string[] args, out List<string> inputFiles, out string outFilename, out string searchPath, out int maxCount, out List<string> exclusions)
        {
            maxCount = int.MaxValue;
            exclusions = new List<string>();
            inputFiles = new List<string>();
            outFilename = null;
            searchPath = null;

            if (args.Length < 1)
                return false;

            uint curArg = 0;
            for (curArg = 0; curArg < args.Length - 1; ++curArg)
            {
                string curArgStr = args[curArg].ToLower();
                if (curArgStr == "-count")
                {
                    try
                    {
                        maxCount = int.Parse(args[++curArg]);
                    }
                    catch (System.FormatException)
                    {
                        return false;
                    }
                }
                else if (curArgStr == "-exclude")
                {
                    exclusions.Add(args[++curArg]);
                }
                else if (curArgStr == "-in")
                {
                    inputFiles.Add(args[++curArg]);
                }
                else if (curArgStr == "-out")
                {
                    outFilename = args[++curArg];
                }
                else if (curArgStr == "-searchpath")
                {
                    searchPath = args[++curArg];
                }
                else
                {
                    Console.WriteLine("Unrecognized option {0}", args[curArg]);
                    return false;
                }
            }

            if (!inputFiles.Any())
            {
                Console.WriteLine("At least one input file must be specified");
                return false;
            }

            return true;
        }

        static void Main(string[] args)
        {
            int maxCount;
            List<string> exclusions;
            List<string> inputFiles;
            string outFilename;
            string searchPath;
            if (!ParseArgs(args, out inputFiles, out outFilename, out searchPath, out maxCount, out exclusions))
            {
                Console.WriteLine();
                Console.WriteLine("Usage: SymbolExtract [options]");
                Console.WriteLine();
                Console.WriteLine("Options:");
                Console.WriteLine("  -in[:type] filename");
                Console.WriteLine("      Specify an input file (EXE or PDB)");
                Console.WriteLine();
                Console.WriteLine("  -out filename");
                Console.WriteLine("      Write output to specified file instead of stdout");
                Console.WriteLine();
                Console.WriteLine("  -exclude substring");
                Console.WriteLine("      Exclude symbols that contain the specified substring");
                Console.WriteLine();
                Console.WriteLine("  -searchpath path");
                Console.WriteLine("      Specify the symbol search path when loading an exe");
                return;
            }

            foreach (string inputFile in inputFiles)
            {
                if (!File.Exists(inputFile))
                {
                    Console.WriteLine("Input file {0} does not exist!", inputFile);
                    return;
                }
            }

            TextWriter writer;
            try
            {
                writer = outFilename != null ? new StreamWriter(outFilename) : Console.Out;
            }
            catch (IOException ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }

            DateTime startTime = DateTime.Now;

            List<Symbol> symbols = new List<Symbol>();
            foreach (string inputFile in inputFiles)
            {
                ReadSymbols(symbols, inputFile, searchPath);
                Console.WriteLine();
            }

            if (exclusions.Any())
            {
                //Console.WriteLine("Removing Exclusions...");
                symbols.RemoveAll(
                    delegate(Symbol s)
                    {
                        foreach (string e in exclusions)
                        {
                            if (s.name.Contains(e))
                                return true;
                        }
                        return false;
                    });
            }

            //Console.WriteLine("Processing raw symbols...");
            {
                long totalCount = 0;
                long totalSize = 0;

                foreach (Symbol s in symbols)
                {
                    totalSize += s.size;
                    totalCount += s.count;
                }

                symbols.Sort(delegate(Symbol s0, Symbol s1) {return s1.size - s0.size;});
                WriteSymbolList(writer, symbols, maxCount);
            }
        }
    }
}
