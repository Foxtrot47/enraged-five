﻿namespace MemoryHasherUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rtbOutput = new System.Windows.Forms.RichTextBox();
            this.cbTelemetry = new System.Windows.Forms.CheckBox();
            this.cbPeer = new System.Windows.Forms.CheckBox();
            this.cbKick = new System.Windows.Forms.CheckBox();
            this.cbMatchmaking = new System.Windows.Forms.CheckBox();
            this.cbReadOnly = new System.Windows.Forms.CheckBox();
            this.cbReadWrite = new System.Windows.Forms.CheckBox();
            this.cbWrite = new System.Windows.Forms.CheckBox();
            this.cbExecute = new System.Windows.Forms.CheckBox();
            this.cbExecuteRead = new System.Windows.Forms.CheckBox();
            this.cbExecuteReadWrite = new System.Windows.Forms.CheckBox();
            this.cbExecuteWrite = new System.Windows.Forms.CheckBox();
            this.cbPrint = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbDllName = new System.Windows.Forms.TextBox();
            this.nudVersion = new System.Windows.Forms.NumericUpDown();
            this.cmbSku = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.nudOffset = new System.Windows.Forms.NumericUpDown();
            this.nudLength = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.tbModuleName = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tbAddress = new System.Windows.Forms.TextBox();
            this.cmbExecutableA = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tbByteStringA = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbByteStringType = new System.Windows.Forms.ComboBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.nudSize = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.cmbExecutableB = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.nudHighPage = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.nudLowPage = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.tbByteStringB = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.nudVersionEnd = new System.Windows.Forms.NumericUpDown();
            this.cbGameserver = new System.Windows.Forms.CheckBox();
            this.cbCrash = new System.Windows.Forms.CheckBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.cmbEcTargetExe = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.nudEcHighPage = new System.Windows.Forms.NumericUpDown();
            this.label19 = new System.Windows.Forms.Label();
            this.nudEcLowPage = new System.Windows.Forms.NumericUpDown();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.tbEcByteString = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.nudVersion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudOffset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLength)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHighPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLowPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVersionEnd)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudEcHighPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEcLowPage)).BeginInit();
            this.SuspendLayout();
            // 
            // rtbOutput
            // 
            this.rtbOutput.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbOutput.Location = new System.Drawing.Point(11, 336);
            this.rtbOutput.Name = "rtbOutput";
            this.rtbOutput.ReadOnly = true;
            this.rtbOutput.Size = new System.Drawing.Size(698, 190);
            this.rtbOutput.TabIndex = 0;
            this.rtbOutput.Text = "";
            // 
            // cbTelemetry
            // 
            this.cbTelemetry.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbTelemetry.AutoSize = true;
            this.cbTelemetry.Location = new System.Drawing.Point(384, 68);
            this.cbTelemetry.Name = "cbTelemetry";
            this.cbTelemetry.Size = new System.Drawing.Size(72, 17);
            this.cbTelemetry.TabIndex = 1;
            this.cbTelemetry.Text = "Telemetry";
            this.cbTelemetry.UseVisualStyleBackColor = true;
            // 
            // cbPeer
            // 
            this.cbPeer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbPeer.AutoSize = true;
            this.cbPeer.Location = new System.Drawing.Point(384, 91);
            this.cbPeer.Name = "cbPeer";
            this.cbPeer.Size = new System.Drawing.Size(46, 17);
            this.cbPeer.TabIndex = 2;
            this.cbPeer.Text = "P2P";
            this.cbPeer.UseVisualStyleBackColor = true;
            // 
            // cbKick
            // 
            this.cbKick.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbKick.AutoSize = true;
            this.cbKick.Location = new System.Drawing.Point(384, 114);
            this.cbKick.Name = "cbKick";
            this.cbKick.Size = new System.Drawing.Size(79, 17);
            this.cbKick.TabIndex = 3;
            this.cbKick.Text = "Player Kick";
            this.cbKick.UseVisualStyleBackColor = true;
            // 
            // cbMatchmaking
            // 
            this.cbMatchmaking.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbMatchmaking.AutoSize = true;
            this.cbMatchmaking.Location = new System.Drawing.Point(384, 137);
            this.cbMatchmaking.Name = "cbMatchmaking";
            this.cbMatchmaking.Size = new System.Drawing.Size(128, 17);
            this.cbMatchmaking.TabIndex = 4;
            this.cbMatchmaking.Text = "Disable Matchmaking";
            this.cbMatchmaking.UseVisualStyleBackColor = true;
            // 
            // cbReadOnly
            // 
            this.cbReadOnly.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbReadOnly.AutoSize = true;
            this.cbReadOnly.Location = new System.Drawing.Point(531, 114);
            this.cbReadOnly.Name = "cbReadOnly";
            this.cbReadOnly.Size = new System.Drawing.Size(76, 17);
            this.cbReadOnly.TabIndex = 5;
            this.cbReadOnly.Text = "Read Only";
            this.cbReadOnly.UseVisualStyleBackColor = true;
            // 
            // cbReadWrite
            // 
            this.cbReadWrite.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbReadWrite.AutoSize = true;
            this.cbReadWrite.Location = new System.Drawing.Point(531, 137);
            this.cbReadWrite.Name = "cbReadWrite";
            this.cbReadWrite.Size = new System.Drawing.Size(88, 17);
            this.cbReadWrite.TabIndex = 6;
            this.cbReadWrite.Text = "Read / Write";
            this.cbReadWrite.UseVisualStyleBackColor = true;
            // 
            // cbWrite
            // 
            this.cbWrite.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbWrite.AutoSize = true;
            this.cbWrite.Location = new System.Drawing.Point(531, 91);
            this.cbWrite.Name = "cbWrite";
            this.cbWrite.Size = new System.Drawing.Size(51, 17);
            this.cbWrite.TabIndex = 7;
            this.cbWrite.Text = "Write";
            this.cbWrite.UseVisualStyleBackColor = true;
            // 
            // cbExecute
            // 
            this.cbExecute.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbExecute.AutoSize = true;
            this.cbExecute.Location = new System.Drawing.Point(531, 68);
            this.cbExecute.Name = "cbExecute";
            this.cbExecute.Size = new System.Drawing.Size(65, 17);
            this.cbExecute.TabIndex = 8;
            this.cbExecute.Text = "Execute";
            this.cbExecute.UseVisualStyleBackColor = true;
            // 
            // cbExecuteRead
            // 
            this.cbExecuteRead.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbExecuteRead.AutoSize = true;
            this.cbExecuteRead.Location = new System.Drawing.Point(531, 158);
            this.cbExecuteRead.Name = "cbExecuteRead";
            this.cbExecuteRead.Size = new System.Drawing.Size(102, 17);
            this.cbExecuteRead.TabIndex = 9;
            this.cbExecuteRead.Text = "Execute / Read";
            this.cbExecuteRead.UseVisualStyleBackColor = true;
            // 
            // cbExecuteReadWrite
            // 
            this.cbExecuteReadWrite.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbExecuteReadWrite.AutoSize = true;
            this.cbExecuteReadWrite.Location = new System.Drawing.Point(531, 181);
            this.cbExecuteReadWrite.Name = "cbExecuteReadWrite";
            this.cbExecuteReadWrite.Size = new System.Drawing.Size(138, 17);
            this.cbExecuteReadWrite.TabIndex = 10;
            this.cbExecuteReadWrite.Text = "Execute / Read / Write";
            this.cbExecuteReadWrite.UseVisualStyleBackColor = true;
            // 
            // cbExecuteWrite
            // 
            this.cbExecuteWrite.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbExecuteWrite.AutoSize = true;
            this.cbExecuteWrite.Location = new System.Drawing.Point(531, 205);
            this.cbExecuteWrite.Name = "cbExecuteWrite";
            this.cbExecuteWrite.Size = new System.Drawing.Size(101, 17);
            this.cbExecuteWrite.TabIndex = 11;
            this.cbExecuteWrite.Text = "Execute / Write";
            this.cbExecuteWrite.UseVisualStyleBackColor = true;
            // 
            // cbPrint
            // 
            this.cbPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbPrint.AutoSize = true;
            this.cbPrint.Location = new System.Drawing.Point(384, 247);
            this.cbPrint.Name = "cbPrint";
            this.cbPrint.Size = new System.Drawing.Size(129, 17);
            this.cbPrint.TabIndex = 12;
            this.cbPrint.Text = "Print Matching Blocks";
            this.cbPrint.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(381, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Version";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "DLL Name";
            // 
            // tbDllName
            // 
            this.tbDllName.Location = new System.Drawing.Point(16, 37);
            this.tbDllName.Name = "tbDllName";
            this.tbDllName.Size = new System.Drawing.Size(240, 20);
            this.tbDllName.TabIndex = 16;
            // 
            // nudVersion
            // 
            this.nudVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.nudVersion.Location = new System.Drawing.Point(429, 16);
            this.nudVersion.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.nudVersion.Name = "nudVersion";
            this.nudVersion.Size = new System.Drawing.Size(84, 20);
            this.nudVersion.TabIndex = 17;
            // 
            // cmbSku
            // 
            this.cmbSku.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbSku.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSku.FormattingEnabled = true;
            this.cmbSku.Items.AddRange(new object[] {
            "ALL-BETA",
            "ALL-BANK",
            "XB1-MASTER",
            "PS4-MASTER",
            "PC-MASTER",
            "EVERYTHING-ELSE"});
            this.cmbSku.Location = new System.Drawing.Point(429, 42);
            this.cmbSku.Name = "cmbSku";
            this.cmbSku.Size = new System.Drawing.Size(280, 21);
            this.cmbSku.TabIndex = 18;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(381, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "SKU";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 129);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(168, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "# of bytes to read when searching";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 51);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 13);
            this.label8.TabIndex = 28;
            this.label8.Text = "Address";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(185, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "Offset";
            // 
            // nudOffset
            // 
            this.nudOffset.Enabled = false;
            this.nudOffset.Location = new System.Drawing.Point(188, 67);
            this.nudOffset.Name = "nudOffset";
            this.nudOffset.Size = new System.Drawing.Size(165, 20);
            this.nudOffset.TabIndex = 21;
            // 
            // nudLength
            // 
            this.nudLength.Location = new System.Drawing.Point(10, 145);
            this.nudLength.Maximum = new decimal(new int[] {
            -1,
            2147483647,
            0,
            0});
            this.nudLength.Name = "nudLength";
            this.nudLength.Size = new System.Drawing.Size(342, 20);
            this.nudLength.TabIndex = 23;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 90);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 13);
            this.label6.TabIndex = 24;
            this.label6.Text = "Module Name";
            // 
            // tbModuleName
            // 
            this.tbModuleName.Enabled = false;
            this.tbModuleName.Location = new System.Drawing.Point(10, 106);
            this.tbModuleName.Name = "tbModuleName";
            this.tbModuleName.Size = new System.Drawing.Size(341, 20);
            this.tbModuleName.TabIndex = 25;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(11, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(367, 318);
            this.tabControl1.TabIndex = 30;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tbDllName);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(359, 292);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "DLL Name";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tbAddress);
            this.tabPage2.Controls.Add(this.cmbExecutableA);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.tbByteStringA);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.cmbByteStringType);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.nudLength);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.tbModuleName);
            this.tabPage2.Controls.Add(this.nudOffset);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(359, 292);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Byte String";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tbAddress
            // 
            this.tbAddress.Enabled = false;
            this.tbAddress.Location = new System.Drawing.Point(10, 67);
            this.tbAddress.Name = "tbAddress";
            this.tbAddress.Size = new System.Drawing.Size(168, 20);
            this.tbAddress.TabIndex = 34;
            // 
            // cmbExecutableA
            // 
            this.cmbExecutableA.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbExecutableA.FormattingEnabled = true;
            this.cmbExecutableA.Items.AddRange(new object[] {
            "GTA5.exe",
            "GTAV.exe",
            "game_win64_bankrelease.exe",
            "game_win64_beta.exe",
            "game_win64_release.exe",
            "game_win64_final.exe"});
            this.cmbExecutableA.Location = new System.Drawing.Point(10, 223);
            this.cmbExecutableA.Name = "cmbExecutableA";
            this.cmbExecutableA.Size = new System.Drawing.Size(343, 21);
            this.cmbExecutableA.TabIndex = 33;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 207);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(94, 13);
            this.label10.TabIndex = 32;
            this.label10.Text = "Target Executable";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 168);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(58, 13);
            this.label9.TabIndex = 31;
            this.label9.Text = "Byte String";
            // 
            // tbByteStringA
            // 
            this.tbByteStringA.Location = new System.Drawing.Point(10, 184);
            this.tbByteStringA.Name = "tbByteStringA";
            this.tbByteStringA.Size = new System.Drawing.Size(343, 20);
            this.tbByteStringA.TabIndex = 30;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 11);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Index Type";
            // 
            // cmbByteStringType
            // 
            this.cmbByteStringType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbByteStringType.FormattingEnabled = true;
            this.cmbByteStringType.Items.AddRange(new object[] {
            "Module Name / Offset",
            "Address"});
            this.cmbByteStringType.Location = new System.Drawing.Point(10, 27);
            this.cmbByteStringType.Name = "cmbByteStringType";
            this.cmbByteStringType.Size = new System.Drawing.Size(342, 21);
            this.cmbByteStringType.TabIndex = 0;
            this.cmbByteStringType.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.nudSize);
            this.tabPage3.Controls.Add(this.label15);
            this.tabPage3.Controls.Add(this.cmbExecutableB);
            this.tabPage3.Controls.Add(this.label14);
            this.tabPage3.Controls.Add(this.nudHighPage);
            this.tabPage3.Controls.Add(this.label13);
            this.tabPage3.Controls.Add(this.nudLowPage);
            this.tabPage3.Controls.Add(this.label12);
            this.tabPage3.Controls.Add(this.label11);
            this.tabPage3.Controls.Add(this.tbByteStringB);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(359, 292);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Paged Byte String";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // nudSize
            // 
            this.nudSize.Location = new System.Drawing.Point(167, 66);
            this.nudSize.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.nudSize.Name = "nudSize";
            this.nudSize.Size = new System.Drawing.Size(142, 20);
            this.nudSize.TabIndex = 8;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(164, 50);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(140, 13);
            this.label15.TabIndex = 7;
            this.label15.Text = "Size of Memory (in kilobytes)";
            // 
            // cmbExecutableB
            // 
            this.cmbExecutableB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbExecutableB.FormattingEnabled = true;
            this.cmbExecutableB.Items.AddRange(new object[] {
            "GTA5.exe",
            "GTAV.exe",
            "game_win64_bankrelease.exe",
            "game_win64_release.exe",
            "game_win64_final.exe",
            "game_win64_beta.exe",
            "ORBIS.elf"});
            this.cmbExecutableB.Location = new System.Drawing.Point(10, 144);
            this.cmbExecutableB.Name = "cmbExecutableB";
            this.cmbExecutableB.Size = new System.Drawing.Size(343, 21);
            this.cmbExecutableB.TabIndex = 6;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(7, 128);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(94, 13);
            this.label14.TabIndex = 5;
            this.label14.Text = "Target Executable";
            // 
            // nudHighPage
            // 
            this.nudHighPage.Location = new System.Drawing.Point(10, 105);
            this.nudHighPage.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.nudHighPage.Name = "nudHighPage";
            this.nudHighPage.Size = new System.Drawing.Size(120, 20);
            this.nudHighPage.TabIndex = 0;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(7, 89);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(57, 13);
            this.label13.TabIndex = 4;
            this.label13.Text = "High Page";
            // 
            // nudLowPage
            // 
            this.nudLowPage.Location = new System.Drawing.Point(10, 66);
            this.nudLowPage.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.nudLowPage.Name = "nudLowPage";
            this.nudLowPage.Size = new System.Drawing.Size(120, 20);
            this.nudLowPage.TabIndex = 3;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(7, 50);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(55, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "Low Page";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 11);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(58, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "Byte String";
            // 
            // tbByteStringB
            // 
            this.tbByteStringB.Location = new System.Drawing.Point(10, 27);
            this.tbByteStringB.Name = "tbByteStringB";
            this.tbByteStringB.Size = new System.Drawing.Size(343, 20);
            this.tbByteStringB.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(384, 270);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(325, 56);
            this.button1.TabIndex = 31;
            this.button1.Text = "Generate Signature";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(519, 18);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(99, 13);
            this.label16.TabIndex = 32;
            this.label16.Text = "Vers. End (optional)";
            // 
            // nudVersionEnd
            // 
            this.nudVersionEnd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.nudVersionEnd.Location = new System.Drawing.Point(624, 16);
            this.nudVersionEnd.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.nudVersionEnd.Name = "nudVersionEnd";
            this.nudVersionEnd.Size = new System.Drawing.Size(85, 20);
            this.nudVersionEnd.TabIndex = 33;
            // 
            // cbGameserver
            // 
            this.cbGameserver.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbGameserver.AutoSize = true;
            this.cbGameserver.Location = new System.Drawing.Point(384, 158);
            this.cbGameserver.Name = "cbGameserver";
            this.cbGameserver.Size = new System.Drawing.Size(118, 17);
            this.cbGameserver.TabIndex = 34;
            this.cbGameserver.Text = "Gameserver Report";
            this.cbGameserver.UseVisualStyleBackColor = true;
            // 
            // cbCrash
            // 
            this.cbCrash.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbCrash.AutoSize = true;
            this.cbCrash.Location = new System.Drawing.Point(384, 181);
            this.cbCrash.Name = "cbCrash";
            this.cbCrash.Size = new System.Drawing.Size(53, 17);
            this.cbCrash.TabIndex = 35;
            this.cbCrash.Text = "Crash";
            this.cbCrash.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.cmbEcTargetExe);
            this.tabPage4.Controls.Add(this.label18);
            this.tabPage4.Controls.Add(this.nudEcHighPage);
            this.tabPage4.Controls.Add(this.label19);
            this.tabPage4.Controls.Add(this.nudEcLowPage);
            this.tabPage4.Controls.Add(this.label20);
            this.tabPage4.Controls.Add(this.label21);
            this.tabPage4.Controls.Add(this.tbEcByteString);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(359, 292);
            this.tabPage4.TabIndex = 4;
            this.tabPage4.Text = "Exe Check";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // cmbEcTargetExe
            // 
            this.cmbEcTargetExe.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEcTargetExe.FormattingEnabled = true;
            this.cmbEcTargetExe.Items.AddRange(new object[] {
            "GTA5.exe",
            "GTAV.exe",
            "game_win64_bankrelease.exe",
            "game_win64_release.exe",
            "game_win64_final.exe",
            "game_win64_beta.exe",
            "ORBIS.elf"});
            this.cmbEcTargetExe.Location = new System.Drawing.Point(10, 144);
            this.cmbEcTargetExe.Name = "cmbEcTargetExe";
            this.cmbEcTargetExe.Size = new System.Drawing.Size(343, 21);
            this.cmbEcTargetExe.TabIndex = 6;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(7, 128);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(94, 13);
            this.label18.TabIndex = 5;
            this.label18.Text = "Target Executable";
            // 
            // nudEcHighPage
            // 
            this.nudEcHighPage.Location = new System.Drawing.Point(10, 105);
            this.nudEcHighPage.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.nudEcHighPage.Name = "nudEcHighPage";
            this.nudEcHighPage.Size = new System.Drawing.Size(120, 20);
            this.nudEcHighPage.TabIndex = 0;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(7, 89);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(57, 13);
            this.label19.TabIndex = 4;
            this.label19.Text = "High Page";
            // 
            // nudEcLowPage
            // 
            this.nudEcLowPage.Location = new System.Drawing.Point(10, 66);
            this.nudEcLowPage.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.nudEcLowPage.Name = "nudEcLowPage";
            this.nudEcLowPage.Size = new System.Drawing.Size(120, 20);
            this.nudEcLowPage.TabIndex = 3;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(7, 50);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(55, 13);
            this.label20.TabIndex = 2;
            this.label20.Text = "Low Page";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(7, 11);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(58, 13);
            this.label21.TabIndex = 1;
            this.label21.Text = "Byte String";
            // 
            // tbEcByteString
            // 
            this.tbEcByteString.Location = new System.Drawing.Point(10, 27);
            this.tbEcByteString.Name = "tbEcByteString";
            this.tbEcByteString.Size = new System.Drawing.Size(343, 20);
            this.tbEcByteString.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(720, 538);
            this.Controls.Add(this.cbCrash);
            this.Controls.Add(this.cbGameserver);
            this.Controls.Add(this.nudVersionEnd);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cmbSku);
            this.Controls.Add(this.nudVersion);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbPrint);
            this.Controls.Add(this.cbExecuteWrite);
            this.Controls.Add(this.cbExecuteReadWrite);
            this.Controls.Add(this.cbExecuteRead);
            this.Controls.Add(this.cbExecute);
            this.Controls.Add(this.cbWrite);
            this.Controls.Add(this.cbReadWrite);
            this.Controls.Add(this.cbReadOnly);
            this.Controls.Add(this.cbMatchmaking);
            this.Controls.Add(this.cbKick);
            this.Controls.Add(this.cbPeer);
            this.Controls.Add(this.cbTelemetry);
            this.Controls.Add(this.rtbOutput);
            this.MinimumSize = new System.Drawing.Size(736, 577);
            this.Name = "Form1";
            this.Text = "MemoryHasherUI";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nudVersion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudOffset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLength)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHighPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLowPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVersionEnd)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudEcHighPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEcLowPage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtbOutput;
        private System.Windows.Forms.CheckBox cbTelemetry;
        private System.Windows.Forms.CheckBox cbPeer;
        private System.Windows.Forms.CheckBox cbKick;
        private System.Windows.Forms.CheckBox cbMatchmaking;
        private System.Windows.Forms.CheckBox cbReadOnly;
        private System.Windows.Forms.CheckBox cbReadWrite;
        private System.Windows.Forms.CheckBox cbWrite;
        private System.Windows.Forms.CheckBox cbExecute;
        private System.Windows.Forms.CheckBox cbExecuteRead;
        private System.Windows.Forms.CheckBox cbExecuteReadWrite;
        private System.Windows.Forms.CheckBox cbExecuteWrite;
        private System.Windows.Forms.CheckBox cbPrint;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbDllName;
        private System.Windows.Forms.NumericUpDown nudVersion;
        private System.Windows.Forms.ComboBox cmbSku;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown nudOffset;
        private System.Windows.Forms.NumericUpDown nudLength;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbModuleName;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmbByteStringType;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbByteStringA;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cmbExecutableA;
        private System.Windows.Forms.NumericUpDown nudHighPage;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown nudLowPage;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tbByteStringB;
        private System.Windows.Forms.ComboBox cmbExecutableB;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.NumericUpDown nudSize;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox tbAddress;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.NumericUpDown nudVersionEnd;
        private System.Windows.Forms.CheckBox cbGameserver;
        private System.Windows.Forms.CheckBox cbCrash;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.ComboBox cmbEcTargetExe;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.NumericUpDown nudEcHighPage;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.NumericUpDown nudEcLowPage;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox tbEcByteString;
    }
}

