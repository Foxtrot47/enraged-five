﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
using System.Security.Cryptography;
namespace MemoryHasherUI
{
    public partial class Form1 : Form
    {
        const String m_binaryPath = @"x:\gta5\tools_ng\bin\MemoryHasher.exe";
        
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private Int32 GetActionFlags()
        {
            BitArray bary = new BitArray(32);

            if (cbTelemetry.Checked)
                bary[0] = true;
            if (cbPeer.Checked)
                bary[1] = true;
            if (cbKick.Checked)
                bary[3] = true;
            if (cbMatchmaking.Checked)
                bary[4] = true;
            if (cbGameserver.Checked)
                bary[5] = true;
            if (cbCrash.Checked)
                bary[6] = true;
            if (cbReadOnly.Checked)
                bary[9] = true;
            if (cbReadWrite.Checked)
                bary[10] = true;
            if (cbWrite.Checked)
                bary[11] = true;
            if (cbExecute.Checked)
                bary[12] = true;
            if (cbExecuteRead.Checked)
                bary[13] = true;
            if (cbExecuteReadWrite.Checked)
                bary[14] = true;
            if (cbExecuteWrite.Checked)
                bary[15] = true;

            int[] array = new int[1];
            bary.CopyTo(array, 0);
            return array[0];
        }
        private String GetSku()
        {
            String sku = cmbSku.SelectedItem.ToString();

            if (sku == "ALL-BETA")
            {
                return "100";
            }
            else if (sku == "ALL-BANK")
            {
                return "101";
            }
            else if (sku == "XB1-MASTER")
            {
                return "6";
            }
            else if (sku == "PS4-MASTER")
            {
                return "5";
            }
            else if (sku == "PC-MASTER")
            {
                return "7";
            }
            else if (sku == "EVERYTHING-ELSE")
            {
                return "0";
            }
            else
            {
                return "0";
            }
        }

        private String ParseHash(String output)
        {
            String[] outputLines = output.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
            foreach (String line in outputLines)
            {
                if (line.Contains("Hash") == true)
                {
                    // 4 is the length of "Hash"
                    String stripped = line.Substring(4);
                    stripped = stripped.Trim();
                    return stripped;
                }
                if (line.Contains("Memory hash") == true)
                {
                    // 4 is the length of "Hash"
                    String stripped = line.Substring(11);
                    stripped = stripped.Trim();
                    return stripped;
                }
            }

            return "";
        }
        private void ExecuteCommand(String commandArgs)

        {
            if (commandArgs == null)
                return;
            // Start the child process.
            Process p = new Process();
            List<String> storedHashes = new List<String>();

            //-versionNumber 
            Int32 versionEnd      = (int)nudVersionEnd.Value;
            Int32 versionStart    = (int)nudVersion.Value;
            rtbOutput.Clear();
            if (versionEnd == 0 || versionEnd  <= versionStart)
            {
                versionEnd = versionStart;

                rtbOutput.AppendText("Optional Version End not specified or invalid. Defaulting to only specify one version.\n\n");
            }
            
            while (versionStart <= versionEnd)
            {
                // Redirect the output stream of the child process.
                String commandArgsCopy = commandArgs + " -versionNumber " + versionStart.ToString();
                versionStart++;
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardOutput = true;
                if (!File.Exists(m_binaryPath))
                {
                    p.StartInfo.FileName = "MemoryHasher.exe";
                }
                else
                {
                    p.StartInfo.FileName = m_binaryPath;
                }
                p.StartInfo.Arguments = commandArgsCopy;
                p.Start();
                // Do not wait for the child process to exit before
                // reading to the end of its redirected stream.
                // p.WaitForExit();
                // Read the output stream first and then wait.
                string output = p.StandardOutput.ReadToEnd();
                p.WaitForExit();
                rtbOutput.AppendText("=============================" + DateTime.Now.ToString() +  " Version: " + versionStart.ToString() + "\n");
                rtbOutput.AppendText("input: " + commandArgsCopy.ToString() + "\n");
                rtbOutput.AppendText(output);
                storedHashes.Add(ParseHash(output));
                rtbOutput.AppendText("\n");
            }

            rtbOutput.AppendText("=============================" + DateTime.Now.ToString() + " Hashes\n");
            StringBuilder agg = new StringBuilder();
            foreach (String s in storedHashes)
            {
                agg.AppendLine(s + ",");
            }
            agg.Remove(agg.Length - 3, 3);
            rtbOutput.AppendText(agg.ToString());
            
            
        }
        private bool IsCommonOkay()
        {

            if (GetActionFlags() == 0)
            {
                MessageBox.Show("Please add Memory Flags");
                return false;
            }
            if (nudVersion.Value == 0)
            {
                MessageBox.Show("Please specify version of game");
                return false;
            }

            if (cmbSku.SelectedItem == null || cmbSku.SelectedItem.ToString().Length == 0)
            {
                MessageBox.Show("Please specify a sku");
                return false;
            }
            return true;
        }
        private String ByteStringArgs()
        {
            if (!IsCommonOkay())
                return null;

            if (cmbExecutableA.SelectedItem.ToString().Length == 0)
            {
                MessageBox.Show("Please specify a target executable");
                return null;
            }

            if (tbByteStringA.Text.Length == 0)
            {
                MessageBox.Show("Please specify a byte string");
                return null;
            }

            if (nudLength.Value == 0)
            { 
                MessageBox.Show("Please specify a length");
                return null;
            }

            String args = "";
            args += " -type bytestring";
            args += " -actionFlags " + GetActionFlags().ToString();
            // Commenting this out because it's handled 
            // in an iterative loop in the ExecuteCommand() function
                //args += " -versionNumber +nudVersion.Value.ToString(); - 
                
            int tabIdx = cmbByteStringType.SelectedIndex;
            switch (tabIdx)
            {
                case 0:
                    {
                        args += " -moduleName " + tbModuleName.Text;
                        args += " -offset " + nudOffset.Value.ToString();
                        args += " -length " + nudLength.Value.ToString();
                        args += " -targetExecutable " + cmbExecutableA.SelectedItem.ToString();
                        args += " -bytestring \"" + tbByteStringA.Text + "\"";
                        args += " -sku " + GetSku();
                        break;
                    }
                case 1:
                    {

                        Int64 addr = Int64.Parse(tbAddress.Text, NumberStyles.HexNumber);
                        Int32 addrL = (((Int32)(addr) << 32) >> 64);
                        Int32 addrH = (Int32)(addr >> 32);
                        args += " -length " + nudLength.Value.ToString();
                        args += " -addrH " + String.Format("0x{0:X}", addrH).ToString();
                        args += " -addrL " + String.Format("0x{0:X}", addrL).ToString();
                        args += " -targetExecutable " + cmbExecutableA.SelectedItem.ToString();
                        args += " -bytestring \"" + tbByteStringA.Text + "\"";
                        args += " -sku " + GetSku();
                        break;
                    }
                default:
                    break;
            }
            if (cbPrint.Checked)
            {
                args += " -printmatchedblocks";
            }
            return args;
        }

        private String RSASignArgs(string outputbuff, int outputCount)
        {
            String args = "";
            args += " -type rsasign";
            args += " -input " + outputbuff;
            return args;
        }

        private String PagedByteStringArgs()
        {
            if (!IsCommonOkay())
                return null;

            if (cmbExecutableB.SelectedItem == null || cmbExecutableB.SelectedItem.ToString().Length == 0)
            {
                MessageBox.Show("Please specify a target executable");
                return null;
            }

            if (tbByteStringB.Text.Length == 0)
            {
                MessageBox.Show("Please specify a byte string");
                return null;
            }

            if (nudLowPage.Value == nudHighPage.Value)
            {
                MessageBox.Show("Low Page cannot equal High Page");
                return null;
            }

            if (nudSize.Value == 0)
            {
                MessageBox.Show("Please specify a size in kilobytes");
                return null;
            }
            String args = "";
            args += " -type pagebytestring";
            args += " -actionFlags " + GetActionFlags().ToString();
            //args += " -versionNumber " + nudVersion.Value.ToString();
            args += " -bytestring \"" + tbByteStringB.Text + "\"";
            args += " -lowPageNumber " + nudLowPage.Value.ToString();
            args += " -highPageNumber " + nudHighPage.Value.ToString();
            args += " -size " + nudSize.Value.ToString();
            args += " -targetExecutable " + cmbExecutableB.SelectedItem.ToString();
            args += " -sku " + GetSku();

            if (cbPrint.Checked)
            {
                args += " -printmatchedblocks";
            }
            return args;
        }

        private String ExecutableCheckerArgs()
        {
            if (!IsCommonOkay())
                return null;

            if (cmbEcTargetExe.SelectedItem == null || cmbEcTargetExe.SelectedItem.ToString().Length == 0)
            {
                MessageBox.Show("Please specify a target executable");
                return null;
            }

            if (tbEcByteString.Text.Length == 0)
            {
                MessageBox.Show("Please specify a byte string");
                return null;
            }

            if (nudEcLowPage.Value == nudEcHighPage.Value)
            {
                MessageBox.Show("Low Page cannot equal High Page");
                return null;
            }

            String args = "";
            args += " -type exectuablechecker";
            args += " -actionFlags " + GetActionFlags().ToString();
            args += " -bytestring \"" + tbEcByteString.Text + "\"";
            args += " -lowPageNumber " + nudEcLowPage.Value.ToString();
            args += " -highPageNumber " + nudEcHighPage.Value.ToString();
            args += " -targetExecutable " + cmbEcTargetExe.SelectedItem.ToString();
            args += " -sku " + GetSku();

            if (cbPrint.Checked)
            {
                args += " -printmatchedblocks";
            }
            return args;
        }

        private String DllNameArgs()
        {

            if (!IsCommonOkay())
                return null;

            String args = "";

            args += " -type dllname";
            args += " -actionFlags " + GetActionFlags().ToString();
            //args += " -versionNumber " + nudVersion.Value.ToString();
            args += " -dllname " + tbDllName.Text;
            args += " -sku " + GetSku();

            return args;
            
        }
        private void button1_Click(object sender, EventArgs e)
        {
            
            // Time to do work
            // First, determine which page we're on.
            int tabIdx = tabControl1.SelectedIndex;
            switch (tabIdx)
            {
                case 0:
                    {
                        ExecuteCommand(DllNameArgs());
                        break;
                    }
                case 1:
                    {
                        ExecuteCommand(ByteStringArgs());
                        break;
                    }
                case 2:
                    {
                        ExecuteCommand(PagedByteStringArgs());
                        break;
                    }
                case 3:
                    {
                        ExecuteCommand(ExecutableCheckerArgs());
                        break;
                    }
                default:
                    break;
            }
           
        }

        //private void RSASignPageRichTextBox(ref string outputBuff, ref int ouputCount)
        //{
        //    // Do some very basic version checking
        //    Int32 versionEnd = (int)nudVersionEnd.Value;
        //    Int32 versionStart = (int)nudVersion.Value;
        //    const uint magicXor = 0xb7ac4b1c;

        //    if (versionStart != versionEnd)
        //    {

        //        MessageBox.Show("Start Version != End Version. Signing, and run-time behaviour *will* eventually fail.");
        //        return;
        //    }
        //    // Pull the text
        //    string hashes = rtbHashesToSign.Text;
        //    // Allocate our output buffer
        //    MemoryStream buildBuff = new MemoryStream();
        //    // Iterate over each line
        //    BinaryWriter writer = new BinaryWriter(buildBuff);
        //    writer.Write(versionStart);
        //    using (StringReader reader = new StringReader(rtbHashesToSign.Text))
        //    {
        //        string line = string.Empty;
        //        do
        //        {
        //            // Read and trim
        //            line = reader.ReadLine();
        //            if (line != null)
        //            {
        //                // First lets store a temp copy
        //                string t = line.Trim();
        //                // Trim it accordingly
        //                t = t.TrimEnd(',').TrimStart('[').TrimEnd(']');
        //                // Split it into our uints
        //                List<uint> hashParams = t.Split(',').Select(uint.Parse).ToList();
        //                // Do another basic version check to dummy proof
        //                uint currentHashVersion = ((hashParams[3] ^ hashParams[0]) ^ magicXor) & 0xFFFF;
        //                if (currentHashVersion != (uint)versionStart)
        //                {
        //                    StringBuilder sb = new StringBuilder();
        //                    sb.AppendFormat("{0} providing mismatched version ({1}) from UI-specified Version ({2})", line, currentHashVersion, versionStart);
        //                    MessageBox.Show(sb.ToString());
        //                    return;
        //                }
        //                // Write the bytes out
        //                buildBuff.Write(BitConverter.GetBytes(hashParams[0]), 0, 4);
        //                buildBuff.Write(BitConverter.GetBytes(hashParams[1]), 0, 4);
        //                buildBuff.Write(BitConverter.GetBytes(hashParams[2]), 0, 4);
        //                buildBuff.Write(BitConverter.GetBytes(hashParams[3]), 0, 4);
        //                buildBuff.Write(BitConverter.GetBytes(hashParams[4]), 0, 4);
    
        //                // Increment the number of hashes we have
        //                ouputCount++;

        //            }
        //        } while (line != null);

        //    }
        //    writer.Write(ouputCount);

        //    StringBuilder sbb = new StringBuilder();
        //    string bas = ByteArrayToString(buildBuff.GetBuffer(),buildBuff.Length);
        //    //sbb.AppendLine("Bytes:");
        //    //sbb.AppendFormat("{0}", bas);
        //    //sbb.AppendLine();
        //    // Now we're going to hash it, because hashing it creates a smaller buffer
        //    byte[] hashValue;
        //    SHA256 mySHA256 = SHA256Managed.Create();
        //    hashValue = mySHA256.ComputeHash(buildBuff.GetBuffer(), 0, (int)buildBuff.Length);
        //    sbb.Append(System.Convert.ToBase64String(hashValue, 0, hashValue.Length));
        //    outputBuff = sbb.ToString();
        //}
        public static string ByteArrayToString(byte[] ba, long Length)
        {
            StringBuilder hex = new StringBuilder((int)Length * 2);
            for (int i = 0; i < Length; i++ )
                hex.AppendFormat("{0:x2}", ba[i]);
            return hex.ToString();
        }
        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            tbAddress.Enabled = true;
            nudOffset.Enabled = true;
            tbModuleName.Enabled = true;

            ComboBox b = (ComboBox)sender;

            if (b.SelectedIndex == 0)
            {
                tbAddress.Enabled = false;
            }
            else if (b.SelectedIndex == 1)
            {
                nudOffset.Enabled = false;
                tbModuleName.Enabled = false;
            }
        }

    }
}
