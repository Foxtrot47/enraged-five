﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;

namespace qaEnvTray
{
    static class Program
    {
        public static qaEnvTrayIcon m_icon;
        public static System.Timers.Timer m_timer;
        [STAThread]
        static void Main()
        {
            Process[] pname = Process.GetProcessesByName("qaEnvTray");
            if (pname.Length > 1)
            {
                MessageBox.Show("Environment Tool Tray Already Running");
                return;
            }
            
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            // Do some checking to be sure that we're being run with admin
            // priv's - even though we should be since I set it to be in the app.manifest
            WindowsPrincipal principal = new WindowsPrincipal(WindowsIdentity.GetCurrent());
            bool administrativeMode = principal.IsInRole(WindowsBuiltInRole.Administrator);

            if (!administrativeMode)
            {
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.Verb = "runas";
                startInfo.FileName = Application.ExecutablePath;
                try
                {
                    Process.Start(startInfo);
                }
                catch
                {
                    return;
                }
                return;
            }
            // Set our polling Timer
            m_timer = new System.Timers.Timer(3500);
            // Create the event handler
            m_timer.Elapsed += OnTimedEvent;
            m_timer.AutoReset = true;
            m_timer.Enabled = true;
            // Create the basic Icon
            m_icon = new qaEnvTrayIcon();
            // Do an initial display
            m_icon.Display();
            // Run the app
            Application.Run();
        }

        private static void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            m_icon.UpdateDisplay();
        }
    }
}
