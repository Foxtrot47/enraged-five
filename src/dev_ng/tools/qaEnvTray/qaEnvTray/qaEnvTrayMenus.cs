﻿using qaEnvTray.Properties;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace qaEnvTray
{
    class qaEnvTrayMenus
    {
        public ContextMenuStrip Create()
        {
            // Add the default menu options.
            ContextMenuStrip ctxMenu = new ContextMenuStrip();
            ToolStripMenuItem item;

            // Pointing to Stage Prod
            item = new ToolStripMenuItem();
            item.Text = "Tray Tool 0.1";
            ctxMenu.Items.Add(item);

            // Create a separator
            ToolStripSeparator sep;
            sep = new ToolStripSeparator();
            ctxMenu.Items.Add(sep);

            // Pointing to Stage Prod
            item = new ToolStripMenuItem();
            item.Text = "Prod -> Prod";
            item.Click += new System.EventHandler(onSetToProd);
            ctxMenu.Items.Add(item);

            // Pointing to Stage Prod
            item = new ToolStripMenuItem();
            item.Text = "Prod -> Stage Prod";
            item.Click += new System.EventHandler(onSetToStageProd);
            ctxMenu.Items.Add(item);

            // Pointing to Stage Prod
            item = new ToolStripMenuItem();
            item.Text = "Prod -> Dev";
            item.Click += new System.EventHandler(onSetToDev);
            ctxMenu.Items.Add(item);

            // Create a separator
            sep = new ToolStripSeparator();
            ctxMenu.Items.Add(sep);

            // Creating the exit button
            item = new ToolStripMenuItem();
            item.Text = "Exit";
            item.Click += new System.EventHandler(onExit);
            ctxMenu.Items.Add(item);

            return ctxMenu;
        }

        void onExit(object sender, EventArgs e)
        {
            // Quit without further ado.
            Application.Exit();
        }

        void onSetToProd(object sender, EventArgs e)
        {
            string systemRoot = Environment.GetEnvironmentVariable("SystemRoot");
            string hostsPath = systemRoot + @"\system32\drivers\etc\hosts";
            string newContents = getNewHostsContent("", hostsPath);
            System.IO.File.WriteAllText(hostsPath, newContents);
            return;
        }

        void onSetToDev(object sender, EventArgs e)
        {
            string systemRoot = Environment.GetEnvironmentVariable("SystemRoot");
            string hostsPath = systemRoot + @"\system32\drivers\etc\hosts";
            string newContents = getNewHostsContent(ConfigurationManager.AppSettings["devip"], hostsPath);
            System.IO.File.WriteAllText(hostsPath, newContents);
            return;
        }

        void onSetToStageProd(object sender, EventArgs e)
        {
            string systemRoot = Environment.GetEnvironmentVariable("SystemRoot");
            string hostsPath = systemRoot + @"\system32\drivers\etc\hosts";
            string newContents = getNewHostsContent(ConfigurationManager.AppSettings["stageprodip"], hostsPath);
            System.IO.File.WriteAllText(hostsPath, newContents);
            return;
        }

        string getNewHostsContent(string newLocation, string hostsPath)
        {
            string[] readLines = System.IO.File.ReadAllLines(hostsPath);
            StringBuilder output = new StringBuilder();

            bool conductorFound = false;
            bool prodFound  = false;
            bool authFound  = false;
            foreach (string line in readLines)
            {
                if (line.Contains("conductor-prod.ros.rockstargames.com"))
                {
                    // Handle the scenario where we're bringing prod back to prod 
                    if (newLocation.Length > 0)
                    {
                        string replaced = newLocation + "\tconductor-prod.ros.rockstargames.com";
                        output.AppendLine(replaced);
                    }
                    conductorFound = true;
                }
                else if (line.Contains("auth-prod.ros.rockstargames.com"))
                {
                    // Handle the scenario where we're bringing prod back to prod 
                    if (newLocation.Length > 0)
                    {
                        string replaced = newLocation + "\tauth-prod.ros.rockstargames.com";
                        output.AppendLine(replaced);
                    }
                    authFound = true;
                }
                else if (line.Contains("prod.ros.rockstargames.com"))
                {
                    // Handle the scenario where we're bringing prod back to prod 
                    if (newLocation.Length > 0)
                    {
                        string replaced = newLocation + "\tprod.ros.rockstargames.com";
                        output.AppendLine(replaced);
                    }
                    prodFound = true;
                }
                else
                {
                    // Other entries, just append as usual
                    output.AppendLine(line);
                }
            }

            if(!conductorFound)
            {
                string addn = newLocation + "\tconductor-prod.ros.rockstargames.com";
                output.AppendLine(addn);
            }
            if (!prodFound)
            {
                string addn = newLocation + "\tprod.ros.rockstargames.com"; 
                output.AppendLine(addn);
            }
            if (!authFound)
            {
                string addn = newLocation + "\tauth-prod.ros.rockstargames.com";
                output.AppendLine(addn);
            }
            return output.ToString();
        }

    }
}
