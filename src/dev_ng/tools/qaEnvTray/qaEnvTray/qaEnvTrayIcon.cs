﻿using qaEnvTray.Properties;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
namespace qaEnvTray
{
    class qaEnvTrayIcon : IDisposable
	{
		NotifyIcon ni;

        public qaEnvTrayIcon()
		{
			// Instantiate the NotifyIcon object.
			ni = new NotifyIcon();
		}

		public void Display()
		{
			// Put the icon in the system tray and allow it react to mouse clicks.			
            UpdateDisplay();
            ni.Visible = true;
			// Attach a context menu.
			ni.ContextMenuStrip = new qaEnvTrayMenus().Create();
		}
        public void UpdateDisplay()
        {
            IPAddress addr = Dns.GetHostEntry("prod.ros.rockstargames.com").AddressList.First();
            if (addr.Equals(IPAddress.Parse(ConfigurationManager.AppSettings["prodip"])))
            {
                ni.Text = "Prod pointing to real-prod";
                ni.Icon = Resources.prod;
            }
            else if (addr.Equals(IPAddress.Parse(ConfigurationManager.AppSettings["stageprodip"])))
            {
                ni.Text = "Prod pointing to stage-prod";
                ni.Icon = Resources.stageprod;
            }
            else if (addr.Equals(IPAddress.Parse(ConfigurationManager.AppSettings["devip"])))
            {
                ni.Text = "Prod pointing to dev";
                ni.Icon = Resources.dev;
            }

            return;
        }
        public void Dispose(){ni.Dispose();}
    }
}
