// =========================
// GeometryCollectorTool.cpp
// =========================

/*
loaded prop group "trees"
  Prop_W_R_Cedar_01 (radius=10.000000)
  Prop_Tree_Cedar_03 (radius=11.000000)
  Prop_Tree_Eng_Oak_01 (radius=12.000000)
  Prop_Tree_Pine_01 (radius=12.000000)
loaded prop group "large_bushes"
  Prop_Bush_Lrg_04b (radius=12.000000)
  Prop_Bush_Lrg_01b (radius=6.000000)
  Prop_Bush_Lrg_02b (radius=10.000000)
  Prop_Bush_Lrg_02 (radius=10.000000)
  Prop_Bush_Lrg_01 (radius=6.000000)
  Prop_Bush_Lrg_04d (radius=16.000000)
  Prop_Bush_Lrg_01d (radius=6.000000)
  Prop_Desert_Iron_01 (radius=8.500000)
loaded prop group "bushes"
  Prop_Bush_Med_03 (radius=2.500000)
  Prop_Bush_Med_02 (radius=2.000000)
  Prop_Bush_Med_01 (radius=2.000000)
  Prop_Skunk_Bush_01 (radius=1.750000)
  Prop_Bush_Med_06 (radius=3.000000)
  Prop_Bush_Med_07 (radius=3.000000)
  Prop_Creosote_B_01 (radius=2.500000)
  Prop_Plant_Group_06a (radius=9.000000)
  Prop_Plant_Group_06b (radius=10.000000)
  Prop_Plant_Group_06c (radius=10.000000)
loaded prop group "fences"
  Prop_Fnc_Farm_01c (radius=0.000000)
  Prop_Fnc_Farm_01d (radius=0.000000)
  Prop_Fnc_Farm_01e (radius=0.000000)
  Prop_Fnc_Farm_01f (radius=0.000000)
  Prop_Fnc_OMesh_01a (radius=0.000000)
  Prop_Fnc_OMesh_02a (radius=0.000000)
  Prop_Fnc_OMesh_03a (radius=0.000000)
  Prop_FncWood_09a (radius=0.000000)
  Prop_FncWood_09b (radius=0.000000)
  Prop_FncWood_09c (radius=0.000000)
  Prop_FncWood_09d (radius=0.000000)
  Prop_FncLink_01a (radius=0.000000)
  Prop_FncLink_01e (radius=0.000000)
  Prop_FncLink_02a (radius=0.000000)
  Prop_FncLink_02f (radius=0.000000)
  Prop_FncLink_02n (radius=0.000000)
  Prop_FncLink_02o (radius=0.000000)
  Prop_FncLink_02p (radius=0.000000)
loaded prop group "props"
  Prop_Telegraph_01b (radius=2.500000)
  Prop_Telegraph_01f (radius=2.500000)
loaded prop group "rocks"
  CS_X_RubSmlB (radius=3.500000)
  CS_X_RubSmlC (radius=2.500000)
  CS_X_RubSmlD (radius=2.500000)
  CS_X_RubmedD (radius=4.500000)
  CS_X_RubmedE (radius=5.000000)
processing x:/gta5/assets/non_final/geometry_test3/collected/water_foam.obj ..
processing 784 lines .. 0 triangles.
processing x:/gta5/assets/non_final/geometry_test3/collected/water_fountain.obj ..
processing 784 lines .. 7724 triangles.
processing x:/gta5/assets/non_final/geometry_test3/collected/water_river.obj ..
processing 784 lines .. 19083 triangles.
processing x:/gta5/assets/non_final/geometry_test3/collected/water_riverfoam.obj ..
processing 784 lines .. 17030 triangles.
processing x:/gta5/assets/non_final/geometry_test3/collected/water_riverlod.obj ..
processing 784 lines .. 14345 triangles.
processing x:/gta5/assets/non_final/geometry_test3/collected/water_riverocean.obj ..
processing 784 lines .. 476 triangles.
processing x:/gta5/assets/non_final/geometry_test3/collected/water_rivershallow.obj ..
processing 784 lines .. 2552 triangles.
processing x:/gta5/assets/non_final/geometry_test3/collected/water_shallow.obj ..
processing 784 lines .. 28478 triangles. (1.35 secs, total 4.37 secs)
processing x:/gta5/assets/non_final/geometry_test3/collected/water_terrainfoam.obj ..
processing 784 lines .. 98540 triangles. (4.50 secs, total 8.88 secs)
processing x:/gta5/assets/non_final/geometry_test3/collected/water_reflective_exterior.obj ..
processing 784 lines .. 43364 triangles. (2.14 secs, total 11.02 secs)
processing x:/gta5/assets/non_final/geometry_test3/collected/water_reflective_interior.obj ..
processing 784 lines .. 816 triangles.
processing x:/gta5/assets/non_final/geometry_test3/collected/water_lod.obj ..
processing 784 lines .. 24077 triangles. (1.19 secs, total 12.26 secs)
processing x:/gta5/assets/non_final/geometry_test3/collected/mirror_default.obj ..
processing 187 lines .. 142 triangles.
processing x:/gta5/assets/non_final/geometry_test3/collected/mirror_crack.obj ..
processing 187 lines .. 11 triangles.
processing x:/gta5/assets/non_final/geometry_test3/collected/mirror_decal.obj ..
processing 187 lines .. 9868 triangles.
processing x:/gta5/assets/non_final/geometry_test3/collected/mirror_all.obj ..
processing 187 lines .. 10021 triangles.
processing x:/gta5/assets/non_final/geometry_test3/collected/cable_exterior.obj ..
processing 3075 lines .. 330557 triangles. (16.58 secs, total 29.79 secs)
processing x:/gta5/assets/non_final/geometry_test3/collected/cable_interior.obj ..
processing 3075 lines .. 24755 triangles. (1.38 secs, total 31.17 secs)
processing x:/gta5/assets/non_final/geometry_test3/collected_lights/exterior ..
processing 32417 lines .. 47680320 triangles, 48314258 assigned, 1233 tiles. (32.63 mins, total 33.15 mins)
processing x:/gta5/assets/non_final/geometry_test3/collected_lights/interior ..
processing 32417 lines .. 8817600 triangles, 8886268 assigned, 255 tiles. (5.50 mins, total 38.65 mins)
processing x:/gta5/assets/non_final/geometry_test3/collected_propgroup/trees ..
processing 314715 lines .. 495936 triangles, 543264 assigned, 972 tiles. (54.93 secs, total 39.57 mins)
processing x:/gta5/assets/non_final/geometry_test3/collected_propgroup/trees_geom ..
processing 314715 lines .. 8026775 triangles, 8117346 assigned, 942 tiles. (8.20 mins, total 47.77 mins)
processing x:/gta5/assets/non_final/geometry_test3/collected_propgroup/large_bushes ..
processing 314715 lines .. 2830912 triangles, 3025182 assigned, 2044 tiles. (4.38 mins, total 52.16 mins)
processing x:/gta5/assets/non_final/geometry_test3/collected_propgroup/large_bushes_geom ..
processing 314715 lines .. 32220973 triangles, 32619222 assigned, 2013 tiles. (32.50 mins, total 84.66 mins)
processing x:/gta5/assets/non_final/geometry_test3/collected_propgroup/bushes ..
processing 314715 lines .. 6046944 triangles, 6199713 assigned, 2043 tiles. (7.36 mins, total 92.02 mins)
processing x:/gta5/assets/non_final/geometry_test3/collected_propgroup/bushes_geom ..
processing 314715 lines .. 40414518 triangles, 40754391 assigned, 2034 tiles. (41.07 mins, total 2.22 hours)
processing x:/gta5/assets/non_final/geometry_test3/collected_propgroup/fences ..
processing 314715 lines .. 1818236 triangles, 1825559 assigned, 387 tiles. (1.86 mins, total 2.25 hours)
processing x:/gta5/assets/non_final/geometry_test3/collected_propgroup/props ..
processing 314715 lines .. 119872 triangles, 122410 assigned, 566 tiles. (16.13 secs, total 2.25 hours)
processing x:/gta5/assets/non_final/geometry_test3/collected_propgroup/props_geom ..
processing 314715 lines .. 945630 triangles, 947882 assigned, 561 tiles. (67.42 secs, total 2.27 hours)
processing x:/gta5/assets/non_final/geometry_test3/collected_propgroup/rocks ..
processing 314715 lines .. 271904 triangles, 279368 assigned, 379 tiles. (32.60 secs, total 2.28 hours)
processing x:/gta5/assets/non_final/geometry_test3/collected_propgroup/rocks_geom ..
processing 314715 lines .. 837395 triangles, 840959 assigned, 374 tiles. (65.14 secs, total 2.30 hours)
done (2.30 hours).
Press any key to continue . . .
*/

#include "../../common/common.h"

#include "../../common/fileutil.h"
#include "../../common/mathutil.h"
#include "../../common/progressdisplay.h"
#include "../../common/stringutil.h"

#include <direct.h>
#include <time.h>

#include "../../../rage/framework/src/fwgeovis/geovis.h"

#define DO_EXPORT_WATER      (1)
#define DO_EXPORT_MIRROR     (1)
#define DO_EXPORT_CABLE      (1)
#define DO_EXPORT_LIGHTS     (1)
#define DO_EXPORT_CORONAS    (1)
#define DO_EXPORT_PROPGROUPS (1)

// this is a way to limit the output for quick testing
#define DO_EXPORT_REGION_MIN_X (-FLT_MAX)
#define DO_EXPORT_REGION_MIN_Y (-FLT_MAX)
#define DO_EXPORT_REGION_MAX_X (+FLT_MAX)
#define DO_EXPORT_REGION_MAX_Y (+FLT_MAX)

static const char* g_collectorGeometryDir = RS_ASSETS "/non_final/geometry";

#define NUM_SLICES 32
#define NUM_STACKS 18

class CTriangle
{
public:
	CTriangle() {}
	CTriangle(const Vec3& p0, const Vec3& p1, const Vec3& p2) { p[0] = p0; p[1] = p1; p[2] = p2; }

	Vec3 p[3];
};

static void ExportLathe(std::vector<CTriangle>& triangles, const Vec3& centre, const Vec3& basisX, const Vec3& basisY, const Vec3& basisZ, const float r[NUM_STACKS + 1], const float d[NUM_STACKS + 1])
{
	static float c[NUM_SLICES + 1] = {0.0f};
	static float s[NUM_SLICES + 1];

	if (c[0] == 0.0f)
	{
		for (int i = 0; i < NUM_SLICES; i++)
		{
			c[i] = cosf(2.0f*PI*(float)i/(float)NUM_SLICES);
			s[i] = sinf(2.0f*PI*(float)i/(float)NUM_SLICES);
		}

		c[NUM_SLICES] = c[0];
		s[NUM_SLICES] = s[0];
	}

	for (int j = 0; j < NUM_STACKS; j++)
	{
		const float r0 = r[j + 0];
		const float d0 = d[j + 0];
		const float r1 = r[j + 1];
		const float d1 = d[j + 1];

		for (int i = 0; i < NUM_SLICES; i++)
		{
			const float c0 = c[i + 0];
			const float s0 = s[i + 0];
			const float c1 = c[i + 1];
			const float s1 = s[i + 1];

			const float x00 = centre.x + (c0*basisX.x + s0*basisY.x)*r0 + basisZ.x*d0;
			const float y00 = centre.y + (c0*basisX.y + s0*basisY.y)*r0 + basisZ.y*d0;
			const float z00 = centre.z + (c0*basisX.z + s0*basisY.z)*r0 + basisZ.z*d0;
			const Vec3 p00(x00, y00, z00);

			const float x10 = centre.x + (c1*basisX.x + s1*basisY.x)*r0 + basisZ.x*d0;
			const float y10 = centre.y + (c1*basisX.y + s1*basisY.y)*r0 + basisZ.y*d0;
			const float z10 = centre.z + (c1*basisX.z + s1*basisY.z)*r0 + basisZ.z*d0;
			const Vec3 p10(x10, y10, z10);

			const float x01 = centre.x + (c0*basisX.x + s0*basisY.x)*r1 + basisZ.x*d1;
			const float y01 = centre.y + (c0*basisX.y + s0*basisY.y)*r1 + basisZ.y*d1;
			const float z01 = centre.z + (c0*basisX.z + s0*basisY.z)*r1 + basisZ.z*d1;
			const Vec3 p01(x01, y01, z01);

			const float x11 = centre.x + (c1*basisX.x + s1*basisY.x)*r1 + basisZ.x*d1;
			const float y11 = centre.y + (c1*basisX.y + s1*basisY.y)*r1 + basisZ.y*d1;
			const float z11 = centre.z + (c1*basisX.z + s1*basisY.z)*r1 + basisZ.z*d1;
			const Vec3 p11(x11, y11, z11);

			triangles.push_back(CTriangle(p00, p10, p11));
			triangles.push_back(CTriangle(p00, p11, p01));
		}
	}
}

static void ExportSphere(std::vector<CTriangle>& triangles, const Vec3& centre, float radius)
{
	float r[NUM_STACKS + 1];
	float d[NUM_STACKS + 1];

	for (int j = 0; j <= NUM_STACKS; j++)
	{
		const float theta = PI*(1.0f - (float)j/(float)NUM_STACKS);
		r[j] = sinf(theta)*radius*-1.0f;
		d[j] = cosf(theta)*radius;
	}

	const Vec3 basisX(1.0f, 0.0f, 0.0f);
	const Vec3 basisY(0.0f, 1.0f, 0.0f);
	const Vec3 basisZ(0.0f, 0.0f, 1.0f);

	ExportLathe(triangles, centre, basisX, basisY, basisZ, r, d);
}

static void ExportCone(std::vector<CTriangle>& triangles, const Vec3& centre, const Vec3& basisX, const Vec3& basisY, const Vec3& basisZ, float radius, float angle)
{
	float r[NUM_STACKS + 1];
	float d[NUM_STACKS + 1];

	r[0] = 0.0f;
	d[0] = 0.0f;

	for (int j = 1; j <= NUM_STACKS; j++)
	{
		const float theta = (angle*PI/180.0f)*(1.0f - (float)(j - 1)/(float)(NUM_STACKS - 1));
		r[j] = sinf(theta)*radius;
		d[j] = cosf(theta)*radius;
	}

	ExportLathe(triangles, centre, basisX, basisY, basisZ, r, d);
}

static void ExportCapsule(std::vector<CTriangle>& triangles, const Vec3& centre, const Vec3& basisX, const Vec3& basisY, const Vec3& basisZ, float radius, float extent)
{
	float r[NUM_STACKS + 1];
	float d[NUM_STACKS + 1];

	for (int j = 0; j <= NUM_STACKS; j++)
	{
		const float theta = PI*(1.0f - (float)j/(float)NUM_STACKS);
		r[j] = sinf(theta)*radius;
		d[j] = cosf(theta)*radius;

		if      (2*j < NUM_STACKS) { d[j] -= extent; }
		else if (2*j > NUM_STACKS) { d[j] += extent; }
	}

	ExportLathe(triangles, centre, basisX, basisY, basisZ, r, d);
}

// NOTE -- this must be kept in sync with x:\gta5\src\dev\game\renderer\Lights\LightSource.h
#define LIGHTFLAG_INTERIOR_ONLY					(1<<0)
#define LIGHTFLAG_EXTERIOR_ONLY					(1<<1)
#define LIGHTFLAG_DONT_USE_IN_CUTSCENE			(1<<2)
#define LIGHTFLAG_VEHICLE						(1<<3)
#define LIGHTFLAG_FX							(1<<4)
#define LIGHTFLAG_TEXTURE_PROJECTION			(1<<5)
#define LIGHTFLAG_CAST_SHADOWS					(1<<6)
#define LIGHTFLAG_CAST_STATIC_GEOM_SHADOWS		(1<<7)
#define LIGHTFLAG_CAST_DYNAMIC_GEOM_SHADOWS		(1<<8)
#define LIGHTFLAG_CALC_FROM_SUN					(1<<9)
#define LIGHTFLAG_ENABLE_BUZZING				(1<<10)
#define LIGHTFLAG_FORCE_BUZZING					(1<<11)
#define LIGHTFLAG_DRAW_VOLUME					(1<<12)
#define LIGHTFLAG_NO_SPECULAR					(1<<13)
#define LIGHTFLAG_BOTH_INTERIOR_AND_EXTERIOR	(1<<14)
#define LIGHTFLAG_CORONA_ONLY					(1<<15)
#define LIGHTFLAG_NOT_IN_REFLECTION				(1<<16)
#define LIGHTFLAG_ONLY_IN_REFLECTION			(1<<17)
#define LIGHTFLAG_USE_CULL_PLANE				(1<<18)
#define LIGHTFLAG_USE_VOLUME_OUTER_COLOUR		(1<<19)
#define LIGHTFLAG_CAST_HIGHER_RES_SHADOWS		(1<<20) // bump the shadow cache priority for this light so it will use hires shadow maps sooner than it's screen space size would suggest
#define LIGHTFLAG_CAST_ONLY_LOWRES_SHADOWS		(1<<21) // never use hires shadow map for this light, even if it's really up close
#define	LIGHTFLAG_FAR_LOD_LIGHT					(1<<22)
#define	LIGHTFLAG_DONT_LIGHT_ALPHA				(1<<23)
#define LIGHTFLAG_CAST_SHADOWS_IF_POSSIBLE		(1<<24)
#define LIGHTFLAG_CUTSCENE						(1<<25)
#define LIGHTFLAG_MOVING_LIGHT_SOURCE			(1<<26) // runtime flag that lets the shadow code know this light moves a lot and might self shadow, to it needs to update late (with the current frames light position)
#define LIGHTFLAG_USE_VEHICLE_TWIN				(1<<27) // used for detecting special twin lights.
#define LIGHTFLAG_FORCE_MEDIUM_LOD_LIGHT		(1<<28)
#define LIGHTFLAG_CORONA_ONLY_LOD_LIGHT			(1<<29)
#define LIGHTFLAG_DELAY_RENDER					(1<<30) // used by cutscenes to create shadow casting lights a frame early and avoid any visible pops

enum
{
	EXPORT_ALLOW_INTERIOR    = BIT(0),
	EXPORT_ALLOW_EXTERIOR    = BIT(1),
	EXPORT_ALLOW_HD          = BIT(2),
	EXPORT_ALLOW_LOD         = BIT(3),
	EXPORT_ALLOW_SCRIPT      = BIT(5),
	EXPORT_ALLOW_NONSCRIPT   = BIT(6),
	EXPORT_LIGHTS            = BIT(7),
	EXPORT_CORONAS_AS_QUADS  = BIT(8),
	EXPORT_CORONAS_AS_BOXES  = BIT(9),

	EXPORT_ALLOW_ALL = EXPORT_ALLOW_INTERIOR | EXPORT_ALLOW_EXTERIOR | EXPORT_ALLOW_HD | EXPORT_ALLOW_LOD | EXPORT_ALLOW_SCRIPT | EXPORT_ALLOW_NONSCRIPT,
};

#define GRID_MIN_X     (-6000)
#define GRID_MIN_Y     (-6000)
#define GRID_MAX_X     (12000)
#define GRID_MAX_Y     (12000)
#define GRID_TILE_SIZE (150)
#define GRID_TILE_STEP (3)
#define GRID_W         ((GRID_MAX_X - GRID_MIN_X)/GRID_TILE_SIZE)
#define GRID_H         ((GRID_MAX_Y - GRID_MIN_Y)/GRID_TILE_SIZE)

class gctTarget
{
public:
	virtual bool ExportStart(const char* path) = 0;
	virtual void ExportTriangle(const char* path, const Vec3& p0, const Vec3& p1, const Vec3& p2, const char* comment = "") = 0;
	virtual void ExportFinish(const char* path, char* finishMsg = NULL) = 0;

	void ExportQuad(const char* path, const Vec3& p0, const Vec3& p1, const Vec3& p2, const Vec3& p3, const char* comment = "")
	{
		ExportTriangle(path, p0, p1, p2, comment);
		ExportTriangle(path, p0, p2, p3, comment);
	}

	void ExportOBJ(
		const class gctExportObject& object,
		const char* sourcePath,
		float radius,
		const Vec3& transform_col0,
		const Vec3& transform_col1,
		const Vec3& transform_col2,
		const Vec3& transform_col3,
		float transform_scaleXY,
		float transform_scaleZ);
};

class gctOBJ : public gctTarget
{
public:
	gctOBJ()
	{
		m_obj = NULL;
		m_numTriangles = 0;
	}

	virtual bool ExportStart(const char* path)
	{
		m_obj = fopen(path, "w");
		return m_obj != NULL;
	}

	virtual void ExportTriangle(const char* path, const Vec3& p0, const Vec3& p1, const Vec3& p2, const char* comment = "")
	{
		if (m_obj)
		{
			fprintf(m_obj, "v %f %f %f%s\n", p0.x, p0.y, p0.z, comment);
			fprintf(m_obj, "v %f %f %f%s\n", p1.x, p1.y, p1.z, comment);
			fprintf(m_obj, "v %f %f %f%s\n", p2.x, p2.y, p2.z, comment);
			m_numTriangles++;
		}
	}

	virtual void ExportFinish(const char* path, char* finishMsg = NULL)
	{
		if (m_obj)
		{
			for (int i = 0; i < m_numTriangles; i++)
			{
				fprintf(m_obj, "f %d %d %d\n", 1 + 3*i, 2 + 3*i, 3 + 3*i);
			}

			fclose(m_obj);
			m_obj = NULL;

			if (finishMsg)
			{
				sprintf(finishMsg, "%d triangles", m_numTriangles);
			}
		}
		else
		{
			if (finishMsg)
			{
				strcpy(finishMsg, "failed!");
			}
		}

		m_numTriangles = 0;
	}

	FILE* m_obj;
	int   m_numTriangles;
};

class gctOBJGrid : public gctTarget
{
public:
	gctOBJGrid()
	{
		m_numTrianglesPerTile = new int[GRID_W*GRID_H];
		memset(m_numTrianglesPerTile, 0, GRID_W*GRID_H*sizeof(int));

		m_currentTileFile        = NULL;
		m_currentTileCoordI      = -1;
		m_currentTileCoordJ      = -1;
		m_totalTriangles         = 0;
		m_totalTrianglesAssigned = 0;
		m_totalTilesCreated      = 0;
	}

	virtual bool ExportStart(const char*)
	{
		return true;
	}

	virtual void ExportTriangle(const char* path, const Vec3& p0, const Vec3& p1, const Vec3& p2, const char* comment = "")
	{
		const float pminx = Min<float>(p0.x, p1.x, p2.x);
		const float pminy = Min<float>(p0.y, p1.y, p2.y);
		const float pmaxx = Max<float>(p0.x, p1.x, p2.x);
		const float pmaxy = Max<float>(p0.y, p1.y, p2.y);

		const int i0 = Max<int>(0,      (int)floorf((pminx - (float)GRID_MIN_X)/(float)GRID_TILE_SIZE));
		const int j0 = Max<int>(0,      (int)floorf((pminy - (float)GRID_MIN_Y)/(float)GRID_TILE_SIZE));
		const int i1 = Min<int>(GRID_W, (int)ceilf ((pmaxx - (float)GRID_MIN_X)/(float)GRID_TILE_SIZE));
		const int j1 = Min<int>(GRID_H, (int)ceilf ((pmaxy - (float)GRID_MIN_Y)/(float)GRID_TILE_SIZE));

		for (int j = j0; j < j1; j++)
		{
			for (int i = i0; i < i1; i++)
			{
				char tilepath[512] = "";
				sprintf(tilepath, "%s/tile_%s.obj", path, gv::GetTileNameFromCoords(i*GRID_TILE_STEP, j*GRID_TILE_STEP));
				int& numTriangles = m_numTrianglesPerTile[i + j*GRID_W];

				if (m_currentTileCoordI != i ||
					m_currentTileCoordJ != j)
				{
					if (m_currentTileFile)
					{
						fclose(m_currentTileFile);
					}

					if (numTriangles == 0)
					{
						m_totalTilesCreated++;
					}

					m_currentTileFile = fopen(tilepath, numTriangles ? "a" : "w");
					m_currentTileCoordI = i;
					m_currentTileCoordJ = j;
				}

				if (m_currentTileFile)
				{
					fprintf(m_currentTileFile, "v %f %f %f%s\n", p0.x, p0.y, p0.z, comment);
					fprintf(m_currentTileFile, "v %f %f %f%s\n", p1.x, p1.y, p1.z, comment);
					fprintf(m_currentTileFile, "v %f %f %f%s\n", p2.x, p2.y, p2.z, comment);
					numTriangles++;
					m_totalTrianglesAssigned++;
				}
			}
		}

		m_totalTriangles++;
	}

	virtual void ExportFinish(const char* path, char* finishMsg = NULL)
	{
		if (m_currentTileFile)
		{
			fclose(m_currentTileFile);
			m_currentTileFile = NULL;
		}

		m_currentTileCoordI = -1;
		m_currentTileCoordJ = -1;

		for (int j = 0; j < GRID_H; j++)
		{
			for (int i = 0; i < GRID_W; i++)
			{
				const int numTriangles = m_numTrianglesPerTile[i + j*GRID_W];

				if (numTriangles > 0)
				{
					char tilepath[512] = "";
					sprintf(tilepath, "%s/tile_%s.obj", path, gv::GetTileNameFromCoords(i*GRID_TILE_STEP, j*GRID_TILE_STEP));
					FILE* tile = fopen(tilepath, "a");

					if (tile)
					{
						for (int k = 0; k < numTriangles; k++)
						{
							fprintf(tile, "f %d %d %d\n", 1 + 3*k, 2 + 3*k, 3 + 3*k);
						}

						fclose(tile);
					}
				}
			}
		}

		delete[] m_numTrianglesPerTile;
		m_numTrianglesPerTile = NULL;

		if (finishMsg)
		{
			sprintf(finishMsg, "%d triangles, %d assigned, %d tiles", m_totalTriangles, m_totalTrianglesAssigned, m_totalTilesCreated);
		}
	}

	int*  m_numTrianglesPerTile;
	FILE* m_currentTileFile;
	int   m_currentTileCoordI;
	int   m_currentTileCoordJ;
	int   m_totalTriangles;
	int   m_totalTrianglesAssigned;
	int   m_totalTilesCreated;
};

class gctExportObject
{
public:
	gctExportObject() {}
	gctExportObject(gctTarget* target, const char* path, const char* base, const char* shaderName, u32 flags, int propGroupIndex = INDEX_NONE, float coronaSize = 0.0f, u32 lightFlags = ~0U, const char* lightTypeStr = NULL)
		: m_target(target)
		, m_path(varString("%s/processed/%s/%s", g_collectorGeometryDir, base, path))
		, m_sourceDir(varString("%s/models/%s", g_collectorGeometryDir, base))
		, m_flags(flags)
		, m_propGroupIndex(propGroupIndex)
		, m_coronaSize(coronaSize)
		, m_lightFlags(lightFlags)
		, m_lightTypeStr(lightTypeStr)
	{
		if (shaderName)
		{
			m_shaderNames.push_back(shaderName);
		}

		if (strstr(m_path.c_str(), ".obj") == NULL)
		{
			_mkdir(m_path.c_str());
		}
	}

	gctTarget*               m_target;
	std::string              m_path; // output OBJ file path, or directory for output OBJ tiles
	std::string              m_sourceDir; // directory where _entitylist.txt and source OBJs are found
	std::vector<std::string> m_shaderNames; // shader names to include, or none to include all shaders
	std::vector<std::string> m_drawableNames; // drawable names to include, or none to include all drawables
	u32                      m_flags;
	int                      m_propGroupIndex;
	float                    m_coronaSize;
	u32                      m_lightFlags;
	const char*              m_lightTypeStr;
};

void gctTarget::ExportOBJ(
	const class gctExportObject& object,
	const char* sourcePath,
	float radius,
	const Vec3& transform_col0,
	const Vec3& transform_col1,
	const Vec3& transform_col2,
	const Vec3& transform_col3,
	float transform_scaleXY,
	float transform_scaleZ)
{
	const char* path = object.m_path.c_str();

	if (radius > 0.0f)
	{
		const float x0 = transform_col3.x;
		const float y0 = transform_col3.y;
		const float z0 = transform_col3.z;

		for (int i = 0; i < NUM_SLICES; i++)
		{
			const float x1 = x0 + cosf(2.0f*PI*(float)(i + 0)/(float)NUM_SLICES)*transform_scaleXY*radius;
			const float y1 = y0 + sinf(2.0f*PI*(float)(i + 0)/(float)NUM_SLICES)*transform_scaleXY*radius;
			const float z1 = z0;
			const float x2 = x0 + cosf(2.0f*PI*(float)(i + 1)/(float)NUM_SLICES)*transform_scaleXY*radius;
			const float y2 = y0 + sinf(2.0f*PI*(float)(i + 1)/(float)NUM_SLICES)*transform_scaleXY*radius;
			const float z2 = z0;

			ExportTriangle(path, Vec3(x0, y0, z0), Vec3(x1, y1, z1), Vec3(x2, y2, z2));
		}
	}
	else
	{
		FILE* obj = fopen(sourcePath, "r");

		if (obj)
		{
			char line[512] = "";
			Vec3 v[3];
			int i = 0;

			while (ReadFileLine(line, sizeof(line), obj))
			{
				if (line[0] == 'v')
				{
					const char* s = line + strlen("v ");
					const float x = (float)atof(s);
					s = strchr(s, ' ') + 1;
					const float y = (float)atof(s);
					s = strchr(s, ' ') + 1;
					const float z = (float)atof(s);
					const Vec3 p = Transform(transform_col0, transform_col1, transform_col2, transform_col3, Vec3(x*transform_scaleXY, y*transform_scaleXY, z*transform_scaleZ));

					if (stristr(line, "[light]"))
					{
						const char* typeStr_ = strsearch(line, "type=");

						if (typeStr_ == NULL)
						{
							continue; // ?
						}

						char typeStr[512] = "";
						strcpy(typeStr, typeStr_);
						if (strchr(typeStr, ' ')) { strchr(typeStr, ' ')[0] = '\0'; }

						if (object.m_lightTypeStr && _stricmp(object.m_lightTypeStr, typeStr) != 0)
						{
							continue;
						}

						const char* flagsStr = strsearch(line, "flags=");
						const u32 flags = flagsStr ? (u32)atoi(flagsStr) : 0;

						if ((object.m_lightFlags & flags) == 0)
						{
							continue;
						}

						if (object.m_flags & EXPORT_LIGHTS)
						{
							const char* radiusStr = strsearch(line, "radius=");
							const char* angleStr  = strsearch(line, "angle=");
							const char* dirStr    = strsearch(line, "dir=");
							const char* extentStr = strsearch(line, "extent=");

							if (flags & LIGHTFLAG_CORONA_ONLY)
							{
								continue;
							}

							if (radiusStr == NULL)
							{
								continue; // ?
							}

							const float radius = (float)atof(radiusStr);
							std::vector<CTriangle> triangles;

							if (_stricmp(typeStr, "POINT") == 0)
							{
								ExportSphere(triangles, p, radius);
							}
							else
							{
								if (dirStr == NULL)
								{
									continue; // ?
								}

								const float dirX = (float)atof(dirStr); dirStr = strchr(dirStr, ','); if (dirStr) { dirStr++; } else { continue; }
								const float dirY = (float)atof(dirStr); dirStr = strchr(dirStr, ','); if (dirStr) { dirStr++; } else { continue; }
								const float dirZ = (float)atof(dirStr);

								const Vec3 basisZ = Normalise(Transform(transform_col0, transform_col1, transform_col2, Vec3(0.0f, 0.0f, 0.0f), Vec3(dirX, dirY, dirZ)));

								const float ax = Abs<float>(basisZ.x);
								const float ay = Abs<float>(basisZ.y);
								const float az = Abs<float>(basisZ.z);

								const float amin = Min<float>(ax, ay, az);

								Vec3 basisV(0.0f, 0.0f, 0.0f);

								if      (amin == ax) { basisV.x = 1.0f; }
								else if (amin == ax) { basisV.y = 1.0f; }
								else                 { basisV.z = 1.0f; }

								const Vec3 basisX = Normalise(Cross(basisZ, basisV));
								const Vec3 basisY = Normalise(Cross(basisZ, basisX));

								if (_stricmp(typeStr, "SPOT") == 0)
								{
									if (angleStr == NULL)
									{
										continue; // ?
									}

									ExportCone(triangles, p, basisX, basisY, basisZ, radius, (float)atof(angleStr));
								}
								else if (_stricmp(typeStr, "CAPSULE") == 0)
								{
									if (extentStr == NULL)
									{
										continue; // ?
									}

									ExportCapsule(triangles, p, basisX, basisY, basisZ, radius, 0.5f*(float)atof(extentStr));
								}
								else
								{
									fprintf(stderr, "unknown light type = '%s'\n", typeStr);
								}
							}

							for (int i = 0; i < (int)triangles.size(); i++)
							{
								ExportTriangle(path, triangles[i].p[0], triangles[i].p[1], triangles[i].p[2]);
							}
						}

						if (object.m_flags & (EXPORT_CORONAS_AS_QUADS | EXPORT_CORONAS_AS_BOXES))
						{
							const char* coronaSizeStr = strsearch(line, "coronaSize=");

							if (coronaSizeStr == NULL)
							{
								continue; // ?
							}

							float coronaSize = (float)atof(coronaSizeStr);

							if (coronaSize == 0.0f)
							{
								continue; // light has no corona
							}

							if (object.m_coronaSize != 0.0f)
							{
								coronaSize = object.m_coronaSize;
							}
							else
							{
								const float CORONA_SCALE = 5.0f;
								const float CORONA_UNITS = 2.5f; // legacy GTA3 stuff

								coronaSize *= CORONA_UNITS*CORONA_SCALE;
							}

							if (object.m_flags & EXPORT_CORONAS_AS_BOXES)
							{
								const Vec3 points[] =
								{
									Vec3(p.x - coronaSize, p.y - coronaSize, p.z - coronaSize),
									Vec3(p.x + coronaSize, p.y - coronaSize, p.z - coronaSize),
									Vec3(p.x - coronaSize, p.y + coronaSize, p.z - coronaSize),
									Vec3(p.x + coronaSize, p.y + coronaSize, p.z - coronaSize),
									Vec3(p.x - coronaSize, p.y - coronaSize, p.z + coronaSize),
									Vec3(p.x + coronaSize, p.y - coronaSize, p.z + coronaSize),
									Vec3(p.x - coronaSize, p.y + coronaSize, p.z + coronaSize),
									Vec3(p.x + coronaSize, p.y + coronaSize, p.z + coronaSize),
								};

								//   6---7
								//  /|  /|
								// 4---5 |
								// | | | |
								// | 2-|-3
								// |/  |/
								// 0---1

								const int quads[6][4] =
								{
									{1,5,4,0},
									{3,7,5,1},
									{2,6,7,3},
									{0,4,6,2},
									{2,3,1,0},
									{5,7,6,4},
								};

								for (int i = 0; i < NELEM(quads); i++)
								{
									ExportQuad(path, points[quads[i][0]], points[quads[i][1]], points[quads[i][2]], points[quads[i][3]], " # [box]");
								}
							}
							else // quads
							{
								const Vec3 p00(p.x - coronaSize, p.y - coronaSize, p.z);
								const Vec3 p10(p.x + coronaSize, p.y - coronaSize, p.z);
								const Vec3 p01(p.x - coronaSize, p.y + coronaSize, p.z);
								const Vec3 p11(p.x + coronaSize, p.y + coronaSize, p.z);

								ExportQuad(path, p00, p10, p11, p01, " # [quad]");
							}
						}
					}
					else // it's actually a vertex
					{
						v[i++] = p;

						if (i == 3)
						{
							ExportTriangle(path, v[0], v[1], v[2]);
							i = 0;
						}
					}
				}
				else // assume triangles are consecutive and don't share vertices
				{
					break;
				}
			}

			fclose(obj);
		}
	}
}

class CPropGroupEntry
{
public:
	CPropGroupEntry() {}
	CPropGroupEntry(const char* name, float fRadius) : m_name(name), m_fRadius(fRadius) {}

	std::string m_name;
	float m_fRadius;
};

class CPropGroup
{
public:
	CPropGroup() {}
	CPropGroup(const char* name) : m_name(name) {}

	std::string m_name;
	std::vector<CPropGroupEntry> m_entries;
};

static std::vector<CPropGroup> g_propGroups;
static bool                    g_propGroupsLoaded = false;

// NOTE -- this function must be kept in sync between 3 files:
// x:\gta5\src\dev\game\debug\AssetAnalysis\GeometryCollector.cpp
// x:\gta5\src\dev\tools\GeometryCollectorTool\src\GeometryCollectorTool.cpp
// x:\gta5\src\dev\tools\HeightMapImageProcessor\src\HeightMapImageProcessor.cpp
static void LoadPropGroups(bool bSeparateGeomGroups = true, bool bVerbose = true)
{
	if (!g_propGroupsLoaded)
	{
		FILE* pGroupList = fopen(varString("%s/non_final/propgroups/list.txt", RS_ASSETS).c_str(), "r");

		if (pGroupList)
		{
			char line[128] = "";

			while (rage_fgetline(line, sizeof(line), pGroupList))
			{
				const varString propGroupPath("%s/non_final/propgroups/%s.txt", RS_ASSETS, line);
				FILE* pGroup = fopen(propGroupPath.c_str(), "r");

				if (pGroup)
				{
					char* pExt = strrchr(line, '.');

					if (pExt)
					{
						*pExt = '\0';
					}

					g_propGroups.push_back(CPropGroup(line));

					float fPropRadiusMax = 0.0f;

					while (rage_fgetline(line, sizeof(line), pGroup))
					{
						const char* pPropName = strtok(line, ",\t ");

						if (pPropName)
						{
							const char* pPropRadiusStr = strtok(NULL, ",\t ");
							const float fPropRadius = pPropRadiusStr ? (float)atof(pPropRadiusStr) : 0.0f;

							g_propGroups.back().m_entries.push_back(CPropGroupEntry(pPropName, fPropRadius));
							fPropRadiusMax = Max<float>(fPropRadius, fPropRadiusMax);
						}
					}

					if (bVerbose)
					{
						fprintf(stdout, "loaded prop group \"%s\"\n", g_propGroups.back().m_name.c_str());

						for (int i = 0; i < (int)g_propGroups.back().m_entries.size(); i++)
						{
							fprintf(stdout, "  %s (radius=%f)\n", g_propGroups.back().m_entries[i].m_name.c_str(), g_propGroups.back().m_entries[i].m_fRadius);
						}
					}

					if (bSeparateGeomGroups && fPropRadiusMax > 0.0f)
					{
						rewind(pGroup);

						sprintf(line, "%s_geom", g_propGroups.back().m_name.c_str());
						g_propGroups.push_back(CPropGroup(line));

						while (rage_fgetline(line, sizeof(line), pGroup))
						{
							const char* pPropName = strtok(line, ",\t ");

							if (pPropName)
							{
								g_propGroups.back().m_entries.push_back(CPropGroupEntry(pPropName, 0.0f));
							}
						}

						if (bVerbose)
						{
							fprintf(stdout, "loaded prop group \"%s\"\n", g_propGroups.back().m_name.c_str());
							fprintf(stdout, "  all radii set to zero\n");
						}
					}

					fclose(pGroup);
				}
				else
				{
					fprintf(stderr, "failed to open \"%s\"\n", propGroupPath.c_str());
				}
			}

			fclose(pGroupList);
		}

		g_propGroupsLoaded = true;
	}
}

static void Export()
{
	std::vector<gctExportObject*> objects;

	char temp[512] = "";
	sprintf(temp, "%s/processed", g_collectorGeometryDir);
	_mkdir(temp);

	if (DO_EXPORT_WATER)
	{
		char temp[512] = "";
		sprintf(temp, "%s/processed/water", g_collectorGeometryDir);
		_mkdir(temp);

		const char* waterShaderNames[] =
		{
			"water_foam",
			"water_fountain",
			"water_river",
			"water_riverfoam",
			"water_riverlod",
			"water_riverocean",
			"water_rivershallow",
			"water_shallow",
			"water_poolenv",
			"water_terrainfoam",
		};

		for (int i = 0; i < NELEM(waterShaderNames); i++)
		{
			objects.push_back(new gctExportObject(new gctOBJ, varString("%s.obj", waterShaderNames[i]).c_str(), "water", waterShaderNames[i], EXPORT_ALLOW_ALL));
		}

		objects.push_back(new gctExportObject(new gctOBJ, "water_reflective_exterior.obj", "water", NULL, EXPORT_ALLOW_ALL & ~EXPORT_ALLOW_INTERIOR));
		gctExportObject* reflectiveExterior = objects.back();
		objects.push_back(new gctExportObject(new gctOBJ, "water_reflective_interior.obj", "water", NULL, EXPORT_ALLOW_ALL & ~EXPORT_ALLOW_EXTERIOR));
		gctExportObject* reflectiveInterior = objects.back();
		objects.push_back(new gctExportObject(new gctOBJ, "water_lod.obj", "water", NULL, EXPORT_ALLOW_ALL & ~EXPORT_ALLOW_HD));
		gctExportObject* waterLOD = objects.back();

		for (int i = 0; i < NELEM(waterShaderNames); i++)
		{
			if (strcmp(waterShaderNames[i], "water_foam") != 0 && // <- deprecated shader
				strcmp(waterShaderNames[i], "water_riverfoam") != 0 &&
				strcmp(waterShaderNames[i], "water_terrainfoam") != 0 &&
				strcmp(waterShaderNames[i], "water_shallow") != 0 &&
				strcmp(waterShaderNames[i], "water_poolenv") != 0)
			{
				reflectiveExterior->m_shaderNames.push_back(waterShaderNames[i]);
				reflectiveInterior->m_shaderNames.push_back(waterShaderNames[i]);
			}

			waterLOD->m_shaderNames.push_back(waterShaderNames[i]);
		}
	}

	if (DO_EXPORT_MIRROR)
	{
		char temp[512] = "";
		sprintf(temp, "%s/processed/mirror", g_collectorGeometryDir);
		_mkdir(temp);

		const char* mirrorShaderNames[] =
		{
			"mirror_default",
			"mirror_crack",
			"mirror_decal",
		};

		for (int i = 0; i < NELEM(mirrorShaderNames); i++)
		{
			objects.push_back(new gctExportObject(new gctOBJ, varString("%s.obj", mirrorShaderNames[i]).c_str(), "mirror", mirrorShaderNames[i], EXPORT_ALLOW_ALL));
		}

		objects.push_back(new gctExportObject(new gctOBJ, "mirror_all.obj", "mirror", NULL, EXPORT_ALLOW_ALL));
		gctExportObject* mirrorAll = objects.back();

		for (int i = 0; i < NELEM(mirrorShaderNames); i++)
		{
			mirrorAll->m_shaderNames.push_back(mirrorShaderNames[i]);
		}
	}

	if (DO_EXPORT_CABLE)
	{
		char temp[512] = "";
		sprintf(temp, "%s/processed/cable", g_collectorGeometryDir);
		_mkdir(temp);

		objects.push_back(new gctExportObject(new gctOBJ, "cable_exterior.obj", "cable", "cable", EXPORT_ALLOW_ALL & ~EXPORT_ALLOW_INTERIOR));
		objects.push_back(new gctExportObject(new gctOBJ, "cable_interior.obj", "cable", "cable", EXPORT_ALLOW_ALL & ~EXPORT_ALLOW_EXTERIOR));
	}

	if (DO_EXPORT_LIGHTS)
	{
		char temp[512] = "";
		sprintf(temp, "%s/processed/lights", g_collectorGeometryDir);
		_mkdir(temp);

		objects.push_back(new gctExportObject(new gctOBJGrid, "lights_exterior",            "lights", NULL, (EXPORT_ALLOW_ALL & ~(EXPORT_ALLOW_INTERIOR | EXPORT_ALLOW_SCRIPT)) | EXPORT_LIGHTS, INDEX_NONE));
		objects.push_back(new gctExportObject(new gctOBJGrid, "lights_interior",            "lights", NULL, (EXPORT_ALLOW_ALL & ~(EXPORT_ALLOW_EXTERIOR | EXPORT_ALLOW_SCRIPT)) | EXPORT_LIGHTS, INDEX_NONE));
		objects.push_back(new gctExportObject(new gctOBJ,     "lights_volume.obj",          "lights", NULL, (EXPORT_ALLOW_ALL & ~EXPORT_ALLOW_SCRIPT) | EXPORT_LIGHTS, INDEX_NONE, 0.0f, LIGHTFLAG_DRAW_VOLUME, NULL));
	//	objects.push_back(new gctExportObject(new gctOBJ,     "lights_exterior_shadow.obj", "lights", NULL, (EXPORT_ALLOW_ALL & ~(EXPORT_ALLOW_INTERIOR | EXPORT_ALLOW_SCRIPT)) | EXPORT_LIGHTS, INDEX_NONE, 0.0f, LIGHTFLAG_CAST_STATIC_GEOM_SHADOWS | LIGHTFLAG_CAST_DYNAMIC_GEOM_SHADOWS, NULL));
	}

	if (DO_EXPORT_CORONAS)
	{
		char temp[512] = "";
		sprintf(temp, "%s/processed/lights", g_collectorGeometryDir);
		_mkdir(temp);

		objects.push_back(new gctExportObject(new gctOBJ, "coronas_exterior.obj",         "lights", NULL, (EXPORT_ALLOW_ALL & ~(EXPORT_ALLOW_INTERIOR | EXPORT_ALLOW_SCRIPT)) | EXPORT_CORONAS_AS_QUADS));
		objects.push_back(new gctExportObject(new gctOBJ, "coronas_interior.obj",         "lights", NULL, (EXPORT_ALLOW_ALL & ~(EXPORT_ALLOW_EXTERIOR | EXPORT_ALLOW_SCRIPT)) | EXPORT_CORONAS_AS_QUADS));
		objects.push_back(new gctExportObject(new gctOBJ, "coronas_fixed_size_boxes.obj", "lights", NULL, (EXPORT_ALLOW_ALL & ~EXPORT_ALLOW_SCRIPT) | EXPORT_CORONAS_AS_BOXES, INDEX_NONE, 2.5f, ~0U, NULL));
		objects.push_back(new gctExportObject(new gctOBJ, "coronas_fixed_size.obj",       "lights", NULL, (EXPORT_ALLOW_ALL & ~EXPORT_ALLOW_SCRIPT) | EXPORT_CORONAS_AS_QUADS, INDEX_NONE, 1.0f, ~0U, NULL));
		objects.push_back(new gctExportObject(new gctOBJ, "coronas_point.obj",            "lights", NULL, (EXPORT_ALLOW_ALL & ~EXPORT_ALLOW_SCRIPT) | EXPORT_CORONAS_AS_QUADS, INDEX_NONE, 0.0f, ~0U, "POINT"));
		objects.push_back(new gctExportObject(new gctOBJ, "coronas_spot.obj",             "lights", NULL, (EXPORT_ALLOW_ALL & ~EXPORT_ALLOW_SCRIPT) | EXPORT_CORONAS_AS_QUADS, INDEX_NONE, 0.0f, ~0U, "SPOT"));
		objects.push_back(new gctExportObject(new gctOBJ, "coronas_capsule.obj",          "lights", NULL, (EXPORT_ALLOW_ALL & ~EXPORT_ALLOW_SCRIPT) | EXPORT_CORONAS_AS_QUADS, INDEX_NONE, 0.0f, ~0U, "CAPSULE"));
	}

	if (DO_EXPORT_PROPGROUPS)
	{
		LoadPropGroups();

		char temp[512] = "";
		sprintf(temp, "%s/processed/propgroup", g_collectorGeometryDir);
		_mkdir(temp);

		for (int i = 0; i < (int)g_propGroups.size(); i++)
		{
			const CPropGroup& pg = g_propGroups[i];

			objects.push_back(new gctExportObject(new gctOBJGrid, pg.m_name.c_str(), "propgroup", NULL, EXPORT_ALLOW_ALL, i));

			for (int j = 0; j < (int)pg.m_entries.size(); j++)
			{
				objects.back()->m_drawableNames.push_back(pg.m_entries[j].m_name.c_str());
			}
		}
	}

	// ============================================================================================
	for (int objectIndex = 0; objectIndex < (int)objects.size(); objectIndex++)
	{
		const gctExportObject& object = *objects[objectIndex];

		if (!object.m_target->ExportStart(object.m_path.c_str()))
		{
			continue; // ?
		}

		fprintf(stdout, "processing %s ..\n", object.m_path.c_str());

		FILE* entityList = fopen(varString("%s/_entitylist.txt", object.m_sourceDir.c_str()).c_str(), "r");

		if (entityList)
		{
			int lineIndex = 0;
			int numLines = 0;
			int numVerts = 0;
			char line[1024] = "";

			while (ReadFileLine(line, sizeof(line), entityList))
			{
				numLines++;
			}

			rewind(entityList);

			ProgressDisplay progress("processing %d lines", numLines);

			while (ReadFileLine(line, sizeof(line), entityList))
			{
				do 
				{
					// e.g.:
					//[1.000000,0.000000,0.000000][0.000000,1.000000,0.000000][0.000000,0.000000,1.000000][3525.610352,3718.963379,17.494772]:geom_v_28_lab_pool_wat1[0][0]_water_fountain.obj #[MLO=v_lab]
					//[1.000000,0.000000,0.000000][-0.000000,1.000000,0.000000][0.000000,0.000000,1.000000][-1169.576904,2751.448242,-0.737667]:geom_cs3_05_watermesh01_lod[0][0]_water_river.obj #[lod]

					if (line[0] != '[')
					{
						break; // don't process this line
					}

					const char* s = strchr(line, '[') + 1;
					const float col0x   = (float)atof(s); s = strchr(s, ',') + 1;
					const float col0y   = (float)atof(s); s = strchr(s, ',') + 1;
					const float col0z   = (float)atof(s); s = strchr(s, '[') + 1;
					const float col1x   = (float)atof(s); s = strchr(s, ',') + 1;
					const float col1y   = (float)atof(s); s = strchr(s, ',') + 1;
					const float col1z   = (float)atof(s); s = strchr(s, '[') + 1;
					const float col2x   = (float)atof(s); s = strchr(s, ',') + 1;
					const float col2y   = (float)atof(s); s = strchr(s, ',') + 1;
					const float col2z   = (float)atof(s); s = strchr(s, '[') + 1;
					const float col3x   = (float)atof(s); s = strchr(s, ',') + 1;
					const float col3y   = (float)atof(s); s = strchr(s, ',') + 1;
					const float col3z   = (float)atof(s); s = strchr(s, '[') + 1;
					const float scaleXY = (float)atof(s); s = strchr(s, ',') + 1;
					const float scaleZ  = (float)atof(s); s = strchr(s, ':') + 1;

					const Vec3 col0(col0x, col0y, col0z);
					const Vec3 col1(col1x, col1y, col1z);
					const Vec3 col2(col2x, col2y, col2z);
					const Vec3 col3(col3x, col3y, col3z);

					if (col3.x < DO_EXPORT_REGION_MIN_X ||
						col3.y < DO_EXPORT_REGION_MIN_Y ||
						col3.x > DO_EXPORT_REGION_MAX_X ||
						col3.y > DO_EXPORT_REGION_MAX_Y)
					{
						break; // don't process this line
					}

					char geomName[512] = "";
					strcpy(geomName, s);

					char* comment = strstr(geomName, "#");

					if (comment) // strip comment
					{
						*(comment++) = '\0';
					}
					else
					{
						comment = "";
					}

					if (strstr(geomName, "geom_") != geomName)
					{
						break; // don't process this line
					}

					char drawableName[512] = "";
					strcpy(drawableName, geomName + strlen("geom_"));

					if (strchr(drawableName, '['))
					{
						strchr(drawableName, '[')[0] = '\0';
					}

					if (strrchr(geomName, ']') == NULL)
					{
						break; // don't process this line
					}

					char shaderName[512] = "";
					strcpy(shaderName, strrchr(geomName, ']') + 2);

					if (strrchr(shaderName, '.'))
					{
						strrchr(shaderName, '.')[0] = '\0';
					}

					if (object.m_drawableNames.size() > 0)
					{
						bool bMatchDrawableName = false;

						for (int j = 0; j < (int)object.m_drawableNames.size(); j++)
						{
							if (_stricmp(object.m_drawableNames[j].c_str(), drawableName) == 0)
							{
								bMatchDrawableName = true;
								break;
							}
						}

						if (!bMatchDrawableName)
						{
							break; // don't process this line
						}
					}

					if (object.m_shaderNames.size() > 0)
					{
						bool bMatchShaderName = false;

						for (int j = 0; j < (int)object.m_shaderNames.size(); j++)
						{
							if (_stricmp(object.m_shaderNames[j].c_str(), shaderName) == 0)
							{
								bMatchShaderName = true;
								break;
							}
						}

						if (!bMatchShaderName)
						{
							break; // don't process this line
						}
					}

					const bool bIsInterior = (stristr(comment, "[MLO=") != NULL);
					const bool bIsLOD      = (stristr(comment, "[LOD]") != NULL);
					const bool bIsScript   = (stristr(comment, "[SCRIPT]") != NULL);

					if ((bIsInterior && !(object.m_flags & EXPORT_ALLOW_INTERIOR)) || (!bIsInterior && !(object.m_flags & EXPORT_ALLOW_EXTERIOR)))
					{
						break; // don't process this line
					}

					if ((bIsLOD && !(object.m_flags & EXPORT_ALLOW_LOD)) || (!bIsLOD && !(object.m_flags & EXPORT_ALLOW_HD)))
					{
						break; // don't process this line
					}

					if ((bIsScript && !(object.m_flags & EXPORT_ALLOW_SCRIPT)) || (!bIsScript && !(object.m_flags & EXPORT_ALLOW_NONSCRIPT)))
					{
						break; // don't process this line
					}

					const varString srcPath("%s/%s", object.m_sourceDir.c_str(), geomName);
					float radius = 0.0f;

					if (object.m_propGroupIndex != INDEX_NONE)
					{
						const CPropGroup& pg = g_propGroups[object.m_propGroupIndex];

						for (int i = 0; i < (int)pg.m_entries.size(); i++)
						{
							if (_stricmp(drawableName, pg.m_entries[i].m_name.c_str()) == 0)
							{
								radius = pg.m_entries[i].m_fRadius;
								break;
							}
						}
					}

					object.m_target->ExportOBJ(object, srcPath.c_str(), radius, col0, col1, col2, col3, scaleXY, scaleZ);
				}
				while (false);

				progress.Update(lineIndex++, numLines);
			}

			fclose(entityList);

			char finishMsg[512] = "";
			object.m_target->ExportFinish(object.m_path.c_str(), finishMsg);
			progress.End(finishMsg);
		}
	}
}

int main(int argc, const char* argv[])
{
	PrintDirectoryPaths();

	bool bSyncIn  = false;
	bool bSyncOut = false;
	bool bPause   = IsDebuggerPresent() ? true : false;

	const char* syncInPath  = "x:/sync.gc1";
	const char* syncOutPath = "x:/sync.gc2";

	for (int i = 1; i < argc; i++)
	{
		if      (strcmp(argv[i], "-sync_in" ) == 0) { bSyncIn = true; }
		else if (strcmp(argv[i], "-sync_out") == 0) { bSyncOut = true; }
		else if (strcmp(argv[i], "-pause"   ) == 0) { bPause = true; }
	}

	if (bSyncIn)
	{
		fprintf(stdout, "waiting for %s .. ", syncInPath);
		fflush(stdout);

		while (1)
		{
			FILE* sync = fopen(syncInPath, "rb");

			if (sync)
			{
				fclose(sync);
				fprintf(stdout, "ok.\n");
				break;
			}
			else
			{
				Sleep(1000); // sleep for one second
			}
		}
	}

	const clock_t time0 = clock();

	Export();

	// report total time
	{
		const float totalSecs = (float)(clock() - time0)/(float)CLOCKS_PER_SEC;
		const float totalMins = totalSecs/60.0f;
		const float totalHrs  = totalMins/60.0f;

		if      (totalSecs <=   1.0f) { fprintf(stdout, "done.\n"); }
		else if (totalSecs <= 100.0f) { fprintf(stdout, "done (%.2f secs).\n", totalSecs); }
		else if (totalMins <= 100.0f) { fprintf(stdout, "done (%.2f mins).\n", totalMins); }
		else                          { fprintf(stdout, "done (%.2f hours).\n", totalHrs); }
	}

	if (bSyncOut)
	{
		fclose(fopen(syncOutPath, "wb"));
	}

	if (bPause)
	{
		system("pause");
	}

	return 0;
}
