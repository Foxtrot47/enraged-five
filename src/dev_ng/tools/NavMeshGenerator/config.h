//
// navmeshgenerator/config.h
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef NAVMESHGENERATOR_CONFIG_H
#define NAVMESHGENERATOR_CONFIG_H

#if __DEV && !__OPTIMIZED
#define _CRTDBG_MAP_ALLOC	1
#endif

#include "fwnavgen/config.h"
#include "atl/array.h"
#include "string/string.h"
#include "vector/vector3.h"

//-----------------------------------------------------------------------------

struct CNavMeshGeneratorConfig : public fwNavGenConfig
{
	static const CNavMeshGeneratorConfig& Get()
	{	return static_cast<const CNavMeshGeneratorConfig&>(fwNavGenConfigManager::GetInstance().GetConfig());	}

	bool	m_ExportVehicles;

	PAR_PARSABLE;
};

//-----------------------------------------------------------------------------

/*
PURPOSE
	Simple XML-loadable list of names of IPL groups that should be loaded by default
	when exporting level geometry to build navmesh data from.
*/
struct CNavMeshDataDefaultIplGroups
{
public:
	atArray<ConstString> m_DefaultIplGroups;

	PAR_SIMPLE_PARSABLE;
};

//-----------------------------------------------------------------------------

/*
PURPOSE
	Simple XML-loadable list of entity set definitions that should be loaded by default
	when exporting level geometry to build navmesh data from.
	Format follows that which script use to request entity sets:
		XYZ position, interior name, entity set name
*/

struct CEntitySetDesc
{
public:
	Vector3 m_vPosition;
	ConstString m_InteriorName;
	ConstString m_EntitySetName;

	PAR_SIMPLE_PARSABLE;
};

struct CNavMeshDataDefaultEntitySets
{
public:
	atArray<CEntitySetDesc> m_DefaultEntitySets;

	PAR_SIMPLE_PARSABLE;
};

//-----------------------------------------------------------------------------

#endif	// NAVMESHGENERATOR_CONFIG_H

// End of file navmeshgenerator/config.h
