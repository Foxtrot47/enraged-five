#define EXPORTCOLLISION_OPTIMISATIONS_OFF() OPTIMISATIONS_OFF()

#if 1 && __DEV
	EXPORTCOLLISION_OPTIMISATIONS_OFF()
	#pragma warning(disable:4748) // shut up.
#endif

#include "exportcollision.h"
#include "config.h"
#include "exportcollisionvehicles.h"

#include "fwnavgen/datatypes.h"
#include "fwnavgen/helperfunc.h"

// Rage headers
#include "diag/output.h"
#include "Physics/iterator.h"
#include "string/stringutil.h"
#include "system/bootmgr.h"
#include "system/param.h"
#include "system/xtl.h"		// For MEMORYSTATUS, etc.

// Framework headers
#include "fwscene/stores/boxstreamer.h"
#include "fwscene/stores/mapdatastore.h"
#include "fwscene\stores\maptypesstore.h"
#include "fwscene/stores/staticboundsstore.h"
#include "fwscene/world/WorldLimits.h"
#include "spatialdata/sphere.h"

// Game headers
#include "camera/viewports/ViewportManager.h"
#include "script/commands_interiors.h"
#include "modelinfo/MloModelInfo.h"
#include "Objects/DummyObject.h"
#include "Objects/Object.h"
#include "Objects/ObjectPopulation.h"
#include "PathServer/PathServer.h"
#include "Physics/Physics.h"
#include "Renderer/PlantsMgr.h"
#include "Renderer/Water.h"
#include "scene/ipl/IplCullBox.h"
#include "scene/entities/compEntity.h"
#include "scene/FocusEntity.h"
#include "scene/loader/MapData_Interiors.h"
#include "scene/portals/Portal.h"
#include "scene/streamer/streamvolume.h"
#include "scene/world/GameWorld.h"
#include "script/script.h"			// CTheScripts
#include "streaming/packfilemanager.h"
#include "streaming/streaming.h"
#include "streaming/streaminginfo.h"
#include "Task/Movement/Climbing/TaskGoToAndClimbLadder.h"
#include "VehicleAi/PathFind.h"
#include "vehicles\train.h"

#include <time.h>

//-----------------------------------------------------------------

PARAM(exportall, "This will export all navmeshes");
PARAM(exportsectors, "This will export specified navmeshes given in sector coords (x1,y1,x2,y2)");
PARAM(exportregion, "This will export all navmeshes intersecting the world coords (x1,y1,x2,y2)");
PARAM(exportcontinuefrom, "This will export all navmeshes starting from theese coords x,y");
PARAM(exportdlc, "This will export all the navmeshes intersecting the specified list of CDLCArea's");
PARAM(exportFolder, "Specifies the folder that the navmeshes should be written to.");
PARAM(exportiplgroups, "Path and file name for an XML file containing a list of IPL groups to load, using a CNavMeshDataDefaultIplGroups object.");
PARAM(exportentitysets, "Path and file name for an XML file containing a list of interior entity sets to load, using a CNavMeshDataDefaultEntitySets object.");
PARAM(scheduledCompile, "Specifies that this is a scheduled navmesh compile, and completed.txt must be written at the end.");
PARAM(navgenBatchFile, "-navGenBatchFile specifies the batch file to run when generating navmeshes");
PARAM(navgenCmdFile, "-navGenCmdFile specifies the cmd-file to use when generating navmeshes");
PARAM(navgencfg, "Path and file name for the navmesh tool's configuration file, if desired.");
PARAM(checkEntityName, "Name of entity to check for during export.");
PARAM(checkEntityPos, "Position of entity to check for during export.");

//-----------------------------------------------------------------

XPARAM(nopeds);
XPARAM(nocars);
XPARAM(noambient);
XPARAM(nonetwork);

#if __WIN32PC && __BANK
XPARAM(hidewidgets);			// Currently defined in 'bank/pane.cpp'.
#endif

namespace rage
{
	XPARAM(noaudio);
	XPARAM(noaudiothread);
	XPARAM(nowaveslots);

#if __WIN32PC && !__FINAL
	XPARAM(hidewindow);			// Currently defined in 'grcore/device_win32.cpp'.
#endif
}	// namespace rage

#if 0
void PrintInterior(CInteriorInst * pIntInst)
{
	const char * pInteriorName = pIntInst->GetModelName();

	// This is an interior.  Go through all the entities in the interior & export.
	CEntity * pIntEntity = NULL;
	pIntInst->RestartEntityInInteriorList();
	while(pIntInst->GetNextEntityInInteriorList(&pIntEntity))
	{
		const char * pEntityName = pIntEntity->GetModelName();
		if(stricmp(pEntityName, testEntityName)==0)
		{
			bBreak = true;
		}
	}
}
#endif

CNavMeshDataExporterInterfaceTool::CNavMeshDataExporterInterfaceTool(CNavMeshDataExporterTool &tool)
		: m_Tool(&tool)
{}


bool CNavMeshDataExporterInterfaceTool::EntityIntersectsCurrentNavMesh(CEntity* pEntity) const
{
	return m_Tool->EntityIntersectsCurrentNavMesh(pEntity);
}

bool CNavMeshDataExporterInterfaceTool::ExtentsIntersectCurrentNavMesh(Vec3V_In vMin, Vec3V_In vMax) const
{
	return m_Tool->ExtentsIntersectCurrentNavMesh(vMin, vMax);
}

bool CNavMeshDataExporterInterfaceTool::GetCurrentNavMeshExtents(spdAABB & outAABB) const
{
	return m_Tool->GetCurrentNavMeshExtents(outAABB);
}

bool CNavMeshDataExporterInterfaceTool::IsAlreadyInPhysicsLevel(CPhysical* pPhysical, phArchetype* pArchetype) const
{
	return CNavMeshDataExporterTool::IsAlreadyInPhysicsLevel(pPhysical, pArchetype);
}


void CNavMeshDataExporterInterfaceTool::AppendToActiveBuildingsArray(atArray<CEntity*>& entityArray)
{
	m_Tool->AppendToActiveBuildingsArray(entityArray);
}

void CNavMeshDataExporterInterfaceTool::AddSearches(atArray<fwBoxStreamerSearch>& searchList)
{
	m_Tool->AddSearches(searchList);
}

void CNavMeshDataExporterTool::PreInit()
{
	Assert(CNavMeshDataExporter::GetExportMode() == CNavMeshDataExporter::eNotExporting);
	CNavMeshDataExporter::SetExportMode(CNavMeshDataExporter::eWaitingToStartExport);

	// This is needed for now to prevent the pools from becoming too small when the values
	// from the configuration file override the larger values in code. We may want to
	// solve this in a different way, perhaps either loading a secondary configuration
	// file containing the larger pool values, or adding a conditional section within
	// the single configuration file which is somehow only used from this tool.
	fwConfigManager::SetUseLargerPoolFromCodeOrData(true);

	PARAM_noaudio.Set("");
	PARAM_noaudiothread.Set("");
	PARAM_nowaveslots.Set("");
	PARAM_nopeds.Set("");
	PARAM_nocars.Set("");
	PARAM_noambient.Set("");
	PARAM_nonetwork.Set("");

#if __WIN32PC && !__FINAL
	PARAM_hidewindow.Set("");
#endif

#if __WIN32PC && __BANK
	PARAM_hidewidgets.Set("");
#endif
}


void CNavMeshDataExporterTool::Init()
{
	fwExportCollisionGridTool::Init();

	InitConfig();

	LoadDefaultIplGroupList();
	RequestDefaultIplGroups();

	LoadDefaultEntitySetList();
	RequestDefaultEntitySets();
}


void CNavMeshDataExporterTool::Shutdown()
{
	ShutdownConfig();

	fwExportCollisionGridTool::Shutdown();
}


void CNavMeshDataExporterTool::InitConfig()
{
	const char *cfgFileName = NULL;
	PARAM_navgencfg.Get(cfgFileName);

	if(cfgFileName && cfgFileName[0])
	{
		Displayf("Loading NavMeshGenerator configuration file from '%s'.", cfgFileName);
	}
	else
	{
		Displayf("No configuration file specified for NavMeshGenerator.");
	}

	fwNavGenConfigManagerImpl<CNavMeshGeneratorConfig>::InitClass(cfgFileName);
}


void CNavMeshDataExporterTool::ShutdownConfig()
{
	fwNavGenConfigManagerImpl<CNavMeshGeneratorConfig>::ShutdownClass();
}


bool CNavMeshDataExporterTool::UpdateExport()
{
	// When exporting navmeshes, use a cut-down game update loop.
	CNavMeshDataExporterTool::NotifyMainGameLoopHasStarted();

	if(CNavMeshDataExporter::IsExportingCollision())
	{
		CNavMeshDataExporterTool::ProcessCollisionExport();
	}

	return false;
}


void CNavMeshDataExporterTool::LoadDefaultIplGroupList()
{
	// Clear out any old data, if this is not the first time it's called.
	delete m_DefaultIplGroups;
	m_DefaultIplGroups = NULL;

	const char* filename = NULL;
	if(PARAM_exportiplgroups.Get(filename))
	{
		if(PARSER.LoadObjectPtr(filename, "xml", m_DefaultIplGroups))
		{
			Displayf("List of IPL groups to load successfully loaded from '%s'.", filename);
		}
		else
		{
			Errorf("List of IPL groups to couldn't be loaded from '%s'.", filename);
		}
	}
	else
	{
		Displayf("No list of IPL groups to load was specified (-exportiplgroups).");
	}
}

void CNavMeshDataExporterTool::LoadDefaultEntitySetList()
{
	//------------------------------------------------------------------------

	// Clear out any old data, if this is not the first time it's called.
	delete m_DefaultEntitySets;
	m_DefaultEntitySets = NULL;


	const char* filename = NULL;
	if(PARAM_exportentitysets.Get(filename))
	{
		if(PARSER.LoadObjectPtr(filename, "xml", m_DefaultEntitySets))
		{
			Displayf("List of entity sets to load successfully loaded from '%s'.", filename);
		}
		else
		{
			Errorf("List of entity sets to couldn't be loaded from '%s'.", filename);
		}
	}
	else
	{
		Displayf("No list of entity sets to load was specified (-exportentitysets).");
	}
}

// NAME : RequestDefaultIplGroups
// PURPOSE : Request all the IPL groups required to define the default/base state of the map

// TODO -- this function in rdr3 is significantly different
void CNavMeshDataExporterTool::RequestDefaultIplGroups()
{
	/*
	// check all .ityps
	for(s32 index=0; index<g_MapTypesStore.GetSize(); index++)
	{
		// every .ityp must be marked as permenent, or as a dependency
		fwMapTypesDef* pDef = g_MapTypesStore.GetSlot(index);
		if (pDef && !pDef->GetIsPermanent() && !pDef->GetIsDependency())
		{
			if(pDef->GetIsDLC() && pDef->GetIsMLOType())
			{
				pDef->SetIsPermanent(true);
			}
			//Assertf(false,"%s : not currently used. Not set as Permanent in images.meta file, or set as any .imap file dependency", g_MapTypesStore.GetName(index));
		}
	}
	*/

	if(!m_DefaultIplGroups)
	{
		return;
	}

	for(int c = 0; c < m_DefaultIplGroups->m_DefaultIplGroups.GetCount(); c++)
	{
		const char* pName = m_DefaultIplGroups->m_DefaultIplGroups[c];
		s32 index = INSTANCE_STORE.FindSlot(pName).Get();
		if(index != -1)
		{
			if( INSTANCE_STORE.GetBoxStreamer().GetIsIgnored(index) )
			{
				INSTANCE_STORE.GetBoxStreamer().SetIsIgnored(index, false);
			}

			INSTANCE_STORE.RequestGroup(strLocalIndex(index), atStringHash(pName), false);	// was true - but that made them resident throughout the whole export
		}
	}
}

void CNavMeshDataExporterTool::RequestDefaultEntitySets()
{
	if(!m_DefaultEntitySets)
	{
		return;
	}

	Assert(m_DefaultEntitySets->m_DefaultEntitySets.GetCount() < MAX_ENTITY_SETS);

	for(int c = 0; c < m_DefaultEntitySets->m_DefaultEntitySets.GetCount(); c++)
	{
		const CEntitySetDesc & desc = m_DefaultEntitySets->m_DefaultEntitySets[c];

		int iInteriorProxy = interior_commands::GetProxyAtCoords(desc.m_vPosition, desc.m_InteriorName);
		Assertf(iInteriorProxy != NULL_IN_SCRIPTING_LANGUAGE, "Could not find interior \"%s\" specified in entity-set.");

		if(iInteriorProxy != NULL_IN_SCRIPTING_LANGUAGE)
		{
			// get the interior 
			CInteriorProxy *pIntProxy = CInteriorProxy::GetPool()->GetAt(iInteriorProxy);
			Assertf(pIntProxy, "Interior proxy doesn't exist");
			if (pIntProxy)
			{
				pIntProxy->ActivateEntitySet(atHashString(desc.m_EntitySetName));
			}
		}
	}
}

//-----------------------------------------------------------------

// TODO: Probably move the definition of many of these to here
XPARAM(exportcontinuefrom);
XPARAM(exportsectors);
XPARAM(exportdlc);
XPARAM(exportFolder);
XPARAM(scheduledCompile);
XPARAM(navgenBatchFile);
XPARAM(navgenCmdFile);
XPARAM(noBlockOnLostFocus);

//NAVMESH_OPTIMISATIONS()
//OPTIMISATIONS_OFF()

using namespace std;

namespace
{

// how much area of empty sea to export for navmesh outside inhabited world extents
float g_fSeaBorderSize = 300.0f;

// This is the range which is used for loading in data around the origin, when we are exporting
// collision.  This value is added to the default BOUNDS_BB_EXTRASIZE value in fwStaticBoundsStore.

float TIME_TO_ALLOW_STREAMING_TO_CATCH_UP = 1.0f;
float MAX_TIME_TO_WAIT_FOR_STREAMING_TO_LOAD = 2.0f;
float fExtra = 0.0f; //50.0f;

Vector3 vExpandForInteriors(0.0f, 0.0f, 0.0f);
CLinkList<CEntity*> dbgBuildingsWithPhysicsList;


bool IsEntityInList(CEntity * pEntity, atArray<CEntity*> & potentialEntitiesToExport)
{
	for(u32 e=0; e<potentialEntitiesToExport.size(); e++)
		if(potentialEntitiesToExport[e]==pEntity)
			return true;
	return false;
}
bool IsEntityInList(fwEntity * pEntity, vector<fwEntity*> & potentialEntitiesToExport)
{
	for(u32 e=0; e<potentialEntitiesToExport.size(); e++)
		if(potentialEntitiesToExport[e]==pEntity)
			return true;
	return false;
}
s32 GetEntityIndexInList(CEntity * pEntity, vector<fwEntity*> & potentialEntitiesToExport)
{
	for(u32 e=0; e<potentialEntitiesToExport.size(); e++)
		if(potentialEntitiesToExport[e]==pEntity)
			return e;
	return -1;
}

static char szCheckEntityName[256] = { 0 };

#define __CHECK_ENTITY_NAME		__BANK

#if __CHECK_ENTITY_NAME
bool CheckEntityName(CEntity * pEntity)
{
	static bool b1stTime=true;
	static Vector3 vMatchPos( 0.0f, 0.0f, 0.0f );
	if(b1stTime)
	{
		const char * pEntityName = NULL;
		if(PARAM_checkEntityName.Get(pEntityName) && pEntityName && *pEntityName)
		{
			strcpy(&szCheckEntityName[0], pEntityName);

			const char * pEntityPos = NULL;
			if(PARAM_checkEntityPos.Get(pEntityPos) && pEntityPos && *pEntityPos)
			{
				sscanf_s(pEntityPos, "%f,%f,%f", &vMatchPos.x, &vMatchPos.y, &vMatchPos.z);
			}
		}
		b1stTime = false;
	}

	const fwTransform & xf = pEntity->GetTransform();
	Vector3 vPos = VEC3V_TO_VECTOR3(xf.GetPosition());

	if(vPos.IsClose(vMatchPos, 0.25f) || vMatchPos.IsClose(VEC3_ZERO, 0.25f))
	{
		const char * pEntityName = NULL;

		CBaseModelInfo * pModelInfo = pEntity->GetBaseModelInfo();
		if(pModelInfo)
		{
			pEntityName = pModelInfo->GetModelName();
		}
		else if (pEntity->GetOwnedBy() == ENTITY_OWNEDBY_STATICBOUNDS)
		{
			pEntityName = g_StaticBoundsStore.GetName(pEntity->GetIplIndex());
		}

		if(!pEntityName)
		{
			return false;
		}

		if(stricmp(pEntityName, szCheckEntityName)==0)
		{
			bool bMatched = true;
			bMatched = bMatched;
			vPos = vPos;

			return true;
		}
	}
	return false;
}
#else
bool CheckEntityName(CEntity * UNUSED_PARAM(pEntity)) { return false; }
#endif

bool AddInteriorsToExportListCB(CEntity* pEntity, void* data)
{
	Assert(*(reinterpret_cast<u32*>(pEntity))!=0xdddddddd);
	Assert(pEntity);
	atArray<CEntity*> * entitiesToExport = reinterpret_cast<atArray<CEntity*> *>(data);
	CBaseModelInfo * pModelInfo = pEntity->GetBaseModelInfo();
	Assert(pModelInfo);

	if(pModelInfo->GetModelType() == MI_TYPE_MLO)
	{
		CInteriorInst * pIntInst = (CInteriorInst*)pEntity;

		if(!IsEntityInList(pIntInst, *entitiesToExport))
			entitiesToExport->PushAndGrow(pIntInst);

		if(CheckEntityName(pIntInst))
			Displayf("Found entity as in interior inst");

		// This is an interior.  Go through all the entities in the interior & export.
		CEntity * pIntEntity = NULL;
		pIntInst->RestartEntityInInteriorList();
		while(pIntInst->GetNextEntityInInteriorList(&pIntEntity))
		{
			if(pIntEntity)
			{
				if(CheckEntityName(pIntEntity))
					Displayf("Found entity inside an interior");

				if(!IsEntityInList(pIntEntity, *entitiesToExport))
				{
					Assert(*(reinterpret_cast<u32*>(pIntEntity))!=0xdddddddd);
					entitiesToExport->PushAndGrow(pIntEntity);
				}
			}
		}
	}

	return true;
}


void ReplaceChar(char * pString, char cReplace, char cWith)
{
	while(*pString)
	{
		if(*pString==cReplace)
			*pString=cWith;
		pString++;
	}
}

}	// anon namespace

// Set this to 1 to prevent any triangle exporting/clipping/etc - just to do the streaming request stuff
#define __NAVMESH_EXPORT_DISABLE_COLLISION_EXPORT	0
#define __BREAK_ON_ENTITY_NAME						0

#if __WIN32
#pragma warning(push)
#pragma warning(disable: 4355) // 'this' used in base member initializer list
#endif

CNavMeshDataExporterTool::CNavMeshDataExporterTool(fwLevelProcessTool * pTool)
	: fwExportCollisionGridTool(pTool),
	  m_ExporterInterface(*this)
{
	m_fTimeUntilWeExportThisNavMesh = 0.0f;

	m_bErrorsInExport = false;
	m_bHasBuildingsWithPhysics = false;
	m_bHasCompEntities = false;

	m_pExportCollisionLogFile = NULL;
	m_DefaultIplGroups = NULL;
	m_DefaultEntitySets = NULL;

	m_bLaunchCompilerAfterExporting = false;

	m_bAlwaysWaitForStreaming = false;
	m_bExportObjects = true;
	m_bExportOctrees = true;
	m_bExportInteriors = true;
	m_bPrintOutAllLoadedCollision = false;

	m_iPeakNumRefs = 0;
	m_iPeakNumBuildings = 0;
	m_iPeakNumObjects = 0;
	m_iPeakNumDummyObjects = 0;
	m_iPeakNumStreamedArchetypes = 0;
	m_iPeakNumPtrSingle = 0;
	m_iPeakNumPtrDouble = 0;
}

#if __WIN32
#pragma warning(pop)
#endif

CNavMeshDataExporterTool::~CNavMeshDataExporterTool()
{
	delete m_DefaultIplGroups;
	m_DefaultIplGroups = NULL;
}


void CNavMeshDataExporterTool::OpenExportLog()
{
	char filename[512];
	formatf(filename, "%s\\export_log.txt", m_OutputPath);

	m_pExportCollisionLogFile = fiStream::Create(filename);
}

// Helper function to change the width of a fwVariableBoxStreamer, as if it had
// been set before its Init() call.
// Note: would perhaps be handy to turn this into fwVariableBoxStreamer::SetAllWidths()
// or something.
/*
static void sChangeVarBoxStreamerWidth(fwBoxStreamerNew& streamer, float newWidth)
{
	strStreamingModule* pMod = streamer.GetStreamingModule();
	if(!Verifyf(pMod, "Expected streaming module"))
	{
		return;
	}

	const int cnt = pMod->GetCount();
	for(int i = 0; i < cnt; i++)
	{
		streamer.SetWidth(i, newWidth);
	}

	static_cast<fwBoxStreamerNew&>(streamer).SetWidth(newWidth);
}
*/

bool CNavMeshDataExporterTool::StartCollisionExport()
{
	Displayf("CNavMeshDataExporterTool::StartCollisionExport()\n");

	// TODO: Think more about what to do about this stuff. Will we ever need to launch anything this way?
	m_bLaunchCompilerAfterExporting = true;

	m_OutputPath[0] = 0;
	GetOutputFolder();
	if(!m_OutputPath[0])
		return false;

	OpenExportLog();

	// Turn off rendering, etc.
	// Ideally we'd want to stop creation of D3D device objects too
//	gVpMan.GetRenderSettings().ExcludeFlags(0xFFFFFFFF);

	CPathServerExtents::m_iNumSectorsPerNavMesh = CPathServer::GetNavMeshStore(kNavDomainRegular)->GetNumSectorsPerMesh();

//	const float fExportWidth = CPathServer::GetNavMeshStore(kNavDomainRegular)->GetMeshSize();
//	sChangeVarBoxStreamerWidth(g_StaticBoundsStore.GetBoxStreamer(), fExportWidth);
//	INSTANCE_STORE.GetBoxStreamer().SetWidth(fExportWidth);


	// Also done by BaseStartCollisionExport(), but might need these values for ExportCollisionForAllVehicles().
	// TODO: Think more about this part, perhaps something could be reordered to avoid doing
	// this in two places.
	m_fWidthOfNavMesh = CPathServerExtents::GetWorldWidthOfSector() * CPathServerExtents::m_iNumSectorsPerNavMesh;
	m_fDepthOfNavMesh = CPathServerExtents::GetWorldWidthOfSector() * CPathServerExtents::m_iNumSectorsPerNavMesh;


	// Firstly export collision for all the large vehicles in the game
	if(CNavMeshGeneratorConfig::Get().m_ExportVehicles)
	{
		CNavMeshVehicleDataFunctions::ExportCollisionForAllVehicles(*this, m_OutputPath);

		Displayf("Completed CNavMeshDataExporterTool::ExportCollisionForAllVehicles()\n");
	}

	if(!fwExportCollisionGridTool::StartCollisionExport())
	{
		return false;
	}


	ThePaths.bLoadAllRegions = true;
	CTheScripts::SetAllowGameToPauseForStreaming(true);
	m_bHasBuildingsWithPhysics = false;
	m_bHasCompEntities = false;
//	gVpMan.GetRenderSettings().SetFlags(0);
	gbPlantMgrActive = FALSE;

	CNavMeshDataExporter::SetActiveExporter(&m_ExporterInterface);
	Assert(CNavMeshDataExporter::GetExportMode() == CNavMeshDataExporter::eWaitingToStartExport);
	CNavMeshDataExporter::SetExportMode(CNavMeshDataExporter::eMapExport);

//	CPhysics::SetScanForBuildingsFunctor(MakeFunctor(CNavMeshDataExporterTool::ScanForBuildings));

	m_fTimeUntilWeExportThisNavMesh = TIME_TO_ALLOW_STREAMING_TO_CATCH_UP;

	m_iPeakNumRefs = 0;
	m_iPeakNumBuildings = 0;
	m_iPeakNumObjects = 0;
	m_iPeakNumPtrSingle = 0;
	m_iPeakNumPtrDouble = 0;

	dbgBuildingsWithPhysicsList.Init(4096);

#if __WIN32PC
	// Make sure that the application will still run when it loses focus
	GRCDEVICE.SetBlockOnLostFocus(false);
	// Make sure that Rage won't halt the app with dialog boxes, or automatically quit upon an error
	diagOutput::DisablePopUpErrors();
	diagOutput::DisablePopUpQuits();
#endif

	return true;
}


bool CNavMeshDataExporterTool::EntityIntersectsCurrentNavMesh(CEntity * pEntity)
{
	/*
	if(!pEntity->GetIsTypeDummyObject() && !pEntity->GetIsTypeComposite())
	{
		phInst* pInst = pEntity->GetFragInst();
		if(pInst==NULL)
			pInst = pEntity->GetCurrentPhysicsInst();

		phArchetype * pArchetype = NULL;
		if(pInst)
			pArchetype = pInst->GetArchetype();

		if(!pArchetype)
			return false;
	}
	*/

	Vector3 vMid = (m_vNavMeshMins + m_vNavMeshMaxs) * 0.5f;
	Vector3 vNavMeshMinsExtra = m_vNavMeshMins;
	Vector3 vNavMeshMaxsExtra = m_vNavMeshMaxs;
	Vector3 vEntityMin = pEntity->GetBoundingBoxMin();
	Vector3 vEntityMax = pEntity->GetBoundingBoxMax();

	const bool bBoundsAreInWorldSpace = false; //pEntity->GetIsTypeComposite();
	if(!bBoundsAreInWorldSpace)
	{
		fwNavToolHelperFuncs::GetRotatedMinMax(vEntityMin, vEntityMax, MAT34V_TO_MATRIX34(pEntity->GetMatrix()));
	}

	if(pEntity->GetIsTypeMLO())
	{
		vEntityMin -= vExpandForInteriors;
		vEntityMax += vExpandForInteriors;
	}

	if(vEntityMin.x > vNavMeshMaxsExtra.x || vEntityMin.y > vNavMeshMaxsExtra.y || vEntityMin.z > vNavMeshMaxsExtra.z ||
		vNavMeshMinsExtra.x > vEntityMax.x || vNavMeshMinsExtra.y > vEntityMax.y || vNavMeshMinsExtra.z > vEntityMax.z)
	{
		return false;
	}
	else
	{
		return true;
	}
}

bool CNavMeshDataExporterTool::ExtentsIntersectCurrentNavMesh(Vec3V_In vMinV, Vec3V_In vMaxV)
{
	Vector3 vNavMeshMinsExtra = m_vNavMeshMins;
	Vector3 vNavMeshMaxsExtra = m_vNavMeshMaxs;
	Vector3 vEntityMin = VEC3V_TO_VECTOR3(vMinV);
	Vector3 vEntityMax = VEC3V_TO_VECTOR3(vMaxV);

	if(vEntityMin.x > vNavMeshMaxsExtra.x || vEntityMin.y > vNavMeshMaxsExtra.y || vEntityMin.z > vNavMeshMaxsExtra.z ||
		vNavMeshMinsExtra.x > vEntityMax.x || vNavMeshMinsExtra.y > vEntityMax.y || vNavMeshMinsExtra.z > vEntityMax.z)
	{
		return false;
	}
	else
	{
		return true;
	}
}

bool CNavMeshDataExporterTool::GetCurrentNavMeshExtents(spdAABB & outAABB)
{
	outAABB.Set(VECTOR3_TO_VEC3V(m_vNavMeshMins), VECTOR3_TO_VEC3V(m_vNavMeshMaxs));
	return true;
}

static atArray<atString> g_InteriorsNotLoaded;
static bool g_bAllInteriorsInstanced = false;
static bool g_bWaitingForInteriorArchetypes = false;

// build list of objects which need to have physics loaded
// ToDo: only pull in objects which are necessary. (rooms with active portal tracking taking place).
void CNavMeshDataExporterTool::AppendToActiveBuildingsArray(atArray<CEntity*>& entityArray)
{
	CInteriorProxy::ImmediateStateUpdate();

	g_InteriorsNotLoaded.clear();
	g_bAllInteriorsInstanced = true;

	if(!m_bExportInteriors)
	{
		return;
	}
	
	static const float fExtraRadius = 50.0f;

	int iSize = CInteriorProxy::GetPool()->GetSize();
	for(int i=0; i<iSize; i++)
	{
		CInteriorProxy * pIntProxy = CInteriorProxy::GetPool()->GetSlot(i);
		if(!pIntProxy)
			continue;

		bool bRequestThisInterior = false;

		spdAABB bbox;
		pIntProxy->GetBoundBox(bbox);
		Vector3 vIntMin = VEC3V_TO_VECTOR3(bbox.GetMin());
		Vector3 vIntMax = VEC3V_TO_VECTOR3(bbox.GetMax());
		vIntMin -= Vector3(fExtraRadius, fExtraRadius, fExtraRadius);
		vIntMax += Vector3(fExtraRadius, fExtraRadius, fExtraRadius);

		// Catch cases of interiors with bad bbox data ranging from -INF to INF
		if(vIntMin.x < WORLDLIMITS_XMIN && vIntMin.y < WORLDLIMITS_YMIN &&
			vIntMax.x > WORLDLIMITS_XMAX && vIntMax.y > WORLDLIMITS_YMAX)
			continue;

		bRequestThisInterior = fwNavToolHelperFuncs::BoundingBoxesOverlap(vIntMin, vIntMax, m_vNavMeshMins, m_vNavMeshMaxs);

		CInteriorInst * pIntInst = pIntProxy->GetInteriorInst();

		if(pIntInst)
		{
			// Allow us to ignore a specific interior - for testing purposes
			static char * pIgnoreNamedInterior = NULL; //"v_abattoir";	
			const char * pIntName = fwArchetypeManager::GetArchetypeName(pIntInst->GetModelIndex());
			if(pIgnoreNamedInterior && pIntName && stricmp(pIntName, pIgnoreNamedInterior)==0)
			{
				continue;
			}
		}

		// Is this the interior which we are currently exporting?

		if(bRequestThisInterior)
		{
			ASSERT_ONLY( const bool bImapActive = pIntProxy->IsContainingImapActive(); )
			Assert(bImapActive);
			/*
			if(!bImapActive)
			{
				fwMapDataStore::GetStore().GetBoxStreamer().SetIsIgnored( pIntProxy->GetMapDataSlotIndex(), false );
			}
			*/

			m_bHasBuildingsWithPhysics = true;

			pIntProxy->SetRequestedState(CInteriorProxy::RM_SCRIPT, CInteriorProxy::PS_FULL_WITH_COLLISIONS, true);
			pIntProxy->SetRequiredByLoadScene(true);

			if(pIntProxy->GetCurrentState() != CInteriorProxy::PS_FULL_WITH_COLLISIONS)
			{
				g_InteriorsNotLoaded.PushAndGrow(atString(pIntProxy->GetModelName()));
				g_bAllInteriorsInstanced = false;
			}

			if(pIntInst)
			{
				entityArray.Append() = pIntInst;
				CPortal::AddToActiveInteriorList(pIntInst);

#if !HEIGHTMAP_GENERATOR_TOOL
				const char * pIntName = fwArchetypeManager::GetArchetypeName(pIntInst->GetModelIndex());
				if(!pIntInst->m_bInUse)
				{
					printf("Populating interior \"%s\" (0x%x)\n", pIntName, pIntInst);
					Displayf("Populating interior \"%s\" (0x%x)\n", pIntName, pIntInst);
				}
#endif

				pIntInst->m_bInUse = true;

				const u32 reqFlags = STRFLAG_LOADSCENE|STRFLAG_PRIORITY_LOAD|STRFLAG_FORCE_LOAD; //|STRFLAG_DONTDELETE;
				//if(pIntInst->m_bInUse)
					pIntInst->RequestObjects(reqFlags, true);

				CStreaming::LoadAllRequestedObjects();

				// convert dummy objects to real ones (every frame)
				if(pIntInst->m_bInstanceDataGenerated)
					pIntInst->AppendPortalActiveObjects(entityArray);
				pIntInst->AppendActiveObjects(entityArray);


				//---------------------------------------------
				// Instance comp entities within this interior
				// These entities will be added as CObject's and then 
				// be accessible via the object population as normal.

				static bool bBreak = false;
				CEntity * pIntEntity = NULL;
				pIntInst->RestartEntityInInteriorList();
				while(pIntInst->GetNextEntityInInteriorList(&pIntEntity))
				{
#if __DEV
					CheckEntityName(pIntEntity);
#endif
					if(EntityIntersectsCurrentNavMesh(pIntEntity))
					{
						CModelInfo::RequestAssets(pIntEntity->GetBaseModelInfo(), STRFLAG_FORCE_LOAD);
					}

					if(pIntEntity->GetIsTypeComposite())
					{
						CCompEntity * pComp = (CCompEntity*)pIntEntity;
						if(pComp->GetState() != CE_STATE_SYNC_STARTING &&
							pComp->GetState() != CE_STATE_STARTING && 
							pComp->GetState() != CE_STATE_START)
						{
							pComp->SetToStarting();
						}
						pComp->ImmediateUpdate();
					}
				}			

#if __DEV
				//PrintInterior(pIntInst);
#endif
			}
		}
		else
		{
			pIntProxy->SetRequestedState(CInteriorProxy::RM_SCRIPT, CInteriorProxy::PS_NONE, true);
			pIntProxy->CleanupInteriorImmediately();
			pIntProxy->SetRequiredByLoadScene(false);

			CInteriorInst * pIntInst = pIntProxy->GetInteriorInst();
			if(pIntInst)
			{
				CPortal::RemoveFromActiveInteriorList(pIntInst);
			}
		}
	}
}


void CNavMeshDataExporterTool::AddSearches(atArray<fwBoxStreamerSearch>& searchList)
{
	fwBoxStreamerAssetFlags iStreamTypes = /*fwBoxStreamerAsset::FLAG_MAPDATA_HIGH*/ fwBoxStreamerAsset::MASK_MAPDATA | fwBoxStreamerAsset::MASK_STATICBOUNDS;

	Vector3 meshMins = m_vNavMeshMins - Vector3(fExtra, fExtra, 0.0f);
	Vector3 meshMaxs = m_vNavMeshMaxs + Vector3(fExtra, fExtra, 0.0f);

	fwBoxStreamerSearch customSearch(
		spdAABB(VECTOR3_TO_VEC3V(meshMins), VECTOR3_TO_VEC3V(meshMaxs)),
		fwBoxStreamerSearch::TYPE_NAVMESHEXPORT,
		iStreamTypes, false);

	searchList.Grow() = customSearch;
}

void CNavMeshDataExporterTool::ResetAndOpenTriFile(const char *pFilename)
{
	Assert(!m_pCurrentTriFile);
	m_vNavMeshMins = Vector3(-m_fWidthOfNavMesh,-m_fDepthOfNavMesh,-1000.0f);
	m_vNavMeshMaxs = Vector3(m_fWidthOfNavMesh,m_fDepthOfNavMesh,1000.0f);

	m_iHasWaterInCurrentNavMesh = false;
	m_fAverageWaterLevelInCurrentNavMesh =  0.0f;
	m_pWaterLevels = NULL;

	OpenTriFile(pFilename);
}


void CNavMeshDataExporterTool::GridCellFinished()
{
	WriteExtrasFile(m_ExtrasFileName.c_str(), m_LadderInfos, m_CarNodesList, m_PortalBoundaries, m_DynamicEntitiesList);

	m_LadderInfos.clear();
	m_CarNodesList.clear();
	m_PortalBoundaries.clear();
	m_DynamicEntitiesList.clear();
}


void CNavMeshDataExporterTool::GridProcessFinished()
{
	CNavMeshDataExporter::SetExportMode(CNavMeshDataExporter::eNotExporting);
	CNavMeshDataExporter::SetActiveExporter(NULL);

//	gVpMan.GetRenderSettings().SetFlags(0x0); // all off

#if !HEIGHTMAP_GENERATOR_TOOL
	printf("Finished Exporting.\n");
#endif

	if(m_pExportCollisionLogFile)
		m_pExportCollisionLogFile->Close();

#if HEIGHTMAP_GENERATOR_TOOL
	fclose(fopen("x:/sync.hm1", "wb"));

	if (sysBootManager::IsDebuggerPresent())
	{
		system("pause"); // don't close the window yet, in case we want to see what got logged
	}

#if __WIN32PC
	// Hack to work around various shutdown problems, such as systems that assert or crash
	// just from static objects being created and then destroyed again.
	ExitProcess(0);
#else
	exit(0);
#endif
#endif

#if !HEIGHTMAP_GENERATOR_TOOL
	if(m_bLaunchCompilerAfterExporting)
	{
		LaunchNavMeshCompiler();

		if(m_bErrorsInExport)
		{
			while(1)
			{
				sysIpcYield(0);
			}
		}
		else
		{
#if __WIN32PC
			// Hack to work around various shutdown problems, such as systems that assert or crash
			// just from static objects being created and then destroyed again.
			ExitProcess(0);
#else
			exit(0);
#endif
		}
	}
#endif
}


void CNavMeshDataExporterTool::UpdateStreamingPos(const Vector3 &pos)
{
	CGameWorld::SetPlayerFallbackPos(pos);

	CFocusEntityMgr::GetMgr().SetPosAndVel(pos, VEC3_ZERO);
}


void CNavMeshDataExporterTool::NotifyMainGameLoopHasStarted()
{
	static float fTimeToWaitBeforeExport = 8.0f;
	fTimeToWaitBeforeExport -= fwTimer::GetTimeStep();

	if(fTimeToWaitBeforeExport > 0.0f)
		return;

	// First time, we'll check to see if the "-exportall" param exists.  If so then start exporting all the navmeshes from the world.
	static bool bHaveCheckedParams=false;
	if(!bHaveCheckedParams)
	{
		Displayf("CNavMeshDataExporterTool::NotifyMainGameLoopHasStarted()\n");

		bHaveCheckedParams = true;

		if(!ParseArgumentsAndStartExportCollision())
		{
			// Failure
		}
	}
}


void CNavMeshDataExporterTool::DisplayPoolUsage()
{
#if  __WIN32
	MEMORYSTATUS ms;
	GlobalMemoryStatus(&ms);
#endif

	m_iPeakNumBuildings = Max(m_iPeakNumBuildings, (int)CBuilding::GetPool()->GetNoOfUsedSpaces());
	m_iPeakNumObjects = Max(m_iPeakNumObjects, (int)CObject::GetPool()->GetNoOfUsedSpaces());
	m_iPeakNumDummyObjects = Max(m_iPeakNumDummyObjects, (int)CDummyObject::GetPool()->GetNoOfUsedSpaces());
	m_iPeakNumStreamedArchetypes = 0;//Max(m_iPeakNumStreamedArchetypes, fwArchetypeManager::GetCurrentNumStreamedArchetypes());

#if !HEIGHTMAP_GENERATOR_TOOL
#if __WIN32
	static sysMemAllocator & rageMemAllocator = sysMemAllocator::GetMaster();
#if !HEIGHTMAP_GENERATOR_TOOL
	size_t iTotalRageMemUsed = rageMemAllocator.GetMemoryUsed();
#endif
#endif

	Displayf("------------------------------------------------------------------\n");

	Displayf("Now Exporting : \"%s\\navmesh[%i][%i].tri\"\n", m_OutputPath, m_iCurrentSectorX, m_iCurrentSectorY);

	Displayf("(%.1f, %.1f, %.1f) to (%.1f, %.1f, %.1f)\n", m_vNavMeshMins.x, m_vNavMeshMins.y, m_vNavMeshMins.z, m_vNavMeshMaxs.x, m_vNavMeshMaxs.y, m_vNavMeshMaxs.z);

	Displayf("Current Num Buildings : %i/%i (Peak:%i)\n", CBuilding::GetPool()->GetNoOfUsedSpaces(), CBuilding::GetPool()->GetSize(), m_iPeakNumBuildings);

	Displayf("Current Num Objects : %i/%i (Peak:%i)\n", CObject::GetPool()->GetNoOfUsedSpaces(), CObject::GetPool()->GetSize(), m_iPeakNumObjects);

	Displayf("Current Num Dummys : %i/%i (Peak:%i)\n", CDummyObject::GetPool()->GetNoOfUsedSpaces(), CDummyObject::GetPool()->GetSize(), m_iPeakNumDummyObjects);

	// Displayf("Current Num Streamed Archetypes : %i/%i (Peak:%i)\n", fwArchetypeManager::GetCurrentNumStreamedArchetypes(), fwArchetypeManager::GetNumStreamedSlots(), m_iPeakNumStreamedArchetypes);

#if __WIN32
	Displayf("AvailPhysical : %u Mb, AvailVirtual  : %u Mb, RAGE Used : %i Mb\n", ms.dwAvailPhys / (1024*1024), ms.dwAvailVirtual / (1024*1024), iTotalRageMemUsed / (1024*1024));
#endif

	Displayf("------------------------------------------------------------------\n");

	if(m_pExportCollisionLogFile)
	{
		fprintf(m_pExportCollisionLogFile, "------------------------------------------------------------------\n");
		fprintf(m_pExportCollisionLogFile, "Now Exporting : \"%s\\navmesh[%i][%i].tri\"\n", m_OutputPath, m_iCurrentSectorX, m_iCurrentSectorY);
		fprintf(m_pExportCollisionLogFile, "Current Num Buildings : %i/%i (Peak:%i)\n", CBuilding::GetPool()->GetNoOfUsedSpaces(), CBuilding::GetPool()->GetSize(), m_iPeakNumBuildings);
		fprintf(m_pExportCollisionLogFile, "Current Num Objects : %i/%i (Peak:%i)\n", CObject::GetPool()->GetNoOfUsedSpaces(), CObject::GetPool()->GetSize(), m_iPeakNumObjects);
		fprintf(m_pExportCollisionLogFile, "Current Num Dummys : %i/%i (Peak:%i)\n", CDummyObject::GetPool()->GetNoOfUsedSpaces(), CDummyObject::GetPool()->GetSize(), m_iPeakNumDummyObjects);
	//	fprintf(m_pExportCollisionLogFile, "Current Num Streamed Archetypes : %i/%i (Peak:%i)\n", fwArchetypeManager::GetCurrentNumStreamedArchetypes(), fwArchetypeManager::GetNumStreamedSlots(), m_iPeakNumStreamedArchetypes);
#if __WIN32
		fprintf(m_pExportCollisionLogFile, "AvailPhysical : %u Mb, AvailVirtual  : %u Mb, RAGE Used : %i Mb\n", ms.dwAvailPhys / (1024*1024), ms.dwAvailVirtual / (1024*1024), iTotalRageMemUsed / (1024*1024));
#endif
		fprintf(m_pExportCollisionLogFile, "------------------------------------------------------------------\n");
		m_pExportCollisionLogFile->Flush();
	}
#endif
}


void CNavMeshDataExporterTool::AddInteriorsToExportList(atArray<CEntity*> & entitiesToExport /*, vector<fwSphere> & portalBoundaries*/)
{
	//g_pPortalBoundaries = &portalBoundaries;

	fwIsBoxIntersectingApprox cullBox(spdAABB( VECTOR3_TO_VEC3V(m_vNavMeshMins), VECTOR3_TO_VEC3V(m_vNavMeshMaxs)));

	CGameWorld::ForAllEntitiesIntersecting(
		&cullBox,
		AddInteriorsToExportListCB,
		(void*) &entitiesToExport,
		ENTITY_TYPE_MASK_MLO,
		SEARCH_LOCATION_INTERIORS|SEARCH_LOCATION_EXTERIORS,
		SEARCH_LODTYPE_ALL,
		SEARCH_OPTION_FORCE_PPU_CODEPATH
	);

}

// HACK: Make sure we don't already have physics added for this entity.
// TODO: Get to the bottom of why this is occurring
bool CNavMeshDataExporterTool::IsAlreadyInPhysicsLevel(CPhysical * pPhysical, phArchetype * pArchetype)
{
	static dev_bool bIgnoreTest = true;
	if(bIgnoreTest)
		return false;

	Vector3 vEntityPos = VEC3V_TO_VECTOR3(pPhysical->GetTransform().GetPosition());
	const int iMaxObjs = CPhysics::GetLevel()->GetMaxObjects();
	for(int l=0; l<iMaxObjs; l++)
	{
		if(CPhysics::GetLevel()->GetState(l)!=phLevelNew::OBJECTSTATE_NONEXISTENT)
		{
			phInst * pInstance = CPhysics::GetLevel()->GetInstance(l);
			CEntity * pUserData = (CEntity*)pInstance->GetUserData();
			if(pUserData==pPhysical)
				return true;
			if(pArchetype)
			{
				Vector3 vInstPos( pInstance->GetPosition().GetXf(), pInstance->GetPosition().GetYf(), pInstance->GetPosition().GetZf() );
				if(pInstance->GetArchetype()==pArchetype && vEntityPos.IsClose(vInstPos, 0.01f))
					return true;
			}
		}
	}
	return false;
}

#if HEIGHTMAP_GENERATOR_TOOL
class ExportProgressDisplay // TODO -- move this class somewhere accessible by heightmapgenerator.cpp
{
public:
	ExportProgressDisplay(const char* format, ...) : m_progress(0.0f), m_numChars(0), m_clockStart(0), m_clockPrev(0), m_reset(false)
	{
		char temp[1024] = "";
		va_list args;
		va_start(args, format);
		vsnprintf(temp, sizeof(temp), format, args);
		va_end(args);

		sprintf(m_str, "%s .. ", temp);
		fprintf(stdout, m_str);
		fflush(stdout);

		Update(-1, 1);

		m_clockStart = clock();
		m_clockPrev = m_clockStart;

		m_remainingTimeEnabled = true;
		m_remainingTime = 0;
		m_remainingTimeStr[0] = '\0';
	}

	void UpdateInternal(float f, int index, int count, bool bRedisplay = false)
	{
		if (m_reset)
		{
			fprintf(stdout, m_str);
			fflush(stdout);

			m_numChars = 0;
			m_reset = false;
		}

		const clock_t c = clock();

		m_progress = f;
		char temp[64] = "";

		if (count > 0)
		{
			sprintf(temp, "%.3f%% (%d of %d)", 100.0f*m_progress, index + 1, count);
		}
		else
		{
			sprintf(temp, "%.3f%%", 100.0f*m_progress);
		}

		Erase(m_numChars);
		fprintf(stdout, "%s", temp);
		fflush(stdout);
		m_numChars = (int)strlen(temp);

		if (m_remainingTimeEnabled)
		{
			const float elapsed = (float)(c - m_clockStart)/(float)CLOCKS_PER_SEC;

			if (elapsed > 5.0f && m_progress > 0.0f)
			{
				if (c - m_remainingTime > CLOCKS_PER_SEC) // update once per second
				{
					const float secs = elapsed*(1.0f - m_progress)/m_progress;
					const float mins = secs/60.0f;
					const float hrs  = mins/60.0f;

					if      (secs <= 100.0f) { sprintf(m_remainingTimeStr, " (%.2f secs remaining)", secs); }
					else if (mins <= 100.0f) { sprintf(m_remainingTimeStr, " (%.2f mins remaining)", mins); }
					else                     { sprintf(m_remainingTimeStr, " (%.2f hours remaining)", hrs); }

					m_remainingTime = c;
				}

				fprintf(stdout, m_remainingTimeStr);
				fflush(stdout);
				m_numChars += (int)strlen(m_remainingTimeStr);
			}
		}

		if (m_clockPrev && !bRedisplay) // if too much time elapsed, print warning
		{
			const float elapsedSinceLastUpdate = (float)(c - m_clockPrev)/(float)CLOCKS_PER_SEC;

			if (elapsedSinceLastUpdate > 20.0f)
			{
				fprintf(stdout, " -- Slow update (%.2f secs)\n", elapsedSinceLastUpdate);
				fprintf(stdout, "%s", m_str);
				fflush(stdout);

				m_numChars = 0;

				UpdateInternal(f, index, count, true);
			}
		}

		m_clockPrev = c;
	}

	void Update(int i, int n, bool bShowIndexAndCount = false)
	{
		UpdateInternal((float)(i + 1)/(float)n, i, bShowIndexAndCount ? n : 0);
	}

	template <typename T> void Update(int i, const T& v, bool bShowIndexAndCount = false)
	{
		UpdateInternal((float)(i + 1)/(float)v.size(), i, bShowIndexAndCount ? (int)v.size() : 0);
	}

	void Reset()
	{
		m_reset = true;
	}

	void End(const char* msg = "done", bool bShowTime = true)
	{
		Erase(m_numChars);

		fprintf(stdout, "%s.", msg);

		if (bShowTime)
		{
			char totalStr[64] = "";

			if (1) // show total
			{
				static clock_t total = 0;

				total += clock() - m_clockStart;

				const float totalSecs = (float)total/(float)CLOCKS_PER_SEC;
				const float totalMins = totalSecs/60.0f;
				const float totalHrs  = totalMins/60.0f;

				if      (totalSecs <=   1.0f) { sprintf(totalStr, ""); }
				else if (totalSecs <= 100.0f) { sprintf(totalStr, ", total %.2f secs", totalSecs); }
				else if (totalMins <= 100.0f) { sprintf(totalStr, ", total %.2f mins", totalMins); }
				else                          { sprintf(totalStr, ", total %.2f hours", totalHrs); }
			}

			const float secs = (float)(clock() - m_clockStart)/(float)CLOCKS_PER_SEC;
			const float mins = secs/60.0f;
			const float hrs  = mins/60.0f;

			if      (secs <=   1.0f) { fprintf(stdout, ""); }
			else if (secs <= 100.0f) { fprintf(stdout, " (%.2f secs%s)", secs, totalStr); }
			else if (mins <= 100.0f) { fprintf(stdout, " (%.2f mins%s)", mins, totalStr); }
			else                     { fprintf(stdout, " (%.2f hours%s)", hrs, totalStr); }
		}

		if (m_remainingTimeEnabled)
		{
			fprintf(stdout, "                             \n"); // extra space to clear leftover chars
		}
		else
		{
			fprintf(stdout, "    \n"); // extra space to clear leftover chars
		}
	}

protected:
	static void Erase(int numChars)
	{
		for (int k = 0; k < numChars; k++)
		{
			fprintf(stdout, "%c", 0x08);
		}

		fflush(stdout);
	}

	char    m_str[256];
	float   m_progress;
	int     m_numChars;
	clock_t m_clockStart;
	clock_t m_clockPrev;
	bool    m_reset;

	bool    m_remainingTimeEnabled;
	clock_t m_remainingTime;
	char    m_remainingTimeStr[64];
};

void CNavMeshDataExporterTool::ExportMapData(const atArray<u32> & slotList, vector<Vector3> & triangleVertices, vector<phMaterialMgr::Id> & triangleMaterials, vector<u16> & colPolyFlags, vector<u32> & archetypeFlags, vector<fwNavTriData> & triDataArray)
{
	fwMapDataStore & store = fwMapDataStore::GetStore();

	for (s32 i=0; i<slotList.GetCount(); i++)
	{
		u32 slot = slotList[i];
		if (slot != 0xffffffff)
		{
			bool bLoaded = store.HasObjectLoaded(strLocalIndex(slot));
			if (bLoaded)
			{
				const CMapData * pMapData = static_cast<const CMapData*>(store.Get(strLocalIndex(slot))->GetMapData_public());

				if (pMapData)
				{
					for (int j = 0; j < pMapData->m_DistantLODLightsSOA.m_position.GetCount(); j++)
					{
						const Vec3V centre
						(
							pMapData->m_DistantLODLightsSOA.m_position[j].x,
							pMapData->m_DistantLODLightsSOA.m_position[j].y,
							pMapData->m_DistantLODLightsSOA.m_position[j].z
						);
						const Vec3V points[] =
						{
							centre + Vec3V(-1.0f, -1.0f, 0.0f),
							centre + Vec3V(+1.0f, -1.0f, 0.0f),
							centre + Vec3V(+1.0f, +1.0f, 0.0f),
							centre + Vec3V(-1.0f, +1.0f, 0.0f),
						};

						fwNavTriData triData;

						triData.m_iFlagsStruct.m_bIsDistantLODLight = true;

						AddPolyToList(points, NELEM(points), false, triangleVertices, triangleMaterials, phMaterialMgr::DEFAULT_MATERIAL_ID, colPolyFlags, EXPORTPOLYFLAG_DONT_CREATE_NAVMESH_ON_THIS, archetypeFlags, 0, triDataArray, triData);
					}

					for (int j = 0; j < pMapData->m_boxOccluders.GetCount(); j++)
					{
						const BoxOccluder & occluder = pMapData->m_boxOccluders[j];

						Vec3V verts[8];
						occluder.CalculateVerts(verts);

						Vec3V pmin = verts[0];
						Vec3V pmax = pmin;

						for (int j = 1; j < NELEM(verts); j++)
						{
							pmin = Min(verts[j], pmin);
							pmax = Max(verts[j], pmax);
						}
#if __ASSERT
						const u8 expectedIndices[] = // from boxoccluder.cpp
						{
							2,3,1, 0,2,1, // 2,3,1,0,
							4,5,3, 2,4,3, // 4,5,3,2,
							6,7,5, 4,6,5, // 6,7,5,4,
							0,1,7, 6,0,7, // 0,1,7,6,
							3,5,7, 1,3,7, // 3,5,7,1,
							4,2,0, 6,4,0, // 4,2,0,6,
						};
#endif // __ASSERT
						const u8 quadIndices[] =
						{
							2,3,1,0,
							4,5,3,2,
							6,7,5,4,
							0,1,7,6,
							3,5,7,1,
							4,2,0,6,
						};

						static bool bChecked = false;

						if (!bChecked) // check once that the triangle indices match what we expect, if not then the quad indices are probably wrong too
						{
							bChecked = true;

							Assert(occluder.GetNumOccluderIndices() == NELEM(expectedIndices));
							Assert(memcmp(occluder.GetOccluderIndices(), expectedIndices, NELEM(expectedIndices)*sizeof(u8)) == 0);
						}

						for (int j = 0; j < NELEM(quadIndices); j += 4)
						{
							const Vec3V points[] =
							{
								verts[quadIndices[j + 0]],
								verts[quadIndices[j + 1]],
								verts[quadIndices[j + 2]],
								verts[quadIndices[j + 3]],
							};

							fwNavTriData triData;

							triData.m_iFlagsStruct.m_bIsOccluder = true;
							triData.m_iFlagsStruct.m_bIsBoxOccluder = true;
							triData.m_iFlagsStruct.m_iOccluderFlags = 0;

							AddPolyToList(points, NELEM(points), false, triangleVertices, triangleMaterials, phMaterialMgr::DEFAULT_MATERIAL_ID, colPolyFlags, EXPORTPOLYFLAG_DONT_CREATE_NAVMESH_ON_THIS, archetypeFlags, 0, triDataArray, triData);
						}
					}

					for (int j = 0; j < pMapData->m_occludeModels.GetCount(); j++)
					{
						const OccludeModel & occluder = pMapData->m_occludeModels[j];

						if (!occluder.IsEmpty())
						{
							Vec3V verts[rstFastTiler::MaxIndexedVerts];
							occluder.UnPackVertices(verts, rstFastTiler::MaxIndexedVerts, ScalarV(V_HALF));

							for (int k = 0; k < occluder.GetNumIndices(); k += 3)
							{
								const Vec3V points[] =
								{
									verts[occluder.GetIndices()[k + 0]],
									verts[occluder.GetIndices()[k + 1]],
									verts[occluder.GetIndices()[k + 2]],
								};

								fwNavTriData triData;

								triData.m_iFlagsStruct.m_bIsOccluder = true;
								triData.m_iFlagsStruct.m_bIsBoxOccluder = false;
								triData.m_iFlagsStruct.m_iOccluderFlags = occluder.GetFlags();

								AddPolyToList(points, NELEM(points), false, triangleVertices, triangleMaterials, phMaterialMgr::DEFAULT_MATERIAL_ID, colPolyFlags, EXPORTPOLYFLAG_DONT_CREATE_NAVMESH_ON_THIS, archetypeFlags, 0, triDataArray, triData);
							}
						}
					}
				}
			}
		}
	}
}

#endif

const int iNumWaitFrames = (CInteriorProxy::STATE_COOLDOWN * CInteriorProxy::PS_NUM_STATES) + 4;
int iWaitFrames = iNumWaitFrames;

#if HEIGHTMAP_GENERATOR_TOOL
PARAM(StartAt, "Start at specific tile");
#endif

bool CNavMeshDataExporterTool::ProcessCollisionExport()
{
#if HEIGHTMAP_GENERATOR_TOOL
	static ExportProgressDisplay* pProgress = NULL;

	if (pProgress == NULL)
	{
		const int iNumTilesX = (m_iEndSectorX - m_iStartSectorX)/CPathServerExtents::m_iNumSectorsPerNavMesh;
		const int iNumTilesY = (m_iEndSectorY - m_iStartSectorY)/CPathServerExtents::m_iNumSectorsPerNavMesh;

		pProgress = rage_new ExportProgressDisplay("exporting %d tri files", iNumTilesX*iNumTilesY);
		pProgress->Update(0, iNumTilesX*iNumTilesY, true);
	}
#endif

	if(CNavMeshDataExporter::GetExportMode() != CNavMeshDataExporter::eMapExport)
	{
		Assert(0);
		return false;
	}

	CalcCurrentNavMeshExtents();

	fwTimer::Update();

	CGameWorld::SetPlayerFallbackPos(m_vCentreOfCurrentNavMesh);
	CFocusEntityMgr::GetMgr().SetPosAndVel(m_vCentreOfCurrentNavMesh, VEC3_ZERO);
	CIplCullBox::Reset();

	static bool bOutputMissingIPLs = true;
	static bool bOutputWaitExtra = true;


	// Request IPL groups every frame
	// (Without this they will not load)
	RequestDefaultIplGroups();
	RequestDefaultEntitySets();

	// Ensure mapdata is loaded
	// (Without this we get no exterior world collision)

	fwBoxStreamerAssetFlags iIplReqFlags = fwBoxStreamerAsset::FLAG_MAPDATA_HIGH;
#if HEIGHTMAP_GENERATOR_TOOL
	//iIplReqFlags |= fwBoxStreamerAsset::FLAG_MAPDATA_LOW; // pull these in for distant LOD lights
#endif

	char * pIgnoreSpecificIPL = NULL;  //"v_metro_interior_211_metro_newwalk3_milo_";	// see url:bugstar:1072601

	//---------------------------------------------------------------------
	// Add references to all IPLs which are intersecting our export region
	// Priority/force load any IPLs which are not yet loaded

	fwMapDataStore & store = fwMapDataStore::GetStore();
	static bool bAddedIplRefs = false;

	static Vector3 reqIplMinsLast(0.0f, 0.0f, 0.0f);
	static Vector3 reqIplMaxsLast(0.0f, 0.0f, 0.0f);

	static float fExtraForIPL = 0.0f;
	Vector3 reqIplMins = m_vNavMeshMins - Vector3(fExtraForIPL, fExtraForIPL, 0.0f);
	Vector3 reqIplMaxs = m_vNavMeshMaxs + Vector3(fExtraForIPL, fExtraForIPL, 0.0f);

	spdAABB aabb(VECTOR3_TO_VEC3V(reqIplMins), VECTOR3_TO_VEC3V(reqIplMaxs));
	spdAABB aabbLast(VECTOR3_TO_VEC3V(reqIplMinsLast), VECTOR3_TO_VEC3V(reqIplMaxsLast));

	atArray<const char*> IPLsNotLoadedNames;
	const char * pNotLoadedName = NULL;

	bool bAllIPLsLoaded = true;

	//--------------------
	// Request IPLs

	atArray<u32> slotList;
	store.GetBoxStreamer().GetIntersectingAABB(aabb, iIplReqFlags, slotList);		
	atArray<u32> slotListLast;
	store.GetBoxStreamer().GetIntersectingAABB(aabbLast, iIplReqFlags, slotListLast);

	for(int s=0; s<slotListLast.GetCount(); s++)
	{
		if(slotList.Find(slotListLast[s])==-1)
			slotList.PushAndGrow(slotListLast[s]);
	}

	for (s32 slot=0; slot<store.GetSize(); slot++)
	{
		if(!store.GetBoxStreamer().GetIsIgnored(slot))
		{
			if(slotList.Find(slot)!=-1)
			{
				bool bLoaded = store.HasObjectLoaded(strLocalIndex(slot));
				if (!bLoaded)
				{
					pNotLoadedName = store.GetName(slot);
					if(pNotLoadedName)
						IPLsNotLoadedNames.PushAndGrow(pNotLoadedName);

					if(pIgnoreSpecificIPL && strcmp(pIgnoreSpecificIPL, pNotLoadedName)==0)
					{
						// ignore known artwork which has an invalid min/max
					}
					else
					{
						bAllIPLsLoaded = false;
						store.StreamingRequest(strLocalIndex(slot), STRFLAG_PRIORITY_LOAD|STRFLAG_FORCE_LOAD|STRFLAG_LOADSCENE);
					}
				}
				if(!bAddedIplRefs)
				{
					store.SetRequiredFlag(slot, STRFLAG_MISSION_REQUIRED);	//STRFLAG_DONTDELETE
				}
			}
			// Remove anything not in this area
			else
			{
				if(!bAddedIplRefs)
				{
					store.ClearRequiredFlag(slot, STRFLAG_MISSION_REQUIRED);	//STRFLAG_DONTDELETE
				}
			}
		}
	}


	//--------------------------
	// Request entities in IPLs

	for(s32 s=0; s<INSTANCE_STORE.GetSize(); s++)
	{
		fwMapDataDef * pDef = INSTANCE_STORE.GetSlot(strLocalIndex(s));
		//const bool bStreamed = true;

		if(pDef && pDef->IsLoaded()) //&& bStreamed && INSTANCE_STORE.GetStreamingFlags(i)&STR_DONTDELETE_MASK)
		{
			fwEntity ** entities = pDef->GetEntities();

			if (entities)
			{
				for (u32 i=0; i<pDef->GetNumEntities(); i++)
				{
					CEntity * pEntity = (CEntity*)entities[i];
					if(pEntity)
					{
						if(CheckEntityName(pEntity))
							Displayf("Found entity in ipl store (%s).", INSTANCE_STORE.GetName(s));

						if(pEntity->GetBaseModelInfo() && EntityIntersectsCurrentNavMesh(pEntity))
						{
							CModelInfo::RequestAssets(pEntity->GetBaseModelInfo(), STRFLAG_FORCE_LOAD);
						}
					}
				}
			}
		}
	}

	//------------------------------------------------------------------------

	bAddedIplRefs = true;

	if(!bAllIPLsLoaded)
		bOutputMissingIPLs = false;

	//**********************************************************************

	bool bHasObjectsIntersectingNavMesh = false;

	int o;
	for(o=0; o<CObject::GetPool()->GetSize(); o++)
	{
		CObject * pObj = CObject::GetPool()->GetSlot(o);
		if(pObj && EntityIntersectsCurrentNavMesh(pObj))
		{
			bHasObjectsIntersectingNavMesh = true;
		}
	}
	for(o=0; o<CDummyObject::GetPool()->GetSize(); o++)
	{
		CDummyObject * pDummyObj = CDummyObject::GetPool()->GetSlot(o);
		if(pDummyObj && EntityIntersectsCurrentNavMesh(pDummyObj))
		{
			bHasObjectsIntersectingNavMesh = true;
		}
	}

	//**********************************************************************

	//sysIpcSleep(0);

	//CStreaming::LoadAllRequestedObjects();

	//****************************************************************************************
	// If we have no buildings with physics to load, then skip the waiting process.
	// NB : Maybe this will cause problems when we want to load in objects?

	const bool bCanSkipWait = 
		(m_bHasBuildingsWithPhysics == false) &&
		(bHasObjectsIntersectingNavMesh == false) &&
		(m_bHasCompEntities == false) &&
		(bAllIPLsLoaded == true) &&
		(g_bWaitingForInteriorArchetypes == false) &&
		(m_bAlwaysWaitForStreaming == false);

	//****************************************************************************************
	// Always allow a certain amount of time for streaming to request physics models nearby..

	m_fTimeUntilWeExportThisNavMesh -= fwTimer::GetSystemTimeStep();	// fwTimer::GetTimeStep();
	iWaitFrames--;
	
	CStreaming::LoadAllRequestedObjects();

	if((m_fTimeUntilWeExportThisNavMesh > 0.0f && !bCanSkipWait) || iWaitFrames > 0)
	{
		return false;
	}

	//************************************************************************************************
	// Wait for the physics to load for all the buildings
	// This can get caught in an infinite loop, because sometimes the streaming is unable to
	// load everything.  To catch this case I've got a maximum time which we'll wait for the streaming
	// and I've also put a CStreaming::Purge.. at the end of the export so we're not left waiting for
	// the same object(s) forever

	const int iNumObjectsRequested = CStreaming::GetNumberObjectsRequested();
	const bool bWaitLonger =
		(iNumObjectsRequested > 0) ||
		(g_bAllInteriorsInstanced == false) ||
		(g_bWaitingForInteriorArchetypes == true) ||
		(bAllIPLsLoaded == false)
#if defined(HACK_COMP_ENTITY_EXPORT_CODE)
		|| (bAllCompEntitiesInStartState == false)
#endif
		;

	if( (bWaitLonger && m_fTimeUntilWeExportThisNavMesh > -MAX_TIME_TO_WAIT_FOR_STREAMING_TO_LOAD) )
	{
		static bool bAlwaysReport = false; // can set this to true in the debugger
		bool bSilent = false;
#if HEIGHTMAP_GENERATOR_TOOL
		if (iNumObjectsRequested > 0 ||
			g_bAllInteriorsInstanced == false ||
			g_bWaitingForInteriorArchetypes == true ||
			bAllIPLsLoaded == false)
		{
			// report
		}
		else if (!bAlwaysReport)
		{
			bSilent = true; // don't report if it's just bAllCompEntitiesInStartState == false
		}
#define LOG_ERR_FILE m_pExportCollisionLogFile
#else
#define LOG_ERR_FILE stdout
#endif
		if (!bSilent && LOG_ERR_FILE)
		{
			if(bOutputWaitExtra || bAlwaysReport)
			{
				char reasons[256] = "";

				if (iNumObjectsRequested > 0)					{ strcat(reasons, atVarString(" objreq=%d", iNumObjectsRequested).c_str()); } 
				if (g_bAllInteriorsInstanced == false)			{ strcat(reasons, atVarString(" !intinst=%d", g_InteriorsNotLoaded.GetCount()).c_str()); }
				if (g_bWaitingForInteriorArchetypes == true)	{ strcat(reasons, " waitint"); }
				if (bAllIPLsLoaded == false)					{ strcat(reasons, atVarString(" !allIPLs=%d", IPLsNotLoadedNames.GetCount()).c_str()); }
#if defined(HACK_COMP_ENTITY_EXPORT_CODE)
				if (bAllCompEntitiesInStartState == false)		{ strcat(reasons, atVarString(" !allcomp=%d", CompEntityNotStartNames.GetCount()).c_str()); }
#endif
				fprintf(LOG_ERR_FILE, "%sNot all entities are loaded:%s\n", HEIGHTMAP_GENERATOR_TOOL ? " -- " : "", reasons);

				for (int i = 0; i < IPLsNotLoadedNames.GetCount(); i++)
				{
					fprintf(LOG_ERR_FILE, "  failed to load IPL %s\n", IPLsNotLoadedNames[i]);
				}

				for (int i = 0; i < g_InteriorsNotLoaded.GetCount(); i++)
				{
					fprintf(LOG_ERR_FILE, "  failed to load INTERIOR %s\n", g_InteriorsNotLoaded[i].c_str());
				}

#if defined(HACK_COMP_ENTITY_EXPORT_CODE)
				for (int i = 0; i < CompEntityNotStartNames.GetCount(); i++)
				{
					fprintf(LOG_ERR_FILE, "  composite entity %s not in start state\n", CompEntityNotStartNames[i]);
				}
#endif

#if HEIGHTMAP_GENERATOR_TOOL
				LOG_ERR_FILE->Flush();
				pProgress->Reset();
#endif
			}
		}

		bOutputWaitExtra = false;
		return false;
	}

	static bool bLoadedAllRequested = false;
	if(!bLoadedAllRequested)
	{
		CStreaming::LoadAllRequestedObjects();
		bLoadedAllRequested = true;
		return false;
	}
	bLoadedAllRequested = false;



	bOutputMissingIPLs = true;
	bOutputWaitExtra = true;

	m_bPrintOutAllLoadedCollision = false;
	if(m_bPrintOutAllLoadedCollision)
	{
		Displayf("---------------------------\n");
		Displayf("Loaded IPLs\n");
		Displayf("---------------------------\n");

		for (s32 slot=0; slot<store.GetSize(); slot++)
		{
			if(store.HasObjectLoaded(strLocalIndex(slot)))
			{
				const char * pIplName = store.GetName(slot);
				//fwMapDataDef * pIplDef = store.GetSlot(slot);

				Displayf("%i) %s", slot, pIplName);
			}
		}

		Displayf("---------------------------\n");
		Displayf("Static bounds\n");
		Displayf("---------------------------\n");

		phLevelNew * phLevel = CPhysics::GetLevel();

		phIterator iterator(phIterator::PHITERATORLOCKTYPE_READLOCK);	// PHITERATORLOCKTYPE_WRITELOCK ?

		// Use this option to get all objects matching the req'd state
		iterator.InitCull_All();
		iterator.SetStateIncludeFlags(phLevelNew::STATE_FLAG_FIXED);

		u16 iIndexInLevel = phLevel->GetFirstCulledObject(iterator);

		while(iIndexInLevel != phInst::INVALID_INDEX)
		{
			phInst * pInstance = phLevel->GetInstance(iIndexInLevel);
			if(pInstance)
			{
				CEntity * pOctreeEntity = (CEntity*)pInstance->GetUserData();
				if (pOctreeEntity->GetOwnedBy() == ENTITY_OWNEDBY_STATICBOUNDS)
				{
					//const fwBoundDef* pDef = g_StaticBoundsStore.GetSlot(pOctreeEntity->GetIplIndex());
					const char * pStaticName = g_StaticBoundsStore.GetName(pOctreeEntity->GetIplIndex());
					Vector3 vEntityPos = VEC3V_TO_VECTOR3(pOctreeEntity->GetTransform().GetPosition());
					Displayf("%s (%.1f,%.1f,%.1f) (inst:0x%p) \n", pStaticName, vEntityPos.x, vEntityPos.y, vEntityPos.z, pOctreeEntity->GetCurrentPhysicsInst());
				}
			}
			iIndexInLevel = phLevel->GetNextCulledObject(iterator);
		}

		/*

		Displayf("---------------------------\n");
		Displayf("Buildings with physics\n");
		Displayf("---------------------------\n");

		dbgBuildingsWithPhysicsList.Clear();

		GetAllBuildingsWithPhysics(INSTANCE_STORE, &dbgBuildingsWithPhysicsList);
		CLink<CEntity*> * pBuildingPtr = dbgBuildingsWithPhysicsList.GetFirst();
		while(pBuildingPtr)
		{
			CEntity* pEntity = pBuildingPtr->item;
			if(pEntity)
			{
				CBaseModelInfo* pModelInfo = pEntity->GetBaseModelInfo();
				const char * pRoomName = pModelInfo->GetModelName();

				Vector3 vEntityPos = VEC3V_TO_VECTOR3(pEntity->GetTransform().GetPosition());
				Displayf("%s (MI:%i) (%.1f,%.1f,%.1f) (inst:0x%p) \n", pRoomName, pEntity->GetModelIndex(), vEntityPos.x, vEntityPos.y, vEntityPos.z, pEntity->GetCurrentPhysicsInst());

				if(pModelInfo->HasPhysics() && !pEntity->GetCurrentPhysicsInst())
				{
					Displayf("PHYSICS NOT LOADED FOR THIS BUILDING\n");
				}
			}
			else
			{
				Displayf("(NULL)\n");
			}
			pBuildingPtr = pBuildingPtr->GetNext();
		}
		dbgBuildingsWithPhysicsList.Clear();
		*/

		Displayf("--------------------------------\n");
		Displayf("Physics building request list\n");
		Displayf("--------------------------------\n");


		CLink<RegdEnt> * pTestPtr = CPhysics::ms_physicsReqList.GetFirst();
		while(pTestPtr)
		{
			CEntity * pEntity = pTestPtr->item;
			if(pEntity)
			{
				CBaseModelInfo* pModelInfo = pEntity->GetBaseModelInfo();
				const char * pRoomName = pModelInfo->GetModelName();

				Vector3 vEntityPos = VEC3V_TO_VECTOR3(pEntity->GetTransform().GetPosition());
				Displayf("%s (MI:%i) (%.1f,%.1f,%.1f) (inst:0x%p) \n", pRoomName, pEntity->GetModelIndex(), vEntityPos.x, vEntityPos.y, vEntityPos.z, pEntity->GetCurrentPhysicsInst());
			}
			else
			{
				Displayf("(NULL)\n");
			}
			pTestPtr = pTestPtr->GetNext();
		}

		Displayf("---------------------------\n");
		Displayf("Objects\n");
		Displayf("---------------------------\n");

		CObject::Pool* pObjectPool = CObject::GetPool();

		for(int o=0; o<pObjectPool->GetSize(); o++)
		{
			CObject * pObj = pObjectPool->GetSlot(o);
			if(!pObj)
				continue;

			CBaseModelInfo * pModelInfo = pObj->GetBaseModelInfo();
			Assert(pModelInfo);

			Vector3 vEntityPos = VEC3V_TO_VECTOR3(pObj->GetTransform().GetPosition());
			Displayf("%s (MI:%i) (%.1f,%.1f,%.1f) (inst:0x%p) \n", pModelInfo->GetModelName(), pObj->GetModelIndex(), vEntityPos.x, vEntityPos.y, vEntityPos.z, pObj->GetCurrentPhysicsInst());
		}

		Displayf("---------------------------\n");
		Displayf("Dummy Objects\n");
		Displayf("---------------------------\n");

		CDummyObject::Pool* pDummyPool = CDummyObject::GetPool();

		for(int d=0; d<pDummyPool->GetSize(); d++)
		{
			CDummyObject * pDummy = pDummyPool->GetSlot(d);
			if(!pDummy)
				continue;

			CBaseModelInfo * pModelInfo = pDummy->GetBaseModelInfo();
			Assert(pModelInfo);

			Vector3 vEntityPos = VEC3V_TO_VECTOR3(pDummy->GetTransform().GetPosition());
			Displayf("%s (MI:%i) (%.1f,%.1f,%.1f) (inst:0x%p) \n", pModelInfo->GetModelName(), pDummy->GetModelIndex(), vEntityPos.x, vEntityPos.y, vEntityPos.z, pDummy->GetCurrentPhysicsInst());
		}

		Displayf("\n");
	}

#if !__NAVMESH_EXPORT_DISABLE_COLLISION_EXPORT

	m_DynamicEntitiesList.clear();

	bool bFlush = false; //m_bHasBuildingsWithPhysics;

	m_bHasBuildingsWithPhysics = false;
	m_bHasCompEntities = false;
	fwTimer::Suspend();

	char extrasFileName[1024];
	GenerateFileNames(m_TriFileName, sizeof(m_TriFileName), extrasFileName, sizeof(extrasFileName));

	// Stop rage from whining
	phBound::SetBoundFlags(0);

	// We'll build up a list of triangles & surface-types
	// At the end we will create a physics bounds from all of the triangles, and save
	// We don't build up the bound as we go along, because we can't add verts/polys to a phBoundGeometry dynamically
	vector<Vector3> triangleVertices;
	vector<phMaterialMgr::Id> triangleMaterials;
	vector<u16> colPolyFlags;
	vector<u32> archetypeFlags;
	vector<fwNavTriData> triDataArray;

	m_iNumTrianglesThisTriFile = 0;
	DisplayPoolUsage();


	Vector3 vMid = (m_vNavMeshMins + m_vNavMeshMaxs) * 0.5f;
	Vector3 vNavMeshMinsExtra = m_vNavMeshMins;
	Vector3 vNavMeshMaxsExtra = m_vNavMeshMaxs;


	CTrain::CullTrackNodesToArea(m_vNavMeshMins - Vector3(10.0f, 10.0f, 10.0f), m_vNavMeshMaxs + Vector3(10.0f, 10.0f, 10.0f));


	//************************************************************
	//	Find out the water level.
	//	This data will be written into the start of the tri file.
	//************************************************************

	FindWaterLevelForCurrentNavMesh();


	//********************************************
	// Now export all the bounds from the octree
	//********************************************

	m_PotentialEntitiesToExport.clear();


	if(m_bExportOctrees)
	{
		phLevelNew * phLevel = CPhysics::GetLevel();

		phIterator iterator(phIterator::PHITERATORLOCKTYPE_READLOCK);	// PHITERATORLOCKTYPE_WRITELOCK ?

		// Use this option to get all objects matching the req'd state
		iterator.InitCull_All();
		iterator.SetStateIncludeFlags(phLevelNew::STATE_FLAG_FIXED);

		u16 iIndexInLevel = phLevel->GetFirstCulledObject(iterator);

		while(iIndexInLevel != phInst::INVALID_INDEX)
		{
			phInst * pInstance = phLevel->GetInstance(iIndexInLevel);
			if(pInstance)
			{
				CEntity * pOctreeEntity = (CEntity*)pInstance->GetUserData();

				if(AssertVerify(pOctreeEntity))
				{
					Assert(pOctreeEntity->GetType()!=ENTITY_TYPE_DUMMY_OBJECT);

					if(pOctreeEntity->GetType()!=ENTITY_TYPE_DUMMY_OBJECT)
					{
						if(CheckEntityName(pOctreeEntity))
							Displayf("Found entity in octree.");

						if( !IsEntityInList(pOctreeEntity, m_PotentialEntitiesToExport) )
						{
							m_PotentialEntitiesToExport.PushAndGrow(pOctreeEntity);
						}

						AddDynamicObjectFromMapCB(pOctreeEntity, &m_DynamicEntitiesList);
					}
				}
			}

			iIndexInLevel = phLevel->GetNextCulledObject(iterator);
		}
	}

	//**************************************************************************
	// Now export all the objects in the world which are 'fixed for navigation'
	//**************************************************************************

	if(m_bExportObjects)
	{
		CObject::Pool* pObjectPool = CObject::GetPool();

		for(int o=0; o<pObjectPool->GetSize(); o++)
		{
			CObject * pObj = pObjectPool->GetSlot(o);
			if(!pObj)
				continue;

			if(CheckEntityName(pObj))
				Displayf("Found entity in objects pool.");

			AddDynamicObjectFromMapCB(pObj, &m_DynamicEntitiesList);

			if(IsEntityInList(pObj, m_PotentialEntitiesToExport))
				continue;

			CBaseModelInfo * pModelInfo = pObj->GetBaseModelInfo();

			bool bFixedForNav = pModelInfo->GetIsFixedForNavigation() != 0;
			bool bFixedFlagSet = pObj->IsBaseFlagSet(fwEntity::IS_FIXED) || pModelInfo->GetIsFixed();
			bool bIsLadder = pModelInfo->GetIsLadder() != 0;
			bool bScriptObject = (pObj->GetOwnedBy() == ENTITY_OWNEDBY_SCRIPT);
			bool bClimbableObject = pModelInfo->GetIsClimbableByAI();

			if(bScriptObject)
				continue;

			// Export ladder info so we can link ladders up in the navmesh
			if(bIsLadder && EntityIntersectsCurrentNavMesh(pObj))
			{
				ExportLadderInfo(pObj, -1, &m_LadderInfos);
			}

			// Don't export any geometry which isn't avoided by peds
			if(pModelInfo->GetNotAvoidedByPeds())
				continue;

			// If this object is not fixed in any way, then don't export its geometry
			if(!bFixedFlagSet && !bFixedForNav && !bClimbableObject)
				continue;

			// Also add ladders to the export list, as we will want to export them if they are fixed objects
			Assert(*(reinterpret_cast<u32*>(pObj))!=0xdddddddd);
			m_PotentialEntitiesToExport.PushAndGrow(pObj);
		}
	}


	//*****************************************************************
	// Now export all the extra bounds from the buildings in the world
	//*****************************************************************

	/*

	static CLinkList<CEntity*> * pBuildingsWithPhysicsList = NULL;
	if(!pBuildingsWithPhysicsList)
	{
		pBuildingsWithPhysicsList = rage_new CLinkList<CEntity*>;
		memset(pBuildingsWithPhysicsList, 0, sizeof(CLinkList<CEntity*>));
		pBuildingsWithPhysicsList->Init(32768);
		pBuildingsWithPhysicsList->Clear();
	}

	GetAllBuildingsWithPhysics(INSTANCE_STORE, pBuildingsWithPhysicsList);
	CLink<CEntity*> * pBuildingPtr = pBuildingsWithPhysicsList->GetFirst();

	while(pBuildingPtr)
	{
		CEntity* pEntity = pBuildingPtr->item;

		if(pEntity)
		{
			if(CheckEntityName(pEntity))
				Displayf("Found entity in buildings list.");

			AddDynamicObjectFromMapCB(pEntity, &m_DynamicEntitiesList);

			if(!IsEntityInList(pEntity, m_PotentialEntitiesToExport))
			{
				Assert(*(reinterpret_cast<u32*>(pEntity))!=0xdddddddd);
				m_PotentialEntitiesToExport.PushAndGrow(pEntity);
			}
		}
		pBuildingPtr = pBuildingPtr->GetNext();
	}
	pBuildingsWithPhysicsList->Clear();

	*/

	//*************************
	// Go though IPL store.
	//*************************

	for(s32 s=0; s<INSTANCE_STORE.GetSize(); s++)
	{
		fwMapDataDef * pDef = INSTANCE_STORE.GetSlot(strLocalIndex(s));

		if(pDef && pDef->IsLoaded())
		{
			fwEntity ** entities = pDef->GetEntities();

			if (entities)
			{
				for (u32 i=0; i<pDef->GetNumEntities(); i++)
				{
					CEntity * pEntity = (CEntity*)entities[i];
					if(!pEntity)
						continue;
					
						if(CheckEntityName(pEntity))
							Displayf("Found entity in ipl store (%s).", INSTANCE_STORE.GetName(s));

						AddDynamicObjectFromMapCB(pEntity, &m_DynamicEntitiesList);

						if(EntityIntersectsCurrentNavMesh(pEntity) && !IsEntityInList(pEntity, m_PotentialEntitiesToExport))
						{
							Assert(*(reinterpret_cast<u32*>(pEntity))!=0xdddddddd);
							m_PotentialEntitiesToExport.PushAndGrow(pEntity);
						}
					}
				}
			}
		}

	if (m_bExportInteriors)
	{
		//**********************************************
		// Gather interiors and add all entities within
		//***********************************************

		AddInteriorsToExportList(m_PotentialEntitiesToExport /*, portalBoundaries*/);

		int iSize = CInteriorInst::GetPool()->GetSize();
		for(int i=0; i<iSize; i++)
		{
			CInteriorInst * pIntInst = CInteriorInst::GetPool()->GetSlot(i);
			if(!pIntInst)
				continue;

			if(CheckEntityName(pIntInst))
				Displayf("Found entity in interiors pool.");

			if(!IsEntityInList(pIntInst, m_PotentialEntitiesToExport))
			{
				Assert(*(reinterpret_cast<u32*>(pIntInst))!=0xdddddddd);
				m_PotentialEntitiesToExport.PushAndGrow(pIntInst);
			}

			AddInteriorsToExportListCB(pIntInst, (void*)&m_PotentialEntitiesToExport);
		}
	}

	//************************************************************************************

	u32 e;
	for(e=0; e<m_PotentialEntitiesToExport.size(); e++)
	{
		CEntity * pEntity = m_PotentialEntitiesToExport[e];

		if(!pEntity)
			continue;

		if (pEntity->GetOwnedBy() == ENTITY_OWNEDBY_STATICBOUNDS)
		{
			const s32 boundsStoreIndex = pEntity->GetIplIndex();
			Assert(g_StaticBoundsStore.IsValidSlot(strLocalIndex(boundsStoreIndex)));

			int iStrFlags = g_StaticBoundsStore.GetStreamingFlags(strLocalIndex(boundsStoreIndex));
			
			bool bDoNotDelete = ((iStrFlags&STRFLAG_DONTDELETE)!=0);
			if(!bDoNotDelete)
			{
#if 0
				phInst * physicsInst = pEntity->GetCurrentPhysicsInst();
				if(physicsInst)
				{
					Vec3V vLocalMin = physicsInst->GetArchetype()->GetBound()->GetBoundingBoxMin();
					Vec3V vLocalMax = physicsInst->GetArchetype()->GetBound()->GetBoundingBoxMax();
					Vec3V vWorldMin = Transform(physicsInst->GetMatrix(), vLocalMin);
					Vec3V vWorldMax = Transform(physicsInst->GetMatrix(), vLocalMax);

					bool bIntersectsNavMesh = ExtentsIntersectCurrentNavMesh( vWorldMin, vWorldMax );
					Assert(bIntersectsNavMesh);
				}
#endif
				m_PotentialEntitiesToExport.Delete(e);
				e--;
			}
		}
	}

	for(e=0; e<m_PotentialEntitiesToExport.size(); e++)
	{
		CEntity * pEntity = m_PotentialEntitiesToExport[e];

		if(!pEntity)
			continue;

		u32 * ptr = reinterpret_cast<u32*>(pEntity);
		if(*ptr == 0xdddddddd)
		{
			Errorf("FOUND DELETED ENTITY PTR");
			FastAssert(false);
		}

		if(CheckEntityName(pEntity))
		{
			Displayf("Entity was present in m_PotentialEntitiesToExport list (phInst: 0x%p)", pEntity->GetCurrentPhysicsInst() );
		}

		fragInst * pFrag = pEntity->GetFragInst();
		const fragType * pFragType = pFrag ? pFrag->GetType() : NULL;

		s32 iRetVal = MaybeExportCollisionForEntity(pEntity, pFragType, vNavMeshMinsExtra, vNavMeshMaxsExtra, triangleVertices, triangleMaterials, colPolyFlags, archetypeFlags, triDataArray);

		if(CheckEntityName(pEntity))
		{
			Displayf("MaybeExportCollisionForEntity returned: %i", iRetVal);
		}
	}

#if HEIGHTMAP_GENERATOR_TOOL
	ExportMapData(slotList, triangleVertices, triangleMaterials, colPolyFlags, archetypeFlags, triDataArray);
#endif

	//*************************************************************************************************************************

	Vector3 vActualMinOfGeometry(FLT_MAX, FLT_MAX, FLT_MAX);
	Vector3 vActualMaxOfGeometry(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	size_t iNumVerts = triangleVertices.size();

	u32 v;
	for(v=0; v<iNumVerts; v++)
	{
		Vector3 * vVert = &triangleVertices[v];
		if(vVert->x < vActualMinOfGeometry.x) vActualMinOfGeometry.x = vVert->x;
		if(vVert->y < vActualMinOfGeometry.y) vActualMinOfGeometry.y = vVert->y;
		if(vVert->z < vActualMinOfGeometry.z) vActualMinOfGeometry.z = vVert->z;
		if(vVert->x > vActualMaxOfGeometry.x) vActualMaxOfGeometry.x = vVert->x;
		if(vVert->y > vActualMaxOfGeometry.y) vActualMaxOfGeometry.y = vVert->y;
		if(vVert->z > vActualMaxOfGeometry.z) vActualMaxOfGeometry.z = vVert->z;
	}

#if !HEIGHTMAP_GENERATOR_TOOL
	if(iNumVerts)
	{
		Displayf("Actual Mins Of Geometry : (%.2f, %.2f, %.2f)\n", vActualMinOfGeometry.x, vActualMinOfGeometry.y, vActualMinOfGeometry.z);
		Displayf("Actual Maxs Of Geometry : (%.2f, %.2f, %.2f)\n", vActualMaxOfGeometry.x, vActualMaxOfGeometry.y, vActualMaxOfGeometry.z);
	}
#endif

	//*****************************************************************
	// Only export tri/extras files if this navmesh section intersects
	// the world extents + a size for open sea around the world

	spdAABB worldBoxWithSeaBorder = g_WorldLimits_MapDataExtentsAABB;
	ScalarV fSeaBorderSize;
	fSeaBorderSize.Setf(g_fSeaBorderSize);
	worldBoxWithSeaBorder.GrowUniform(fSeaBorderSize);

	Vector3 bbMin = VEC3V_TO_VECTOR3( worldBoxWithSeaBorder.GetMin() );
	Vector3 bbMax = VEC3V_TO_VECTOR3( worldBoxWithSeaBorder.GetMax() );

	const fwNavGenConfig & cfg = fwNavGenConfigManager::GetInstance().GetConfig();

	bbMin.z = cfg.m_ExporterMinZCutoff;
	bbMax.z = cfg.m_ExporterMaxZCutoff;

	// May as well at least have an assert somewhere, now when these values are
	// specified in a data file. /FF
	Assertf(bbMin.z < bbMax.z, "Invalid Z cutoff values %.1f/%.1f.", bbMin.z, bbMax.z);

	worldBoxWithSeaBorder.Set(VECTOR3_TO_VEC3V(bbMin), VECTOR3_TO_VEC3V(bbMax));

	if(worldBoxWithSeaBorder.IntersectsAABB(spdAABB(VECTOR3_TO_VEC3V(m_vNavMeshMins), VECTOR3_TO_VEC3V(m_vNavMeshMaxs))))
	{

		//*************************************************************************
		//	Flush any remaining collision triangles to disk

		if(triangleVertices.size() || m_iHasWaterInCurrentNavMesh)
		{
			FlushTriListToTriFile(triangleVertices, triangleMaterials, colPolyFlags, archetypeFlags, triDataArray);
		}

		//********************************************************
		// Gather a list of ladders from this section of the map

		atArray<CEntity*> laddersFromMap;

		// Find nearby ladders via a scan

		Vector3 vExtra(50.0f,50.0f,50.0f);
		fwIsBoxIntersectingApprox cullBox(spdAABB(VECTOR3_TO_VEC3V(m_vNavMeshMins-vExtra), VECTOR3_TO_VEC3V(m_vNavMeshMaxs+vExtra)));
		const Vec3V cullSphereCenterV = VECTOR3_TO_VEC3V((m_vNavMeshMins+m_vNavMeshMaxs)*0.5f);
		const ScalarV cullSphereRadiusV(m_fWidthOfNavMesh*2.0f);
		fwIsSphereIntersecting cullSphere(spdSphere(cullSphereCenterV, cullSphereRadiusV));

		CGameWorld::ForAllEntitiesIntersecting(
			&cullBox,
			AddLadderFromMapCB,
			&laddersFromMap,
			ENTITY_TYPE_MASK_BUILDING | ENTITY_TYPE_MASK_ANIMATED_BUILDING,
			SEARCH_LOCATION_EXTERIORS | SEARCH_LOCATION_INTERIORS,
			SEARCH_LODTYPE_ALL,
			SEARCH_OPTION_FORCE_PPU_CODEPATH);

		// Also use the ladders list in the task CTaskGoToAndClimbLadder

		for(s32 i=0; i<CTaskGoToAndClimbLadder::ms_Ladders.GetCount(); i++)
		{
			CEntity * pLadderEntity = CTaskGoToAndClimbLadder::ms_Ladders[i];
			if(!IsEntityInList(pLadderEntity, laddersFromMap))
			{
				laddersFromMap.PushAndGrow(pLadderEntity);
			}
		}

		//*************************************************************************************************
		// Now go through this list and export the ladders
		// I had to break this code up because we can no longer do nested ForAllEntitiesIntersecting()..

		for(u32 l=0; l<laddersFromMap.size(); l++)
		{
			CEntity * pLadderEntity = laddersFromMap[l];
			ScanForLaddersToExport(pLadderEntity, &m_LadderInfos);
		}

		//*****************************************************************************
		// Output the positions & radii of all dynamic objects in this area of map
		// This is used to reset the "bNetworkSpawnCandidate" flag on polygons which
		// are intersecting dynamic objects

		CObject::Pool * pObjectPool = CObject::GetPool();
		for(int o=0; o<pObjectPool->GetSize(); o++)
		{
			CObject * pObj = pObjectPool->GetSlot(o);
			if(!pObj)
				continue;

			if(EntityIntersectsCurrentNavMesh(pObj))
			{
				AddDynamicObjectFromMapCB(pObj, &m_DynamicEntitiesList);
			}
		}

		//**************************
		// Find car nodes in area
		FindCarNodesInArea(ThePaths, m_vNavMeshMins, m_vNavMeshMaxs, m_CarNodesList);
	}

	CloseTriFile(true);

	triangleVertices.clear();
	triangleMaterials.clear();

#if !HEIGHTMAP_GENERATOR_TOOL
	Displayf("Num triangles exported : %i\n", m_iNumTrianglesThisTriFile);

// Maybe this would be useful? /FF
#if 0
	u8 bHasVariableWaterLevel = (m_pWaterLevels != NULL);
	s32 iNumSamples = 0;
	if(bHasVariableWaterLevel)
	{
		u32 iBlockSize = CPathServerExtents::m_iNumSectorsPerNavMesh;
		float fSampleSpacing = (CPathServerExtents::GetWorldWidthOfSector() * ((float)iBlockSize)) / ms_fWaterSamplingStepSize;
		iNumSamples = (s32)(fSampleSpacing * fSampleSpacing);
	}
	Displayf("Num water samples exported : %i", iNumSamples);
#endif

	if(m_pExportCollisionLogFile)
	{
		fprintf(m_pExportCollisionLogFile, "Num triangles exported : %i\n", m_iNumTrianglesThisTriFile);
	}
#endif

#endif	//__NAVMESH_EXPORT_DISABLE_COLLISION_EXPORT


	//----------------------------

	reqIplMinsLast = reqIplMins;
	reqIplMaxsLast = reqIplMaxs;

	bAddedIplRefs = false;	// reset static variable for next map section

	//----------------------------

	iWaitFrames = iNumWaitFrames;
	m_fTimeUntilWeExportThisNavMesh = TIME_TO_ALLOW_STREAMING_TO_CATCH_UP;

	m_ExtrasFileName = extrasFileName;
	if(!NextCellOrSubSection())
		{
#if HEIGHTMAP_GENERATOR_TOOL
		pProgress->End();
		delete pProgress;

		if (!sysBootManager::IsDebuggerPresent())
		{
			// Hack to work around various shutdown problems ..
			ExitProcess(0);
		}
#endif
		return false;
	}
#if HEIGHTMAP_GENERATOR_TOOL
	else
	{
		while (true)
		{
			const int iNumTilesX = (m_iEndSectorX - m_iStartSectorX)/CPathServerExtents::m_iNumSectorsPerNavMesh;
			const int iNumTilesY = (m_iEndSectorY - m_iStartSectorY)/CPathServerExtents::m_iNumSectorsPerNavMesh;
			const int iCurrentTileX = (m_iCurrentSectorX - m_iStartSectorX)/CPathServerExtents::m_iNumSectorsPerNavMesh;
			const int iCurrentTileY = (m_iCurrentSectorY - m_iStartSectorY)/CPathServerExtents::m_iNumSectorsPerNavMesh;
			const int iCurrentTile = iCurrentTileX + iCurrentTileY*iNumTilesX;

			pProgress->Update(iCurrentTile, iNumTilesX*iNumTilesY, true);

			int iFastForwardToTile = -1; // <-- can set this to a particular tile to "resume" exporting if it crashes (progress display won't be accurate though)
			int iStartAt = 0;

			if (PARAM_StartAt.Get(iStartAt))
			{
				iFastForwardToTile = iStartAt;
			}

			if (iFastForwardToTile != -1 && iCurrentTile < iFastForwardToTile - 2)
			{
				NextCellOrSubSection();
			}
			else
			{
				break;
			}
		}
	}
#endif

	fwTimer::Resume();

	//***********************************************************************************
	// WORKAROUND for interiors leaking fwArchetypes under the new streaming archetype code.
	// If we had interiors streamed in this navmesh, then perform a streaming flush.

	if(bFlush)
	{
		CStreaming::GetCleanup().RequestFlush(false);
	}

	g_bWaitingForInteriorArchetypes = false;

	return true;
}


void CNavMeshDataExporterTool::LaunchNavMeshCompiler()
{
	printf("Launching NavMeshMaker....\n");
	char fullBatchFileName[512];

	if(PARAM_scheduledCompile.Get())
	{
		formatf(fullBatchFileName, "%s\\export_complete.txt", m_OutputPath);
		fiStream * pFile = fiStream::Create(fullBatchFileName);
		Assert(pFile);

		pFile->Close();
		return;
	}
	else
	{
		const char * pExportPath = m_OutputPath;
		const char * pCmdFileName = NULL;
		const char * pBatchFileName = NULL;

		if(!PARAM_navgenBatchFile.Get(pBatchFileName))
		{
			Assertf(0, "You need to specify a batch file with \'-navGenBatchFile\'");
			return;
		}

		if(!PARAM_navgenCmdFile.Get(pCmdFileName))
		{
			Assertf(0, "You need to specify a cmd file with \'-navGenCmdFile\'");
			return;
		}

		ReplaceChar(const_cast<char*>(pBatchFileName), '\\', '/');
		ReplaceChar(const_cast<char*>(pExportPath), '\\', '/');
		ReplaceChar(const_cast<char*>(pCmdFileName), '\\', '/');

		formatf(
			fullBatchFileName,
			"%s %s %s",
			pBatchFileName,
			pExportPath,
			pCmdFileName
		);
	}

#if __WIN32PC

	ShellExecute(
		GetDesktopWindow(),
		"open",
		fullBatchFileName,
		NULL,
		"x:/tools_release/navgen/NavMeshMaker",
		SW_SHOW
		);

#else

	sysExec(fullBatchFileName);

#endif

}

//***********************************************************************
//	Not actually an export function.  This just adds the info about this
//	ladder to a list, to be exported later

bool CNavMeshDataExporterTool::ExportLadderInfo(CEntity * pLadder, s32 UNUSED_PARAM(iLadderIndex), vector<fwNavLadderInfo> * laddersList)
{
	Vector3 vTopOfLadder, vBottomOfLadder, vNormal;
	bool bCanGetOffAtTop;
	static const float fSameLadderEps = 1.0f;

	CBaseModelInfo * pModelInfo = pLadder->GetBaseModelInfo();
	if(!pModelInfo)
	{
		return false;
	}

	GET_2D_EFFECTS_NOLIGHTS(pModelInfo);
	if(iNumEffects == 0)
	{
		return false;
	}

	for(s32 iLadderIndex=0; iLadderIndex < iNumEffects; iLadderIndex++)
	{
		C2dEffect* pEffect = (*pa2dEffects)[iLadderIndex];
		if(!pEffect || pEffect->GetType() != ET_LADDER)	
		{
			continue;
		}

		CLadderInfo* pLadderInfo = pEffect->GetLadder();
		if(!pLadderInfo)
		{
			continue;
		}

		// Get positions in local space
		pLadderInfo->vTop.Get(vTopOfLadder);
		pLadderInfo->vBottom.Get(vBottomOfLadder);
		pLadderInfo->vNormal.Get(vNormal);

		// Transform to world space
	const Matrix34 mat = MAT34V_TO_MATRIX34(pLadder->GetMatrix());
		mat.Transform(vTopOfLadder);
	mat.Transform(vBottomOfLadder);
	mat.Transform3x3(vNormal);

		bCanGetOffAtTop = pLadderInfo->bCanGetOffAtTop;

		//-------------------------------------------------------------------------

	if(vTopOfLadder.z < vBottomOfLadder.z)
	{
		float z = vTopOfLadder.z;
		vTopOfLadder.z = vBottomOfLadder.z;
		vBottomOfLadder.z = z;
	}

	if(vTopOfLadder.x < m_vNavMeshMins.x || vTopOfLadder.y < m_vNavMeshMins.y ||
		vTopOfLadder.x > m_vNavMeshMaxs.x || vTopOfLadder.y > m_vNavMeshMaxs.y)
		{
			continue;
		}


		// Let's assume that we can get off at the top of all ladders, mostly because artists
		// forget to set this flag.  But also whats the point otherwise?  (We no longer have
		// stupid telegraph pole climbing like we once did in IV)

//		if(!bCanGetOffAtTop)
//		{
//			continue;
//		}

	// See if we already have this ladder in our list.  This will happen because some ladders are
	// made up of multiple CObject sections, and this function will be called for each sections.
	// The calculated top & bottom will be of all the ladder sections.
		bool bAlreadyExists = false;
	for(u32 l=0; l<laddersList->size(); l++)
	{
		fwNavLadderInfo & ladderInfo = (*laddersList)[l];
		if(ladderInfo.GetBase().IsClose(vBottomOfLadder, fSameLadderEps) &&
			ladderInfo.GetTop().IsClose(vTopOfLadder, fSameLadderEps))
		{
				bAlreadyExists = true;
				break;
		}
	}
		if(bAlreadyExists)
		{
			continue;
		}


	fwNavLadderInfo ladderInfo;
	ladderInfo.SetTop(vTopOfLadder);
	ladderInfo.SetBase(vBottomOfLadder);
	ladderInfo.SetNormal(vNormal);

	laddersList->push_back(ladderInfo);
	}

	return true;
}

bool CNavMeshDataExporterTool::AddLadderFromMapCB(CEntity * pEntity, void * pData)
{
	atArray<CEntity*> * ladderEntitiesFromMap = (atArray<CEntity*>*) pData;
	ladderEntitiesFromMap->PushAndGrow(pEntity);
	return true;
}

bool CNavMeshDataExporterTool::ScanForLaddersToExport(CEntity * pEntity, vector<fwNavLadderInfo> * laddersList)
{
	if( !pEntity->IsArchetypeSet() || !CModelInfo::IsValidModelInfo(pEntity->GetModelIndex() ))
		return true;

	ExportLadderInfo(pEntity, -1, laddersList);

	return true;
}

bool CNavMeshDataExporterTool::AddDynamicObjectFromMapCB(CEntity * pEntity, void * pData)
{
	Assert(*(reinterpret_cast<u32*>(pEntity))!=0xdddddddd);

	vector<fwEntity*> * entitiesFromMap = (vector<fwEntity*>*) pData;
	if( IsEntityInList(pEntity, *entitiesFromMap) )
	{
		return true;
	}

	switch(pEntity->GetType())
	{
		case ENTITY_TYPE_OBJECT:
		{
			// If the object has a related dummy already in the list, then overwrite it in the list
			CObject * pObject = (CObject*)pEntity;
			if(pObject->GetRelatedDummy())
			{
				s32 iExistingSlot = GetEntityIndexInList(pObject->GetRelatedDummy(), *entitiesFromMap);
				if(iExistingSlot != -1)
				{
					(*entitiesFromMap)[iExistingSlot] = (fwEntity*)pObject;
					return true;
				}
			}
			break;
		}
		case ENTITY_TYPE_DUMMY_OBJECT:
		{
			// Go through list and see if we have a CObject already in there, whose related dummy is this
			// If we find such an object then we can quit without adding the dummy.
			for(u32 i=0; i<entitiesFromMap->size(); i++)
			{
				fwEntity * pExistingEntity = (*entitiesFromMap)[i];
				if(pExistingEntity->GetType()==ENTITY_TYPE_OBJECT)
				{
					CObject * pObject = (CObject*)pExistingEntity;
					if(pObject->GetRelatedDummy() && pObject->GetRelatedDummy()==pEntity)
					{
						return true;
					}
				}
			}
			break;
		}
		default:
			return true;
	}
	
	Assert(*(reinterpret_cast<u32*>(pEntity))!=0xdddddddd);
	entitiesFromMap->push_back((fwEntity*)pEntity);

	return true;
}


//////////////////////////////////////////////////////////////////////////
// FUNCTION:	GetAllBuildingsWithPhysics
// PURPOSE:		adds all loaded buildings with physics insts to results list
//////////////////////////////////////////////////////////////////////////

/*
void CNavMeshDataExporterTool::GetAllBuildingsWithPhysics(fwMapDataStore & UNUSED_PARAM(store), CLinkList<CEntity*>* pResultsList)
{
	atArray<RegdEnt> & list = CPhysics::ms_buildingsWithPhysics;
	for(int i = 0; i < list.GetCount(); i++)
	{
		CEntity * pEntity = list[i];
		if(pEntity && pEntity->GetCurrentPhysicsInst() && pEntity->GetIsTypeBuilding())
		{
			pResultsList->Insert(pEntity);
		}
	}
}

bool CNavMeshDataExporterTool::AppendBuildingCB(fwEntity* entity, void* pData)
{
#if !HEIGHTMAP_GENERATOR_TOOL
	CheckEntityName((CEntity*)entity);
#endif
	return CPhysics::AppendBuildingCB(entity, pData);
}
*/

bool InstanceBuildingPhysicsCB(fwEntity* entity, void* UNUSED_PARAM(pData))
{
	Assert(entity->GetType()==ENTITY_TYPE_BUILDING || entity->GetType()==ENTITY_TYPE_ANIMATED_BUILDING);

	CEntity * pEntity = (CEntity*)entity;
	pEntity->CreateDrawable();

	return true;
}

void CNavMeshDataExporterTool::ScanForBuildings()
{
	// This is probably not technically necessary when we have an active exporter,
	// but it's being called just in case, so that the new box-based code can't
	// cause any objects to go missing that were present before.
//	CPhysics::ScanForBuildingsNearPlayer(aRequiredBuildings);

	CNavMeshDataExporterInterface* ex = CNavMeshDataExporter::GetActiveExporter();
	if(!ex)
	{
		// The export may have ended or something, no worries.
		return;
	}

	// Since CNavMeshDataExporterTool contains the CNavMeshDataExporterInterfaceTool,
	// that's certainly what we would expect here.
	Assert(dynamic_cast<CNavMeshDataExporterInterfaceTool*>(ex));

	// Need to cast it to find the CNavMeshDataExporterTool instance to get the bounding box of the current cell.
	CNavMeshDataExporterInterfaceTool* exCast = static_cast<CNavMeshDataExporterInterfaceTool*>(ex);
	Vector3 meshMins = exCast->m_Tool->m_vNavMeshMins;
	Vector3 meshMaxs = exCast->m_Tool->m_vNavMeshMaxs;

	meshMins -= Vector3(fExtra, fExtra, 0.0f);
	meshMaxs += Vector3(fExtra, fExtra, 0.0f);

	// Create a bounding box and do a search, using the same callback as in
	// the usual CPhysics::ScanForBuildingsNearPlayer().
	spdAABB bbox(VECTOR3_TO_VEC3V(meshMins), VECTOR3_TO_VEC3V(meshMaxs));
	fwIsBoxIntersectingBB searchVol(bbox);
	CGameWorld::ForAllEntitiesIntersecting
	(
		&searchVol,
		InstanceBuildingPhysicsCB,
		NULL,
		ENTITY_TYPE_MASK_BUILDING | ENTITY_TYPE_MASK_ANIMATED_BUILDING, //ENTITY_TYPE_MASK_MLO,
		SEARCH_LOCATION_EXTERIORS | SEARCH_LOCATION_INTERIORS,
		SEARCH_LODTYPE_ALL,
		SEARCH_OPTION_FORCE_PPU_CODEPATH
	);

}

void CNavMeshDataExporterTool::FindCarNodesInArea(CPathFind &paths, const Vector3 & vMin, const Vector3 & vMax, atArray<Vector3> & carNodesList)
{
	carNodesList.clear();

	for(int Region=0; Region<PATHFINDREGIONS; Region++)
	{
		// Only if this Region is loaded
		if(!paths.apRegions[Region] || !paths.apRegions[Region]->aNodes)
		{
			continue;
		}

		int iStart = 0;
		int iEnd = paths.apRegions[Region]->NumNodesCarNodes;

		for(int n=iStart; n<iEnd; n++)
		{
			CPathNode * pNode = &((paths.apRegions[Region]->aNodes)[n]);
			if(pNode && pNode->m_1.m_specialFunction!=SPECIAL_USE_NONE)
			{
				continue;
			}

			Vector3 vPos;
			pNode->GetCoors(vPos);

			if(vPos.x >= vMin.x && vPos.x <= vMax.x && vPos.y >= vMin.y && vPos.y <= vMax.y)
			{
				carNodesList.PushAndGrow(vPos);
			}
		}
	}
}


void CNavMeshDataExporterTool::FindWaterLevelForCurrentNavMesh()
{
	float fAverageHeight = 0.0f;
	int iNumHits = 0;

	float fX,fY;
	static const float fZ = 100.0f;

	float fSampleSpacing = (CPathServerExtents::GetWorldWidthOfSector() * ((float)CPathServerExtents::m_iNumSectorsPerNavMesh)) / ms_fWaterSamplingStepSize;
	s32 iNumSamples = (s32)(fSampleSpacing * fSampleSpacing);

	bool bVariableWaterHeight = false;
	float fLastValidSample = -32000.0f;

	if(m_pWaterLevels)
	{
		delete[] m_pWaterLevels;
		m_pWaterLevels = NULL;
	}

	m_pWaterLevels = rage_new s16[iNumSamples];

	int iSample=0;

	for(fY=m_vNavMeshMins.y; fY<m_vNavMeshMaxs.y; fY+=ms_fWaterSamplingStepSize)
	{
		for(fX=m_vNavMeshMins.x; fX<m_vNavMeshMaxs.x; fX+=ms_fWaterSamplingStepSize)
		{
			float fWaterHeight = 0.0f;
			bool bHitWater = Water::GetWaterLevelNoWaves(Vector3(fX, fY, fZ), &fWaterHeight, 2000.0f, 2000.0f, NULL);

			if(bHitWater)
			{
				iNumHits++;
				fAverageHeight+=fWaterHeight;

				m_pWaterLevels[iSample] = (s16)fWaterHeight;

				if(fLastValidSample != -32000.0f && fLastValidSample != fWaterHeight)
				{
					bVariableWaterHeight = true;
				}

				fLastValidSample = fWaterHeight;
			}
			else
			{
				m_pWaterLevels[iSample] = -32000;
			}

			iSample++;
		}
	}

	// If we hit less water than the samples we took, then this counts as variable water heights
	if(iNumHits != 0 && iNumHits != iNumSamples)
	{
		bVariableWaterHeight = true;
	}

	if(iNumHits>0)
	{
		fAverageHeight /= ((float)iNumHits);
		m_iHasWaterInCurrentNavMesh = true;
		m_fAverageWaterLevelInCurrentNavMesh = fAverageHeight;
	}
	else
	{
		m_iHasWaterInCurrentNavMesh = false;
		m_fAverageWaterLevelInCurrentNavMesh = 0.0f;
	}

	//**************************************************************************
	//	If there was a variable water height (or if there were some samples
	//	with water & some without) then we'll need to save out a file detailing
	//	the water levels for this navmesh region.

	if(!bVariableWaterHeight)
	{
		if(m_pWaterLevels)
		{
			delete[] m_pWaterLevels;
			m_pWaterLevels = NULL;
		}
	}
}


s32 CNavMeshDataExporterTool::MaybeExportCollisionForEntity(CEntity * pEntity, const fragType * pFragType, Vector3 & vNavMeshMinsExtra, Vector3 & vNavMeshMaxsExtra, vector<Vector3> & triangleVertices, vector<phMaterialMgr::Id> & triangleMaterials, vector<u16> & colPolyFlags, vector<u32> & archetypeFlags, vector<fwNavTriData> & triDataArray)
{
#if __DEV
	CheckEntityName(pEntity);
#endif

	// Never export dummy objects
	if(pEntity->GetIsTypeDummyObject())
		return -1;

	CBaseModelInfo * pModelInfo = pEntity->GetBaseModelInfo();

	u16 iColPolyFlag=0;

	// Don't export trees
	if(pModelInfo && pModelInfo->GetIsTree())
		iColPolyFlag |= EXPORTPOLYFLAG_DONT_CREATE_NAVMESH_ON_THIS;

	if(pModelInfo && pModelInfo->GetDoesNotProvideAICover())
		iColPolyFlag |= EXPORTPOLYFLAG_DOESNTPROVIDECOVER;

	// Set a flag for interiors
	if(pEntity->GetIsTypeMLO())
	{
		if (m_pLevelProcessTool->GetExportExteriorPortals())
		{
			CMloModelInfo * pMloModelInfo = (CMloModelInfo*)pModelInfo;

			for (int i = 0; i < pMloModelInfo->GetPortals().GetCount(); i++)
			{
				const CMloPortalDef & portalData = pMloModelInfo->GetPortals()[i];

				if ((portalData.m_roomFrom == 0 || portalData.m_roomTo == 0) &&
					(portalData.m_flags & INTINFO_PORTAL_LINK) == 0)
				{
					const u16 iPortalFlag = iColPolyFlag | EXPORTPOLYFLAG_EXTERIOR_PORTAL | EXPORTPOLYFLAG_DONT_CREATE_NAVMESH_ON_THIS;
					Vec3V corners[4];

					for (int j = 0; j < 4; j++)
					{
						corners[j] = Transform(pEntity->GetMatrix(), RCC_VEC3V(portalData.m_corners[j]));
					}

					AddPolyToList(corners, 4, false, triangleVertices, triangleMaterials, phMaterialMgr::DEFAULT_MATERIAL_ID, colPolyFlags, iPortalFlag, archetypeFlags, 0, triDataArray, fwNavTriData());
				}
			}
		}

		iColPolyFlag |= EXPORTPOLYFLAG_INTERIOR;
	}
	else if (pEntity->GetInteriorLocation().IsValid())
	{
		iColPolyFlag |= EXPORTPOLYFLAG_INTERIOR;
	}
	else if (pEntity->GetOwnedBy() == ENTITY_OWNEDBY_STATICBOUNDS)
	{
		const phInst* pPhysInst = pEntity->GetCurrentPhysicsInst();

		if (pPhysInst)
		{
			const s32 boundsStoreIndex = pEntity->GetIplIndex();
			Assert(g_StaticBoundsStore.IsValidSlot(strLocalIndex(boundsStoreIndex)));

			s16 proxyId = -1;
			s16 depSlot = -1;
			g_StaticBoundsStore.GetDummyBoundData(strLocalIndex(boundsStoreIndex), proxyId, depSlot);

			if (proxyId != -1)
			{
				iColPolyFlag |= EXPORTPOLYFLAG_INTERIOR;
			}
		}
	}

	bool bFixedForNav = false;
	bool bFixedFlagSet = false;
	bool bClimbableObject = false;

	// Only ever export objects if they have the "fixed for navigation" flag set.  This means that we can
	// cut them into the navmesh, since they will never ever move or be traversable - even if fragmented.
	if(pEntity->GetIsTypeObject())
	{
		CObject * pObj = (CObject*)pEntity;
		CBaseModelInfo * pModelInfo = pObj->GetBaseModelInfo();

		bFixedForNav = (pModelInfo->GetIsFixedForNavigation() != 0);
		bFixedFlagSet = pObj->IsBaseFlagSet(fwEntity::IS_FIXED) || pModelInfo->GetIsFixed();
		bClimbableObject = pModelInfo->GetIsClimbableByAI();

		bool bScriptObject = (pObj->GetOwnedBy() == ENTITY_OWNEDBY_SCRIPT);


		if(bScriptObject)
			return -2;

		// Don't export any geometry which isn't avoided by peds
		if(pModelInfo->GetNotAvoidedByPeds())
			return -3;

		// If this object is not fixed in any way, then don't export its geometry
		if(!bFixedFlagSet && !bFixedForNav && !bClimbableObject)
			return -4;

		if(pModelInfo->GetUsesDoorPhysics())
			return -5;

		if(bFixedFlagSet || bFixedForNav)
		{
			iColPolyFlag |= EXPORTPOLYFLAG_FIXEDOBJECT;
		}
		else if(bClimbableObject) // never set both FIXED and CLIMBABLE flags
		{
			iColPolyFlag |= EXPORTPOLYFLAG_CLIMBABLEOBJECT;
		}
	}

	phInst * pInstance = pEntity->GetCurrentPhysicsInst();

	if(pInstance)
	{
		if(CPhysics::GetLevel()->IsInLevel(pInstance->GetLevelIndex()))
			CPhysics::GetLevel()->UpdateObjectArchetype(pInstance->GetLevelIndex());

		u32 iArchetypeFlags = CPhysics::GetLevel()->GetInstanceTypeFlags(pInstance->GetLevelIndex());

		// Objects are neither weapon/mover; we mark them as both here to ensure they get processed
		if((iColPolyFlag & EXPORTPOLYFLAG_FIXEDOBJECT)!=0)
		{
			iArchetypeFlags |= (ArchetypeFlags::GTA_MAP_TYPE_MOVER | ArchetypeFlags::GTA_MAP_TYPE_WEAPON);
		}

		/*
		// Don't create navmesh on anything marked as foliage
		if( (iArchetypeFlags & ArchetypeFlags::GTA_FOLIAGE_TYPE)!=0)	
		{
			iColPolyFlag |= EXPORTPOLYFLAG_DONT_CREATE_NAVMESH_ON_THIS;
		}
		*/

		const u32 exportFlags = m_pLevelProcessTool->GetCollisionArchetypeFlags();
		if((iArchetypeFlags & exportFlags)==0)
			return -6;

		phArchetype * pArchetype = pInstance->GetArchetype();
		phBound * pBound = pArchetype->GetBound();

		if(pBound)
		{
			const Matrix34 & instanceMat = RCC_MATRIX34(pInstance->GetMatrix());

			// Check that the min/max of this bound intersect the min/max of this navmesh area
			Vector3 vBoundMin;
			Vector3 vBoundMax;
			fwNavToolHelperFuncs::TransformObjectAABB(instanceMat,
					RCC_VECTOR3(pBound->GetBoundingBoxMin()),
					RCC_VECTOR3(pBound->GetBoundingBoxMax()),
					vBoundMin, vBoundMax);

			// We pass down a flag which tells that this object is fixed - in this way FIXED objects
			// generally DON'T HAVE NAVMESH CREATED UPON THEM.  However, if the fixed object is over
			// a certain size then we ignore this rule and allow it to generate navmesh - this is
			// because the artists are sometimes fond of placing huge fixed objects on the map, and
			// its crap when peds can't navigate on them.
			static const float fMinSizeForFixed = 4.0f;
			const Vector3 vBoundBoxSize = vBoundMax - vBoundMin;
			// Note: the condition below is suspicious. It's possible that it should be something like
			// !bFixedForNav && (Abs...) instead. The way it is now, bFixedForNav objects will always have
			// nav mesh on them, regardless of size, which may not be intentional. Fredrik and James discussed
			// this issue, and the conclusion was that it may be incorrect the way it is, but that it's best to
			// wait with a change until some situation comes up in practice. /FF
			if(bFixedForNav || (Abs(vBoundBoxSize.x) > fMinSizeForFixed && Abs(vBoundBoxSize.y) > fMinSizeForFixed))
			{
				iColPolyFlag &= ~EXPORTPOLYFLAG_FIXEDOBJECT;
			}

			if(pEntity->GetIsTypeMLO())
			{
				vBoundMin -= vExpandForInteriors;
				vBoundMax += vExpandForInteriors;
			}

#if __CHECK_TRIANGLES_AGAINST_NAVMESH_EXTENTS
			// Test against the bounds of the current navmesh
			if(vBoundMax.x < vNavMeshMinsExtra.x || vBoundMax.y < vNavMeshMinsExtra.y || vBoundMax.z < vNavMeshMinsExtra.z ||
				vBoundMin.x > vNavMeshMaxsExtra.x || vBoundMin.y > vNavMeshMaxsExtra.y || vBoundMin.z > vNavMeshMaxsExtra.z)
			{
				// No intersection possible
				return -7;
			}
#else
			vNavMeshMinsExtra; vNavMeshMaxsExtra;
#endif

			fwNavTriData triData;

#if HEIGHTMAP_GENERATOR_TOOL
			triData.m_iArchetypeFlags = iArchetypeFlags;

			if (pEntity->GetOwnedBy() == ENTITY_OWNEDBY_STATICBOUNDS)
			{
				const fwBoundDef* pDef = g_StaticBoundsStore.GetSlot(strLocalIndex(pEntity->GetIplIndex()));

				triData.m_pBoundName = pDef->m_name.GetCStr();
				triData.m_iBoundHash = (u32)pEntity->GetIplIndex();
				triData.m_iFlagsStruct.m_bIsOwnedByStaticBounds = true;
				triData.m_iFlagsStruct.m_bIsScriptManaged = g_StaticBoundsStore.IsScriptManaged(pEntity->GetIplIndex());

				const strIndex streamingIndex = g_StaticBoundsStore.GetStreamingIndex(strLocalIndex(triData.m_iBoundHash));

				if (AssertVerify(streamingIndex.IsValid()))
				{
					const strStreamingInfo* pInfo = strStreamingEngine::GetInfo().GetStreamingInfo(streamingIndex);
					const strStreamingFile* pFile = strPackfileManager::GetImageFileFromHandle(pInfo->GetHandle());

					if (AssertVerify(pFile))
					{
						triData.m_iRegionHash = atStringHash(pFile->m_packfile->GetDebugName(), 0);
					}
				}
			}
			else if (pModelInfo)
			{
				triData.m_pBoundName = pModelInfo->GetModelName();
				triData.m_iBoundHash = pModelInfo->GetModelNameHash();
			}
			else
			{
				Assertf(0, "### entity not owned by static bounds but has no model info");
			}

			triData.m_iBoundHash = atDataHash((const char*)&instanceMat.a, 3*sizeof(float), triData.m_iBoundHash);
			triData.m_iBoundHash = atDataHash((const char*)&instanceMat.b, 3*sizeof(float), triData.m_iBoundHash);
			triData.m_iBoundHash = atDataHash((const char*)&instanceMat.c, 3*sizeof(float), triData.m_iBoundHash);
			triData.m_iBoundHash = atDataHash((const char*)&instanceMat.d, 3*sizeof(float), triData.m_iBoundHash);

			u32 iCombinedArchetypeFlags = iArchetypeFlags;
			if(pBound && pBound->GetType() == phBound::COMPOSITE)
			{
				const phBoundComposite * pBoundComposite = static_cast<const phBoundComposite*>(pBound);
				for (s32 i=0; i<pBoundComposite->GetNumBounds(); i++)
				{
					// check this bound is still valid (smashable code may have removed it)
					const phBound * pChildBound = pBoundComposite->GetBound(i);
					if (pChildBound)
					{
						if (pBoundComposite->GetTypeAndIncludeFlags())
						{
							iCombinedArchetypeFlags |= pBoundComposite->GetTypeFlags(i);
						}
					}
				}
			}

			ExportEntityBegin(iCombinedArchetypeFlags, Vec3V(-100.0f, -100.0f, -16000.0f), Vec3V(+100.0f, +100.0f, 10.0f));

			if(pModelInfo && pModelInfo->GetIsProp())
				triData.m_iFlagsStruct.m_bIsProp = true;
			if(stristr(triData.m_pBoundName, "proxy"))
				triData.m_iFlagsStruct.m_bIsProxy = true;
			if(iCombinedArchetypeFlags & ArchetypeFlags::GTA_MAP_TYPE_MOVER)
				triData.m_iFlagsStruct.m_bContainsMoverBounds = true;
			if(iCombinedArchetypeFlags & ArchetypeFlags::GTA_MAP_TYPE_WEAPON)
				triData.m_iFlagsStruct.m_bContainsWeaponBounds = true;
			if(iCombinedArchetypeFlags & ArchetypeFlags::GTA_VEHICLE_INCLUDE_TYPES)
				triData.m_iFlagsStruct.m_bContainsVehicleBounds = true;
#endif
			ExportBound(pBound, pFragType, instanceMat, triangleVertices, triangleMaterials, colPolyFlags, iColPolyFlag, archetypeFlags, iArchetypeFlags, triDataArray, triData);

#if HEIGHTMAP_GENERATOR_TOOL
			if(WereAnyTrianglesExported())
			{
				static atMap<u32,bool> s_interestHits;
				const Vec3V vInstancePos = RCC_VEC3V(instanceMat.d);
				char msg[2048] = "";

				if(1) // interest region and zero intersection
				{
					if(WasInterestRegionHit())
					{
						sprintf(msg, "### %s hit interest region", triData.m_pBoundName);
						if(!IsZeroAll(vInstancePos))
							strcat(msg, atVarString(" (%f,%f,%f)", VEC3V_ARGS(vInstancePos)).c_str());
					}
					else if(false && IsZeroAll(vInstancePos)) // we could enable this if needed .. but lots of entities have their pivot at the origin
					{
						sprintf(msg, "### %s is at 0,0,0", triData.m_pBoundName);
						if(!WereAnyTrianglesExported())
							strcat(msg, " (no triangles exported)");
					}

					if(msg[0] != '\0')
					{
						const u32 iMsgHash = atDataHash(msg, istrlen(msg), 0);

						if(s_interestHits.Access(iMsgHash) == NULL)
						{
							s_interestHits[iMsgHash] = true;

							if(m_pExportCollisionLogFile)
							{
								fprintf(m_pExportCollisionLogFile, "%s\n", msg);
								m_pExportCollisionLogFile->Flush();

								if(WasInterestRegionHit())
								{
									printf("%s\n", msg); // also print to tty
								}
							}
							else
							{
								printf("%s\n", msg);
							}
						}
					}
				}

				// movnoveh
				if ((iCombinedArchetypeFlags & ArchetypeFlags::GTA_MAP_TYPE_MOVER) != 0 &&
					(iCombinedArchetypeFlags & ArchetypeFlags::GTA_VEHICLE_INCLUDE_TYPES) == 0)
				{
					sprintf(msg, "### %s has mover but no vehicle collision%s", triData.m_pBoundName, triData.m_iFlagsStruct.m_bIsProp ? " (PROP)" : "");

					const Vec3V vCentre = (m_vExportBoundsMax + m_vExportBoundsMin)*ScalarV(V_HALF);
					const Vec3V vExtent = (m_vExportBoundsMax - m_vExportBoundsMin)*ScalarV(V_HALF);

					strcat(msg, atVarString(" (%f,%f,%f,r=%f)", VEC3V_ARGS(vCentre), Mag(vExtent).Getf()).c_str());

					if(!IsZeroAll(vInstancePos))
						strcat(msg, atVarString(" (pivot=%f,%f,%f)", VEC3V_ARGS(vInstancePos)).c_str());

					const u32 iMsgHash = atDataHash(msg, istrlen(msg), 0);

					if(s_interestHits.Access(iMsgHash) == NULL)
					{
						s_interestHits[iMsgHash] = true;

						if(m_pExportCollisionLogFile)
						{
							fprintf(m_pExportCollisionLogFile, "%s\n", msg);
							m_pExportCollisionLogFile->Flush();
						}
						else
						{
							printf("%s\n", msg);
						}
					}
				}
			}
#endif
			return 1;
		}
	}

	return -8;
}

//-----------------------------------------------------------------------------
