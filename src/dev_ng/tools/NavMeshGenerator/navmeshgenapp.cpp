#include "navmeshgenapp.h"
#include "exportcollision.h"
#include "materialinterface.h"

#include "ai/ambient/AmbientModelSetManager.h"
#include "animation/AnimManager.h"
#include "audio/audiogeometry.h"
#include "audio/northaudioengine.h"
#include "camera/system/CameraManager.h"
#include "camera/viewports/ViewportManager.h"
#include "control/gamelogic.h"
#include "Core/game.h"								// CGame::Shutdown()
#include "debug/blockview.h"						// TODO: See if/why this is needed.
#include "diag/restservice.h"
#include "fwscene/stores/blendshapestore.h"
#include "fwscene/stores/expressionsdictionarystore.h"
#include "fwscene/stores/framefilterdictionarystore.h"
#include "fwscene/stores/mapdatastore.h"
#include "fwscene/stores/maptypesstore.h"
#include "fwscene/stores/networkdefstore.h"
#include "fwscene/stores/posematcherstore.h"
#include "fwscene/stores/staticboundsstore.h"
#include "fwsys/game.h"
#include "fwsys/metadatastore.h"
#include "game/clock.h"
#include "game/ModelIndices.h"
#include "game/Performance.h"
#include "modelinfo/PedModelInfo.h"
#include "modelinfo/VehicleModelInfoVariation.h"
#include "objects/object.h"							// TODO: Check if needed
#include "objects/objectpopulationNY.h"
#include "objects/ProcObjects.h"
#include "network/Live/livemanager.h"
#include "pathserver/PathServer.h"
#if RDR_VERSION
#include "Peds/Horse/horseTune.h"
#endif
#include "peds/ped.h"
#include "peds/pedpopulation.h"
#include "peds/popcycle.h"
#include "peds/popzones.h"
#include "physics/physics.h"
#include "renderer/lights/LODLights.h"
#include "renderer/MeshBlendManager.h"
#include "renderer/occlusion.h"
#include "renderer/PostScan.h"
#include "renderer/water.h"
#include "renderer/ZoneCull.h"
#include "scene/entities/compEntity.h"
#include "scene/FocusEntity.h"
#include "scene/loader/mapFileMgr.h"
#include "scene/portals/portal.h"
#include "scene/scene.h"
#include "scene/ExtraContent.h"
#include "scene/ExtraMetadataMgr.h"
#include "scene/world/GameWorld.h"
#include "scene/worldpoints.h"
#include "scene/loader/mapTypes.h"
#include "scene/loader/mapFileMgr.h"
#include "script/script_hud.h"						// TODO: See if/why this is needed.
#include "streaming/CacheLoader.h"
#include "streaming/populationstreaming.h"
#include "streaming/streaming.h"
#include "streaming/streamingdebuggraph.h"
#include "streaming/streamingrequestlist.h"
#include "Task/Default/Patrol/PatrolRoutes.h"
#include "Task/Scenario/ScenarioManager.h"
#include "timecycle/TimeCycle.h"
#include "timecycle/TimeCycleConfig.h"
#include "vehicleAi/pathfind.h"
#include "vehicles/Metadata/VehicleMetadataManager.h"
#include "vehicles/train.h"
#include "vehicles/VehicleFactory.h"
#include "vehicles/vehiclepopulation.h"
#include "vfx/ptfx/ptfxmanager.h"
#include "vfx/visualeffects.h"
#include "vfx/misc/LODLightManager.h"
#include "grprofile/timebars.h"
#include "frontend/Scaleform/ScaleFormStore.h"

PARAM(allowCacheFile, "Allow usage of cache file - default is to disable this.");
PARAM(MP_DLC, "Specify this to mount the MP DLC, instead of SP DLC (which gets mounted by default if specified).");

//-----------------------------------------------------------------------------

class CTimeCycleWrapper
{
public:
	static void Init(unsigned initMode)
	{
		g_timeCycle.Init(initMode);
	}
};

//-----------------------------------------------------------------

class CGtaNavGenInterface : public fwGameInterface
{
public:
	virtual void RegisterStreamingModules();
};

void CGtaNavGenInterface::RegisterStreamingModules()
{
	// TODO: Continue to find out exactly which ones of these we need:

	g_PoseMatcherStore.RegisterStreamingModule(); 
	g_ClipDictionaryStore.RegisterStreamingModule();
	g_ExpressionDictionaryStore.RegisterStreamingModule();

	g_ScaleformStore.RegisterStreamingModule(); 

	g_StaticBoundsStore.RegisterStreamingModule();
	ptfxManager::GetAssetStore().RegisterStreamingModule();
	g_NetworkDefStore.RegisterStreamingModule();
	g_FrameFilterDictionaryStore.RegisterStreamingModule();

	CModelInfo::RegisterStreamingModule();
	CPathFind::RegisterStreamingModule();
	CPathServer::RegisterStreamingModule();
}

//-----------------------------------------------------------------------------

// PURPOSE: This interface is used for some game-specific extensions to CPathServer.
static CPathServerGameInterfaceGta s_PathServerGameInterface;

void CNavMeshScene::Init(unsigned /*initMode*/)
{
	// The code here was originally adapted from CScene::Init(). /FF

	fwScene::InitClass(rage_new CGtaSceneInterface());
	fwScene::Init();

	INSTANCE_STORE.Init(INIT_CORE);
	// Maybe necessary for Water::Init():
	//	CLights::Init(); 

	// JB: I really didn't want to resort to const_cast, but don't want to maintain a separate gameconfig.xml
	C2dEffectConfig& cfg2d = const_cast<C2dEffectConfig&>(CGameConfig::Get().GetConfig2dEffects());
	cfg2d.m_MaxEffects2d = 32768;

	CModelInfo::Init(INIT_CORE); 

	fwSimpleTransform::ShutdownPool();
	fwSimpleTransform::InitPool(50000);

	fwMatrixTransform::ShutdownPool();
	fwMatrixTransform::InitPool(50000);

	fwQuaternionTransform::ShutdownPool();
	fwQuaternionTransform::InitPool(20000);	// initially 7000

	fwQuaternionScaledTransform::ShutdownPool();
	fwQuaternionScaledTransform::InitPool(10000);	// initially 7000

	ThePaths.Init(INIT_CORE);
	CPathServer::Init(s_PathServerGameInterface, CPathServer::EMultiThreaded);
	CVehicle::InitSystem();

	CGameWorld::Init(INIT_CORE); // depends on InitPools()
	CPhysics::Init(INIT_CORE); // depends on InitPools() 
	// Technically, it would be more correct to call this (since we need water data),
	// but currently Init(INIT_CORE) only seems to do graphics stuff, and may have
	// some issues on PC. /FF
	//	Water::Init(INIT_CORE); // dependent on CLights::Init()

	CMapFileMgr::Init(INIT_CORE);
	g_MapTypesStore.Init(INIT_CORE);

	// Prevent streaming archetype references being handed over to the renderer, and thus our
	// app running out of streaming archetype slots (since we never render anything and therefore
	// the references are never released).
	dlDrawListMgr::SuppressAddTypeFileReferencesBegin();

	// Load the startup files - the RPFs we need right away.
	DATAFILEMGR.Load("commoncrc:/data/startup");
}


void CNavMeshScene::Update()
{
//	CPhysics::ScanForBuildings_StartAsyncSearch();
//	CPhysics::ScanForBuildings_EndAsyncSearch();

	CVehicleModelInfo::UpdateHDRequests();

	CPortal::Update();

	CCompEntity::Update();

	// Disabled, this was using the camera position. Instead,
	// we make sure in CNavMeshDataExporterTool::ProcessCollisionExport()
	// that the CCompEntity objects get streamed in and updated.
	//	CCompEntity::Update();
	
	CPopCycle::Update();

	CPhysics::UpdateRequests();		// Needed for streaming in buildings (interiors?).

	ThePaths.Update();

	// This doesn't do much except failing an assert if the player doesn't exist
	// (which is true in our case).
	//	CPathServer::Process();
	// though we may still want this to be set:
	CPathServer::m_bGameRunning = true;

	CScene::TidyReferences();

	// The deferred calls below copied from CGameWorld
	// Evidently these were implemented after this exporter framework was created,
	// and therefore not incorporated (gameworld is not updated by the exporter).

	//we need to update these otherwise they aren't not cleared out correctly and the pool fills up
#if LEVELNEW_ENABLE_DEFERRED_COMPOSITE_BVH_UPDATE
	// update/rebuild composite BVHs to reflect game side changes
	PHLEVEL->ProcessDeferredCompositeBvhUpdates();

	// until the end of the function, it is safe to update composite BVHs instantly
	PHLEVEL->SetEnableDeferredCompositeBvhUpdate(false);
#endif

	// Update physics octtree
	CPhysics::CommitDeferredOctreeUpdates();
}

//-----------------------------------------------------------------

bool CLevelProcessToolGameInterfaceGta::InitGame()
{
	if(!CSystem::Init("NavMeshGenerator"))
	{
		return false;
	}

	// The following part was adapted from CGame::PreLoadingScreensInit():

	fwGame::InitClass(rage_new CGtaNavGenInterface);
	INIT_DATAFILEMGR;
	fwGame::Init();

	CStreaming::Init(INIT_CORE);
	CMapFileMgr::Init(INIT_CORE);
	// Load the startup files - the RPFs we need right away.
	DATAFILEMGR.Load("platformcrc:/data/startup");
	// Make their contents available to the streaming system.
	CFileLoader::AddAllRpfFiles(true);

	// Start up our REST services (and web server)
#if __BANK
	REST.Init();
#endif

	CFileLoader::InitClass();

	if(ASSET.Exists("commoncrc:/data/contentpatch.xml", NULL))
	{
		DATAFILEMGR.Load("commoncrc:/data/contentpatch");
	}

	if(ASSET.Exists("update:/content.xml", NULL))
	{
		DATAFILEMGR.Load("update:/content");
	}

	DATAFILEMGR.Load("platformcrc:/data/default");

	AssertVerify(CLiveManager::Init(INIT_CORE));

	CStreaming::LoadStreamingMetaFile();
	CStreaming::RegisterStreamingFiles();
	DATAFILEMGR.SnapshotFiles();

#if __BANK
	INIT_DEBUGSTREAMGRAPH;
#endif

	CMessages::Init();

	//INIT_EXTRACONTENT;

	CExtraContentWrapper::Init(INIT_CORE);
	CExtraMetadataMgr::ClassInit(INIT_CORE);

	InitForNavMeshExport();

	//---

	InitLevelForNavMeshExport(CGameLogic::GetRequestedLevelIndex());
	//CGame::InitSession(CGameLogic::GetRequestedLevelIndex(), false);

	CFileMgr::SetupDevicesAfterInit();

	return true;
}


void CLevelProcessToolGameInterfaceGta::ShutdownGame()
{
	// TODO: Fix this, if it matters - most likely doesn't shut down cleanly now,
	// as this doesn't match what we do during initialization.

	CGame::Shutdown();
	CSystem::Shutdown();
}


void CLevelProcessToolGameInterfaceGta::Update1()
{
	PF_FRAMEINIT_TIMEBARS(0);

	CSystem::BeginUpdate();

	gVpMan.PushViewport(gVpMan.GetGameViewport());

#if __BANK
	CGameWorld::AllowDelete(true);
#endif

	// Not sure - do we need the gPopStreaming update?
	//	gPopStreaming.Update();

	CVehicleVariationInstance::ProcessStreamRequests();

	CStreaming::Update();
	CNavMeshScene::Update();

	CObjectPopulationNY::Process();
}


void CLevelProcessToolGameInterfaceGta::Update2()
{
	// Not sure /FF
	CScriptHud::bHideLoadingAnimThisFrame = true;

	// This should be AFTER scripts and AFTER objects are moved
	gVpMan.Process();

	// Not sure if needed:
	//	camManager::Update();

	gVpMan.PopViewport();
	CRenderer::ClearRenderLists();
	gPostScan.Reset();

	CSystem::EndUpdate();
}

void CLevelProcessToolGameInterfaceGta::InitForNavMeshExport()
{	
	AutoIdInit();

	RegisterGameSkeletonFunctions();

	if(!PARAM_allowCacheFile.Get())
		strCacheLoader::Disable();

	CGame::CreateFactories();

	// Create the class which decides which attributes to bake into exported navmeshes
	fwExportCollisionBaseTool::SetProjectSpecificDataFilter(rage_new CNavMeshMaterialInterfaceGta());

	// Not sure if we should do this here directly, or let it happen through
	// CVisualEffects::Init() - the latter does a lot of other initialization
	// of graphics systems we probably don't need for navigation data, though.
	g_visualSettings.LoadAll();

#if RDR_VERSION
	// Horse simulation tuning
	hrsSimTuneMgrSingleton::Instantiate();
#endif

	m_NavGenSkeleton.Init(INIT_CORE);


}


void CLevelProcessToolGameInterfaceGta::InitLevelForNavMeshExport(int level)
{
	m_NavGenSkeleton.Init(INIT_BEFORE_MAP_LOADED);

	// These are necessary now for the new configuration system to work, otherwise
	// there appears to be some problem when exporting vehicle data.
#if ENABLE_BLENDSHAPES
	g_BlendShapeStore.FinalizeSize();
#endif

	g_ExpressionDictionaryStore.FinalizeSize();

	// Initialise DLC system
	CExtraContentWrapper::Init(INIT_CORE);
	CExtraMetadataMgr::ClassInit(INIT_CORE);
	EXTRACONTENT.ExecuteTitleUpdateDataPatch(CCS_TITLE_UPDATE_STARTUP, true);
	EXTRACONTENT.ExecuteTitleUpdateDataPatch((u32)CCS_TITLE_UPDATE_TEXT, true); 

	{
		CScene::LoadMap(level);
		CModelIndex::MatchAllModelStrings();
	}


	m_NavGenSkeleton.Init(INIT_AFTER_MAP_LOADED);

	// This part was adapted from InitSession() and InitLevelAfterLoadingFromMemoryCard():

	m_NavGenSkeleton.Init(INIT_SESSION);

	CGameLogic::SetCurrentLevelIndex(level);

	// In the regular game code, the player would have been created
	// at around this time. In this tool, we just set this "fake" position that will
	// be used when the player doesn't exist.
	Vector3 playerPos;
	playerPos.Zero();

	// Actually, the player position generally gets set according to the
	// GET_PLAYER_START_POS() function in script, which we don't currently
	// run in this tool. If this position really matters (perhaps mostly
	// for comparisons with the old way), we may need a better solution. /FF
	playerPos.Set(-1.47f, 75.30f, 6.35f);	// testbed

	CGameWorld::SetPlayerFallbackPos(playerPos);

	CFocusEntityMgr::GetMgr().SetPosAndVel(playerPos, VEC3_ZERO);

#if __BANK
	CVehicleFactory::UpdateCarList();
#endif

	//EXTRACONTENT.ExecuteTitleUpdateDataPatch(CCS_TITLE_UPDATE_STARTUP, true); 
	//EXTRACONTENT.ExecuteTitleUpdateDataPatch((u32)CCS_TITLE_UPDATE_TEXT, true); 

	//	TODO: FIX this, crashes in CVehicleMetaDataFileMounter::LoadDataFile() - it's related to the DATAFILEMGR slot (79) not being setup, but should be by DATAFILEMGR.Load("platformcrc:/data/default"); (according to the gamecode).
//	CExtraContentWrapper::Init(INIT_SESSION);	// note that SP DLC is automatically mounted within this call

	// Calling again as some new models could be loaded with DLCs from CGame::InitSession
	CModelIndex::MatchAllModelStrings();

	if(PARAM_MP_DLC.Get())	// If we specify "-MP_DLC" on the command line, then multiplayer DLC is mounted & SP unmounted below
	{
		// Now we have to mount the MP map changes.
		// See "Network.cpp" CNetwork::StartMatch()
		EXTRACONTENT.ExecuteScriptPatch();
		EXTRACONTENT.ExecuteWeaponPatchMP(true);
		EXTRACONTENT.RevertContentChangeSetGroupForAll((u32)CCS_GROUP_MAP_SP);
		EXTRACONTENT.ExecuteContentChangeSetGroupForAll((u32)CCS_GROUP_MAP);
	}
}

//-----------------------------------------------------------------------------

#define REGISTER_INIT_CALL(func, name)                             m_NavGenSkeleton.RegisterInitSystem(func, name)
#define REGISTER_SHUTDOWN_CALL(func, name)                         m_NavGenSkeleton.RegisterShutdownSystem(func, name)
#define REGISTER_UPDATE_CALL(func, name)                           m_NavGenSkeleton.RegisterUpdateSystem(func, name)
#define REGISTER_UPDATE_CALL_TIMED(func, name)                     m_NavGenSkeleton.RegisterUpdateSystem(func, name, true)
#define REGISTER_UPDATE_CALL_TIMED_BUDGETTED(func, name, budget)   m_NavGenSkeleton.RegisterUpdateSystem(func, name, true, budget)
#define PUSH_UPDATE_GROUP(name)                                    m_NavGenSkeleton.PushUpdateGroup(name, true)
#define PUSH_UPDATE_GROUP_BUDGETTED(name, budget)                  m_NavGenSkeleton.PushUpdateGroup(name, true, budget)
#define POP_UPDATE_GROUP()                                         m_NavGenSkeleton.PopUpdateGroup()
#define SET_INIT_TYPE(type)                                        m_NavGenSkeleton.SetCurrentInitType(type)
#define SET_SHUTDOWN_TYPE(type)                                    m_NavGenSkeleton.SetCurrentShutdownType(type)
#define SET_UPDATE_TYPE(type)                                      m_NavGenSkeleton.SetCurrentUpdateType(type)
#define SET_CURRENT_DEPENDENCY_LEVEL(level)                        m_NavGenSkeleton.SetCurrentDependencyLevel(level)

const unsigned MAP_LOADED_DEPENDENCY_LEVEL_1 = 1;
const unsigned MAP_LOADED_DEPENDENCY_LEVEL_2 = 2;
const unsigned MAP_LOADED_DEPENDENCY_LEVEL_3 = 3;
const unsigned MAP_LOADED_DEPENDENCY_LEVEL_4 = 4;
const unsigned MAP_LOADED_DEPENDENCY_LEVEL_5 = 5;
const unsigned MAP_LOADED_DEPENDENCY_LEVEL_6 = 6;
const unsigned MAP_LOADED_DEPENDENCY_LEVEL_7 = 7;
const unsigned MAP_LOADED_DEPENDENCY_LEVEL_8 = 8;

void CLevelProcessToolGameInterfaceGta::RegisterGameSkeletonFunctions()
{
	RegisterCoreInitFunctions();

	RegisterBeforeMapLoadedInitFunctions();
	RegisterAfterMapLoadedInitFunctions();

	RegisterSessionInitFunctions();
}

void CLevelProcessToolGameInterfaceGta::RegisterCoreInitFunctions()
{
	// Register all functions for calling at the start of the game
	SET_INIT_TYPE(INIT_CORE);

	SET_CURRENT_DEPENDENCY_LEVEL(MAP_LOADED_DEPENDENCY_LEVEL_1);
	{
//		REGISTER_INIT_CALL(CStreaming::Init,                     "CStreaming");
		REGISTER_INIT_CALL(CShaderLib::InitTxd,					 "CShaderLib");
		REGISTER_INIT_CALL(CScaleformMgr::Init,					"CScaleformMgr");
        REGISTER_INIT_CALL(CGtaAnimManager::Init,                "CGtaAnimManager");
		REGISTER_INIT_CALL(fwClipSetManager::Init,				 "fwClipSetManager");
		REGISTER_INIT_CALL(audNorthAudioEngine::Init,            "audNorthAudioEngine");
		REGISTER_INIT_CALL(CClock::Init,                         "CClock");
		REGISTER_INIT_CALL(CCullZones::Init,                     "CCullZones");
		REGISTER_INIT_CALL(CExpensiveProcessDistributer::Init,   "CExpensiveProcessDistributer");	// Used through CVehicleFactory::Create() for some vehicle AI stuff, may be possible to avoid.
		REGISTER_INIT_CALL(CGameLogic::Init,                     "CGameLogic");						// This handles parsing of -level, so it's probably needed.

		REGISTER_INIT_CALL(CObjectPopulationNY::Init,            "CObjectPopulationNY");
		REGISTER_INIT_CALL(COcclusion::Init,                     "COcclusion");
		REGISTER_INIT_CALL(CPopulationStreamingWrapper::Init,    "CPopulationStreamingWrapper");
		//REGISTER_INIT_CALL(CRenderer::Init,                      "CRenderer");	

		REGISTER_INIT_CALL(CTrain::Init,                         "CTrain");

		REGISTER_INIT_CALL(CTaskClassInfoManager::Init,          "CTaskClassInfoManager");
	}

	SET_CURRENT_DEPENDENCY_LEVEL(MAP_LOADED_DEPENDENCY_LEVEL_2);
	{
		REGISTER_INIT_CALL(fwMetaDataStore::Init, "fwMetaDataStore");
		REGISTER_INIT_CALL(CNavMeshScene::Init,		"CScene");				// dependent on CreateFactories(), CStreaming::Init(), CRenderer::Init()
		REGISTER_INIT_CALL(MeshBlendManager::Init,	"MeshBlendManager");	// dependent on CreateFactories(), CStreaming::Init(), CRenderer::Init()
		REGISTER_INIT_CALL(CVisualEffects::Init,	"CVisualEffects");		// dependent on some rendering setup (not exactly sure what)

		// Added for nav tool, I believe the game does this through CVisualEffects::Init().
		//REGISTER_INIT_CALL(CTimeCycleWrapper::Init, "CTimeCycleWrapper");

		REGISTER_INIT_CALL(CGame::ViewportSystemInit,   "ViewportSystemInit"); // dependent on CScene::Init()

		REGISTER_INIT_CALL(CLODLightManager::Init,	"CLODLightManager");
		REGISTER_INIT_CALL(CLODLights::Init,		"CLODLights");
	}

	SET_CURRENT_DEPENDENCY_LEVEL(MAP_LOADED_DEPENDENCY_LEVEL_3);
	{
		REGISTER_INIT_CALL(camManager::Init,     "camManager"); // dependent on CScene::Init() and ViewportSystemInit()
		REGISTER_INIT_CALL(CDebug::Init,         "CDebug");		// dependant on Cscene:Init()
	}
}

void CLevelProcessToolGameInterfaceGta::RegisterBeforeMapLoadedInitFunctions()
{
	SET_INIT_TYPE(INIT_BEFORE_MAP_LOADED);

	SET_CURRENT_DEPENDENCY_LEVEL(MAP_LOADED_DEPENDENCY_LEVEL_1);
	{
#if !__FINAL
		REGISTER_INIT_CALL(CBlockView::Init,          "CBlockView");
#endif
		// This vehicle data must be set up before we parse vehicles.ide in LoadMap
		REGISTER_INIT_CALL(CGtaAnimManager::Init,     "CGtaAnimManager");
		REGISTER_INIT_CALL(CGameWorld::Init,          "CGameWorld");
		REGISTER_INIT_CALL(CHandlingDataMgr::Init,    "CHandlingDataMgr");
		REGISTER_INIT_CALL(CInstanceStoreWrapper::Init,    "CInstanceStoreWrapper");
		REGISTER_INIT_CALL(CMapTypesStoreWrapper::Init,   "fwMapTypesStore");
		REGISTER_INIT_CALL(CModelInfo::Init,          "CModelInfo");
		REGISTER_INIT_CALL(CPatrolRoutes::Init,       "CPatrolRoutes");
		REGISTER_INIT_CALL(CPopZones::Init,           "CPopZones");
		REGISTER_INIT_CALL(CPhysics::Init,            "CPhysics");
		REGISTER_INIT_CALL(CTrain::Init,              "CTrain");	
		REGISTER_INIT_CALL(Water::Init,               "Water");
		REGISTER_INIT_CALL(COcclusion::Init,          "COcclusion");
		REGISTER_INIT_CALL(fwClipSetManager::Init,	  "fwClipSetManager");
		REGISTER_INIT_CALL(CAmbientModelSetManager::InitClass, "CAmbientModelSetManager");
		REGISTER_INIT_CALL(CVehicleMetadataMgr::Init, "CVehicleMetadataMgr");
		REGISTER_INIT_CALL(CScenarioManager::Init,    "CScenarioManager");
		REGISTER_INIT_CALL(CPortal::Init,             "CPortal");
	}
}

void CLevelProcessToolGameInterfaceGta::RegisterAfterMapLoadedInitFunctions()
{
	SET_INIT_TYPE(INIT_AFTER_MAP_LOADED);

	SET_CURRENT_DEPENDENCY_LEVEL(MAP_LOADED_DEPENDENCY_LEVEL_1);
	{
		REGISTER_INIT_CALL(audNorthAudioEngine::Init,         "audNorthAudioEngine");

		REGISTER_INIT_CALL(CClock::Init,                      "CClock");
		REGISTER_INIT_CALL(CHandlingDataMgr::Init,			  "CHandlingDataMgr");
		REGISTER_INIT_CALL(Water::Init,						  "Water");
		REGISTER_INIT_CALL(fwClipSetManager::Init,			  "fwClipSetManager");
		REGISTER_INIT_CALL(fwAnimDirector::InitStatic,		  "fwAnimDirector");
	}

	SET_CURRENT_DEPENDENCY_LEVEL(MAP_LOADED_DEPENDENCY_LEVEL_2);
	{
		// CTimeCycleWrapper was added to the tool only, the game does it through CVisualEffects::Init().
		REGISTER_INIT_CALL(CTimeCycleWrapper::Init, "CTimeCycleWrapper");

		REGISTER_INIT_CALL(CVehicleModelInfo::InitClass,      "CVehicleModelInfo");
		REGISTER_INIT_CALL(CTrain::Init,                      "CTrain");
	}
}

void CLevelProcessToolGameInterfaceGta::RegisterSessionInitFunctions()
{
	SET_INIT_TYPE(INIT_SESSION);

	SET_CURRENT_DEPENDENCY_LEVEL(MAP_LOADED_DEPENDENCY_LEVEL_1);
	{
		REGISTER_INIT_CALL(CStreaming::Init,                  "CStreaming"); // streaming has to be done first since lots of systems depend on it
	}

	SET_CURRENT_DEPENDENCY_LEVEL(MAP_LOADED_DEPENDENCY_LEVEL_2);
	{
        REGISTER_INIT_CALL(fwClipSetManager::Init,			  "fwClipSetManager");
        REGISTER_INIT_CALL(fwAnimDirector::InitStatic,              "fwAnimDirector");
		REGISTER_INIT_CALL(camManager::Init,                  "camManager");
		REGISTER_INIT_CALL(CGameLogic::Init,                  "CGameLogic");
		REGISTER_INIT_CALL(CGameWorld::Init,                  "CGameWorld");
		REGISTER_INIT_CALL(CObjectPopulationNY::Init,         "CObjectPopulationNY");
		REGISTER_INIT_CALL(CPathFindWrapper::Init,            "CPathFind");
		REGISTER_INIT_CALL(CPed::Init,                        "CPed");
		REGISTER_INIT_CALL(CPedModelInfo::InitClass,		  "CPedModelInfo");
		REGISTER_INIT_CALL(CPedPopulation::Init,              "CPedPopulation");
		REGISTER_INIT_CALL(CPerformance::Init,                "CPerformance");
		REGISTER_INIT_CALL(CPhysics::Init,                    "CPhysics");
		REGISTER_INIT_CALL(CPopCycle::Init,                   "CPopCycle");
		REGISTER_INIT_CALL(CProcObjectManWrapper::Init,       "CProcObjectMan");

		REGISTER_INIT_CALL(CStreamingRequestList::Init,		  "CStreamingRequestList");
		REGISTER_INIT_CALL(CTextFileWrapper::Load,            "CTextFile");

		REGISTER_INIT_CALL(CTrain::Init,                      "CTrain");
		REGISTER_INIT_CALL(CVehiclePopulation::Init,          "CVehiclePopulation");
		// Added for nav tool, the game does it through CVisualEffects::Init():
		REGISTER_INIT_CALL(CTimeCycleWrapper::Init,           "CTimeCycleWrapper");
		REGISTER_INIT_CALL(CWorldPoints::Init,                "CWorldPoints");
		REGISTER_INIT_CALL(CGame::TimerInit,                  "fwTimer");
		REGISTER_INIT_CALL(CRenderer::Init,                   "CRenderer");
		REGISTER_INIT_CALL(CGame::ViewportSystemInit,                "ViewportSystemInitLevel");
	}

	SET_CURRENT_DEPENDENCY_LEVEL(MAP_LOADED_DEPENDENCY_LEVEL_3);
	{
		REGISTER_INIT_CALL(CPopulationStreamingWrapper::Init, "CPopulationStreaming"); // dependent on popcycle being initialised
   }
}

//-----------------------------------------------------------------
