#include "materialinterface.h"
#include "fwnavgen/datatypes.h"
#include "fragment/instance.h"
#include "objects/object.h"
#include "physics/gtaMaterialManager.h"
#include "scene/Entity.h"
#include "scene/world/GameWorldHeightMap.h"
#include "vehicleAI/pathfind.h"
#include "vehicles/train.h"

//-----------------------------------------------------------------------------

bool CNavMeshMaterialInterfaceGta::GetIsBakedInAttribute1(Vector3 * UNUSED_PARAM(pTriVerts), phMaterialMgr::Id UNUSED_PARAM(iSurfaceType))
{
	return false;
}

bool CNavMeshMaterialInterfaceGta::GetIsBakedInAttribute2(Vector3 * UNUSED_PARAM(pTriVerts), phMaterialMgr::Id UNUSED_PARAM(iSurfaceType))
{
	return false;
}

bool CNavMeshMaterialInterfaceGta::GetIsBakedInAttribute3(Vector3 * UNUSED_PARAM(pTriVerts), phMaterialMgr::Id UNUSED_PARAM(iSurfaceType))
{
	return false;
}

bool CNavMeshMaterialInterfaceGta::GetIsBakedInAttribute4(Vector3 * UNUSED_PARAM(pTriVerts), phMaterialMgr::Id UNUSED_PARAM(iSurfaceType))
{
	return false;
}

bool CNavMeshMaterialInterfaceGta::GetIsBakedInAttribute5(Vector3 * UNUSED_PARAM(pTriVerts), phMaterialMgr::Id UNUSED_PARAM(iSurfaceType))
{
	return false;
}

bool CNavMeshMaterialInterfaceGta::GetIsBakedInAttribute6(Vector3 * UNUSED_PARAM(pTriVerts), phMaterialMgr::Id UNUSED_PARAM(iSurfaceType))
{
	return false;
}

bool CNavMeshMaterialInterfaceGta::GetIsBakedInAttribute7(Vector3 * UNUSED_PARAM(pTriVerts), phMaterialMgr::Id UNUSED_PARAM(iSurfaceType))
{
	return false;
}

bool CNavMeshMaterialInterfaceGta::GetIsBakedInAttribute8(Vector3 * UNUSED_PARAM(pTriVerts), phMaterialMgr::Id UNUSED_PARAM(iSurfaceType))
{
	return false;
}

bool CNavMeshMaterialInterfaceGta::IsThisSurfaceTypeGlass(phMaterialMgr::Id iSurfaceType)
{
	return PGTAMATERIALMGR->GetIsGlass(iSurfaceType);
}

bool CNavMeshMaterialInterfaceGta::IsThisSurfaceTypeStairs(phMaterialMgr::Id iSurfaceType)
{
	const bool bStairs = PGTAMATERIALMGR->GetPolyFlagStairs(iSurfaceType);
	return bStairs;
}

bool CNavMeshMaterialInterfaceGta::IsThisSurfaceTypePavement(phMaterialMgr::Id iSurfaceType)
{
	// New-style pavement markup which is done per-poly, using a few special per-poly bits
	// stored in the iSurfaceType value.
	bool bPerPolyPavement = PGTAMATERIALMGR->GetPolyFlagWalkablePath(iSurfaceType);
	if(bPerPolyPavement)
		return true;

	VfxGroup_e iSurfaceGroup = PGTAMATERIALMGR->GetMtlVfxGroup(iSurfaceType);
	if(iSurfaceGroup == VFXGROUP_PAVING)
		return true;

	return false;
}

bool CNavMeshMaterialInterfaceGta::IsThisSurfaceTypeRoad(Vector3 * pTriVerts, phMaterialMgr::Id iSurfaceType)
{
	static dev_bool bFullRoadTest = true;

	if(bFullRoadTest)
	{
		((void*)iSurfaceType);
		return ThePaths.CollisionTriangleIntersectsRoad(pTriVerts, false);
	}
	else
	{
		return PGTAMATERIALMGR->GetIsRoad(iSurfaceType);
	}
}

bool CNavMeshMaterialInterfaceGta::IsThisSurfaceTypeTrainTracks(Vector3 * pTriVerts, phMaterialMgr::Id UNUSED_PARAM(iSurfaceType))
{
	return CTrain::CollisionTriangleIntersectsTrainTracks(pTriVerts);
}

bool CNavMeshMaterialInterfaceGta::IsThisSurfaceTypeSeeThrough(phMaterialMgr::Id iSurfaceType)
{
	bool b = PGTAMATERIALMGR->GetIsSeeThrough(iSurfaceType);
	return b;
}

bool CNavMeshMaterialInterfaceGta::IsThisSurfaceTypeShootThrough(phMaterialMgr::Id iSurfaceType)
{
	bool b = PGTAMATERIALMGR->GetIsShootThrough(iSurfaceType);
	return b;
}

bool CNavMeshMaterialInterfaceGta::IsThisSurfaceAllowedForNetworkRespawning(phMaterialMgr::Id iSurfaceType)
{
	bool b = (PGTAMATERIALMGR->GetPolyFlagNoNetworkSpawn(iSurfaceType)==false);
	return b;
}

bool CNavMeshMaterialInterfaceGta::IsThisSurfaceWalkable(Vector3 * UNUSED_PARAM(pTriVerts), phMaterialMgr::Id iSurfaceType)
{
	if(PGTAMATERIALMGR->GetPolyFlagNoNavmesh(iSurfaceType))
		return false;

	return true;
}

bool CNavMeshMaterialInterfaceGta::IsThisSurfaceClimbable(phMaterialMgr::Id iSurfaceType)
{
	if(PGTAMATERIALMGR->GetPolyFlagNotClimbable(iSurfaceType))
		return false;

	return true;
}

// This returns the ped density associated with the surface.
// NOTE : iSurface type is not a full 32 bits of index.  In fact some of these 32 bits are used
// to represent other per-polygon data.  This is to support new stuff such as per-poly ped density.
// In order to use this in tandem with the original material-based ped density, we'll for now
// take the max of the two ped-density values obtained with both old & new methods.
float CNavMeshMaterialInterfaceGta::GetPedDensityForThisSurfaceType(phMaterialMgr::Id iSurfaceType)
{
	int iVal = PGTAMATERIALMGR->UnpackPedDensity(iSurfaceType);

	// "paving_slabs" material shall default to a medium ped-density (3)
	// for now.  Once out-sourced roads are back in the studio we will
	// turn this off, and map artists will need to set it manually
	// via the packed ped-density stored in the collision polys.
	VfxGroup_e iSurfaceGroup = PGTAMATERIALMGR->GetMtlVfxGroup(iSurfaceType);
	if(iSurfaceGroup == VFXGROUP_PAVING)
	{
		if(iVal > 0)
			return (float)iVal;
		else
			return 3.0f;
	}
	
	return (float)iVal;
}

u32 CNavMeshMaterialInterfaceGta::GetDynamicEntityInfoFlags(const fwEntity * pEntity)
{
	u32 iFlags = 0;

	fragInst * pFragInst = pEntity->GetFragInst();
	const bool bHasUprootLimit = (pFragInst!=NULL && (pFragInst->GetTypePhysics()->GetMinMoveForce() > 0.0f) && !((CObject*)pEntity)->m_nObjectFlags.bHasBeenUprooted);
	if( bHasUprootLimit )
	{
		iFlags |= fwDynamicEntityInfo::FLAG_UPROOTABLE_OBJECT;
	}
	CBaseModelInfo * pModelInfo = CModelInfo::GetBaseModelInfo(pEntity->GetModelId());
	if(pModelInfo && pModelInfo->GetIsClimbableByAI())
	{
		iFlags |= fwDynamicEntityInfo::FLAG_CLIMABABLE_BY_AI;
	}

	if( ((CEntity*)pEntity)->GetIsTypeObject() && ((CObject*)pEntity)->IsADoor())
	{
		iFlags |= fwDynamicEntityInfo::FLAG_DOOR;
	}

	return iFlags;
}

void CNavMeshMaterialInterfaceGta::SetCollisionPolyDataFromArchetypeFlags(TColPolyData & colPolyData, const u32 iArchetypeFlags)
{
	if ((iArchetypeFlags & ArchetypeFlags::GTA_MAP_TYPE_MOVER)!=0)
	{
		colPolyData.SetIsMoverBound(true);
	}

	if ((iArchetypeFlags & ArchetypeFlags::GTA_MAP_TYPE_WEAPON)!=0)
	{
		colPolyData.SetIsWeaponBound(true);
	}

	if ((iArchetypeFlags & ArchetypeFlags::GTA_MAP_TYPE_COVER)!=0)
	{
		colPolyData.SetIsCoverBound(true);
	}

	if ((iArchetypeFlags & ArchetypeFlags::GTA_STAIR_SLOPE_TYPE)!=0)
	{
		colPolyData.SetIsStairSlopeBound(true);
	}

#if HEIGHTMAP_GENERATOR_TOOL
	if ((iArchetypeFlags & ArchetypeFlags::GTA_VEHICLE_INCLUDE_TYPES)!=0)
	{
		colPolyData.SetIsVehicleBound(true);
	}
#endif

	if ((iArchetypeFlags & ArchetypeFlags::GTA_RIVER_TYPE)!=0)
	{
		colPolyData.SetIsRiverBound(true);
	}
}

#if HEIGHTMAP_GENERATOR_TOOL
int CNavMeshMaterialInterfaceGta::GetMaterialVfxGroup(phMaterialMgr::Id iSurfaceType, const char *& outName)
{
	VfxGroup_e iSurfaceGroup = PGTAMATERIALMGR->GetMtlVfxGroup(iSurfaceType);
	outName = g_fxGroupsList[iSurfaceGroup];
	return iSurfaceGroup;
}

int CNavMeshMaterialInterfaceGta::GetMaterialMaskIndex(phMaterialMgr::Id iSurfaceType)
{
	return CGameWorldHeightMap::GetMaterialMaskIndex(iSurfaceType);
}

int CNavMeshMaterialInterfaceGta::GetMaterialProcIndex(phMaterialMgr::Id iSurfaceType)
{
	return PGTAMATERIALMGR->UnpackProcId(iSurfaceType);
}
#endif

//-----------------------------------------------------------------------------
