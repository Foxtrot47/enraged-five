#ifndef NAVMESHGENERATOR_EXPORTCOLLISION_H
#define NAVMESHGENERATOR_EXPORTCOLLISION_H

#include "fwgeovis/geovis.h"
#include "fwscene/stores/mapdatastore.h"
#include "fwnavgen/baseexportcollisiongrid.h"
#include "fwutil/PtrList.h"
#include "pathserver/ExportCollision.h"
#include "scene/RegdRefTypes.h"
#include "templates/LinkList.h"

namespace rage
{
	class fiStream;
	class Vector2;
	class Vector3;
}

class CIplStore;
class CNavMeshDataExporterTool;
class CPathFind;
struct CNavMeshDataDefaultIplGroups;
struct CNavMeshDataDefaultEntitySets;

//-----------------------------------------------------------------------------

//*********************************************************************************
//
//	ExportCollision.h
//
//	The classes/functions in this file are concerned with exporting the collision
//	data from within the game, as small .bnd or .tri files.  These files can then
//	be read in by the NavMeshMaker tool to create the .nav navmeshes for the game.
//
//*********************************************************************************

class CNavMeshDataExporterInterfaceTool : public CNavMeshDataExporterInterface
{
public:
	explicit CNavMeshDataExporterInterfaceTool(CNavMeshDataExporterTool &tool);

	virtual bool EntityIntersectsCurrentNavMesh(CEntity* pEntity) const;

	virtual bool ExtentsIntersectCurrentNavMesh(Vec3V_In vMin, Vec3V_In vMax) const;

	virtual bool GetCurrentNavMeshExtents(spdAABB & outAABB) const;

	virtual bool IsAlreadyInPhysicsLevel(CPhysical* pPhysical, phArchetype* pArchetype) const;

	virtual void AppendToActiveBuildingsArray(atArray<CEntity*>& entityArray);

	virtual void AddSearches(atArray<fwBoxStreamerSearch>& searchList);

	CNavMeshDataExporterTool*	m_Tool;
};


class CNavMeshDataExporterTool : public fwExportCollisionGridTool
{
public:
	CNavMeshDataExporterTool(fwLevelProcessTool * pTool);
	virtual ~CNavMeshDataExporterTool();

	// -- fwLevelProcessToolExporterInterface interface --

	virtual void PreInit();
	virtual void Init();
	virtual void Shutdown();

	virtual bool UpdateExport();

	// -- CNavMeshDataExporterTool interface --

	static void InitConfig();
	static void ShutdownConfig();

	bool EntityIntersectsCurrentNavMesh(CEntity * pEntity);
	bool ExtentsIntersectCurrentNavMesh(Vec3V_In vMin, Vec3V_In vMax);
	bool GetCurrentNavMeshExtents(spdAABB & outAABB);

	static bool IsAlreadyInPhysicsLevel(CPhysical * pPhysical, phArchetype * pArchetype=NULL);

	void ResetAndOpenTriFile(const char *pFilename);

	void AppendToActiveBuildingsArray(atArray<CEntity*>& entityArray);

	void AddSearches(atArray<fwBoxStreamerSearch>& searchList);

protected:
	virtual void GridCellFinished();
	virtual void GridProcessFinished();
	virtual void UpdateStreamingPos(const Vector3 &pos);

	void NotifyMainGameLoopHasStarted();

	// Opportunity to set the default of IPL groups across the map
	void LoadDefaultIplGroupList();
	void RequestDefaultIplGroups();

	void LoadDefaultEntitySetList();
	void RequestDefaultEntitySets();

	// Searches added to box streamed should be requesting mapdata; but sometimes this doesn't seem to work, so request explicitly
	bool EnsureMapDataLoaded();

	//**********************************************************************
	//	Exports the .bnd files for every navmesh in the world.
	//	Each .bnd file will contain the geometry for a 2x2 block of sectors
	//**********************************************************************

	virtual bool StartCollisionExport();
	void OpenExportLog();

	void AddInteriorsToExportList(atArray<CEntity*> & entitiesToExport /*, std::vector<fwSphere> & portalBoundaries*/);

	//	Called every frame when collision export is active.
	bool ProcessCollisionExport();

	void DisplayPoolUsage();

	void LaunchNavMeshCompiler();

#if HEIGHTMAP_GENERATOR_TOOL
	void ExportMapData(const atArray<u32> & slotList, std::vector<Vector3> & triangleVertices, std::vector<phMaterialMgr::Id> & triangleMaterials, std::vector<u16> & colPolyFlags, std::vector<u32> & archetypeFlags, std::vector<fwNavTriData> & triDataArray);
#endif

	//****************************************************************************
	//	Exports a bound.  Calls the appropriate function.
	//****************************************************************************

	s32 MaybeExportCollisionForEntity(CEntity * pEntity, const fragType * pFragType, Vector3 & vNavMeshMinsExtra, Vector3 & vNavMeshMaxsExtra, std::vector<Vector3> & triangleVertices, std::vector<phMaterialMgr::Id> & triangleMaterials, std::vector<u16> & colPolyFlags, std::vector<u32> & archetypeFlags, std::vector<fwNavTriData> & triDataArray);

	static bool AddLadderFromMapCB(CEntity * pEntity, void * pData);
	bool ScanForLaddersToExport(CEntity * pEntity, std::vector<fwNavLadderInfo> * laddersList);
	bool ExportLadderInfo(CEntity * pLadder, s32 iLadderIndex, std::vector<fwNavLadderInfo> * laddersList);

	static bool AddDynamicObjectFromMapCB(CEntity * pEntity, void * pData);

	static void FindCarNodesInArea(CPathFind &paths, const Vector3 & vMin, const Vector3 & vMax, atArray<Vector3> & carNodesList);

	//static void GetAllBuildingsWithPhysics(fwMapDataStore &store, CLinkList<CEntity*>* pResultsList);

	static void ScanForBuildings();

	//static bool AppendBuildingCB(fwEntity* entity, void* pData);

	//************************************************************************************
	// Function for finding the average water level for current navmesh
	//************************************************************************************

	void FindWaterLevelForCurrentNavMesh();

	// PURPOSE:	Temporarily stores the name of the "extras" file, so it's available to GridCellFinished()
	//			when it's time to write the file.
	ConstString							m_ExtrasFileName;

	CNavMeshDataExporterInterfaceTool	m_ExporterInterface;
	atArray<CEntity*>					m_PotentialEntitiesToExport;

	atArray<Vector3>					m_CarNodesList;
	std::vector<fwNavLadderInfo>		m_LadderInfos;
	std::vector<spdSphere>				m_PortalBoundaries;
	std::vector<fwEntity*>				m_DynamicEntitiesList;

	fiStream*							m_pExportCollisionLogFile;
	CNavMeshDataDefaultIplGroups*		m_DefaultIplGroups;			// Owned
	CNavMeshDataDefaultEntitySets*		m_DefaultEntitySets;		// Owned

	int m_iPeakNumRefs;
	int m_iPeakNumBuildings;
	int m_iPeakNumObjects;
	int m_iPeakNumDummyObjects;
	u32 m_iPeakNumStreamedArchetypes;
	int m_iPeakNumPtrSingle;
	int m_iPeakNumPtrDouble;

	float m_fTimeUntilWeExportThisNavMesh;

	bool m_bErrorsInExport;
	bool m_bHasBuildingsWithPhysics;
	bool m_bHasCompEntities;

	bool m_bLaunchCompilerAfterExporting;

	bool m_bAlwaysWaitForStreaming;
	bool m_bExportObjects;
	bool m_bExportOctrees;
	bool m_bExportInteriors;
	bool m_bPrintOutAllLoadedCollision;
};

//-----------------------------------------------------------------------------

#endif	// NAVMESHGENERATOR_EXPORTCOLLISION_H
