@echo off
setlocal

pushd %~dp0

call ..\..\setenv.bat

call %RS_TOOLSROOT%\script\util\projgen\rebuildGameTool.bat -changelist -force %* NavMeshGenerator.slndef
popd
:end