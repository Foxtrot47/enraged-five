#define LARGE_BUDDY_HEAP

#if __WIN32PC

#define SIMPLE_HEAP_SIZE		(200*1024)
#define SIMPLE_PHYSICAL_SIZE	(500*1024)

#else

#error "Not supported for this platform.")

#endif	// __WIN32PC

#include "navmeshgenapp.h"
#include "exportcollision.h"
#include "fwnavgen/config.h"
#include "parser/manager.h"
#include "system/FileMgr.h"
#include "physics/gtaArchetype.h"
#include "grprofile/timebars.h"

#define PGKNOWNREFPOOLSIZE (RSG_PC ? 65536 : 32768)	// Must be a multiple of 32. This overrides the size of pgBaseKnownReferencePool, which seems to be needed for this tool to work.

#include "fwnavgen/main.h"


XPARAM(exportsectors);
XPARAM(exportregion);
XPARAM(exportall);
XPARAM(exportcontinuefrom);
XPARAM(exportdlc);
XPARAM(numStreamedSlots);
XPARAM(nogameaudio);

PARAM(noptfx, "disables particle effects");
XPARAM(nullDriver);
XPARAM(simplemapdata);

#include "ai\navmesh\navmesh.h"

CompileTimeAssert(HEIGHTMAP_GENERATOR_TOOL == 0); // must be disabled to compile this project

namespace rage
{
	int NavMeshMakerMain();
	XPARAM(dontwarnonmissingmodule);
}

int Main()
{
	PARAM_noptfx.Set("");
	PARAM_dontwarnonmissingmodule.Set("");
	PARAM_numStreamedSlots.Set("40000");
	PARAM_nogameaudio.Set("");
	PARAM_nullDriver.Set("");
	PARAM_simplemapdata.Set("");

	if(PARAM_exportregion.Get() || PARAM_exportcontinuefrom.Get() || PARAM_exportsectors.Get() || PARAM_exportall.Get() || PARAM_exportdlc.Get())
	{
		fwLevelProcessToolImpl<CLevelProcessToolGameInterfaceGta, CNavMeshDataExporterTool> myApp;

		const u32 NAVMESH_EXPORT_COLLISON_ARCHETYPES = 
			ArchetypeFlags::GTA_MAP_TYPE_MOVER
			| ArchetypeFlags::GTA_OBJECT_TYPE
			| ArchetypeFlags::GTA_RIVER_TYPE
			| ArchetypeFlags::GTA_GLASS_TYPE
			| ArchetypeFlags::GTA_STAIR_SLOPE_TYPE
			| ArchetypeFlags::GTA_MAP_TYPE_COVER;

		myApp.SetCollisionArchetypeFlags(NAVMESH_EXPORT_COLLISON_ARCHETYPES);

		// For very round pillars, export COVER boxes on them to support corner cover generation
		const bool bCoverBoxesForPillarShapes = true;
		myApp.SetExportCoverBoxesForPillarShapes(bCoverBoxesForPillarShapes);
		myApp.SetCoverBoxCollisionArchetypeFlags(ArchetypeFlags::GTA_MAP_TYPE_COVER);

		myApp.Run();
	}
	else
	{
		PF_INIT_TIMEBARS("Main", 1260, 99999999.9f);
		PF_INIT_STARTUPBAR("Startup", 1260);


		// Load configuration data. INIT_PARSER needs to be called before we can do that,
		// and initializing CFileMgr allows us to use the type of file paths that we normally
		// use in this game ("common:/...", etc). In the -exportregion/-exportsectors/-exportall case above,
		// this is all done within the fwLevelProcessToolImpl::Run() call.
//		CFileMgr::Initialise();
		INIT_PARSER;
		CNavMeshDataExporterTool::InitConfig();

		NavMeshMakerMain();

		CNavMeshDataExporterTool::ShutdownConfig();
		SHUTDOWN_PARSER;
		// No Shutdown() in CFileMgr?
		//	CFileMgr::Shutdown();
	}


#if __WIN32PC
	// Hack to work around various shutdown problems, such as systems that assert or crash
	// just from static objects being created and then destroyed again.
	ExitProcess(0);
#else
	return 0;
#endif
}
