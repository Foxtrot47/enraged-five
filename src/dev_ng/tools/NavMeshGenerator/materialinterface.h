#ifndef NAVMESHGENERATOR_MATERIALINTERFACE_H
#define NAVMESHGENERATOR_MATERIALINTERFACE_H

#include "fwnavgen/materialinterface.h"		// For fwNavMeshMaterialInterface

//-----------------------------------------------------------------------------

//***************************************************************************************************
//	CNavMeshMaterialInterfaceGta
//	Class used to define which surface attributes are to be saved in the navmesh for GTA
//***************************************************************************************************
class CNavMeshMaterialInterfaceGta : public fwNavMeshMaterialInterface
{
public:

	bool GetIsBakedInAttribute1(Vector3 * pTriVerts, phMaterialMgr::Id iSurfaceType);
	bool GetIsBakedInAttribute2(Vector3 * pTriVerts, phMaterialMgr::Id iSurfaceType);
	bool GetIsBakedInAttribute3(Vector3 * pTriVerts, phMaterialMgr::Id iSurfaceType);
	bool GetIsBakedInAttribute4(Vector3 * pTriVerts, phMaterialMgr::Id iSurfaceType);
	bool GetIsBakedInAttribute5(Vector3 * pTriVerts, phMaterialMgr::Id iSurfaceType);
	bool GetIsBakedInAttribute6(Vector3 * pTriVerts, phMaterialMgr::Id iSurfaceType);
	bool GetIsBakedInAttribute7(Vector3 * pTriVerts, phMaterialMgr::Id iSurfaceType);
	bool GetIsBakedInAttribute8(Vector3 * pTriVerts, phMaterialMgr::Id iSurfaceType);

	bool IsThisSurfaceTypeGlass(phMaterialMgr::Id iSurfaceType);
	bool IsThisSurfaceTypeStairs(phMaterialMgr::Id iSurfaceType);
	bool IsThisSurfaceTypePavement(phMaterialMgr::Id iSurfaceType);
	bool IsThisSurfaceTypeRoad(Vector3 * pTriVerts, phMaterialMgr::Id iSurfaceType);
	bool IsThisSurfaceTypeTrainTracks(Vector3 * pTriVerts, phMaterialMgr::Id iSurfaceType);
	bool IsThisSurfaceTypeSeeThrough(phMaterialMgr::Id iSurfaceType);
	bool IsThisSurfaceTypeShootThrough(phMaterialMgr::Id iSurfaceType);
	bool IsThisSurfaceAllowedForNetworkRespawning(phMaterialMgr::Id iSurfaceType);
	bool IsThisSurfaceWalkable(Vector3 * pTriVerts, phMaterialMgr::Id iSurfaceType);
	bool IsThisSurfaceClimbable(phMaterialMgr::Id iSurfaceType);
	float GetPedDensityForThisSurfaceType(phMaterialMgr::Id iSurfaceType);

	u32 GetDynamicEntityInfoFlags(const fwEntity * pEntity);

	void SetCollisionPolyDataFromArchetypeFlags(TColPolyData & colPolyData, const u32 iArchetypeFlags);

#if HEIGHTMAP_GENERATOR_TOOL
	int GetMaterialVfxGroup(phMaterialMgr::Id iSurfaceType, const char *& outName);
	int GetMaterialMaskIndex(phMaterialMgr::Id iSurfaceType);
	int GetMaterialProcIndex(phMaterialMgr::Id iSurfaceType);
#endif
};

//-----------------------------------------------------------------------------

#endif	// NAVMESHGENERATOR_MATERIALINTERFACE_H
