#ifndef STRICT
# define STRICT
#endif
# pragma warning(push)
# pragma warning(disable: 4668)
# include <WinSock2.h>  //Prevent windows.h from including winsock.h
# include <windows.h>
# pragma warning(pop)

#include <psapi.h>
#pragma comment(lib, "Psapi.lib")

// C headers
#include <stdio.h>
#include <vector>

//Rage headers
#include "atl/array.h"
#include "system/param.h"

PARAM(exe, "Executable name");
PARAM(dll, "DLL to inject");

HANDLE GetProcessHandle(const char* pProcessName)
{
	DWORD processIds[256];
	DWORD numBytesReturned;

	if(!EnumProcesses(processIds, sizeof(processIds), &numBytesReturned))
	{
		Printf("Failed to enumerate processes");
		return 0;
	}

	for(int i=0; i<numBytesReturned/sizeof(DWORD); i++)
	{
		char processName[259];
		DWORD size = sizeof(processName);

		// Ignore process 0
		if(processIds[i] == 0)
			continue;

		HANDLE hProcess = OpenProcess( PROCESS_ALL_ACCESS,
			FALSE, processIds[i] );

		if(hProcess == 0)
		{
			continue;
		}

		if(!QueryFullProcessImageName(hProcess, 0, processName, &size))
		{
			Printf("Failed to get process name\n");
			CloseHandle(hProcess);
			return 0;
		}

		char* filename = strrchr(processName, '\\');
		if(filename)
		{
			filename++;
			if(_stricmp(filename, pProcessName) == 0)
			{
				Printf("Connected to process %s\n", filename);
				return hProcess;
			}
		}
		CloseHandle(hProcess);
	}

	return 0;
}

u8* LoadDll(const char* pDllName, int& size)
{
	FILE* file = fopen(pDllName, "r");
	if(file == NULL)
		return NULL;
	// get size
	fseek(file, 0, SEEK_END);
	size = ftell(file);
	fseek(file, 0, SEEK_SET);

	u8* pBuffer = new u8[size];
	fread(pBuffer, 1, size, file);
	fclose(file);

	return pBuffer;
}

int main(int argc, char**argv)
{
	const char* pProcessName = "GTA5.exe";
	const char* pDllName = "test.dll";

	sysParam::Init(argc, argv);

	// Process params
	PARAM_exe.Get(pProcessName);
	PARAM_dll.Get(pDllName);

	HANDLE hProcess = GetProcessHandle(pProcessName);
	if(hProcess == 0)
	{
		Printf("Failed to find process\n");
		return -1;
	}

/*	int size = 0;
	u8* dll = LoadDll(pDllName, size);
	if(dll == NULL)
	{
		Printf("Failed to load %s\n", pDllName);
		return -1;
	}*/

	// allocate space for DLL

	void* mem = VirtualAllocEx(hProcess, NULL, strlen(pDllName)+1, MEM_COMMIT, PAGE_READWRITE);
	if(mem == NULL)
	{
		Printf("Failed to allocate memory\n");
		return -1;
	}

	if(!WriteProcessMemory(hProcess, mem, pDllName, strlen(pDllName)+1, NULL))
	{
		Printf("Failed to write memory\n");
		return -1;
	}

	LPTHREAD_START_ROUTINE pLoadLibrary = (LPTHREAD_START_ROUTINE )GetProcAddress(GetModuleHandle("kernel32.dll"), "LoadLibraryA");
	HANDLE hThread = CreateRemoteThread(hProcess, NULL, 0, pLoadLibrary, mem, 0, NULL);

	if(hThread == NULL)
	{
		Printf("Failed to create DLL create thread\n");
		return -1;
	}

	CloseHandle(hProcess);

	Printf("DLL %s injected into %s\n", pDllName, pProcessName);

}

