@ECHO OFF

REM
REM Setenv for visual studio changes to prevent having to run the tools 
REM installer, and be able to run several projects in VS with ease.
REM

set RS_BUILDBRANCH=.
set RS_CODEBRANCH=x:\gta5\src\dev_temp
set RS_PROJECT=gta5
set RS_PROJROOT=x:\gta5
set RS_SCRIPTBRANCH=x:\gta5\script\dev_temp
set RS_TOOLSROOT=X:\gta5\tools_ng
set RAGE_DIR=X:\gta5\src\dev_temp\rage
set RS_TITLE_UPDATE_NG=x:\gta5\titleupdate\dev_temp

ECHO SETTING SPECIFIC ENVIRONMENT VALUES TO OVERRIDE SETENV.BAT
ECHO OVERRIDES:
ECHO RS_BUILDBRANCH:    %RS_BUILDBRANCH%
ECHO RS_CODEBRANCH:     %RS_CODEBRANCH%
ECHO RS_PROJECT:        %RS_PROJECT%
ECHO RS_PROJROOT:       %RS_PROJROOT%
ECHO RS_SCRIPTBRANCH:   %RS_SCRIPTBRANCH%
ECHO RS_TOOLSROOT:      %RS_TOOLSROOT%
ECHO RAGE_DIR:          %RAGE_DIR%
ECHO RS_TITLE_UPDATE_NG:          %RS_TITLE_UPDATE_NG%

CALL %RS_TOOLSROOT%\bin\setenv.bat
