// dllmain.cpp : Defines the entry point for the DLL application.
#include "stdafx.h"

#include <stdio.h>
#include <string.h>
#include "mhook/mhook.h"

typedef enum _MEMORY_INFORMATION_CLASS {
	MemoryBasicInformation,
	MemoryWorkingSetList,
	MemorySectionName,
	MemoryBasicVlmInformation,
	blah_last_MEMORY_INFORMATION_CLASS  = 0xffffffffffffffff	// Need to force this enum to be 64bit
} MEMORY_INFORMATION_CLASS;  

typedef DWORD  (WINAPI *MyNtQueryVirtualMemoryFnPtr)(	IN HANDLE               ProcessHandle,
										  IN PVOID                BaseAddress,
										  IN MEMORY_INFORMATION_CLASS MemoryInformationClass,
										  OUT PVOID               Buffer,
										  IN size_t                Length,
										  OUT size_t*	              ResultLength OPTIONAL);

MyNtQueryVirtualMemoryFnPtr TrueNtQueryVirtualMemory = (MyNtQueryVirtualMemoryFnPtr)GetProcAddress(GetModuleHandle(L"ntdll"), "NtQueryVirtualMemory");

DWORD  WINAPI MyNtQueryVirtualMemory(	IN HANDLE               ProcessHandle,
													  IN PVOID                BaseAddress,
													  IN MEMORY_INFORMATION_CLASS MemoryInformationClass,
													  OUT PVOID               Buffer,
													  IN size_t                Length,
													  OUT size_t*	              ResultLength OPTIONAL)
{
	wchar_t output[256];
	_snwprintf_s(output, 256, L"NtQueryVirtualMemory(%p, %p, %d, %p, %lu, %p) hooked\n", ProcessHandle, BaseAddress, MemoryInformationClass, Buffer, Length, ResultLength);
	OutputDebugString(output);
	return TrueNtQueryVirtualMemory(ProcessHandle, BaseAddress, MemoryInformationClass, Buffer, Length, ResultLength);
}

void DllThread()
{
	HMODULE hModule = GetModuleHandle(L"ntdll.dll");
	Mhook_SetHook((PVOID*)&TrueNtQueryVirtualMemory, (void*)MyNtQueryVirtualMemory);

	while(true)
	{
		Sleep(1024);
		OutputDebugString(L"Attached DLL!!!\n");
	}
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		if(!CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE) DllThread, NULL, NULL, NULL))
			return FALSE;
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}

	OutputDebugString(L"Attached DLL!!\n");
	return TRUE;
}

