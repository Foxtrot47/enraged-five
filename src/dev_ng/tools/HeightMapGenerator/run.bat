echo off

set COMMANDS=-exportall -exportscriptstaticbounds -exportFolder=X:/tris_heightmap -level=gta5 -nohangdetectthread -noaudiothread -nogameaudio -nobndwarn -noscaleformprealloc -nopopups -simplemapdata -aggressivecleanup -nulldriver -art_all=fatal -physics_all=fatal -modelinfo_all=fatal -Audio_NorthAudio_tty=fatal

echo ###############################################################################################
echo ### GENERATING TRI FILES ######################################################################
echo ###############################################################################################

	start "gen tri files" /WAIT /Dx:\gta5\build\dev_ng heightmapgenerator.exe -logfile=c:/hm_log.txt %COMMANDS%

echo ###############################################################################################
echo ### RASTERISING HEIGHTMAP TILES ###############################################################
echo ###############################################################################################

REM #########################################################
REM ## generate heightmap/density/water images
REM #########################################################

	start "gen tiles 4,5" /WAIT /Dx:\gta5\build\dev_ng heightmapgenerator.exe -heightmaponly -exporttiles -autorender -autodelete -res=4 -smp=5 %COMMANDS%
	start "gen tiles 1,1" /WAIT /Dx:\gta5\build\dev_ng heightmapgenerator.exe -heightmaponly -exporttiles -autorender -autodelete -res=1 -smp=1 %COMMANDS%

REM #########################################################
REM ## generate medium-res water images (and waterheight.dat)
REM #########################################################

	start "gen tiles 2,1 (water)" /WAIT /Dx:\gta5\build\dev_ng heightmapgenerator.exe -heightmaponly -wateronly -exporttiles -autorender -autodelete -res=2 -smp=1 %COMMANDS%

REM #########################################################
REM ## generate heightmap.dat
REM #########################################################

	start "heightmap" /WAIT /Dx:\gta5\build\dev_ng heightmapgenerator.exe -heightmaponly -includescriptstaticbounds %COMMANDS%

echo ###############################################################################################
echo ### DONE ######################################################################################
echo ###############################################################################################

pause
