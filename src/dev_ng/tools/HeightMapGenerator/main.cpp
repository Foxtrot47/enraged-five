#define LARGE_BUDDY_HEAP

#if __WIN32PC

#include "heightmapgenerator.h"
#if HEIGHTMAP_GENERATE_NAVMESH_TRI_FILES
	#define SIMPLE_HEAP_SIZE		(200*1024) // keep these in sync with tools/NavMeshGenerator/main.cpp
	#define SIMPLE_PHYSICAL_SIZE	(500*1024)
#else
	#define SIMPLE_HEAP_SIZE		(700*1024) // heightmap generation requires much larger heaps for high-res
	#define SIMPLE_PHYSICAL_SIZE	(400*1024)
#endif

#else

#error "Not supported for this platform.")

#endif	// __WIN32PC

#define PGKNOWNREFPOOLSIZE 65536	// Must be a multiple of 32. This overrides the size of pgBaseKnownReferencePool, which seems to be needed for this tool to work.

// rage
#include "parser/manager.h"
#include "system/bootmgr.h"
#include "system/param.h"

// framework
//#include "ai/navmesh/navmesh.h"
#include "fwgeovis/geovis.h"
#include "fwnavgen/config.h"
#include "fwnavgen/main.h"
#include "streaming/streamingdefs.h"

// game
#include "renderer/Water.h"
#include "system/FileMgr.h"
#include "physics/gtaArchetype.h"

#include "exportcollision.h"
#include "heightmapgenerator.h"
#include "navmeshgenapp.h"

XPARAM(exportsectors);
XPARAM(exportregion);
XPARAM(exportall);
XPARAM(exporttiles);
//XPARAM(nullDriver);
XPARAM(level);
XPARAM(numStreamedSlots);

PARAM(noptfx, "disables particle effects");

PARAM(heightmaponly, "generate heightmap using existing tri files");
PARAM(heightmapgen, "generate tri files and then heightmap");
PARAM(sync_in, "wait for sync.hm1 (tri file generation) and sync.gc2 files (geometry collector offline tool)");

CompileTimeAssert(HEIGHTMAP_GENERATOR_TOOL); // must be enabled locally to compile this project

namespace rage
{
	XPARAM(dontwarnonmissingmodule);
	XPARAM(nopopups);
}

int Main()
{
	PARAM_noptfx.Set("");
	PARAM_dontwarnonmissingmodule.Set("");
	PARAM_numStreamedSlots.Set("32768");
	//PARAM_nullDriver.Set("");

	char args[2048] = "";

	for (int i = 0; i < sysParam::GetArgCount(); i++)
	{
		strcat(args, sysParam::GetArg(i));
		strcat(args, " ");
	}

	Displayf(args);

	/*
	// ================================================================================================================
	for running full synchronised process .. (WIP)

	===========
	step 0
	* delete all x:/sync* files (these are used to synchronise the various tools)

	===========
	step 1
	* launch in-game Geometry Collector and Asset Analysis on PS3/beta
		required local code changes:
			x:\rdr3\src\dev\rage\base\src\file\streambuffers.cpp
			DECLARE_STREAM_BUFFERS(64) // i'm not sure how many we really need, but it is > 32 to run both gc and aa simultaneously
		required commandline args:
			-rag
			-nopopups (recommended but not required)
			-numStreamedSlots=50000
			-nevertimeout (?)
		1. fly off the map somewhere so nothing but ocean is in view
		2. drop the player
		3. wait for streaming to settle down, then press PAUSE
		4. in rag, press "Streaming Iterator/Start All"
		this will write x:/sync.gc1 and x:/sync.aa1 when complete
	* run HeightMapGenerator/run_tri_files_only.bat on PC
		always load HeightMapGenerator_2010_unity.sln locally and compile before proceeding (this is heavily dependent on game code changes)
		this will write x:/sync.hm1 when complete

	these can be started in either order, and can run simultaneously
	the next step (2) can be started immediately afterwards, but it would be best give step 1 at least 5-10 minutes head start in case there are any problems

	===========
	step 2
	* run HeightMapGenerator/run_process.bat on PC
		this will start 3 processes simultaneously ..
		1. wait for x:/sync.aa1 (from step 1) before running AssetAnalysisTool.exe
		2. wait for x:/sync.gc1 (from step 1) before running GeometryCollectorTool.exe, and then write x:/sync.gc2 when done
		3. wait for x:/sync.hm1 (from step 1) and x:/sync.gc2 (from step 2) before running HeightMapGenerator.exe
	// ================================================================================================================
	*/

	const char* syncInPath1 = "x:/sync.hm1"; // tri file generation
	const char* syncInPath2 = "x:/sync.gc2"; // geometry collector offline tool

	if (PARAM_sync_in.Get())
	{
		fprintf(stdout, "waiting for %s .. ", syncInPath1);
		fflush(stdout);

		while (1)
		{
			FILE* sync = fopen(syncInPath1, "rb");

			if (sync)
			{
				fclose(sync);
				fprintf(stdout, "ok.\n");
				break;
			}
			else
			{
				Sleep(1000); // sleep for one second
			}
		}

		fprintf(stdout, "waiting for %s .. ", syncInPath2);
		fflush(stdout);

		while (1)
		{
			FILE* sync = fopen(syncInPath2, "rb");

			if (sync)
			{
				fclose(sync);
				fprintf(stdout, "ok.\n");
				break;
			}
			else
			{
				Sleep(1000); // sleep for one second
			}
		}
	}

	// NOTE -- we currently only support navmesh tri file generation or heightmap generation, but not both
	{
		if (PARAM_heightmapgen.Get() && !PARAM_exporttiles.Get())
		{
			Quitf("### command line 'heightmapgen' not supported, cannot generate navmesh tri files and heightmap from the same executable");
		}
		else
		{
#if HEIGHTMAP_GENERATE_NAVMESH_TRI_FILES
			if (PARAM_heightmaponly.Get() && PARAM_exportall.Get() && !PARAM_exporttiles.Get())
			{
				//Quitf("### command line 'heightmaponly' not supported, must set HEIGHTMAP_GENERATE_NAVMESH_TRI_FILES to 0");
			}
#else
			if (!PARAM_heightmaponly.Get())
			{
				Quitf("### command line 'heightmaponly' required, to generate navmesh tri files you must set HEIGHTMAP_GENERATE_NAVMESH_TRI_FILES to 1");
			}
#endif
		}
	}

	if (PARAM_exportregion.Get() || PARAM_exportsectors.Get() || PARAM_exportall.Get())
	{
		//if (PARAM_heightmaponly.Get() || PARAM_heightmapgen.Get())
		{
			PARAM_nopopups.Set(""); // TODO -- fix asserts
		}

		if (!PARAM_heightmaponly.Get())
		{
			fwLevelProcessToolImpl<CLevelProcessToolGameInterfaceGta, CNavMeshDataExporterTool> myApp;

			const u32 HEIGHTMAP_EXPORT_COLLISON_ARCHETYPES = (ArchetypeFlags::GTA_MAP_TYPE_MOVER | ArchetypeFlags::GTA_MAP_TYPE_WEAPON | ArchetypeFlags::GTA_OBJECT_TYPE | ArchetypeFlags::GTA_RIVER_TYPE | ArchetypeFlags::GTA_GLASS_TYPE);
			myApp.SetCollisionArchetypeFlags(HEIGHTMAP_EXPORT_COLLISON_ARCHETYPES);
			myApp.SetExportExteriorPortals(true);
			myApp.SetExportPolyDensities(true);

			myApp.Run();
		}
		else
		{
			//PF_INIT_TIMEBARS("Main", 1260, 99999999.9f);
			//PF_INIT_STARTUPBAR("Startup", 1260);

			//Set the maximum number of archives/packs that can be mounted by the game.
			rage::fiDevice::SetMaxMounts(MAX_ARCHIVES_MOUNTED);

			CFileMgr::Initialise();
			INIT_PARSER;
			CNavMeshDataExporterTool::InitConfig();

			// load water
			{
				const char* pLevelName = NULL;

				if (PARAM_level.Get(pLevelName))
				{
					Water::LoadWater(atVarString("common:/data/levels/%s/water.xml", pLevelName).c_str());
				}
				else
				{
					Quitf("no level specified");
				}
			}

			HeightMapGeneratorMain(true);

			CNavMeshDataExporterTool::ShutdownConfig();
			SHUTDOWN_PARSER;
		}
	}

#if __WIN32PC
	if (sysBootManager::IsDebuggerPresent())
	{
		system("pause"); // don't close the window yet, in case we want to see what got logged
	}

	// Hack to work around various shutdown problems, such as systems that assert or crash
	// just from static objects being created and then destroyed again.
	ExitProcess(0);
#else
	return 0;
#endif
}
