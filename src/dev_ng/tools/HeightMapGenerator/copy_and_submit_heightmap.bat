
call x:\gta5\tools\bin\setenv.bat

p4 sync %RS_PROJROOT%\build\dev\common\data\levels\gta5\heightmap.dat
p4 edit %RS_PROJROOT%\build\dev\common\data\levels\gta5\heightmap.dat

copy %RS_PROJROOT%\build\dev\common\data\debug\heightmap\heightmap_level.dat %RS_PROJROOT%\build\dev\common\data\levels\gta5\heightmap.dat

p4 submit -d "Automated Height Map check-in" %RS_PROJROOT%\build\dev\common\data\levels\gta5\heightmap.dat
