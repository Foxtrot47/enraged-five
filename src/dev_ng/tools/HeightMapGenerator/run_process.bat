echo off

	start "asset analysis tool"     %RS_CODEBRANCH%\tools\AssetAnalysisTool\AssetAnalysisTool.exe         -sync_in -sync_out -pause
	start "geometry collector tool" %RS_CODEBRANCH%\tools\GeometryCollectorTool\GeometryCollectorTool.exe -sync_in -sync_out -pause

	set COMMANDS=-exportall -exportscriptstaticbounds -exportFolder=X:/tris_heightmap -level=gta5 -nohangdetectthread -noaudiothread -nogameaudio -nobndwarn -noscaleformprealloc -nopopups -simplemapdata -aggressivecleanup -nulldriver -art_all=fatal -physics_all=fatal -modelinfo_all=fatal -Audio_NorthAudio_tty=fatal
	set TILECMDS=-sync_in -heightmaponly -exporttiles -autorender -autodelete %COMMANDS%

	start "gen tiles 1,1"         /WAIT /D%RS_BUILDBRANCH% heightmapgenerator.exe -res=1 -smp=1 %TILECMDS%
	start "gen tiles 2,1 (water)" /WAIT /D%RS_BUILDBRANCH% heightmapgenerator.exe -res=2 -smp=1 %TILECMDS% -wateronly
	start "gen tiles 4,5"         /WAIT /D%RS_BUILDBRANCH% heightmapgenerator.exe -res=4 -smp=5 %TILECMDS% 

	start "heightmap"             /WAIT /D%RS_BUILDBRANCH% heightmapgenerator.exe -sync_in -heightmaponly -includescriptstaticbounds %COMMANDS%

pause
