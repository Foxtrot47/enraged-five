echo off

set COMMANDS=-exportall -exportscriptstaticbounds -exportFolder=X:/tris_heightmap -level=gta5 -nohangdetectthread -noaudiothread -nogameaudio -nobndwarn -noscaleformprealloc -nopopups -simplemapdata -aggressivecleanup -nulldriver -art_all=fatal -physics_all=fatal -modelinfo_all=fatal -Audio_NorthAudio_tty=fatal

echo ###############################################################################################
echo ### GENERATING TRI FILES ######################################################################
echo ###############################################################################################

	start "gen tri files" /WAIT /Dx:\gta5\build\dev_ng heightmapgenerator.exe -logfile=c:/hm_log.txt %COMMANDS%

echo ###############################################################################################
echo ### DONE ######################################################################################
echo ###############################################################################################

pause
