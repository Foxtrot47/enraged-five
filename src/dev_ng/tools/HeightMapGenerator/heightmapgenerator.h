#ifndef _HEIGHTMAP_GENERATOR_H
#define _HEIGHTMAP_GENERATOR_H

// NOTE -- you must define this as 1 when generating tri files, 0 when generating the heightmap itself
// this is because the memory allocation is different between these two processes (i know, what a pain)
#define HEIGHTMAP_GENERATE_NAVMESH_TRI_FILES (1)

int HeightMapGeneratorMain(bool bInitConfig);

#endif	//_HEIGHTMAP_GENERATOR_H
