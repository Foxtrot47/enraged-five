echo off

	set COMMANDS=-exportall -exportscriptstaticbounds -exportFolder=X:/tris_heightmap -level=gta5 -nohangdetectthread -noaudiothread -nogameaudio -nobndwarn -noscaleformprealloc -nopopups -simplemapdata -aggressivecleanup -nulldriver -art_all=fatal -physics_all=fatal -modelinfo_all=fatal -Audio_NorthAudio_tty=fatal
	set TILECMDS=-heightmaponly -exporttiles -autorender -autodelete %COMMANDS%

	start "gen tiles 4,5"         /WAIT /Dx:\gta5\build\dev_ng heightmapgenerator.exe -res=4 -smp=5 %TILECMDS% 
	start "gen tiles 1,1"         /WAIT /Dx:\gta5\build\dev_ng heightmapgenerator.exe -res=1 -smp=1 %TILECMDS%
	start "gen tiles 2,1 (water)" /WAIT /Dx:\gta5\build\dev_ng heightmapgenerator.exe -res=2 -smp=1 %TILECMDS% -wateronly

pause
