// ======================
// heightmapgenerator.cpp
// ======================

#define HEIGHTMAP_OPTIMISATIONS_OFF() OPTIMISATIONS_OFF()

#ifndef NAVGEN_TOOL
#define NAVGEN_TOOL
#endif

#if 1 && __DEV
	HEIGHTMAP_OPTIMISATIONS_OFF()
	#pragma warning(disable:4748) // shut up.
#endif

#include <direct.h>
#include <time.h>

#include "ai/navmesh/navmeshextents.h"

#include "phcore/materialmgr.h"
#include "spatialdata/aabb.h"
#include "string/stringutil.h"
#include "system/bootmgr.h"
#include "system/param.h"

#include "fwgeovis/geovis.h"
#include "fwnavgen/NavGen.h"
#include "fwnavgen/NavMeshMaker.h"

#include "debug/AssetAnalysis/GeometryCollector.h"
#include "physics/gtaArchetype.h"
#include "physics/gtaMaterialManager.h"
#include "renderer/Water.h"
#include "scene/world/GameWorldHeightMap.h"

#include "config.h"
#include "exportcollision.h"
#include "heightmapgenerator.h"

#define GEOMS_PATH "x:/gta5/assets_ng/non_final/geometry"
#define HMAPS_PATH "x:/gta5/assets_ng/non_final/heightmap"
#define TOOLS_PATH "x:/gta5/src/dev_ng/tools"

PARAM(res, "resolution");
PARAM(smp, "downsample");

PARAM(exporttiles, ""); // export world mask as single tiles, which can be combined afterwards (allows for extremely high resolutions)
PARAM(underwater , ""); // flip triangles in z so that an underwater heightmap can be generated
PARAM(wateronly  , ""); // create water-related images only (for tiled mode)
PARAM(waterbounds, ""); // include water collision bounds in water heightmap - this is a TEMPORARY HACK for until we have all the water occluders and exclusion zones set up
PARAM(autorender , ""); // run HeightMapImageProcessor and WaterHeightImageProcessor after tiles have been rasterised
PARAM(autodelete , ""); // delete tiles after running processors
PARAM(autosubmit , "");
PARAM(densityex  , ""); // experimental density maps (only for tiled mode, otherwise takes up too much memory)

PARAM(includescriptstaticbounds,"");

XPARAM(exportregion);
XPARAM(exportall);

namespace rage
{
	extern float g_fMinimumVerticalDistanceBetweenNodes;
}

/*
// =============================================================================
// Here's a little maxima function which will give you the worldspace bounds for
// a given pixel rectangle on the high-res heightmap, assuming the image size
// is 2720x3840:

getbounds(boundsmin,boundsmax):=block
(
	[
		worldmin:[-4000,-4000],
		worldmax:[+4500,+8000],
		imagesize:[2720,3840],
		bmin,
		bmax
	],
	bmin:worldmin + (worldmax - worldmin)*[boundsmin[1],imagesize[2]-boundsmin[2]]/imagesize,
	bmax:worldmin + (worldmax - worldmin)*[boundsmax[1],imagesize[2]-boundsmax[2]]/imagesize,
	[
		[bmin[1],bmax[2]],
		[bmax[1],bmin[2]]
	]
)$

// e.g.:
//
//	getbounds([1319,1884],[1520,1989]),numer
//
// =============================================================================
*/

class ProgressDisplay // TODO -- move this class somewhere accessible by heightmapgenerator.cpp
{
public:
	ProgressDisplay(const char* format, ...) : m_progress(0.0f), m_numChars(0), m_clockStart(0), m_clockPrev(0), m_reset(false)
	{
		char temp[1024] = "";
		va_list args;
		va_start(args, format);
		vsnprintf(temp, sizeof(temp), format, args);
		va_end(args);

		sprintf(m_str, "%s .. ", temp);
		fprintf(stdout, m_str);
		fflush(stdout);

		Update(-1, 1);

		m_clockStart = clock();
		m_clockPrev = m_clockStart;

		m_remainingTimeEnabled = true;
		m_remainingTime = 0;
		m_remainingTimeStr[0] = '\0';
	}

	void UpdateInternal(float f, int index, int count, bool _bRedisplay_ = false)
	{
		if (m_reset)
		{
			fprintf(stdout, m_str);
			fflush(stdout);

			m_numChars = 0;
			m_reset = false;
		}

		const clock_t c = clock();

		m_progress = f;
		char temp[64] = "";

		if (count > 0)
		{
			sprintf(temp, "%.3f%% (%d of %d)", 100.0f*m_progress, index + 1, count);
		}
		else
		{
			sprintf(temp, "%.3f%%", 100.0f*m_progress);
		}

		Erase(m_numChars);
		fprintf(stdout, "%s", temp);
		fflush(stdout);
		m_numChars = (int)strlen(temp);

		if (m_remainingTimeEnabled)
		{
			const float elapsed = (float)(c - m_clockStart)/(float)CLOCKS_PER_SEC;

			if (elapsed > 5.0f && m_progress > 0.0f)
			{
				if (c - m_remainingTime > CLOCKS_PER_SEC) // update once per second
				{
					const float secs = elapsed*(1.0f - m_progress)/m_progress;
					const float mins = secs/60.0f;
					const float hrs  = mins/60.0f;

					if      (secs <= 100.0f) { sprintf(m_remainingTimeStr, " (%.2f secs remaining)", secs); }
					else if (mins <= 100.0f) { sprintf(m_remainingTimeStr, " (%.2f mins remaining)", mins); }
					else                     { sprintf(m_remainingTimeStr, " (%.2f hours remaining)", hrs); }

					m_remainingTime = c;
				}

				fprintf(stdout, m_remainingTimeStr);
				fflush(stdout);
				m_numChars += (int)strlen(m_remainingTimeStr);
			}
		}

		if (m_clockPrev && !_bRedisplay_) // if too much time elapsed, print warning
		{
			const float elapsedSinceLastUpdate = (float)(c - m_clockPrev)/(float)CLOCKS_PER_SEC;

			if (elapsedSinceLastUpdate > 20.0f)
			{
				fprintf(stdout, " -- Slow update (%.2f secs)\n", elapsedSinceLastUpdate);
				fprintf(stdout, "%s", m_str);
				fflush(stdout);

				m_numChars = 0;

				UpdateInternal(f, index, count, true);
			}
		}

		m_clockPrev = c;
	}

	void Update(int i, int n, bool bShowIndexAndCount = false)
	{
		UpdateInternal((float)(i + 1)/(float)n, i, bShowIndexAndCount ? n : 0);
	}

	template <typename T> void Update(int i, const T& v, bool bShowIndexAndCount = false)
	{
		UpdateInternal((float)(i + 1)/(float)v.size(), i, bShowIndexAndCount ? (int)v.size() : 0);
	}

	void Reset()
	{
		m_reset = true;
	}

	void End(const char* msg = "done", bool bShowTime = true)
	{
		Erase(m_numChars);

		fprintf(stdout, "%s.", msg);

		if (bShowTime)
		{
			char totalStr[64] = "";

			if (1) // show total
			{
				static clock_t total = 0;

				total += clock() - m_clockStart;

				const float totalSecs = (float)total/(float)CLOCKS_PER_SEC;
				const float totalMins = totalSecs/60.0f;
				const float totalHrs  = totalMins/60.0f;

				if      (totalSecs <=   1.0f) { sprintf(totalStr, ""); }
				else if (totalSecs <= 100.0f) { sprintf(totalStr, ", total %.2f secs", totalSecs); }
				else if (totalMins <= 100.0f) { sprintf(totalStr, ", total %.2f mins", totalMins); }
				else                          { sprintf(totalStr, ", total %.2f hours", totalHrs); }
			}

			const float secs = (float)(clock() - m_clockStart)/(float)CLOCKS_PER_SEC;
			const float mins = secs/60.0f;
			const float hrs  = mins/60.0f;

			if      (secs <=   1.0f) { fprintf(stdout, ""); }
			else if (secs <= 100.0f) { fprintf(stdout, " (%.2f secs%s)", secs, totalStr); }
			else if (mins <= 100.0f) { fprintf(stdout, " (%.2f mins%s)", mins, totalStr); }
			else                     { fprintf(stdout, " (%.2f hours%s)", hrs, totalStr); }
		}

		if (m_remainingTimeEnabled)
		{
			fprintf(stdout, "                             \n"); // extra space to clear leftover chars
		}
		else
		{
			fprintf(stdout, "    \n"); // extra space to clear leftover chars
		}
	}

protected:
	static void Erase(int numChars)
	{
		for (int k = 0; k < numChars; k++)
		{
			fprintf(stdout, "%c", 0x08);
		}

		fflush(stdout);
	}

	char    m_str[256];
	float   m_progress;
	int     m_numChars;
	clock_t m_clockStart;
	clock_t m_clockPrev;
	bool    m_reset;

	bool    m_remainingTimeEnabled;
	clock_t m_remainingTime;
	char    m_remainingTimeStr[64];
};

class CObjTriExporter
{
public:
	CObjTriExporter()
	{
		sysMemSet(this, 0, sizeof(*this));
	}

	void Open(const char* path)
	{
		m_stream = fiStream::Create(path);
	}

	void AddTri(Vec3V_In p0, Vec3V_In p1, Vec3V_In p2)
	{
		if (m_stream)
		{
			fprintf(m_stream, "v %f %f %f\n", VEC3V_ARGS(p0));
			fprintf(m_stream, "v %f %f %f\n", VEC3V_ARGS(p1));
			fprintf(m_stream, "v %f %f %f\n", VEC3V_ARGS(p2));
			m_numTris++;
		}
	}

	void Close(bool bOnlyPoints = false)
	{
		if (m_stream)
		{
			if (bOnlyPoints)
			{
				for (int i = 0; i < m_numTris; i++)
				{
					fprintf(m_stream, "p %d\n", 3 + 3*i);
					fprintf(m_stream, "p %d\n", 2 + 3*i);
					fprintf(m_stream, "p %d\n", 1 + 3*i);
				}
			}
			else // triangles
			{
				for (int i = 0; i < m_numTris; i++)
				{
					fprintf(m_stream, "f %d %d %d\n", 3 + 3*i, 2 + 3*i, 1 + 3*i);
				}
			}

			fprintf(m_stream, "# total verts = %d\n", m_numTris*3);
			fprintf(m_stream, "# total tris = %d\n", m_numTris);
			m_stream->Close();
			m_stream = NULL;
			m_numTris = 0;
		}
	}

	fiStream* m_stream;
	int       m_numTris;
};

static int GetIntegerParams(int* params, int paramCountMax, const char* pParamTxt) // returns number of params
{
	int paramCount = 0;
	char temp[256] = "";
	strcpy(temp, pParamTxt);
	const char* pParam = strtok(temp, ",\t ");

	while (pParam && paramCount < paramCountMax)
	{
		params[paramCount++] = atoi(pParam);
		pParam = strtok(NULL, ",\t ");
	}

	return paramCount;
}

static int GetFloatParams(float* params, int paramCountMax, const char* pParamTxt) // returns number of params
{
	int paramCount = 0;
	char temp[256] = "";
	strcpy(temp, pParamTxt);
	const char* pParam = strtok(temp, ",\t ");

	while (pParam && paramCount < paramCountMax)
	{
		params[paramCount++] = (float)atof(pParam);
		pParam = strtok(NULL, ",\t ");
	}

	return paramCount;
}

static ScalarV_Out CalcTriangleArea(Vec3V_In p0, Vec3V_In p1, Vec3V_In p2)
{
	const ScalarV a = Mag(p0 - p1);
	const ScalarV b = Mag(p1 - p2);
	const ScalarV c = Mag(p2 - p0);
	const ScalarV s = (a + b + c)*ScalarV(V_HALF);

	return Sqrt(s*(s - a)*(s - b)*(s - c));
}

static u8 CalcTriangleWeaponBoundDensity(Vec3V_In p0, Vec3V_In p1, Vec3V_In p2)
{
	const float minArea = 0.001f; // maps to zero density
//	const float midArea = 1.8f; // maps to 0.5 density
	const float maxArea = 4.61f; // maps to 1 density
	const float clampedArea = Clamp<float>(CalcTriangleArea(p0, p1, p2).Getf(), minArea, maxArea);
	const Vec3V v(clampedArea*clampedArea, clampedArea, 1.0f);
	const Vec3V quadraticConstants(-0.021698f, 0.317010f, -0.000317f);
	const float sm_TypeFlagDensityScaling_0 = 5.0f;

	const float density = Clamp<float>(1.0f - sm_TypeFlagDensityScaling_0*Dot(v, quadraticConstants).Getf(), 0.0f, 1.0f);

	return (u8)(0.5f + 255.0f*density);
}

/*
// ================================================================================================
What if we quantise triangles to 16-bit/component? We could reduce the tri file size by nearly half
if we quantise each navmesh (150x150 metres) to the full 0..65535 range, which would give a res
of about 0.23 centimetres. Accounting for triangles which hang off the navmesh .. the maximum
extent outside of the navmesh I've seen is <440, so we could quantise each 150+880 = 1030x1030 tile
which would give ~1.57 centimetre resolution.

Another idea - convert the tri file to an indexed ".itri" file (should reduce size by nearly 5/6).
// ================================================================================================
*/

#define TILE_MAP_EMPTY '.'
#define TILE_MAP_HOLES 'o'
#define TILE_MAP_OK    '_'

static int GetMaterialMaskIndex(const fwNavTriData& triData)
{
#if 1 // recalculate material mask index
	static int s_materials[256];
	static bool s_materialsInited = false;

	if (PGTAMATERIALMGR == NULL)
	{
		phMaterialMgrGta::Create();
		MATERIALMGR.Load();
	}

	if (!s_materialsInited)
	{
		for (int i = 0; i < 256; i++)
		{
			s_materials[i] = CGameWorldHeightMap::GetMaterialMaskIndex((phMaterialMgr::Id)i);
		}

		s_materialsInited = true;
	}

	if (triData.m_iMaterialId != phMaterialMgr::MATERIAL_NOT_FOUND &&
		triData.m_iMaterialId != phMaterialMgr::DEFAULT_MATERIAL_ID)
	{
		return s_materials[(u8)PGTAMATERIALMGR->UnpackMtlId(triData.m_iMaterialId)];
	}
	else
	{
		return -1;
	}
#else // use already calculated material mask index (might be wrong if the material list has changed)
	const u8 index = tri.m_colPolyData.m_triData.m_iMaterialMaskIndex;

	if (index != 0xff)
	{
		return (int)index;
	}
	else
	{
		return -1;
	}
#endif
}

int HeightMapGeneratorMain(bool bInitConfig)
{
	const clock_t startTime = clock();

	//**********************************************************
	// Get the config data, and set various vars to stop asserts
	//**********************************************************
	if (bInitConfig)
	{
		if (!rage::CNavMeshMaker::ParseParams(sysParam::GetArgCount(), sysParam::GetArgArray()))
		{
			return -1;
		}

		XPARAM(nocfg);
		if (PARAM_nocfg.Get())
		{
			fwNavGenConfigManagerImpl<CNavMeshGeneratorConfig>::InitClass(NULL);
		}

		// NOTE -- nav mesh config crashes, so i've hardcoded these values (they should not matter to the heightmap)
		//const CNavMeshGeneratorConfig& cfg = CNavMeshGeneratorConfig::Get();
		g_fSampleResolution = 1.0f;//cfg.m_SampleResolutionDefault;
		g_fMaxHeightChangeUnderNavMeshTriEdge = 0.4f;//cfg.m_MaxHeightChangeUnderTriangleEdge;
		g_fMinimumVerticalDistanceBetweenNodes = 2.0f;//cfg.m_MinimumVerticalDistanceBetweenNodes;
		//CNavGen::m_fTestClearHeightInTriangulation = 1.0f;//cfg.m_TestClearHeightInTriangulation;
		CPathServerExtents::m_iNumSectorsPerNavMesh = gv::WORLD_CELLS_PER_TILE;//CPathServer::GetNavMeshStore(kNavDomainRegular)->GetNumSectorsPerMesh();

		// Set the "mode" so that the tri files have the correct extension (is set by the -sample command line, which I no longer use.)
		// Needed by InitMeshFilenames()
		g_Mode = GenerateMeshes;
	}

	if (!g_InputPath) // if no input path, copy from export folder
	{
		const char* pExportFolder = NULL;
		XPARAM(exportFolder);
		if (PARAM_exportFolder.Get(pExportFolder))
		{
			g_InputPath = _strdup(pExportFolder);
		}
		else
		{
			Quitf("no input path or export folder");
		}
	}

	if (!g_OutputPath)
	{
		g_OutputPath = _strdup(g_InputPath);
	}

	Vec3V flip(V_ONE);

	if (PARAM_underwater.Get())
	{
		flip.SetZf(-1.0f);
	}

	const bool bCreateExperimentalDensityMaps = PARAM_densityex.Get();
	const bool bCreateDensityMap              = true;
	const bool bCreateWaterOnly               = PARAM_wateronly.Get();
	const bool bIncludeScriptStaticBounds     = PARAM_includescriptstaticbounds.Get();

	const float worldBoundsMinZ = (float)gv::WORLD_BOUNDS_MIN_Z; // lowest point on seabed floor (this doesn't work, i'd like to fix it sometime ..)
	const float worldBoundsMaxZ = (float)gv::WORLD_BOUNDS_MAX_Z;

	Vec3V* boundsMinPtr = NULL;
	Vec3V* boundsMaxPtr = NULL;
	Vec3V  boundsMin(V_ZERO);
	Vec3V  boundsMax(V_ZERO);

	const u32 iNumSectorsPerNavMesh = CPathServerExtents::m_iNumSectorsPerNavMesh;

	int x0 = 0;
	int y0 = 0;
	int x1 = CPathServerExtents::m_iNumNavMeshesInX;
	int y1 = CPathServerExtents::m_iNumNavMeshesInY;

	bool bTestingRegion = false;

	// TODO -- use bounds passed into fwExportCollisionGridTool::StartExportCollisionForSpecifiedWorldCoords
	const char* pExportRegion = NULL;

	float clipMinX = -FLT_MAX;
	float clipMinY = -FLT_MAX;
	float clipMaxX = +FLT_MAX;
	float clipMaxY = +FLT_MAX;

	if (PARAM_exportregion.Get(pExportRegion))
	{
		int params[4];
		int paramCount = GetIntegerParams(params, NELEM(params), pExportRegion);
		int iStartX = 0;
		int iStartY = 0;
		int iEndX = -1;
		int iEndY = -1;

		if (paramCount == 4)
		{
			iStartX = params[0];
			iStartY = params[1];
			iEndX = params[2];
			iEndY = params[3];
		}
		else if (paramCount == 3)
		{
			int iCentreX = params[0];
			int iCentreY = params[1];
			int iRadius = params[2];

			if (iRadius < 0) // TODO -- to make this feature compatible with exporting tri files, this code needs to be replicated in baseexportcollisiongrid.cpp
			{
				// pixel coordinates on 1-resolution heightmap (assuming -res=1)
				const float worldMinX = (float)gv::WORLD_BOUNDS_MIN_X;
				const float worldMinY = (float)gv::WORLD_BOUNDS_MIN_Y;
				const float worldMaxX = (float)gv::WORLD_BOUNDS_MAX_X;
				const float worldMaxY = (float)gv::WORLD_BOUNDS_MAX_Y;

				const float x = worldMinX + (worldMaxX - worldMinX)*(float)iCentreX/(float)((gv::WORLD_BOUNDS_MAX_X - gv::WORLD_BOUNDS_MIN_X)/2);
				const float y = worldMaxY + (worldMinY - worldMaxY)*(float)iCentreY/(float)((gv::WORLD_BOUNDS_MAX_Y - gv::WORLD_BOUNDS_MIN_Y)/2); // flip in y

				iCentreX = (int)x;
				iCentreY = (int)y;
				iRadius = -iRadius;
			}

			iStartX = iCentreX - iRadius;
			iStartY = iCentreY - iRadius;
			iEndX = iCentreX + iRadius;
			iEndY = iCentreY + iRadius;
		}

		if (iStartX <= iEndX)
		{
			clipMinX = (float)iStartX;
			clipMinY = (float)iStartY;
			clipMaxX = (float)iEndX;
			clipMaxY = (float)iEndY;

			const u32 iSectorStartX = (u32)Max<int>(0, CPathServerExtents::GetWorldToSectorX((float)iStartX));
			const u32 iSectorStartY = (u32)Max<int>(0, CPathServerExtents::GetWorldToSectorY((float)iStartY));
			const u32 iSectorEndX = (u32)Max<int>(0, CPathServerExtents::GetWorldToSectorXRoundUp((float)iEndX));
			const u32 iSectorEndY = (u32)Max<int>(0, CPathServerExtents::GetWorldToSectorYRoundUp((float)iEndY));

			x0 = (int)(iSectorStartX/iNumSectorsPerNavMesh); // round down
			y0 = (int)(iSectorStartY/iNumSectorsPerNavMesh);
			x1 = (int)(iSectorEndX + iNumSectorsPerNavMesh - 1)/iNumSectorsPerNavMesh; // round up
			y1 = (int)(iSectorEndY + iNumSectorsPerNavMesh - 1)/iNumSectorsPerNavMesh;

			boundsMin = Vec3V((float)iStartX, (float)iStartY, worldBoundsMinZ);
			boundsMax = Vec3V((float)iEndX, (float)iEndY, worldBoundsMaxZ);
			boundsMinPtr = &boundsMin;
			boundsMaxPtr = &boundsMax;

			bTestingRegion = true;
		}
	}
	else if (PARAM_exportall.Get()) // "exportall" doesn't really mean all for the heightmap, we only need the main map
	{
		boundsMin = Vec3V((float)gv::WORLD_BOUNDS_MIN_X, (float)gv::WORLD_BOUNDS_MIN_Y, worldBoundsMinZ);
		boundsMax = Vec3V((float)gv::WORLD_BOUNDS_MAX_X, (float)gv::WORLD_BOUNDS_MAX_Y, worldBoundsMaxZ);
		boundsMinPtr = &boundsMin;
		boundsMaxPtr = &boundsMax;

		x0 = (gv::WORLD_BOUNDS_MIN_X - gv::NAVMESH_BOUNDS_MIN_X)/gv::WORLD_TILE_SIZE;
		y0 = (gv::WORLD_BOUNDS_MIN_Y - gv::NAVMESH_BOUNDS_MIN_Y)/gv::WORLD_TILE_SIZE;
		x1 = (gv::WORLD_BOUNDS_MAX_X - gv::NAVMESH_BOUNDS_MIN_X)/gv::WORLD_TILE_SIZE;
		y1 = (gv::WORLD_BOUNDS_MAX_Y - gv::NAVMESH_BOUNDS_MIN_Y)/gv::WORLD_TILE_SIZE;
	}

	int iMaxTris = 0;//3*1024*1024; // this will get reallocated if necessary
	CNavGenTriForHeightMap* tris = NULL;//rage_new CNavGenTriForHeightMap[iMaxTris];
	atArray<spdAABB> excludeBounds;
	atArray<spdAABB> includeBounds; // extra quads inserted for interiors which need height map representation

	// don't rasterise triangles way up in the sky (there's a platform above the city at z=1000)
	{
		excludeBounds.PushAndGrow(spdAABB(Vec3V(-FLT_MAX, -FLT_MAX, 999.0f), Vec3V(V_FLT_MAX)));
	}

#if 0 && HACK_GTA4
	// don't rasterise blimp (this was for the static blimp, now the blimp is dynamic, this might not be necessary but it doesn't hurt ..)
	{
		const Vec3V blimpCentre(-725.74f, -992.48f, 304.50f);
		const ScalarV blimpRadius(50.0f); // actual radius is around 30
		excludeBounds.PushAndGrow(spdAABB(blimpCentre - Vec3V(blimpRadius), blimpCentre + Vec3V(blimpRadius)));
	}

	// don't rasterise pylon thingies out in the ocean ..
	{
		const Vec3V pylonMin(3450.0f, -4050.0f, -16000.0f);
		const Vec3V pylonMax(3950.0f, -3550.0f, +16000.0f);
		excludeBounds.PushAndGrow(spdAABB(pylonMin, pylonMax));
	}

	// add quad for v_31_tun_02
	{
		const Vec3V quadMin(-52.0f, -685.0f, 5.7f);
		const Vec3V quadMax(+75.0f, -580.0f, 5.7f);
		includeBounds.PushAndGrow(spdAABB(quadMin, quadMax));
	}
#endif // 0 && HACK_GTA4

	int resolution = 1;
	int downsample = 1;

	if (!PARAM_res.Get(resolution)) { resolution = 1; }
	if (!PARAM_smp.Get(downsample)) { downsample = 1; }

	// ============================================================================================
	if (PARAM_exporttiles.Get())
	// ============================================================================================
	{
		//
		{
			char dir[RAGE_MAX_PATH] = "";

			if (bCreateWaterOnly)
			{
				sprintf(dir, "tiles_%d_water", resolution);
			}
			else
			{
				sprintf(dir, "tiles_%d", resolution);
			}

			const atVarString tileMapFilePath(HEIGHTMAP_TOOL_DEBUG_OUTPUT_DIR"/%s/tiles.txt", dir);
			fiStream* tileMapFile = fiStream::Create(tileMapFilePath.c_str());

			if (Verifyf(tileMapFile, "failed to create \"%s\"", tileMapFilePath.c_str()))
			{
				atMap<u16,int> materialMap;
				atArray<u16> materials;

				if (!PARAM_wateronly.Get()) // scan tri files to generate list of material/procedural combo flags
				{
					ProgressDisplay progress("scanning for materials");

					atMap<u16,bool> materialSet;
					u8 allProcIds[32];
					sysMemSet(allProcIds, 0, sizeof(allProcIds));

					int matTriangleCount[64]; // for each material, how many triangles use it
					int matUniqueIdCount[64]; // for each material, how many unique procedurals (other than id=0)
					sysMemSet(matTriangleCount, 0, sizeof(matTriangleCount));
					sysMemSet(matUniqueIdCount, 0, sizeof(matUniqueIdCount));

					for (int y = y0; y < y1; y++)
					{
						for (int x = x0; x < x1; x++)
						{
							TNavMeshParams* pNavParams = rage::CNavMeshMaker::InitNavMeshParams(x*iNumSectorsPerNavMesh, y*iNumSectorsPerNavMesh, NULL);

							if (strstr(pNavParams->m_pFileName, ".tri"))
							{
								CNavGen navGen;
								const int count = navGen.LoadCollisionTrianglesForHeightMap(tris, iMaxTris, pNavParams->m_pFileName);

								if (count > 0)
								{
									// Whizz through the triangles
									for (int t = 0; t < count; t++)
									{
										const CNavGenTriForHeightMap& tri = tris[t];

										int materialId = 0;

										int matId  = GetMaterialMaskIndex(tri.m_colPolyData.m_triData);
										int procId = (int)tri.m_colPolyData.m_triData.m_iMaterialProcIndex;

										if (matId != -1)
										{
											materialId = (matId | (procId << 8));
										}
										else
										{
											materialId = -1;
										}

										if (materialId != -1 && AssertVerify(matId < NELEM(matTriangleCount)))
										{
											if (materialSet.Access((u16)materialId) == NULL)
											{
												if ((allProcIds[procId/8] & BIT(procId%8)) == 0)
												{
													allProcIds[procId/8] |= BIT(procId%8);
													materialSet[(u16)(materialId | 0x003f)] = true;
													materials.PushAndGrow((u16)(materialId | 0x003f));
												}

												materialSet[(u16)materialId] = true;
												materials.PushAndGrow((u16)materialId);

												if (tri.m_colPolyData.m_triData.m_iMaterialProcIndex != 0)
												{
													matUniqueIdCount[matId]++;
												}
											}

											matTriangleCount[matId]++;
										}
									}
								}

								// Clean up all the data
								navGen.Shutdown();
							}
						}

						progress.Update(y - y0, y1 - y0);
					}

					progress.End(atVarString("found %d unique material/procedural combos", materials.GetCount()).c_str());

					for (int i = 0; i < 64; i++)
					{
						if (matUniqueIdCount[i] > 0)
						{
							fprintf(stdout, "found %d triangles using material %s, with %d unique procedural ids\n", matTriangleCount[i], CGameWorldHeightMap::GetMaterialMaskList()[i], matUniqueIdCount[i]);
						}
						else if (matTriangleCount[i] > 0)
						{
							fprintf(stdout, "found %d triangles using material %s\n", matTriangleCount[i], CGameWorldHeightMap::GetMaterialMaskList()[i]);
						}
					}

					// don't bother registering mat/proc combo if the only procedural used by the material is 'empty'
					{
						atArray<u16> temp;

						for (int i = 0; i < materials.GetCount(); i++)
						{
							const int matId = (int)(materials[i] & 0x003f);

							if (matId == 0x003f || matUniqueIdCount[matId] > 0) 
							{
								temp.PushAndGrow(materials[i]);
							}
						}

						materials.CopyFrom(temp.GetElements(), (u16)temp.GetCount());
					}

					if (materials.GetCount() > 0)
					{
						class CompareMaterialIds { public: static int func(const void* pVoidA, const void* pVoidB)
						{
							const u16 a = *(const u16*)pVoidA;
							const u16 b = *(const u16*)pVoidB;

							const int matIdA = (int)(a & 0x003f);
							const int matIdB = (int)(b & 0x003f);
							const int procIdA = (int)(a >> 8);
							const int procIdB = (int)(b >> 8);

							// sort by material
							if (matIdA < matIdB) { return -1; }
							if (matIdA > matIdB) { return +1; }

							// sort by procedural
							if (procIdA < procIdB) { return -1; }
							if (procIdA > procIdB) { return +1; }

							return 0;
						}};
						qsort(&materials[0], materials.GetCount(), sizeof(u16), CompareMaterialIds::func);

						for (int i = 0; i < materials.GetCount(); i++)
						{
							materialMap[materials[i]] = i;
						}
					}
				}

				const int lineLength = 4 + x1 - x0 + 1;
				char* line = rage_new char[lineLength];

				memset(line, 0, lineLength);

				fprintf(tileMapFile, "tiles_x0=%d\n", x0*iNumSectorsPerNavMesh);
				fprintf(tileMapFile, "tiles_y0=%d\n", y0*iNumSectorsPerNavMesh);
				fprintf(tileMapFile, "tiles_x1=%d\n", x1*iNumSectorsPerNavMesh);
				fprintf(tileMapFile, "tiles_y1=%d\n", y1*iNumSectorsPerNavMesh);
				fprintf(tileMapFile, "downsamp=%d\n", downsample);

				ProgressDisplay progress("processing tiles x=[%d..%d], y=[%d..%d]", x0*iNumSectorsPerNavMesh, (x1 - 1)*iNumSectorsPerNavMesh, y0*iNumSectorsPerNavMesh, (y1 - 1)*iNumSectorsPerNavMesh);

				for (int y = y1 - 1; y >= y0; y--)
				{
					sprintf(line, "%03d:", y*iNumSectorsPerNavMesh);

					for (int x = x0; x < x1; x++)
					{
						int numWorldTris = 0;

						TNavMeshParams* pNavParams = rage::CNavMeshMaker::InitNavMeshParams(x*iNumSectorsPerNavMesh, y*iNumSectorsPerNavMesh, NULL);

						if (strstr(pNavParams->m_pFileName, ".tri"))
						{
							CNavGen navGen;
							const int count = navGen.LoadCollisionTrianglesForHeightMap(tris, iMaxTris, pNavParams->m_pFileName);

							if (count > 0)
							{
								// Whizz through the triangles
								for (int t = 0; t < count; t++)
								{
									const CNavGenTriForHeightMap& tri = tris[t];

									const bool bIsRiver          = tri.m_colPolyData.GetIsRiverBound();
									const bool bIsStairs         = tri.m_colPolyData.GetIsStairs();
									const bool bIsStairSlope     = (tri.m_colPolyData.m_triData.m_iArchetypeFlags & ArchetypeFlags::GTA_STAIR_SLOPE_TYPE) ? true : false;
									const bool bIsRoad           = tri.m_colPolyData.GetIsRoad();
									const bool bIsMoverBound     = tri.m_colPolyData.GetIsMoverBound();
									const bool bIsWeaponBound    = tri.m_colPolyData.GetIsWeaponBound();
									const bool bIsVehicleBound   = tri.m_colPolyData.GetIsVehicleBound();
									const bool bIsExteriorPortal = tri.m_colPolyData.GetIsExteriorPortal();
									const bool bIsInterior       = tri.m_colPolyData.GetIsInterior() && !bIsExteriorPortal;

									Vec3V p0 = tri.m_vPts[0];
									Vec3V p1 = tri.m_vPts[1];
									Vec3V p2 = tri.m_vPts[2];

									if (!IsFiniteStable(p0) ||
										!IsFiniteStable(p1) ||
										!IsFiniteStable(p2))
									{
										//__debugbreak();
										continue;
									}

									const spdAABB triangleBounds(Min(p0, p1, p2), Max(p0, p1, p2));

									// check for exclusion
									{
										bool bIsExcluded = false;

										for (int k = 0; k < excludeBounds.GetCount(); k++)
										{
											if (excludeBounds[k].ContainsAABB(triangleBounds))
											{
												bIsExcluded = true;
												break;
											}
										}

										if (bIsExcluded)
										{
											continue;
										}
									}

									const bool bAddToHeightMap = true;//(bAddPortalTriangles || !bIsExteriorPortal);

									if (bAddToHeightMap) 
									{
										if (numWorldTris == 0)
										{
											const bool bMasksOnly = false;

											CGameWorldHeightMap::ResetWorldHeightMapTiles(x, y, 1, 1, iNumSectorsPerNavMesh, resolution, downsample, bMasksOnly, bCreateDensityMap, bCreateExperimentalDensityMaps, bCreateWaterOnly, &worldBoundsMinZ, &materialMap, materials.GetCount());
											CGameWorldHeightMap::AddGeometryBegin();

											// add water drawable triangles etc. (we have to extract these from an OBJ file)
											{
												class ImportForeignData { public: static void func(std::vector<Vec3V>& data, u32 propGroupMask, u32 flags, int numSides, const char* path)
												{
													if (data.size() == 0)
													{
														FILE* obj = fopen(path, "r");

														if (obj)
														{
															char line[256] = "";

															while (fgets(line, sizeof(line), obj))
															{
																if (line[0] == '#')
																{
																	continue;
																}
																else if (line[0] == 'v')
																{
																	const char* s = line + strlen("v ");
																	float vertCoords[3];

																	vertCoords[0] = (float)atof(s); s = strchr(s, ' ') + 1;
																	vertCoords[1] = (float)atof(s); s = strchr(s, ' ') + 1;
																	vertCoords[2] = (float)atof(s);

																	data.push_back(Vec3V(vertCoords[0], vertCoords[1], vertCoords[2]));
																}
																else if (line[0] == 'f')
																{
																	break; // no more verts
																}
															}

															fclose(obj);
														}
													}

													for (int i = 0; i < (int)data.size(); i += numSides)
													{
														for (int j = 2; j < numSides; j++)
														{
															const Vec3V p0 = data[i + 0];
															const Vec3V p1 = data[i + j - 1];
															const Vec3V p2 = data[i + j];

															CGameWorldHeightMap::AddTriangleToWorldHeightMap(
																p0,
																p1,
																p2,
																flags,
																-1, // materialId
																propGroupMask,
																0, // pedDensity
																0, // moverBoundPolyDensity
																0, // moverBoundPrimDensity
																0, // weaponBoundPolyDensity
																0, // weaponBoundPrimDensity
																0, // boundID_rpf
																0, // boundId
																-1 // boundPrimitiveType
															);
														}
													}
												}};

												static std::vector<Vec3V> s_waterDrawableData;
												static std::vector<Vec3V> s_waterSurfacePortalData;
												static std::vector<Vec3V> s_waterNoReflectionData;
												static std::vector<Vec3V> s_mirrorDrawableData;
												static std::vector<Vec3V> s_mirrorSurfacePortalData;
												static std::vector<Vec3V> s_cableExteriorData; // TODO -- cable geometry is rather dense, we might need to split these into tiles
												static std::vector<Vec3V> s_cableInteriorData;
												static std::vector<Vec3V> s_coronaQuadData;
												static std::vector<Vec3V> s_animatedBuildingData;

												ImportForeignData::func(s_waterDrawableData,       0, GWHM_FLAG_WATER_DRAWABLE,        3, GEOMS_PATH"/processed/water/water_reflective_exterior.obj");
												ImportForeignData::func(s_waterSurfacePortalData,  0, GWHM_FLAG_WATER_SURFACE_PORTAL,  4, GEOMS_PATH"/collected/water/water_portals.obj");
												ImportForeignData::func(s_waterNoReflectionData,   0, GWHM_FLAG_WATER_NO_REFLECTION,   4, GEOMS_PATH"/collected/water/water_no_reflection.obj");
												ImportForeignData::func(s_mirrorDrawableData,      0, GWHM_FLAG_MIRROR_DRAWABLE,       3, GEOMS_PATH"/processed/mirror/mirror_all.obj");
												ImportForeignData::func(s_mirrorSurfacePortalData, 0, GWHM_FLAG_MIRROR_SURFACE_PORTAL, 4, GEOMS_PATH"/collected/mirror/mirror_portals.obj");
												ImportForeignData::func(s_cableExteriorData,       0, GWHM_FLAG_CABLE,                 3, GEOMS_PATH"/processed/cable/cable_exterior.obj");
												ImportForeignData::func(s_cableInteriorData,       0, GWHM_FLAG_CABLE,                 3, GEOMS_PATH"/processed/cable/cable_interior.obj");
												ImportForeignData::func(s_coronaQuadData,          0, GWHM_FLAG_CORONA_QUAD,           3, GEOMS_PATH"/processed/lights/coronas_fixed_size.obj");
												ImportForeignData::func(s_animatedBuildingData,    0, GWHM_FLAG_ANIMATED_BUILDING,     4, GEOMS_PATH"/collected/animated_buildings.obj");

												// lights
												{
													std::vector<Vec3V> lightExteriorData;
													std::vector<Vec3V> lightInteriorData;

													ImportForeignData::func(lightExteriorData, 0, GWHM_FLAG_LIGHT, 3, atVarString(GEOMS_PATH"/processed/lights/lights_exterior/tile_%s.obj", gv::GetTileNameFromCoords(x*iNumSectorsPerNavMesh, y*iNumSectorsPerNavMesh)).c_str());
													ImportForeignData::func(lightInteriorData, 0, GWHM_FLAG_LIGHT, 3, atVarString(GEOMS_PATH"/processed/lights/lights_interior/tile_%s.obj", gv::GetTileNameFromCoords(x*iNumSectorsPerNavMesh, y*iNumSectorsPerNavMesh)).c_str());
												}

												// propgroups
												{
													const atArray<GeomCollector::CPropGroup>& propGroups = GeomCollector::GetPropGroups();

													for (int propGroupIndex = 0; propGroupIndex < propGroups.GetCount(); propGroupIndex++)
													{
														const GeomCollector::CPropGroup& pg = propGroups[propGroupIndex];
														const u32 propGroupMask = BIT(propGroupIndex);
														std::vector<Vec3V> propGroupData;

														ImportForeignData::func(propGroupData, propGroupMask, GWHM_FLAG_MASK_ONLY, 3, atVarString(GEOMS_PATH"/processed/propgroup/%s/tile_%s.obj", pg.m_name.c_str(), gv::GetTileNameFromCoords(x*iNumSectorsPerNavMesh, y*iNumSectorsPerNavMesh)).c_str());
													}
												}
											}
										}

										p0 *= flip;
										p1 *= flip;
										p2 *= flip;

										const u8 pedDensity = (u8)(0.5f + 255.0f*(float)tri.m_colPolyData.m_iPedDensity/7.0f);
										const u8 moverBoundPolyDensity = bIsMoverBound ? tri.m_colPolyData.m_triData.m_iPolyDensity : 0;
										const u8 moverBoundPrimDensity = tri.m_colPolyData.GetPrimDensity_MOVER();
										const u8 weaponBoundPolyDensity = (bIsWeaponBound && tri.m_colPolyData.m_triData.m_iPolyDensity > 0) ? CalcTriangleWeaponBoundDensity(p0, p1, p2) : 0;
										const u8 weaponBoundPrimDensity = tri.m_colPolyData.GetPrimDensity_WEAPON();

										const bool bIsWaterOccluder = tri.m_colPolyData.m_triData.m_iFlagsStruct.m_bIsOccluder && (tri.m_colPolyData.m_triData.m_iFlagsStruct.m_iOccluderFlags & OccludeModel::FLAG_WATER_ONLY) != 0;
										const bool bIsNonBVHPrim = tri.m_colPolyData.m_triData.GetPrimitiveType() != phBound::BVH && tri.m_colPolyData.m_triData.GetPrimitiveType() != phBound::GEOMETRY;
									//	const bool bIsNonBVHPrim = tri.m_colPolyData.m_triData.m_iFlagsStruct.m_iBoundType == phBound::BVH;
										const bool bIsScript = tri.m_colPolyData.m_triData.m_iFlagsStruct.m_bIsScriptManaged;

										u32 flags = 0;

										if (bIsRiver)          { flags |= GWHM_FLAG_WATER_COLLISION; }
										if (bIsWaterOccluder)  { flags |= GWHM_FLAG_WATER_OCCLUDER; }
										if (bIsStairs)         { flags |= GWHM_FLAG_STAIRS; }
										if (bIsStairSlope)     { flags |= GWHM_FLAG_STAIRSLOPE; }
										if (bIsRoad)           { flags |= GWHM_FLAG_ROAD; }
										if (bIsScript)         { flags |= GWHM_FLAG_SCRIPT; }
										if (bIsMoverBound)     { flags |= GWHM_FLAG_MOVER_BOUND; }
										if (bIsWeaponBound)    { flags |= GWHM_FLAG_WEAPON_BOUND; }
										if (bIsVehicleBound)   { flags |= GWHM_FLAG_VEHICLE_BOUND; }
										if (bIsInterior)       { flags |= GWHM_FLAG_INTERIOR; }
										if (bIsExteriorPortal) { flags |= GWHM_FLAG_EXTERIOR_PORTAL; }
										if (bIsNonBVHPrim)     { flags |= GWHM_FLAG_NON_BVH_PRIMITIVE; }

										if (tri.m_colPolyData.m_triData.m_iFlagsStruct.m_bIsProp               ) { flags |= GWHM_FLAG_PROP; }
										if (tri.m_colPolyData.m_triData.m_iFlagsStruct.m_bContainsMoverBounds  ) { flags |= GWHM_FLAG_CONTAINS_MOVER_BOUNDS; }
										if (tri.m_colPolyData.m_triData.m_iFlagsStruct.m_bContainsWeaponBounds ) { flags |= GWHM_FLAG_CONTAINS_WEAPON_BOUNDS; }
										if (tri.m_colPolyData.m_triData.m_iFlagsStruct.m_bContainsVehicleBounds) { flags |= GWHM_FLAG_CONTAINS_VEHICLE_BOUNDS; }

										if (bIsScript && !bIncludeScriptStaticBounds)
										{
											flags = GWHM_FLAG_SCRIPT | GWHM_FLAG_MASK_ONLY;
										}

										int materialId = GetMaterialMaskIndex(tri.m_colPolyData.m_triData);

										if (materialId != -1)
										{
											materialId |= ((int)tri.m_colPolyData.m_triData.m_iMaterialProcIndex) << 8;
										}

										CGameWorldHeightMap::AddTriangleToWorldHeightMap(
											p0,
											p1,
											p2,
											flags,
											materialId,
											0, // propGroupMask
											pedDensity,
											moverBoundPolyDensity,
											moverBoundPrimDensity,
											weaponBoundPolyDensity,
											weaponBoundPrimDensity,
											tri.m_colPolyData.m_triData.m_iRegionHash,
											tri.m_colPolyData.m_triData.m_iBoundHash,
											tri.m_colPolyData.m_triData.GetPrimitiveType()
										);

										numWorldTris++;
									}
								}
							}

							// Clean up all the data
							navGen.Shutdown();
						}

						if (numWorldTris > 0)
						{
							CGameWorldHeightMap::AddGeometryEnd();

							if (!PARAM_underwater.Get())
							{
								atArray<Water::CWaterPoly> oceanPolys;
								Water::GetWaterPolys(oceanPolys);

								CGameWorldHeightMap::AddWaterGeometryBegin();

								for (int i = 0; i < oceanPolys.GetCount(); i++)
								{
									const Water::CWaterPoly& poly = oceanPolys[i];

									//if (poly.m_points[0].GetZf() != 0.0f)
									{
										Vec3V points[NELEM(poly.m_points)];
										memcpy(points, poly.m_points, sizeof(poly.m_points));

										for (int j = 2; j < poly.m_numPoints; j++)
										{
											CGameWorldHeightMap::AddTriangleToWorldHeightMap(
												points[0],
												points[j - 1],
												points[j],
												0 // flags
											);
										}
									}
								}

								CGameWorldHeightMap::AddWaterGeometryEnd();
							}

							int numEmptyWorldCells = 0;

							CGameWorldHeightMap::FinaliseWorldHeightMaps(false);
							CGameWorldHeightMap::SaveWorldHeightMapTile(dir, x, y, iNumSectorsPerNavMesh, &numEmptyWorldCells);

							if (numEmptyWorldCells > 0)
							{
								line[4 + x - x0] = TILE_MAP_HOLES;
							}
							else
							{
								line[4 + x - x0] = TILE_MAP_OK;
							}

							progress.Update(x - x0 + (y1 - 1 - y)*(x1 - x0), (x1 - x0)*(y1 - y0), true);
						}
						else
						{
							line[4 + x - x0] = TILE_MAP_EMPTY;
						}
					}

					fprintf(tileMapFile, "%s\n", line);
				}

				delete[] line;

				fprintf(tileMapFile, "materials=%d\n", materials.GetCount());

				for (int i = 0; i < materials.GetCount(); i++)
				{
					fprintf(tileMapFile, "%d\n", (int)materials[i]);
				}

				tileMapFile->Close();

				progress.End();
			}
		}
	}
	// ============================================================================================
	else // non-tiled mode, for generating in-game heightmap.dat file (and certain obj files)
	// ============================================================================================
	{
		// build a list of tri files
		{
			const int totalCount = (x1 - x0 + 1)*(y1 - y0 + 1);
			ProgressDisplay progress("finding tri files (%d)", totalCount);
			int progressIndex = 0;
			int maxTriangles = 0;

			for (int x = x0; x < x1; x++)
			{
				for (int y = y0; y < y1; y++)
				{
					TNavMeshParams* pNavParams = rage::CNavMeshMaker::InitNavMeshParams(x*iNumSectorsPerNavMesh, y*iNumSectorsPerNavMesh, NULL);

					if (strstr(pNavParams->m_pFileName, ".tri"))
					{
						fiStream* pStream = fiStream::Open(pNavParams->m_pFileName);

						if (pStream)
						{
							char header[8];
							pStream->Read(header, 8);

							u32 iReserved = 0;
							pStream->ReadInt(&iReserved, 1);

							u32 iNumTriangles = 0;
							pStream->ReadInt(&iNumTriangles, 1);

							maxTriangles = Max<int>((int)iNumTriangles, maxTriangles);

							pStream->Close();
							g_SourceNavMeshes.push_back(pNavParams);
						}
					}

					progressIndex++;
				}

				progress.Update(progressIndex - 1, totalCount);
			}

			progress.End(atVarString("max triangles = %d", maxTriangles).c_str());
		}

		CObjTriExporter objWorld;
		CObjTriExporter objStairs;
		CObjTriExporter objRiver;
		CObjTriExporter objMovNoVeh;
		CObjTriExporter objInterior;
		CObjTriExporter objOccluder;
		CObjTriExporter objOccluderWater;

		//
		{
			if (g_SourceNavMeshes.size() <= 64) // only create world obj if we're debugging a smallish area, otherwise this would contain ~61 million triangles
			{
				objWorld.Open(HEIGHTMAP_TOOL_DEBUG_OUTPUT_DIR"/world.obj");
			}

			if (0)
			{
				objStairs.Open(HEIGHTMAP_TOOL_DEBUG_OUTPUT_DIR"/stairs.obj");
			}

			if (1)
			{
				objRiver.Open(HEIGHTMAP_TOOL_DEBUG_OUTPUT_DIR"/river.obj");
			}

			if (1)
			{
				objMovNoVeh.Open(HEIGHTMAP_TOOL_DEBUG_OUTPUT_DIR"/movnoveh.obj");
			}

			if (1)
			{
				objInterior.Open(HEIGHTMAP_TOOL_DEBUG_OUTPUT_DIR"/interior.obj");
			}

			if (1)
			{
				objOccluder.Open(HEIGHTMAP_TOOL_DEBUG_OUTPUT_DIR"/occluder.obj");
				objOccluderWater.Open(HEIGHTMAP_TOOL_DEBUG_OUTPUT_DIR"/occluder_water.obj");
			}
		}

		//
		{
			const char* waterBoundaryPath = NULL;

			waterBoundaryPath = HEIGHTMAP_TOOL_DEBUG_OUTPUT_DIR"/waterboundary.eps";

			const bool bMasksOnly = false;
			const bool bHeightmapValid = true;

			CGameWorldHeightMap::ResetWorldHeightMaps(waterBoundaryPath, boundsMinPtr, boundsMaxPtr, resolution, downsample, bMasksOnly, false);

			int numWorldTrisMax = 0;
			int numWorldTrisTotal = 0;
			int numRiverTrisTotal = 0;

			// process meshes
			{
				ProgressDisplay progress("processing meshes");

				for (int m = 0; m < g_SourceNavMeshes.size(); m++)
				{
					const TNavMeshParams* pNavParams = g_SourceNavMeshes[m];
					CNavGen navGen;

					const int count = navGen.LoadCollisionTrianglesForHeightMap(tris, iMaxTris, pNavParams->m_pFileName);

					if (count > 0)
					{
						int numWorldTris = 0;
						int numRiverTris = 0;

						CGameWorldHeightMap::AddGeometryBegin();

						// Whizz through the triangles
						for (int t = 0; t < count; t++)
						{
							const CNavGenTriForHeightMap& tri = tris[t];

							const bool bIsRiver          = tri.m_colPolyData.GetIsRiverBound();
							const bool bIsStairs         = tri.m_colPolyData.GetIsStairs();
							const bool bIsStairSlope     = (tri.m_colPolyData.m_triData.m_iArchetypeFlags & ArchetypeFlags::GTA_STAIR_SLOPE_TYPE) ? true : false;
							const bool bIsRoad           = tri.m_colPolyData.GetIsRoad();
							const bool bIsMoverBound     = tri.m_colPolyData.GetIsMoverBound();
							const bool bIsWeaponBound    = tri.m_colPolyData.GetIsWeaponBound();
							const bool bIsVehicleBound   = tri.m_colPolyData.GetIsVehicleBound();
							const bool bIsExteriorPortal = tri.m_colPolyData.GetIsExteriorPortal();
							const bool bIsInterior       = tri.m_colPolyData.GetIsInterior() && !bIsExteriorPortal;

							Vec3V p0 = tri.m_vPts[0];
							Vec3V p1 = tri.m_vPts[1];
							Vec3V p2 = tri.m_vPts[2];

							if (!IsFiniteStable(p0) ||
								!IsFiniteStable(p1) ||
								!IsFiniteStable(p2))
							{
								//__debugbreak();
								continue;
							}

							spdAABB triangleBounds(Min(p0, p1, p2), Max(p0, p1, p2));

							if (tri.m_colPolyData.m_triData.m_iFlagsStruct.m_bIsOccluder)
							{
								if (tri.m_colPolyData.m_triData.m_iFlagsStruct.m_iOccluderFlags & OccludeModel::FLAG_WATER_ONLY)
								{
									objOccluderWater.AddTri(p0, p1, p2);
								}
								else
								{
									objOccluder.AddTri(p0, p1, p2);
								}

								continue;
							}

							// check for exclusion
							{
								bool bIsExcluded = false;

								for (int k = 0; k < excludeBounds.GetCount(); k++)
								{
									if (excludeBounds[k].ContainsAABB(triangleBounds))
									{
										bIsExcluded = true;
										break;
									}
								}

								if (bIsExcluded)
								{
									continue;
								}
							}

							const bool bAddToHeightMap = true;//(bAddPortalTriangles || !bIsExteriorPortal);

							p0 *= flip;
							p1 *= flip;
							p2 *= flip;

							if (bAddToHeightMap && bHeightmapValid) 
							{
								const u8 pedDensity = (u8)(0.5f + 255.0f*(float)tri.m_colPolyData.m_iPedDensity/7.0f);
								const u8 moverBoundPolyDensity = bIsMoverBound ? tri.m_colPolyData.m_triData.m_iPolyDensity : 0;
								const u8 moverBoundPrimDensity = tri.m_colPolyData.GetPrimDensity_MOVER();
								const u8 weaponBoundPolyDensity = (bIsWeaponBound && tri.m_colPolyData.m_triData.m_iPolyDensity > 0) ? CalcTriangleWeaponBoundDensity(p0, p1, p2) : 0;
								const u8 weaponBoundPrimDensity = tri.m_colPolyData.GetPrimDensity_WEAPON();

								const bool bIsWaterOccluder = tri.m_colPolyData.m_triData.m_iFlagsStruct.m_bIsOccluder && (tri.m_colPolyData.m_triData.m_iFlagsStruct.m_iOccluderFlags & OccludeModel::FLAG_WATER_ONLY) != 0;
								const bool bIsNonBVHPrim = tri.m_colPolyData.m_triData.GetPrimitiveType() != phBound::BVH && tri.m_colPolyData.m_triData.GetPrimitiveType() != phBound::GEOMETRY;
							//	const bool bIsNonBVHPrim = tri.m_colPolyData.m_triData.m_iFlagsStruct.m_iBoundType == phBound::BVH;
								const bool bIsScript = tri.m_colPolyData.m_triData.m_iFlagsStruct.m_bIsScriptManaged;

								u32 flags = 0;

								if (bIsRiver)          { flags |= GWHM_FLAG_WATER_COLLISION; }
								if (bIsWaterOccluder)  { flags |= GWHM_FLAG_WATER_OCCLUDER; }
								if (bIsStairs)         { flags |= GWHM_FLAG_STAIRS; }
								if (bIsStairSlope)     { flags |= GWHM_FLAG_STAIRSLOPE; }
								if (bIsRoad)           { flags |= GWHM_FLAG_ROAD; }
								if (bIsScript)         { flags |= GWHM_FLAG_SCRIPT; }
								if (bIsMoverBound)     { flags |= GWHM_FLAG_MOVER_BOUND; }
								if (bIsWeaponBound)    { flags |= GWHM_FLAG_WEAPON_BOUND; }
								if (bIsVehicleBound)   { flags |= GWHM_FLAG_VEHICLE_BOUND; }
								if (bIsInterior)       { flags |= GWHM_FLAG_INTERIOR; }
								if (bIsExteriorPortal) { flags |= GWHM_FLAG_EXTERIOR_PORTAL; }
								if (bIsNonBVHPrim)     { flags |= GWHM_FLAG_NON_BVH_PRIMITIVE; }

								if (tri.m_colPolyData.m_triData.m_iFlagsStruct.m_bIsProp               ) { flags |= GWHM_FLAG_PROP; }
								if (tri.m_colPolyData.m_triData.m_iFlagsStruct.m_bContainsMoverBounds  ) { flags |= GWHM_FLAG_CONTAINS_MOVER_BOUNDS; }
								if (tri.m_colPolyData.m_triData.m_iFlagsStruct.m_bContainsWeaponBounds ) { flags |= GWHM_FLAG_CONTAINS_WEAPON_BOUNDS; }
								if (tri.m_colPolyData.m_triData.m_iFlagsStruct.m_bContainsVehicleBounds) { flags |= GWHM_FLAG_CONTAINS_VEHICLE_BOUNDS; }

								if (bIsScript && !bIncludeScriptStaticBounds)
								{
									flags = GWHM_FLAG_SCRIPT | GWHM_FLAG_MASK_ONLY;
								}

								int materialId = GetMaterialMaskIndex(tri.m_colPolyData.m_triData);

								if (materialId != -1)
								{
									materialId |= ((int)tri.m_colPolyData.m_triData.m_iMaterialProcIndex) << 8;
								}

								CGameWorldHeightMap::AddTriangleToWorldHeightMap(
									p0,
									p1,
									p2,
									flags,
									materialId,
									0, // propGroupMask
									pedDensity,
									moverBoundPolyDensity,
									moverBoundPrimDensity,
									weaponBoundPolyDensity,
									weaponBoundPrimDensity,
									tri.m_colPolyData.m_triData.m_iRegionHash,
									tri.m_colPolyData.m_triData.m_iBoundHash,
									tri.m_colPolyData.m_triData.GetPrimitiveType()
								);
							}

							if (triangleBounds.GetMax().GetXf() > clipMinX &&
								triangleBounds.GetMax().GetYf() > clipMinY &&
								triangleBounds.GetMin().GetXf() < clipMaxX &&
								triangleBounds.GetMin().GetYf() < clipMaxY)// &&
								//tri.m_colPolyData.m_triData.m_iFlagsStruct.m_bIsOwnedByStaticBounds)
							{
								if (bIsStairs)
								{
									objStairs.AddTri(p0, p1, p2);
								}

								if (bIsRiver)
								{
									objRiver.AddTri(p0, p1, p2);
									numRiverTris++;
								}
								else if (bAddToHeightMap && !bIsExteriorPortal)
								{
									objWorld.AddTri(p0, p1, p2);
									numWorldTris++;
								}

								if (tri.m_colPolyData.m_triData.m_iFlagsStruct.m_bContainsMoverBounds && !tri.m_colPolyData.m_triData.m_iFlagsStruct.m_bContainsVehicleBounds && !tri.m_colPolyData.m_triData.m_iFlagsStruct.m_bIsProp)//bIsMoverBound && !bIsVehicleBound)
								{
									objMovNoVeh.AddTri(p0, p1, p2);
								}

								if (bIsInterior)
								{
									objInterior.AddTri(p0, p1, p2);
								}
							}
						}

						CGameWorldHeightMap::AddGeometryEnd();

						numWorldTrisMax = Max<int>(numWorldTris, numWorldTrisMax);
						numWorldTrisTotal += numWorldTris;
						numRiverTrisTotal += numRiverTris;
					}

					// Clean up all the data
					navGen.Shutdown();

					progress.Update(m, g_SourceNavMeshes, true);
				}

				progress.End();

				if (includeBounds.GetCount() > 0)
				{
					CGameWorldHeightMap::AddGeometryBegin();

					for (int i = 0; i < includeBounds.GetCount(); i++)
					{
						const Vec3V p00 = includeBounds[i].GetMin();
						const Vec3V p10 = GetFromTwo<Vec::X2,Vec::Y1,Vec::Z1>(includeBounds[i].GetMin(), includeBounds[i].GetMax());
						const Vec3V p01 = GetFromTwo<Vec::X1,Vec::Y2,Vec::Z1>(includeBounds[i].GetMin(), includeBounds[i].GetMax());
						const Vec3V p11 = includeBounds[i].GetMax();

						CGameWorldHeightMap::AddTriangleToWorldHeightMap(
							p00,
							p10,
							p11,
							GWHM_FLAG_EXTERIOR_PORTAL,
							-1,
							0, // propGroupMask
							0,//pedDensity,
							0,//moverBoundPolyDensity,
							0,//moverBoundPrimDensity,
							0,//weaponBoundPolyDensity,
							0,//weaponBoundPrimDensity,
							0,//tri.m_colPolyData.m_triData.m_iRegionHash,
							0,//tri.m_colPolyData.m_triData.m_iBoundHash,
							-1
						);

						CGameWorldHeightMap::AddTriangleToWorldHeightMap(
							p00,
							p11,
							p01,
							GWHM_FLAG_EXTERIOR_PORTAL,
							-1,
							0, // propGroupMask
							0,//pedDensity,
							0,//moverBoundPolyDensity,
							0,//moverBoundPrimDensity,
							0,//weaponBoundPolyDensity,
							0,//weaponBoundPrimDensity,
							0,//tri.m_colPolyData.m_triData.m_iRegionHash,
							0,//tri.m_colPolyData.m_triData.m_iBoundHash,
							-1
						);
					}

					CGameWorldHeightMap::AddGeometryEnd();
				}
			}

			if (1) // add quad representing expanded world bounds (so meshlab will open the obj file in the centre)
			{
				const float cex = (clipMaxX - clipMinX)*10.0f;
				const float cey = (clipMaxY - clipMinY)*10.0f;
				const float cx0 = clipMinX - cex;
				const float cy0 = clipMinY - cey;
				const float cx1 = clipMaxX + cex;
				const float cy1 = clipMaxY + cey;
				objWorld.AddTri(Vec3V(cx0, cy0, 0.0f), Vec3V(cx1, cy0, 0.0f), Vec3V(cx1, cy1, 0.0f));
				objWorld.AddTri(Vec3V(cx0, cy0, 0.0f), Vec3V(cx1, cy1, 0.0f), Vec3V(cx0, cy1, 0.0f));
			}

			objWorld.Close(false);
			objStairs.Close();
			objRiver.Close();
			objMovNoVeh.Close();
			objInterior.Close();
			objOccluder.Close();
			objOccluderWater.Close();

			fprintf(stdout, "total tris = %d (max=%d), total river = %d\n", numWorldTrisTotal, numWorldTrisMax, numRiverTrisTotal);
			fflush(stdout);

			//
			{
				if (!PARAM_underwater.Get())
				{
					atArray<Water::CWaterPoly> polys;
					Water::GetWaterPolys(polys);

					ProgressDisplay progress("rasterising %d water quads ... ", polys.GetCount());

					CGameWorldHeightMap::AddWaterGeometryBegin();

					for (int i = 0; i < polys.GetCount(); i++)
					{
						const Water::CWaterPoly& poly = polys[i];

						//if (poly.m_points[0].GetZf() != 0.0f)
						{
							Vec3V points[NELEM(poly.m_points)];
							memcpy(points, poly.m_points, sizeof(poly.m_points));

							for (int j = 2; j < poly.m_numPoints; j++)
							{
								CGameWorldHeightMap::AddTriangleToWorldHeightMap(
									points[0],
									points[j - 1],
									points[j],
									0 // flags
								);
							}
						}

						progress.Update(i, polys);
					}

					CGameWorldHeightMap::AddWaterGeometryEnd();

					progress.End();
				}

				CGameWorldHeightMap::FinaliseWorldHeightMaps(false);

				fprintf(stdout, "saving heightmaps ... ");
				fflush(stdout);
				{
					const bool bAutoImageRange = PARAM_underwater.Get() || bTestingRegion;
					const bool bIsUnderwater   = PARAM_underwater.Get();

					CGameWorldHeightMap::SaveWorldHeightMaps(bAutoImageRange, bIsUnderwater);
				}
				fprintf(stdout, "done.\n");
			}
		}

		for (int i = 0; i < g_SourceNavMeshes.size(); i++)
		{
			delete g_SourceNavMeshes[i];
		}

		g_SourceNavMeshes.clear();
	}

	if (PARAM_autorender.Get() && PARAM_exporttiles.Get())
	{
		char dir[RAGE_MAX_PATH] = "";
		sprintf(dir, HMAPS_PATH"/tiles_%d", resolution);

		if (bCreateWaterOnly)
		{
			strcat(dir, "_water");

			char cmd[1024] = "";
			sprintf(cmd, "%s %s%s", TOOLS_PATH"/WaterHeightImageProcessor/WaterHeightImageProcessor.exe", dir, sysBootManager::IsDebuggerPresent() ? " -pause" : "");
			fprintf(stdout, "\n\n> %s\n", cmd);
			system(cmd);
		}
		else
		{
			char cmd[1024] = "";
			sprintf(cmd, "%s %d%s", TOOLS_PATH"/HeightMapImageProcessor/HeightMapImageProcessor.exe", resolution, sysBootManager::IsDebuggerPresent() ? " -pause" : "");
			fprintf(stdout, "\n\n> %s\n", cmd);
			system(cmd);
		}

		if (PARAM_autodelete.Get())
		{
			char cmd[1024] = "";
			sprintf(cmd, "del %s/heightmap_tile_*", dir);
			for (char* s = cmd; *s; s++) { if (*s == '/') { *s = '\\'; }} // replace with backslashes
			fprintf(stdout, "\n\n> %s .. ", cmd);
			fflush(stdout);
			system(cmd);
			fprintf(stdout, "done.\n");
		}
	}

	if (PARAM_autosubmit.Get())
	{
		char cmd[1024] = "";
		sprintf(cmd, "%s", TOOLS_PATH"/HeightMapGenerator/copy_and_submit_heightmap_test.bat");
		fprintf(stdout, "\n\n> %s\n", cmd);
		system(cmd);
	}

	const float secs = (float)(clock() - startTime)/(float)CLOCKS_PER_SEC;
	const float mins = secs/60.0f;
	const float hrs  = mins/60.0f;

	if (!sysBootManager::IsDebuggerPresent())
	{
		fprintf(stdout, "%s ", sysParam::GetArg(0));
	}

	if      (secs <= 100) { fprintf(stdout, "done. (%.2f secs)\n", secs); }
	else if (mins <= 100) { fprintf(stdout, "done. (%.2f mins)\n", mins); }
	else                  { fprintf(stdout, "done. (%.2f hours)\n", hrs); }

	return 0;
}
