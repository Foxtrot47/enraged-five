<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
>
	<structdef type="CNavMeshGeneratorConfig" autoregister="true" base="fwNavGenConfig">
		<bool name="m_ExportVehicles" init="true"/>
	</structdef>

	<structdef type="CNavMeshDataDefaultIplGroups" autoregister="true">
		<array name="m_DefaultIplGroups" type="atArray">
			<string type="ConstString"/>
		</array>
	</structdef>

	<structdef type="CEntitySetDesc" autoregister="true">
		<Vector3 name="m_vPosition"/>
		<string name="m_InteriorName" type="ConstString"/>
		<string name="m_EntitySetName" type="ConstString"/>
	</structdef>

	<structdef type="CNavMeshDataDefaultEntitySets" autoregister="true">
		<array name="m_DefaultEntitySets" type="atArray">
			<struct type="CEntitySetDesc"/>
		</array>
	</structdef>

</ParserSchema>