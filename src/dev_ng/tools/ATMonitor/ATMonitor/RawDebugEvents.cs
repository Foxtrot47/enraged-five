﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System.Threading;
using System.Reflection;
using log4net;

namespace ATMonitor
{
    // Eventargs to contain information to send to the subscriber
    public class RawDebugEventsProgressArgs : EventArgs
    {
        public int AdditionValue { get; set; }

    }
    class RawDebugEvents
    {
        public static EventHandler<RawDebugEventsProgressArgs> ReportProgress;
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        static RawDebugEvents()
        {
            m_cq = new ConcurrentQueue<RawDebugEventObject>();
        }
        static private ConcurrentQueue<RawDebugEventObject> m_cq;
        static public ConcurrentQueue<RawDebugEventObject> GetConcurrentQueue() 
        {
            if (m_cq == null)
            {
                log.Debug("Concurrent Queue is null.");
                m_cq = new ConcurrentQueue<RawDebugEventObject>();   
            }
            return m_cq; 
        }
        static public void Enqueue(object obj)
        {
            String l_string = (String)obj;
            RawDebugEventObject p_item = new RawDebugEventObject(l_string);
            m_cq.Enqueue(p_item);
            if (ReportProgress != null)
            {
                ReportProgress(null, new RawDebugEventsProgressArgs { AdditionValue = 1 });
            }
            return;
        }

    }
}
