﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using log4net;

namespace ATMonitor
{
    class RawDebugEventObject
    {

        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        DateTime m_time;
        String m_string;

        public RawDebugEventObject(String p_string)
        {
            log.DebugFormat("Creating object of {0}", p_string);
            m_string = p_string;
            m_time = DateTime.Now;
        }

        public DateTime Time
        {
            get { return m_time; }
        }
        public String EventString
        {
            get { return m_string; }
        }
    }
}
