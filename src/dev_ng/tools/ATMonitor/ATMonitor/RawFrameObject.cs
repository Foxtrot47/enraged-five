﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Reflection;
using System.Collections.Concurrent;
namespace ATMonitor
{
    public class RawFrameObject
    {
        public RawFrameObject(float fps, float fr)
        {
            m_time = DateTime.Now;
            m_fps = fps;
            m_fr = fr;
        }
        public DateTime m_time;
        public float m_fps;
        public float m_fr;

        public override string ToString()
        {
            String s = "";
            s += "FPS : " + m_fps.ToString() + " ";
            s += "FR  : " + m_fr.ToString() + " ";
            s += "Date: " + m_time.ToString() + " ";
            return s;

        }
    }

    public class RawFrameQueue
    {
        static RawFrameQueue()
        {
            m_RawFrameObjects = new ConcurrentQueue<RawFrameObject>();
        }

        private static ConcurrentQueue<RawFrameObject> m_RawFrameObjects;
        public static ConcurrentQueue<RawFrameObject> Queue
        {
            get { return m_RawFrameObjects; }
        }
    }
    
}
