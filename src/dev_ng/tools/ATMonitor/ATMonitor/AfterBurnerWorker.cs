﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.ComponentModel;
using System.Reflection;
using System.Windows.Forms;
using MSI.Afterburner;
using MSI.Afterburner.Exceptions;
using log4net;

namespace ATMonitor
{
    class AfterBurnerWorker
    {

        public static System.ComponentModel.BackgroundWorker m_worker;
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        [DllImport("user32.dll")]
        static extern IntPtr GetForegroundWindow();
        [DllImport("user32.dll")]
        static extern int GetWindowText(IntPtr hWnd, StringBuilder text, int count);

        private static HardwareMonitor m_hwm;
        private static HardwareMonitorEntry m_fps;
        private static HardwareMonitorEntry m_fr;
        private static bool m_initialized;
        public static bool IsInitialized
        {
            get { return m_initialized; }
        }
        public static void Initialize()
        {
            
            try
            {
                log.Debug("Creating HardwareMonitor");
                m_hwm = new HardwareMonitor();

                log.Debug("Getting Frame Rate Entry");
                m_fps = m_hwm.GetEntry(HardwareMonitor.GPU_GLOBAL_INDEX,"Framerate");
                m_fr = m_hwm.GetEntry(HardwareMonitor.GPU_GLOBAL_INDEX, "Frametime");
                
                if (m_fps == null)
                {
                    log.Error("Framerate came back null.");
                }

                m_worker = new System.ComponentModel.BackgroundWorker();
                m_worker.DoWork +=
                    new DoWorkEventHandler(DoWork);
                m_worker.RunWorkerCompleted +=
                    new RunWorkerCompletedEventHandler(RunWorkerCompleted);
                m_worker.ProgressChanged +=
                    new ProgressChangedEventHandler(ProgressChanged);
                m_worker.WorkerSupportsCancellation = true;

                m_initialized = true;
                
            }
            catch
            {
                // Initializes the variables to pass to the MessageBox.Show method. 

                string message = "AfterBurner is not running or in paused state. Exiting.";
                string caption = "Warning";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult result;

                // Displays the MessageBox.

                result = MessageBox.Show(message, caption, buttons);
                Environment.Exit(Environment.ExitCode);
            }
        }
        public static void DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                log.Info("AfterBurnerWorker:DoWork");
                BackgroundWorker worker = sender as BackgroundWorker;
                log.Debug("Entering Loop");
                while (worker.CancellationPending == false)
                {
                    float fps = m_fps.Data;
                    float fr = m_fr.Data;
                    
                    const int nChars = 256;
                    StringBuilder Buff = new StringBuilder(nChars);
                    IntPtr handle = GetForegroundWindow();

                    if (GetWindowText(handle, Buff, nChars) > 0)
                    {
                        //log.Info(Buff.ToString());
                        // Now lets wait for our next second
                        String appName = Buff.ToString();
                        if (appName.Contains("Fuzzy") || appName.Contains("Grand Theft")  || appName.Contains("Launcher") || appName.Contains("Max Payne"))
                        {
                            if (fps > 120 && appName.Contains("Max Payne"))
                            {
                                fps = 120;
                            }
                            RawFrameObject frame = new RawFrameObject(fps, fr);
                            RawFrameQueue.Queue.Enqueue(frame);

                        }
                        //log.InfoFormat("Framerate:{0}", fps);

                    }
                    System.Threading.Thread.Sleep(100);
                    m_hwm.ReloadEntry(m_fr);
                    m_hwm.ReloadEntry(m_fps);
                }
            }
            catch (Exception me)
            {
                log.Error(me.Message);
            }

        }

        // This event handler deals with the results of the 
        // background operation. 
        public static void RunWorkerCompleted(
            object sender, RunWorkerCompletedEventArgs e)
        {
            log.Info("AfterBurnerWorker:RunWorkerCompleted");
        }

        // This event handler updates the progress bar. 
        public static void ProgressChanged(object sender,
            ProgressChangedEventArgs e)
        {
            log.InfoFormat("AfterBurnerWorker:ProgressChanged:{0}", e.ProgressPercentage);
            //this.progressBar1.Value = e.ProgressPercentage;
        }
    }
}
