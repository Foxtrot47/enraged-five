﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Reflection;
using log4net;
using log4net.Config;
public delegate void OnOutputDebugStringHandler(int pid, string text);
namespace ATMonitor
{

    // Eventargs to contain information to send to the subscriber
    public class CaptureDebugEventsProgressArgs : EventArgs
    {
        public int AdditionValue { get; set; }

    }
    class CaptureDebugEventsWorker
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        public static EventHandler<CaptureDebugEventsProgressArgs> ReportProgress;
        public static System.ComponentModel.BackgroundWorker m_worker;
        private static bool m_initialized;
        public static bool IsInitialized
        {
            get { return m_initialized; }
        }
        #region Win32 API Imports

        [StructLayout(LayoutKind.Sequential)]
        private struct SECURITY_DESCRIPTOR
        {
            public byte revision;
            public byte size;
            public short control;
            public IntPtr owner;
            public IntPtr group;
            public IntPtr sacl;
            public IntPtr dacl;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct SECURITY_ATTRIBUTES
        {
            public int nLength;
            public IntPtr lpSecurityDescriptor;
            public int bInheritHandle;
        }

        [Flags]
        private enum PageProtection : uint
        {
            NoAccess = 0x01,
            Readonly = 0x02,
            ReadWrite = 0x04,
            WriteCopy = 0x08,
            Execute = 0x10,
            ExecuteRead = 0x20,
            ExecuteReadWrite = 0x40,
            ExecuteWriteCopy = 0x80,
            Guard = 0x100,
            NoCache = 0x200,
            WriteCombine = 0x400,
        }


        private const int WAIT_OBJECT_0 = 0;
        private const int WAIT_TIMEOUT = 0x102;
        private const uint INFINITE = 0xFFFFFFFF;
        private const int ERROR_ALREADY_EXISTS = 183;
        private const uint SECURITY_DESCRIPTOR_REVISION = 1;
        private const uint SECTION_MAP_READ = 0x0004;
        private const String GUARD_SIGNIFIER = "_00";
        private const int UPDATING_THRESHOLD = 10000;

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern IntPtr MapViewOfFile(IntPtr hFileMappingObject, uint
            dwDesiredAccess, uint dwFileOffsetHigh, uint dwFileOffsetLow,
            uint dwNumberOfBytesToMap);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool UnmapViewOfFile(IntPtr lpBaseAddress);

        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool InitializeSecurityDescriptor(ref SECURITY_DESCRIPTOR sd, uint dwRevision);

        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool SetSecurityDescriptorDacl(ref SECURITY_DESCRIPTOR sd, bool daclPresent, IntPtr dacl, bool daclDefaulted);

        [DllImport("kernel32.dll")]
        private static extern IntPtr CreateEvent(ref SECURITY_ATTRIBUTES sa, bool bManualReset, bool bInitialState, string lpName);

        [DllImport("kernel32.dll")]
        private static extern bool PulseEvent(IntPtr hEvent);

        [DllImport("kernel32.dll")]
        private static extern bool SetEvent(IntPtr hEvent);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern IntPtr CreateFileMapping(IntPtr hFile,
            ref SECURITY_ATTRIBUTES lpFileMappingAttributes, PageProtection flProtect, uint dwMaximumSizeHigh,
            uint dwMaximumSizeLow, string lpName);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool CloseHandle(IntPtr hHandle);

        [DllImport("kernel32", SetLastError = true, ExactSpelling = true)]
        private static extern Int32 WaitForSingleObject(IntPtr handle, uint milliseconds);

        //[DllImport("kernel32", SetLastError = true, ExactSpelling = true)]
        //private static extern Int32 WaitForMultipleObjects(Int16 nCount, IntPtr* lpHandles, bool bWaitAll, IntPtr dwMilliseconds);
        #endregion


        private static IntPtr m_AckEvent = IntPtr.Zero;

        private static IntPtr m_ReadyEvent = IntPtr.Zero;

        private static IntPtr m_SharedFile = IntPtr.Zero;

        private static IntPtr m_SharedMem = IntPtr.Zero;


        private static object m_SyncRoot = new object();

        private static Mutex m_Mutex = null;

        private static bool m_isCapturing = false;

        

        public static bool Capturing
        {
            get { return m_isCapturing; }
        }

        static public void DoWork(object sender, DoWorkEventArgs e)
        {

            BackgroundWorker worker = sender as BackgroundWorker;

            InitializeCapture();
            try
            {
                IntPtr pString = new IntPtr(
                    m_SharedMem.ToInt32() + Marshal.SizeOf(typeof(int))
                );

                m_isCapturing = true;
                Int32 numQueued = 0;
                int ret = 0;
                while (worker.CancellationPending == false)
                {
                    //log.Debug("Entering loop");
                    if (ret != WAIT_TIMEOUT)
                    {
                        SetEvent(m_AckEvent);
                    }
                    
                    ret = WaitForSingleObject(m_ReadyEvent, 1);
                    
                    if (ret == WAIT_OBJECT_0)
                    {
                        string text = Marshal.PtrToStringAnsi(new IntPtr(m_SharedMem.ToInt32() + Marshal.SizeOf(typeof(int)))).TrimEnd(null);
                        if (string.IsNullOrEmpty(text))
                            continue;
                        String trimmedText = text.Trim();
                        if (trimmedText.Contains(GUARD_SIGNIFIER))
                        {
                            ThreadPool.QueueUserWorkItem(RawDebugEvents.Enqueue, text);
                            m_worker.ReportProgress(0);
                        }
                        numQueued++;
                    }
                }

            }
            catch(Exception me)
            {
                log.Error(me.Message);
            }
            finally
            {
                Dispose();
            }
        }

        static public void InitializeCapture()
        {
            lock (m_SyncRoot)
            {

                if (Environment.OSVersion.ToString().IndexOf("Microsoft") == -1)
                    throw new NotSupportedException("This DebugMonitor is only supported on Microsoft operating systems.");

                bool createdNew = false;
                m_Mutex = new Mutex(false, typeof(CaptureDebugEventsWorker).Namespace, out createdNew);
                if (!createdNew)
                    throw new ApplicationException("There is already an instance of 'DbMon.NET' running.");

                SECURITY_DESCRIPTOR sd = new SECURITY_DESCRIPTOR();

                if (!InitializeSecurityDescriptor(ref sd, SECURITY_DESCRIPTOR_REVISION))
                {
                    throw new ApplicationException("Failed to initializes the security descriptor.");
                }

                if (!SetSecurityDescriptorDacl(ref sd, true, IntPtr.Zero, false))
                {
                    throw new ApplicationException("Failed to initializes the security descriptor");
                }

                SECURITY_ATTRIBUTES sa = new SECURITY_ATTRIBUTES();

                m_AckEvent = CreateEvent(ref sa, false, false, "DBWIN_BUFFER_READY");
                if (m_AckEvent == IntPtr.Zero)
                {
                    throw new ApplicationException("Failed to create event 'DBWIN_BUFFER_READY'");
                }

                m_ReadyEvent = CreateEvent(ref sa, false, false, "DBWIN_DATA_READY");
                if (m_ReadyEvent == IntPtr.Zero)
                {
                    throw new ApplicationException("Failed to create event 'DBWIN_DATA_READY'");
                }

                m_SharedFile = CreateFileMapping(new IntPtr(-1), ref sa, PageProtection.ReadWrite, 0, 4096, "DBWIN_BUFFER");
                if (m_SharedFile == IntPtr.Zero)
                {
                    throw new ApplicationException("Failed to create a file mapping to slot 'DBWIN_BUFFER'");
                }

                m_SharedMem = MapViewOfFile(m_SharedFile, SECTION_MAP_READ, 0, 0, 512);
                if (m_SharedMem == IntPtr.Zero)
                {
                    throw new ApplicationException("Failed to create a mapping view for slot 'DBWIN_BUFFER'");
                }
            }
        }
        static public void RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            log.Info("CaptureDebugEventsWorker:RunWorkerCompleted");
        }

        static public void ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (ReportProgress != null)
            {
                ReportProgress(null, new CaptureDebugEventsProgressArgs { AdditionValue = 1 });
            }
        }

        static public void Initialize()
        {
            try
            {
                m_worker = new System.ComponentModel.BackgroundWorker();
                m_worker.DoWork +=
                    new DoWorkEventHandler(DoWork);
                m_worker.RunWorkerCompleted +=
                    new RunWorkerCompletedEventHandler(RunWorkerCompleted);
                m_worker.ProgressChanged +=
                    new ProgressChangedEventHandler(ProgressChanged);
                m_worker.WorkerSupportsCancellation = true;
                m_worker.WorkerReportsProgress = true;
                m_initialized = true;
            }
            catch (Exception me)
            {
                log.Error(me.Message);
            }
        }

        private static void Dispose()
        {
            if (m_AckEvent != IntPtr.Zero)
            {
                if (!CloseHandle(m_AckEvent))
                {
                    throw new ApplicationException("Failed to close handle for 'AckEvent'");
                }
                m_AckEvent = IntPtr.Zero;
            }

            if (m_ReadyEvent != IntPtr.Zero)
            {
                if (!CloseHandle(m_ReadyEvent))
                {
                    throw new ApplicationException("Failed to close handle for 'ReadyEvent'");
                }
                m_ReadyEvent = IntPtr.Zero;
            }

            if (m_SharedFile != IntPtr.Zero)
            {
                if (!CloseHandle(m_SharedFile))
                {
                    throw new ApplicationException("Failed to close handle for 'SharedFile'");
                }
                m_SharedFile = IntPtr.Zero;
            }


            if (m_SharedMem != IntPtr.Zero)
            {
                if (!UnmapViewOfFile(m_SharedMem))
                {
                    throw new ApplicationException("Failed to unmap view for slot 'DBWIN_BUFFER'");
                }
                m_SharedMem = IntPtr.Zero;
            }

            if (m_Mutex != null)
            {
                m_Mutex.Close();
                m_Mutex = null;
            }
        }

        public static void Stop()
        {
            lock (m_SyncRoot)
            {
                PulseEvent(m_ReadyEvent);
                while (m_AckEvent != IntPtr.Zero)
                    ;
            }
        }
    }
}
