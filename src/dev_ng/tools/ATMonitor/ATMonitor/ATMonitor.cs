﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Reflection;
using System.Collections.Concurrent;
using System.IO;
using System.Windows.Forms.DataVisualization.Charting;
using System.Runtime.InteropServices;
using System.Configuration;
using log4net;
using log4net.Config;
using MSI.Afterburner;
using MSI.Afterburner.Exceptions;
namespace ATMonitor
{
    public partial class ATMonitor : Form
    {

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        public static extern void OutputDebugString(string message);
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private static Int64 m_debugEventsCount;
        private static Int64 m_debugEventsProcessedCount;
        private static Int64 m_debugEventsAnalyzedCount;
        private static DateTime m_startTime;
        private static DateTime m_stopTime;
        private static Int32 m_fpsGraphYMax;
        private static Int32 m_frGraphYMax;

        //private static bool m_clickingTrackBar;
        public ATMonitor()
        {
            InitializeComponent();
            XmlConfigurator.Configure(); //only once
            log.Info("Starting ATMonitor");

            // Initialize each of my workers
            AfterBurnerWorker.Initialize();
            FrameWorker.Initialize();
            CaptureDebugEventsWorker.Initialize();
            AnalyzeDebugEventsWorker.Initialize();

            // Subscribe to updates so I know when to draw a new graph
            FrameWorker.ReportProgress += new EventHandler<FrameProgressArgs>(FrameChart_ReportProgress);
            GuardTracking.ReportProgress += new EventHandler<GuardTrackingProgressArgs>(GuardTracking_ReportProgress);
            CaptureDebugEventsWorker.ReportProgress += new EventHandler<CaptureDebugEventsProgressArgs>(CaptureDebugEvents_ReportProgress);
            RawDebugEvents.ReportProgress += new EventHandler<RawDebugEventsProgressArgs>(RawDebugEvents_ReportProgress);
            //AnalyzeDebugEventsWorker.ReportProgress += new EventHandler<AnalyzeDebugEventsProgressArgs>(AnalyzeDebugEvents_ReportProgress);

            m_debugEventsCount = 0;
            m_debugEventsProcessedCount = 0;
            m_debugEventsAnalyzedCount = 0;

            m_debugEventsCountLabel.Text = "";
            m_debugEventsProcessedLabel.Text = "";
            m_startScaleTrackBar.Enabled = false;
            m_stopScaleTrackBar.Enabled = false;
            m_clearButton.Enabled = false;
            m_analyzeDebugEventsButton.Enabled = false;

            CreateFpsChart();
            CreateFramerateChart();
            
           
        }

        public void CreateFpsChart()
        {
            // Create my chart
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            chartArea1.Name = "ChartArea1";
            this.m_fpsChart.ChartAreas.Add(chartArea1);
            var s = new Series();
            s.ChartType = SeriesChartType.FastLine;
            s.Name = "FPS";
            m_fpsChart.Series.Add(s);
            m_fpsChart.Series[0].XValueType = ChartValueType.DateTime;
            m_fpsChart.ChartAreas[0].AxisX.LabelStyle.Format = "HH:mm:ss";
            m_fpsChart.ChartAreas[0].AxisX.Interval = 60;
            m_fpsChart.ChartAreas[0].AxisX.IntervalType = DateTimeIntervalType.Seconds;
            m_fpsChart.ChartAreas[0].AxisX.ScaleView.Zoomable = true;
            m_fpsChart.ChartAreas[0].AxisX.Minimum = DateTime.Now.ToOADate();
            m_fpsChart.ChartAreas[0].AxisY.Maximum = 60;
            m_fpsChart.ChartAreas[0].AxisY.Interval = 5;
            m_fpsChart.ChartAreas[0].CursorX.LineColor = Color.Black;
            m_fpsChart.ChartAreas[0].CursorX.LineWidth = 1;
            m_fpsChart.ChartAreas[0].CursorX.LineDashStyle = ChartDashStyle.Dot;
            m_fpsChart.ChartAreas[0].CursorX.Interval = 10;
            m_fpsChart.Series[0].XValueType = ChartValueType.DateTime;
            m_fpsChart.Series[0].YValueType = ChartValueType.Double;
            m_fpsGraphYMax = (int)m_fpsChart.ChartAreas[0].AxisY.Maximum;
        }
        public void CreateFramerateChart()
        {
            // FR chart
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            chartArea2.Name = "ChartArea1";
            this.m_frChart.ChartAreas.Add(chartArea2);
            var s = new Series();
            s.ChartType = SeriesChartType.FastLine;
            s.Name = "Framerate";
            m_frChart.Series.Add(s);
            m_frChart.Series[0].XValueType = ChartValueType.DateTime;
            m_frChart.ChartAreas[0].AxisX.LabelStyle.Format = "HH:mm:ss";
            m_frChart.ChartAreas[0].AxisX.Interval = 60;
            m_frChart.ChartAreas[0].AxisX.IntervalType = DateTimeIntervalType.Seconds;
            m_frChart.ChartAreas[0].AxisX.ScaleView.Zoomable = true;
            m_frChart.ChartAreas[0].AxisX.Minimum = DateTime.Now.ToOADate();
            m_frChart.ChartAreas[0].AxisY.Maximum = 60;
            m_frChart.ChartAreas[0].AxisY.Interval= 10;
            m_frChart.ChartAreas[0].CursorX.LineColor = Color.Black;
            m_frChart.ChartAreas[0].CursorX.LineWidth = 1;
            m_frChart.ChartAreas[0].CursorX.LineDashStyle = ChartDashStyle.Dot;
            m_frChart.ChartAreas[0].CursorX.Interval = 0;
            m_frChart.Series[0].XValueType = ChartValueType.DateTime;
            m_frChart.Series[0].YValueType = ChartValueType.Double;
            m_frGraphYMax = (int)m_frChart.ChartAreas[0].AxisY.Maximum;
        }

        // Here is the code responsible for updating the checkbox list.
        // I wouldn't think a delegate would be necessary, but for some reason it is.
        public delegate void AddToGuardDisplayCheckboxListDelegate(GuardTrackingProgressArgs args);
        public void AddToGuardDisplayCheckboxList(GuardTrackingProgressArgs args)
        {
            m_guardDisplayCheckboxList.Items.Add(args.Message);
            string value = ConfigurationManager.AppSettings["trusted"];
            if (value != null && value.Equals("true"))
            {
                log.InfoFormat("{0} gets resolved to {1}", args.Message, GuardTracking.CalculateHash(args.Message));
            }
            return;
        }
        public void GuardTracking_ReportProgress(object sender, GuardTrackingProgressArgs args)
        {
            if (args.MessageType == GuardTrackingProgressType.AdditionEvent)
            {
                m_guardDisplayCheckboxList.Invoke(new AddToGuardDisplayCheckboxListDelegate(AddToGuardDisplayCheckboxList), new Object[] { args });
            }
            else if (args.MessageType == GuardTrackingProgressType.ProcessedEvent)
            {
                m_guardDisplayCheckboxList.Invoke(new PerformProgressStepDelegate(PerformProgressStep), new Object[] { args });
            }
        }

        // Code to update our FPS graph.
        public void FrameChart_ReportProgress(object sender, FrameProgressArgs args)
        {
            
            RawFrameObject obj = (RawFrameObject)args.RFOData;
            log.DebugFormat("Plotting {0}", obj);
            m_fpsChart.Series[0].Points.AddXY(obj.m_time.ToOADate(), System.Convert.ToDouble(obj.m_fps));
            m_frChart.Series[0].Points.AddXY(obj.m_time.ToOADate(), System.Convert.ToDouble(obj.m_fr));
            // Lets check to be sure that we haven't exceeded our max. If so, set it to be the max and add 
            // ten percent so we have some give.

            if (m_fpsChart.ChartAreas[0].AxisY.Maximum < obj.m_fps)
            {
                // Multiply by 1.1 to add 10%
                m_fpsChart.ChartAreas[0].AxisY.Maximum = (int)((double)obj.m_fps * 1.1);
                m_fpsGraphYMax = (int)m_fpsChart.ChartAreas[0].AxisY.Maximum;
            }

            if (m_frChart.ChartAreas[0].AxisY.Maximum < obj.m_fr)
            {
                // Multiply by 1.1 to add 10%
                m_frChart.ChartAreas[0].AxisY.Maximum = (int)((double)obj.m_fr * 1.1);
                m_frGraphYMax = (int)m_fpsChart.ChartAreas[0].AxisY.Maximum;
            }

            TabPage current = m_graphTabs.SelectedTab;
            if (current.Name.Equals("m_fpsTabPage"))
            {
                m_verticalTrackBar.Maximum = m_fpsGraphYMax;
                m_verticalTrackBar.Value = m_verticalTrackBar.Maximum;
            }
            else if (current.Name.Equals("m_frTabPage"))
            {
                m_verticalTrackBar.Maximum = m_frGraphYMax;
                m_verticalTrackBar.Value = m_verticalTrackBar.Maximum;
            }

            m_fpsChart.Update();
            m_frChart.Update();
        }


        // Report our analysis engine
        // Report how many debug events we've captured
        public delegate void PerformProgressStepDelegate(GuardTrackingProgressArgs args);
        public void PerformProgressStep(GuardTrackingProgressArgs args)
        {
            // First add to our count
            m_debugEventsAnalyzedCount += 1;
            // Now lets update our progress bar, but for that we need to calculate percentage

            double pect = ((double)m_debugEventsAnalyzedCount / (double)m_debugEventsCount) * 100;
            if((int)pect > m_analyzedProgressBar.Value)
                m_analyzedProgressBar.PerformStep();
        }

        public void AnalyzeDebugEvents_ReportProgress(object sender, AnalyzeDebugEventsProgressArgs args)
        {
            try
            {
                m_analyzedProgressBar.Invoke(new PerformProgressStepDelegate(PerformProgressStep),
                    new Object[] { args });
            }
            catch(Exception me)
            {
                log.Error(me.Message);
            }
        }

        // Report how many debug events we've captured
        public void CaptureDebugEvents_ReportProgress(object sender, CaptureDebugEventsProgressArgs args)
        {
            if (m_debugEventsCountLabel.Text.Length > 0)
                m_debugEventsCountLabel.Text.Remove(0);
            m_debugEventsCount += args.AdditionValue;
            m_debugEventsCountLabel.Text = " / " + m_debugEventsCount.ToString();

        }

        // Report how many debug events we've captured
        public delegate void AddRawDebugEventsCounterDelegate(RawDebugEventsProgressArgs args);
        public void AddRawDebugEventsCounter(RawDebugEventsProgressArgs args)
        {
            if (m_debugEventsProcessedLabel.Text.Length > 0)
                m_debugEventsProcessedLabel.Text.Remove(0);
            m_debugEventsProcessedCount += args.AdditionValue;
            m_debugEventsProcessedLabel.Text = m_debugEventsProcessedCount.ToString();
        }

        public void RawDebugEvents_ReportProgress(object sender, RawDebugEventsProgressArgs args)
        {
            try
            {
                m_debugEventsProcessedLabel.Invoke(new AddRawDebugEventsCounterDelegate(AddRawDebugEventsCounter),
                    new Object[] { args });
            }
            catch(Exception me)
            {
                log.Error(me.Message);
            }
        }

        private void m_captureButton_Click(object sender, EventArgs e)
        {

            if (AfterBurnerWorker.m_worker.IsBusy != true
                && FrameWorker.m_worker.IsBusy != true
                && CaptureDebugEventsWorker.m_worker.IsBusy != true
                //&& ProcessDebugEventsWorker.m_worker.IsBusy != true
                )
            {
                m_startTime = DateTime.Now;
                m_captureButton.Text = "Stop";
                log.Debug("Running async operations");

                // Start the asynchronous operation.
                AfterBurnerWorker.m_worker.RunWorkerAsync();
                FrameWorker.m_worker.RunWorkerAsync();
                CaptureDebugEventsWorker.m_worker.RunWorkerAsync();
                m_debugPlotter.Start();
                m_startScaleTrackBar.Enabled = false;
                m_stopScaleTrackBar.Enabled = false;
                m_clearButton.Enabled = false;
            }
            else
            {

                try
                {

                    m_stopTime = DateTime.Now;
                    log.DebugFormat("Business Detected {0}:{1}:{2}", AfterBurnerWorker.m_worker.IsBusy.ToString(),
                        FrameWorker.m_worker.IsBusy.ToString(),
                        CaptureDebugEventsWorker.m_worker.IsBusy.ToString());
                    m_captureButton.Text = "Start";

                    //m_captureButton.Enabled = false;
                    m_clearButton.Enabled = true;
                    m_analyzeDebugEventsButton.Enabled = true;
                    // Cancel the asynchronous operation.
                    AfterBurnerWorker.m_worker.CancelAsync();
                    FrameWorker.m_worker.CancelAsync();
                    CaptureDebugEventsWorker.m_worker.CancelAsync();
                    // ProcessDebugEventsWorker.m_worker.CancelAsync();
                    // I need to fire a debug event to free the lock in
                    // CaptureDebugEventsWorker

                    //log.Debug("Sending closing ODS");
                    OutputDebugString("283E91EB85D58FFC2AD922884B644C41B54140660F6D1FDA3C403D543EEDD839");
                    m_debugPlotter.Stop();
                    m_startScaleTrackBar.Maximum = Convert.ToInt32(m_stopTime.Subtract(m_startTime).TotalSeconds);
                    m_startScaleTrackBar.Enabled = true;
                    m_stopScaleTrackBar.Maximum = Convert.ToInt32(m_stopTime.Subtract(m_startTime).TotalSeconds);
                    m_stopScaleTrackBar.Value = Convert.ToInt32(m_stopTime.Subtract(m_startTime).TotalSeconds);
                    m_stopScaleTrackBar.Enabled = true;
                    m_fpsChart.ChartAreas[0].AxisX.Maximum = m_stopTime.ToOADate();

                }
                catch (Exception me)
                {
                    log.Error(me.Message);
                }
            }
        }

        Point? m_chartPrevPosition = null;
        ToolTip m_chartTooltip = new ToolTip();

        public int BinarySearch<TItem, TSearch>(IList<TItem> list, TSearch value, Func<TSearch, TItem, int> comparer)
        {
            int lower = 0;
            int upper = list.Count - 1;

            while (lower <= upper)
            {
                int middle = lower + (upper - lower) / 2;
                int comparisonResult = comparer(value, list[middle]);

                if (comparisonResult < 0)
                {
                    upper = middle - 1;
                }
                else if (comparisonResult > 0)
                {
                    lower = middle + 1;
                }
                else
                {
                    return middle;
                }
            }

            return ~lower;
        }

        private int nearestPreceedingValueInChart(Chart _chart, double x)
        {
            var bpData = _chart.Series[0].Points;
            int bpIndex = BinarySearch(bpData, x, (xVal, point) => Math.Sign(x - point.XValue));

            if (bpIndex < 0)
            {
                bpIndex = ~bpIndex;                // BinarySearch() returns the index of the next element LARGER than the target.
                bpIndex = Math.Max(0, bpIndex - 1);  // We want the value of the previous element, so we must decrement the returned index.
            }                                      // If this is before the start of the graph, use the first valid data point.

            return bpIndex;
        }

        void m_fpsChart_MouseMove(object sender, MouseEventArgs e)
        {

            //try
            {

                Chart selChart = (Chart)sender;
                if (selChart.Series[0].Points.Count == 0)
                    return;
                var pos = e.Location;
                if (m_chartPrevPosition.HasValue && pos == m_chartPrevPosition.Value)
                    return;
                m_chartTooltip.RemoveAll();
                m_chartPrevPosition = pos;
                var results = selChart.HitTest(pos.X, pos.Y, false, ChartElementType.PlottingArea);
                foreach (var result in results)
                {
                    if (result.ChartElementType == ChartElementType.PlottingArea)
                    {
                        double xVal = result.ChartArea.AxisX.PixelPositionToValue(pos.X);
                        double yVal = selChart.Series[0].Points[nearestPreceedingValueInChart(selChart, xVal)].YValues[0];

                        //float yVal = (float)result.ChartArea.AxisY.PixelPositionToValue(pos.Y);

                        if (!double.IsNaN(yVal))
                        {
                            m_chartTooltip.Show("X=" + DateTime.FromOADate(xVal).ToString("HH:mm:ss:fff") + ", Y=" + yVal, selChart,
                                     pos.X, pos.Y - 15);

                        }
                    }
                }


                /**
                 * Okay so this solution works if I want to trace along the line, but that's boring and not useful.
                 */
                //HitTestResult result = selChart.HitTest(e.X, e.Y);
                //if (result.ChartElementType == ChartElementType.DataPoint && result.PointIndex != -1)
                //{
                //    var xValaue = selChart.Series[0].Points[result.PointIndex].XValue;
                //    var yValaue = selChart.Series[0].Points[result.PointIndex].YValues[0];
                //    m_chartTooltip.Show("X=" + DateTime.FromOADate(xValaue).ToString("HH:mm:ss:fff") + ", Y=" + yValaue, selChart,
                //             pos.X, pos.Y - 15);
                //}

                // Now lets create a vertical bar
                Point mousePoint = new Point(e.X, e.Y);
                selChart.ChartAreas[0].CursorX.SetCursorPixelPosition(mousePoint, true);

            }
            //catch (Exception me)
            //{
            //    log.Error(me.Message);
            //}
        }


        private void m_guardDisplayCheckboxList_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            // My E has the index of the element, which stores the 
            // name of the guard we care about
            String guardName = (String)m_guardDisplayCheckboxList.Items[e.Index];

            // First lets see if we're being checked, or unchecked
            if (e.NewValue == CheckState.Checked)
            {
                // Now that we've got our guard name, lets get the data associated with it.
                HashSet<DateTime> times = GuardTracking.GuardsByName[guardName].TimesOfRuns;

                //// Creating the new series
                var s = new Series();
                s.ChartType = SeriesChartType.FastPoint;
                s.Name = guardName;
                s.LegendText = guardName;

                double yAxisLocation = m_fpsChart.Series.Count * 2 * -1;

                foreach (DateTime time in times)
                {
                    s.Points.AddXY(time.ToOADate(), yAxisLocation);
                }

                m_fpsChart.Series.Add(s);
                m_frChart.Series.Add(s);
            }
            else
            {
                // In this scenario, we want to delete the series that we've added
                int seriesIndex = m_fpsChart.Series.IndexOf(guardName);
                m_fpsChart.Series.RemoveAt(seriesIndex);
                m_frChart.Series.RemoveAt(seriesIndex);
            }

        }

        private void m_fpsChart_MouseLeave(object sender, EventArgs e)
        {
            Chart selChart = (Chart)sender;
            if (selChart.Focused)
                selChart.Parent.Focus();
        }

        private void m_fpsChart_MouseEnter(object sender, EventArgs e)
        {
            Chart selChart = (Chart)sender;
            if (!selChart.Focused)
                selChart.Focus();
        }

        private void m_selectAll_Click(object sender, EventArgs e)
        {

            for (int i = 0; i < m_guardDisplayCheckboxList.Items.Count; i++)
            {
                m_guardDisplayCheckboxList.SetItemCheckState(i, CheckState.Checked);
            }
        }

        private void m_analyzeDebugEventsButton_Click(object sender, EventArgs e)
        {
            if (AnalyzeDebugEventsWorker.m_worker.IsBusy != true)
            {
                try
                {
                    
                    AnalyzeDebugEventsWorker.m_worker.RunWorkerAsync();
                    m_analyzeDebugEventsButton.Enabled = false;
                    
                }
                catch (Exception me)
                {
                    log.Error(me.Message);
                }
            }
            else
            {
                AnalyzeDebugEventsWorker.m_worker.CancelAsync();
            }
        }

        private void m_selectNone_Click(object sender, EventArgs e)
        {
            GenerateHashes();
            for (int i = 0; i < m_guardDisplayCheckboxList.Items.Count; i++)
            {
                m_guardDisplayCheckboxList.SetItemCheckState(i, CheckState.Unchecked);
            }
        }

        private void GenerateHashes()
        {
            string value = ConfigurationManager.AppSettings["trusted"];
            if (value != null && value.Equals("true"))
            {

                string line;

                // Read the file and display it line by line.
                System.IO.StreamReader file =
                   new System.IO.StreamReader("X:\\gta5\\tools_ng\\script\\coding\\protection\\gtav_pc.gsml");
                string sPattern =  "guard_cmd name";
                while ((line = file.ReadLine()) != null)
                {
                    if (System.Text.RegularExpressions.Regex.IsMatch(line, sPattern, System.Text.RegularExpressions.RegexOptions.IgnoreCase))
                    {
                        string lcp = line;
                        lcp = lcp.Replace("<guard_cmd name=\"", "");
                        lcp = lcp.Replace("\">", "");
                        lcp = lcp.Trim();
                        log.DebugFormat("Match for {0} pattern found in {1} - turns into {2} from {3}", sPattern, line, GuardTracking.CalculateHash(lcp), lcp);
                    }
                    
                }

                file.Close();

            }
        }
        private void ResetCharts()
        {

            log.Debug("Clearing");
            int i = 0;
            while (m_fpsChart.Series.Count > i)
            {
                log.DebugFormat("Removing Series {0}", m_fpsChart.Series[i].Name);
                try
                {
                    m_fpsChart.Series.RemoveAt(i);
                }
                catch (Exception me)
                {
                    log.Error(me.Message);
                }
            }
            while (m_frChart.Series.Count > i)
            {
                log.DebugFormat("Removing Series {0}", m_frChart.Series[i].Name);
                try
                {
                    m_frChart.Series.RemoveAt(i);
                }
                catch (Exception me)
                {
                    log.Error(me.Message);
                }
            }

            m_fpsChart.ChartAreas.RemoveAt(0);
            m_frChart.ChartAreas.RemoveAt(0);
        }
        private void m_clearButton_Click(object sender, EventArgs e)
        {
            ResetCharts();
            CreateFpsChart();
            CreateFramerateChart();

            m_guardDisplayCheckboxList.Items.Clear();
            m_debugEventsProcessedCount = 0;
            m_debugEventsCount = 0;
            m_debugEventsProcessedCount = 0;
            m_debugEventsAnalyzedCount = 0;
            if (m_debugEventsProcessedLabel.Text.Length > 0)
                m_debugEventsProcessedLabel.Text.Remove(0);
            if (m_debugEventsCountLabel.Text.Length > 0)
                m_debugEventsCountLabel.Text.Remove(0);
            m_debugEventsCountLabel.Text = "";
            m_debugEventsProcessedLabel.Text = "";
            m_analyzedProgressBar.Value = 0;
            GuardTracking.Reset();
            m_captureButton.Enabled = true;
            m_clearButton.Enabled = false;
            m_analyzeDebugEventsButton.Enabled = false;

            // Lets set our trackbar locations
            m_startScaleTrackBar.Value = m_startScaleTrackBar.Minimum;
            m_stopScaleTrackBar.Value = m_stopScaleTrackBar.Maximum;

            try
            {
                AnalyzeDebugEventsWorker.m_worker.CancelAsync();
            }
            catch (Exception me)
            {
                log.Error(me.Message);
            }
        }

        private void m_startScaleTrackBar_Scroll(object sender, EventArgs e)
        {
            // Get our minimum
            DateTime changeMin = m_startTime.AddSeconds(m_startScaleTrackBar.Value);
            DateTime changeMax = m_startTime.AddSeconds(m_stopScaleTrackBar.Value);
            // We actually want to compare to the stop scale track bar
            if (changeMin.CompareTo(changeMax) < 0)
            {
                m_fpsChart.ChartAreas[0].AxisX.Minimum = changeMin.ToOADate();
                m_frChart.ChartAreas[0].AxisX.Minimum = changeMin.ToOADate();
            }
        }

        private void m_stopScaleTrackBar_Scroll(object sender, EventArgs e)
        {
            // Get our minimum
            DateTime changeMin = m_startTime.AddSeconds(m_startScaleTrackBar.Value);
            DateTime changeMax = m_startTime.AddSeconds(m_stopScaleTrackBar.Value);
            // We actually want to compare to the stop scale track bar
            if (changeMax.CompareTo(changeMin) > 0)
            {
                m_fpsChart.ChartAreas[0].AxisX.Maximum = changeMax.ToOADate();
                m_frChart.ChartAreas[0].AxisX.Maximum = changeMax.ToOADate();
            }
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void m_graphTabs_SelectedIndexChanged(object sender, EventArgs e)
        {
            TabPage current = (sender as TabControl).SelectedTab;

            if (current.Name.Equals("m_fpsTabPage"))
            {
                m_verticalTrackBar.Maximum = m_fpsGraphYMax;
                m_fpsChart.ChartAreas[0].AxisY.Maximum = (double)m_fpsGraphYMax;
            }
            else if (current.Name.Equals("m_frTabPage"))
            {
                m_verticalTrackBar.Maximum = m_frGraphYMax;
                m_frChart.ChartAreas[0].AxisY.Maximum = (double)m_frGraphYMax;
            }
            
            // First lets set the max for our vertical track bar
            m_verticalTrackBar.Value = m_verticalTrackBar.Maximum;
        }


        private void m_verticalTrackBar_Scroll(object sender, EventArgs e)
        {
            // Get our minimum
            TabPage current = m_graphTabs.SelectedTab;
            double newYMax = 0;
            Chart refChart = null;
            if (current.Name.Equals("m_fpsTabPage"))
            {
                log.DebugFormat("{0} {1} {2}", m_fpsGraphYMax, m_verticalTrackBar.Value, m_verticalTrackBar.TickFrequency);
                newYMax = /*m_fpsGraphYMax - */ m_verticalTrackBar.Value;
                refChart = m_fpsChart;
            }
            else if (current.Name.Equals("m_frTabPage"))
            {
                log.DebugFormat("{0} {1} {2}", m_frGraphYMax, m_verticalTrackBar.Value, m_verticalTrackBar.TickFrequency);
                newYMax = /*m_frGraphYMax - */ m_verticalTrackBar.Value;
                refChart = m_frChart;
            }
            else
            {
                throw new Exception("Tab page wasn't handled correctly in the scroll");
            }

            log.DebugFormat("Setting new Y max to : {0}", newYMax);
            refChart.ChartAreas[0].AxisY.Maximum = newYMax;
        }

        private void m_saveButton_Click(object sender, EventArgs e)
        {
            string path = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            path += "\\Rockstar Games\\ATMonitor\\";
            string timestamped = path +m_startTime.ToString("yyyyMMddHHmmss");
            Directory.CreateDirectory(path);
            m_fpsChart.SaveImage(timestamped + ".fps.png", ChartImageFormat.Png);
            m_frChart.SaveImage(timestamped + ".fr.png", ChartImageFormat.Png);
            MessageBox.Show("Saved Images to " + path, "Images Saved!", MessageBoxButtons.OK);


            // FPS at the guard's location
            //double yVal = selChart.Series[0].Points[nearestPreceedingValue(selChart, xVal)].YValues[0];

            // First lets write out the simple series
            StreamWriter writer = new StreamWriter(timestamped + ".fps.csv");
            writer.Write("TIME,FPS\n");
            foreach (DataPoint point in m_fpsChart.Series[0].Points)
            {
                writer.Write(DateTime.FromOADate(point.XValue).ToString("HH:mm:ss:fff") + ","  + point.YValues[0] + "\n");
            }
            writer.Close();

            // Now our framerate
            writer = new StreamWriter(timestamped + ".fr.csv");
            writer.Write("TIME,FR\n");
            foreach (DataPoint point in m_frChart.Series[0].Points)
            {
                writer.Write(DateTime.FromOADate(point.XValue).ToString("HH:mm:ss:fff") + "," + point.YValues[0] + "\n");
            }
            writer.Close();

            // Now all of our guards
            
        }
    }
}
