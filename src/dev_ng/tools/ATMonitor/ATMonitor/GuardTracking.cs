﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Reflection;
using log4net;
namespace ATMonitor
{

    // Eventargs to contain information to send to the subscriber
    public enum GuardTrackingProgressType
    {
        AdditionEvent = 1, 
        RunEvent, 
        ProcessedEvent
    }
    public class GuardTrackingProgressArgs : EventArgs
    {
        public String Message { get; set; }
        public GuardTrackingProgressType MessageType  { get; set;}

    }
    class GuardTracking
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static EventHandler<GuardTrackingProgressArgs> ReportProgress;
        static GuardTracking()
        {
            m_guardsEachMillisecond = new Dictionary<DateTime, HashSet<String>>();
            m_guardsInOrder = new List<String>();
            m_guardsByName = new Dictionary<String, GuardObject>();
        }
        static private Dictionary<DateTime, HashSet<String>> m_guardsEachMillisecond;
        static private List<String> m_guardsInOrder;
        static private Dictionary<String, GuardObject> m_guardsByName;

        static public Dictionary<DateTime, HashSet<String>> GuardsEachSecond
        {
            get 
            { 
                return m_guardsEachMillisecond; 
            }
        }
        static public List<String> GuardsInOrder
        {
            get { return m_guardsInOrder; }
        }
        static public Dictionary<String, GuardObject> GuardsByName
        {
            get { return m_guardsByName; }
        }

        public  static DateTime Truncate(DateTime dateTime, TimeSpan timeSpan)
        {
            if (timeSpan == TimeSpan.Zero) return dateTime; // Or could throw an ArgumentException
            return dateTime.AddTicks(-(dateTime.Ticks % timeSpan.Ticks));
        }

        static private void AddToTracking(RawDebugEventObject p_obj, String p_guardName)
        {
            GuardObject go =  GuardObjectFactory.CreateGuardObject(p_guardName, p_obj.Time);
            // Guards By Name
            m_guardsByName.Add(p_guardName, go);

            //Guards In Order
            if(m_guardsInOrder.Contains(p_guardName)==false)
                m_guardsInOrder.Add(p_guardName);

            // Guards Each Second
            DateTime currSec = Truncate(p_obj.Time, TimeSpan.FromMilliseconds(1));
            if (m_guardsEachMillisecond.ContainsKey(currSec))
            {
                m_guardsEachMillisecond[currSec].Add(p_guardName);
            }
        }

        static public String CalculateHash(string read)
        {
            UInt64 hashedValue = 3074457345618258791ul;
            for (int i = 0; i < read.Length; i++)
            {
                hashedValue += read[i];
                hashedValue *= 3074457345618258799ul;
            }
            return hashedValue.ToString();
        }

        static public bool ProcessEvent(RawDebugEventObject p_obj)
        {

            log.Info("Processing Event");
            log.Debug("Pattern matching.");
            // Okay, we now have a valid object to work with
            string pattern = @"([0-9A-Za-z_]*)";
            log.Debug("Declaring Regex.");
            Regex r = new Regex(pattern);
            log.DebugFormat("Performing match on {0}", p_obj.EventString);
            Match m = r.Match(p_obj.EventString);

            // If we've failed, just move on
            if (!m.Success)
            {
                log.DebugFormat("{0} failed the regex.", p_obj.EventString);
                return false;
            }

            // Process The Guard
            String guardName = m.Value.Trim();
            string value = ConfigurationManager.AppSettings["trusted"];
            if (value == null || !value.Equals("true"))
            {
                guardName = CalculateHash(guardName);
            }
            
            // Check to see if it exists in our map yet
            if (GuardTracking.GuardsByName.ContainsKey(guardName) == false)
            {
                log.DebugFormat("{0} did not exist in the GuardMap", guardName);
                log.Debug("Adding to Tracking");
                AddToTracking(p_obj, guardName);
                
                if (m_guardsByName[guardName].DidInvoke(p_obj.EventString))
                {
                    log.Debug("First occurrence is an invocation");
                    m_guardsByName[guardName].Invoke(p_obj.Time);
                }


                // Notify our checkbox list that we've found a new guard
                if (ReportProgress != null)
                {
                    // Tell anybody listening about our newly added Guard
                    ReportProgress(null, new GuardTrackingProgressArgs { Message = guardName, MessageType = GuardTrackingProgressType.AdditionEvent });
                }

            }
            else
            {
                log.DebugFormat("{0} exists in the GuardMap", guardName);
                if (m_guardsByName[guardName].DidInvoke(p_obj.EventString))
                {
                    m_guardsByName[guardName].Invoke(p_obj.Time);
                }
                else if (m_guardsByName[guardName].DidRun(p_obj.EventString))
                {
                    m_guardsByName[guardName].Run(p_obj.Time);
                }
                else if (m_guardsByName[guardName].DidFire(p_obj.EventString))
                {
                    m_guardsByName[guardName].Fired();
                    log.Error(p_obj.EventString);
                }
                else if (m_guardsByName[guardName].DidExit(p_obj.EventString))
                {
                    m_guardsByName[guardName].Exited(p_obj.Time);
                }
            }


            // Notify our checkbox list that we've found a new guard
            if (ReportProgress != null)
            {
                // Tell anybody listening about our newly added Guard
                ReportProgress(null, new GuardTrackingProgressArgs { Message = guardName, MessageType = GuardTrackingProgressType.ProcessedEvent });
            }
            return true;
        }

        public override string ToString()
        {

            string s = "";

            foreach (KeyValuePair<String, GuardObject> entry in m_guardsByName)
            {
                s += String.Format("{0,-24} {1,8} {2,8} {3,8} {4,8} {5,8}",
                                    entry.Key,
                                    entry.Value.NumInvoked,
                                    entry.Value.NumRun,
                                    entry.Value.NumFired,
                                    entry.Value.AverageInvoked,
                                    entry.Value.AverageRun);
            }
            return s;

        }


        public static void Reset()
        {
            try
            {

                m_guardsEachMillisecond.Clear();
                m_guardsInOrder.Clear();
                m_guardsByName.Clear();
            }
            catch (Exception me)
            {
                log.Error(me.Message);
            }
                
        }
    }
}
