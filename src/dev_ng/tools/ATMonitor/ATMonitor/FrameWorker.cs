﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.ComponentModel;
using System.Reflection;
using MSI.Afterburner;
using MSI.Afterburner.Exceptions;
using log4net;

namespace ATMonitor
{

    // Eventargs to contain information to send to the subscriber
    public class FrameProgressArgs : EventArgs
    {
        public RawFrameObject RFOData { get; set; }

    }
    class FrameWorker
    {

        // Event handler to bind to for reporting progress
        public static EventHandler<FrameProgressArgs> ReportProgress;
        
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static System.ComponentModel.BackgroundWorker m_worker;

        private static bool m_initialized;
        public static bool IsInitialized
        {
            get { return m_initialized; }
        }
        public static void Initialize()
        {
            try
            {
                m_worker = new System.ComponentModel.BackgroundWorker();
                m_worker.DoWork +=
                    new DoWorkEventHandler(DoWork);
                m_worker.RunWorkerCompleted +=
                    new RunWorkerCompletedEventHandler(RunWorkerCompleted);
                m_worker.ProgressChanged +=
                    new ProgressChangedEventHandler(ProgressChanged);
                m_worker.WorkerSupportsCancellation = true;

                m_initialized = true;
            }
            catch(Exception me)
            {
                log.Error(me.Message);
            }
        }
        public static void DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                log.Info("FpsPlotterWorker:DoWork");
                BackgroundWorker worker = sender as BackgroundWorker;

                while (worker.CancellationPending == false)
                {
                    if (RawFrameQueue.Queue.IsEmpty)
                    {
                        // Lets just sleep for 20 ms, since that's 1/5 the time of a second, when we should be sampling for more data, next.
                        System.Threading.Thread.Sleep(20);
                        continue;
                    }
                    RawFrameObject frame;
                    bool dequeued = RawFrameQueue.Queue.TryDequeue(out frame);
                    if (!dequeued)
                    {
                        log.Warn("There was a problem attempting to dequeue a buffer that wasn't empty.");
                        continue;
                    }
                    else
                    {
                        if (ReportProgress != null)
                        {
                            // Great the process args object to send in our event handler
                            FrameProgressArgs pargs = new FrameProgressArgs();
                            // Populate the data with the frame
                            pargs.RFOData = frame;
                            // Report the progress to anybody listening
                            ReportProgress(null, pargs);
                        }
                    }

                    // Sleep this thread for a little bit.
                    System.Threading.Thread.Sleep(50);
                }
            }
            catch (Exception me)
            {
                // Output the exception for debugging
                log.Error(me.Message);
            }

        }

        // This event handler deals with the results of the 
        // background operation. 
        public static void RunWorkerCompleted(
            object sender, RunWorkerCompletedEventArgs e)
        {
            log.Info("FpsPlotterWorker:RunWorkerCompleted");
        }

        // This event handler updates the progress bar. 
        public static void ProgressChanged(object sender,
            ProgressChangedEventArgs e)
        {

        }
    }
}
