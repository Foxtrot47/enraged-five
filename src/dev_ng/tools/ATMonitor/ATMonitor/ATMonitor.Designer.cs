﻿namespace ATMonitor
{
    partial class ATMonitor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            this.m_captureButton = new System.Windows.Forms.Button();
            this.m_guardDisplayCheckboxList = new System.Windows.Forms.CheckedListBox();
            this.m_debugPlotter = new System.Windows.Forms.Timer(this.components);
            this.m_selectAll = new System.Windows.Forms.Button();
            this.m_debugEventsCountLabel = new System.Windows.Forms.Label();
            this.m_analyzeDebugEventsButton = new System.Windows.Forms.Button();
            this.m_selectNone = new System.Windows.Forms.Button();
            this.m_debugEventsProcessedLabel = new System.Windows.Forms.Label();
            this.m_clearButton = new System.Windows.Forms.Button();
            this.m_startScaleTrackBar = new System.Windows.Forms.TrackBar();
            this.m_stopScaleTrackBar = new System.Windows.Forms.TrackBar();
            this.m_analyzedProgressBar = new System.Windows.Forms.ProgressBar();
            this.m_graphTabs = new System.Windows.Forms.TabControl();
            this.m_fpsTabPage = new System.Windows.Forms.TabPage();
            this.m_fpsChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.m_frTabPage = new System.Windows.Forms.TabPage();
            this.m_frChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.m_verticalTrackBar = new System.Windows.Forms.TrackBar();
            this.m_saveButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.m_startScaleTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_stopScaleTrackBar)).BeginInit();
            this.m_graphTabs.SuspendLayout();
            this.m_fpsTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_fpsChart)).BeginInit();
            this.m_frTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_frChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_verticalTrackBar)).BeginInit();
            this.SuspendLayout();
            // 
            // m_captureButton
            // 
            this.m_captureButton.Location = new System.Drawing.Point(942, 12);
            this.m_captureButton.Name = "m_captureButton";
            this.m_captureButton.Size = new System.Drawing.Size(310, 188);
            this.m_captureButton.TabIndex = 1;
            this.m_captureButton.Text = "Start";
            this.m_captureButton.UseVisualStyleBackColor = true;
            this.m_captureButton.Click += new System.EventHandler(this.m_captureButton_Click);
            // 
            // m_guardDisplayCheckboxList
            // 
            this.m_guardDisplayCheckboxList.FormattingEnabled = true;
            this.m_guardDisplayCheckboxList.Location = new System.Drawing.Point(22, 12);
            this.m_guardDisplayCheckboxList.Name = "m_guardDisplayCheckboxList";
            this.m_guardDisplayCheckboxList.Size = new System.Drawing.Size(224, 304);
            this.m_guardDisplayCheckboxList.Sorted = true;
            this.m_guardDisplayCheckboxList.TabIndex = 3;
            this.m_guardDisplayCheckboxList.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.m_guardDisplayCheckboxList_ItemCheck);
            // 
            // m_selectAll
            // 
            this.m_selectAll.Location = new System.Drawing.Point(253, 13);
            this.m_selectAll.Name = "m_selectAll";
            this.m_selectAll.Size = new System.Drawing.Size(683, 66);
            this.m_selectAll.TabIndex = 4;
            this.m_selectAll.Text = "Select All";
            this.m_selectAll.UseVisualStyleBackColor = true;
            this.m_selectAll.Click += new System.EventHandler(this.m_selectAll_Click);
            // 
            // m_debugEventsCountLabel
            // 
            this.m_debugEventsCountLabel.AutoSize = true;
            this.m_debugEventsCountLabel.Location = new System.Drawing.Point(129, 319);
            this.m_debugEventsCountLabel.Name = "m_debugEventsCountLabel";
            this.m_debugEventsCountLabel.Size = new System.Drawing.Size(13, 13);
            this.m_debugEventsCountLabel.TabIndex = 5;
            this.m_debugEventsCountLabel.Text = "0";
            // 
            // m_analyzeDebugEventsButton
            // 
            this.m_analyzeDebugEventsButton.Location = new System.Drawing.Point(252, 146);
            this.m_analyzeDebugEventsButton.Name = "m_analyzeDebugEventsButton";
            this.m_analyzeDebugEventsButton.Size = new System.Drawing.Size(347, 54);
            this.m_analyzeDebugEventsButton.TabIndex = 6;
            this.m_analyzeDebugEventsButton.Text = "Analyze";
            this.m_analyzeDebugEventsButton.UseVisualStyleBackColor = true;
            this.m_analyzeDebugEventsButton.Click += new System.EventHandler(this.m_analyzeDebugEventsButton_Click);
            // 
            // m_selectNone
            // 
            this.m_selectNone.Location = new System.Drawing.Point(252, 85);
            this.m_selectNone.Name = "m_selectNone";
            this.m_selectNone.Size = new System.Drawing.Size(684, 55);
            this.m_selectNone.TabIndex = 7;
            this.m_selectNone.Text = "Select None";
            this.m_selectNone.UseVisualStyleBackColor = true;
            this.m_selectNone.Click += new System.EventHandler(this.m_selectNone_Click);
            // 
            // m_debugEventsProcessedLabel
            // 
            this.m_debugEventsProcessedLabel.AutoSize = true;
            this.m_debugEventsProcessedLabel.Location = new System.Drawing.Point(19, 319);
            this.m_debugEventsProcessedLabel.Name = "m_debugEventsProcessedLabel";
            this.m_debugEventsProcessedLabel.Size = new System.Drawing.Size(13, 13);
            this.m_debugEventsProcessedLabel.TabIndex = 8;
            this.m_debugEventsProcessedLabel.Text = "0";
            // 
            // m_clearButton
            // 
            this.m_clearButton.Location = new System.Drawing.Point(942, 262);
            this.m_clearButton.Name = "m_clearButton";
            this.m_clearButton.Size = new System.Drawing.Size(310, 54);
            this.m_clearButton.TabIndex = 9;
            this.m_clearButton.Text = "Clear";
            this.m_clearButton.UseVisualStyleBackColor = true;
            this.m_clearButton.Click += new System.EventHandler(this.m_clearButton_Click);
            // 
            // m_startScaleTrackBar
            // 
            this.m_startScaleTrackBar.Location = new System.Drawing.Point(12, 343);
            this.m_startScaleTrackBar.Maximum = 1;
            this.m_startScaleTrackBar.Name = "m_startScaleTrackBar";
            this.m_startScaleTrackBar.Size = new System.Drawing.Size(1240, 45);
            this.m_startScaleTrackBar.TabIndex = 10;
            this.m_startScaleTrackBar.Scroll += new System.EventHandler(this.m_startScaleTrackBar_Scroll);
            // 
            // m_stopScaleTrackBar
            // 
            this.m_stopScaleTrackBar.Location = new System.Drawing.Point(12, 394);
            this.m_stopScaleTrackBar.Maximum = 1;
            this.m_stopScaleTrackBar.Name = "m_stopScaleTrackBar";
            this.m_stopScaleTrackBar.Size = new System.Drawing.Size(1240, 45);
            this.m_stopScaleTrackBar.TabIndex = 11;
            this.m_stopScaleTrackBar.Value = 1;
            this.m_stopScaleTrackBar.Scroll += new System.EventHandler(this.m_stopScaleTrackBar_Scroll);
            // 
            // m_analyzedProgressBar
            // 
            this.m_analyzedProgressBar.Location = new System.Drawing.Point(605, 146);
            this.m_analyzedProgressBar.Name = "m_analyzedProgressBar";
            this.m_analyzedProgressBar.Size = new System.Drawing.Size(330, 54);
            this.m_analyzedProgressBar.Step = 1;
            this.m_analyzedProgressBar.TabIndex = 12;
            // 
            // m_graphTabs
            // 
            this.m_graphTabs.Controls.Add(this.m_fpsTabPage);
            this.m_graphTabs.Controls.Add(this.m_frTabPage);
            this.m_graphTabs.Location = new System.Drawing.Point(12, 435);
            this.m_graphTabs.Name = "m_graphTabs";
            this.m_graphTabs.SelectedIndex = 0;
            this.m_graphTabs.Size = new System.Drawing.Size(1212, 555);
            this.m_graphTabs.TabIndex = 13;
            this.m_graphTabs.SelectedIndexChanged += new System.EventHandler(this.m_graphTabs_SelectedIndexChanged);
            // 
            // m_fpsTabPage
            // 
            this.m_fpsTabPage.Controls.Add(this.m_fpsChart);
            this.m_fpsTabPage.Location = new System.Drawing.Point(4, 22);
            this.m_fpsTabPage.Name = "m_fpsTabPage";
            this.m_fpsTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.m_fpsTabPage.Size = new System.Drawing.Size(1204, 529);
            this.m_fpsTabPage.TabIndex = 0;
            this.m_fpsTabPage.Text = "FPS";
            this.m_fpsTabPage.UseVisualStyleBackColor = true;
            this.m_fpsTabPage.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // m_fpsChart
            // 
            legend1.Name = "Legend1";
            this.m_fpsChart.Legends.Add(legend1);
            this.m_fpsChart.Location = new System.Drawing.Point(6, 6);
            this.m_fpsChart.Name = "m_fpsChart";
            this.m_fpsChart.Size = new System.Drawing.Size(1160, 520);
            this.m_fpsChart.TabIndex = 3;
            this.m_fpsChart.Text = "m_fpsChart";
            this.m_fpsChart.MouseEnter += new System.EventHandler(this.m_fpsChart_MouseEnter);
            this.m_fpsChart.MouseLeave += new System.EventHandler(this.m_fpsChart_MouseLeave);
            this.m_fpsChart.MouseMove += new System.Windows.Forms.MouseEventHandler(this.m_fpsChart_MouseMove);
            // 
            // m_frTabPage
            // 
            this.m_frTabPage.Controls.Add(this.m_frChart);
            this.m_frTabPage.Location = new System.Drawing.Point(4, 22);
            this.m_frTabPage.Name = "m_frTabPage";
            this.m_frTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.m_frTabPage.Size = new System.Drawing.Size(1204, 529);
            this.m_frTabPage.TabIndex = 1;
            this.m_frTabPage.Text = "Frametime";
            this.m_frTabPage.UseVisualStyleBackColor = true;
            // 
            // m_frChart
            // 
            legend2.Name = "Legend1";
            this.m_frChart.Legends.Add(legend2);
            this.m_frChart.Location = new System.Drawing.Point(6, 6);
            this.m_frChart.Name = "m_frChart";
            this.m_frChart.Size = new System.Drawing.Size(1160, 520);
            this.m_frChart.TabIndex = 0;
            this.m_frChart.Text = "m_frChart";
            this.m_frChart.MouseEnter += new System.EventHandler(this.m_fpsChart_MouseEnter);
            this.m_frChart.MouseLeave += new System.EventHandler(this.m_fpsChart_MouseLeave);
            this.m_frChart.MouseMove += new System.Windows.Forms.MouseEventHandler(this.m_fpsChart_MouseMove);
            // 
            // m_verticalTrackBar
            // 
            this.m_verticalTrackBar.Location = new System.Drawing.Point(1226, 457);
            this.m_verticalTrackBar.Name = "m_verticalTrackBar";
            this.m_verticalTrackBar.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.m_verticalTrackBar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.m_verticalTrackBar.Size = new System.Drawing.Size(45, 529);
            this.m_verticalTrackBar.TabIndex = 14;
            this.m_verticalTrackBar.Value = 10;
            this.m_verticalTrackBar.Scroll += new System.EventHandler(this.m_verticalTrackBar_Scroll);
            // 
            // m_saveButton
            // 
            this.m_saveButton.Location = new System.Drawing.Point(252, 262);
            this.m_saveButton.Name = "m_saveButton";
            this.m_saveButton.Size = new System.Drawing.Size(683, 54);
            this.m_saveButton.TabIndex = 15;
            this.m_saveButton.Text = "Save Artifacts";
            this.m_saveButton.UseVisualStyleBackColor = true;
            this.m_saveButton.Click += new System.EventHandler(this.m_saveButton_Click);
            // 
            // ATMonitor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 1002);
            this.Controls.Add(this.m_saveButton);
            this.Controls.Add(this.m_verticalTrackBar);
            this.Controls.Add(this.m_graphTabs);
            this.Controls.Add(this.m_analyzedProgressBar);
            this.Controls.Add(this.m_stopScaleTrackBar);
            this.Controls.Add(this.m_startScaleTrackBar);
            this.Controls.Add(this.m_clearButton);
            this.Controls.Add(this.m_debugEventsProcessedLabel);
            this.Controls.Add(this.m_selectNone);
            this.Controls.Add(this.m_analyzeDebugEventsButton);
            this.Controls.Add(this.m_debugEventsCountLabel);
            this.Controls.Add(this.m_selectAll);
            this.Controls.Add(this.m_guardDisplayCheckboxList);
            this.Controls.Add(this.m_captureButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1198, 768);
            this.Name = "ATMonitor";
            this.Text = "ATMonitor";
            ((System.ComponentModel.ISupportInitialize)(this.m_startScaleTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_stopScaleTrackBar)).EndInit();
            this.m_graphTabs.ResumeLayout(false);
            this.m_fpsTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.m_fpsChart)).EndInit();
            this.m_frTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.m_frChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_verticalTrackBar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button m_captureButton;
        private System.Windows.Forms.CheckedListBox m_guardDisplayCheckboxList;
        private System.Windows.Forms.Timer m_debugPlotter;
        private System.Windows.Forms.Button m_selectAll;
        private System.Windows.Forms.Label m_debugEventsCountLabel;
        private System.Windows.Forms.Button m_analyzeDebugEventsButton;
        private System.Windows.Forms.Button m_selectNone;
        private System.Windows.Forms.Label m_debugEventsProcessedLabel;
        private System.Windows.Forms.Button m_clearButton;
        private System.Windows.Forms.TrackBar m_startScaleTrackBar;
        private System.Windows.Forms.TrackBar m_stopScaleTrackBar;
        private System.Windows.Forms.ProgressBar m_analyzedProgressBar;
        private System.Windows.Forms.TabControl m_graphTabs;
        private System.Windows.Forms.TabPage m_fpsTabPage;
        private System.Windows.Forms.TabPage m_frTabPage;
        private System.Windows.Forms.DataVisualization.Charting.Chart m_fpsChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart m_frChart;
        private System.Windows.Forms.TrackBar m_verticalTrackBar;
        private System.Windows.Forms.Button m_saveButton;


    }
}

