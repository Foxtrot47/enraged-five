﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Text.RegularExpressions;
using System.ComponentModel;
using log4net;

namespace ATMonitor
{
    public class AnalyzeDebugEventsProgressArgs : EventArgs
    {
        public Int32 AdditionValue{ get; set; }

    }
    class AnalyzeDebugEventsWorker
    {

        public static System.ComponentModel.BackgroundWorker m_worker;
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        //public static EventHandler<AnalyzeDebugEventsProgressArgs> ReportProgress;
        private static bool m_initialized;
        public static bool IsInitialized
        {
            get { return m_initialized; }
        }
        public static void Initialize()
        {
            try
            {
                log.Debug("Initialization");
                m_worker = new System.ComponentModel.BackgroundWorker();
                m_worker.DoWork +=
                    new DoWorkEventHandler(DoWork);
                m_worker.RunWorkerCompleted +=
                    new RunWorkerCompletedEventHandler(RunWorkerCompleted);
                m_worker.ProgressChanged +=
                    new ProgressChangedEventHandler(ProgressChanged);
                m_worker.WorkerSupportsCancellation = true;

                m_initialized = true;
            }
            catch (Exception me)
            {
                log.Error(me.Message);
            }
        }
        static public void DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {

                log.Info("ProcessDebugEventsWorker:DoWork");
                int attemptCounter = 0;
            
                BackgroundWorker worker = sender as BackgroundWorker;
                // First check for cancelled state
                while (worker.CancellationPending == false)
                {
                    log.Debug("Loop");
                    // Put the sleep at the begining of the loop, because it'll always happen
                    System.Threading.Thread.Sleep(10);
                    RawDebugEventObject dbo;
                    log.Debug("Peeking");
                    // Check to see that there are events to consume
                    if (RawDebugEvents.GetConcurrentQueue().IsEmpty)
                    {
                        log.Debug("No front item to peek into.");
                        attemptCounter++;
                        if (attemptCounter == 1000 / 10 * 60)
                            m_worker.CancelAsync();
                        continue;
                    }

                    // Try to dequeue
                    log.Debug("Dequeuing");
                    // If it comes back false, just move on
                    if (!RawDebugEvents.GetConcurrentQueue().TryDequeue(out dbo))
                    {
                        log.Debug("Failed to dequeue.");
                        continue;
                    }
                    log.Debug("Calling Guard Tracking to Process Event");

                    GuardTracking.ProcessEvent(dbo);
                    // Notify our checkbox list that we've found a new guard
                    //if (ReportProgress != null)
                    //{
                    //    // Tell anybody listening about our newly added Guard
                    //    ReportProgress(null, new AnalyzeDebugEventsProgressArgs { AdditionValue = 1});
                    //}
                }
            }
            catch (Exception me)
            {
                log.Error(me.Message);
            }
        }
        static public void RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            log.Info("ProcessDebugEventsWorker:RunWorkerCompleted");
        }

        static public void ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            log.Info("ProcessDebugEventsWorker:ProgressChanged");
        }

    }
}
