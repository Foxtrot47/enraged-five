﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Windows.Forms;
namespace ATMonitor
{
    class GuardObject
    {
        public GuardObject(String p_name, DateTime p_invocationTime)
        {
            m_name = p_name;
            m_fired = 0;
            m_run = 0;
            m_invoked = 0;
            m_timeOfRuns = new HashSet<DateTime>();
            m_timeOfRuns.Add(p_invocationTime);
        }
        virtual public void Invoke(DateTime p_timeRun)
        {
            m_invoked++;
            m_lastInvoked = p_timeRun;
        }
        virtual public void Run(DateTime p_timeRun)
        {
            m_didRun = true;
            m_run++;
            m_lastRun = p_timeRun;
            m_timeOfRuns.Add(p_timeRun);
        }
        virtual public void Fired()
        {
            m_fired++;
        }
        virtual public void Exited(DateTime p_timeRun)
        {
            // First handle our run scenario
            if (m_didRun)
            {
                var runDiff = p_timeRun.Subtract(m_lastRun);
                if (m_run > 1)
                {
                    m_averageRun -= m_averageRun / m_run;
                    m_averageRun += runDiff.Ticks / m_run;
                }
                else
                {
                    m_averageRun = runDiff.Ticks;
                }
                m_didRun = false;
            }

            // Next handle our invoke scenario
            var invDiff = p_timeRun.Subtract(m_lastInvoked);
            if (m_run > 1)
            {
                m_averageInvoked -= m_averageInvoked / m_run;
                m_averageInvoked += invDiff.Ticks / m_run;
            }
            else
            {
                m_averageInvoked = invDiff.Ticks;
            }

            
        }
        virtual public bool DidRun(String p_debugEvent)
        {
            if (p_debugEvent.Contains("ran"))
                return true;
            else
                return false;
        }
        virtual public bool DidInvoke(String p_debugEvent)
        {
            if (p_debugEvent.Contains("invoked"))
                return true;
            else
                return false;
        }
        virtual public bool DidFire(String p_debugEvent)
        {
            if (p_debugEvent.Contains("fired"))
                return true;
            else
                return false;
        }
        virtual public bool DidExit(String p_debugEvent)
        {
            if (p_debugEvent.Contains("exit"))
                return true;
            else
                return false;
        }

        public int NumInvoked
        {
            get { return m_invoked; }
        }

        public int NumRun
        {
            get { return m_run; }
        }

        public int NumFired
        {
            get { return m_fired; }
        }

        public DateTime AverageInvoked
        {
            get
            {
                return new DateTime(m_averageInvoked);
            }
        }

        public DateTime AverageRun
        {
            get
            {
                return new DateTime(m_averageRun);
            }
        }

        public HashSet<DateTime> TimesOfRuns
        {
            get
            {
                return m_timeOfRuns;
            }
        }

        protected HashSet<DateTime> m_timeOfRuns;
        protected String m_name;
        protected int m_invoked;
        protected int m_run;
        protected int m_fired;
        
        private DateTime m_lastInvoked;
        private DateTime m_lastRun;
        private long m_averageInvoked;
        private long m_averageRun;
        private bool m_didRun;

    }
    class VVGuardObject : GuardObject
    {
        public VVGuardObject(String p_name, DateTime p_invocationTime)
            : base(p_name, p_invocationTime)
        {

        }
        public override void Invoke(DateTime p_timeRun)
        {
            m_invoked++;
            m_run++;
            return;
        }
        public override bool DidInvoke(String p_debugEvent)
        {
            if (p_debugEvent.Contains("is being invoked"))
                return true;
            else
                return false;
        }
        public override bool DidFire(String p_debugEvent)
        {
            if (p_debugEvent.Contains("did not match"))
                return true;
            else
                return false;
        }
    }

    class GuardObjectFactory
    {
        static public GuardObject CreateGuardObject(String p_name, DateTime p_invocationTime)
        {
            if(p_name.Contains("VVG")) 
            {
                return new VVGuardObject(p_name, p_invocationTime);
            }
            else
            {
                return new GuardObject(p_name, p_invocationTime);
            }
        }


    }

}
