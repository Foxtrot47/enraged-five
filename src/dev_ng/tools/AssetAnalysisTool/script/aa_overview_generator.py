#!python2
'''
Name: aa_overview_generator
Purpose: Parse asset analysis .csv data for concise detail, treesize overview, & type/class/path/name searches.
Author: Iain Downie <iain.downie@rockstarnorth.com>
'''

import argparse
import cPickle
import csv
import datetime
import os
import sys

#===================#
# Class definitions #
#===================#

class Asset:
	'''Class describing an individual asset'''
	def __init__(self, type, assetclass, path, physsize, virtsize):
		self.type = str(type)
		self.assetclass = str(assetclass)
		self.splitpath = path.split('/')
		self.path = str('/'.join(self.splitpath))
		self.folder = str('/'.join(self.splitpath[:-1]))
		self.parent = str('/'.join(self.splitpath[:-2]))
		self.name = self.splitpath[-1]
		self.physsize = int(physsize)
		self.virtsize = int(virtsize)

	# Type & class check member functions
	def isType(self, type):
		if self.type == str(type):
			return True
		else:
			return False
	
	def isClass(self, assetclass):
		if self.assetclass == str(assetclass):
			return True
		else:
			return False

class Assets:
	'''Class describing a group of assets of certain type and class'''
	def __init__(self, type, classname, children):
		self.type = str(type)
		self.classname = str(classname)
		self.children = children
		self.physsize = sum( [ x.physsize for x in self.children ] )
		self.virtsize = sum( [ x.virtsize for x in self.children ] )
		self.totalsize = int(self.physsize + self.virtsize)

class Folder:
	'''Class describing a filesystem-representative folder of assets (for treesize report)'''
	def __init__(self, path, assets):
		self.path = str(path)
		self.parent = '/'.join(self.path.split('/')[:-1])
		self.children = [ x for x in assets if self.path in x.path ]
		self.filecount = len(self.children)
		self.physsize = sum( [ x.physsize for x in self.children ] )
		self.virtsize = sum( [ x.virtsize for x in self.children ] )
		self.totalsize = int(self.physsize + self.virtsize)

class Output:
	'''Controls output to console and/or file.'''
	def __init__(self, logfile):
		self.log = logfile
		with open(self.log, 'w'):
			pass # Cleans or initialises logfile

	def GetTimestamp(cls):
		return str(datetime.datetime.now())[:-7]

	def all(self, message):
		print(message)
		with open(self.log, 'a+') as l:
			l.write('['+Output.GetTimestamp(self)+'] '+message.replace('\n','')+'\n')

	def fatal(self, message):
		with open(self.log, 'a+') as l:
			l.write('['+Output.GetTimestamp(self)+'] '+message.replace('\n','')+'\n')
		sys.exit(message)
		os.system('pause')

	def log(self, message):
		with open(self.log, 'a+') as l:
			l.write('['+Output.GetTimestamp(self)+'] '+message.replace('\n','')+'\n')

	def report(self, message):
		print(message)
		with open(self.log, 'a+') as l:
			l.write(message+'\n')

	def tty(self, message):
		print(message)

#==================#
# Helper functions #
#==================#

def compute_virt_size(asset_row):
	virt_size_bytes = asset_row[5]
	return int(virt_size_bytes)

def compute_phys_size(asset_row):
	phys_size_bytes = asset_row[3]
	return int(phys_size_bytes)

def downsize(bytevalue):
	kbvalue = int(bytevalue) / 1024
	return int(kbvalue)

def downsize_mb(bytevalue):
	kbvalue = (bytevalue) / 1024
	return int(kbvalue) / 1024.0

def get_virtsize(parent):
	virtsize = 0
	for asset in parent.children:
		virtsize += asset.virtsize
	return virtsize

def get_physsize(parent):
	physsize = 0
	for asset in parent.children:
		physsize += asset.physsize
	return physsize

def get_totalsize(parent):
	totalsize = 0
	for asset in parent.children:
		totalsize += (asset.physsize + asset.virtsize)
	return totalsize


#============================#
# Input processing functions #
#============================#

def process_input_data(inputs):
	'''Function to process input data and store it in dictionary format'''

	# Construct all assets list
	log('\nProcessing input files, please wait...\n')
	all_assets = []
	for key, value in inputs.iteritems():
		with open(value, 'r') as input:
			reader = csv.reader(input)
			for row in reader:
				if key == 'drawable_input':
					all_assets.append(Asset('Drawable', row[0], row[1], row[3], row[5]))
				elif key == 'dwd_input':
					all_assets.append(Asset('DWD', row[0], row[1], row[3], row[5]))
				elif key == 'fragment_input':
					all_assets.append(Asset('Fragment', row[0], row[1], row[3], row[5]))
				elif key == 'txd_input':
					all_assets.append(Asset('TXD', row[0], row[1], row[3], row[5]))

	master_asset_list = []

	# Subdivide by type/class as Assets objects
	for assetType in assetTypes:
		for assetClass in assetClasses:
			master_asset_list.append(Assets(assetType, assetClass, [ a for a in all_assets if a.isType(assetType) and a.isClass(assetClass) ]))

	# Categorise in dictionary
	master_asset_dict = {}
	idx = 0
	for assetType in assetTypes:
		classesdict = {}
		i = 0
		while i < len(assetClasses):
			key = str(assetClasses[i])
			classesdict[key] = master_asset_list[idx]
			i += 1
			idx += 1
		master_asset_dict[assetType] = classesdict

	master_asset_dict['All'] = { 'ALL' : Assets('All', 'ALL', all_assets) }

	# Pre-process the treesize report data
	master_asset_dict['Treesize'] = process_treesize(all_assets)

	# Store the asset identifier tuples
	master_asset_dict['Types'] = assetTypes
	master_asset_dict['Classes'] = assetClasses

	# Return the dictionary
	return master_asset_dict


def process_treesize(assets):
	'''Function to gather directory treesize data'''

	print '\nProcessing treesize report...'
	folderlist = []
	folderfiles = []
	treesizedata = []
	folderCount = 1

	all_assets = sorted(assets, key=lambda x: x.path)

	# Arbitrarily assemble a list of target folders
	for asset in all_assets:
		if asset.path.startswith('levels/gta5/'):
			if asset.splitpath[2].startswith('_') or asset.splitpath[2] == 'props' or asset.splitpath[2] == 'generic':
				if '/'.join(asset.splitpath[:4]) not in folderlist:
					folderlist.append('/'.join(asset.splitpath[:4]))
			elif '/'.join(asset.splitpath[:3]) not in folderlist:
				folderlist.append('/'.join(asset.splitpath[:3]))
		elif asset.path.startswith('models/cdimages'):
			if '/'.join(asset.splitpath[:3]) not in folderlist:
				folderlist.append('/'.join(asset.splitpath[:3]))
		elif asset.splitpath[0] == 'textures':
			if asset.splitpath[0] not in folderlist:
				folderlist.append(asset.splitpath[0])
		elif '/'.join(asset.splitpath[:2]) not in folderlist:
			folderlist.append('/'.join(asset.splitpath[:2]))

	# Get the files for each folder
	for f in folderlist:
		foldertemp = [ x for x in all_assets if x.path.startswith(f) ]
		if len(foldertemp) > 0:
			folderfiles.append(Assets(f,'',foldertemp))
			foldertemp = []

	# Group the folder/file combos in a list of Folders
	for folder in folderfiles:
		treesizedata.append(Folder(folder.type, all_assets))
	
	return treesizedata


def get_criteria():
	'''Function to process detailed user criteria inputs'''
	classChosen, typeChosen = ( False for x in range(2) )
	classInt, typeInt = ( 0 for i in range(2) )
	spacer = '                                         '
	type_choice = raw_input('\n****************\n* Asset search *\n****************\n\nRefine by asset type? Drawables (1) | DWDs (2) | Fragments (3) | TXDs (4): ')

	#Get asset type criteria
	if len(type_choice) > 0:
		for assetType in assetTypes:
			if assetType in [ type_choice.upper(), assetTypes[int(type_choice) - 1] ]:
				chosen_type = assetType
				typeChosen = True
				break
			else:
				typeInt += 1

	if not typeChosen and len(type_choice) == 0:
		chosen_type = 'All'
		typeChosen = True

	# Get asset class criteria
	class_choice = raw_input('\nRefine by class? Specify name or number: CLOUDHAT  (1) |    DECAL (2) |      DEFAULT (3) | DESTRUCTION (4) |    GENERIC (5)\n'+spacer+'GLOBALTXD (6) | INTERIOR (7) |          MAP (8) |       MODEL (9) | OUTSOURCE (10)\n'+spacer+'PARTICLE (11) |     PED (12) | PED_OVERLAY (13) |       PROP (14) | SCALEFORM (15)\n'+spacer+'TEXTURE  (16) | VEHICLE (17) |  VEHICLEMOD (18) |     WEAPON (19): ')

	if len(class_choice) > 0:
		for assetClass in assetClasses:
			if assetClass in [ class_choice.upper(), assetClasses[int(class_choice) - 1] ]:
					chosen_class = assetClass
					classChosen = True
					break
			else:
				classInt += 1

	if not classChosen and len(class_choice) == 0:
		chosen_class = 'ALL'
		classChosen = True

	# Bail out if we failed to determine choices
	if not typeChosen or not classChosen:
		log('\nInvalid choice(s)')
		return

	# Get optional keyword
	chosen_keyword = raw_input('\nRefine by keyword? Leave blank to bypass: ')
	
	# Collate & return criteria
	criteria = { 'type' : chosen_type,
				'class' : chosen_class,
				'keyword' : chosen_keyword }

	return criteria


#==========================#
# Output display functions #
#==========================#

def display_asset_count(assets):
	'''Function to display asset count breakdown'''
	# Print headers
	border = '+-----------+------+-----+------+------+------+------+-------+--------+------+------+------+-------+------+-------+------+------+------+-------+------+--------+'
	output('\n\n************************************************************* Asset count summary ******************************************************************************')
	output(border+'\n| >>> Class | CLOU-        DEF-  DESTR-  GEN-  GLOB-                          OUTS-  PART-          PED_O-         SCALE-  TEX-   VEH-  VEHIC-   WEA-          |\n| vvv Type  | DHAT  DECAL  AULT  UCTION  ERIC  ALTXD INTERIOR   MAP    MODEL  OURCE  ICLE    PED    VERLAY  PROP   FORM	   TURE   ICLE  LEMOD    PON   [TOTAL] |\n'+border)
	# Print drawable figures
	cloudhats_total, decals_total, defaults_total, destructions_total, generics_total, globaltxds_total, interiors_total, maps_total, models_total, outsources_total, particles_total, peds_total, pedoverlays_total, props_total, scaleforms_total, textures_total, vehicles_total, vehiclemods_total, weapons_total, all_total = (0 for x in range(20)) 

	assetCounts = { 'Totals' : { 'All' : 0 },
			        'Drawable' : {},
				    'DWD' : {},
				    'Fragment' : {},
				    'TXD' : {} }

	for assetClass in assetClasses:
		assetCounts['Totals'][str(assetClass)] = 0

	for assetType in assetTypes:
		i = 0
		assetCounts['Totals'][assetType] = 0

		while i < len(assetClasses):
			classKey = str(assetClasses[i])
			classCount = len(assets[assetType][classKey].children)
			assetCounts[assetType][classKey] = classCount
			assetCounts['Totals'][classKey] += classCount
			assetCounts['Totals'][assetType] += classCount
			assetCounts['Totals']['All'] += classCount
			i += 1

	for assetType in assetTypes:
		typecount = assetCounts[assetType]
		output('{:<1s} {:>9s} {:<1s} {:<4d} {:^1s} {:<3d} {:^1s} {:<4d} {:^1s} {:<4d} {:^1s} {:<4d} {:^1s} {:<4d} {:^1s} {:<5d} {:^1s} {:<6d} {:^1s} {:<4d} {:^1s} {:<4d} {:^1s} {:<4d} {:^1s} {:<5d} {:^1s} {:<4d} {:^1s} {:<5d} {:^1s} {:<4d} {:^1s} {:<4d} {:^1s} {:<4d} {:^1s} {:<5d} {:^1s} {:<4d} {:^1s} {:<6d} {:^1s}'.format('|', str(assetType), '|', typecount['CLOUDHAT'], '|', typecount['DECAL'], '|', typecount['DEFAULT'], '|', typecount['DESTRUCTION'], '|', typecount['GENERIC'], '|', typecount['GLOBALTXD'], '|', typecount['INTERIOR'], '|', typecount['MAP'], '|', typecount['MODEL'], '|', typecount['OUTSOURCE'], '|', typecount['PARTICLE'], '|', typecount['PED'], '|', typecount['PED_OVERLAY'], '|', typecount['PROP'], '|', typecount['SCALEFORM'], '|', typecount['TEXTURE'], '|', typecount['VEHICLE'], '|', typecount['VEHICLEMOD'], '|', typecount['WEAPON'], '|', assetCounts['Totals'][assetType], '|'))
	output(border)
	totals = assetCounts['Totals']
	output('{:<1s} {:>9s} {:<1s} {:<4d} {:^1s} {:<3d} {:^1s} {:<4d} {:^1s} {:<4d} {:^1s} {:<4d} {:^1s} {:<4d} {:^1s} {:<5d} {:^1s} {:<6d} {:^1s} {:<4d} {:^1s} {:<4d} {:^1s} {:<4d} {:^1s} {:<5d} {:^1s} {:<4d} {:^1s} {:<5d} {:^1s} {:<4d} {:^1s} {:<4d} {:^1s} {:<4d} {:^1s} {:<5d} {:^1s} {:<4d} {:^1s} {:<6d} {:^1s}'.format('|', '[All]', '|', totals['CLOUDHAT'], '|', totals['DECAL'], '|', totals['DEFAULT'], '|', totals['DESTRUCTION'], '|', totals['GENERIC'], '|', totals['GLOBALTXD'], '|', totals['INTERIOR'], '|', totals['MAP'], '|', totals['MODEL'], '|', totals['OUTSOURCE'], '|', totals['PARTICLE'], '|', totals['PED'], '|', totals['PED_OVERLAY'], '|', totals['PROP'], '|', totals['SCALEFORM'], '|', totals['TEXTURE'], '|', totals['VEHICLE'], '|', totals['VEHICLEMOD'], '|', totals['WEAPON'], '|', totals['All'], '|'))
	output(border+'\n')
	return


def display_assetstats(assets, mode):
	'''Function to display asset size breakdowns'''
	# Display info for selected asset type
	if mode == 0:
		mode = raw_input('\nAsset size breakdown - default = maximum info.\n\nAlternative display options:\n\n1 = All types, no total\n2 = Drawables only\n3 = DWDs only\n4 = Fragments only\n5 = TXDs only\n6 = Total only \n7 = None/skip\n\nSelection: ')
	if len(str(mode)) > 0:
		mode = int(mode)
	else:
		mode = 0

	row_split = '+-------------+-------+-----------+----------+-----------+'

	# Optionally print drawables size rundown
	if mode == 0:
		modeSet = ('Drawable', 'DWD', 'Fragment', 'TXD', 'Total')
	elif mode == 1:
		modeSet = ('Drawable', 'DWD', 'Fragment', 'TXD')
	elif mode == 2:
		modeSet = ('Drawable', '')
	elif mode == 3:
		modeSet = ('DWD', '')
	elif mode == 4:
		modeSet = ('Fragment', '')
	elif mode == 5:
		modeSet = ('TXD', '')
	elif mode == 6:
		modeSet = ('Total', '')
	elif mode == 7:
		modeSet = ()
	for assetType in modeSet:
		suff = ' '+str('*' * (8 - len(assetType)))
		if len(assetType) > 2:
			output('\n*********************** '+assetType+suff+'*************************\n'+row_split)
			output('{:14s} {:5s} {:10s} {:10s} {:10s}'.format('|             |', '      |', 'Size (MB) |', 'Size (MB)|', ' Size (MB)|'))
			output('{:14s} {:5s} {:10s} {:10s} {:10s}'.format('| AssetClass  |', 'Count |', '[physical]|', '[virtual]|', ' [total]  |'))
			output(row_split)
			tcount, tvsize, tpsize, ttsize = ( 0 for x in range(4))
			for assetClass in assetClasses:
				if assetType != 'Total':
					count = len(assets[assetType][assetClass].children)
					vsize = get_virtsize(assets[assetType][assetClass])
					psize = get_physsize(assets[assetType][assetClass])
					tsize = get_totalsize(assets[assetType][assetClass])
					tcount += count
					tvsize += vsize
					tpsize += psize
					ttsize += (vsize + psize)
					output('{:<1s} {:>13s} {:<7d} {:<10d} {:<10d} {:<10d} {:^1s}'.format('|', str(assetClass)+' |', count, downsize(psize), downsize(vsize), downsize(tsize), '|'))
				elif assetType == 'Total':
					count = len(assets['Drawable'][assetClass].children) + len(assets['DWD'][assetClass].children) + len(assets['Fragment'][assetClass].children) + len(assets['TXD'][assetClass].children)
					vsize = get_virtsize(assets['Drawable'][assetClass]) + get_virtsize(assets['DWD'][assetClass]) + get_virtsize(assets['Fragment'][assetClass]) + get_virtsize(assets['TXD'][assetClass])
					psize = get_physsize(assets['Drawable'][assetClass]) + get_physsize(assets['DWD'][assetClass]) + get_physsize(assets['Fragment'][assetClass]) + get_physsize(assets['TXD'][assetClass])
					tsize = get_totalsize(assets['Drawable'][assetClass]) + get_totalsize(assets['DWD'][assetClass]) + get_totalsize(assets['Fragment'][assetClass]) + get_totalsize(assets['TXD'][assetClass])
					tcount += count
					tvsize += vsize
					tpsize += psize
					ttsize += (vsize + psize)
					output('{:<1s} {:>13s} {:<7d} {:<10s} {:<10s} {:<10s} {:^1s}'.format('|', str(assetClass)+' |', count, str('%.2f' % downsize_mb(psize)), str('%.2f' % downsize_mb(vsize)), str('%.2f' % downsize_mb(tsize)), '|'))
			output(row_split)
			output('{:<1s} {:>13s} {:<7d} {:<10s} {:<10s} {:<10s} {:^1s}'.format('|', 'TOTAL (MB) |', tcount, str('%.2f' % downsize_mb(tpsize)), str('%.2f' % downsize_mb(tvsize)), str('%.2f' % downsize_mb(ttsize)), '|'))
			output(row_split+'\n')


def display_treesize(treesizedata):
	'''Function to display directory treesize report'''

	border = '+-----------------------------------------------------------------+------+---------+---------+-------------+'
	
	totalfiles, totalphys, totalvirt = ( 0 for x in range(3) )
	sortmode = 1
	sortchoice = raw_input('\nSort data by folder name (1) or size (2): ')

	if int(sortchoice) > 0 < 3:
		sortmode = sortchoice
		if int(sortmode) == 1:
			treesizedata = sorted(treesizedata, key=lambda x: x.path)
		elif int(sortmode) == 2:
			treesizedata = sorted(treesizedata, key=lambda x: x.totalsize)

	output('\n'+border)
	output('{:<1s} {:<64s} {:<6s} {:<9s} {:<8s} {:<1s} {:>13s}'.format('|', 'Folder', 'Files', 'KB Phys', 'KB Virt', '|', 'Total size |'))
	output(border)
	folderIdx = 0
	for folder in treesizedata:
		if folder.parent == treesizedata[folderIdx-1].parent and ( len(folder.path.split('/')) > 3 or folder.path.split('/')[-1][:13] in ('componentpeds', 'streamedpeds_') ) : # Indent if nested folder
			folderIndent = '   '
		else:
			folderIndent = ''

		totalfiles += folder.filecount
		totalphys += folder.physsize
		totalvirt += folder.virtsize
		output('{:<1s} {:<64s} {:<6d} {:<9d} {:<8d} {:<1s} {:>13s}'.format('|', folderIndent+folder.path, folder.filecount, downsize(folder.physsize), downsize(folder.virtsize), '|', str('%.2f' % downsize_mb(folder.totalsize))+' MB |'))
		folderIdx += 1
	output(border)
	output('{:<1s} {:<64s} {:<6d} {:<9d} {:<8d} {:<1s} {:>13s}'.format('|', 'Total', totalfiles, downsize(totalphys), downsize(totalvirt), '|', str('%.2f' % downsize_mb(totalphys + totalvirt))+' MB |'))
	output(border)

	return


def refine_results(assets, criteria):
	'''Function to output results that matching values returned by get_criteria()'''
	# Initialise local variables.
	total_bytes_all, total_bytes_phys, total_bytes_virt = ( 0 for x in range(3))
	type = criteria['type']
	assetclass = criteria['class']
	keyword = criteria['keyword']

	# If no criteria are supplied, double check and provide another opportunity
	if type == 'All' and assetclass == 'ALL' and len(keyword) == 0:
		really = raw_input('\nNo search criteria supplied.\nAre you sure you want to display '+str(len(assets['All']['ALL'].children))+' asset details? Y/N: ')
		if really.lower() != 'y':
			criteria = get_criteria()
			type = criteria['type']
			assetclass = criteria['class']
			keyword = criteria['keyword']

	results = []
	num_hits = 0
	# Specify table elements for output structure.
	header1 = '{:<12s} {:<110s} {:<12s} {:<11s} {:<20s}'.format( '| Class      ', '| Path', '| Physical', '| Virtual', '| Total size         |' )
	header2 = '|             |                                                                                                              | (bytes)    | (bytes)   | (KB)      (MB)     |'
	border = '+-------------+--------------------------------------------------------------------------------------------------------------+------------+-----------+----------+---------+'
	nohits = '|             |                                *** No assets match those criteria ***                                        |            |           |          |         |\n'+border
	# Iterate over refined set (if populated) to collate hits
	if len(assets[type][assetclass].children) > 0:
		for asset in assets[type][assetclass].children:
			if len(keyword) > 0 and str(keyword.lower()) in str(asset.path).lower() or len(assets[type][assetclass].children) > 0 and len(keyword) == 0:
				results.append(asset) # Add the hit to a list for smoother subsequent rendering
		
		# Sort the hits for better readability
		results = sorted(results, key=lambda x: x.assetclass)
		results = sorted(results, key=lambda x: x.path)

		# Format and display the results
		output('\n'+border+'\n'+header1+'\n'+header2+'\n'+border)
		for asset in results:
			num_hits += 1
			byte_total = int(asset.physsize) + int(asset.virtsize)
			kb_total = downsize(byte_total)
			mb_total = downsize_mb(byte_total)
			total_bytes_all += byte_total
			total_bytes_phys += int(asset.physsize)
			total_bytes_virt += int(asset.virtsize)
			output('{:<1s} {:<11s} {:<110s} {:<12s} {:<11s} {:<10s} {:<9s} {:>1s}'.format('|', asset.assetclass, str('| '+asset.path), str('| '+str(asset.physsize)), str('| '+str(asset.virtsize)), str('| '+str(kb_total)), str('| '+str('%.2f' % mb_total)), '|'))
	if num_hits == 0:
		output(nohits)
	elif num_hits > 0:
		total_mb_all = downsize_mb(total_bytes_all)
		total_kb_all = downsize(total_bytes_all)
		output(border)
		output('{:<1s} {:<11s} {:<110s} {:<12s} {:<11s} {:<10s} {:<9s} {:>1s}'.format('|', 'Total', '| Count: '+str(num_hits), str('| '+str(total_bytes_phys)), str('| '+str(total_bytes_virt)), '| '+str(total_kb_all), str('| '+str('%.2f' % total_mb_all)), '|'))
		output(border)
	return results


#========================================#
# Main loop, args parsing, & entry point #
#========================================#

# Global asset identifier tuples
assetTypes = ('Drawable', 'DWD', 'Fragment', 'TXD')
assetClasses = ('CLOUDHAT', 'DECAL', 'DEFAULT', 'DESTRUCTION', 'GENERIC', 'GLOBALTXD', 'INTERIOR', 'MAP', 'MODEL', 'OUTSOURCE', 'PARTICLE', 'PED', 'PED_OVERLAY', 'PROP', 'SCALEFORM', 'TEXTURE', 'VEHICLE', 'VEHICLEMOD', 'WEAPON')

def main(argv):
	'''aa_overview_generator.py main loop function'''
	print '\n*************************\n* AA Overview Generator *\n*************************'

	# Paths
	projname = os.environ['RS_PROJECT']
	projroot = os.environ['RS_PROJROOT']
	assetspath = projroot+'\\assets_ng\\' if projname == 'gta5' else projroot+'\\assets\\'

	txd_input = open(assetspath+'non_final\\aa\\reports\\aa_texture_dictionary.csv', 'r')
	dwd_input = open(assetspath+'\\non_final\\aa\\reports\\aa_drawable_dictionary.csv', 'r')
	drawable_input = open(assetspath+'\\non_final\\aa\\reports\\aa_drawable.csv', 'r')
	fragment_input = open(assetspath+'\\non_final\\aa\\reports\\aa_fragment.csv', 'r')
	columns_input = open(assetspath+'\\non_final\\aa\\reports\\aa_columns.txt', 'r')
	
	assetdict = assetspath+'\\non_final\\aa\\assetdict.dat'
	
	inputs = { 'txd_input' : txd_input,
				'dwd_input' : dwd_input,
				'drawable_input' : drawable_input,
				'fragment_input' : fragment_input }

	# Try to load an existing asset dictionary
	log('\nSearching for existing asset data file...')
	try:
		with open(assetdict, 'rb') as infile:
			assets = cPickle.loads(infile.read())
			process_csv = False
	except IOError:
		print '\n'+assetdict+' not found. Importing data.'
		process_csv = True

	# Do we want to import the csv data anyway?
	if not process_csv:
		import_csv = raw_input('\nProcessed data exists in '+assetdict+'\nRefresh from current .csv reports? Y/N: ')
		if import_csv.lower() in ('y', 'yes'):
			process_csv = True

	# Parse & save the csv data - or not - accordingly
	if process_csv:
		assets = process_input_data(inputs)
		with open(assetdict, 'wb') as outfile:
			cPickle.dump(assets, outfile)

	# We want to go at least once
	repeat = True

	while repeat:
		if not args.searchonly and not args.nostats or args.fullinfo:
			display_assetstats(assets, 0)
		elif not args.fullinfo and not args.searchonly and not args.nostats:
			statdisplay = raw_input('Display asset size statistics? y/n: ')
			if statdisplay.lower() in ('y', 'yes'):
				display_assetstats(assets, 0)

		if args.treesize:
			display_treesize(assets['Treesize'])
		elif not args.treesize and not args.searchonly:
			treesize_choice = raw_input('Display treesize report? y/n: ')
			if treesize_choice.lower() in ('y', 'yes'):
				display_treesize(assets['Treesize'])

		display_asset_count(assets) # Point of reference to base criteria on
				
		if not args.nosearch:
			criteria = get_criteria() # Get the user's search criteria
			results = refine_results(assets, criteria) # Display search results

		repeat = raw_input('\nSearch again (1) or exit (0): ')
		if repeat == '1' or repeat.lower() in ('y', 'yes', 'repeat'):
			repeat = True
		else:
			repeat = False
	if not repeat:
		log('\nExiting...\n')
		sys.exit()

def parse_args(argv):
	'''Function to parse script arguments'''
	parser = argparse.ArgumentParser( description = 'Parse asset analysis .csv reports to provide overview & customisable details.' )
	parser.add_argument( '-f', '--fullinfo', action = 'store_true', help = 'Display maximum degree of info' )
	parser.add_argument( '-c', '--carboncopy', action = 'store_true', help = 'Save out displayed data to .txt' )
	parser.add_argument( '-n', '--nostats', action = 'store_true', help = 'Skip asset statistic displays' )
	parser.add_argument( '-o', '--nosearch', action = 'store_true', help = 'Don\'t search assets' )
	parser.add_argument( '-s', '--searchonly', action = 'store_true', help = 'Skip past stat & treesize displays to search' )
	parser.add_argument( '-t', '--treesize', action = 'store_true', help = 'Always display treesize report' )
	args = parser.parse_args(argv)
	return args

# Entry point
if __name__ == "__main__":
	# Get arguments
	args = parse_args(sys.argv[1:])
	# Initalise logging & report output
	log = Output(os.getcwd()+'\\aa_overview_generator.log').all
	if args.carboncopy:
		output = Output(assetspath+'\\non_final\\aa\\reports\\overview_report_'+str(datetime.datetime.now())[:-7].replace('-','').replace(' ','-').replace(':','')+'.txt').report
	else:
		output = sys.stdout.write
	# Go to main
	main(sys.argv[1:])

# EOF