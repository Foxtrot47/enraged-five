@echo off

set reports="X:\gta5\assets_ng\non_final\aa\reports"

:: Get reports from P4 if not already present
if not exist %reports%\aa_drawable.csv				p4 sync -f //depot/gta5/assets_ng/non_final/aa/reports/aa_drawable.csv
if not exist %reports%\aa_drawable_dictionary.csv	p4 sync -f //depot/gta5/assets_ng/non_final/aa/reports/aa_drawable_dictionary.csv
if not exist %reports%\aa_fragment.csv				p4 sync -f //depot/gta5/assets_ng/non_final/aa/reports/aa_fragment.csv
if not exist %reports%\aa_texture_dictionary.csv	p4 sync -f //depot/gta5/assets_ng/non_final/aa/reports/aa_texture_dictionary.csv

:: Run the script
call aa_overview_generator.py