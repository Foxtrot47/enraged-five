// ======================
// AssetAnalysisTool.cpp
// (c) 2013 RockstarNorth
// ======================

#include "../../common/fileutil.h"

#include "AssetAnalysisTool.h"
#include "../../../game/debug/AssetAnalysis/AssetAnalysisCommon.h"
#include "../../../rage/base/src/grcore/texturedebugflags.h"

/*
===================================================================================================
this tool is damn slow, latest run in release config:

loading 289839 strings .......................................................... done. (3.53 mins, total 3.53 mins)
loading 18011 records %RS_ASSETS%/non_final/aa/aa_archetype.dat ................. done. (4.20 secs, total 3.60 mins)
loading 189330 records %RS_ASSETS%/non_final/aa/aa_archetype_instance.dat ....... done. (2.22 mins, total 5.82 mins)
loading 3058 records %RS_ASSETS%/non_final/aa/aa_mapdata.dat .................... done.
loading 63138 records %RS_ASSETS%/non_final/aa/aa_texture.dat ................... done. (32.78 secs, total 6.38 mins)
loading 21484 records %RS_ASSETS%/non_final/aa/aa_texture_dictionary.dat ........ done. (4.41 secs, total 6.45 mins)
23 crc's failed to resolve in string table (see error file)
loading 27160 records %RS_ASSETS%/non_final/aa/aa_drawable.dat .................. done. (6.85 secs, total 6.56 mins)
32 crc's failed to resolve in string table (see error file)
loading 5868 records %RS_ASSETS%/non_final/aa/aa_drawable_dictionary.dat ........ done. (1.24 secs, total 6.58 mins)
loading 1650 records %RS_ASSETS%/non_final/aa/aa_fragment.dat ................... done.
loading 1603 records %RS_ASSETS%/non_final/aa/aa_light.dat ...................... done.
loading 66 records %RS_ASSETS%/non_final/aa/aa_particle.dat ..................... done.
loading 58872 records %RS_ASSETS%/non_final/aa/aa_material.dat .................. done. (33.16 secs, total 7.15 mins)
844 crc's failed to resolve in string table (see error file)
loading 220118 records %RS_ASSETS%/non_final/aa/aa_material_texture_var.dat ..... done. (2.81 mins, total 9.96 mins)
64809 crc's failed to resolve in string table (see error file)
building archetype name map ..................................................... done. (4.10 secs, total 10.03 mins)
ERROR: 1 archetype name hash collisions:
### p_cannonball01x
building texture name/path maps ................................................. done. (38.72 secs, total 10.68 mins)
building texture dictionary path map ............................................ done. (4.44 secs, total 10.75 mins)
building drawable path map ...................................................... done. (7.15 secs, total 10.87 mins)
building drawable dictionary path map ........................................... done. (1.21 secs, total 10.89 mins)
building fragment path map ...................................................... done.
building drawable<->txd links ................................................... done. (6.38 secs, total 11.00 mins)
building material texture var<->drawable links .................................. done. (3.69 mins, total 14.69 mins)
building texture dictionary hierarchy ........................................... done. (11.69 secs, total 14.89 mins)
building texture<->txd links .................................................... done. (58.92 secs, total 15.87 mins)
building shader name map ........................................................ done, found 383 shaders.
writing %RS_ASSETS%/non_final/aa/reports/aa_archetype.dat ....................... done. (5.35 secs, total 15.96 mins)
writing %RS_ASSETS%/non_final/aa/reports/aa_archetype_instance.dat .............. done. (4.07 mins, total 20.03 mins)
writing %RS_ASSETS%/non_final/aa/reports/aa_mapdata.dat ......................... done.
writing %RS_ASSETS%/non_final/aa/reports/aa_texture.dat ......................... done. (80.43 secs, total 21.38 mins)
writing %RS_ASSETS%/non_final/aa/reports/aa_texture_dictionary.dat .............. done. (9.39 secs, total 21.54 mins)
writing %RS_ASSETS%/non_final/aa/reports/aa_drawable.dat ........................ done. (17.58 secs, total 21.83 mins)
writing %RS_ASSETS%/non_final/aa/reports/aa_drawable_dictionary.dat ............. done. (1.70 secs, total 21.86 mins)
writing %RS_ASSETS%/non_final/aa/reports/aa_fragment.dat ........................ done.
writing %RS_ASSETS%/non_final/aa/reports/aa_light.dat ........................... done.
writing %RS_ASSETS%/non_final/aa/reports/aa_particle.dat ........................ done.
writing %RS_ASSETS%/non_final/aa/reports/aa_material.dat ........................ done. (68.03 secs, total 23.01 mins)
writing %RS_ASSETS%/non_final/aa/reports/aa_material_texture_var.dat ............ done. (3.68 mins, total 26.69 mins)
marking material shader usage ................................................... done. (54.26 secs, total 27.59 mins)
writing %RS_ASSETS%/non_final/aa/reports/shader_usage.csv ....................... done, found 224 (58.49%).
marking instanced archetypes .................................................... done. (3.33 mins, total 30.93 mins)
writing %RS_ASSETS%/non_final/aa/reports/archetypes_not_placed.csv .............. done, found 2055 (11.41%). (6.08 secs, total 31.03 mins)
writing %RS_ASSETS%/non_final/aa/reports/drawables_not_used.csv ................. done, found 187 (0.69%). (4.69 secs, total 31.10 mins)
marking textures used by materials .............................................. done. (3.91 mins, total 35.01 mins)
writing %RS_ASSETS%/non_final/aa/reports/textures_not_used(names).csv ........... done, found 1355 (8.77%). (4.89 secs, total 35.10 mins)
writing %RS_ASSETS%/non_final/aa/reports/textures_not_used.csv .................. done, found 567 (0.90%). (38.93 secs, total 35.74 mins)
64812 of 220118 material texture vars could not resolve texture name
71449 of 220118 material texture vars could not resolve texture path
marking textures with alpha (DXT3/DXT5) ......................................... marked 2710 textures. (3.32 secs, total 35.80 mins)
marking textures with legitimate alpha .......................................... unmarked 94597 textures. (3.34 mins, total 39.14 mins)
writing %RS_ASSETS%/non_final/aa/reports/textures_with_unused_alpha.csv ......... done, found 488 (0.77%). (36.75 secs, total 39.75 mins)
writing %RS_ASSETS%/non_final/aa/reports/textures_with_unused_alpha.csv ......... done. (67.08 secs, total 40.87 mins)
marking textures used as normal maps ............................................ marked 15963 textures (25.28%). (3.81 mins, total 44.68 mins)
writing %RS_ASSETS%/non_final/aa/reports/shifted_normal_maps.csv ................ done, found 7349 (11.64%). (56.98 secs, total 45.63 mins)
writing %RS_ASSETS%/non_final/aa/reports/textures_found_in_parent_txd.csv ....... done, found 22309 (35.33%). (53.84 secs, total 46.53 mins)
writing %RS_ASSETS%/non_final/aa/reports/textures_should_be_moved_up_2_0.csv .... done, found 304 (0.48%). (58.80 secs, total 47.51 mins)
writing %RS_ASSETS%/non_final/aa/reports/textures_should_be_moved_up_5_1.csv .... done, found 129 (0.20%). (53.33 secs, total 48.40 mins)
writing %RS_ASSETS%/non_final/aa/reports/textures_should_be_moved_down_2_1.csv .. done, found 1156 (1.83%). (51.22 secs, total 49.25 mins)
writing %RS_ASSETS%/non_final/aa/reports/textures_should_be_moved_down_5_2.csv .. done, found 1304 (2.07%). (54.84 secs, total 50.17 mins)
total time 50.17 mins
Press any key to continue . . .
===================================================================================================
*/

/*
===================================================================================================
TODO
- hierarchy reports
	1. Texture should definitely be moved down in hierarchy. This would indicate textures which are in a shared txd but only one of the children actually uses the texture (only if there are multiple children).
	2. Texture should probably be moved down in hierarchy. This would indicate textures which are in a shared txd but the number of children which actually use the texture is less than some threshold (like 25% of the total children).
	3. Texture should definitely be moved up in hierarchy. This would indicate textures which exist in all the children of a particular txd (only if there are multiple children).
	4. Texture should probably be moved up in hierarchy. This would indicate textures which exist in most of the children, say 75%.
	5. Texture exists in parent txd. This would indicate a texture which is also in the parent txd chain, so it is in essence a duplicate (although some other child might be using it directly .. it gets rather complicated in the general case).
	6. Drawable has unnecessary txd dependency. This would indicate that a drawable has a parent txd, but the drawable doesn�t use any of the textures in it (or in its parent txd chain).
	7. Txd is not used by any children. This would indicate a texture dictionary where none of the textures are used by any of the child drawables throughout the hierarchy.

- hierarchy validity checking
	- also report things like a texture with no assigned txd (not sure how this would happen ..)

- move up/move down report should be more explicit
	- should say how many levels to move up/down, and into which txd it should be moved, and how many txd children there were (should be > 1)
===================================================================================================
*/

#include "../../common/dds.h"
#include "../../common/fileutil.h"
#include "../../common/mathutil.h"
#include "../../common/progressdisplay.h"
#include "../../common/stringutil.h"

using namespace AssetAnalysis;

void assertfunc(bool cond, const char* str)
{
	if (!cond)
	{
		fprintf(stderr, "ASSERT: %s\n", str);
		system("pause");
		//exit(-1);
	}
}

enum eModelInfoType
{
	MI_TYPE_NONE     ,
	MI_TYPE_BASE     ,
	MI_TYPE_MLO      ,
	MI_TYPE_TIME     ,
	MI_TYPE_WEAPON   ,
	MI_TYPE_VEHICLE  ,
	MI_TYPE_PED      ,
	MI_TYPE_COMPOSITE,
	MI_TYPE_COUNT    ,
};

static const char* g_modelInfoTypeStrings[] =
{
	STRING(NONE     ),
	STRING(BASE     ),
	STRING(MLO      ),
	STRING(TIME     ),
	STRING(WEAPON   ),
	STRING(VEHICLE  ),
	STRING(PED      ),
	STRING(COMPOSITE),
};
CompileTimeAssert(NELEM(g_modelInfoTypeStrings) == MI_TYPE_COUNT);

enum eLightType
{
	LIGHT_TYPE_NONE       ,
	LIGHT_TYPE_POINT      ,
	LIGHT_TYPE_SPOT       ,
	LIGHT_TYPE_CAPSULE    ,
	LIGHT_TYPE_DIRECTIONAL,
	LIGHT_TYPE_AO_VOLUME  ,
	LIGHT_TYPE_COUNT      ,
};

static const char* g_lightTypeStrings[] =
{
	STRING(NONE       ),
	STRING(POINT      ),
	STRING(SPOT       ),
	STRING(CAPSULE    ),
	STRING(DIRECTIONAL),
	STRING(AO_VOLUME  ),
};
CompileTimeAssert(NELEM(g_lightTypeStrings) == LIGHT_TYPE_COUNT);

enum eLightFlashiness
{
	FL_CONSTANT              ,
	FL_RANDOM                ,
	FL_RANDOM_OVERRIDE_IF_WET,
	FL_ONCE_SECOND           ,
	FL_TWICE_SECOND          ,
	FL_FIVE_SECOND           ,
	FL_RANDOM_FLASHINESS     ,
	FL_OFF                   ,
	FL_UNUSED1               ,
	FL_ALARM                 ,
	FL_ON_WHEN_RAINING       ,
	FL_CYCLE_1               ,
	FL_CYCLE_2               ,
	FL_CYCLE_3               ,
	FL_DISCO                 ,
	FL_CANDLE                ,
	FL_PLANE                 ,
	FL_FIRE                  ,
	FL_THRESHOLD             ,
	FL_ELECTRIC              ,
	FL_STROBE                ,
	FL_COUNT                 ,
};

static const char* g_lightFlashninessStrings[] =
{
	STRING(CONSTANT              ),
	STRING(RANDOM                ),
	STRING(RANDOM_OVERRIDE_IF_WET),
	STRING(ONCE_SECOND           ),
	STRING(TWICE_SECOND          ),
	STRING(FIVE_SECOND           ),
	STRING(RANDOM_FLASHINESS     ),
	STRING(OFF                   ),
	STRING(UNUSED1               ),
	STRING(ALARM                 ),
	STRING(ON_WHEN_RAINING       ),
	STRING(CYCLE_1               ),
	STRING(CYCLE_2               ),
	STRING(CYCLE_3               ),
	STRING(DISCO                 ),
	STRING(CANDLE                ),
	STRING(PLANE                 ),
	STRING(FIRE                  ),
	STRING(THRESHOLD             ),
	STRING(ELECTRIC              ),
	STRING(STROBE                ),
};
CompileTimeAssert(NELEM(g_lightFlashninessStrings) == FL_COUNT);

enum eTextureUsage
{
	TEXTURE_USAGE_DEFAULT      ,
	TEXTURE_USAGE_DIFFUSE      ,
	TEXTURE_USAGE_BUMP         ,
	TEXTURE_USAGE_SPECULAR     ,
	TEXTURE_USAGE_ANISOSPEC    ,
	TEXTURE_USAGE_VOLUMEDETAIL ,
	TEXTURE_USAGE_DISTANCEMAP  ,
	TEXTURE_USAGE_WATERFOAM    ,
	TEXTURE_USAGE_WATERFLOW    ,
	TEXTURE_USAGE_CABLE        ,
	TEXTURE_USAGE_TINTPALETTE  ,
	TEXTURE_USAGE_TERRAINLOOKUP,
	TEXTURE_USAGE_COUNT        ,
};

static const char* g_textureUsageStrings[] =
{
	STRING(DEFAULT      ),
	STRING(DIFFUSE      ),
	STRING(BUMP         ),
	STRING(SPECULAR     ),
	STRING(ANISOSPEC    ),
	STRING(VOLUMEDETAIL ),
	STRING(DISTANCEMAP  ),
	STRING(WATERFOAM    ),
	STRING(WATERFLOW    ),
	STRING(CABLE        ),
	STRING(TINTPALETTE  ),
	STRING(TERRAINLOOKUP),
};
CompileTimeAssert(NELEM(g_textureUsageStrings) == TEXTURE_USAGE_COUNT);

static eTextureUsage GetTextureUsageForMaterialVarName(const char* varName, const char* shaderName)
{
	if (stristr(varName, "DiffuseTex") ||
		stristr(varName, "DirtTex") ||
		stristr(varName, "SnowTex") ||
		stristr(varName, "DensityTex") ||
		stristr(varName, "Density2Tex") ||
		stristr(varName, "WrinkleMaskTex") ||
		stristr(varName, "PlateBgTex") ||
		stristr(varName, "StubbleTex") ||
		stristr(varName, "EnvironmentTex")) // treat this as diffuse for now ..
	{
		return TEXTURE_USAGE_DIFFUSE;
	}

	if (stristr(varName, "BumpTex") ||
		stristr(varName, "DetailTex") ||
		stristr(varName, "NormalTex") ||
		stristr(varName, "Normal2Tex") || // e.g. DetailNormal2Texture
		stristr(varName, "NormalMap") ||  // e.g. FontNormalMap
		stristr(varName, "WrinkleTex") ||
		stristr(varName, "MirrorCrackTex"))
	{
		return TEXTURE_USAGE_BUMP;
	}

	if (stristr(varName, "VolumeTex"))
	{
		return TEXTURE_USAGE_VOLUMEDETAIL;
	}

	if (stristr(varName, "SpecularTex"))
	{
		return TEXTURE_USAGE_SPECULAR;
	}

	if (stristr(varName, "AnisoNoiseSpec"))
	{
		return TEXTURE_USAGE_ANISOSPEC;
	}

	if (_stricmp(varName, "DistanceMap") == 0 ||
		_stricmp(varName, "FontTexture") == 0)
	{
		return TEXTURE_USAGE_DISTANCEMAP;
	}

	if (_stricmp(varName, "FoamTexture" ) == 0 || // FoamOpacityMap.tcp
		_stricmp(varName, "FoamAnimTex" ) == 0 || // WaterFoamMap.tcp
		_stricmp(varName, "RiverFoamTex") == 0)   // WaterFogFoamMap.tcp
	{
		return TEXTURE_USAGE_WATERFOAM;
	}

	if (_stricmp(varName, "FlowTexture") == 0)
	{
		return TEXTURE_USAGE_WATERFLOW;
	}

	if (_stricmp(varName, "texture") == 0 && _stricmp(shaderName, "cable")) // ack, should be "CableTex"
	{
		return TEXTURE_USAGE_CABLE;
	}

	if (_stricmp(varName, "TintPaletteTex") == 0)
	{
		return TEXTURE_USAGE_TINTPALETTE;
	}

	if (_stricmp(varName, "lookupTexture") == 0)
	{
		return TEXTURE_USAGE_TERRAINLOOKUP;
	}

	return TEXTURE_USAGE_DEFAULT;
}

enum eAssetClass
{
	ASSET_CLASS_DEFAULT      , // ?
	ASSET_CLASS_MAP          , // levels/<project>/... (not interiors,props,destruction,vehicles,cloudhats,outsource,generic)
	ASSET_CLASS_AREA         , // levels/<project>/area/...
	ASSET_CLASS_TERRAIN_RAILS, // levels/<project>/terrain/a_rails*
	ASSET_CLASS_TERRAIN_ROADS, // levels/<project>/terrain/a_roads*
	ASSET_CLASS_TERRAIN_WATER, // levels/<project>/terrain/a_water*
	ASSET_CLASS_TERRAIN      , // levels/<project>/terrain/...
	ASSET_CLASS_INTERIOR     , // levels/<project>/interiors/...
	ASSET_CLASS_PROP         , // levels/<project>/props/...
	ASSET_CLASS_DESTRUCTION  , // levels/<project>/destruction/...
	ASSET_CLASS_VEHICLEMOD   , // levels/<project>/vehicemods/...
	ASSET_CLASS_VEHICLE      , // levels/<project>/vehicle* (not vehiclemods)
	ASSET_CLASS_CLOUDHAT     , // levels/<project>/cloudhats/...
	ASSET_CLASS_OUTSOURCE    , // levels/<project>/outsource/...
	ASSET_CLASS_GLOBALTXD    , // levels/<project>/generic/gtxd/...
	ASSET_CLASS_GENERIC      , // levels/<project>/genetic/... (not gtxd), or levels/generic/...
	ASSET_CLASS_PED          , // models/cdimages/... (not weapons), or levels/<project>/generic/cutspeds/...
	ASSET_CLASS_PED_OVERLAY  , // models/cdimages/ped_*
	ASSET_CLASS_WEAPON       , // models/cdimages/weapons/...
	ASSET_CLASS_MODEL        , // models/... (not cdimages)
	ASSET_CLASS_SCALEFORM    , // data/cdimages/scaleform*
	ASSET_CLASS_PARTICLE     , // data/effects/ptfx/...
	ASSET_CLASS_DECAL        , // textures/fxdecal*
	ASSET_CLASS_TEXTURE      , // textures/... (not fxdecal)
	ASSET_CLASS_COUNT        ,
};

static const char* g_assetClassStrings[] =
{
	STRING(DEFAULT      ),
	STRING(MAP          ),
	STRING(AREA         ),
	STRING(TERRAIN_RAILS),
	STRING(TERRAIN_ROADS),
	STRING(TERRAIN_WATER),
	STRING(TERRAIN      ),
	STRING(INTERIOR     ),
	STRING(PROP         ),
	STRING(DESTRUCTION  ),
	STRING(VEHICLEMOD   ),
	STRING(VEHICLE      ),
	STRING(CLOUDHAT     ),
	STRING(OUTSOURCE    ),
	STRING(GLOBALTXD    ),
	STRING(GENERIC      ),
	STRING(PED          ),
	STRING(PED_OVERLAY  ),
	STRING(WEAPON       ),
	STRING(MODEL        ),
	STRING(SCALEFORM    ),
	STRING(PARTICLE     ),
	STRING(DECAL        ),
	STRING(TEXTURE      ),
};
CompileTimeAssert(NELEM(g_assetClassStrings) == ASSET_CLASS_COUNT);

static eAssetClass GetAssetClassFromPath(const char* path)
{
	if (if_strskip(path, "levels/"))
	{
		if (if_strskip(path, varString("%s/", RS_PROJECT).c_str()))
		{
			if      (strstr(path, "interiors/"     ) == path) { return ASSET_CLASS_INTERIOR     ; }
			else if (strstr(path, "props/"         ) == path) { return ASSET_CLASS_PROP         ; }
			else if (strstr(path, "destruction/"   ) == path) { return ASSET_CLASS_DESTRUCTION  ; }
			else if (strstr(path, "vehiclemods/"   ) == path) { return ASSET_CLASS_VEHICLEMOD   ; }
			else if (strstr(path, "vehicle"        ) == path) { return ASSET_CLASS_VEHICLE      ; } // e.g. levels/<project>/vehicles_packed/...
			else if (strstr(path, "cloudhats/"     ) == path) { return ASSET_CLASS_CLOUDHAT     ; }
			else if (strstr(path, "area/"          ) == path) { return ASSET_CLASS_AREA         ; }
			else if (strstr(path, "terrain/a_rails") == path) { return ASSET_CLASS_TERRAIN_RAILS; }
			else if (strstr(path, "terrain/a_roads") == path) { return ASSET_CLASS_TERRAIN_ROADS; }
			else if (strstr(path, "terrain/a_water") == path) { return ASSET_CLASS_TERRAIN_WATER; }
			else if (strstr(path, "terrain/"       ) == path) { return ASSET_CLASS_TERRAIN      ; }
			else if (strstr(path, "outsource/"     ) == path) { return ASSET_CLASS_OUTSOURCE    ; }
			else
			{
				if (if_strskip(path, "generic/"))
				{
					if      (strstr(path, "cutspeds/") == path) { return ASSET_CLASS_PED      ; }
					else if (strstr(path, "gtxd/"    ) == path) { return ASSET_CLASS_GLOBALTXD; }
					else                                        { return ASSET_CLASS_GENERIC  ; }
				}
				else
				{
					return ASSET_CLASS_MAP; // e.g. levels/<project>/_citye/...
				}
			}
		}
		else if (strstr(path, "generic/") == path)
		{
			return ASSET_CLASS_GENERIC; // levels/generic/...
		}
	}
	else if (if_strskip(path, "models/"))
	{
		if (if_strskip(path, "cdimages/"))
		{
			if      (strstr(path, "weapons/") == path) { return ASSET_CLASS_WEAPON     ; }
			else if (strstr(path, "ped_"    ) == path) { return ASSET_CLASS_PED_OVERLAY; } // e.g. models/cdimages/ped_mp_overlay_txds
			else if (strstr(path, "ped"     ))         { return ASSET_CLASS_PED        ; } // e.g. models/cdimages/streamedpeds
		}
		else
		{
			return ASSET_CLASS_MODEL; // e.g. models/plantsmgr.idd, or z_z_fred.ift
		}
	}
	else if (strstr(path, "data/cdimages/scaleform") == path) { return ASSET_CLASS_SCALEFORM; } // e.g. data/cdimages/scaleform_generic
	else if (strstr(path, "data/effects/ptfx/"     ) == path) { return ASSET_CLASS_PARTICLE ; }
	else if (strstr(path, "textures/fxdecal"       ) == path) { return ASSET_CLASS_DECAL    ; } // textures/fxdecal.itd
	else if (strstr(path, "textures/"              ) == path) { return ASSET_CLASS_TEXTURE  ; }

	return ASSET_CLASS_DEFAULT;
}

static bool AssetPathIsHDTxd(const char* path)
{
	if (strstr(path, "+hi.itd") ||
		strstr(path, "+hidd.itd") ||
		strstr(path, "+hidr.itd") ||
		strstr(path, "+hifr.itd"))
	{
		return true;
	}

	return false;
}

static bool AssetPathIsHDDrawableOrFragment(const char* path)
{
	if (strstr(path, "_hi.idr") || // e.g. weapon HD drawable
		strstr(path, "_hi.ift"))   // e.g. vehicle HD fragment
	{
		return true;
	}

	return false;
}

static bool TextureCanHaveSkipProcessing(const char* path)
{
	const eAssetClass ac = GetAssetClassFromPath(path);

	if (ac == ASSET_CLASS_PED_OVERLAY ||
		ac == ASSET_CLASS_SCALEFORM   ||
		ac == ASSET_CLASS_PARTICLE    ||
		ac == ASSET_CLASS_DECAL       ||
		ac == ASSET_CLASS_TEXTURE     ||
		false)
	{
		return true;
	}

	return false;
}

static bool TextureCanHaveAlphaInDiffuseMap(const char* path, const char* shaderName)
{
	return true; // ?
}

static bool TextureCanHaveAlphaInBumpMap(const char* path, const char* shaderName)
{
	if (strcmp(shaderName, "decal_normal_only") == 0 || // these shaders define "DECAL_USE_NORMAL_MAP_ALPHA"
		strcmp(shaderName, "vehicle_licenseplate") == 0)
	{
		return true;
	}

	return GetAssetClassFromPath(path) == ASSET_CLASS_DECAL;
}

static bool TextureCanHaveAlphaInSpecularMap(const char* path, const char* shaderName)
{
	return GetAssetClassFromPath(path) == ASSET_CLASS_PED;
}

enum eTextureFormat // this must match enum in grcImage
{
	TEXTURE_FORMAT_UNKNOWN                    , // Undefined
	TEXTURE_FORMAT_DXT1                       , // DXT1 (aka 'BC1')
	TEXTURE_FORMAT_DXT3                       , // DXT3 (aka 'BC2')
	TEXTURE_FORMAT_DXT5                       , // DXT5 (aka 'BC3', 'DXT5NM', 'RXGB')
	TEXTURE_FORMAT_CTX1                       , // CTX1 (like DXT1 but anchor colors are 8.8 instead of 5.6.5)
	TEXTURE_FORMAT_DXT3A                      , // alpha block of DXT3 (XENON-specific)
	TEXTURE_FORMAT_DXT3A_1111                 , // alpha block of DXT3, split into four 1-bit channels (XENON-specific)
	TEXTURE_FORMAT_DXT5A                      , // alpha block of DXT5 (aka 'BC4', 'ATI1')
	TEXTURE_FORMAT_DXN                        , // DXN (aka 'BC5', 'ATI2', '3Dc', 'RGTC', 'LATC', etc.) // [CLEMENSP]
	TEXTURE_FORMAT_A8R8G8B8                   , // 32-bit color with alpha, matches Color32 class
	TEXTURE_FORMAT_A8B8G8R8                   , // 32-bit color with alpha, provided for completeness
	TEXTURE_FORMAT_A8                         , // 8-bit alpha-only (color is black)
	TEXTURE_FORMAT_L8                         , // 8-bit luminance (R=G=B=L, alpha is opaque)
	TEXTURE_FORMAT_A8L8                       , // 16-bit alpha + luminance
	TEXTURE_FORMAT_A4R4G4B4                   , // 16-bit color and alpha
	TEXTURE_FORMAT_A1R5G5B5                   , // 16-bit color with 1-bit alpha
	TEXTURE_FORMAT_R5G6B5                     , // 16-bit color
	TEXTURE_FORMAT_R3G3B2                     , // 8-bit color (not supported on consoles)
	TEXTURE_FORMAT_A8R3G3B2                   , // 16-bit color with 8-bit alpha (not supported on consoles)
	TEXTURE_FORMAT_A4L4                       , // 8-bit alpha + luminance (not supported on consoles)
	TEXTURE_FORMAT_A2R10G10B10                , // 32-bit color with 2-bit alpha
	TEXTURE_FORMAT_A2B10G10R10                , // 32-bit color with 2-bit alpha
	TEXTURE_FORMAT_A16B16G16R16               , // 64-bit four channel fixed point (s10e5 per channel -- sign, 5 bit exponent, 10 bit mantissa)
	TEXTURE_FORMAT_G16R16                     , // 32-bit two channel fixed point
	TEXTURE_FORMAT_L16                        , // 16-bit luminance (use this instead of 'R16')
	TEXTURE_FORMAT_A16B16G16R16F              , // 64-bit four channel floating point (s10e5 per channel)
	TEXTURE_FORMAT_G16R16F                    , // 32-bit two channel floating point (s10e5 per channel)
	TEXTURE_FORMAT_R16F                       , // 16-bit single channel floating point (s10e5 per channel)
	TEXTURE_FORMAT_A32B32G32R32F              , // 128-bit four channel floating point (s23e8 per channel)
	TEXTURE_FORMAT_G32R32F                    , // 64-bit two channel floating point (s23e8 per channel)
	TEXTURE_FORMAT_R32F                       , // 32-bit single channel floating point (s23e8 per channel)
	TEXTURE_FORMAT_D15S1                      , // 16-bit depth + stencil (depth is 15-bit fixed point, stencil is 1-bit) (not supported on consoles)
	TEXTURE_FORMAT_D24S8                      , // 32-bit depth + stencil (depth is 24-bit fixed point, stencil is 8-bit)
	TEXTURE_FORMAT_D24FS8                     , // 32-bit depth + stencil (depth is 24-bit s15e8, stencil is 8-bit)
	TEXTURE_FORMAT_P4                         , // 4-bit palettized (not supported on consoles)
	TEXTURE_FORMAT_P8                         , // 8-bit palettized (not supported on consoles)
	TEXTURE_FORMAT_A8P8                       , // 16-bit palettized with 8-bit alpha (not supported on consoles)
	TEXTURE_FORMAT_LINA32B32G32R32F_DEPRECATED,
	TEXTURE_FORMAT_LINA8R8G8B8_DEPRECATED     ,
	TEXTURE_FORMAT_LIN8_DEPRECATED            ,
	TEXTURE_FORMAT_RGBE                       ,
	TEXTURE_FORMAT_COUNT                      ,
};

static const char* g_textureFormatStrings[] =
{
	STRING(UNKNOWN                    ),
	STRING(DXT1                       ),
	STRING(DXT3                       ),
	STRING(DXT5                       ),
	STRING(CTX1                       ),
	STRING(DXT3A                      ),
	STRING(DXT3A_1111                 ),
	STRING(DXT5A                      ),
	STRING(DXN                        ),
	STRING(A8R8G8B8                   ),
	STRING(A8B8G8R8                   ),
	STRING(A8                         ),
	STRING(L8                         ),
	STRING(A8L8                       ),
	STRING(A4R4G4B4                   ),
	STRING(A1R5G5B5                   ),
	STRING(R5G6B5                     ),
	STRING(R3G3B2                     ),
	STRING(A8R3G3B2                   ),
	STRING(A4L4                       ),
	STRING(A2R10G10B10                ),
	STRING(A2B10G10R10                ),
	STRING(A16B16G16R16               ),
	STRING(G16R16                     ),
	STRING(L16                        ),
	STRING(A16B16G16R16F              ),
	STRING(G16R16F                    ),
	STRING(R16F                       ),
	STRING(A32B32G32R32F              ),
	STRING(G32R32F                    ),
	STRING(R32F                       ),
	STRING(D15S1                      ),
	STRING(D24S8                      ),
	STRING(D24FS8                     ),
	STRING(P4                         ),
	STRING(P8                         ),
	STRING(A8P8                       ),
	STRING(LINA32B32G32R32F_DEPRECATED),
	STRING(LINA8R8G8B8_DEPRECATED     ),
	STRING(LIN8_DEPRECATED            ),
	STRING(RGBE                       ),
};
CompileTimeAssert(NELEM(g_textureFormatStrings) == TEXTURE_FORMAT_COUNT);

static void LoadStringTables(std::xmap<u32,std::string>& table32, std::xmap<u64,std::string>& table64, const char* path)
{
	FILE* fp = fopen(path, "rb");
	assert(fp);

	if (fp)
	{
		fseek(fp, -(int)sizeof(u32)*2, SEEK_END);
		s32 numRecords = 0;
		u32 tag = 0;
		fread(&numRecords, sizeof(numRecords), 1, fp);
		fread(&tag, sizeof(tag), 1, fp);
		assert(tag == 'EOF.');
		const int numBytes = (int)(ftell(fp) - sizeof(u32)*2);
		fseek(fp, 0, SEEK_SET);

		if (numRecords > 0)
		{
			ProgressDisplay progress("loading %d strings", numRecords);

			char* data = new char[numBytes];
			char* ptr = data;
			const char* end = data + numBytes;

			fread(data, numBytes, 1, fp);

			const bool bForceLowercase = false;

			for (int index = 0; index < numRecords; index++)
			{
				const u16 tag = *(const u16*)ptr;
				ptr += sizeof(u16);

				const int length = (int)(tag & ~0xf000);

				if (tag & STRING_FLAG_EMBEDDED_CRC)
				{
					if (tag & STRING_FLAG_64BIT) // 64-bit crc
					{
						const u64 crc = *(const u64*)ptr; // embedded
						ptr += sizeof(u64);

						if (tag & STRING_FLAG_APPEND)
						{
							const u64 prependHash = *(const u64*)ptr;
							ptr += sizeof(u64);
							if (bForceLowercase) { _strlwr(ptr); }
							const std::string* ppstr = table64.access(prependHash);
							const std::string str = (ppstr ? *ppstr : std::string("?")) + "/" + std::string(ptr);
#if defined(_DEBUG)
							const u64 expected = crc64_lower(str.c_str());
							assert(crc == expected);
#endif // defined(_DEBUG)
							table64[crc] = str;
						}
						else
						{
							if (bForceLowercase) { _strlwr(ptr); }
#if defined(_DEBUG)
							const u64 expected = crc64_lower(ptr);
							assert(crc == expected);
#endif // defined(_DEBUG)
							table64[crc] = ptr;
						}
					}
					else // 32-bit crc
					{
						const u32 crc = *(const u32*)ptr; // embedded
						ptr += sizeof(u32);
						if (bForceLowercase) { _strlwr(ptr); }
#if defined(_DEBUG)
						const u32 expected = crc32_lower(ptr);
						assert(crc == expected);
#endif // defined(_DEBUG)
						table32[crc] = std::string(ptr);
					}
				}
				else if (tag & STRING_FLAG_64BIT) // 64-bit crc
				{
					if (tag & STRING_FLAG_APPEND)
					{
						const u64 prependHash = *(const u64*)ptr;
						ptr += sizeof(u64);
						if (bForceLowercase) { _strlwr(ptr); }
						const std::string* ppstr = table64.access(prependHash);
						const std::string str = (ppstr ? *ppstr : std::string("?")) + "/" + std::string(ptr);
						const u64 crc = crc64_lower(str.c_str());
						table64[crc] = str;
					}
					else
					{
						if (bForceLowercase) { _strlwr(ptr); }
						const u64 crc = crc64_lower(ptr);
						table64[crc] = std::string(ptr);
					}
				}
				else // 32-bit crc
				{
					if (bForceLowercase) { _strlwr(ptr); }
					const u32 crc = crc32_lower(ptr);
					table32[crc] = std::string(ptr);
				}

				assert(ptr + length < end);
				assert(ptr[length] == '\0');
				ptr += length + 1; // past '\0' terminator

				progress.Update(index, numRecords);
			}

			assert(ptr == end);
			delete[] data;

			progress.End();
		}

		fclose(fp);
	}
	else
	{
		fprintf(stderr, "failed to open %s\n", path);
	}
}

static std::xmap<u32,std::string> g_StringTable32;
static std::xmap<u64,std::string> g_StringTable64;

template <typename T> static void LoadStream(std::vector<T>& list, const char* path, FILE* errFile = NULL)
{
	FILE* fp = fopen(path, "rb");
	//assert(fp);

	if (fp)
	{
		fseek(fp, -(int)sizeof(u32)*2, SEEK_END);
		s32 numRecords = 0;
		u32 tag = 0;
		fread(&numRecords, sizeof(numRecords), 1, fp);
		fread(&tag, sizeof(tag), 1, fp);
		assert(tag == 'EOF.');
		fseek(fp, 0, SEEK_SET);

		if (numRecords > 0)
		{
			ProgressDisplay progress("loading %d records %s", numRecords, path);

			list.resize(numRecords);
			memset(&list[0], 0, numRecords*sizeof(T));
			int errCount = 0;

			for (int i = 0; i < numRecords; i++)
			{
				fread(static_cast<T::RecordType*>(&list[i]), sizeof(T::RecordType), 1, fp);
				assert(list[i].m_tag == T::TAG);

				list[i].Init(g_StringTable32, g_StringTable64, errFile, &errCount);

				progress.Update(i, list);
			}

			std::sort(list.begin(), list.end());

			progress.End();

			if (errCount > 0)
			{
				fprintf(stdout, "%d crc's failed to resolve in string table (see error file)\n", errCount);
			}
		}

		fclose(fp);
	}
	else
	{
		fprintf(stderr, "failed to open %s\n", path);
	}
}

static std::vector<aaArchetype         > g_Archetypes               ;
static std::vector<aaArchetypeInstance > g_ArchetypeInstances       ;
static std::vector<aaMapData           > g_MapDatas                 ;
static std::vector<aaTexture           > g_Textures                 ;
static std::vector<aaTextureDictionary > g_TextureDictionaries      ;
static std::vector<aaDrawable          > g_Drawables                ;
static std::vector<aaDrawableDictionary> g_DrawableDictionaries     ;
static std::vector<aaFragment          > g_Fragments                ;
static std::vector<aaLight             > g_Lights                   ;
static std::vector<aaParticle          > g_Particles                ;
static std::vector<aaMaterial          > g_Materials                ;
static std::vector<aaMaterialTextureVar> g_MaterialTextureVars      ;
static std::xmap<u64,int>                g_ArchetypeNameMap         ;
static std::vector<const char*>          g_TextureNames             ;
static std::xmap<u64,int>                g_TextureNameMap           ;
static std::xmap<u64,int>                g_TexturePathMap           ;
static std::xmap<u64,int>                g_TextureDictionaryPathMap ;
static std::xmap<u64,int>                g_DrawablePathMap          ;
static std::xmap<u64,int>                g_DrawableDictionaryPathMap;
static std::xmap<u64,int>                g_FragmentPathMap          ;
static std::vector<std::string>          g_ShaderNames              ;
static std::xmap<u64,int>                g_ShaderNameMap            ;

static void LoadAllStreams()
{
	LoadStringTables(g_StringTable32, g_StringTable64, varString("%s/non_final/aa/%s", RS_ASSETS, GetStreamName(STREAM_TYPE_STRING_TABLE)).c_str());

	//_mkdir(varString("%s/non_final/aa/reports", RS_ASSETS).c_str());

	FILE* errFile = fopen(varString("%s/non_final/aa/reports/err1.log", RS_ASSETS).c_str(), "w");

	LoadStream(g_Archetypes          , varString("%s/non_final/aa/%s", RS_ASSETS, GetStreamName(STREAM_TYPE_ARCHETYPE           )).c_str(), errFile);
	LoadStream(g_ArchetypeInstances  , varString("%s/non_final/aa/%s", RS_ASSETS, GetStreamName(STREAM_TYPE_ARCHETYPE_INSTANCE  )).c_str(), errFile);
	LoadStream(g_MapDatas            , varString("%s/non_final/aa/%s", RS_ASSETS, GetStreamName(STREAM_TYPE_MAPDATA             )).c_str(), errFile);
	LoadStream(g_Textures            , varString("%s/non_final/aa/%s", RS_ASSETS, GetStreamName(STREAM_TYPE_TEXTURE             )).c_str(), errFile);
	LoadStream(g_TextureDictionaries , varString("%s/non_final/aa/%s", RS_ASSETS, GetStreamName(STREAM_TYPE_TEXTURE_DICTIONARY  )).c_str(), errFile);
	LoadStream(g_Drawables           , varString("%s/non_final/aa/%s", RS_ASSETS, GetStreamName(STREAM_TYPE_DRAWABLE            )).c_str(), errFile);
	LoadStream(g_DrawableDictionaries, varString("%s/non_final/aa/%s", RS_ASSETS, GetStreamName(STREAM_TYPE_DRAWABLE_DICTIONARY )).c_str(), errFile);
	LoadStream(g_Fragments           , varString("%s/non_final/aa/%s", RS_ASSETS, GetStreamName(STREAM_TYPE_FRAGMENT            )).c_str(), errFile);
	LoadStream(g_Lights              , varString("%s/non_final/aa/%s", RS_ASSETS, GetStreamName(STREAM_TYPE_LIGHT               )).c_str(), errFile);
	LoadStream(g_Particles           , varString("%s/non_final/aa/%s", RS_ASSETS, GetStreamName(STREAM_TYPE_PARTICLE            )).c_str(), errFile);
	LoadStream(g_Materials           , varString("%s/non_final/aa/%s", RS_ASSETS, GetStreamName(STREAM_TYPE_MATERIAL            )).c_str(), errFile);
	LoadStream(g_MaterialTextureVars , varString("%s/non_final/aa/%s", RS_ASSETS, GetStreamName(STREAM_TYPE_MATERIAL_TEXTURE_VAR)).c_str(), errFile);

	// build archetype name map
	{
		ProgressDisplay progress("building archetype name map");
		std::vector<std::string> collisions;

		for (int i = 0; i < (int)g_Archetypes.size(); i++)
		{
			const aaArchetype& arch = g_Archetypes[i];
			
			if (g_ArchetypeNameMap.access(arch.m_archetypeNameHash))
			{
				collisions.push_back(arch.m_archetypeName);
			}

			g_ArchetypeNameMap[arch.m_archetypeNameHash] = i;
			progress.Update(i, g_Archetypes);
		}

		progress.End();

		if (collisions.size() > 0)
		{
			fprintf(stdout, "ERROR: %d archetype name hash collisions:\n", (int)collisions.size());

			for (int i = 0; i < (int)collisions.size(); i++)
			{
				fprintf(stdout, "\t### %s\n", collisions[i].c_str());
			}
		}
	}

	// build texture name/path maps
	{
		ProgressDisplay progress("building texture name/path maps");

		for (int i = 0; i < (int)g_Textures.size(); i++)
		{
			const aaTexture& texture = g_Textures[i];

			if (g_TextureNameMap.access(texture.m_textureNameHash) == NULL)
			{
				g_TextureNameMap[texture.m_textureNameHash] = (int)g_TextureNames.size();
				g_TextureNames.push_back(texture.m_textureName);
			}

			g_TexturePathMap[texture.m_texturePathHash] = i;

			progress.Update(i, g_Textures);
		}

		progress.End();
	}

	// build texture dictionary path map
	{
		ProgressDisplay progress("building texture dictionary path map");

		for (int i = 0; i < (int)g_TextureDictionaries.size(); i++)
		{
			const aaTextureDictionary& txd = g_TextureDictionaries[i];
			g_TextureDictionaryPathMap[txd.m_txdPathHash] = i;
			progress.Update(i, g_TextureDictionaries);
		}

		progress.End();
	}

	// build drawable path map
	{
		ProgressDisplay progress("building drawable path map");

		for (int i = 0; i < (int)g_Drawables.size(); i++)
		{
			const aaDrawable& drawable = g_Drawables[i];
			g_DrawablePathMap[drawable.m_drawablePathHash] = i;
			progress.Update(i, g_Drawables);
		}

		progress.End();
	}

	// build drawable dictionary path map
	{
		ProgressDisplay progress("building drawable dictionary path map");

		for (int i = 0; i < (int)g_DrawableDictionaries.size(); i++)
		{
			const aaDrawableDictionary& dwd = g_DrawableDictionaries[i];
			g_DrawableDictionaryPathMap[dwd.m_assetPathHash] = i;
			progress.Update(i, g_DrawableDictionaries);
		}

		progress.End();
	}

	// build fragment path map
	{
		ProgressDisplay progress("building fragment path map");

		for (int i = 0; i < (int)g_Fragments.size(); i++)
		{
			const aaFragment& fragment = g_Fragments[i];
			g_FragmentPathMap[fragment.m_assetPathHash] = i;
			progress.Update(i, g_Fragments);
		}

		progress.End();
	}

#if HIERARCHY
	// build drawable<->txd links
	{
		ProgressDisplay progress("building drawable<->txd links");

		for (int i = 0; i < (int)g_Drawables.size(); i++)
		{
			aaDrawable& drawable = g_Drawables[i];
			const int txdIndex = g_TextureDictionaryPathMap.accessindex(drawable.m_drawablePathHash);

			if (txdIndex != INDEX_NONE)
			{
				aaTextureDictionary& packedTxd = g_TextureDictionaries[txdIndex];
				drawable.m_packedTxd = &packedTxd;
				packedTxd.m_drawable = &drawable;
			}

			if (drawable.m_assetType == ASSET_TYPE_DWD)
			{
				const int dwdIndex = g_DrawableDictionaryPathMap.accessindex(drawable.m_assetPathHash);

				if (dwdIndex != INDEX_NONE)
				{
					aaDrawableDictionary& dwd = g_DrawableDictionaries[dwdIndex];
					drawable.m_dwd = &dwd;
					drawable.m_dwdNextDrawable = dwd.m_firstDrawable;
					dwd.m_firstDrawable = &drawable;
				}
			}
			else if (drawable.m_assetType == ASSET_TYPE_FRAGMENT)
			{
				const int fragmentIndex = g_FragmentPathMap.accessindex(drawable.m_assetPathHash);

				if (fragmentIndex != INDEX_NONE)
				{
					aaFragment& fragment = g_Fragments[fragmentIndex];
					drawable.m_fragment = &fragment;

					switch (drawable.m_drawableType)
					{
					case DRAWABLE_TYPE_FRAGMENT_COMMON_DRAWABLE:
						assert(fragment.m_commonDrawable == NULL);
						fragment.m_commonDrawable = &drawable;
						break;
					case DRAWABLE_TYPE_FRAGMENT_DAMAGED_DRAWABLE:
						assert(fragment.m_damagedDrawable == NULL);
						fragment.m_damagedDrawable = &drawable;
						break;
					case DRAWABLE_TYPE_FRAGMENT_CLOTH_DRAWABLE:
						assert(fragment.m_clothDrawable == NULL);
						fragment.m_clothDrawable = &drawable;
						break;
					default:
						assert(0);
					}
				}
			}

			progress.Update(i, g_Drawables);
		}

		progress.End();
	}

	// build material texture var<->drawable links
	{
		ProgressDisplay progress("building material texture var<->drawable links");

		for (int i = 0; i < (int)g_MaterialTextureVars.size(); i++)
		{
			aaMaterialTextureVar& mtv = g_MaterialTextureVars[i];
			char temp[256] = "";
			strcpy(temp, mtv.m_materialPath);
			char* s = strrchr(temp, '/');

			if (s)
			{
				*s = '\0';
				const u64 drawablePathHash = crc64_lower(temp);
				const int drawableIndex = g_DrawablePathMap.accessindex(drawablePathHash);

				if (drawableIndex != INDEX_NONE)
				{
					aaDrawable& drawable = g_Drawables[drawableIndex];
					mtv.m_drawable = &drawable;
					mtv.m_nextVar = drawable.m_firstMaterialVar;
					drawable.m_firstMaterialVar = &mtv;
				}
			}

			progress.Update(i, g_MaterialTextureVars);
		}

		progress.End();
	}

	// build texture dictionary hierarchy
	{
		ProgressDisplay progress("building texture dictionary hierarchy");

		for (int i = 0; i < (int)g_TextureDictionaries.size(); i++)
		{
			aaTextureDictionary& txd = g_TextureDictionaries[i];
			const int parentTxdIndex = g_TextureDictionaryPathMap.accessindex(txd.m_parentTxdPathHash);

			if (parentTxdIndex != INDEX_NONE)
			{
				aaTextureDictionary& parentTxd = g_TextureDictionaries[parentTxdIndex];
				txd.m_parentTxd = &parentTxd;
				txd.m_nextSiblingTxd = parentTxd.m_firstChildTxd;
				parentTxd.m_firstChildTxd = &txd;
			}

			progress.Update(i, g_TextureDictionaries);
		}

		progress.End();
	}

	// build texture<->txd links
	{
		ProgressDisplay progress("building texture<->txd links");

		for (int i = 0; i < (int)g_Textures.size(); i++)
		{
			aaTexture& texture = g_Textures[i];
			char temp[256] = "";
			strcpy(temp, texture.m_texturePath);
			char* s = strrchr(temp, '/');

			if (s)
			{
				*s = '\0';
				const u64 txdPathHash = crc64_lower(temp);
				const int txdIndex = g_TextureDictionaryPathMap.accessindex(txdPathHash);

				if (txdIndex != INDEX_NONE)
				{
					aaTextureDictionary& txd = g_TextureDictionaries[txdIndex];
					texture.m_txd = &txd;
					texture.m_nextSiblingTexture = txd.m_firstTexture;
					txd.m_firstTexture = &texture;
				}
			}

			progress.Update(i, g_Textures);
		}

		progress.End();
	}
#endif // HIERARCHY

	// build shader name map
	{
		FILE* preloadList = fopen(varString("%s/shaders/preload.list", RS_COMMON).c_str(), "r");

		if (preloadList)
		{
			ProgressDisplay progress("building shader name map");
			char line[1024] = "";

			while (fgets(line, sizeof(line), preloadList))
			{
				char* ext = strstr(line, ".fx");

				if (ext)
				{
					ext[0] = '\0';

					const char* shaderName     = _strlwr(line);
					const u32   shaderNameHash = crc32_lower(shaderName);

					g_ShaderNameMap[shaderNameHash] = (int)g_ShaderNames.size();
					g_ShaderNames.push_back(shaderName);
				}
			}

			fclose(preloadList);
			progress.End("done, found %d shaders", (int)g_ShaderNames.size());
		}
		else
		{
			fprintf(stderr, "failed to open preload.list\n");
		}
	}

	if (errFile)
	{
		fclose(errFile);
	}
}

static FILE* g_ColumnsFile = NULL;
#define WRITE_COLUMN(s) fprintf(g_ColumnsFile, "\t%c%c - %s\n", col < 26 ? ' ' : ('A' + (col/26 - 1)), 'A' + col%26, s); col++

static void WriteArchetypes(const char* path_)
{
	char path[256] = "";
	strcpy_ext(path, path_, ".csv");
	FILE* fp = fopen(path, "w");

	if (fp)
	{
		ProgressDisplay progress("writing %s", path_);

		for (int i = 0; i < (int)g_Archetypes.size(); i++)
		{
			const aaArchetype& it = g_Archetypes[i];

			enum
			{
				DEBUG_ARCHETYPE_PROXY_IS_PROP           = BIT(0),
				DEBUG_ARCHETYPE_PROXY_IS_TREE           = BIT(1),
				DEBUG_ARCHETYPE_PROXY_DONT_CAST_SHADOWS = BIT(2),
				DEBUG_ARCHETYPE_PROXY_SHADOW_PROXY      = BIT(3),
				DEBUG_ARCHETYPE_PROXY_HAS_UV_ANIMATION  = BIT(4),
			};

			char flagsStr[256] = "[";

			if (it.m_archetypeFlags & DEBUG_ARCHETYPE_PROXY_IS_PROP          ) { if (flagsStr[0]) { strcat(flagsStr, "+"); } strcat(flagsStr, "PROP"); }
			if (it.m_archetypeFlags & DEBUG_ARCHETYPE_PROXY_IS_TREE          ) { if (flagsStr[0]) { strcat(flagsStr, "+"); } strcat(flagsStr, "TREE"); }
			if (it.m_archetypeFlags & DEBUG_ARCHETYPE_PROXY_DONT_CAST_SHADOWS) { if (flagsStr[0]) { strcat(flagsStr, "+"); } strcat(flagsStr, "NOSHADOW"); }
			if (it.m_archetypeFlags & DEBUG_ARCHETYPE_PROXY_SHADOW_PROXY     ) { if (flagsStr[0]) { strcat(flagsStr, "+"); } strcat(flagsStr, "SHADOWPROXY"); }
			if (it.m_archetypeFlags & DEBUG_ARCHETYPE_PROXY_HAS_UV_ANIMATION ) { if (flagsStr[0]) { strcat(flagsStr, "+"); } strcat(flagsStr, "UVANIM"); }

			strcat(flagsStr, "]");

			static bool first = true;
			if (first && g_ColumnsFile)
			{
				char col = 0;
				fprintf(g_ColumnsFile, "aa_archetype\n");
				WRITE_COLUMN("archetypePath");
				WRITE_COLUMN("archetypeName");
				WRITE_COLUMN("flagsStr");
				WRITE_COLUMN("modelInfoType");
				WRITE_COLUMN("HDTexDistance");
				WRITE_COLUMN("lodDistance");
				WRITE_COLUMN("parentParticlePath");
				WRITE_COLUMN("HDTxdPath");
				WRITE_COLUMN("HDDrawableOrFragmentPath");
				WRITE_COLUMN("pedCompDwdPath");
				WRITE_COLUMN("pedPropDwdPath");
				fprintf(g_ColumnsFile, "\n");
				first = false;
			}

			fprintf(fp, "%s,%s,%s,%s,%.2f,%.2f,%s,%s,%s,%s,%s\n",
				it.m_archetypePath,
				it.m_archetypeName,
				flagsStr,
				it.m_modelInfoType < NELEM(g_modelInfoTypeStrings) ? g_modelInfoTypeStrings[it.m_modelInfoType] : "?",
				it.m_HDTexDistance,
				it.m_lodDistance,
				it.m_parentParticlePath,
				it.m_HDTxdPath,
				it.m_HDDrawableOrFragmentPath,
				it.m_pedCompDwdPath,
				it.m_pedPropDwdPath
			);

			progress.Update(i, g_Archetypes);
		}

		fclose(fp);
		progress.End();
	}
	else
	{
		fprintf(stderr, "failed to open %s for writing\n", path);
	}
}

static void WriteArchetypeInstances(const char* path_)
{
	char path[256] = "";
	strcpy_ext(path, path_, ".csv");
	FILE* fp = fopen(path, "w");

	if (fp)
	{
		ProgressDisplay progress("writing %s", path_);

		for (int i = 0; i < (int)g_ArchetypeInstances.size(); i++)
		{
			const aaArchetypeInstance& it = g_ArchetypeInstances[i];
			char lodPivotStr[256] = "";

			if (memcmp(it.m_matrixCol3, it.m_lodPivot, 3*sizeof(float)) != 0)
			{
				sprintf(lodPivotStr, "[%.2f %.2f %.2f]", it.m_lodPivot[0], it.m_lodPivot[1], it.m_lodPivot[2]);
			}

			static bool first = true;
			if (first && g_ColumnsFile)
			{
				char col = 0;
				fprintf(g_ColumnsFile, "aa_archetype_instance\n");
				WRITE_COLUMN("archetypePath");
				WRITE_COLUMN("archetypeName");
				WRITE_COLUMN("mapDataPath");
				WRITE_COLUMN("containingMLOName");
				WRITE_COLUMN("lodArchetypeName");
				WRITE_COLUMN("entityLodDistance");
				WRITE_COLUMN("entityBoundRadius");
				WRITE_COLUMN("worldPosition");
				WRITE_COLUMN("lodPivotStr");
				WRITE_COLUMN("lodParentRatio");
				WRITE_COLUMN("lodRadiusRatio");
				WRITE_COLUMN("lodWarningFlags");
				fprintf(g_ColumnsFile, "\n");
				first = false;
			}

			fprintf(fp, "%s,%s,%s,%s,%s,%d,%f,[%.2f %.2f %.2f],%s,%f,%f,0x%02x\n",
				it.m_archetypePath,
				it.m_archetypeName,
				it.m_mapDataPath,
				it.m_containingMLOName,
				it.m_lodArchetypeName,
				it.m_entityLodDistance,
				it.m_entityBoundRadius,
				it.m_matrixCol3[0], it.m_matrixCol3[1], it.m_matrixCol3[2],
				lodPivotStr,
				it.m_lodParentRatio,
				it.m_lodRadiusRatio,
				it.m_lodWarningFlags
			);

			progress.Update(i, g_ArchetypeInstances);
		}

		fclose(fp);
		progress.End();
	}
	else
	{
		fprintf(stderr, "failed to open %s for writing\n", path);
	}
}

static void WriteMapDatas(const char* path_)
{
	char path[256] = "";
	strcpy_ext(path, path_, ".csv");
	FILE* fp = fopen(path, "w");

	if (fp)
	{
		ProgressDisplay progress("writing %s", path_);

		for (int i = 0; i < (int)g_MapDatas.size(); i++)
		{
			const aaMapData& it = g_MapDatas[i];
			char contentFlagsStr[256] = "";

			enum eContentTypes
			{
				CONTENTFLAG_ENTITIES_HD             = BIT(0),
				CONTENTFLAG_ENTITIES_LOD            = BIT(1),
				CONTENTFLAG_ENTITIES_CONTAINERLOD   = BIT(2),
				CONTENTFLAG_MLO                     = BIT(3),
				CONTENTFLAG_BLOCKINFO               = BIT(4),
				CONTENTFLAG_OCCLUDER                = BIT(5),
				CONTENTFLAG_ENTITIES_USING_PHYSICS  = BIT(6),
				CONTENTFLAG_LOD_LIGHTS              = BIT(7),
				CONTENTFLAG_DISTANT_LOD_LIGHTS      = BIT(8),
				CONTENTFLAG_ENTITIES_CRITICAL       = BIT(9),
			};

			if (it.m_contentsFlags & CONTENTFLAG_ENTITIES_HD           ) { if (contentFlagsStr[0]) { strcat(contentFlagsStr, "+"); } strcat(contentFlagsStr, "HD"); }
			if (it.m_contentsFlags & CONTENTFLAG_ENTITIES_LOD          ) { if (contentFlagsStr[0]) { strcat(contentFlagsStr, "+"); } strcat(contentFlagsStr, "LOD"); }
			if (it.m_contentsFlags & CONTENTFLAG_ENTITIES_CONTAINERLOD ) { if (contentFlagsStr[0]) { strcat(contentFlagsStr, "+"); } strcat(contentFlagsStr, "CONT"); }
			if (it.m_contentsFlags & CONTENTFLAG_MLO                   ) { if (contentFlagsStr[0]) { strcat(contentFlagsStr, "+"); } strcat(contentFlagsStr, "MLO"); }
			if (it.m_contentsFlags & CONTENTFLAG_BLOCKINFO             ) { if (contentFlagsStr[0]) { strcat(contentFlagsStr, "+"); } strcat(contentFlagsStr, "BLOCK"); }
			if (it.m_contentsFlags & CONTENTFLAG_OCCLUDER              ) { if (contentFlagsStr[0]) { strcat(contentFlagsStr, "+"); } strcat(contentFlagsStr, "OCCL"); }
			if (it.m_contentsFlags & CONTENTFLAG_ENTITIES_USING_PHYSICS) { if (contentFlagsStr[0]) { strcat(contentFlagsStr, "+"); } strcat(contentFlagsStr, "PHYS"); }
			if (it.m_contentsFlags & CONTENTFLAG_LOD_LIGHTS            ) { if (contentFlagsStr[0]) { strcat(contentFlagsStr, "+"); } strcat(contentFlagsStr, "LODLIGHTS"); }
			if (it.m_contentsFlags & CONTENTFLAG_DISTANT_LOD_LIGHTS    ) { if (contentFlagsStr[0]) { strcat(contentFlagsStr, "+"); } strcat(contentFlagsStr, "DISTLODLIGHTS"); }
			if (it.m_contentsFlags & CONTENTFLAG_ENTITIES_CRITICAL     ) { if (contentFlagsStr[0]) { strcat(contentFlagsStr, "+"); } strcat(contentFlagsStr, "CRITICAL"); }

			static bool first = true;
			if (first && g_ColumnsFile)
			{
				char col = 0;
				fprintf(g_ColumnsFile, "aa_mapdata\n");
				WRITE_COLUMN("mapDataPath");
				WRITE_COLUMN("parentPath");
				WRITE_COLUMN("physicalSize (KB)");
				WRITE_COLUMN("physicalSize (bytes)");
				WRITE_COLUMN("virtualSize (KB)");
				WRITE_COLUMN("virtualSize (bytes)");
				WRITE_COLUMN("contentFlagsStr");
				fprintf(g_ColumnsFile, "\n");
				first = false;
			}

			fprintf(fp, "%s,%s,%.3fKB,%d,%.3fKB,%d,%s\n",
				it.m_mapDataPath,
				it.m_parentPath,
				(float)it.m_physicalSize/1024.0f,
				it.m_physicalSize,
				(float)it.m_virtualSize/1024.0f,
				it.m_virtualSize,
				contentFlagsStr
			);

			progress.Update(i, g_MapDatas);
		}

		fclose(fp);
		progress.End();
	}
	else
	{
		fprintf(stderr, "failed to open %s for writing\n", path);
	}
}

static void WriteTextures(const char* path_)
{
	char path[256] = "";
	strcpy_ext(path, path_, ".csv");
	FILE* fp = fopen(path, "w");

	if (fp)
	{
		ProgressDisplay progress("writing %s", path_);

		for (int i = 0; i < (int)g_Textures.size(); i++)
		{
			const aaTexture& it = g_Textures[i];
			char conversionFlagsStr[256] = "";
			char templateTypeStr[256] = "";
			char templateTypeStr2[256] = "";
			strcpy(conversionFlagsStr, rage::GetTextureConversionFlagsString(it.m_textureConversionFlags).c_str());
			strcpy(templateTypeStr, rage::GetTextureTemplateTypeString(it.m_textureTemplateType).c_str());

			if (templateTypeStr[0] == '\0')
			{
				strcpy(templateTypeStr, "...");
				strcpy(templateTypeStr2, "...");
			}
			else
			{
				char* plus = strchr(templateTypeStr, '+');
			
				if (plus)
				{
					strcpy(templateTypeStr2, plus + 1);
					*plus = '\0';
				}
			}

			static bool first = true;
			if (first && g_ColumnsFile)
			{
				char col = 0;
				fprintf(g_ColumnsFile, "aa_texture\n");
				WRITE_COLUMN("assetClass");
				WRITE_COLUMN("texturePath");
				WRITE_COLUMN("textureName");
				WRITE_COLUMN("dimensions");
				WRITE_COLUMN("mips");
				WRITE_COLUMN("format/swizzle");
				WRITE_COLUMN("pixelDataHash");
				WRITE_COLUMN("physicalSize (KB)");
				WRITE_COLUMN("physicalSize (bytes)");
				WRITE_COLUMN("conversionFlagsStr");
				WRITE_COLUMN("templateTypeStr");
				WRITE_COLUMN("templateTypeStr2");
				// extra:
				WRITE_COLUMN("minColour");
				WRITE_COLUMN("maxColour");
				WRITE_COLUMN("maxRGBDiff");
				fprintf(g_ColumnsFile, "\n");
				first = false;
			}

			fprintf(fp, "%s,%s,%s,%dx%d,mips=%d,%s-%c%c%c%c,crc=0x%08x,%.3fKB,%d,%s,%s,%s",
				g_assetClassStrings[GetAssetClassFromPath(it.m_texturePath)],
				it.m_texturePath,
				it.m_textureName,
				it.m_textureWidth, it.m_textureHeight,
				it.m_textureMips,
				g_textureFormatStrings[it.m_textureFormat], it.m_textureSwizzle[0], it.m_textureSwizzle[1], it.m_textureSwizzle[2], it.m_textureSwizzle[3],
				it.m_texturePixelDataHash,
				(float)it.m_texturePhysicalSize/1024.0f,
				it.m_texturePhysicalSize,
				conversionFlagsStr,
				templateTypeStr,
				templateTypeStr2
			);

			if (it.m_rgbmin != 0xffff)
			{
				const int rmin = (it.m_rgbmin >>  0) & 31;
				const int gmin = (it.m_rgbmin >>  5) & 63;
				const int bmin = (it.m_rgbmin >> 11) & 31;
				const int rmax = (it.m_rgbmax >>  0) & 31;
				const int gmax = (it.m_rgbmax >>  5) & 63;
				const int bmax = (it.m_rgbmax >> 11) & 31;

				fprintf(fp, ",min=[%d %d %d],max=[%d %d %d],%.1f\n",
					rmin, gmin, bmin,
					rmax, gmax, bmax,
					Max<float>(Max<float>((float)(rmax - rmin), 0.5f*(float)(gmax - gmin)), (float)(bmax - bmin))
				);
			}
			else
			{
				fprintf(fp, "\n");
			}

			progress.Update(i, g_Textures);
		}

		fclose(fp);
		progress.End();
	}
	else
	{
		fprintf(stderr, "failed to open %s for writing\n", path);
	}
}

static void WriteTextureDictionaries(const char* path_)
{
	char path[256] = "";
	strcpy_ext(path, path_, ".csv");
	FILE* fp = fopen(path, "w");

	if (fp)
	{
		ProgressDisplay progress("writing %s", path_);

		for (int i = 0; i < (int)g_TextureDictionaries.size(); i++)
		{
			const aaTextureDictionary& it = g_TextureDictionaries[i];

			static bool first = true;
			if (first && g_ColumnsFile)
			{
				char col = 0;
				fprintf(g_ColumnsFile, "aa_texture_dictionary\n");
				WRITE_COLUMN("assetClass");
				WRITE_COLUMN("txdPath");
				WRITE_COLUMN("physicalSize (KB)");
				WRITE_COLUMN("physicalSize (bytes)");
				WRITE_COLUMN("virtualSize (KB)");
				WRITE_COLUMN("virtualSize (bytes)");
				WRITE_COLUMN("numEntries");
				WRITE_COLUMN("parentTxdPath");
				fprintf(g_ColumnsFile, "\n");
				first = false;
			}

			fprintf(fp, "%s,%s,%.3fKB,%d,%.3fKB,%d,%d,%s\n",
				g_assetClassStrings[GetAssetClassFromPath(it.m_txdPath)],
				it.m_txdPath,
				(float)it.m_physicalSize/1024.0f,
				it.m_physicalSize,
				(float)it.m_virtualSize/1024.0f,
				it.m_virtualSize,
				it.m_numEntries,
				it.m_parentTxdPath
			);

			progress.Update(i, g_TextureDictionaries);
		}

		fclose(fp);
		progress.End();
	}
	else
	{
		fprintf(stderr, "failed to open %s for writing\n", path);
	}
}

static void WriteDrawables(const char* path_)
{
	char path[256] = "";
	strcpy_ext(path, path_, ".csv");
	FILE* fp = fopen(path, "w");

	if (fp)
	{
		ProgressDisplay progress("writing %s", path_);

		for (int i = 0; i < (int)g_Drawables.size(); i++)
		{
			const aaDrawable& it = g_Drawables[i];
			const char* drawableTypeStr = "?";

			switch (it.m_drawableType)
			{
			case DRAWABLE_TYPE_NONE                      : drawableTypeStr = "none"; break; // ?
			case DRAWABLE_TYPE_DWD_ENTRY                 : drawableTypeStr = ""; break;
			case DRAWABLE_TYPE_DRAWABLE                  : drawableTypeStr = ""; break;
			case DRAWABLE_TYPE_FRAGMENT_COMMON_DRAWABLE  : drawableTypeStr = ""; break;
			case DRAWABLE_TYPE_FRAGMENT_DAMAGED_DRAWABLE : drawableTypeStr = "frag_damaged"; break;
			case DRAWABLE_TYPE_FRAGMENT_EXTRA_DRAWABLE   : drawableTypeStr = "frag_extra"; break;
			case DRAWABLE_TYPE_FRAGMENT_CLOTH_DRAWABLE   : drawableTypeStr = "frag_cloth"; break; 
			case DRAWABLE_TYPE_PARTICLE_MODEL            : drawableTypeStr = "particle_model"; break; 
			}

			bool bDrawableNotUsed = false;

			if (it.m_archetypeNameHash == 0)
			{
				// well, it might be used by ped/vehicle/weapon in another way (e.g. vehicle HD fragment)
				// for now, let's just ignore if the asset class indicates ped/vehicle/weapon
				const eAssetClass ac = GetAssetClassFromPath(it.m_drawablePath);

				if (ac == ASSET_CLASS_PED || ac == ASSET_CLASS_VEHICLEMOD || ac == ASSET_CLASS_MODEL)
				{
					// ok ..
				}
				else if (ac == ASSET_CLASS_WEAPON || ac == ASSET_CLASS_VEHICLE)
				{
					if (AssetPathIsHDDrawableOrFragment(it.m_drawablePath))
					{
						// ok
					}
					else
					{
						bDrawableNotUsed = true;
					}
				}
			}

			static bool first = true;
			if (first && g_ColumnsFile)
			{
				char col = 0;
				fprintf(g_ColumnsFile, "aa_drawable\n");
				WRITE_COLUMN("assetClass");
				WRITE_COLUMN("drawablePath");
				WRITE_COLUMN("physicalSize (KB)");
				WRITE_COLUMN("physicalSize (bytes)");
				WRITE_COLUMN("virtualSize (KB)");
				WRITE_COLUMN("virtualSize (bytes)");
				WRITE_COLUMN("parentTxdPath");
				WRITE_COLUMN("drawableType");
				WRITE_COLUMN("numTriangles (LOD_HIGH)");
				WRITE_COLUMN("numTriangles (LOD_MED)");
				WRITE_COLUMN("numTriangles (LOD_LOW)");
				WRITE_COLUMN("numTriangles (LOD_VLOW)");
				WRITE_COLUMN("tintDataSize");
				WRITE_COLUMN("numLights (with texture)");
				WRITE_COLUMN("not used by archetype?");
				fprintf(g_ColumnsFile, "\n");
				first = false;
			}

			fprintf(fp, "%s,%s,%.3fKB,%d,%.3fKB,%d,%s,%s,%d,%d,%d,%d,%d,%d(%d),%s\n",
				g_assetClassStrings[GetAssetClassFromPath(it.m_drawablePath)],
				it.m_drawablePath,
				(float)it.m_physicalSize/1024.0f,
				it.m_physicalSize,
				(float)it.m_virtualSize/1024.0f,
				it.m_virtualSize,
				it.m_parentTxdPath,
				drawableTypeStr,
				it.m_numTriangles[0],
				it.m_numTriangles[1],
				it.m_numTriangles[2],
				it.m_numTriangles[3],
				it.m_tintDataSize,
				it.m_numLights, it.m_numLightsWithTextures,
				bDrawableNotUsed ? "NO ARCHETYPE" : ""
			);

			progress.Update(i, g_Drawables);
		}

		fclose(fp);
		progress.End();
	}
	else
	{
		fprintf(stderr, "failed to open %s for writing\n", path);
	}
}

static void WriteDrawableDictionaries(const char* path_)
{
	char path[256] = "";
	strcpy_ext(path, path_, ".csv");
	FILE* fp = fopen(path, "w");

	if (fp)
	{
		ProgressDisplay progress("writing %s", path_);

		for (int i = 0; i < (int)g_DrawableDictionaries.size(); i++)
		{
			const aaDrawableDictionary& it = g_DrawableDictionaries[i];

			static bool first = true;
			if (first && g_ColumnsFile)
			{
				char col = 0;
				fprintf(g_ColumnsFile, "aa_drawable_dictionary\n");
				WRITE_COLUMN("assetClass");
				WRITE_COLUMN("assetPath");
				WRITE_COLUMN("physicalSize (KB)");
				WRITE_COLUMN("physicalSize (bytes)");
				WRITE_COLUMN("virtualSize (KB)");
				WRITE_COLUMN("virtualSize (bytes)");
				WRITE_COLUMN("numEntries");
				WRITE_COLUMN("parentTxdPath");
				fprintf(g_ColumnsFile, "\n");
				first = false;
			}

			fprintf(fp, "%s,%s,%.3fKB,%d,%.3fKB,%d,%d,%s\n",
				g_assetClassStrings[GetAssetClassFromPath(it.m_assetPath)],
				it.m_assetPath,
				(float)it.m_physicalSize/1024.0f,
				it.m_physicalSize,
				(float)it.m_virtualSize/1024.0f,
				it.m_virtualSize,
				it.m_numEntries,
				it.m_parentTxdPath
			);

			progress.Update(i, g_DrawableDictionaries);
		}

		fclose(fp);
		progress.End();
	}
	else
	{
		fprintf(stderr, "failed to open %s for writing\n", path);
	}
}

static void WriteFragments(const char* path_)
{
	char path[256] = "";
	strcpy_ext(path, path_, ".csv");
	FILE* fp = fopen(path, "w");

	if (fp)
	{
		ProgressDisplay progress("writing %s", path_);

		for (int i = 0; i < (int)g_Fragments.size(); i++)
		{
			const aaFragment& it = g_Fragments[i];
			char clothAndGlassStr[256] = "";

			if (it.m_numEnvCloths > 0)
			{
				strcat(clothAndGlassStr, varString("(%d env cloths)", it.m_numEnvCloths).c_str());
			}

			if (it.m_numCharCloths > 0)
			{
				strcat(clothAndGlassStr, varString("(%d char cloths)", it.m_numCharCloths).c_str());
			}

			if (it.m_numGlassPanes > 0)
			{
				strcat(clothAndGlassStr, varString("(%d glass panes)", it.m_numGlassPanes).c_str());
			}

			static bool first = true;
			if (first && g_ColumnsFile)
			{
				char col = 0;
				fprintf(g_ColumnsFile, "aa_fragment\n");
				WRITE_COLUMN("assetClass");
				WRITE_COLUMN("assetPath");
				WRITE_COLUMN("physicalSize (KB)");
				WRITE_COLUMN("physicalSize (bytes)");
				WRITE_COLUMN("virtualSize (KB)");
				WRITE_COLUMN("virtualSize (bytes)");
				WRITE_COLUMN("parentTxdPath");
				WRITE_COLUMN("tintDataSize");
				WRITE_COLUMN("numLights (with texture)");
				WRITE_COLUMN("clothAndGlass");
				fprintf(g_ColumnsFile, "\n");
				first = false;
			}

			fprintf(fp, "%s,%s,%.3fKB,%d,%.3fKB,%d,%s,%d,%d(%d),%s\n",
				g_assetClassStrings[GetAssetClassFromPath(it.m_assetPath)],
				it.m_assetPath,
				(float)it.m_physicalSize/1024.0f,
				it.m_physicalSize,
				(float)it.m_virtualSize/1024.0f,
				it.m_virtualSize,
				it.m_parentTxdPath,
				it.m_tintDataSize,
				it.m_numLights, it.m_numLightsWithTextures,
				clothAndGlassStr
			);

			progress.Update(i, g_Fragments);
		}

		fclose(fp);
		progress.End();
	}
	else
	{
		fprintf(stderr, "failed to open %s for writing\n", path);
	}
}

static void WriteLights(const char* path_)
{
	char path[256] = "";
	strcpy_ext(path, path_, ".csv");
	FILE* fp = fopen(path, "w");

	if (fp)
	{
		ProgressDisplay progress("writing %s", path_);

		for (int i = 0; i < (int)g_Lights.size(); i++)
		{
			const aaLight& it = g_Lights[i];

			// ==================================================================================
			// WARNING!! IF YOU CHANGE THE FIELDS HERE YOU MUST ALSO KEEP IN SYNC WITH:
			// 
			// CLightAttrDef - %RS_CODEBRANCH%\game\scene\loader\MapData_Extensions.psc
			// CLightAttr    - %RS_CODEBRANCH%\game\scene\2dEffect.h
			// CLightAttr    - %RS_CODEBRANCH%\rage\framework\tools\src\cli\ragebuilder\gta_res.h
			// WriteLight    - %RS_CODEBRANCH%\game\debug\AssetAnalysis\AssetAnalysis.cpp
			// WriteLights   - %RS_CODEBRANCH%\tools\AssetAnalysisTool\src\AssetAnalysisTool.cpp
			// aaLightRecord - %RS_CODEBRANCH%\tools\AssetAnalysisTool\src\AssetAnalysisTool.h
			// ==================================================================================

			static bool first = true;
			if (first && g_ColumnsFile)
			{
				char col = 0;
				fprintf(g_ColumnsFile, "aa_light\n");
				WRITE_COLUMN("assetClass");
				WRITE_COLUMN("drawablePath/index");
				WRITE_COLUMN("lightType");
				WRITE_COLUMN("flashiness");
				WRITE_COLUMN("flags");
				WRITE_COLUMN(""); // falloff=
				WRITE_COLUMN("falloff");
				WRITE_COLUMN("falloffExponent");
				WRITE_COLUMN(""); // intensity=
				WRITE_COLUMN("intensity");
				WRITE_COLUMN(""); // colour=
				WRITE_COLUMN("colour");
				WRITE_COLUMN("maxIntensity");
				WRITE_COLUMN(""); // corona=
				WRITE_COLUMN("coronaSize");
				WRITE_COLUMN("coronaIntensity");
				WRITE_COLUMN("coronaZBias");
				WRITE_COLUMN(""); // vol=
				WRITE_COLUMN("volSizeScale");
				WRITE_COLUMN("volIntensity");
				WRITE_COLUMN("volOuterIntensity");
				WRITE_COLUMN("volOuterColour");
				WRITE_COLUMN(""); // spot=
				WRITE_COLUMN("coneOuterAngle");
				WRITE_COLUMN("coneInnerAngle");
				WRITE_COLUMN(""); // extent=
				WRITE_COLUMN("extents");
				WRITE_COLUMN(""); // fade=
				WRITE_COLUMN("lightFadeDistance");
				WRITE_COLUMN("shadowFadeDistance");
				WRITE_COLUMN("specularFadeDistance");
				WRITE_COLUMN("volumetricFadeDistance");
				WRITE_COLUMN(""); // texture=
				WRITE_COLUMN("projectedTexturePath");
				WRITE_COLUMN(""); // shadow=
				WRITE_COLUMN("shadowNearClip");
				WRITE_COLUMN("shadowBlur");
				fprintf(g_ColumnsFile, "\n");
				first = false;
			}

			fprintf(fp, "%s,%s/light[%d],%s,%s,0x%08x,falloff=,%f,%f,intensity=,%f,colour=,[%d %d %d],%f,corona=,%f,%f,%f,vol=,%f,%f,%f,[%d %d %d],spot=,%f,%f,extent=,%f,fade=,%d,%d,%d,%d,texture=,%s,shadow=,%f,%d\n",
				g_assetClassStrings[GetAssetClassFromPath(it.m_drawablePath)],
				it.m_drawablePath, it.m_lightIndexInDrawable,
				g_lightTypeStrings[it.m_lightType],
				g_lightFlashninessStrings[it.m_flashiness],
				it.m_flags,
				// falloff=
				it.m_falloff,
				it.m_falloffExponent,
				// intensity=
				it.m_intensity,
				// colour=
				it.m_colour[0], it.m_colour[1], it.m_colour[2],
				it.m_intensity*(float)Max<u8>(it.m_colour[0], it.m_colour[1], it.m_colour[2])/255.0f, // max intensity
				// corona=
				it.m_coronaSize,
				it.m_coronaIntensity,
				it.m_coronaZBias,
				// vol=
				it.m_volSizeScale,
				it.m_volIntensity,
				it.m_volOuterIntensity,
				it.m_volOuterColour[0], it.m_volOuterColour[1], it.m_volOuterColour[2],
				// spot=
				it.m_coneOuterAngle,
				it.m_coneInnerAngle,
				// extent=
				it.m_extents[0],
				// fade=
				it.m_lightFadeDistance,
				it.m_shadowFadeDistance,
				it.m_specularFadeDistance,
				it.m_volumetricFadeDistance,
				// texture=
				it.m_projectedTexturePath,
				// shadow=
				it.m_shadowNearClip,
				it.m_shadowBlur
			);

			progress.Update(i, g_Lights);
		}

		fclose(fp);
		progress.End();
	}
	else
	{
		fprintf(stderr, "failed to open %s for writing\n", path);
	}
}

static void WriteParticles(const char* path_)
{
	char path[256] = "";
	strcpy_ext(path, path_, ".csv");
	FILE* fp = fopen(path, "w");

	if (fp)
	{
		ProgressDisplay progress("writing %s", path_);

		for (int i = 0; i < (int)g_Particles.size(); i++)
		{
			const aaParticle& it = g_Particles[i];

			static bool first = true;
			if (first && g_ColumnsFile)
			{
				char col = 0;
				fprintf(g_ColumnsFile, "aa_particle\n");
				WRITE_COLUMN("assetClass");
				WRITE_COLUMN("assetPath");
				WRITE_COLUMN("physicalSize (KB)");
				WRITE_COLUMN("physicalSize (bytes)");
				WRITE_COLUMN("virtualSize (KB)");
				WRITE_COLUMN("virtualSize (bytes)");
				fprintf(g_ColumnsFile, "\n");
				first = false;
			}

			fprintf(fp, "%s,%s,%.3fKB,%d,%.3fKB,%d\n",
				g_assetClassStrings[GetAssetClassFromPath(it.m_assetPath)],
				it.m_assetPath,
				(float)it.m_physicalSize/1024.0f,
				it.m_physicalSize,
				(float)it.m_virtualSize/1024.0f,
				it.m_virtualSize
			);

			progress.Update(i, g_Particles);
		}

		fclose(fp);
		progress.End();
	}
	else
	{
		fprintf(stderr, "failed to open %s for writing\n", path);
	}
}

static void WriteMaterials(const char* path_)
{
	char path[256] = "";
	strcpy_ext(path, path_, ".csv");
	FILE* fp = fopen(path, "w");

	if (fp)
	{
		ProgressDisplay progress("writing %s", path_);

		for (int i = 0; i < (int)g_Materials.size(); i++)
		{
			const aaMaterial& it = g_Materials[i];

			const char* diffTexPath = it.m_diffTexPath;
			const char* bumpTexPath = it.m_bumpTexPath;
			const char* specTexPath = it.m_specTexPath;

			if (0) // if textures are packed in the drawable, just show the texture name
			{
				const char* drawablePathEnd = strrchr(it.m_materialPath, '[');

				if (drawablePathEnd && drawablePathEnd[-1] == '/')
				{
					const ptrdiff_t drawablePathLen = drawablePathEnd - it.m_materialPath;

					
					if (strncmp(diffTexPath, it.m_materialPath, drawablePathLen) == 0) { diffTexPath += drawablePathLen; }
					if (strncmp(bumpTexPath, it.m_materialPath, drawablePathLen) == 0) { bumpTexPath += drawablePathLen; }
					if (strncmp(specTexPath, it.m_materialPath, drawablePathLen) == 0) { specTexPath += drawablePathLen; }
				}
			}

			static bool first = true;
			if (first && g_ColumnsFile)
			{
				char col = 0;
				fprintf(g_ColumnsFile, "aa_material\n");
				WRITE_COLUMN("assetClass");
				WRITE_COLUMN("materialPath");
				WRITE_COLUMN("shaderName");
				WRITE_COLUMN(""); // diff=
				WRITE_COLUMN("diffTexPath");
				WRITE_COLUMN(""); // bump=
				WRITE_COLUMN("bumpiness");
				WRITE_COLUMN("bumpTexPath");
				WRITE_COLUMN(""); // spec=
				WRITE_COLUMN("specIntensity");
				WRITE_COLUMN("specIntensityMask");
				WRITE_COLUMN("specTexPath");
				fprintf(g_ColumnsFile, "\n");
				first = false;
			}

			fprintf(fp, "%s,%s,%s,diff=,%s,bump=,%f,%s,spec=,%f,[%.2f %.2f %.2f],%s\n",
				g_assetClassStrings[GetAssetClassFromPath(it.m_materialPath)],
				it.m_materialPath,
				it.m_shaderName,
				// diff=
				diffTexPath,
				// bump=
				it.m_bumpiness,
				bumpTexPath,
				// spec=
				it.m_specIntensity,
				it.m_specIntensityMask[0], it.m_specIntensityMask[1], it.m_specIntensityMask[2],
				specTexPath
			);

			progress.Update(i, g_Materials);
		}

		fclose(fp);
		progress.End();
	}
	else
	{
		fprintf(stderr, "failed to open %s for writing\n", path);
	}
}

static void WriteMaterialTextureVars(const char* path_)
{
	char path[256] = "";
	strcpy_ext(path, path_, ".csv");
	FILE* fp = fopen(path, "w");

	if (fp)
	{
		ProgressDisplay progress("writing %s", path_);

		for (int i = 0; i < (int)g_MaterialTextureVars.size(); i++)
		{
			const aaMaterialTextureVar& it = g_MaterialTextureVars[i];

			static bool first = true;
			if (first && g_ColumnsFile)
			{
				char col = 0;
				fprintf(g_ColumnsFile, "aa_material_texture_var\n");
				WRITE_COLUMN("assetClass");
				WRITE_COLUMN("materialPath");
				WRITE_COLUMN("shaderName");
				WRITE_COLUMN("textureUsage");
				WRITE_COLUMN("materialVarName");
				WRITE_COLUMN("texturePath");
				fprintf(g_ColumnsFile, "\n");
				first = false;
			}

			fprintf(fp, "%s,%s,%s,%s,%s,%s\n",
				g_assetClassStrings[GetAssetClassFromPath(it.m_materialPath)],
				it.m_materialPath,
				it.m_shaderName,
				g_textureUsageStrings[GetTextureUsageForMaterialVarName(it.m_materialVarName, it.m_shaderName)],
				it.m_materialVarName,
				it.m_texturePath
			);

			progress.Update(i, g_MaterialTextureVars);
		}

		fclose(fp);
		progress.End();
	}
	else
	{
		fprintf(stderr, "failed to open %s for writing\n", path);
	}
}

// ================================================================================================

static void WriteReportMaterialShaderUsage(const char* path)
{
	int* shaderUsageCounts = new int[(int)g_ShaderNames.size()];
	int shaderNamesUnresolved = 0;

	// mark shader usage
	{
		ProgressDisplay progress("marking material shader usage");

		memset(shaderUsageCounts, 0, (int)g_ShaderNames.size()*sizeof(int));

		for (int i = 0; i < (int)g_Materials.size(); i++)
		{
			const aaMaterial& mat = g_Materials[i];
			const int shaderIndex = g_ShaderNameMap.accessindex(mat.m_shaderNameHash);

			if (shaderIndex != INDEX_NONE)
			{
				shaderUsageCounts[shaderIndex]++;
			}
			else
			{
				shaderNamesUnresolved++;
			}

			progress.Update(i, g_Materials);
		}

		progress.End();
	}

	// write report
	{
		FILE* fp = fopen(path, "w");

		if (fp)
		{
			ProgressDisplay progress("writing %s", path);
			int count = 0;

			for (int i = 0; i < (int)g_ShaderNames.size(); i++)
			{
				const char* shaderName = g_ShaderNames[i].c_str();
				bool bShaderIsUsedByCode = false;

				if (strcmp(shaderName, "light_draw"           ) == 0 ||
					strcmp(shaderName, "light_frag"           ) == 0 ||
					strcmp(shaderName, "im"                   ) == 0 ||
					strcmp(shaderName, "glass_breakable_crack") == 0 ||
					strstr(shaderName, "vfx_decal"            ) != NULL ||
					strstr(shaderName, "projtex"              ) != NULL ||
					strcmp(shaderName, "grass"                ) == 0 ||
					strcmp(shaderName, "grass_fur"            ) == 0 ||
					strcmp(shaderName, "grass_fur_lod"        ) == 0 ||
					strcmp(shaderName, "radar"                ) == 0 ||
					strcmp(shaderName, "billboard_nobump"     ) == 0 ||
					strcmp(shaderName, "ptxgpu_update"        ) == 0 ||
					strcmp(shaderName, "ptxgpu_render"        ) == 0 ||
					strcmp(shaderName, "rmptfx_default"       ) == 0 ||
					strcmp(shaderName, "rmptfx_litsprite"     ) == 0 ||
					strcmp(shaderName, "postfx"               ) == 0 ||
					strcmp(shaderName, "water"                ) == 0 ||
					strcmp(shaderName, "watertex"             ) == 0 ||
					strcmp(shaderName, "damage_blits"         ) == 0 ||
					strcmp(shaderName, "rage_bink"            ) == 0 ||
					strcmp(shaderName, "rage_curvedmodel"     ) == 0 ||
					strcmp(shaderName, "rage_fastmipmap"      ) == 0 ||
					strcmp(shaderName, "scaleform_shaders"    ) == 0)
				{
					bShaderIsUsedByCode = true;
				}

				if (shaderUsageCounts[i] > 0 || !bShaderIsUsedByCode)
				{
					static bool first = true;
					if (first && g_ColumnsFile)
					{
						char col = 0;
						fprintf(g_ColumnsFile, "%s\n", path);
						WRITE_COLUMN("shaderName");
						WRITE_COLUMN("usageCount");
						WRITE_COLUMN("used by code?");
						fprintf(g_ColumnsFile, "\n");
						first = false;
					}

					fprintf(fp, "%s,%d,%s\n", g_ShaderNames[i].c_str(), shaderUsageCounts[i], bShaderIsUsedByCode ? "code" : "");
				}

				if (shaderUsageCounts[i] == 0 && !bShaderIsUsedByCode)
				{
					count++;
				}

				progress.Update(i, g_ShaderNames);
			}

			fclose(fp);
			progress.End("done, found %d (%.2f%%)", count, 100.0f*(float)count/(float)g_ShaderNames.size());
		}
		else
		{
			fprintf(stderr, "failed to open %s for writing\n", path);
		}
	}

	delete[] shaderUsageCounts;
}

static void WriteReportArchetypesNotPlacedInMapData(const char* path)
{
	if (g_ArchetypeInstances.size() == 0) // can't generate this report if we don't have instances
	{
		return;
	}

	const int archetypeFlagsSize = ((int)g_Archetypes.size() + 7)/8;
	u8*       archetypeFlags     = new u8[archetypeFlagsSize];

	int archetypeNamesUnresolved = 0;

	// mark instanced archetypes
	{
		ProgressDisplay progress("marking instanced archetypes");

		memset(archetypeFlags, 0, archetypeFlagsSize);

		for (int i = 0; i < (int)g_ArchetypeInstances.size(); i++)
		{
			const aaArchetypeInstance& ai = g_ArchetypeInstances[i]; 
			const int archetypeIndex = g_ArchetypeNameMap.accessindex(ai.m_archetypeNameHash);

			if (archetypeIndex != INDEX_NONE)
			{
				archetypeFlags[archetypeIndex/8] |= BIT(archetypeIndex%8);
			}
			else
			{
				archetypeNamesUnresolved++;
			}

			progress.Update(i, g_ArchetypeInstances);
		}

		progress.End();
	}

	// write report
	{
		FILE* fp = fopen(path, "w");

		if (fp)
		{
			ProgressDisplay progress("writing %s", path);
			int count = 0;

			for (int i = 0; i < (int)g_Archetypes.size(); i++)
			{
				if ((archetypeFlags[i/8] & BIT(i%8)) == 0)
				{
					const aaArchetype& arch = g_Archetypes[i]; 

					if (arch.m_modelInfoType != MI_TYPE_VEHICLE &&
						arch.m_modelInfoType != MI_TYPE_PED &&
						strstr(arch.m_archetypeName, "_cs_") == NULL &&
						strstr(arch.m_archetypeName, "des_") == NULL &&
						strstr(arch.m_archetypeName, "_ilev_") == NULL)
					{
						static bool first = true;
						if (first && g_ColumnsFile)
						{
							char col = 0;
							fprintf(g_ColumnsFile, "%s\n", path);
							WRITE_COLUMN("archetypePath");
							WRITE_COLUMN("archetypeName");
							fprintf(g_ColumnsFile, "\n");
							first = false;
						}

						fprintf(fp, "%s,%s\n",
							arch.m_archetypePath,
							arch.m_archetypeName
						);

						count++;
					}
				}

				progress.Update(i, g_Archetypes);
			}

			fclose(fp);
			progress.End("done, found %d (%.2f%%)", count, 100.0f*(float)count/(float)g_Archetypes.size());
		}
		else
		{
			fprintf(stderr, "failed to open %s for writing\n", path);
		}
	}

	delete[] archetypeFlags;

	if (archetypeNamesUnresolved > 0)
	{
		fprintf(stderr, "%d of %d archetype instances could not resolve archetype name\n", archetypeNamesUnresolved, (int)g_ArchetypeInstances.size());
	}
}

static void WriteReportDrawablesNotUsed(const char* path)
{
	// write report
	{
		FILE* fp = fopen(path, "w");

		if (fp)
		{
			ProgressDisplay progress("writing %s", path);
			int count = 0;

			for (int i = 0; i < (int)g_Drawables.size(); i++)
			{
				const aaDrawable& drawable = g_Drawables[i];

				if (drawable.m_archetypeNameHash == 0)
				{
					// well, it might be used by ped/vehicle/weapon in another way (e.g. vehicle HD fragment)
					// for now, let's just ignore if the asset class indicates ped/vehicle/weapon
					const eAssetClass ac = GetAssetClassFromPath(drawable.m_drawablePath);

					if (ac == ASSET_CLASS_PED || ac == ASSET_CLASS_VEHICLEMOD || ac == ASSET_CLASS_MODEL)
					{
						continue;
					}
					else if (ac == ASSET_CLASS_WEAPON || ac == ASSET_CLASS_VEHICLE)
					{
						if (AssetPathIsHDDrawableOrFragment(drawable.m_drawablePath))
						{
							continue;
						}
					}

					static bool first = true;
					if (first && g_ColumnsFile)
					{
						char col = 0;
						fprintf(g_ColumnsFile, "%s\n", path);
						WRITE_COLUMN("drawablePath");
						fprintf(g_ColumnsFile, "\n");
						first = false;
					}

					fprintf(fp, "%s\n", drawable.m_drawablePath);
					count++;
				}

				progress.Update(i, g_Drawables);
			}

			fclose(fp);
			progress.End("done, found %d (%.2f%%)", count, 100.0f*(float)count/(float)g_Drawables.size());
		}
		else
		{
			fprintf(stderr, "failed to open %s for writing\n", path);
		}
	}
}

static void WriteReportTexturesNotUsedByMaterials(const char* path)
{
	const int textureNameCount       = (int)g_TextureNames.size();
	const int texturePathCount       = (int)g_Textures.size();
	const int textureNameFlagsSize   = (textureNameCount + 7)/8;
	const int texturePathFlagsSize   = (texturePathCount + 7)/8;
	u8*       textureNameFlags       = new u8[textureNameFlagsSize];
	u8*       texturePathFlags       = new u8[texturePathFlagsSize];
	int       textureNamesUnresolved = 0;
	int       texturePathsUnresolved = 0;

	// mark textures which are used by materials
	{
		ProgressDisplay progress("marking textures used by materials");

		memset(textureNameFlags, 0, textureNameFlagsSize);
		memset(texturePathFlags, 0, texturePathFlagsSize);

		for (int i = 0; i < (int)g_MaterialTextureVars.size(); i++)
		{
			const aaMaterialTextureVar& mtv = g_MaterialTextureVars[i];

			const int textureNameIndex = g_TextureNameMap.accessindex(mtv.m_textureNameHash);
			const int texturePathIndex = g_TexturePathMap.accessindex(mtv.m_texturePathHash);

			if (textureNameIndex != INDEX_NONE) { textureNameFlags[textureNameIndex/8] |= BIT(textureNameIndex%8); } else { textureNamesUnresolved++; }
			if (texturePathIndex != INDEX_NONE) { texturePathFlags[texturePathIndex/8] |= BIT(texturePathIndex%8); } else { texturePathsUnresolved++; }

			progress.Update(i, g_MaterialTextureVars);
		}

		progress.End();
	}

	// write report (names)
	{
		char path2[256] = "";
		strcpy_ext(path2, path, "(names).csv");
		FILE* fp = fopen(path2, "w");

		if (fp)
		{
			ProgressDisplay progress("writing %s", path2);
			int count = 0;

			for (int i = 0; i < (int)g_TextureNames.size(); i++)
			{
				if ((textureNameFlags[i/8] & BIT(i%8)) == 0)
				{
					static bool first = true;
					if (first && g_ColumnsFile)
					{
						char col = 0;
						fprintf(g_ColumnsFile, "%s\n", path2);
						WRITE_COLUMN("textureName");
						fprintf(g_ColumnsFile, "\n");
						first = false;
					}

					fprintf(fp, "%s\n", g_TextureNames[i]);
					count++;
				}

				progress.Update(i, g_TextureNames);
			}

			progress.End("done, found %d (%.2f%%)", count, 100.0f*(float)count/(float)g_TextureNames.size());
			fclose(fp);
		}
		else
		{
			fprintf(stderr, "failed to open %s for writing\n", path2);
		}
	}

	// write report (paths)
	{
		FILE* fp = fopen(path, "w");

		if (fp)
		{
			ProgressDisplay progress("writing %s", path);
			int count = 0;

			for (int i = 0; i < (int)g_Textures.size(); i++)
			{
				const aaTexture& texture = g_Textures[i];

				if ((texturePathFlags[i/8] & BIT(i%8)) == 0)
				{
					const eAssetClass ac = GetAssetClassFromPath(texture.m_texturePath);

					if (ac == ASSET_CLASS_DEFAULT     ||
						ac == ASSET_CLASS_PED         ||
						ac == ASSET_CLASS_PED_OVERLAY ||
						ac == ASSET_CLASS_SCALEFORM   ||
						ac == ASSET_CLASS_PARTICLE    || // TODO -- once we support particle records we can remove this condition
						ac == ASSET_CLASS_DECAL       ||
						ac == ASSET_CLASS_TEXTURE     ||
						false)
					{
						continue;
					}

					if (AssetPathIsHDTxd(texture.m_texturePath))
					{
						continue;
					}

					const int textureNameIndex = g_TextureNameMap.accessindex(texture.m_textureNameHash);
					const bool bPotentialDuplicate = textureNameIndex != INDEX_NONE && (textureNameFlags[textureNameIndex/8] & BIT(textureNameIndex%8));

					static bool first = true;
					if (first && g_ColumnsFile)
					{
						char col = 0;
						fprintf(g_ColumnsFile, "%s\n", path);
						WRITE_COLUMN("texturePath");
						WRITE_COLUMN("textureName");
						WRITE_COLUMN("duplicate ?");
						WRITE_COLUMN("dimensions");
						WRITE_COLUMN("physicalSize (KB)");
						WRITE_COLUMN("physicalSize (bytes)");
						fprintf(g_ColumnsFile, "\n");
						first = false;
					}

					fprintf(fp, "%s,%s,%s,%dx%d,%.3fKB,%d\n",
						texture.m_texturePath,
						texture.m_textureName,
						bPotentialDuplicate ? "duplicate?" : "",
						texture.m_textureWidth, texture.m_textureHeight,
						(float)texture.m_texturePhysicalSize/1024.0f,
						texture.m_texturePhysicalSize
					);
					count++;
				}

				progress.Update(i, g_Textures);
			}

			progress.End("done, found %d (%.2f%%)", count, 100.0f*(float)count/(float)g_Textures.size());
			fclose(fp);
		}
		else
		{
			fprintf(stderr, "failed to open %s for writing\n", path);
		}
	}

	delete[] textureNameFlags;
	delete[] texturePathFlags;

	if (textureNamesUnresolved > 0)
	{
		fprintf(stderr, "%d of %d material texture vars could not resolve texture name\n", textureNamesUnresolved, (int)g_MaterialTextureVars.size());
	}

	if (texturePathsUnresolved > 0)
	{
		fprintf(stderr, "%d of %d material texture vars could not resolve texture path\n", texturePathsUnresolved, (int)g_MaterialTextureVars.size());
	}
}

static void WriteReportShiftedNormalMaps(const char* path)
{
	const int texturesUsedAsNormalMapsSize = ((int)g_Textures.size() + 7)/8;
	u8*       texturesUsedAsNormalMaps     = new u8[texturesUsedAsNormalMapsSize];

	// mark textures used as normal maps
	{
		ProgressDisplay progress("marking textures used as normal maps");
		int count = 0;

		memset(texturesUsedAsNormalMaps, 0, texturesUsedAsNormalMapsSize);

		for (int i = 0; i < (int)g_MaterialTextureVars.size(); i++)
		{
			const aaMaterialTextureVar& mtv = g_MaterialTextureVars[i];
			const eTextureUsage tu = GetTextureUsageForMaterialVarName(mtv.m_materialVarName, mtv.m_shaderName);

			if (tu == TEXTURE_USAGE_BUMP)
			{
				const int textureIndex = g_TexturePathMap.accessindex(mtv.m_texturePathHash);

				if (textureIndex != INDEX_NONE && (texturesUsedAsNormalMaps[textureIndex/8] & BIT(textureIndex%8)) == 0)
				{
					texturesUsedAsNormalMaps[textureIndex/8] |= BIT(textureIndex%8);
					count++;
				}
			}

			progress.Update(i, g_MaterialTextureVars);
		}

		progress.End("marked %d textures (%.2f%%)", count, 100.0f*(float)count/(float)g_Textures.size());
	}

	// write report
	{
		FILE* fp = fopen(path, "w");

		if (fp)
		{
			ProgressDisplay progress("writing %s", path);
			int count = 0;

			for (int i = 0; i < (int)g_Textures.size(); i++)
			{
				if (texturesUsedAsNormalMaps[i/8] & BIT(i%8))
				{
					const aaTexture& texture = g_Textures[i];
					const char* suffix = strrchr(texture.m_textureName, '_');
					bool bHasBumpMapSuffix = false;
					bool bHasAvgBlueColour = false;

					if (suffix)
					{
						// standard bump map suffixes
						if (strcmp(suffix, "_n") == 0 ||
							strcmp(suffix, "_nm") == 0 ||
							strcmp(suffix, "_nrm") == 0 ||
							strcmp(suffix, "_norm") == 0 ||
							strcmp(suffix, "_bump") == 0 ||
							strcmp(suffix, "_wrinkle") == 0)
						{
							bHasBumpMapSuffix = true;
						}
					}

					if (texture.m_average[0] >= 0.40f && texture.m_average[0] < 0.55f &&
						texture.m_average[1] >= 0.40f && texture.m_average[1] < 0.55f &&
						texture.m_average[2] >= 0.70f)
					{
						bHasAvgBlueColour = true;
					}

					if (bHasBumpMapSuffix || bHasAvgBlueColour)
					{
						const float maxRGDev = Max<float>(Abs<float>(texture.m_average[0] - 0.5f), Abs<float>(texture.m_average[1] - 0.5f));

						if (maxRGDev > 0.02f)
						{
							char templateTypeStr[256] = "";
							char templateTypeStr2[256] = "";
							strcpy(templateTypeStr, rage::GetTextureTemplateTypeString(texture.m_textureTemplateType).c_str());

							if (templateTypeStr[0] == '\0')
							{
								strcpy(templateTypeStr, "...");
								strcpy(templateTypeStr2, "...");
							}
							else
							{
								char* plus = strchr(templateTypeStr, '+');

								if (plus)
								{
									strcpy(templateTypeStr2, plus + 1);
									*plus = '\0';
								}
							}

							static bool first = true;
							if (first && g_ColumnsFile)
							{
								char col = 0;
								fprintf(g_ColumnsFile, "%s\n", path);
								WRITE_COLUMN("texturePath");
								WRITE_COLUMN("textureName");
								WRITE_COLUMN("maxRGDev (red-green deviation)");
								WRITE_COLUMN("averageColour");
								WRITE_COLUMN("format/swizzle");
								WRITE_COLUMN("templateTypeStr");
								WRITE_COLUMN("templateTypeStr2");
								fprintf(g_ColumnsFile, "\n");
								first = false;
							}

							fprintf(fp, "%s,%s,%f,[%.2f %.2f %.2f %.2f],%s-%c%c%c%c,%s,%s\n",
								texture.m_texturePath,
								texture.m_textureName,
								maxRGDev,
								texture.m_average[0], texture.m_average[1], texture.m_average[2], texture.m_average[3],
								g_textureFormatStrings[texture.m_textureFormat], texture.m_textureSwizzle[0], texture.m_textureSwizzle[1], texture.m_textureSwizzle[2], texture.m_textureSwizzle[3],
								templateTypeStr,
								templateTypeStr2
							);
							count++;
						}
					}
				}

				progress.Update(i, g_Textures);
			}

			fclose(fp);
			progress.End("done, found %d (%.2f%%)", count, 100.0f*(float)count/(float)g_Textures.size());
		}
		else
		{
			fprintf(stderr, "failed to open %s for writing\n", path);
		}
	}
}

class CTextureUsage
{
public:
	CTextureUsage() {}
	CTextureUsage(const aaMaterialTextureVar& var) : m_shaderName(var.m_shaderName), m_shaderVarName(var.m_materialVarName) {}

	bool operator ==(const CTextureUsage& rhs) const
	{
		if (_stricmp(m_shaderName, rhs.m_shaderName) == 0 &&
			_stricmp(m_shaderVarName, rhs.m_shaderVarName) == 0)
		{
			return true;
		}

		return false;
	}

	const char* m_shaderName;
	const char* m_shaderVarName;
};

static void WriteReportTexturesWithUnusedAlpha(const char* path)
{
	const int texturesWithAlphaFlagsSize = ((int)g_Textures.size() + 7)/8;
	u8*       texturesWithAlphaFlags     = new u8[texturesWithAlphaFlagsSize];
	u8*       legitimateAlphaUseFlags    = new u8[texturesWithAlphaFlagsSize];

	// mark textures with alpha (DXT3/DXT5)
	{
		ProgressDisplay progress("marking textures with alpha (DXT3/DXT5)");
		int count = 0;

		memset(texturesWithAlphaFlags, 0, texturesWithAlphaFlagsSize);

		for (int i = 0; i < (int)g_Textures.size(); i++)
		{
			const aaTexture& texture = g_Textures[i];

			if (texture.m_textureFormat != TEXTURE_FORMAT_DXT3 && // don't mark textures which aren't DXT3 or DXT5 (the only formats we could remove alpha from ..)
				texture.m_textureFormat != TEXTURE_FORMAT_DXT5)
			{
				continue;
			}

			if (memcmp(texture.m_textureSwizzle, "RGB", 3) != 0) // don't mark textures with swizzle (they might be "HighQuality" textures)
			{
				continue;
			}

			const eAssetClass ac = GetAssetClassFromPath(texture.m_texturePath);

			if (ac == ASSET_CLASS_MAP           ||
				ac == ASSET_CLASS_AREA          ||
				ac == ASSET_CLASS_TERRAIN_RAILS ||
				ac == ASSET_CLASS_TERRAIN_ROADS ||
				ac == ASSET_CLASS_TERRAIN_WATER ||
				ac == ASSET_CLASS_TERRAIN       ||
				ac == ASSET_CLASS_INTERIOR      ||
				ac == ASSET_CLASS_PROP          ||
				ac == ASSET_CLASS_DESTRUCTION   ||
				ac == ASSET_CLASS_VEHICLE       ||
				ac == ASSET_CLASS_OUTSOURCE     ||
				ac == ASSET_CLASS_GLOBALTXD     ||
			//	ac == ASSET_CLASS_PED           ||
				ac == ASSET_CLASS_WEAPON        ||
				false)
			{
				if (!AssetPathIsHDTxd(texture.m_texturePath)) // don't mark HD textures, since materials wouldn't point directly to them anyway
				{
					texturesWithAlphaFlags[i/8] |= BIT(i%8);
					count++;
				}
			}

			progress.Update(i, g_Textures);
		}

		progress.End("marked %d textures", count);
	}

	// mark textures which have legitimate use for alpha
	{
		ProgressDisplay progress("marking textures with legitimate alpha");
		int count = 0;

		memset(legitimateAlphaUseFlags, 0, texturesWithAlphaFlagsSize);

		for (int i = 0; i < (int)g_MaterialTextureVars.size(); i++)
		{
			const aaMaterialTextureVar& mtv = g_MaterialTextureVars[i];
			const int textureIndex = g_TexturePathMap.accessindex(mtv.m_texturePathHash);

			if (textureIndex != INDEX_NONE)// && (texturesWithAlphaFlags[textureIndex/8] & BIT(textureIndex%8)) != 0)
			{
				const aaTexture& texture = g_Textures[textureIndex];
				const eTextureUsage tu = GetTextureUsageForMaterialVarName(mtv.m_materialVarName, mtv.m_shaderName);
				bool bCanHaveAlpha = false;

				if (tu == TEXTURE_USAGE_DIFFUSE) // TODO -- how do we determine if a diffuse texture's alpha is unused?
				{
					if (TextureCanHaveAlphaInDiffuseMap(texture.m_texturePath, mtv.m_shaderName))
					{
						bCanHaveAlpha = true;
					}
				}
				else if (tu == TEXTURE_USAGE_BUMP)
				{
					if (TextureCanHaveAlphaInBumpMap(texture.m_texturePath, mtv.m_shaderName))
					{
						bCanHaveAlpha = true;
					}
				}
				else if (tu == TEXTURE_USAGE_SPECULAR)
				{
					if (TextureCanHaveAlphaInSpecularMap(texture.m_texturePath, mtv.m_shaderName))
					{
						bCanHaveAlpha = true;
					}
				}
				else
				{
					bCanHaveAlpha = true;
				}

				if (bCanHaveAlpha)
				{
					//texturesWithAlphaFlags[textureIndex/8] &= ~BIT(textureIndex%8); // remove mark
					legitimateAlphaUseFlags[textureIndex/8] |= BIT(textureIndex%8);
					count++;
				}
			}

			progress.Update(i, g_MaterialTextureVars);
		}

		progress.End("unmarked %d textures", count);
	}

	// write report
	{
		FILE* fp = fopen(path, "w");

		if (fp)
		{
			ProgressDisplay progress("writing %s", path);
			int count = 0;

			for (int i = 0; i < (int)g_Textures.size(); i++)
			{
				if ((texturesWithAlphaFlags[i/8] & BIT(i%8)) != 0 &&
					(legitimateAlphaUseFlags[i/8] & BIT(i%8)) == 0)
				{
					const aaTexture& texture = g_Textures[i];

					static bool first = true;
					if (first && g_ColumnsFile)
					{
						char col = 0;
						fprintf(g_ColumnsFile, "%s\n", path);
						WRITE_COLUMN("texturePath");
						WRITE_COLUMN("textureName");
						WRITE_COLUMN("dimensions");
						WRITE_COLUMN("physicalSize (KB)");
						WRITE_COLUMN("physicalSize (bytes)");
						fprintf(g_ColumnsFile, "\n");
						first = false;
					}

					fprintf(fp, "%s,%s,%dx%d,%.3fKB,%d\n",
						texture.m_texturePath,
						texture.m_textureName,
						texture.m_textureWidth, texture.m_textureHeight,
						(float)texture.m_texturePhysicalSize/1024.0f,
						texture.m_texturePhysicalSize
					);
					count++;
				}

				progress.Update(i, g_Textures);
			}

			fclose(fp);
			progress.End("done, found %d (%.2f%%)", count, 100.0f*(float)count/(float)g_Textures.size());
		}
		else
		{
			fprintf(stderr, "failed to open %s for writing\n", path);
		}
	}

	// write extended report
	{
		char path2[256] = "";
		strcpy(path2, path);
		strcpy(strrchr(path2, '.'), "_ex.txt");
		FILE* fp = fopen(path2, "w");

		if (fp)
		{
			ProgressDisplay progress("writing %s", path);

			for (int i = 0; i < (int)g_Textures.size(); i++)
			{
				if (texturesWithAlphaFlags[i/8] & BIT(i%8))
				{
					const aaTexture& texture = g_Textures[i];
					char usagesStr[8192] = "";

					if (texture.m_texturePathHash != 0)
					{
						std::vector<CTextureUsage> usages;

						for (int j = 0; j < (int)g_MaterialTextureVars.size(); j++)
						{
							if (g_MaterialTextureVars[j].m_texturePathHash == texture.m_texturePathHash)
							{
								const CTextureUsage usage(g_MaterialTextureVars[j]);
								bool bUnique = true;

								for (int k = 0; k < (int)usages.size(); k++)
								{
									if (usages[k] == usage)
									{
										bUnique = false;
										break;
									}
								}

								if (bUnique)
								{
									usages.push_back(usage);
								}
							}
						}

						for (int k = 0; k < (int)usages.size(); k++)
						{
							strcat(usagesStr, varString("[%s/%s]", usages[k].m_shaderName, usages[k].m_shaderVarName).c_str());
						}
					}

					static bool first = true;
					if (first && g_ColumnsFile)
					{
						char col = 0;
						fprintf(g_ColumnsFile, "%s\n", path);
						WRITE_COLUMN("texturePath");
						WRITE_COLUMN("templateType");
						WRITE_COLUMN("legitimate ?");
						WRITE_COLUMN("usage");
						fprintf(g_ColumnsFile, "\n");
						first = false;
					}

					fprintf(fp, "%s,%s,%s,%s\n",
						texture.m_texturePath,
						rage::GetTextureTemplateTypeString(texture.m_textureTemplateType).c_str(),
						(legitimateAlphaUseFlags[i/8] & BIT(i%8)) ? "(legitimate?)" : "",
						usagesStr
					);
				}

				progress.Update(i, g_Textures);
			}

			fclose(fp);
			progress.End();
		}
		else
		{
			fprintf(stderr, "failed to open %s for writing\n", path2);
		}
	}

	delete[] texturesWithAlphaFlags;
}

#if HIERARCHY
static const aaTexture* FindTextureInHierarchy(const aaTexture* texture, const aaTextureDictionary* txd, bool bMatchName, bool bMatchHash)
{
	if (txd)
	{
		for (const aaTexture* t = txd->m_firstTexture; t; t = t->m_nextSiblingTexture)
		{
			if (bMatchName && strcmp(t->m_textureName, texture->m_textureName) == 0)
			{
				return t;
			}
			else if (bMatchHash && t->m_texturePixelDataHash == texture->m_texturePixelDataHash)
			{
				return t;
			}
		}

		return FindTextureInHierarchy(texture, txd->m_parentTxd, bMatchName, bMatchHash);
	}

	return NULL;
}

static void WriteReportTexturesFoundInParentTxd(const char* path)
{
	// write report
	{
		FILE* fp = fopen(path, "w");

		if (fp)
		{
			ProgressDisplay progress("writing %s", path);
			int count = 0;

			for (int i = 0; i < (int)g_Textures.size(); i++)
			{
				const aaTexture& texture = g_Textures[i];

				if (texture.m_txd)
				{
					const aaTexture* duplicate = FindTextureInHierarchy(&texture, texture.m_txd->m_parentTxd, true, true);

					if (duplicate)
					{
						const bool bMatchName = (_stricmp(duplicate->m_textureName, texture.m_textureName) == 0);
						const bool bMatchHash = (duplicate->m_texturePixelDataHash == texture.m_texturePixelDataHash);
						const char* matchStr = "?";
						const char* packedStr = "";
						char duplicateResStr[32] = "";

						if (bMatchName && bMatchHash)
						{
							matchStr = "";
						}
						else if (bMatchName)
						{
							matchStr = "(name)";
						}
						else if (bMatchHash)
						{
							matchStr = "(hash)";
						}

						char path1[512] = "";
						char path2[512] = "";
						strcpy(path1, texture.m_texturePath);
						strcpy(path2, duplicate->m_texturePath);
						if (strrchr(path1, '.')) { strrchr(path1, '.')[0] = '\0'; }
						if (strrchr(path2, '.')) { strrchr(path2, '.')[0] = '\0'; }
						if (strcmp(path1, path2) == 0)
						{
							packedStr = "(packed)"; // note that this texture is packed in a drawable and also in the txd of the same name
						}

						if (texture.m_textureWidth  != duplicate->m_textureWidth ||
							texture.m_textureHeight != duplicate->m_textureHeight)
						{
							sprintf(duplicateResStr, "%dx%d", duplicate->m_textureWidth, duplicate->m_textureHeight);
						}

						static bool first = true;
						if (first && g_ColumnsFile)
						{
							char col = 0;
							fprintf(g_ColumnsFile, "%s\n", path);
							WRITE_COLUMN("texturePath");
							WRITE_COLUMN("textureName");
							WRITE_COLUMN("dimensions");
							WRITE_COLUMN("physicalSize (KB)");
							WRITE_COLUMN("physicalSize (bytes)");
							WRITE_COLUMN("duplicateTexturePath");
							WRITE_COLUMN("match");
							WRITE_COLUMN("packed");
							WRITE_COLUMN("duplicateDimensions");
							fprintf(g_ColumnsFile, "\n");
							first = false;
						}

						fprintf(fp, "%s,%s,%dx%d,%.3fKB,%d,%s,%s,%s,%s\n",
							texture.m_texturePath,
							texture.m_textureName,
							texture.m_textureWidth, texture.m_textureHeight,
							(float)texture.m_texturePhysicalSize/1024.0f,
							texture.m_texturePhysicalSize,
							duplicate->m_texturePath,
							matchStr,
							packedStr,
							duplicateResStr
						);
						count++;
					}
				}

				progress.Update(i, g_Textures);
			}

			fclose(fp);
			progress.End("done, found %d (%.2f%%)", count, 100.0f*(float)count/(float)g_Textures.size());
		}
		else
		{
			fprintf(stderr, "failed to open %s for writing\n", path);
		}
	}
}

static bool IsTextureUsedByMaterialInDrawable(const aaTexture* texture, const aaDrawable* drawable, bool bMatchNameOnly)
{
	if (drawable)
	{
		for (const aaMaterialTextureVar* mtv = drawable->m_firstMaterialVar; mtv; mtv = mtv->m_nextVar)
		{
			if (mtv->m_texturePathHash == texture->m_texturePathHash)
			{
				return true;
			}
			else if (bMatchNameOnly && mtv->m_textureNameHash == texture->m_textureNameHash)
			{
				return true;
			}
		}
	}

	return false;
}

static bool IsTextureUsedByMaterialInTxdHierarchy(const aaTexture* texture, const aaTextureDictionary* txd, bool bMatchNameOnly)
{
	if (txd)
	{
		if (txd->m_drawable)
		{
			if (IsTextureUsedByMaterialInDrawable(texture, txd->m_drawable, bMatchNameOnly))
			{
				return true;
			}
		}

		if (txd->m_dwd)
		{
			for (const aaDrawable* drawable = txd->m_dwd->m_firstDrawable; drawable; drawable = drawable->m_dwdNextDrawable)
			{
				if (IsTextureUsedByMaterialInDrawable(texture, drawable, bMatchNameOnly))
				{
					return true;
				}
			}
		}

		if (txd->m_fragment)
		{
			if (IsTextureUsedByMaterialInDrawable(texture, txd->m_fragment->m_commonDrawable, bMatchNameOnly) ||
				IsTextureUsedByMaterialInDrawable(texture, txd->m_fragment->m_damagedDrawable, bMatchNameOnly) ||
				IsTextureUsedByMaterialInDrawable(texture, txd->m_fragment->m_clothDrawable, bMatchNameOnly))
			{
				return true;
			}
		}

		for (txd = txd->m_firstChildTxd; txd; txd = txd->m_nextSiblingTxd)
		{
			if (IsTextureUsedByMaterialInTxdHierarchy(texture, txd, bMatchNameOnly))
			{
				return true;
			}
		}
	}

	return false;
}

static void WriteReportTexturesShouldBeMovedUpInHierarchy(const char* path, int minChildrenTotal = 2, int maxChildrenNotUsingThisTexture = 0)
{
	// write report
	{
		FILE* fp = fopen(path, "w");

		if (fp)
		{
			ProgressDisplay progress("writing %s", path);
			int count = 0;

			for (int i = 0; i < (int)g_Textures.size(); i++)
			{
				const aaTexture& texture = g_Textures[i];
				const aaTextureDictionary* txd = texture.m_txd;

				if (txd)
				{
					bool bMatchNameOnly = false;
					int hierarchyDepth = 0;

					if (GetAssetClassFromPath(txd->m_txdPath) == ASSET_CLASS_MAP           ||
						GetAssetClassFromPath(txd->m_txdPath) == ASSET_CLASS_AREA          ||
						GetAssetClassFromPath(txd->m_txdPath) == ASSET_CLASS_TERRAIN_RAILS ||
						GetAssetClassFromPath(txd->m_txdPath) == ASSET_CLASS_TERRAIN_ROADS ||
						GetAssetClassFromPath(txd->m_txdPath) == ASSET_CLASS_TERRAIN_WATER ||
						GetAssetClassFromPath(txd->m_txdPath) == ASSET_CLASS_TERRAIN       ||
						GetAssetClassFromPath(txd->m_txdPath) == ASSET_CLASS_PROP          ||
						GetAssetClassFromPath(txd->m_txdPath) == ASSET_CLASS_INTERIOR      ||
						GetAssetClassFromPath(txd->m_txdPath) == ASSET_CLASS_GLOBALTXD     ||
						false)
					{
						bMatchNameOnly = true;
					}

					while (true)
					{
						const aaTextureDictionary* parent = txd->m_parentTxd;

						int numChildTxdsNotUsingThisTexture = 0;
						int numChildTxds = 0;

						if (parent)
						{
							for (const aaTextureDictionary* childTxd = parent->m_firstChildTxd; childTxd; childTxd = childTxd->m_nextSiblingTxd)
							{
								if (!IsTextureUsedByMaterialInTxdHierarchy(&texture, childTxd, bMatchNameOnly))
								{
									numChildTxdsNotUsingThisTexture++;
								}

								numChildTxds++;
							}
						}

						if (numChildTxds >= minChildrenTotal &&
							numChildTxdsNotUsingThisTexture <= maxChildrenNotUsingThisTexture)
						{
							txd = parent;
							hierarchyDepth++;
						}
						else
						{
							break;
						}
					}

					if (hierarchyDepth > 0)
					{
						static bool first = true;
						if (first && g_ColumnsFile)
						{
							char col = 0;
							fprintf(g_ColumnsFile, "%s\n", path);
							WRITE_COLUMN("texturePath");
							WRITE_COLUMN("textureName");
							WRITE_COLUMN("dimensions");
							WRITE_COLUMN("physicalSize (KB)");
							WRITE_COLUMN("physicalSize (bytes)");
							WRITE_COLUMN("txdPath");
							WRITE_COLUMN("hierarchyDepth");
							fprintf(g_ColumnsFile, "\n");
							first = false;
						}

						fprintf(fp, "%s,%s,%dx%d,%.3fKB,%d,%s,%d\n",
							texture.m_texturePath,
							texture.m_textureName,
							texture.m_textureWidth, texture.m_textureHeight,
							(float)texture.m_texturePhysicalSize/1024.0f,
							texture.m_texturePhysicalSize,
							txd->m_txdPath,
							hierarchyDepth
						);
						count++;
					}
				}

				progress.Update(i, g_Textures);
			}

			fclose(fp);
			progress.End("done, found %d (%.2f%%)", count, 100.0f*(float)count/(float)g_Textures.size());
		}
		else
		{
			fprintf(stderr, "failed to open %s for writing\n", path);
		}
	}
}

static void WriteReportTexturesShouldBeMovedDownInHierarchy(const char* path, int minChildrenTotal = 2, int maxChildrenUsingThisTexture = 1)
{
	// write report
	{
		FILE* fp = fopen(path, "w");

		if (fp)
		{
			ProgressDisplay progress("writing %s", path);
			int count = 0;

			for (int i = 0; i < (int)g_Textures.size(); i++)
			{
				const aaTexture& texture = g_Textures[i];
				const aaTextureDictionary* txd = texture.m_txd;

				if (txd)
				{
					bool bMatchNameOnly = false;
					int hierarchyDepth = 0;

					if (GetAssetClassFromPath(txd->m_txdPath) == ASSET_CLASS_MAP           ||
						GetAssetClassFromPath(txd->m_txdPath) == ASSET_CLASS_AREA          ||
						GetAssetClassFromPath(txd->m_txdPath) == ASSET_CLASS_TERRAIN_RAILS ||
						GetAssetClassFromPath(txd->m_txdPath) == ASSET_CLASS_TERRAIN_ROADS ||
						GetAssetClassFromPath(txd->m_txdPath) == ASSET_CLASS_TERRAIN_WATER ||
						GetAssetClassFromPath(txd->m_txdPath) == ASSET_CLASS_TERRAIN       ||
						GetAssetClassFromPath(txd->m_txdPath) == ASSET_CLASS_PROP          ||
						GetAssetClassFromPath(txd->m_txdPath) == ASSET_CLASS_INTERIOR      ||
						GetAssetClassFromPath(txd->m_txdPath) == ASSET_CLASS_GLOBALTXD     ||
						false)
					{
						bMatchNameOnly = true;
					}

					while (true)
					{
						const aaTextureDictionary* firstChildTxdUsingThisTexture = NULL;
						int numChildTxdsUsingThisTexture = 0;
						int numChildTxds = 0;

						for (const aaTextureDictionary* childTxd = txd->m_firstChildTxd; childTxd; childTxd = childTxd->m_nextSiblingTxd)
						{
							if (IsTextureUsedByMaterialInTxdHierarchy(&texture, childTxd, bMatchNameOnly))
							{
								if (firstChildTxdUsingThisTexture == NULL)
								{
									firstChildTxdUsingThisTexture = childTxd;
								}

								numChildTxdsUsingThisTexture++;
							}

							numChildTxds++;
						}

						if (numChildTxds >= minChildrenTotal &&
							numChildTxdsUsingThisTexture > 0 &&
							numChildTxdsUsingThisTexture <= maxChildrenUsingThisTexture)
						{
							txd = firstChildTxdUsingThisTexture;
							hierarchyDepth++;

							if (maxChildrenUsingThisTexture > 1)
							{
								// we could recursively determine if the texture should be moved down, but this would be more complicated ..
								break;
							}
						}
						else
						{
							break;
						}
					}

					if (hierarchyDepth > 0)
					{
						static bool first = true;
						if (first && g_ColumnsFile)
						{
							char col = 0;
							fprintf(g_ColumnsFile, "%s\n", path);
							WRITE_COLUMN("texturePath");
							WRITE_COLUMN("textureName");
							WRITE_COLUMN("dimensions");
							WRITE_COLUMN("physicalSize (KB)");
							WRITE_COLUMN("physicalSize (bytes)");
							WRITE_COLUMN("txdPath");
							WRITE_COLUMN("hierarchyDepth");
							fprintf(g_ColumnsFile, "\n");
							first = false;
						}

						fprintf(fp, "%s,%s,%dx%d,%.3fKB,%d,%s,%d\n",
							texture.m_texturePath,
							texture.m_textureName,
							texture.m_textureWidth, texture.m_textureHeight,
							(float)texture.m_texturePhysicalSize/1024.0f,
							texture.m_texturePhysicalSize,
							(maxChildrenUsingThisTexture == 1) ? txd->m_txdPath : "",
							hierarchyDepth
						);
						count++;
					}
				}

				progress.Update(i, g_Textures);
			}

			fclose(fp);
			progress.End("done, found %d (%.2f%%)", count, 100.0f*(float)count/(float)g_Textures.size());
		}
		else
		{
			fprintf(stderr, "failed to open %s for writing\n", path);
		}
	}
}
#endif // HIERARCHY

// useful utility
static void CreateUniqueStringList(const char* path)
{
	char path2[256] = "";
	strcpy(path2, path);
	strcpy(strrchr(path2, '.'), "_unique");
	strcat(path2, strrchr(path, '.'));

	FILE* f1 = fopen(path, "r");
	FILE* f2 = fopen(path2, "w");

	char line[1024] = "";
	char prev[1024] = "...";

	while (ReadFileLine(line, sizeof(line), f1))
	{
		if (strcmp(prev, line) != 0)
		{
			fprintf(f2, "%s\n", line);
			strcpy(prev, line);
		}
	}

	fclose(f1);
	fclose(f2);

	exit(0);
}

int main(int argc, const char* argv[])
{
	PrintDirectoryPaths();
	//CreateUniqueStringList("missing.txt");

	bool bSyncIn  = false;
	bool bSyncOut = false;
	bool bPause   = IsDebuggerPresent() ? true : false;

	const char* syncInPath  = "x:/sync.aa1";
	const char* syncOutPath = "x:/sync.aa2";

	for (int i = 1; i < argc; i++)
	{
		if      (strcmp(argv[i], "-sync_in" ) == 0) { bSyncIn = true; }
		else if (strcmp(argv[i], "-sync_out") == 0) { bSyncOut = true; }
		else if (strcmp(argv[i], "-pause"   ) == 0) { bPause = true; }
	}

	if (bSyncIn)
	{
		fprintf(stdout, "waiting for %s .. ", syncInPath);
		fflush(stdout);

		while (1)
		{
			FILE* sync = fopen(syncInPath, "rb");

			if (sync)
			{
				fclose(sync);
				fprintf(stdout, "ok.\n");
				break;
			}
			else
			{
				Sleep(1000); // sleep for one second
			}
		}
	}

	const clock_t time0 = clock();

	LoadAllStreams();

	g_ColumnsFile = fopen(varString("%s/non_final/aa/reports/aa_columns.txt", RS_ASSETS).c_str(), "w");

	WriteArchetypes          (varString("%s/non_final/aa/reports/%s", RS_ASSETS, GetStreamName(STREAM_TYPE_ARCHETYPE           )).c_str());
	WriteArchetypeInstances  (varString("%s/non_final/aa/reports/%s", RS_ASSETS, GetStreamName(STREAM_TYPE_ARCHETYPE_INSTANCE  )).c_str());
	WriteMapDatas            (varString("%s/non_final/aa/reports/%s", RS_ASSETS, GetStreamName(STREAM_TYPE_MAPDATA             )).c_str());
	WriteTextures            (varString("%s/non_final/aa/reports/%s", RS_ASSETS, GetStreamName(STREAM_TYPE_TEXTURE             )).c_str());
	WriteTextureDictionaries (varString("%s/non_final/aa/reports/%s", RS_ASSETS, GetStreamName(STREAM_TYPE_TEXTURE_DICTIONARY  )).c_str());
	WriteDrawables           (varString("%s/non_final/aa/reports/%s", RS_ASSETS, GetStreamName(STREAM_TYPE_DRAWABLE            )).c_str());
	WriteDrawableDictionaries(varString("%s/non_final/aa/reports/%s", RS_ASSETS, GetStreamName(STREAM_TYPE_DRAWABLE_DICTIONARY )).c_str());
	WriteFragments           (varString("%s/non_final/aa/reports/%s", RS_ASSETS, GetStreamName(STREAM_TYPE_FRAGMENT            )).c_str());
	WriteLights              (varString("%s/non_final/aa/reports/%s", RS_ASSETS, GetStreamName(STREAM_TYPE_LIGHT               )).c_str());
	WriteParticles           (varString("%s/non_final/aa/reports/%s", RS_ASSETS, GetStreamName(STREAM_TYPE_PARTICLE            )).c_str());
	WriteMaterials           (varString("%s/non_final/aa/reports/%s", RS_ASSETS, GetStreamName(STREAM_TYPE_MATERIAL            )).c_str());
	WriteMaterialTextureVars (varString("%s/non_final/aa/reports/%s", RS_ASSETS, GetStreamName(STREAM_TYPE_MATERIAL_TEXTURE_VAR)).c_str());

	WriteReportMaterialShaderUsage                  (varString("%s/non_final/aa/reports/shader_usage.csv"                 , RS_ASSETS).c_str());
	WriteReportArchetypesNotPlacedInMapData         (varString("%s/non_final/aa/reports/archetypes_not_placed.csv"        , RS_ASSETS).c_str());
	WriteReportDrawablesNotUsed                     (varString("%s/non_final/aa/reports/drawables_not_used.csv"           , RS_ASSETS).c_str());
	WriteReportTexturesNotUsedByMaterials           (varString("%s/non_final/aa/reports/textures_not_used.csv"            , RS_ASSETS).c_str());
	WriteReportTexturesWithUnusedAlpha              (varString("%s/non_final/aa/reports/textures_with_unused_alpha.csv"   , RS_ASSETS).c_str());
	WriteReportShiftedNormalMaps                    (varString("%s/non_final/aa/reports/shifted_normal_maps.csv"          , RS_ASSETS).c_str());
#if HIERARCHY
	WriteReportTexturesFoundInParentTxd             (varString("%s/non_final/aa/reports/textures_found_in_parent_txd.csv"     , RS_ASSETS).c_str());
	WriteReportTexturesShouldBeMovedUpInHierarchy   (varString("%s/non_final/aa/reports/textures_should_be_moved_up_2_0.csv"  , RS_ASSETS).c_str(), 2, 0);
	WriteReportTexturesShouldBeMovedUpInHierarchy   (varString("%s/non_final/aa/reports/textures_should_be_moved_up_5_1.csv"  , RS_ASSETS).c_str(), 5, 1);
	WriteReportTexturesShouldBeMovedDownInHierarchy (varString("%s/non_final/aa/reports/textures_should_be_moved_down_2_1.csv", RS_ASSETS).c_str(), 2, 1);
	WriteReportTexturesShouldBeMovedDownInHierarchy (varString("%s/non_final/aa/reports/textures_should_be_moved_down_5_2.csv", RS_ASSETS).c_str(), 5, 2);
//	WriteReportDrawablesWithUnnecessaryTxdDependency(); // drawables which use packed textures but still rely on parent txd
#endif // HIERARCHY

	if (g_ColumnsFile)
	{
		fclose(g_ColumnsFile);
		g_ColumnsFile = NULL;
	}

	const clock_t total = clock() - time0;

	// display total time
	{
		const float totalSecs = (float)total/(float)CLOCKS_PER_SEC;
		const float totalMins = totalSecs/60.0f;
		const float totalHrs  = totalMins/60.0f;

		if      (totalSecs <=   1.0f) { fprintf(stdout, "total time < 1 secs"); }
		else if (totalSecs <= 100.0f) { fprintf(stdout, "total time %.2f secs\n", totalSecs); }
		else if (totalMins <= 100.0f) { fprintf(stdout, "total time %.2f mins\n", totalMins); }
		else                          { fprintf(stdout, "total time %.2f hours\n", totalHrs); }
	}

	if (bSyncOut)
	{
		fclose(fopen(syncOutPath, "wb"));
	}

	if (bPause)
	{
		system("pause");
	}

	return 0;
}
