// ======================
// AssetAnalysisTool.h
// (c) 2013 RockstarNorth
// ======================

#ifndef _ASSETANALYSISTOOL_H_
#define _ASSETANALYSISTOOL_H_

#include "../../common/common.h"
#include "../../../game/debug/AssetAnalysis/AssetAnalysisCommon.h"

#define HIERARCHY (1)

namespace std {

template <typename KeyType, typename DataType> class xmap : public map<KeyType,DataType>
{
public:
	inline const DataType* access(const KeyType& key) const
	{
		map<KeyType,DataType>::const_iterator it = find(key);
		return (it != end()) ? &it->second : NULL;
	}

	inline DataType* access(const KeyType& key)
	{
		map<KeyType,DataType>::iterator it = find(key);
		return (it != end()) ? &it->second : NULL;
	}

	inline int accessindex(const KeyType& key) const
	{
		map<KeyType,DataType>::const_iterator it = find(key);
		return (it != end()) ? it->second : -1;
	}
};

} // namespace std

#pragma pack(push)
#pragma pack(1)

#define SET_STRING32(classname,var) \
{ \
	const std::string* pstr = st32.access(var##Hash); \
	if (pstr) { var = pstr->c_str(); } \
	else { var = "?"; if (errFile) { fprintf(errFile, "ERROR: no string found for %s param '%s' 0x%08x\n", #classname, #var, var##Hash); fflush(errFile); if (errCount) { (*errCount)++; } } } \
}

#define SET_STRING64(classname,var) \
{ \
	const std::string* pstr = st64.access(var##Hash); \
	if (pstr) { var = pstr->c_str(); } \
	else { var = "?"; if (errFile) { fprintf(errFile, "ERROR: no string found for %s param '%s' 0x%08x%08x\n", #classname, #var, U64_ARGS(var##Hash)); fflush(errFile); if (errCount) { (*errCount)++; } } } \
}

class aaArchetype;
class aaArchetypeInstance;
class aaMapData;
class aaTexture;
class aaTextureDictionary;
class aaDrawable;
class aaDrawableDictionary;
class aaFragment;
class aaLight;
class aaParticle;
class aaMaterial;
class aaMaterialTextureVar;

// ==============================================
class aaArchetypeRecord
{
public:
	enum { TAG = 'ARCH' };

	u32   m_tag;
	u64   m_archetypeNameHash;
	u64   m_archetypePathHash;
	u8    m_archetypeFlags;
	u8    m_modelInfoType;
	float m_HDTexDistance;
	float m_lodDistance;
	u64   m_parentParticlePathHash;
	u64   m_HDTxdPathHash; // peds and vehicles
	u64   m_HDDrawableOrFragmentPathHash; // vehicles and weapons
	u64   m_pedCompDwdPathHash; // peds only
	u64   m_pedPropDwdPathHash; // peds only
};
class aaArchetype : public aaArchetypeRecord
{
public:
	typedef aaArchetypeRecord RecordType;

	void Init(const std::xmap<u32,std::string>& st32, const std::xmap<u64,std::string>& st64, FILE* errFile = NULL, int* errCount = NULL)
	{
		SET_STRING64(Archetype,m_archetypeName);
		SET_STRING64(Archetype,m_archetypePath);
		SET_STRING64(Archetype,m_parentParticlePath);
		SET_STRING64(Archetype,m_HDTxdPath);
		SET_STRING64(Archetype,m_HDDrawableOrFragmentPath);
		SET_STRING64(Archetype,m_pedCompDwdPath);
		SET_STRING64(Archetype,m_pedPropDwdPath);
	}

	bool operator <(const aaArchetype& rhs) const
	{
		return strcmp(m_archetypePath, rhs.m_archetypePath) < 0;
	}

	const char* m_archetypeName;
	const char* m_archetypePath;
	const char* m_parentParticlePath;
	const char* m_HDTxdPath; // peds and vehicles
	const char* m_HDDrawableOrFragmentPath; // vehicles only
	const char* m_pedCompDwdPath; // peds only
	const char* m_pedPropDwdPath; // peds only
};
// ==============================================

// ==============================================
class aaArchetypeInstanceRecord
{
public:
	enum { TAG = 'INST' };

	u32   m_tag;
	u64   m_archetypeNameHash;
	u64   m_archetypePathHash;
	u64   m_mapDataPathHash;
	u64   m_containingMLONameHash; // 0 if this is not an interior entity
	u64   m_lodArchetypeNameHash;
	u16   m_entityLodDistance;
	float m_entityBoundRadius;
	float m_lodPivot[3];
	float m_lodParentRatio;
	float m_lodRadiusRatio;
	u8    m_lodWarningFlags;
	float m_matrixCol0[3];
	float m_matrixCol1[3];
	float m_matrixCol2[3];
	float m_matrixCol3[3];
};
class aaArchetypeInstance : public aaArchetypeInstanceRecord
{
public:
	typedef aaArchetypeInstanceRecord RecordType;

	void Init(const std::xmap<u32,std::string>& st32, const std::xmap<u64,std::string>& st64, FILE* errFile = NULL, int* errCount = NULL)
	{
		SET_STRING64(ArchetypeInstance,m_archetypeName);
		SET_STRING64(ArchetypeInstance,m_archetypePath);
		SET_STRING64(ArchetypeInstance,m_mapDataPath);

		if (m_containingMLONameHash != 0)
		{
			SET_STRING64(ArchetypeInstance,m_containingMLOName);
		}
		else
		{
			m_containingMLOName = "";
		}

		if (m_lodArchetypeNameHash != 0)
		{
			SET_STRING64(ArchetypeInstance,m_lodArchetypeName);
		}
		else
		{
			m_lodArchetypeName = "";
		}
	}

	bool operator <(const aaArchetypeInstance& rhs) const
	{
		return false;
	}

	const char* m_archetypeName;
	const char* m_archetypePath;
	const char* m_mapDataPath;
	const char* m_containingMLOName;
	const char* m_lodArchetypeName;
};
// ==============================================

// ==============================================
class aaMapDataRecord
{
public:
	enum { TAG = 'MAPD' };

	u32 m_tag;
	u32 m_physicalSize;
	u32 m_virtualSize;
	u64 m_mapDataPathHash;
	u64 m_parentPathHash;
	u32 m_contentsFlags;
};
class aaMapData : public aaMapDataRecord
{
public:
	typedef aaMapDataRecord RecordType;

	void Init(const std::xmap<u32,std::string>& st32, const std::xmap<u64,std::string>& st64, FILE* errFile = NULL, int* errCount = NULL)
	{
		SET_STRING64(MapData,m_mapDataPath);
		SET_STRING64(MapData,m_parentPath);
	}

	bool operator <(const aaMapData& rhs) const
	{
		return false;
	}

	const char* m_mapDataPath;
	const char* m_parentPath;
};
// ==============================================

// ==============================================
class aaTextureRecord
{
public:
	enum { TAG = 'TXTR' };

	u32   m_tag;
	u64   m_textureNameHash;
	u64   m_texturePathHash;
	u32   m_texturePixelDataHash;
	u32   m_texturePhysicalSize;
	u16   m_textureWidth;
	u16   m_textureHeight;
	u8    m_textureMips;
	u8    m_textureFormat;
	char  m_textureSwizzle[4];
	u8    m_textureConversionFlags;
	u16   m_textureTemplateType;
	u16   m_rgbmin;
	u16   m_rgbmax;
	float m_average[4];
};
class aaTexture : public aaTextureRecord
{
public:
	typedef aaTextureRecord RecordType;

	void Init(const std::xmap<u32,std::string>& st32, const std::xmap<u64,std::string>& st64, FILE* errFile = NULL, int* errCount = NULL)
	{
		SET_STRING64(Texture,m_textureName);
		SET_STRING64(Texture,m_texturePath);
#if HIERARCHY
		m_txd                = NULL;
		m_nextSiblingTexture = NULL;
#endif // HIERARCHY
	}

	bool operator <(const aaTexture& rhs) const
	{
		return strcmp(m_texturePath, rhs.m_texturePath) < 0;
	}

	const char* m_textureName;
	const char* m_texturePath;
#if HIERARCHY
	const aaTextureDictionary* m_txd;
	const aaTexture*           m_nextSiblingTexture;
#endif // HIERARCHY
};
// ==============================================

// ==============================================
class aaTextureDictionaryRecord
{
public:
	enum { TAG = 'TXD ' };

	u32 m_tag;
	u32 m_physicalSize;
	u32 m_virtualSize;
	u16 m_numEntries;
	u8  m_assetType; // eAssetType
	u64 m_txdPathHash;
	u64 m_parentTxdPathHash;
};
class aaTextureDictionary : public aaTextureDictionaryRecord
{
public:
	typedef aaTextureDictionaryRecord RecordType;

	void Init(const std::xmap<u32,std::string>& st32, const std::xmap<u64,std::string>& st64, FILE* errFile = NULL, int* errCount = NULL)
	{
		SET_STRING64(TextureDictionary,m_txdPath);
		SET_STRING64(TextureDictionary,m_parentTxdPath);
#if HIERARCHY
		m_parentTxd      = NULL;
		m_nextSiblingTxd = NULL;
		m_firstChildTxd  = NULL;
		m_firstTexture   = NULL;
		m_drawable       = NULL;
		m_dwd            = NULL;
		m_fragment       = NULL;
#endif // HIERARCHY
	}

	bool operator <(const aaTextureDictionary& rhs) const
	{
		return strcmp(m_txdPath, rhs.m_txdPath) < 0;
	}

	const char* m_txdPath;
	const char* m_parentTxdPath;
#if HIERARCHY
	const aaTextureDictionary*  m_parentTxd;
	const aaTextureDictionary*  m_nextSiblingTxd;
	const aaTextureDictionary*  m_firstChildTxd;
	const aaTexture*            m_firstTexture;
	const aaDrawable*           m_drawable;
	const aaDrawableDictionary* m_dwd;
	const aaFragment*           m_fragment;
#endif // HIERARCHY
};
// ==============================================

// ==============================================
class aaDrawableRecord
{
public:
	enum { TAG = 'DRAW' };

	u32 m_tag;
	u32 m_physicalSize;
	u32 m_virtualSize;
	u8  m_assetType; // eAssetType
	u8  m_drawableType; // eDrawableType
	u64 m_drawablePathHash;
	u64 m_assetPathHash;
	u64 m_parentTxdPathHash;
	u64 m_archetypeNameHash;
	u16 m_numTriangles[4];
	u16 m_tintDataSize;
	u16 m_numLights;
	u16 m_numLightsWithTextures;
};
class aaDrawable : public aaDrawableRecord
{
public:
	typedef aaDrawableRecord RecordType;

	void Init(const std::xmap<u32,std::string>& st32, const std::xmap<u64,std::string>& st64, FILE* errFile = NULL, int* errCount = NULL)
	{
		SET_STRING64(Drawable,m_drawablePath);
		SET_STRING64(Drawable,m_assetPath);
		SET_STRING64(Drawable,m_parentTxdPath);
		SET_STRING64(Drawable,m_archetypeName);
#if HIERARCHY
		m_packedTxd        = NULL;
		m_dwd              = NULL;
		m_dwdNextDrawable  = NULL;
		m_fragment         = NULL;
		m_firstMaterialVar = NULL;
#endif // HIERARCHY
	}

	bool operator <(const aaDrawable& rhs) const
	{
		return strcmp(m_drawablePath, rhs.m_drawablePath) < 0;
	}

	const char* m_drawablePath;
	const char* m_assetPath;
	const char* m_parentTxdPath;
	const char* m_archetypeName;
#if HIERARCHY
	const aaTextureDictionary*  m_packedTxd;
	const aaDrawableDictionary* m_dwd;
	const aaDrawable*           m_dwdNextDrawable; // next sibling if this drawable is in a dwd
	const aaFragment*           m_fragment;
	const aaMaterialTextureVar* m_firstMaterialVar;
#endif // HIERARCHY
};
// ==============================================

// ==============================================
class aaDrawableDictionaryRecord
{
public:
	enum { TAG = 'DWD ' };

	u32 m_tag;
	u32 m_physicalSize;
	u32 m_virtualSize;
	u16 m_numEntries;
	u64 m_assetPathHash;
	u64 m_parentTxdPathHash;
};
class aaDrawableDictionary : public aaDrawableDictionaryRecord
{
public:
	typedef aaDrawableDictionaryRecord RecordType;

	void Init(const std::xmap<u32,std::string>& st32, const std::xmap<u64,std::string>& st64, FILE* errFile = NULL, int* errCount = NULL)
	{
		SET_STRING64(DrawableDictionary,m_assetPath);
		SET_STRING64(DrawableDictionary,m_parentTxdPath);
#if HIERARCHY
		m_firstDrawable = NULL;
#endif // HIERARCHY
	}

	bool operator <(const aaDrawableDictionary& rhs) const
	{
		return strcmp(m_assetPath, rhs.m_assetPath) < 0;
	}

	const char* m_assetPath;
	const char* m_parentTxdPath;
#if HIERARCHY
	const aaDrawable* m_firstDrawable;
#endif // HIERARCHY
};
// ==============================================

// ==============================================
class aaFragmentRecord
{
public:
	enum { TAG = 'FRAG' };

	u32 m_tag;
	u32 m_physicalSize;
	u32 m_virtualSize;
	u64 m_assetPathHash;
	u64 m_parentTxdPathHash;
	u16 m_tintDataSize;
	u16 m_numLights;
	u16 m_numLightsWithTextures;
	u8  m_numEnvCloths;
	u8  m_numCharCloths;
	u8  m_numGlassPanes;
};
class aaFragment : public aaFragmentRecord
{
public:
	typedef aaFragmentRecord RecordType;

	void Init(const std::xmap<u32,std::string>& st32, const std::xmap<u64,std::string>& st64, FILE* errFile = NULL, int* errCount = NULL)
	{
		SET_STRING64(Fragment,m_assetPath);
		SET_STRING64(Fragment,m_parentTxdPath);
#if HIERARCHY
		m_commonDrawable  = NULL;
		m_damagedDrawable = NULL;
		m_clothDrawable   = NULL;
#endif // HIERARCHY
	}

	bool operator <(const aaFragment& rhs) const
	{
		return strcmp(m_assetPath, rhs.m_assetPath) < 0;
	}

	const char* m_assetPath;
	const char* m_parentTxdPath;
#if HIERARCHY
	const aaDrawable* m_commonDrawable;
	const aaDrawable* m_damagedDrawable;
	const aaDrawable* m_clothDrawable;
#endif // HIERARCHY
};
// ==============================================

// ==============================================
class aaLightRecord
{
public:
	enum { TAG = 'LITE' };

	u32   m_tag;
	u64   m_drawablePathHash;
	u16   m_lightIndexInDrawable;
	u64   m_projectedTexturePathHash;

	float m_position[3];

	// ==================================================================================
	// WARNING!! IF YOU CHANGE THE FIELDS HERE YOU MUST ALSO KEEP IN SYNC WITH:
	// 
	// CLightAttrDef - x:\gta5\src\dev\game\scene\loader\MapData_Extensions.psc
	// CLightAttr    - x:\gta5\src\dev\game\scene\2dEffect.h
	// CLightAttr    - x:\gta5\src\dev\rage\framework\tools\src\cli\ragebuilder\gta_res.h
	// WriteLight    - x:\gta5\src\dev\game\debug\AssetAnalysis\AssetAnalysis.cpp
	// WriteLights   - X:\gta5\src\dev\tools\AssetAnalysisTool\src\AssetAnalysisTool.cpp
	// aaLightRecord - X:\gta5\src\dev\tools\AssetAnalysisTool\src\AssetAnalysisTool.h
	// ==================================================================================

	// General data
	u8    m_colour[3];
	u8    m_flashiness;
	float m_intensity;
	u32   m_flags;
	s16   m_boneTag;
	u8    m_lightType;
	u8    m_groupId;
	u32   m_timeFlags;
	float m_falloff;
	float m_falloffExponent;
	float m_cullingPlane[4];
	u8    m_shadowBlur;
	u8    m_padding1;
	s16   m_padding2;
	u32   m_padding3;

	// Volume data
	float m_volIntensity;
	float m_volSizeScale;
	u8    m_volOuterColour[3];
	u8    m_lightHash;
	float m_volOuterIntensity;

	// Corona data
	float m_coronaSize;
	float m_volOuterExponent;
	u8    m_lightFadeDistance;
	u8    m_shadowFadeDistance;
	u8    m_specularFadeDistance;
	u8    m_volumetricFadeDistance;
	float m_shadowNearClip;
	float m_coronaIntensity;
	float m_coronaZBias;

	// Spot data
	float m_direction[3];
	float m_tangent[3];
	float m_coneInnerAngle;
	float m_coneOuterAngle;

	// Line data
	float m_extents[3];

	// Texture data
	u32 m_projectedTextureKey;
};
class aaLight : public aaLightRecord
{
public:
	typedef aaLightRecord RecordType;

	void Init(const std::xmap<u32,std::string>& st32, const std::xmap<u64,std::string>& st64, FILE* errFile = NULL, int* errCount = NULL)
	{
		SET_STRING64(Light,m_drawablePath);
		SET_STRING64(Light,m_projectedTexturePath);
	}

	bool operator <(const aaLight& rhs) const
	{
		// sort by drawable path
		const int cmp0 = strcmp(m_drawablePath, rhs.m_drawablePath);

		if      (cmp0 < 0) return true;
		else if (cmp0 > 0) return false;

		// .. then by light index in drawable
		return m_lightIndexInDrawable < rhs.m_lightIndexInDrawable;
	}

	const char* m_drawablePath;
	const char* m_projectedTexturePath;
};
// ==============================================

// ==============================================
class aaParticleRecord
{
public:
	enum { TAG = 'PART' };

	u32 m_tag;
	u32 m_physicalSize;
	u32 m_virtualSize;
	u64 m_assetPathHash;
};
class aaParticle : public aaParticleRecord
{
public:
	typedef aaParticleRecord RecordType;

	void Init(const std::xmap<u32,std::string>& st32, const std::xmap<u64,std::string>& st64, FILE* errFile = NULL, int* errCount = NULL)
	{
		SET_STRING64(Particle,m_assetPath);
	}

	bool operator <(const aaParticle& rhs) const
	{
		return strcmp(m_assetPath, rhs.m_assetPath) < 0;
	}

	const char* m_assetPath;
};
// ==============================================

// ==============================================
class aaMaterialRecord
{
public:
	enum { TAG = 'MAT ' };

	u32   m_tag;
	u64   m_materialPathHash;
	u64   m_diffTexPathHash;
	u64   m_bumpTexPathHash;
	float m_bumpiness;
	u64   m_specTexPathHash;
	float m_specIntensity;
	float m_specIntensityMask[3];
};
class aaMaterial : public aaMaterialRecord
{
public:
	typedef aaMaterialRecord RecordType;

	void Init(const std::xmap<u32,std::string>& st32, const std::xmap<u64,std::string>& st64, FILE* errFile = NULL, int* errCount = NULL)
	{
		SET_STRING64(Material,m_materialPath);
		SET_STRING64(Material,m_diffTexPath);
		SET_STRING64(Material,m_bumpTexPath);
		SET_STRING64(Material,m_specTexPath);

		m_shaderName = strrchr(m_materialPath, ']');

		if (m_shaderName && m_shaderName[1] == '_')
		{
			m_shaderName += 2;
			m_shaderNameHash = AssetAnalysis::crc32_lower(m_shaderName);
		}
		else
		{
			m_shaderName = "?";
			m_shaderNameHash = 0;
		}
	}

	bool operator <(const aaMaterial& rhs) const
	{
		return strcmp(m_materialPath, rhs.m_materialPath) < 0;
	}

	const char* m_materialPath; // <assetpath>/[<shaderindex>]_<shadername>
	const char* m_shaderName;
	u32         m_shaderNameHash;
	const char* m_diffTexPath;
	const char* m_bumpTexPath;
	const char* m_specTexPath;
};
// ==============================================

// ==============================================
class aaMaterialTextureVarRecord
{
public:
	enum { TAG = 'MTEX' };

	u32 m_tag;
	u32 m_materialVarNameHash;
	u64 m_materialPathHash;
	u64 m_texturePathHash;
};
class aaMaterialTextureVar : public aaMaterialTextureVarRecord
{
public:
	typedef aaMaterialTextureVarRecord RecordType;

	void Init(const std::xmap<u32,std::string>& st32, const std::xmap<u64,std::string>& st64, FILE* errFile = NULL, int* errCount = NULL)
	{
		SET_STRING32(MaterialTextureVar,m_materialVarName);
		SET_STRING64(MaterialTextureVar,m_materialPath);
		SET_STRING64(MaterialTextureVar,m_texturePath);

		m_shaderName = strrchr(m_materialPath, ']');

		if (m_shaderName && m_shaderName[1] == '_')
		{
			m_shaderName += 2;
			m_shaderNameHash = AssetAnalysis::crc32_lower(m_shaderName);
		}
		else
		{
			m_shaderName = "?";
			m_shaderNameHash = 0;
		}

		m_textureName = strrchr(m_texturePath, '/');

		if (m_textureName)
		{
			m_textureName++;
			m_textureNameHash = AssetAnalysis::crc64_lower(m_textureName);
		}
		else
		{
			m_textureName = "?";
			m_textureNameHash = 0;
		}

#if HIERARCHY
		m_drawable = NULL;
		m_nextVar  = NULL;
#endif // HIERARCHY
	}

	bool operator <(const aaMaterialTextureVar& rhs) const
	{
		// sort by material path
		const int cmp0 = strcmp(m_materialPath, rhs.m_materialPath);

		if      (cmp0 < 0) return true;
		else if (cmp0 > 0) return false;

		// .. then by material var name
		const int cmp1 = strcmp(m_materialVarName, rhs.m_materialVarName);

		if      (cmp1 < 0) return true;
		else if (cmp1 > 0) return false;

		return false;
	}

	const char* m_materialVarName;
	const char* m_materialPath;
	const char* m_shaderName;
	u32         m_shaderNameHash;
	const char* m_texturePath;
	const char* m_textureName;
	u64         m_textureNameHash;
#if HIERARCHY
	const aaDrawable*           m_drawable; // drawable this var belongs to
	const aaMaterialTextureVar* m_nextVar;  // vars in this material
#endif // HIERARCHY
};
// ==============================================

#undef SET_STRING32
#undef SET_STRING64

#pragma pack(pop)

#endif // _ASSETANALYSISTOOL_H_
