echo off

set TOOLDIR=%cd%
set AA_DIR=x:\gta5\assets_ng\non_final\aa\
set AA_REPORTS_DIR=%aa_dir%reports\

if not exist %AA_DIR% mkdir %AA_DIR%
if not exist %AA_REPORTS_DIR% mkdir %AA_REPORTS_DIR%

echo.
cd %AA_DIR%

call x:\gta5\assets_ng\non_final\aa\clean_aa.bat

echo.
cd %TOOLDIR%

del x:\sync.aa1

start "asset analysis tool"     %RS_CODEBRANCH%\tools\AssetAnalysisTool\AssetAnalysisTool.exe         -sync_in -sync_out -disableoutput -pause

pause
