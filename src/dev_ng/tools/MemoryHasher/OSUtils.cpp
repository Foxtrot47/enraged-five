#include "OSUtils.h"
static std::set<unsigned int> sm_AttemptedPids;

/**
 *	This function is used to get all of the executable blocks within a memory space.
 *  hProcess:	Handle to the process, that's already been opened
 *	memory	:	MemoryRegionArray block that is initialized and unpopulated.
 */
void GetProcessExecutableBlocks(HANDLE hProcess, MemoryRegionArray& memory, const char * nameFilter)
{
	unsigned char *p = NULL;
	MEMORY_BASIC_INFORMATION info;
	for ( p = NULL;
		VirtualQueryEx(hProcess, p, &info, sizeof(info)) == sizeof(info);
		p += info.RegionSize ) 
	{
		if (info.State == MEM_COMMIT && 
			(	info.Protect == PAGE_EXECUTE			||
				info.Protect == PAGE_EXECUTE_READ		|| 
				info.Protect == PAGE_EXECUTE_READWRITE	|| 
				info.Protect == PAGE_EXECUTE_WRITECOPY)) 
		{
			MemoryRegion mem;
			mem.baseAddress = info.BaseAddress;
			mem.allocationBase = info.AllocationBase;
			mem.size = info.RegionSize;
			char szModName[MAX_PATH] = {0};
			GetModuleFileNameEx(hProcess, (HMODULE) info.AllocationBase, szModName, MAX_PATH);
			if(nameFilter!=NULL)
			{
				if(strstr(szModName, nameFilter) == 0)
				{
					continue;
				}
			}
			// Push it onto our stack of candidate memory blocks
			memory.push_back(mem);
		}
	}
}

/**
 *	This function is used to get all of the executable blocks within a memory space.
 *  hProcess:	Handle to the process, that's already been opened
 *	memory	:	MemoryRegionArray block that is initialized and unpopulated.
 */
void GetProcessTextSection(HANDLE hProcess, MemoryRegionArray& memory, const char * nameFilter)
{
	unsigned char *p = NULL;
	MEMORY_BASIC_INFORMATION info;
	for ( p = NULL;
		VirtualQueryEx(hProcess, p, &info, sizeof(info)) == sizeof(info);
		p += info.RegionSize ) 
	{
		if (info.State == MEM_COMMIT && 
			(	info.Protect == PAGE_EXECUTE			||
			info.Protect == PAGE_EXECUTE_READ		|| 
			info.Protect == PAGE_EXECUTE_READWRITE	|| 
			info.Protect == PAGE_EXECUTE_WRITECOPY)) 
		{
			MemoryRegion mem;
			mem.baseAddress = info.BaseAddress;
			mem.allocationBase = info.AllocationBase;
			mem.size = info.RegionSize;
			char szModName[MAX_PATH] = {0};
			GetModuleFileNameEx(hProcess, (HMODULE) info.AllocationBase, szModName, MAX_PATH);
			if(nameFilter!=NULL)
			{
				if(strstr(szModName, nameFilter) == 0)
				{
					continue;
				}
			}

			if(info.RegionSize == 0x1000)
				continue;

			// Push it onto our stack of candidate memory blocks
			memory.push_back(mem);
		}
	}
}

void GetMatchingProcessBlocks(HANDLE hProcess, MemoryRegionArray& memory, DWORD memoryConstraints, int desiredSizeFilter)
{
	unsigned char *p = NULL;
	MEMORY_BASIC_INFORMATION info;
	// Size is given in Kb, so lets multiply it out correctly
	int kbSize = desiredSizeFilter * 1024;
	for ( p = NULL;
		VirtualQueryEx(hProcess, p, &info, sizeof(info)) == sizeof(info);
		p += info.RegionSize ) 
	{
		if (info.State == MEM_COMMIT && 
			((info.Protect & memoryConstraints & 0xFF) == (info.Protect & 0xFF)))
		{
			MemoryRegion mem;
			mem.baseAddress = info.BaseAddress;
			mem.allocationBase = info.AllocationBase;
			mem.size = info.RegionSize;
			mem.protects = info.Protect;
			if(desiredSizeFilter > 0)
			{
				int lowSizeVariance = (int)((double)kbSize * 0.90);
				int highSizeVariance = (int)((double)kbSize * 1.1);
				if( mem.size >= lowSizeVariance && mem.size <=highSizeVariance)
				{
					char szModName[MAX_PATH];
					GetModuleFileNameEx(hProcess, (HMODULE) info.AllocationBase, szModName, MAX_PATH);
					// Push it onto our stack of candidate memory blocks
					memory.push_back(mem);
				}
			}
			else
			{
				char szModName[MAX_PATH];
				GetModuleFileNameEx(hProcess, (HMODULE) info.AllocationBase, szModName, MAX_PATH);
				// Push it onto our stack of candidate memory blocks
				memory.push_back(mem);
			}
		}
	}
}

// This function gets a handle on the process
// The arguments are weird because it's setup to be used in a loop
// where the user can iterate through the applicable pids. 
// This is useful in the scenario where you have multiple copies of a game running, etc.
//
// ... I think.
void	GetProcessHandle(const char* pProcessName, HANDLE &processHandle, DWORD &processPid)
{

	if(!pProcessName)
	{
		Printf("No valid process name specified. Exiting.");
		exit(0);
	}
	DWORD processIds[256];
	DWORD numBytesReturned;

	if(!EnumProcesses(processIds, sizeof(processIds), &numBytesReturned))
	{
		Printf("Failed to enumerate processes");
		processHandle = 0;
		processPid = 0;
		return;
	}

	for(int i=0; i<numBytesReturned/sizeof(DWORD); i++)
	{
		char processName[259];
		DWORD size = sizeof(processName);

		// Ignore process 0
		if(processIds[i] == 0)
			continue;

		HANDLE hProcess = OpenProcess( PROCESS_QUERY_INFORMATION | PROCESS_VM_READ | PROCESS_DUP_HANDLE,
			FALSE, processIds[i] );

		if(hProcess == 0)
		{
			continue;
		}

		if(!QueryFullProcessImageName(hProcess, 0, processName, &size))
		{
			Printf("Failed to get process name\n");
			CloseHandle(hProcess);
			processHandle = 0;
			processPid = 0;
			return;
		}

		char* filename = strrchr(processName, '\\');

		if(filename)
		{
			filename++;
			//Printf("%s\n", filename);
			if((_stricmp(filename, pProcessName) == 0) &&
				sm_AttemptedPids.find(processIds[i])==sm_AttemptedPids.end())
			{
				Printf("Connected to process %s\n", filename);
				sm_AttemptedPids.insert(processIds[i]);
				processHandle = hProcess;
				processPid = processIds[i];
				return;
			}
		}
		CloseHandle(hProcess);
	}

	processHandle = 0;
	processPid = 0;
}

void DumpMemory(HANDLE hProcess,void* address, unsigned int length)
{
	u8* memoryDump = new u8[length];
	ReadProcessMemory(hProcess, address, memoryDump, length, NULL);
	
	
	Printf("[%lp]", address);
	// Convert our byte array to a string
	char *str = new char[length];

	unsigned char * pin = memoryDump;
	const char * hex = "0123456789ABCDEF";
	char * pout = str;
	int i = 0;
	for(; i < sizeof(memoryDump)-1; ++i){
		*pout++ = hex[(*pin>>4)&0xF];
		*pout++ = hex[(*pin++)&0xF];
		*pout++ = ' ';
	}
	*pout++ = hex[(*pin>>4)&0xF];
	*pout++ = hex[(*pin)&0xF];
	*pout = 0;

	Printf("\t[%s]", str);
	//delete [] str;
	//delete [] memoryDump;
}

// This function gets the appropriate starting address of the module in question
void* GetStartingAddress(HANDLE hProcess, MemoryRegionArray memoryArray,
			const unsigned int addrH, const unsigned int addrL, 
			const char * moduleName, const unsigned int offset)
{
	u64 addr = 0;

	// If the user provides us with a low and high address, just use those
	if(addrL && addrH)
	{
		addr += ((u64)addrH) << 32 | (u64)addrL;
	}
	// Otherwise, if they give us a module name, lets index into that
	else if (moduleName)
	{
		// find the dll
		for(int i=0; i<memoryArray.size(); i++)
		{
			char moduleNameRead[MAX_PATH];
			if(GetModuleFileNameEx(hProcess, (HMODULE)memoryArray[i].allocationBase, moduleNameRead,MAX_PATH))
			{
				char* filename = strrchr((char*)moduleNameRead, '\\');
				if(filename)
				{
					filename++;
					if(!_stricmp(filename, moduleName))
					{
						// Handle the scenario where they also give me an offset, and don't
						// want to start from the begining of the image
						addr = (u64)memoryArray[i].baseAddress+offset;
						break;
					}
				}	
			}
		}
	}
	else
	{
		Printf("Insufficient parameters to continue execution.");
		exit(0);
	}

	return (void*)addr;
}

// Do a GetProcAddress based on the library name and the function name
PVOID GetLibraryProcAddress(PSTR libraryName, PSTR nameOfFunction)
{
	return GetProcAddress(GetModuleHandleA(libraryName), nameOfFunction);
}

const char * GetProtectsStr(DWORD protects)
{
	
	if (protects & PAGE_READONLY)				{ return "R";}
	if (protects & PAGE_READWRITE)				{ return "R/W";}
	if (protects & PAGE_WRITECOPY)				{ return "WC";}
	if (protects & PAGE_EXECUTE)				{ return "X";}
	if (protects & PAGE_EXECUTE_READ)			{ return "R/X";}
	if (protects & PAGE_EXECUTE_READWRITE)		{ return "R/W/X";}
	if (protects & PAGE_EXECUTE_WRITECOPY)		{ return "W/X";}
	return "INVALID";
}

void PrintMatchedBlocks(HANDLE hProcess, const MemoryRegionArray& memory)
{
	for(int i=0; i<memory.size(); i++)
	{
		//HMODULE hModule;
		char moduleName[256]="|unknown|";
		//if(GetModuleHandleEx (GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS | GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT, 
		//(LPCTSTR)memory[i].baseAddress, &hModule))
		{
			GetModuleFileNameEx(hProcess, (HMODULE)memory[i].allocationBase, moduleName, sizeof(moduleName));
		}
		Printf("Addr %p, size %lu, module:%s protects: %s\n", memory[i].baseAddress, memory[i].size, moduleName, GetProtectsStr(memory[i].protects));
	}
}

void SaveMemoryRegion(const char *pFilename, HANDLE hProcess, void* pAddr, u32 size)
{
	FILE* pFile = fopen(pFilename, "wb");
	if(pFile == NULL)
	{
		Printf("Failed to open %s\n", pFilename);
		return;
	}
	u8* pMem = new u8[size];
	ReadProcessMemory(hProcess, pAddr, pMem, size, NULL);
	fwrite(pMem, 1, size, pFile);
	fclose(pFile);
	delete[] pMem;
	Printf("Save memory 0x%p size 0x%x to file %s\n", pAddr, size, pFilename);
}


