#ifndef STRICT
# define STRICT
#endif
# pragma warning(push)
# pragma warning(disable: 4668)
# include <WinSock2.h>  //Prevent windows.h from including winsock.h
# include <windows.h>
# pragma warning(pop)

#include <stdio.h>

#include "string/string.h"

#if !__NO_OUTPUT

void Printf(const char* fmt, va_list args)
{
	char buffer[4096];
	vformatf(buffer,sizeof(buffer),fmt,args);
	printf("%s", buffer);
#if __DEV
	OutputDebugString(buffer);
#endif
}

void Printf(char const* fmt, ...)
{
	va_list args;
	va_start(args,fmt);
	Printf(fmt, args);
	va_end(args);
}

void Warningf(char const* fmt, ...)
{
	va_list args;
	va_start(args,fmt);
	Printf(fmt, args);
	va_end(args);
}

void Quitf(char const* fmt, ...)
{
	va_list args;
	va_start(args,fmt);
	Printf(fmt, args);
	va_end(args);
	exit(-1);
}

void Quitf(int rt, char const* fmt, ...)
{
	va_list args;
	va_start(args,fmt);
	Printf(fmt, args);
	va_end(args);
	exit(rt);
}

#endif 

void QuitNoOutput(int rt)
{
	exit(rt);
}

namespace rage {

int diagAssertHelper(char const *file,int line, char const *msg, ...)
{
	char buffer[4096];
	formatf(buffer,sizeof(buffer),"%s: %s:Line:%s", msg, file, line);

	va_list args;
	va_start(args,msg);
	Printf(buffer, args);
	va_end(args);
	return 1;
}


int diagAssertHelper2(char const *file,int line, char const *msg)
{
	diagAssertHelper(file, line, msg);
	return 1;
}

int vformatf_n( char *dest, size_t maxLen, const char *fmt, va_list args )
{
	// Standard version is annoying like strncpy in that it
	// won't guarantee nul termination.

#if defined(__GNUC__) || defined(__SNC__)
	DEV_ONLY(memset(dest,0xFE,maxLen));	// Fill buffer with crap to catch bad size quickly
	dest[ maxLen - 1 ] = 0;
	return vsnprintf( dest, maxLen, fmt, args );
#else
	DEV_ONLY(memset(dest,0xFE,maxLen));	// Fill buffer with crap to catch bad size quickly
	dest[ --maxLen ] = 0;
	int res = _vsnprintf( dest, maxLen, fmt, args );
#if __ASSERT && __GAMETOOL
	Assert( res <= maxLen );
#endif 
	return res;
#endif
}


} // namespace rage
