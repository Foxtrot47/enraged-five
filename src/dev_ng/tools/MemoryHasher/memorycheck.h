// Copied from GTA codebase
#ifndef MEMORY_CHECK_H
#define MEMORY_CHECK_H


struct MemoryCheck
{
	enum ActionFlags
	{
		FLAG_Telemetry			 = 0x00000001,
		FLAG_Report				 = 0x00000002,
		FLAG_FrackDefrag		 = 0x00000004,
		FLAG_KickRandomly		 = 0x00000008,
		FLAG_NoMatchmake		 = 0x00000010,
		FLAG_Gameserver			 = 0x00000020,
		FLAG_Crash				 = 0x00000040,
		FLAG_MemRead			 = 0x00000200,
		FLAG_MemReadwrite		 = 0x00000400,
		FLAG_MemWrite			 = 0x00000800,
		FLAG_MemExecute			 = 0x00001000,
		FLAG_MemExecuteRead		 = 0x00002000,
		FLAG_MemExecuteWrite	 = 0x00004000,
		FLAG_MemExecuteReadwrite = 0x00008000,
		FLAG_SP_Processed		 = 0x08000000,
		FLAG_MP_Processed		 = 0x80000000
	};

	enum CheckType
	{
		// For the check to pass, the computed CRC must equal nValue.
		CHECK_CRCEquals = 0,

		// For the check to pass, the computed CRC must NOT equal nValue.
		CHECK_CRCNotEquals = 1,

		// For the check to pass, every byte in the range must equal the low 8 bits of nValue.
		CHECK_RangeAllBytesCRC = 2,

		// For the check to pass, every u32 in the range must equal nValue.
		CHECK_RangeAllWordsCRC = 3,

		// For the check to pass, dll name shouldn't be available
		CHECK_DllName = 4,

		// For the check to pass, process with name shouldn't be running on the system
		CHECK_ProcessNames = 5,

		// For the check to pass, the range must not contain the pattern of size nSize>>16 represented by nValue
		CHECK_ByteString = 6,

		// Modified version of chilead, that utilises the page siezs.
		CHECK_PageByteString = 7,

		// Modified version of chilead, that utilises the page siezs.; negated
		CHECK_ExecutableChecker = 8

	};

	static const u32 s_magicXorValue = 0xb7ac4b1c;
	static const u32 s_versionAndSkuMask = 0x00ffffff;
	static const u32 s_typeMask = 0xff000000;

	// Helper function to access the unobfuscated check type
	__forceinline CheckType getCheckType() const { return (CheckType)((nVersionAndType ^ nValue ^ s_magicXorValue) >> 24); }
	__forceinline static u32 makeVersionAndSku(int version, u8 sku)
	{
		return (((((u32)version)&0xffff)  | (((u32)sku) << 16)) ^ s_magicXorValue) & s_versionAndSkuMask;
	}
	void setVersionAndType(int version, u8 sku, CheckType type) { nVersionAndType = ((((type << 24) ^ s_magicXorValue) & s_typeMask) | makeVersionAndSku(version, sku)) ^ nValue; }

	unsigned nVersionAndType;	// Top 8 bits = check type, bottom 24 bits = version
	unsigned nAddressStart;
	unsigned nSize;
	unsigned nValue;
	unsigned nFlags; 
};

#endif