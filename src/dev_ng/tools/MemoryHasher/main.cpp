#ifndef STRICT
# define STRICT
#endif


// C headers
#include <malloc.h>
#include <stdio.h>
#include <vector>

//Rage headers
#include "atl/array.h"
#include "string/stringhash.h"
#include "system/param.h"

#include "memorycheck.h"
#include "HashCreator.h"
// Actually supported arguments
// Generic First
PARAM(type, "Type of Signature Generation");
PARAM(actionFlags, "Flags to include in hash info for tunables, 1=telemetry, 2=report, 8=randomkick, 16=no matchmake, 32=gameserver report");
PARAM(versionNumber, "Game version string");
PARAM(length, "length of memory");

// Needed for DllName
PARAM(dllname, "Name of the DLL to hash and create a signature for");

// Needed for bytestring
PARAM(bytestring, "String to scan for in memory (in 2-chr hex bytes, eg, \"31 2E 32\" which is \"1.2\" in text)");
PARAM(moduleName, "Name of the module to find a byte-string in");
PARAM(offset, "offset into memory region (overrides addr)");
PARAM(addrL, "memory address Low 32bits");
PARAM(addrH, "memory address High 32bits");

// Needed for pagebytestring
PARAM(lowPageNumber, "Low-end Page Number Index");
PARAM(highPageNumber, "High-end Page Number Index");
PARAM(size, "Size of the matching block of memory");
PARAM(invertedSearch, "Negate the PBS search");
// Process name
PARAM(processname, "The name of the process that we want to hash / blacklist");

// Old ones I need to port upwards
PARAM(targetExecutable, "Executable name");

// This one's useful. It'll give us the information at runtime about the blocks
// That we're actually inspecting
PARAM(printmatchedblocks, "print details about matched regions");
PARAM(sku, "SKU ID");

// Needed for RSA Signing
PARAM(input, "Base64 input provided of hashes in-memory-representation");
// Deprecating these two. We won't need to save to files, and I 
// display the memory by default when we pick up duplicates.
//PARAM(savetofile, "Save memory to indicated file");
//PARAM(dumpscanmem, "Dump the memory used for bytes to verify");

void PrecheckActionFlags(const char * pType, int actionFlags)
{

	if (pType!= NULL || strlen(pType) != 0)
	{
		if(stricmp(pType, "rsasign") != 0 )
		{
			if((actionFlags >> 8) == 0)
			{
				Printf("Error - you have not provided any memory regions this should apply to. Exiting.");
				exit(0);
			}
		}
	}
}

int main(int argc, char**argv)
{
	MemoryRegionArray memory;

	const char* pType;
	int skuid = 0;
	int actionFlags = 0;

	sysParam::Init(argc, argv);

	// First lets get the type of signature we're generating
	PARAM_type.Get(pType);

	PARAM_actionFlags.Get(actionFlags);
	if(PARAM_sku.Get())
		PARAM_sku.Get(skuid);

	u8 skuShort = (u8)skuid;

	PrecheckActionFlags(pType, actionFlags);

	if (pType== NULL || strlen(pType) == 0)
	{
		Printf("Warning: No type specified.\n");
		if (PARAM_printmatchedblocks.Get())
		{
			Printf("Attempting to read executable blocks.\n");
			const char * pTargetExecutable = NULL;
			PARAM_targetExecutable.Get(pTargetExecutable);

			if(pTargetExecutable == NULL || strlen(pTargetExecutable) == 0)
			{
				Printf("Error: No parameter for targetExecutable provided: don't know what process to read from!\n");
				exit(0);
			}
			HANDLE hProcess;
			DWORD processPid;
			GetProcessHandle(pTargetExecutable, hProcess, processPid);
			// Check to be sure that the process exists
			if(hProcess == 0)
			{
				Printf("Failed to find process\n");
				exit(0);
			}
			GetMatchingProcessBlocks(hProcess, memory, actionFlags >> 8);
			PrintMatchedBlocks(hProcess, memory);
		}

	}
	else if(stricmp(pType, "dllname") == 0 )
	{
		// Excellent. We want to check DLL Names

		// First lets set the Hashed DLL to NULL
		
		// Now lets ensure that  we can get the name of the DLL
		if( PARAM_dllname.Get() &&	PARAM_versionNumber.Get())
		{
			const char* pDllName=NULL;
			int versionNumber = 0;
			PARAM_dllname.Get(pDllName);
			PARAM_versionNumber.Get(versionNumber);
			HashCreator::NameHash(pDllName, actionFlags, versionNumber, MemoryCheck::CHECK_DllName, skuShort);
		}
	}
	else if (stricmp(pType, "bytestring") == 0 )
	{
		
		int versionNumber = 0;
		int length = 0;
		int offset = 0;
		unsigned int addrH = 0;
		unsigned int addrL = 0;
		const char * pByteString = NULL;
		const char * pTargetExecutable = NULL;
		const char * pModuleName = NULL;
		// Get our parameters
		PARAM_bytestring.Get(pByteString);
		PARAM_versionNumber.Get(versionNumber);
		PARAM_length.Get(length);
		PARAM_offset.Get(offset);
		PARAM_targetExecutable.Get(pTargetExecutable);
		PARAM_moduleName.Get(pModuleName);
		PARAM_addrH.Get(addrH);
		PARAM_addrL.Get(addrL);
		// Initialize Process Memory, as we'll need to iterate through it
		HANDLE hProcess;
		DWORD processPid;
		
		GetProcessHandle(pTargetExecutable, hProcess, processPid);
		// Check to be sure that the process exists
		if(hProcess == 0)
		{
			Printf("Failed to find process\n");
			exit(0);
		} 
		GetMatchingProcessBlocks(hProcess, memory, actionFlags >> 8);

		if (PARAM_printmatchedblocks.Get())
		{
			PrintMatchedBlocks(hProcess, memory);
		}
		void* pStartingAddress = GetStartingAddress(hProcess, memory, addrH, addrL, pModuleName, offset);
		HashCreator::ByteStringHash(hProcess, memory, pByteString, length, 
			actionFlags, versionNumber,	pStartingAddress, skuShort);

		CloseHandle(hProcess);
	}
	else if (stricmp(pType, "pagebytestring") == 0 )
	{
		int versionNumber = 0;
		int lowPageNumber = 0;
		int size = 0;
		int highPageNumber = 0;
		const char * pByteString = NULL;
		const char * pTargetExecutable = NULL;
		bool invertedSearch = 0;
		// Get our parameters
		PARAM_bytestring.Get(pByteString);
		PARAM_lowPageNumber.Get(lowPageNumber);
		PARAM_highPageNumber.Get(highPageNumber);
		PARAM_versionNumber.Get(versionNumber);
		PARAM_targetExecutable.Get(pTargetExecutable);
		PARAM_size.Get(size);
		
		invertedSearch = PARAM_invertedSearch.Get();
		// Initialize Process Memory, as we'll need to iterate through it
		HANDLE hProcess = NULL;
		DWORD processPid = 0;
		if(( lowPageNumber >> 16) > 0)
		{
			Printf("Error - your low-page bit number has too many bits. Exiting.");
			exit(0);
		}
		if(( highPageNumber >> 16) > 0)
		{
			Printf("Error - your low-page bit number has too many bits. Exiting.");
			exit(0);
		}
		if(size <=0 || size > 0x3FFFF)
		{
			Printf("Error - invalid size of %d specified. Exiting.", size);
			exit(0);
		}
		std::string targetExeStr(pTargetExecutable);
		if(targetExeStr.find("elf") == std::string::npos)
		{
			GetProcessHandle(pTargetExecutable, hProcess, processPid);
			// Check to be sure that the process exists
			if(hProcess == 0)
			{
				Printf("Failed to find process\n");
				exit(0);
			} 
			GetMatchingProcessBlocks(hProcess, memory, actionFlags >> 8, size);


			if (PARAM_printmatchedblocks.Get())
			{
				PrintMatchedBlocks(hProcess, memory);
			}

			HashCreator::PageByteStringHash(
				hProcess,
				memory,
				lowPageNumber,
				highPageNumber,
				size,
				pByteString,
				versionNumber,
				skuShort,
				actionFlags);
		}
		else
		{
			HashCreator::PageByteStringHashPS4(
				lowPageNumber,
				highPageNumber,
				size,
				pByteString,
				versionNumber,
				skuShort,
				actionFlags);
		}
	
		if(targetExeStr.find("elf") == std::string::npos) { CloseHandle(hProcess); }
	}
	else if (stricmp(pType, "exectuablechecker") == 0 )
	{
		int versionNumber = 0;
		int lowPageNumber = 0;
		int highPageNumber = 0;
		const char * pByteString = NULL;
		const char * pTargetExecutable = NULL;
		// Get our parameters
		PARAM_bytestring.Get(pByteString);
		PARAM_lowPageNumber.Get(lowPageNumber);
		PARAM_highPageNumber.Get(highPageNumber);
		PARAM_versionNumber.Get(versionNumber);
		PARAM_targetExecutable.Get(pTargetExecutable);
		//PARAM_size.Get(size);

		// Initialize Process Memory, as we'll need to iterate through it
		HANDLE hProcess = NULL;
		DWORD processPid = 0;
		if(( lowPageNumber >> 16) > 0)
		{
			Printf("Error - your low-page bit number has too many bits. Exiting.");
			exit(0);
		}
		if(( highPageNumber >> 16) > 0)
		{
			Printf("Error - your low-page bit number has too many bits. Exiting.");
			exit(0);
		}

		GetProcessHandle(pTargetExecutable, hProcess, processPid);
		// Check to be sure that the process exists
		if(hProcess == 0)
		{
			Printf("Failed to find process\n");
			exit(0);
		} 
		GetProcessTextSection(hProcess, memory, pTargetExecutable);


		if (PARAM_printmatchedblocks.Get())
		{
			PrintMatchedBlocks(hProcess, memory);
		}

		HashCreator::ExecutableCheckerStringHash(
			hProcess,
			memory,
			lowPageNumber,
			highPageNumber,
		//	size,
			pByteString,
			versionNumber,
			skuShort,
			actionFlags);

	}
	else if (stricmp(pType, "verifyequality") == 0 )
	{
		int versionNumber = 0;
		int length = 0;
		int offset = 0;
		unsigned int addrH = 0;
		unsigned int addrL = 0;
		const char * pByteString = NULL;
		const char * pTargetExecutable = NULL;
		const char * pModuleName = NULL;
		// Get our parameters
		PARAM_bytestring.Get(pByteString);
		PARAM_versionNumber.Get(versionNumber);
		PARAM_length.Get(length);
		PARAM_offset.Get(offset);
		PARAM_targetExecutable.Get(pTargetExecutable);
		PARAM_moduleName.Get(pModuleName);
		// Initialize Process Memory, as we'll need to iterate through it
		HANDLE hProcess = NULL;
		DWORD processPid = 0;

		GetProcessHandle(pTargetExecutable, hProcess, processPid);
		// Check to be sure that the process exists
		if (hProcess == 0)
		{
			Printf("Failed to find process\n");
			exit(0);
		}
		GetProcessExecutableBlocks(hProcess, memory);

		void* pStartingAddress = GetStartingAddress(hProcess, memory, addrH, addrL, pModuleName, offset);
		HashCreator::VerifyEquality(hProcess, memory, pByteString, length,
			actionFlags, versionNumber, pStartingAddress, skuShort);

		CloseHandle(hProcess);
	}
	else if (stricmp(pType, "rsasign") == 0 )
	{
		const char * pInput;
		PARAM_input.Get(pInput);
		HashCreator::RsaSign(pInput);
	}
	// Disabling until we have the privacy conversation
	//else if(stricmp(pType, "processname") == 0 )
	//{
	//	// Excellent. We want to check DLL Names
	//
	//	// First lets set the Hashed DLL to NULL
	//
	//	// Now lets ensure that  we can get the name of the DLL
	//	if( PARAM_processname.Get() &&  PARAM_actionFlags.Get() &&	PARAM_versionNumber.Get())
	//	{
	//		const char* pProcessName=NULL;
	//		int actionFlags = 0;
	//		int versionNumber = 0;
	//		PARAM_processname.Get(pProcessName);
	//		PARAM_actionFlags.Get(actionFlags);
	//		PARAM_versionNumber.Get(versionNumber);
	//		HashCreator::NameHash(pProcessName, actionFlags, versionNumber, MemoryCheck::CHECK_ProcessName);
	//	}
	//}
	else
	{
		Printf("The type of memory signature you want to generate [%s] is not available.", pType);
		exit(0);
	}
	return 0;
}

