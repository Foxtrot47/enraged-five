#include "HashCreator.h"
#include <stdlib.h>
#include <sstream>
#include "Arxan/TransformIT/x64/include/TFIT_RSA_iRSA1024.h"
#include "TFIT_keys.h"

#pragma comment(lib, "TFIT_Utils_64.lib")
#pragma comment(lib, "TFIT_RSA_iRSA1024_64.lib")
// This must match the CRC function in the GenerateCRCs tool!
u32 HashCreator::crcByteRange(const u8* pBuff, const u8* pEnd)
{
	// Based on the Jenkins hash from http://en.wikipedia.org/wiki/Jenkins_hash_function, with different initial value
	// This happens to be the same as the hash we use with atDataHash, except not restricted to 4-byte blocks.
	u32 hash = 0x04c11db7;
	while(pBuff != pEnd)
	{
		hash += *pBuff++;
		hash += (hash << 10);
		hash ^= (hash >> 6);
	}
	hash += (hash << 3);
	hash ^= (hash >> 11);
	hash += (hash << 15);
	return hash;
}

// Use this to CRC strings. This is different from the standard hashing routine we use in GTA
u32 HashCreator::crcString(const char* pString)
{
	size_t length = strlen(pString);
	char* pCopy = (char*)alloca(length+1);
	strcpy(pCopy, pString);
	_strupr(pCopy);

	return crcByteRange((u8*)pCopy, (u8*)pCopy+length);
}

#define HASH_OUTPUT_FORMAT "[%d,%d,%d,%u,%d]"

void HashCreator::NameHash(const char* dllName, const int actionFlags, const int versionNumber, MemoryCheck::CheckType checkType, u8 sku)
{
	u32 hash = crcString(dllName);
	MemoryCheck memoryCheck;
	memoryCheck.nValue = hash;
	memoryCheck.nAddressStart = hash ^ MemoryCheck::s_magicXorValue;
	memoryCheck.nSize = hash ^ MemoryCheck::s_magicXorValue;
	memoryCheck.nFlags = actionFlags  ^ hash ^ MemoryCheck::s_magicXorValue;
	memoryCheck.setVersionAndType(versionNumber, sku, checkType);
	Printf("\n\nRaw info    [0x%08x,0x%08x,0x%08x]", versionNumber, hash, actionFlags);
	Printf("\nHash            "HASH_OUTPUT_FORMAT"\n", memoryCheck.nVersionAndType, memoryCheck.nAddressStart, memoryCheck.nSize, memoryCheck.nValue, memoryCheck.nFlags);
	Printf("Database Entry: [%d]", hash);
}


void HashCreator::ScriptDetectHash(const int versionNumber, MemoryCheck::CheckType checkType, u8 sku)
{
	u32 hash = crcString("A1372F1E");
	MemoryCheck memoryCheck;
	memoryCheck.nValue = hash;
	memoryCheck.nAddressStart = hash ^ MemoryCheck::s_magicXorValue;
	memoryCheck.nSize = hash ^ MemoryCheck::s_magicXorValue;
	memoryCheck.nFlags = 0x54CD13D7  ^ hash ^ MemoryCheck::s_magicXorValue;
	memoryCheck.setVersionAndType(versionNumber, sku, checkType);
	Printf("Hash            "HASH_OUTPUT_FORMAT"\n", memoryCheck.nVersionAndType, memoryCheck.nAddressStart, memoryCheck.nSize, memoryCheck.nValue, memoryCheck.nFlags);
	Printf("Database Entry: [%d]", hash);
}


void HashCreator::VerifyEquality(HANDLE hProcess, MemoryRegionArray memoryArray,
	const char* byteString, const unsigned int length,
	const unsigned int actionFlags, const unsigned int versionNumber, void *startingAddress, u8 sku)
{

	u32 localLength = length;
	u32 hash = 0;
	MemoryCheck::CheckType type = MemoryCheck::CHECK_CRCEquals;

	// Iterate over each of the memory blocks
	for (int i = 0; i < memoryArray.size(); i++)
	{
		// As aforementioned, make sure that it sanity checks the memory area that we're going into
		// It does a simple check for base address, and making sure that it's within the size constraints
		if ((startingAddress >= memoryArray[i].baseAddress) &&
			((((u8*)startingAddress) + localLength - memoryArray[i].size) <= memoryArray[i].baseAddress))
		{
			u64 startAddressVal = (u8*)startingAddress - (u8*)memoryArray[i].baseAddress;

			// Now lets create the hash of the byte range
			char byteStringHex[1024] = { '\0' };
			u8 composite[1024] = { 0 };
			int compositeLength = 0;
			bool failedStringCheck = false;
			bool foundMany = false;
			// Ensure that the byte string is non-null
			if (byteString)
			{
				// Another null check it seems
				if (byteString != NULL && byteString[0] != '\0')
				{
					// Iterate over the byte array, and make it a to-upper, for standardization
					int i = 0;
					for (i = 0; i < strlen(byteString); i++)
					{
						byteStringHex[i] = (char)toupper(byteString[i]);
						// Ensure that it's a hex character
						if (!((byteStringHex[i] >= '0' && byteStringHex[i] <= '9') || (byteStringHex[i] >= 'A' && byteStringHex[i] <= 'F')) && byteStringHex[i] != ' ')
						{
							Printf("Error:  Invalid character in signature string\n");
							failedStringCheck = true;
							break;
						}
					}
					// If we've failed, move on
					if (!failedStringCheck)
					{
						// If we haven't failed, copy it into our composite array, 
						// converting it to an unsigned long integer, so it's true data
						// instead of just a byte string
						byteStringHex[i] = '\0';
						for (i = 0; i < strlen(byteStringHex); i++)
						{
							if (byteStringHex[i] != ' ')
							{
								char *endchr = &byteStringHex[i + 1];
								composite[compositeLength] = (u8)strtoul(&byteStringHex[i], &endchr, 16);
								i++;
								compositeLength++;
							}
						}
					}
				}
				else
					failedStringCheck = true;

				if (failedStringCheck)
				{
					Printf("Error:  Must specify valid signature string\n");
					continue;
				}
				// Before we go through and actually create the hash, we're going to do a search
				// in the process' memory space to ensure that there's only one copy of this byte 
				// string. Otherwise, Sean is going to ban people incorrectly and have a bad week
				std::string needle(composite, composite + compositeLength);

				// Allocate an array big enough to store the data we've said we're going to look inside
				u8* haystackMemory = new u8[localLength];
				ReadProcessMemory(hProcess, startingAddress, haystackMemory, localLength, NULL);
				std::string haystackString(haystackMemory, haystackMemory + localLength);

				// Now do a find, to be sure that it exists
				std::string::size_type startPos = 0;
				bool foundOne = false;
				while (std::string::npos != (startPos = haystackString.find(needle, startPos)))
				{
					if (foundOne)
					{
						// In this conditional, we've already found one instance, so we need
						// to display the pertinent information for multiple copies
						// I think useful information would be the offsets
						Printf("\nWARNING: FOUND MORE THAN ONE COPY OF THE BYTE RANGE");
						foundMany = true;
					}
					else
					{
						// Set our boolean so we don't error out shortly below
						foundOne = true;
					}
					u64 val = (u64)startingAddress;
					val += startPos;
					Printf("\nByte Range found: ", val);
					DumpMemory(hProcess, (void*)(val), localLength);
					// Increase our iterator so we can continue to find multiple copies
					++startPos;
				}

				if (!foundOne)
				{
					Printf("\nError: didn't find any of your string in the selected memory region.");
					continue;
				}

				// Now that we have a composite array of hex bytes instead of a byte string
				// we can finally go through and actually create a hash of the range
				hash = crcByteRange(composite, composite + compositeLength);
				// Sanity length checking, before we pack some data into the upper bits
				if (localLength <= 0xffff)
				{
					if (compositeLength > 255)
						Printf("Error:  signature string must be < %d bytes\n", 0xff + 1);
					// Pack the composite length into the upper bits
					// What we're doing here is storing the first byte so that in our range
					// We know what we have to look for
					localLength |= ((u32)composite[0]) << 24;
					// And here we're storing the composite length, so we know how mnay 
					// bytes we should hash up at runtime
					localLength |= (compositeLength & 0xff) << 16;
				}
				else
				{
					Printf("Error:  Scan block size must be < %d for use with stringcheck\n", 0xffff + 1);
					localLength = (compositeLength << 16) | 0xffff;
					localLength |= ((u32)composite[0]) << 24;
				}
				delete[] haystackMemory;
			}
			if (!foundMany)
			{
				// Now lets create our memory check values for the user
				MemoryCheck memoryCheck;
				// Throw the values together
				memoryCheck.nValue = hash;
				memoryCheck.nAddressStart = ((u32)startAddressVal) ^ hash ^ MemoryCheck::s_magicXorValue;
				memoryCheck.nSize = localLength ^ hash ^ MemoryCheck::s_magicXorValue;
				memoryCheck.nFlags = actionFlags  ^ hash ^ MemoryCheck::s_magicXorValue;
				memoryCheck.setVersionAndType(versionNumber, sku, type);
				// Display them
				Printf("\n\nRaw info    [0x%08x,+0x%08x,0x%08x,0x%08x,0x%08x]", (type << 24) | versionNumber, (u32)startAddressVal, localLength, hash, actionFlags);
				Printf("\nMemory hash	"HASH_OUTPUT_FORMAT"\n", memoryCheck.nVersionAndType, memoryCheck.nAddressStart, memoryCheck.nSize, memoryCheck.nValue, memoryCheck.nFlags);
				Printf("Database Entry: [%d]\n", hash);
			}
		}
	}

}

u8 * HashCreator::BytifyString(const char * byteString, int &compositeLength)
{
	char byteStringHex[1024]={'\0'};
	u8 *composite = new u8[1024];
	memset(composite, 0, 1024);
	bool failedStringCheck = false;
	// Another null check it seems
	if(byteString!=NULL && byteString[0]!='\0')
	{
		// Iterate over the byte array, and make it a to-upper, for standardization
		int i=0;
		for(i=0;i<strlen(byteString);i++)
		{
			byteStringHex[i]=(char)toupper(byteString[i]);
			// Ensure that it's a hex character
			if(!((byteStringHex[i]>='0' && byteStringHex[i]<='9') || (byteStringHex[i]>='A' && byteStringHex[i]<='F')) && byteStringHex[i]!=' ')
			{
				Printf("Error:  Invalid character in signature string\n");
				failedStringCheck = true;
				break;
			}
		}
		// If we've failed, move on
		if(!failedStringCheck)
		{
			// If we haven't failed, copy it into our composite array, 
			// converting it to an unsigned long integer, so it's true data
			// instead of just a byte string
			byteStringHex[i]='\0';
			for(i=0;i<strlen(byteStringHex);i++)
			{
				if(byteStringHex[i]!=' ')
				{
					char *endchr=&byteStringHex[i+1];
					composite[compositeLength] = (u8)strtoul(&byteStringHex[i],&endchr,16);
					i++;
					compositeLength++;
				}
			}
		}
	}
	else
		failedStringCheck = true;

	if(failedStringCheck)
	{
		Printf("Error - Couldn't build byte string. Exiting.");
		exit(0);
	}
	else
		return composite;
}


void HashCreator::PageByteStringHash(
	HANDLE hProcess, 
	MemoryRegionArray& memoryArray, 
	const int pageLow, 
	const int pageHigh, 
	const int sizeOfModule,
	const char * inputBytes,
	const unsigned int versionNumber,
	u8 sku, 
	const int actionFlags)
{
	MemoryRegionArray filteredMemory = memoryArray;
	MemoryCheck::CheckType type = MemoryCheck::CHECK_PageByteString;
	// Lets get our correct addresses
	int pageSize = 4*1024;
	int lowPageOffset = pageLow * pageSize;
	int highPageOffset = pageHigh * pageSize;
	bool foundMany = false;
	bool foundOne = false;
	int bytesToSearchForLength = 0;
	u32 hash = 0;
	// Create a string from the memory read
	u8* bytesToSearchFor = BytifyString(inputBytes, bytesToSearchForLength);
	std::string strBytesToSearchFor(bytesToSearchFor, bytesToSearchFor+bytesToSearchForLength);
	
	// Loop through each of the candidates
	for(int i=0; i<memoryArray.size(); i++)
	{
		// Set the base address
		u64 allocBase = (u64)memoryArray[i].baseAddress;
		u64 lowAddr		= allocBase + lowPageOffset;
		u64 highAddr	= allocBase + highPageOffset;
		int numToRead = (int)(highAddr-lowAddr);
		// Allocate space
		u8* bytes	= new u8[numToRead];
		// Set to 0
		memset(bytes, 0, numToRead);
		// Read memory
		ReadProcessMemory(hProcess, (void*)lowAddr, bytes, numToRead, NULL);
		
		std::string strBytesRead(bytes, bytes+numToRead);
		std::string::size_type startPos=0;
		
		while(std::string::npos != (startPos = strBytesRead.find(strBytesToSearchFor, startPos)))
		{
			if(foundOne)
			{
				// In this conditional, we've already found one instance, so we need
				// to display the pertinent information for multiple copies
				// I think useful information would be the offsets
				Printf("\nWARNING: FOUND MORE THAN ONE COPY OF THE BYTE RANGE");
				foundMany  = true;
			}
			else
			{
				// Set our boolean so we don't error out shortly below
				foundOne = true;
			}
			u64 val = (u64)lowAddr;
			val+=startPos;
			Printf("\nByte Range found: ", val);
			DumpMemory(hProcess, (void*)(val), numToRead);
			// Increase our iterator so we can continue to find multiple copies
			++startPos;
		}

		hash = crcByteRange(bytesToSearchFor, bytesToSearchFor+bytesToSearchForLength);
	}

	if(!foundMany && foundOne)
	{
		// Time to do my magic packing, to get this to work for Sean.
		MemoryCheck memoryCheck;
		// The hash is easy
		memoryCheck.nValue = hash;
		// Version/Type/Sku is easy.
		memoryCheck.setVersionAndType(versionNumber, sku, type);
		// The Address Start is going to be something else:
		// The top 16 bits are going to be the High Page #
		// The bot 16 bits are going to be the Low Page #
		// Error handling / checking has been done up front higher in the call stack
		u32 rawAddressStart = ((pageHigh << 16) | (pageLow & 0xFFFF));
		memoryCheck.nAddressStart = rawAddressStart ^ hash ^ MemoryCheck::s_magicXorValue;

		// The size is going to be packed as well, with all kinds of fuckery
		// Bits 00:17 Size of the targeted block of memory
		// Bits 18:23 Length of the byte string
		// Bits 24:31 First byte in the string to search for
		// Lets mask away, just in case
		u32 rawSize = 
			((bytesToSearchFor[0]		<< 24) & 0xFF000000 ) |
			((bytesToSearchForLength	<< 18) & 0x00FC0000 ) |
			((sizeOfModule)					   & 0x0003FFFF ) ;
		memoryCheck.nSize = rawSize ^ hash ^ MemoryCheck::s_magicXorValue;
		memoryCheck.nFlags = actionFlags ^ hash ^ MemoryCheck::s_magicXorValue;
	
		// Display them
		Printf("\n\nRaw info    [0x%08x,+0x%08x,0x%08x,0x%08x,0x%08x]", (type<<24)|versionNumber, (u32)rawAddressStart, rawSize, hash, actionFlags);
		Printf("\nMemory hash	"HASH_OUTPUT_FORMAT"\n", memoryCheck.nVersionAndType, memoryCheck.nAddressStart, memoryCheck.nSize, memoryCheck.nValue, memoryCheck.nFlags);
		Printf("Database Entry: [%d]\n",hash);
	}
	else
	{
		Printf("Error - Many or no matches found in the provided arguments.");
		exit(0);
	}

}


void HashCreator::ExecutableCheckerStringHash(
	HANDLE hProcess, 
	MemoryRegionArray& memoryArray, 
	const int pageLow, 
	const int pageHigh, 
	const char * inputBytes,
	const unsigned int versionNumber,
	u8 sku, 
	const int actionFlags)
{
	MemoryRegionArray filteredMemory = memoryArray;
	MemoryCheck::CheckType type = MemoryCheck::CHECK_ExecutableChecker;
	// Lets get our correct addresses
	int pageSize = 4*1024;
	int lowPageOffset = pageLow * pageSize;
	int highPageOffset = pageHigh * pageSize;
	bool foundMany = false;
	bool foundOne = false;
	int bytesToSearchForLength = 0;
	u32 hash = 0;
	// Create a string from the memory read
	u8* bytesToSearchFor = BytifyString(inputBytes, bytesToSearchForLength);
	std::string strBytesToSearchFor(bytesToSearchFor, bytesToSearchFor+bytesToSearchForLength);

	// Loop through each of the candidates
	for(int i=0; i<memoryArray.size(); i++)
	{
		// Set the base address
		u64 allocBase = (u64)memoryArray[i].baseAddress;
		u64 lowAddr		= allocBase + lowPageOffset;
		u64 highAddr	= allocBase + highPageOffset;
		int numToRead = (int)(highAddr-lowAddr);
		// Allocate space
		u8* bytes	= new u8[numToRead];
		// Set to 0
		memset(bytes, 0, numToRead);
		// Read memory
		ReadProcessMemory(hProcess, (void*)lowAddr, bytes, numToRead, NULL);

		std::string strBytesRead(bytes, bytes+numToRead);
		std::string::size_type startPos=0;
		std::set<u64> addressesFound;
		while(std::string::npos != (startPos = strBytesRead.find(strBytesToSearchFor, startPos)))
		{

			u64 val = (u64)lowAddr;
			val+=startPos;
			bool printOutput = false;

			// Add a check that we haven't doubled up on our addresses
			if(foundOne)
			{
				if(addressesFound.find(val) != addressesFound.end())
				{
					printOutput = false;
				}
				else
				{
					// In this conditional, we've already found one instance, so we need
					// to display the pertinent information for multiple copies
					// I think useful information would be the offsets
					Printf("\nWARNING: FOUND MORE THAN ONE COPY OF THE BYTE RANGE");
					printOutput = true;
					foundMany  = true;
				}
			}
			else
			{
				// Set our boolean so we don't error out shortly below
				foundOne = true;
				addressesFound.insert(val);
				printOutput = true;
			}

			if(printOutput)
			{
				Printf("\nByte Range found: ", val);
				DumpMemory(hProcess, (void*)(val), numToRead);
			}
			// Increase our iterator so we can continue to find multiple copies
			++startPos;
		}

		hash = crcByteRange(bytesToSearchFor, bytesToSearchFor+bytesToSearchForLength);
	}

	if(!foundMany && foundOne)
	{
		// Time to do my magic packing, to get this to work for Sean.
		MemoryCheck memoryCheck;
		// The hash is easy
		memoryCheck.nValue = hash;
		// Version/Type/Sku is easy.
		memoryCheck.setVersionAndType(versionNumber, sku, type);
		// The Address Start is going to be something else:
		// The top 16 bits are going to be the High Page #
		// The bot 16 bits are going to be the Low Page #
		// Error handling / checking has been done up front higher in the call stack
		u32 rawAddressStart = ((pageHigh << 16) | (pageLow & 0xFFFF));
		memoryCheck.nAddressStart = rawAddressStart ^ hash ^ MemoryCheck::s_magicXorValue;

		// The size is going to be packed as well, with all kinds of fuckery
		// Bits 00:17 Size of the targeted block of memory
		// Bits 18:23 Length of the byte string
		// Bits 24:31 First byte in the string to search for
		// Lets mask away, just in case
		u32 rawSize = 
			((bytesToSearchFor[0]		<< 24) & 0xFF000000 ) |
			((bytesToSearchForLength	<< 18) & 0x00FC0000 );
		memoryCheck.nSize = rawSize ^ hash ^ MemoryCheck::s_magicXorValue;
		memoryCheck.nFlags = actionFlags ^ hash ^ MemoryCheck::s_magicXorValue;

		// Display them
		Printf("\n\nRaw info    [0x%08x,+0x%08x,0x%08x,0x%08x,0x%08x]", (type<<24)|versionNumber, (u32)rawAddressStart, rawSize, hash, actionFlags);
		Printf("\nMemory hash	"HASH_OUTPUT_FORMAT"\n", memoryCheck.nVersionAndType, memoryCheck.nAddressStart, memoryCheck.nSize, memoryCheck.nValue, memoryCheck.nFlags);
		Printf("Database Entry: [%d]\n",hash);
	}
	else
	{
		Printf("Error - Many or no matches found in the provided arguments.");
		exit(0);
	}

}


void HashCreator::PageByteStringHashPS4(
	const int pageLow, 
	const int pageHigh, 
	const int sizeOfModule,
	const char * inputBytes,
	const unsigned int versionNumber,
	u8 sku, 
	const int actionFlags)
{

	MemoryCheck::CheckType type = MemoryCheck::CHECK_PageByteString;

	// Lets get our correct addresses
	int bytesToSearchForLength = 0;
	u32 hash = 0;
	// Create a string from the memory read
	u8* bytesToSearchFor = BytifyString(inputBytes, bytesToSearchForLength);
	std::string strBytesToSearchFor(bytesToSearchFor, bytesToSearchFor+bytesToSearchForLength);

	hash = crcByteRange(bytesToSearchFor, bytesToSearchFor+bytesToSearchForLength);

	// Time to do my magic packing, to get this to work for Sean.
	MemoryCheck memoryCheck;
	// The hash is easy
	memoryCheck.nValue = hash;
	// Version/Type/Sku is easy.
	memoryCheck.setVersionAndType(versionNumber, sku, type);
	// The Address Start is going to be something else:
	// The top 16 bits are going to be the High Page #
	// The bot 16 bits are going to be the Low Page #
	// Error handling / checking has been done up front higher in the call stack
	u32 rawAddressStart = ((pageHigh << 16) | (pageLow & 0xFFFF));
	memoryCheck.nAddressStart = rawAddressStart ^ hash ^ MemoryCheck::s_magicXorValue;

	// The size is going to be packed as well, with all kinds of fuckery
	// Bits 00:17 Size of the targeted block of memory
	// Bits 18:23 Length of the byte string
	// Bits 24:31 First byte in the string to search for
	// Lets mask away, just in case
	u32 rawSize = 
		((bytesToSearchFor[0]		<< 24) & 0xFF000000 ) |
		((bytesToSearchForLength	<< 18) & 0x00FC0000 ) |
		((sizeOfModule)					   & 0x0003FFFF ) ;
	memoryCheck.nSize = rawSize ^ hash ^ MemoryCheck::s_magicXorValue;
	memoryCheck.nFlags = actionFlags ^ hash ^ MemoryCheck::s_magicXorValue;

	// Display them
	Printf("\n\nRaw info    [0x%08x,+0x%08x,0x%08x,0x%08x,0x%08x]", (type<<24)|versionNumber, (u32)rawAddressStart, rawSize, hash, actionFlags);
	Printf("\nMemory hash	"HASH_OUTPUT_FORMAT"\n", memoryCheck.nVersionAndType, memoryCheck.nAddressStart, memoryCheck.nSize, memoryCheck.nValue, memoryCheck.nFlags);
	Printf("Database Entry: [%d]\n",hash);
	

}

void HashCreator::ByteStringHash(
	HANDLE hProcess,
	MemoryRegionArray memoryArray, 
	const char* byteString,
	const unsigned int length, 
	const unsigned int actionFlags,
	const unsigned int versionNumber,
	void *startingAddress,
	u8 sku)
{

	u32 localLength = length;
	u32 hash = 0;
	MemoryCheck::CheckType type = MemoryCheck::CHECK_ByteString;
	

	// Now we get the applicable base address of the module that we want to index into
	// This will prove to be a sanity check below to be sure that we're targetting the memory
	// block we want, though it seems like this has no actual bearing on the hash of the bytes
	
	// Iterate over each of the memory blocks
	for(int i = 0; i < memoryArray.size(); i++)
	{
		// As aforementioned, make sure that it sanity checks the memory area that we're going into
		// It does a simple check for base address, and making sure that it's within the size constraints
		if(	(startingAddress >= memoryArray[i].baseAddress)	&& 
			((((u8*)startingAddress) + localLength - memoryArray[i].size) <= memoryArray[i].baseAddress))
		{
			u64 startAddressVal= (u8*)startingAddress - (u8*)memoryArray[i].baseAddress;

			// Now lets create the hash of the byte range
			u8 *composite;
			int compositeLength= 0;
			bool foundMany = false;
			// Ensure that the byte string is non-null
			if(byteString)
			{
				composite = BytifyString(byteString, compositeLength);
				// Before we go through and actually create the hash, we're going to do a search
				// in the process' memory space to ensure that there's only one copy of this byte 
				// string. Otherwise, Sean is going to ban people incorrectly and have a bad week
				std::string needle(composite, composite+compositeLength);
				
				if(localLength < compositeLength)
				{
					Printf("\nWarning: Length of the input string [%d] is greater than the amount of length specified to read from process memory [%d] .", localLength, compositeLength);
					continue;
				}
				// Allocate an array big enough to store the data we've said we're going to look inside
				u8* haystackMemory = new u8[localLength];
				ReadProcessMemory(hProcess, startingAddress, haystackMemory, localLength, NULL);
				std::string haystackString(haystackMemory, haystackMemory+localLength);

				// Now do a find, to be sure that it exists
				std::string::size_type startPos=0;
				bool foundOne = false;
				while(std::string::npos != (startPos = haystackString.find(needle, startPos)))
				{
					if(foundOne)
					{
						// In this conditional, we've already found one instance, so we need
						// to display the pertinent information for multiple copies
						// I think useful information would be the offsets
						Printf("\nWARNING: FOUND MORE THAN ONE COPY OF THE BYTE RANGE");
						foundMany  = true;
					}
					else
					{
						// Set our boolean so we don't error out shortly below
						foundOne = true;
					}
					u64 val = (u64)startingAddress;
					val+=startPos;
					Printf("\nByte Range found: ", val);
					DumpMemory(hProcess, (void*)(val), localLength);
					// Increase our iterator so we can continue to find multiple copies
					++startPos;
				}

				if(!foundOne)
				{
					Printf("\nError: didn't find any of your string in the selected memory region.");
					continue;
				}

				// Now that we have a composite array of hex bytes instead of a byte string
				// we can finally go through and actually create a hash of the range
				hash = crcByteRange(composite, composite + compositeLength);
				// Sanity length checking, before we pack some data into the upper bits
				if (localLength<=0xffff)
				{
					if(compositeLength>255)
						Printf("Error:  signature string must be < %d bytes\n",0xff+1);
					// Pack the composite length into the upper bits
					// What we're doing here is storing the first byte so that in our range
					// We know what we have to look for
					localLength |= ((u32)composite[0])<<24;
					// And here we're storing the composite length, so we know how mnay 
					// bytes we should hash up at runtime
					localLength |= (compositeLength&0xff)<<16;
				}
				else
				{
					Printf("Error:  Scan block size must be < %d for use with stringcheck\n",0xffff+1);
					localLength = (compositeLength<<16)|0xffff;
					localLength |= ((u32)composite[0])<<24;
				}
				delete [] haystackMemory;
			}
			if(!foundMany)
			{
				// Now lets create our memory check values for the user
				MemoryCheck memoryCheck;
				// Throw the values together
				memoryCheck.nValue = hash;
				memoryCheck.nAddressStart = ((u32)startAddressVal) ^ hash ^ MemoryCheck::s_magicXorValue;
				memoryCheck.nSize = localLength ^ hash ^ MemoryCheck::s_magicXorValue;
				memoryCheck.nFlags = actionFlags  ^ hash ^ MemoryCheck::s_magicXorValue;
				memoryCheck.setVersionAndType(versionNumber, sku, type);
				// Display them
				Printf("\n\nRaw info    [0x%08x,+0x%08x,0x%08x,0x%08x,0x%08x]", (type<<24)|versionNumber, (u32)startAddressVal, localLength, hash, actionFlags);
				Printf("\nMemory hash	"HASH_OUTPUT_FORMAT"\n", memoryCheck.nVersionAndType, memoryCheck.nAddressStart, memoryCheck.nSize, memoryCheck.nValue, memoryCheck.nFlags);
				Printf("Database Entry: [%d]\n",hash);
			}	
		}
	}
}

void HashCreator::RsaSign(const char* input)
{
	std::vector<unsigned char> buff;
	std::vector<unsigned char> decodedInput = Base64Decode(input);
	buff.insert(buff.end(), decodedInput.begin(), decodedInput.end());

	unsigned char signature[2048] = {0};
	int bytes_written = 0;
	unsigned char *buffPtr = &buff[0];
	TFIT_rsa_context_iRSA1024_t ctx;
	int ret = 0;
	TFIT_rsa_key_iRSA1024_t privKey;
	ret = TFIT_rsa_deserialize_key_iRSA1024(TFIT_rsa_priv_iRSA1024, TFIT_rsa_priv_iRSA1024_len, &privKey);
	if(ret!=TFIT_WBRSA_OK) {Printf("Error: Failed to deserialize PRIV TFIT RSA key"); }
	TFIT_rsa_prepare_context_iRSA1024(&privKey, &ctx);

	ret = TFIT_rsa_sign_iRSA1024(WBC_SIGTYPE_PKCS1_SHA256, 
			buffPtr, 
			(int)buff.size(), 
			&privKey,
			&ctx, 
			signature,
			&bytes_written);
	if(ret!=TFIT_WBRSA_OK) {Printf("Error: Failed to sign with TFIT RSA key"); }

	std::string base64rsaSignature= Base64Encode(signature, bytes_written);


	TFIT_rsa_context_iRSA1024_t verifyCtx;

	TFIT_rsa_key_iRSA1024_t pubKey;
	ret = TFIT_rsa_deserialize_key_iRSA1024(TFIT_rsa_pub_iRSA1024, TFIT_rsa_pub_iRSA1024_len, &pubKey);
	if(ret!=TFIT_WBRSA_OK) {Printf("Error: Failed to deserialize TFIT PUB RSA key"); }
	TFIT_rsa_prepare_context_iRSA1024(&pubKey, &verifyCtx);

	ret = TFIT_rsa_verify_iRSA1024(WBC_SIGTYPE_PKCS1_SHA256,
		buffPtr,
		(int)buff.size(), 
		signature,
		bytes_written,
		&pubKey,
		&verifyCtx);
	if(ret!=TFIT_WBRSA_OK) {Printf("Error: Failed to verify generated RSA Signature"); }

	std::ostringstream oss;
	oss << "[";
	for(int j = 0; j < bytes_written/4; j++)
	{
		unsigned *ptr = (unsigned int *)(signature+j*4);
		oss << *ptr << ",";
	}
	oss.seekp(-1, std::ios_base::end);
	oss << "]";
	Printf("%s", oss.str().c_str());
	
}
static const std::string base64_chars = 
	"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	"abcdefghijklmnopqrstuvwxyz"
	"0123456789+/";


static inline bool is_base64(BYTE c) {
	return (isalnum(c) || (c == '+') || (c == '/'));
}

std::string HashCreator::Base64Encode(BYTE const* buf, unsigned int bufLen) {
	std::string ret;
	int i = 0;
	int j = 0;
	BYTE char_array_3[3];
	BYTE char_array_4[4];

	while (bufLen--) {
		char_array_3[i++] = *(buf++);
		if (i == 3) {
			char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
			char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
			char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
			char_array_4[3] = char_array_3[2] & 0x3f;

			for(i = 0; (i <4) ; i++)
				ret += base64_chars[char_array_4[i]];
			i = 0;
		}
	}

	if (i)
	{
		for(j = i; j < 3; j++)
			char_array_3[j] = '\0';

		char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
		char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
		char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
		char_array_4[3] = char_array_3[2] & 0x3f;

		for (j = 0; (j < i + 1); j++)
			ret += base64_chars[char_array_4[j]];

		while((i++ < 3))
			ret += '=';
	}

	return ret;
}


std::vector<BYTE> HashCreator::Base64Decode(std::string const& encoded_string) {
	int in_len = (int)encoded_string.size();
	int i = 0;
	int j = 0;
	int in_ = 0;
	BYTE char_array_4[4], char_array_3[3];
	std::vector<BYTE> ret;

	while (in_len-- && ( encoded_string[in_] != '=') && is_base64(encoded_string[in_])) {
		char_array_4[i++] = encoded_string[in_]; in_++;
		if (i ==4) {
			for (i = 0; i <4; i++)
				char_array_4[i] = (BYTE)base64_chars.find(char_array_4[i]);

			char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
			char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
			char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

			for (i = 0; (i < 3); i++)
				ret.push_back(char_array_3[i]);
			i = 0;
		}
	}

	if (i) {
		for (j = i; j <4; j++)
			char_array_4[j] = 0;

		for (j = 0; j <4; j++)
			char_array_4[j] = (BYTE)base64_chars.find(char_array_4[j]);

		char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
		char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
		char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

		for (j = 0; (j < i - 1); j++) ret.push_back(char_array_3[j]);
	}

	return ret;
}