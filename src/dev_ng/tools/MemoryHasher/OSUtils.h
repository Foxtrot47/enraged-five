#ifndef OS_UTILS_H
#define OS_UTILS_H
# pragma warning(push)
# pragma warning(disable: 4668)
# include <WinSock2.h>  //Prevent windows.h from including winsock.h
# include <windows.h>
# pragma warning(pop)

#include <psapi.h>
#pragma comment(lib, "Psapi.lib")
#include <stdlib.h>
#include <vector>

#include <set>

//Rage headers
#include "atl/array.h"
#include "string/stringhash.h"
#include "system/param.h"

struct MemoryRegion
{
	void* baseAddress;
	void* allocationBase;
	size_t size;
	DWORD protects;

};
typedef std::vector<MemoryRegion> MemoryRegionArray;

typedef NTSTATUS (NTAPI *_NtQuerySystemInformation)(
	ULONG SystemInformationClass,
	PVOID SystemInformation,
	ULONG SystemInformationLength,
	PULONG ReturnLength
	);
typedef NTSTATUS (NTAPI *_NtDuplicateObject)(
	HANDLE SourceProcessHandle,
	HANDLE SourceHandle,
	HANDLE TargetProcessHandle,
	PHANDLE TargetHandle,
	ACCESS_MASK DesiredAccess,
	ULONG Attributes,
	ULONG Options
	);
typedef NTSTATUS (NTAPI *_NtQueryObject)(
	HANDLE ObjectHandle,
	ULONG ObjectInformationClass,
	PVOID ObjectInformation,
	ULONG ObjectInformationLength,
	PULONG ReturnLength
	);

void	GetProcessExecutableBlocks(HANDLE hProcess, MemoryRegionArray& memoryArray, const char *nameFilter = NULL);
void    GetProcessTextSection(HANDLE hProcess, MemoryRegionArray& memoryArray, const char *nameFilter = NULL);
void	GetMatchingProcessBlocks(HANDLE hProcess, MemoryRegionArray& memoryArray, DWORD permissions, int desiredSizeFilter = -1);
void	GetProcessHandle(const char* pProcessName, HANDLE &processHandle, DWORD &processPid);
void	DumpMemory(HANDLE hProcess,void* address, unsigned int length);
void *	GetStartingAddress(HANDLE hProcess, MemoryRegionArray memoryArray,
								const unsigned int addrH, const unsigned int addrL, 
								const char * moduleName, const unsigned int offset);
void PrintMatchedBlocks(HANDLE hProcess, const MemoryRegionArray& memory);
void SaveMemoryRegion(const char *pFilename, HANDLE hProcess, void* pAddr, u32 size);

#endif