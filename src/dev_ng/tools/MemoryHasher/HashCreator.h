#ifndef HASH_CREATOR_H
#define HASH_CREATOR_H

#include <stdlib.h>
#include <stdio.h>
# pragma warning(push)
# pragma warning(disable: 4668)
# include <WinSock2.h>  //Prevent windows.h from including winsock.h
# include <windows.h>
# pragma warning(pop)

#include <psapi.h>
#pragma comment(lib, "Psapi.lib")
#include "OSUtils.h"
#include "memorycheck.h"

//Rage headers
#include "atl/array.h"
#include "string/stringhash.h"
#include "system/param.h"

#include <vector>
#include <string>

class HashCreator
{
public:
	static void ScriptDetectHash(
		const int versionNumber,
		MemoryCheck::CheckType checkType,
		u8 sku);
	static void NameHash(
		const char* dllName,
		const int flags,
		const int version,
		const MemoryCheck::CheckType,
		u8 sku);
	static void ByteStringHash(
		HANDLE hProcess, 
		MemoryRegionArray memoryArray,
		const char* byteString,
		const unsigned  int size, 
		const unsigned int actionFlags, 
		const unsigned int versionNumber,
		void * startingAddress,
		u8 sku);
	static u8 * BytifyString(
		const char * byteString,
		int &compositeLength);

	static void PageByteStringHash(
		HANDLE hProcess, 
		MemoryRegionArray& memoryArray, 
		const int pageLow, 
		const int pageHigh,
		const int sizeOfModule, 
		const char * inputBytes,
		const unsigned int version,
		u8 sku,
		const int actionFlags);

	static void ExecutableCheckerStringHash(
		HANDLE hProcess, 
		MemoryRegionArray& memoryArray, 
		const int pageLow, 
		const int pageHigh,
		const char * inputBytes,
		const unsigned int version,
		u8 sku,
		const int actionFlags);

	static void PageByteStringHashPS4(
		const int pageLow, 
		const int pageHigh,
		const int sizeOfModule, 
		const char * inputBytes,
		const unsigned int version,
		u8 sku,
		const int actionFlags);

	static void VerifyEquality(
		HANDLE hProcess,
		MemoryRegionArray memoryArray,
		const char* byteString,
		const unsigned  int size,
		const unsigned int actionFlags,
		const unsigned int versionNumber,
		void * startingAddress, u8 sku);

	static void HashCreator::RsaSign(const char* input);
	static std::vector<BYTE> HashCreator::Base64Decode(std::string const& encoded_string);
	static std::string HashCreator::Base64Encode(BYTE const* buf, unsigned int bufLen);
private:
	static u32 crcByteRange(
		const u8* pBuff,
		const u8* pEnd);
	static u32 crcString(const char* pString);
};
#endif