
#include "pathserver/PathServer.h"
#include "pathserver/ExportCollision.h"

#include "fwscene/stores/mapdatastore.h"
#include "fwscene/stores/maptypesstore.h"
#include "scene/world/GameWorld.h"
#include "scene/portals/portal.h"
#include "scene/scene.h"
#include "scene/loader/mapTypes.h"
#include "scene/loader/mapFileMgr.h"
#include "vehicles/Trailer.h"
#include "vehicles/VehicleFactory.h"
#include "physics/physics.h"
#include "peds/ped.h"
#include "peds/popcycle.h"

#include "renderer/GtaDrawable.h"
#include "renderer/lights/lights.h"
#include "renderer/lights/LightEntity.h"

#include "ToolScene.h"

//-----------------------------------------------------------------------------

// PURPOSE: This interface is used for some game-specific extensions to CPathServer.
static CPathServerGameInterfaceGta s_PathServerGameInterface;

void CToolScene::Init(unsigned UNUSED_PARAM(initMode))
{
	// WRAP this in eMapExport to allow big pools
	CNavMeshDataExporter::SetExportMode(CNavMeshDataExporter::eMapExport);

	// The code here was originally adapted from CScene::Init(). /FF
	fwScene::InitClass(rage_new CGtaSceneInterface());
	fwScene::Init();

	CNavMeshDataExporter::SetExportMode(CNavMeshDataExporter::eNotExporting);


	// BODGE fwMapDataLoadedNode Pool
//	fwMapDataLoadedNode::ShutdownPool(); 
//	fwMapDataLoadedNode::InitPool( 50000, MEMBUCKET_WORLD );
	// BODGE LightEntity Pool
	CLightEntity::ShutdownPool();
	CLightEntity::InitPool( 50000, MEMBUCKET_RENDER );

	INSTANCE_STORE.Init(INIT_CORE);

	// Maybe necessary for Water::Init():
	//	CLights::Init(); 
	CModelInfo::Init(INIT_CORE); 
//TEMP	ThePaths.Init(INIT_CORE);
//TEMP  CPathServer::Init(s_PathServerGameInterface, CPathServer::EMultiThreaded);
	CVehicle::InitSystem();

	CGameWorld::Init(INIT_CORE); // depends on InitPools()
	CPhysics::Init(INIT_CORE); // depends on InitPools() 
	// Technically, it would be more correct to call this (since we need water data),
	// but currently Init(INIT_CORE) only seems to do graphics stuff, and may have
	// some issues on PC. /FF
	//	Water::Init(INIT_CORE); // dependent on CLights::Init()

	CMapFileMgr::Init(INIT_CORE);
	g_MapTypesStore.Init(INIT_CORE);
}


void CToolScene::Update()
{
	CVehicleModelInfo::UpdateHDRequests();

	CPortal::Update();

	// Disabled, this was using the camera position. Instead,
	// we make sure in CNavMeshDataExporterTool::ProcessCollisionExport()
	// that the CCompEntity objects get streamed in and updated.
	//	CCompEntity::Update();

//TEMP	CPopCycle::Update();

	CPhysics::UpdateRequests();		// Needed for streaming in buildings (interiors?).

//TEMP	ThePaths.Update();

	// This doesn't do much except failing an assert if the player doesn't exist
	// (which is true in our case).
	//	CPathServer::Process();
	// though we may still want this to be set:
	CPathServer::m_bGameRunning = true;

	CScene::TidyReferences();


	// Added to prevent drawlist refs hanging around
	gDrawListMgr->RemoveAllRefs();

}
