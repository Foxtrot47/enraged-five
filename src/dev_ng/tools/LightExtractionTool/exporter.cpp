#include "exporter.h"

// Rage headers
#include "diag/output.h"
#include "system/param.h"
#include "system/xtl.h"		// For MEMORYSTATUS, etc.

#include "renderer/GtaDrawable.h"
#include "renderer/lights/lights.h"
#include "renderer/lights/LightEntity.h"


// Framework headers
#include "fwscene/stores/boxstreamer.h"
#include "fwscene/stores/mapdatastore.h"
#include "fwscene/stores/staticboundsstore.h"
#include "fwscene/world/WorldLimits.h"
#include "spatialdata\sphere.h"

// Game headers
#include "camera/viewports/ViewportManager.h"
#include "Objects/DummyObject.h"
#include "Objects/Object.h"
#include "Objects/ObjectPopulation.h"
#include "PathServer/PathServer.h"
#include "Physics/Physics.h"
#include "Renderer/PlantsMgr.h"
#include "Renderer/Water.h"
#include "scene/scene.h"
#include "scene/entities/compEntity.h"
#include "scene/FocusEntity.h"
#include "scene/portals/Portal.h"
#include "scene/streamer/streamvolume.h"
#include "scene/world/GameWorld.h"
#include "script/script.h"			// CTheScripts
#include "streaming/streaming.h"
#include "Task/Movement/Climbing/TaskGoToAndClimbLadder.h"
#include "control/gamelogic.h"
#include "renderer/DrawLists/drawListMgr.h"

//OPTIMISATIONS_OFF()

//-----------------------------------------------------------------

XPARAM(nopeds);
XPARAM(nocars);
XPARAM(noambient);
XPARAM(nonetwork);
XPARAM(level);

#if __WIN32PC && __BANK
XPARAM(hidewidgets);			// Currently defined in 'bank/pane.cpp'.
#endif

namespace rage
{
	XPARAM(noaudio);
	XPARAM(noaudiothread);
	XPARAM(nowaveslots);

#if __WIN32PC && !__FINAL
	XPARAM(hidewindow);			// Currently defined in 'grcore/device_win32.cpp'.
#endif
}	// namespace rage

#if __WIN32
#pragma warning(push)
#pragma warning(disable: 4355) // 'this' used in base member initializer list
#endif


//////////////////////////////////////////////////////////////////////////////
/////////////////////	CLightExporterInterfaceTool	   ///////////////////////
//////////////////////////////////////////////////////////////////////////////

CLightExporterInterfaceTool::CLightExporterInterfaceTool(CLightsExporterTool &tool)
: m_Tool(&tool)
{

}

bool CLightExporterInterfaceTool::EntityIntersectsCurrentNavMesh(CEntity* pEntity) const
{
	return m_Tool->EntityIntersectsCurrentNavMesh(pEntity);
}

bool CLightExporterInterfaceTool::ExtentsIntersectCurrentNavMesh(Vec3V_In vMin, Vec3V_In vMax) const
{
	return m_Tool->ExtentsIntersectCurrentNavMesh(vMin, vMax);
}


bool CLightExporterInterfaceTool::IsAlreadyInPhysicsLevel(CPhysical* pPhysical, phArchetype* pArchetype) const
{
	return CLightsExporterTool::IsAlreadyInPhysicsLevel(pPhysical, pArchetype);
}

fwPtrList & CLightExporterInterfaceTool::BuildActiveBuildingsList()
{
	return m_Tool->BuildActiveBuildingsList();
}

void CLightExporterInterfaceTool::AppendToActiveBuildingsArray(atArray<CEntity*>& UNUSED_PARAM(entityArray))
{
}


void CLightExporterInterfaceTool::AddSearches(atArray<fwBoxStreamerSearch>& searchList)
{
	m_Tool->AddSearches(searchList);
}

//////////////////////////////////////////////////////////////////////////////
/////////////////////	CLightExporterPassInfo		   ///////////////////////
//////////////////////////////////////////////////////////////////////////////

CLightExporterPassInfo	g_ExportInfo[] = 
{
	{	"LODLights",			"",					false	},
	{	"PrologueLODLights",	"prologue",			true	}
};

//////////////////////////////////////////////////////////////////////////////
/////////////////////	CLightsExporterTool			   ///////////////////////
//////////////////////////////////////////////////////////////////////////////

CLightsExporterTool::CLightsExporterTool(fwLevelProcessTool * pTool) : m_ExporterInterface(*this), fwLevelProcessToolExporterInterface(pTool)
{
	CNavMeshDataExporter::SetActiveExporter(&m_ExporterInterface);

	// Prevent any Drawables being made while this is on.
	CNavMeshDataExporter::SetExportMode(CNavMeshDataExporter::eMapExport);

}

CLightsExporterTool::~CLightsExporterTool()
{

}

void CLightsExporterTool::PreInit()
{
	// This is needed for now to prevent the pools from becoming too small when the values
	// from the configuration file override the larger values in code. We may want to
	// solve this in a different way, perhaps either loading a secondary configuration
	// file containing the larger pool values, or adding a conditional section within
	// the single configuration file which is somehow only used from this tool.
	fwConfigManager::SetUseLargerPoolFromCodeOrData(true);

	PARAM_noaudio.Set("1");
	PARAM_noaudiothread.Set("1");
	PARAM_nowaveslots.Set("1");
	PARAM_nopeds.Set("1");
	PARAM_nocars.Set("1");
	PARAM_noambient.Set("1");
	PARAM_nonetwork.Set("1");

#if __WIN32PC && !__FINAL
	PARAM_hidewindow.Set("1");
#endif

#if __WIN32PC && __BANK
	PARAM_hidewidgets.Set("1");
#endif
}


#define MAP_SECTION_START_INDEX	(0)

void CLightsExporterTool::Init()
{
	m_ExportInfoNum = 0;
	m_MapSectionIDX = 0;

	m_MapSectionIDX = MAP_SECTION_START_INDEX;


	// NOTE, check this is what the game is setting
	// NO LONGER QUIRED
	//fwMapData::SetEntityPriorityCap((fwEntityDef::ePriorityLevel)fwEntityDef::PRI_REQUIRED);
}

void CLightsExporterTool::Shutdown()
{
}

bool CLightsExporterTool::UpdateExport()
{
	if(CStreaming::GetNumberObjectsRequested() <= 0)
	{

#if 0	// DEBUG, don't make the data, load from XML

		int	numInfos = sizeof(g_ExportInfo) / sizeof(CLightExporterPassInfo);
		for(m_ExportInfoNum = 0; m_ExportInfoNum<numInfos; m_ExportInfoNum++)
		{
			m_pExportInfo = &g_ExportInfo[m_ExportInfoNum];

			// Init a LightDataCollector
			m_pLightDataCollector = rage_new CLightDataCollector(m_pExportInfo->pName);
			m_pLightDataCollector->LoadAllLights();
			m_pLightDataCollector->PostProcessCollectedLights();
			m_pLightDataCollector->ChopUpCollectedLightData(m_pExportInfo);

			delete m_pLightDataCollector;
		}

		// Exit
		ExitProcess(0);

#else	// DEBUG

		if(m_MapSectionIDX == MAP_SECTION_START_INDEX)
		{
			m_pExportInfo = &g_ExportInfo[m_ExportInfoNum];

			// Init a LightDataCollector
			m_pLightDataCollector = rage_new CLightDataCollector(m_pExportInfo->pName);
		}

		// Allow drawables to be created.
		CNavMeshDataExporter::SetExportMode(CNavMeshDataExporter::eNotExporting);
		// run over imap store
		fwMapDataStore& mapStore = fwMapDataStore::GetStore();

		if( m_MapSectionIDX >= mapStore.GetCount() )
		{
			Displayf( "COMPLETE: Collected %d Lights (%s)", m_pLightDataCollector->GetCountOfLightsCollected(), m_pExportInfo->pName );
			// Save all the lights (for reference)
			m_pLightDataCollector->SaveAllLights();
			m_pLightDataCollector->PostProcessCollectedLights();

			m_pLightDataCollector->ChopUpCollectedLightData(m_pExportInfo);

			delete m_pLightDataCollector;

			m_MapSectionIDX = 0;
			m_ExportInfoNum++;

			int	numInfos = sizeof(g_ExportInfo) / sizeof(CLightExporterPassInfo);
			if( m_ExportInfoNum >= numInfos )
			{
				// Exit
				ExitProcess(0);
			}

		}
		else
		{

			bool	doProcess = false;
			fwMapDataDef* pDef = mapStore.GetSlot(strLocalIndex(m_MapSectionIDX));
			if(pDef)
			{
				if( pDef->GetIsValid() &&													// only want a real IMAP file, not an empty slot in the store
					pDef->GetContentFlags()&fwMapData::CONTENTFLAG_ENTITIES_HD &&			// only want IMAP file containing high-detail entities
					!(pDef->GetContentFlags()&fwMapData::CONTENTFLAG_MLO) )					// Skip interiors
				{
					// So far so good, process script controlled if we want, otherwise not.
					if( !m_pExportInfo->isScriptControlled && !pDef->GetIsScriptManaged() ) 
					{
						// It's not script controlled.
						doProcess = true;
					}
					else if( m_pExportInfo->isScriptControlled && pDef->GetIsScriptManaged() )
					{
						// It is script controlled.
						// Check the name of the imap is one we want
						// Should possibly do a case insensitive compare, this works for now (for prologue)
						if( strstr(pDef->m_name.GetCStr(), m_pExportInfo->pGroupName) != 0)
						{
							// START Narsty bodge to exclude prologue imaps with _grv in the name (B*1834194)
							if( strstr(pDef->m_name.GetCStr(), "_grv") == 0 )
							// END Narsty bodge to exclude prologue imaps with _grv in the name
							{
								doProcess = true;
							}						
						}
					}
				}
			}

			if ( doProcess )
			{
				const spdAABB &theStreamingBox = fwMapDataStore::GetStore().GetStreamingBounds(strLocalIndex(m_MapSectionIDX));

				Displayf("Processing IMAP file: %.5d - %s", m_MapSectionIDX, pDef->m_name.GetCStr());
				Vector3 min,max;
				min = VEC3V_TO_VECTOR3(theStreamingBox.GetMin());
				max = VEC3V_TO_VECTOR3(theStreamingBox.GetMax());
				Displayf(" Streaming Bounds Min(%.2f,%.2f,%.2f), Max(%.2f,%.2f,%.2f)", min.x, min.y, min.z, max.x, max.y, max.z);

				const spdAABB &thePhysicalBox = fwMapDataStore::GetStore().GetPhysicalBounds(strLocalIndex(m_MapSectionIDX));

//#define		TEST_POINT_CHECK

#ifdef	TEST_POINT_CHECK
				// Only Include imaps that intersect out testpoint
				Vector3 testpos(100.0436f,469.3799f,146.2201f);
				if( theStreamingBox.ContainsPoint( VECTOR3_TO_VEC3V(testpos) ))
#endif
				{
					min = VEC3V_TO_VECTOR3(thePhysicalBox.GetMin());
					max = VEC3V_TO_VECTOR3(thePhysicalBox.GetMax());
					Displayf(" Phsyical  Bounds Min(%.2f,%.2f,%.2f), Max(%.2f,%.2f,%.2f)", min.x, min.y, min.z, max.x, max.y, max.z);

					// issue IMAP streaming request, block until it is in memory
					if (!pDef->IsLoaded())
					{
						CStreaming::SetIsPlayerPositioned(true);
						mapStore.StreamingRequest(strLocalIndex(m_MapSectionIDX),STRFLAG_FORCE_LOAD|STRFLAG_PRIORITY_LOAD);
						strStreamingEngine::GetLoader().LoadAllRequestedObjects();
					}

					if( Verifyf(pDef->IsLoaded(), "%s failed to load", pDef->m_name.GetCStr()) )
					{
						// obtain entity list
						fwEntity** entities = pDef->GetEntities();
						u32 numEntities = pDef->GetNumEntities();

						// run over entities for this IMAP file
						for (u32 j=0; j<numEntities; j++)
						{
							CEntity* pEntity = (CEntity*) entities[j];
							if (pEntity)
							{
								// need to request the assets for this entity, wait until they are loaded, and then
								// create the drawable.
								CBaseModelInfo* pModelInfo = pEntity->GetBaseModelInfo();

								if (pModelInfo && pModelInfo->GetDrawableType()!=fwArchetype::DT_ASSETLESS)
								{
									CModelInfo::RequestAssets(pEntity->GetModelId(), 0);
									strStreamingEngine::GetLoader().LoadAllRequestedObjects();

									if (Verifyf(pModelInfo->GetHasLoaded(), "Entity %s really should be loaded by now", pEntity->GetModelName()))
									{

										bool physInit = false;
										{
											pEntity->InitPhys();
											physInit = true;
										}

										pEntity->CreateDrawable();

										// Extract lights from the archetype and the drawable (both are hidden behind GetNum2dEffects()
										for(int k=0;k<pModelInfo->GetNum2dEffects();k++)
										{
											C2dEffect *pEffect = pModelInfo->Get2dEffect(k);
											if(pEffect->GetType() == ET_LIGHT )
											{
												// We have a light! :)

												CLightEntity* pLightEntity = rage_new CLightEntity( ENTITY_OWNEDBY_OTHER );
												//pLightEntity->SetLightGroupId(pEntity->GetLightGroupId());	// GONE
												pLightEntity->SetParent(pEntity);
												pLightEntity->Set2dEffectIdAndType(k, (e2dEffectType)pEffect->GetType());
												pLightEntity->SetupLight(pEntity);

												m_pLightDataCollector->CollectLight(pEntity, pLightEntity, m_MapSectionIDX, j);

												delete pLightEntity;
											}
										}

										pEntity->DeleteDrawable();

										// Cleanup, if we were setup
										if ( physInit )
										{
											pEntity->DeleteInst();
										}
										
										CModelInfo::RemoveAssets(pEntity->GetModelId());

										DRAWLISTMGR->ClothCleanup();
										DRAWLISTMGR->CharClothCleanup();
									}
								}
							}
						}
						// Remove the DONTDELETE flag so mapsections can be successfully removed.
						mapStore.ClearRequiredFlag(m_MapSectionIDX, STRFLAG_DONTDELETE);
						// finally, unload the IMAP
						mapStore.SafeRemove(strLocalIndex(m_MapSectionIDX));
					}
				}
			}
			m_MapSectionIDX++;
		}

		// Prevent drawables being created.
		CNavMeshDataExporter::SetExportMode(CNavMeshDataExporter::eMapExport);

#endif	// DEBUG

	}

	return false;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
// Support funcs for CLightExporterInterfaceTool()
///////////////////////////////////////////////////////////////////////////////////////////////////

// HACK: Make sure we don't already have physics added for this entity.
// TODO: Get to the bottom of why this is occurring
// JAY, not sure if this is required anymore
bool CLightsExporterTool::IsAlreadyInPhysicsLevel(CPhysical * UNUSED_PARAM(pPhysical), phArchetype * UNUSED_PARAM(pArchetype))
{

/*
	Vector3 vEntityPos = VEC3V_TO_VECTOR3(pPhysical->GetTransform().GetPosition());
	const int iMaxObjs = CPhysics::GetLevel()->GetMaxObjects();
	for(int l=0; l<iMaxObjs; l++)
	{
		if(CPhysics::GetLevel()->GetState(l)!=phLevelNew::OBJECTSTATE_NONEXISTENT)
		{
			phInst * pInstance = CPhysics::GetLevel()->GetInstance(l);
			CEntity * pUserData = (CEntity*)pInstance->GetUserData();
			if(pUserData==pPhysical)
				return true;
			if(pArchetype)
			{
				Vector3 vInstPos( pInstance->GetPosition().GetXf(), pInstance->GetPosition().GetYf(), pInstance->GetPosition().GetZf() );
				if(pInstance->GetArchetype()==pArchetype && vEntityPos.IsClose(vInstPos, 0.01f))
					return true;
			}
		}
	}
*/

	return false;
}

// JAY - Not sure is required anymore
bool CLightsExporterTool::EntityIntersectsCurrentNavMesh(CEntity * UNUSED_PARAM(pEntity))
{
	return false;
}

// JAY - Not sure is required anymore
bool CLightsExporterTool::ExtentsIntersectCurrentNavMesh(Vec3V_In UNUSED_PARAM(vMin), Vec3V_In UNUSED_PARAM(vMax))
{
	return false;
}


// JAY - Not sure is required anymore
fwPtrList &CLightsExporterTool::BuildActiveBuildingsList()
{
	m_ActiveBuildingsList.Flush();

	//TODO: fill this in.

	return m_ActiveBuildingsList;
}

// JAY - Not sure is required anymore
void CLightsExporterTool::AddSearches(atArray<fwBoxStreamerSearch>& UNUSED_PARAM(searchList))
{
/*
	const u32 iStreamTypes = fwBoxStreamerAsset::FLAG_STATICBOUNDS_MOVER | fwBoxStreamerAsset::FLAG_STATICBOUNDS_WEAPONS | fwBoxStreamerAsset::FLAG_MAPDATA;
	fwBoxStreamerSearch customSearch(	spdAABB(VECTOR3_TO_VEC3V(m_vCurrentSectorMins), VECTOR3_TO_VEC3V(m_vCurrentSectorMaxs)),
										fwBoxStreamerSearch::TYPE_NAVMESHEXPORT,
										iStreamTypes	);
	searchList.Grow() = customSearch;
*/
}

///////////////////////////////////////////////////////////////////////////////////////////////////

