<?xml version="1.0"?> 
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
>

<structdef type="CCompleteLODLight" >
	<array name="m_position" type="member" size="3">
		<float />
	</array>
	<array name="m_colour" type="member" size="3">
		<u8 />
	</array>
	<float name="m_intensity"/>
	<array name="m_direction" type="member" size="3">
		<float />
	</array>
	<float name="m_falloff"/>
	<float name="m_falloffExponent"/>
	<float name="m_coneInnerAngle"/>
	<float name="m_coneOuterAngle"/>
	<float name="m_capsuleExtent"/>
	<float name="m_coronaIntensity"/>
	<u32 name="m_timeAndStateFlags"/>
	<u32 name="m_hash"/>
  <u32 name="m_LightAttrFlags"/>
  <u8	name="m_lightType"/>
	<struct name = "m_PhysicalExtents" type = "spdAABB"/>
	<string name = "m_FromEntityName" type="atString" />
</structdef>

<structdef type="CCollectedLODLightData" autoregister="true">
	<array name="m_AllLightData" type="atArray">
	 <struct type="CCompleteLODLight"/>
	</array>
</structdef>
							
</ParserSchema>