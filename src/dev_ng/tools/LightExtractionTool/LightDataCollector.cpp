#include <math.h>

#include "parser/manager.h"
#include "renderer/GtaDrawable.h"
#include "renderer/lights/lights.h"
#include "renderer/lights/LightEntity.h"
#include "scene/entity.h"

#include "ai/navmesh/navmeshextents.h"	// For world size

#include "fwscene/mapdata/mapdata.h"

#include "LightDataCollector.h"
#include "LightDataCollector_parser.h"

#include "exporter.h"

#define EXPORT_PATH "X:/gta5/exported_LODLights"

PARAM(GridSizeX, "Grid Size X");
PARAM(GridSizeY, "Grid Size Y");

//PARAM(LODLightRange, "The max range of a LODLight");
//PARAM(DistLightRange, "The max range of a DistantLight");


////////////////////////////////////////////////////////////////////////////////////////////////////

RAGE_DEFINE_CHANNEL(lightextraction)

////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////
// Stuff for trapping duplicate hashes and finding duplicate lights
////////////////////////////////////////////////////////////////////////////////////////////////////

CLightDataCollector::ENTITY_LIGHT_AND_HASH_INFO::ENTITY_LIGHT_AND_HASH_INFO() 
{ 
	hash = 0; 
	entPos.Zero();
	bbMin.Zero();
	bbMax.Zero();
	lightID = -1;
	entityName[0] = 0;
	mapSectionIDX = -1;
}

CLightDataCollector::ENTITY_LIGHT_AND_HASH_INFO::ENTITY_LIGHT_AND_HASH_INFO(CLightEntity *pLightEntity, int aMapSectionIDX, u32 aEntityIDX)
{
	CEntity *pEntity = pLightEntity->GetParent();

	hash = CLODLightManager::GenerateLODLightHash(pLightEntity);
	entPos = VEC3V_TO_VECTOR3(pEntity->GetTransform().GetPosition());

	spdAABB boundBox;
	pEntity->GetAABB(boundBox);
	bbMin = VEC3V_TO_VECTOR3(boundBox.GetMin());
	bbMax = VEC3V_TO_VECTOR3(boundBox.GetMax());
	lightID = pLightEntity->Get2dEffectId();
	mapSectionIDX = aMapSectionIDX;
	entityIDX = aEntityIDX;
	strcpy( entityName, pEntity->GetModelName());
}

bool  CLightDataCollector::ENTITY_LIGHT_AND_HASH_INFO::operator == (const ENTITY_LIGHT_AND_HASH_INFO& other) const
{
	if( entPos == other.entPos &&
		bbMin == other.bbMin &&
		bbMax == other.bbMax &&
		lightID == other.lightID &&
		(strcmp(entityName, other.entityName)== 0) )
	{
		return true;
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

CLightDataCollector::CLightDataCollector(char *pName)
{
	char	buffer[256];
	m_pOutputName = pName;

	// Create the LOG folder for this collector
	sprintf(buffer, "%s\\%s",EXPORT_PATH, m_pOutputName);
	CreateDirectory(buffer, NULL);

	m_CollectedLightData.m_AllLightData.Reset();
	
	sprintf(buffer, "%s/AALightsLOG.log", m_pOutputName);
	pAALightsLOG = rage_new CSimpleLog(buffer);
	sprintf(buffer, "%s/AADupesLOG.log", m_pOutputName);
	pAADupesLOG = rage_new CSimpleLog(buffer);
	sprintf(buffer, "%s/AAHashClashLOG.log", m_pOutputName);
	pAAHashClashLOG = rage_new CSimpleLog(buffer);

	//m_LODLightRange = 300.0f;
	const float expansion = 200.0f;

	m_SmallLODLightRange = expansion + 250.0f;
	m_MediumLODLightRange = expansion + 750.0f;
	m_LargeLODLightRange = expansion + 2500.0f;

	m_SmallDistLightRange = expansion + 250.0f;
	m_MediumDistLightRange = 3000.0f;	// Already way over
	m_LargeDistLightRange = expansion + 2500.0f;
}

CLightDataCollector::~CLightDataCollector()
{
	delete pAALightsLOG;
	delete pAADupesLOG;
	delete pAAHashClashLOG;
}

// pEntity is used to check for dupes of entity data for anything we might want to use for it's unique hash
void CLightDataCollector::CollectLight(CEntity *pEntity, CLightEntity *pLightEntity, int mapSectionIDX, u32 entityIDX)
{

	// Code to find a specific object by name and position
	/*
	{
		if(strcmp(pEntity->GetModelName(),"prop_vend_soda_01") == 0 )
		{
			Vector3 testpos(-483.81f,1104.29f,320.05f);
			Vector3	propPos = VEC3V_TO_VECTOR3(pEntity->GetTransform().GetPosition());
			Displayf("Prop pos:- (%f, %f, %f)", propPos.x, propPos.y, propPos.z);
			float	dist = (propPos - testpos).Mag();
			Displayf("Prop Dist:- (%f)", dist);
			if( dist < 2.0f )
			{
				Displayf("Got it!");
			}
		}
	}
	*/

	// B*951557 - We need to not generate lod lights for greater than the object cap
	//	if( pEntity->GetDebugPriority() > fwMapData::GetEntityPriorityCap() && !pEntity->GetDebugIsFixed() )
	if( !pEntity->GetDebugIsFixed() && pEntity->GetDebugPriority() > fwEntityDef::PRI_REQUIRED  )	// Don't call fwMapData::GetEntityPriorityCap(), it's updated as the gameloop runs. Only want PRI_REQUIRED now
	{
		return;
	}

	// Create some storage info about this light
	ENTITY_LIGHT_AND_HASH_INFO	aEntityLightHashInfo(pLightEntity, mapSectionIDX, entityIDX);

	// B*1356504 - There was a double light on the bouy prop
	// buoy sways, so light hash changes and thus don't match.. remove!
	if(strstr(aEntityLightHashInfo.entityName,"prop_dock_bouy") != NULL )
	{
		return;
	}
	
	// Check if this is a dupe of another entity
	for(int i=0;i<m_EntityLightAndHash.size();i++)
	{
		if(m_EntityLightAndHash[i] == aEntityLightHashInfo)
		{
			fwMapDataStore& mapStore = fwMapDataStore::GetStore();

			pAADupesLOG->Add("Entity %s at coords (%.3f,%.3f,%.3f), in mapsection %s appears to be a duplicate of entity %s at coords (%.3f,%.3f,%.3f) in mapsection %s... SKIPPING\n",
				aEntityLightHashInfo.entityName,
				aEntityLightHashInfo.entPos.x, aEntityLightHashInfo.entPos.y, aEntityLightHashInfo.entPos.z,
				mapStore.GetSlot(strLocalIndex(aEntityLightHashInfo.mapSectionIDX))->m_name.GetCStr(),
				m_EntityLightAndHash[i].entityName,
				m_EntityLightAndHash[i].entPos.x, m_EntityLightAndHash[i].entPos.y, m_EntityLightAndHash[i].entPos.z,
				mapStore.GetSlot(strLocalIndex(m_EntityLightAndHash[i].mapSectionIDX))->m_name.GetCStr() );

			return;
		}
	}

	// Check if the hash is unique
	ENTITY_LIGHT_AND_HASH_INFO *pOtherLightDataAndHash = CheckIsUniqueHashID(aEntityLightHashInfo);
	if(pOtherLightDataAndHash)
	{
		fwMapDataStore& mapStore = fwMapDataStore::GetStore();

		pAAHashClashLOG->Add("Entity %s at coords (%.3f,%.3f,%.3f), in mapsection %s appears to be have an identical hashID to entity %s at coords (%.3f,%.3f,%.3f) in mapsection %s... SKIPPING\n",
			aEntityLightHashInfo.entityName,
			aEntityLightHashInfo.entPos.x, aEntityLightHashInfo.entPos.y, aEntityLightHashInfo.entPos.z,
			mapStore.GetSlot(strLocalIndex(aEntityLightHashInfo.mapSectionIDX))->m_name.GetCStr(),
			pOtherLightDataAndHash->entityName,
			pOtherLightDataAndHash->entPos.x, pOtherLightDataAndHash->entPos.y, pOtherLightDataAndHash->entPos.z,
			mapStore.GetSlot(strLocalIndex(pOtherLightDataAndHash->mapSectionIDX))->m_name.GetCStr() );

		//LightExtractionFatalAssertf(0, "Hash Clash - Cannot Continue");

		return;	// For consistency
	}

	CLightAttr *pLightAttr = pLightEntity->GetLight();
	// B*1786337 - Can we please exclude lights with a a light fade distance from being LOD light
	if( pLightAttr->m_lightFadeDistance > 0 )
	{
		return;
	}

	// Not a dupe, and the hash doesn't clash.
	// Store this Data and Hash
	m_EntityLightAndHash.PushAndGrow(aEntityLightHashInfo);

	//	Store this light
	CCompleteLODLight	thisLightData;

	// DistantLODLight Members
	// POSITION
	Vector3	position;
	pLightEntity->GetWorldPosition(position);
	thisLightData.m_position[0] = position.x;
	thisLightData.m_position[1] = position.y;
	thisLightData.m_position[2] = position.z;

	// COLOUR
	thisLightData.m_colour[0] = pLightAttr->m_colour.red;
	thisLightData.m_colour[1] = pLightAttr->m_colour.green;
	thisLightData.m_colour[2] = pLightAttr->m_colour.blue;

	// INTENSITY (in the range 0->40)
	thisLightData.m_intensity = pLightAttr->m_intensity;

	// LODLight members
	// LIGHT TYPE
	thisLightData.m_lightType = pLightAttr->m_lightType;		// See eLightType (LightCommon.h)

	// DIRECTION
	Matrix34 mat;
	pLightEntity->GetWorldMatrix(mat);	
	Vector3 vLightDir;
	vLightDir.x = pLightAttr->m_direction.x;
	vLightDir.y = pLightAttr->m_direction.y;
	vLightDir.z = pLightAttr->m_direction.z;
	mat.Transform3x3(vLightDir);
	thisLightData.m_direction[0] = vLightDir.x;
	thisLightData.m_direction[1] = vLightDir.y;
	thisLightData.m_direction[2] = vLightDir.z;

	// FALLOFF
	thisLightData.m_falloff = pLightAttr->m_falloff;

	// FALLOFF EXPONENT
	thisLightData.m_falloffExponent = pLightAttr->m_falloffExponent;

	// CONE ANGLES
	thisLightData.m_coneInnerAngle = pLightAttr->m_coneInnerAngle;
	thisLightData.m_coneOuterAngle = pLightAttr->m_coneOuterAngle;

	// CAPSULE EXTENT
	Vector3	extents = pLightAttr->m_extents.GetVector3();

	// Piotr informs me that this is now just the X component.
	//float	length = extents.Mag();
	thisLightData.m_capsuleExtent = extents.x;	//length;

	// B*2043802 - Jay can we change it so that if we compress an extent and get a 0 extent on a capsule we just convert to an omni?
	if( thisLightData.m_lightType == LIGHT_TYPE_CAPSULE )
	{
		float minVal = (float)MAX_LODLIGHT_CAPSULE_EXTENT / 255.0f;
		if( thisLightData.m_capsuleExtent < minVal )
		{
			thisLightData.m_lightType = LIGHT_TYPE_POINT;
		}
	}

	// CORONA INTENSITY
	if( pLightAttr->m_coronaSize < 0.05f)
	{
		thisLightData.m_coronaIntensity = 0.0f;
	}
	else
	{
		thisLightData.m_coronaIntensity = pLightAttr->m_coronaIntensity;
	}

	// TIME AND STATE
	thisLightData.m_timeAndStateFlags = 0;
	packedTimeAndStateFlags &flags = (packedTimeAndStateFlags&)(thisLightData.m_timeAndStateFlags);
	flags.timeFlags = pLightAttr->m_timeFlags;
	flags.lightType = pLightAttr->m_lightType;

	// See if we can identify a street light
	if(strstr(aEntityLightHashInfo.entityName,"streetlight") != NULL )
	{
		flags.bIsStreetLight = true;
	}
	else if(strstr(aEntityLightHashInfo.entityName,"street_light") != NULL )
	{
		flags.bIsStreetLight = true;
	}

	// Store the flags for use in categorisation
	thisLightData.m_LightAttrFlags = pLightAttr->m_flags;	// See LightSource.h

	// Process what we can now into the flags field in the LODLight
	if(pLightAttr->m_flags & (LIGHTFLAG_CORONA_ONLY|LIGHTFLAG_CORONA_ONLY_LOD_LIGHT))
	{
		flags.bIsCoronaOnly = true;
	}

	if(pLightAttr->m_flags & LIGHTFLAG_DONT_USE_IN_CUTSCENE)
	{
		flags.bDontUseInCutscene = true;
	}

	// STORE THE HASH
	thisLightData.m_hash = aEntityLightHashInfo.hash;

	/*
	// Trap a specific light by hash
	if( thisLightData.m_hash == 3885144215)
	{
		Displayf("Found it!\n");
	}
	*/

	// Add in the bounding box for this light
	const Vector3 &BBmin = pLightEntity->GetBoundingBoxMin();
	const Vector3 &BBmax = pLightEntity->GetBoundingBoxMax();
	thisLightData.m_PhysicalExtents.Set( VECTOR3_TO_VEC3V(BBmin), VECTOR3_TO_VEC3V(BBmax) );

	// Find lights with AABB's with zero Width/Height
	{
		Vector3	vMin = thisLightData.m_PhysicalExtents.GetMinVector3();
		Vector3	vMax = thisLightData.m_PhysicalExtents.GetMaxVector3();

		float w,h;
		w = vMax.x - vMin.x;
		h = vMax.y - vMin.y;

/*
		if(w<=0.0f || h<=0.0f)
		{
			char logFileEntry[1024];
			sprintf(logFileEntry, "ZERO WIDTH//HEIGHT AABB Entity:%s LightID %d - Pos (%.7f,%.7f,%.7f) \n", aEntityLightHashInfo.entityName, pLightEntity->m_2dEffectId, aEntityLightHashInfo.entPos.x, aEntityLightHashInfo.entPos.y, aEntityLightHashInfo.entPos.z );
			WriteLogFileEntry(logFileEntry);
		}
*/

	}



	// Store the streaming extents for this light
	//fwMapDataStore& mapStore = fwMapDataStore::GetStore();
	//thisLightData.m_StreamingExtents = mapStore.GetSlot(aEntityLightHashInfo.mapSectionIDX)->GetStreamingBounds();

	// Store the name of the model the light came from
	thisLightData.m_FromEntityName = pEntity->GetModelName();

	// Store the data
	m_CollectedLightData.m_AllLightData.PushAndGrow(thisLightData);

	// Save to log file
	// Write out entity name, boundbox, light ID and hash
	pAALightsLOG->Add("Entity:%s LightID %d - Pos (%.7f,%.7f,%.7f) \n", aEntityLightHashInfo.entityName, pLightEntity->Get2dEffectId(), aEntityLightHashInfo.entPos.x, aEntityLightHashInfo.entPos.y, aEntityLightHashInfo.entPos.z );
	pAALightsLOG->Add("bbMin = (%.7f,%.7f,%.7f) bbMax = (%.7f,%.7f,%.7f) hash = 0x%x\n", aEntityLightHashInfo.bbMin.x, aEntityLightHashInfo.bbMin.y, aEntityLightHashInfo.bbMin.z, aEntityLightHashInfo.bbMax.x, aEntityLightHashInfo.bbMax.y, aEntityLightHashInfo.bbMax.z, aEntityLightHashInfo.hash  );

	/*
	// Calc the hash array (same is the hash code in LODLightManager)
	u32	hashArray[7];
	spdAABB boundBox;
	const CEntity *pSourceEntity = pLightEntity->GetParent();
	s32 lightID = pLightEntity->m_2dEffectId;
	pSourceEntity->GetAABB(boundBox);
	Vector3 bbMin = VEC3V_TO_VECTOR3(Abs(boundBox.GetMin())); 
	Vector3 bbMax = VEC3V_TO_VECTOR3(Abs(boundBox.GetMax()));
	bbMin *= 100.0f;
	bbMax *= 100.0f;
	hashArray[0] = static_cast<u32>(bbMin.x);
	hashArray[1] = static_cast<u32>(bbMin.y);
	hashArray[2] = static_cast<u32>(bbMin.z);
	hashArray[3] = static_cast<u32>(bbMax.x);
	hashArray[4] = static_cast<u32>(bbMax.y);
	hashArray[5] = static_cast<u32>(bbMax.z);
	hashArray[6] = static_cast<u32>(lightID);
	sprintf(logFileEntry, "HashArray Entries:- 0x%.8x 0x%.8x 0x%.8x 0x%.8x 0x%.8x 0x%.8x 0x%.8x\n\n", hashArray[0], hashArray[1], hashArray[2], hashArray[3], hashArray[4], hashArray[5], hashArray[6] );
	WriteLogFileEntry(logFileEntry);
	*/

}

CLightDataCollector::ENTITY_LIGHT_AND_HASH_INFO *CLightDataCollector::CheckIsUniqueHashID( ENTITY_LIGHT_AND_HASH_INFO &aEntityLightHashInfo)
{
	s32	otherIDX = -1;
	for(int i=0;i<m_EntityLightAndHash.size();i++)
	{
		if(m_EntityLightAndHash[i].hash == aEntityLightHashInfo.hash)
		{
			otherIDX = i;
			break;
		}
	}
	ENTITY_LIGHT_AND_HASH_INFO	*pOtherLightHash = NULL;
	if(otherIDX != -1)
	{
		// Get the data for it
		pOtherLightHash = &m_EntityLightAndHash[otherIDX];

	}
	return pOtherLightHash;
}

void CLightDataCollector::LoadAllLights()
{
	char	buffer[256];
	sprintf(buffer, "%s/%s/AllLODLights", EXPORT_PATH, m_pOutputName);
	INIT_PARSER;
	PARSER.LoadObject(buffer, "xml", m_CollectedLightData);
}

void CLightDataCollector::SaveAllLights()
{
	char	buffer[256];
	sprintf(buffer, "%s/%s/AllLODLights", EXPORT_PATH, m_pOutputName);
	INIT_PARSER;
	PARSER.SaveObject(buffer, "xml", &m_CollectedLightData, parManager::XML);
}

void CLightDataCollector::PostProcessCollectedLights()
{
	for(int i=0;i<m_CollectedLightData.m_AllLightData.size();i++)
	{
		CCompleteLODLight &thisLightData = m_CollectedLightData.m_AllLightData[i];
		PackLightData(thisLightData);
	}

	// Checks the packed data isn't crap
	CheckPackedLightData();
}


void CLightDataCollector::PackLightData(CCompleteLODLight &thisLODLight)
{
	// Cone Inner Angle
	thisLODLight.m_packed_coneInnerAngle = LL_PACK_U8(thisLODLight.m_coneInnerAngle, MAX_LODLIGHT_CONE_ANGLE);

	// Cone Outer Angle
	thisLODLight.m_packed_coneOuterAngle = LL_PACK_U8(thisLODLight.m_coneOuterAngle, MAX_LODLIGHT_CONE_ANGLE);

	// Intensity
	thisLODLight.m_packed_intensity = LL_PACK_U8(thisLODLight.m_intensity, MAX_LODLIGHT_INTENSITY);

	// Capsule Extent
	thisLODLight.m_packed_capsuleExtent = LL_PACK_U8(thisLODLight.m_capsuleExtent, MAX_LODLIGHT_CAPSULE_EXTENT);

	// Corona Intensity
	thisLODLight.m_packed_coronaIntensity = LL_PACK_U8(thisLODLight.m_coronaIntensity, MAX_LODLIGHT_CORONA_INTENSITY);
}


// Returns the category ID.
int	CLightDataCollector::GetLightCategory(CCompleteLODLight &thisLODLight)
{
	return Lights::CalculateLightCategory(thisLODLight.m_lightType, thisLODLight.m_falloff, thisLODLight.m_intensity, thisLODLight.m_capsuleExtent, thisLODLight.m_LightAttrFlags );


	/*	// NOW SUPERCEDED
		Categories are:-
		1. Small lights
			- Lights smaller than 10m OR intensity less than 1
			- Want to draw these for 100m, so the streaming extents need to be set accordingly

		2. Street lights
			- Lights between 10m and 30m, intensity greater than 1
			- Specific models for street lights
			- Want to draw these for 450m, so the streaming extents need to be set accordingly

		3. Important lights
			- A small number of important lights such as the uplighters on skyscrapers
			- Lights above 30m,. intensity greater than 1
			- May need to just select specific model names or have them tagged by art
			- Want to draw these for 1000m, so the streaming extents need to be set accordingly.
	*/
	/*
	// Start with important and work down

	// IMPORTANT/LARGE
	//----------------

	float length = thisLODLight.GetLength();

	// Now check for height and intensity and see if those fall into this category
	if( length >= 50.0f && thisLODLight.m_intensity >= 1.0f )
	{
		return LIGHT_CATEGORY_LARGE;
	}

	// STREET/MEDIUM
	//--------------
	packedTimeAndStateFlags &flags = (packedTimeAndStateFlags&)(thisLODLight.m_timeAndStateFlags);
	if(flags.bIsStreetLight)
	{
		return LIGHT_CATEGORY_MEDIUM;
	}
	if( length >= 10.0f && thisLODLight.m_intensity >= 1.0f  )
	{
		return LIGHT_CATEGORY_MEDIUM;
	}

	// SMALL - everything else
	//--------
	return LIGHT_CATEGORY_SMALL;
*/

}

void CLightDataCollector::CheckPackedLightData()
{
	float maxConeAngle = FLT_MIN;
	float minConeAngle = FLT_MAX;
	float maxCoronaIntensity  = FLT_MIN;
	float minCoronaIntensity = FLT_MAX;
	float maxCapsuleExtent = FLT_MIN;
	float minCapsuleExtent = FLT_MAX;

	// Calculate min and max's, just so I know what they are likely to be.
	for(int i=0;i<m_CollectedLightData.m_AllLightData.size();i++)
	{
		CCompleteLODLight &thisLightData = m_CollectedLightData.m_AllLightData[i];

		if( thisLightData.m_capsuleExtent > MAX_LODLIGHT_CAPSULE_EXTENT)
		{
			Displayf("ERROR: Potential Extent Error in light from entity: %s, capsule extent == %f", thisLightData.m_FromEntityName.c_str(), thisLightData.m_capsuleExtent);
			thisLightData.m_capsuleExtent = MAX_LODLIGHT_CAPSULE_EXTENT - 1.0f;
		}

		maxConeAngle = Max(maxConeAngle, thisLightData.m_coneInnerAngle);
		maxConeAngle = Max(maxConeAngle, thisLightData.m_coneOuterAngle);
		minConeAngle = Min(minConeAngle, thisLightData.m_coneInnerAngle);
		minConeAngle = Min(minConeAngle, thisLightData.m_coneOuterAngle);
		maxCoronaIntensity = Max(maxCoronaIntensity, thisLightData.m_coronaIntensity);
		minCoronaIntensity = Min(minCoronaIntensity, thisLightData.m_coronaIntensity);
		maxCapsuleExtent = Max(maxCapsuleExtent, thisLightData.m_capsuleExtent);
		minCapsuleExtent = Min(minCapsuleExtent, thisLightData.m_capsuleExtent);
	}

	float maxIntensityDeviation = 0.0f;
	float maxConeAngleDeviation = 0.0f;
	float maxCoronaIntensityDeviation = 0.0f;
	float maxCapsuleExtentDeviation = 0.0f;

	for(int i=0;i<m_CollectedLightData.m_AllLightData.size();i++)
	{
		float	unpackedData;
		float	thisDeviation;
		CCompleteLODLight &thisLightData = m_CollectedLightData.m_AllLightData[i];

		// Cone Inner Angle
		unpackedData = LL_UNPACK_U8( thisLightData.m_packed_coneInnerAngle, MAX_LODLIGHT_CONE_ANGLE );
		thisDeviation = thisLightData.m_coneInnerAngle - unpackedData;
		Assertf( Abs(thisDeviation) < 1.0f, "Cone Inner Angle Deviation is too large");
		maxConeAngleDeviation = Max( maxConeAngleDeviation, Abs( thisDeviation ) );

		// Cone Outer Angle
		unpackedData = LL_UNPACK_U8( thisLightData.m_packed_coneOuterAngle, MAX_LODLIGHT_CONE_ANGLE );
		thisDeviation = thisLightData.m_coneOuterAngle - unpackedData;
		Assertf( Abs(thisDeviation) < 1.0f, "Cone Outer Angle Deviation is too large");
		maxConeAngleDeviation = Max( maxConeAngleDeviation, Abs( thisDeviation ) );

		// Intensity
		unpackedData = LL_UNPACK_U8( thisLightData.m_packed_intensity, MAX_LODLIGHT_INTENSITY );
		thisDeviation = thisLightData.m_intensity - unpackedData;
		Assertf( Abs(thisDeviation) < 1.0f, "Intensity Deviation is too large");
		maxIntensityDeviation = Max( maxIntensityDeviation, Abs( thisDeviation ) );

		// Capsule Extent
		unpackedData = LL_UNPACK_U8( thisLightData.m_packed_capsuleExtent, MAX_LODLIGHT_CAPSULE_EXTENT );
		thisDeviation = thisLightData.m_capsuleExtent - unpackedData;
		Assertf( Abs(thisDeviation) < 1.0f, "Capsule Extent Deviation is too large");
		maxCapsuleExtentDeviation = Max( maxCapsuleExtentDeviation, Abs( thisDeviation ) );

		// Corona Intensity
		unpackedData = LL_UNPACK_U8( thisLightData.m_packed_coronaIntensity, MAX_LODLIGHT_CORONA_INTENSITY );
		thisDeviation = thisLightData.m_coronaIntensity - unpackedData;
		Assertf( Abs(thisDeviation) < 1.0f, "Corona Intensity Deviation is too large");
		maxCoronaIntensityDeviation = Max( maxCoronaIntensityDeviation, Abs( thisDeviation ) );
	}

	Displayf("Max Intensity Deviation == %f", maxIntensityDeviation);
	Displayf("Max Cone Angle Deviation == %f", maxConeAngleDeviation);
	Displayf("Max Corona Intensity Deviation == %f", maxCoronaIntensityDeviation);
	Displayf("Max Capsule Extent Deviation == %f", maxCapsuleExtentDeviation);
	if(maxCapsuleExtentDeviation > 1.0f)
	{
		Displayf("Min Capsule Extent == %f", minCapsuleExtent );
		Displayf("Max Capsule Extent == %f", maxCapsuleExtent );
	}
}

int CLightDataCollector::SortByHashFunc(const CCompleteLODLight *a, const CCompleteLODLight *b)
{
	if( a->m_hash < b->m_hash )
		return -1;
	else if( a->m_hash > b->m_hash )
		return 1;
	return	0;
}

////////////////////////////////////////////////////////////////////////////////////////////////
//	CHOPPING UP
////////////////////////////////////////////////////////////////////////////////////////////////

CChoppedLightDataGrid CLightDataCollector::m_LODLightSmallGrid;
CChoppedLightDataGrid CLightDataCollector::m_LODLightMediumGrid;
CChoppedLightDataGrid CLightDataCollector::m_LODLightLargeGrid;

// Chops up into a grid
bool CLightDataCollector::ChopUpCollectedLightData(CLightExporterPassInfo *pInfo)
{
	if(!pInfo->isScriptControlled)
	{
		// Normal LODLights
		atArray <CCompleteLODLight>	lodLightsSmall;
		atArray <CCompleteLODLight>	lodLightsMedium;
		atArray <CCompleteLODLight>	lodLightsLarge;

		for (int i=m_CollectedLightData.m_AllLightData.size()-1; i>=0; i--)
		{
			CCompleteLODLight &thisLightData = m_CollectedLightData.m_AllLightData[i];

			switch(GetLightCategory(thisLightData))
			{
			case Lights::LIGHT_CATEGORY_LARGE:
				lodLightsLarge.PushAndGrow(thisLightData);
				break;

			case Lights::LIGHT_CATEGORY_MEDIUM:
				lodLightsMedium.PushAndGrow(thisLightData);
				break;

			case Lights::LIGHT_CATEGORY_SMALL:
				lodLightsSmall.PushAndGrow(thisLightData);
				break;

			default:
				//GetLightCategory(thisLightData)
				Assertf( 0, "ERROR: LODLight doesn't belong to any category!" );
			}

			m_CollectedLightData.m_AllLightData.Delete( i );
		}

		// GONE for now
		//	DumpLightCategoryLogs(lodLightsSmall, lodLightsMedium, lodLightsLarge);

		float	worldSizeX = CPathServerExtents::m_vWorldMax.x - CPathServerExtents::m_vWorldMin.x;
		float	worldSizeY = CPathServerExtents::m_vWorldMax.y - CPathServerExtents::m_vWorldMin.y;

		// Now just places the entirety of the lights in one cell inside the grid
		ChopUpLightData(lodLightsSmall, m_LODLightSmallGrid, Lights::LIGHT_CATEGORY_SMALL, worldSizeX, worldSizeY, m_SmallDistLightRange, m_SmallLODLightRange );
		SubdivideOverpopulatedCells(m_LODLightSmallGrid);
		ConsolitdateSparseCells(m_LODLightSmallGrid);
		GetStatsOnChoppedData(m_LODLightSmallGrid);
		MakeCellsSquareIsh(m_LODLightSmallGrid);
		DumpDimensionsOfGridCells(m_LODLightSmallGrid);
		CreateChoppedDataImaps(m_LODLightSmallGrid, pInfo);

		ChopUpLightData(lodLightsMedium, m_LODLightMediumGrid, Lights::LIGHT_CATEGORY_MEDIUM, worldSizeX, worldSizeY, m_MediumDistLightRange, m_MediumLODLightRange );
		SubdivideOverpopulatedCells(m_LODLightMediumGrid);
		ConsolitdateSparseCells(m_LODLightMediumGrid);
		GetStatsOnChoppedData(m_LODLightMediumGrid);
		MakeCellsSquareIsh(m_LODLightMediumGrid);
		DumpDimensionsOfGridCells(m_LODLightMediumGrid);
		CreateChoppedDataImaps(m_LODLightMediumGrid, pInfo);

		ChopUpLightData(lodLightsLarge, m_LODLightLargeGrid, Lights::LIGHT_CATEGORY_LARGE, worldSizeX, worldSizeY, m_LargeDistLightRange, m_LargeLODLightRange );
		SubdivideOverpopulatedCells(m_LODLightLargeGrid);
		ConsolitdateSparseCells(m_LODLightLargeGrid);
		GetStatsOnChoppedData(m_LODLightLargeGrid);
		MakeCellsSquareIsh(m_LODLightLargeGrid);
		DumpDimensionsOfGridCells(m_LODLightLargeGrid);
		CreateChoppedDataImaps(m_LODLightLargeGrid, pInfo);
	}
	else
	{
		atArray <CCompleteLODLight>	lodLightsMedium;

		// Just one group, medium, for script controlled areas
		float	worldSizeX = CPathServerExtents::m_vWorldMax.x - CPathServerExtents::m_vWorldMin.x;
		float	worldSizeY = CPathServerExtents::m_vWorldMax.y - CPathServerExtents::m_vWorldMin.y;
		ChopUpLightData(m_CollectedLightData.m_AllLightData, m_LODLightMediumGrid, Lights::LIGHT_CATEGORY_MEDIUM, worldSizeX, worldSizeY, m_MediumDistLightRange, m_MediumLODLightRange );
		CreateChoppedDataImaps(m_LODLightMediumGrid, pInfo);
	}
	return true;
}

/*	// RE_THINK
void CLightDataCollector::DumpLightCategoryLog(const char *pCategoryName, atArray <CCompleteLODLight> &info)
{
	CSimpleLog *pThisLog = rage_new CSimpleLog(pCategoryName);

	for(int i=0;i<info.size();i++)
	{
		CCompleteLODLight &thisLODLight = info[i];
		pThisLog->Add("Light from entity %s - type = %s, size = %f\n", thisLODLight.m_FromEntityName.c_str(), thisLODLight.m_lightType == LIGHT_TYPE_CAPSULE?"capsule":"spot/point", thisLODLight.GetLength() );
	}
	delete	pThisLog;
}

void CLightDataCollector::DumpLightCategoryLogs(atArray <CCompleteLODLight> &cat0, atArray <CCompleteLODLight> &cat1, atArray <CCompleteLODLight> &cat2)
{
	DumpLightCategoryLog("AAACategory0.log", cat0);
	DumpLightCategoryLog("AAACategory1.log", cat1);
	DumpLightCategoryLog("AAACategory2.log", cat2);
}
*/


bool CLightDataCollector::ChopUpLightData(atArray <CCompleteLODLight> &LODLightsInCategory, CChoppedLightDataGrid &LODLightCategoryGrid, int categoryID, float gridsizeX, float gridsizeY, float DISTVisRadius, float LODVisRadius)
{
	float	worldSizeX = CPathServerExtents::m_vWorldMax.x - CPathServerExtents::m_vWorldMin.x;
	float	worldSizeY = CPathServerExtents::m_vWorldMax.y - CPathServerExtents::m_vWorldMin.y;

	// Define sector size by passed sizes
	int iNumSectorsX = static_cast<s32>(ceilf(worldSizeX/gridsizeX));
	int iNumSectorsY = static_cast<s32>(ceilf(worldSizeY/gridsizeY));

	// Overide
	//gridsizeX = worldSizeX;
	//gridsizeY = worldSizeY;
	//iNumSectorsX = 1;	
	//iNumSectorsY = 1;


	int iStartSectorX = 0;
	int iStartSectorY = 0;
	int iEndSectorX = iNumSectorsX;
	int iEndSectorY = iNumSectorsY;
	int iCurrentSectorX = iStartSectorX;
	int iCurrentSectorY = iStartSectorY;

	// Set the hreader info in the grid
	LODLightCategoryGrid.gridEntries.Reset();
	LODLightCategoryGrid.gridElementsX = iEndSectorX;
	LODLightCategoryGrid.gridElementsY = iEndSectorY;
	LODLightCategoryGrid.categoryID = categoryID;
	LODLightCategoryGrid.gridsizeX = gridsizeX;
	LODLightCategoryGrid.gridsizeY = gridsizeY;
	LODLightCategoryGrid.DISTVisibilityRadius = DISTVisRadius;
	LODLightCategoryGrid.LODVisibilityRadius = LODVisRadius;

	// Start
	while(	iCurrentSectorY < iEndSectorY  )
	{
		CChoppedLightDataGridEntry	thisGridEntry;

		GetLightsForGridEntry(LODLightsInCategory, iCurrentSectorX, iCurrentSectorY, gridsizeX, gridsizeY, CPathServerExtents::m_vWorldMin.x, CPathServerExtents::m_vWorldMin.y, thisGridEntry);
		if(thisGridEntry.thisGridLights.size())
		{
			LODLightCategoryGrid.gridEntries.PushAndGrow(thisGridEntry);
		}

		// update sector coords
		iCurrentSectorX++;
		if(iCurrentSectorX >= iEndSectorX)
		{
			iCurrentSectorX = 0;
			iCurrentSectorY++;
		}
	}

	RemoveEmptyCells(LODLightCategoryGrid);

	bool success = false;
	if( LODLightsInCategory.size() == 0 )
		success = true;

	return success;
}

void CLightDataCollector::GetLightsForGridEntry(atArray <CCompleteLODLight> &LODLights, int iCurrentSectorX, int iCurrentSectorY, float gridsizeX, float gridsizeY, float worldStartX, float worldStartY, CChoppedLightDataGridEntry &thisGridEntry)
{
	thisGridEntry.startx = worldStartX + (iCurrentSectorX * gridsizeX);
	thisGridEntry.starty = worldStartY + (iCurrentSectorY * gridsizeY);
	thisGridEntry.width = gridsizeX;
	thisGridEntry.height = gridsizeY;

	// Calculate the grid extents
	Vector3	boxMin, boxMax;
	boxMin.x = worldStartX + (iCurrentSectorX * gridsizeX);
	boxMin.y = worldStartY + (iCurrentSectorY * gridsizeY);
	boxMin.z = 0;
	boxMax.x = boxMin.x + gridsizeX;
	boxMax.y = boxMin.y + gridsizeY;
	boxMax.z = 0;

	for (int i=LODLights.size()-1; i>=0; i--)
	{
		CCompleteLODLight &LODLight = LODLights[i];
		Vector3 point( LODLight.m_position[0], LODLight.m_position[1], LODLight.m_position[2] );

		if( point.x >= boxMin.x && point.x < boxMax.x )
		{
			if( point.y >= boxMin.y && point.y < boxMax.y )
			{
				thisGridEntry.thisGridLights.PushAndGrow( LODLight );
				// Remove from the array of all lights
				LODLights.Delete( i );
			}
		}
	}

	UpdateCellExtentsFromLights(thisGridEntry);
}

void	CLightDataCollector::SubdivideOverpopulatedCells(CChoppedLightDataGrid &LODLightCategoryGrid)
{

#define MAX_LIGHTS_PER_CELL		(800)
#define MAX_LIGHTS_PER_CELL_2	(MAX_LIGHTS_PER_CELL)
	
	// Basic plan.
	//	1..	Go through every cell and see if it's light count is higher than X.
	//	2.. Subdivide if it is higher, removing lights from cells as we go and adding new cells onto the end.
	//  3..	Move to the next cell and do it all again

	for(int i=0;i<LODLightCategoryGrid.gridEntries.size();i++)
	{
		CChoppedLightDataGridEntry &thisCell = LODLightCategoryGrid.gridEntries[i];

		// Split across the axis that balances the best
		if(thisCell.thisGridLights.size() > (int)MAX_LIGHTS_PER_CELL_2)
		{

/*
			float lightsInCell = (float)thisCell.thisGridLights.size();
			float horizSplitDelta = (float)GetHorizSplitDifference(thisCell);
			float vertSplitDelta = (float)GetVertSplitDifference(thisCell);

			// Get the horizontal and vertical splits "effectiveness" as a percent of the total number of lights
			float horizSplitPercentDifference = (horizSplitDelta / lightsInCell)*100;
			float vertSplitPercentDifference = (vertSplitDelta / lightsInCell)*100;

			float whRatio = (thisCell.width / thisCell.height);

			horizSplitDelta *= ((whRatio * whRatio));
			vertSplitDelta /= ((whRatio * whRatio));

			// Get W/H ratio
			// (whRatio < 1.50f && whRatio > 0.50f)

			if( fabs( horizSplitPercentDifference - vertSplitPercentDifference ) > 20.0f )
			{
				// Can and should we split across the y axis?
				if( (horizSplitDelta > vertSplitDelta) && thisCell.width > LODLightCategoryGrid.LODVisibilityRadius )
				{
					// Do a vertical split
					DivideCellV(LODLightCategoryGrid, thisCell);
				}
				else if( thisCell.height > LODLightCategoryGrid.LODVisibilityRadius)
				{
					// Do a horizontal split
					DivideCellH(LODLightCategoryGrid, thisCell);
				}

			}
			else
			{
*/

				if( thisCell.width > LODLightCategoryGrid.LODVisibilityRadius )
				{
					DivideCellVH(LODLightCategoryGrid, thisCell);
				}
//			}
		}
	}

	RemoveEmptyCells(LODLightCategoryGrid);

}


int CLightDataCollector::GetHorizSplitDifference(CChoppedLightDataGridEntry &theCell)
{
	int	above = 0;
	int below = 0;

	float	centre = theCell.starty + (theCell.height/2.0f);

	for(int i=0;i<theCell.thisGridLights.size();i++)
	{
		CCompleteLODLight &theLight = theCell.thisGridLights[i];

		if(theLight.m_position[1] >= centre)
		{
			above++;
		}
		else
		{
			below++;
		}
	}
	return abs(above - below);
}

int CLightDataCollector::GetVertSplitDifference(CChoppedLightDataGridEntry &theCell)
{
	int	left = 0;
	int right = 0;
	float	centre = theCell.startx + (theCell.width/2.0f);

	for(int i=0;i<theCell.thisGridLights.size();i++)
	{
		CCompleteLODLight &theLight = theCell.thisGridLights[i];

		if(theLight.m_position[0] >= centre)
		{
			right++;
		}
		else
		{
			left++;
		}
	}
	return abs(right - left);
}

// Split into TOP/BOTTOM
void	CLightDataCollector::DivideCellH(CChoppedLightDataGrid &LODLightCategoryGrid, CChoppedLightDataGridEntry &theCell)
{
	float	newWidth = theCell.width;
	float	newHeight = theCell.height/2.0f;

	// Top
	CChoppedLightDataGridEntry newT;
	newT.width = newWidth;
	newT.height = newHeight;
	newT.startx = theCell.startx;
	newT.starty = theCell.starty;

	// Bottom
	CChoppedLightDataGridEntry newB;
	newB.width = newWidth;
	newB.height = newHeight;
	newB.startx = theCell.startx;
	newB.starty = theCell.starty + newHeight;

	atArray<CChoppedLightDataGridEntry*> newCells;
	newCells.PushAndGrow(&newT);
	newCells.PushAndGrow(&newB);

	MoveLightsContainedInNewCells( theCell, newCells );

	// theCell Should now be empty!
	Assertf( theCell.thisGridLights.size() == 0, "ERROR::Subdivided Cell still has lights!!" );
	if(theCell.thisGridLights.size() != 0)
	{
		Displayf("It's gone wrong!");	// Left in because we run with nopopups, to trap in debugger
	}

	UpdateCellExtentsFromLights(newT);
	UpdateCellExtentsFromLights(newB);

	// Push newly created cells
	LODLightCategoryGrid.gridEntries.PushAndGrow(newT);
	LODLightCategoryGrid.gridEntries.PushAndGrow(newB);
}

// Split into LEFT/RIGHT
void	CLightDataCollector::DivideCellV(CChoppedLightDataGrid &LODLightCategoryGrid, CChoppedLightDataGridEntry &theCell)
{
	float	newWidth = theCell.width/2.0f;
	float	newHeight = theCell.height;

	// Left
	CChoppedLightDataGridEntry newL;
	newL.width = newWidth;
	newL.height = newHeight;
	newL.startx = theCell.startx;
	newL.starty = theCell.starty;

	// Right
	CChoppedLightDataGridEntry newR;
	newR.width = newWidth;
	newR.height = newHeight;
	newR.startx = theCell.startx + newWidth;
	newR.starty = theCell.starty;

	atArray<CChoppedLightDataGridEntry*> newCells;
	newCells.PushAndGrow(&newL);
	newCells.PushAndGrow(&newR);

	MoveLightsContainedInNewCells( theCell, newCells );

	// theCell Should now be empty!
	Assertf( theCell.thisGridLights.size() == 0, "ERROR::Subdivided Cell still has lights!!" );
	if(theCell.thisGridLights.size() != 0)
	{
		Displayf("It's gone wrong!");	// Left in because we run with nopopups, to trap in debugger
	}

	UpdateCellExtentsFromLights(newL);
	UpdateCellExtentsFromLights(newR);

	// Push newly created cells
	LODLightCategoryGrid.gridEntries.PushAndGrow(newL);
	LODLightCategoryGrid.gridEntries.PushAndGrow(newR);
}

// Split into QUARTERS
void	CLightDataCollector::DivideCellVH(CChoppedLightDataGrid &LODLightCategoryGrid, CChoppedLightDataGridEntry &theCell)
{
	float	newWidth = theCell.width/2.0f;
	float	newHeight = theCell.height/2.0f;

	// Chop first then push new cells, because pushing new cells may move the storage for theCell.

	// Top Left
	CChoppedLightDataGridEntry newTL;
	newTL.width = newWidth;
	newTL.height = newHeight;
	newTL.startx = theCell.startx;
	newTL.starty = theCell.starty;

	// Bottom Left
	CChoppedLightDataGridEntry newBL;
	newBL.width = newWidth;
	newBL.height = newHeight;
	newBL.startx = theCell.startx;
	newBL.starty = theCell.starty + newHeight;

	// Top Right
	CChoppedLightDataGridEntry newTR;
	newTR.width = newWidth;
	newTR.height = newHeight;
	newTR.startx = theCell.startx + newWidth;
	newTR.starty = theCell.starty;

	// Bottom Right
	CChoppedLightDataGridEntry newBR;
	newBR.width = newWidth;
	newBR.height = newHeight;
	newBR.startx = theCell.startx + newWidth;
	newBR.starty = theCell.starty + newHeight;

	atArray<CChoppedLightDataGridEntry*> newCells;
	newCells.PushAndGrow(&newTL);
	newCells.PushAndGrow(&newBL);
	newCells.PushAndGrow(&newTR);
	newCells.PushAndGrow(&newBR);

	MoveLightsContainedInNewCells( theCell, newCells );

	// theCell Should now be empty
	Assertf( theCell.thisGridLights.size() == 0, "ERRO::Subdivided Cell still has lights!!" );
	if(theCell.thisGridLights.size() != 0)
	{
		Displayf("It's gone wrong!");
	}


	// Set new extents based on lights inside
	UpdateCellExtentsFromLights(newTL);
	UpdateCellExtentsFromLights(newBL);
	UpdateCellExtentsFromLights(newTR);
	UpdateCellExtentsFromLights(newBL);

	// Push newly created cells
	LODLightCategoryGrid.gridEntries.PushAndGrow(newTL);
	LODLightCategoryGrid.gridEntries.PushAndGrow(newBL);
	LODLightCategoryGrid.gridEntries.PushAndGrow(newTR);
	LODLightCategoryGrid.gridEntries.PushAndGrow(newBR);
}

void	CLightDataCollector::DivideCellVByCount(CChoppedLightDataGrid &LODLightCategoryGrid, CChoppedLightDataGridEntry &theCell, int count)
{
	float newWidth = theCell.width / count;
	float newHeight = theCell.height;

	CChoppedLightDataGridEntry	*pNewEntries = rage_new CChoppedLightDataGridEntry[count];
	atArray<CChoppedLightDataGridEntry*> newCells;
	for(int i=0;i<count;i++)
	{
		pNewEntries[i].startx = theCell.startx + (newWidth*i);
		pNewEntries[i].starty = theCell.starty;
		pNewEntries[i].width = newWidth;
		pNewEntries[i].height = newHeight;
		newCells.PushAndGrow(&pNewEntries[i]);
	}

	MoveLightsContainedInNewCells( theCell, newCells );

	// theCell Should now be empty
	Assertf( theCell.thisGridLights.size() == 0, "ERRO::Subdivided Cell still has lights!!" );
	if(theCell.thisGridLights.size() != 0)
	{
		Displayf("It's gone wrong!");
	}

	for(int i=0;i<count;i++)
	{
		UpdateCellExtentsFromLights(*newCells[i]);
		LODLightCategoryGrid.gridEntries.PushAndGrow(*newCells[i]);
	}

	delete []pNewEntries;
}

void	CLightDataCollector::DivideCellHByCount(CChoppedLightDataGrid &LODLightCategoryGrid, CChoppedLightDataGridEntry &theCell, int count)
{
	float newWidth = theCell.width;
	float newHeight = theCell.height / count;

	CChoppedLightDataGridEntry	*pNewEntries = rage_new CChoppedLightDataGridEntry[count];
	atArray<CChoppedLightDataGridEntry*> newCells;
	for(int i=0;i<count;i++)
	{
		pNewEntries[i].startx = theCell.startx;
		pNewEntries[i].starty = theCell.starty + (newHeight*i);
		pNewEntries[i].width = newWidth;
		pNewEntries[i].height = newHeight;
		newCells.PushAndGrow(&pNewEntries[i]);
	}

	MoveLightsContainedInNewCells( theCell, newCells );

	// theCell Should now be empty
	Assertf( theCell.thisGridLights.size() == 0, "ERRO::Subdivided Cell still has lights!!" );
	if(theCell.thisGridLights.size() != 0)
	{
		Displayf("It's gone wrong!");
	}

	for(int i=0;i<count;i++)
	{
		UpdateCellExtentsFromLights(*newCells[i]);
		LODLightCategoryGrid.gridEntries.PushAndGrow(*newCells[i]);
	}

	delete []pNewEntries;
}

void	CLightDataCollector::MoveLightsContainedInNewCells(CChoppedLightDataGridEntry &oldCell, atArray<CChoppedLightDataGridEntry*> &newCells )
{
	for (int i=oldCell.thisGridLights.size()-1; i>=0; i--)
	{
		CCompleteLODLight &theLight = oldCell.thisGridLights[i];
		Vector3	theLightPos(theLight.m_position[0], theLight.m_position[1], theLight.m_position[2]);

		u32	closestCell = GetClosestCell(theLightPos, newCells);

		newCells[ closestCell ]->thisGridLights.PushAndGrow(theLight);
		oldCell.thisGridLights.Delete(i);


	/*	// Didn't work, error in float subdivision
		if( theLightPos.x >= newCell.startx && theLightPos.x <= (newCell.startx + newCell.width) )
		{
			if( theLightPos.y >= newCell.starty && theLightPos.y <= (newCell.starty + newCell.height) )
			{
				newCell.thisGridLights.PushAndGrow(theLight);
				oldCell.thisGridLights.Delete(i);
			}
		}
	*/

	}
}


u32	CLightDataCollector::GetClosestCell(Vector3 &pos, atArray<CChoppedLightDataGridEntry*> &newCells )
{
int		closestID = -1;
float	closestDist = FLT_MAX;

	for(int i=0;i<newCells.size();i++)
	{
		CChoppedLightDataGridEntry *pThisCell = newCells[i];

		// Calculate the distance to the edges of the closest cell. Smallest wins
		float dist = CalcDistToRectangleSquared(pos.x,pos.y,pThisCell);
		if(dist < closestDist)
		{
			closestDist = dist;
			closestID = i;
		}
	}
	return closestID;
}


float CLightDataCollector::CalcDistToRectangleSquared(float X, float Y, CChoppedLightDataGridEntry *pTheCell)
{
	float	DistX, DistY;
	float	MinX = pTheCell->startx;
	float	MinY = pTheCell->starty;
	float	MaxX = pTheCell->startx + pTheCell->width;
	float	MaxY = pTheCell->starty + pTheCell->height;

	if (X < MinX)
	{
		DistX = X - MinX;
	}
	else if (X > MaxX)
	{
		DistX = X - MaxX;
	}
	else
	{
		DistX = 0.0f;
	}

	if (Y < MinY)
	{
		DistY = Y - MinY;
	}
	else if (Y > MaxY)
	{
		DistY = Y - MaxY;
	}
	else
	{
		DistY = 0.0f;
	}

	return (DistX*DistX + DistY*DistY);
}



void	CLightDataCollector::UpdateCellExtentsFromLights(CChoppedLightDataGridEntry &theCell)
{
	Vector3	vmin(FLT_MAX, FLT_MAX, FLT_MAX);
	Vector3	vmax(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	for(int i=0;i<theCell.thisGridLights.size();i++)
	{
		CCompleteLODLight &theLight = theCell.thisGridLights[i];
		Vector3	pos(theLight.m_position[0], theLight.m_position[1], theLight.m_position[2]);

		if( vmin.x > pos.x )
			vmin.x = pos.x;
		if( vmin.y > pos.y )
			vmin.y = pos.y;

		if( vmax.x < pos.x )
			vmax.x = pos.x;
		if( vmax.y < pos.y )
			vmax.y = pos.y;
	}

	theCell.startx = vmin.x;
	theCell.starty = vmin.y;
	theCell.width = vmax.x - vmin.x;
	theCell.height = vmax.y - vmin.y;
}


void	CLightDataCollector::RemoveEmptyCells(CChoppedLightDataGrid &LODLightCategoryGrid)
{
	for (int i=LODLightCategoryGrid.gridEntries.size()-1; i>=0; i--)
	{
		if( LODLightCategoryGrid.gridEntries[i].thisGridLights.size() == 0 )
		{
			LODLightCategoryGrid.gridEntries.Delete(i);
		}
	}
}


// Basic plan, go through all the cells looking for ones that have counts lower than "x" and add them to the nearest cell.
// Will make some overlap, but shouldn't be an issue. Does not screate new cells
void	CLightDataCollector::ConsolitdateSparseCells(CChoppedLightDataGrid &LODLightCategoryGrid)
{
#define MIN_LIGHT_COUNT_TO_TRY_CONSOLIDATE (100)
	
	int	nCells = LODLightCategoryGrid.gridEntries.size();
	for( int i=0;i<nCells;i++ )
	{
		CChoppedLightDataGridEntry &thisCell = LODLightCategoryGrid.gridEntries[i];

		int nLights = thisCell.thisGridLights.size();

		if(nLights && nLights < MIN_LIGHT_COUNT_TO_TRY_CONSOLIDATE)
		{
			// Get a list of all the cells this light ISN'T part of
			atArray<CChoppedLightDataGridEntry*> otherCells;
			for(int k = 0;k<nCells;k++)
			{
				CChoppedLightDataGridEntry &otherCell = LODLightCategoryGrid.gridEntries[k];
				// If the other cell has lights
				if( otherCell.thisGridLights.size() > MIN_LIGHT_COUNT_TO_TRY_CONSOLIDATE )
				{
					// And it's not the same cell
					if( &otherCell != &thisCell )
					{
						otherCells.PushAndGrow(&otherCell);
					}
				}
			}

			if( otherCells.size() )
			{
				for(int j = thisCell.thisGridLights.size()-1; j>=0; j--)
				{
					CCompleteLODLight &thisLight = thisCell.thisGridLights[j];
					Vector3 lightPos(thisLight.m_position[0], thisLight.m_position[1], thisLight.m_position[2]);

					int otherCellIDX = GetClosestCell(lightPos, otherCells );

					CChoppedLightDataGridEntry &otherCell = *otherCells[otherCellIDX];

					otherCell.thisGridLights.PushAndGrow( thisLight );
					UpdateCellExtentsFromLights(otherCell);
					thisCell.thisGridLights.Delete(j);
				}
			}
		}
	}

	RemoveEmptyCells(LODLightCategoryGrid);
}


// Go Through all the cells that have anything in and look for odd aspect ratios.
// Chop extreme ones in half to make them more square
void	CLightDataCollector::MakeCellsSquareIsh(CChoppedLightDataGrid &LODLightCategoryGrid)
{
	int	nCells = LODLightCategoryGrid.gridEntries.size();

	for( int i=0;i<nCells;i++ )
	{
		CChoppedLightDataGridEntry &thisCell = LODLightCategoryGrid.gridEntries[i];
		if(thisCell.thisGridLights.size())
		{
			// Get Aspect Ratio
			float whRatio = thisCell.width / thisCell.height;

			if( whRatio > 1.0f )
			{
				// It's wider than high
				// Calc number of chops
				float	value = whRatio;
				if(value > 2.0f)
				{
					// Subdivide Vertically.
					DivideCellVByCount( LODLightCategoryGrid, thisCell, (int)value );
				}
			}
			else if( whRatio < 1.0f )
			{
				// It's higher than wide
				float	value = 1.0f / whRatio;
				if(value > 2.0f)
				{
					// Subdivide Horizontally.
					DivideCellHByCount( LODLightCategoryGrid, thisCell, (int)value );
				}
			}
		}
	}

	RemoveEmptyCells(LODLightCategoryGrid);
}


void	CLightDataCollector::GetStatsOnChoppedData(CChoppedLightDataGrid &LODLightCategoryGrid)
{
	SampledValue<u32> value;

	for(int i=0;i<LODLightCategoryGrid.gridEntries.size();i++)
	{
		CChoppedLightDataGridEntry &thisCell = LODLightCategoryGrid.gridEntries[i];
		if( thisCell.thisGridLights.size() )
		{
			value.AddSample( thisCell.thisGridLights.size() );
		}
	}

	Displayf("\nLODLights of category %d has %d used cells",  LODLightCategoryGrid.categoryID, value.Samples());
	Displayf("Total Lights:- %d", value.Sum());
	Displayf("Min Lights Per Cell = %d", value.Min());
	Displayf("Max Lights Per Cell = %d", value.Max());
	Displayf("Average Lights Per Cell = %f", value.Mean());
	Displayf("Std Deviation = %f", value.StandardDeviation());
}


void	CLightDataCollector::DumpDimensionsOfGridCells(CChoppedLightDataGrid &LODLightCategoryGrid)
{
	for(int i=0;i<LODLightCategoryGrid.gridEntries.size();i++)
	{
		CChoppedLightDataGridEntry &thisCell = LODLightCategoryGrid.gridEntries[i];
		if(thisCell.thisGridLights.size())
		{
			float x = thisCell.startx;
			float y = thisCell.starty;
			float w = thisCell.width;
			float h = thisCell.height;
			int	numLights = thisCell.thisGridLights.size();

			Displayf("{	%ff,%ff,%ff,%ff, %d },", x,y,w,h, numLights );
		}
	}
}


void	CLightDataCollector::CreateChoppedDataImaps(CChoppedLightDataGrid &LODLightCategoryGrid, CLightExporterPassInfo *pInfo)
{
char	*pLabel = "small";

	switch(LODLightCategoryGrid.categoryID)
	{
	case Lights::LIGHT_CATEGORY_MEDIUM:
		pLabel = "medium";
		break;
	case Lights::LIGHT_CATEGORY_LARGE:
		pLabel = "large";
		break;
	}

	for(int i=0;i<LODLightCategoryGrid.gridEntries.size();i++)
	{
		spdAABB physicalExtents;
		spdAABB streamingExtentsDistLights;
		spdAABB streamingExtentsLODLights;

		CChoppedLightDataGridEntry &LODLightCategoryGridEntry = LODLightCategoryGrid.gridEntries[i];

		// Only export imaps that have any lights in them
		int	lightsInThisCell = LODLightCategoryGridEntry.thisGridLights.size();
		if( lightsInThisCell )
		{
			CalcIMapExtents(LODLightCategoryGridEntry, LODLightCategoryGrid.DISTVisibilityRadius, physicalExtents, streamingExtentsDistLights);
			CalcIMapExtents(LODLightCategoryGridEntry, LODLightCategoryGrid.LODVisibilityRadius, physicalExtents, streamingExtentsLODLights);

			// Sort into Streetlight/others
			atArray <CCompleteLODLight>	ChoppedLightData;
			atArray <CCompleteLODLight>	ChoppedStreetLightData;

			for (int j=LODLightCategoryGridEntry.thisGridLights.size()-1; j>=0; j--)
			{
				CCompleteLODLight *pLODLight = &LODLightCategoryGridEntry.thisGridLights[j];

				packedTimeAndStateFlags *pFlags = (packedTimeAndStateFlags*)(&pLODLight->m_timeAndStateFlags);
				if( pFlags->bIsStreetLight )
				{
					// Add to street light array
					ChoppedStreetLightData.PushAndGrow(*pLODLight);
				}
				else
				{
					// Add this ChoppedLightData (not street light)
					ChoppedLightData.PushAndGrow(*pLODLight);
				}
				// Remove light from source
				LODLightCategoryGridEntry.thisGridLights.Delete(j);
			}

			// Sort by Hash
			ChoppedStreetLightData.QSort(0,-1,SortByHashFunc);
			ChoppedLightData.QSort(0,-1,SortByHashFunc);

			// Create some imaps
			char	buffer[256];
			char	name[256];

			// Distant Lights imaps
			CMapData *pMapData;
			pMapData = rage_new CMapData;

			// Add in streetlights
			for(int j=0;j<ChoppedStreetLightData.size();j++)
			{
				CCompleteLODLight *pThisLODLight = &ChoppedStreetLightData[j];
				AddDistantLightToIMAP(pThisLODLight, pMapData);
			}
			// Add in normal lights
			for(int j=0;j<ChoppedLightData.size();j++)
			{
				CCompleteLODLight *pThisLODLight = &ChoppedLightData[j];
				AddDistantLightToIMAP(pThisLODLight, pMapData);
			}

			// Add the count of the street lights
			pMapData->m_DistantLODLightsSOA.m_numStreetLights = (u16)ChoppedStreetLightData.size();
			pMapData->m_DistantLODLightsSOA.m_category = (u16)LODLightCategoryGrid.categoryID;

			// Save it out
			pMapData->SetFlags(rage::fwMapData::FLAG_IS_PARENT);
			pMapData->SetPhysicalExtents(physicalExtents);
			pMapData->SetStreamingExtents(streamingExtentsDistLights);
			pMapData->SetContentFlags(rage::fwMapData::CONTENTFLAG_DISTANT_LOD_LIGHTS);

			// A different name for script controlled imaps
			if(pInfo->isScriptControlled)
			{
				pMapData->SetFlags(rage::fwMapData::FLAG_IS_PARENT | rage::fwMapData::FLAG_MANUAL_STREAM_ONLY);
				sprintf(name, "%s_DistantLights", pInfo->pGroupName);
			}
			else
			{
				// Normal
				sprintf(name, "DistLODLights_%s%.3d", pLabel, i);
			}

			pMapData->SetName(name);
			sprintf(buffer, "%s/%s", EXPORT_PATH, name);
			PARSER.SaveObject(buffer, "imap", pMapData, parManager::XML);

			//Displayf("Saved file: %s.imap contains %d DistantLODLights", buffer, pMapData->m_DistantLODLightsSOA.m_position.size() );

			delete pMapData;

			// LOD Lights imaps
			pMapData = rage_new CMapData;
			// Add in street lights
			for(int j=0;j<ChoppedStreetLightData.size();j++)
			{
				CCompleteLODLight *pThisLODLight = &ChoppedStreetLightData[j];
				AddLightToIMAP(pThisLODLight, pMapData);
			}
			// Add in normal lights
			for(int j=0;j<ChoppedLightData.size();j++)
			{
				CCompleteLODLight *pThisLODLight = &ChoppedLightData[j];
				AddLightToIMAP(pThisLODLight, pMapData);
			}

			pMapData->SetFlags(0);
			pMapData->SetPhysicalExtents(physicalExtents);
			pMapData->SetStreamingExtents(streamingExtentsLODLights);
			pMapData->SetParentName(name);								// The name for the parent imap comes from the previous DistanceLODLight export
			pMapData->SetContentFlags(rage::fwMapData::CONTENTFLAG_LOD_LIGHTS);

			// A different name for script controlled imaps
			if(pInfo->isScriptControlled)
			{
				pMapData->SetFlags(rage::fwMapData::FLAG_MANUAL_STREAM_ONLY);
				sprintf(name, "%s_LODLights", pInfo->pGroupName);
			}
			else
			{
				sprintf(name, "LODLights_%s%.3d", pLabel, i);
			}

			pMapData->SetName(name);
			sprintf(buffer, "%s/%s", EXPORT_PATH, name);
			PARSER.SaveObject(buffer, "imap", pMapData, parManager::XML);

			//Displayf("Saved file: %s.imap contains %d LODLights", buffer, pMapData->m_LODLightsSOA.m_direction.size() );
			delete pMapData;

			Displayf("Saved Category %d iMaps for cell %d - contains %d Lights.", LODLightCategoryGrid.categoryID, i, lightsInThisCell );
		}
	}
}

void CLightDataCollector::CalcIMapExtents(CChoppedLightDataGridEntry &LODLightCategoryGridEntry, float visibilityRadius, spdAABB &physicalExtents, spdAABB &streamingExtents )
{
	physicalExtents.Invalidate();
	streamingExtents.Invalidate();
	for(int i=0;i<LODLightCategoryGridEntry.thisGridLights.size();i++)
	{
		CCompleteLODLight ThisLODLight = LODLightCategoryGridEntry.thisGridLights[i];

		physicalExtents.GrowAABB( ThisLODLight.m_PhysicalExtents );

		Vector3 newMin(	ThisLODLight.m_position[0] - visibilityRadius,
						ThisLODLight.m_position[1] - visibilityRadius,
						ThisLODLight.m_position[2] - visibilityRadius);

		Vector3 newMax(	ThisLODLight.m_position[0] + visibilityRadius,
						ThisLODLight.m_position[1] + visibilityRadius,
						ThisLODLight.m_position[2] + visibilityRadius);

		spdAABB theBox(VECTOR3_TO_VEC3V(newMin), VECTOR3_TO_VEC3V(newMax) );
		streamingExtents.GrowAABB( theBox );
	}
}

void CLightDataCollector::AddLightToIMAP( CCompleteLODLight *pThisLODLight, CMapData *pMapData )
{
	// Direction
	FloatXYZ	direction;
	direction.x = pThisLODLight->m_direction[0];
	direction.y = pThisLODLight->m_direction[1];
	direction.z = pThisLODLight->m_direction[2];
	pMapData->m_LODLightsSOA.m_direction.PushAndGrow(direction);

	// falloff
	pMapData->m_LODLightsSOA.m_falloff.PushAndGrow(pThisLODLight->m_falloff);

	//falloffExponent
	pMapData->m_LODLightsSOA.m_falloffExponent.PushAndGrow(pThisLODLight->m_falloffExponent);

	//coneInnerAngle
	pMapData->m_LODLightsSOA.m_coneInnerAngle.PushAndGrow(pThisLODLight->m_packed_coneInnerAngle);

	//coneOuterAngle or capsule extent (mutually exclusive depending on lightType)
	if( pThisLODLight->m_lightType == LIGHT_TYPE_CAPSULE )
	{
		pMapData->m_LODLightsSOA.m_coneOuterAngleOrCapExt.PushAndGrow(pThisLODLight->m_packed_capsuleExtent);
	}
	else
	{
		pMapData->m_LODLightsSOA.m_coneOuterAngleOrCapExt.PushAndGrow(pThisLODLight->m_packed_coneOuterAngle);
	}

	// timeAndStateFlags
	pMapData->m_LODLightsSOA.m_timeAndStateFlags.PushAndGrow(pThisLODLight->m_timeAndStateFlags);

	// hash
	pMapData->m_LODLightsSOA.m_hash.PushAndGrow(pThisLODLight->m_hash);

	// lightType, now part of the flags bitfield
	//pMapData->m_LODLightsSOA.m_lightType.PushAndGrow(pThisLODLight->m_lightType);

	// Corona Intensity
	pMapData->m_LODLightsSOA.m_coronaIntensity.PushAndGrow(pThisLODLight->m_packed_coronaIntensity);
}

void CLightDataCollector::AddDistantLightToIMAP( CCompleteLODLight *pLight, CMapData *pMapData )
{
	// Position
	FloatXYZ	position;
	position.x = pLight->m_position[0];
	position.y = pLight->m_position[1];
	position.z = pLight->m_position[2];
	pMapData->m_DistantLODLightsSOA.m_position.PushAndGrow(position);

	// Colour + Intensity
	Color32 rgbi = Color32( pLight->m_colour[0], pLight->m_colour[1], pLight->m_colour[2], pLight->m_packed_intensity);
	pMapData->m_DistantLODLightsSOA.m_RGBI.PushAndGrow(rgbi.GetColor());
}

////////////////////////////////////////////////////////////////////////////////////////////////////

/*
fiStream *CLightDataCollector::OpenLogFile()
{
	char filenameAndPath[1024];
	sprintf(filenameAndPath, "%s/AllLODLights.log", EXPORT_PATH);
	m_pLogFileHandle = ASSET.Create(filenameAndPath, "");
	return m_pLogFileHandle;
}

void CLightDataCollector::CloseLogFile()
{
	fflush(m_pLogFileHandle);
	m_pLogFileHandle->Close();
}

void CLightDataCollector::WriteLogFileEntry(char *entry)
{
	if( m_pLogFileHandle )
	{
		fprintf(m_pLogFileHandle, entry);
		fflush(m_pLogFileHandle);
	}
}
*/


////////////////////////////////////////////////////////////////////////////////////////////////////

CSimpleLog::CSimpleLog(const char *pFilename)
{
	char filenameAndPath[1024];
	sprintf(filenameAndPath, "%s/%s", EXPORT_PATH, pFilename);
	m_pLogFileHandle = ASSET.Create(filenameAndPath, "");
}

CSimpleLog::~CSimpleLog()
{
	fflush(m_pLogFileHandle);
	m_pLogFileHandle->Close();
	m_pLogFileHandle = NULL;
}

void CSimpleLog::Add(const char *fmt,...)
{
	char buffer[1024];

	va_list args;
	va_start (args, fmt);
	vformatf(buffer,sizeof(buffer),fmt,args);
	va_end (args);
	fprintf (m_pLogFileHandle,buffer);
}

