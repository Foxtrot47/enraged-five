#ifndef __EXPORTER_H
#define __EXPORTER_H

#include "fwnavgen/basetool.h"

#include "fwscene/stores/mapdatastore.h"
#include "fwutil/PtrList.h"
#include "pathserver/ExportCollision.h"
#include "scene/RegdRefTypes.h"
#include "templates/LinkList.h"

#include "LightDataCollector.h"

class CLightsExporterTool;

//////////////////////////////////////////////////////////////////////////////
/////////////////////	CLightExporterInterfaceTool	   ///////////////////////
//////////////////////////////////////////////////////////////////////////////

class CLightExporterInterfaceTool : public CNavMeshDataExporterInterface
{
public:
	explicit CLightExporterInterfaceTool(CLightsExporterTool &tool);

	virtual bool EntityIntersectsCurrentNavMesh(CEntity* pEntity) const;
	virtual bool ExtentsIntersectCurrentNavMesh(Vec3V_In vMin, Vec3V_In vMax) const;
	virtual bool IsAlreadyInPhysicsLevel(CPhysical* pPhysical, phArchetype* pArchetype) const;
	virtual fwPtrList & BuildActiveBuildingsList();
	virtual bool GetCurrentNavMeshExtents(rage::spdAABB & /*outAABB*/) const { return false; /* In imagine this should do something */}
	virtual void AppendToActiveBuildingsArray(atArray<CEntity*>& entityArray);
	virtual void AddSearches(atArray<fwBoxStreamerSearch>& searchList);
	CLightsExporterTool *m_Tool;
};

//////////////////////////////////////////////////////////////////////////////
/////////////////////	CLightExporterPassInfo		   ///////////////////////
//////////////////////////////////////////////////////////////////////////////


class CLightExporterPassInfo
{
public:

	char	*pName;					// Prepended to any LOGS
	char	*pGroupName;			// The name to do a substring search for in script controlled imaps;
	bool	isScriptControlled;

	// Optional based on script controlled flag (for later if other groups come in?)
//	bool	hasNameList;			// If true, compare entire list (strcmp), otherwise compare 
//	char	**ppIPLNames;			// A list of IPL names that are needed for this group.
};

//////////////////////////////////////////////////////////////////////////////
/////////////////////	CLightsExporterTool			   ///////////////////////
//////////////////////////////////////////////////////////////////////////////

class CLightsExporterTool : public fwLevelProcessToolExporterInterface
{
public:

	CLightsExporterTool(fwLevelProcessTool * pTool);
	virtual ~CLightsExporterTool();

	// -- fwLevelProcessToolExporterInterface interface --
	virtual void PreInit();
	virtual void Init();
	virtual void Shutdown();
	virtual bool UpdateExport();

	// -- CLightExporterInterfaceTool support functions --
	static bool IsAlreadyInPhysicsLevel(CPhysical * pPhysical, phArchetype * pArchetype=NULL);
	bool EntityIntersectsCurrentNavMesh(CEntity * pEntity);
	bool ExtentsIntersectCurrentNavMesh(Vec3V_In vMin, Vec3V_In vMax);
	fwPtrList & BuildActiveBuildingsList();
	void AddSearches(atArray<fwBoxStreamerSearch>& searchList);
	bool GetCurrentNavMeshExtents(spdAABB & outAABB) const;

protected:

	CLightExporterInterfaceTool	m_ExporterInterface;

	fwPtrListSingleLink			m_ActiveBuildingsList;

private:

	int					m_MapSectionIDX;
	int					m_ExportInfoNum;

	CLightDataCollector *m_pLightDataCollector;
	CLightExporterPassInfo	*m_pExportInfo;
};

#endif	//__EXPORTER_H
