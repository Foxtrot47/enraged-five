#ifndef __LIGHT_DATA_COLLECTOR_H__
#define	__LIGHT_DATA_COLLECTOR_H__

// rage headers
#include "parser/macros.h"
#include "atl/map.h"
#include "atl/array.h"
#include "string/string.h"
#include "vector/vector3.h"
#include "vector/color32.h"

#include "diag/channel.h"
#include "fwscene/stores/boxstreamer.h"
#include "fwscene/stores/mapdatastore.h"
#include "fwscene/stores/staticboundsstore.h"
#include "fwscene/stores/mapdatastore.h"

// game headers
#include "scene/2dEffect.h"
#include "scene/loader/MapData_Misc.h"
#include "vfx/misc/LODLightManager.h"

#include "scene/loader/MapData.h"

class CLightExporterPassInfo;

RAGE_DECLARE_CHANNEL(lightextraction)
#define LightExtractionFatalAssertf(cond,fmt,...)		RAGE_FATALASSERTF(lightextraction,cond,fmt,##__VA_ARGS__)

class CSimpleLog
{
public:
	CSimpleLog(const char *pFilename);
	~CSimpleLog();

	void Add(const char *fmt,...);
private:
	fiStream	*m_pLogFileHandle;
};

// Contains all the data needed to make both LODLights and DistantLODLights
class CCompleteLODLight
{
public:

	//CDistantLODLight data
	float			m_position[3];
	u8				m_colour[3];
	
	float			m_intensity;

	//CLODLight data
	float			m_direction[3];
	float			m_falloff;
	float			m_falloffExponent;

	float			m_coneInnerAngle;
	float			m_coneOuterAngle;
	float			m_capsuleExtent;
	float			m_coronaIntensity;

	u32				m_timeAndStateFlags;
	u32				m_hash;

	u32				m_LightAttrFlags;			// Stash these, as some are used further down the pipeline beyond mere collection.

	u8				m_lightType;

	// Extra stuff specific to exporter
	spdAABB			m_PhysicalExtents;			// Physical extents of the actual light
	atString		m_FromEntityName;			// The modelname of the entity

	// Store of packed data, to check precision against original data. Created after light is loaded/parsed
	u8				m_packed_intensity;
	u8				m_packed_coneInnerAngle;
	u8				m_packed_coneOuterAngle;
	u8				m_packed_capsuleExtent;
	u8				m_packed_coronaIntensity;

	PAR_SIMPLE_PARSABLE;
};


class CChoppedLightDataGridEntry
{
public:

	// Potentially usefull for joining up
	float	startx;
	float	starty;
	float	width;
	float	height;

	atArray<CCompleteLODLight>	thisGridLights;
};

class	CChoppedLightDataGrid
{
public:
	int		gridElementsX;			// Count of max elements in X
	int		gridElementsY;
	float	gridsizeX;				// The gridsize
	float	gridsizeY;
	int		categoryID;				// The category
	float	DISTVisibilityRadius;	// For imap generation
	float	LODVisibilityRadius;	// For imap generation
	atArray<CChoppedLightDataGridEntry>	gridEntries;	// The lights for this box
};

class CCollectedLODLightData
{
public:
	atArray <CCompleteLODLight>	m_AllLightData;

	PAR_SIMPLE_PARSABLE;
};


class CLightDataCollector
{
public:

	CLightDataCollector(char *pName);
	virtual ~CLightDataCollector();

	class ENTITY_LIGHT_AND_HASH_INFO
	{
	public:
		ENTITY_LIGHT_AND_HASH_INFO();
		ENTITY_LIGHT_AND_HASH_INFO(CLightEntity *pLightEntity, int mapSectionIDX, u32 entityIDX);
		// Does not check the hash or mapsectionIDX or entityIDX entry!
		bool  operator ==(const ENTITY_LIGHT_AND_HASH_INFO& other) const;

		u32				hash;
		Vector3			entPos;
		Vector3			bbMin, bbMax;
		s32				lightID;
		int				mapSectionIDX;
		u32				entityIDX;
		char			entityName[64];
	};

	// Returns the pointer to collected data that clashes with current entity
	void	CollectLight(CEntity *pEntity, CLightEntity *pLight, int mapSectionIDX, u32 entityIDX);
	ENTITY_LIGHT_AND_HASH_INFO *CheckIsUniqueHashID( ENTITY_LIGHT_AND_HASH_INFO &aEntityLightHashInfo);

	int		GetCountOfLightsCollected() { return m_CollectedLightData.m_AllLightData.size(); }

	void	LoadAllLights();
	void	SaveAllLights();

	// Post process, after load or light collection.
	// Creates the packed data, and checks it against unpacked.
	void	PostProcessCollectedLights();

private:

	// Calculates the packed members of the LODLight
	void	PackLightData(CCompleteLODLight &thisLODLight);				
	// Calculates the category of the light
	int		GetLightCategory(CCompleteLODLight &thisLODLight);

	// Print a log of any info for each category
	void	DumpLightCategoryLog(const char *pCategoryName, atArray <CCompleteLODLight> &info);
	void	DumpLightCategoryLogs(atArray<CCompleteLODLight> &a, atArray<CCompleteLODLight> &b, atArray<CCompleteLODLight> &c);

	// Whizzes through all the lights checking for errors in the packed data
	void	CheckPackedLightData();

	static int SortByHashFunc(const CCompleteLODLight *a, const CCompleteLODLight *b);

	static	const char						*m_ModelsNamesForLargeCategory[];
	atArray <ENTITY_LIGHT_AND_HASH_INFO>	m_EntityLightAndHash;

public:
	CCollectedLODLightData					m_CollectedLightData;

	////////////////////////////////////
	// Stuff for chopping up
	////////////////////////////////////

public:

	bool		ChopUpCollectedLightData(CLightExporterPassInfo *pInfo);

private:

	bool		ChopUpLightData(atArray <CCompleteLODLight> &LODLightCategory, CChoppedLightDataGrid &LODLightCategoryGrid, int categoryID, float gridsizeX, float gridsizeY, float DISTVisRadius, float LODVisRadius);
	void		GetLightsForGridEntry(atArray <CCompleteLODLight> &LODLights, int iCurrentSectorX, int iCurrentSectorY, float gridsizeX, float gridsizeY, float worldStartX, float worldStartY, CChoppedLightDataGridEntry &thisGridEntry);
	void		SubdivideOverpopulatedCells(CChoppedLightDataGrid &LODLightCategoryGrid);


	void		SubdivideCell(CChoppedLightDataGrid &LODLightCategoryGrid, CChoppedLightDataGridEntry &theCell );
	int			GetHorizSplitDifference(CChoppedLightDataGridEntry &theCell);
	int			GetVertSplitDifference(CChoppedLightDataGridEntry &theCell);

	void		DivideCellH(CChoppedLightDataGrid &LODLightCategoryGrid, CChoppedLightDataGridEntry &theCell);
	void		DivideCellV(CChoppedLightDataGrid &LODLightCategoryGrid, CChoppedLightDataGridEntry &theCell);
	void		DivideCellVH(CChoppedLightDataGrid &LODLightCategoryGrid, CChoppedLightDataGridEntry &theCell);
	void		DivideCellVByCount(CChoppedLightDataGrid &LODLightCategoryGrid, CChoppedLightDataGridEntry &theCell, int count);
	void		DivideCellHByCount(CChoppedLightDataGrid &LODLightCategoryGrid, CChoppedLightDataGridEntry &theCell, int count);

	void		MoveLightsContainedInNewCells(CChoppedLightDataGridEntry &oldCell, atArray<CChoppedLightDataGridEntry*> &newCells );
	u32			GetClosestCell(Vector3 &pos, atArray<CChoppedLightDataGridEntry*> &newCells );
	float		CalcDistToRectangleSquared(float X, float Y, CChoppedLightDataGridEntry *theCell);

	void		UpdateCellExtentsFromLights(CChoppedLightDataGridEntry &newCell);
	void		RemoveEmptyCells(CChoppedLightDataGrid &LODLightCategoryGrid);

	void		ConsolitdateSparseCells(CChoppedLightDataGrid &LODLightCategoryGrid);
	void		MakeCellsSquareIsh(CChoppedLightDataGrid &m_LODLightSmallGrid);

	void		GetStatsOnChoppedData(CChoppedLightDataGrid &LODLightCategoryGrid);

	void		DumpDimensionsOfGridCells(CChoppedLightDataGrid &LODLightCategoryGrid);

	void		CreateChoppedDataImaps(CChoppedLightDataGrid &LODLightCategoryGrid, CLightExporterPassInfo *pInfo);
	void		CalcIMapExtents(CChoppedLightDataGridEntry &LODLightCategoryGridEntry, float visibilityRadius, spdAABB &physicalExtents, spdAABB &streamingExtents);

	void		AddLightToIMAP( CCompleteLODLight *pLight, CMapData *pMapData );
	void		AddDistantLightToIMAP( CCompleteLODLight *pLight, CMapData *pMapData );

	static		CChoppedLightDataGrid m_LODLightSmallGrid;
	static		CChoppedLightDataGrid m_LODLightMediumGrid;
	static		CChoppedLightDataGrid m_LODLightLargeGrid;
	
	float		m_SmallDistLightRange;
	float		m_SmallLODLightRange;

	float		m_MediumDistLightRange;
	float		m_MediumLODLightRange;

	float		m_LargeDistLightRange;
	float		m_LargeLODLightRange;

	char		*m_pOutputName;

	////////////////////////////////////

	// Logging
	CSimpleLog	*pAALightsLOG;
	CSimpleLog	*pAADupesLOG;
	CSimpleLog	*pAAHashClashLOG;

	////////////////////////////////////
};

template<typename T> class Limits
{
public:
	static T Min();
	static T Max();
};

template<> class Limits<u32>
{
public:
	static u32 Min() { return 0; }
	static u32 Max() { return 0xFFFFFFFF; }
};

template<typename T> class SampledValue
{
public:
	SampledValue()
	{
		Reset();
	}

	void AddSample(T sample)
	{
		++m_Samples;

		// From http://www.johndcook.com/standard_deviation.html 
		// See Knuth TAOCP vol 2, 3rd edition, page 232
		if (m_Samples == 1)
		{
			m_Mean = float(sample);
			m_Sum = 0.0f;
		}
		else
		{
			float oldMean = m_Mean;
			float oldSum = m_Sum;
			m_Mean = oldMean + (sample - oldMean)/m_Samples;
			m_Sum = oldSum + (sample - oldMean)*(sample - m_Mean);
		}

		m_Min = rage::Min(sample, m_Min);
		m_Max = rage::Max(sample, m_Max);
	}

	void Reset()
	{
		m_Sum = 0;
		m_Mean = 0;
		m_Samples = 0;
		m_Min = Limits<T>::Max();
		m_Max = Limits<T>::Min();
	}

	const T& Min() const
	{
		return m_Min;
	}

	const T& Max() const
	{
		return m_Max;
	}

	float Mean() const
	{
		return (m_Samples > 0 ? m_Mean : 0.0f);
	}

	float Variance() const
	{
		return ( (m_Samples > 1) ? m_Sum/(static_cast<float>(m_Samples) - 1) : 0.0f );
	}

	float StandardDeviation() const
	{
		return sqrt( Variance() );
	}

	int Samples() const
	{
		return m_Samples;
	}

	T Sum() const
	{
		return static_cast<T>(m_Mean * m_Samples);	
	}

private:
	int m_Samples;
	float m_Mean;
	float m_Sum;
	T m_Min;
	T m_Max;
};



#endif	//__LIGHT_DATA_COLLECTOR_H__
