# Requirements
1. [Visual Studio 2012](https://archive.org/download/dev-microsoft-visual-studio-2005-2015-Pro/en_visual_studio_professional_2012_x86_dvd_2262334.iso) 
with [update 4](https://archive.org/download/msdn_2023/mu_visual_studio_2012_update_4_x86_dvd_3161759.iso)
2. [Incredibuild 4.0](https://xoreax-incredibuild.software.informer.com/4.0/)
    - Note to install Incredibuild 4.0 on Windows 11 you will have to disable Admin Approval for apps
    in group policy editor

    ![Group Policy](images/mmc_CIV71u9mod.png "Disabling admin approval")

    **For obvious reasons enable it back it after you install, else every program you run will be executed as admin**

3. Fake Network Adapter with Rockstar Studios IP/Subnet
    - Follow [this](https://www.process.st/how-to/enable-microsoft-loopback-adapter) on how to create a vritual adapter
    - Use `10.10.0.1` as IP Address , `255.255.0.0` as Subnet mask, `10.11.0.1` as Default Gateway

# Building game exe, shaders and scripts

1. Clone this repository to X:\gta5
    - **Yes this exact path is mandatory for builds to work correctly**

2. Setup Environment variables
    
    ![Env vars](images/SystemPropertiesAdvanced_U19SW3YHYZ.png "Environment variables")

4. Open `X:\gta5\src\dev_ng\game\VS_Project\game_2012_unity.sln` using visual studio 2012

5. Set build mode to BankRelease and make sure platform is x64

6. Compile game project

7. 

7. Run `X:\gta5\tools_ng\bin\RageScriptEditor\RebuildGTA5_SP.bat`
    - This will a long time. You can see build progress in incredibuild icon in task bar

# Running game

1. Download and instal full game to `X:\gta5\titleupdate\dev_ng`

2. Install OpenIV

3. Open update.rpf with OpenIV and in in gameconfig.xml (`update.rpf/common/data/`) set SizeOfStack for MISSION to 53000

4. Copy compiled scripts.rpf 
(located in `X:\gta5\titleupdate\dev_ng\x64\levels\gta5\script`) to `update2.rpf/levels/gta5/scripts`

    - **Note: OpenIV loves to load stuff from mods directory using plugins, disable this functionality rpf might not load**

5. Run `X:\gta5\archivefix\ArchiveFix.exe fix "X:\gta5\titleupdate\dev_ng\update\update.rpf"` 
and `X:\gta5\archivefix\ArchiveFix.exe fix "X:\gta5\titleupdate\dev_ng\update\update2.rpf"` and 

6. run game with these params

    `./game_win64_bankrelease.exe -noSocialClub -nokeyboardhook -nonetlogs -output -rag -ragUseOwnWindow -doreleasestartup`

7. Press LCtrl+Tab to switch keyboard mode from debug to gameplay, otherwise a controller is required