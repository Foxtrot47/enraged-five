USING "rage_builtins.sch"
USING "globals.sch"
USING "shop_public.sch"
USING "respawn_location_private.sch"
USING "website_public.sch"
USING "player_ped_public.sch"
USING "script_clock.sch"
USING "flow_mission_data_public.sch"
#IF IS_DEBUG_BUILD
	#if USE_CLF_DLC
	USING "code_control_data_CLF.sch"	
	#endif
	#if USE_NRM_DLC
	USING "code_control_data_NRM.sch"	
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		USING "code_control_data_GTA5.sch"
	#endif
	#endif
	USING "flow_debug_GAME.sch"	
#ENDIF

#IF IS_DEBUG_BUILD
	#IF MISSION_FLOW_INITIALISER_DEBUG_OUTPUT
		USING "comms_control_private.sch"
	#ENDIF
#ENDIF


USING "flow_commands_core_override.sch"
USING "flow_storage_core.sch"


// -----------------------------------------------------------------------------------------------------------
//		Comms Array Storage
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store all the variables associated with standard communication mission flow commands
//
PROC STORE_COMMS_STORAGE_TYPE_VARIABLES(INT paramArrayPos, INT paramInt1, INT paramInt2, INT paramInt3, INT paramInt4, INT paramInt5, INT paramInt6, INT paramInt7, INT paramInt8, VectorID paramVectorID, CC_CodeID paramCodeID, FLOW_CHECK_IDS paramCheckID)
	
	g_flowUnsaved.commsVars[paramArrayPos].iValue1 		= paramInt1
	g_flowUnsaved.commsVars[paramArrayPos].iValue2 		= paramInt2
	g_flowUnsaved.commsVars[paramArrayPos].iValue3 		= paramInt3
	g_flowUnsaved.commsVars[paramArrayPos].iValue4 		= paramInt4
	g_flowUnsaved.commsVars[paramArrayPos].iValue5 		= paramInt5
	g_flowUnsaved.commsVars[paramArrayPos].iValue6 		= paramInt6
	g_flowUnsaved.commsVars[paramArrayPos].iValue7 		= paramInt7
	g_flowUnsaved.commsVars[paramArrayPos].iValue8 		= paramInt8
	g_flowUnsaved.commsVars[paramArrayPos].eVectorID 	= paramVectorID
	g_flowUnsaved.commsVars[paramArrayPos].eCodeID		= paramCodeID
	g_flowUnsaved.commsVars[paramArrayPos].eCheckID		= paramCheckID

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//		Comms large Array Storage
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store all the variables associated with standard communication mission flow commands
//
PROC STORE_COMMS_LARGE_STORAGE_TYPE_VARIABLES(INT paramArrayPos, INT paramInt1, INT paramInt2, INT paramInt3, INT paramInt4, INT paramInt5, INT paramInt6, INT paramInt7, INT paramInt8, INT paramInt9, JUMP_LABEL_IDS paramJump1, JUMP_LABEL_IDS paramJump2, JUMP_LABEL_IDS paramJump3, VectorID paramVectorID, CC_CodeID paramCodeID, FLOW_CHECK_IDS paramCheckID)
	
	g_flowUnsaved.commsLargeVars[paramArrayPos].iValue1 	= paramInt1
	g_flowUnsaved.commsLargeVars[paramArrayPos].iValue2 	= paramInt2
	g_flowUnsaved.commsLargeVars[paramArrayPos].iValue3 	= paramInt3
	g_flowUnsaved.commsLargeVars[paramArrayPos].iValue4 	= paramInt4
	g_flowUnsaved.commsLargeVars[paramArrayPos].iValue5 	= paramInt5
	g_flowUnsaved.commsLargeVars[paramArrayPos].iValue6 	= paramInt6
	g_flowUnsaved.commsLargeVars[paramArrayPos].iValue7 	= paramInt7
	g_flowUnsaved.commsLargeVars[paramArrayPos].iValue8 	= paramInt8
	g_flowUnsaved.commsLargeVars[paramArrayPos].iValue9 	= paramInt9
	g_flowUnsaved.commsLargeVars[paramArrayPos].eJump1		= paramJump1
	g_flowUnsaved.commsLargeVars[paramArrayPos].eJump2		= paramJump2
	g_flowUnsaved.commsLargeVars[paramArrayPos].eVectorID 	= paramVectorID
	g_flowUnsaved.commsLargeVars[paramArrayPos].eCodeID		= paramCodeID
	g_flowUnsaved.commsLargeVars[paramArrayPos].eCheckID	= paramCheckID
	
	#IF IS_DEBUG_BUILD
		g_flowUnsaved.commsLargeVars[paramArrayPos].eJump3		= paramJump3
	#ENDIF
	#IF IS_FINAL_BUILD
		UNUSED_PARAMETER(paramJump3)
	#ENDIF

ENDPROC



// PURPOSE: Store 'Activate RandomChar Mission' command
// 
// INPUT PARAMS:	paramRandomCharID		The RandomChar ID
PROC SETFLOW_ACTIVATE_RANDOMCHAR_MISSION(g_eRC_MissionIDs paramRandomCharID, BOOL bFullActivated)

	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_ACTIVATE_RANDOMCHAR_MISSION
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	STORE_CORE_STORAGE_TYPE_VARIABLES(	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
										ENUM_TO_INT(paramRandomCharID),
										PICK_INT(bFullActivated, 1, 0),
										-1,										// Not Needed
										-1)										// Not Needed
											
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": ACTIVATE RANDOMCHAR MISSION [ENUM AS INT=", ENUM_TO_INT(paramRandomCharID), "]")
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC



// PURPOSE: Store 'Deactivate RandomChar Mission' command
// 
// INPUT PARAMS:	paramRandomCharID		The RandomChar ID
PROC SETFLOW_DEACTIVATE_RANDOMCHAR_MISSION(g_eRC_MissionIDs paramRandomCharID)

	FLOW_STORAGE_TYPES	thisStorageType = FST_INDEX_DIRECT
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_DEACTIVATE_RANDOMCHAR_MISSION
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= ENUM_TO_INT(paramRandomCharID) //NB. Store value directly as array index.
	
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": SETFLOW_DEACTIVATE_RANDOMCHAR_MISSION [ENUM AS INT=", ENUM_TO_INT(paramRandomCharID), "]")
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Display Help Text' command
// 
// INPUT PARAMS:	paramDisplayTime		The amount of time each section of the help message will remain on screen.
//					paramHelpLabel			A text label pointing to the help text message.
//					paramJumpToFront		Should this help messages be jumped to the front of the help text queue?
//					paramCharBitset			A bitset that sets which characters are allowed to see this help message. Defaults to (CHAR_MICHAEL|CHAR_FRANKLIN|CHAR_TREVOR).
//					paramExecuteOnSend		Custom block of script to run when the help text sends.
PROC SETFLOW_DISPLAY_HELP_TEXT(STRING paramHelpLabel, INT paramDisplayTime = DEFAULT_HELP_TEXT_TIME, FlowHelpPriority paramPriority = FHP_HIGH, INT paramCharBitset = 7, CC_CodeID paramExecuteOnSend = CID_BLANK, INT paramHashModifier = 0)

	//Using the phonecall array because it has a bunch of text label elements in it. This is quite a large fudge!
	FLOW_STORAGE_TYPES	thisStorageType = FST_TEXT_LARGE
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_DISPLAY_HELP_TEXT
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	TEXT_LABEL_31 tHelpLabel = paramHelpLabel
	
	INT iData = ENUM_TO_INT(paramPriority)
	iData += paramHashModifier*FLOW_COMMAND_HASH_MODIFIER_MAGNITUDE

	STORE_TEXT_LARGE_TYPE_VARIABLES(g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
									tHelpLabel,
									iData,
									paramDisplayTime,
									paramCharBitset,
									ENUM_TO_INT(paramExecuteOnSend))
											
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": DISPLAY HELP TEXT [DISPLAY-TIME=", paramDisplayTime, " LABEL=", paramHelpLabel, "]")
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// PURPOSE: Store 'Cancel Help Text' command
// 
// INPUT PARAMS:	paramHelpLabel			A text label pointing to the help text message to be removed from the help queue.
PROC SETFLOW_CANCEL_HELP_TEXT(STRING paramHelpLabel, INT paramHashModifier = 0)

	//Using the phonecall array because it has a bunch of text label elements in it. This is quite a large fudge!
	FLOW_STORAGE_TYPES	thisStorageType = FST_TEXT_LARGE
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_CANCEL_HELP_TEXT
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	TEXT_LABEL_31 tHelpLabel = paramHelpLabel
	
	STORE_TEXT_LARGE_TYPE_VARIABLES(	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
										tHelpLabel,
										paramHashModifier,
										0,
										0,
										0)
											
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": CANCEL HELP TEXT [LABEL=", paramHelpLabel, "]")
	
	STOP_FLOW_COMMAND(thisStorageType)
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Do Mission At Blip' command + variables
//
// INPUT PARAMS:	paramMissionID		The mission ID.
//					paramPassGoto		The command to move on to when the mission passes.
//					paramFailGoto		The command to move on to when the mission fails.

PROC SETFLOW_DO_MISSION_AT_BLIP(SP_MISSIONS paramMissionID, JUMP_LABEL_IDS paramPassGoto, JUMP_LABEL_IDS paramFailGoto, JUMP_LABEL_IDS paramTimeMissedGoto = STAY_ON_THIS_COMMAND, JUMP_LABEL_IDS paramDebugGoto = STAY_ON_THIS_COMMAND)

	//If no specific debug jump is set, use the pass jump by default.
	IF paramDebugGoto = STAY_ON_THIS_COMMAND
		paramDebugGoto = paramPassGoto
	ENDIF
	
	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE

	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_DO_MISSION_AT_BLIP
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	//Store the script variables index in the mission data array.
	g_sMissionStaticData[paramMissionID].coreVariablesIndex = g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	STORE_CORE_STORAGE_TYPE_VARIABLES(	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
										ENUM_TO_INT(paramMissionID),
										ENUM_TO_INT(paramPassGoto),
										ENUM_TO_INT(paramFailGoto),
										ENUM_TO_INT(paramDebugGoto))
											
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": DO_MISSION_AT_BLIP [ID=", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMissionID), " COREVARS=", g_sMissionStaticData[paramMissionID].coreVariablesIndex, " PASSGOTO=", Get_Label_Display_String_From_Label_ID(paramPassGoto), " FAILGOTO=", Get_Label_Display_String_From_Label_ID(paramFailGoto), " TIMEMISSEDGOTO=", Get_Label_Display_String_From_Label_ID(paramTimeMissedGoto), " DEBUGGOTO=", Get_Label_Display_String_From_Label_ID(paramFailGoto), "]")
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Do Mission Now' command + variables
//
// INPUT PARAMS:	paramMissionID		The mission ID.
//					paramPassGoto		The command to move on to when the mission passes.
//					paramFailGoto		The command to move on to when the mission fails.
//					paramDebugGoto		The command to move on to when we're stepping through the flow in debug mode with the flow launcher.

PROC SETFLOW_DO_MISSION_NOW(SP_MISSIONS paramMissionID, JUMP_LABEL_IDS paramPassGoto, JUMP_LABEL_IDS paramFailGoto, JUMP_LABEL_IDS paramDebugGoto = STAY_ON_THIS_COMMAND)

	//If no specific debug jump is set, use the pass jump by default.
	IF paramDebugGoto = STAY_ON_THIS_COMMAND
		paramDebugGoto = paramPassGoto
	ENDIF
	
	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_DO_MISSION_NOW
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	//Store the script variables index in the mission data array.
	g_sMissionStaticData[paramMissionID].coreVariablesIndex = g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]

	STORE_CORE_STORAGE_TYPE_VARIABLES(	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
										ENUM_TO_INT(paramMissionID),
										ENUM_TO_INT(paramPassGoto),
										ENUM_TO_INT(paramFailGoto),
										ENUM_TO_INT(paramDebugGoto))							
											
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": DO_MISSION_NOW [ID=", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMissionID), " PASSGOTO=", Get_Label_Display_String_From_Label_ID(paramPassGoto), " FAILGOTO=", Get_Label_Display_String_From_Label_ID(paramFailGoto), " DEBUGGOTO=", Get_Label_Display_String_From_Label_ID(paramDebugGoto), "]")
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// PURPOSE: Store 'Do Mission At Switch' command + variables
//
// INPUT PARAMS:	paramMissionID		The mission ID.
//					paramPassGoto		The command to move on to when the mission passes.
//					paramFailGoto		The command to move on to when the mission fails.

PROC SETFLOW_DO_MISSION_AT_SWITCH(SP_MISSIONS paramMissionID, JUMP_LABEL_IDS paramPassGoto, JUMP_LABEL_IDS paramFailGoto, JUMP_LABEL_IDS paramDebugGoto = STAY_ON_THIS_COMMAND)

	//If no specific debug jump is set, use the pass jump by default.
	IF paramDebugGoto = STAY_ON_THIS_COMMAND
		paramDebugGoto = paramPassGoto
	ENDIF
	
	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE

	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_DO_MISSION_AT_SWITCH
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	//Store the script variables index in the mission data array.
	g_sMissionStaticData[paramMissionID].coreVariablesIndex = g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	STORE_CORE_STORAGE_TYPE_VARIABLES(	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
										ENUM_TO_INT(paramMissionID),
										ENUM_TO_INT(paramPassGoto),
										ENUM_TO_INT(paramFailGoto),
										ENUM_TO_INT(paramDebugGoto))
											
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": DO_MISSION_AT_SWITCH [ID=", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMissionID), " PASSGOTO=", Get_Label_Display_String_From_Label_ID(paramPassGoto), " FAILGOTO=", Get_Label_Display_String_From_Label_ID(paramFailGoto), " DEBUGGOTO=", Get_Label_Display_String_From_Label_ID(paramFailGoto), "]")
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// PURPOSE: Store 'Set Bikes Supressed State' command + variables
//
// INPUT PARAMS:	paramState	TRUE if motorbikes should be blocked from general population. FALSE if they are available.

PROC SETFLOW_SET_BIKES_SUPRESSED_STATE(BOOL paramState)

	FLOW_STORAGE_TYPES	thisStorageType = FST_INDEX_DIRECT
	
	START_FLOW_COMMAND(thisStorageType)
	
	INT iState = 0
	IF paramState
		iState = 1
	ENDIF
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_SET_BIKES_SUPRESSED_STATE
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= iState // Direct index storage. Store state as the index itself.
	
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": SET_BIKES_SUPRESSED_STATE [STATE=", paramState, "]")
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC

/// PURPOSE:
///    Store 'FLOW_SET_FRANKLIN_OUTFIT_BITSET_STATE' command + variables
/// PARAMS:
///    paramState - TRUE if we're activating Franklin's outift checks, FALSE if we're ending them
PROC SETFLOW_SET_FRANKLIN_OUTFIT_BITSET_STATE(BOOL paramState)
	FLOW_STORAGE_TYPES	thisStorageType = FST_INDEX_DIRECT
	
	START_FLOW_COMMAND(thisStorageType)
	
	INT iState = 0
	IF paramState
		iState = 1
	ENDIF
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_SET_FRANKLIN_OUTFIT_BITSET_STATE
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= iState // Direct index storage. Store state as the index itself.
	
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": SET_FRANKLIN_OUTFIT_BITSET_STATE [STATE=", paramState, "]")
	
	STOP_FLOW_COMMAND(thisStorageType)
ENDPROC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Do Autosave' command
//

PROC SETFLOW_WAIT_FOR_AUTOSAVE(INT paramHashModifier = 0)

	FLOW_STORAGE_TYPES	thisStorageType = FST_INDEX_DIRECT
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_WAIT_FOR_AUTOSAVE
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= paramHashModifier
											
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": WAIT_FOR_AUTOSAVE")
	
	STOP_FLOW_COMMAND(thisStorageType)
	
ENDPROC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Launch Script and Forget' command + variables
//
// INPUT PARAMS:	paramFilename		The script filename
// INPUT PARAMS:	paramStackSize		The stack to assign this script as we launch it.

PROC SETFLOW_LAUNCH_SCRIPT_AND_FORGET(STRING paramScriptName, INT paramStackSize = DEFAULT_STACK_SIZE)

	FLOW_STORAGE_TYPES	thisStorageType = FST_TEXT_LARGE

	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_LAUNCH_SCRIPT_AND_FORGET
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	TEXT_LABEL_31 tScriptName = paramScriptName
	
	STORE_TEXT_LARGE_TYPE_VARIABLES(g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
									tScriptName,
									paramStackSize,
									0,					// Not Needed
									0,					// Not Needed
									0)					// Not Needed
											
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": LAUNCH_SCRIPT_AND_FORGET [FILE=", paramScriptName, " STACK=", paramStackSize, "]")
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// PURPOSE: Store 'Run Code ID' command + variables
//
PROC SETFLOW_RUN_CODE_ID(CC_CodeID paramCodeID, INT paramDelay)

	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE_SMALL
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_RUN_CODE_ID
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	STORE_CORE_SMALL_STORAGE_TYPE_VARIABLES(g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
											ENUM_TO_INT(paramCodeID),
											paramDelay)

											
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": RUN CODE ID [ID=", ENUM_TO_INT(paramCodeID), " DELAY= ", paramDelay, "]")
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// PURPOSE: Store 'Debug Run Code ID' command + variables
//
PROC SETFLOW_DEBUG_RUN_CODE_ID(CC_CodeID paramCodeID, INT paramDelay, INT paramHashModifier = 0)

#IF IS_DEBUG_BUILD
	FLOW_STORAGE_TYPES	thisStorageType = FST_DEBUG
	
	START_FLOW_COMMAND(thisStorageType)
	
	INT iData = ENUM_TO_INT(paramCodeID)
	iData += (paramHashModifier*FLOW_COMMAND_HASH_MODIFIER_MAGNITUDE)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_DEBUG_RUN_CODE_ID
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	
		STORE_DEBUG_STORAGE_TYPE_VARIABLES(		g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
												iData,
												paramDelay)
#ENDIF

#IF IS_FINAL_BUILD
	FLOW_STORAGE_TYPES	thisStorageType = FST_INDEX_DIRECT
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_DEBUG_DUMMY
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= paramHashModifier
#ENDIF
						
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": RUN DEBUG CODE ID [ID=", ENUM_TO_INT(paramCodeID), " DELAY= ", paramDelay, "]")
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Set Blip Long-Range' command
// 
// INPUT PARAMS:	paramBlipID				The static blip ID
//					paramState				The new blip long-range state: TRUE (long-range) or FALSE (short-range)
//					paramOnMission			Should the blip remain long range while on mission?

PROC SETFLOW_ENABLE_BLIP_LONG_RANGE(STATIC_BLIP_NAME_ENUM paramBlipID, BOOL paramNotDuringMission = TRUE)

	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE_SMALL
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_ENABLE_BLIP_LONG_RANGE
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	//Use the general use INT to store the OnMission flag. 0 = FALSE, 1 = TRUE
	INT notDuringMissionFlag
	IF paramNotDuringMission
		notDuringMissionFlag = 1
	ELSE
		notDuringMissionFlag = 0
	ENDIF
	
	STORE_CORE_SMALL_STORAGE_TYPE_VARIABLES(g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
											ENUM_TO_INT(paramBlipID),			
											notDuringMissionFlag)
											
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": SET BLIP LONG-RANGE [BLIP=", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(ENUM_TO_INT(paramBlipID)), " STATE=ON ON-MISSION=", Get_Bool_Display_String_From_Bool(paramNotDuringMission), "]")
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Set Blip Long-Range' command
// 
// INPUT PARAMS:	paramBlipID				The static blip ID
//					paramState				The new blip long-range state: TRUE (long-range) or FALSE (short-range)
//					paramOnMission			Should the blip remain long range while on mission?

PROC SETFLOW_DISABLE_BLIP_LONG_RANGE(STATIC_BLIP_NAME_ENUM paramBlipID, BOOL paramNotDuringMission = TRUE)

	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE_SMALL
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_DISABLE_BLIP_LONG_RANGE
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	//Use the general use INT to store the OnMission flag. 0 = FALSE, 1 = TRUE
	INT notDuringMissionFlag
	IF paramNotDuringMission
		notDuringMissionFlag = 1
	ELSE
		notDuringMissionFlag = 0
	ENDIF
	
	STORE_CORE_SMALL_STORAGE_TYPE_VARIABLES(g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
											ENUM_TO_INT(paramBlipID),			
											notDuringMissionFlag)
											
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": SET BLIP LONG-RANGE [BLIP=", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(ENUM_TO_INT(paramBlipID)), " STATE=OFF ON-MISSION=", Get_Bool_Display_String_From_Bool(paramNotDuringMission), "]")
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Set Building State' command
// 
// INPUT PARAMS:	paramBuildingID				An enum representing the building that will have its state changed.
//					paramStateID				An enum representing the state that the building will be changed to.

PROC SETFLOW_SET_BUILDING_STATE(BUILDING_NAME_ENUM paramBuildingID, BUILDING_STATE_ENUM paramStateID, BOOL paramLeaveArea = TRUE, INT paramHashModifier = 0)

	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_SET_BUILDING_STATE
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	INT iLeaveArea = 0
	IF paramLeaveArea
		iLeaveArea = 1
	ENDIF
	
	STORE_CORE_STORAGE_TYPE_VARIABLES(	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
										ENUM_TO_INT(paramBuildingID),
										ENUM_TO_INT(paramStateID),
										iLeaveArea,
										paramHashModifier)
											
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": SET BUILDING STATE [BUILDING=", ENUM_TO_INT(paramBuildingID), " STATE=", ENUM_TO_INT(paramStateID), "]")
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Set Weapon Lock State' command
// 
// INPUT PARAMS:	paramWeaponCompID		The weapon enum
//					paramState				The new lock state: TRUE (1) or FALSE (0)
//
// NOTES:	The weapon enum will be converted to and stored as an INT

PROC SETFLOW_SET_WEAPON_LOCK_STATE(WEAPON_TYPE paramWeaponID, BOOL paramStateUnlocked)

	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE_SMALL
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_SET_WEAPON_LOCK_STATE
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	INT iParamStateUnlocked = 0
	IF paramStateUnlocked
		iParamStateUnlocked = 1
	ENDIF
	
	STORE_CORE_SMALL_STORAGE_TYPE_VARIABLES(g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
											GET_WEAPON_INDEX_FROM_WEAPON_TYPE(paramWeaponID),
											iParamStateUnlocked)

											
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": SET WEAPON LOCK STATE [WEAPON=", GET_STRING_FROM_TEXT_FILE(GET_WEAPON_NAME(paramWeaponID)), " STATE=", Get_Bool_Display_String_From_Bool(paramStateUnlocked),"]")
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Set Buyable Vehicle State' command
// 
// INPUT PARAMS:	paramWeaponCompID		The buyable vehicle enum
//					paramState				The new purchased state: TRUE (1) or FALSE (0)
//
// NOTES:	The buyable vehicle enum will be converted to and stored as an INT

PROC SETFLOW_SET_BUYABLE_VEHICLE_STATE(SITE_BUYABLE_VEHICLE paramVehicleID, enumCharacterList eCharacter, BOOL paramStatePurchased)

	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_SET_BUYABLE_VEHICLE_STATE
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	INT iStatePurchased = 0
	IF paramStatePurchased
		iStatePurchased = 1
	ENDIF
	
	STORE_CORE_STORAGE_TYPE_VARIABLES(g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
											ENUM_TO_INT(paramVehicleID),
											ENUM_TO_INT(eCharacter),
											iStatePurchased,
											0)
											
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": SET BUYABLE VEHICLE STATE [VEHICLE=", GET_STRING_FROM_TEXT_FILE(GET_LABEL_BUYABLE_VEHICLE(paramVehicleID)), " CHAR=", eCharacter, " STATE=", Get_Bool_Display_String_From_Bool(paramStatePurchased),"]")
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Set Static Blip State' command
// 
// INPUT PARAMS:	paramBlipID				The static blip ID
//					paramState				The new static blip active state: TRUE (1) or FALSE (0)

PROC SETFLOW_SET_STATIC_BLIP_STATE(STATIC_BLIP_NAME_ENUM paramBlipID, BOOL paramState)

	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE_SMALL
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_SET_STATIC_BLIP_STATE
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	INT iState = 0
	IF paramState
		iState = 1
	ENDIF
	
	STORE_CORE_SMALL_STORAGE_TYPE_VARIABLES(g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
											ENUM_TO_INT(paramBlipID),			
											iState)
											
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": SET STATIC BLIP STATE [BLIP=", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(ENUM_TO_INT(paramBlipID)), " STATE=", Get_Bool_Display_String_From_Bool(paramState), "]")
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Do Switch To Character' command
// 
// INPUT PARAMS:	paramCharacterID		The character we want to switch to
//
// NOTES:	The character enum will be converted to and stored as an INT

PROC SETFLOW_DO_SWITCH_TO_CHARACTER(enumCharacterList paramCharacterID)

	FLOW_STORAGE_TYPES	thisStorageType = FST_INDEX_DIRECT
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_DO_SWITCH_TO_CHARACTER
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= ENUM_TO_INT(paramCharacterID)
											
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": DO SWITCH TO CHARACTER [CHAR=", GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(paramCharacterID), "]")
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Set Strand Save Override' command
// 
// INPUT PARAMS:	paramMissionID		ID of any mission to set as uncompleted when restoring this save override.
//					paramMPSwitchOnly	Set TRUE if this strand override should only be applied when switching from SP to MP and not on loading a save.		

PROC SETFLOW_SET_STRAND_SAVE_OVERRIDE(SP_MISSIONS paramMissionID = SP_MISSION_NONE, BOOL paramMPSwitchOnly = FALSE)
	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE_SMALL
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_SET_STRAND_SAVE_OVERRIDE
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	INT iParamMPSwitchOnly = 0
	IF paramMPSwitchOnly
		iParamMPSwitchOnly = 1
	ENDIF
	
	STORE_CORE_SMALL_STORAGE_TYPE_VARIABLES(g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
											ENUM_TO_INT(paramMissionID),
											iParamMPSwitchOnly)
									
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": SETFLOW_SET_STRAND_SAVE_OVERRIDE")
	STOP_FLOW_COMMAND(thisStorageType)
ENDPROC


// PURPOSE: Store 'Clear Strand Save Override' command
// 
PROC SETFLOW_CLEAR_STRAND_SAVE_OVERRIDE(INT paramHashModifier = 0)
	FLOW_STORAGE_TYPES	thisStorageType = FST_INDEX_DIRECT
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_CLEAR_STRAND_SAVE_OVERRIDE
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= paramHashModifier
											
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": CLEAR_STRAND_SAVE_OVERRIDE")
	
	STOP_FLOW_COMMAND(thisStorageType)
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Add Phone Contact' command
// 
// INPUT PARAMS:	paramContact						The contact character being added to the phone.
//					paramPhoneOwner						The character who owns the phone. (Must be a playable characters.
//					paramDisplayNewContactSignifier		Should an on-screen icon be displayed as this contact is added to the phone?

PROC SETFLOW_ADD_PHONE_CONTACT(enumCharacterList paramCharacterContact, enumCharacterList paramCharacterPhoneOwner, BOOL paramDisplayNewContactSignifier = TRUE)

	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE
	
	//Check the phone owner parameter is valid.
	IF 	paramCharacterPhoneOwner <> CHAR_MICHAEL
	AND paramCharacterPhoneOwner <> CHAR_FRANKLIN
	AND paramCharacterPhoneOwner <> CHAR_TREVOR
		SCRIPT_ASSERT("SETFLOW_ADD_PHONE_CONTACT: Phone owner parameter was not a playable character. Failed to add flow command.")
		EXIT
	ENDIF
	
	INT iNewContactSignifier = 0
	IF paramDisplayNewContactSignifier
		iNewContactSignifier = 1
	ENDIF
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_ADD_PHONE_CONTACT
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	STORE_CORE_STORAGE_TYPE_VARIABLES(	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
										ENUM_TO_INT(paramCharacterPhoneOwner),
										ENUM_TO_INT(paramCharacterContact),
										iNewContactSignifier,
										0)											// Not Needed.
											
	// Debug Output
	#IF IS_DEBUG_BUILD
		#IF MISSION_FLOW_INITIALISER_DEBUG_OUTPUT
			PRINTINT(g_flowUnsaved.iNumStoredFlowCommands)
			PRINTSTRING(": ADD-PHONE-CONTACT [CONTACT=")
			PRINTSTRING(GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(paramCharacterContact))
			PRINTSTRING(" PHONE-OWNER=")
			PRINTSTRING(GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(paramCharacterPhoneOwner))
			PRINTSTRING(" DISPLAY-ON-SCREEN=")
			PRINTBOOL(paramDisplayNewContactSignifier)
			PRINTSTRING("]")
			PRINTNL()
		#ENDIF
	#ENDIF
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// PURPOSE: Store 'Remove Phone Contact' command
// 
// INPUT PARAMS:	paramContact		The contact character being added to the phone.
//					paramPhoneOwner		The character who owns the phone. (Must be a playable characters.)

PROC SETFLOW_REMOVE_PHONE_CONTACT(enumCharacterList paramCharacterContact, enumCharacterList paramCharacterPhoneOwner)

	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE_SMALL
	
	//Check the phone owner parameter is valid.
	IF 	paramCharacterPhoneOwner <> CHAR_MICHAEL
	AND paramCharacterPhoneOwner <> CHAR_FRANKLIN
	AND paramCharacterPhoneOwner <> CHAR_TREVOR
		SCRIPT_ASSERT("SETFLOW_REMOVE_PHONE_CONTACT: Phone owner parameter was not a playable character. Failed to add flow command.")
		EXIT
	ENDIF
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_REMOVE_PHONE_CONTACT
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	STORE_CORE_SMALL_STORAGE_TYPE_VARIABLES(g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
											ENUM_TO_INT(paramCharacterPhoneOwner),
											ENUM_TO_INT(paramCharacterContact))
											
	// Debug Output
	#IF IS_DEBUG_BUILD
		#IF MISSION_FLOW_INITIALISER_DEBUG_OUTPUT
			PRINTINT(g_flowUnsaved.iNumStoredFlowCommands)
			PRINTSTRING(": REMOVE-PHONE-CONTACT [CONTACT=")
			PRINTSTRING(GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(paramCharacterContact))
			PRINTSTRING(" PHONE-OWNER=")
			PRINTSTRING(GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(paramCharacterPhoneOwner))
			PRINTSTRING("]")
			PRINTNL()
		#ENDIF
	#ENDIF
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// PURPOSE: Store 'Add New Friend Contact' command
// 
// INPUT PARAMS:	paramContact		The contact character being added to the Friend.
//					paramFriendOwner	The character who owns the Friend. (Must be a playable characters.)

PROC SETFLOW_ADD_NEW_FRIEND_CONTACT(enumCharacterList paramCharacterContact, enumCharacterList paramCharacterFriendOwner)

	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE_SMALL

	//Check the Friend owner parameter is valid.
	IF 	paramCharacterFriendOwner <> CHAR_MICHAEL
	AND paramCharacterFriendOwner <> CHAR_FRANKLIN
	AND paramCharacterFriendOwner <> CHAR_TREVOR
		SCRIPT_ASSERT("SetFlow_Add_New_Friend_Contact: Friend owner parameter was not a playable character. Failed to add flow command.")
		EXIT
	ENDIF
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_ADD_NEW_FRIEND_CONTACT
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	STORE_CORE_SMALL_STORAGE_TYPE_VARIABLES(g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
											ENUM_TO_INT(paramCharacterContact),
											ENUM_TO_INT(paramCharacterFriendOwner))
											
	// Debug Output
	#IF IS_DEBUG_BUILD
		#IF MISSION_FLOW_INITIALISER_DEBUG_OUTPUT
			PRINTINT(g_flowUnsaved.iNumStoredFlowCommands)
			PRINTSTRING(": FRIEND_PLAYER [CONTACT=")
			PRINTSTRING(GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(paramCharacterContact))
			PRINTSTRING(" FRIEND-OWNER=")
			PRINTSTRING(GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(paramCharacterFriendOwner))
			PRINTSTRING("]")
			PRINTNL()
		#ENDIF
	#ENDIF
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// PURPOSE: Store 'Block Friend Contact' command
// 
// INPUT PARAMS:	paramContact		The contact character being Blocked to the Friend.
//					paramFriendOwner	The character who owns the Friend. (Must be a playable characters.)

PROC SETFLOW_BLOCK_FRIEND_CONTACT(enumCharacterList paramCharacterContact, enumCharacterList paramCharacterFriendOwner, BOOL paramState, INT paramHashModifier = 0)

	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE
	
	//Check the Friend owner parameter is valid.
	IF 	paramCharacterFriendOwner <> CHAR_MICHAEL
	AND paramCharacterFriendOwner <> CHAR_FRANKLIN
	AND paramCharacterFriendOwner <> CHAR_TREVOR
	AND paramCharacterFriendOwner <> CHAR_LAMAR
		SCRIPT_ASSERT("SetFlow_Block_Friend_Contact: Friend owner parameter was not a playable character. Failed to Block flow command.")
		EXIT
	ENDIF
	
	INT iState = 0
	IF paramState
		iState = 1
	ENDIF
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_BLOCK_FRIEND_CONTACT
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	STORE_CORE_STORAGE_TYPE_VARIABLES(	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
										ENUM_TO_INT(paramCharacterFriendOwner),
										ENUM_TO_INT(paramCharacterContact),
										iState,
										paramHashModifier)
										
	// Debug Output
	#IF IS_DEBUG_BUILD
		#IF MISSION_FLOW_INITIALISER_DEBUG_OUTPUT
			PRINTINT(g_flowUnsaved.iNumStoredFlowCommands)
			PRINTSTRING(": FRIEND_PLAYER [CONTACT=")
			PRINTSTRING(GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(paramCharacterContact))
			PRINTSTRING(" FRIEND-OWNER=")
			PRINTSTRING(GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(paramCharacterFriendOwner))
			PRINTSTRING("]")
			PRINTNL()
		#ENDIF
	#ENDIF
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// PURPOSE: Store 'Block Friend Contact' command
// 
// INPUT PARAMS:	paramContact		The contact character being Blocked to the Friend.
//					paramFriendOwner	The character who owns the Friend. (Must be a playable characters.)

PROC SETFLOW_BLOCK_FRIEND_CLASH(enumCharacterList paramCharacterContact, enumCharacterList paramCharacterFriendOwner, BOOL paramState)

	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE
	
	//Check the Friend owner parameter is valid.
	IF 	paramCharacterFriendOwner <> CHAR_MICHAEL
	AND paramCharacterFriendOwner <> CHAR_FRANKLIN
	AND paramCharacterFriendOwner <> CHAR_TREVOR
	AND paramCharacterFriendOwner <> CHAR_LAMAR
		SCRIPT_ASSERT("SETFLOW_BLOCK_FRIEND_CLASH: Friend owner parameter was not a playable character. Failed to Block flow command.")
		EXIT
	ENDIF
	
	INT iState = 0
	IF paramState
		iState = 1
	ENDIF
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_BLOCK_FRIEND_CLASH
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	STORE_CORE_STORAGE_TYPE_VARIABLES(	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
											ENUM_TO_INT(paramCharacterFriendOwner),
											ENUM_TO_INT(paramCharacterContact),
											iState,
											0)									// Not Needed.
											
	// Debug Output
	#IF IS_DEBUG_BUILD
		#IF MISSION_FLOW_INITIALISER_DEBUG_OUTPUT
			PRINTINT(g_flowUnsaved.iNumStoredFlowCommands)
			PRINTSTRING(": FRIEND_PLAYER [CONTACT=")
			PRINTSTRING(GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(paramCharacterContact))
			PRINTSTRING(" FRIEND-OWNER=")
			PRINTSTRING(GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(paramCharacterFriendOwner))
			PRINTSTRING("]")
			PRINTNL()
		#ENDIF
	#ENDIF
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// PURPOSE: Store 'busy Friend Contact' command
// 
// INPUT PARAMS:	paramContact		The contact character being busyed to the Friend.
//					paramFriendOwner		The character who owns the Friend. (Must be a playable characters.)

PROC SETFLOW_FRIEND_CONTACT_BUSY(enumCharacterList paramCharacterContact, enumCharacterList paramCharacterFriendOwner, SP_MISSIONS paramMissionID)

	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE
	
	//Check the Friend owner parameter is valid.
	IF 	paramCharacterFriendOwner <> CHAR_MICHAEL
	AND paramCharacterFriendOwner <> CHAR_FRANKLIN
	AND paramCharacterFriendOwner <> CHAR_TREVOR
	AND paramCharacterFriendOwner <> CHAR_LAMAR
		SCRIPT_ASSERT("SETFLOW_FRIEND_CONTACT_BUSY: Friend owner parameter was not a friend character. Failed to busy flow command.")
		EXIT
	ENDIF
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_FRIEND_CONTACT_BUSY
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	STORE_CORE_STORAGE_TYPE_VARIABLES(	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
										ENUM_TO_INT(paramCharacterFriendOwner),
										ENUM_TO_INT(paramCharacterContact),
										ENUM_TO_INT(paramMissionID),
										0)								// Not Needed
											
	// Debug Output
	#IF IS_DEBUG_BUILD
		#IF MISSION_FLOW_INITIALISER_DEBUG_OUTPUT
			PRINTINT(g_flowUnsaved.iNumStoredFlowCommands)
			PRINTSTRING(": FRIEND_PLAYER [CONTACT=")
			PRINTSTRING(GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(paramCharacterContact))
			PRINTSTRING(" FRIEND-OWNER=")
			PRINTSTRING(GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(paramCharacterFriendOwner))
			PRINTSTRING("]")
			PRINTNL()
		#ENDIF
	#ENDIF
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Is Player Phoning NPC' command + variables
//
// INPUT PARAMS:	paramPlayerBitset			A bitset defining which player chars we are checking for making the call.
//					paramCharSheetNPC			The charSheet ID of the NPC contact who is to be called.

PROC SETFLOW_IS_PLAYER_PHONING_NPC(INT paramPlayerBitset, enumCharacterList paramCharSheetNPC, JUMP_LABEL_IDS paramTrueJump,  JUMP_LABEL_IDS paramFalseJump,  JUMP_LABEL_IDS paramDebugJump = STAY_ON_THIS_COMMAND, INT paramHashModifier = 0)

	FLOW_STORAGE_TYPES	thisStorageType = FST_COMMS

	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_IS_PLAYER_PHONING_NPC
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	STORE_COMMS_STORAGE_TYPE_VARIABLES (	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
											paramPlayerBitset,
											ENUM_TO_INT(paramCharSheetNPC),
											ENUM_TO_INT(paramTrueJump),
											ENUM_TO_INT(paramFalseJump),
											ENUM_TO_INT(paramDebugJump),
											paramHashModifier,
											0,								// Not Needed.
											0,								// Not Needed.
											VID_BLANK,						// Not Needed.
											CID_BLANK,						// Not Needed.
											FLOW_CHECK_NONE)				// Not Needed.
							
	// Debug Output
	#IF IS_DEBUG_BUILD
		#IF MISSION_FLOW_INITIALISER_DEBUG_OUTPUT
			PRINTLN(g_flowUnsaved.iNumStoredFlowCommands, ": IS_PLAYER_PHONING_NPC [M=",
			IS_BIT_SET(paramPlayerBitset, ENUM_TO_INT(CHAR_MICHAEL)),
			" F=", IS_BIT_SET(paramPlayerBitset, ENUM_TO_INT(CHAR_FRANKLIN)),
			" T=", IS_BIT_SET(paramPlayerBitset, ENUM_TO_INT(CHAR_TREVOR)),
			" NPC=", GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(paramCharSheetNPC),
			" TRUE-JUMP=", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramTrueJump),
			" FALSE-JUMP=", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramFalseJump),
			" DEBUG-JUMP=", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramDebugJump),
			"]")
		#ENDIF
	#ENDIF
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC



// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Player Phone NPC' command + variables
//
// INPUT PARAMS:	paramCharSheetFrom			The charSheet ID of the playable character who should make the outgoing call. (Must be set to a playable character or CHAR_BLANK_ENTRY if no character needs to be specified.)
//					paramCharSheetTo			The charSheet ID of the NPC contact who is to be called.
//					paramCommunicationType		The type of this communication.
//					paramPreCallDelay			The initial delay (msec) before the phonecall is attempted
//					paramRequeueDelay			The delay (msec) when requeuing the call if it fails to go through.
//					paramCommID 				The enum referencing which phonecall this is. String data stored in comms_control_data_GTA5.sch 
//					paramRestrictedArea			A vector ID defining an area in the world the player must not be for the call to trigger.
//					paramExecuteOnComplete		A code ID defining a block of script that will execute when the phonecall completes.
//					paramIsConverenceCall		Is this call a conference call?
//					paramFCBFlags				Settings flags for this communication defined with Flow Communication Bitset flags.

PROC SETFLOW_PLAYER_PHONE_NPC(enumCharacterList paramCharSheetFrom, enumCharacterList paramCharSheetTo, CC_CommunicationType paramCommunicationType, INT paramPreCallDelay, INT paramRequeueDelay, CC_CommID paramCommID, VectorID paramRestrictedArea = VID_BLANK, CC_CodeID paramExecuteOnComplete = CID_BLANK, FLOW_CHECK_IDS paramSendCheck = FLOW_CHECK_NONE, INT paramFCBFlags = 0)

	FLOW_STORAGE_TYPES	thisStorageType = FST_COMMS
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_PLAYER_PHONE_NPC
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	STORE_COMMS_STORAGE_TYPE_VARIABLES(	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
										ENUM_TO_INT(paramCommID),
										ENUM_TO_INT(paramCharSheetTo),
										ENUM_TO_INT(paramCharSheetFrom),
										ENUM_TO_INT(paramCommunicationType),					
										ENUM_TO_INT(paramPreCallDelay),
										ENUM_TO_INT(paramRequeueDelay),
										ENUM_TO_INT(paramFCBFlags),
										0,									// Not Needed.
										paramRestrictedArea,
										paramExecuteOnComplete,
										paramSendCheck)
											
	// Debug Output
	#IF IS_DEBUG_BUILD
		#IF MISSION_FLOW_INITIALISER_DEBUG_OUTPUT
			PRINTINT(g_flowUnsaved.iNumStoredFlowCommands)
			PRINTSTRING(": PLAYER_PHONE_NPC [CHAR-FROM=")
			PRINTSTRING(GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(paramCharSheetFrom))
			PRINTSTRING(" CHAR-TO=")
			PRINTSTRING(GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(paramCharSheetTo))
			PRINTSTRING(" COMMID=")
			PRINTSTRING(GET_COMM_ID_DEBUG_STRING(paramCommID))
			PRINTSTRING("]")
			PRINTNL()
		#ENDIF
	#ENDIF
	
	if IS_BIT_SET(paramFCBFlags,FLOW_COMM_BIT_TEXT_ON_MISS)
		SCRIPT_ASSERT("setflow_player_phone_npc: BIT_MISSED_TEXT shouldn't be applied to outgoing calls from the player.")
	endif
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Player Phone NPC with Branch' command + variables
//
// INPUT PARAMS:	paramCharSheetFrom			The charSheet ID of the playable character who should make the outgoing call. (Must be set to a playable character or CHAR_BLANK_ENTRY if no character needs to be specified.)
//					paramCharSheetTo			The charSheet ID of the NPC contact who is to be called.
//					paramCommunicationType		The type of this communication.
//					paramPreCallDelay			The initial delay (msec) before the phonecall is attempted
//					paramRequeueDelay			The delay (msec) when requeuing the call if it fails to go through.
//					paramCommID 				The enum referencing which phonecall this is. String data stored in comms_control_data_GTA5.sch
//					paramCommTrueID				The enum referencing which conversation should take place if the branch check returns TRUE.
//					paramCommFalseID			The enum referencing which conversation should take place if the branch check returns FALSE.
//					paramBranchCheck			The check to run to decide which branch to take.//					paramRestrictedArea			A vector ID defining an area in the world the player must not be for the call to trigger.
//					paramExecuteOnComplete		A code ID defining a block of script that will execute when the phonecall completes.
//					paramIsConverenceCall		Is this call a conference call?
//					paramFCBFlags				Settings flags for this communication defined with Flow Communication Bitset flags.

PROC SETFLOW_PLAYER_PHONE_NPC_WITH_BRANCH(enumCharacterList paramCharSheetFrom, enumCharacterList paramCharSheetTo, CC_CommunicationType paramCommunicationType, INT paramPreCallDelay, INT paramRequeueDelay, CC_CommID paramCommID, CC_CommID paramCommTrueID, CC_CommID paramCommFalseID, FLOW_CHECK_IDS paramBranchCheck, VectorID paramRestrictedArea = VID_BLANK, CC_CodeID paramExecuteOnComplete = CID_BLANK, INT paramFCBFlags = 0)

	FLOW_STORAGE_TYPES	thisStorageType = FST_COMMS_LARGE
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_PLAYER_PHONE_NPC_WITH_BRANCH
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	STORE_COMMS_LARGE_STORAGE_TYPE_VARIABLES(	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
												ENUM_TO_INT(paramCommID),
												ENUM_TO_INT(paramCommTrueID),
												ENUM_TO_INT(paramCommFalseID),
												ENUM_TO_INT(paramCharSheetTo),
												ENUM_TO_INT(paramCharSheetFrom),
												ENUM_TO_INT(paramCommunicationType),					
												ENUM_TO_INT(paramPreCallDelay),
												ENUM_TO_INT(paramRequeueDelay),
												ENUM_TO_INT(paramFCBFlags),
												STAY_ON_THIS_COMMAND,				//Not Needed.
												STAY_ON_THIS_COMMAND,				//Not Needed.
												STAY_ON_THIS_COMMAND,				//Not Needed.
												paramRestrictedArea,
												paramExecuteOnComplete,
												paramBranchCheck)
											
	// Debug Output
	#IF IS_DEBUG_BUILD
		#IF MISSION_FLOW_INITIALISER_DEBUG_OUTPUT
			PRINTINT(g_flowUnsaved.iNumStoredFlowCommands)
			PRINTSTRING(": PLAYER_PHONE_NPC_WITH_BRANCH")
			PRINTNL()
		#ENDIF
	#ENDIF
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Phone Player' command + variables
//
// INPUT PARAMS:	paramPlayerBitset			A bitset containing which of the three main characters this call should go to.
//					paramCharSheetFrom			The charSheet ID of the contact initiating the phonecall.
//					paramCommunicationType		The type of this communication.
//					paramPreCallDelay			The initial delay (msec) before the phonecall is attempted.
//					paramRequeueDelay			The delay (msec) when requeuing the call if it fails to go through.
//					paramCommID 				The enum referencing which phonecall this is. String data stored in comms_control_data_GTA5.sch 
//					paramRestrictedArea			A vector ID defining an area in the world the player must not be for the call to trigger.
//					paramExecuteOnComplete		A code ID defining a block of script that will execute when the phonecall completes.
//					paramFCBFlags				Settings flags for this communication defined with Flow Communication Bitset flags.

PROC SETFLOW_PHONE_PLAYER(INT paramPlayerBitset, enumCharacterList paramCharSheetFrom, CC_CommunicationType paramCommunicationType, INT paramPreCallDelay, INT paramRequeueDelay, CC_CommID paramCommID, CC_CommID paramMissedTextID, VectorID paramRestrictedArea = VID_BLANK, CC_CodeID paramExecuteOnComplete = CID_BLANK, FLOW_CHECK_IDS paramSendCheck = FLOW_CHECK_NONE, INT paramFCBFlags = 0)

	FLOW_STORAGE_TYPES	thisStorageType = FST_COMMS
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_PHONE_PLAYER
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	STORE_COMMS_STORAGE_TYPE_VARIABLES(	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
										ENUM_TO_INT(paramCommID),
										ENUM_TO_INT(paramMissedTextID),
										paramPlayerBitset,
										ENUM_TO_INT(paramCharSheetFrom),
										ENUM_TO_INT(paramCommunicationType),
										paramPreCallDelay,
										paramRequeueDelay,
										paramFCBFlags,
										paramRestrictedArea,
										paramExecuteOnComplete,
										paramSendCheck)
											
	// Debug Output
	#IF IS_DEBUG_BUILD
		#IF MISSION_FLOW_INITIALISER_DEBUG_OUTPUT
			PRINTINT(g_flowUnsaved.iNumStoredFlowCommands)
			PRINTSTRING(": PHONE_PLAYER [CHAR-TO=")
			//PRINTSTRING(GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET( paramCharSheetTo))
			PRINTINT(paramPlayerBitset)
			PRINTSTRING(" CHAR-FROM=")
			PRINTSTRING(GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(paramCharSheetFrom))
			PRINTSTRING(" TYPE=")
			PRINTSTRING(PRIVATE_GET_DEBUG_STRING_FOR_COMMUNICATION_TYPE(paramCommunicationType))
			PRINTSTRING(" PRECALLmsec=")
			PRINTINT(paramPreCallDelay)
			PRINTSTRING(" REQUEUEmsec=")
			PRINTINT(paramRequeueDelay)
			PRINTSTRING(" COMM.ID=")
			PRINTSTRING(GET_COMM_ID_DEBUG_STRING(paramCommID))
			PRINTSTRING(" MISSED.TEXT.ID=")
			PRINTSTRING(GET_COMM_ID_DEBUG_STRING(paramMissedTextID))
			PRINTSTRING(" VECTORID=")
			PRINTSTRING(GET_DEBUG_STRING_FOR_VECTOR_ID(paramRestrictedArea))
			PRINTSTRING(" CODEID=")
			PRINTSTRING(GET_DEBUG_STRING_FOR_COMMUNICATION_CODE_ID(paramExecuteOnComplete))
			PRINTSTRING(" CID-DELAY=")
			PRINTINT(ENUM_TO_INT(paramExecuteOnComplete))
			PRINTSTRING("]")
			PRINTNL()
		#ENDIF
	#ENDIF
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Phone Player with Branch' command + variables
//
// INPUT PARAMS:	paramPlayerBitset			A bitset containing which of the three main characters this call should go to.
//					paramCharSheetFrom			The charSheet ID of the contact initiating the phonecall.
//					paramCommunicationType		The type of this communication.
//					paramPreCallDelay			The initial delay (msec) before the phonecall is attempted.
//					paramRequeueDelay			The delay (msec) when requeuing the call if it fails to go through.
//					paramCommID 				The enum referencing which phonecall this is. String data stored in comms_control_data_GTA5.sch
//					paramCommTrueID				The enum referencing which conversation should take place if the branch check returns TRUE.
//					paramCommFalseID			The enum referencing which conversation should take place if the branch check returns FALSE.
//					paramBranchCheck			The check to run to decide which branch to take.
//					paramRestrictedArea			A vector ID defining an area in the world the player must not be for the call to trigger.
//					paramExecuteOnComplete		A code ID defining a block of script that will execute when the phonecall completes.
//					paramFCBFlags				Settings flags for this communication defined with Flow Communication Bitset flags.

PROC SETFLOW_PHONE_PLAYER_WITH_BRANCH(INT paramPlayerBitset, enumCharacterList paramCharSheetFrom, CC_CommunicationType paramCommunicationType, INT paramPreCallDelay, INT paramRequeueDelay, CC_CommID paramCommID, CC_CommID paramCommTrueID, CC_CommID paramCommFalseID, FLOW_CHECK_IDS paramBranchCheck, VectorID paramRestrictedArea = VID_BLANK, CC_CodeID paramExecuteOnComplete = CID_BLANK, INT paramFCBFlags = 0)

	FLOW_STORAGE_TYPES	thisStorageType = FST_COMMS_LARGE
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_PHONE_PLAYER_WITH_BRANCH
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	STORE_COMMS_LARGE_STORAGE_TYPE_VARIABLES(	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
												ENUM_TO_INT(paramCommID),
												ENUM_TO_INT(paramCommTrueID),
												ENUM_TO_INT(paramCommFalseID),
												paramPlayerBitset,
												ENUM_TO_INT(paramCharSheetFrom),
												ENUM_TO_INT(paramCommunicationType),
												paramPreCallDelay,
												paramRequeueDelay,
												paramFCBFlags,
												STAY_ON_THIS_COMMAND,				//Not Needed.
												STAY_ON_THIS_COMMAND,				//Not Needed.
												STAY_ON_THIS_COMMAND,				//Not Needed.
												paramRestrictedArea,
												paramExecuteOnComplete,
												paramBranchCheck)
											
	// Debug Output
	#IF IS_DEBUG_BUILD
		#IF MISSION_FLOW_INITIALISER_DEBUG_OUTPUT
			PRINTINT(g_flowUnsaved.iNumStoredFlowCommands)
			PRINTSTRING(": PHONE_PLAYER WITH BRANCH")
			PRINTNL()
		#ENDIF
	#ENDIF
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Phone Player With Jumps' command + variables
//
// INPUT PARAMS:	paramPlayerBitset			A bitset containing which of the three main characters this call should go to.
//					paramCharSheetFrom			The charSheet ID of the contact initiating the phonecall.
//					paramCommunicationType		The type of this communication.
//					paramPreCallDelay			The initial delay (msec) before the phonecall is attempted
//					paramRequeueDelay			The delay (msec) when requeuing the call if it fails to send initially.
//					paramCommID 				The enum referencing which phonecall this is. String data stored in comms_control_data_GTA5.sch 
//					paramAnsweredJump			The command to jump to if this call is answered.
//					paramUnansweredJump			The command to jump to if this call is left unanswered.
//					paramDebugJump				The command to jump to when debug skipping through the flow. Default will automatically jump to the "answered" label.
//					paramRestrictedArea			A vector ID defining an area in the world the player must not be for the call to trigger.
//					paramExecuteOnComplete		A code ID defining a block of script that will execute when the phonecall completes.
//					paramFCBFlags				Settings flags for this communication defined with Flow Communication Bitset flags.

PROC SETFLOW_PHONE_PLAYER_WITH_JUMPS(INT paramPlayerBitset, enumCharacterList paramCharSheetFrom, CC_CommunicationType paramCommunicationType, INT paramPreCallDelay, INT paramRequeueDelay, CC_CommID paramCommID, JUMP_LABEL_IDS paramAnsweredJump, JUMP_LABEL_IDS paramUnansweredJump, JUMP_LABEL_IDS paramDebugJump = STAY_ON_THIS_COMMAND, VectorID paramRestrictedArea = VID_BLANK, CC_CodeID paramExecuteOnComplete = CID_BLANK, FLOW_CHECK_IDS paramSendCheck = FLOW_CHECK_NONE, INT paramFCBFlags = 0)

	FLOW_STORAGE_TYPES	thisStorageType = FST_COMMS_LARGE
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_PHONE_PLAYER_WITH_JUMPS
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	STORE_COMMS_LARGE_STORAGE_TYPE_VARIABLES(	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
												ENUM_TO_INT(paramCommID),
												paramPlayerBitset,
												ENUM_TO_INT(paramCharSheetFrom),
												ENUM_TO_INT(paramCommunicationType),
												paramPreCallDelay,
												paramRequeueDelay,
												paramFCBFlags,
												0,						//Not Needed.
												0,						//Not Needed.
												paramAnsweredJump,
												paramUnansweredJump,
												paramDebugJump,
												paramRestrictedArea,
												paramExecuteOnComplete,
												paramSendCheck)
											
	// Debug Output
	#IF IS_DEBUG_BUILD
		#IF MISSION_FLOW_INITIALISER_DEBUG_OUTPUT
			PRINTINT(g_flowUnsaved.iNumStoredFlowCommands)
			PRINTSTRING(": PHONE_PLAYER [CHAR-TO=")
			//PRINTSTRING(GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(paramCharSheetTo))
			PRINTINT(paramPlayerBitset)
			PRINTSTRING(" CHAR-FROM=")
			PRINTSTRING(GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(paramCharSheetFrom))
			PRINTSTRING(" TYPE=")
			PRINTSTRING(PRIVATE_GET_DEBUG_STRING_FOR_COMMUNICATION_TYPE(paramCommunicationType))
			PRINTSTRING(" PRECALLmsec=")
			PRINTINT(paramPreCallDelay)
			PRINTSTRING(" REQUEUEmsec=")
			PRINTINT(paramRequeueDelay)
			PRINTSTRING(" COMM.ID=")
			PRINTSTRING(GET_COMM_ID_DEBUG_STRING(paramCommID))
			PRINTSTRING(" ANS-JUMP=")
			PRINTSTRING(GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramAnsweredJump))
			PRINTSTRING(" UNANS-JUMP=")
			PRINTSTRING(GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramUnansweredJump))
			PRINTSTRING(" DEBUG-JUMP=")
			PRINTSTRING(GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramDebugJump))
			PRINTSTRING(" VECTORID=")
			PRINTSTRING(GET_DEBUG_STRING_FOR_VECTOR_ID(paramRestrictedArea))
			PRINTSTRING(" CODEID=")
			PRINTSTRING(GET_DEBUG_STRING_FOR_COMMUNICATION_CODE_ID(paramExecuteOnComplete))
			PRINTSTRING("]")
			PRINTNL()
		#ENDIF
	#ENDIF
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Phone Player Yes No' command + variables
//
// INPUT PARAMS:	paramPlayerBitset			A bitset containing which of the three main characters this call should go to.
//					paramCharSheetFrom			The charSheet ID of the contact initiating the phonecall.
//					paramCommunicationType		The type of this communication.
//					paramPreCallDelay			The initial delay (msec) before the phonecall is attempted.
//					paramRequeueDelay			The delay (msec) when requeuing this call if it fails to go through initially.
//					paramCommID 				The enum referencing which question phonecall this is. String data stored in comms_control_data_GTA5.sch 
//					paramYesLabelID				The LabelID to jump to when the answer is Yes
//					paramNoLabelID				The LabelID to jump to when the answer is No
//					paramDebugLabelID			The LabelID to jump to when we're stepping through the flow in debug mode with the flow launcher.
//					paramRestrictedArea			A vector ID defining an area in the world the player must not be for the call to trigger.
//					paramExecuteOnComplete		A code ID defining a block of script that will execute when the phonecall completes.
//					paramFCBFlags				Settings flags for this communication defined with Flow Communication Bitset flags.

PROC SETFLOW_PHONE_PLAYER_YES_NO(INT paramToPlayerBitset, enumCharacterList paramCharSheetFrom, CC_CommunicationType paramCommunicationType, INT paramPreCallDelay, INT paramRequeueDelay, CC_CommID paramCommID, CC_QuestionCallResponse paramYesResp, CC_QuestionCallResponse paramNoResp, JUMP_LABEL_IDS paramYesLabelID, JUMP_LABEL_IDS paramNoLabelID, JUMP_LABEL_IDS paramDebugLabelID = STAY_ON_THIS_COMMAND, VectorID paramRestrictedArea = VID_BLANK, CC_CodeID paramExecuteOnComplete = CID_BLANK, FLOW_CHECK_IDS paramSendCheck = FLOW_CHECK_NONE, INT paramFCBFlags = 0)

	//If no specific debug jump is set use the yes jump by default.
	IF paramDebugLabelID = STAY_ON_THIS_COMMAND
		paramDebugLabelID = paramYesLabelID
	ENDIF
	
	FLOW_STORAGE_TYPES	thisStorageType = FST_COMMS_LARGE
	
	INT iSettings = paramFCBFlags
	SET_BIT(iSettings, FLOW_COMM_BIT_WAIT_UNTIL_SENT)	// Always wait until sent for calls with questions.
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_PHONE_PLAYER_YES_NO
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]


	STORE_COMMS_LARGE_STORAGE_TYPE_VARIABLES(	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
												ENUM_TO_INT(paramCommID),
												ENUM_TO_INT(paramYesResp),
												ENUM_TO_INT(paramNoResp),
												paramToPlayerBitset,
												ENUM_TO_INT(paramCharSheetFrom),
												ENUM_TO_INT(paramCommunicationType),
												paramPreCallDelay,
												paramRequeueDelay,
												iSettings,
												paramYesLabelID,
												paramNoLabelID,
												paramDebugLabelID,
												paramRestrictedArea,
												paramExecuteOnComplete,
												paramSendCheck)
											
	// Debug Output
	#IF IS_DEBUG_BUILD
		#IF MISSION_FLOW_INITIALISER_DEBUG_OUTPUT
			PRINTINT(g_flowUnsaved.iNumStoredFlowCommands)
			PRINTSTRING(": PHONE_PLAYER [CHAR-TO=")
			//PRINTSTRING(GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(paramCharSheetTo))
			PRINTINT(paramToPlayerBitset)
			PRINTSTRING(" CHAR-FROM=")
			PRINTSTRING(GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(paramCharSheetFrom))
			PRINTSTRING(" TYPE=")
			PRINTSTRING(PRIVATE_GET_DEBUG_STRING_FOR_COMMUNICATION_TYPE(paramCommunicationType))
			PRINTSTRING(" PRECALLmsec=")
			PRINTINT(paramPreCallDelay)
			PRINTSTRING(" REQUEUEmsec=")
			PRINTINT(paramRequeueDelay)
			PRINTSTRING(" COMM.ID=")
			PRINTSTRING(GET_COMM_ID_DEBUG_STRING(paramCommID))
			PRINTNL()
			PRINTSTRING(" YESGOTO=")
			PRINTSTRING(GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramYesLabelID))
			PRINTSTRING(" NOGOTO=")
			PRINTSTRING(GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramNoLabelID))
			PRINTSTRING(" DEBUGGOTO=")
			PRINTSTRING(GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramDebugLabelID))
			PRINTSTRING(" VECTORID=")
			PRINTSTRING(GET_DEBUG_STRING_FOR_VECTOR_ID(paramRestrictedArea))
			PRINTSTRING(" CODEID=")
			PRINTSTRING(GET_DEBUG_STRING_FOR_COMMUNICATION_CODE_ID(paramExecuteOnComplete))
			PRINTSTRING("]")
			PRINTNL()
		#ENDIF
	#ENDIF
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// PURPOSE: Store 'Text Player' command + variables
//
// INPUT PARAMS:	paramPlayerBitset			A bitset containing which of the three main characters this text should go to.
//					paramCharSheetFrom			The charSheet ID of the contact initiating the phonecall.
//					paramCommunicationType		The type of this communication.
//					paramCommID 				The enum referencing which text message this is. String data stored in comms_control_data_GTA5.sch 					
//					paramPreTextDelay			The initial delay (msec) before the text is attempted
//					paramRequeueDelay			The delay (msec) when requeueing this text message if it fails to send.
//					paramRestrictedArea			A vector ID defining an area in the world the player must not be for the call to trigger.
//					paramExecuteOnComplete		A code ID defining a block of script that will execute when the phonecall completes.	
//					paramFCBFlags				Settings flags for this communication defined with Flow Communication Bitset flags.

PROC SETFLOW_TEXT_PLAYER(INT paramToPlayerBitset, enumCharacterList paramCharSheetFrom, CC_CommunicationType paramCommunicationType, CC_CommID paramCommID, INT paramPreTextDelay, INT paramRequeueDelay, VectorID paramRestrictedArea = VID_BLANK, CC_CodeID paramExecuteOnComplete = CID_BLANK, FLOW_CHECK_IDS paramSendCheck = FLOW_CHECK_NONE, INT paramFCBFlags = 0, INT paramHashModifier = 0)

	FLOW_STORAGE_TYPES	thisStorageType = FST_COMMS
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_TEXT_PLAYER
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	STORE_COMMS_STORAGE_TYPE_VARIABLES(	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
										ENUM_TO_INT(paramCommID),
										paramToPlayerBitset,
										ENUM_TO_INT(paramCharSheetFrom),
										ENUM_TO_INT(paramCommunicationType),
										paramPreTextDelay,
										paramRequeueDelay,
										paramFCBFlags,
										paramHashModifier,
										paramRestrictedArea,
										paramExecuteOnComplete,
										paramSendCheck)
											
	// Debug Output
	#IF IS_DEBUG_BUILD
		#IF MISSION_FLOW_INITIALISER_DEBUG_OUTPUT
			PRINTINT(g_flowUnsaved.iNumStoredFlowCommands)
			PRINTSTRING(": TEXT_PLAYER [CHAR-TO=")
			//PRINTSTRING(GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(paramCharSheetTo))
			PRINTINT(paramToPlayerBitset)
			PRINTSTRING(" CHAR-FROM=")
			PRINTSTRING(GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(paramCharSheetFrom))
			PRINTSTRING(" TYPE=")
			PRINTSTRING(PRIVATE_GET_DEBUG_STRING_FOR_COMMUNICATION_TYPE(paramCommunicationType))
			PRINTSTRING(" COMM.ID=")
			PRINTSTRING(GET_COMM_ID_DEBUG_STRING(paramCommID))
			PRINTSTRING(" PRETEXTmsec=")
			PRINTINT(paramPreTextDelay)
			PRINTSTRING(" REQUEUETmsec=")
			PRINTINT(paramRequeueDelay)
			PRINTSTRING(" VECTORID=")
			PRINTSTRING(GET_DEBUG_STRING_FOR_VECTOR_ID(paramRestrictedArea))
			PRINTSTRING(" CODEID=")
			PRINTSTRING(GET_DEBUG_STRING_FOR_COMMUNICATION_CODE_ID(paramExecuteOnComplete))
			PRINTSTRING("]")
			PRINTNL()
		#ENDIF
	#ENDIF
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// PURPOSE: Store 'Email Player' command + variables
//
// INPUT PARAMS:	paramPlayerBitset			A bitset containing which of the three main characters this email should go to.
//					paramCharSheetFrom			The charSheet ID of the contact initiating the email. Set to CHAR_BLANK_ENTRY if the email does not have a sender with a charsheet enum.
//					paramCommunicationType		The type of this communication.
//					paramCommID 				The enum referencing which email message this is. String data stored in comms_control_data_GTA5.sch 
//					paramEmailThread			The email thread that should be progressed when this email sends.
//					paramQueueTime				The initial delay (msec) before the email is attempted to be sent.
//					paramRequeueTime			The initial delay (msec) before the email is attempted to be sent.
//					paramRestrictedArea			A vector ID defining an area in the world the player must not be for the email to trigger.
//					paramExecuteOnComplete		A code ID defining a block of script that will execute when the email completes.	
//					paramFCBFlags				Settings flags for this communication defined with Flow Communication Bitset flags.

PROC SETFLOW_EMAIL_PLAYER(INT paramToPlayerBitset, enumCharacterList paramCharSheetFrom, CC_CommunicationType paramCommunicationType, CC_CommID paramCommID, INT paramQueueTime, INT paramRequeueTime, VectorID paramRestrictedArea = VID_BLANK, CC_CodeID paramExecuteOnComplete = CID_BLANK, FLOW_CHECK_IDS paramSendCheck = FLOW_CHECK_NONE, INT paramFCBFlags = 0)

	FLOW_STORAGE_TYPES	thisStorageType = FST_COMMS
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_EMAIL_PLAYER
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	STORE_COMMS_STORAGE_TYPE_VARIABLES(	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
										ENUM_TO_INT(paramCommID),
										paramToPlayerBitset,
										ENUM_TO_INT(paramCharSheetFrom),
										ENUM_TO_INT(paramCommunicationType),
										paramQueueTime,
										paramRequeueTime,
										paramFCBFlags,
										0,								// Not Needed.
										paramRestrictedArea,
										paramExecuteOnComplete,
										paramSendCheck)
											
	// Debug Output
	#IF IS_DEBUG_BUILD
		#IF MISSION_FLOW_INITIALISER_DEBUG_OUTPUT
			PRINTINT(g_flowUnsaved.iNumStoredFlowCommands)
			PRINTSTRING(": EMAIL_PLAYER")
			PRINTNL()
		#ENDIF
	#ENDIF
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// PURPOSE: Store 'Cancel Phonecall' command + variables
//
// INPUT PARAMS:	paramCommID		The enum referencing which phonecall this is. String data stored in comms_control_data_GTA5.sch 

PROC SETFLOW_CANCEL_PHONECALL(CC_CommID paramCommID)

	FLOW_STORAGE_TYPES	thisStorageType = FST_INDEX_DIRECT
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_CANCEL_PHONECALL
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= ENUM_TO_INT(paramCommID)  // NB. Store call ID directly in the index.
	
									
	// Debug Output
	#IF IS_DEBUG_BUILD
		#IF MISSION_FLOW_INITIALISER_DEBUG_OUTPUT
			PRINTINT(g_flowUnsaved.iNumStoredFlowCommands)
			PRINTSTRING(": SETFLOW_CANCEL_PHONECALL [COMM.ID=")
			PRINTSTRING(GET_COMM_ID_DEBUG_STRING(paramCommID))
			PRINTSTRING("]\n")
		#ENDIF
	#ENDIF
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// PURPOSE: Store 'Cancel Phonecall' command + variables
//
// INPUT PARAMS:	paramCommID 	The enum referencing which phonecall this is. String data stored in comms_control_data_GTA5.sch 

PROC SETFLOW_CANCEL_TEXT(CC_CommID paramCommID)

	FLOW_STORAGE_TYPES	thisStorageType = FST_INDEX_DIRECT

	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_CANCEL_PHONECALL
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= ENUM_TO_INT(paramCommID)  // NB. Store call ID directly in the index.
	
									
	// Debug Output
	#IF IS_DEBUG_BUILD
		#IF MISSION_FLOW_INITIALISER_DEBUG_OUTPUT
			PRINTINT(g_flowUnsaved.iNumStoredFlowCommands)
			PRINTSTRING(": SETFLOW_CANCEL_TEXT [COMM.ID=")
			PRINTSTRING(GET_COMM_ID_DEBUG_STRING(paramCommID))
			PRINTSTRING("]\n")
		#ENDIF
	#ENDIF
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Set Char Chat Phonecall' command + variables
//
// INPUT PARAMS:	paramCharSheetNPC				The charSheet ID of the NPC character who will need to phoned to trigger the chat call. 
//					paramCharSheetActivatingPlayer	The charSheet ID of the playable character who can initiate the chat call. (Must be set to a playable character or CHAR_BLANK_ENTRY if no character needs to be specified.)
//					paramBlockID					The phonecall Block ID
//					paramConversationID				The phonecall conversation ID

PROC SETFLOW_SET_CHAR_CHAT_PHONECALL(enumCharacterList paramCharSheetNPC, INT paramTriggerPlayerBitset, CC_CommID paramCommID, FLOW_CHECK_IDS paramSendCheck = FLOW_CHECK_NONE)

	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE

	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_SET_CHAR_CHAT_PHONECALL
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
						
	STORE_CORE_STORAGE_TYPE_VARIABLES(	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
										ENUM_TO_INT(paramCommID),
										paramTriggerPlayerBitset,
										ENUM_TO_INT(paramCharSheetNPC),
										ENUM_TO_INT(paramSendCheck))
											
	// Debug Output
	#IF IS_DEBUG_BUILD
		#IF MISSION_FLOW_INITIALISER_DEBUG_OUTPUT
			PRINTINT(g_flowUnsaved.iNumStoredFlowCommands)
			PRINTSTRING	(": SET_CHAR_CHAT_PHONECALL [TO-NPC-CHAR=")
			PRINTSTRING	(GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(paramCharSheetNPC))
			PRINTSTRING	(" TRIGGER-CHAR-BITSET=")
			PRINTINT	(paramTriggerPlayerBitset)
			PRINTSTRING	(" BLOCKID=")
			//PRINTSTRING	(paramBlockID)
			PRINTSTRING	(" CONV.ID=")
			PRINTINT	(ENUM_TO_INT( paramCommID))
			PRINTSTRING (" SEND.CHECK=")
			PRINTSTRING	(GET_DEBUG_STRING_FOR_FLOW_CHECK_ID(paramSendCheck))
			PRINTSTRING	("]")
			PRINTNL()
		#ENDIF
	#ENDIF
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC



// ===========================================================================================================
//		Command Storage
// ===========================================================================================================


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Add Organiser Event' command + variables
//
// INPUT PARAMS:	paramEventType		- The type of event to add.
//					paramHourOfDay		- The hour of the day this event starts on (24 hour clock).
//					paramLengthInHours	- The length of this event in hours.
//					paramTomorrow		- Should this event be added for tomorrow. If FALSE the event will be added for the same day.

PROC SETFLOW_ADD_ORGANISER_EVENT(ORGANISER_EVENT_ENUM paramEventType, INT paramStartHour, INT paramLengthInHours, BOOL paramTomorrow)

	FLOW_STORAGE_TYPES	thisStorageType 	= FST_CORE
	
	Start_Flow_Command(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_ADD_ORGANISER_EVENT
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	//Convert paramTomorrow to an INT.
	INT iTomorrow = 0
	IF paramTomorrow
		iTomorrow = 1
	ENDIF
	
	STORE_CORE_STORAGE_TYPE_VARIABLES(	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],											
										ENUM_TO_INT(paramEventType),
										paramStartHour,
										paramLengthInHours,
										iTomorrow)
						
	// Debug Output
	#IF IS_DEBUG_BUILD
		#IF MISSION_FLOW_INITIALISER_DEBUG_OUTPUT
			//INT arrayPos = g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index
			PRINTINT(g_flowUnsaved.iNumStoredFlowCommands)
			PRINTSTRING(": ADD_ORGANISER_EVENT [TYPE=")
			PRINTINT(ENUM_TO_INT(paramEventType))
			PRINTSTRING(" HOUR=")
			PRINTINT(ENUM_TO_INT(paramStartHour))
			PRINTSTRING(" LENGTH=")
			PRINTINT(paramLengthInHours)
			PRINTSTRING(" TOMORROW=")
			PRINTBOOL(paramTomorrow)
			PRINTSTRING("]")
			PRINTNL()
		#ENDIF
	#ENDIF
	
	Stop_Flow_Command(thisStorageType)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Do Board Interaction' command + variables
//
// INPUT PARAMS:	paramHeist			The heist index of the heist that the planning board belongs to.
//					paramInteraction	The type of interaction to enable for the heist board.
//					paramDisplayGroup	The board display group to turn on when this interaction is completed.
//					paramCompleteJump	The command to move to once the board interaction has been completed.
//					paramBackOutJump	The command to move to if the player backs out of this board interaction.
//					paramLockExit		Do we want to disable the player from being able to quit out of the board view until the interaction is completed?
//					paramViewWhenDone	Should we activate viewing mode when the interaction is complete?

PROC SETFLOW_DO_BOARD_INTERACTION(INT paramHeist, g_eBoardInteractions paramInteraction, g_eBoardDisplayGroups paramDisplayGroup, JUMP_LABEL_IDS paramCompleteJump, JUMP_LABEL_IDS paramBackOutJump, BOOL paramLockExit = FALSE, BOOL paramViewWhenDone = FALSE)

	//Check the heist index is valid.
	IF paramHeist < 0 OR paramHeist >= NO_HEISTS
		SCRIPT_ASSERT("SetFlow_Do_Planning_Board_Interaction: Invalid heist index passed.")
	ENDIF
	
	//Check the interaction enum is valid.
	IF paramInteraction = INTERACT_MAX OR paramInteraction = INTERACT_NONE
		SCRIPT_ASSERT("SetFlow_Do_Planning_Board_Interaction: Invalid interaction enum passed.")
	ENDIF
	
	FLOW_STORAGE_TYPES	thisStorageType 	= FST_COMMS

	INT iSettings = 0
	IF paramLockExit		SET_BIT(iSettings, FLOW_BOARD_BIT_LOCK_EXIT) 		ENDIF
	IF paramViewWhenDone	SET_BIT(iSettings, FLOW_BOARD_BIT_VIEW_WHEN_DONE)  	ENDIF
	
	Start_Flow_Command(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_DO_BOARD_INTERACTION
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	STORE_COMMS_STORAGE_TYPE_VARIABLES(	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
										paramHeist,
										ENUM_TO_INT(paramInteraction),
										ENUM_TO_INT(paramDisplayGroup),
										ENUM_TO_INT(paramCompleteJump),
										ENUM_TO_INT(paramBackOutJump),
										iSettings,
										0,								//Not Needed.
										0,								//Not Needed.
										VID_BLANK,						//Not Needed.
										CID_BLANK,						//Not Needed.
										FLOW_CHECK_NONE)				//Not Needed.
											
	// Debug Output
	#IF IS_DEBUG_BUILD
		#IF MISSION_FLOW_INITIALISER_DEBUG_OUTPUT
			//INT arrayPos = g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index
			PRINTINT(g_flowUnsaved.iNumStoredFlowCommands)
			PRINTSTRING(": DO_PLANNING_BOARD_INTERACTION [HEIST=")
			PRINTINT(paramHeist)
			PRINTSTRING(" INTERACT=")
			PRINTSTRING(Get_Interaction_Display_String_From_Interaction_ID(paramInteraction))
			PRINTSTRING(" DISPLAYGROUP=")
			PRINTINT(ENUM_TO_INT(paramDisplayGroup))
			PRINTSTRING(" COMPLETEJUMP=")
			PRINTSTRING(Get_Label_Display_String_From_Label_ID(paramCompleteJump))
			PRINTSTRING(" BACKOUTJUMP=")
			PRINTSTRING(Get_Label_Display_String_From_Label_ID(paramBackOutJump))
			PRINTSTRING(" LOCKEXIT=")
			PRINTBOOL(paramLockExit)
			PRINTSTRING(" VIEWWHENDONE=")
			PRINTBOOL(paramViewWhenDone)
			PRINTSTRING("]")
			PRINTNL()
		#ENDIF
	#ENDIF
	
	Stop_Flow_Command(thisStorageType)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Is Mission Completed' command
// 
// INPUT PARAMS:	paramMissionID		The enum of the SP mission we are checking for completion.
//					paramTrueJump		The label to jump to if the mission is completed.
//					paramFalseJump		The label to jump to if the mission is not completed.
//					paramDebugJump		The label to jump to when debug skipping through the flow.

PROC SETFLOW_IS_MISSION_COMPLETED(SP_MISSIONS paramMissionID, JUMP_LABEL_IDS paramTrueJump, JUMP_LABEL_IDS paramFalseJump, JUMP_LABEL_IDS paramDebugJump = STAY_ON_THIS_COMMAND)

	//If no specific debug jump is set, use the true jump by default.
	IF paramDebugJump = STAY_ON_THIS_COMMAND
		paramDebugJump = paramTrueJump
	ENDIF
	
	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE

	Start_Flow_Command(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_IS_MISSION_COMPLETED
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
		
	STORE_CORE_STORAGE_TYPE_VARIABLES(	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
										ENUM_TO_INT(paramMissionID),
										ENUM_TO_INT(paramTrueJump),
										ENUM_TO_INT(paramFalseJump),
										ENUM_TO_INT(paramDebugJump))
										
	// Debug Output
	#IF IS_DEBUG_BUILD
		#IF MISSION_FLOW_INITIALISER_DEBUG_OUTPUT
			PRINTINT(g_flowUnsaved.iNumStoredFlowCommands)
			PRINTSTRING(": IS MISSION COMPLETED [FILE=")
			PRINTINT 	(ENUM_TO_INT( paramMissionID))
			PRINTSTRING(" TRUEGOTO=")
			PRINTSTRING(Get_Label_Display_String_From_Label_ID(paramTrueJump))
			PRINTSTRING(" FALSEGOTO=")
			PRINTSTRING(Get_Label_Display_String_From_Label_ID(paramFalseJump))
			PRINTSTRING(" DEBUGGOTO=")
			PRINTSTRING(Get_Label_Display_String_From_Label_ID(paramDebugJump))
			PRINTSTRING("]")
			PRINTNL()
		#ENDIF
	#ENDIF
	
	Stop_Flow_Command(thisStorageType)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Is Playing As Character' command
// 
// INPUT PARAMS:	paramCharacter		The character that we're checking to see if the player is currently playing as.
//					paramTrueJump		The label to jump to if the player is playing as the character.
//					paramFalseJump		The label to jump to if the player is not playing as the character.
//					paramDebugJump		The label to jump to when debug skipping through the flow.

PROC SETFLOW_IS_PLAYING_AS_CHARACTER(enumCharacterList paramCharacter, JUMP_LABEL_IDS paramTrueJump, JUMP_LABEL_IDS paramFalseJump, JUMP_LABEL_IDS paramDebugJump = STAY_ON_THIS_COMMAND)

	//If no specific debug jump is set, use the true jump by default.
	IF paramDebugJump = STAY_ON_THIS_COMMAND
		paramDebugJump = paramTrueJump
	ENDIF
	
	//Check the character is playable.
	IF 	paramCharacter <> CHAR_MICHAEL
	AND paramCharacter <> CHAR_FRANKLIN
	AND paramCharacter <> CHAR_TREVOR
		SCRIPT_ASSERT("SETFLOW_IS_PLAYING_AS_CHARACTER: The character being checked is not playable. This will always return false.")
	ENDIF
	
	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE
	
	Start_Flow_Command(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_IS_PLAYING_AS_CHARACTER
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
		
	STORE_CORE_STORAGE_TYPE_VARIABLES(	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
										ENUM_TO_INT(paramCharacter),
										ENUM_TO_INT(paramTrueJump),
										ENUM_TO_INT(paramFalseJump),
										ENUM_TO_INT(paramDebugJump))
											
	// Debug Output
	#IF IS_DEBUG_BUILD
		#IF MISSION_FLOW_INITIALISER_DEBUG_OUTPUT
			PRINTINT(g_flowUnsaved.iNumStoredFlowCommands)
			PRINTSTRING(": IS PLAYING AS CHARACTER [CHAR=")
			PRINTSTRING(Get_CharSheet_Display_String_From_CharSheet(paramCharacter))
			PRINTSTRING(" TRUEGOTO=")
			PRINTSTRING(Get_Label_Display_String_From_Label_ID(paramTrueJump))
			PRINTSTRING(" FALSEGOTO=")
			PRINTSTRING(Get_Label_Display_String_From_Label_ID(paramFalseJump))
			PRINTSTRING(" DEBUGGOTO=")
			PRINTSTRING(Get_Label_Display_String_From_Label_ID(paramDebugJump))
			PRINTSTRING("]")
			PRINTNL()
		#ENDIF
	#ENDIF
	
	Stop_Flow_Command(thisStorageType)

ENDPROC



// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Is Switching To Character' command
// 
// INPUT PARAMS:	paramCharacter		The character that we're checking to see if the player is currently switching to.
//					paramTrueJump		The label to jump to if the player is playing as the character.
//					paramFalseJump		The label to jump to if the player is not playing as the character.
//					paramDebugJump		The label to jump to when debug skipping through the flow.

PROC SETFLOW_IS_SWITCHING_TO_CHARACTER(enumCharacterList paramCharacter, JUMP_LABEL_IDS paramTrueJump, JUMP_LABEL_IDS paramFalseJump, JUMP_LABEL_IDS paramDebugJump = STAY_ON_THIS_COMMAND)

	//If no specific debug jump is set, use the true jump by default.
	IF paramDebugJump = STAY_ON_THIS_COMMAND
		paramDebugJump = paramTrueJump
	ENDIF
	
	//Check the character is playable.
	IF 	paramCharacter <> CHAR_MICHAEL
	AND paramCharacter <> CHAR_FRANKLIN
	AND paramCharacter <> CHAR_TREVOR
		SCRIPT_ASSERT("SETFLOW_IS_SWITCHING_TO_CHARACTER: The character being checked is not playable. This will always return false.")
	ENDIF
	
	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE
	
	Start_Flow_Command(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_IS_SWITCHING_TO_CHARACTER
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
		
	STORE_CORE_STORAGE_TYPE_VARIABLES(	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
											ENUM_TO_INT(paramCharacter),
											ENUM_TO_INT(paramTrueJump),
											ENUM_TO_INT(paramFalseJump),
											ENUM_TO_INT(paramDebugJump))
											
	// Debug Output
	#IF IS_DEBUG_BUILD
		#IF MISSION_FLOW_INITIALISER_DEBUG_OUTPUT
			PRINTINT(g_flowUnsaved.iNumStoredFlowCommands)
			PRINTSTRING(": IS SWITCHING TO CHARACTER [CHAR=")
			PRINTSTRING(Get_CharSheet_Display_String_From_CharSheet(paramCharacter))
			PRINTSTRING(" TRUEGOTO=")
			PRINTSTRING(Get_Label_Display_String_From_Label_ID(paramTrueJump))
			PRINTSTRING(" FALSEGOTO=")
			PRINTSTRING(Get_Label_Display_String_From_Label_ID(paramFalseJump))
			PRINTSTRING(" DEBUGGOTO=")
			PRINTSTRING(Get_Label_Display_String_From_Label_ID(paramDebugJump))
			PRINTSTRING("]")
			PRINTNL()
		#ENDIF
	#ENDIF
	
	Stop_Flow_Command(thisStorageType)

ENDPROC



// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Set Door State' command
// 
// INPUT PARAMS:	paramDoorID				An enum representing the door that will have its state changed.
//					paramStateID			An enum representing the state that the door will be changed to.

PROC SETFLOW_SET_DOOR_STATE(DOOR_NAME_ENUM paramDoorID, DOOR_STATE_ENUM paramStateID, INT paramHashModifier = 0)

	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE_SMALL
	
	Start_Flow_Command(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_SET_DOOR_STATE
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	// Convert the state enum to a BOOL.
	INT iState = 0
	IF paramStateID = DOORSTATE_UNLOCKED // 1 is unlocked
		iState = 1
	ENDIF
	
	iState += (paramHashModifier*FLOW_COMMAND_HASH_MODIFIER_MAGNITUDE)
	
	STORE_CORE_SMALL_STORAGE_TYPE_VARIABLES(	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
												ENUM_TO_INT(paramDoorID),
												iState)
											
	// Debug Output
	#IF IS_DEBUG_BUILD
		#IF MISSION_FLOW_INITIALISER_DEBUG_OUTPUT
			PRINTINT(g_flowUnsaved.iNumStoredFlowCommands)
			PRINTSTRING(": SET DOOR STATE [DOOR=")
			PRINTINT(ENUM_TO_INT(paramDoorID))
			PRINTSTRING(" STATE=")
			//PRINTSTRING(Get_Bool_Display_String_From_Bool(theState))
			PRINTINT(ENUM_TO_INT (paramStateID))
			PRINTSTRING("]")
			PRINTNL()
		#ENDIF
	#ENDIF
	
	Stop_Flow_Command(thisStorageType)

ENDPROC

// PURPOSE: Reset priority switch scenes command
// 
// INPUT PARAMS:	paramScene			The Scene character being Seted to the Request.
//					paramRequestOwner	The character who owns the Request. (Must be a playable characters.)

PROC SETFLOW_RESET_PRIORITY_SWITCH_SCENES(enumCharacterList paramCharacter)

	FLOW_STORAGE_TYPES	thisStorageType	= FST_INDEX_DIRECT
	
	//Check the Request owner parameter is valid.
	IF 	paramCharacter <> CHAR_MICHAEL
	AND paramCharacter <> CHAR_FRANKLIN
	AND paramCharacter <> CHAR_TREVOR
		SCRIPT_ASSERT("SETFLOW_RESET_PRIORITY_SWITCH_SCENES: Request owner parameter was not a playable character. Failed to Set flow command.")
		EXIT
	ENDIF
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_RESET_PRIORITY_SWITCH_SCENES
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= ENUM_TO_INT(paramCharacter) 
	//NB. Optimisation. Storing character data directly into index.
	
	// Debug Output
	#IF IS_DEBUG_BUILD
		#IF MISSION_FLOW_INITIALISER_DEBUG_OUTPUT
			PRINTINT(g_flowUnsaved.iNumStoredFlowCommands)
			PRINTSTRING(": RESET_PRIORITY_SWITCH_SCENES [Character=")
			PRINTSTRING(GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(paramCharacter))
			PRINTSTRING("]")
			PRINTNL()
		#ENDIF
	#ENDIF
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// PURPOSE: Store 'Set Ped Request Scene' command
// 
// INPUT PARAMS:	paramScene		The Scene character being Seted to the Request.
//					paramRequestOwner	The character who owns the Request. (Must be a playable characters.)

PROC SETFLOW_SET_PED_REQUEST_SCENE(enumCharacterList paramCharacter, PED_REQUEST_SCENE_ENUM paramReqScene)

	FLOW_STORAGE_TYPES	thisStorageType	= FST_CORE_SMALL
	
	//Check the Request owner parameter is valid.
	IF 	paramCharacter <> CHAR_MICHAEL
	AND paramCharacter <> CHAR_FRANKLIN
	AND paramCharacter <> CHAR_TREVOR
		SCRIPT_ASSERT("SETFLOW_SET_PED_REQUEST_SCENE: Request owner parameter was not a playable character. Failed to Set flow command.")
		EXIT
	ENDIF
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_SET_PED_REQUEST_SCENE
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	STORE_CORE_SMALL_STORAGE_TYPE_VARIABLES(g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
											ENUM_TO_INT(paramReqScene),
											ENUM_TO_INT(paramCharacter))
											
	// Debug Output
	#IF IS_DEBUG_BUILD
		#IF MISSION_FLOW_INITIALISER_DEBUG_OUTPUT
			PRINTINT(g_flowUnsaved.iNumStoredFlowCommands)
			PRINTSTRING(": Request_PLAYER [Scene=")
			//PRINTSTRING(GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(paramCharacterScene))
			PRINTINT(ENUM_TO_INT(paramCharacter))
			PRINTSTRING(" Request-OWNER=")
			//PRINTSTRING(GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(paramCharacterRequestOwner))
			PRINTINT(ENUM_TO_INT(paramReqScene))
			PRINTSTRING("]")
			PRINTNL()
		#ENDIF
	#ENDIF
	
	STOP_FLOW_COMMAND(thisStorageType)
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Set Char Hotswap Availability' command
// 
// INPUT PARAMS:	paramCharacter			The playable character who's hotswap availability is to be set.
//					paramState				The new hotswap availability state.

PROC SETFLOW_SET_CHAR_HOTSWAP_AVAILABILITY(enumCharacterList paramCharacter, BOOL paramState, INT paramHashModifier = 0)

	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE_SMALL
	
	INT iState = 0
	IF paramState
		iState = 1
	ENDIF
	iState += (paramHashModifier*FLOW_COMMAND_HASH_MODIFIER_MAGNITUDE)

	Start_Flow_Command(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_SET_CHAR_HOTSWAP_AVAILABILITY
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	//Ensure the character is playable.
	IF 	paramCharacter <> CHAR_MICHAEL
	AND paramCharacter <> CHAR_FRANKLIN
	AND paramCharacter <> CHAR_TREVOR
		SCRIPT_ASSERT("SETFLOW_SET_CHAR_HOTSWAP_AVAILABILITY: Illegal character ID passed. The character was not a playable character. Tell BenR.")
	ENDIF
	
	STORE_CORE_SMALL_STORAGE_TYPE_VARIABLES(g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
											ENUM_TO_INT(paramCharacter),
											iState)

	// Debug Output
	#IF IS_DEBUG_BUILD
		#IF MISSION_FLOW_INITIALISER_DEBUG_OUTPUT
			PRINTINT(g_flowUnsaved.iNumStoredFlowCommands)
			PRINTSTRING(": SET CHAR HOTSWAP AVAILABILITY [CHAR=")
			PRINTSTRING(Get_CharSheet_Display_String_From_CharSheet(paramCharacter))
			PRINTSTRING(" STATE=")
			PRINTSTRING(Get_Bool_Display_String_From_Bool(paramState))
			PRINTSTRING("]")
			PRINTNL()
		#ENDIF
	#ENDIF
	
	Stop_Flow_Command(thisStorageType)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Set Player Character' command
// 
// INPUT PARAMS:	paramPlayableCharacter		The character sheet ENUM of the character the player should be swapped to.

PROC SETFLOW_SET_PLAYER_CHARACTER(enumCharacterList paramPlayableCharacter)

	//Check the passed character ENUM correspondes to a playable character.
	IF NOT IS_PLAYER_PED_PLAYABLE(paramPlayableCharacter)
		SCRIPT_ASSERT("SetFlow_Set_Hotswap_Stage_Flag: Tried to setup a player swap to a character that is not playable! Tell BenR.")
	ENDIF
	
	FLOW_STORAGE_TYPES	thisStorageType = FST_INDEX_DIRECT
	
	Start_Flow_Command(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_SET_PLAYER_CHARACTER
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= ENUM_TO_INT(paramPlayableCharacter) //Directly store character enum in index.
							
	// Debug Output
	#IF IS_DEBUG_BUILD
		#IF MISSION_FLOW_INITIALISER_DEBUG_OUTPUT
			PRINTINT(g_flowUnsaved.iNumStoredFlowCommands)
			PRINTSTRING(": SET PLAYER CHARACTER [CHAR=")
			PRINTSTRING(Get_CharSheet_Display_String_From_CharSheet(paramPlayableCharacter))
			PRINTSTRING("]")
			PRINTNL()
		#ENDIF
	#ENDIF
	
	Stop_Flow_Command(thisStorageType)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Set Savehouse State' command
// 
// INPUT PARAMS:	paramSavehouseID		The shop ID
//					paramState				The new savehouse active state: TRUE (1) or FALSE (0)
//
// NOTES:	The shop enum will be converted to and stored as an INT

PROC SETFLOW_SET_SAVEHOUSE_STATE(SAVEHOUSE_NAME_ENUM paramSavehouseID, BOOL paramAvailable, BOOL paramLongRangeBlip)

	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE_SMALL
	
	Start_Flow_Command(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_SET_SAVEHOUSE_STATE
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	INT iSettings
	IF paramAvailable		SET_BIT(iSettings, REPAWN_FLAG_AVAILABLE_BIT)			ENDIF
	IF paramLongRangeBlip	SET_BIT(iSettings, REPAWN_FLAG_LONG_RANGE_BLIP_BIT)		ENDIF
	
	STORE_CORE_SMALL_STORAGE_TYPE_VARIABLES(g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
											ENUM_TO_INT(paramSavehouseID),
											iSettings)
											
	// Debug Output
	#IF IS_DEBUG_BUILD
		#IF MISSION_FLOW_INITIALISER_DEBUG_OUTPUT
			PRINTINT(g_flowUnsaved.iNumStoredFlowCommands)
			PRINTSTRING(": SET SAVEHOUSE STATE [SAVEHOUSE=")
			PRINTSTRING(Get_Savehouse_Respawn_Name(paramSavehouseID))
			PRINTSTRING(" STATE=")
			PRINTSTRING(Get_Bool_Display_String_From_Bool(paramAvailable))
			PRINTSTRING("]")
			PRINTNL()
		#ENDIF
	#ENDIF
	
	Stop_Flow_Command(thisStorageType)

ENDPROC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Set Shop State' command
// 
// INPUT PARAMS:	paramShopID				The shop ID
//					paramState				The new shop active state: TRUE (1) or FALSE (0)
//
// NOTES:	The shop enum will be converted to and stored as an INT

PROC SETFLOW_SET_SHOP_STATE(SHOP_NAME_ENUM paramShopID, BOOL paramState)

	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE_SMALL
	
	Start_Flow_Command(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_SET_SHOP_STATE
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	INT iState = 0
	IF paramState
		iState = 1
	ENDIF
	
	STORE_CORE_SMALL_STORAGE_TYPE_VARIABLES(g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
											ENUM_TO_INT(paramShopID),
											iState)
											
	// Debug Output
	#IF IS_DEBUG_BUILD
		#IF MISSION_FLOW_INITIALISER_DEBUG_OUTPUT
			PRINTINT(g_flowUnsaved.iNumStoredFlowCommands)
			PRINTSTRING(": SET SHOP STATE [SHOP=")
			PRINTSTRING(Get_Shop_Display_String_From_Shop_ID(paramShopID))
			PRINTSTRING(" STATE=")
			PRINTSTRING(Get_Bool_Display_String_From_Bool(paramState))
			PRINTSTRING("]")
			PRINTNL()
		#ENDIF
	#ENDIF
	
	Stop_Flow_Command(thisStorageType)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Set Weapon Comp Lock State' command
// 
// INPUT PARAMS:	paramWeaponCompID		The weapon component enum
//					paramState				The new lock state: TRUE (1) or FALSE (0)
//
// NOTES:	The weapon comp enum will be converted to and stored as an INT

PROC SETFLOW_SET_WEAPON_COMP_LOCK_STATE(WEAPON_TYPE paramWeaponID, WEAPONCOMPONENT_TYPE paramWeaponCompID, BOOL paramState)

	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE
	
	Start_Flow_Command(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_SET_WEAPON_COMP_LOCK_STATE
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	INT iState = 0
	IF paramState
		iState = 1
	ENDIF
	
	STORE_CORE_STORAGE_TYPE_VARIABLES(	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
										ENUM_TO_INT(paramWeaponID),
										ENUM_TO_INT(paramWeaponCompID),
										iState,
										0)								//Not Needed.
											
	// Debug Output
	#IF IS_DEBUG_BUILD
		#IF MISSION_FLOW_INITIALISER_DEBUG_OUTPUT
			PRINTINT(g_flowUnsaved.iNumStoredFlowCommands)
			PRINTSTRING(": SET WEAPON COMP LOCK STATE [WEAPONCOMP=")
			PRINTSTRING(GET_STRING_FROM_TEXT_FILE(GET_WEAPON_COMPONENT_NAME(paramWeaponCompID)))
			PRINTSTRING(" STATE=")
			PRINTSTRING(Get_Bool_Display_String_From_Bool(paramState))
			PRINTSTRING("]")
			PRINTNL()
		#ENDIF
	#ENDIF
	
	Stop_Flow_Command(thisStorageType)

ENDPROC





// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Set Vehicle Gen State' command
// 
// INPUT PARAMS:	paramVehicleGenID		The vehicle gen ID
//					paramState				The new vehicle gen active state: TRUE (1) or FALSE (0)
//
// NOTES:	The shop enum will be converted to and stored as an INT

PROC SETFLOW_SET_VEHICLE_GEN_STATE(VEHICLE_GEN_NAME_ENUM paramVehicleGenID, BOOL paramState)

	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE_SMALL
	
	Start_Flow_Command(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_SET_VEHICLE_GEN_STATE
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	INT iState = 0
	IF paramState
		iState = 1
	ENDIF
	
	STORE_CORE_SMALL_STORAGE_TYPE_VARIABLES(g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
											ENUM_TO_INT(paramVehicleGenID),
											iState)
											
	// Debug Output
	#IF IS_DEBUG_BUILD
		#IF MISSION_FLOW_INITIALISER_DEBUG_OUTPUT
			PRINTINT(g_flowUnsaved.iNumStoredFlowCommands)
			PRINTSTRING(": SET VEHICLE GEN STATE [VEHGEN=")
			PRINTSTRING(Get_Vehicle_Gen_Display_String_From_VehicleGen_ID(paramVehicleGenID))
			PRINTSTRING(" STATE=")
			PRINTSTRING(Get_Bool_Display_String_From_Bool(paramState))
			PRINTSTRING("]")
			PRINTNL()
		#ENDIF
	#ENDIF
	
	Stop_Flow_Command(thisStorageType)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Set Speical Ability Unlock State' command
// 
// INPUT PARAMS:	paramVehicleGenID		The player character enum
//					paramState				The new special ability unlock state: TRUE (1) or FALSE (0)
//

PROC SETFLOW_SET_SPECIAL_ABILITY_UNLOCK_STATE(enumCharacterList paramCharacter, BOOL paramState)

	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE_SMALL
	
	Start_Flow_Command(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_SET_SPECIAL_ABILITY_UNLOCK_STATE
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	//Check the passed character ENUM correspondes to a playable character.
	IF NOT IS_PLAYER_PED_PLAYABLE(paramCharacter)
		SCRIPT_ASSERT("SETFLOW_SET_SPECIAL_ABILITY_UNLOCK_STATE: Tried to set special ability unlock state for a character that is not playable! Tell BenR.")
	ENDIF
	
	INT iState = 0
	IF paramState
		iState = 1
	ENDIF
	
	STORE_CORE_SMALL_STORAGE_TYPE_VARIABLES(g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
											ENUM_TO_INT(paramCharacter),
											iState)
											
	// Debug Output
	#IF IS_DEBUG_BUILD
		#IF MISSION_FLOW_INITIALISER_DEBUG_OUTPUT
			PRINTINT(g_flowUnsaved.iNumStoredFlowCommands)
			PRINTSTRING(": SET SPECIAL ABILITY UNLOCK STATE [CHAR=")
			PRINTSTRING(Get_CharSheet_Display_String_From_CharSheet(paramCharacter))
			PRINTSTRING("]")
			PRINTNL()
		#ENDIF
	#ENDIF
	
	Stop_Flow_Command(thisStorageType)

ENDPROC



// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Toggle Board Viewing' command + variables
//
// INPUT PARAMS:	paramHeist				The heist index of the heist that the planning board belongs to.
//					paramInteraction		The type of interaction to enable for the heist board.
//					paramCompleteJump		The command to move to once the board interaction has been completed.
//					paramLockExit			Do we want to disable the player from being able to quit out of the board view until the interaction is completed?
//					paramWaitForCutscene	Should we wait for a cutscene to start before performing the toggling?
//					paramFlowFlag			A flow flag that can be set to be toggled as the board view toggles.
PROC SETFLOW_TOGGLE_BOARD_VIEWING(INT paramHeist, BOOL paramLockExit = FALSE, BOOL paramWaitForCutsceneStart = TRUE, FLOW_FLAG_IDS paramFlowFlag = FLOWFLAG_NONE)

	//Check the heist index is valid.
	IF paramHeist < 0 OR paramHeist >= NO_HEISTS
		SCRIPT_ASSERT("SETFLOW_TOGGLE_BOARD_VIEWING: Invalid heist index passed.")
	ENDIF
	
	FLOW_STORAGE_TYPES	thisStorageType		= FST_CORE
	
	INT iSettings = 0
	IF paramLockExit				SET_BIT(iSettings, FLOW_BOARD_BIT_LOCK_EXIT) 			ENDIF
	IF paramWaitForCutsceneStart	SET_BIT(iSettings, FLOW_BOARD_BIT_WAIT_FOR_CUTSCENE)  	ENDIF
	
	Start_Flow_Command(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_TOGGLE_BOARD_VIEWING
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	STORE_CORE_STORAGE_TYPE_VARIABLES(	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
										paramHeist,
										ENUM_TO_INT(paramFlowFlag),
										iSettings,
										0)							// Not Needed.
											
	// Debug Output
	#IF IS_DEBUG_BUILD
		#IF MISSION_FLOW_INITIALISER_DEBUG_OUTPUT
			INT arrayPos = g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index
			PRINTINT(g_flowUnsaved.iNumStoredFlowCommands)
			PRINTSTRING(": TOGGLE_BOARD_VIEWING [HEIST=")
			PRINTSTRING(g_flowUnsaved.scriptVars[arrayPos].filename)
			PRINTSTRING(" LOCKEXIT=")
			PRINTBOOL(paramLockExit)
			PRINTSTRING("]")
			PRINTNL()
		#ENDIF
	#ENDIF
	
	Stop_Flow_Command(thisStorageType)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Set Board Display Group Visibility' command
// 
// INPUT PARAMS:	paramHeist			The heist index of the heist that the planning board belongs to.
//					paramDisplayGroup	An ENUM referencing the display group that is to have its visibility changed.
//					paramIsVisible		The visibility state we want to assign to the display group.

PROC SETFLOW_SET_BOARD_DISPLAY_GROUP_VISIBILITY(INT paramHeist, g_eBoardDisplayGroups paramDisplayGroup, BOOL paramIsVisible, INT paramHashModifier = 0)

	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE
	
	Start_Flow_Command(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_SET_BOARD_DISPLAY_GROUP_VISIBILITY
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	INT iSettings = 0
	IF paramIsVisible	SET_BIT(iSettings, FLOW_BOARD_BIT_DGROUP_VISIBLE)	ENDIF
	
	STORE_CORE_STORAGE_TYPE_VARIABLES(	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
										paramHeist,
										ENUM_TO_INT(paramDisplayGroup),
										iSettings,
										paramHashModifier)
											
	// Debug Output
	#IF IS_DEBUG_BUILD
		#IF MISSION_FLOW_INITIALISER_DEBUG_OUTPUT
			PRINTINT(g_flowUnsaved.iNumStoredFlowCommands)
			PRINTSTRING(": SET BOARD DISPLAY GROUP VISIBILITY [HEIST.ID=")
			PRINTINT(paramHeist)
			PRINTSTRING(" DISP.ID=")
			PRINTINT(ENUM_TO_INT(paramDisplayGroup))
			PRINTSTRING(" VISIBLE=")
			PRINTBOOL(paramIsVisible)
			PRINTSTRING("]")
			PRINTNL()
		#ENDIF
	#ENDIF
	
	Stop_Flow_Command(thisStorageType)

ENDPROC



// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Set Mission Passed' command
// 
// INPUT PARAMS:	paramMissionName	The name of the mission script to pass without a .sc on the end.

PROC SETFLOW_SET_MISSION_PASSED(SP_MISSIONS paramMissionID)

	FLOW_STORAGE_TYPES	thisStorageType = FST_INDEX_DIRECT
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_SET_MISSION_PASSED
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= ENUM_TO_INT(paramMissionID)
		
	STOP_FLOW_COMMAND(thisStorageType)
	
ENDPROC



// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Set Mission Not Passed' command
// 
// INPUT PARAMS:	paramMissionName	The name of the mission script to pass without a .sc on the end.

PROC SETFLOW_SET_MISSION_NOT_PASSED(SP_MISSIONS paramMissionID)

	FLOW_STORAGE_TYPES	thisStorageType = FST_INDEX_DIRECT
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_SET_MISSION_NOT_PASSED
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= ENUM_TO_INT(paramMissionID)
		
	STOP_FLOW_COMMAND(thisStorageType)
	
ENDPROC



// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Is Player In Area' command
// 
// INPUT PARAMS:	paramArea			The VectorID used to define the area we want to check.
//					paramInvertCheck	FALSE if we want to check when the player is in the area, TRUE if we want to check if the player isn't in the area.

PROC SETFLOW_IS_PLAYER_IN_AREA(VectorID paramArea,  BOOL paramInvertCheck)

	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE_SMALL
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_IS_PLAYER_IN_AREA
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
		
	INT iInvertCheck = 0
	IF paramInvertCheck
		iInvertCheck = 1
	ENDIF
		
	STORE_CORE_SMALL_STORAGE_TYPE_VARIABLES(g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
											ENUM_TO_INT(paramArea),
											iInvertCheck)
	
	STOP_FLOW_COMMAND(thisStorageType)
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Do Player Bank Action' command
// 
// INPUT PARAMS:	paramCharacter		The playable character whose account to debit.
//					paramActionType		The type of account action this is. Credit or debit.
//					paramSource			An enum referencing what the source of the account action is. Determines the note that appears in the account log.
//					paramAmount			The value of money to credit/debit into the account.

PROC SETFLOW_DO_PLAYER_BANK_ACTION(enumCharacterList paramCharacter, BANK_ACCOUNT_ACTION_TYPE paramActionType, BANK_ACCOUNT_ACTION_SOURCE_BAAC paramSource, INT paramAmount)

	IF ENUM_TO_INT(paramCharacter) > ENUM_TO_INT(CHAR_TREVOR)
		SCRIPT_ASSERT("SETFLOW_DO_PLAYER_BANK_ACTION: The character passed was not playable. Could not store command.")
	ENDIF
	IF paramAmount <= 0
		SCRIPT_ASSERT("SETFLOW_DO_PLAYER_BANK_ACTION: The value of the bank action is invalid. Cannot be <= 0.")
	ENDIF
	
	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE
	
	START_FLOW_COMMAND(thisStorageType)

	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_DO_PLAYER_BANK_ACTION
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	STORE_CORE_STORAGE_TYPE_VARIABLES(	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
										ENUM_TO_INT(paramCharacter),
										ENUM_TO_INT(paramActionType),
										ENUM_TO_INT(paramSource),
										paramAmount)
	
	STOP_FLOW_COMMAND(thisStorageType)
	
ENDPROC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Is Shrink Session Valid' command
// 
// INPUT PARAMS:	paramCharacter		The playable character whose account to debit.
//
PROC SETFLOW_IS_SHRINK_SESSION_AVAILABLE(SHRINK_SESSION paramSession, INT paramHashModifier = 0)
	
	FLOW_STORAGE_TYPES	thisStorageType = FST_INDEX_DIRECT
	
	START_FLOW_COMMAND(thisStorageType)
	
	INT iData = ENUM_TO_INT(paramSession)
	iData += paramHashModifier*FLOW_COMMAND_HASH_MODIFIER_MAGNITUDE

	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_IS_SHRINK_SESSION_AVAILABLE
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= iData
	
	STOP_FLOW_COMMAND(thisStorageType)
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Fast Track Strand' command
//
PROC SETFLOW_FAST_TRACK_STRAND()
	
	FLOW_STORAGE_TYPES	thisStorageType = FST_INDEX_DIRECT
	
	START_FLOW_COMMAND(thisStorageType)

	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_FAST_TRACK_STRAND
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= 0
	
	STOP_FLOW_COMMAND(thisStorageType)
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Fast Track Strand' command
//
PROC SETFLOW_DO_CUSTOM_CHECK_JUMPS(FLOW_CHECK_IDS paramCheckID, JUMP_LABEL_IDS paramTrueJump, JUMP_LABEL_IDS paramFalseJump, JUMP_LABEL_IDS paramDebugJump = STAY_ON_THIS_COMMAND)
	
		FLOW_STORAGE_TYPES	thisStorageType 	= FST_CORE
	
	Start_Flow_Command(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_DO_CUSTOM_CHECK_JUMPS
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	STORE_CORE_STORAGE_TYPE_VARIABLES(	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],											
										ENUM_TO_INT(paramCheckID),
										ENUM_TO_INT(paramTrueJump),
										ENUM_TO_INT(paramFalseJump),
										ENUM_TO_INT(paramDebugJump))
						
	Stop_Flow_Command(thisStorageType)
	
ENDPROC

/// PURPOSE:
///    Store 'Set Component Acquired' command
/// PARAMS:
///    eItem - ped component enum of the component item the player has acquired
///    eType - the type of item the player has acquireds
PROC SETFLOW_SET_COMPONENT_ACQUIRED(enumCharacterList eCharacter, PED_COMP_NAME_ENUM eItem, PED_COMP_TYPE_ENUM eType)

	IF ENUM_TO_INT(eCharacter) > ENUM_TO_INT(CHAR_TREVOR)
		SCRIPT_ASSERT("SETFLOW_SET_COMPONENT_ACQUIRED: The character passed was not playable. Could not store command.")
	ENDIF
	IF ENUM_TO_INT(eItem) <= 0
		SCRIPT_ASSERT("SETFLOW_SET_COMPONENT_ACQUIRED: The item was invalid. Cannot be <= 0.")
	ENDIF

	FLOW_STORAGE_TYPES	thisStorageType 	= FST_CORE
	
	Start_Flow_Command(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_SET_COMPONENT_ACQUIRED
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	

	STORE_CORE_STORAGE_TYPE_VARIABLES(g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],											
										ENUM_TO_INT(eCharacter),
										ENUM_TO_INT(eItem),
										ENUM_TO_INT(eType),
										0)
						
	Stop_Flow_Command(thisStorageType)
ENDPROC

