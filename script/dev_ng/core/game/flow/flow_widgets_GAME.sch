
USING "commands_debug.sch"
USING "flow_structs_core.sch"

PROC CREATE_MISSION_FLOW_INTERNALS_WIDGETS_CUSTOM(FLOW_DEBUG &paramDebugVars)
	ADD_WIDGET_BOOL("MP/SP [READ-ONLY]? Multiplayer (Ticked) or Singleplayer (Unticked)", paramDebugVars.boolInMultiplayer)
	ADD_WIDGET_BOOL("Silence Flow Phonecalls", g_bDebugFlowSilencePhonecalls)
	ADD_WIDGET_BOOL("Exile area block", g_bExileMidTriggering)
ENDPROC

