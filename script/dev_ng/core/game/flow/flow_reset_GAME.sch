USING "rage_builtins.sch"
USING "globals.sch"

USING "flow_private_core.sch"
USING "commands_player.sch"
USING "shop_private.sch"
USING "player_ped_public.sch"
USING "stats_private.sch"
USING "building_control_private.sch"
USING "respawn_location_data.sch"
USING "context_control_public.sch"
USING "replay_public.sch"
USING "dialogue_public.sch"
USING "flow_help_public.sch"
USING "friend_flow_public.sch"
USING "cellphone_public.sch"
USING "mission_control_private.sch"
USING "social_public.sch"
USING "mission_stat_public.sch"
USING "email_public.sch"
USING "code_control_public.sch"
USING "commands_replay.sch"

#IF NOT USE_SP_DLC
	USING "randomChar_Public.sch"
	USING "Flow_Heist_Data_GTA5.sch"
	USING "photographyWildlife.sch"
#ENDIF


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		HEADER NAME		:	Gameflow_System_Resets.sch
//		CREATED			:	BenR
//		DESCRIPTION		:	Procedures for resetting any subsystems that need to be
//							reinitialised on a gameflow restart.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************


PROC Reset_SP_Mission_Flow()

	// Clear out all gameflow specific globals.
	#if USE_CLF_DLC
		CLEAR_MISSION_FLOW_SAVED_GLOBALS_CLF()
		Clear_Available_Missions_Array()
		Setup_Strand_Commands_To_Be_Lower_Boundary_Commands()
		// Clear relaunch scripts bitsets.
//		g_savedGlobalsClifford.sFlow.controls.bitsetIDs[FLOWBITSET_RESTART_FLOW_LAUNCHED_SCRIPTS_1] = 0
//		g_savedGlobalsClifford.sFlow.controls.bitsetIDs[FLOWBITSET_RESTART_FLOW_LAUNCHED_SCRIPTS_2] = 0	
		// Reset strand timers.
		INT iStrandIndex
		REPEAT MAX_STRANDS_CLF iStrandIndex
			g_flowUnsaved.bStrandTimerSet[iStrandIndex] = FALSE
		ENDREPEAT
		g_savedGlobalsClifford.sFlow.flowCompleted = FALSE
		g_savedGlobalsClifford.sFlowCustom.spInitBitset = 0
	#endif
	#if USE_NRM_DLC
		CLEAR_MISSION_FLOW_SAVED_GLOBALS_NRM()
		Clear_Available_Missions_Array()
		Setup_Strand_Commands_To_Be_Lower_Boundary_Commands()
		// Clear relaunch scripts bitsets.
//		g_savedGlobalsnorman.sFlow.controls.bitsetIDs[FLOWBITSET_RESTART_FLOW_LAUNCHED_SCRIPTS_1] = 0
//		g_savedGlobalsnorman.sFlow.controls.bitsetIDs[FLOWBITSET_RESTART_FLOW_LAUNCHED_SCRIPTS_2] = 0	
		// Reset strand timers.
		INT iStrandIndex
		REPEAT MAX_STRANDS_NRM iStrandIndex
			g_flowUnsaved.bStrandTimerSet[iStrandIndex] = FALSE
		ENDREPEAT
		g_savedGlobalsnorman.sFlow.flowCompleted = FALSE
		g_savedGlobalsnorman.sFlowCustom.spInitBitset = 0
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		Clear_Mission_Flow_Saved_Globals()
		Clear_Available_Missions_Array()
		Setup_Strand_Commands_To_Be_Lower_Boundary_Commands()
		// Clear relaunch scripts bitsets.
		g_savedGlobals.sFlow.controls.bitsetIDs[FLOWBITSET_RESTART_FLOW_LAUNCHED_SCRIPTS_1] = 0
		g_savedGlobals.sFlow.controls.bitsetIDs[FLOWBITSET_RESTART_FLOW_LAUNCHED_SCRIPTS_2] = 0	
		// Reset strand timers.
		INT iStrandIndex
		REPEAT MAX_STRANDS iStrandIndex
			g_flowUnsaved.bStrandTimerSet[iStrandIndex] = FALSE
		ENDREPEAT
		g_savedGlobals.sFlow.flowCompleted = FALSE
		g_savedGlobals.sFlowCustom.spInitBitset = 0
	#endif
	#endif
		
	// Reset flow special mode flags.
	g_flowUnsaved.eLockInStrand = STRAND_NONE
	g_flowUnsaved.bFlowProcessAllStrandsNextFrame = FALSE
	g_flowUnsaved.bFlowFastTrackCurrentStrand = FALSE
	g_flowUnsaved.bIdleUntilActiveStrand = TRUE
	
	g_eRunningMission = SP_MISSION_NONE
	g_eTriggerRCI = RCI_NONE
ENDPROC


// PURPOSE: Resets all mission data in the flow mission data array.
//
PROC Reset_Mission_Data()
	INT iMissionIndex
#if USE_CLF_DLC
	REPEAT SP_MISSION_MAX_CLF iMissionIndex
		SP_MISSIONS eMissionID = INT_TO_ENUM(SP_MISSIONS, iMissionIndex)	
		// Set the complete state of each mission to FALSE.
		g_savedGlobalsClifford.sFlow.missionSavedData[iMissionIndex].completed = FALSE
		// Reset the number of fails tracked by the shitskip system.
		g_savedGlobalsClifford.sFlow.missionSavedData[iMissionIndex].missionFailsNoProgress = 0
		// Reset the total number of fails for each mission.
		g_savedGlobalsClifford.sFlow.missionSavedData[iMissionIndex].missionFailsTotal = 0
		// Set the leave area flag of each mission to FALSE.
		Set_Leave_Area_Flag_For_Mission(eMissionID, FALSE)
		// Reset the completion order for repeat play
		g_savedGlobalsClifford.sFlow.missionSavedData[iMissionIndex].iCompletionOrder = -1
		// Reset mission scrore and stats.
		g_savedGlobalsClifford.sFlow.missionSavedData[iMissionIndex].iScore = 0
		g_savedGlobalsClifford.sFlow.missionSavedData[iMissionIndex].fStatCompletion = 0
		
		// Reset flow completion order stat for this mission.
		IF NOT IS_REPEAT_PLAY_ACTIVE()
			IF NOT IS_BIT_SET(g_sMissionStaticData[iMissionIndex].settingsBitset, MF_INDEX_NO_COMP_ORDER)
				STATSENUM eCompletionOrderStat = GET_SP_MISSION_COMPLETION_ORDER_STAT(eMissionID)
				IF eCompletionOrderStat != CITIES_PASSED
					#IF IS_DEBUG_BUILD
						CDEBUG3LN(DEBUG_FLOW, "Resetting flow CO stat for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID), ".")
					#ENDIF
					STAT_SET_INT(eCompletionOrderStat, 0)
				ENDIF				
			ENDIF
		ENDIF
	ENDREPEAT

	// Reset number of missions the player has completed (used in repeat play system)
	g_savedGlobalsClifford.sFlowCustom.iMissionsCompleted = 0
	// g_savedGlobals.sFlowCustom.iMissionGolds = 0 // we don't want to reset this value or you'll never get ACH10
	
	// Reset first activation bits for story missions.
	INT iBitsetIndex
	REPEAT NO_MISSION_ACTIVATE_BITSETS iBitsetIndex
		g_savedGlobalsClifford.sFlowCustom.missionFirstActivateBitset[iBitsetIndex] = 0
	ENDREPEAT
	
#endif
#if USE_NRM_DLC
	REPEAT SP_MISSION_MAX_NRM iMissionIndex
		SP_MISSIONS eMissionID = INT_TO_ENUM(SP_MISSIONS, iMissionIndex)	
		// Set the complete state of each mission to FALSE.
		g_savedGlobalsnorman.sFlow.missionSavedData[iMissionIndex].completed = FALSE
		// Reset the number of fails tracked by the shitskip system.
		g_savedGlobalsnorman.sFlow.missionSavedData[iMissionIndex].missionFailsNoProgress = 0
		// Reset the total number of fails for each mission.
		g_savedGlobalsnorman.sFlow.missionSavedData[iMissionIndex].missionFailsTotal = 0
		// Set the leave area flag of each mission to FALSE.
		Set_Leave_Area_Flag_For_Mission(eMissionID, FALSE)
		// Reset the completion order for repeat play
		g_savedGlobalsnorman.sFlow.missionSavedData[iMissionIndex].iCompletionOrder = -1
		// Reset mission scrore and stats.
		g_savedGlobalsnorman.sFlow.missionSavedData[iMissionIndex].iScore = 0
		g_savedGlobalsnorman.sFlow.missionSavedData[iMissionIndex].fStatCompletion = 0
		
		// Reset flow completion order stat for this mission.
		IF NOT IS_REPEAT_PLAY_ACTIVE()
			IF NOT IS_BIT_SET(g_sMissionStaticData[iMissionIndex].settingsBitset, MF_INDEX_NO_COMP_ORDER)
				STATSENUM eCompletionOrderStat = GET_SP_MISSION_COMPLETION_ORDER_STAT(eMissionID)
				IF eCompletionOrderStat != CITIES_PASSED
					#IF IS_DEBUG_BUILD
						CDEBUG3LN(DEBUG_FLOW, "Resetting flow CO stat for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID), ".")
					#ENDIF
					STAT_SET_INT(eCompletionOrderStat, 0)
				ENDIF				
			ENDIF
		ENDIF
	ENDREPEAT

	// Reset number of missions the player has completed (used in repeat play system)
	g_savedGlobalsnorman.sFlowCustom.iMissionsCompleted = 0
	// g_savedGlobals.sFlowCustom.iMissionGolds = 0 // we don't want to reset this value or you'll never get ACH10
	
	// Reset first activation bits for story missions.
	INT iBitsetIndex
	REPEAT NO_MISSION_ACTIVATE_BITSETS iBitsetIndex
		g_savedGlobalsnorman.sFlowCustom.missionFirstActivateBitset[iBitsetIndex] = 0
	ENDREPEAT
	
#endif

#if not USE_CLF_DLC
#if not USE_NRM_DLC
	REPEAT SP_MISSION_MAX iMissionIndex
		SP_MISSIONS eMissionID = INT_TO_ENUM(SP_MISSIONS, iMissionIndex)	
		// Set the complete state of each mission to FALSE.
		g_savedGlobals.sFlow.missionSavedData[iMissionIndex].completed = FALSE
		// Reset the number of fails tracked by the shitskip system.
		g_savedGlobals.sFlow.missionSavedData[iMissionIndex].missionFailsNoProgress = 0
		// Reset the total number of fails for each mission.
		g_savedGlobals.sFlow.missionSavedData[iMissionIndex].missionFailsTotal = 0
		// Set the leave area flag of each mission to FALSE.
		Set_Leave_Area_Flag_For_Mission(eMissionID, FALSE)
		// Reset the completion order for repeat play
		g_savedGlobals.sFlow.missionSavedData[iMissionIndex].iCompletionOrder = -1
		// Reset mission scrore and stats.
		g_savedGlobals.sFlow.missionSavedData[iMissionIndex].iScore = 0
		g_savedGlobals.sFlow.missionSavedData[iMissionIndex].fStatCompletion = 0
		
		// Reset flow completion order stat for this mission.
		IF NOT IS_REPEAT_PLAY_ACTIVE()
			IF NOT IS_BIT_SET(g_sMissionStaticData[iMissionIndex].settingsBitset, MF_INDEX_NO_COMP_ORDER)
				IF eMissionID != SP_HEIST_JEWELRY_2
					STATSENUM eCompletionOrderStat = GET_SP_MISSION_COMPLETION_ORDER_STAT(eMissionID)
					IF eCompletionOrderStat != CITIES_PASSED
						#IF IS_DEBUG_BUILD
							CDEBUG3LN(DEBUG_FLOW, "Resetting flow CO stat for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID), ".")
						#ENDIF
						STAT_SET_INT(eCompletionOrderStat, 0)
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						CDEBUG3LN(DEBUG_FLOW, "Resetting flow CO stat for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID), " (A variation).")
					#ENDIF
					STAT_SET_INT(FL_CO_JH2A, 0)
					#IF IS_DEBUG_BUILD
						CDEBUG3LN(DEBUG_FLOW, "Resetting flow CO stat for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID), " (B variation).")
					#ENDIF
					STAT_SET_INT(FL_CO_JH2B, 0)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

	// Reset number of missions the player has completed (used in repeat play system)
	g_savedGlobals.sFlowCustom.iMissionsCompleted = 0
	// g_savedGlobals.sFlowCustom.iMissionGolds = 0 // we don't want to reset this value or you'll never get ACH10
	
	// Reset first activation bits for story missions.
	INT iBitsetIndex
	REPEAT NO_MISSION_ACTIVATE_BITSETS iBitsetIndex
		g_savedGlobals.sFlowCustom.missionFirstActivateBitset[iBitsetIndex] = 0
	ENDREPEAT
#endif
#endif
	
ENDPROC


PROC Reset_Code_Controller()
	INT iCodeIDIndex
	#if USE_CLF_DLC
		REPEAT CID_CLF_MAX iCodeIDIndex
			g_savedGlobalsClifford.sCodeControlData.bRunCodeID[iCodeIDIndex] = FALSE
			g_savedGlobalsClifford.sCodeControlData.iExecuteTimeForCodeID[iCodeIDIndex] = -1
		ENDREPEAT
	#endif
	#if USE_NRM_DLC
		REPEAT CID_NRM_MAX iCodeIDIndex
			g_savedGlobalsnorman.sCodeControlData.bRunCodeID[iCodeIDIndex] = FALSE
			g_savedGlobalsnorman.sCodeControlData.iExecuteTimeForCodeID[iCodeIDIndex] = -1
		ENDREPEAT
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		REPEAT CID_MAX iCodeIDIndex
			g_savedGlobals.sCodeControlData.bRunCodeID[iCodeIDIndex] = FALSE
			g_savedGlobals.sCodeControlData.iExecuteTimeForCodeID[iCodeIDIndex] = -1
		ENDREPEAT
	#endif
	#endif
ENDPROC


PROC Reset_Cutscenes()
	INT index
	REPEAT g_iFlowCurrentCutsceneVariationCount index
		g_sFlowCutsceneVariations[index].iHandleIndex = -1
		g_sFlowCutsceneVariations[index].eComponent = PED_COMP_HEAD
		g_sFlowCutsceneVariations[index].iDrawable = 0
		g_sFlowCutsceneVariations[index].iTexture = 0
	ENDREPEAT
	g_iFlowCurrentCutsceneVariationCount = 0
	
	REPEAT g_iFlowCurrentCutsceneOutfitCount index
		g_sFlowCutsceneOutfit[index].iHandleIndex = -1
		g_sFlowCutsceneOutfit[index].eModel = DUMMY_MODEL_FOR_SCRIPT
		g_sFlowCutsceneOutfit[index].eOutfit = DUMMY_PED_COMP
	ENDREPEAT
	g_iFlowCurrentCutsceneOutfitCount = 0
	
	REPEAT g_iFlowCurrentPedVariationCount index
		g_sFlowPedVariation[index].iHandleIndex = -1
		g_sFlowPedVariation[index].ped = NULL
	ENDREPEAT
	g_iFlowCurrentPedVariationCount = 0
	
	REPEAT g_iFlowCurrentCutscenPropCount index
		g_sFlowCutsceneProp[index].iHandleIndex = -1
		g_sFlowCutsceneProp[index].ePosition = ANCHOR_HEAD
		g_sFlowCutsceneProp[index].iPropIndex = 0
		g_sFlowCutsceneProp[index].iTextureIndex = 0
	ENDREPEAT
	g_iFlowCurrentCutscenPropCount = 0
	
	REPEAT g_iFlowCurrentCutsceneHandleCount index
		g_txtFlowCutsceneHandles[index] = ""
	ENDREPEAT
	g_iFlowCurrentCutsceneHandleCount = 0
	
	REPEAT 3 index
		g_iFlowBitsetBlockPlayerCutsceneVariations[index] = 0
	ENDREPEAT
	
	g_eMissionIDToLoadCutscene			= SP_MISSION_NONE
	g_iCharIntroSectionsToLoad[0]		= CS_NONE
	g_iCharIntroSectionsToLoad[1]		= CS_NONE
	g_iCharIntroSectionsToLoad[2]		= CS_NONE
	g_bFlowLoadIntroCutscene 			= FALSE
	g_bFlowIntroVariationRequestsMade 	= FALSE
	g_bFlowLoadingIntroCutscene 		= FALSE
	g_bFlowLoadRequestStarted			= FALSE
	g_bFlowCleanupIntroCutscene 		= TRUE
ENDPROC



// PURPOSE: Resets all heist data.
#if not USE_CLF_DLC
#if not USE_NRM_DLC
PROC Reset_Heist_Data()
	INT index
	
	REPEAT NO_HEISTS index
		g_savedGlobals.sHeistData.bChoiceHelpDisplayed[index] = FALSE
		g_savedGlobals.sHeistData.bCrewHelpDisplayed[index] = FALSE
		g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[index] = 0
		
		//-------Reset end screen data.-------
		g_savedGlobals.sHeistData.sEndScreenData[index].iPotentialTake = 0
		g_savedGlobals.sHeistData.sEndScreenData[index].iActualTake = 0
		g_savedGlobals.sHeistData.sEndScreenData[index].iTimeTaken = 0

		INT index2
		REPEAT 3 index2
			g_savedGlobals.sHeistData.sEndScreenData[index].iPlayerTake[index2] = 0
			g_savedGlobals.sHeistData.sEndScreenData[index].fPlayerPercentage[index2] = 0.0
		ENDREPEAT
		REPEAT MAX_CREW_SIZE index2
			g_savedGlobals.sHeistData.sEndScreenData[index].eCrewStatus[index2] = CMST_NOT_SET
			g_savedGlobals.sHeistData.sEndScreenData[index].iCrewMemberTake[index2] = 0
			g_sHeistTempStats[index2].statsIncreased = FALSE
			g_sHeistTempStats[index2].statsA = 0
			g_sHeistTempStats[index2].statsA = 0
		ENDREPEAT
		//-----------------------------------
	ENDREPEAT
	
	REPEAT MAX_HEIST_CHOICES index
		INT iCrewIndex
		REPEAT MAX_CREW_SIZE iCrewIndex
			g_savedGlobals.sHeistData.eSelectedCrew[index][iCrewIndex] = CM_EMPTY
		ENDREPEAT
		g_sHeistChoiceData[index].iBitsetChoiceDialoguePlayed = 0
	ENDREPEAT
	
	//Reset crew member unlocked states.
	SET_BIT(g_savedGlobals.sHeistData.iCrewUnlockedBitset, 		ENUM_TO_INT(CM_GUNMAN_G_GUSTAV))
	CLEAR_BIT(g_savedGlobals.sHeistData.iCrewUnlockedBitset, 	ENUM_TO_INT(CM_GUNMAN_G_KARL))
	CLEAR_BIT(g_savedGlobals.sHeistData.iCrewUnlockedBitset, 	ENUM_TO_INT(CM_GUNMAN_G_PACKIE_UNLOCK))
	CLEAR_BIT(g_savedGlobals.sHeistData.iCrewUnlockedBitset, 	ENUM_TO_INT(CM_GUNMAN_G_CHEF_UNLOCK))
	CLEAR_BIT(g_savedGlobals.sHeistData.iCrewUnlockedBitset, 	ENUM_TO_INT(CM_GUNMAN_M_HUGH))
	SET_BIT(g_savedGlobals.sHeistData.iCrewUnlockedBitset, 		ENUM_TO_INT(CM_GUNMAN_B_NORM))
	CLEAR_BIT(g_savedGlobals.sHeistData.iCrewUnlockedBitset, 	ENUM_TO_INT(CM_GUNMAN_B_DARYL))
														
	SET_BIT(g_savedGlobals.sHeistData.iCrewUnlockedBitset, 		ENUM_TO_INT(CM_HACKER_G_PAIGE))
	SET_BIT(g_savedGlobals.sHeistData.iCrewUnlockedBitset, 		ENUM_TO_INT(CM_HACKER_M_CHRIS))
	CLEAR_BIT(g_savedGlobals.sHeistData.iCrewUnlockedBitset, 	ENUM_TO_INT(CM_HACKER_B_RICKIE_UNLOCK))
														
	SET_BIT(g_savedGlobals.sHeistData.iCrewUnlockedBitset, 		ENUM_TO_INT(CM_DRIVER_G_EDDIE))
	CLEAR_BIT(g_savedGlobals.sHeistData.iCrewUnlockedBitset, 	ENUM_TO_INT(CM_DRIVER_G_TALINA_UNLOCK))
	SET_BIT(g_savedGlobals.sHeistData.iCrewUnlockedBitset, 		ENUM_TO_INT(CM_DRIVER_B_KARIM))
	
	Store_All_GTA5_Crew_Member_Data()
	
	//Reset crew member death states.
	g_savedGlobals.sHeistData.iCrewDeadBitset = 0
	
	//Reset crew member dialogue states.
	g_savedGlobals.sHeistData.iCrewDialogueBitset = 0
	
	g_bHeistBoardViewActive = FALSE
	g_bHeistTempCrewStatsPrimed = FALSE
	
	sEndScreen.bSformeUploadDone = FALSE
	
ENDPROC
#endif
#endif

// PURPOSE:	Deactivates all static blips.
//
PROC Reset_Static_Blip_States()

	INT iBlipIndex
	REPEAT STATIC_BLIP_NAME_DUMMY_FINAL iBlipIndex
	
		// Only change the state if the blip is currently active.
		IF IS_BIT_SET(g_GameBlips[iBlipIndex].iSetting,STATIC_BLIP_SETTING_ACTIVE ) 
			//Clear the bit.
			CLEAR_BIT(g_GameBlips[iBlipIndex].iSetting,STATIC_BLIP_SETTING_ACTIVE )
			//CLEAR_BIT(g_GameBlips[iBlipIndex].iSetting,STATIC_BLIP_SETTING_CURRENTLY_SUPPRESSED_BY_AMBIENT )
			//CLEAR_BIT(g_GameBlips[iBlipIndex].iSetting,STATIC_BLIP_SETTING_CURRENTLY_SUPPRESSED_BY_MISSION )
			CLEAR_BIT(g_GameBlips[iBlipIndex].iSetting,STATIC_BLIP_SETTING_ACTIVATED_EXTERNALLY )
			//CLEAR_BIT(g_GameBlips[iBlipIndex].iSetting,STATIC_BLIP_SETTING_HAS_FLASHED)

			SET_BIT(g_GameBlips[iBlipIndex].iSetting,STATIC_BLIP_SETTING_STATUS_CHANGED)//g_GameBlips[iBlipIndex].bStatusChanged = TRUE
		ENDIF
		
	ENDREPEAT	
	
ENDPROC


// PURPOSE:	Resets all the respawn location data flags so the controller script can re-initialise the defaults.
//
PROC Reset_Respawn_Location_Data_Flags()

#if USE_CLF_DLC
	g_savedGlobalsClifford.sRespawnData.bDefaultSavehouseDataSet = FALSE
	Initialise_Savehouse_Respawn_Locations()
	g_savedGlobalsClifford.sRespawnData.bDefaultHospitalDataSet = FALSE
	Initialise_Hospital_Respawn_Locations()
	g_savedGlobalsClifford.sRespawnData.bDefaultPoliceStationDataSet = FALSE
	Initialise_Police_Respawn_Locations()
	
	g_savedGlobalsClifford.sRespawnData.bSeenFirstTimeWasted = FALSE
	g_savedGlobalsClifford.sRespawnData.bSeenFirstTimeDrowned = FALSE
	g_savedGlobalsClifford.sRespawnData.bSeenFirstTimeBusted = FALSE
#endif	
#if USE_NRM_DLC
	g_savedGlobalsnorman.sRespawnData.bDefaultSavehouseDataSet = FALSE
	Initialise_Savehouse_Respawn_Locations()
	g_savedGlobalsnorman.sRespawnData.bDefaultHospitalDataSet = FALSE
	Initialise_Hospital_Respawn_Locations()
	g_savedGlobalsnorman.sRespawnData.bDefaultPoliceStationDataSet = FALSE
	Initialise_Police_Respawn_Locations()
	
	g_savedGlobalsnorman.sRespawnData.bSeenFirstTimeWasted = FALSE
	g_savedGlobalsnorman.sRespawnData.bSeenFirstTimeDrowned = FALSE
	g_savedGlobalsnorman.sRespawnData.bSeenFirstTimeBusted = FALSE
#endif

#if not USE_CLF_DLC
#if not USE_NRM_DLC
	g_savedGlobals.sRespawnData.bDefaultSavehouseDataSet = FALSE
	Initialise_Savehouse_Respawn_Locations()
	g_savedGlobals.sRespawnData.bDefaultHospitalDataSet = FALSE
	Initialise_Hospital_Respawn_Locations()
	g_savedGlobals.sRespawnData.bDefaultPoliceStationDataSet = FALSE
	Initialise_Police_Respawn_Locations()
	
	g_savedGlobals.sRespawnData.bSeenFirstTimeWasted = FALSE
	g_savedGlobals.sRespawnData.bSeenFirstTimeDrowned = FALSE
	g_savedGlobals.sRespawnData.bSeenFirstTimeBusted = FALSE
#endif
#endif

ENDPROC


// PURPOSE:	Resets all the player info and stat values.
//
PROC Reset_Player_Data_Flags()

	#IF IS_DEBUG_BUILD
		g_bKeepAmmoWhenResettingPlayerData = TRUE
	#ENDIF
	
	#IF IS_DEBUG_BUILD
		// This clears all the stats tracked by code.
		STAT_RESET_ALL_SINGLE_PLAYER_STATS()
	#ENDIF
	
	SETUP_DEFAULT_PLAYER_INFO()
	SETUP_DEFAULT_PLAYER_STATS()
	
	//Reset the players last known ped info
	#if USE_CLF_DLC
		ResetLastKnownPedInfo(g_savedGlobalsClifford.sPlayerData.sInfo, SP_MISSION_NONE)
	#endif
	#if USE_NRM_DLC
		ResetLastKnownPedInfo(g_savedGlobalsnorman.sPlayerData.sInfo, SP_MISSION_NONE)
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		ResetLastKnownPedInfo(g_savedGlobals.sPlayerData.sInfo, SP_MISSION_NONE)
	#endif
	#endif
	
	#IF IS_DEBUG_BUILD
		g_bKeepAmmoWhenResettingPlayerData = FALSE
	#ENDIF
	
	g_bHandleEndOfMission_Martin1 = FALSE
ENDPROC


// PURPOSE:	Resets all the shop states.
//
PROC Reset_Shop_Data_Flags()
	SETUP_DEFAULT_SHOP_DATA()
ENDPROC


// PURPOSE:	Resets all the buildings and door states.
//
PROC Reset_Building_And_Door_Flags()
	WAIT(0)
	g_bResetBuildingController = TRUE
	SETUP_DEFAULT_BUILDING_DATA()	
	INITIALISE_STORED_BUILDING_STATES_ON_STARTUP(TRUE)
	RESET_ALL_AUTOMATIC_DOOR_REGISTERED_PEDS()
	PED_INDEX playerPed = PLAYER_PED_ID()
	REGISTER_PLAYER_PED_WITH_AUTOMATIC_DOORS(GET_CURRENT_PLAYER_PED_ENUM(), playerPed)
	WAIT(0)
ENDPROC


// PURPOSE:	Resets the state of the communication controller.
//
PROC Reset_Communication_Controller()
	CC_CallData sCallDataNew
	CC_TextMessageData sTextDataNew
	CC_EmailData sEmailDataNew

	//Reset the global peds for conversation struct.
	structPedsForConversation sPedsForConversationNew
	g_sPedsForConversation = sPedsForConversationNew
	
	//Reset wait timers.
	INT iGameTime = GET_GAME_TIMER()
	g_iGlobalWaitTime = iGameTime
	INT index
	
#if USE_CLF_DLC
	REPEAT MAX_CLF_CHARACTERS index
		g_iCharWaitTime[index] = iGameTime
	ENDREPEAT
	//Reset character priority levels.
	REPEAT 3 index
		g_savedGlobalsClifford.sCommsControlData.eCharacterPriorityLevel[index] = CPR_VERY_LOW
	ENDREPEAT
	//Reset call queue.
	REPEAT CC_MAX_QUEUED_CALLS index
		g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[index] = sCallDataNew
	ENDREPEAT
	g_savedGlobalsClifford.sCommsControlData.iNoQueuedCalls = 0	
	//Reset missed call queue.
	REPEAT CC_MAX_MISSED_CALLS index
		g_savedGlobalsClifford.sCommsControlData.sMissedCalls[index] = sCallDataNew
	ENDREPEAT
	g_savedGlobalsClifford.sCommsControlData.iNoMissedCalls = 0		 
	//Reset chat call queue.
	REPEAT CC_MAX_CHAT_CALLS index
		g_savedGlobalsClifford.sCommsControlData.sChatCalls[index] = sCallDataNew
	ENDREPEAT
	g_savedGlobalsClifford.sCommsControlData.iNoChatCalls = 0	
	//Reset text message queue.
	REPEAT CC_MAX_QUEUED_TEXTS index
		g_savedGlobalsClifford.sCommsControlData.sQueuedTexts[index] = sTextDataNew
	ENDREPEAT
	g_savedGlobalsClifford.sCommsControlData.iNoQueuedTexts = 0	
	//Reset sent text message queue.
	REPEAT CC_MAX_SENT_TEXTS index
		g_savedGlobalsClifford.sCommsControlData.sSentTexts[index] = sTextDataNew
	ENDREPEAT
	g_savedGlobalsClifford.sCommsControlData.iNoSentTexts = 0	
	//Reset email queue.
	REPEAT CC_MAX_QUEUED_EMAILS index
		g_savedGlobalsClifford.sCommsControlData.sQueuedEmails[index] = sEmailDataNew
	ENDREPEAT
	g_savedGlobalsClifford.sCommsControlData.iNoQueuedEmails = 0	
	//Reset last call flags.
	g_savedGlobalsClifford.sCommsControlData.eLastCompletedCall = COMM_NONE
	g_savedGlobalsClifford.sCommsControlData.bLastCallAnswered = FALSE
	g_savedGlobalsClifford.sCommsControlData.bLastCallHadResponse = FALSE
	g_savedGlobalsClifford.sCommsControlData.bLastCallResponse = FALSE	
	//Reset last text flags.
	g_savedGlobalsClifford.sCommsControlData.eLastCompletedText = COMM_NONE
	g_savedGlobalsClifford.sCommsControlData.bLastTextHadResponse = FALSE
	g_savedGlobalsClifford.sCommsControlData.bLastTextResponse = FALSE	
	//Reset last email flags.
	g_savedGlobalsClifford.sCommsControlData.eLastCompletedEmail = COMM_NONE	
	//reset bitset to zero 
	g_savedGlobalsClifford.sCommsControlData.iExileWarningBitset = 0
#endif
#if USE_NRM_DLC
	REPEAT MAX_NRM_CHARACTERS index
		g_iCharWaitTime[index] = iGameTime
	ENDREPEAT
	//Reset character priority levels.
	REPEAT 3 index
		g_savedGlobalsnorman.sCommsControlData.eCharacterPriorityLevel[index] = CPR_VERY_LOW
	ENDREPEAT
	//Reset call queue.
	REPEAT CC_MAX_QUEUED_CALLS index
		g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[index] = sCallDataNew
	ENDREPEAT
	g_savedGlobalsnorman.sCommsControlData.iNoQueuedCalls = 0	
	//Reset missed call queue.
	REPEAT CC_MAX_MISSED_CALLS index
		g_savedGlobalsnorman.sCommsControlData.sMissedCalls[index] = sCallDataNew
	ENDREPEAT
	g_savedGlobalsnorman.sCommsControlData.iNoMissedCalls = 0		 
	//Reset chat call queue.
	REPEAT CC_MAX_CHAT_CALLS index
		g_savedGlobalsnorman.sCommsControlData.sChatCalls[index] = sCallDataNew
	ENDREPEAT
	g_savedGlobalsnorman.sCommsControlData.iNoChatCalls = 0	
	//Reset text message queue.
	REPEAT CC_MAX_QUEUED_TEXTS index
		g_savedGlobalsnorman.sCommsControlData.sQueuedTexts[index] = sTextDataNew
	ENDREPEAT
	g_savedGlobalsnorman.sCommsControlData.iNoQueuedTexts = 0	
	//Reset sent text message queue.
	REPEAT CC_MAX_SENT_TEXTS index
		g_savedGlobalsnorman.sCommsControlData.sSentTexts[index] = sTextDataNew
	ENDREPEAT
	g_savedGlobalsnorman.sCommsControlData.iNoSentTexts = 0	
	//Reset email queue.
	REPEAT CC_MAX_QUEUED_EMAILS index
		g_savedGlobalsnorman.sCommsControlData.sQueuedEmails[index] = sEmailDataNew
	ENDREPEAT
	g_savedGlobalsnorman.sCommsControlData.iNoQueuedEmails = 0	
	//Reset last call flags.
	g_savedGlobalsnorman.sCommsControlData.eLastCompletedCall = COMM_NONE
	g_savedGlobalsnorman.sCommsControlData.bLastCallAnswered = FALSE
	g_savedGlobalsnorman.sCommsControlData.bLastCallHadResponse = FALSE
	g_savedGlobalsnorman.sCommsControlData.bLastCallResponse = FALSE	
	//Reset last text flags.
	g_savedGlobalsnorman.sCommsControlData.eLastCompletedText = COMM_NONE
	g_savedGlobalsnorman.sCommsControlData.bLastTextHadResponse = FALSE
	g_savedGlobalsnorman.sCommsControlData.bLastTextResponse = FALSE	
	//Reset last email flags.
	g_savedGlobalsnorman.sCommsControlData.eLastCompletedEmail = COMM_NONE	
	//reset bitset to zero 
	g_savedGlobalsnorman.sCommsControlData.iExileWarningBitset = 0
#endif


#if not USE_CLF_DLC	
#if not use_NRM_DLC
	INT iMaxCharacter = ENUM_TO_INT(GLOBAL_CHARACTER_SHEET_GET_MAX_CHARACTERS_FOR_GAMEMODE())
	REPEAT iMaxCharacter index
		g_iCharWaitTime[index] = iGameTime
	ENDREPEAT
	//Reset character priority levels.
	REPEAT 3 index
		g_savedGlobals.sCommsControlData.eCharacterPriorityLevel[index] = CPR_VERY_LOW
	ENDREPEAT
	//Reset call queue.
	REPEAT CC_MAX_QUEUED_CALLS index
		g_savedGlobals.sCommsControlData.sQueuedCalls[index] = sCallDataNew
	ENDREPEAT
	g_savedGlobals.sCommsControlData.iNoQueuedCalls = 0	
	//Reset missed call queue.
	REPEAT CC_MAX_MISSED_CALLS index
		g_savedGlobals.sCommsControlData.sMissedCalls[index] = sCallDataNew
	ENDREPEAT
	g_savedGlobals.sCommsControlData.iNoMissedCalls = 0		 
	//Reset chat call queue.
	REPEAT CC_MAX_CHAT_CALLS index
		g_savedGlobals.sCommsControlData.sChatCalls[index] = sCallDataNew
	ENDREPEAT
	g_savedGlobals.sCommsControlData.iNoChatCalls = 0	
	//Reset text message queue.
	REPEAT CC_MAX_QUEUED_TEXTS index
		g_savedGlobals.sCommsControlData.sQueuedTexts[index] = sTextDataNew
	ENDREPEAT
	g_savedGlobals.sCommsControlData.iNoQueuedTexts = 0	
	//Reset sent text message queue.
	REPEAT CC_MAX_SENT_TEXTS index
		g_savedGlobals.sCommsControlData.sSentTexts[index] = sTextDataNew
	ENDREPEAT
	g_savedGlobals.sCommsControlData.iNoSentTexts = 0	
	//Reset email queue.
	REPEAT CC_MAX_QUEUED_EMAILS index
		g_savedGlobals.sCommsControlData.sQueuedEmails[index] = sEmailDataNew
	ENDREPEAT
	g_savedGlobals.sCommsControlData.iNoQueuedEmails = 0	
	//Reset last call flags.
	g_savedGlobals.sCommsControlData.eLastCompletedCall = COMM_NONE
	g_savedGlobals.sCommsControlData.bLastCallAnswered = FALSE
	g_savedGlobals.sCommsControlData.bLastCallHadResponse = FALSE
	g_savedGlobals.sCommsControlData.bLastCallResponse = FALSE	
	//Reset last text flags.
	g_savedGlobals.sCommsControlData.eLastCompletedText = COMM_NONE
	g_savedGlobals.sCommsControlData.bLastTextHadResponse = FALSE
	g_savedGlobals.sCommsControlData.bLastTextResponse = FALSE	
	//Reset last email flags.
	g_savedGlobals.sCommsControlData.eLastCompletedEmail = COMM_NONE	
	//reset bitset to zero 
	g_savedGlobals.sCommsControlData.iExileWarningBitset = 0
#endif
#endif
	//Unpause the queue.
	g_bPauseCommsQueues = FALSE
ENDPROC


// PURPOSE:	Cleans up the state of the cellphone when resetting the gameflow.
//
PROC Reset_Cellphone()

	//Cleans out all phone contacts that have been added to the playable characters' phone books.
	REMOVE_ALL_CONTACTS_FROM_ALL_PLAYER_CHARACTERS_PHONEBOOKS()
	
	//Ensure the cellphone is taken out of SLEEP mode
	SET_CELLPHONE_PROFILE_TO_NORMAL()
	
	//Make sure the cellphone is re-enabled if it has been disabled before the restart.
	DISABLE_CELLPHONE(FALSE)
	
	//Clear out all player text message lists.
	PURGE_TEXT_MESSAGES_FOR_ALL_PLAYER_CHARACTERS()
	
	//Stop all ongoing conversations.
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	
ENDPROC


// PURPOSE:	Calls the context system reset procedures.
//
PROC Reset_Context_System()
	FORCE_CONTEXT_SYSTEM_RESET()
ENDPROC


// PURPOSE: Reinitialises the help text chain controller state.
//
PROC Reset_Help_Text_Controller()
	PRIVATE_Clear_Flow_Help_Queue()
	PAUSE_FLOW_HELP_QUEUE(FALSE)
	
	//Clear all "one time" help bits.
	INT index
	REPEAT FLOW_HELP_BITSET_COUNT index
	#if USE_CLF_DLC
		g_savedGlobalsClifford.sFlowHelp.iHelpDisplayedBitset[index] = 0
	#endif
	#if USE_NRM_DLC
		g_savedGlobalsnorman.sFlowHelp.iHelpDisplayedBitset[index] = 0
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		g_savedGlobals.sFlowHelp.iHelpDisplayedBitset[index] = 0
	#endif
	#endif
	ENDREPEAT
	
	g_savedGlobals.sFlowCustom.iFirstPersonCoverHelpCountMission = 0
	g_savedGlobals.sFlowCustom.iFirstPersonCoverHelpCountFlow = 0
ENDPROC


// PURPOSE: Clear out the Replay globals
PROC Reset_Replay_Controller()
	Cleanup_Replay_Controller()
ENDPROC


// PURPOSE: Resets all the ambient saved flags.
//
PROC RESET_AMBIENT_FLAGS()
	g_ambientSavedData sNewData
	g_savedGlobals.sAmbient = sNewData
ENDPROC


// PURPOSE: Resets all the vehicle gen flags.
//
PROC Reset_Vehicle_Gen_Flags()

	g_sVehicleGenNSData.bDisabledForMissions = FALSE
	
	INT i
	REPEAT NUMBER_OF_VEHICLES_TO_GEN i
		// Non-saved data
		g_sVehicleGenNSData.bLeaveAreaBeforeCreating[i] = FALSE
		g_sVehicleGenNSData.bCheckPlayerVehicleCleanupTimer[i] = FALSE
		
		// Saved data
		#if USE_CLF_DLC
			g_savedGlobalsClifford.sVehicleGenData.iProperties[i] = 0
		#endif
		#if USE_NRM_DLC
			g_savedGlobalsnorman.sVehicleGenData.iProperties[i] = 0
		#endif
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			g_savedGlobals.sVehicleGenData.iProperties[i] = 0
		#endif
		#endif
	ENDREPEAT
	
	REPEAT NUMBER_OF_DYNAMIC_VEHICLE_GENS i
		#if USE_CLF_DLC
			g_savedGlobalsClifford.sVehicleGenData.fDynamicHeading[i] = -1
		#endif
		#if USE_NRM_DLC
			g_savedGlobalsnorman.sVehicleGenData.fDynamicHeading[i] = -1
		#endif
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			g_savedGlobals.sVehicleGenData.fDynamicHeading[i] = -1
		#endif		
		#endif
	ENDREPEAT
	
	REPEAT NUMBER_OF_BUYABLE_VEHICLES_SP i
		#if USE_CLF_DLC
			g_savedGlobalsClifford.sVehicleGenData.sWebVehicles[0].todEmailDate[i] = INVALID_TIMEOFDAY
			g_savedGlobalsClifford.sVehicleGenData.sWebVehicles[1].todEmailDate[i] = INVALID_TIMEOFDAY
			g_savedGlobalsClifford.sVehicleGenData.sWebVehicles[2].todEmailDate[i] = INVALID_TIMEOFDAY
		#endif
		#if USE_NRM_DLC
			g_savedGlobalsnorman.sVehicleGenData.sWebVehicles[0].todEmailDate[i] = INVALID_TIMEOFDAY
			g_savedGlobalsnorman.sVehicleGenData.sWebVehicles[1].todEmailDate[i] = INVALID_TIMEOFDAY
			g_savedGlobalsnorman.sVehicleGenData.sWebVehicles[2].todEmailDate[i] = INVALID_TIMEOFDAY
		#endif
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			g_savedGlobals.sVehicleGenData.sWebVehicles[0].todEmailDate[i] = INVALID_TIMEOFDAY
			g_savedGlobals.sVehicleGenData.sWebVehicles[1].todEmailDate[i] = INVALID_TIMEOFDAY
			g_savedGlobals.sVehicleGenData.sWebVehicles[2].todEmailDate[i] = INVALID_TIMEOFDAY
		#endif	
		#endif
	ENDREPEAT
	
	
	REPEAT COUNT_OF(g_savedGlobals.sVehicleGenData.sHeistPrepVehicles) i
		#if USE_CLF_DLC
			g_savedGlobalsClifford.sVehicleGenData.sHeistPrepVehicles[i].eModel = DUMMY_MODEL_FOR_SCRIPT
		#endif
		#if USE_NRM_DLC
			g_savedGlobalsnorman.sVehicleGenData.sHeistPrepVehicles[i].eModel = DUMMY_MODEL_FOR_SCRIPT
		#endif
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			g_savedGlobals.sVehicleGenData.sHeistPrepVehicles[i].eModel = DUMMY_MODEL_FOR_SCRIPT
		#endif		
		#endif
	ENDREPEAT
	
	INT n
	#if USE_CLF_DLC
		REPEAT NUM_OF_PLAYABLE_PEDS n
			REPEAT get_vehicle_setup_array_size(g_savedGlobalsClifford.sVehicleGenData.sImpoundVehicles[n]) i
				g_savedGlobalsClifford.sVehicleGenData.sImpoundVehicles[n][i].eModel = DUMMY_MODEL_FOR_SCRIPT
			ENDREPEAT
			g_savedGlobalsClifford.sVehicleGenData.bImpoundVehicleHelp[n] = FALSE		
			g_savedGlobalsClifford.sVehicleGenData.sImpoundSwitchVehicles[n].eModel = DUMMY_MODEL_FOR_SCRIPT
		ENDREPEAT	
		g_savedGlobalsClifford.sVehicleGenData.sCarForImpound.eModel = DUMMY_MODEL_FOR_SCRIPT
		g_savedGlobalsClifford.sVehicleGenData.bTrackingImpoundVehicle = FALSE	
		g_savedGlobalsClifford.sVehicleGenData.eMissionVehTimeStamp = INVALID_TIMEOFDAY
	#endif
	#if USE_NRM_DLC
		REPEAT NUM_OF_PLAYABLE_PEDS n
			REPEAT get_vehicle_setup_array_size(g_savedGlobalsnorman.sVehicleGenData.sImpoundVehicles[n]) i
				g_savedGlobalsnorman.sVehicleGenData.sImpoundVehicles[n][i].eModel = DUMMY_MODEL_FOR_SCRIPT
			ENDREPEAT
			g_savedGlobalsnorman.sVehicleGenData.bImpoundVehicleHelp[n] = FALSE		
			g_savedGlobalsnorman.sVehicleGenData.sImpoundSwitchVehicles[n].eModel = DUMMY_MODEL_FOR_SCRIPT
		ENDREPEAT	
		g_savedGlobalsnorman.sVehicleGenData.sCarForImpound.eModel = DUMMY_MODEL_FOR_SCRIPT
		g_savedGlobalsnorman.sVehicleGenData.bTrackingImpoundVehicle = FALSE	
		g_savedGlobalsnorman.sVehicleGenData.eMissionVehTimeStamp = INVALID_TIMEOFDAY
	#endif
	
	#if not USE_CLF_DLC
	#if not use_NRM_DLC
		REPEAT NUM_OF_PLAYABLE_PEDS n
			REPEAT get_vehicle_setup_array_size(g_savedGlobals.sVehicleGenData.sImpoundVehicles[n]) i
				g_savedGlobals.sVehicleGenData.sImpoundVehicles[n][i].eModel = DUMMY_MODEL_FOR_SCRIPT
			ENDREPEAT
			g_savedGlobals.sVehicleGenData.bImpoundVehicleHelp[n] = FALSE		
			g_savedGlobals.sVehicleGenData.sImpoundSwitchVehicles[n].eModel = DUMMY_MODEL_FOR_SCRIPT
		ENDREPEAT	
		g_savedGlobals.sVehicleGenData.sCarForImpound.eModel = DUMMY_MODEL_FOR_SCRIPT
		g_savedGlobals.sVehicleGenData.bTrackingImpoundVehicle = FALSE	
		g_savedGlobals.sVehicleGenData.eMissionVehTimeStamp = INVALID_TIMEOFDAY
	#endif	
	#endif
	
ENDPROC


// PURPOSE: Resets the Random Character controller
//
PROC Reset_Random_Character_Controller()
#if not USE_CLF_DLC
#if not use_NRM_DLC
	Reset_Random_Character_Stored_Mission_Details()
	
	//Reset which RCs we've flagged as being revealed in the FOW.
	INT iFOWVisibleBitsetIndex
	REPEAT RC_MAX_FOW_VISIBLE_BITSETS iFOWVisibleBitsetIndex
		g_savedGlobals.sRandomChars.g_iVisibleInFOWBitset[iFOWVisibleBitsetIndex] = 0
	ENDREPEAT	
#endif
#endif
ENDPROC


// PURPOSE: Resets the date stamps for all random events.
//
PROC Reset_Random_Event_States()

	g_iLastSuccessfulAmbientLaunchTime = -61000
	g_iLastRandomEventLaunch = GET_GAME_TIMER()
	g_savedGlobals.sRandomEventData.iBitsetReUnlockA = 0
	g_savedGlobals.sRandomEventData.iBitsetReUnlockB = 0
	
	INT iCountREs
	REPEAT MAX_RANDOM_EVENTS iCountREs
		g_savedGlobals.sRandomEventData.iREVariationComplete[iCountREs] = 0
		SET_TIMEOFDAY_YEAR(g_savedGlobals.sRandomEventData.eTimeBlockUntil[iCountREs], 1999)
	ENDREPEAT
	
	PRINTLN(GET_THIS_SCRIPT_NAME(), " - Reset_Random_Event_States is resetting all RE timestamps.")
ENDPROC


// PURPOSE: Resets the Friend controller
//
PROC Reset_Friend_Controller()
	Initialise_FriendSheet_Global_Variables_On_Startup()
ENDPROC

/// PURPOSE: Cleans up any mission scripts that may be running.
PROC Cleanup_Running_Mission_Scripts()

	// Call Force Cleanup to ensure all running scripts clean up after themselves
	// NOTE: The default flag is the DeathArrest flag - which kills all scripts listening for Deatharrest.
	// (wait a couple of frames to make sure all scripts get the message)
	FORCE_CLEANUP()
	WAIT(0)
	WAIT(0)
		
	// Clean up any scripted cameras that may have been created.
	DESTROY_ALL_CAMS()

	// Ensure the player can move.
	IF (IS_PLAYER_PLAYING(PLAYER_ID()))
		SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
	ENDIF
	
	//Ensure that the proglogue cellphone is deactivated.
	g_Use_Prologue_Cellphone = FALSE
ENDPROC

/// PURPOSE: Ensure the player's ability bar is cleaned up.
PROC Reset_Player_Special_Ability()
	SPECIAL_ABILITY_RESET(PLAYER_ID())
	SPECIAL_ABILITY_LOCK(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
	SPECIAL_ABILITY_LOCK(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))
	SPECIAL_ABILITY_LOCK(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
ENDPROC

/// PURPOSE: Ensure the app data is wiped clean
PROC Reset_App_Data()
	
	RESET_ALL_STORED_APP_DATA()
	
	// B*485914 - Ensure "Download the Chop app" help text doesn't appear
	//            when reloading after setting it in debug_app_select_screen.sc
	#IF NOT USE_CLF_DLC
	#IF NOT USE_NRM_DLC
		#IF IS_DEBUG_BUILD
			IF g_bDebugAppScreenOverride
				IF g_bDebugChopPlayedStatusOverride
					SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_DOWNLOAD_CHOP_APP)
				ENDIF
			ENDIF
		#ENDIF
	#ENDIF
	#ENDIF
ENDPROC


PROC Reset_Player_State()
	
	// Fix for bug 1249700 - Player is warped out of getaway car on completion during Mag Demo 2
	IF g_bMagDemoActive
		EXIT
	ENDIF
	
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			//Get the player out of, and clean up, any vehicle they are in as we gameflow reset.
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
				VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				IF IS_VEHICLE_DRIVEABLE(vehPlayer)
					SET_ENTITY_AS_MISSION_ENTITY(vehPlayer, TRUE, TRUE)
					SET_VEHICLE_AS_NO_LONGER_NEEDED(vehPlayer)
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
	
ENDPROC

PROC RESET_MISSION_STATS()
	INT i
	REPEAT MAX_TRACKED_MISSION_STATS i
		g_MissionStatTrackingArray[i].TrackingEntity = NULL
		
		g_MissionStatTrackingArray[i].iTrackingDelta = 0
		g_MissionStatTrackingArray[i].ivalue = 0
		g_MissionStatTrackingArray[i].bTrackingEntityChanged = FALSE
	ENDREPEAT
ENDPROC

#IF NOT USE_SP_DLC

PROC RESET_PHOTOGRAPHY_COLLECTIBLES_SYSTEMS()
	
	CLEAR_ALL_WILDLIFE_PHOTOGRAPHY_DATA()
	g_bCompletedAnimalPhotosCount = 0

ENDPROC


PROC RESET_ANIMAL_CONTROLLER()
	g_savedGlobals.sAmbient.bPeyoteProgressionComplete = FALSE
	g_savedGlobals.sAmbient.iPeyoteAnimalSeen = 0
	INT iTypeIndex
	REPEAT APT_SECRET iTypeIndex 
		g_savedGlobals.sAmbient.iPeyotePickupOfTypeFound[iTypeIndex] = 0
	ENDREPEAT
ENDPROC


PROC RESET_COUNTRY_RACES()
	g_savedGlobals.sCountryRaceData.iCurrentRace = 0
	g_savedGlobals.sCountryRaceData.iBestTime = 0
	g_savedGlobals.sCountryRaceData.iSlipStreamHelpCount = 0
	g_savedGlobals.sCountryRaceData.bStallionUnlocked = FALSE
	g_savedGlobals.sCountryRaceData.bGauntletUnlocked = FALSE
	g_savedGlobals.sCountryRaceData.bDominatorUnlocked = FALSE
	g_savedGlobals.sCountryRaceData.bBuffaloUnlocked = FALSE
	g_savedGlobals.sCountryRaceData.bMarshallUnlocked = FALSE
	g_savedGlobals.sCountryRaceData.bRaceJustCompleted = FALSE
	g_savedGlobals.sCountryRaceData.bDisrupted = FALSE
	g_savedGlobals.sCountryRaceData.bTextRegistered = FALSE
ENDPROC


#IF FEATURE_SP_DLC_DIRECTOR_MODE
	PROC RESET_DIRECTOR_MODE()
		g_savedGlobals.sDirectorModeData.iBitsetCharacterStoryUnlock = 0
		g_savedGlobals.sDirectorModeData.iBitsetCharacterHeistUnlock = 0
		g_savedGlobals.sDirectorModeData.iBitsetCharacterSpecialUnlock = 0
		g_savedGlobals.sDirectorModeData.iBitsetCharacterAnimalUnlock = 0
		//g_savedGlobals.sDirectorModeData.iBitsetTravelLocationRevealed = 0
		g_savedGlobals.sDirectorModeData.iExitHelpShownCount = 0
	ENDPROC
#ENDIF

#ENDIF



// ===========================================================================================================
//		Gameflow Activation
// ===========================================================================================================

// PURPOSE:	Resets the gameflow variables back to their default state.
// 
PROC RESET_GAMEFLOW()
	CPRINTLN(DEBUG_CLEANUP_SP, "<GENERAL-CLEANUP> Starting gameflow reset.")

	g_flowUnsaved.bUpdatingGameflow = TRUE

	// Kill off any mission scripts that are currently running.
	CLEANUP_RUNNING_MISSION_SCRIPTS()

	// Reset the single player mission flow state.
	RESET_SP_MISSION_FLOW()

	// Reset the state of the communication controller.
	RESET_COMMUNICATION_CONTROLLER()
	
	// Reset the state of the data for the code controller.
	RESET_CODE_CONTROLLER()

	// Clean up the state of the cellphone.
	RESET_CELLPHONE()

	// Reset the complete status of all missions.
	RESET_MISSION_DATA()
	
#IF NOT USE_SP_DLC
	// Reset all heist globals.
	RESET_HEIST_DATA()
#ENDIF

	// Reset the state of all static blips.
	RESET_STATIC_BLIP_STATES()

	// Reset the default respawn location flags.
	RESET_RESPAWN_LOCATION_DATA_FLAGS()

	// Reset the default player info and stat flags.
	RESET_PLAYER_DATA_FLAGS()

	// Reset the default shop flags.
	RESET_SHOP_DATA_FLAGS()

	// Reset the Mission Triggering System.
	RESET_ALL_MISSION_TRIGGERS()

	// Reset the default building and door flags.
	RESET_BUILDING_AND_DOOR_FLAGS()

	// Reset the context system.
	RESET_CONTEXT_SYSTEM()

	// Reset the help text chain controller.
	RESET_HELP_TEXT_CONTROLLER()

	// Reset replay controller
	RESET_REPLAY_CONTROLLER()

	#IF NOT USE_SP_DLC
		// Reset the Random Character controller.
		RESET_RANDOM_CHARACTER_CONTROLLER()
		
		// Reset the state of all random events.
		RESET_RANDOM_EVENT_STATES()	
		
		// Reset the Friend controller
		RESET_FRIEND_CONTROLLER()

		// Reset the wildlife photography competition.
		RESET_PHOTOGRAPHY_COLLECTIBLES_SYSTEMS()
		
		// Reset the peyote animal pickups.
		RESET_ANIMAL_CONTROLLER()
		
		// Reset the country race globals.
		RESET_COUNTRY_RACES()
		
		#IF FEATURE_SP_DLC_DIRECTOR_MODE
			RESET_DIRECTOR_MODE()
		#ENDIF
	#ENDIF

	// Reset the ambient saved flags.
	RESET_AMBIENT_FLAGS()

	// Reset the vehicle gen flags.
	RESET_VEHICLE_GEN_FLAGS()

	// Reset the player's special ability system.
	RESET_PLAYER_SPECIAL_ABILITY()

	// Reset the state of any cutscenes that are currently active.
	RESET_CUTSCENES()
	
	// Resets all the app data
	// Note: This must be called after RESET_HELP_TEXT_CONTROLLER - Dave R
	RESET_APP_DATA()
	
	// Reset 100% completion
	RESET_COMPLETION_PERCENTAGE_TRACKING()
		
	INITIALISE_EMAIL_SYSTEM()
	
	RESET_PLAYER_STATE()
	
	RESET_MISSION_STATS()
	
	g_flowUnsaved.bUpdatingGameflow = FALSE
ENDPROC



// PURPOSE:	Resets the gameflow and then activates the first strand.
// 
PROC ACTIVATE_GAMEFLOW_AT_FIRST_STRAND()
	
	// Set gameflow mode
	#if USE_CLF_DLC
		g_savedGlobalsClifford.sFlow.isGameflowActive = TRUE
	#endif
	#if USE_NRM_DLC
		g_savedGlobalsnorman.sFlow.isGameflowActive = TRUE
	#endif
	#if not USE_SP_DLC
		g_savedGlobals.sFlow.isGameflowActive = TRUE
	#endif
	
	// Reset all gameflow variables to their default state
	RESET_GAMEFLOW()

	// Activate the first strand
	#if USE_CLF_DLC
		CPRINTLN(DEBUG_FLOW,"...Activating the CLF T Strand")	
		SET_BIT(g_savedGlobalsClifford.sFlow.strandSavedVars[STRAND_CLF].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
	#endif
	#if USE_NRM_DLC
		CPRINTLN(DEBUG_FLOW,"...Activating the norman start Strand")	
		SET_BIT(g_savedGlobalsnorman.sFlow.strandSavedVars[STRAND_NRM_SURVIVE].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
	#endif
	#if not USE_SP_DLC
		CPRINTLN(DEBUG_FLOW,"...Activating the PROLOGUE Strand")
		SET_BIT(g_savedGlobals.sFlow.strandSavedVars[STRAND_PROLOGUE].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
	#endif
	
ENDPROC


PROC RUN_GENERAL_SCRIPT_CLEANUP(BOOL rockstarEditorCleanup = FALSE)
	CPRINTLN(DEBUG_CLEANUP_SP, "<GENERAL-CLEANUP> Starting general cleanup.")

	SET_GAME_PAUSED(FALSE)
	
#IF FEATURE_SP_DLC_DIRECTOR_MODE
	//B* 2268960: If Director Mode is running, wait until a data save is performed
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("director_mode")) > 0
		CPRINTLN(DEBUG_CLEANUP_SP,"Director Mode running, forcing autosave")
		SET_REPLAY_SYSTEM_PAUSED_FOR_SAVE(TRUE)
		g_bDirectorForceSaveData = TRUE
		WHILE g_bDirectorForceSaveData
			WAIT(0)
		ENDWHILE
		CPRINTLN(DEBUG_CLEANUP_SP, "Director mode finished saving, continuing general script cleanup")
		SET_REPLAY_SYSTEM_PAUSED_FOR_SAVE(FALSE)
	ENDIF
#ENDIF			
	FORCE_CLEANUP(FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
	FORCE_CLEANUP(FORCE_CLEANUP_FLAG_REPEAT_PLAY)	
	CPRINTLN(DEBUG_CLEANUP_SP, "<GENERAL-CLEANUP> Fired repeat play and random event force cleanups.")
	WAIT(0)
	
	IF IS_CUTSCENE_ACTIVE()
		STOP_CUTSCENE_IMMEDIATELY()
	ENDIF

	IF rockstarEditorCleanup 
		WHILE g_eRunningMission != SP_MISSION_NONE
			CPRINTLN(DEBUG_CLEANUP_SP, "<GENERAL-CLEANUP> Waiting for mission state cleanup before proceeding...")
			CLEAR_AVAILABLE_MISSIONS_ARRAY()
			IF g_eRunningMission != SP_MISSION_NONE
				CPRINTLN(DEBUG_CLEANUP_SP, "<GENERAL-CLEANUP> Force cleaning up running mission script ", g_sMissionStaticData[g_eRunningMission].scriptName, ".")
				FORCE_CLEANUP_FOR_ALL_THREADS_WITH_THIS_NAME(g_sMissionStaticData[g_eRunningMission].scriptName, FORCE_CLEANUP_FLAG_SP_TO_MP)
			ENDIF
			IF g_CurrentlyRunningCandidateThread != NULL
				CPRINTLN(DEBUG_CLEANUP_SP, "<GENERAL-CLEANUP> Force cleaning up running candidate thread.")
				FORCE_CLEANUP_FOR_THREAD_WITH_THIS_ID(g_CurrentlyRunningCandidateThread, FORCE_CLEANUP_FLAG_SP_TO_MP)
				g_CurrentlyRunningCandidateThread = NULL
			ENDIF
			IF g_iCurrentlyRunningCandidateID != NO_CANDIDATE_ID
				CPRINTLN(DEBUG_CLEANUP_SP, "<GENERAL-CLEANUP> Force cleaning up running candidate ID.")
				Mission_Over(g_iCurrentlyRunningCandidateID)
			ENDIF
			
			g_bMissionTriggerFired = FALSE
			g_bCandidateSystemMidProcessing = FALSE
			g_eRunningMission = SP_MISSION_NONE
		ENDWHILE
		
		CLEAR_AREA(<<0,0,0>>,10000,FALSE) //map wide
		CPRINTLN(DEBUG_CLEANUP_SP, "<GENERAL-CLEANUP> ...rockstarEditorCleanup.")
		
		g_bMissionTriggerFired = FALSE
		g_bCandidateSystemMidProcessing = FALSE
		g_eRunningMission = SP_MISSION_NONE
		g_iCurrentlyRunningCandidateID = NO_CANDIDATE_ID
		g_CurrentlyRunningCandidateThread = NULL
		
		//Display to console friends that we're now in the R* Editor.
		SET_RICH_PRESENCE_FOR_SP_ROCKSTAR_EDITOR()
		
	ELSE	
		//Safeguard: Kill off any running mission scripts as part of the cleanup.
		WHILE g_bMissionTriggerFired
		OR g_bCandidateSystemMidProcessing
		OR g_eRunningMission != SP_MISSION_NONE
		OR g_iCurrentlyRunningCandidateID != NO_CANDIDATE_ID
		OR g_CurrentlyRunningCandidateThread != NULL
			CPRINTLN(DEBUG_CLEANUP_SP, "<GENERAL-CLEANUP> Waiting for mission state cleanup before proceeding...")
			CLEAR_AVAILABLE_MISSIONS_ARRAY()
			WAIT(0)
			IF g_eRunningMission != SP_MISSION_NONE
				CPRINTLN(DEBUG_CLEANUP_SP, "<GENERAL-CLEANUP> Force cleaning up running mission script ", g_sMissionStaticData[g_eRunningMission].scriptName, ".")
				FORCE_CLEANUP_FOR_ALL_THREADS_WITH_THIS_NAME(g_sMissionStaticData[g_eRunningMission].scriptName, FORCE_CLEANUP_FLAG_SP_TO_MP)
			ENDIF
			IF g_CurrentlyRunningCandidateThread != NULL
				CPRINTLN(DEBUG_CLEANUP_SP, "<GENERAL-CLEANUP> Force cleaning up running candidate thread.")
				FORCE_CLEANUP_FOR_THREAD_WITH_THIS_ID(g_CurrentlyRunningCandidateThread, FORCE_CLEANUP_FLAG_SP_TO_MP)
				g_CurrentlyRunningCandidateThread = NULL
			ENDIF
			IF g_iCurrentlyRunningCandidateID != NO_CANDIDATE_ID
				CPRINTLN(DEBUG_CLEANUP_SP, "<GENERAL-CLEANUP> Force cleaning up running candidate ID.")
				Mission_Over(g_iCurrentlyRunningCandidateID)
			ENDIF
			
			g_bMissionTriggerFired = FALSE
			g_bCandidateSystemMidProcessing = FALSE
			g_eRunningMission = SP_MISSION_NONE
			WAIT(0)
		ENDWHILE
	ENDIF
	
	CPRINTLN(DEBUG_CLEANUP_SP, "<GENERAL-CLEANUP> Finished mission state cleanup. Now proceeding...")
	
	//Make sure any active cutscene is halted.
	IF IS_CUTSCENE_ACTIVE()
		STOP_CUTSCENE_IMMEDIATELY()
		REMOVE_CUTSCENE()
	ENDIF
	
	//Clear up text on the screen.
	CLEAR_HELP()
	CLEAR_PRINTS()
	THEFEED_FLUSH_QUEUE()

	// Ensure all flow cutscenes are unloaded.	
	WHILE (g_eMissionIDToLoadCutscene != SP_MISSION_NONE)
		IF NOT g_bFlowCleanupIntroCutscene
			CPRINTLN(DEBUG_CLEANUP_SP, "<GENERAL-CLEANUP> Unloading flow cutscene...")
			g_bFlowCleanupIntroCutscene = TRUE
		ENDIF		
		WAIT(0)
	ENDWHILE
	CPRINTLN(DEBUG_CLEANUP_SP, "<GENERAL-CLEANUP> Flow cutscenes unloaded.")
	
	INT iEarlyOut = GET_GAME_TIMER() + 1000 // Early out of 1 second.
	
	WHILE IS_CUTSCENE_ACTIVE()
	AND iEarlyOut > GET_GAME_TIMER() 
		WAIT(0)
		STOP_CUTSCENE_IMMEDIATELY()
		REMOVE_CUTSCENE()
	ENDWHILE
	
	IF iEarlyOut > GET_GAME_TIMER()
		CPRINTLN(DEBUG_CLEANUP_SP, "<GENERAL-CLEANUP> Cleared active cutscene.")
	ELSE
		CPRINTLN(DEBUG_CLEANUP_SP, "<GENERAL-CLEANUP> Failed to clear cut-scene: early out.")
	ENDIF
	
	//Disengage the mission flow + pause comm queues
	g_savedGlobals.sFlow.isGameflowActive = FALSE
	
	// Stop the player being drunk.
	Force_All_Drunk_Peds_To_Become_Sober_And_Quit_Camera()
	
	// If the player has his phone out, put it away.
	HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
	
	//Ensure all shops have their leave area flags cleared.
	CLEAR_LAUNCHED_FLAG_FOR_ALL_SHOPS()
	
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		CPRINTLN(DEBUG_CLEANUP_SP, "<GENERAL-CLEANUP> Clearing wanted levels now.")
		CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
	ENDIF

	CLEAR_GPS_PLAYER_WAYPOINT()
	
	//Take this opportunity to ensure the player is visible and in a clean state before the mission script begins.
	//REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)
	//Michael parachute drawable
	IF	g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_MICHAEL].iDrawableVariation[PED_COMP_SPECIAL2] = 5
		g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_MICHAEL].iDrawableVariation[PED_COMP_SPECIAL2] = 0
	ENDIF
	//Franklin parachute drawable
	IF	g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_FRANKLIN].iDrawableVariation[PED_COMP_SPECIAL] = 1
		g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_FRANKLIN].iDrawableVariation[PED_COMP_SPECIAL] = 0
	ENDIF
	//Trevor parachute drawable
	IF	g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_TREVOR].iDrawableVariation[PED_COMP_SPECIAL] = 3
		g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_TREVOR].iDrawableVariation[PED_COMP_SPECIAL] = 0
	ENDIF
	
	CPRINTLN(DEBUG_CLEANUP_SP, "<GENERAL-CLEANUP> Starting default player model check...")
	WHILE NOT IS_PLAYER_MODEL_PLAYABLE(GET_PLAYER_MODEL())
		REQUEST_MODEL(PLAYER_ZERO)
		IF HAS_MODEL_LOADED(PLAYER_ZERO)
			CPRINTLN(DEBUG_CLEANUP_SP, "<GENERAL-CLEANUP> Swapping out player model to default PLAYER_ZERO.")
			SET_PLAYER_MODEL(PLAYER_ID(), PLAYER_ZERO)
		ENDIF
		WAIT(0)
	ENDWHILE
	CPRINTLN(DEBUG_CLEANUP_SP, "<GENERAL-CLEANUP> ...Finished default player model check.")
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		REMOVE_PED_HELMET(PLAYER_PED_ID(), TRUE)
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
	ENDIF
	
	#IF USE_CLF_DLC
		//always remove the spy vehicle for a repeat play
		spy_vehicle_system_struct temp_spy_veh_system
		spy_vehicle_remove_all_system_assets(temp_spy_veh_system)
	#ENDIF 

ENDPROC



