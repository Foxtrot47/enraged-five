
USING "globals.sch"
USING "rage_builtins.sch"
USING "debug_channels_structs.sch"

USING "flow_mission_data_public.sch"
USING "friend_flow_public.sch"
USING "mission_control_private.sch"
USING "cellphone_public.sch"
USING "flow_help_public.sch"
USING "minigame_blip_support.sch"
USING "cutscene_control_public.sch"
USING "dialogue_public.sch"
USING "comms_control_public.sch"
USING "help_at_location.sch"

#IF IS_DEBUG_BUILD
	USING "flow_debug_core.sch"
	USING "shared_debug.sch"
#ENDIF
#if USE_CLF_DLC
	Using "static_mission_data_helpCLF.sch"
#endif
#if USE_NRM_DLC
	Using "static_mission_data_helpNRM.sch"
#endif

/// PURPOSE:
///    Checks if the player died or got arrested and set the fail reason appropriately
///    Also sets whether the player will be warped to police station / hospital on rejecting replay
PROC SET_FORCE_CLEANUP_FAIL_REASON()
	g_bReplayDoRejectWarp = TRUE 
	
	IF IS_PLAYER_BEING_ARRESTED(PLAYER_ID())
		IF IS_STRING_NULL_OR_EMPTY(g_txtMissionFailReason)
			// only set the fail reason if we don't already have one
			SWITCH GET_CURRENT_PLAYER_PED_ENUM()
				CASE CHAR_MICHAEL
					g_txtMissionFailReason = "CMN_MARRE"
				BREAK
				CASE CHAR_FRANKLIN
					g_txtMissionFailReason = "CMN_FARRE"
				BREAK
				CASE CHAR_TREVOR
					g_txtMissionFailReason = "CMN_TARRE"
				BREAK
			ENDSWITCH
			g_txtMissionFailAddText = ""
		ENDIF
		
		g_bReplayDoRejectWarp = FALSE // player will be warped to the police station, don't warp back to start
		CPRINTLN(DEBUG_REPLAY, "Player arrested- if replay rejected warp to police station.")
		
	ELIF NOT IS_PLAYER_PLAYING(PLAYER_ID())
	
		IF IS_STRING_NULL_OR_EMPTY(g_txtMissionFailReason)
		// only set the fail reason if we don't already have one
	
			SWITCH GET_CURRENT_PLAYER_PED_ENUM()
				CASE CHAR_MICHAEL
					g_txtMissionFailReason = "CMN_MDIED"
				BREAK
				CASE CHAR_FRANKLIN
					g_txtMissionFailReason = "CMN_FDIED"
				BREAK
				CASE CHAR_TREVOR
					g_txtMissionFailReason = "CMN_TDIED"
				BREAK
			ENDSWITCH
			g_txtMissionFailAddText = ""
		ENDIF
		
		g_bReplayDoRejectWarp = FALSE // player will be warped to hospital, don't warp back to start
		CPRINTLN(DEBUG_REPLAY,"Blocking the respawn controller")
		SET_BIT(g_replay.iReplayBits,ENUM_TO_INT(RB_BLOCK_RESPAWN))	// Block the respawn controller from taking over the fail effects
		CPRINTLN(DEBUG_REPLAY, "Player killed- if replay rejected warp to hospital.")
	ELSE
		CPRINTLN(DEBUG_REPLAY, "Player not arrested/killed.")
	ENDIF
ENDPROC


// -----------------------------------------------------------------------------------------------------------
// PURPOSE:	Clears the contents of the comms variables array
// 
PROC CLEAR_COMMS_VARIABLES_ARRAY()
	INT tempLoop = 0
	
	REPEAT MAX_COMMS_VARIABLE_SLOTS tempLoop
		g_flowUnsaved.commsVars[tempLoop].iValue1		= 0
		g_flowUnsaved.commsVars[tempLoop].iValue2		= 0
		g_flowUnsaved.commsVars[tempLoop].iValue3		= 0
		g_flowUnsaved.commsVars[tempLoop].iValue4		= 0
		g_flowUnsaved.commsVars[tempLoop].iValue5		= 0
		g_flowUnsaved.commsVars[tempLoop].iValue6		= 0
		g_flowUnsaved.commsVars[tempLoop].iValue7		= 0
		g_flowUnsaved.commsVars[tempLoop].iValue8		= 0
		g_flowUnsaved.commsVars[tempLoop].eVectorID		= VID_BLANK
		g_flowUnsaved.commsVars[tempLoop].eCodeID		= CID_BLANK
		g_flowUnsaved.commsVars[tempLoop].eCheckID		= FLOW_CHECK_NONE
	ENDREPEAT
ENDPROC


// -----------------------------------------------------------------------------------------------------------
// PURPOSE:	Clears the contents of the comms large variables array
// 
PROC CLEAR_COMMS_LARGE_VARIABLES_ARRAY()
	INT tempLoop = 0
	
	REPEAT MAX_COMMS_LARGE_VARIABLE_SLOTS tempLoop
		g_flowUnsaved.commsLargeVars[tempLoop].iValue1		= 0
		g_flowUnsaved.commsLargeVars[tempLoop].iValue2		= 0
		g_flowUnsaved.commsLargeVars[tempLoop].iValue3		= 0
		g_flowUnsaved.commsLargeVars[tempLoop].iValue4		= 0
		g_flowUnsaved.commsLargeVars[tempLoop].iValue5		= 0
		g_flowUnsaved.commsLargeVars[tempLoop].iValue6		= 0
		g_flowUnsaved.commsLargeVars[tempLoop].iValue7		= 0
		g_flowUnsaved.commsLargeVars[tempLoop].iValue8		= 0
		g_flowUnsaved.commsLargeVars[tempLoop].iValue9		= 0
		g_flowUnsaved.commsLargeVars[tempLoop].eJump1		= STAY_ON_THIS_COMMAND
		g_flowUnsaved.commsLargeVars[tempLoop].eJump2		= STAY_ON_THIS_COMMAND
#IF IS_DEBUG_BUILD
		g_flowUnsaved.commsLargeVars[tempLoop].eJump3		= STAY_ON_THIS_COMMAND
#ENDIF
		g_flowUnsaved.commsLargeVars[tempLoop].eVectorID	= VID_BLANK
		g_flowUnsaved.commsLargeVars[tempLoop].eCodeID		= CID_BLANK
		g_flowUnsaved.commsLargeVars[tempLoop].eCheckID		= FLOW_CHECK_NONE
	ENDREPEAT
ENDPROC


#IF IS_DEBUG_BUILD

PROC PRINT_AVAILABLE_MISSION_ARRAY()

	CDEBUG1LN(DEBUG_FLOW, "----- Available Mission Array -----")
	INT tempLoop
#IF USE_CLF_DLC
	REPEAT MAX_MISSIONS_AVAILABLE_TU tempLoop
		IF g_availableMissionsTU[tempLoop].index != ILLEGAL_ARRAY_POSITION
			CDEBUG1LN(DEBUG_FLOW, "[", tempLoop, "] ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(INT_TO_ENUM(SP_MISSIONS, g_flowUnsaved.coreVars[g_availableMissionsTU[tempLoop].index].iValue1)), " Bitflags:", g_availableMissionsTU[tempLoop].bitflags)
#ENDIF
#IF USE_NRM_DLC
	REPEAT MAX_MISSIONS_AVAILABLE_TU tempLoop
		IF g_availableMissionsTU[tempLoop].index != ILLEGAL_ARRAY_POSITION
			CDEBUG1LN(DEBUG_FLOW, "[", tempLoop, "] ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(INT_TO_ENUM(SP_MISSIONS, g_flowUnsaved.coreVars[g_availableMissionsTU[tempLoop].index].iValue1)), " Bitflags:", g_availableMissionsTU[tempLoop].bitflags)
#ENDIF
#IF NOT USE_CLF_DLC
#IF NOT USE_NRM_DLC
	REPEAT MAX_MISSIONS_AVAILABLE tempLoop
		IF g_availableMissions[tempLoop].index != ILLEGAL_ARRAY_POSITION
			CDEBUG1LN(DEBUG_FLOW, "[", tempLoop, "] ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(INT_TO_ENUM(SP_MISSIONS, g_flowUnsaved.coreVars[g_availableMissions[tempLoop].index].iValue1)), " Bitflags:", g_availableMissions[tempLoop].bitflags)
#ENDIF
#ENDIF
ELSE
			CDEBUG1LN(DEBUG_FLOW, "[", tempLoop, "] Empty") 
		ENDIF
	ENDREPEAT
	CDEBUG1LN(DEBUG_FLOW, "-----------------------------------")
	
ENDPROC

#ENDIF


// PURPOSE:	Clears the contents of one available mission in the array
//
// INPUT PARAMS:		paramArrayPos			Available Mission array position to be cleared 
PROC CLEAR_ONE_AVAILABLE_MISSION(INT paramArrayPos)

#IF USE_CLF_DLC
	g_availableMissionsTU[paramArrayPos].index				= ILLEGAL_ARRAY_POSITION
	g_availableMissionsTU[paramArrayPos].bitflags				= CLEAR_ALL_AVAILABLE_MISSIONS_BITFLAGS
	g_availableMissionsTU[paramArrayPos].missionTriggerID		= NO_CANDIDATE_ID
	g_availableMissionsTU[paramArrayPos].missionCandidateID	= NO_CANDIDATE_ID
	g_availableMissionsTU[paramArrayPos].theThread			= NULL
#ENDIF
#IF USE_NRM_DLC
	g_availableMissionsTU[paramArrayPos].index				= ILLEGAL_ARRAY_POSITION
	g_availableMissionsTU[paramArrayPos].bitflags			= CLEAR_ALL_AVAILABLE_MISSIONS_BITFLAGS
	g_availableMissionsTU[paramArrayPos].missionTriggerID	= NO_CANDIDATE_ID
	g_availableMissionsTU[paramArrayPos].missionCandidateID	= NO_CANDIDATE_ID
	g_availableMissionsTU[paramArrayPos].theThread			= NULL
#ENDIF
#IF NOT USE_CLF_DLC
#IF NOT USE_NRM_DLC
	g_availableMissions[paramArrayPos].index				= ILLEGAL_ARRAY_POSITION
	g_availableMissions[paramArrayPos].bitflags				= CLEAR_ALL_AVAILABLE_MISSIONS_BITFLAGS
	g_availableMissions[paramArrayPos].missionTriggerID		= NO_CANDIDATE_ID
	g_availableMissions[paramArrayPos].missionCandidateID	= NO_CANDIDATE_ID
	g_availableMissions[paramArrayPos].theThread			= NULL
#ENDIF
#ENDIF

ENDPROC

/// PURPOSE:
///    Reset all saved global data used for repeat play
PROC CLEAR_MISSION_FLOW_REPEAT_PLAY_VARIABLES()

#if USE_CLF_DLC
	INT iMissionIndex
	REPEAT MAX_MISSION_DATA_SLOTS iMissionIndex
		// Reset the completion order for repeat play
		g_savedGlobalsClifford.sFlow.missionSavedData[iMissionIndex].iCompletionOrder = -1
	ENDREPEAT
	// reset number of missions the player has completed (used in repeat play system)
	g_savedGlobalsClifford.sFlowCustom.iMissionsCompleted = 0
#endif
#if USE_NRM_DLC
	INT iMissionIndex
	REPEAT MAX_MISSION_DATA_SLOTS iMissionIndex
		// Reset the completion order for repeat play
		g_savedGlobalsnorman.sFlow.missionSavedData[iMissionIndex].iCompletionOrder = -1
	ENDREPEAT
	// reset number of missions the player has completed (used in repeat play system)
	g_savedGlobalsClifford.sFlowCustom.iMissionsCompleted = 0
#endif

#if not USE_CLF_DLC
#if not USE_NRM_DLC
	INT iMissionIndex
	REPEAT MAX_MISSION_DATA_SLOTS iMissionIndex
		// Reset the completion order for repeat play
		g_savedGlobals.sFlow.missionSavedData[iMissionIndex].iCompletionOrder = -1
	ENDREPEAT
	REPEAT MAX_RC_MISSIONS iMissionIndex 
		// Reset the completion order for repeat play 
		g_savedGlobals.sRandomChars.savedRC[iMissionIndex].iCompletionOrder = -1
	ENDREPEAT	
	// Reset completion order for RE Domestic
	g_savedGlobals.sRandomChars.g_iREDomesticCompOrder = -1	
	// reset number of missions the player has completed (used in repeat play system)
	g_savedGlobals.sFlowCustom.iMissionsCompleted = 0
	g_savedGlobals.sRandomChars.iRCMissionsCompleted = 0
#endif
#endif

ENDPROC



// ===========================================================================================================
//		AVAILABLE MISSION functions
// ===========================================================================================================

// PURPOSE:	Checks if an available mission is already registered in the 'available missions' array.
//
// INPUT PARAMS:		paramCoreVarsArrayPos		Index into the Core Variables array
// RETURN VALUE:		INT							Available Missions array position if found, otherwise ILLEGAL_ARRAY_POSITION.
//
// NOTES:	Registration is checked by comparing this mission's index into the 'mission variables' array
//				with the stored 'mission variables' array indexes of all 'available missions'.

FUNC INT GET_AVAILABLE_MISSION_INDEX_FOR_STORED_MISSION(INT paramCoreVarsArrayPos)

	INT tempLoop = 0
	
#IF USE_CLF_DLC
	REPEAT MAX_MISSIONS_AVAILABLE_TU tempLoop
		IF (g_availableMissionsTU[tempLoop].index = paramCoreVarsArrayPos)
#ENDIF
#IF USE_NRM_DLC
	REPEAT MAX_MISSIONS_AVAILABLE_TU tempLoop
		IF (g_availableMissionsTU[tempLoop].index = paramCoreVarsArrayPos)
#ENDIF
#IF NOT USE_CLF_DLC
#IF NOT USE_NRM_DLC
	REPEAT MAX_MISSIONS_AVAILABLE tempLoop
		IF (g_availableMissions[tempLoop].index = paramCoreVarsArrayPos)
#ENDIF
#ENDIF
// Already registered
			RETURN tempLoop
		ENDIF
	ENDREPEAT
	
	// Not registered
	RETURN ILLEGAL_ARRAY_POSITION

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Registers a new mission as 'available' by updating the Mission Flow's 'available missions' array.
//
// INPUT PARAMS:		paramCoreVarsArrayPos		Index into the Core Variables array
// RETURN VALUE:		INT							'available mission' array index, or ILLEGAL_ARRAY_POSITION
//
// NOTES:	If unregistered, the array index into the mission variables array for this mission is stored
//				in the 'available missions' array.

FUNC INT REGISTER_AVAILABLE_MISSION(INT paramCoreVarsArrayPos)
	
	// The array position should always be valid
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW, "Register_Available_Mission: Array Index into Core Variables array is ILLEGAL_ARRAY_POSITION", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(INT_TO_ENUM(SP_MISSIONS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1)))
		RETURN ILLEGAL_ARRAY_POSITION
	ENDIF
	
	// Nothing to do if the mission is already registered
	INT availableMissionIndex = GET_AVAILABLE_MISSION_INDEX_FOR_STORED_MISSION(paramCoreVarsArrayPos)
	
	IF NOT (availableMissionIndex = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW, "Register_Available_Mission: Attempting to register this mission as 'available' more than once. Returning originally-registered array index.", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(INT_TO_ENUM(SP_MISSIONS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1)))
		RETURN availableMissionIndex
	ENDIF
	
	// Find a free slot
	INT		tempLoop				= 0
	INT		freeSlot 				= ILLEGAL_ARRAY_POSITION
	BOOL	searchingForFreeSlot	= TRUE
	
	WHILE (searchingForFreeSlot)
	
	#IF USE_CLF_DLC
		IF (g_availableMissionsTU[tempLoop].index = ILLEGAL_ARRAY_POSITION)
	#ENDIF
	#IF USE_NRM_DLC
		IF (g_availableMissionsTU[tempLoop].index = ILLEGAL_ARRAY_POSITION)
	#ENDIF
	#IF NOT USE_CLF_DLC
	#IF NOT USE_NRM_DLC
		IF (g_availableMissions[tempLoop].index = ILLEGAL_ARRAY_POSITION)
	#ENDIF
	#ENDIF
			freeSlot				= tempLoop
			searchingForFreeSlot	= FALSE
		ELSE			
			#IF USE_CLF_DLC
				tempLoop++
				IF (tempLoop >= MAX_MISSIONS_AVAILABLE_TU)
					searchingForFreeSlot = FALSE
				ENDIF
			#ENDIF
			#IF USE_NRM_DLC
				tempLoop++
				IF (tempLoop >= MAX_MISSIONS_AVAILABLE_TU)
					searchingForFreeSlot = FALSE
				ENDIF
			#ENDIF
			#IF NOT USE_CLF_DLC
			#IF NOT USE_NRM_DLC
				tempLoop++
				IF (tempLoop >= MAX_MISSIONS_AVAILABLE)
					searchingForFreeSlot = FALSE
				ENDIF
			#ENDIF
			#ENDIF
		ENDIF
	ENDWHILE
	
	// Check if a free slot was found
	IF (freeSlot = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW, "Register_Available_Mission: Failed to find a free slot in the Available Missions array. May need to increase MAX_MISSIONS_AVAILABLE.", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(INT_TO_ENUM(SP_MISSIONS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1)))
		RETURN ILLEGAL_ARRAY_POSITION
	ENDIF
	
	// A free slot was found, so store the details
#IF USE_CLF_DLC
	g_availableMissionsTU[freeSlot].index	= paramCoreVarsArrayPos
#ENDIF
#IF USE_NRM_DLC
	g_availableMissionsTU[freeSlot].index	= paramCoreVarsArrayPos
#ENDIF
#IF NOT USE_CLF_DLC
#IF NOT USE_NRM_DLC
	g_availableMissions[freeSlot].index	= paramCoreVarsArrayPos
#ENDIF
#ENDIF

	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_FLOW, "Mission ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(INT_TO_ENUM(SP_MISSIONS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1)), " successfully registered as available.")
		PRINT_AVAILABLE_MISSION_ARRAY()
	#ENDIF
	
	RETURN freeSlot

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Unregisters a previously 'available' mission by clearing the 'available missions' array entry.
//
// INPUT PARAMS:		paramCoreVarsArrayPos		Index into the Core Variables array
//
// NOTES:	If registered, the array index into the mission variables array for this mission is found
//				in the 'available missions' array and the data is cleared.
//			The search for the mission variables array entry is intended to be a safeguard against bugs
//				like deleting the wrong 'available mission' or an unregistered mission.

PROC UNREGISTER_MISSION_PREVIOUSLY_AVAILABLE(INT paramCoreVarsArrayPos)

	// The array position should always be valid
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		#if USE_CLF_DLC
			CERRORLN(DEBUG_FLOW, "Unregister_Mission_Previously_Available: Array Index into Mission Variables array is ILLEGAL_ARRAY_POSITION", GET_SCRIPT_FROM_HASH(g_sMissionStaticData[g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1].scriptHash))	
		#endif	
		#if USE_NRM_DLC
			CERRORLN(DEBUG_FLOW, "Unregister_Mission_Previously_Available: Array Index into Mission Variables array is ILLEGAL_ARRAY_POSITION", GET_SCRIPT_FROM_HASH_NRM(g_sMissionStaticData[g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1].scriptHash))	
		#endif	
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			CERRORLN(DEBUG_FLOW, "Unregister_Mission_Previously_Available: Array Index into Mission Variables array is ILLEGAL_ARRAY_POSITION", g_sMissionStaticData[g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1].scriptName)
		#endif	
		#endif
		EXIT
	ENDIF
	
	
	// Ensure the mission is already registered
	INT availableMissionIndex = GET_AVAILABLE_MISSION_INDEX_FOR_STORED_MISSION(paramCoreVarsArrayPos)
	
	IF (availableMissionIndex = ILLEGAL_ARRAY_POSITION)
		#if USE_CLF_DLC
			CERRORLN(DEBUG_FLOW, "Unregister_Mission_Previously_Available: Mission is not stored on the 'available missions' array", GET_SCRIPT_FROM_HASH(g_sMissionStaticData[g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1].scriptHash))	
		#endif	
		#if USE_NRM_DLC
			CERRORLN(DEBUG_FLOW, "Unregister_Mission_Previously_Available: Mission is not stored on the 'available missions' array", GET_SCRIPT_FROM_HASH_NRM(g_sMissionStaticData[g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1].scriptHash))	
		#endif
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			CERRORLN(DEBUG_FLOW, "Unregister_Mission_Previously_Available: Mission is not stored on the 'available missions' array", g_sMissionStaticData[g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1].scriptName)
		#endif	
		#endif
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			#if USE_CLF_DLC
				CERRORLN(DEBUG_FLOW, "......Unregister mission because no longer Available: ", GET_SCRIPT_FROM_HASH(g_sMissionStaticData[g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1].scriptHash))	
			#endif	
			#if USE_NRM_DLC
				CERRORLN(DEBUG_FLOW, "......Unregister mission because no longer Available: ", GET_SCRIPT_FROM_HASH_NRM(g_sMissionStaticData[g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1].scriptHash))	
			#endif	
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC
				CERRORLN(DEBUG_FLOW, "......Unregister mission because no longer Available: ", g_sMissionStaticData[g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1].scriptName)
			#endif
			#endif
		ENDIF
	#ENDIF
	
	// Clear out these 'available mission' details
	CLEAR_ONE_AVAILABLE_MISSION(availableMissionIndex)
	
	#IF IS_DEBUG_BUILD
		PRINT_AVAILABLE_MISSION_ARRAY()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Searches the Available Missions array for a 'running' mission.
//
// RETURN VALUE:		INT							Available Missions array position if found, otherwise ILLEGAL_ARRAY_POSITION.

FUNC INT GET_AVAILABLE_MISSION_INDEX_FOR_RUNNING_MISSION()

	INT tempLoop = 0
	
	#IF IS_DEBUG_BUILD
		PRINT_AVAILABLE_MISSION_ARRAY()
	#ENDIF
	
#IF USE_CLF_DLC
	REPEAT MAX_MISSIONS_AVAILABLE_TU tempLoop
		IF (IS_BIT_SET(g_availableMissionsTU[tempLoop].bitflags, BITS_AVAILABLE_MISSION_RUNNING))
#ENDIF
#IF USE_NRM_DLC
	REPEAT MAX_MISSIONS_AVAILABLE_TU tempLoop
		IF (IS_BIT_SET(g_availableMissionsTU[tempLoop].bitflags, BITS_AVAILABLE_MISSION_RUNNING))
#ENDIF
#IF NOT USE_CLF_DLC
#IF NOT USE_NRM_DLC
	REPEAT MAX_MISSIONS_AVAILABLE tempLoop
		IF (IS_BIT_SET(g_availableMissions[tempLoop].bitflags, BITS_AVAILABLE_MISSION_RUNNING))
#ENDIF
#ENDIF
CDEBUG3LN(DEBUG_FLOW, "GET_AVAILABLE_MISSION_INDEX_FOR_RUNNING_MISSION: Running mission was available mission ", tempLoop, ".")
			// Found a Running Script
			RETURN tempLoop
		ENDIF
	ENDREPEAT
	
	// No script is running
	CDEBUG3LN(DEBUG_FLOW, "GET_AVAILABLE_MISSION_INDEX_FOR_RUNNING_MISSION: Running mission was not found. Returning ", ILLEGAL_ARRAY_POSITION, ".")
	RETURN ILLEGAL_ARRAY_POSITION

ENDFUNC

// ===========================================================================================================
//		MISSION TERMINATION functions
//		(also see Flow_Public.sch)
// ===========================================================================================================


// PURPOSE:	Gets called by both the Mission Fail and the Mission Force_Cleanup routines to handle common requirements.
// 
// INPUT PARAMS:	paramAvailableArrayPos		Array Position of running mission within Available Missions array
// RETURN VALUE:	BOOL						TRUE if everything ok, FALSE if error
FUNC BOOL MISSION_FLOW_MISSION_FAILED_AND_FORCE_CLEANUP(INT paramAvailableArrayPos)

	// Set whether player gets warped when rejecting replay + update fail reason if killed / arrested
	SET_FORCE_CLEANUP_FAIL_REASON()

	//Clean up wanted level.
	IF (IS_PLAYER_PLAYING(PLAYER_ID()))
        START_FIRING_AMNESTY()
    ENDIF
    
    // Set the missions's leaveArea flag.
#IF USE_CLF_DLC
    INT availableMissionIndex   = g_availableMissionsTU[paramAvailableArrayPos].index
#ENDIF
#IF USE_NRM_DLC
    INT availableMissionIndex   = g_availableMissionsTU[paramAvailableArrayPos].index
#ENDIF
#IF NOT USE_CLF_DLC
#IF NOT USE_NRM_DLC
    INT availableMissionIndex   = g_availableMissions[paramAvailableArrayPos].index
#ENDIF
#ENDIF
INT missionDataIndex        = g_flowUnsaved.coreVars[availableMissionIndex].iValue1
	Set_Leave_Area_Flag_For_Mission(INT_TO_ENUM(SP_MISSIONS, missionDataIndex), TRUE)
	
    // Tell code to update the player's Special Ability index on mission failed
    SPECIAL_ABILITY_CHARGE_ON_MISSION_FAILED(PLAYER_ID())
	SPECIAL_ABILITY_DEACTIVATE(PLAYER_ID())

	//Reset the last known ped info on a fail/cleanup
	#if USE_CLF_DLC
		ResetLastKnownPedInfo(g_savedGlobalsClifford.sPlayerData.sInfo, INT_TO_ENUM(SP_MISSIONS, missionDataIndex))
		//Increment the shitskip counter but only if the retry value is the same as last time
		IF g_iLastObservedRetryStatusOnFail = g_replayMissionStage
	    	g_savedGlobalsClifford.sFlow.missionSavedData[missionDataIndex].missionFailsNoProgress++
			CPRINTLN(DEBUG_REPLAY, "Mission fail without progress counter increased to ",g_savedGlobalsClifford.sFlow.missionSavedData[missionDataIndex].missionFailsNoProgress)
		ENDIF	
		//Check if this mission has a fail screen. If so block code from fading the screen in.
		IF NOT IS_BIT_SET(g_sMissionStaticData[missionDataIndex].settingsBitset, MF_INDEX_NO_FAIL)
			IF NOT IS_PLAYER_PLAYING(PLAYER_ID())
				//	We should only get in here if the mission failed because the player was killed or arrested
				SET_FADE_IN_AFTER_DEATH_ARREST(FALSE)
			ENDIF
		ENDIF	
		//Increment the total fail counter.
		g_savedGlobalsClifford.sFlow.missionSavedData[missionDataIndex].missionFailsTotal++	
		g_iLastObservedRetryStatusOnFail = g_replayMissionStage
		// Ensure there is a running mission (but only in gameflow mode)
		IF (paramAvailableArrayPos = ILLEGAL_ARRAY_POSITION)
			IF (g_savedGlobalsClifford.sFlow.isGameflowActive)
				CERRORLN(DEBUG_FLOW, "Mission_Flow_Mission_Failed_And_Force_Cleanup: Failed to find an Available Mission that is RUNNING")
			ENDIF		
			RETURN FALSE
		ENDIF
		
		// Found a Running Mission, so ensure it hasn't already PASSED or FAILED
		// ...check for previous pass
		IF (IS_BIT_SET(g_availableMissionsTU[paramAvailableArrayPos].bitflags, BITS_AVAILABLE_MISSION_PASSED))
			CERRORLN(DEBUG_FLOW, "Flow_Mission_Failed: Trying to Pass a RUNNING Available Mission that has already Passed - Tell BenR", GET_SCRIPT_FROM_HASH(g_sMissionStaticData[missionDataIndex].scriptHash))
			RETURN FALSE
		ENDIF

		// ...check for previous fail
		IF (IS_BIT_SET(g_availableMissionsTU[paramAvailableArrayPos].bitflags, BITS_AVAILABLE_MISSION_FAILED))
			CERRORLN(DEBUG_FLOW, "Flow_Mission_Failed: Trying to Fail a RUNNING Available Mission that has already Failed - Tell BenR", GET_SCRIPT_FROM_HASH(g_sMissionStaticData[missionDataIndex].scriptHash))
			RETURN FALSE
		ENDIF	
	#endif
	#if USE_NRM_DLC
		ResetLastKnownPedInfo(g_savedGlobalsnorman.sPlayerData.sInfo, INT_TO_ENUM(SP_MISSIONS, missionDataIndex))
		//Increment the shitskip counter but only if the retry value is the same as last time
		IF g_iLastObservedRetryStatusOnFail = g_replayMissionStage
	    	g_savedGlobalsnorman.sFlow.missionSavedData[missionDataIndex].missionFailsNoProgress++
			CPRINTLN(DEBUG_REPLAY, "Mission fail without progress counter increased to ",g_savedGlobalsnorman.sFlow.missionSavedData[missionDataIndex].missionFailsNoProgress)
		ENDIF	
		//Check if this mission has a fail screen. If so block code from fading the screen in.
		IF NOT IS_BIT_SET(g_sMissionStaticData[missionDataIndex].settingsBitset, MF_INDEX_NO_FAIL)
			IF NOT IS_PLAYER_PLAYING(PLAYER_ID())
				//	We should only get in here if the mission failed because the player was killed or arrested
				SET_FADE_IN_AFTER_DEATH_ARREST(FALSE)
			ENDIF
		ENDIF	
		//Increment the total fail counter.
		g_savedGlobalsnorman.sFlow.missionSavedData[missionDataIndex].missionFailsTotal++	
		g_iLastObservedRetryStatusOnFail = g_replayMissionStage
		// Ensure there is a running mission (but only in gameflow mode)
		IF (paramAvailableArrayPos = ILLEGAL_ARRAY_POSITION)
			IF (g_savedGlobalsnorman.sFlow.isGameflowActive)
				CERRORLN(DEBUG_FLOW, "Mission_Flow_Mission_Failed_And_Force_Cleanup: Failed to find an Available Mission that is RUNNING")
			ENDIF		
			RETURN FALSE
		ENDIF
		
		// Found a Running Mission, so ensure it hasn't already PASSED or FAILED
		// ...check for previous pass
		IF (IS_BIT_SET(g_availableMissionsTU[paramAvailableArrayPos].bitflags, BITS_AVAILABLE_MISSION_PASSED))
			CERRORLN(DEBUG_FLOW, "Flow_Mission_Failed: Trying to Pass a RUNNING Available Mission that has already Passed - Tell BenR", GET_SCRIPT_FROM_HASH_NRM(g_sMissionStaticData[missionDataIndex].scriptHash))
			RETURN FALSE
		ENDIF

		// ...check for previous fail
		IF (IS_BIT_SET(g_availableMissionsTU[paramAvailableArrayPos].bitflags, BITS_AVAILABLE_MISSION_FAILED))
			CERRORLN(DEBUG_FLOW, "Flow_Mission_Failed: Trying to Fail a RUNNING Available Mission that has already Failed - Tell BenR", GET_SCRIPT_FROM_HASH_NRM(g_sMissionStaticData[missionDataIndex].scriptHash))
			RETURN FALSE
		ENDIF	
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		ResetLastKnownPedInfo(g_savedGlobals.sPlayerData.sInfo, INT_TO_ENUM(SP_MISSIONS, missionDataIndex))
		//Increment the shitskip counter but only if the retry value is the same as last time
		IF g_iLastObservedRetryStatusOnFail = g_replayMissionStage
	    	g_savedGlobals.sFlow.missionSavedData[missionDataIndex].missionFailsNoProgress++
			CPRINTLN(DEBUG_REPLAY, "Mission fail without progress counter increased to ",g_savedGlobals.sFlow.missionSavedData[missionDataIndex].missionFailsNoProgress)
		ENDIF	
		//Check if this mission has a fail screen. If so block code from fading the screen in.
		IF NOT IS_BIT_SET(g_sMissionStaticData[missionDataIndex].settingsBitset, MF_INDEX_NO_FAIL)
			IF NOT IS_PLAYER_PLAYING(PLAYER_ID())
				//	We should only get in here if the mission failed because the player was killed or arrested
				SET_FADE_IN_AFTER_DEATH_ARREST(FALSE)
			ENDIF
		ENDIF	
		//Increment the total fail counter.
		g_savedGlobals.sFlow.missionSavedData[missionDataIndex].missionFailsTotal++	
		g_iLastObservedRetryStatusOnFail = g_replayMissionStage
		// Ensure there is a running mission (but only in gameflow mode)
		IF (paramAvailableArrayPos = ILLEGAL_ARRAY_POSITION)
			IF (g_savedGlobals.sFlow.isGameflowActive)
				CERRORLN(DEBUG_FLOW, "Mission_Flow_Mission_Failed_And_Force_Cleanup: Failed to find an Available Mission that is RUNNING")
			ENDIF		
			RETURN FALSE
		ENDIF
		
		// Found a Running Mission, so ensure it hasn't already PASSED or FAILED
		// ...check for previous pass
		IF (IS_BIT_SET(g_availableMissions[paramAvailableArrayPos].bitflags, BITS_AVAILABLE_MISSION_PASSED))
			CERRORLN(DEBUG_FLOW, "Flow_Mission_Failed: Trying to Pass a RUNNING Available Mission that has already Passed - Tell BenR", g_sMissionStaticData[missionDataIndex].scriptName)
			RETURN FALSE
		ENDIF

		// ...check for previous fail
		IF (IS_BIT_SET(g_availableMissions[paramAvailableArrayPos].bitflags, BITS_AVAILABLE_MISSION_FAILED))
			CERRORLN(DEBUG_FLOW, "Flow_Mission_Failed: Trying to Fail a RUNNING Available Mission that has already Failed - Tell BenR", g_sMissionStaticData[missionDataIndex].scriptName)
			RETURN FALSE
		ENDIF	
	#endif
	#endif
	
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Sets all motorbikes to be suppressed / not suppressed
/// PARAMS:
///    bSuppress - are we suppressing the bikes?
PROC SUPPRESS_MOTORBIKES(BOOL bSuppress)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(FAGGIO2, bSuppress)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(HEXER, bSuppress)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(POLICEB, bSuppress)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(SANCHEZ, bSuppress)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(VADER, bSuppress)
ENDPROC


FUNC INT PRIVATE_Register_Flow_Cutscene_Handle(STRING paramHandle)

	//CDEBUG1LN(DEBUG_FLOW, "Attempting to register cutscene handle \"", paramHandle, "\".")

	IF NOT IS_STRING_NULL_OR_EMPTY(paramHandle)
		INT iHandleIndex = -1
		
		INT index = 0
		WHILE index < g_iFlowCurrentCutsceneHandleCount AND iHandleIndex = -1
			IF ARE_STRINGS_EQUAL(g_txtFlowCutsceneHandles[index], paramHandle)
				iHandleIndex = index
			ENDIF
			index++
		ENDWHILE

		IF iHandleIndex = -1
			IF g_iFlowCurrentCutsceneHandleCount < MAX_QUEUED_CUTSCENE_HANDLES
				//CDEBUG1LN(DEBUG_FLOW, "Handle was not already registed. Creating new handle at index ", g_iFlowCurrentCutsceneHandleCount, ".")
				g_txtFlowCutsceneHandles[g_iFlowCurrentCutsceneHandleCount] = paramHandle
				g_iFlowCurrentCutsceneHandleCount++
				RETURN g_iFlowCurrentCutsceneHandleCount - 1
			ELSE
				SCRIPT_ASSERT("PRIVATE_Register_Flow_Cutscene_Handle: Too many flow cutscene handles registered. Does MAX_QUEUED_CUTSCENE_HANDLES need increasing? Bug BenR.")
				RETURN -1
			ENDIF
		ELSE
			//CDEBUG1LN(DEBUG_FLOW, "Handle was already registered at index ", iHandleIndex, ".")
			RETURN iHandleIndex
		ENDIF
	ELSE
		SCRIPT_ASSERT("PRIVATE_Register_Flow_Cutscene_Handle: Null or empty cutscene handle passed.")
		RETURN -1
	ENDIF
ENDFUNC


PROC PRIVATE_Cleanup_Cutscene_Handles()
	INT index
	REPEAT g_iFlowCurrentCutsceneHandleCount index
		g_txtFlowCutsceneHandles[index] = ""
	ENDREPEAT
	g_iFlowCurrentCutsceneHandleCount = 0
	//CDEBUG1LN(DEBUG_FLOW, "Intro cutscene variation handles were cleaned up.")
ENDPROC


FUNC enumCharacterList PRIVATE_Get_Player_Ped_Enum_From_Scene_Handle(STRING paramHandle)
	IF ARE_STRINGS_EQUAL(paramHandle, "Michael")
		RETURN CHAR_MICHAEL
	ELIF ARE_STRINGS_EQUAL(paramHandle, "Franklin")
		RETURN CHAR_FRANKLIN
	ELIF ARE_STRINGS_EQUAL(paramHandle, "Trevor")
		RETURN CHAR_TREVOR
	ENDIF
	RETURN CHAR_BLANK_ENTRY
ENDFUNC


PROC PRIVATE_Set_Story_Mission_First_Activated_Bit(SP_MISSIONS paramMission)
	INT iBit = ENUM_TO_INT(paramMission)
	INT iBitset = 0
	WHILE iBit > 31
		iBit -= 32
		iBitset++
	ENDWHILE
	#IF IS_DEBUG_BUILD
		IF iBitset > NO_MISSION_ACTIVATE_BITSETS
			SCRIPT_ASSERT("PRIVATE_Set_Story_Mission_First_Activated_Bit: Not enough first activate bitsets to hold all SP missions. Increase NO_MISSION_ACTIVATE_BITSETS.")
		ENDIF
	#ENDIF
	#if USE_CLF_DLC
		SET_BIT(g_savedGlobalsClifford.sFlowCustom.missionFirstActivateBitset[iBitset], iBit)
	#endif
	#if USE_NRM_DLC
		SET_BIT(g_savedGlobalsnorman.sFlowCustom.missionFirstActivateBitset[iBitset], iBit)
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		SET_BIT(g_savedGlobals.sFlowCustom.missionFirstActivateBitset[iBitset], iBit)
	#endif
	#endif
ENDPROC


FUNC BOOL PRIVATE_Is_Story_Mission_First_Activated_Bit_Set(SP_MISSIONS paramMission)
	INT iBit = ENUM_TO_INT(paramMission)
	INT iBitset = 0
	WHILE iBit > 31
		iBit -= 32
		iBitset++
	ENDWHILE
	#IF IS_DEBUG_BUILD
		IF iBitset > NO_MISSION_ACTIVATE_BITSETS
			SCRIPT_ASSERT("PRIVATE_Is_Story_Mission_First_Activated_Bit_Set: Not enough first activate bitsets to hold all SP missions. Increase NO_MISSION_ACTIVATE_BITSETS.")
		ENDIF
	#ENDIF
	#if USE_CLF_DLC
		RETURN IS_BIT_SET(g_savedGlobalsClifford.sFlowCustom.missionFirstActivateBitset[iBitset], iBit)	
	#endif
	#if USE_NRM_DLC
		RETURN IS_BIT_SET(g_savedGlobalsnorman.sFlowCustom.missionFirstActivateBitset[iBitset], iBit)	
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		RETURN IS_BIT_SET(g_savedGlobals.sFlowCustom.missionFirstActivateBitset[iBitset], iBit)	
	#endif
	#endif
ENDFUNC


// ===========================================================================================================
//		GTA V specific flow command hash ID generation
// ===========================================================================================================

FUNC INT GET_STRAND_INDEX_FOR_COMMAND_INDEX(INT iCommandIndex)
	INT iStrandIndex
	#if USE_CLF_DLC
		REPEAT MAX_STRANDS_CLF iStrandIndex
			IF iCommandIndex >= g_flowUnsaved.flowCommandsIndex[iStrandIndex].lower
			AND iCommandIndex <= g_flowUnsaved.flowCommandsIndex[iStrandIndex].upper
				RETURN iStrandIndex
			ENDIF
		ENDREPEAT	
	#endif
	#if USE_NRM_DLC
		REPEAT MAX_STRANDS_NRM iStrandIndex
			IF iCommandIndex >= g_flowUnsaved.flowCommandsIndex[iStrandIndex].lower
			AND iCommandIndex <= g_flowUnsaved.flowCommandsIndex[iStrandIndex].upper
				RETURN iStrandIndex
			ENDIF
		ENDREPEAT	
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		REPEAT MAX_STRANDS iStrandIndex
			IF iCommandIndex >= g_flowUnsaved.flowCommandsIndex[iStrandIndex].lower
			AND iCommandIndex <= g_flowUnsaved.flowCommandsIndex[iStrandIndex].upper
				RETURN iStrandIndex
			ENDIF
		ENDREPEAT	
	#endif
	#endif
	
	SCRIPT_ASSERT("GET_STRAND_INDEX_FOR_COMMAND_INDEX: Couldn't find valid strand index for command index.")
	RETURN -1
ENDFUNC


PROC GET_FLOW_COMMAND_HASH_STRING_COMMS(INT iCommandIndex, TEXT_LABEL_63 &txtToHash)
	TEXT_LABEL_63 txtTemp
	
	txtToHash = ENUM_TO_INT(g_flowUnsaved.flowCommands[iCommandIndex].command)			
	txtToHash += GET_STRAND_INDEX_FOR_COMMAND_INDEX(iCommandIndex)
	
	txtTemp = g_flowUnsaved.commsVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue1
	txtTemp += g_flowUnsaved.commsVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue2
	txtToHash += GET_HASH_KEY(txtTemp)
	
	txtTemp = g_flowUnsaved.commsVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue3
	txtTemp += g_flowUnsaved.commsVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue4
	txtToHash += GET_HASH_KEY(txtTemp)
	
	txtTemp = g_flowUnsaved.commsVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue5
	txtTemp += g_flowUnsaved.commsVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue6
	txtToHash += GET_HASH_KEY(txtTemp)
	
	txtTemp = g_flowUnsaved.commsVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue7
	txtTemp += g_flowUnsaved.commsVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue8
	txtTemp += ENUM_TO_INT(g_flowUnsaved.commsVars[g_flowUnsaved.flowCommands[iCommandIndex].index].eVectorID)
	txtToHash += GET_HASH_KEY(txtTemp)
ENDPROC


PROC GET_FLOW_COMMAND_HASH_STRING_COMMS_LARGE(INT iCommandIndex, TEXT_LABEL_63 &txtToHash)
	TEXT_LABEL_63 txtTemp

	txtToHash = ENUM_TO_INT(g_flowUnsaved.flowCommands[iCommandIndex].command)
	txtToHash += GET_STRAND_INDEX_FOR_COMMAND_INDEX(iCommandIndex)
	
	txtTemp = g_flowUnsaved.commsLargeVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue1
	txtTemp += g_flowUnsaved.commsLargeVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue2
	txtTemp += g_flowUnsaved.commsLargeVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue3
	txtToHash += GET_HASH_KEY(txtTemp)

	txtTemp = g_flowUnsaved.commsLargeVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue4
	txtTemp += g_flowUnsaved.commsLargeVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue5
	txtTemp += g_flowUnsaved.commsLargeVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue6
	txtToHash += GET_HASH_KEY(txtTemp)
	
	txtTemp = g_flowUnsaved.commsLargeVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue7
	txtTemp += g_flowUnsaved.commsLargeVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue8
	txtTemp += g_flowUnsaved.commsLargeVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue9
	txtToHash += GET_HASH_KEY(txtTemp)

	txtTemp = ENUM_TO_INT(g_flowUnsaved.commsLargeVars[g_flowUnsaved.flowCommands[iCommandIndex].index].eJump1)
	txtTemp += ENUM_TO_INT(g_flowUnsaved.commsLargeVars[g_flowUnsaved.flowCommands[iCommandIndex].index].eJump2)
	txtTemp += ENUM_TO_INT(g_flowUnsaved.commsLargeVars[g_flowUnsaved.flowCommands[iCommandIndex].index].eVectorID)
	txtTemp += ENUM_TO_INT(g_flowUnsaved.commsLargeVars[g_flowUnsaved.flowCommands[iCommandIndex].index].eCodeID)
	txtTemp += ENUM_TO_INT(g_flowUnsaved.commsLargeVars[g_flowUnsaved.flowCommands[iCommandIndex].index].eCheckID)
	txtToHash += GET_HASH_KEY(txtTemp)
ENDPROC


PROC GET_FLOW_COMMAND_HASH_STRING_NONE(INT iCommandIndex, TEXT_LABEL_63 &txtToHash)
	txtToHash = ENUM_TO_INT(g_flowUnsaved.flowCommands[iCommandIndex].command)
	txtToHash += GET_STRAND_INDEX_FOR_COMMAND_INDEX(iCommandIndex)
ENDPROC


PROC GET_FLOW_COMMAND_HASH_STRING_INDEX_DIRECT(INT iCommandIndex, TEXT_LABEL_63 &txtToHash)
	txtToHash = ENUM_TO_INT(g_flowUnsaved.flowCommands[iCommandIndex].command)
	txtToHash += GET_STRAND_INDEX_FOR_COMMAND_INDEX(iCommandIndex)
	txtToHash += g_flowUnsaved.flowCommands[iCommandIndex].index
ENDPROC


PROC GET_FLOW_COMMAND_HASH_STRING_CORE(INT iCommandIndex, TEXT_LABEL_63 &txtToHash)
	TEXT_LABEL_63 txtTemp

	txtToHash = ENUM_TO_INT(g_flowUnsaved.flowCommands[iCommandIndex].command)
	txtToHash += GET_STRAND_INDEX_FOR_COMMAND_INDEX(iCommandIndex)
	
	txtTemp = g_flowUnsaved.coreVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue1
	txtTemp += g_flowUnsaved.coreVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue2
	txtToHash += GET_HASH_KEY(txtTemp)
	
	txtTemp = g_flowUnsaved.coreVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue3
	txtTemp += g_flowUnsaved.coreVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue4
	txtToHash += GET_HASH_KEY(txtTemp)
ENDPROC


PROC GET_FLOW_COMMAND_HASH_STRING_CORE_SMALL(INT iCommandIndex, TEXT_LABEL_63 &txtToHash)
	txtToHash = ENUM_TO_INT(g_flowUnsaved.flowCommands[iCommandIndex].command)
	txtToHash += GET_STRAND_INDEX_FOR_COMMAND_INDEX(iCommandIndex)
	txtToHash += g_flowUnsaved.coreSmallVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue1
	txtToHash += g_flowUnsaved.coreSmallVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue2
ENDPROC


PROC GET_FLOW_COMMAND_HASH_STRING_TEXT(INT iCommandIndex, TEXT_LABEL_63 &txtToHash)
	txtToHash = ENUM_TO_INT(g_flowUnsaved.flowCommands[iCommandIndex].command)
	txtToHash += GET_STRAND_INDEX_FOR_COMMAND_INDEX(iCommandIndex)
	txtToHash += g_flowUnsaved.textVars[g_flowUnsaved.flowCommands[iCommandIndex].index].txtValue
ENDPROC


PROC GET_FLOW_COMMAND_HASH_STRING_TEXT_LARGE(INT iCommandIndex, TEXT_LABEL_63 &txtToHash)
	TEXT_LABEL_63 txtTemp
	
	txtToHash = ENUM_TO_INT(g_flowUnsaved.flowCommands[iCommandIndex].command)
	txtToHash += GET_STRAND_INDEX_FOR_COMMAND_INDEX(iCommandIndex)
	
	txtTemp= g_flowUnsaved.textLargeVars[g_flowUnsaved.flowCommands[iCommandIndex].index].txtValue
	txtToHash += GET_HASH_KEY(txtTemp)
	
	txtTemp = g_flowUnsaved.textLargeVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue1
	txtTemp += g_flowUnsaved.textLargeVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue2
	txtToHash += GET_HASH_KEY(txtTemp)
	
	txtTemp = g_flowUnsaved.textLargeVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue3
	txtTemp += g_flowUnsaved.textLargeVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue4
	txtToHash += GET_HASH_KEY(txtTemp)
ENDPROC


#IF IS_DEBUG_BUILD
PROC GET_FLOW_COMMAND_HASH_STRING_DEBUG(INT iCommandIndex, TEXT_LABEL_63 &txtToHash)
	txtToHash = ENUM_TO_INT(g_flowUnsaved.flowCommands[iCommandIndex].command)
	txtToHash += GET_STRAND_INDEX_FOR_COMMAND_INDEX(iCommandIndex)
	txtToHash += g_flowUnsaved.debugVars[g_flowUnsaved.flowCommands[iCommandIndex].index].anInt1
	txtToHash += g_flowUnsaved.debugVars[g_flowUnsaved.flowCommands[iCommandIndex].index].anInt2
ENDPROC
#ENDIF


FUNC INT GET_FLOW_COMMAND_HASH_ID(INT iCommandIndex)

	TEXT_LABEL_63 txtToHash = "NULL"
	
	IF iCommandIndex != ILLEGAL_ARRAY_POSITION
		SWITCH g_flowUnsaved.flowCommands[iCommandIndex].storageType
			CASE FST_NONE
				GET_FLOW_COMMAND_HASH_STRING_NONE(iCommandIndex, txtToHash)
			BREAK
			CASE FST_INDEX_DIRECT
				GET_FLOW_COMMAND_HASH_STRING_INDEX_DIRECT(iCommandIndex, txtToHash)
			BREAK
			CASE FST_CORE
				GET_FLOW_COMMAND_HASH_STRING_CORE(iCommandIndex, txtToHash)
			BREAK
			CASE FST_CORE_SMALL
				GET_FLOW_COMMAND_HASH_STRING_CORE_SMALL(iCommandIndex, txtToHash)
			BREAK
			CASE FST_TEXT
				GET_FLOW_COMMAND_HASH_STRING_TEXT(iCommandIndex, txtToHash)
			BREAK
			CASE FST_TEXT_LARGE
				GET_FLOW_COMMAND_HASH_STRING_TEXT_LARGE(iCommandIndex, txtToHash)
			BREAK
			CASE FST_COMMS
				GET_FLOW_COMMAND_HASH_STRING_COMMS(iCommandIndex, txtToHash)
			BREAK
			CASE FST_COMMS_LARGE
				GET_FLOW_COMMAND_HASH_STRING_COMMS_LARGE(iCommandIndex, txtToHash)
			BREAK
			
			#IF IS_DEBUG_BUILD
			CASE FST_DEBUG
				GET_FLOW_COMMAND_HASH_STRING_DEBUG(iCommandIndex, txtToHash)
			BREAK
			#ENDIF
		ENDSWITCH
	ENDIF
	
	RETURN GET_HASH_KEY(txtToHash)
ENDFUNC


#IF IS_DEBUG_BUILD
#IF ENABLE_FLOW_COMMAND_HASH_ID_CHECKING
	PROC RUN_FLOW_COMMAND_HASH_ID_UNIQUENESS_CHECK()
		CPRINTLN(DEBUG_FLOW, "<FLOW-HASH> --------------------------------------------")
		CPRINTLN(DEBUG_FLOW, "<FLOW-HASH> RUNNING FLOW COMMAND HASH UNIQUENESS CHECK")
		CPRINTLN(DEBUG_FLOW, "<FLOW-HASH> --------------------------------------------")
	
		INT iCommandHashIndex
		INT iCommandChecksThisFrame = 0
		INT iCollisionCount = 0
		REPEAT g_flowUnsaved.iNumStoredFlowCommands iCommandHashIndex
			INT iNewCommandHash = GET_FLOW_COMMAND_HASH_ID(iCommandHashIndex)
			CPRINTLN(DEBUG_FLOW, "<FLOW-HASH> Index: ", iCommandHashIndex, " Hash: ", iNewCommandHash)
			
			IF iNewCommandHash = HASH("NULL")
				CPRINTLN(DEBUG_FLOW, "<FLOW-HASH> NULL!")
			ELSE
				INT iCommandCompareIndex
				REPEAT iCommandHashIndex iCommandCompareIndex
					IF iNewCommandHash = g_iCommandHashes[iCommandCompareIndex]
						CPRINTLN(DEBUG_FLOW, "<FLOW-HASH> Collision at index ", iCommandCompareIndex, "! Hash: ", iNewCommandHash)
						CPRINTLN(DEBUG_FLOW, "NEW( ", iCommandHashIndex, ")")
						PRINT_FLOW_COMMAND(iCommandHashIndex)
						CPRINTLN(DEBUG_FLOW, "STORED( ", iCommandCompareIndex, ")")
						PRINT_FLOW_COMMAND(iCommandCompareIndex)
						iCollisionCount++
					ENDIF
				ENDREPEAT
			ENDIF
			g_iCommandHashes[iCommandHashIndex] = iNewCommandHash
			
			iCommandChecksThisFrame++
			IF iCommandChecksThisFrame > 40
				WAIT(0)
				iCommandChecksThisFrame = 0
			ENDIF
		ENDREPEAT
		CPRINTLN(DEBUG_FLOW, "<FLOW-HASH> --------------------------------------------")
		CPRINTLN(DEBUG_FLOW, "<FLOW-HASH> DONE. Found ", iCollisionCount, " collisions.")
		CPRINTLN(DEBUG_FLOW, "<FLOW-HASH> --------------------------------------------")
		IF iCollisionCount > 0
			SCRIPT_ASSERT("RUN_FLOW_COMMAND_HASH_ID_UNIQUENESS_CHECK: Collisions found!")
		ENDIF
	ENDPROC
#ENDIF
#ENDIF


/// PURPOSE:
///    Reset all stored strand pointer overrides.
PROC CLEAR_ALL_STRAND_POINTER_OVERRIDES()
	INT index
	
	#if USE_CLF_DLC
		REPEAT MAX_STRAND_POINTER_OVERRIDES index
			g_savedGlobalsClifford.sFlowCustom.strandToOverride[index] = STRAND_NONE
			g_savedGlobalsClifford.sFlowCustom.commandPointerOverride[index] = -1
			g_savedGlobalsClifford.sFlowCustom.commandPointerHashID[index] = 0
			g_savedGlobalsClifford.sFlowCustom.applyOnMPSwitchOnly[index] = FALSE
			g_savedGlobalsClifford.sFlowCustom.missionToUncomplete[index] = SP_MISSION_NONE
		ENDREPEAT	
		g_savedGlobalsClifford.sFlowCustom.numberStoredOverrides = 0
	#endif
	#if USE_NRM_DLC
		REPEAT MAX_STRAND_POINTER_OVERRIDES index
			g_savedGlobalsnorman.sFlowCustom.strandToOverride[index] = STRAND_NONE
			g_savedGlobalsnorman.sFlowCustom.commandPointerOverride[index] = -1
			g_savedGlobalsnorman.sFlowCustom.commandPointerHashID[index] = 0
			g_savedGlobalsnorman.sFlowCustom.applyOnMPSwitchOnly[index] = FALSE
			g_savedGlobalsnorman.sFlowCustom.missionToUncomplete[index] = SP_MISSION_NONE
		ENDREPEAT	
		g_savedGlobalsnorman.sFlowCustom.numberStoredOverrides = 0
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		REPEAT MAX_STRAND_POINTER_OVERRIDES index
			g_savedGlobals.sFlowCustom.strandToOverride[index] = STRAND_NONE
			g_savedGlobals.sFlowCustom.commandPointerOverride[index] = -1
			g_savedGlobals.sFlowCustom.commandPointerHashID[index] = 0
			g_savedGlobals.sFlowCustom.applyOnMPSwitchOnly[index] = FALSE
			g_savedGlobals.sFlowCustom.missionToUncomplete[index] = SP_MISSION_NONE
		ENDREPEAT	
		g_savedGlobals.sFlowCustom.numberStoredOverrides = 0
	#endif	
	#endif
	
ENDPROC


/// PURPOSE:
///    Clear any command pointer overrides that has been set for a specific strand.
PROC CLEAR_STRAND_POINTER_OVERRIDE_FOR_STRAND(STRANDS paramStrandID)
	CPRINTLN(DEBUG_FLOW, "<SAVE-POINTER> Clearing strand pointer override for strand ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrandID), ".")
	IF paramStrandID != STRAND_NONE
		INT index
		#if USE_CLF_DLC
			REPEAT g_savedGlobalsClifford.sFlowCustom.numberStoredOverrides index
				IF g_savedGlobalsClifford.sFlowCustom.strandToOverride[index] = paramStrandID
					CDEBUG1LN(DEBUG_FLOW, "<SAVE-POINTER> Found matching strand pointer at index ", index, ".")
					
					//Found override to clear. Shift all array positions up one to wipe out the match at this index.
					IF g_savedGlobalsClifford.sFlowCustom.numberStoredOverrides > 1
						INT index2
						FOR index2 = index TO (g_savedGlobalsClifford.sFlowCustom.numberStoredOverrides-2)
							CDEBUG1LN(DEBUG_FLOW, "<SAVE-POINTER> Overwriting pointer at ", index2, " with pointer at ", index2+1, ".")
							g_savedGlobalsClifford.sFlowCustom.strandToOverride[index2] = g_savedGlobalsClifford.sFlowCustom.strandToOverride[index2+1]
							g_savedGlobalsClifford.sFlowCustom.commandPointerOverride[index2] = g_savedGlobalsClifford.sFlowCustom.commandPointerOverride[index2+1]
							g_savedGlobalsClifford.sFlowCustom.commandPointerHashID[index2] = g_savedGlobalsClifford.sFlowCustom.commandPointerHashID[index2+1]
							g_savedGlobalsClifford.sFlowCustom.applyOnMPSwitchOnly[index2] = g_savedGlobalsClifford.sFlowCustom.applyOnMPSwitchOnly[index2+1]
							g_savedGlobalsClifford.sFlowCustom.missionToUncomplete[index2] = g_savedGlobalsClifford.sFlowCustom.missionToUncomplete[index2+1]
						ENDFOR
					ENDIF
					
					IF g_savedGlobalsClifford.sFlowCustom.numberStoredOverrides > 0
						CDEBUG1LN(DEBUG_FLOW, "<SAVE-POINTER> Setting pointer at ", g_savedGlobalsClifford.sFlowCustom.numberStoredOverrides-1, " to be blank.")
						g_savedGlobalsClifford.sFlowCustom.strandToOverride[g_savedGlobalsClifford.sFlowCustom.numberStoredOverrides-1] = STRAND_NONE
						g_savedGlobalsClifford.sFlowCustom.commandPointerOverride[g_savedGlobalsClifford.sFlowCustom.numberStoredOverrides-1] = -1
						g_savedGlobalsClifford.sFlowCustom.commandPointerHashID[g_savedGlobalsClifford.sFlowCustom.numberStoredOverrides-1] = 0
						g_savedGlobalsClifford.sFlowCustom.applyOnMPSwitchOnly[g_savedGlobalsClifford.sFlowCustom.numberStoredOverrides-1] = FALSE
						g_savedGlobalsClifford.sFlowCustom.missionToUncomplete[g_savedGlobalsClifford.sFlowCustom.numberStoredOverrides-1] = SP_MISSION_NONE
						g_savedGlobalsClifford.sFlowCustom.numberStoredOverrides--
					ENDIF
					
					//Step back an array position to recheck whatever has moved into the current array position.
					index--
				ENDIF
			ENDREPEAT
		#endif
		#if USE_NRM_DLC
			REPEAT g_savedGlobalsnorman.sFlowCustom.numberStoredOverrides index
				IF g_savedGlobalsnorman.sFlowCustom.strandToOverride[index] = paramStrandID
					CDEBUG1LN(DEBUG_FLOW, "<SAVE-POINTER> Found matching strand pointer at index ", index, ".")
					
					//Found override to clear. Shift all array positions up one to wipe out the match at this index.
					IF g_savedGlobalsnorman.sFlowCustom.numberStoredOverrides > 1
						INT index2
						FOR index2 = index TO (g_savedGlobalsnorman.sFlowCustom.numberStoredOverrides-2)
							CDEBUG1LN(DEBUG_FLOW, "<SAVE-POINTER> Overwriting pointer at ", index2, " with pointer at ", index2+1, ".")
							g_savedGlobalsnorman.sFlowCustom.strandToOverride[index2] = g_savedGlobalsnorman.sFlowCustom.strandToOverride[index2+1]
							g_savedGlobalsnorman.sFlowCustom.commandPointerOverride[index2] = g_savedGlobalsnorman.sFlowCustom.commandPointerOverride[index2+1]
							g_savedGlobalsnorman.sFlowCustom.commandPointerHashID[index2] = g_savedGlobalsnorman.sFlowCustom.commandPointerHashID[index2+1]
							g_savedGlobalsnorman.sFlowCustom.applyOnMPSwitchOnly[index2] = g_savedGlobalsnorman.sFlowCustom.applyOnMPSwitchOnly[index2+1]
							g_savedGlobalsnorman.sFlowCustom.missionToUncomplete[index2] = g_savedGlobalsnorman.sFlowCustom.missionToUncomplete[index2+1]
						ENDFOR
					ENDIF
					
					IF g_savedGlobalsnorman.sFlowCustom.numberStoredOverrides > 0
						CDEBUG1LN(DEBUG_FLOW, "<SAVE-POINTER> Setting pointer at ", g_savedGlobalsnorman.sFlowCustom.numberStoredOverrides-1, " to be blank.")
						g_savedGlobalsnorman.sFlowCustom.strandToOverride[g_savedGlobalsnorman.sFlowCustom.numberStoredOverrides-1] = STRAND_NONE
						g_savedGlobalsnorman.sFlowCustom.commandPointerOverride[g_savedGlobalsnorman.sFlowCustom.numberStoredOverrides-1] = -1
						g_savedGlobalsnorman.sFlowCustom.commandPointerHashID[g_savedGlobalsnorman.sFlowCustom.numberStoredOverrides-1] = 0
						g_savedGlobalsnorman.sFlowCustom.applyOnMPSwitchOnly[g_savedGlobalsnorman.sFlowCustom.numberStoredOverrides-1] = FALSE
						g_savedGlobalsnorman.sFlowCustom.missionToUncomplete[g_savedGlobalsnorman.sFlowCustom.numberStoredOverrides-1] = SP_MISSION_NONE
						g_savedGlobalsnorman.sFlowCustom.numberStoredOverrides--
					ENDIF
					
					//Step back an array position to recheck whatever has moved into the current array position.
					index--
				ENDIF
			ENDREPEAT
		#endif
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			REPEAT g_savedGlobals.sFlowCustom.numberStoredOverrides index
				IF g_savedGlobals.sFlowCustom.strandToOverride[index] = paramStrandID
					CDEBUG1LN(DEBUG_FLOW, "<SAVE-POINTER> Found matching strand pointer at index ", index, ".")
					
					//Found override to clear. Shift all array positions up one to wipe out the match at this index.
					IF g_savedGlobals.sFlowCustom.numberStoredOverrides > 1
						INT index2
						FOR index2 = index TO (g_savedGlobals.sFlowCustom.numberStoredOverrides-2)
							CDEBUG1LN(DEBUG_FLOW, "<SAVE-POINTER> Overwriting pointer at ", index2, " with pointer at ", index2+1, ".")
							g_savedGlobals.sFlowCustom.strandToOverride[index2] = g_savedGlobals.sFlowCustom.strandToOverride[index2+1]
							g_savedGlobals.sFlowCustom.commandPointerOverride[index2] = g_savedGlobals.sFlowCustom.commandPointerOverride[index2+1]
							g_savedGlobals.sFlowCustom.commandPointerHashID[index2] = g_savedGlobals.sFlowCustom.commandPointerHashID[index2+1]
							g_savedGlobals.sFlowCustom.applyOnMPSwitchOnly[index2] = g_savedGlobals.sFlowCustom.applyOnMPSwitchOnly[index2+1]
							g_savedGlobals.sFlowCustom.missionToUncomplete[index2] = g_savedGlobals.sFlowCustom.missionToUncomplete[index2+1]
						ENDFOR
					ENDIF
					
					IF g_savedGlobals.sFlowCustom.numberStoredOverrides > 0
						CDEBUG1LN(DEBUG_FLOW, "<SAVE-POINTER> Setting pointer at ", g_savedGlobals.sFlowCustom.numberStoredOverrides-1, " to be blank.")
						g_savedGlobals.sFlowCustom.strandToOverride[g_savedGlobals.sFlowCustom.numberStoredOverrides-1] = STRAND_NONE
						g_savedGlobals.sFlowCustom.commandPointerOverride[g_savedGlobals.sFlowCustom.numberStoredOverrides-1] = -1
						g_savedGlobals.sFlowCustom.commandPointerHashID[g_savedGlobals.sFlowCustom.numberStoredOverrides-1] = 0
						g_savedGlobals.sFlowCustom.applyOnMPSwitchOnly[g_savedGlobals.sFlowCustom.numberStoredOverrides-1] = FALSE
						g_savedGlobals.sFlowCustom.missionToUncomplete[g_savedGlobals.sFlowCustom.numberStoredOverrides-1] = SP_MISSION_NONE
						g_savedGlobals.sFlowCustom.numberStoredOverrides--
					ENDIF
					
					//Step back an array position to recheck whatever has moved into the current array position.
					index--
				ENDIF
			ENDREPEAT
		#endif	
		#endif
		
	ELSE
		SCRIPT_ASSERT("CLEAR_STRAND_POINTER_OVERRIDE_FOR_STRAND: Trying to clear a strand pointer override for STRAND_NONE. Bug BenR.")
	ENDIF
ENDPROC


/// PURPOSE:
///    Swap out a saved strand command pointer with the value that has been stored in the override struct.
///    Currently used when loading saves that were made during a phonecall triggered mission.
PROC SET_STRAND_POINTER_OVERRIDE_TO_CURRENT_STRAND_POINTER(STRANDS paramStrandID, SP_MISSIONS paramMissionToUncomplete, BOOL paramMPSwitchOnly)
#if USE_CLF_DLC	
	IF g_savedGlobalsClifford.sFlowCustom.numberStoredOverrides < MAX_STRAND_POINTER_OVERRIDES
		IF paramStrandID != STRAND_NONE
			// Check if an override is already set for this strand. If so overwrite it.
			INT index
			REPEAT g_savedGlobalsClifford.sFlowCustom.numberStoredOverrides index
				IF g_savedGlobalsClifford.sFlowCustom.strandToOverride[index] = paramStrandID
					CPRINTLN(DEBUG_FLOW, "<SAVE-POINTER> Overwriting a previous strand pointer override for strand ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrandID), ".")
					g_savedGlobalsClifford.sFlowCustom.commandPointerOverride[index] = g_savedGlobalsClifford.sFlow.strandSavedVars[paramStrandID].thisCommandPos
					g_savedGlobalsClifford.sFlowCustom.commandPointerHashID[index] = GET_FLOW_COMMAND_HASH_ID(g_savedGlobalsClifford.sFlow.strandSavedVars[paramStrandID].thisCommandPos)
					g_savedGlobalsClifford.sFlowCustom.missionToUncomplete[index] = paramMissionToUncomplete
					g_savedGlobalsClifford.sFlowCustom.applyOnMPSwitchOnly[index] = paramMPSwitchOnly
					EXIT
				ENDIF
			ENDREPEAT
			
			//Store a new override for this strand.
			CPRINTLN(DEBUG_FLOW, "<SAVE-POINTER> Setting a new strand pointer override for strand ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrandID), ".")
			g_savedGlobalsClifford.sFlowCustom.strandToOverride[g_savedGlobalsClifford.sFlowCustom.numberStoredOverrides] = paramStrandID
			g_savedGlobalsClifford.sFlowCustom.commandPointerOverride[g_savedGlobalsClifford.sFlowCustom.numberStoredOverrides] = g_savedGlobalsClifford.sFlow.strandSavedVars[paramStrandID].thisCommandPos
			g_savedGlobalsClifford.sFlowCustom.commandPointerHashID[g_savedGlobalsClifford.sFlowCustom.numberStoredOverrides] = GET_FLOW_COMMAND_HASH_ID(g_savedGlobalsClifford.sFlow.strandSavedVars[paramStrandID].thisCommandPos)
			g_savedGlobalsClifford.sFlowCustom.missionToUncomplete[g_savedGlobalsClifford.sFlowCustom.numberStoredOverrides] = paramMissionToUncomplete
			g_savedGlobalsClifford.sFlowCustom.applyOnMPSwitchOnly[g_savedGlobalsClifford.sFlowCustom.numberStoredOverrides] = paramMPSwitchOnly
			g_savedGlobalsClifford.sFlowCustom.numberStoredOverrides++
		ELSE
			SCRIPT_ASSERT("SET_STRAND_POINTER_OVERRIDE_TO_CURRENT_STRAND_POINTER: Trying to set a strand pointer override for STRAND_NONE. Bug BenR.")
		ENDIF
	ELSE
		SCRIPT_ASSERT("SET_STRAND_POINTER_OVERRIDE_TO_CURRENT_STRAND_POINTER: Too many strand pointer overrides set. Bug BenR.")
	ENDIF
#endif
#if USE_NRM_DLC
	IF g_savedGlobalsnorman.sFlowCustom.numberStoredOverrides < MAX_STRAND_POINTER_OVERRIDES
		IF paramStrandID != STRAND_NONE
			// Check if an override is already set for this strand. If so overwrite it.
			INT index
			REPEAT g_savedGlobalsnorman.sFlowCustom.numberStoredOverrides index
				IF g_savedGlobalsnorman.sFlowCustom.strandToOverride[index] = paramStrandID
					CPRINTLN(DEBUG_FLOW, "<SAVE-POINTER> Overwriting a previous strand pointer override for strand ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrandID), ".")
					g_savedGlobalsnorman.sFlowCustom.commandPointerOverride[index] = g_savedGlobalsnorman.sFlow.strandSavedVars[paramStrandID].thisCommandPos
					g_savedGlobalsnorman.sFlowCustom.commandPointerHashID[index] = GET_FLOW_COMMAND_HASH_ID(g_savedGlobalsnorman.sFlow.strandSavedVars[paramStrandID].thisCommandPos)
					g_savedGlobalsnorman.sFlowCustom.missionToUncomplete[index] = paramMissionToUncomplete
					g_savedGlobalsnorman.sFlowCustom.applyOnMPSwitchOnly[index] = paramMPSwitchOnly
					EXIT
				ENDIF
			ENDREPEAT
			
			//Store a new override for this strand.
			CPRINTLN(DEBUG_FLOW, "<SAVE-POINTER> Setting a new strand pointer override for strand ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrandID), ".")
			g_savedGlobalsnorman.sFlowCustom.strandToOverride[g_savedGlobalsnorman.sFlowCustom.numberStoredOverrides] = paramStrandID
			g_savedGlobalsnorman.sFlowCustom.commandPointerOverride[g_savedGlobalsnorman.sFlowCustom.numberStoredOverrides] = g_savedGlobalsnorman.sFlow.strandSavedVars[paramStrandID].thisCommandPos
			g_savedGlobalsnorman.sFlowCustom.commandPointerHashID[g_savedGlobalsnorman.sFlowCustom.numberStoredOverrides] = GET_FLOW_COMMAND_HASH_ID(g_savedGlobalsnorman.sFlow.strandSavedVars[paramStrandID].thisCommandPos)
			g_savedGlobalsnorman.sFlowCustom.missionToUncomplete[g_savedGlobalsnorman.sFlowCustom.numberStoredOverrides] = paramMissionToUncomplete
			g_savedGlobalsnorman.sFlowCustom.applyOnMPSwitchOnly[g_savedGlobalsnorman.sFlowCustom.numberStoredOverrides] = paramMPSwitchOnly
			g_savedGlobalsnorman.sFlowCustom.numberStoredOverrides++
		ELSE
			SCRIPT_ASSERT("SET_STRAND_POINTER_OVERRIDE_TO_CURRENT_STRAND_POINTER: Trying to set a strand pointer override for STRAND_NONE. Bug BenR.")
		ENDIF
	ELSE
		SCRIPT_ASSERT("SET_STRAND_POINTER_OVERRIDE_TO_CURRENT_STRAND_POINTER: Too many strand pointer overrides set. Bug BenR.")
	ENDIF
#endif

#if not USE_CLF_DLC	
#if not USE_NRM_DLC
	IF g_savedGlobals.sFlowCustom.numberStoredOverrides < MAX_STRAND_POINTER_OVERRIDES
		IF paramStrandID != STRAND_NONE
			// Check if an override is already set for this strand. If so overwrite it.
			INT index
			REPEAT g_savedGlobals.sFlowCustom.numberStoredOverrides index
				IF g_savedGlobals.sFlowCustom.strandToOverride[index] = paramStrandID
					CPRINTLN(DEBUG_FLOW, "<SAVE-POINTER> Overwriting a previous strand pointer override for strand ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrandID), ".")
					g_savedGlobals.sFlowCustom.commandPointerOverride[index] = g_savedGlobals.sFlow.strandSavedVars[paramStrandID].thisCommandPos
					g_savedGlobals.sFlowCustom.commandPointerHashID[index] = GET_FLOW_COMMAND_HASH_ID(g_savedGlobals.sFlow.strandSavedVars[paramStrandID].thisCommandPos)
					g_savedGlobals.sFlowCustom.missionToUncomplete[index] = paramMissionToUncomplete
					g_savedGlobals.sFlowCustom.applyOnMPSwitchOnly[index] = paramMPSwitchOnly
					EXIT
				ENDIF
			ENDREPEAT
			
			//Store a new override for this strand.
			CPRINTLN(DEBUG_FLOW, "<SAVE-POINTER> Setting a new strand pointer override for strand ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrandID), ".")
			g_savedGlobals.sFlowCustom.strandToOverride[g_savedGlobals.sFlowCustom.numberStoredOverrides] = paramStrandID
			g_savedGlobals.sFlowCustom.commandPointerOverride[g_savedGlobals.sFlowCustom.numberStoredOverrides] = g_savedGlobals.sFlow.strandSavedVars[paramStrandID].thisCommandPos
			g_savedGlobals.sFlowCustom.commandPointerHashID[g_savedGlobals.sFlowCustom.numberStoredOverrides] = GET_FLOW_COMMAND_HASH_ID(g_savedGlobals.sFlow.strandSavedVars[paramStrandID].thisCommandPos)
			g_savedGlobals.sFlowCustom.missionToUncomplete[g_savedGlobals.sFlowCustom.numberStoredOverrides] = paramMissionToUncomplete
			g_savedGlobals.sFlowCustom.applyOnMPSwitchOnly[g_savedGlobals.sFlowCustom.numberStoredOverrides] = paramMPSwitchOnly
			g_savedGlobals.sFlowCustom.numberStoredOverrides++
		ELSE
			SCRIPT_ASSERT("SET_STRAND_POINTER_OVERRIDE_TO_CURRENT_STRAND_POINTER: Trying to set a strand pointer override for STRAND_NONE. Bug BenR.")
		ENDIF
	ELSE
		SCRIPT_ASSERT("SET_STRAND_POINTER_OVERRIDE_TO_CURRENT_STRAND_POINTER: Too many strand pointer overrides set. Bug BenR.")
	ENDIF
#endif	
#endif
ENDPROC


/// PURPOSE:
///    Swap out a saved strand command pointer with the value that has been stored in the override struct.
///    Currently used when loading saves that were made during a phonecall triggered mission.
#if USE_CLF_DLC
PROC APPLY_STRAND_POINTER_OVERRIDES_CLF(BOOL paramSwitchingToMP = FALSE)
	
	IF g_savedGlobalsClifford.sFlowCustom.numberStoredOverrides > 0
		INT index
		REPEAT g_savedGlobalsClifford.sFlowCustom.numberStoredOverrides index
			IF g_savedGlobalsClifford.sFlowCustom.strandToOverride[index] != STRAND_NONE
				IF paramSwitchingToMP OR (NOT g_savedGlobalsClifford.sFlowCustom.applyOnMPSwitchOnly[index])
					CPRINTLN(DEBUG_FLOW, "<SAVE-POINTER> Overriding saved command pointer for strand ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID_CLF(g_savedGlobalsClifford.sFlowCustom.strandToOverride[index]), ".")
					g_savedGlobalsClifford.sFlow.strandSavedVars[g_savedGlobalsClifford.sFlowCustom.strandToOverride[index]].thisCommandPos = g_savedGlobalsClifford.sFlowCustom.commandPointerOverride[index]
					g_savedGlobalsClifford.sFlow.strandSavedVars[g_savedGlobalsClifford.sFlowCustom.strandToOverride[index]].thisCommandHashID = g_savedGlobalsClifford.sFlowCustom.commandPointerHashID[index]
					IF g_savedGlobalsClifford.sFlowCustom.missionToUncomplete[index] != SP_MISSION_NONE
						IF GET_STORY_MISSION_STRAND_CLF(g_savedGlobalsClifford.sFlowCustom.missionToUncomplete[index]) = g_savedGlobalsClifford.sFlowCustom.strandToOverride[index]
							CPRINTLN(DEBUG_FLOW, "<SAVE-POINTER> Uncompleting mission ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID_CLF(g_savedGlobalsClifford.sFlowCustom.missionToUncomplete[index]), " as save override is restored.")
							SET_MISSION_COMPLETE_STATE_CLF(g_savedGlobalsClifford.sFlowCustom.missionToUncomplete[index], FALSE)
						ENDIF
					ENDIF
			#IF IS_DEBUG_BUILD
				ELSE
					CPRINTLN(DEBUG_FLOW, "<SAVE-POINTER> Not applying override for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID_CLF(g_savedGlobalsClifford.sFlowCustom.missionToUncomplete[index]), " as it is flagged to only be applied on MP switches.")
			#ENDIF
				ENDIF
			ELSE
				SCRIPT_ASSERT("APPLY_STRAND_POINTER_OVERRIDES: Trying to set a strand pointer override for STRAND_NONE. This override should never have been set. Bug BenR.")
			ENDIF
		ENDREPEAT
		
		CPRINTLN(DEBUG_FLOW, "<SAVE-POINTER> Overriding last call settings as we override strand command pointers..")
		g_savedGlobalsClifford.sCommsControlData.eLastCompletedCall		= COMM_NONE
		g_savedGlobalsClifford.sCommsControlData.bLastCallAnswered		= FALSE
		g_savedGlobalsClifford.sCommsControlData.bLastCallHadResponse		= FALSE
	ENDIF
ENDPROC
#endif
#if USE_NRM_DLC
PROC APPLY_STRAND_POINTER_OVERRIDES_NRM(BOOL paramSwitchingToMP = FALSE)
	
	IF g_savedGlobalsnorman.sFlowCustom.numberStoredOverrides > 0
		INT index
		REPEAT g_savedGlobalsnorman.sFlowCustom.numberStoredOverrides index
			IF g_savedGlobalsnorman.sFlowCustom.strandToOverride[index] != STRAND_NONE
				IF paramSwitchingToMP OR (NOT g_savedGlobalsnorman.sFlowCustom.applyOnMPSwitchOnly[index])
					CPRINTLN(DEBUG_FLOW, "<SAVE-POINTER> Overriding saved command pointer for strand ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID_NRM(g_savedGlobalsnorman.sFlowCustom.strandToOverride[index]), ".")
					g_savedGlobalsnorman.sFlow.strandSavedVars[g_savedGlobalsnorman.sFlowCustom.strandToOverride[index]].thisCommandPos = g_savedGlobalsnorman.sFlowCustom.commandPointerOverride[index]
					g_savedGlobalsnorman.sFlow.strandSavedVars[g_savedGlobalsnorman.sFlowCustom.strandToOverride[index]].thisCommandHashID = g_savedGlobalsnorman.sFlowCustom.commandPointerHashID[index]
					IF g_savedGlobalsnorman.sFlowCustom.missionToUncomplete[index] != SP_MISSION_NONE
						IF GET_STORY_MISSION_STRAND_NRM(g_savedGlobalsnorman.sFlowCustom.missionToUncomplete[index]) = g_savedGlobalsnorman.sFlowCustom.strandToOverride[index]
							CPRINTLN(DEBUG_FLOW, "<SAVE-POINTER> Uncompleting mission ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID_NRM(g_savedGlobalsnorman.sFlowCustom.missionToUncomplete[index]), " as save override is restored.")
							SET_MISSION_COMPLETE_STATE_NRM(g_savedGlobalsnorman.sFlowCustom.missionToUncomplete[index], FALSE)
						ENDIF
					ENDIF
			#IF IS_DEBUG_BUILD
				ELSE
					CPRINTLN(DEBUG_FLOW, "<SAVE-POINTER> Not applying override for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID_NRM(g_savedGlobalsnorman.sFlowCustom.missionToUncomplete[index]), " as it is flagged to only be applied on MP switches.")
			#ENDIF
				ENDIF
			ELSE
				SCRIPT_ASSERT("APPLY_STRAND_POINTER_OVERRIDES: Trying to set a strand pointer override for STRAND_NONE. This override should never have been set. Bug BenR.")
			ENDIF
		ENDREPEAT
		
		CPRINTLN(DEBUG_FLOW, "<SAVE-POINTER> Overriding last call settings as we override strand command pointers..")
		g_savedGlobalsnorman.sCommsControlData.eLastCompletedCall		= COMM_NONE
		g_savedGlobalsnorman.sCommsControlData.bLastCallAnswered		= FALSE
		g_savedGlobalsnorman.sCommsControlData.bLastCallHadResponse		= FALSE
	ENDIF
ENDPROC
#endif
PROC APPLY_STRAND_POINTER_OVERRIDES(BOOL paramSwitchingToMP = FALSE)
	
	#if USE_CLF_DLC
		APPLY_STRAND_POINTER_OVERRIDES_CLF(paramSwitchingToMP)
		exit
	#endif
	
	#if USE_NRM_DLC
		APPLY_STRAND_POINTER_OVERRIDES_NRM(paramSwitchingToMP)
		exit
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	IF g_savedGlobals.sFlowCustom.numberStoredOverrides > 0
		INT index
		REPEAT g_savedGlobals.sFlowCustom.numberStoredOverrides index
			IF g_savedGlobals.sFlowCustom.strandToOverride[index] != STRAND_NONE
				IF paramSwitchingToMP OR (NOT g_savedGlobals.sFlowCustom.applyOnMPSwitchOnly[index])
					CPRINTLN(DEBUG_FLOW, "<SAVE-POINTER> Overriding saved command pointer for strand ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(g_savedGlobals.sFlowCustom.strandToOverride[index]), ".")
					g_savedGlobals.sFlow.strandSavedVars[g_savedGlobals.sFlowCustom.strandToOverride[index]].thisCommandPos = g_savedGlobals.sFlowCustom.commandPointerOverride[index]
					g_savedGlobals.sFlow.strandSavedVars[g_savedGlobals.sFlowCustom.strandToOverride[index]].thisCommandHashID = g_savedGlobals.sFlowCustom.commandPointerHashID[index]
					IF g_savedGlobals.sFlowCustom.missionToUncomplete[index] != SP_MISSION_NONE
						IF GET_STORY_MISSION_STRAND(g_savedGlobals.sFlowCustom.missionToUncomplete[index]) = g_savedGlobals.sFlowCustom.strandToOverride[index]
							CPRINTLN(DEBUG_FLOW, "<SAVE-POINTER> Uncompleting mission ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_savedGlobals.sFlowCustom.missionToUncomplete[index]), " as save override is restored.")
							SET_MISSION_COMPLETE_STATE(g_savedGlobals.sFlowCustom.missionToUncomplete[index], FALSE)
						ENDIF
					ENDIF
			#IF IS_DEBUG_BUILD
				ELSE
					CPRINTLN(DEBUG_FLOW, "<SAVE-POINTER> Not applying override for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_savedGlobals.sFlowCustom.missionToUncomplete[index]), " as it is flagged to only be applied on MP switches.")
			#ENDIF
				ENDIF
			ELSE
				SCRIPT_ASSERT("APPLY_STRAND_POINTER_OVERRIDES: Trying to set a strand pointer override for STRAND_NONE. This override should never have been set. Bug BenR.")
			ENDIF
		ENDREPEAT
		
		CPRINTLN(DEBUG_FLOW, "<SAVE-POINTER> Overriding last call settings as we override strand command pointers..")
		g_savedGlobals.sCommsControlData.eLastCompletedCall		= COMM_NONE
		g_savedGlobals.sCommsControlData.bLastCallAnswered		= FALSE
		g_savedGlobals.sCommsControlData.bLastCallHadResponse	= FALSE
	ENDIF
	#endif
	#endif
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clears the contents of the available missions array
// 
PROC CLEAR_AVAILABLE_MISSIONS_ARRAY()
	INT tempLoop = 0
	#IF USE_CLF_DLC
		REPEAT MAX_MISSIONS_AVAILABLE_TU tempLoop
	#ENDIF
	#IF USE_NRM_DLC
		REPEAT MAX_MISSIONS_AVAILABLE_TU tempLoop
	#ENDIF
	#IF NOT USE_CLF_DLC
	#IF NOT USE_NRM_DLC
		REPEAT MAX_MISSIONS_AVAILABLE tempLoop
	#ENDIF
	#ENDIF
	CLEAR_ONE_AVAILABLE_MISSION(tempLoop)
	ENDREPEAT
ENDPROC


PROC CLEAR_MISSION_FLOW_NON_SAVED_GLOBALS_CUSTOM()
	CLEAR_COMMS_VARIABLES_ARRAY()
	CLEAR_COMMS_LARGE_VARIABLES_ARRAY()
	CLEAR_AVAILABLE_MISSIONS_ARRAY()
	RESET_ALL_MISSION_TRIGGERS()
ENDPROC


PROC CLEAR_MISSION_FLOW_SAVED_GLOBALS_CUSTOM()
	#IF IS_DEBUG_BUILD
		CLEAR_MISSION_FLOW_DEBUG_LAUNCH_AND_FORGET_DISPLAY_VARIABLES()
	#ENDIF
	// Repeat play
	CLEAR_MISSION_FLOW_REPEAT_PLAY_VARIABLES()
	CLEAR_ALL_STRAND_POINTER_OVERRIDES()
ENDPROC

