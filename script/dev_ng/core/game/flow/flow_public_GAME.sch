// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   flow_public_GAME.sch
//      CREATED         :   Keith, BenR
//      DESCRIPTION     :   Contains all Mission Flow public functions.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

USING "rage_builtins.sch"
USING "globals.sch"

USING "savegame_public.sch"
USING "script_player.sch"
USING "flow_mission_data_public.sch"
USING "blip_control_public.sch"
USING "friend_flow_public.sch"
USING "family_flow_public.sch"
USING "flow_private_core.sch"
USING "trigger_scene_private.sch"
USING "respawn_cleanup_data.sch"

USING "replay_public.sch"
USING "screenDisplayState.sch"

USING "savegame_private.sch"	//	Used for SAFE_TO_SAVE_GAME in Feature blocking criteria - RowanJ B* 2218023

#if USE_CLF_DLC
	USING "static_mission_data_helpCLF.sch"
#endif
#if USE_NRM_DLC
	USING "static_mission_data_helpNRM.sch"
#endif

// ===========================================================================================================
//      Mission Termination Functions
//      (See also Flow_Private.sch)
// ===========================================================================================================


// PURPOSE: Project override - Look up a mission's script name from a mission ID.
//
FUNC STRING GET_SCRIPT_NAME_FROM_MISSION_ID(INT paramMissionID)
	#if USE_CLF_DLC
		RETURN GET_SCRIPT_FROM_HASH(g_sMissionStaticData[paramMissionID].scriptHash)
	#endif
	#if USE_NRM_DLC
		RETURN GET_SCRIPT_FROM_HASH_NRM(g_sMissionStaticData[paramMissionID].scriptHash)
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		RETURN g_sMissionStaticData[paramMissionID].scriptName
	#endif
	#endif
ENDFUNC


// PURPOSE: Gets called by all missions when they pass to let the Mission Flow know how to proceed.
//
// PARAMS:  paramSeamlessMissionPass	-	If TRUE the mission will pass without any delay and the mission
//											pass screen will be blocked from displaying until g_bMissionStatSystemBlocker
//											is set FALSE by the flow.
//
// NOTES:   The intention is that the Mission Flow can work out which mission was active without accepting
//              any extra parameters.
PROC MISSION_FLOW_MISSION_PASSED(BOOL paramSeamlessMissionPass = FALSE, BOOL paramHidePassScreen = FALSE)

	//g_bMissionStatSystemHeistMode = paramHidePassScreen // the fuck is this shit?
	IF !g_MissionStatSystemSuppressVisual
		g_MissionStatSystemSuppressVisual = paramHidePassScreen
	ENDIF


	IF paramSeamlessMissionPass
		//Seamless mission pass. Block displaying pass screen until the flow allows it.
		
		// This isn't needed if repeat playing a heist mission that normally ends with a planning board
		IF IS_REPEAT_PLAY_ACTIVE()
		AND g_RepeatPlayData.eMissionType = CP_GROUP_MISSIONS // repeat playing a story mission
		AND DOES_MISSION_END_WITH_PLANNING_BOARD(INT_TO_ENUM(SP_MISSIONS, g_RepeatPlayData.iMissionIndex)) // that ends in a heist planning board
			CPRINTLN(DEBUG_REPEAT, "Skipping the seamless mission pass for repeat play of heist planning board mission.")
		ELSE
			g_bMissionStatSystemBlocker = TRUE
		ENDIF
	ENDIF
	#if USE_CLF_DLC
		IF g_savedGlobalsClifford.sFlow.isGameflowActive
		OR IS_REPEAT_PLAY_ACTIVE()
		
			// Lookup mission indexes.
			INT runningAvailableMissionPos 	= GET_AVAILABLE_MISSION_INDEX_FOR_RUNNING_MISSION()
			
			INT availableMissionIndex = g_availableMissionsTU[runningAvailableMissionPos].index
			SP_MISSIONS missionID = INT_TO_ENUM(SP_MISSIONS, g_flowUnsaved.coreVars[availableMissionIndex].iValue1)
			
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_FLOW, "MISSION_FLOW_MISSION_PASSED: ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(missionID))
				IF paramSeamlessMissionPass
					CPRINTLN(DEBUG_FLOW, "Running seamless mission pass. Pass screen blocked until flagged otherwise.")
				ENDIF
			#ENDIF
			
		    // Ensure there is a running mission (but only in gameflow mode)
		    IF (runningAvailableMissionPos = ILLEGAL_ARRAY_POSITION)
		        IF (g_savedGlobalsClifford.sFlow.isGameflowActive)
		            CERRORLN(DEBUG_FLOW, "MISSION PASSED: Failed to find an Available Mission that is RUNNING.") 
		        ENDIF
		        EXIT
		    ENDIF
		    
		    // ...check for previous pass
		    IF (IS_BIT_SET(g_availableMissionsTU[runningAvailableMissionPos].bitflags, BITS_AVAILABLE_MISSION_PASSED))
		        CERRORLN(DEBUG_FLOW, "Flow_Mission_Passed: Trying to Pass a RUNNING Available Mission that has already Passed - Tell BenR", GET_SCRIPT_FROM_HASH(g_sMissionStaticData[missionID].scripthash))
		        EXIT
		    ENDIF

		    // ...check for previous fail
		    IF (IS_BIT_SET(g_availableMissionsTU[runningAvailableMissionPos].bitflags, BITS_AVAILABLE_MISSION_FAILED))
		        CERRORLN(DEBUG_FLOW, "Flow_Mission_Passed: Trying to Pass a RUNNING Available Mission that has already Failed - Tell BenR", GET_SCRIPT_FROM_HASH(g_sMissionStaticData[missionID].scripthash))
		        EXIT
		    ENDIF
			
			//Inform the autoplay system we just passed a mission.
		    #IF IS_DEBUG_BUILD
				g_bFlowAutoplayJustPassed = TRUE
		    #ENDIF
		    
		    // Set the Mission Passed flag
		    SET_BIT(g_availableMissionsTU[runningAvailableMissionPos].bitflags, BITS_AVAILABLE_MISSION_PASSED)
			SET_BIT(g_iRepeatPlayBits, ENUM_TO_INT(RPB_PASSED)) // (and the repeat play one too)
			
			//Set global "last mission passed" to this mission
			g_eLastMissionPassed = missionID
			g_iLastMissionPassedGameTime = GET_GAME_TIMER()
			
			//when a mission cleans up all assets created on mission will be set as nolonger needed.  
			//including the spy vehicle.
			//The flag forces the ambient spy vehicle system to take ownership of the spy vehicle if it exists. 
			spy_vehicle_handed_over_to_ambient_spy_vehicle_system = true 
			
		ENDIF
		
	#endif
	#if USE_NRM_DLC
		IF g_savedGlobalsnorman.sFlow.isGameflowActive
		OR IS_REPEAT_PLAY_ACTIVE()
		
			// Lookup mission indexes.
			INT runningAvailableMissionPos 	= GET_AVAILABLE_MISSION_INDEX_FOR_RUNNING_MISSION()
			
			INT availableMissionIndex = g_availableMissionsTU[runningAvailableMissionPos].index
			SP_MISSIONS missionID = INT_TO_ENUM(SP_MISSIONS, g_flowUnsaved.coreVars[availableMissionIndex].iValue1)
			
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_FLOW, "MISSION_FLOW_MISSION_PASSED: ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(missionID))
				IF paramSeamlessMissionPass
					CPRINTLN(DEBUG_FLOW, "Running seamless mission pass. Pass screen blocked until flagged otherwise.")
				ENDIF
			#ENDIF
			
		    // Ensure there is a running mission (but only in gameflow mode)
		    IF (runningAvailableMissionPos = ILLEGAL_ARRAY_POSITION)
		        IF (g_savedGlobalsnorman.sFlow.isGameflowActive)
		            CERRORLN(DEBUG_FLOW, "MISSION PASSED: Failed to find an Available Mission that is RUNNING.") 
		        ENDIF
		        EXIT
		    ENDIF
		    
		    // ...check for previous pass
		    IF (IS_BIT_SET(g_availableMissionsTU[runningAvailableMissionPos].bitflags, BITS_AVAILABLE_MISSION_PASSED))
		        CERRORLN(DEBUG_FLOW, "Flow_Mission_Passed: Trying to Pass a RUNNING Available Mission that has already Passed - Tell BenR", GET_SCRIPT_FROM_HASH_NRM(g_sMissionStaticData[missionID].scripthash))
		        EXIT
		    ENDIF

		    // ...check for previous fail
		    IF (IS_BIT_SET(g_availableMissionsTU[runningAvailableMissionPos].bitflags, BITS_AVAILABLE_MISSION_FAILED))
		        CERRORLN(DEBUG_FLOW, "Flow_Mission_Passed: Trying to Pass a RUNNING Available Mission that has already Failed - Tell BenR", GET_SCRIPT_FROM_HASH_NRM(g_sMissionStaticData[missionID].scripthash))
		        EXIT
		    ENDIF
			
			//Inform the autoplay system we just passed a mission.
		    #IF IS_DEBUG_BUILD
				g_bFlowAutoplayJustPassed = TRUE
		    #ENDIF
		    
		    // Set the Mission Passed flag
		    SET_BIT(g_availableMissionsTU[runningAvailableMissionPos].bitflags, BITS_AVAILABLE_MISSION_PASSED)
			SET_BIT(g_iRepeatPlayBits, ENUM_TO_INT(RPB_PASSED)) // (and the repeat play one too)
			
			//Set global "last mission passed" to this mission
			g_eLastMissionPassed = missionID
			g_iLastMissionPassedGameTime = GET_GAME_TIMER()
		ENDIF
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		IF g_savedGlobals.sFlow.isGameflowActive
		OR IS_REPEAT_PLAY_ACTIVE()
		
			// Lookup mission indexes.
			INT runningAvailableMissionPos 	= GET_AVAILABLE_MISSION_INDEX_FOR_RUNNING_MISSION()
			
			INT availableMissionIndex = g_availableMissions[runningAvailableMissionPos].index
			SP_MISSIONS missionID = INT_TO_ENUM(SP_MISSIONS, g_flowUnsaved.coreVars[availableMissionIndex].iValue1)
			
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_FLOW, "MISSION_FLOW_MISSION_PASSED: ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(missionID))
				IF paramSeamlessMissionPass
					CPRINTLN(DEBUG_FLOW, "Running seamless mission pass. Pass screen blocked until flagged otherwise.")
				ENDIF
			#ENDIF
			
		    // Ensure there is a running mission (but only in gameflow mode)
		    IF (runningAvailableMissionPos = ILLEGAL_ARRAY_POSITION)
		        IF (g_savedGlobals.sFlow.isGameflowActive)
		            CERRORLN(DEBUG_FLOW, "MISSION PASSED: Failed to find an Available Mission that is RUNNING.") 
		        ENDIF
		        EXIT
		    ENDIF
		    
		    // ...check for previous pass
		    IF (IS_BIT_SET(g_availableMissions[runningAvailableMissionPos].bitflags, BITS_AVAILABLE_MISSION_PASSED))
		        CERRORLN(DEBUG_FLOW, "Flow_Mission_Passed: Trying to Pass a RUNNING Available Mission that has already Passed - Tell BenR", g_sMissionStaticData[missionID].scriptName)
		        EXIT
		    ENDIF

		    // ...check for previous fail
		    IF (IS_BIT_SET(g_availableMissions[runningAvailableMissionPos].bitflags, BITS_AVAILABLE_MISSION_FAILED))
		        CERRORLN(DEBUG_FLOW, "Flow_Mission_Passed: Trying to Pass a RUNNING Available Mission that has already Failed - Tell BenR", g_sMissionStaticData[missionID].scriptName)
		        EXIT
		    ENDIF
			
			//Inform the autoplay system we just passed a mission.
		    #IF IS_DEBUG_BUILD
				g_bFlowAutoplayJustPassed = TRUE
		    #ENDIF
		    
		    // Set the Mission Passed flag
		    SET_BIT(g_availableMissions[runningAvailableMissionPos].bitflags, BITS_AVAILABLE_MISSION_PASSED)
			SET_BIT(g_iRepeatPlayBits, ENUM_TO_INT(RPB_PASSED)) // (and the repeat play one too)
			
			//Set global "last mission passed" to this mission
			g_eLastMissionPassed = missionID
			g_iLastMissionPassedGameTime = GET_GAME_TIMER()
		ENDIF
	#endif
	#endif
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Gets called by all missions when they Fail to let the Mission Flow know how to proceed.
//
// NOTES:   The intention is that the Mission Flow can work out which mission was active without accepting
//              any extra parameters.
PROC MISSION_FLOW_MISSION_FAILED(BOOL bSkipFailScreen = FALSE)
#if  USE_CLF_DLC
	IF g_savedGlobalsClifford.sFlow.isGameflowActive
	OR IS_REPEAT_PLAY_ACTIVE()
	
	    // Find the available mission that is currently active
	    INT runningAvailableMissionPos = GET_AVAILABLE_MISSION_INDEX_FOR_RUNNING_MISSION()
		
		#IF IS_DEBUG_BUILD		
			INT theCoreVarsIndex = g_availableMissionsTU[runningAvailableMissionPos].index
			CPRINTLN(DEBUG_FLOW, "MISSION FAILED: ", GET_SCRIPT_FROM_HASH(g_sMissionStaticData[g_flowUnsaved.coreVars[theCoreVarsIndex].iValue1].scriptHash))
		#ENDIF
		
	    // Call the common Fail and Force_Cleanup function
	    IF NOT (MISSION_FLOW_MISSION_FAILED_AND_FORCE_CLEANUP(runningAvailableMissionPos))
	        EXIT
	    ENDIF
		
		// Set the Mission Failed flag
	    SET_BIT(g_availableMissionsTU[runningAvailableMissionPos].bitflags, BITS_AVAILABLE_MISSION_FAILED)
		
		// Set whether we should skip the fail screen
		g_bSkipFailScreen = bSkipFailScreen

	    #IF IS_DEBUG_BUILD
			g_bFlowAutoplayJustPassed = FALSE
	    #ENDIF
		
		//when a mission cleans up all assets created on mission will be set as nolonger needed.  
		//including the spy vehicle.
		//The flag forces the ambient spy vehicle system to take ownership of the spy vehicle if it exists. 
		spy_vehicle_handed_over_to_ambient_spy_vehicle_system = true 
		
	ENDIF

#endif
#if  USE_NRM_DLC
	IF g_savedGlobalsnorman.sFlow.isGameflowActive
	OR IS_REPEAT_PLAY_ACTIVE()
	
	    // Find the available mission that is currently active
	    INT runningAvailableMissionPos = GET_AVAILABLE_MISSION_INDEX_FOR_RUNNING_MISSION()
		
		#IF IS_DEBUG_BUILD		
			INT theCoreVarsIndex = g_availableMissionsTU[runningAvailableMissionPos].index
			CPRINTLN(DEBUG_FLOW, "MISSION FAILED: ", GET_SCRIPT_FROM_HASH_NRM(g_sMissionStaticData[g_flowUnsaved.coreVars[theCoreVarsIndex].iValue1].scriptHash))
		#ENDIF
		
	    // Call the common Fail and Force_Cleanup function
	    IF NOT (MISSION_FLOW_MISSION_FAILED_AND_FORCE_CLEANUP(runningAvailableMissionPos))
	        EXIT
	    ENDIF
		
		// Set the Mission Failed flag
	    SET_BIT(g_availableMissionsTU[runningAvailableMissionPos].bitflags, BITS_AVAILABLE_MISSION_FAILED)
		
		// Set whether we should skip the fail screen
		g_bSkipFailScreen = bSkipFailScreen

	    #IF IS_DEBUG_BUILD
			g_bFlowAutoplayJustPassed = FALSE
	    #ENDIF
	ENDIF
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC
	IF g_savedGlobals.sFlow.isGameflowActive
	OR IS_REPEAT_PLAY_ACTIVE()
	
	    // Find the available mission that is currently active
	    INT runningAvailableMissionPos = GET_AVAILABLE_MISSION_INDEX_FOR_RUNNING_MISSION()
		
		#IF IS_DEBUG_BUILD		
			INT theCoreVarsIndex = g_availableMissions[runningAvailableMissionPos].index
			CPRINTLN(DEBUG_FLOW, "MISSION FAILED: ", g_sMissionStaticData[g_flowUnsaved.coreVars[theCoreVarsIndex].iValue1].scriptName)
		#ENDIF
		
	    // Call the common Fail and Force_Cleanup function
	    IF NOT (MISSION_FLOW_MISSION_FAILED_AND_FORCE_CLEANUP(runningAvailableMissionPos))
	        EXIT
	    ENDIF
		
		// Set the Mission Failed flag
	    SET_BIT(g_availableMissions[runningAvailableMissionPos].bitflags, BITS_AVAILABLE_MISSION_FAILED)
		
		// Set whether we should skip the fail screen
		g_bSkipFailScreen = bSkipFailScreen

	    #IF IS_DEBUG_BUILD
			g_bFlowAutoplayJustPassed = FALSE
	    #ENDIF
	ENDIF
#endif	
#endif
ENDPROC

/// PURPOSE:
///    Allows setting of the fail message that will display on the replay screen.
/// PARAMS:
///    failReason - the fail reason to display
PROC MISSION_FLOW_SET_FAIL_REASON(STRING failReason)
	CPRINTLN(DEBUG_REPLAY, GET_THIS_SCRIPT_NAME(), " tried to set fail reason.")

	IF NOT IS_STRING_NULL_OR_EMPTY(failReason)
		IF GET_LENGTH_OF_LITERAL_STRING(failReason) <= 16
			g_txtMissionFailReason = failReason
			g_txtMissionFailAddText = ""
			CPRINTLN(DEBUG_REPLAY, GET_THIS_SCRIPT_NAME(), " set mission fail reason label to [", g_txtMissionFailReason, "].")
			
			IF IS_REPLAY_RECORDING()
				STOP_REPLAY_RECORDING()	//Fix for bug 2216866
				CPRINTLN(DEBUG_REPLAY, GET_THIS_SCRIPT_NAME(), " stopped REPLAY RECORDING as the mission has failed.")
			ENDIF
		ELSE
			SCRIPT_ASSERT("MISSION_FLOW_SET_FAIL_REASON: Fail text label was longer than 16 characters. This is too long.")
		ENDIF
	ELSE
		CPRINTLN(DEBUG_REPLAY, "Fail reason is null or empty.")
	ENDIF
ENDPROC


// PURPOSE: Allows setting of the fail message that will display on the next fail screen that is displayed in game. 
//			This fail message contains another text label inserted inline.
PROC MISSION_FLOW_SET_FAIL_REASON_CONTAINING_TEXT(STRING failReason, STRING failAddText)
	IF NOT IS_STRING_NULL_OR_EMPTY(failReason)
		IF GET_LENGTH_OF_LITERAL_STRING(failReason) <= 16
			IF GET_LENGTH_OF_LITERAL_STRING(failAddText) <= 16
				CPRINTLN(DEBUG_FLOW, GET_THIS_SCRIPT_NAME(), " set mission fail reason label to [", g_txtMissionFailReason, "] with sub-label [", failAddText, "].")
				g_txtMissionFailReason = failReason
				g_txtMissionFailAddText = failAddText
			ELSE
				CERRORLN(DEBUG_FLOW, "MISSION_FLOW_SET_FAIL_REASON_CONTAINING_TEXT: Add text label was longer than 16 characters. This is too long.")
			ENDIF
		ELSE
			CERRORLN(DEBUG_FLOW, "MISSION_FLOW_SET_FAIL_REASON_CONTAINING_TEXT: Fail text label was longer than 16 characters. This is too long.")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets where the player will be placed if they reject a replay of this mission
/// PARAMS:
///    failWarp - where to put the player
///    failHeading - heading to use for the player
PROC MISSION_FLOW_SET_FAIL_WARP_LOCATION(VECTOR failWarp, FLOAT failHeading)
	IF ARE_VECTORS_EQUAL(g_vMissionFailWarp, <<0,0,0>>)
		CPRINTLN(DEBUG_REPLAY, "Script ", GET_THIS_SCRIPT_NAME(), " setting the fail warp location to ", failWarp, ".")
		g_vMissionFailWarp = failWarp
		g_fMissionFailHeading = failHeading
	ELSE
		CERRORLN(DEBUG_REPLAY, "MISSION_FLOW_SET_FAIL_WARP_LOCATION: ", GET_THIS_SCRIPT_NAME(), " Tried to set the fail warp location when already set.")
	ENDIF
ENDPROC


// PURPOSE: To be called by missions to tell the flow controller they have failed. Allows setting of the fail message that will
//			display on the fail screen.
PROC MISSION_FLOW_MISSION_FAILED_WITH_REASON(STRING failReason)
	MISSION_FLOW_SET_FAIL_REASON(failReason)
	MISSION_FLOW_MISSION_FAILED()
ENDPROC


// PURPOSE: To be called by missions to tell the flow controller they have failed. Allows setting of the fail message that will
//			display on the fail screen. This fail message contains another text label inserted inline.
PROC MISSION_FLOW_MISSION_FAILED_WITH_REASON_CONTAINING_TEXT(STRING failReason, STRING failAddText)
	MISSION_FLOW_SET_FAIL_REASON_CONTAINING_TEXT(failReason, failAddText)
	MISSION_FLOW_MISSION_FAILED()
ENDPROC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Gets called by all missions when they Force Cleanup to let the Mission Flow know how to proceed.
//
// NOTES:   The intention is that the Mission Flow can work out which mission was active without accepting
//              any extra parameters.
PROC MISSION_FLOW_MISSION_FORCE_CLEANUP()

	IF HAS_SCRIPT_LOADED("buddyDeathResponse")
		START_NEW_SCRIPT("buddyDeathResponse", DEFAULT_STACK_SIZE)
		CPRINTLN(DEBUG_FLOW, "MISSION FORCE CLEANUP: Launched the buddy response script to deal with player deaths.")
	ENDIF
#if USE_CLF_DLC
	IF g_savedGlobalsClifford.sFlow.isGameflowActive
	OR IS_REPEAT_PLAY_ACTIVE()
	
		IF NOT IS_REPLAY_BEING_PROCESSED()
		    // Find the available mission that is currently active
		    INT runningAvailableMissionPos = Get_Available_Mission_Index_For_Running_Mission()			
			IF runningAvailableMissionPos != ILLEGAL_ARRAY_POSITION
			    // Call the common Fail and Force_Cleanup function
			    IF NOT (MISSION_FLOW_MISSION_FAILED_AND_FORCE_CLEANUP(runningAvailableMissionPos))
			        EXIT
			    ENDIF				
				// Set the Mission Failed flag
		   		SET_BIT(g_availableMissionsTU[runningAvailableMissionPos].bitflags, BITS_AVAILABLE_MISSION_FAILED)					
			    #IF IS_DEBUG_BUILD
					g_bFlowAutoplayJustPassed = FALSE	
					INT theCoreVarsIndex = g_availableMissionsTU[runningAvailableMissionPos].index
		        	CPRINTLN(DEBUG_FLOW, "MISSION FORCE CLEANUP: ", GET_SCRIPT_FROM_HASH(g_sMissionStaticData[g_flowUnsaved.coreVars[theCoreVarsIndex].iValue1].scriptHash))
				#ENDIF				
				EXIT
			ELSE
				CERRORLN(DEBUG_FLOW, "Mission_Flow_Mission_Force_Cleanup: Tried to clean up a mission script that was no longer running.")
			ENDIF
		ELSE
			// replay is being processed, handle player being killed during fail screen..
			// Set whether player gets warped when rejecting replay + update fail reason if killed / arrested
			SET_FORCE_CLEANUP_FAIL_REASON()
		ENDIF
	ENDIF
#endif
#if USE_NRM_DLC
	IF g_savedGlobalsnorman.sFlow.isGameflowActive
	OR IS_REPEAT_PLAY_ACTIVE()
	
		IF NOT IS_REPLAY_BEING_PROCESSED()
		    // Find the available mission that is currently active
		    INT runningAvailableMissionPos = Get_Available_Mission_Index_For_Running_Mission()
			
			IF runningAvailableMissionPos != ILLEGAL_ARRAY_POSITION
			    // Call the common Fail and Force_Cleanup function
			    IF NOT (MISSION_FLOW_MISSION_FAILED_AND_FORCE_CLEANUP(runningAvailableMissionPos))
			        EXIT
			    ENDIF
				
				// Set the Mission Failed flag
		   		SET_BIT(g_availableMissionsTU[runningAvailableMissionPos].bitflags, BITS_AVAILABLE_MISSION_FAILED)
								
			    #IF IS_DEBUG_BUILD
					g_bFlowAutoplayJustPassed = FALSE	
					INT theCoreVarsIndex = g_availableMissionsTU[runningAvailableMissionPos].index		        	
					CPRINTLN(DEBUG_FLOW, "MISSION FORCE CLEANUP: ", GET_SCRIPT_FROM_HASH_NRM(g_sMissionStaticData[g_flowUnsaved.coreVars[theCoreVarsIndex].iValue1].scriptHash))					
				#ENDIF
				
				EXIT
			ELSE
				CERRORLN(DEBUG_FLOW, "Mission_Flow_Mission_Force_Cleanup: Tried to clean up a mission script that was no longer running.")
			ENDIF
		ELSE
			// replay is being processed, handle player being killed during fail screen..
			// Set whether player gets warped when rejecting replay + update fail reason if killed / arrested
			SET_FORCE_CLEANUP_FAIL_REASON()
		ENDIF
	ENDIF
#endif

#if not USE_CLF_DLC
#if not USE_NRM_DLC
	IF g_savedGlobals.sFlow.isGameflowActive
	OR IS_REPEAT_PLAY_ACTIVE()
	
		IF NOT IS_REPLAY_BEING_PROCESSED()
		    // Find the available mission that is currently active
		    INT runningAvailableMissionPos = Get_Available_Mission_Index_For_Running_Mission()
			
			IF runningAvailableMissionPos != ILLEGAL_ARRAY_POSITION
			    // Call the common Fail and Force_Cleanup function
			    IF NOT (MISSION_FLOW_MISSION_FAILED_AND_FORCE_CLEANUP(runningAvailableMissionPos))
			        EXIT
			    ENDIF
				
				// Set the Mission Failed flag
		   		SET_BIT(g_availableMissions[runningAvailableMissionPos].bitflags, BITS_AVAILABLE_MISSION_FAILED)
								
			    #IF IS_DEBUG_BUILD
					g_bFlowAutoplayJustPassed = FALSE	
					INT theCoreVarsIndex = g_availableMissions[runningAvailableMissionPos].index
		        	CPRINTLN(DEBUG_FLOW, "MISSION FORCE CLEANUP: ", g_sMissionStaticData[g_flowUnsaved.coreVars[theCoreVarsIndex].iValue1].scriptName)
				#ENDIF
				
				EXIT
			ELSE
				CERRORLN(DEBUG_FLOW, "Mission_Flow_Mission_Force_Cleanup: Tried to clean up a mission script that was no longer running.")
			ENDIF
		ELSE
			// replay is being processed, handle player being killed during fail screen..
			// Set whether player gets warped when rejecting replay + update fail reason if killed / arrested
			SET_FORCE_CLEANUP_FAIL_REASON()
		ENDIF
	ENDIF
#endif	
#endif
ENDPROC

// ------------------------------------------------------------------------------------------------------------------------------------

// PURPOSE: Registers the designated script for relaunch upon save game restoration.
//
// INPUT PARAMS:    ParamBit        The bit, designated by enum entry, of FLOWBITSET_RESTART_FLOW_LAUNCHED_SCRIPTS desired to be set
//
// RETURN VALUE:    BOOL            The function will return FALSE if the chosen bit has already been set, TRUE otherwise.
FUNC BOOL REGISTER_SCRIPT_TO_RELAUNCH_LIST(e_g_Restore_Launched_ScriptBits ParamBit)

    #IF IS_DEBUG_BUILD
        PRINTSTRING ("RELAUNCH SYSTEM - Script call made to ADD ")
        PRINTSTRING (g_Restore_Launched_Script[ParamBit].Name) 
        PRINTSTRING (" to relaunch list...")
        PRINTNL()
    #ENDIF
	
	
#if USE_CLF_DLC
ParamBit = ParamBit
	return false
#endif
#if USE_NRM_DLC    
ParamBit = ParamBit
    RETURN FALSE	
#endif

#if not USE_CLF_DLC
#if not USE_NRM_DLC

	FLOW_BITSET_IDS eFlowBitsetToQuery
	INT iOffsetBitIndex
	
	//Is this bit in our first or second bitset?
	IF ENUM_TO_INT(ParamBit) <= 31
		eFlowBitsetToQuery = FLOWBITSET_RESTART_FLOW_LAUNCHED_SCRIPTS_1
		iOffsetBitIndex = ENUM_TO_INT(ParamBit)
	ELSE
		eFlowBitsetToQuery = FLOWBITSET_RESTART_FLOW_LAUNCHED_SCRIPTS_2
		iOffsetBitIndex = ENUM_TO_INT(ParamBit) - 32
	ENDIF
	IF IS_BIT_SET (g_savedGlobals.sFlow.controls.bitsetIDs[eFlowBitsetToQuery], iOffsetBitIndex)
	
        #IF IS_DEBUG_BUILD
            PRINTSTRING ("but the relaunch bit has already been set for ")
            PRINTSTRING (g_Restore_Launched_Script[ParamBit].Name) 
            PRINTSTRING (".sc")
            PRINTNL()
        #ENDIF

        RETURN FALSE
		
    ELSE
	
        SET_BIT (g_savedGlobals.sFlow.controls.bitsetIDs[eFlowBitsetToQuery], iOffsetBitIndex)    
		
        #IF IS_DEBUG_BUILD
            PRINTSTRING ("and relaunch bit for ")
            PRINTSTRING (g_Restore_Launched_Script[ParamBit].Name) 
            PRINTSTRING (".sc ")
            PRINTSTRING ("successfully set!")
            PRINTNL()
        #ENDIF

        RETURN TRUE
    ENDIF
#endif
#endif
ENDFUNC




// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Removes the designated script from relaunch list so that it will not be loaded upon save game restoration.
//
// INPUT PARAMS:    ParamBit        The bit, designated by enum entry, of FLOWBITSET_RESTART_FLOW_LAUNCHED_SCRIPTS desired to be cleared
//
// RETURN VALUE:    BOOL            The function will return FALSE if the chosen bit is not already in SET state, TRUE otherwise.
FUNC BOOL REMOVE_SCRIPT_FROM_RELAUNCH_LIST(e_g_Restore_Launched_ScriptBits ParamBit)

#if USE_CLF_DLC
ParamBit = ParamBit
   	RETURN FALSE    
#endif
#if USE_NRM_DLC
ParamBit = ParamBit
    RETURN FALSE   
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC
	#IF IS_DEBUG_BUILD
        PRINTSTRING ("RELAUNCH SYSTEM - Script call made to REMOVE ")
        PRINTSTRING (g_Restore_Launched_Script[ParamBit].Name) 
        PRINTSTRING (" from relaunch list...")
        PRINTNL()
    #ENDIF
	
	FLOW_BITSET_IDS eFlowBitsetToQuery
	INT iOffsetBitIndex
	//Is this bit in our first or second bitset?
	IF ENUM_TO_INT(ParamBit) <= 31
		eFlowBitsetToQuery = FLOWBITSET_RESTART_FLOW_LAUNCHED_SCRIPTS_1
		iOffsetBitIndex = ENUM_TO_INT(ParamBit)
	ELSE
		eFlowBitsetToQuery = FLOWBITSET_RESTART_FLOW_LAUNCHED_SCRIPTS_2
		iOffsetBitIndex = ENUM_TO_INT(ParamBit) - 32
	ENDIF
	
    IF IS_BIT_SET (g_savedGlobals.sFlow.controls.bitsetIDs[eFlowBitsetToQuery], iOffsetBitIndex)
	
        CLEAR_BIT (g_savedGlobals.sFlow.controls.bitsetIDs[eFlowBitsetToQuery], iOffsetBitIndex)

        #IF IS_DEBUG_BUILD
            PRINTSTRING ("and relaunch bit for ")
            PRINTSTRING (g_Restore_Launched_Script[ParamBit].Name) 
            PRINTSTRING (".sc ")
            PRINTSTRING ("successfully cleared!")
            PRINTNL()
        #ENDIF

        RETURN TRUE
    ELSE     
        #IF IS_DEBUG_BUILD
            PRINTSTRING ("but the relaunch bit is not currently set for ")
            PRINTSTRING (g_Restore_Launched_Script[ParamBit].Name) 
            PRINTSTRING (".sc")
            PRINTNL()
        #ENDIF

        RETURN FALSE
    ENDIF

#endif
#endif
ENDFUNC

// ===========================================================================================================
//      Request_and_Load_Scripts_for_Restore_Launch
// ===========================================================================================================

// PURPOSE: Cross references bit state of FLOWBITSET_RESTART_FLOW_LAUNCHED_SCRIPTS bitset with                                   
//          g_Restore_Launched_ScriptNames array to ascertain which scripts have been launched through flow
//          progress and relaunch upon save game restoration.
PROC Request_and_Load_Scripts_for_Restore_Launch()
    CPRINTLN(DEBUG_SYSTEM,"Request_and_Load_Scripts_for_Restore_Launch launched by script ",GET_THIS_SCRIPT_NAME())
    CPRINTLN(DEBUG_INIT_SP, "Checking for any required relaunches of flow dependent scripts.") 
	
	//Clifford ambient scripts go here
	#IF USE_CLF_DLC
	#ENDIF
	
	//Norman ambient scripts go here
	#IF USE_NRM_DLC
	#ENDIF

	//SP ambient scripts go here
	#IF NOT USE_SP_DLC
    IF ENUM_TO_INT(MAXIMUM_LAUNCH_BITS) > 60 //Upped by Steve T 10.01.13  bug #1013186
        SCRIPT_ASSERT("Request_and_Load_Scripts_for_Restore_Launch: MAXIMUM_LAUNCH_BITS nearing bitset size limit. Tell SteveT.")
    ENDIF
    
    FLOW_BITSET_IDS eFlowBitsetToQuery
    INT iOffsetBitIndex
    
    //First request all scripts in one frame.
    INT loopIndex
    REPEAT MAXIMUM_LAUNCH_BITS loopIndex
        
        //Is this bit in our first or second bitset?
        IF loopIndex <= 31
            eFlowBitsetToQuery = FLOWBITSET_RESTART_FLOW_LAUNCHED_SCRIPTS_1
            iOffsetBitIndex = loopIndex
        ELSE
            eFlowBitsetToQuery = FLOWBITSET_RESTART_FLOW_LAUNCHED_SCRIPTS_2
            iOffsetBitIndex = loopIndex - 32
        ENDIF
        
        IF IS_BIT_SET (g_savedGlobals.sFlow.controls.bitsetIDs[eFlowBitsetToQuery], iOffsetBitIndex)
			//Check that this bit is bound to a script. If it's not clear the bit.
			IF NOT IS_STRING_NULL_OR_EMPTY(g_Restore_Launched_Script[loopIndex].Name)
	            CPRINTLN(DEBUG_INIT_SP, "Relaunch bit has been set for ", g_Restore_Launched_Script[loopIndex].Name, ".sc. Requesting.")  
	            IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(g_Restore_Launched_Script[loopIndex].NameHash) > 0
	                CPRINTLN(DEBUG_INIT_SP, "Warning - script already running. Abandoning request and load.")
	            ELSE
	                REQUEST_SCRIPT (g_Restore_Launched_Script[loopIndex].Name)
	            ENDIF
			ELSE
				CPRINTLN(DEBUG_INIT_SP, "Relaunch bit set for null relaunch index. Clearing bit.")
				CLEAR_BIT(g_savedGlobals.sFlow.controls.bitsetIDs[eFlowBitsetToQuery], iOffsetBitIndex)
			ENDIF
        ENDIF
    ENDREPEAT
    
    //Now wait for all scripts to be started.
    BOOL bAllLaunched = FALSE
    WHILE NOT bAllLaunched
        bAllLaunched = TRUE
        
        REPEAT MAXIMUM_LAUNCH_BITS loopIndex
        
            //Is this bit in our first or second bitset?
            IF loopIndex <= 31
                eFlowBitsetToQuery = FLOWBITSET_RESTART_FLOW_LAUNCHED_SCRIPTS_1
                iOffsetBitIndex = loopIndex
            ELSE
                eFlowBitsetToQuery = FLOWBITSET_RESTART_FLOW_LAUNCHED_SCRIPTS_2
                iOffsetBitIndex = loopIndex - 32
            ENDIF
        
            IF IS_BIT_SET (g_savedGlobals.sFlow.controls.bitsetIDs[eFlowBitsetToQuery], iOffsetBitIndex)
                IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(g_Restore_Launched_Script[loopIndex].NameHash) = 0
                    IF NOT HAS_SCRIPT_LOADED (g_Restore_Launched_Script[loopIndex].Name)
                        bAllLaunched = FALSE
                    ELSE
                        CPRINTLN(DEBUG_INIT_SP, "Started relaunch script ", g_Restore_Launched_Script[loopIndex].Name, ".sc.")  
                        START_NEW_SCRIPT (g_Restore_Launched_Script[loopIndex].Name, g_Restore_Launched_Script[loopIndex].Stacksize) //we may need to have a matching array of stack sizes or use a struct for each script.
                        SET_SCRIPT_AS_NO_LONGER_NEEDED (g_Restore_Launched_Script[loopIndex].Name)
                    ENDIF
                ENDIF
            ENDIF
        ENDREPEAT
        
        WAIT(0)
    ENDWHILE
	#ENDIF
    
    CPRINTLN(DEBUG_INIT_SP, "All relaunch scripts have started.")  
ENDPROC



// PURPOSE: Sets the flow controller to start loading an intro cutscene for a mission. Ownership of
// this cutscene will be automatically handed over to the mission script as it launches.
//
// INPUT PARAMS:	paramMissionID			The enum ID of the mission that needs to have its intro cutscene loaded by the flow.
//					paramCutsceneName		The name of the cutscene to load for this mission.
//					paramMichaelSections	OR-ed bitflags defining the cutscene sections to load if Michael is the player character 
//											as the request is made.
//					paramFranklinSections	OR-ed bitflags defining the cutscene sections to load if Franklin is the player character 
//											as the request is made.
//					paramMichaelSections	OR-ed bitflags defining the cutscene sections to load if Trevor is the player character 
//											as the request is made.
PROC MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(	SP_MISSIONS paramMissionID,
													STRING paramCutsceneName, 
													INT paramMichaelSections = CS_NONE,
													INT paramFranklinSections = CS_NONE,
													INT paramTrevorSections = CS_NONE)

#if USE_CLF_DLC	
	IF g_savedGlobalsClifford.sFlow.isGameflowActive
		IF NOT (g_bFlowLoadIntroCutscene AND g_eMissionIDToLoadCutscene = paramMissionID)
			CPRINTLN(DEBUG_FLOW, "Script ", GET_THIS_SCRIPT_NAME(), " requested streaming of intro mocap ", paramCutsceneName, " for mission ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMissionID), ".")

			//Set the loadMocap flag for this mission.
			g_eMissionIDToLoadCutscene = paramMissionID
			g_txtIntroMocapToLoad = paramCutsceneName
			g_iCharIntroSectionsToLoad[CHAR_MICHAEL] = paramMichaelSections
			g_iCharIntroSectionsToLoad[CHAR_FRANKLIN] = paramFranklinSections
			g_iCharIntroSectionsToLoad[CHAR_TREVOR] = paramTrevorSections
			
			//Flag the flow controller to start loading a cutscene next frame.
			g_bFlowLoadIntroCutscene 			= TRUE
			g_bFlowIntroVariationRequestsMade 	= FALSE
			PRIVATE_Cleanup_Cutscene_Handles()
		ENDIF
	ENDIF
#endif
#if USE_NRM_DLC
	IF g_savedGlobalsnorman.sFlow.isGameflowActive
		IF NOT (g_bFlowLoadIntroCutscene AND g_eMissionIDToLoadCutscene = paramMissionID)
			CPRINTLN(DEBUG_FLOW, "Script ", GET_THIS_SCRIPT_NAME(), " requested streaming of intro mocap ", paramCutsceneName, " for mission ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMissionID), ".")

			//Set the loadMocap flag for this mission.
			g_eMissionIDToLoadCutscene = paramMissionID
			g_txtIntroMocapToLoad = paramCutsceneName
			g_iCharIntroSectionsToLoad[CHAR_MICHAEL] = paramMichaelSections
			g_iCharIntroSectionsToLoad[CHAR_FRANKLIN] = paramFranklinSections
			g_iCharIntroSectionsToLoad[CHAR_TREVOR] = paramTrevorSections
			
			//Flag the flow controller to start loading a cutscene next frame.
			g_bFlowLoadIntroCutscene 			= TRUE
			g_bFlowIntroVariationRequestsMade 	= FALSE
			PRIVATE_Cleanup_Cutscene_Handles()
		ENDIF
	ENDIF
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC
	IF g_savedGlobals.sFlow.isGameflowActive
		IF NOT (g_bFlowLoadIntroCutscene AND g_eMissionIDToLoadCutscene = paramMissionID)
			CPRINTLN(DEBUG_FLOW, "Script ", GET_THIS_SCRIPT_NAME(), " requested streaming of intro mocap ", paramCutsceneName, " for mission ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMissionID), ".")

			//Set the loadMocap flag for this mission.
			g_eMissionIDToLoadCutscene = paramMissionID
			g_txtIntroMocapToLoad = paramCutsceneName
			g_iCharIntroSectionsToLoad[CHAR_MICHAEL] = paramMichaelSections
			g_iCharIntroSectionsToLoad[CHAR_FRANKLIN] = paramFranklinSections
			g_iCharIntroSectionsToLoad[CHAR_TREVOR] = paramTrevorSections
			
			//Flag the flow controller to start loading a cutscene next frame.
			g_bFlowLoadIntroCutscene 			= TRUE
			g_bFlowIntroVariationRequestsMade 	= FALSE
			PRIVATE_Cleanup_Cutscene_Handles()
		ENDIF
	ENDIF
#endif
#endif
ENDPROC


// PURPOSE: Resets and cleans up any cutscene preloading the flow is currently doing. Includes variation
//			and prop requests.
//
PROC MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
#if USE_CLF_DLC
	IF g_savedGlobalsClifford.sFlow.isGameflowActive
		CPRINTLN(DEBUG_FLOW, "Script ", GET_THIS_SCRIPT_NAME(), " requested to clean up intro mocap.")

		g_bFlowCleanupIntroCutscene = TRUE
	ENDIF
#endif	
#if USE_NRM_DLC
	IF g_savedGlobalsnorman.sFlow.isGameflowActive
		CPRINTLN(DEBUG_FLOW, "Script ", GET_THIS_SCRIPT_NAME(), " requested to clean up intro mocap.")

		g_bFlowCleanupIntroCutscene = TRUE
	ENDIF
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC
	IF g_savedGlobals.sFlow.isGameflowActive
		CPRINTLN(DEBUG_FLOW, "Script ", GET_THIS_SCRIPT_NAME(), " requested to clean up intro mocap.")

		g_bFlowCleanupIntroCutscene = TRUE
	ENDIF
#endif	
#endif
ENDPROC


// PURPOSE: Tells the flow to set a specific variation on a ped	in a mission intro cutscene that is being preloaded.
//			Should be called directly after a MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE request.
//
// INPUT PARAMS:	paramMissionID		The mission that the intro cutscene belongs to. Used to check this request is being made at the correct time.
//					paramSceneHandle	The scene handle of the ped to have the variations set in the cutscene.
//					paramComponent		The component that is to be set on the ped.
//					paramDrawable		The drawable to use on the selected component.
//					paramTexture		The texture to use on the selected component.
//
PROC MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSIONS paramMissionID, STRING paramSceneHandle, PED_COMPONENT paramComponent, INT paramDrawable, INT paramTexture)
	IF g_eMissionIDToLoadCutscene = paramMissionID
		IF g_iFlowCurrentCutsceneVariationCount < MAX_QUEUED_CUTSCENE_VARIATIONS

			//Store the request in the global queue for the flow controller to operate on.
			g_sFlowCutsceneVariations[g_iFlowCurrentCutsceneVariationCount].iHandleIndex = PRIVATE_Register_Flow_Cutscene_Handle(paramSceneHandle)
			g_sFlowCutsceneVariations[g_iFlowCurrentCutsceneVariationCount].eComponent = paramComponent
			g_sFlowCutsceneVariations[g_iFlowCurrentCutsceneVariationCount].iDrawable = paramDrawable
			g_sFlowCutsceneVariations[g_iFlowCurrentCutsceneVariationCount].iTexture = paramTexture
			g_iFlowCurrentCutsceneVariationCount++
			
			//Block auto updates to a player ped's variations for this component.
			enumCharacterList ePlayerChar = PRIVATE_Get_Player_Ped_Enum_From_Scene_Handle(paramSceneHandle)
			IF ePlayerChar != CHAR_BLANK_ENTRY
				SET_BIT(g_iFlowBitsetBlockPlayerCutsceneVariations[ENUM_TO_INT(ePlayerChar)], ENUM_TO_INT(paramComponent))
			ENDIF
			
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_FLOW, 	"Cutscene variation request registered for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMissionID), 
										": Handle[", paramSceneHandle,
										"] HandleIndex[", g_sFlowCutsceneVariations[g_iFlowCurrentCutsceneVariationCount-1].iHandleIndex,
										"] Comp[", ENUM_TO_INT(paramComponent),
										"] Draw[", paramDrawable,
										"] Text[", paramTexture, "].")
			#ENDIF
		ELSE
			SCRIPT_ASSERT("MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION: Too many cutscene variation requests have been made at once. Failed to register request. Increase MAX_QUEUED_CUTSCENE_VARIATIONS?")
		ENDIF
	ELSE
		SCRIPT_ASSERT("MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION: Tried to set flow cutscene variation for a mission that isn't currently loading a cutscene. Failed to register request.")
	ENDIF
ENDPROC


// PURPOSE: Tells the flow to set the variations for a ped in a mission intro cutscene based on the components defined
//			in an outfit enum. Should be called directly after a MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE request.
//
// INPUT PARAMS:	paramMissionID		The mission that the intro cutscene belongs to. Used to check this request is being made at the correct time.
//					paramSceneHandle	The scene handle of the ped to have the variations set in the cutscene.
//					paramModel			The model of the character to be configured.
//					paramOutfit			The outfit enum referencing the set of component variations to be applied.
//
PROC MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_OUTFIT(SP_MISSIONS paramMissionID, STRING paramSceneHandle, MODEL_NAMES paramModel, PED_COMP_NAME_ENUM paramOutfit)
	IF g_eMissionIDToLoadCutscene = paramMissionID
		IF g_iFlowCurrentCutsceneOutfitCount < MAX_QUEUED_OUTFIT_VARIATIONS
		
			//Store the request in the global queue for the flow controller to operate on.
			g_sFlowCutsceneOutfit[g_iFlowCurrentCutsceneOutfitCount].iHandleIndex = PRIVATE_Register_Flow_Cutscene_Handle(paramSceneHandle)
			g_sFlowCutsceneOutfit[g_iFlowCurrentCutsceneOutfitCount].eModel = paramModel
			g_sFlowCutsceneOutfit[g_iFlowCurrentCutsceneOutfitCount].eOutfit = paramOutfit
			g_iFlowCurrentCutsceneOutfitCount++

			//Block auto updates to a player ped's variations for all components as we set an outfit.
			enumCharacterList ePlayerChar = PRIVATE_Get_Player_Ped_Enum_From_Scene_Handle(paramSceneHandle)
			IF ePlayerChar != CHAR_BLANK_ENTRY
				INT index
				PED_COMP_OUTFIT_DATA_STRUCT sOutfitComps = GET_PED_COMPONENT_DATA_FOR_OUTFIT(paramModel, paramOutfit)
				REPEAT NUM_PED_COMPONENTS index
					PED_COMP_TYPE_ENUM eCompType = GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, index))
					INT iPedCompIndex = ENUM_TO_INT(eCompType)
					IF (eCompType = COMP_TYPE_HAIR OR eCompType = COMP_TYPE_HEAD)
					AND sOutfitComps.eItems[iPedCompIndex] = DUMMY_PED_COMP
						CLEAR_BIT(g_iFlowBitsetBlockPlayerCutsceneVariations[ENUM_TO_INT(ePlayerChar)], index)
					ELSE
						SET_BIT(g_iFlowBitsetBlockPlayerCutsceneVariations[ENUM_TO_INT(ePlayerChar)], index)
					ENDIF
				ENDREPEAT
				REPEAT NUMBER_OF_PED_PROP_TYPES index
					SET_BIT(g_iFlowBitsetBlockPlayerCutsceneVariations[ENUM_TO_INT(ePlayerChar)], NUM_PED_COMPONENTS + index)
				ENDREPEAT
			ENDIF
			
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_FLOW, 	"Cutscene outfit registered for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMissionID), 
										": Handle[", paramSceneHandle,
										"] HandleIndex[", g_sFlowCutsceneOutfit[g_iFlowCurrentCutsceneOutfitCount-1].iHandleIndex,
										"] Model[", ENUM_TO_INT(paramModel),
										"] Outfit[", ENUM_TO_INT(paramOutfit), "].")
			#ENDIF
		ELSE
			SCRIPT_ASSERT("MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_OUTFIT: Too many outfit variation requests have been made at once. Failed to register request. Increase MAX_QUEUED_OUTFIT_VARIATIONS?")
		ENDIF
	ELSE
		SCRIPT_ASSERT("MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_OUTFIT: Tried to set flow outfit variation for a mission that isn't currently loading a cutscene. Failed to register request.")
	ENDIF
ENDPROC


// PURPOSE: Tells the flow to set the variations of a ped that exists in the world as the variations of a ped
//			in a mission intro cutscene. Should be called directly after a 
//			MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE request.
//
// INPUT PARAMS:	paramMissionID		The mission that the intro cutscene belongs to. Used to check this request is being made at the correct time.
//					paramSceneHandle	The scene handle of the ped to have the variations set in the cutscene.
//					paramPed			The ped to lift the variations from.
//
PROC MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_MISSIONS paramMissionID, STRING paramSceneHandle, PED_INDEX paramPed)
	IF g_eMissionIDToLoadCutscene = paramMissionID
		IF g_iFlowCurrentPedVariationCount < MAX_QUEUED_PED_VARIATIONS
		
			//Store the request in the global queue for the flow controller to operate on.
			g_sFlowPedVariation[g_iFlowCurrentPedVariationCount].iHandleIndex = PRIVATE_Register_Flow_Cutscene_Handle(paramSceneHandle)
			g_sFlowPedVariation[g_iFlowCurrentPedVariationCount].ped = paramPed
			g_iFlowCurrentPedVariationCount++
			
			//Block auto updates to a player ped's variations for all components.
			enumCharacterList ePlayerChar = PRIVATE_Get_Player_Ped_Enum_From_Scene_Handle(paramSceneHandle)
			IF ePlayerChar != CHAR_BLANK_ENTRY
				SET_BIT(g_iFlowBitsetBlockPlayerCutsceneVariations[ENUM_TO_INT(ePlayerChar)], ENUM_TO_INT(PED_COMP_HEAD))
				SET_BIT(g_iFlowBitsetBlockPlayerCutsceneVariations[ENUM_TO_INT(ePlayerChar)], ENUM_TO_INT(PED_COMP_BERD))
				SET_BIT(g_iFlowBitsetBlockPlayerCutsceneVariations[ENUM_TO_INT(ePlayerChar)], ENUM_TO_INT(PED_COMP_HAIR))
				SET_BIT(g_iFlowBitsetBlockPlayerCutsceneVariations[ENUM_TO_INT(ePlayerChar)], ENUM_TO_INT(PED_COMP_TORSO))
				SET_BIT(g_iFlowBitsetBlockPlayerCutsceneVariations[ENUM_TO_INT(ePlayerChar)], ENUM_TO_INT(PED_COMP_LEG))
				SET_BIT(g_iFlowBitsetBlockPlayerCutsceneVariations[ENUM_TO_INT(ePlayerChar)], ENUM_TO_INT(PED_COMP_HAND))
				SET_BIT(g_iFlowBitsetBlockPlayerCutsceneVariations[ENUM_TO_INT(ePlayerChar)], ENUM_TO_INT(PED_COMP_FEET))
				SET_BIT(g_iFlowBitsetBlockPlayerCutsceneVariations[ENUM_TO_INT(ePlayerChar)], ENUM_TO_INT(PED_COMP_TEETH))
				SET_BIT(g_iFlowBitsetBlockPlayerCutsceneVariations[ENUM_TO_INT(ePlayerChar)], ENUM_TO_INT(PED_COMP_SPECIAL))
				SET_BIT(g_iFlowBitsetBlockPlayerCutsceneVariations[ENUM_TO_INT(ePlayerChar)], ENUM_TO_INT(PED_COMP_SPECIAL2))
				SET_BIT(g_iFlowBitsetBlockPlayerCutsceneVariations[ENUM_TO_INT(ePlayerChar)], ENUM_TO_INT(PED_COMP_DECL))
				SET_BIT(g_iFlowBitsetBlockPlayerCutsceneVariations[ENUM_TO_INT(ePlayerChar)], ENUM_TO_INT(PED_COMP_JBIB))
			ENDIF
			
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_FLOW, 	"Cutscene ped registered for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMissionID), 
										": Handle[", paramSceneHandle, 
										"] HandleIndex[", g_sFlowPedVariation[g_iFlowCurrentPedVariationCount-1].iHandleIndex, "].")
			#ENDIF
		ELSE
			SCRIPT_ASSERT("MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED: Too many ped variation requests have been made at once. Failed to register request. Increase MAX_QUEUED_PED_VARIATIONS?")
		ENDIF
	ELSE
		SCRIPT_ASSERT("MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED: Tried to set flow ped variation for a mission that isn't currently loading a cutscene. Failed to register request.")
	ENDIF
ENDPROC


// PURPOSE: Tells the flow to force a specific prop on a ped for a mission intro cutscene. Should be called 
//			directly after a MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE request.
//
// INPUT PARAMS:	paramMissionID		The mission that the intro cutscene belongs to. Used to check this request is being made at the correct time.
//					paramSceneHandle	The scene handle of the ped to have the prop set in the cutscene.
//					paramPropPosition	The position the prop should be attached to the ped in the cutscene.
//					paramPropIndex		The ID of the prop to set.
//					paramPropTexture 	The ID of the texture to use on the prop.
//
PROC MISSION_FLOW_SET_INTRO_CUTSCENE_PROP(SP_MISSIONS paramMissionID, STRING paramSceneHandle, PED_PROP_POSITION paramPropPosition, INT paramPropIndex, INT paramPropTexture)
	IF g_eMissionIDToLoadCutscene = paramMissionID
		IF g_iFlowCurrentCutscenPropCount < MAX_QUEUED_CUTSCENE_PROPS
		
			//Store the request in the global queue for the flow controller to operate on.
			g_sFlowCutsceneProp[g_iFlowCurrentCutscenPropCount].iHandleIndex = PRIVATE_Register_Flow_Cutscene_Handle(paramSceneHandle)
			g_sFlowCutsceneProp[g_iFlowCurrentCutscenPropCount].ePosition = paramPropPosition
			g_sFlowCutsceneProp[g_iFlowCurrentCutscenPropCount].iPropIndex = paramPropIndex
			g_sFlowCutsceneProp[g_iFlowCurrentCutscenPropCount].iTextureIndex = paramPropTexture
			g_iFlowCurrentCutscenPropCount++
			
			//Block auto updates to a player ped's prop variations for all prop slots.
			enumCharacterList ePlayerChar = PRIVATE_Get_Player_Ped_Enum_From_Scene_Handle(paramSceneHandle)
			IF ePlayerChar != CHAR_BLANK_ENTRY
				SET_BIT(g_iFlowBitsetBlockPlayerCutsceneVariations[ENUM_TO_INT(ePlayerChar)], NUM_PED_COMPONENTS + ENUM_TO_INT(paramPropPosition))
			ENDIF

			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_FLOW, 	"Cutscene prop registered for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMissionID), 
										": Handle[", paramSceneHandle, 
										"] HandleIndex[", g_sFlowCutsceneProp[g_iFlowCurrentCutscenPropCount-1].iHandleIndex,
										"] Position[", ENUM_TO_INT(paramPropPosition),
										"] PropIndex[", paramPropIndex,
										"] PropTexture[", paramPropTexture, "].")
			#ENDIF
		ELSE
			SCRIPT_ASSERT("MISSION_FLOW_SET_INTRO_CUTSCENE_PROP: Too many prop requests have been made at once. Failed to register request. Increase MAX_QUEUED_CUTSCENE_PROPS?")
		ENDIF
	ELSE
		SCRIPT_ASSERT("MISSION_FLOW_SET_INTRO_CUTSCENE_PROP: Tried to set prop for a mission that isn't currently loading a cutscene. Failed to register request.")
	ENDIF
ENDPROC


// PURPOSE: Triggers the end-of-mission music playing. All audio streaming, playing and cleaning-up is handled
//			codeside now. The music will vary based on the current player character.
//
// INPUT PARAMS:	bPlaySmallVariation		When TRUE the small music variation will be played (used by minigames 
//											and ambient missions).
PROC MISSION_FLOW_PLAY_END_OF_MISSION_MUSIC(BOOL bPlaySmallVariation = FALSE)//, BOOL bBlockForUI = FALSE)
	
	REGISTER_SCRIPT_WITH_AUDIO()
	STRING strMusicVariation
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			IF bPlaySmallVariation
				strMusicVariation = "MICHAEL_SMALL_01"
			ELSE
				strMusicVariation = "MICHAEL_BIG_01"
			ENDIF
		BREAK
		CASE CHAR_FRANKLIN
			IF bPlaySmallVariation
				strMusicVariation = "FRANKLIN_SMALL_01"
			ELSE
				strMusicVariation = "FRANKLIN_BIG_01"
			ENDIF
		BREAK
		CASE CHAR_TREVOR
			IF bPlaySmallVariation
				strMusicVariation = "TREVOR_SMALL_01"
			ELSE
				strMusicVariation = "TREVOR_BIG_01"
			ENDIF
		BREAK
	ENDSWITCH
	
	
	CPRINTLN(DEBUG_FLOW_AUDIO, "Mission passed music triggered by script ", GET_THIS_SCRIPT_NAME(),": [", strMusicVariation, "]")	
	PLAY_MISSION_COMPLETE_AUDIO(strMusicVariation)

ENDPROC

// PURPOSE:	Flags a custom block of cleanup script to run after the player has 
//			respawned. Useful for scripts that need to run cleanup on death/arrest
//			after the screen has faded.
//
//	PARAMS	paramRCI	-	An ENUM that links to a custom block of script
//							defined in respawn_cleanup_data_public.sch
//
PROC SET_CLEANUP_TO_RUN_ON_RESPAWN(RESPAWN_CLEANUP_ID paramRCI)
	PRINTLN("<RESPAWN> Script ", GET_THIS_SCRIPT_NAME(), " flagging ", GET_DEBUG_STRING_FROM_RESPAWN_CLEANUP_ID(paramRCI), " to run on next respawn.")
	IF paramRCI != RCI_NONE
		IF g_eTriggerRCI = RCI_NONE
			PRINTLN("<RESPAWN> RESPAWN_CLEANUP_ID flagged sucessfully.")
			g_eTriggerRCI = paramRCI
		ELSE
			SCRIPT_ASSERT("SET_CLEANUP_TO_RUN_ON_RESPAWN: Tried to flag a RESPAWN_CLEANUP_ID to run, but an ID was already flagged.")
		ENDIF
	ELSE
		SCRIPT_ASSERT("SET_CLEANUP_TO_RUN_ON_RESPAWN: RCI_NONE is not a valid RESPAWN_CLEANUP_ID to flag to run.")
	ENDIF
ENDPROC

//DLC Versions of the Function below
#IF USE_SP_DLC
FUNC SP_MISSIONS MISSION_FLOW_GET_MISSION_AT_BLIP_DLC(BLIP_INDEX paramBlip)
	INT iTriggerIndex
	REPEAT MAX_MISSION_TRIGGERS_TU iTriggerIndex
		IF g_TriggerableMissionsTU[iTriggerIndex].bUsed
			// B*1581878 - array overrun safeguard - inactive blips are being set to STATIC_BLIP_NAME_DUMMY_FINAL which happens to be max array size			
			IF g_TriggerableMissionsTU[iTriggerIndex].eBlip != STATIC_BLIP_NAME_DUMMY_FINAL
				//Is this trigger registered to use the blip we are interested in?
				IF g_GameBlips[g_TriggerableMissionsTU[iTriggerIndex].eBlip].biBlip = paramBlip
					RETURN g_TriggerableMissionsTU[iTriggerIndex].eMissionID
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN SP_MISSION_NONE
ENDFUNC
#ENDIF


// PURPOSE:	Gets the mission associated with a blip if there is one.
//
//	PARAMS	paramBlip - The blip we are checking for mission associations.
//
FUNC SP_MISSIONS MISSION_FLOW_GET_MISSION_AT_BLIP(BLIP_INDEX paramBlip)

#IF USE_SP_DLC
	RETURN MISSION_FLOW_GET_MISSION_AT_BLIP_DLC(paramBlip)
#ENDIF

#IF NOT USE_SP_DLC
	INT iTriggerIndex
	REPEAT MAX_MISSION_TRIGGERS iTriggerIndex
		IF g_TriggerableMissions[iTriggerIndex].bUsed
			// B*1581878 - array overrun safeguard - inactive blips are being set to STATIC_BLIP_NAME_DUMMY_FINAL which happens to be max array size			
			IF g_TriggerableMissions[iTriggerIndex].eBlip != STATIC_BLIP_NAME_DUMMY_FINAL
				//Is this trigger registered to use the blip we are interested in?
				IF g_GameBlips[g_TriggerableMissions[iTriggerIndex].eBlip].biBlip = paramBlip
					RETURN g_TriggerableMissions[iTriggerIndex].eMissionID
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN SP_MISSION_NONE
#ENDIF

ENDFUNC


// PURPOSE:	Checks if a mission has a trigger scene and if so starts loading the assets that 
//			the scene will require. Used when skipping taxi rides.
//
//	PARAMS	paramMission - The story mission we want to preload trigger scene assets for.
//
PROC MISSION_FLOW_PRELOAD_ASSETS_FOR_MISSION_TRIGGER(SP_MISSIONS paramMission)
	IF DOES_MISSION_HAVE_TRIGGER_SCENE(paramMission)
		//Yes. Flag this mission to have it's trigger scene assets start loading.
		g_eMissionSceneToPreLoad = paramMission
		g_bMissionTriggerLoading = TRUE
		CPRINTLN(DEBUG_TRIGGER, "Script ", GET_THIS_SCRIPT_NAME(), " requested to preload assets for the ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_eMissionSceneToPreLoad), " trigger scene.")
		EXIT
	ENDIF
ENDPROC


// PURPOSE:	Tells the mission triggering system to force cleanup assets for a mission trigger scene
//			that may have left assets in memory to allow the mission to take ownership of them. Should
//			be called by mission scripts that have trigger scenes after they have taken ownership of
//			the assets they need from the scene.
//
//	PARAMS	paramMissionID	-	The ID of the mission that wants assets released from memory.
//
PROC MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_MISSIONS paramMissionID)
	CPRINTLN(DEBUG_TRIGGER, "Script ", GET_THIS_SCRIPT_NAME(), " requested trigger scene assets for mission ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMissionID), " were released.")

	IF g_eMissionSceneToCleanup != SP_MISSION_NONE
		IF paramMissionID  = g_eMissionSceneToCleanup
			CPRINTLN(DEBUG_TRIGGER, "Mission ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMissionID), " trigger scene flagged to cleanup.")
			g_bCleanupTriggerScene = TRUE
			EXIT
		ELSE
			SCRIPT_ASSERT("MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS: Mission requested trigger scene cleanup when another mission had an outstanding trigger scene. Bug BenR.") 
		ENDIF
	ENDIF
	
	CPRINTLN(DEBUG_TRIGGER, "No scene in memory to clean up.")
ENDPROC



// PURPOSE:	Tells the mission triggering system to force a mission to trigger if there is
//			a registered trigger for that mission.
//
//	PARAMS	paramMissionID	-	The ID of the mission to forcibly trigger.
//
PROC MISSION_FLOW_FORCE_TRIGGER_MISSION(SP_MISSIONS paramMissionID)
	CPRINTLN(DEBUG_TRIGGER, "Script ", GET_THIS_SCRIPT_NAME(), " force triggering mission ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMissionID),".")

	IF g_eMissionForceTrigger = SP_MISSION_NONE
		g_eMissionForceTrigger = paramMissionID
	ELSE
		SCRIPT_ASSERT("MISSION_FLOW_FORCE_TRIGGER_MISSION: A force trigger request was made while an outstanding request was unfulfilled. Tell BenR.")
	ENDIF
ENDPROC


// PURPOSE:	Queries which SP mission is currently running this frame.
//
FUNC SP_MISSIONS MISSION_FLOW_GET_RUNNING_MISSION()
	RETURN g_eRunningMission
ENDFUNC

// PURPOSE:	Queries which SP mission currently has an active trigger scene.
//
FUNC SP_MISSIONS MISSION_FLOW_GET_ACTIVE_TRIGGER_SCENE()
	RETURN g_eMissionSceneToCleanup
ENDFUNC


/// PURPOSE:
///    Contains the area check for the city exile area
/// PARAMS:
///    v - the position
/// RETURNS:
///    true if the coord is inside the exile zone
FUNC BOOL CITY_EXILE_AREA_CHECK(Vector v, BOOL &CityBool)

	IF NOT (IS_SCREEN_FADED_IN() OR IS_SCREEN_FADING_IN())
		RETURN FALSE//cannot trigger exile if the screen is faded
	ENDIF

#IF IS_DEBUG_BUILD
	//Don't ever return that we are in the exile area mid way 
	//through a debug gameflow launch.
	IF g_flowUnsaved.bUpdatingGameflow
		RETURN FALSE
	ENDIF
#ENDIF

	IF v.y < 400.0
		IF v.x < 1400.0
			IF v.x > -1900.0
				IF v.y > -3500.0
					CityBool = TRUE
					RETURN TRUE
				ENDIF 
			ENDIF
		ENDIF  
	ENDIF
	
	IF v.x < 1536.35
		IF v.y > 1016.18
			IF v.y < 1213.53
				IF v.x > 1278.08
					CityBool = FALSE
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Find out if the current missions is a dlc mission
///    
/// RETURNS: 	TRUE if dlc mission is runnnig 
///    			FALSE if mission is not dlc or no mission is runnning
FUNC BOOL IS_CURRENT_MISSION_DLC_CLF()
	//Check for Agent T game flow
	
	if g_bLoadedClifford
		IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY)
			IF g_savedGlobalsClifford.sFlow.isGameflowActive
			OR IS_REPEAT_PLAY_ACTIVE()
				RETURN TRUE
			ENDIF
		ENDIF
	endif	
	
	return false
ENDFUNC
FUNC BOOL IS_CURRENT_MISSION_DLC_NRM()
	//Check for Agent T game flow
	
	if g_bLoadedNorman
		IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY)
			IF g_savedGlobalsnorman.sFlow.isGameflowActive
			OR IS_REPEAT_PLAY_ACTIVE()
				RETURN TRUE
			ENDIF
		ENDIF
	endif	
	
	return false
ENDFUNC
FUNC BOOL IS_CURRENT_MISSION_DLC()
	//Check for Agent T game flow
	#IF USE_CLF_DLC
		IS_CURRENT_MISSION_DLC_CLF()
	#ENDIF
	#IF USE_NRM_DLC
		IS_CURRENT_MISSION_DLC_NRM()
	#ENDIF
	
	IF g_bLoadedClifford
		IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY)
			IF g_savedGlobalsClifford.sFlow.isGameflowActive
			OR IS_REPEAT_PLAY_ACTIVE()
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	IF g_bLoadedNorman
		IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY)
			IF g_savedGlobalsnorman.sFlow.isGameflowActive
			OR IS_REPEAT_PLAY_ACTIVE()
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	
	RETURN FALSE
ENDFUNC

////
////	Feature blocking 
////

ENUM FEATURE_BLOCK_REASON	//	Reasons for blocking features such as Feature and benchmark testing.
	FBR_NONE = 0,
	
	FBR_FORCE_CLEANUP,
	FBR_RUNNING_CURR,
	FBR_ON_MISSION,
	FBR_IN_CUTSCENE,
	FBR_IN_SHOP,
	FBR_WANTED,
	FBR_ONLINE,
	FBR_UNSAFE_ACTION,
	FBR_UNSAFE_LOCATION,
	FBR_IN_VEHICLE,	
	FBR_WEARING_PARA,	
	FBR_FALLING,		
	FBR_DEAD			
ENDENUM

BOOL g_bFeaturesInteriorsCached = FALSE

/// PURPOSE:
///    Returns the text label for the alert screen id blocking has occured
/// PARAMS:
///    eBlockReason - pass in the block reason from GET_FEATURE_BLOCKED_REASON()
/// RETURNS:
///    
FUNC STRING GET_FEATURE_BLOCK_MESSAGE_TEXT(FEATURE_BLOCK_REASON eBlockReason, DEBUG_CHANNELS dbChannel = DEBUG_MISSION)
	dbChannel = dbChannel
	SWITCH eBlockReason
		CASE FBR_FORCE_CLEANUP		RETURN "FBR_BLK_CLEAN"	BREAK
		CASE FBR_RUNNING_CURR		RETURN "FBR_BLK_RNNNG"	BREAK
		CASE FBR_DEAD				RETURN "FBR_BLK_DEAD"	BREAK
		CASE FBR_ON_MISSION			RETURN "FBR_BLK_MISS" 	BREAK
		CASE FBR_IN_SHOP			RETURN "FBR_BLK_SHOP"	BREAK
		CASE FBR_IN_CUTSCENE		RETURN "FBR_BLK_CUTS"	BREAK
		CASE FBR_WANTED				RETURN "FBR_BLK_WANT"	BREAK
		CASE FBR_ONLINE				RETURN "FBR_BLK_ONLI"	BREAK
		CASE FBR_UNSAFE_ACTION		RETURN "FBR_BLK_ACT"	BREAK
		CASE FBR_UNSAFE_LOCATION	RETURN "FBR_BLK_LOC"	BREAK
		CASE FBR_IN_VEHICLE			RETURN "FBR_BLK_VEH"	BREAK
		CASE FBR_WEARING_PARA		RETURN "FBR_BLK_PARA"	BREAK
		CASE FBR_FALLING			RETURN "FBR_BLK_FALL"	BREAK
		DEFAULT
			CASSERTLN(dbChannel, GET_THIS_SCRIPT_NAME()," -Feature Blocking- Feature is blocked for a reason that is not being handled. RowanJ")
			RETURN "ERROR"
		BREAK
	ENDSWITCH
	RETURN "ERROR"
ENDFUNC

/// PURPOSE:
///    Cache interior indices for blocked areas
PROC CACHE_FEATURE_BLOCKED_INTERIORS(DEBUG_CHANNELS dbChannel = DEBUG_MISSION)
	dbChannel = dbChannel
	CPRINTLN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocking- Caching blocked interior instances")
	GET_INTERIOR_INSTANCE_INDEX_FOR_INTERIOR(INTERIOR_V_58_SOL_OFFICE, g_intSolomonOffice)
	GET_INTERIOR_INSTANCE_INDEX_FOR_INTERIOR(INTERIOR_V_GARAGEM_SP, g_intPlayerGarage)
	GET_INTERIOR_INSTANCE_INDEX_FOR_INTERIOR(INTERIOR_V_CINEMA_DOWNTOWN, g_intCinema1)
	GET_INTERIOR_INSTANCE_INDEX_FOR_INTERIOR(INTERIOR_V_CINEMA_MORNINGWOOD, g_intCinema2)
	GET_INTERIOR_INSTANCE_INDEX_FOR_INTERIOR(INTERIOR_V_CINEMA_VINEWOOD, g_intCinema3)
	GET_INTERIOR_INSTANCE_INDEX_FOR_INTERIOR(INTERIOR_V_CHOPSHOP, g_intChopShop)
	GET_INTERIOR_INSTANCE_INDEX_FOR_INTERIOR(INTERIOR_V_EPSILONISM, g_intEpsilon2)
	//GET_INTERIOR_INSTANCE_INDEX_FOR_INTERIOR(INTERIOR_V_FOUNDRY, g_intFoundry)
	GET_INTERIOR_INSTANCE_INDEX_FOR_INTERIOR(INTERIOR_V_SWEAT, g_intSweatshop)
	GET_INTERIOR_INSTANCE_INDEX_FOR_INTERIOR(INTERIOR_V_SWEATEMPTY, g_intSweatshop2)
	
	g_intChickenFactory1 = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -76.6618, 6222.1914, 32.2412 >>, "V_factory1")
	g_intChickenFactory2 = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -98.2637, 6210.0225, 31.9240 >>, "V_factory2")
	g_intChickenFactory3 = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -115.8956, 6179.7485, 32.4102 >>, "V_factory3")
	g_intChickenFactory4 = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -149.8199, 6144.9775, 31.3353 >>, "V_factory4")
	g_intShootRange = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 16.3605, -1100.2587, 28.7970 >>, "v_gun")
	g_intStripClub = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 125.1948, -1284.1304, 28.2847 >>, "v_strip3")
	g_intMine = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-545.5717, 1987.1454, 126.0262>>, "cs6_08_mine_int")
	g_intOmega = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<2330.5984, 2571.9353, 45.6802>>, "ch3_01_trlr_int")
	g_intCarDealer = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-54.7,-1092.7,26.4>>, "v_carshowroom")
	
	//Shop robbery interiors
	int i
	FOR i = 0 to ENUM_TO_INT(NUM_SHOP_ROBBERIES_SHOPS) -1
		g_intShopRob[i] = GET_INTERIOR_AT_COORDS(GET_SHOP_ROBBERY_POSITION_BY_SHOP_INDEX(INT_TO_ENUM(SHOP_ROBBERIES_SHOP_INDEX, i)))
	ENDFOR
	
	g_bFeaturesInteriorsCached = TRUE	
ENDPROC


/// PURPOSE: 
///    Processes a list of blocked locations, returning whether the player is in any of them
FUNC FEATURE_BLOCK_REASON GET_FEATURE_BLOCKED_LOCATION_REASON(DEBUG_CHANNELS dbChannel = DEBUG_MISSION)
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<207.433578,-1019.795410,-100.472763>>, <<189.933777,-1019.623474,-95.568832>>, 17.187500)
	AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)	
		CDEBUG3LN(dbChannel, GET_THIS_SCRIPT_NAME()," -Feature Blocked- Player in vehicle in garage.")
		RETURN FBR_IN_SHOP
	ENDIF
	VECTOR vPlayerPos = GET_PLAYER_COORDS(PLAYER_ID())
	
	//Epsilon 2 door check
	IF g_RandomChars[RC_EPSILON_2].rcIsAwaitingTrigger	//Should be IS_RC_MISSION_AVAILABLE(RC_EPSILON_2) but can't include randomchar_public
		IF VDIST2(vPlayerPos, <<241.9889, 360.4732, 105.6166>>) < 2
			CDEBUG3LN(dbChannel, GET_THIS_SCRIPT_NAME()," -Feature Blocked- Player too close to Epsilon2 starting area.")
			RETURN FBR_ON_MISSION
		ENDIF
	ENDIF
	
	//B* 2272952: Block DM close to the Director Mode broken window
	IF GET_MISSION_COMPLETE_STATE(SP_MISSION_ARMENIAN_3)
	AND NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_1)
		IF VDIST2(vPlayerPos, <<-59.966285,-1099.005493,25.520878>>) < 8
			IF IS_POINT_IN_ANGLED_AREA( vplayerPos, <<-61.266285,-1096.505493,25.520878>>, <<-58.433327,-1101.147339,29.249371>>, 1.250000)
				CDEBUG3LN(dbChannel, GET_THIS_SCRIPT_NAME()," -Feature Blocked- The player is close to Simeon's dealership's broken window")
				RETURN FBR_UNSAFE_LOCATION
			ENDIF
		ENDIF
	ENDIF
	
	//Cablecar interior checks
	IF VDIST2(vPlayerPos, <<-740.93457, 5599.42627, 40.71515>>)<11
		IF IS_POINT_IN_ANGLED_AREA( vPlayerPos, <<-746.129883,5599.225586,40.475605>>, <<-737.631958,5599.363770,44.169304>>, 3.375000)
			CDEBUG3LN(dbChannel, GET_THIS_SCRIPT_NAME()," -Feature Blocked- The player is in Cablecar 1")
			RETURN FBR_UNSAFE_LOCATION
		ENDIF
	ENDIF
		IF VDIST2(vPlayerPos, <<-740.93457, 5590.42627, 40.71515>>)<11
		IF IS_POINT_IN_ANGLED_AREA( vPlayerPos, <<-746.129883,5590.667480,40.439201>>, <<-737.658508,5590.591797,44.523270>>, 3.375000)
			CDEBUG3LN(dbChannel, GET_THIS_SCRIPT_NAME()," -Feature Blocked- The player is in Cablecar 2")
			RETURN FBR_UNSAFE_LOCATION
		ENDIF
	ENDIF
	IF VDIST2(vPlayerPos, <<446.32654, 5566.35010, 780.21515>>)<11
		IF IS_POINT_IN_ANGLED_AREA( vPlayerPos, <<451.166077,5566.451172,780.170288>>, <<442.521051,5566.374023,783.981934>>, 3.375000)	
			CDEBUG3LN(dbChannel, GET_THIS_SCRIPT_NAME()," -Feature Blocked- The player is in Cablecar 3")
			RETURN FBR_UNSAFE_LOCATION
		ENDIF
	ENDIF
	IF VDIST2(vPlayerPos, <<446.32654, 5577.35010, 780.21515>>)<11
		IF IS_POINT_IN_ANGLED_AREA( vPlayerPos, <<451.166077,5577.866699,780.189880>>, <<442.576501,5577.579102,783.908630>>, 3.375000)
			CDEBUG3LN(dbChannel, GET_THIS_SCRIPT_NAME()," -Feature Blocked- The player is in Cablecar 4")
			RETURN FBR_UNSAFE_LOCATION
		ENDIF
	ENDIF
	
	//---------Various small positions - cheap checks (vdist2) preferred---------
	//Strip club doors
	IF VDIST2(vPlayerPos,<<128.83,-1297.98,29.3>>) < 2
	OR 	VDIST2(vPlayerPos,<<95.07,-1284.98,29.3>>) < 2
		CDEBUG3LN(dbChannel, GET_THIS_SCRIPT_NAME()," -Feature Blocked- The player is next to a stripclub entrance")
		RETURN FBR_UNSAFE_LOCATION
	ENDIF
	//Foundry side door
	IF VDIST2(vPlayerPos,<<1081.95056, -1976.76184, 30.47218>>) < 6
#IF NOT USE_SP_DLC
	AND IS_MISSION_AVAILABLE(SP_MISSION_FINALE_C1)
#ENDIF
		CDEBUG3LN(dbChannel, GET_THIS_SCRIPT_NAME()," -Feature Blocked- The player is next to the foundry door")
		RETURN FBR_UNSAFE_LOCATION
	ENDIF
	
	//Cache interior indices for blocked areas
	IF NOT g_bFeaturesInteriorsCached
		CACHE_FEATURE_BLOCKED_INTERIORS(dbChannel)
	ENDIF
	
	INTERIOR_INSTANCE_INDEX intPlayer = GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())
	IF intPlayer = NULL
		//CDEBUG1LN(dbChannel, " - flow_public_game - GET_FEATURE_BLOCKED_LOCATION_REASON - Player is outside so returning FBR_NONE.")
		RETURN FBR_NONE
		//Do nothing as the player is outside
	ELIF intPlayer = g_intCinema1 OR intPlayer = g_intCinema2 OR intPlayer = g_intCinema3
		CDEBUG3LN(dbChannel, GET_THIS_SCRIPT_NAME()," -Feature Blocked- The player is in a Cinema.")
		RETURN FBR_UNSAFE_LOCATION
	ELIF intPlayer = g_intSolomonOffice
		CDEBUG3LN(dbChannel, GET_THIS_SCRIPT_NAME()," -Feature Blocked- The player is in Solomon's office.")
		RETURN FBR_UNSAFE_LOCATION
	ELIF intPlayer = g_intPlayerGarage
		CDEBUG3LN(dbChannel, GET_THIS_SCRIPT_NAME()," -Feature Blocked- Feature Blocked - The player is in the Garage.")
		RETURN FBR_UNSAFE_LOCATION
	ELIF intPlayer = g_intChickenFactory1
		CDEBUG3LN(dbChannel, GET_THIS_SCRIPT_NAME()," -Feature Blocked- Feature Blocked - The player is in the Chicken Factory 1.")
		RETURN FBR_UNSAFE_LOCATION
	ELIF intPlayer = g_intChickenFactory2
		CDEBUG3LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- The player is in the Chicken Factory 2.")
		RETURN FBR_UNSAFE_LOCATION
	ELIF intPlayer = g_intChickenFactory3
		CDEBUG3LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- The player is in the Chicken Factory 3.")
		RETURN FBR_UNSAFE_LOCATION
	ELIF intPlayer = g_intChickenFactory4
		CDEBUG3LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- The player is in the Chicken Factory 4.")
		RETURN FBR_UNSAFE_LOCATION
	ELIF intPlayer = g_intShootRange
		CDEBUG3LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- The player is in the Shooting Range.")
		RETURN FBR_UNSAFE_LOCATION
	ELIF intPlayer = g_intStripClub
		CDEBUG3LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- The player is in the Strip Club.")
		RETURN FBR_UNSAFE_LOCATION
	ELIF intPlayer = g_intChopShop
		CDEBUG3LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- The player is in the Chop Shop.")
		RETURN FBR_UNSAFE_LOCATION
	ELIF intPlayer = g_intEpsilon2
		CDEBUG3LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- The player is in the Epsilon2 room.")
		RETURN FBR_UNSAFE_LOCATION
	ELIF intPlayer = g_intMine
		CDEBUG3LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- The player is in the Murder Mistery mine area.")
		RETURN FBR_UNSAFE_LOCATION
	ELIF intPlayer = g_intOmega
		CDEBUG3LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- Feature Blocked - The player is in Omega's Garage.")
		RETURN FBR_UNSAFE_LOCATION
	ELIF intPlayer = g_intCarDealer
		CDEBUG3LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- Feature Blocked - The player is in Simeon's showroom")
		RETURN FBR_UNSAFE_LOCATION
//	ELIF intPlayer = g_intFoundry
//		CDEBUG1LN(dbChannel, " - flow_public_game - GET_FEATURE_BLOCKED_LOCATION_REASON - Feature Blocked - The player is inside the foundry.")
//		RETURN FBR_UNSAFE_LOCATION
	ELIF (intPlayer = g_intSweatshop OR intPlayer = g_intSweatshop2)
		#IF NOT USE_SP_DLC 
			AND GET_MISSION_COMPLETE_STATE(SP_HEIST_AGENCY_2)
		#ENDIF
		CDEBUG3LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- The player is inside Lester's Sweatshop.")
		RETURN FBR_UNSAFE_LOCATION
	ENDIF
	
	//Check shop robbery interiors
	INT i
	FOR i = 0 to ENUM_TO_INT(NUM_SHOP_ROBBERIES_SHOPS)-1
		IF intPlayer = g_intShopRob[i]
			CDEBUG3LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- The player is in robbable shop ",i, " / ",ENUM_TO_INT(NUM_SHOP_ROBBERIES_SHOPS))
			RETURN FBR_UNSAFE_LOCATION
		ENDIF
	ENDFOR
	
	RETURN FBR_NONE
ENDFUNC


/// PURPOSE:
///    Returns whether Feature is currently blocked and why. (used in director mode, and benchmarking)
/// PARAMS:
///    bSilenceDebug - 		Block the debug logging
///    bBlockDupeFeature - 	Block check whether the script is already running.
/// RETURNS:
///    FEATURE_BLOCK_REASON - Enum reason for blocking the feature
FUNC FEATURE_BLOCK_REASON GET_FEATURE_BLOCKED_REASON(BOOL bSilenceDebug = FALSE, BOOL bIsBenchmark = FALSE, DEBUG_CHANNELS dbChannel = DEBUG_MISSION)
	dbChannel = dbChannel
	#IF IS_FINAL_BUILD
		UNUSED_PARAMETER(bSilenceDebug)
	#ENDIF

	IF IS_ENTITY_DEAD(PLAYER_PED_ID())
		#IF IS_DEBUG_BUILD
			IF NOT bSilenceDebug
				CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- Player dead / arrested.")
			ENDIF
		#ENDIF
		RETURN FBR_DEAD
	ENDIF
	
	IF bIsBenchmark
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_OF_THIS_SCRIPT_NAME()) > 1
			#IF IS_DEBUG_BUILD
				IF NOT bSilenceDebug
					CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- Feature already running.")
				ENDIF
			#ENDIF
			RETURN FBR_RUNNING_CURR
		ENDIF
	ENDIF
	
	IF IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE()
	AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
		#IF IS_DEBUG_BUILD
			IF NOT bSilenceDebug	
				CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- On mission.")
			ENDIF
		#ENDIF
		RETURN FBR_ON_MISSION
	ENDIF
	
	IF IS_RESULT_SCREEN_DISPLAYING()
		#IF IS_DEBUG_BUILD
			IF NOT bSilenceDebug	
				CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- Results screen on.")
			ENDIF
		#ENDIF
		RETURN FBR_ON_MISSION
	ENDIF
	
	IF IS_COLLECTED_SCREEN_DISPLAYING()
		#IF IS_DEBUG_BUILD
			IF NOT bSilenceDebug	
				CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- Collected screen on.")
			ENDIF
		#ENDIF
		RETURN FBR_UNSAFE_ACTION
	ENDIF
	
	IF bIsBenchmark
		IF IS_AUTOSAVE_REQUEST_IN_PROGRESS()
			#IF IS_DEBUG_BUILD
				IF NOT bSilenceDebug	
					CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- IS_AUTOSAVE_REQUEST_IN_PROGRESS.")
				ENDIF
			#ENDIF
			RETURN FBR_UNSAFE_ACTION
		ENDIF
	ENDIF
	
	IF g_bPlayerLockedInToTrigger
		#IF IS_DEBUG_BUILD
			IF NOT bSilenceDebug	
				CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- The player is locked in to triggering a mission.")
			ENDIF
		#ENDIF
		RETURN FBR_ON_MISSION
	ENDIF
	
	IF g_bCurrentlyBuyingProperty
		#IF IS_DEBUG_BUILD
			IF NOT bSilenceDebug	
				CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- The player is currently buying property.")
			ENDIF
		#ENDIF
		RETURN FBR_UNSAFE_ACTION
	ENDIF
	
	IF g_bPlayerIsInCinema
		#IF IS_DEBUG_BUILD
			IF NOT bSilenceDebug	
				CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- The player is currently entering/in the cinema")
			ENDIF
		#ENDIF
		RETURN FBR_UNSAFE_ACTION
	ENDIF
	
	// Covers the gap between being on mission + the pass screen displaying.
	IF g_bLoadedClifford
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("mission_stat_watchCLF")) > 0 
			#IF IS_DEBUG_BUILD
				IF NOT bSilenceDebug	
					CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- Mission stat watcher is running.")
				ENDIF
			#ENDIF
			RETURN FBR_ON_MISSION
		ENDIF
	ELIF g_bLoadedNorman
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("mission_stat_watchNRM")) > 0 
			#IF IS_DEBUG_BUILD
				IF NOT bSilenceDebug	
					CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- Mission stat watcher is running.")
				ENDIF
			#ENDIF
			RETURN FBR_ON_MISSION
		ENDIF
	ELSE
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("mission_stat_watcher")) > 0 
			#IF IS_DEBUG_BUILD
				IF NOT bSilenceDebug	
					CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- Mission stat watcher is running.")
				ENDIF
			#ENDIF
			RETURN FBR_ON_MISSION
		ENDIF
	ENDIF
	
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("finale_choice")) > 0 
		#IF IS_DEBUG_BUILD
			IF NOT bSilenceDebug	
				CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- The Finale Choice script is running.")
			ENDIF
		#ENDIF
		RETURN FBR_ON_MISSION
	ENDIF
	
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("creator")) > 0 
		#IF IS_DEBUG_BUILD
			IF NOT bSilenceDebug	
				CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- Creator is running.")
			ENDIF
		#ENDIF
		RETURN FBR_ON_MISSION
	ENDIF
	
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("respawn_controller")) > 0 
		#IF IS_DEBUG_BUILD
			IF NOT bSilenceDebug	
				CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- Respawn controller is running (death/arrest).")
			ENDIF
		#ENDIF
		RETURN FBR_DEAD
	ENDIF
	
	IF IS_REPEAT_PLAY_ACTIVE(bIsBenchmark)
		#IF IS_DEBUG_BUILD
			IF NOT bSilenceDebug	
				CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- Repeat play in progress.")
			ENDIF
		#ENDIF
		RETURN FBR_ON_MISSION
	ENDIF
	
	//Falling/skydiving
	IF IS_PED_FALLING(PLAYER_PED_ID()) OR IS_PED_RAGDOLL(PLAYER_PED_ID())
		#IF IS_DEBUG_BUILD
			IF NOT bSilenceDebug	
				CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- Player is falling.")
			ENDIF
		#ENDIF
		RETURN FBR_FALLING
	ENDIF
	
	IF IS_BITMASK_AS_ENUM_SET(g_StripclubGlobals.iStripclubFlags, GSF_GETTING_LAP_DANCE)
		#IF IS_DEBUG_BUILD
			IF NOT bSilenceDebug	
				CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- Player getting a lapdance.")
			ENDIF
		#ENDIF
		RETURN FBR_ON_MISSION
	ENDIF
	
	//In any shop interior
	IF (IS_PLAYER_IN_ANY_SHOP() OR IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP() OR IS_ANY_SHOP_INTRO_RUNNING() OR IS_PLAYER_IN_BARBER_CHAIR_OR_DOING_BARBER_INTRO())
		#IF IS_DEBUG_BUILD
			IF NOT bSilenceDebug	
				CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- Player is inside a shop.")
			ENDIF
		#ENDIF
		RETURN FBR_IN_SHOP
	ENDIF
	
	//Browsing specific shops
	IF IS_PLAYER_IN_BARBER_CHAIR_OR_DOING_BARBER_INTRO()
		#IF IS_DEBUG_BUILD
			IF NOT bSilenceDebug	
				CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- Player gettting haircut (or in barber intro).")
			ENDIF
		#ENDIF
		RETURN FBR_IN_SHOP
	ENDIF
	
	IF IS_PLAYER_BROWSING_MOD_SHOP()
		#IF IS_DEBUG_BUILD
			IF NOT bSilenceDebug	
				CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- Player is inside a mod shop.")
			ENDIF
		#ENDIF
		RETURN FBR_IN_SHOP
	ENDIF
	
	IF IS_PLAYER_BROWSING_TATTOO_SHOP()
		#IF IS_DEBUG_BUILD
			IF NOT bSilenceDebug	
				CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- Player browsing tattoo shop.")
			ENDIF
		#ENDIF
		RETURN FBR_IN_SHOP
	ENDIF
	
	IF IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
		#IF IS_DEBUG_BUILD
			IF NOT bSilenceDebug	
				CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- Player browsing in a shop.")
			ENDIF
		#ENDIF
		RETURN FBR_IN_SHOP
	ENDIF
	
	IF IS_CUSTOM_MENU_ON_SCREEN()
	#IF FEATURE_SP_DLC_DIRECTOR_MODE
	AND NOT g_bDirectorPIMenuUp
	#ENDIF
		#IF IS_DEBUG_BUILD
			IF NOT bSilenceDebug	
				CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- Player using a custom menu.")
			ENDIF
		#ENDIF
		RETURN FBR_UNSAFE_ACTION
	ENDIF
	
	IF IS_ANY_SHOP_INTRO_RUNNING()
		#IF IS_DEBUG_BUILD
			IF NOT bSilenceDebug	
				CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- Shop intro is running.")
			ENDIF
		#ENDIF
		RETURN FBR_IN_SHOP
	ENDIF
	
	IF IS_PLAYER_SWITCH_IN_PROGRESS()
	OR Is_Player_Timetable_Scene_In_Progress()
		#IF IS_DEBUG_BUILD
			IF NOT bSilenceDebug	
				CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- Player switch is running.")
			ENDIF
		#ENDIF
		RETURN FBR_IN_CUTSCENE
	ENDIF
	
	IF IS_TRANSITION_ACTIVE()
		#IF IS_DEBUG_BUILD
			IF NOT bSilenceDebug	
				CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- IS_TRANSITION_ACTIVE.")
			ENDIF
		#ENDIF
		RETURN FBR_IN_CUTSCENE
	ENDIF
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			//Location checks
			FEATURE_BLOCK_REASON br = GET_FEATURE_BLOCKED_LOCATION_REASON()
			IF br <> FBR_NONE
				#IF IS_DEBUG_BUILD
					IF NOT bSilenceDebug	
						CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- Unsuitable location.")
					ENDIF
				#ENDIF
				RETURN br
			ENDIF
			
			IF IS_PED_IN_PARACHUTE_FREE_FALL(PLAYER_PED_ID())
			OR GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) != PPS_INVALID
				#IF IS_DEBUG_BUILD
					IF NOT bSilenceDebug	
						CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- The player is parachuting.")
					ENDIF
				#ENDIF
				RETURN FBR_WEARING_PARA
			ENDIF
			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
			OR g_bPlayerIsInTaxi
				#IF IS_DEBUG_BUILD
					IF NOT bSilenceDebug	
						CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- The player is in a vehicle.")
					ENDIF
				#ENDIF
				RETURN FBR_IN_VEHICLE
			ENDIF
		ENDIF
	ENDIF
	
	//Vending machine check
	IF g_bCurrentlyUsingVendingMachine
		#IF IS_DEBUG_BUILD
			IF NOT bSilenceDebug	
				CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- Player using a vending machine.")
			ENDIF
		#ENDIF
		RETURN FBR_UNSAFE_ACTION
	ENDIF
	
	IF g_bScriptsSetSafeForCutscene
		#IF IS_DEBUG_BUILD
			IF NOT bSilenceDebug	
				CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- Scripts are set safe for cutscene.")
			ENDIF
		#ENDIF
		RETURN FBR_IN_CUTSCENE
	ENDIF
	
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
		OR IS_PLAYER_BEING_ARRESTED(PLAYER_ID())
			#IF IS_DEBUG_BUILD
				IF NOT bSilenceDebug	
					CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- The player has a wanted rating.")
				ENDIF
			#ENDIF
			RETURN FBR_WANTED
		ENDIF
		
		IF IS_PLAYER_CLIMBING(PLAYER_ID())
			#IF IS_DEBUG_BUILD
				IF NOT bSilenceDebug	
					CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- The player is climbing.")
				ENDIF
			#ENDIF
			RETURN FBR_UNSAFE_ACTION
		ENDIF
		
		IF IS_PED_GETTING_UP(PLAYER_PED_ID())
			#IF IS_DEBUG_BUILD
				IF NOT bSilenceDebug	
					CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- The player is getting up.")
				ENDIF
			#ENDIF
			RETURN FBR_UNSAFE_ACTION
		ENDIF
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		#IF IS_DEBUG_BUILD
			IF NOT bSilenceDebug	
				CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- The player is in an online session.")
			ENDIF
		#ENDIF
		RETURN FBR_ONLINE
	ENDIF
	
	IF IS_PLAYER_USING_ATM()
		#IF IS_DEBUG_BUILD
			IF NOT bSilenceDebug	
				CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- The player is using an ATM.")
			ENDIF
		#ENDIF
		RETURN FBR_UNSAFE_ACTION
	ENDIF
	
	//On Rampage mission
	IF g_bIsOnRampage
		#IF IS_DEBUG_BUILD
			IF NOT bSilenceDebug	
				CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- The player is locked into a rampage mission.")
			ENDIF
		#ENDIF
		RETURN FBR_ON_MISSION
	ENDIF
	
	//Locked in a Gameplay Hint cam
	IF IS_GAMEPLAY_HINT_ACTIVE()
		#IF IS_DEBUG_BUILD
			IF NOT bSilenceDebug	
				CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- The player is locked into a Hint Cam.")
			ENDIF
		#ENDIF
		RETURN FBR_IN_CUTSCENE
	ENDIF
	
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),<<439.4357, -997.4747, 28.9584>>,<<428.3288, -997.0398, 24.8372>>,8)
		#IF IS_DEBUG_BUILD
			IF NOT bSilenceDebug	
				CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- The player is in impound garage")
			ENDIF
		#ENDIF
		RETURN FBR_UNSAFE_LOCATION
	ENDIF
	
	IF IS_ENTITY_IN_WATER(player_ped_id())
		#IF IS_DEBUG_BUILD
			IF NOT bSilenceDebug	
				CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- The player is in water.")
			ENDIF
		#ENDIF
		RETURN FBR_UNSAFE_LOCATION
	ENDIF

	IF bIsBenchmark
		IF NOT SAFE_TO_SAVE_GAME()
			#IF IS_DEBUG_BUILD
				IF NOT bSilenceDebug	
					CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- NOT SAFE_TO_SAVE_GAME.")
				ENDIF
			#ENDIF
			RETURN FBR_UNSAFE_ACTION
		ENDIF
	ENDIF
	
	IF IS_PED_ON_VEHICLE(PLAYER_PED_ID())
		#IF IS_DEBUG_BUILD
			IF NOT bSilenceDebug	
				CDEBUG1LN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocked- IS_PED_ON_VEHICLE")
			ENDIF
		#ENDIF
		RETURN FBR_UNSAFE_LOCATION
	ENDIF
	
	//CDEBUG3LN(dbChannel, " - flow_public_game - GET_FEATURE_MODE_BLOCKED_REASON - Okay to launch feature.")
	RETURN FBR_NONE
ENDFUNC

#IF IS_DEBUG_BUILD
	FUNC STRING DEBUG_GET_FEATURE_BLOCK_REASON_STRING(FEATURE_BLOCK_REASON eBlockReason, DEBUG_CHANNELS dbChannel = DEBUG_MISSION)
		SWITCH eBlockReason
			CASE FBR_NONE				RETURN "NONE"				BREAK
			CASE FBR_FORCE_CLEANUP		RETURN "FORCE_CLEANUP"		BREAK
			CASE FBR_RUNNING_CURR		RETURN "RUNNING_CURR"		BREAK
			CASE FBR_DEAD				RETURN "DEAD"				BREAK
			CASE FBR_ON_MISSION			RETURN "ON_MISSION" 		BREAK
			CASE FBR_IN_SHOP			RETURN "IN_SHOP"			BREAK
			CASE FBR_IN_CUTSCENE		RETURN "IN_CUTSCENE"		BREAK
			CASE FBR_WANTED				RETURN "WANTED"				BREAK
			CASE FBR_ONLINE				RETURN "ONLINE"				BREAK
			CASE FBR_UNSAFE_ACTION		RETURN "UNSAFE_ACTION"		BREAK
			CASE FBR_UNSAFE_LOCATION	RETURN "UNSAFE_LOCATION"	BREAK
			CASE FBR_IN_VEHICLE			RETURN "IN_VEHICLE"			BREAK
			CASE FBR_WEARING_PARA		RETURN "WEARING_PARA"		BREAK
			CASE FBR_FALLING			RETURN "FALLING"			BREAK
		ENDSWITCH

		CASSERTLN(dbChannel,GET_THIS_SCRIPT_NAME()," -Feature Blocking- Block reason has no debug string defined.")
		RETURN "INVALID!"
	ENDFUNC
#ENDIF


