FUNC BOOL MAINTAIN_CURRENT_MISSION_FLOW_COMMAND_CUSTOM(FLOW_COMMAND_IDS eCommand, INT iStorageIndex, STRANDS paramStrandID, JUMP_LABEL_IDS &eNextJumpLabel)

	SWITCH (eCommand)
		// Add a new contact to a playable character's phone.
		CASE FLOW_ADD_PHONE_CONTACT
			eNextJumpLabel = PERFORM_ADD_PHONE_CONTACT(iStorageIndex)
			BREAK
			
		// Add a new contact to a playable character's phone.
		CASE FLOW_REMOVE_PHONE_CONTACT
			eNextJumpLabel = PERFORM_REMOVE_PHONE_CONTACT(iStorageIndex)
			BREAK
			
		// Add a new contact to a playable character's Friend.
		CASE FLOW_ADD_NEW_FRIEND_CONTACT
			eNextJumpLabel = PERFORM_ADD_NEW_FRIEND_CONTACT(iStorageIndex)
			BREAK
			
		// Block a contact to a playable character's Friend.
		CASE FLOW_BLOCK_FRIEND_CONTACT
			eNextJumpLabel = PERFORM_BLOCK_FRIEND_CONTACT(iStorageIndex)
			BREAK
			
		// Block a clashing contact to a playable character's Friend.
		CASE FLOW_BLOCK_FRIEND_CLASH
			eNextJumpLabel = PERFORM_BLOCK_FRIEND_CLASH(iStorageIndex)
			BREAK
			
		// Set a contact and playable character's Friend as busy.
		CASE FLOW_FRIEND_CONTACT_BUSY
			eNextJumpLabel = PERFORM_FRIEND_CONTACT_BUSY(iStorageIndex)
			BREAK
			
		// Check if the player is phoning a specific character.
		CASE FLOW_IS_PLAYER_PHONING_NPC
			eNextJumpLabel = PERFORM_IS_PLAYER_PHONING_NPC(iStorageIndex)
			BREAK
			
		// Player phones an NPC.
		CASE FLOW_PLAYER_PHONE_NPC
			eNextJumpLabel = PERFORM_PLAYER_PHONE_NPC(iStorageIndex)
			BREAK
			
		// Player phones an NPC with a different ending conversation based on a flow check.
		CASE FLOW_PLAYER_PHONE_NPC_WITH_BRANCH
			eNextJumpLabel = PERFORM_PLAYER_PHONE_NPC_WITH_BRANCH(iStorageIndex)
			BREAK
			
		// Phone the player.
		CASE FLOW_PHONE_PLAYER
			eNextJumpLabel = PERFORM_PHONE_PLAYER(iStorageIndex)
			BREAK
			
		// Phone the player with a different ending conversation based on a flow check.
		CASE FLOW_PHONE_PLAYER_WITH_BRANCH
			eNextJumpLabel = PERFORM_PHONE_PLAYER_WITH_BRANCH(iStorageIndex)
			BREAK
			
		// Phone the player with "was answered" Jumps.
		CASE FLOW_PHONE_PLAYER_WITH_JUMPS
			eNextJumpLabel = PERFORM_PHONE_PLAYER_WITH_JUMPS(iStorageIndex)
			BREAK
			
		// Phone the player but with a Yes/No option.
		CASE FLOW_PHONE_PLAYER_YES_NO
			eNextJumpLabel = PERFORM_PHONE_PLAYER_YES_NO(iStorageIndex)
			BREAK
			
		// Send the player a text message.
		CASE FLOW_TEXT_PLAYER
			eNextJumpLabel = PERFORM_TEXT_PLAYER(iStorageIndex)
			BREAK
			
		// Send the player an email.
		CASE FLOW_EMAIL_PLAYER
			eNextJumpLabel = PERFORM_EMAIL_PLAYER(iStorageIndex)
			BREAK
			
		// Cancel an unsent phonecall.
		CASE FLOW_CANCEL_PHONECALL
			eNextJumpLabel = PERFORM_CANCEL_PHONECALL(iStorageIndex)
			BREAK
			
		// Cancel an unsent text.
		CASE FLOW_CANCEL_TEXT
			eNextJumpLabel = PERFORM_CANCEL_TEXT(iStorageIndex)
			BREAK
		
		CASE FLOW_SET_CHAR_CHAT_PHONECALL
			eNextJumpLabel = PERFORM_SET_CHAR_CHAT_PHONECALL(iStorageIndex)
			BREAK
			
		// Setup and perform a mission launched immediately, waiting for pass or fail
		CASE FLOW_ADD_ORGANISER_EVENT
			eNextJumpLabel = PERFORM_ADD_ORGANISER_EVENT(iStorageIndex)
			BREAK
		
		// Setup and perform a mission launched immediately, waiting for pass or fail
		CASE FLOW_DO_BOARD_INTERACTION
			eNextJumpLabel = PERFORM_DO_BOARD_INTERACTION(iStorageIndex)
			BREAK
			
		// Perform a Jump based on whether the player is current playing as the specified character
		CASE FLOW_IS_MISSION_COMPLETED
			eNextJumpLabel = PERFORM_IS_MISSION_COMPLETED(iStorageIndex)
			BREAK
			
		// Perform a Jump based on whether the player is current playing as the specified character
		CASE FLOW_IS_PLAYING_AS_CHARACTER
			eNextJumpLabel = PERFORM_IS_PLAYING_AS_CHARACTER(iStorageIndex)
			BREAK
			
		// Perform a Jump based on whether the player is currently switching to the specified character.
		CASE FLOW_IS_SWITCHING_TO_CHARACTER
			eNextJumpLabel = PERFORM_IS_SWITCHING_TO_CHARACTER(iStorageIndex)
			BREAK
			
		// Hold flow until any autosaves have finished processing.
		CASE FLOW_WAIT_FOR_AUTOSAVE	
			eNextJumpLabel = PERFORM_WAIT_FOR_AUTOSAVE()
			BREAK
			
		CASE FLOW_SET_BOARD_DISPLAY_GROUP_VISIBILITY
			eNextJumpLabel = PERFORM_SET_BOARD_DISPLAY_GROUP_VISIBILITY(iStorageIndex)
			BREAK
			
		CASE FLOW_SET_DOOR_STATE
			eNextJumpLabel = PERFORM_SET_DOOR_STATE(iStorageIndex)
			BREAK
			
		CASE FLOW_RESET_PRIORITY_SWITCH_SCENES
			eNextJumpLabel = PERFORM_RESET_PRIORITY_SWITCH_SCENES(iStorageIndex)
			BREAK
			
		CASE FLOW_SET_PED_REQUEST_SCENE
			eNextJumpLabel = PERFORM_SET_PED_REQUEST_SCENE(iStorageIndex)
			BREAK
			
		CASE FLOW_SET_CHAR_HOTSWAP_AVAILABILITY
			eNextJumpLabel = PERFORM_SET_CHAR_HOTSWAP_AVAILABILITY(iStorageIndex)
			BREAK
			
		CASE FLOW_SET_PLAYER_CHARACTER
			eNextJumpLabel = PERFORM_SET_PLAYER_CHARACTER(iStorageIndex)
			BREAK
			
		CASE FLOW_SET_SAVEHOUSE_STATE
			eNextJumpLabel = PERFORM_SET_SAVEHOUSE_STATE(iStorageIndex)
			BREAK
			
		CASE FLOW_SET_SHOP_STATE
			eNextJumpLabel = PERFORM_SET_SHOP_STATE(iStorageIndex)
			BREAK
			
		CASE FLOW_SET_WEAPON_COMP_LOCK_STATE
			eNextJumpLabel = PERFORM_SET_WEAPON_COMP_LOCK_STATE(iStorageIndex)
			BREAK
			
		CASE FLOW_SET_VEHICLE_GEN_STATE
			eNextJumpLabel = PERFORM_SET_VEHICLE_GEN_STATE(iStorageIndex)
			BREAK
			
		CASE FLOW_SET_SPECIAL_ABILITY_UNLOCK_STATE
			eNextJumpLabel = PERFORM_SET_SPECIAL_ABILITY_UNLOCK_STATE(iStorageIndex)
			BREAK
			
		CASE FLOW_SET_MISSION_PASSED
			eNextJumpLabel = PERFORM_SET_MISSION_PASSED(iStorageIndex)
			BREAK
			
		CASE FLOW_SET_MISSION_NOT_PASSED
			eNextJumpLabel = PERFORM_SET_MISSION_NOT_PASSED(iStorageIndex)
			BREAK

		CASE FLOW_TOGGLE_BOARD_VIEWING
			eNextJumpLabel = PERFORM_TOGGLE_BOARD_VIEWING(iStorageIndex)
			BREAK
		
		CASE FLOW_IS_PLAYER_IN_AREA
			eNextJumpLabel = PERFORM_IS_PLAYER_IN_AREA(iStorageIndex)
			BREAK
			
		CASE FLOW_DO_PLAYER_BANK_ACTION
			eNextJumpLabel = PERFORM_DO_PLAYER_BANK_ACTION(iStorageIndex)
			BREAK
		#if not USE_CLF_DLC	
		#if not USE_NRM_DLC
		CASE FLOW_IS_SHRINK_SESSION_AVAILABLE
			eNextJumpLabel = PERFORM_IS_SHRINK_SESSION_AVAILABLE(iStorageIndex)
			BREAK
		#endif
		#endif
		CASE FLOW_DO_MISSION_AT_SWITCH
			eNextJumpLabel = PERFORM_DO_MISSION_AT_SWITCH(paramStrandID, iStorageIndex)
			BREAK
			
		CASE FLOW_SET_BIKES_SUPRESSED_STATE
			eNextJumpLabel = PERFORM_SET_BIKES_SUPRESSED_STATE(iStorageIndex)
			BREAK
			
		CASE FLOW_SET_FRANKLIN_OUTFIT_BITSET_STATE
			eNextJumpLabel = PERFORM_SET_FRANKLIN_OUTFIT_BITSET_STATE(iStorageIndex)
			BREAK
			
		CASE FLOW_FAST_TRACK_STRAND
			g_flowUnsaved.bFlowFastTrackCurrentStrand = TRUE
			eNextJumpLabel = NEXT_SEQUENTIAL_COMMAND
			BREAK
			
		CASE FLOW_DO_CUSTOM_CHECK_JUMPS
			eNextJumpLabel = PERFORM_DO_CUSTOM_CHECK_JUMPS(iStorageIndex)
			BREAK
			
		CASE FLOW_SET_BUYABLE_VEHICLE_STATE
			eNextJumpLabel = PERFORM_SET_BUYABLE_VEHICLE_STATE(iStorageIndex)
			BREAK
			
		CASE FLOW_SET_COMPONENT_ACQUIRED
			eNextJumpLabel = PERFORM_SET_COMPONENT_ACQUIRED(iStorageIndex)
			BREAK
			
		CASE FLOW_DO_SWITCH_TO_CHARACTER
			eNextJumpLabel = PERFORM_DO_SWITCH_TO_CHARACTER(iStorageIndex)
			BREAK
			
		CASE FLOW_SET_STRAND_SAVE_OVERRIDE
			eNextJumpLabel = PERFORM_SET_STRAND_SAVE_OVERRIDE(iStorageIndex, paramStrandID)
			BREAK
			
		CASE FLOW_CLEAR_STRAND_SAVE_OVERRIDE
			eNextJumpLabel = PERFORM_CLEAR_STRAND_SAVE_OVERRIDE(paramStrandID)
			BREAK

		DEFAULT
			RETURN FALSE
			BREAK
	ENDSWITCH
	
	RETURN TRUE	
ENDFUNC
