#IF NOT DEFINED(FLOW_COMMAND_IDS)
CONST_INT   ONLY_CUSTOM_COMMANDS      1
ENUM FLOW_COMMAND_IDS
#ENDIF
	// Modern Game Commands
	FLOW_ADD_PHONE_CONTACT,					// Unlocks a new contact on one of the playable character's phones.
	FLOW_REMOVE_PHONE_CONTACT,				// Removes a contact from one of the playable character's phones.
	
	FLOW_ADD_NEW_FRIEND_CONTACT,			// Unlocks a new friend contact for one of the playable characters.
	FLOW_BLOCK_FRIEND_CONTACT,				// Unlocks a new friend contact from being contactable. 
	FLOW_BLOCK_FRIEND_CLASH,				// Unlocks a new friend contact from being contactable. 
	FLOW_FRIEND_CONTACT_BUSY,
	
	FLOW_IS_PLAYER_PHONING_NPC,				// Wait for the player to make a call to a specific NPC.
	FLOW_PLAYER_PHONE_NPC,					// Force the player to make a phonecall to an NPC.
	FLOW_PLAYER_PHONE_NPC_WITH_BRANCH,		// Force the player to make a phonecall to an NPC with two different endings based on a a flow check.
	FLOW_PHONE_PLAYER,						// Ped makes a phonecall to the player
	FLOW_PHONE_PLAYER_WITH_BRANCH,			// Ped makes a call to the player with two different endings based on a a flow check.	
	FLOW_PHONE_PLAYER_YES_NO,				// Ped makes a YES/NO phonecall to the player
	FLOW_PHONE_PLAYER_WITH_JUMPS,			// Ped makes a phonecall to the player and flow jumps to label based on whether the player answers it.
	FLOW_TEXT_PLAYER,						// Ped sends a text message to the player (either 'WaitUntilSent' or 'TextAndForget')
	FLOW_EMAIL_PLAYER,						// Send an email to the player.
	FLOW_CANCEL_PHONECALL,					// Cancel an unsent phonecall (delete it from the communication controller).
	FLOW_CANCEL_TEXT,						// Cancel an unsent text message (delete it from the communication controller).
	FLOW_SET_CHAR_CHAT_PHONECALL,			// Sets up a chat phonecall for a specific NPC character.

	// GTA Specific
	FLOW_ADD_ORGANISER_EVENT,				// Adds an event with a time and date to a player character's organiser. They will receive reminders on their phone for this event.
	FLOW_DO_BOARD_INTERACTION,				// Sets this strand to wait for a specific planning board interaction to be completed.
	FLOW_IS_PLAYING_AS_CHARACTER,			// Performs either a TRUE or a FALSE jump based on whether the player is currently playing as the specified character.
	FLOW_IS_SWITCHING_TO_CHARACTER,			// Has the player started a switch to a specific character?
	FLOW_SET_BOARD_DISPLAY_GROUP_VISIBILITY,// Allows the flow to modify the visibility of a group of items on a planning board.
	FLOW_SET_CHAR_HOTSWAP_AVAILABILITY,		// Sets whether or not a playable character can be hotswapped to while off mission.s
	FLOW_SET_HOTSWAP_STAGE_FLAG,			// Sets a hotswap timetable stage flag to ensure ambient hotswap locations make sense for the current point in the flow.
	FLOW_SET_PLAYER_CHARACTER,				// Swaps the current player character to a specified playable character.
	FLOW_SET_SPECIAL_ABILITY_UNLOCK_STATE,	// Sets whether a characters special ability has been unlocked.
	FLOW_TOGGLE_BOARD_VIEWING,				// Tells this strand to turn a board view on or off for a heist's planning board.
	FLOW_SET_SAVEHOUSE_STATE,				// Sets whether a savehouse has been introduced to the game or not (also displays blip).
	FLOW_SET_SHOP_STATE,					// Sets whether a shop has been introduced to the game or not (also displays blip).
	FLOW_SET_WEAPON_COMP_LOCK_STATE,		// Sets whether a weapon component should be unlocked or not.
	FLOW_SET_VEHICLE_GEN_STATE,				// Sets whether a vehicle gen is active or not.
	FLOW_SET_DOOR_STATE,
	FLOW_RESET_PRIORITY_SWITCH_SCENES,		// Resets the flag to set the switch scheduler to select a priority scene
	FLOW_SET_PED_REQUEST_SCENE,				// Sets the ped request scene for a player char
	FLOW_SET_MISSION_PASSED,				// Sets a mission in the game to the completed state without the player having to play it.
	FLOW_SET_MISSION_NOT_PASSED,			// Sets a mission in the game to the uncompleted state.
	FLOW_IS_PLAYER_IN_AREA,					// Blocks the flow until the player has left a specific area.
	FLOW_DO_PLAYER_BANK_ACTION,				// Allows the flow to credit/debit money into/from a playable character's bank account.	
	FLOW_IS_SHRINK_SESSION_AVAILABLE,		// Checks to see if there is any valid shrink dialogue available for a given shrink session.
	FLOW_DO_MISSION_AT_SWITCH,				// Allows a mission to trigger when the player initiates a switch to a specific playable character.
	FLOW_SET_BIKES_SUPRESSED_STATE,			// Sets whether motorbikes are available in general vehicle population.
	FLOW_SET_FRANKLIN_OUTFIT_BITSET_STATE,	// Sets whether Franklin's outfit changes are being tracked.
	FLOW_FAST_TRACK_STRAND,					// Keep updating this flow strand until it hits a blocking command.
	FLOW_DO_CUSTOM_CHECK_JUMPS,				// Perform jumps based on the boolean result of a custom function.
	FLOW_SET_BUYABLE_VEHICLE_STATE,         // Sets whether a website vehicle has been purchased or not.
	FLOW_SET_COMPONENT_ACQUIRED,			// Sets an outfit as acquired makingit available in the player's wardrboe
	FLOW_DO_SWITCH_TO_CHARACTER,			// Performs an ambient player switch.
	FLOW_SET_STRAND_SAVE_OVERRIDE,			// Set a strand command pointer override to return to if the game is saved before the strand clears it.
	FLOW_CLEAR_STRAND_SAVE_OVERRIDE,		// Clear a strand command pointer override to allow a strand to save naturally again.
	
	// New commands added for multiplayer. 
	FLOW_MP_WAIT_UNTIL_RANK,				// For The Multiplayer Flow, wait untill a certain rank is acheived.
	FLOW_MP_PUSH_TOWARDS_WEATHER_TYPE,		// For The Multiplayer Flow, Change weather type
	FLOW_MP_CONTINUE_MAIN,		// For The Multiplayer Flow, Will set timmed mission to be active
	FLOW_MP_SET_TUTORIAL_SESSION_STATE,		// For The Multiplayer Flow, Will set the tutorial session state
	FLOW_MP_SET_MGROUP_ACTIVE,				// For The Multiplayer Flow, Sets a mission Group active
	FLOW_MP_SET_MISSION_ACTIVE_IN_MGROUP,	// For The Multiplayer Flow, Sets a mission active in a mission Group active
	FLOW_MP_SETUP_NEXT_MGROUP_MISSION,		// For The Multiplayer Flow, Setsup the next mission 
	FLOW_MP_UPDATE_MGROUP_ORDER,			// For The Multiplayer Flow, Sets up the order of the next mission group.
	FLOW_MP_WAIT_UNTIL_ON_MISSION,			// For The Multiplayer Flow, Flow is paused untill the player is on the given mission.
	FLOW_MP_SET_STAT,						// For The Multiplayer Flow, Set the passed in STAT
	FLOW_MP_DO_RACES, 						// For The Multiplayer Flow, To deal with the races
	FLOW_MP_AWAIT_EXTERNAL_INFLUENCE, 		// For The Multiplayer Flow, To deal with the races
	
	// New commands added for DLC.
	FLOW_SET_ASSASSINATION_STAGE_DLC,
	
#IF DEFINED(ONLY_CUSTOM_COMMANDS)
    // Leave this at the bottom.
    FLOW_NONE = -1
ENDENUM
#ENDIF
